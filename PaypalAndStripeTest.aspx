﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true"
     CodeFile="PaypalAndStripeTest.aspx.cs" Inherits="PaypalAndStripeTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

    <div style="padding: 50px; min-height: 300px;">
        <%--<asp:Panel runat="server" ID="pnlStripButton">
            <script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="<%= stripePublishableKey %>"
                data-amount="10000"
                data-currency="AUD"
                data-name="DBGurus.com.au"
                data-description="eContractor Monthly Charge"
                
                data-locale="auto"
                data-zip-code="false"
                data-panel-label="Pay Monthly {{amount}}">
            </script>
        </asp:Panel>--%>


       <table>
           <tr>
               <td>
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnPaypal" Text="Recurring Pay by Paypal" OnClick="btnPaypal_Click" />

                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnPayOneTime" Text="One time Pay by Paypal" OnClick="btnPayOneTime_Click" />

                   <br />

                   <asp:Label runat="server" ID="lblMessage"></asp:Label>
                   <br />

                   <br />
                   <br />
                  
                   <%--<asp:Button runat="server" ID="btnDllPayment" Text="dll - Create a Payment " OnClick="btnDllPayment_Click" />--%>
                   

                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnPP_Del_Subscription" Text="Paypal delete subscription" OnClick="btnPP_Del_Subscription_Click" />

                   <br />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnCheckAPI" Text="API Get Access" OnClick="btnCheckAPI_Click" />
                   <br />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnAPI_CreateProduct" Text="API - create product" OnClick="btnAPI_CreateProduct_Click" />

                   <br />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnAPI_ListProduct" Text="API - list product" OnClick="btnAPI_ListProduct_Click" />

                   <br />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnAPI_CreatePlan" Text="API - create Plan" OnClick="btnAPI_CreatePlan_Click" />

                   <br />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnAPI_CreateMonthlyPlan" Text="API - create monthly Plan" OnClick="btnAPI_CreateMonthlyPlan_Click" />

                   <br />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnAPI_CreateYearlyPaln" Text="API - create yearly Plan" OnClick="btnAPI_CreateYearlyPaln_Click" />

                   <br />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnAPI_ViewPlan" Text="API - View Plan" OnClick="btnAPI_ViewPlan_Click" />

                   <br />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnAPI_UpdatePlan" Text="API - Update Plan" OnClick="btnAPI_UpdatePlan_Click" />



                    <br />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnAPI_ListPlan" Text="API - List Plan" OnClick="btnAPI_ListPlan_Click" />

                   <br />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnAPI_UpdateSubscription" Text="API - Update Subscription" OnClick="btnAPI_UpdateSubscription_Click" />


                   
                   <br />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnGetWebProfile" Text="API - Get web-profiles" OnClick="btnGetWebProfile_Click" />

                   

                   <br />
               </td>
               <td style="width:50px;">

               </td>
               <td>
                   <asp:Button runat="server" ID="btnGetAPaymentByAPI" Text="API- Get A Payment" OnClick="btnGetAPaymentByAPI_Click" />
                   <%--<br />
                   <br />
                   <br />--%>
                    <%--<asp:Button runat="server" ID="btnGetPaymentByService" Text="Service - Get payment info" OnClick="btnGetPaymentByService_Click" />--%>

                   <%--<br />
                   <br />
                   <br />--%>
                   <%--<asp:Button runat="server" ID="btnGetPaymentByDLL" Text="DLL - Get a payment info" OnClick="btnGetPaymentByDLL_Click" />--%>

                   <br />
                   <br />
                   <br />

               </td>

               <td style="width:50px;"></td>
               <td>
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnCleanCustomer" Text="Clean stripe Customers" OnClick="btnCleanCustomer_Click" />

                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnCutomersManagement" Text="Stripe Customers" OnClick="btnCutomersManagement_Click" />

                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnStripeSubscription" Text="Stripe payment by subscription" OnClick="btnStripeSubscription_Click" />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnStripeSinglePayment" Text="Stripe Single Payment" OnClick="btnStripeSinglePayment_Click" />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnStripeUpdateSubscription" Text="Stripe update subscription" OnClick="btnStripeUpdateSubscription_Click" />

                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnStripeFindCustomerAndSubscription" Text="Stripe Find Customer And Subscription" OnClick="btnStripeFindCustomerAndSubscription_Click"/>

                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnStripeNextUpcomingInvoice"
                       Text="Stripe Next Upcoming Invoice" OnClick="btnStripeNextUpcomingInvoice_Click" />
                   <br />
                   <br />
                   <asp:Button runat="server" ID="btnStripeDeleteSubscriptionAndCreateAgain"
                       Text="Stripe Delete Subscription And Create Again" OnClick="btnStripeDeleteSubscriptionAndCreateAgain_Click" />


               </td>

           </tr>
       </table>
        



    </div>


</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Text;

public class JSONValue : IJSONItem
{
    private string name;
    private object value;

    public JSONValue(string name, object value)
    {
        this.name = name;
        this.value = value;
    }

    string Name
    {
        get { return name; }
    }

    object Value
    {
        get { return value; }
    }

    public override string ToString()
    {
        StringBuilder output = new StringBuilder();

        if (!String.IsNullOrWhiteSpace(name))
        {
            output.Append("\"" + name + "\":");
        }

        if (value == null)
        {
            output.Append("\"\"");
        }
        else
        {
            output.Append("\"" + JSONClean(value.ToString()) + "\"");
        }

        return output.ToString();
    }

    private string JSONClean(string data)
    {
        if (!String.IsNullOrEmpty(data))
        {
            data = data.Replace("\r", "<br />");
            data = data.Replace("\n", "<br />");
            data = data.Replace("\t", "");

            // Replace the \ character with \\
            data = data.Replace(@"\", @"\\");

            // Replace the " character with \"
            string slashQuote = "\\" + '"';
            data = data.Replace("\"", slashQuote);

            // Replace HTML symbols.

            data = data.Replace("%20", " ");
            data = data.Replace("%2F", "/");
        }

        return data;
    }
}
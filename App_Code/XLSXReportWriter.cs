﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace XlsxExport
{
    public class XlsxReportWriter
    {
        public XlsxReportWriter()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public void CreateWorkbook(MemoryStream stream, int reportId, DateTime? reportStartDate, DateTime? reportEndDate, int AccountID)
        {
            DataTable dtReportItems = Common.DataTableFromText("SELECT * FROM ReportItem WHERE ReportID =" + reportId.ToString() + " ORDER BY ItemPosition");

            using (XlsxWriter xlsxWriter = new XlsxWriter(stream))
            {
                if (dtReportItems.Rows.Count == 0)
                {
                    xlsxWriter.CreateInfoWorksheet("Info", "There are not any sheets in this report");
                }
                else
                {
                    foreach (DataRow reportItem in dtReportItems.Rows)
                    {
                        int nItemType = (int) reportItem["ItemType"];
                        if (Enum.IsDefined(typeof(ReportItem.ReportItemType), nItemType))
                        {
                            ReportItem.ReportItemType itemType = (ReportItem.ReportItemType) nItemType;
                            switch (itemType)
                            {
                                case ReportItem.ReportItemType.ItemTypeTable:
                                    CreateDataTableSheet(xlsxWriter, (int) reportItem["ReportItemID"],
                                        reportStartDate, reportEndDate);
                                    break;
                                case ReportItem.ReportItemType.ItemTypeGraph:
                                    CreateChartSheet(xlsxWriter, (int) reportItem["ReportItemID"],
                                        reportStartDate, reportEndDate);
                                    break;
                                case ReportItem.ReportItemType.ItemTypeView:
                                    CreateDataViewSheet(xlsxWriter, (int) reportItem["ReportItemID"], reportId,
                                        reportStartDate, reportEndDate, AccountID);
                                    break;
                            }
                        }
                    }
                }

                xlsxWriter.FinalizeWorkbook();
            }
        }

        private void CreateDataTableSheet(XlsxWriter xlsxWriter, int reportItemId, DateTime? reportStartDate, DateTime? reportEndDate)
        {
            ReportItem reportItem = ReportManager.ets_ReportItem_Detail(reportItemId);
            bool useColors = reportItem.UseColors.HasValue && reportItem.UseColors.Value;
            bool highlightWarnings = reportItem.HighlightWarnings.HasValue && reportItem.HighlightWarnings.Value;
            DataTable dtReportTableColumns = Common.DataTableFromText("SELECT * FROM ReportTableItem WHERE ReportItemID =" + reportItemId.ToString() + " ORDER BY ColumnPosition");

            OrderBy orderBy = null;
            if (reportItem.ApplySort.HasValue && reportItem.ApplySort.Value)
            {
                DataTable dtSort = ReportManager.ets_ReportItemSortOrder_Select(reportItemId);
                if (dtSort != null)
                {
                    foreach (DataRow drSort in dtSort.Rows)
                    {
                        int columnId = (int) drSort["SortColumnID"];
                        bool ascening = drSort["IsDescending"] == DBNull.Value || !(bool) drSort["IsDescending"];
                        if (orderBy == null)
                            orderBy = new OrderBy(columnId, ascening);
                        else
                            orderBy.ThenBy(columnId, ascening);
                    }
                }
            }

            WhereClause whereClausePeriod = null;
            if (reportItem.PeriodColumnID.HasValue && reportStartDate.HasValue && reportEndDate.HasValue)
            {
                whereClausePeriod = new WhereClause(reportItem.PeriodColumnID.Value, "greaterthanequal", reportStartDate.Value);
                whereClausePeriod.And(reportItem.PeriodColumnID.Value, "lessthan", reportEndDate.Value);
            }

            WhereClause whereClauseSettings = null;
            if (reportItem.ApplyFilter.HasValue && reportItem.ApplyFilter.Value)
            {
                DataTable filter = ReportManager.ets_ReportItemFilter_Select(reportItemId);
                foreach (DataRow filterRow in filter.Rows)
                {
                    string filterOperator = string.Empty;
                    if (filterRow.IsNull("FilterOperator") || String.IsNullOrEmpty(filterRow["FilterOperator"].ToString()))
                        break;
                    else
                    {
                        filterOperator = filterRow["FilterOperator"].ToString();
                    }

                    if (whereClauseSettings == null)
                        whereClauseSettings = new WhereClause((int)filterRow["FilterColumnID"], filterOperator, filterRow["FilterColumnValue"].ToString());
                    else
                    {
                        string joinOperator = string.Empty;
                        if (filterRow.IsNull("JoinOperator") || String.IsNullOrEmpty(filterRow["JoinOperator"].ToString()))
                            break;
                        else
                        {
                            joinOperator = filterRow["JoinOperator"].ToString();
                        }
                        if (joinOperator == "or")
                            whereClauseSettings.Or((int)filterRow["FilterColumnID"], filterOperator, filterRow["FilterColumnValue"].ToString());
                        else
                            whereClauseSettings.And((int)filterRow["FilterColumnID"], filterOperator, filterRow["FilterColumnValue"].ToString());
                    }
                }
            }

            WhereClause whereClause = null;
            if (whereClausePeriod != null || whereClauseSettings != null)
            {
                if (whereClausePeriod != null)
                {
                    if (whereClauseSettings == null)
                        whereClause = whereClausePeriod;
                    else
                    {
                        whereClause = new WhereClause(whereClausePeriod);
                        whereClause.And(whereClauseSettings);
                    }
                }
                else
                {
                    whereClause = whereClauseSettings;
                }
            }

            xlsxWriter.CreateWorksheet(reportItem.ItemTitle, reportItem.ObjectId, false,
                whereClause, orderBy, null, dtReportTableColumns, useColors, highlightWarnings);
        }


        private void CreateDataViewSheet(XlsxWriter xlsxWriter, int reportItemId, int reportId,
            DateTime? reportStartDate, DateTime? reportEndDate, int AccountID)
        {
            ReportItem reportItem = ReportManager.ets_ReportItem_Detail(reportItemId);
            DataTable dtReportTableColumns = Common.DataTableFromText("SELECT * FROM ReportTableItem WHERE ReportItemID = -1");

            Dictionary<string, dynamic> parameters = new Dictionary<string, dynamic>
            {
                { "AccountID", AccountID }
            };

            if (reportStartDate.HasValue && reportEndDate.HasValue)
            {
                parameters.Add("DateRangeFrom", reportStartDate.Value);
                parameters.Add("DateRangeTo", reportEndDate.Value);
            }

            //KG 2/3/18 Add AccountID as parameter

            xlsxWriter.CreateWorksheet(reportItem.ItemTitle, reportItem.ObjectId, true,
                null, null, parameters, dtReportTableColumns, false, false, reportId);
        }


        private void CreateChartSheet(XlsxWriter xlsxWriter, int reportItemId, DateTime? reportStartDate, DateTime? reportEndDate)
        {
            ReportItem reportItem = ReportManager.ets_ReportItem_Detail(reportItemId);

            GraphOption graphOption = GraphManager.ets_GraphOption_Detail(reportItem.ObjectId);

            string chartType = "";
            if (graphOption != null && graphOption.GraphDefinitionID.HasValue)
            {
                GraphDefinition graphDefinition =
                    GraphManager.ets_GraphDefinition_Detail(graphOption.GraphDefinitionID.Value);
                if (graphDefinition != null && !String.IsNullOrEmpty(graphDefinition.Definition))
                {
                    string definition = graphDefinition.Definition;
                    int position =
                        definition.IndexOf("%ExcelChart%", StringComparison.InvariantCultureIgnoreCase);
                    if (position > -1)
                    {
                        position += 12;
                        position = definition.IndexOf('"', position);
                        int startPosition = -1;
                        int endPosition = -1;
                        if (position > -1)
                            startPosition = position + 1;
                        position = definition.IndexOf('"', startPosition);
                        if (position > -1)
                            endPosition = position;
                        if (startPosition > -1 && endPosition > -1)
                            chartType = definition.Substring(startPosition, endPosition - startPosition)
                                .ToLower(CultureInfo.InvariantCulture);
                    }

                    if (String.IsNullOrEmpty(chartType))
                    {
                        string pattern = @"(?s)highcharts.*chart:\s*\{[^\}]*type:\s*'([^'\{]*)";
                        foreach (Match match in Regex.Matches(graphDefinition.Definition, pattern))
                            if (match.Groups.Count > 1)
                                chartType = match.Groups[1].Value;
                    }
                }
            }

            int chartTypeCode = chartType == "column" ? 1 : 0;

            int iTotalRowsNum = 0;
            DataTable graphOptionDetails = GraphManager.ets_GraphOptionDetail_Select(reportItem.ObjectId, null, null, null, null, null, ref iTotalRowsNum);
            if (graphOptionDetails.Rows.Count > 0)
            {
                DataRow details = graphOptionDetails.Rows[0];
                int tableId = details.IsNull("TableID") ? 0 : (int) graphOptionDetails.Rows[0]["TableID"];
                int dataColumnId = details.IsNull("ColumnID") ? 0 : (int) graphOptionDetails.Rows[0]["ColumnID"];
                int xAxisColumnId = 0;
                int seriesColumnId = 0;
                Table table = RecordManager.ets_Table_Details(tableId);
                if (table != null)
                {
                    xAxisColumnId = table.GraphXAxisColumnID ?? 0;
                    seriesColumnId = table.GraphSeriesColumnID ?? 0;
                }

                if (tableId != 0 && dataColumnId != 0 && xAxisColumnId != 0)
                {
                    bool IsOneDate = false; // Red Ticket 5042
                    WhereClause whereClausePeriod = null;
                    if (reportStartDate.HasValue && reportEndDate.HasValue)
                    {
                        whereClausePeriod = new WhereClause(xAxisColumnId, "greaterthanequal",
                            reportStartDate.Value);
                        whereClausePeriod.And(xAxisColumnId, "lessthan", reportEndDate.Value);

                        /* == Red 07012020: Ticket 5042 == */
                        int NoOfDays = (reportEndDate - reportStartDate).Value.Days;
                        if (NoOfDays == 1)
                        {
                            IsOneDate = true;
                        }
                        /* == End Red == */
                    }

                    WhereClause whereClauseSeries = null;
                    List<string> seriesIdList = new List<string>();
                    foreach (DataRow detailsRow in graphOptionDetails.Rows)
                    {
                        if (!detailsRow.IsNull("GraphSeriesID"))
                        {
                            string id = detailsRow["GraphSeriesID"].ToString();
                            if (id != "")
                            {
                                if (!seriesIdList.Contains(id))
                                    seriesIdList.Add(id);
                            }
                        }
                    }

                    Column seriesColumn = RecordManager.ets_Column_Details(seriesColumnId);
                    if (seriesColumn != null && seriesIdList.Count > 0)
                    {
                        string filterString = "[Record].[" + seriesColumn.SystemName + "] IN (";
                        foreach (string id in seriesIdList)
                        {
                            filterString += "'" + id + "', ";
                        }
                        filterString = filterString.Substring(0, filterString.Length - 2);
                        filterString += ")";

                        whereClauseSeries = new WhereClause(filterString);
                    }

                    WhereClause whereClause = null;
                    if (whereClauseSeries == null)
                        whereClause = whereClausePeriod;
                    else
                    {
                        whereClause = new WhereClause(whereClausePeriod);
                        whereClause.And(whereClauseSeries);
                    }

                    /* === Red: Charting, just a temp list to match with the module;
                     * did not touch any yet here === */
                    List<Tuple<int, string>> listSeriesColors = null;

                    xlsxWriter.CreateChartWorksheet(reportItem.ItemTitle,
                        tableId, dataColumnId, xAxisColumnId, seriesColumnId,
                        whereClause,
                        graphOption.Heading, graphOption.SubHeading,
                        graphOption.WarningCaption + " Upper", graphOption.WarningValue, graphOption.WarningColor,
                        graphOption.ExceedanceCaption + " Upper", graphOption.ExceedanceValue, graphOption.ExceedanceColor,
                        /* === Red: Charting pass Min Values === */
                        graphOption.WarningCaption + " Lower", graphOption.WarningColor,
                        graphOption.WarningValueMin,
                         graphOption.ExceedanceCaption + " Lower", graphOption.ExceedanceColor,
                        graphOption.ExceedanceValueMin, listSeriesColors,
                        /* === end Red === */
                        
                        chartTypeCode, true, IsOneDate, 
                        //RP Added Ticket 5119
                        graphOption.YAxisHighestValue.ToString(), graphOption.YAxisLowestValue.ToString(), graphOption.YAxisInterval.ToString());
                        //End Modification
                }
            }
        }
    }
}

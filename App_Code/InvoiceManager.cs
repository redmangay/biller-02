﻿using System;
//using System.Linq;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for InvoiceManager
/// </summary>
public class InvoiceManager
{
    public InvoiceManager()
    {
        //
        // TODO: Add constructor logic here
        //

    }



    public static int ets_Invoice_Insert(Invoice p_Invoice)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Invoice_Insert", connection))
            {

                command.CommandType = CommandType.StoredProcedure;

                SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int);
                pRV.Direction = ParameterDirection.Output;

                command.Parameters.Add(pRV);
                command.Parameters.Add(new SqlParameter("@nAccountID", p_Invoice.AccountID));
                command.Parameters.Add(new SqlParameter("@sAccountName", p_Invoice.AccountName));
                command.Parameters.Add(new SqlParameter("@nAccountTypeID", p_Invoice.AccountTypeID));
                command.Parameters.Add(new SqlParameter("@nNetAmountAUD", p_Invoice.NetAmountAUD));
                command.Parameters.Add(new SqlParameter("@nGSTAmountAUD", p_Invoice.GSTAmountAUD));

                command.Parameters.Add(new SqlParameter("@nGrossAmountAUD", p_Invoice.GrossAmountAUD));
                command.Parameters.Add(new SqlParameter("@dInvoiceDate", p_Invoice.InvoiceDate));
                command.Parameters.Add(new SqlParameter("@dStartDate", p_Invoice.StartDate));
                command.Parameters.Add(new SqlParameter("@dEndDate", p_Invoice.EndDate));
                command.Parameters.Add(new SqlParameter("@sPaymentMethod", p_Invoice.PaymentMethod));
                command.Parameters.Add(new SqlParameter("@nPaypalID", p_Invoice.PaypalID));
                command.Parameters.Add(new SqlParameter("@dPaidDate", p_Invoice.PaidDate));
                command.Parameters.Add(new SqlParameter("@sNotes", p_Invoice.Notes));
                command.Parameters.Add(new SqlParameter("@sOrganisationName", p_Invoice.OrganisationName));
                command.Parameters.Add(new SqlParameter("@sBillingEmail", p_Invoice.BillingEmail));
                command.Parameters.Add(new SqlParameter("@sBillingAddress", p_Invoice.BillingAddress));
                command.Parameters.Add(new SqlParameter("@sCountry", p_Invoice.Country));
                command.Parameters.Add(new SqlParameter("@sClientRef", p_Invoice.ClientRef));

                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Dispose();
                    return int.Parse(pRV.Value.ToString());
                }
                catch
                {
                    connection.Close();
                    connection.Dispose();

                }
                return -1;

            }
        }




    }


    public static int ets_Invoice_Update(Invoice p_Invoice)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Invoice_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nInvoiceID", p_Invoice.InvoiceID));
                command.Parameters.Add(new SqlParameter("@nAccountID", p_Invoice.AccountID));
                command.Parameters.Add(new SqlParameter("@sAccountName", p_Invoice.AccountName));
                command.Parameters.Add(new SqlParameter("@nAccountTypeID", p_Invoice.AccountTypeID));
                command.Parameters.Add(new SqlParameter("@nNetAmountAUD", p_Invoice.NetAmountAUD));
                command.Parameters.Add(new SqlParameter("@nGSTAmountAUD", p_Invoice.GSTAmountAUD));

                command.Parameters.Add(new SqlParameter("@nGrossAmountAUD", p_Invoice.GrossAmountAUD));
                command.Parameters.Add(new SqlParameter("@dInvoiceDate", p_Invoice.InvoiceDate));
                command.Parameters.Add(new SqlParameter("@dStartDate", p_Invoice.StartDate));
                command.Parameters.Add(new SqlParameter("@dEndDate", p_Invoice.EndDate));
                command.Parameters.Add(new SqlParameter("@sPaymentMethod", p_Invoice.PaymentMethod));
                command.Parameters.Add(new SqlParameter("@nPaypalID", p_Invoice.PaypalID));
                command.Parameters.Add(new SqlParameter("@dPaidDate", p_Invoice.PaidDate));
                command.Parameters.Add(new SqlParameter("@sNotes", p_Invoice.Notes));
                command.Parameters.Add(new SqlParameter("@sOrganisationName", p_Invoice.OrganisationName));
                command.Parameters.Add(new SqlParameter("@sBillingEmail", p_Invoice.BillingEmail));
                command.Parameters.Add(new SqlParameter("@sBillingAddress", p_Invoice.BillingAddress));
                command.Parameters.Add(new SqlParameter("@sCountry", p_Invoice.Country));
                command.Parameters.Add(new SqlParameter("@sClientRef", p_Invoice.ClientRef));

                command.Parameters.Add(new SqlParameter("@nPaidAmount", p_Invoice.PaidAmount));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;

            }
        }






    }



    public static DataTable ets_Invoice_Select(int? nAccountID, string sAccountName, int? nAccountTypeID,
       DateTime? dInvoiceDateFrom, DateTime? dInvoiceDateTo, string sPaymentMethod,
        bool? bIsPaid, string sOrganisationName, string sBillingEmail, string sBillingAddress,
        string sCountry, string sClientRef, DateTime? dDateAdded, DateTime? dDateUpdated, string sOrder,
      string sOrderDirection, int? nStartRow, int? nMaxRows, ref int iTotalRowsNum)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Invoice_Select", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nAccountID", nAccountID));

                if (sAccountName != "")
                    command.Parameters.Add(new SqlParameter("@sAccountName", sAccountName));

                if (nAccountTypeID != null)
                    command.Parameters.Add(new SqlParameter("@nAccountTypeID", nAccountTypeID));

                if (dInvoiceDateFrom != null)
                    command.Parameters.Add(new SqlParameter("@dInvoiceDateFrom", dInvoiceDateFrom));
                if (dInvoiceDateTo != null)
                    command.Parameters.Add(new SqlParameter("@dInvoiceDateTo", dInvoiceDateTo));



                if (sPaymentMethod != "")
                    command.Parameters.Add(new SqlParameter("@sPaymentMethod", sPaymentMethod));
                if (bIsPaid != null)
                    command.Parameters.Add(new SqlParameter("@bIsPaid", bIsPaid));
                if (sOrganisationName != "")
                    command.Parameters.Add(new SqlParameter("@sOrganisationName", sOrganisationName));
                if (sBillingEmail != "")
                    command.Parameters.Add(new SqlParameter("@sBillingEmail", sBillingEmail));
                if (sBillingAddress != "")
                    command.Parameters.Add(new SqlParameter("@sBillingAddress", sBillingAddress));
                if (sCountry != "")
                    command.Parameters.Add(new SqlParameter("@sCountry", sCountry));
                if (sClientRef != "")
                    command.Parameters.Add(new SqlParameter("@sClientRef", sClientRef));

                if (dDateAdded != null)
                    command.Parameters.Add(new SqlParameter("@dDateAdded", dDateAdded));

                if (dDateUpdated != null)
                    command.Parameters.Add(new SqlParameter("@dDateUpdated", dDateUpdated));


                if (string.IsNullOrEmpty(sOrder) || string.IsNullOrEmpty(sOrderDirection))
                { sOrder = "InvoiceID"; sOrderDirection = "DESC"; }

                command.Parameters.Add(new SqlParameter("@sOrder", sOrder + " " + sOrderDirection));

                if (nStartRow != null)
                    command.Parameters.Add(new SqlParameter("@nStartRow", nStartRow + 1));

                if (nMaxRows != null)
                    command.Parameters.Add(new SqlParameter("@nMaxRows", nMaxRows));




                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                System.Data.DataSet ds = new System.Data.DataSet();

                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();

                iTotalRowsNum = 0;
                if (ds == null) return null;


                if (ds.Tables.Count > 1)
                {
                    iTotalRowsNum = int.Parse(ds.Tables[1].Rows[0][0].ToString());
                }
                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
                {
                    return null;
                }


            }
        }
    }




    public static int ets_Invoice_Delete(int nInvoiceID)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Invoice_Delete", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nInvoiceID ", nInvoiceID));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;

            }
        }
    }


    public static Invoice ets_Invoice_Detail(int nInvoiceID)
    {


        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Invoice_Details", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nInvoiceID", nInvoiceID));

                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Invoice temp = new Invoice(
                                (int)reader["InvoiceID"], reader["AccountID"] == DBNull.Value ? null : (int?)reader["AccountID"],
                               reader["AccountName"] == DBNull.Value ? "" : (string)reader["AccountName"],
                               reader["AccountTypeID"] == DBNull.Value ? null : (int?)reader["AccountTypeID"], (double?)double.Parse(reader["NetAmountAUD"].ToString()),
                               reader["GSTAmountAUD"] == DBNull.Value ? null : (double?)double.Parse(reader["GSTAmountAUD"].ToString()),
                                reader["GrossAmountAUD"] == DBNull.Value ? null : (double?)double.Parse(reader["GrossAmountAUD"].ToString()),
                                 reader["InvoiceDate"] == DBNull.Value ? null : (DateTime?)reader["InvoiceDate"],
                                  reader["StartDate"] == DBNull.Value ? null : (DateTime?)reader["StartDate"],
                                   reader["EndDate"] == DBNull.Value ? null : (DateTime?)reader["EndDate"],
                                    reader["PaymentMethod"] == DBNull.Value ? "" : (string)reader["PaymentMethod"],
                                    reader["PaypalID"] == DBNull.Value ? null : (int?)reader["PaypalID"],
                                    reader["PaidDate"] == DBNull.Value ? null : (DateTime?)reader["PaidDate"],
                                    reader["Notes"] == DBNull.Value ? "" : (string)reader["Notes"],
                                    reader["OrganisationName"] == DBNull.Value ? "" : (string)reader["OrganisationName"],
                                    reader["BillingEmail"] == DBNull.Value ? "" : (string)reader["BillingEmail"],
                                    reader["BillingAddress"] == DBNull.Value ? "" : (string)reader["BillingAddress"],
                                    reader["Country"] == DBNull.Value ? "" : (string)reader["Country"],
                                    reader["ClientRef"] == DBNull.Value ? "" : (string)reader["ClientRef"],
                                (DateTime)reader["DateAdded"],
                                (DateTime)reader["DateUpdated"]
                                );
                            temp.PaidAmount = reader["PaidAmount"] == DBNull.Value ? null : (double?)double.Parse(reader["PaidAmount"].ToString());


                            connection.Close();
                            connection.Dispose();

                            return temp;
                        }

                    }
                }
                catch
                {

                }

                connection.Close();
                connection.Dispose();
                return null;

            }

        }





    }


    public static int AccountInvite_Insert(AccountInvite p_AccountInvite)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("AccountInvite_Insert", connection))
            {

                command.CommandType = CommandType.StoredProcedure;

                SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int);
                pRV.Direction = ParameterDirection.Output;

                command.Parameters.Add(pRV);
                command.Parameters.Add(new SqlParameter("@nAccountID", p_AccountInvite.AccountID));
                command.Parameters.Add(new SqlParameter("@sEmail", p_AccountInvite.Email));
                command.Parameters.Add(new SqlParameter("@sName", p_AccountInvite.Name));
                command.Parameters.Add(new SqlParameter("@dAcceptedDate", p_AccountInvite.AcceptedDate));
                command.Parameters.Add(new SqlParameter("@dPaidToDate", p_AccountInvite.PaidToDate));
                command.Parameters.Add(new SqlParameter("@dExpiredDate", p_AccountInvite.ExpiredDate));
                command.Parameters.Add(new SqlParameter("@bIsActive", p_AccountInvite.IsActive));
                command.Parameters.Add(new SqlParameter("@nInvitedAccountID ", p_AccountInvite.InvitedAccountID));
                command.Parameters.Add(new SqlParameter("@sPhoneNumber ", p_AccountInvite.PhoneNumber));
                command.Parameters.Add(new SqlParameter("@nInvitedUserID ", p_AccountInvite.InvitedUserID));
                command.Parameters.Add(new SqlParameter("@nInviterUserID ", p_AccountInvite.InviterUserID));
                command.Parameters.Add(new SqlParameter("@sInviteStatus ", p_AccountInvite.InviteStatus));
                command.Parameters.Add(new SqlParameter("@sInviteType ", p_AccountInvite.InviteType));
                command.Parameters.Add(new SqlParameter("@nTaskRecordID ", p_AccountInvite.TaskRecordID));
                command.Parameters.Add(new SqlParameter("@bAddAsTeamMember ", p_AccountInvite.AddAsTeamMember));

                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Dispose();
                    return int.Parse(pRV.Value.ToString());
                }
                catch
                {
                    connection.Close();
                    connection.Dispose();

                }
                return -1;

            }
        }




    }


    public static int CancelForwardTask(int accountId, AccountInviteField info)
    {
        try
        {
            int iAccountInviteID = -1;

            if(!string.IsNullOrEmpty(info.AccountInviteID))
            {
                iAccountInviteID = int.Parse(info.AccountInviteID);
                AccountInvite editAccountInvite = InvoiceManager.AccountInvite_Detail(iAccountInviteID);
                if (editAccountInvite != null)
                {
                    editAccountInvite.InviteStatus = "C";
                    InvoiceManager.AccountInvite_Update(editAccountInvite);
                }
            }
            else
            {
                //it's a brand new AccountInvite with Cancel status
                AccountInvite theAccountInvite = new AccountInvite();
                theAccountInvite.InviterUserID = string.IsNullOrEmpty(info.FromUserID) ? null : (int?)int.Parse(info.FromUserID);
                theAccountInvite.InvitedUserID = string.IsNullOrEmpty(info.ToUserID) ? null : (int?)int.Parse(info.ToUserID);

                theAccountInvite.TaskRecordID = string.IsNullOrEmpty(info.TaskID) ? null : (int?)int.Parse(info.TaskID);
                theAccountInvite.Name = info.ToFullName;
                theAccountInvite.Email = info.ToEmailAddress;
                theAccountInvite.PhoneNumber = info.ToMobile;
                //theAccountInvite.AddAsTeamMember = string.IsNullOrEmpty(info.AddAsTeamMember) ? null : (bool?)bool.Parse(info.AddAsTeamMember);
                theAccountInvite.InviteType = info.InviteType;
                theAccountInvite.AccountID = (int?)accountId;
                theAccountInvite.IsActive = true;
                theAccountInvite.InviteStatus = "C";
                string strInvitedUserID = "";
                if (!string.IsNullOrEmpty(theAccountInvite.Email))
                {
                    strInvitedUserID = Common.GetValueFromSQL("SELECT UserID FROM [User] WHERE [Email] ='" + theAccountInvite.Email + "'");
                    if (strInvitedUserID != "")
                    {
                        theAccountInvite.InvitedUserID = int.Parse(strInvitedUserID);

                    }
                }
                if (theAccountInvite.InvitedUserID != null)
                {
                    theAccountInvite.InvitedAccountID = SecurityManager.GetPrimaryAccountID(int.Parse(strInvitedUserID));
                }

                iAccountInviteID = InvoiceManager.AccountInvite_Insert(theAccountInvite);
                theAccountInvite.AccountInviteID = iAccountInviteID;

            }
                      

            return iAccountInviteID;
        }
        catch
        {
            return -1;
        }
    }


    //public static int CancelForwardTask(int accountId, AccountInviteField info)
    //{
    //    try
    //    {
    //        int iAccountInviteID = -1;
    //        AccountInvite theAccountInvite = new AccountInvite();
    //        theAccountInvite.InviterUserID = string.IsNullOrEmpty(info.FromUserID) ? null : (int?)int.Parse(info.FromUserID);
    //        theAccountInvite.InvitedUserID = string.IsNullOrEmpty(info.ToUserID) ? null : (int?)int.Parse(info.ToUserID);

    //        theAccountInvite.TaskRecordID = string.IsNullOrEmpty(info.TaskID) ? null : (int?)int.Parse(info.TaskID);
    //        theAccountInvite.Name = info.ToFullName;
    //        theAccountInvite.Email = info.ToEmailAddress;
    //        theAccountInvite.PhoneNumber = info.ToMobile;
    //        //theAccountInvite.AddAsTeamMember = string.IsNullOrEmpty(info.AddAsTeamMember) ? null : (bool?)bool.Parse(info.AddAsTeamMember);
    //        theAccountInvite.InviteType = info.InviteType;
    //        theAccountInvite.AccountID = (int?)accountId;
    //        theAccountInvite.IsActive = true;
    //        theAccountInvite.InviteStatus = string.Empty;
    //        string strInvitedUserID = "";
    //        if (!string.IsNullOrEmpty(theAccountInvite.Email))
    //        {
    //            strInvitedUserID = Common.GetValueFromSQL("SELECT UserID FROM [User] WHERE [Email] ='" + theAccountInvite.Email + "'");
    //            if (strInvitedUserID != "")
    //            {
    //                theAccountInvite.InvitedUserID = int.Parse(strInvitedUserID);

    //            }
    //        }

    //        if (theAccountInvite.InvitedUserID != null)
    //        {
    //            theAccountInvite.InvitedAccountID = SecurityManager.GetPrimaryAccountID(int.Parse(strInvitedUserID));
    //        }


    //        iAccountInviteID = InvoiceManager.AccountInvite_Insert(theAccountInvite);
    //        theAccountInvite.AccountInviteID = iAccountInviteID;

    //        User theInviterUser = null;
    //        string strInvitedUserName = "";
    //        if (theAccountInvite.InviterUserID != null)
    //        {
    //            theInviterUser = SecurityManager.User_Details((int)theAccountInvite.InviterUserID);
    //        }

    //        //string strAcceptURL = TheDatabase.SiteRoot() + "/AccountInvite.aspx?ID=" + Cryptography.Encrypt(iAccountInviteID.ToString()) + "&Response=Accept";
    //        //string strDeclineURL = TheDatabase.SiteRoot() + "/AccountInvite.aspx?ID=" + Cryptography.Encrypt(iAccountInviteID.ToString()) + "&Response=Decline";


    //        Message theMessage = new Message(null, null, null, theAccountInvite.AccountID,
    //           DateTime.Now, "E", "E",
    //               null, "", "", "", null, "");

    //        if (!string.IsNullOrEmpty(theAccountInvite.Email))
    //        {
    //            theMessage.OtherParty = theAccountInvite.Email;
    //            Content theContentEmail = null;

    //            theContentEmail = SystemData.Content_Details_ByKey("OnTask_CancelInvite", null);

    //            //theContentEmail.ContentP = theContentEmail.ContentP.Replace("[Accept]", strAcceptURL);
    //            //theContentEmail.ContentP = theContentEmail.ContentP.Replace("[Decline]", strDeclineURL);

    //            theContentEmail.ContentP = theContentEmail.ContentP.Replace("[InvitedEmail]", theAccountInvite.Email);

    //            if (theInviterUser != null)
    //            {
    //                theContentEmail.ContentP = theContentEmail.ContentP.Replace("[SenderFullName]", theInviterUser.FirstName + " " + theInviterUser.LastName);
    //                theContentEmail.Heading = theContentEmail.Heading.Replace("[SenderFullName]", theInviterUser.FirstName + " " + theInviterUser.LastName);
    //            }

    //            string strInvitedFullName = theAccountInvite.Name;

    //            if (string.IsNullOrEmpty(strInvitedFullName))
    //            {
    //                if (theAccountInvite.InvitedUserID != null)
    //                {
    //                    User theInvitedUser = SecurityManager.User_Details((int)theAccountInvite.InvitedUserID);
    //                    if (theInviterUser != null)
    //                    {
    //                        strInvitedFullName = theInvitedUser.FirstName + " " + theInvitedUser.LastName;

    //                        strInvitedUserName = theInvitedUser.FirstName + " " + theInvitedUser.LastName;

    //                    }
    //                }
    //            }

    //            theContentEmail.ContentP = theContentEmail.ContentP.Replace("[InvitedFullName]", strInvitedFullName);

    //            if (theAccountInvite.TaskRecordID != null)
    //            {
    //                Record theTaskRecord = RecordManager.ets_Record_Detail_Full((int)theAccountInvite.TaskRecordID, null, false);
    //                if (theTaskRecord != null)
    //                {
    //                    theMessage.TableID = theTaskRecord.TableID;

    //                    theContentEmail.Heading = theContentEmail
    //                                                .Heading
    //                                                .Replace("[InvitedFullName]", strInvitedFullName);

    //                    theContentEmail.ContentP = theContentEmail.ContentP.Replace("[TaskName]", theTaskRecord.V002);

    //                    if (theTaskRecord.V001 != "")
    //                    {
    //                        string strClientName = Common.GetValueFromSQL("SELECT V001 FROM [Record] WHERE RecordID=" + theTaskRecord.V001);
    //                        theContentEmail.ContentP = theContentEmail.ContentP.Replace("[ClientName]", strClientName);
    //                    }
    //                    DateTime dtTest = DateTime.Today;
    //                    if (theTaskRecord.V017 != "")
    //                    {
    //                        if (DateTime.TryParse(theTaskRecord.V017, out dtTest))
    //                        {
    //                            theContentEmail.ContentP = theContentEmail.ContentP.Replace("[StartDate]", dtTest.ToShortDateString());
    //                            theContentEmail.ContentP = theContentEmail.ContentP.Replace("[StartTime]", dtTest.ToShortTimeString());
    //                        }
    //                    }
    //                    if (theTaskRecord.V018 != "")
    //                    {
    //                        if (DateTime.TryParse(theTaskRecord.V018, out dtTest))
    //                        {
    //                            theContentEmail.ContentP = theContentEmail.ContentP.Replace("[EndDate]", dtTest.ToShortDateString());
    //                            theContentEmail.ContentP = theContentEmail.ContentP.Replace("[EndTime]", dtTest.ToShortTimeString());
    //                        }
    //                    }
    //                    if (theTaskRecord.V004 != "")
    //                    {
    //                        LocationColumn theLocationColumn = DocGen.DAL.JSONField.GetTypedObject<LocationColumn>(theTaskRecord.V004);
    //                        if (theLocationColumn != null)
    //                        {
    //                            if (!string.IsNullOrEmpty(theLocationColumn.Address))
    //                            {
    //                                theContentEmail.ContentP = theContentEmail.ContentP.Replace("[TaskAddress]", theLocationColumn.Address);
    //                            }
    //                        }
    //                    }

    //                    if (strInvitedUserName == "" && !string.IsNullOrEmpty(theAccountInvite.Name))
    //                    {
    //                        strInvitedUserName = theAccountInvite.Name;
    //                    }
    //                    if (strInvitedUserName == "" && !string.IsNullOrEmpty(theAccountInvite.Email))
    //                    {
    //                        strInvitedUserName = theAccountInvite.Email;
    //                    }
    //                    theTaskRecord.V026 = string.Empty;//Status of Invite
    //                    theTaskRecord.V027 = string.Empty;//invited username
    //                    RecordManager.ets_Record_Update(theTaskRecord, true);
    //                }
    //            }


    //            //JA 23 FEB 2017 Summary email
    //            string sSendEmailError = "";
    //            theMessage.Subject = theContentEmail.Heading;
    //            theMessage.Body = theContentEmail.ContentP;
    //            DBGurus.SendEmail(theContentEmail.ContentKey, true, null, theContentEmail.Heading, theContentEmail.ContentP
    //                , "", theAccountInvite.Email, theInviterUser.Email, "", null, theMessage, out sSendEmailError);


    //        }

    //        return iAccountInviteID;
    //    }
    //    catch
    //    {
    //        return -1;
    //    }
    //}

    public static int ForwardTask(int accountId, AccountInviteField info)
    {
        try
        {
            int iAccountInviteID = -1;
            AccountInvite theAccountInvite = new AccountInvite();
            theAccountInvite.InviterUserID = string.IsNullOrEmpty(info.FromUserID) ? null : (int?)int.Parse(info.FromUserID);
            theAccountInvite.InvitedUserID = string.IsNullOrEmpty(info.ToUserID) ? null : (int?)int.Parse(info.ToUserID);

            theAccountInvite.TaskRecordID = string.IsNullOrEmpty(info.TaskID) ? null : (int?)int.Parse(info.TaskID);
            theAccountInvite.Name = info.ToFullName;
            theAccountInvite.Email = info.ToEmailAddress;
            theAccountInvite.PhoneNumber = info.ToMobile;
            theAccountInvite.AddAsTeamMember = string.IsNullOrEmpty(info.AddAsTeamMember) ? null : (bool?)bool.Parse(info.AddAsTeamMember);
            theAccountInvite.InviteType = info.InviteType;
            theAccountInvite.AccountID = (int?)accountId;
            theAccountInvite.IsActive = true;
            theAccountInvite.InviteStatus = "P";
            string strInvitedUserID = "";
            if (!string.IsNullOrEmpty(theAccountInvite.Email))
            {
                strInvitedUserID = Common.GetValueFromSQL("SELECT UserID FROM [User] WHERE [Email] ='" + theAccountInvite.Email + "'");
                if (strInvitedUserID != "")
                {
                    theAccountInvite.InvitedUserID = int.Parse(strInvitedUserID);

                }
            }

            if (theAccountInvite.InvitedUserID != null)
            {
                theAccountInvite.InvitedAccountID = SecurityManager.GetPrimaryAccountID(int.Parse(strInvitedUserID));
            }


            iAccountInviteID = InvoiceManager.AccountInvite_Insert(theAccountInvite);
            theAccountInvite.AccountInviteID = iAccountInviteID;

            User theInviterUser = null;
            string strInvitedUserName = "";
            if (theAccountInvite.InviterUserID != null)
            {
                theInviterUser = SecurityManager.User_Details((int)theAccountInvite.InviterUserID);
            }

            string strAcceptURL = TheDatabase.SiteRoot() + "/AccountInvite.aspx?ID=" + Cryptography.Encrypt(iAccountInviteID.ToString()) + "&Response=Accept";
            string strDeclineURL = TheDatabase.SiteRoot() + "/AccountInvite.aspx?ID=" + Cryptography.Encrypt(iAccountInviteID.ToString()) + "&Response=Decline";


            Message theMessage = new Message(null, null, null, theAccountInvite.AccountID,
               DateTime.Now, "E", "E",
                   null, "", "", "", null, "");

            if (!string.IsNullOrEmpty(theAccountInvite.Email))
            {
                theMessage.OtherParty = theAccountInvite.Email;
                Content theContentEmail = null;
                if (theAccountInvite.InvitedUserID != null)
                {
                    theContentEmail = SystemData.Content_Details_ByKey("OnTask_ExistingUserTaskinvite", null);
                }
                else
                {
                    theContentEmail = SystemData.Content_Details_ByKey("OnTask_NewUserTaskinvite", null);
                }
                theContentEmail.ContentP = theContentEmail.ContentP.Replace("[Accept]", strAcceptURL);
                theContentEmail.ContentP = theContentEmail.ContentP.Replace("[Decline]", strDeclineURL);

                theContentEmail.ContentP = theContentEmail.ContentP.Replace("[InvitedEmail]", theAccountInvite.Email);

                if (theInviterUser != null)
                {
                    theContentEmail.ContentP = theContentEmail.ContentP.Replace("[SenderFullName]", theInviterUser.FirstName + " " + theInviterUser.LastName);
                    theContentEmail.Heading = theContentEmail.Heading.Replace("[SenderFullName]", theInviterUser.FirstName + " " + theInviterUser.LastName);
                }

                string strInvitedFullName = theAccountInvite.Name;

                if (string.IsNullOrEmpty(strInvitedFullName))
                {
                    if (theAccountInvite.InvitedUserID != null)
                    {
                        User theInvitedUser = SecurityManager.User_Details((int)theAccountInvite.InvitedUserID);
                        if (theInviterUser != null)
                        {
                            strInvitedFullName = theInvitedUser.FirstName + " " + theInvitedUser.LastName;

                            strInvitedUserName = theInvitedUser.FirstName + " " + theInvitedUser.LastName;

                        }
                    }
                }

                theContentEmail.ContentP = theContentEmail.ContentP.Replace("[InvitedFullName]", strInvitedFullName);

                if (theAccountInvite.TaskRecordID != null)
                {
                    Record theTaskRecord = RecordManager.ets_Record_Detail_Full((int)theAccountInvite.TaskRecordID, null, false);
                    if (theTaskRecord != null)
                    {
                        theMessage.TableID = theTaskRecord.TableID;

                        theContentEmail.Heading = theContentEmail.Heading.Replace("[TaskName]", theTaskRecord.V002);
                        theContentEmail.ContentP = theContentEmail.ContentP.Replace("[TaskName]", theTaskRecord.V002);

                        if (theTaskRecord.V001 != "")
                        {
                            string strClientName = Common.GetValueFromSQL("SELECT V001 FROM [Record] WHERE RecordID=" + theTaskRecord.V001);
                            theContentEmail.ContentP = theContentEmail.ContentP.Replace("[ClientName]", strClientName);
                        }
                        DateTime dtTest = DateTime.Today;
                        if (theTaskRecord.V017 != "")
                        {
                            if (DateTime.TryParse(theTaskRecord.V017, out dtTest))
                            {
                                theContentEmail.ContentP = theContentEmail.ContentP.Replace("[StartDate]", dtTest.ToShortDateString());
                                theContentEmail.ContentP = theContentEmail.ContentP.Replace("[StartTime]", dtTest.ToShortTimeString());
                            }
                        }
                        if (theTaskRecord.V018 != "")
                        {
                            if (DateTime.TryParse(theTaskRecord.V018, out dtTest))
                            {
                                theContentEmail.ContentP = theContentEmail.ContentP.Replace("[EndDate]", dtTest.ToShortDateString());
                                theContentEmail.ContentP = theContentEmail.ContentP.Replace("[EndTime]", dtTest.ToShortTimeString());
                            }
                        }
                        if (theTaskRecord.V004 != "")
                        {
                            LocationColumn theLocationColumn = DocGen.DAL.JSONField.GetTypedObject<LocationColumn>(theTaskRecord.V004);
                            if (theLocationColumn != null)
                            {
                                if (!string.IsNullOrEmpty(theLocationColumn.Address))
                                {
                                    theContentEmail.ContentP = theContentEmail.ContentP.Replace("[TaskAddress]", theLocationColumn.Address);
                                }
                            }
                        }

                        if (strInvitedUserName == "" && !string.IsNullOrEmpty(theAccountInvite.Name))
                        {
                            strInvitedUserName = theAccountInvite.Name;
                        }
                        if (strInvitedUserName == "" && !string.IsNullOrEmpty(theAccountInvite.Email))
                        {
                            strInvitedUserName = theAccountInvite.Email;
                        }
                        theTaskRecord.V026 = "P";
                        theTaskRecord.V027 = strInvitedUserName;
                        RecordManager.ets_Record_Update(theTaskRecord, true);
                    }
                }


                //JA 23 FEB 2017 Summary email
                string sSendEmailError = "";
                theMessage.Subject = theContentEmail.Heading;
                theMessage.Body = theContentEmail.ContentP;
                DBGurus.SendEmail(theContentEmail.ContentKey, true, null, theContentEmail.Heading, theContentEmail.ContentP
                    , "", theAccountInvite.Email, theInviterUser.Email, "", null, theMessage, out sSendEmailError);


            }

            return iAccountInviteID;
        }
        catch
        {
            return -1;
        }
    }

    public static int AccountInvite_Update(AccountInvite p_AccountInvite)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("AccountInvite_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nAccountInviteID", p_AccountInvite.AccountInviteID));
                command.Parameters.Add(new SqlParameter("@nAccountID", p_AccountInvite.AccountID));
                command.Parameters.Add(new SqlParameter("@sEmail", p_AccountInvite.Email));
                command.Parameters.Add(new SqlParameter("@sName", p_AccountInvite.Name));
                command.Parameters.Add(new SqlParameter("@dAcceptedDate", p_AccountInvite.AcceptedDate));
                command.Parameters.Add(new SqlParameter("@dPaidToDate", p_AccountInvite.PaidToDate));
                command.Parameters.Add(new SqlParameter("@dExpiredDate", p_AccountInvite.ExpiredDate));
                command.Parameters.Add(new SqlParameter("@bIsActive", p_AccountInvite.IsActive));
                command.Parameters.Add(new SqlParameter("@nInvitedAccountID ", p_AccountInvite.InvitedAccountID));
                command.Parameters.Add(new SqlParameter("@sPhoneNumber ", p_AccountInvite.PhoneNumber));
                command.Parameters.Add(new SqlParameter("@nInvitedUserID ", p_AccountInvite.InvitedUserID));
                command.Parameters.Add(new SqlParameter("@nInviterUserID ", p_AccountInvite.InviterUserID));
                command.Parameters.Add(new SqlParameter("@sInviteStatus ", p_AccountInvite.InviteStatus));
                command.Parameters.Add(new SqlParameter("@sInviteType ", p_AccountInvite.InviteType));

                command.Parameters.Add(new SqlParameter("@nTaskRecordID ", p_AccountInvite.TaskRecordID));
                command.Parameters.Add(new SqlParameter("@bAddAsTeamMember ", p_AccountInvite.AddAsTeamMember));
                command.Parameters.Add(new SqlParameter("@sRandomPassword ", p_AccountInvite.RandomPassword));



                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;

            }
        }






    }


    public static DataTable AccountInvite_Select(int? nAccountID, string sEmail, string sName,
       DateTime? dAcceptedDate, DateTime? dPaidToDate, DateTime? dExpiredDate, bool? bIsActive,
        DateTime? dDateAdded, DateTime? dDateUpdated, string sOrder,
      string sOrderDirection, int? nStartRow, int? nMaxRows, ref int iTotalRowsNum)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("AccountInvite_Select", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nAccountID", nAccountID));

                if (sEmail != "")
                    command.Parameters.Add(new SqlParameter("@sEmail", sEmail));

                if (sName != "")
                    command.Parameters.Add(new SqlParameter("@sName", sName));

                if (dAcceptedDate != null)
                    command.Parameters.Add(new SqlParameter("@dAcceptedDate", dAcceptedDate));
                if (dPaidToDate != null)
                    command.Parameters.Add(new SqlParameter("@dPaidToDate", dPaidToDate));
                if (dExpiredDate != null)
                    command.Parameters.Add(new SqlParameter("@dExpiredDate", dExpiredDate));

                if (bIsActive != null)
                    command.Parameters.Add(new SqlParameter("@bIsActive", bIsActive));
                if (dDateAdded != null)
                    command.Parameters.Add(new SqlParameter("@dDateAdded", dDateAdded));

                if (dDateUpdated != null)
                    command.Parameters.Add(new SqlParameter("@dDateUpdated", dDateUpdated));


                if (string.IsNullOrEmpty(sOrder) || string.IsNullOrEmpty(sOrderDirection))
                { sOrder = "AccountInviteID"; sOrderDirection = "DESC"; }

                command.Parameters.Add(new SqlParameter("@sOrder", sOrder + " " + sOrderDirection));

                if (nStartRow != null)
                    command.Parameters.Add(new SqlParameter("@nStartRow", nStartRow + 1));

                if (nMaxRows != null)
                    command.Parameters.Add(new SqlParameter("@nMaxRows", nMaxRows));




                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                System.Data.DataSet ds = new System.Data.DataSet();

                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();

                iTotalRowsNum = 0;
                if (ds == null) return null;


                if (ds.Tables.Count > 1)
                {
                    iTotalRowsNum = int.Parse(ds.Tables[1].Rows[0][0].ToString());
                }
                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
                {
                    return null;
                }


            }
        }
    }


    public static int AccountInvite_Delete(int nAccountInviteID)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("AccountInvite_Delete", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nAccountInviteID ", nAccountInviteID));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;

            }
        }
    }

    public static AccountInvite AccountInvite_Detail(int nAccountInviteID)
    {


        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("AccountInvite_Detail", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nAccountInviteID", nAccountInviteID));

                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AccountInvite temp = new AccountInvite(
                                (int)reader["AccountInviteID"],
                                reader["AccountID"] == DBNull.Value ? null : (int?)reader["AccountID"],
                                reader["InviterUserID"] == DBNull.Value ? null : (int?)reader["InviterUserID"],
                                reader["InvitedAccountID"] == DBNull.Value ? null : (int?)reader["InvitedAccountID"],
                                  reader["InvitedUserID"] == DBNull.Value ? null : (int?)reader["InvitedUserID"],
                               reader["Email"] == DBNull.Value ? "" : (string)reader["Email"],
                                reader["Name"] == DBNull.Value ? "" : (string)reader["Name"],
                                 reader["PhoneNumber"] == DBNull.Value ? "" : (string)reader["PhoneNumber"],
                                  reader["InviteStatus"] == DBNull.Value ? "" : (string)reader["InviteStatus"],
                                   reader["InviteType"] == DBNull.Value ? "" : (string)reader["InviteType"]

                                );
                            temp.DateAdded = reader["DateAdded"] == DBNull.Value ? null : (DateTime?)reader["DateAdded"];
                            temp.DateUpdated = reader["DateUpdated"] == DBNull.Value ? null : (DateTime?)reader["DateUpdated"];
                            temp.AcceptedDate = reader["AcceptedDate"] == DBNull.Value ? null : (DateTime?)reader["AcceptedDate"];
                            temp.PaidToDate = reader["PaidToDate"] == DBNull.Value ? null : (DateTime?)reader["PaidToDate"];
                            temp.ExpiredDate = reader["ExpiredDate"] == DBNull.Value ? null : (DateTime?)reader["ExpiredDate"];
                            temp.IsActive = reader["IsActive"] == DBNull.Value ? null : (bool?)reader["IsActive"];

                            temp.TaskRecordID = reader["TaskRecordID"] == DBNull.Value ? null : (int?)reader["TaskRecordID"];
                            temp.AddAsTeamMember = reader["AddAsTeamMember"] == DBNull.Value ? null : (bool?)reader["AddAsTeamMember"];
                            temp.RandomPassword = reader["RandomPassword"] == DBNull.Value ? "" : (string)reader["RandomPassword"];

                            connection.Close();
                            connection.Dispose();

                            return temp;
                        }

                    }
                }
                catch
                {

                }

                connection.Close();
                connection.Dispose();
                return null;

            }

        }





    }



    public static int Payment_Insert(Payment p_Payment)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Payment_Insert", connection))
            {

                command.CommandType = CommandType.StoredProcedure;

                SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int);
                pRV.Direction = ParameterDirection.Output;

                command.Parameters.Add(pRV);
                command.Parameters.Add(new SqlParameter("@nAccountID", p_Payment.AccountID));
                command.Parameters.Add(new SqlParameter("@nAccountInviteID", p_Payment.AccountInviteID));
                command.Parameters.Add(new SqlParameter("@nAmount", p_Payment.Amount));
                command.Parameters.Add(new SqlParameter("@dPaymentDate", p_Payment.PaymentDate));
                command.Parameters.Add(new SqlParameter("@sPaymentStatus", p_Payment.PaymentStatus));

                command.Parameters.Add(new SqlParameter("@sPaymentDetails", p_Payment.PaymentDetails));

                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Dispose();
                    return int.Parse(pRV.Value.ToString());
                }
                catch
                {
                    connection.Close();
                    connection.Dispose();

                }
                return -1;

            }
        }




    }

    public static int Payment_Update(Payment p_Payment)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Payment_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nPaymentID", p_Payment.PaymentID));
                command.Parameters.Add(new SqlParameter("@nAccountID", p_Payment.AccountID));
                command.Parameters.Add(new SqlParameter("@nAccountInviteID", p_Payment.AccountInviteID));
                command.Parameters.Add(new SqlParameter("@nAmount", p_Payment.Amount));
                command.Parameters.Add(new SqlParameter("@dPaymentDate", p_Payment.PaymentDate));
                command.Parameters.Add(new SqlParameter("@sPaymentStatus", p_Payment.PaymentStatus));

                command.Parameters.Add(new SqlParameter("@sPaymentDetails", p_Payment.PaymentDetails));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;

            }
        }






    }

    public static DataTable Payment_Select(int? nAccountID, int? nAccountInviteID, int? nAmount,
       DateTime? dPaymentDate, string sPaymentStatus, string sPaymentDetails,
        DateTime? dDateAdded, DateTime? dDateUpdated, string sOrder,
      string sOrderDirection, int? nStartRow, int? nMaxRows, ref int iTotalRowsNum)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Payment_Select", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nAccountID", nAccountID));

                if (nAccountInviteID != null)
                    command.Parameters.Add(new SqlParameter("@nAccountInviteID", nAccountInviteID));

                if (nAmount != null)
                    command.Parameters.Add(new SqlParameter("@nAmount", nAmount));

                if (dPaymentDate != null)
                    command.Parameters.Add(new SqlParameter("@dPaymentDate", dPaymentDate));
                if (sPaymentStatus != "")
                    command.Parameters.Add(new SqlParameter("@sPaymentStatus", sPaymentStatus));
                if (sPaymentDetails != "")
                    command.Parameters.Add(new SqlParameter("@sPaymentDetails", sPaymentDetails));

                if (dDateAdded != null)
                    command.Parameters.Add(new SqlParameter("@dDateAdded", dDateAdded));

                if (dDateUpdated != null)
                    command.Parameters.Add(new SqlParameter("@dDateUpdated", dDateUpdated));


                if (string.IsNullOrEmpty(sOrder) || string.IsNullOrEmpty(sOrderDirection))
                { sOrder = "PaymentID"; sOrderDirection = "DESC"; }

                command.Parameters.Add(new SqlParameter("@sOrder", sOrder + " " + sOrderDirection));

                if (nStartRow != null)
                    command.Parameters.Add(new SqlParameter("@nStartRow", nStartRow + 1));

                if (nMaxRows != null)
                    command.Parameters.Add(new SqlParameter("@nMaxRows", nMaxRows));




                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                System.Data.DataSet ds = new System.Data.DataSet();

                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();

                iTotalRowsNum = 0;
                if (ds == null) return null;


                if (ds.Tables.Count > 1)
                {
                    iTotalRowsNum = int.Parse(ds.Tables[1].Rows[0][0].ToString());
                }
                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
                {
                    return null;
                }


            }
        }
    }


    public static int Payment_Delete(int nPaymentID)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Payment_Delete", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nPaymentID ", nPaymentID));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;

            }
        }
    }

    public static Payment Payment_Detail(int nPaymentID)
    {


        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Payment_Details", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nPaymentID", nPaymentID));

                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Payment temp = new Payment(
                                (int)reader["PaymentID"],
                                reader["AccountID"] == DBNull.Value ? null : (int?)reader["AccountID"],
                                reader["AccountInviteID"] == DBNull.Value ? null : (int?)reader["AccountInviteID"],
                                reader["Amount"] == DBNull.Value ? null : (double?)reader["Amount"],
                                 reader["PaymentDate"] == DBNull.Value ? null : (DateTime?)reader["PaymentDate"],
                               reader["PaymentStatus"] == DBNull.Value ? "" : (string)reader["PaymentStatus"],
                                reader["PaymentDetails"] == DBNull.Value ? "" : (string)reader["PaymentDetails"]
                                );
                            temp.DateAdded = reader["DateAdded"] == DBNull.Value ? null : (DateTime?)reader["DateAdded"];
                            temp.DateUpdated = reader["DateUpdated"] == DBNull.Value ? null : (DateTime?)reader["DateUpdated"];


                            connection.Close();
                            connection.Dispose();

                            return temp;
                        }

                    }
                }
                catch
                {

                }

                connection.Close();
                connection.Dispose();
                return null;

            }

        }





    }





    public static int PaymentMethod_Insert(PaymentMethod p_PaymentMethod)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("PaymentMethod_Insert", connection))
            {

                command.CommandType = CommandType.StoredProcedure;

                SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int);
                pRV.Direction = ParameterDirection.Output;

                command.Parameters.Add(pRV);
                command.Parameters.Add(new SqlParameter("@nAccountID", p_PaymentMethod.AccountID));
                command.Parameters.Add(new SqlParameter("@sPaymentMethod", p_PaymentMethod.PaymentMethodP));
                command.Parameters.Add(new SqlParameter("@sDetails", p_PaymentMethod.Details));
                command.Parameters.Add(new SqlParameter("@sStatus", p_PaymentMethod.Status));
                command.Parameters.Add(new SqlParameter("@nPreferenceOrder", p_PaymentMethod.PreferenceOrder));


                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Dispose();
                    return int.Parse(pRV.Value.ToString());
                }
                catch
                {
                    connection.Close();
                    connection.Dispose();

                }
                return -1;

            }
        }




    }


    public static int PaymentMethod_Update(PaymentMethod p_PaymentMethod)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("PaymentMethod_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nPaymentMethodID", p_PaymentMethod.PaymentMethodID));

                command.Parameters.Add(new SqlParameter("@nAccountID", p_PaymentMethod.AccountID));
                command.Parameters.Add(new SqlParameter("@sPaymentMethod", p_PaymentMethod.PaymentMethodP));
                command.Parameters.Add(new SqlParameter("@sDetails", p_PaymentMethod.Details));
                command.Parameters.Add(new SqlParameter("@sStatus", p_PaymentMethod.Status));
                command.Parameters.Add(new SqlParameter("@nPreferenceOrder", p_PaymentMethod.PreferenceOrder));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;

            }
        }






    }



    public static DataTable PaymentMethod_Select(int nAccountID)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("PaymentMethod_Select", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nAccountID", nAccountID));

                
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                System.Data.DataSet ds = new System.Data.DataSet();

                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();
                               
                if (ds == null) return null;

                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
                {
                    return null;
                }


            }
        }
    }




    public static int PaymentMethod_Delete(int nPaymentMethodID)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("PaymentMethod_Delete", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nPaymentMethodID ", nPaymentMethodID));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;

            }
        }
    }


    public static PaymentMethod PaymentMethod_Detail(int nPaymentMethodID)
    {


        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("PaymentMethod_Details", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nPaymentMethodID", nPaymentMethodID));

                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PaymentMethod temp = new PaymentMethod(
                                (int)reader["PaymentMethodID"], reader["AccountID"] == DBNull.Value ? null : (int?)reader["AccountID"],
                               reader["PaymentMethod"] == DBNull.Value ? "" : (string)reader["PaymentMethod"],
                               reader["Details"] == DBNull.Value ? "" : (string)reader["Details"],
                               reader["Status"] == DBNull.Value ? "" : (string)reader["Status"],
                               reader["PreferenceOrder"] == DBNull.Value ? null : (int?)reader["PreferenceOrder"]
                                );

                            connection.Close();
                            connection.Dispose();

                            return temp;
                        }

                    }
                }
                catch
                {

                }

                connection.Close();
                connection.Dispose();
                return null;

            }
        }
    }

}




[Serializable]
public class Invoice
{
    private int? _iInvoiceID;
    private int? _iAccountID;
    private string _strAccountName;
    private int? _iAccountTypeID;
    private double? _dNetAmountAUD;
    private double? _dGSTAmountAUD;
    private double? _dGrossAmountAUD;
    private DateTime? _dateInvoiceDate;
    private DateTime? _dateStartDate;
    private DateTime? _dateEndDate;
    private string _strPaymentMethod;
    private int? _iPaypalID;
    private DateTime? _datePaidDate;
    private string _strNotes;
    private string _strOrganisationName;
    private string _strBillingEmail;
    private string _strBillingAddress;
    private string _strCountry;
    private string _strClientRef;
    private DateTime? _dateAdded;
    private DateTime? _dateUpdated;

    public double? PaidAmount { get; set; }

    public Invoice(int? p_iInvoiceID, int? p_iAccountID, string p_strAccountName,
        int? p_iAccountTypeID, double? p_dNetAmountAUD, double? p_dGSTAmountAUD, double? p_dGrossAmountAUD,
        DateTime? p_dateInvoiceDate, DateTime? p_dateStartDate, DateTime? p_dateEndDate,
        string p_strPaymentMethod, int? p_iPaypalID, DateTime? p_datePaidDate,
        string p_strNotes, string p_strOrganisationName, string p_strBillingEmail, string p_strBillingAddress,
        string p_strCountry, string p_strClientRef,
        DateTime? p_dateAdded, DateTime? p_dateUpdated)
    {
        _iInvoiceID = p_iInvoiceID;
        _iAccountID = p_iAccountID;
        _strAccountName = p_strAccountName;
        _iAccountTypeID = p_iAccountTypeID;
        _dNetAmountAUD = p_dNetAmountAUD;
        _dGSTAmountAUD = p_dGSTAmountAUD;
        _dGrossAmountAUD = p_dGrossAmountAUD;
        _dateInvoiceDate = p_dateInvoiceDate;
        _dateStartDate = p_dateStartDate;
        _dateEndDate = p_dateEndDate;
        _strPaymentMethod = p_strPaymentMethod;
        _iPaypalID = p_iPaypalID;
        _datePaidDate = p_datePaidDate;
        _strNotes = p_strNotes;
        _strOrganisationName = p_strOrganisationName;
        _strBillingEmail = p_strBillingEmail;
        _strBillingAddress = p_strBillingAddress;
        _strCountry = p_strCountry;
        _strClientRef = p_strClientRef;

        _dateAdded = p_dateAdded;
        _dateUpdated = p_dateUpdated;
    }

    public int? InvoiceID
    {
        get { return _iInvoiceID; }
        set { _iInvoiceID = value; }
    }
    public int? AccountID
    {
        get { return _iAccountID; }
        set { _iAccountID = value; }
    }
    public string AccountName
    {
        get { return _strAccountName; }
        set { _strAccountName = value; }
    }
    public int? AccountTypeID
    {
        get { return _iAccountTypeID; }
        set { _iAccountTypeID = value; }
    }





    public Double? NetAmountAUD
    {
        get { return _dNetAmountAUD; }
        set { _dNetAmountAUD = value; }
    }

    public Double? GSTAmountAUD
    {
        get { return _dGSTAmountAUD; }
        set { _dGSTAmountAUD = value; }
    }
    public Double? GrossAmountAUD
    {
        get { return _dGrossAmountAUD; }
        set { _dGrossAmountAUD = value; }
    }


    public DateTime? InvoiceDate
    {
        get { return _dateInvoiceDate; }
        set { _dateInvoiceDate = value; }
    }
    public DateTime? StartDate
    {
        get { return _dateStartDate; }
        set { _dateStartDate = value; }
    }
    public DateTime? EndDate
    {
        get { return _dateEndDate; }
        set { _dateEndDate = value; }
    }


    public string PaymentMethod
    {
        get { return _strPaymentMethod; }
        set { _strPaymentMethod = value; }
    }

    public int? PaypalID
    {
        get { return _iPaypalID; }
        set { _iPaypalID = value; }
    }

    public DateTime? PaidDate
    {
        get { return _datePaidDate; }
        set { _datePaidDate = value; }
    }


    public string Notes
    {
        get { return _strNotes; }
        set { _strNotes = value; }
    }

    public string OrganisationName
    {
        get { return _strOrganisationName; }
        set { _strOrganisationName = value; }
    }

    public string BillingEmail
    {
        get { return _strBillingEmail; }
        set { _strBillingEmail = value; }
    }

    public string BillingAddress
    {
        get { return _strBillingAddress; }
        set { _strBillingAddress = value; }
    }

    public string Country
    {
        get { return _strCountry; }
        set { _strCountry = value; }
    }

    public string ClientRef
    {
        get { return _strClientRef; }
        set { _strClientRef = value; }
    }

    public DateTime? DateAdded
    {
        get { return _dateAdded; }
        set { _dateAdded = value; }
    }
    public DateTime? DateUpdated
    {
        get { return _dateUpdated; }
        set { _dateUpdated = value; }
    }


}
﻿using System;
using System.Collections.Generic;
using System.Text;

public class JSONList : IJSONItem
{
    public string Name;
    public List<JSONObject> Objects;

    public JSONList(string name)
    {
        Name = name;
        Objects = new List<JSONObject>();
    }

    public JSONList(string name, List<JSONObject> objects)
    {
        Name = name;
        Objects = objects;
    }

    public JSONList AddObject(JSONObject obj)
    {
        if (obj != null)
        {
            Objects.Add(obj);
        }

        return this;
    }

    public override string ToString()
    {
        StringBuilder output = new StringBuilder();

        if (!String.IsNullOrWhiteSpace(Name))
        {
            output.Append("\"" + Name + "\":");
        }

        output.AppendLine("[");

        if (Objects.Count > 0)
        {
            bool firstObj = true;

            foreach (JSONObject obj in Objects)
            {
                if (!firstObj)
                {
                    output.Append(",");
                }

                output.AppendLine(obj.ToString());
                firstObj = false;
            }
        }

        output.AppendLine("]");
        return output.ToString();
    }

    private string JSONClean(string data)
    {
        if (!String.IsNullOrEmpty(data))
        {
            data = data.Replace("\r", "<br />");
            data = data.Replace("\n", "<br />");
            data = data.Replace("\t", "");

            // Replace the \ character with \\
            data = data.Replace(@"\", @"\\");

            // Replace the " character with \"
            string slashQuote = "\\" + '"';
            data = data.Replace("\"", slashQuote);

            // Replace HTML symbols.

            data = data.Replace("%20", " ");
            data = data.Replace("%2F", "/");
        }

        return data;
    }

    public JSONList Rename(string name)
    {
        this.Name = name;
        return this;
    }
}
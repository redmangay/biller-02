﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using XlsxExport;
using System.Configuration;
using System.Data.Common;
using System.Web;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Net;
using System.Web.Services.Protocols;
using DBGReportExecutionService;
/// <summary>
/// Summary description for DocumentManager
/// </summary>
public class ReportManager
{
	public ReportManager()
	{
		//
		// TODO: Add constructor logic here
		//
    }


    public static DataTable Generate_ReportParameters(int? recordId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ReportParameters", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                if (recordId.HasValue)
                    command.Parameters.Add(new SqlParameter("@nRecordID", recordId));
               

                SqlDataAdapter da = new SqlDataAdapter()
                {
                    SelectCommand = command
                };
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();

              
                if (ds != null && ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
                else
                {
                    return null;
                }
            }
        }
    }


    public static string GenerateReport(int reportId, int recordId, string strReportFormat)
    {
        try
        {
            List<ParameterValue> parameters = new List<ParameterValue>();

            DataTable dtReportParameters = null;
            dtReportParameters = ReportManager.Generate_ReportParameters(recordId);

            if (dtReportParameters != null)
            {
                for (int i = 0; i < dtReportParameters.Columns.Count; i++)
                {

                    string name = dtReportParameters.Columns[i].ToString();
                    string value = dtReportParameters.Rows[0][name].ToString();
                    if (value.Contains("|"))
                    {
                        string[] multiValues = value.Split('|');

                        foreach (string multiValue in multiValues)
                        {
                            parameters.Add(new ParameterValue() { Name = name, Value = multiValue });
                        }
                    }
                    else
                    {
                        if (value == "NULL")
                        {
                            value = null;
                        }

                        parameters.Add(new ParameterValue() { Name = name, Value = value });
                    }
                }
            }

            ReportManager.ReportFormat reportFormat = ReportManager.ReportFormat.PDF;

            if (strReportFormat == "Excel")
            {
                reportFormat = ReportManager.ReportFormat.Excel;
            }

            string strFilePath =  ReportManager.GenerateReport(reportId, parameters.ToArray(), reportFormat);

            return strFilePath;
        }
        catch (Exception ex)
        {
           // red
        }

        return null;
    }

    public static string GenerateReport(
        int reportId, 
        ParameterValue[] parameters, 
        ReportFormat reportFormat 
        )
    {

        

        Document theDocument = DocumentManager.ets_Document_Detail(reportId);
        string strReportServerDomain = SystemData.SystemOption_ValueByKey_Account("ReportServerDomain", null, null);
        string strReportServerURL = SystemData.SystemOption_ValueByKey_Account("ReportServerURL", null, null);
        string strReportServerPassword = SystemData.SystemOption_ValueByKey_Account("ReportServerPassword", null, null);
        string strReportServerUsername = SystemData.SystemOption_ValueByKey_Account("ReportServerUsername", null, null);

        string strCustomFileName = "";
        for (int i = 0; i < parameters.Length; i++)
        {
            if (parameters[i].Name == "Param_RecordID")
            {
                if (!String.IsNullOrEmpty(parameters[i].Value.ToString()))
                {
                    strCustomFileName = ReportManager.GetCustomReportFileName(int.Parse(parameters[i].Value.ToString()), theDocument.DocumentID);
                }
            }
        }

        string _strFilesLocation = "";
        string _strFilesPhisicalPath = "";

        if (HttpContext.Current.Session["FilesLocation"] != null)
            _strFilesLocation = HttpContext.Current.Session["FilesLocation"].ToString();
        if (HttpContext.Current.Session["FilesPhisicalPath"] != null)
            _strFilesPhisicalPath = HttpContext.Current.Session["FilesPhisicalPath"].ToString();

        string reportPath = theDocument.DocumentDescription;  //"/Associate_Sydney_Accounting_Test/rptPrintInvoice";
        string reportName = theDocument.DocumentText;
        string reportDirectory = _strFilesPhisicalPath + "\\UserFiles\\Export\\"; //ConfigurationManager.AppSettings["ReportDirectory"];
        string reportLocation = _strFilesLocation + "/UserFiles/Export/";
        DBGReportExecutionService.ReportExecutionService rs = new DBGReportExecutionService.ReportExecutionService();

        rs.Credentials = new NetworkCredential(
            strReportServerUsername, strReportServerPassword, strReportServerDomain
            );

        rs.Url = strReportServerURL; 
        byte[] result = null;
        
        string format = "";
        string actualFileName = "";
        string strUniqueName = Guid.NewGuid().ToString() + "_" + reportName.Replace(" ","_");
        strUniqueName = Common.GetValidFileName(strUniqueName);

        if (!String.IsNullOrEmpty(strCustomFileName))
        {
            strUniqueName = strCustomFileName;
        }

        if (reportFormat == ReportFormat.Excel)
        {
            format = "Excel";
            actualFileName = strUniqueName + ".xls";
        }
        else if (reportFormat == ReportFormat.PDF)
        {
            format = "PDF";
            actualFileName = strUniqueName + ".pdf";
        }


        try
        {


        string historyID = null;
        string devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
        string encoding;
        string mimeType;
        string extension;
        DBGReportExecutionService.Warning[] warnings = null;
        string[] streamIDs = null;
        DBGReportExecutionService.ExecutionInfo execInfo = new DBGReportExecutionService.ExecutionInfo();
        DBGReportExecutionService.ExecutionHeader execHeader = new DBGReportExecutionService.ExecutionHeader();
        rs.ExecutionHeaderValue = execHeader;
        execInfo = rs.LoadReport(reportPath, historyID);
        String SessionId = rs.ExecutionHeaderValue.ExecutionID;
        rs.Timeout = 1000 * 60 * 60; // 60 minutes

        rs.SetExecutionParameters(parameters, "en-GB");

        result = rs.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamIDs);
        execInfo = rs.GetExecutionInfo();

        FileStream stream = File.Create(reportDirectory + actualFileName, result.Length);
        stream.Write(result, 0, result.Length);
        stream.Close();

        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Print Report", ex.Message, ex.StackTrace, DateTime.Now, "Report Manager/Generate Report");
            SystemData.ErrorLog_Insert(theErrorLog);
        }

        return  reportLocation +  actualFileName;
    }

    public enum ReportFormat
    {
        Excel,
        PDF
    }


    public static string GetCustomReportFileName(int recordId, int? documentId)
    {

        string strCustomeFileName = "";


        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Report_CustomFileName", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = DBGurus.GetCommandTimeout();

                command.Parameters.Add(new SqlParameter("@nRecordID", recordId));
                command.Parameters.Add(new SqlParameter("@nDocumentID", documentId));

                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader[0] != DBNull.Value)
                            {
                                strCustomeFileName = reader[0].ToString();
                                break;
                            }
                        }
                    }
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();
            }
        }

        return strCustomeFileName;

    }



    public static int ets_Report_Clone(int fromId, int toId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Report_Clone", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nReportFromID", fromId));
                command.Parameters.Add(new SqlParameter("@nReportToID", toId));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;
            }
        }
    }

    public static int ets_Report_Insert(Report report)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Report_Insert", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(pRV);

                command.Parameters.Add(new SqlParameter("@nAccountID", report.AccountID));
                command.Parameters.Add(new SqlParameter("@sReportName", report.ReportName));
                command.Parameters.Add(new SqlParameter("@nReportDates", (int)report.ReportDates));
                command.Parameters.Add(new SqlParameter("@nReportDatesBefore", (int)report.ReportDatesBefore));

                if (!String.IsNullOrEmpty(report.ReportDescription))
                    command.Parameters.Add(new SqlParameter("@sReportDescription", report.ReportDescription));
                if (report.ReportStartDate.HasValue)
                    command.Parameters.Add(new SqlParameter("@dStartDate", report.ReportStartDate));
                if (report.ReportEndDate.HasValue)
                    command.Parameters.Add(new SqlParameter("@dEndDate", report.ReportEndDate));
                if (report.UserID.HasValue)
                    command.Parameters.Add(new SqlParameter("@nUserID", report.UserID.Value));
                if (report.DaysFrom.HasValue)
                    command.Parameters.Add(new SqlParameter("@nDaysFrom", report.DaysFrom.Value));
                if (report.DaysTo.HasValue)
                    command.Parameters.Add(new SqlParameter("@nDaysTo", report.DaysTo.Value));


                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Dispose();
                    return int.Parse(pRV.Value.ToString());
                }
                catch(Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "ets_Report_Insert", ex.Message, ex.StackTrace, DateTime.Now, "");
                    SystemData.ErrorLog_Insert(theErrorLog);

                    connection.Close();
                    connection.Dispose();
                }
                return -1;
            }
        }
    }

    public static Report ets_Report_Detail(int reportId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Report_Detail", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nReportID", reportId));
                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Report report = new Report(
                                (int) reader["ReportID"], (int) reader["AccountID"],
                                (string) reader["ReportName"],
                                reader["ReportDescription"] == DBNull.Value
                                    ? (string) null
                                    : (string) reader["ReportDescription"],
                                reader["ReportDates"] == DBNull.Value
                                    ? Report.ReportInterval.IntervalUndefined
                                    : (Enum.IsDefined(typeof(Report.ReportInterval), (int) reader["ReportDates"])
                                        ? (Report.ReportInterval) ((int) reader["ReportDates"])
                                        : Report.ReportInterval.IntervalUndefined),
                                reader["ReportDatesBefore"] == DBNull.Value
                                    ? Report.ReportIntervalEnd.Undefined
                                    : (Enum.IsDefined(typeof(Report.ReportIntervalEnd),
                                        (int) reader["ReportDatesBefore"])
                                        ? (Report.ReportIntervalEnd) ((int) reader["ReportDatesBefore"])
                                        : Report.ReportIntervalEnd.Undefined),
                                reader["StartDate"] == DBNull.Value ? null : (DateTime?) reader["StartDate"],
                                reader["EndDate"] == DBNull.Value ? null : (DateTime?) reader["EndDate"],
                                (DateTime) reader["DateAdded"], (DateTime) reader["DateUpdated"],
                                reader["UserID"] == DBNull.Value ? null : (int?) reader["UserID"],
                                reader["DaysFrom"] == DBNull.Value ? null : (int?) reader["DaysFrom"],
                                 reader["DaysTo"] == DBNull.Value ? null : (int?)reader["DaysTo"]
                            );
                            report.ScheduledFrequency = reader["ScheduledFrequency"] == DBNull.Value ? null : (int?)reader["ScheduledFrequency"];
                            connection.Close();
                            connection.Dispose();
                            return report;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                connection.Close();
                connection.Dispose();

                return null;
            }
        }
    }

    public static int ets_Report_Update(Report report)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Report_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nReportID", report.ReportID));
                command.Parameters.Add(new SqlParameter("@nAccountID", report.AccountID));
                command.Parameters.Add(new SqlParameter("@sReportName", report.ReportName));
                command.Parameters.Add(new SqlParameter("@nReportDates", (int)report.ReportDates));
                command.Parameters.Add(new SqlParameter("@nReportDatesBefore", (int)report.ReportDatesBefore));

                if (!String.IsNullOrEmpty(report.ReportDescription))
                    command.Parameters.Add(new SqlParameter("@sReportDescription", report.ReportDescription));
                if (report.ReportStartDate.HasValue)
                    command.Parameters.Add(new SqlParameter("@dStartDate", report.ReportStartDate));
                if (report.ReportEndDate.HasValue)
                    command.Parameters.Add(new SqlParameter("@dEndDate", report.ReportEndDate));
                if (report.UserID.HasValue)
                    command.Parameters.Add(new SqlParameter("@nUserID", report.UserID.Value));
                //RP Added Ticket 4513
                if(report.DaysFrom.HasValue)
                    command.Parameters.Add(new SqlParameter("@nDaysFrom", report.DaysFrom.Value));

                /* == Red 03022020: Ticket 5146 == */
                if (report.DaysTo.HasValue)
                    command.Parameters.Add(new SqlParameter("@nDaysTo", report.DaysTo.Value));


                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;
            }
        }
    }

    public static DataTable ets_Report_Select(int? reportId, int? accountId,
        int? tableId, int? graphId, string reportText, 
        Report.ReportInterval? reportInterval, DateTime? startDate, DateTime? endDate,
        DateTime? dateAdded, DateTime? dateUpdated, int? userId,
        string order, string orderDirection, int? startRow, int? maxRows, out int iTotalRowsNum)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Report_Select", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                if (reportId.HasValue)
                    command.Parameters.Add(new SqlParameter("@nReportID", reportId));
                if (accountId.HasValue)
                    command.Parameters.Add(new SqlParameter("@nAccountID", accountId));
                if (tableId.HasValue)
                    command.Parameters.Add(new SqlParameter("@nTableID", tableId));
                if (graphId.HasValue)
                    command.Parameters.Add(new SqlParameter("@nGraphID", graphId));
                if (!String.IsNullOrEmpty(reportText))
                    command.Parameters.Add(new SqlParameter("@sReportText", reportText));
                if (reportInterval.HasValue && (reportInterval.Value != Report.ReportInterval.IntervalUndefined))
                    command.Parameters.Add(new SqlParameter("@nReportDates", (int)reportInterval.Value));
                if (reportInterval.HasValue && reportInterval.Value == Report.ReportInterval.IntervalDateRange && startDate.HasValue)
                    command.Parameters.Add(new SqlParameter("@dStartDate", startDate));
                if (reportInterval.HasValue && reportInterval.Value == Report.ReportInterval.IntervalDateRange && endDate.HasValue)
                    command.Parameters.Add(new SqlParameter("@dEndtDate", endDate));
                if (dateAdded.HasValue)
                    command.Parameters.Add(new SqlParameter("@dDateAdded", dateAdded));
                if (dateUpdated.HasValue)
                    command.Parameters.Add(new SqlParameter("@dDateUpdated", dateUpdated));
                if (userId.HasValue)
                    command.Parameters.Add(new SqlParameter("@nUserID", userId));
                if (String.IsNullOrEmpty(order))
                    order = "ReportID";
                command.Parameters.Add(new SqlParameter("@sOrder", "[" + order + "] " + orderDirection));
                if (startRow.HasValue)
                    command.Parameters.Add(new SqlParameter("@nStartRow", startRow + 1));
                if (maxRows.HasValue)
                    command.Parameters.Add(new SqlParameter("@nMaxRows", maxRows));

                SqlDataAdapter da = new SqlDataAdapter()
                {
                    SelectCommand = command
                };
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();

                iTotalRowsNum = 0;
                if (ds != null && ds.Tables.Count > 1)
                {                 
                    iTotalRowsNum = int.Parse(ds.Tables[1].Rows[0][0].ToString());
                }
                if (ds != null && ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
                else
                {
                    return null;
                }
            }
        }
    }

    public static int ets_Report_Delete(int reportId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Report_Delete", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nReportID", reportId));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;
            }
        }
    }


    public static int ets_ReportItem_Insert(ReportItem reportItem)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportItem_Insert", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(pRV);

                command.Parameters.Add(new SqlParameter("@nReportID", reportItem.ReportID));
                command.Parameters.Add(new SqlParameter("@nObjectID", reportItem.ObjectId));
                command.Parameters.Add(new SqlParameter("@nItemType", (int)reportItem.ItemType));
                if (!reportItem.PeriodColumnID.HasValue)
                {
                    if (reportItem.ItemType == ReportItem.ReportItemType.ItemTypeTable)
                    {
                        DataTable dtColumns = Common.DataTableFromText(@"SELECT ColumnID FROM [Column] WHERE 
                            (ColumnType LIKE 'datetime' OR ColumnType LIKE 'date') AND DisplayTextSummary IS NOT NULL AND TableID = " + reportItem.ObjectId.ToString());
                        if (dtColumns.Rows.Count > 0)
                            reportItem.PeriodColumnID = (int) dtColumns.Rows[0]["ColumnID"];
                    }
                }
                if (reportItem.PeriodColumnID.HasValue)
                    command.Parameters.Add(new SqlParameter("@nPeriodColumnID", reportItem.PeriodColumnID.Value));
                if (!String.IsNullOrEmpty(reportItem.ItemTitle))
                    command.Parameters.Add(new SqlParameter("@sReportItemTitle", reportItem.ItemTitle));
                if (reportItem.ApplyFilter.HasValue)
                    command.Parameters.Add(new SqlParameter("@bApplyFilter", reportItem.ApplyFilter.Value));
                if (reportItem.ApplySort.HasValue)
                    command.Parameters.Add(new SqlParameter("@bApplySort", reportItem.ApplySort.Value));
                if (reportItem.UseColors.HasValue)
                    command.Parameters.Add(new SqlParameter("@bUseColors", reportItem.UseColors.Value));
                if (reportItem.HighlightWarnings.HasValue)
                    command.Parameters.Add(new SqlParameter("@bHighlightWarnings", reportItem.HighlightWarnings.Value));
                if (reportItem.UserID.HasValue)
                    command.Parameters.Add(new SqlParameter("@nUserID", reportItem.UserID.Value));

                if (reportItem.ViewID.HasValue)
                    command.Parameters.Add(new SqlParameter("@nViewID", reportItem.ViewID));

                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Dispose();
                    return int.Parse(pRV.Value.ToString());
                }
                catch(Exception)
                {
                    connection.Close();
                    connection.Dispose();
                }
                return -1;
            }
        }
    }

    public static int ets_ReportItem_Update(ReportItem reportItem)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportItem_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nReportItemID", reportItem.ReportItemID));
                command.Parameters.Add(new SqlParameter("@nObjectID", reportItem.ObjectId));
                command.Parameters.Add(new SqlParameter("@nItemType", (int)reportItem.ItemType));
                command.Parameters.Add(new SqlParameter("@sItemTitle", reportItem.ItemTitle));
                command.Parameters.Add(new SqlParameter("@bApplyFilter", reportItem.ApplyFilter));
                command.Parameters.Add(new SqlParameter("@bApplySort", reportItem.ApplySort));
                command.Parameters.Add(new SqlParameter("@bUseColors", reportItem.UseColors));
                command.Parameters.Add(new SqlParameter("@bHighlightWarnings", reportItem.HighlightWarnings));
                command.Parameters.Add(new SqlParameter("@nPeriodColumnID", reportItem.PeriodColumnID));
                command.Parameters.Add(new SqlParameter("@nPosition", reportItem.Position));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;
            }
        }
    }

    public static ReportItem ets_ReportItem_Detail(int reportItemId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportItem_Detail", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nReportItemID", reportItemId));
                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ReportItem reportItem = new ReportItem(
                                (int)reader["ReportItemID"],
                                (int)reader["ReportID"],
                                (int)reader["ObjectID"],
                                Enum.IsDefined(typeof(ReportItem.ReportItemType), (int)reader["ItemType"])
                                        ? (ReportItem.ReportItemType)((int)reader["ItemType"])
                                        : ReportItem.ReportItemType.ItemTypeUndefined,
                                reader["ItemTitle"].ToString(),
                                reader["ApplyFilter"] != DBNull.Value && (bool)reader["ApplyFilter"],
                                reader["ApplySort"] != DBNull.Value && (bool)reader["ApplySort"],
                                reader["UseColors"] != DBNull.Value && (bool)reader["UseColors"],
                                reader["HighlightWarnings"] != DBNull.Value && (bool)reader["HighlightWarnings"],
                                reader["PeriodColumnID"] == DBNull.Value ? null : (int?)reader["PeriodColumnID"],
                                (int)reader["ItemPosition"],
                                (DateTime)reader["DateAdded"], (DateTime)reader["DateUpdated"],
                                reader["UserID"] == DBNull.Value ? null : (int?)reader["UserID"]
                            );

                            connection.Close();
                            connection.Dispose();
                            return reportItem;
                        }
                    }
                }
                catch(Exception)
                {
                }
                connection.Close();
                connection.Dispose();

                return null;
            }
        }
    }


    public static int ets_ReportTableItem_Insert(ReportTableItem reportTableItem)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportTableItem_Insert", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(pRV);

                command.Parameters.Add(new SqlParameter("@nReportItemID", reportTableItem.ReportItemId));
                command.Parameters.Add(new SqlParameter("@nColumnID", reportTableItem.ColumnId));

                if (!String.IsNullOrEmpty(reportTableItem.ItemTitle))
                    command.Parameters.Add(new SqlParameter("@sColumnTitle", reportTableItem.ItemTitle));

                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Dispose();
                    return int.Parse(pRV.Value.ToString());
                }
                catch (Exception)
                {
                    connection.Close();
                    connection.Dispose();
                }
                return -1;
            }
        }
    }

    public static int ets_ReportTableItem_Update(ReportTableItem reportTableItem)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportTableItem_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nReportTableItemID", reportTableItem.ReportTableItemId));
                command.Parameters.Add(new SqlParameter("@nReportItemID", reportTableItem.ReportItemId));
                command.Parameters.Add(new SqlParameter("@nColumnID", reportTableItem.ColumnId));
                command.Parameters.Add(new SqlParameter("@sColumnTitle", reportTableItem.ItemTitle));
                command.Parameters.Add(new SqlParameter("@nPosition", reportTableItem.Position));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;
            }
        }
    }

    public static ReportTableItem ets_ReportTableItem_Detail(int reportTableItemId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportTableItem_Detail", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nReportTableItemID", reportTableItemId));
                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ReportTableItem reportTableItem = new ReportTableItem(
                                (int)reader["ReportTableItemID"],
                                (int)reader["ReportItemID"],
                                (int)reader["ColumnID"],
                                reader["ColumnTitle"].ToString(),
                                (int)reader["ColumnPosition"]
                            );

                            connection.Close();
                            connection.Dispose();
                            return reportTableItem;
                        }
                    }
                }
                catch
                {
                }
                connection.Close();
                connection.Dispose();

                return null;
            }
        }
    }


    public static int ets_ReportItemFilter_Insert(ReportItemFilter reportItemFilter)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportItemFilter_Insert", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(pRV);

                command.Parameters.Add(new SqlParameter("@nReportItemID", reportItemFilter.ReportItemID));
                command.Parameters.Add(new SqlParameter("@nFilterColumnID", reportItemFilter.FilterColumnID));
                if (!String.IsNullOrEmpty(reportItemFilter.FilterColumnValue))
                    command.Parameters.Add(new SqlParameter("@sFilterColumnValue", reportItemFilter.FilterColumnValue));
                if (!String.IsNullOrEmpty(reportItemFilter.FilterOperator))
                    command.Parameters.Add(new SqlParameter("@sFilterOperator", reportItemFilter.FilterOperator));
                command.Parameters.Add(new SqlParameter("@nDisplayOrder", reportItemFilter.DisplayOrder));
                if (!String.IsNullOrEmpty(reportItemFilter.JoinOperator))
                    command.Parameters.Add(new SqlParameter("@sJoinOperator", reportItemFilter.JoinOperator));

               

                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Dispose();
                    return int.Parse(pRV.Value.ToString());
                }
                catch (Exception)
                {
                    connection.Close();
                    connection.Dispose();
                }
                return -1;
            }
        }
    }

    public static int ets_ReportItemFilter_Update(ReportItemFilter reportItemFilter)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportItemFilter_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nReportItemFilterID", reportItemFilter.ReportItemFilterID));
                command.Parameters.Add(new SqlParameter("@nReportItemID", reportItemFilter.ReportItemID));
                command.Parameters.Add(new SqlParameter("@nFilterColumnID", reportItemFilter.FilterColumnID));
                command.Parameters.Add(new SqlParameter("@sFilterColumnValue", reportItemFilter.FilterColumnValue));
                command.Parameters.Add(new SqlParameter("@sFilterOperator", reportItemFilter.FilterOperator));
                command.Parameters.Add(new SqlParameter("@nDisplayOrder", reportItemFilter.DisplayOrder));
                command.Parameters.Add(new SqlParameter("@sJoinOperator", reportItemFilter.JoinOperator));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;
            }
        }
    }

    public static ReportItemFilter ets_ReportItemFilter_Detail(int reportItemFilterId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportItemFilter_Detail", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nReportItemFilterID", reportItemFilterId));
                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ReportItemFilter reportItemFilter = new ReportItemFilter(
                                (int)reader["ReportItemFilterID"],
                                (int)reader["ReportItemID"],
                                (int)reader["FilterColumnID"],
                                reader["FilterColumnValue"] == DBNull.Value ? (string)null : (string)reader["FilterColumnValue"],
                                reader["FilterOperator"] == DBNull.Value ? (string)null : (string)reader["FilterOperator"],
                                (int)reader["DisplayOrder"],
                                reader["JoinOperator"] == DBNull.Value ? (string)null : (string)reader["JoinOperator"]
                            );

                            connection.Close();
                            connection.Dispose();
                            return reportItemFilter;
                        }
                    }
                }
                catch
                {
                }
                connection.Close();
                connection.Dispose();

                return null;
            }
        }
    }

    public static DataTable ets_ReportItemFilter_Select(int reportItemId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportItemFilter_Select", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nReportItemId", reportItemId));

                SqlDataAdapter da = new SqlDataAdapter()
                {
                    SelectCommand = command
                };
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();

                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
                else
                {
                    return null;
                }
            }
        }
    }


    public static int ets_ReportItemSortOrder_Insert(ReportItemSortOrder reportItemSortOrder)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportItemSortOrder_Insert", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(pRV);

                command.Parameters.Add(new SqlParameter("@nReportItemID", reportItemSortOrder.ReportItemID));
                command.Parameters.Add(new SqlParameter("@nSortColumnID", reportItemSortOrder.SortColumnID));
                if (reportItemSortOrder.IsDescending.HasValue)
                    command.Parameters.Add(new SqlParameter("@bIsDescending", reportItemSortOrder.IsDescending.Value));
                command.Parameters.Add(new SqlParameter("@nDisplayOrder", reportItemSortOrder.DisplayOrder));

                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Dispose();
                    return int.Parse(pRV.Value.ToString());
                }
                catch (Exception)
                {
                    connection.Close();
                    connection.Dispose();
                }
                return -1;
            }
        }
    }

    public static int ets_ReportItemSortOrder_Update(ReportItemSortOrder reportItemSortOrder)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportItemSortOrder_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nReportItemSortOrderID", reportItemSortOrder.ReportItemSortOrderID));
                command.Parameters.Add(new SqlParameter("@nReportItemID", reportItemSortOrder.ReportItemID));
                command.Parameters.Add(new SqlParameter("@nSortColumnID", reportItemSortOrder.SortColumnID));
                command.Parameters.Add(new SqlParameter("@bIsDescending", reportItemSortOrder.IsDescending));
                command.Parameters.Add(new SqlParameter("@nDisplayOrder", reportItemSortOrder.DisplayOrder));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;
            }
        }
    }

    public static ReportItemSortOrder ets_ReportItemSortOrder_Detail(int reportItemSortOrderId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportItemSortOrder_Detail", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nReportItemFilterID", reportItemSortOrderId));
                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ReportItemSortOrder reportItemSortOrder = new ReportItemSortOrder(
                                (int)reader["ReportItemSortOrderID"],
                                (int)reader["ReportItemID"],
                                (int)reader["SortColumnID"],
                                reader["IsDescending"] != DBNull.Value && (bool)reader["IsDescending"],
                                (int)reader["DisplayOrder"]
                            );

                            connection.Close();
                            connection.Dispose();
                            return reportItemSortOrder;
                        }
                    }
                }
                catch
                {
                }
                connection.Close();
                connection.Dispose();

                return null;
            }
        }
    }

    public static DataTable ets_ReportItemSortOrder_Select(int reportItemId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportItemSortOrder_Select", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nReportItemId", reportItemId));

                SqlDataAdapter da = new SqlDataAdapter()
                {
                    SelectCommand = command
                };
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();

                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
                else
                {
                    return null;
                }
            }
        }
    }


    public static ReportView ets_ReportView_Detail(int reportViewId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportView_Detail", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nDataViewID", reportViewId));
                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ReportView reportView = new ReportView(
                                (int)reader["DataViewID"],
                                (string)reader["ViewName"],
                                (int)reader["AccountID"],
                                (string)reader["SPName"],
                                (bool)reader["IsActive"]
                            );

                            connection.Close();
                            connection.Dispose();
                            return reportView;
                        }
                    }
                }
                catch (Exception)
                {
                }
                connection.Close();
                connection.Dispose();

                return null;
            }
        }
    }

    public static SqlParameterCollection ets_ReportView_GetSPParameters(string spName)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand(spName, connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                connection.Open();
                try
                {
                    SqlCommandBuilder.DeriveParameters(command);
                    return command.Parameters;
                }
                catch (Exception)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }

    public static DataTable ets_ReportView_GetColumns(int reportViewId, int accountId, int userId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            string spName = String.Empty;

            using (SqlCommand command = new SqlCommand("ets_ReportView_Detail", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nDataViewID", reportViewId));
                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                            spName = (string) reader["SPName"];
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    connection.Close();
                }
            }

            if (!String.IsNullOrEmpty(spName))
            {
                using (SqlCommand command = new SqlCommand(spName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@AccountID", accountId));
                    command.Parameters.Add(new SqlParameter("@UserID", userId));
                    command.Parameters.Add(new SqlParameter("@sType", "header"));

                    SqlDataAdapter da = new SqlDataAdapter()
                    {
                        SelectCommand = command
                    };
                    DataTable dt = new DataTable();
                    DataSet ds = new DataSet();

                    connection.Open();
                    try
                    {
                        da.Fill(ds);
                    }
                    catch
                    {
                        //
                    }
                    connection.Close();
                    connection.Dispose();

                    if (ds.Tables.Count > 0)
                    {
                        return ds.Tables[0];
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
    }

    public static DataTable ets_ReportView_GetData(int reportViewId, int accountId, int userId, int reportId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            string spName = String.Empty;

            using (SqlCommand command = new SqlCommand("ets_ReportView_Detail", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nDataViewID", reportViewId));
                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                            spName = (string)reader["SPName"];
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    connection.Close();
                }
            }

            if (!String.IsNullOrEmpty(spName))
            {
                using (SqlCommand command = new SqlCommand(spName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@AccountID", accountId));
                    command.Parameters.Add(new SqlParameter("@UserID", userId));
                    command.Parameters.Add(new SqlParameter("@ReportID", reportId));
                    command.Parameters.Add(new SqlParameter("@sType", "data"));

                    SqlDataAdapter da = new SqlDataAdapter()
                    {
                        SelectCommand = command
                    };
                    DataTable dt = new DataTable();
                    DataSet ds = new DataSet();

                    connection.Open();
                    try
                    {
                        da.Fill(ds);
                    }
                    catch
                    {
                        //
                    }
                    connection.Close();
                    connection.Dispose();

                    if (ds.Tables.Count > 0)
                    {
                        return ds.Tables[0];
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
    }

    public static DataSet ets_ReportView_GetAll(int reportViewId, int accountId, int userId, int reportId,
        Dictionary<string, dynamic> parameters)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            string spName = String.Empty;

            using (SqlCommand command = new SqlCommand("ets_ReportView_Detail", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nDataViewID", reportViewId));
                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                            spName = (string) reader["SPName"];
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    connection.Close();
                }
            }

            if (!String.IsNullOrEmpty(spName))
            {
                using (SqlCommand command = new SqlCommand(spName, connection))
                {
                    command.CommandTimeout = DBGurus.GetCommandTimeout();
                    command.CommandType = CommandType.StoredProcedure;
                    if (accountId > 0)
                        command.Parameters.Add(new SqlParameter("@AccountID", accountId));
                    command.Parameters.Add(new SqlParameter("@UserID", userId));
                    command.Parameters.Add(new SqlParameter("@ReportID", reportId));
                    command.Parameters.Add(new SqlParameter("@sType", "full"));

                    System.Text.StringBuilder errors = new System.Text.StringBuilder();
                    SqlParameterCollection spParameters = ReportManager.ets_ReportView_GetSPParameters(spName);
                    foreach (string key in parameters.Keys)
                    {
                        bool isValid = false;
                        foreach (SqlParameter p in spParameters)
                        {
                            if (String.Compare(p.ParameterName, "@" + key, StringComparison.OrdinalIgnoreCase) == 0)
                            {
                                isValid = true;
                                break;
                            }
                        }
                        if (isValid)
                        {
                            command.Parameters.Add(new SqlParameter("@" + key, parameters[key]));
                        }
                        else
                        {
                            ErrorLog theErrorLog = new ErrorLog(null, "Report Manager",
                                String.Format("Missed parameter {0} in View SP {1}", key, spName),
                                "ets_ReportView_GetData", DateTime.Now, "");
                            SystemData.ErrorLog_Insert(theErrorLog);
                        }
                    }

                    SqlDataAdapter da = new SqlDataAdapter()
                    {
                        SelectCommand = command
                    };
                    DataTable dt = new DataTable();
                    DataSet ds = new DataSet();

                    connection.Open();
                    try
                    {
                        da.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "Report Manager", ex.Message,
                            "ets_ReportView_GetData", DateTime.Now, "");
                        SystemData.ErrorLog_Insert(theErrorLog);
                    }
                    connection.Close();
                    connection.Dispose();

                    if (ds.Tables.Count > 1)
                    {
                        return ds;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                return null;
            }
        }
    }


    public static ReportSchedule ets_ReportSchedule_Detail(int reportId)
    {
        ReportSchedule reportSchedule = null;
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportSchedule_Detail", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nReportID", reportId));
                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader["ScheduledFrequency"] != DBNull.Value)
                            {
                                ReportSchedule.ReportScheduleFrequency frequency = ReportSchedule.ReportScheduleFrequency.Undefined;
                                Enum.TryParse(reader["ScheduledFrequency"].ToString(), out frequency);
                                switch (frequency)
                                {
                                    case ReportSchedule.ReportScheduleFrequency.Undefined:
                                        reportSchedule =
                                            new ReportSchedule(ReportSchedule.ReportScheduleFrequency.Undefined);
                                        break;
                                    case ReportSchedule.ReportScheduleFrequency.OneOff:
                                        if (reader["NextRunDateTime"] != DBNull.Value)
                                            reportSchedule =
                                                new ReportSchedule(ReportSchedule.ReportScheduleFrequency.OneOff,
                                                    (DateTime) reader["NextRunDateTime"]);
                                        else
                                            reportSchedule =
                                                new ReportSchedule(ReportSchedule.ReportScheduleFrequency.Undefined);
                                        break;
                                    case ReportSchedule.ReportScheduleFrequency.Weekly:
                                        if (reader["NextRunDateTime"] != DBNull.Value && reader["ScheduleOptions"] != DBNull.Value)
                                        {
                                            string[] s = reader["ScheduleOptions"].ToString().Split(';');
                                            if (s.Length == 8)
                                            {
                                                bool isOk = true;
                                                int repeatEvery = 0;
                                                if (int.TryParse(s[0], out repeatEvery))
                                                {
                                                    if (repeatEvery < 1)
                                                        repeatEvery = 1;
                                                    else
                                                    {
                                                        if (repeatEvery > 12)
                                                            repeatEvery = 12;
                                                    }
                                                }
                                                else
                                                {
                                                    isOk = false;
                                                }
                                                for (int i = 1; i < 8; i++)
                                                    if (s[i] != "0" && s[i] != "1")
                                                        isOk = false;

                                                if (isOk)
                                                {
                                                    reportSchedule =
                                                        new ReportSchedule(
                                                            ReportSchedule.ReportScheduleFrequency.Weekly,
                                                            (DateTime) reader["NextRunDateTime"], repeatEvery)
                                                        {
                                                            WeeklyMonday = s[1] == "1",
                                                            WeeklyTuesday = s[2] == "1",
                                                            WeeklyWednesday = s[3] == "1",
                                                            WeeklyThurdsay = s[4] == "1",
                                                            WeeklyFriday = s[5] == "1",
                                                            WeeklySaturday = s[6] == "1",
                                                            WeeklySunday = s[7] == "1",
                                                        };
                                                }
                                                else
                                                    reportSchedule =
                                                        new ReportSchedule(ReportSchedule.ReportScheduleFrequency
                                                            .Undefined);
                                            }
                                        }
                                        else
                                            reportSchedule =
                                                new ReportSchedule(ReportSchedule.ReportScheduleFrequency.Undefined);
                                        break;
                                    case ReportSchedule.ReportScheduleFrequency.Monthly:
                                        if (reader["NextRunDateTime"] != DBNull.Value && reader["ScheduleOptions"] != DBNull.Value)
                                        {
                                            string[] s = reader["ScheduleOptions"].ToString().Split(';');
                                            if (s.Length == 3 || s.Length == 4)
                                            {
                                                bool isOk = true;
                                                int repeatEvery = 0;
                                                if (int.TryParse(s[0], out repeatEvery))
                                                {
                                                    if (repeatEvery < 1)
                                                        repeatEvery = 1;
                                                    else
                                                    {
                                                        if (repeatEvery > 12)
                                                            repeatEvery = 12;
                                                    }
                                                }
                                                else
                                                {
                                                    isOk = false;
                                                }

                                                ReportSchedule.ReportScheduleMonthlyMode monthlyMode = ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth;
                                                if (!Enum.TryParse(s[1], out monthlyMode))
                                                    isOk = false;

                                                int dayOfMonth = 0;
                                                int weekOfMonth = 0;
                                                DayOfWeek dayOfWeek = DayOfWeek.Monday;
                                                switch (monthlyMode)
                                                {
                                                    case ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth:
                                                        if (s.Length != 3)
                                                            isOk = false;
                                                        else
                                                        {
                                                            if (!int.TryParse(s[2], out dayOfMonth))
                                                                isOk = false;
                                                        }
                                                        break;
                                                    case ReportSchedule.ReportScheduleMonthlyMode.DayOfWeek:
                                                        if (s.Length != 4)
                                                            isOk = false;
                                                        else
                                                        {
                                                            if (!int.TryParse(s[2], out weekOfMonth))
                                                                isOk = false;
                                                            if (!Enum.TryParse(s[3], out dayOfWeek))
                                                                isOk = false;
                                                        }
                                                        break;
                                                }

                                                if (isOk)
                                                {
                                                    reportSchedule =
                                                        new ReportSchedule(
                                                            ReportSchedule.ReportScheduleFrequency.Monthly,
                                                            (DateTime) reader["NextRunDateTime"], repeatEvery);
                                                    switch (monthlyMode)
                                                    {
                                                        case ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth:
                                                            reportSchedule.MonthlyMode = ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth;
                                                            reportSchedule.DayOfMonth = dayOfMonth;
                                                            break;
                                                        case ReportSchedule.ReportScheduleMonthlyMode.DayOfWeek:
                                                            reportSchedule.MonthlyMode = ReportSchedule.ReportScheduleMonthlyMode.DayOfWeek;
                                                            reportSchedule.WeekOfMonth = weekOfMonth;
                                                            reportSchedule.MonthlyDayOfWeek = dayOfWeek;
                                                            break;
                                                    }
                                                }
                                                else
                                                    reportSchedule =
                                                        new ReportSchedule(ReportSchedule.ReportScheduleFrequency
                                                            .Undefined);
                                            }
                                        }
                                        else
                                            reportSchedule =
                                                new ReportSchedule(ReportSchedule.ReportScheduleFrequency.Undefined);
                                        break;
                                }
                            }

                            connection.Close();
                            connection.Dispose();
                            return reportSchedule;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                connection.Close();
                connection.Dispose();

                return reportSchedule;
            }
        }
    }

    public static int ets_ReportSchedule_SetNotScheduled(int reportId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportSchedule_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nReportID", reportId));
                command.Parameters.Add(new SqlParameter("@nScheduledFrequency",(object) (int) ReportSchedule.ReportScheduleFrequency.Undefined));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;
            }
        }
    }

    public static int ets_ReportSchedule_SetOneOffSchedule(int reportId, DateTime runDateTime)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportSchedule_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nReportID", reportId));
                command.Parameters.Add(new SqlParameter("@nScheduledFrequency", (int) ReportSchedule.ReportScheduleFrequency.OneOff));
                command.Parameters.Add(new SqlParameter("@dtNextRunDateTime", runDateTime));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;
            }
        }
    }

    public static int ets_ReportSchedule_SetMonthlySchedule(int reportId, DateTime runDateTime,
        int repeatEvery, int dayNumber)
    {
        runDateTime = GetNextRunDateTime(DateTime.Now, runDateTime, repeatEvery, dayNumber);
        string s = repeatEvery.ToString() + ";";
        s += ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth.ToString() + ";";
        s += dayNumber.ToString();
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportSchedule_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nReportID", reportId));
                command.Parameters.Add(new SqlParameter("@nScheduledFrequency", (int)ReportSchedule.ReportScheduleFrequency.Monthly));
                command.Parameters.Add(new SqlParameter("@dtNextRunDateTime", runDateTime));
                command.Parameters.Add(new SqlParameter("@sScheduleOptions", s));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;
            }
        }
    }

    public static int ets_ReportSchedule_SetMonthlySchedule(int reportId, DateTime runDateTime,
        int repeatEvery, int weekNumber, DayOfWeek dayOfWeek)
    {
        runDateTime = GetNextRunDateTime(DateTime.Now, runDateTime, repeatEvery, weekNumber, dayOfWeek);
        string s = repeatEvery.ToString() + ";";
        s += ReportSchedule.ReportScheduleMonthlyMode.DayOfWeek.ToString() + ";";
        s += weekNumber.ToString() + ";";
        s += dayOfWeek.ToString();
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportSchedule_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nReportID", reportId));
                command.Parameters.Add(new SqlParameter("@nScheduledFrequency", (int)ReportSchedule.ReportScheduleFrequency.Monthly));
                command.Parameters.Add(new SqlParameter("@dtNextRunDateTime", runDateTime));
                command.Parameters.Add(new SqlParameter("@sScheduleOptions", s));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;
            }
        }
    }

    public static int ets_ReportSchedule_SetWeeklySchedule(int reportId, DateTime runDateTime, int repeatEvery,
        bool onMonday, bool onTuesday, bool onWednesday, bool onThursday, bool onFriday, bool onSaturday, bool onSunday)
    {
        runDateTime = GetNextRunDateTime(DateTime.Now, runDateTime, repeatEvery, onMonday, onTuesday, onWednesday, onThursday,
            onFriday, onSaturday, onSunday);
        string s = repeatEvery.ToString() + ";";
        s += onMonday ? "1;" : "0;";
        s += onTuesday ? "1;" : "0;";
        s += onWednesday ? "1;" : "0;";
        s += onThursday ? "1;" : "0;";
        s += onFriday ? "1;" : "0;";
        s += onSaturday ? "1;" : "0;";
        s += onSunday ? "1" : "0";
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportSchedule_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nReportID", reportId));
                command.Parameters.Add(new SqlParameter("@nScheduledFrequency", (int)ReportSchedule.ReportScheduleFrequency.Weekly));
                command.Parameters.Add(new SqlParameter("@dtNextRunDateTime", runDateTime));
                command.Parameters.Add(new SqlParameter("@sScheduleOptions", s));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;
            }
        }
    }

    public static List<DateTime> ets_ReportSchedule_GetScheduledDates(DateTime now, DateTime runDateTime, int repeatEvery,
        bool onMonday, bool onTuesday, bool onWednesday, bool onThursday, bool onFriday, bool onSaturday, bool onSunday, int number)
    {
        List<DateTime> ret = new List<DateTime>();
        for (int i = 0; i < number; i++)
        {
            DateTime nextDateTime = GetNextRunDateTime(now, runDateTime, repeatEvery, onMonday, onTuesday, onWednesday,
                onThursday, onFriday, onSaturday, onSunday);
            ret.Add(nextDateTime);
            runDateTime = nextDateTime;
            now = nextDateTime.AddMinutes(1);
        }
        return ret;
    }

    public static List<DateTime> ets_ReportSchedule_GetScheduledDates(DateTime now, DateTime runDateTime,
        int repeatEvery, int dayNumber, int number)
    {
        List<DateTime> ret = new List<DateTime>();
        for (int i = 0; i < number; i++)
        {
            DateTime nextDateTime = GetNextRunDateTime(now, runDateTime, repeatEvery, dayNumber);
            ret.Add(nextDateTime);
            runDateTime = nextDateTime;
            now = nextDateTime.AddMinutes(1);
        }
        return ret;
    }

    public static List<DateTime> ets_ReportSchedule_GetScheduledDates(DateTime now, DateTime runDateTime,
        int repeatEvery, int weekNumber, DayOfWeek dayOfWeek, int number)
    {
        List<DateTime> ret = new List<DateTime>();
        for (int i = 0; i < number; i++)
        {
            DateTime nextDateTime = GetNextRunDateTime(now, runDateTime, repeatEvery, weekNumber, dayOfWeek);
            ret.Add(nextDateTime);
            runDateTime = nextDateTime;
            now = nextDateTime.AddMinutes(1);
        }
        return ret;
    }

    private static DateTime GetNextRunDateTime(DateTime now, DateTime runDateTime, int repeatEvery,
        bool onMonday, bool onTuesday, bool onWednesday, bool onThursday, bool onFriday, bool onSaturday, bool onSunday)
    {
        DateTime nextRunDate = new DateTime();

        if (now > runDateTime)
        {
            DayOfWeek startDateDayOfWeek = runDateTime.DayOfWeek;
            int startOfWeekShift = startDateDayOfWeek == DayOfWeek.Sunday ? 6 : (int) startDateDayOfWeek - 1;
            DateTime startOfWeek = (new DateTime(runDateTime.Year, runDateTime.Month, runDateTime.Day)).AddDays(-1 * startOfWeekShift);
            DateTime endOfWeek = startOfWeek.AddDays(7).AddHours(23).AddMinutes(59).AddSeconds(59);
            if (now > endOfWeek)
            {
                int startScheduledDayShift = 0;
                if (onMonday)
                    startScheduledDayShift = 0;
                else
                {
                    if (onTuesday)
                        startScheduledDayShift = 1;
                    else
                    {
                        if (onWednesday)
                            startScheduledDayShift = 2;
                        else
                        {
                            if (onThursday)
                                startScheduledDayShift = 3;
                            else
                            {
                                if (onFriday)
                                    startScheduledDayShift = 4;
                                else
                                {
                                    if (onSaturday)
                                        startScheduledDayShift = 5;
                                    else
                                    {
                                        if (onSunday)
                                            startScheduledDayShift = 6;
                                    }
                                }
                            }
                        }
                    }
                }
                nextRunDate = startOfWeek.AddDays(startScheduledDayShift).AddHours(runDateTime.Hour).AddMinutes(runDateTime.Minute);
                while (nextRunDate < now)
                    nextRunDate = nextRunDate.AddDays(7 * repeatEvery);
            }
            else
            {
                int weekDaysLeft = now.DayOfWeek == DayOfWeek.Sunday ? 0 : 7 - (int)now.DayOfWeek;
                bool onThisWeek = false;
                nextRunDate = new DateTime(now.Year, now.Month, now.Day, runDateTime.Hour, runDateTime.Minute, 0);
                for (int i = 0; i <= weekDaysLeft; i++)
                {
                    if (nextRunDate > now)
                    {
                        switch (nextRunDate.DayOfWeek)
                        {
                            case DayOfWeek.Monday:
                                if (onMonday)
                                    onThisWeek = true;
                                break;
                            case DayOfWeek.Tuesday:
                                if (onTuesday)
                                    onThisWeek = true;
                                break;
                            case DayOfWeek.Wednesday:
                                if (onWednesday)
                                    onThisWeek = true;
                                break;
                            case DayOfWeek.Thursday:
                                if (onThursday)
                                    onThisWeek = true;
                                break;
                            case DayOfWeek.Friday:
                                if (onFriday)
                                    onThisWeek = true;
                                break;
                            case DayOfWeek.Saturday:
                                if (onSaturday)
                                    onThisWeek = true;
                                break;
                            case DayOfWeek.Sunday:
                                if (onSunday)
                                    onThisWeek = true;
                                break;
                        }
                    }
                    if (onThisWeek)
                        break;
                    nextRunDate = nextRunDate.AddDays(1);
                }
                if (!onThisWeek)
                {
                    nextRunDate = startOfWeek.AddDays(7 * repeatEvery).AddHours(runDateTime.Hour).AddMinutes(runDateTime.Minute);
                    for (int i = 1; i <= 7; i++)
                    {
                        switch (nextRunDate.DayOfWeek)
                        {
                            case DayOfWeek.Monday:
                                if (onMonday)
                                    onThisWeek = true;
                                break;
                            case DayOfWeek.Tuesday:
                                if (onTuesday)
                                    onThisWeek = true;
                                break;
                            case DayOfWeek.Wednesday:
                                if (onWednesday)
                                    onThisWeek = true;
                                break;
                            case DayOfWeek.Thursday:
                                if (onThursday)
                                    onThisWeek = true;
                                break;
                            case DayOfWeek.Friday:
                                if (onFriday)
                                    onThisWeek = true;
                                break;
                            case DayOfWeek.Saturday:
                                if (onSaturday)
                                    onThisWeek = true;
                                break;
                            case DayOfWeek.Sunday:
                                if (onSunday)
                                    onThisWeek = true;
                                break;
                        }
                        if (onThisWeek)
                            break;
                        nextRunDate = nextRunDate.AddDays(1);
                    }
                }
            }
        }
        else
        {
            nextRunDate = runDateTime;
            bool found = false;
            for (int i = 1; i <= 7; i++)
            {
                switch (nextRunDate.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        if (onMonday)
                            found = true;
                        break;
                    case DayOfWeek.Tuesday:
                        if (onTuesday)
                            found = true;
                        break;
                    case DayOfWeek.Wednesday:
                        if (onWednesday)
                            found = true;
                        break;
                    case DayOfWeek.Thursday:
                        if (onThursday)
                            found = true;
                        break;
                    case DayOfWeek.Friday:
                        if (onFriday)
                            found = true;
                        break;
                    case DayOfWeek.Saturday:
                        if (onSaturday)
                            found = true;
                        break;
                    case DayOfWeek.Sunday:
                        if (onSunday)
                            found = true;
                        break;
                }
                if (found)
                    break;
                nextRunDate = nextRunDate.AddDays(1);
            }
        }

        return nextRunDate;
    }

    private static DateTime GetNextRunDateTime(DateTime now, DateTime runDateTime, int repeatEvery, int dayNumber)
    {
        DateTime nextRunDate = runDateTime;

        int month = runDateTime.Month;
        int year = runDateTime.Year;
        while (nextRunDate < now)
        {
            month += repeatEvery;
            if (month > 12)
            {
                month -= 12;
                year++;
            }
            // *** Google Calendar style:
            //if (dayNumber > DateTime.DaysInMonth(year, month))
            //    continue;
            //else
            //{
            //    nextRunDate = new DateTime(year, month, dayNumber);
            //}
            if (dayNumber > DateTime.DaysInMonth(year, month))
            {
                if (dayNumber == 31)
                    dayNumber = DateTime.DaysInMonth(year, month);
                else
                    continue;
            }
            nextRunDate = new DateTime(year, month, dayNumber);
        }

        return nextRunDate;
    }

    private static DateTime GetNextRunDateTime(DateTime now, DateTime runDateTime, int repeatEvery, int weekNumber, DayOfWeek dayOfWeek)
    {
        DateTime nextRunDate = runDateTime;

        int month = runDateTime.Month;
        int year = runDateTime.Year;
        while (nextRunDate < now)
        {
            month += repeatEvery;
            if (month > 12)
            {
                month -= 12;
                year++;
            }
            DateTime dayAtFirstWeek = new DateTime(year, month, 1);
            while (dayAtFirstWeek.DayOfWeek != dayOfWeek)
                dayAtFirstWeek = dayAtFirstWeek.AddDays(1);
            int dayNumber = dayAtFirstWeek.Day + 7 * (weekNumber - 1);
            if (dayNumber > DateTime.DaysInMonth(year, month))
            {
                if (weekNumber == 5)
                    dayNumber -= 7;
                else
                    continue;
            }
            nextRunDate = new DateTime(year, month, dayNumber);
        }

        return nextRunDate;
    }


    public static int ets_ReportRecipient_Insert(ReportRecipient reportRecipient)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ReportRecipient_Insert", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(pRV);

                command.Parameters.Add(new SqlParameter("@nReportID", reportRecipient.ReportId));
                command.Parameters.Add(new SqlParameter("@sEmail", reportRecipient.Email));

                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Dispose();
                    return int.Parse(pRV.Value.ToString());
                }
                catch (Exception)
                {
                    connection.Close();
                    connection.Dispose();
                }
                return -1;
            }
        }
    }
    //RP Added Ticket 4513
    public void SetDates(ref DateTime? startdate, ref DateTime? enddate, Int32 reportId)
    {
        Report report = ReportManager.ets_Report_Detail(reportId);
        DateTime? reportStartDate = null;
        DateTime? reportEndDate = null;

        if ((report != null) && !String.IsNullOrEmpty(report.ReportName))
        {
            switch (report.ReportDatesBefore)
            {
                case Report.ReportIntervalEnd.Today:
                    reportEndDate = DateTime.Today.AddDays(1);
                    break;
                case Report.ReportIntervalEnd.EndOfLastWeek:
                    reportEndDate =
                        DateTime.Today.AddDays(DateTime.Today.DayOfWeek == DayOfWeek.Sunday
                            ? -6
                            : 1 - (int)DateTime.Today.DayOfWeek);
                    break;
                case Report.ReportIntervalEnd.EndOfLastMonth:
                    reportEndDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                    break;
                case Report.ReportIntervalEnd.EndOfLastYear:
                    reportEndDate = new DateTime(DateTime.Today.Year, 1, 1);
                    break;
                case Report.ReportIntervalEnd.LastDataPoint:
                    reportEndDate = GetLastDataPoint(reportId);
                    break;
                case Report.ReportIntervalEnd.EndOfCustom:
                    reportEndDate = DateTime.Today.AddDays(Int32.Parse(report.DaysTo.ToString()) * -1); 
                    break;
                case Report.ReportIntervalEnd.EndOfCustom2:
                    reportEndDate = DateTime.Today.AddDays(Int32.Parse(report.DaysTo.ToString()));
                    break;
            }

            switch (report.ReportDates)
            {
                case Report.ReportInterval.Interval30Days:
                    if (reportEndDate.HasValue)
                        reportStartDate = reportEndDate.Value.AddDays(-30);
                    break;
                case Report.ReportInterval.Interval60Days:
                    if (reportEndDate.HasValue)
                        reportStartDate = reportEndDate.Value.AddDays(-60);
                    break;
                case Report.ReportInterval.Interval90Days:
                    if (reportEndDate.HasValue)
                        reportStartDate = reportEndDate.Value.AddDays(-90);
                    break;
                case Report.ReportInterval.Interval1Year:
                    if (reportEndDate.HasValue)
                        reportStartDate = reportEndDate.Value.AddYears(-1);
                    break;
                case Report.ReportInterval.IntervalYTD:
                    if (reportEndDate.HasValue)
                        reportStartDate = new DateTime(reportEndDate.Value.Year, 1, 1, 0, 0, 0);
                    break;
                case Report.ReportInterval.IntervalCustom:
                    //if (reportEndDate.HasValue && report.DaysFrom.HasValue)
                        reportStartDate = DateTime.Today.AddDays(Int32.Parse(report.DaysFrom.ToString()) * -1);
                    //reportStartDate = reportEndDate.Value.AddDays((Int32.Parse(report.Days.ToString()) * -1)-1);
                    break;
                case Report.ReportInterval.IntervalCustom2:
                        reportStartDate = DateTime.Today.AddDays(Int32.Parse(report.DaysFrom.ToString()));
                    //reportStartDate = reportEndDate.Value.AddDays((Int32.Parse(report.Days.ToString()) * -1)-1);
                    break;
                case Report.ReportInterval.IntervalDateRange:
                    if (report.ReportStartDate.HasValue && report.ReportEndDate.HasValue)
                    {
                        reportStartDate = report.ReportStartDate.Value;
                        reportEndDate = report.ReportEndDate.Value.AddDays(1);
                    }
                    break;
            }

        }
        startdate = reportStartDate;
        enddate = reportEndDate;
    }

    private DateTime? GetLastDataPoint(int reportId)
    {
        DateTime? dateReturn = null;

        DataTable dtReportItems = Common.DataTableFromText("SELECT ReportItemID, ItemType FROM ReportItem WHERE ReportID =" +
                                                           reportId.ToString() + " ORDER BY ItemPosition");

        foreach (DataRow row in dtReportItems.Rows)
        {
            int nItemType = (int)row["ItemType"];
            if (Enum.IsDefined(typeof(ReportItem.ReportItemType), nItemType))
            {
                ReportItem.ReportItemType itemType = (ReportItem.ReportItemType)nItemType;
                if (itemType == ReportItem.ReportItemType.ItemTypeTable)
                {
                    int reportItemId = (int)row["ReportItemID"];
                    ReportItem reportItem = ReportManager.ets_ReportItem_Detail(reportItemId);

                    if (reportItem.PeriodColumnID.HasValue)
                    {
                        WhereClause whereClauseSettings = null;
                        if (reportItem.ApplyFilter.HasValue && reportItem.ApplyFilter.Value)
                        {
                            DataTable filter = ReportManager.ets_ReportItemFilter_Select(reportItemId);
                            foreach (DataRow filterRow in filter.Rows)
                            {
                                string filterOperator = string.Empty;
                                if (filterRow.IsNull("FilterOperator") ||
                                    String.IsNullOrEmpty(filterRow["FilterOperator"].ToString()))
                                    break;
                                else
                                {
                                    filterOperator = filterRow["FilterOperator"].ToString();
                                }

                                if (whereClauseSettings == null)
                                    whereClauseSettings = new WhereClause((int)filterRow["FilterColumnID"],
                                        filterOperator, filterRow["FilterColumnValue"].ToString());
                                else
                                {
                                    string joinOperator = string.Empty;
                                    if (filterRow.IsNull("JoinOperator") ||
                                        String.IsNullOrEmpty(filterRow["JoinOperator"].ToString()))
                                        break;
                                    else
                                    {
                                        joinOperator = filterRow["JoinOperator"].ToString();
                                    }
                                    if (joinOperator == "or")
                                        whereClauseSettings.Or((int)filterRow["FilterColumnID"], filterOperator,
                                            filterRow["FilterColumnValue"].ToString());
                                    else
                                        whereClauseSettings.And((int)filterRow["FilterColumnID"], filterOperator,
                                            filterRow["FilterColumnValue"].ToString());
                                }
                            }
                        }

                        string whereClauseString = String.Empty;
                        if (whereClauseSettings != null)
                            whereClauseString = whereClauseSettings.GetWhereClauseString();

                        Column periodColumn = RecordManager.ets_Column_Details(reportItem.PeriodColumnID.Value);
                        if (periodColumn != null)
                        {
                            string query = String.Format(
                                "SET DATEFORMAT dmy; SELECT MAX(CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103)) FROM [Record] WHERE [TableID] = {1}",
                                periodColumn.SystemName, reportItem.ObjectId);
                            if (!String.IsNullOrEmpty(whereClauseString))
                                query += " AND " + whereClauseString;
                            DataTable dtLastDataPoint = Common.DataTableFromText(query);
                            if (dtLastDataPoint != null && dtLastDataPoint.Rows.Count > 0)
                                dateReturn = (DateTime)dtLastDataPoint.Rows[0][0];
                        }
                    }
                    return dateReturn;
                }
            }
        }
        return null;
    }
    //End Modification
    public string RunScheduledReports()
    {
        int nReports = 0;
        int nSent = 0;
        int nFailed = 0;
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Report_GetScheduled", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@dtNow", DateTime.Now));

                SqlDataAdapter da = new SqlDataAdapter()
                {
                    SelectCommand = command
                };
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();

                if (ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        int reportId = (int)dr["ReportID"];
                        Report report = ReportManager.ets_Report_Detail(reportId);
                        if (report != null)
                        {
                            nReports++;
                            XlsxExport.XlsxReportWriter xlw = new XlsxExport.XlsxReportWriter();
                            using (MemoryStream stream = new MemoryStream())
                            {
                                int accountId = 0;
                                accountId = (int)report.AccountID;
                                try
                                {
                                    //RP Modified Ticket 4513
                                    //xlw.CreateWorkbook(stream, reportId, null, null, accountId);
                                    DateTime? startdate = null, enddate = null;
                                    SetDates(ref startdate, ref enddate, reportId);
                                    xlw.CreateWorkbook(stream, reportId, startdate, enddate, accountId);
                                    //End Of Modification
                                }
                                catch (Exception ex)
                                {
                                    ErrorLog theErrorLog = new ErrorLog(null, "ReportManager.RunScheduledReports - create",
                                        ex.Message, ex.StackTrace, DateTime.Now, "App_Code");
                                    SystemData.ErrorLog_Insert(theErrorLog);
                                    nFailed++;
                                }
                                stream.Position = 0;
                                System.Net.Mime.ContentType ct =
                                    new System.Net.Mime.ContentType(
                                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                                System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(stream, ct);
                                attachment.ContentDisposition.FileName = report.ReportName + ".xlsx";
                                string messageSubject = report.ReportName;
                                string messageBody = String.Empty;

                                //System.Web.HttpContext context = System.Web.HttpContext.Current;
                                //context.Response.ContentType =
                                //    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                //context.Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=\"{0}.xlsx\"", report.ReportName));
                                //stream.Position = 0;
                                //stream.CopyTo(context.Response.OutputStream);
                                //context.Response.Flush();
                                //context.Response.End();

                                DataTable dtRecipients =
                                    Common.DataTableFromText("SELECT * FROM ReportRecipient WHERE ReportID =" +
                                                             reportId.ToString());
                                foreach (DataRow recipient in dtRecipients.Rows)
                                {
                                    string sAddress = recipient["Email"].ToString();
                                    try
                                    {
                                        string sSendEmailError = "";
                                        Message theMessage = new Message(null, null, null, accountId,
                                            DateTime.Now, "R", "E",
                                            null, sAddress, messageSubject, messageBody, null, "");
                                        if (DBGurus.SendEmail("Report", true, null, messageSubject, messageBody, "",
                                                sAddress,
                                                "", "", null, theMessage, out sSendEmailError, attachment) != -1)
                                            nSent++;
                                    }
                                    catch (Exception ex) // ??? should not happen because of try/catch block in SendEmail
                                    {
                                        ErrorLog theErrorLog = new ErrorLog(null, "ReportManager.RunScheduledReports - send",
                                            ex.Message, ex.StackTrace, DateTime.Now, "App_Code");
                                        SystemData.ErrorLog_Insert(theErrorLog);
                                    }
                                }
                            }

                            ReportSchedule reportSchedule = ReportManager.ets_ReportSchedule_Detail(reportId);
                            if (reportSchedule != null && reportSchedule.RunDateTime.HasValue)
                            {
                                DateTime runDateTime = reportSchedule.RunDateTime.Value;
                                System.Collections.Generic.List<DateTime> nextRuns = null;
                                switch (reportSchedule.Frequency)
                                {
                                    case ReportSchedule.ReportScheduleFrequency.OneOff:
                                        ets_ReportSchedule_SetNotScheduled(reportId);
                                        break;
                                    case ReportSchedule.ReportScheduleFrequency.Weekly:
                                        nextRuns = ets_ReportSchedule_GetScheduledDates(DateTime.Now, runDateTime,
                                            reportSchedule.RepeatEvery,
                                            reportSchedule.WeeklyMonday,
                                            reportSchedule.WeeklyTuesday,
                                            reportSchedule.WeeklyWednesday,
                                            reportSchedule.WeeklyTuesday,
                                            reportSchedule.WeeklyFriday,
                                            reportSchedule.WeeklySaturday,
                                            reportSchedule.WeeklySunday,
                                            1);
                                        ets_ReportSchedule_SetWeeklySchedule(reportId, nextRuns[0],
                                            reportSchedule.RepeatEvery,
                                            reportSchedule.WeeklyMonday,
                                            reportSchedule.WeeklyTuesday,
                                            reportSchedule.WeeklyWednesday,
                                            reportSchedule.WeeklyTuesday,
                                            reportSchedule.WeeklyFriday,
                                            reportSchedule.WeeklySaturday,
                                            reportSchedule.WeeklySunday);
                                        break;
                                    case ReportSchedule.ReportScheduleFrequency.Monthly:
                                        switch (reportSchedule.MonthlyMode)
                                        {
                                            case ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth:
                                                nextRuns = ets_ReportSchedule_GetScheduledDates(DateTime.Now,
                                                    runDateTime,
                                                    reportSchedule.RepeatEvery, runDateTime.Day, 1);
                                                ets_ReportSchedule_SetMonthlySchedule(reportId, nextRuns[0],
                                                    reportSchedule.RepeatEvery,
                                                    reportSchedule.DayOfMonth);
                                                break;
                                            case ReportSchedule.ReportScheduleMonthlyMode.DayOfWeek:
                                                nextRuns = ets_ReportSchedule_GetScheduledDates(DateTime.Now,
                                                    runDateTime,
                                                    reportSchedule.RepeatEvery, (runDateTime.Day - 1) / 7 + 1,
                                                    runDateTime.DayOfWeek, 1);
                                                ets_ReportSchedule_SetMonthlySchedule(reportId, nextRuns[0],
                                                    reportSchedule.RepeatEvery,
                                                    reportSchedule.WeekOfMonth,
                                                    reportSchedule.MonthlyDayOfWeek);
                                                break;
                                        }

                                        break;
                                }
                            }
                            else
                            {
                                ErrorLog theErrorLog = new ErrorLog(null, "ReportManager.RunScheduledReports",
                                    "Unexpected error",
                                    reportSchedule == null
                                        ? "reportSchedule is null"
                                        : "reportSchedule.RunDateTime has no value", DateTime.Now, "App_Code");
                                SystemData.ErrorLog_Insert(theErrorLog);
                            }
                        }
                        else
                        {
                            ErrorLog theErrorLog = new ErrorLog(null, "ReportManager.RunScheduledReports",
                                "Unexpected error", "report is null", DateTime.Now, "App_Code");
                            SystemData.ErrorLog_Insert(theErrorLog);
                        }
                    }
                }
            }
        }

        return String.Format("{0} reports; {1} failed; {2} sent", nReports, nFailed, nSent);
    }

}

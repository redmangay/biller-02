﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for DynamicControlWM
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class DynamicControlWM : System.Web.Services.WebService
{

    public DynamicControlWM()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    public string GetTrafficLightImage(Column theTrafficLightColumn, string strSelectedTLValue, string strTLValues)
    {
        return Common.TrafficLightURL(theTrafficLightColumn, strSelectedTLValue, strTLValues);
    }
}

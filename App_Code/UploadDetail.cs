﻿using System;
//using System.Data;
//using System.Linq;
//using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;

[Serializable]
public class UploadDetail
{
    public bool IsReady { get; set; }
    public int ContentLength { get; set; }
    public int UploadedLength { get; set; }
    public string FileName { get; set; }
    public string FileUniqueName { get; set; }
}

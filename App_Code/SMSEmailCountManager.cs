﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SMSEmailCountManager
{
    public class SMSEmailCount
    {

        public string SMSEmailCountReset(string paramIn, out string paramOut)
        {
            paramOut = "";
            bool bResult = false;

            using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                using (SqlCommand command = new SqlCommand("Account_ResetNoficationCount", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add("@bReturnValue", SqlDbType.Bit);
                    command.Parameters["@bReturnValue"].Direction = ParameterDirection.Output;


                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                        bResult = (bool) command.Parameters["@bReturnValue"].Value;

                    }
                    catch (Exception ex)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "SMS Email Count Manager", ex.Message, ex.StackTrace, DateTime.Now, "");
                        SystemData.ErrorLog_Insert(theErrorLog);

                    }
                    connection.Close();
                    connection.Dispose();

                }
            }

            if (bResult)
                return "Count has been reset.";
            else
                return "Checked.";
        }

    }
}
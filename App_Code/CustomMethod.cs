﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
//using ChartDirector;
//using DocGen.DAL;

/// <summary>
/// Summary description for CustomMethod
/// </summary>
public class CustomMethod
{
	public CustomMethod()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public static List<object> DotNetMethod(string sMethodName, List<object> objList)
    {
         List<object> roList = new List<object>();
        try
        {
            string strSite = Common.GetDatabaseName().Replace("thedatabase_", "") + ".thedatabase.net";


            switch (strSite.ToLower())
            {                
                case "associatesydneyaccountingtest.thedatabase.net":
                case "windtechaccountingtest.thedatabase.net":
                case "associatesydney.thedatabase.net":
		        case "biller01.thedatabase.net":
                case "biller01patch.thedatabase.net":
                case "biller01test.thedatabase.net":
                case "windtech.thedatabase.net":
                    roList = CustomMethod_windtech.DotNetMethod(sMethodName, objList);
                    return roList;
                case "renzo.thedatabase.net":
                    roList = CustomMethod_windtech.DotNetMethod(sMethodName, objList);
                    return roList;                
                default:
                    roList = CustomMethod_windtech.DotNetMethod(sMethodName,objList);
                    return roList;
            }

            
        }
        catch(Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "DotNetMethod/SP" + sMethodName, ex.Message, ex.StackTrace, DateTime.Now, "App_Code");
            SystemData.ErrorLog_Insert(theErrorLog);
            if (roList != null)
                roList.Add(ex);
        }
        return null;
    }

    public static List<object> DotNetMethod(string sMethodName, List<object> objList, string client )
    {

        List<object> roList = new List<object>();
        try
        {
            switch (client)
            {
                case "ind":
                    roList = CustomMethod_ind.DotNetMethod(sMethodName, objList);
                    return roList;
                default:
                    roList = CustomMethod_dev.DotNetMethod(sMethodName, objList);
                    return roList;
            }


        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "DotNetMethod with accountId" + sMethodName, ex.Message, ex.StackTrace, DateTime.Now, "App_Code");
            SystemData.ErrorLog_Insert(theErrorLog);
            if (roList != null)
                roList.Add(ex);
        }
        return null;

    }

    
}
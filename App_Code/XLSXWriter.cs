﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Color = DocumentFormat.OpenXml.Spreadsheet.Color;
using Font = DocumentFormat.OpenXml.Spreadsheet.Font;
using Ss = DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Xml;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using Charts = DocumentFormat.OpenXml.Drawing.Charts;
using Graphic = DocumentFormat.OpenXml.Drawing.Graphic;

namespace XlsxExport
{
    public class XlsxWriter : IDisposable
    {
        private const int StyleTextNormal = 0;
        private const int StyleTextBold = 1;
        private const int StyleTextBoldCentered = 2;
        private const int StyleDateTimeNormal = 3;
        private const int StyleShortDateNormal = 4;
        private const int StyleTimeNormal = 5;
        private const int StyleShortTimeNormal = 6;
        private const int StyleNumber0Normal = 7;
        private const int StyleNumber1Normal = 8;
        private const int StyleNumber2Normal = 9;
        private const int StyleNumber3Normal = 10;
        private const int StyleNumber4Normal = 11;
        private const int StyleTextNormalLight = 12;
        private const int StyleTextBoldCenteredLight = 13;
        private const int StyleDateTimeNormalLight = 14;
        private const int StyleShortDateNormalLight = 15;
        
        private const uint LastFixedStyleIndex = 15;

        private enum CellSpecialFill
        {
            Normal = 0,
            Warning,
            Exceedance
        }

        private bool _disposed;
        private readonly SpreadsheetDocument _spreadSheet;
        private readonly WorkbookStylesPart _workbookStylesPart;
        private Tuple<string, CellSpecialFill, long>[] _customStyles;

        public XlsxWriter(MemoryStream stream)
        {
            _spreadSheet = SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook);

            // create the workbook
            _spreadSheet.AddWorkbookPart();

            _workbookStylesPart = _spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();
            _spreadSheet.WorkbookPart.Workbook = new Workbook();
            _spreadSheet.WorkbookPart.Workbook.AppendChild(new Sheets());

            _customStyles = new Tuple<string, CellSpecialFill, long>[0];
        }

        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _spreadSheet.Dispose();
                }

                _disposed = true;
            }
        }


        internal void FinalizeWorkbook()
        {
            _workbookStylesPart.Stylesheet = CreateStyleSheet();
            //_workbookStylesPart.Stylesheet.Save();
            _spreadSheet.WorkbookPart.Workbook.Save();
        }

        internal Stylesheet CreateStyleSheet()
        {
            Stylesheet workBookStylesheet = new Stylesheet();

            NumberingFormats numberingFormats = new NumberingFormats(
                new Ss.NumberingFormat
                {
                    NumberFormatId = 165U,
                    FormatCode = "0.0"
                },
                new Ss.NumberingFormat
                {
                    NumberFormatId = 166U,
                    FormatCode = "0.000"
                },
                new Ss.NumberingFormat
                {
                    NumberFormatId = 167U,
                    FormatCode = "0.0000"
                }
            );

            Ss.Fonts fonts = new Ss.Fonts(
                new Font // all fields are optional
                {
                    FontSize = new Ss.FontSize { Val = 11D },
                    Color = new Color { Theme = 1U },
                    FontName = new FontName { Val = "Calibri" },
                    FontFamilyNumbering = new FontFamilyNumbering { Val = 2 },
                    FontCharSet = new FontCharSet { Val = 1 },
                    FontScheme = new Ss.FontScheme { Val = FontSchemeValues.Minor }
                },
                new Font // "Bold" is required, other are optional
                {
                    Bold = new Bold(),
                    FontSize = new Ss.FontSize { Val = 11D },
                    Color = new Color { Theme = 1U },
                    FontName = new FontName { Val = "Calibri" },
                    FontFamilyNumbering = new FontFamilyNumbering { Val = 2 },
                    FontCharSet = new FontCharSet { Val = 1 },
                    FontScheme = new Ss.FontScheme { Val = FontSchemeValues.Minor }
                },
                new Font
                {
                    FontSize = new Ss.FontSize { Val = 11D },
                    Color = new Color { Theme = 0U, Tint = -0.35D },
                    FontName = new FontName { Val = "Calibri" },
                    FontFamilyNumbering = new FontFamilyNumbering { Val = 2 },
                    FontCharSet = new FontCharSet { Val = 1 },
                    FontScheme = new Ss.FontScheme { Val = FontSchemeValues.Minor }
                },
                new Font
                {
                    Bold = new Bold(),
                    FontSize = new Ss.FontSize { Val = 11D },
                    Color = new Color { Theme = 0U, Tint = -0.35D },
                    FontName = new FontName { Val = "Calibri" },
                    FontFamilyNumbering = new FontFamilyNumbering { Val = 2 },
                    FontCharSet = new FontCharSet { Val = 1 },
                    FontScheme = new Ss.FontScheme { Val = FontSchemeValues.Minor }
                })
            {
                KnownFonts = true
            };

            Dictionary<string, uint> customFonts = new Dictionary<string, uint>();
            uint customFontIndex = (uint)fonts.ChildElements.Count - 1;
            for (int i = 0; i < _customStyles.Length; i++)
            {
                if (!String.IsNullOrEmpty(_customStyles[i].Item1) && !customFonts.ContainsKey(_customStyles[i].Item1))
                {
                    customFonts.Add(_customStyles[i].Item1, ++customFontIndex);
                    fonts.AppendChild(new Font
                    {
                        FontSize = new Ss.FontSize {Val = 11D},
                        Color = new Color {Rgb = "FF" + _customStyles[i].Item1},
                        FontName = new FontName {Val = "Calibri"},
                        FontFamilyNumbering = new FontFamilyNumbering {Val = 2},
                        FontCharSet = new FontCharSet {Val = 1},
                        FontScheme = new Ss.FontScheme {Val = FontSchemeValues.Minor}
                    });
                }
            }

            Fills fills = new Fills(
                new Ss.Fill( // all fields are optional
                    new Ss.PatternFill { PatternType = PatternValues.None }),
                new Ss.Fill(
                    new Ss.PatternFill { PatternType = PatternValues.Gray125 }),
                new Ss.Fill(
                    new Ss.PatternFill(
                        new Ss.ForegroundColor() { Rgb = "FFFFFF00" },
                        new Ss.BackgroundColor() { Indexed = (UInt32Value)64U })
                    {
                        PatternType = PatternValues.Solid
                    }),
                new Ss.Fill(
                    new Ss.PatternFill(
                        new Ss.ForegroundColor() { Rgb = "FFFFCC00" },
                        new Ss.BackgroundColor() { Indexed = (UInt32Value)64U })
                    {
                        PatternType = PatternValues.Solid
                    })
            );

            Borders borders = new Borders(
                new Border( // all fields are optional
                    new Ss.LeftBorder(),
                    new Ss.RightBorder(),
                    new Ss.TopBorder(),
                    new Ss.BottomBorder(),
                    new DiagonalBorder())
            );

            CellStyleFormats cellStyleFormats = new CellStyleFormats(
                new CellFormat()
                {
                    NumberFormatId = 0U,
                    FontId = 0U,
                    FillId = 0U,
                    BorderId = 0U
                }
            );

            CellFormats cellFormats = new CellFormats(
                CreateCellFormat(0),
                CreateCellFormat(1),
                CreateCellFormat(2),
                CreateCellFormat(3),
                CreateCellFormat(4),
                CreateCellFormat(5),
                CreateCellFormat(6),
                CreateCellFormat(7),
                CreateCellFormat(8),
                CreateCellFormat(9),
                CreateCellFormat(10),
                CreateCellFormat(11),
                CreateCellFormat(12),
                CreateCellFormat(13),
                CreateCellFormat(14),
                CreateCellFormat(15)
            );

            for (int i = 0; i < _customStyles.Length; i++)
            {
                CellFormat cellFormat = CreateCellFormat(_customStyles[i].Item3);
                if (!String.IsNullOrEmpty(_customStyles[i].Item1))
                { 
                cellFormat.FontId = customFonts[_customStyles[i].Item1];
                cellFormat.ApplyFont = true;
                    }
                if (_customStyles[i].Item2 != CellSpecialFill.Normal)
                {
                    switch (_customStyles[i].Item2)
                    {
                        case CellSpecialFill.Warning:
                            cellFormat.FillId = 2u;
                            break;
                        case CellSpecialFill.Exceedance:
                            cellFormat.FillId = 3u;
                            break;
                    }
                    cellFormat.ApplyFill = true;
                }
                cellFormats.AppendChild(cellFormat);
            }

            workBookStylesheet.Append(
                numberingFormats,
                fonts,
                fills,
                borders,
                cellStyleFormats,
                cellFormats);

            return workBookStylesheet;
        }

        private CellFormat CreateCellFormat(long index)
        {
            CellFormat cellFormat = new CellFormat();
            switch (index)
            {
                case 0: // Normal
                    cellFormat.NumberFormatId =  0U;
                    cellFormat.FontId =  0U;
                    cellFormat.FillId =  0U;
                    cellFormat.BorderId =  0U;
                    cellFormat.FormatId =  0U;
                    break;
                case 1: // Normal Bold
                    cellFormat.NumberFormatId =  0U;
                    cellFormat.FontId =  1U; // bold
                    cellFormat.FillId =  0U;
                    cellFormat.BorderId =  0U;
                    cellFormat.FormatId =  0U;
                    cellFormat.ApplyFont = true;
                    break;
                case 2: // Normal Bold Centered
                    cellFormat.AppendChild(new Alignment {Horizontal = HorizontalAlignmentValues.Center});
                    cellFormat.NumberFormatId =  0U;
                    cellFormat.FontId =  1U; // bold
                    cellFormat.FillId =  0U;
                    cellFormat.BorderId =  0U;
                    cellFormat.FormatId =  0U;
                    cellFormat.ApplyFont = true;
                    cellFormat.ApplyAlignment = true;
                    break;
                case 3: // Date Time
                    cellFormat.NumberFormatId =  22U; // dd/mm/yyyy h:mm
                    cellFormat.FontId =  0U;
                    cellFormat.FillId =  0U;
                    cellFormat.BorderId =  0U;
                    cellFormat.FormatId =  0U;
                    cellFormat.ApplyNumberFormat = true;
                    break;
                case 4: // Short Date
                    cellFormat.NumberFormatId =  14U; // dd/mm/yyyy
                    cellFormat.FontId =  0U;
                    cellFormat.FillId =  0U;
                    cellFormat.BorderId =  0U;
                    cellFormat.FormatId =  0U;
                    cellFormat.ApplyNumberFormat = true;
                    break;
                case 5: // Time
                    cellFormat.NumberFormatId =  21U; // h:mm:ss
                    cellFormat.FontId =  0U;
                    cellFormat.FillId =  0U;
                    cellFormat.BorderId =  0U;
                    cellFormat.FormatId =  0U;
                    cellFormat.ApplyNumberFormat = true;
                    break;
                case 6: // Short Time
                    cellFormat.NumberFormatId = 20U; // h:mm
                    cellFormat.FontId = 0U;
                    cellFormat.FillId = 0U;
                    cellFormat.BorderId = 0U;
                    cellFormat.FormatId = 0U;
                    cellFormat.ApplyNumberFormat = true;
                    break;
                case 7: // Number 0
                    cellFormat.NumberFormatId =  1U; // 0
                    cellFormat.FontId =  0U;
                    cellFormat.FillId =  0U;
                    cellFormat.BorderId =  0U;
                    cellFormat.FormatId =  0U;
                    cellFormat.ApplyNumberFormat = true;
                    break;
                case 8: // Number 0.0
                    cellFormat.NumberFormatId =  165U; // 0.0
                    cellFormat.FontId =  0U;
                    cellFormat.FillId =  0U;
                    cellFormat.BorderId =  0U;
                    cellFormat.FormatId =  0U;
                    cellFormat.ApplyNumberFormat = true;
                    break;
                case 9: // Number 0.00
                    cellFormat.NumberFormatId =  2U; // 0.00
                    cellFormat.FontId =  0U;
                    cellFormat.FillId =  0U;
                    cellFormat.BorderId =  0U;
                    cellFormat.FormatId =  0U;
                    cellFormat.ApplyNumberFormat = true;
                    break;
                case 10: // Number 0.000
                    cellFormat.NumberFormatId =  166U; // 0.000
                    cellFormat.FontId =  0U;
                    cellFormat.FillId =  0U;
                    cellFormat.BorderId =  0U;
                    cellFormat.FormatId =  0U;
                    cellFormat.ApplyNumberFormat = true;
                    break;
                case 11: // Number 0.0000
                    cellFormat.NumberFormatId =  167U; // 0.0000
                    cellFormat.FontId =  0U;
                    cellFormat.FillId =  0U;
                    cellFormat.BorderId =  0U;
                    cellFormat.FormatId =  0U;
                    cellFormat.ApplyNumberFormat = true;
                    break;
                case 12: // Normal Light
                    cellFormat.NumberFormatId = 0U;
                    cellFormat.FontId = 2U;
                    cellFormat.FillId = 0U;
                    cellFormat.BorderId = 0U;
                    cellFormat.FormatId = 0U;
                    break;
                case 13: // Normal Bold Centered Light
                    cellFormat.AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Center });
                    cellFormat.NumberFormatId = 0U;
                    cellFormat.FontId = 3U; // bold
                    cellFormat.FillId = 0U;
                    cellFormat.BorderId = 0U;
                    cellFormat.FormatId = 0U;
                    cellFormat.ApplyFont = true;
                    cellFormat.ApplyAlignment = true;
                    break;
                case 14: // Date Time Light
                    cellFormat.NumberFormatId = 22U; // dd/mm/yyyy h:mm
                    cellFormat.FontId = 2U;
                    cellFormat.FillId = 0U;
                    cellFormat.BorderId = 0U;
                    cellFormat.FormatId = 0U;
                    cellFormat.ApplyNumberFormat = true;
                    break;
                case 15: // Short Date Light
                    cellFormat.NumberFormatId = 14U; // dd/mm/yyyy
                    cellFormat.FontId = 2U;
                    cellFormat.FillId = 0U;
                    cellFormat.BorderId = 0U;
                    cellFormat.FormatId = 0U;
                    cellFormat.ApplyNumberFormat = true;
                    break;
            }
            return cellFormat;
        }

        internal void CreateWorksheet(string sheetName, int tableId, bool isView,
            WhereClause whereClause, OrderBy orderBy, Dictionary<string, dynamic> parameters,
            System.Data.DataTable dtTableColumns, bool useColors, bool highlightWarnings, int reportId = 0)
        {
            uint lastCustomStyleIndex = 0U;
            bool appendComments = false;

            WorksheetPart wsp = _spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
            wsp.Worksheet = new Worksheet();
            Ss.Columns columns = new Ss.Columns();

            // === Set up columns ===

            // [1] column name, [2] column object, [3] cell style rules*, [4] cell notes rules**, [5] include in output:
            // *Cell style rules:
            // [1] Controlling Column ID, [2] Operator, [3] Value, [4] Colour, [5] ColumnType
            // **Cell notes rules:
            // [1] W(arning)/E(xceedance), [2] Contolling Column ID, [3] Value, [4] Formula
            Tuple<string, Column, List<Tuple<int, string, string, string, string>>,
                List<Tuple<string, int, string, string>>, bool>[] columnsArray = null;
            Dictionary<int, string> controllingColumnMap = new Dictionary<int, string>();
            XmlDocument headerXml = new XmlDocument();
            string sortDirection = String.Empty;
            string sortOrder = String.Empty;
            string whereClauseString = String.Empty;
            System.Data.DataTable data = null;

            double maxWidth = GetWidth("Calibri", 11, new string('m', 20));

            if (isView)
            {
                DataSet ds = ReportManager.ets_ReportView_GetAll(tableId, 0, 0, reportId, parameters);
                System.Data.DataTable viewColumns = null;
                if (ds != null && ds.Tables.Count > 1)
                {
                    viewColumns = ds.Tables[0];
                    data = ds.Tables[1];
                }
                if (viewColumns == null)
                {
                    columns.AppendChild(new Ss.Column
                    {
                        Min = (UInt32)1,
                        Max = (UInt32)1,
                        Width = GetWidth("Calibri", 11, "1234567"),
                        CustomWidth = true
                    });
                }
                else
                {
                    columnsArray =
                        new Tuple<string, Column, List<Tuple<int, string, string, string, string>>,
                            List<Tuple<string, int, string, string>>, bool>[viewColumns.Rows.Count];

                    int i = 0;
                    foreach (DataRow columnRow in viewColumns.Rows)
                    {
                        string columnTitle = columnRow["DisplayName"].ToString();
                        Column column = new Column
                        {
                            ColumnType = columnRow["ColumnType"].ToString(),
                            IsRound = (bool) columnRow["IsRound"],
                            RoundNumber = (int) columnRow["RoundNumber"]
                        };
                        columnsArray[i] =
                            new
                                Tuple<string, Column, List<Tuple<int, string, string, string, string>>,
                                    List<Tuple<string, int, string, string>>, bool>(columnTitle, column, null, null,
                                    true);

                        double width = GetWidth("Calibri", 11, columnTitle);
                        columns.AppendChild(new Ss.Column
                        {
                            Min = (UInt32) i + 1,
                            Max = (UInt32) i + 1,
                            Width = width > maxWidth ? maxWidth : width,
                            CustomWidth = true
                        });

                        i++;
                    }
                }
            }
            else
            {
                columnsArray =
                    new
                        Tuple<string, Column, List<Tuple<int, string, string, string, string>>,
                            List<Tuple<string, int, string, string>>, bool>[dtTableColumns.Rows.Count];

                XmlNode rootNode = headerXml.CreateNode(XmlNodeType.Element, "ExportXML", null);
                headerXml.AppendChild(rootNode);

                if (dtTableColumns.Rows.Count == 0)
                {
                    columns.AppendChild(new Ss.Column
                    {
                        Min = (UInt32) 1,
                        Max = (UInt32) 1,
                        Width = GetWidth("Calibri", 11, "1234567"),
                        CustomWidth = true
                    });
                }
                else
                {
                    List<int> controllingColumnIdList = new List<int>();

                    for (int i = 0; i < dtTableColumns.Rows.Count; i++)
                    {
                        string columnTitle = String.Format("Column {0}", i + 1);

                        int columnId = (int) dtTableColumns.Rows[i]["ColumnID"];
                        Column column = RecordManager.ets_Column_Details(columnId);

                        if (!dtTableColumns.Rows[i].IsNull("ColumnTitle") &&
                            !String.IsNullOrEmpty(dtTableColumns.Rows[i]["ColumnTitle"].ToString()))
                            columnTitle = dtTableColumns.Rows[i]["ColumnTitle"].ToString();
                        else
                        {
                            if (column != null && !String.IsNullOrEmpty(column.DisplayName))
                                columnTitle = column.DisplayName;
                        }
                        if (column != null && column.ColumnID.HasValue)
                        {
                            List<Tuple<int, string, string, string, string>> listCellColourRules =
                                useColors
                                    ? GetCellColourRules(column.ColumnID.Value, controllingColumnIdList)
                                    : null;
                            List<Tuple<string, int, string, string>> listCellCheckRules =
                                highlightWarnings
                                    ? GetCellCheckRules(column.ColumnID.Value, controllingColumnIdList)
                                    : null;
                            columnsArray[i] =
                                new
                                    Tuple<string, Column, List<Tuple<int, string, string, string, string>>,
                                        List<Tuple<string, int, string, string>>, bool>(columnTitle, column,
                                        listCellColourRules, listCellCheckRules, true);
                        }
                        double width = GetWidth("Calibri", 11, columnTitle);
                        columns.AppendChild(new Ss.Column
                        {
                            Min = (UInt32) i + 1,
                            Max = (UInt32) i + 1,
                            Width = width > maxWidth ? maxWidth : width,
                            CustomWidth = true
                        });
                        InsertColumnXmlNode(column, columnTitle, headerXml);
                    }

                    // Append contolling columns which are not in the output list
                    foreach (int controllingColumnId in controllingColumnIdList)
                    {
                        bool append = true;
                        for (int i = 0; i < columnsArray.Length; i++)
                        {
                            if (columnsArray[i].Item2.ColumnID == controllingColumnId)
                            {
                                if (!controllingColumnMap.ContainsKey(controllingColumnId))
                                    controllingColumnMap.Add(controllingColumnId, columnsArray[i].Item1);
                                append = false;
                                break;
                            }
                        }
                        if (append)
                        {
                            string columnTitle = String.Format("[{0}]", controllingColumnId);
                            Column column = RecordManager.ets_Column_Details(controllingColumnId);
                            if (column != null && !String.IsNullOrEmpty(column.DisplayName))
                                columnTitle = column.DisplayName;
                            int arrayLength = columnsArray.Length;
                            Array.Resize(ref columnsArray, arrayLength + 1);
                            columnsArray[arrayLength] =
                                new
                                    Tuple<string, Column, List<Tuple<int, string, string, string, string>>,
                                        List<Tuple<string, int, string, string>>, bool>(columnTitle, column, null, null,
                                        false);
                            InsertColumnXmlNode(column, columnTitle, headerXml);
                            if (!controllingColumnMap.ContainsKey(controllingColumnId))
                                controllingColumnMap.Add(controllingColumnId,
                                    System.Security.SecurityElement.Escape(columnTitle));
                        }
                    }
                }

                // === Set up sort order ===
                List<int> sortColumnIdList = null;
                sortDirection = "";
                sortOrder = GetSortOrderString(orderBy, columnsArray, out sortDirection, out sortColumnIdList);
                // Append sort columns which are not in the output list
                foreach (int sortColumnId in sortColumnIdList)
                {
                    bool append = true;
                    for (int i = 0; i < columnsArray.Length; i++)
                    {
                        if (columnsArray[i].Item2.ColumnID == sortColumnId)
                        {
                            append = false;
                            break;
                        }
                    }
                    if (append)
                    {
                        string columnTitle = String.Format("[{0}]", sortColumnId);
                        Column column = RecordManager.ets_Column_Details(sortColumnId);
                        if (column != null && !String.IsNullOrEmpty(column.DisplayName))
                            columnTitle = column.DisplayName;
                        int arrayLength = columnsArray.Length;
                        Array.Resize(ref columnsArray, arrayLength + 1);
                        columnsArray[arrayLength] =
                            new
                                Tuple<string, Column, List<Tuple<int, string, string, string, string>>,
                                    List<Tuple<string, int, string, string>>, bool>(columnTitle, column, null, null,
                                    false);
                        InsertColumnXmlNode(column, columnTitle, headerXml);
                    }
                }

                // === Set up filter ===
                whereClauseString = whereClause == null ? string.Empty : whereClause.GetWhereClauseString();
            }

            wsp.Worksheet.AppendChild(columns);

            SheetData sheetData = wsp.Worksheet.AppendChild(new SheetData());

            // === Header row ===
            Row hr = sheetData.AppendChild(new Row());
            if (columnsArray == null || columnsArray.Length == 0)
            {
                Cell cell = new Cell
                {
                    CellValue = new CellValue(columnsArray == null
                        ? "Cannot get list of columns"
                        : "No columns in this table or view"),
                    DataType = new EnumValue<CellValues>(CellValues.String),
                    StyleIndex = StyleTextBold
                };
                hr.AppendChild(cell);
            }
            else
            {
                for (int i = 0; i < columnsArray.Length; i++)
                {
                    if (columnsArray[i].Item5)
                    {
                        Cell cell = new Cell
                        {
                            CellValue = new CellValue(columnsArray[i].Item1),
                            DataType = new EnumValue<CellValues>(CellValues.String),
                            StyleIndex = StyleTextBoldCentered
                        };
                        hr.AppendChild(cell);
                    }
                }
            }

            CommentList commentList = new CommentList();
            string commentVmlXml = String.Empty;

            // === Get data ===
            if (columnsArray != null && columnsArray.Length > 0)
            {
                if (isView)
                {
                    // data is retrieved in above call to ets_ReportView_GetAll;
                    if (data == null)
                    {
                        InsertDiagnosticInfo(sheetData, tableId, reportId);
                    }
                }
                else
                {
                    int iTn = 0;
                    int iTotalDynamicColumns = 0;
                    string strNumericSearch = String.Empty;
                    string strReturnSql = String.Empty;
                    string strReturnHeaderSql = String.Empty;
                    Exception exParam = null;

                    data = RecordManager.ets_Record_List(tableId,
                        null, true, null, null, null, // nEnteredBy, bIsActive, bHasWarningResults, dDateFrom, dDateTo
                        sortOrder, sortDirection, null, null, ref iTn, ref iTotalDynamicColumns,
                        "report", // sOrder, strOrderDirection, nStartRow, nMaxRows, iTotalRowsNum, iTotalDynamicColumns, sType 
                        strNumericSearch, whereClauseString, null, null,
                        "", // strNumericSearch, sTextSearch, dDateAddedFrom, dDateAddedTo, sParentColumnSortSQL
                        headerXml.InnerXml, "", null, // sHeaderSQL, sViewName, nViewID 
                        ref strReturnSql, ref strReturnHeaderSql, ref exParam,false);
                    if (data == null)
                    {
                        InsertDiagnosticInfo(sheetData, tableId, sortOrder, sortDirection, whereClauseString, headerXml, exParam);
                    }
                }
                if (data != null)
                {
                    // === Write data rows ===
                    int rowNumber = 1;
                    foreach (DataRow row in data.Rows)
                    {
                        // create row
                        Row r = sheetData.AppendChild(new Row());
                        rowNumber++;

                        string warningColumn = "";
                        if (row.Table.Columns.Contains("warning") || row.Table.Columns["warning"] != null)
                        {
                            warningColumn = row["warning"].ToString();
                        }

                        Dictionary<int, Tuple<string, bool>> dictWarnings = null;
                        if (!String.IsNullOrEmpty(warningColumn))
                        {
                            Regex rx = new Regex(@"(WARNING:\s|EXCEEDANCE:\s)");
                            string [] warnings = rx.Split(warningColumn);
                            dictWarnings = new Dictionary<int, Tuple<string, bool>>();
                            for (int warningIdx = 1; warningIdx < warnings.Length; warningIdx += 2)
                            {
                                for (int columnIdx = 0; columnIdx < columnsArray.Length; columnIdx++)
                                {
                                    if (warnings[warningIdx + 1]
                                            .IndexOf(columnsArray[columnIdx].Item2.DisplayName,
                                                StringComparison.InvariantCultureIgnoreCase) == 0)
                                    {
                                        dictWarnings.Add(columnIdx,
                                            new Tuple<string, bool>(warnings[warningIdx + 1].Trim().Substring(columnsArray[columnIdx].Item2.DisplayName.Length + 3),
                                                string.Compare(warnings[warningIdx].Trim(), "WARNING:",
                                                    StringComparison.InvariantCultureIgnoreCase) == 0));
                                    }
                                }
                            }
                        }

                        for (int columnIdx = 0; columnIdx < columnsArray.Length; columnIdx++)
                        {
                            if (columnsArray[columnIdx].Item5)
                            {
                                Cell cell = new Cell();
                                string columnName = columnsArray[columnIdx].Item1;
                                string columnType = columnsArray[columnIdx].Item2.ColumnType;
                                string cellValue = isView ? row[columnIdx].ToString() : row[columnName].ToString();
                                string cellColour = useColors
                                    ? GetCellColour(columnName, columnsArray[columnIdx].Item3, row,
                                        controllingColumnMap)
                                    : "";
                                int? roundNumber = columnsArray[columnIdx].Item2.IsRound.HasValue &&
                                                   columnsArray[columnIdx].Item2.IsRound.Value
                                    ? columnsArray[columnIdx].Item2.RoundNumber
                                    : null;
                                bool? hideSeconds = columnsArray[columnIdx].Item2.bHideTimeSecond.HasValue &&
                                                       columnsArray[columnIdx].Item2.bHideTimeSecond.Value
                                    ? columnsArray[columnIdx].Item2.bHideTimeSecond
                                    : null;

                                DateTime dtValue;
                                switch (columnType.ToLower())
                                {
                                    case "text":
                                        cell.CellValue = new CellValue(cellValue);
                                        cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                        break;
                                    case "checkbox":
                                        cell.CellValue = new CellValue(cellValue);
                                        cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                        break;
                                    case "datetime":
                                        if (DateTime.TryParse(cellValue, out dtValue))
                                        {
                                            cell.CellValue = new CellValue((DateTime.Parse(cellValue)).ToOADate()
                                                .ToString(CultureInfo.InvariantCulture));
                                            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                            cell.StyleIndex = StyleDateTimeNormal;
                                        }
                                        else
                                        {
                                            cell.CellValue = new CellValue(cellValue);
                                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                        }
                                        break;
                                    case "date":
                                        if (DateTime.TryParse(cellValue, out dtValue))
                                        {
                                            cell.CellValue = new CellValue((DateTime.Parse(cellValue)).ToOADate()
                                                .ToString(CultureInfo.InvariantCulture));
                                            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                            cell.StyleIndex = StyleShortDateNormal;
                                        }
                                        else
                                        {
                                            cell.CellValue = new CellValue(cellValue);
                                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                        }
                                        break;
                                    case "time":
                                        if (DateTime.TryParse(cellValue, out dtValue))
                                        {
                                            DateTime dt = DateTime.Parse(cellValue);
                                            dt = default(DateTime).Add(dt.TimeOfDay);
                                            cell.CellValue = new CellValue(dt.ToOADate()
                                                .ToString(CultureInfo.InvariantCulture));
                                            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                            cell.StyleIndex = hideSeconds.HasValue && hideSeconds.Value
                                                ? (UInt32Value) StyleShortTimeNormal
                                                : (UInt32Value) StyleTimeNormal;
                                        }
                                        else
                                        {
                                            cell.CellValue = new CellValue(cellValue);
                                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                        }
                                        break;
                                    case "number":
                                        string numberString = cellValue.Trim().Replace(",", "");
                                        cell.CellValue = new CellValue(numberString);
                                        if (ContainsNonNumeric(numberString))
                                            cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                        else
                                        {
                                            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                            if (roundNumber.HasValue)
                                            {
                                                switch (roundNumber.Value)
                                                {
                                                    case 0:
                                                        cell.StyleIndex = StyleNumber0Normal;
                                                        break;
                                                    case 1:
                                                        cell.StyleIndex = StyleNumber1Normal;
                                                        break;
                                                    case 2:
                                                        cell.StyleIndex = StyleNumber2Normal;
                                                        break;
                                                    case 3:
                                                        cell.StyleIndex = StyleNumber3Normal;
                                                        break;
                                                    case 4:
                                                        cell.StyleIndex = StyleNumber4Normal;
                                                        break;
                                                    default:
                                                        cell.StyleIndex = StyleNumber4Normal;
                                                        break;
                                                }
                                            }
                                            else
                                            {
                                                //cell.StyleIndex = StyleTimeNormal; --- "number stored as text" if set
                                            }
                                        }
                                        break;
                                    default:
                                        cell.CellValue = new CellValue(cellValue);
                                        cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                        break;
                                }

                                CellSpecialFill cellSpecialFill = CellSpecialFill.Normal;
                                if (highlightWarnings && dictWarnings != null && dictWarnings.ContainsKey(columnIdx))
                                {
                                    string s = String.Empty;
                                    commentList.AppendChild(CreateComment(GetCellComments(columnName,
                                            dictWarnings[columnIdx].Item1, dictWarnings[columnIdx].Item2,
                                            columnsArray[columnIdx].Item4, row,
                                            controllingColumnMap), dictWarnings[columnIdx].Item2, rowNumber,
                                        columnIdx + 1,
                                        out s));
                                    if (commentVmlXml.Length > 0)
                                        s = "\r\n" + s;
                                    commentVmlXml += s;
                                    appendComments = true;
                                    if (dictWarnings[columnIdx].Item2)
                                        cellSpecialFill = CellSpecialFill.Warning;
                                    else
                                        cellSpecialFill = CellSpecialFill.Exceedance;
                                }

                                if ((cellColour != "") || (cellSpecialFill != CellSpecialFill.Normal))
                                {
                                    Tuple<string, CellSpecialFill, long> customStyle =
                                        new Tuple<string, CellSpecialFill, long>(cellColour, cellSpecialFill, cell.StyleIndex ?? 0u);
                                    int index = Array.IndexOf(_customStyles, customStyle);
                                    if (index == -1)
                                    {
                                        int arrayLength = _customStyles.Length;
                                        Array.Resize(ref _customStyles, arrayLength + 1);
                                        _customStyles[arrayLength] = customStyle;
                                        lastCustomStyleIndex++;
                                        cell.StyleIndex = LastFixedStyleIndex + lastCustomStyleIndex;
                                    }
                                    else
                                    {
                                        cell.StyleIndex = LastFixedStyleIndex + (uint) index + 1u;
                                    }
                                }

                                r.AppendChild(cell);
                            }
                        }
                    }
                }
            }

            if (appendComments)
            {
                WorksheetCommentsPart wspComments = wsp.AddNewPart<WorksheetCommentsPart>();
                Comments comments = new Comments(
                    new Authors(
                        new Author
                        {
                            Text = "System"
                        }),
                    commentList);
                wspComments.Comments = comments;

                VmlDrawingPart vmlDrawingPart = wsp.AddNewPart<VmlDrawingPart>();
                GenerateVmlDrawingPartContent(vmlDrawingPart, commentVmlXml);

                wsp.Worksheet.AppendChild(new LegacyDrawing
                {
                    Id = wsp.GetIdOfPart(vmlDrawingPart)
                });

            }

            // save worksheet
            wsp.Worksheet.Save();
            
            // create the worksheet to workbook relation
            Sheets sheets = _spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();
            string relationshipId = _spreadSheet.WorkbookPart.GetIdOfPart(wsp);
            uint sheetId = 1;
            if (sheets.Elements<Sheet>().Any())
            {
                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }

                /* === Red 23042019: Lets rename the sheetName if exists === */
                bool sheetExists = false;
                foreach (Sheet worksheetpart in _spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>())
                {
                    if (worksheetpart.Name == sheetName)
                    {
                        sheetExists = true;
                        break;
                    }
                }

                if (sheetExists)
                {
                    sheetName = sheetName + "_" + sheetId.ToString();
                }
                /* === end Red ===  */

            Sheet sheet = new Sheet
            {
                Id = relationshipId,
                SheetId = sheetId,
                Name = sheetName.Length > 31 ? sheetName.Substring(0, 31) : sheetName
            };
            sheets.AppendChild(sheet);
}

        // Generates content of part.
        private void GenerateVmlDrawingPartContent(VmlDrawingPart part, string commentVmlXml)
        {
            //writer.WriteRaw("<o:shapelayout v:ext=\"edit\">\r\n  <o:idmap v:ext=\"edit\" data=\"1\"/>\r\n </o:shapelayout><v:shapetype id=\"_x0000_t202\" coordsize=\"21600,21600\" o:spt=\"202\"\r\n  path=\"m,l,21600r21600,l21600,xe\">\r\n  <v:stroke joinstyle=\"miter\"/>\r\n  <v:path gradientshapeok=\"t\" o:connecttype=\"rect\"/>\r\n </v:shapetype>
            //<v:shape margin-left:257.25pt;margin-top:37.5pt;width:108pt;height:59.25pt;
            //<v:textbox style=\'mso-direction-alt:auto\'>\r\n<x:Anchor>\r\n    3, 15, 2, 10, 5, 14, 6, 9</x:Anchor>
            //<v:shape margin-left:257.25pt;margin-top:67.5pt;width:108pt;height:59.25pt;
            //<v:textbox style=\'mso-direction-alt:auto\'>\r\n<x:Anchor>\r\n    3, 15, 4, 10, 5, 14, 8, 9</x:Anchor>");


            commentVmlXml = "<xml xmlns:v=\"urn:schemas-microsoft-com:vml\"\r\n" +
                            " xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\n" +
                            " xmlns:x=\"urn:schemas-microsoft-com:office:excel\">\r\n" +
                            " <o:shapelayout v:ext=\"edit\">\r\n" +
                            "  <o:idmap v:ext=\"edit\" data=\"1\"/>\r\n" +
                            " </o:shapelayout><v:shapetype id=\"_x0000_t202\" coordsize=\"21600,21600\" o:spt=\"202\"\r\n" +
                            "  path=\"m,l,21600r21600,l21600,xe\">\r\n" +
                            "  <v:stroke joinstyle=\"miter\"/>\r\n" +
                            "  <v:path gradientshapeok=\"t\" o:connecttype=\"rect\"/>\r\n" +
                            " </v:shapetype>" +
                            commentVmlXml + "\r\n" +
                            "</xml>";

            //commentVmlXml =
            //    "<xml xmlns:v=\"urn:schemas-microsoft-com:vml\"\r\n xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\n xmlns:x=\"urn:schemas-microsoft-com:office:excel\">\r\n <o:shapelayout v:ext=\"edit\">\r\n  <o:idmap v:ext=\"edit\" data=\"1\"/>\r\n </o:shapelayout><v:shapetype id=\"08d43a5a31784e45a14cc28b79e1583f\" coordsize=\"21600,21600\" o:spt=\"202\"\r\n  path=\"m,l,21600r21600,l21600,xe\">\r\n  <v:stroke joinstyle=\"miter\"/>\r\n  <v:path gradientshapeok=\"t\" o:connecttype=\"rect\"/>\r\n </v:shapetype><v:shape id=\"92586115cc1d40bdb0f089c335a444e6\" type=\"#_x0000_t202\" style=\'position:absolute;\r\n  margin-left:257.25pt;margin-top:37.5pt;width:146.25pt;height:45pt;z-index:1;\r\n  visibility:hidden;mso-wrap-style:tight\' fillcolor=\"#ffffe1\" o:insetmode=\"auto\">\r\n  <v:fill color2=\"#ffffe1\"/>\r\n  <v:shadow on=\"t\" color=\"black\" obscured=\"t\"/>\r\n  <v:path o:connecttype=\"none\"/>\r\n  <v:textbox style=\'mso-direction-alt:auto\'>\r\n   <div style=\'text-align:left\'></div>\r\n  </v:textbox>\r\n  <x:ClientData ObjectType=\"Note\">\r\n   <x:MoveWithCells/>\r\n   <x:SizeWithCells/>\r\n   <x:Anchor>4,15,-1,10,6,15,2,4</x:Anchor>\r\n   <x:AutoFill>False</x:AutoFill>\r\n   <x:Row>3</x:Row>\r\n   <x:Column>2</x:Column>\r\n  </x:ClientData>\r\n </v:shape><v:shape id=\"_x0000_s1026\" type=\"#_x0000_t202\" style=\'position:absolute;\r\n  margin-left:257.25pt;margin-top:67.5pt;width:108pt;height:59.25pt;z-index:2;\r\n  visibility:hidden\' fillcolor=\"#ffffe1\" o:insetmode=\"auto\">\r\n  <v:fill color2=\"#ffffe1\"/>\r\n  <v:shadow on=\"t\" color=\"black\" obscured=\"t\"/>\r\n  <v:path o:connecttype=\"none\"/>\r\n  <v:textbox style=\'mso-direction-alt:auto\'>\r\n   <div style=\'text-align:left\'></div>\r\n  </v:textbox>\r\n  <x:ClientData ObjectType=\"Note\">\r\n   <x:MoveWithCells/>\r\n   <x:SizeWithCells/>\r\n   <x:Anchor>4,15,-1,10,6,15,2,4</x:Anchor>\r\n   <x:AutoFill>False</x:AutoFill>\r\n   <x:Row>5</x:Row>\r\n   <x:Column>2</x:Column>\r\n  </x:ClientData>\r\n </v:shape></xml>";
            //commentVmlXml =
            //    "<xml xmlns:v=\"urn:schemas-microsoft-com:vml\"\r\n xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\n xmlns:x=\"urn:schemas-microsoft-com:office:excel\">\r\n <o:shapelayout v:ext=\"edit\">\r\n  <o:idmap v:ext=\"edit\" data=\"1\"/>\r\n </o:shapelayout><v:shapetype id=\"_x0000_t202\" coordsize=\"21600,21600\" o:spt=\"202\"\r\n  path=\"m,l,21600r21600,l21600,xe\">\r\n  <v:stroke joinstyle=\"miter\"/>\r\n  <v:path gradientshapeok=\"t\" o:connecttype=\"rect\"/>\r\n </v:shapetype><v:shape id=\"_x0000_s1025\" type=\"#_x0000_t202\" style=\'position:absolute;\r\n  margin-left:257.25pt;margin-top:37.5pt;width:146.25pt;height:45pt;z-index:1;\r\n  visibility:hidden;mso-wrap-style:tight\' fillcolor=\"#ffffe1\" o:insetmode=\"auto\">\r\n  <v:fill color2=\"#ffffe1\"/>\r\n  <v:shadow color=\"black\" obscured=\"t\"/>\r\n  <v:path o:connecttype=\"none\"/>\r\n  <v:textbox style=\'mso-direction-alt:auto\'>\r\n   <div style=\'text-align:left\'></div>\r\n  </v:textbox>\r\n  <x:ClientData ObjectType=\"Note\">\r\n   <x:MoveWithCells/>\r\n   <x:SizeWithCells/>\r\n   <x:Anchor>\r\n    3, 15, 2, 10, 6, 1, 5, 10</x:Anchor>\r\n   <x:AutoFill>False</x:AutoFill>\r\n   <x:Row>3</x:Row>\r\n   <x:Column>2</x:Column>\r\n  </x:ClientData>\r\n </v:shape><v:shape id=\"_x0000_s1026\" type=\"#_x0000_t202\" style=\'position:absolute;\r\n  margin-left:257.25pt;margin-top:67.5pt;width:108pt;height:59.25pt;z-index:2;\r\n  visibility:hidden\' fillcolor=\"#ffffe1\" o:insetmode=\"auto\">\r\n  <v:fill color2=\"#ffffe1\"/>\r\n  <v:shadow color=\"black\" obscured=\"t\"/>\r\n  <v:path o:connecttype=\"none\"/>\r\n  <v:textbox style=\'mso-direction-alt:auto\'>\r\n   <div style=\'text-align:left\'></div>\r\n  </v:textbox>\r\n  <x:ClientData ObjectType=\"Note\">\r\n   <x:MoveWithCells/>\r\n   <x:SizeWithCells/>\r\n   <x:Anchor>\r\n    3, 15, 4, 10, 5, 14, 8, 9</x:Anchor>\r\n   <x:AutoFill>False</x:AutoFill>\r\n   <x:Row>5</x:Row>\r\n   <x:Column>2</x:Column>\r\n  </x:ClientData>\r\n </v:shape></xml>";
            System.Xml.XmlTextWriter writer =
                new System.Xml.XmlTextWriter(part.GetStream(System.IO.FileMode.Create), System.Text.Encoding.UTF8);
            writer.WriteRaw(commentVmlXml);
            writer.Flush();
            writer.Close();
        }

        private Comment CreateComment(string text, bool isWarning, int rowIndex, int columnIndex, out string commentVmlXml)
        {
            commentVmlXml = " <v:shape id=\"" + Guid.NewGuid().ToString().Replace("-", "") + "\" type=\"#_x0000_t202\" style=\"position:absolute;\r\n" +
                            // "  margin-left:257.25pt;margin-top:37.5pt;width:146.25pt;height:45pt;z-index:1;\r\n" + // ??? the exact meaning is not clear ???
                            "  width:144pt;height:59.25pt;z-index:1;\r\n" + // width = 19.2 * 7.5; height = 59.2 - taken from ClosedXML source
                            "  visibility:hidden;mso-wrap-style:tight\" fillcolor=\"#ffffe1\" o:insetmode=\"auto\">\r\n" +
                            "  <v:fill color2=\"#ffffe1\"/>\r\n" +
                            "  <v:shadow on=\"f\" color=\"black\" obscured=\"t\"/>\r\n" +
                            "  <v:path o:connecttype=\"none\"/>\r\n" +
                            "  <v:textbox style=\"mso-fit-shape-to-text:true;mso-direction-alt:auto\">\r\n" +
                            "   <div style=\"text-align:left\"></div>\r\n" +
                            "  </v:textbox>\r\n" +
                            "  <x:ClientData ObjectType=\"Note\">\r\n" +
                            "   <x:MoveWithCells/>\r\n" +
                            "   <x:SizeWithCells/>\r\n" +
                            "   <x:Anchor>" + GetAnchorCoordinatesForVMLCommentShape(rowIndex - 1, columnIndex - 1) + "</x:Anchor>\r\n" +
                            "   <x:AutoFill>False</x:AutoFill>\r\n" +
                            "   <x:Row>" + (rowIndex - 1) + "</x:Row>\r\n" +
                            "   <x:Column>" + (columnIndex - 1) + "</x:Column>\r\n" +
                            "  </x:ClientData>\r\n" +
                            " </v:shape>";

                return new Comment(
                new CommentText(
                    new Ss.Run(
                        new Ss.RunProperties(
                            new Bold(),
                            new Ss.FontSize
                            {
                                Val= 9D
                            },
                            new Color
                            {
                                Indexed = (UInt32Value)81U
                            },
                            new RunFont
                            {
                                Val = "Tahoma"
                            },
                            new RunPropertyCharSet
                            {
                                Val = 1
                            }),
                        new Ss.Text()
                        {
                            Text = isWarning ? "Warning" : "Exceedance"
                        }),
                    new Ss.Run(
                        new Ss.RunProperties(
                            new Ss.FontSize
                            {
                                Val = 9D
                            },
                            new Color
                            {
                                Indexed = (UInt32Value)81U
                            },
                            new RunFont
                            {
                                Val = "Tahoma"
                            },
                            new RunPropertyCharSet
                            {
                                Val = 1
                            }),
                        new Ss.Text
                        {
                            Space = SpaceProcessingModeValues.Preserve,
                            Text = "\n" + text
                        })))
            {
                Reference = String.Format("{0}{1}", GetExcelColumnName(columnIndex), rowIndex),
                AuthorId = (UInt32Value) 0U,
                ShapeId = (UInt32Value) 0U
            };
        }

        private string GetAnchorCoordinatesForVMLCommentShape(int rowIndex, int columnIndex)
        {
            // anchor is for Edit Comment box, not for comment itself!
            string coordinates = String.Empty;
            int startingRow = rowIndex;
            int startingColumn = columnIndex;

            // From (upper right coordinate of a rectangle)
            // [0] Left column
            // [1] Left column offset
            // [2] Left row
            // [3] Left row offset
            // To (bottom right coordinate of a rectangle)
            // [4] Right column
            // [5] Right column offset
            // [6] Right row
            // [7] Right row offset
            List<int> coordList = new List<int>(8) { 0, 0, 0, 0, 0, 0, 0, 0 };

            coordList[0] = startingColumn + 1; // If starting column is A, display shape in column B
            coordList[1] = 15;
            coordList[4] = startingColumn + 3; // If starting column is A, display shape till column D
            coordList[5] = 15;
            coordList[6] = startingRow + 3; // If starting row is 0, display 3 rows down to row 3

            // The row offsets change if the shape is defined in the first row
            if (startingRow == 0)
            {
                coordList[2] = startingRow;
                coordList[3] = 2;
                coordList[7] = 16;
            }
            else
            {
                coordList[2] = startingRow - 1;
                coordList[3] = 10;
                coordList[7] = 4;
            }

            coordinates = string.Join(", ", coordList.ConvertAll<string>(x => x.ToString()).ToArray());

            return coordinates;
        }

        internal void CreateChartWorksheet(string chartName, int tableId, int dataColumnId, int xAxisColumnId, int seriesColumnId, WhereClause whereClause,
            string chartHeading, string chartSubHeading,
            string warningCaption, double? warningValue, string warningColor,
            string exceedanceCaption, double? exceedanceValue, string exceedanceColor,

             string warningMinCaption, string warningMinColor,
            double? warningMinValue,
             string exceedanceMinCaption, string exceedanceMinColor,
            double? exceedanceMinValue, List<Tuple<int, string>> listSeriesColors,

            int chartType, bool useLightText, bool IsOneDate, 
            //RP Added Ticket 5119
            string highestvalue = "", string lowestvalue = "", string interval = "" 
            //End Modification
            )
        {
            WorksheetPart wsp = _spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
            wsp.Worksheet = new Worksheet();
            SheetData sheetData = wsp.Worksheet.AppendChild(new SheetData());


            /* === Red 23042019: Transferred here  === */
            // create the worksheet to workbook relation
            Sheets sheets = _spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();
            string relationshipId = _spreadSheet.WorkbookPart.GetIdOfPart(wsp);
            uint sheetId = 1;
            if (sheets.Elements<Sheet>().Any())
            {
                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }

            bool sheetExists = false;
            foreach (Sheet worksheetpart in _spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>())
            {

                if (worksheetpart.Name == chartName)
                    sheetExists = true;
            }

            if (sheetExists)
            {
                chartName = chartName + "_" + sheetId.ToString();
            }


            Sheet sheet = new Sheet
            {
                Id = relationshipId,
                SheetId = sheetId,
                Name = chartName
            };
            sheets.AppendChild(sheet);

            /* === end Red === */


            chartName = chartName.Length > 31 ? chartName.Substring(0, 31) : chartName;

            Column xAxisColumn = RecordManager.ets_Column_Details(xAxisColumnId);
            Column seriesColumn = RecordManager.ets_Column_Details(seriesColumnId);
            Column dataColumn = RecordManager.ets_Column_Details(dataColumnId);

            if (xAxisColumn != null && seriesColumn != null && dataColumn != null)
            {
                XmlDocument headerXml = new XmlDocument();
                XmlNode rootNode = headerXml.CreateNode(XmlNodeType.Element, "ExportXML", null);
                headerXml.AppendChild(rootNode);

                InsertColumnXmlNode(xAxisColumn, "X", headerXml);
                InsertColumnXmlNode(seriesColumn, "Series", headerXml);
                InsertColumnXmlNode(dataColumn, "Value", headerXml);

                string whereClauseString = whereClause == null ? "" : whereClause.GetWhereClauseString();

                int iTn = 0;
                int iTotalDynamicColumns = 0;
                string sortOrder =
                    "CONVERT(datetime, [dbo].[fnRemoveNonDate]([X]), 103) ASC, DBGSystemRecordID ASC";
                string sortDirection = String.Empty;
                string strNumericSearch = String.Empty;
                string strReturnSql = String.Empty;
                string strReturnHeaderSql = String.Empty;
                Exception exParam = null;

                System.Data.DataTable data = RecordManager.ets_Record_List(tableId,
                    null, true, null, null, null, // nEnteredBy, bIsActive, bHasWarningResults, dDateFrom, dDateTo
                    sortOrder, sortDirection, null, null, ref iTn, ref iTotalDynamicColumns, // sOrder, strOrderDirection, nStartRow, nMaxRows, iTotalRowsNum, iTotalDynamicColumns
                    "report", // sType 
                    strNumericSearch, whereClauseString, null, null,
                    "", // strNumericSearch, sTextSearch, dDateAddedFrom, dDateAddedTo, sParentColumnSortSQL
                    headerXml.InnerXml, "", null, // sHeaderSQL, sViewName, nViewID 
                    ref strReturnSql, ref strReturnHeaderSql, ref exParam,false);

                bool IsOnlyOneData = false; //== Red, Ticket 5042


                if (data == null)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "CreateChartWorksheet", "Data is null",
                        tableId.ToString() + Environment.NewLine +
                        sortOrder + Environment.NewLine +
                        sortDirection + Environment.NewLine +
                        whereClauseString + Environment.NewLine +
                        headerXml.InnerXml + Environment.NewLine +
                        exParam.Message,
                        DateTime.Now, "");
                    SystemData.ErrorLog_Insert(theErrorLog);
                    InsertDiagnosticInfo(sheetData, tableId, sortOrder, sortDirection, whereClauseString, headerXml,
                        exParam);
                }
                else
                {

                    DateTime recordDateColumnX = new DateTime(1, 1, 1);
                    int k = 0;
                    foreach (DataRow row in data.Rows)
                    {
                        DateTime recordDateColumn;
                        if (row["X"] != DBNull.Value && DateTime.TryParse(row["X"].ToString(), out recordDateColumn))
                        {
                            if (k == 0)
                            {
                                recordDateColumnX = recordDateColumn;
                                k = 1;
                            }
                            if (recordDateColumn == recordDateColumnX)
                            {
                                IsOnlyOneData = true;
                            }
                            else //(recordDateColumn != recordDateColumnX)
                            {
                                IsOnlyOneData = false;
                                break;
                            }

                        }
                        k++;
                    }
                    if (IsOneDate)
                    {
                        IsOnlyOneData = true;
                    }

                    List<string> series = (from row in data.AsEnumerable()
                                           select row.Field<string>("Series")).Distinct()
                        .OrderBy(x => x)
                        .ToList();

                    System.Data.DataTable crossDataTable = new System.Data.DataTable();
                    crossDataTable.Columns.Add(xAxisColumn.DisplayName, typeof(DateTime));
                    foreach (string s in series)
                    {
                        crossDataTable.Columns.Add(s, typeof(string));
                    }

                    DataRow crossDataTableDataRow = null;
                    DataRow crossDataTableDataRowBefore = null;
                    DataRow crossDataTableDataRowAfter = null;
                    DateTime lastDateTimeX = new DateTime(1, 1, 1);
                    DateTime lastDateTimeBeforeX = new DateTime(1, 1, 1);
                    DateTime lastDateTimeAfterX = new DateTime(1, 1, 1);

                    if (IsOnlyOneData)
                    {
                        foreach (DataRow row in data.Rows)
                        {
                            /* == Red: Ticket 5042 == */

                            DateTime dtBeforeX;
                            if (IsOnlyOneData)
                            {
                                if (row["X"] != DBNull.Value && DateTime.TryParse(row["X"].ToString(), out dtBeforeX))
                                {

                                    if (xAxisColumn.ColumnType == "date")
                                    {
                                        dtBeforeX = dtBeforeX.AddDays(-1);
                                    }
                                    else
                                    {
                                        dtBeforeX = dtBeforeX.AddMinutes(-1);
                                    }
                                    if (dtBeforeX != lastDateTimeBeforeX)
                                    {
                                        if (crossDataTableDataRowBefore != null)
                                        {
                                            crossDataTable.Rows.Add(crossDataTableDataRowBefore);
                                        }
                                        crossDataTableDataRowBefore = crossDataTable.NewRow();
                                        crossDataTableDataRowBefore[0] = dtBeforeX;
                                        lastDateTimeBeforeX = dtBeforeX;
                                    }
                                    if (row["Series"] != DBNull.Value && row["Value"] != DBNull.Value)
                                        crossDataTableDataRowBefore[row["Series"].ToString()] = "#N/A";
                                }
                            }
                            /* == End Red == */
                        }
                    }


                    foreach (DataRow row in data.Rows)
                    {

                        DateTime dtX;
                        if (row["X"] != DBNull.Value && DateTime.TryParse(row["X"].ToString(), out dtX))
                        {
                            if (dtX != lastDateTimeX)
                            {
                                if (crossDataTableDataRow != null)
                                {
                                    crossDataTable.Rows.Add(crossDataTableDataRow);
                                }
                                crossDataTableDataRow = crossDataTable.NewRow();
                                crossDataTableDataRow[0] = dtX;
                                lastDateTimeX = dtX;
                            }
                            if (row["Series"] != DBNull.Value && row["Value"] != DBNull.Value)
                                crossDataTableDataRow[row["Series"].ToString()] = row["Value"];
                        }
                    }

                    if (IsOnlyOneData)
                    {
                        foreach (DataRow row in data.Rows)
                        {
                            /* == Red: Ticket 5042 == */
                            DateTime dtAfterX;

                            /* == Red: Ticket 5042 == */
                            if (IsOnlyOneData)
                            {
                                if (row["X"] != DBNull.Value && DateTime.TryParse(row["X"].ToString(), out dtAfterX))
                                {

                                    if (xAxisColumn.ColumnType == "date")
                                    {
                                        dtAfterX = dtAfterX.AddDays(1);
                                    }
                                    else
                                    {
                                        dtAfterX = dtAfterX.AddMinutes(1);
                                    }
                                    if (dtAfterX != lastDateTimeAfterX)
                                    {
                                        if (crossDataTableDataRowAfter != null)
                                        {
                                            crossDataTable.Rows.Add(crossDataTableDataRow);
                                        }
                                        crossDataTableDataRowAfter = crossDataTable.NewRow();
                                        crossDataTableDataRowAfter[0] = dtAfterX;
                                        lastDateTimeAfterX = dtAfterX;
                                    }
                                    if (row["Series"] != DBNull.Value && row["Value"] != DBNull.Value)
                                        crossDataTableDataRowAfter[row["Series"].ToString()] = "#N/A";
                                }
                            }
                            /* == End Red == */
                        }
                    }
                    /* == Red 04012020: Before if one day only range == */
                    if (crossDataTableDataRowBefore != null)
                    {
                        crossDataTable.Rows.Add(crossDataTableDataRowBefore);
                    }
                    crossDataTable.AcceptChanges();

                    if (crossDataTableDataRow != null)
                    {
                        crossDataTable.Rows.Add(crossDataTableDataRow);
                    }
                    crossDataTable.AcceptChanges();

                    /* == Red 04012020: After if one day only range == */
                    if (crossDataTableDataRowAfter != null)
                    {
                        crossDataTable.Rows.Add(crossDataTableDataRowAfter);
                    }
                    crossDataTable.AcceptChanges();

                    Row hr = sheetData.AppendChild(new Row());
                    int headerRowStyleIndex = useLightText ? StyleTextBoldCenteredLight : StyleTextBoldCentered;

                    Cell cellXHeader = new Cell
                    {
                        CellValue = new CellValue(xAxisColumn.DisplayName),
                        DataType = new EnumValue<CellValues>(CellValues.String),
                        StyleIndex = (uint) headerRowStyleIndex
                    };
                    hr.AppendChild(cellXHeader);

                    bool displayWarning = warningValue.HasValue;
                    bool displayExceedance = exceedanceValue.HasValue;

                    foreach (string s in series)
                    {
                        Cell cell = new Cell
                        {
                            CellValue = new CellValue(s),
                            DataType = new EnumValue<CellValues>(CellValues.String),
                            StyleIndex = (uint) headerRowStyleIndex
                        };
                        hr.AppendChild(cell);
                    }
                    if (displayWarning || displayExceedance)
                    {
                        Cell cell = new Cell
                        {
                            CellValue = new CellValue(""),
                            DataType = new EnumValue<CellValues>(CellValues.String),
                            StyleIndex = (uint) headerRowStyleIndex
                        };
                        hr.AppendChild(cell);
                    }
                    if (displayWarning)
                    {
                        Cell cell = new Cell
                        {
                            CellValue =
                                new CellValue(String.IsNullOrEmpty(warningCaption) ? "Warning" : warningCaption),
                            DataType = new EnumValue<CellValues>(CellValues.String),
                            StyleIndex = (uint) headerRowStyleIndex
                        };
                        hr.AppendChild(cell);
                    }
                    if (displayExceedance)
                    {
                        Cell cell = new Cell
                        {
                            CellValue = new CellValue(String.IsNullOrEmpty(exceedanceCaption)
                                ? "Exceedance"
                                : exceedanceCaption),
                            DataType = new EnumValue<CellValues>(CellValues.String),
                            StyleIndex = (uint) headerRowStyleIndex
                        };
                        hr.AppendChild(cell);
                    }

                    /* ===  Red: Charting === */
                    bool displayMinWarning = warningMinValue.HasValue;
                    bool displayMinExceedance = exceedanceMinValue.HasValue;

                    if (displayMinWarning)
                    {
                        Cell cell = new Cell
                        {
                            CellValue =
                                new CellValue(String.IsNullOrEmpty(warningMinCaption) ? "Warning" : warningMinCaption),
                            DataType = new EnumValue<CellValues>(CellValues.String),
                            StyleIndex = (uint)headerRowStyleIndex
                        };
                        hr.AppendChild(cell);
                    }
                    if (displayMinExceedance)
                    {
                        Cell cell = new Cell
                        {
                            CellValue = new CellValue(String.IsNullOrEmpty(exceedanceMinCaption)
                                ? "Exceedance"
                                : exceedanceMinCaption),
                            DataType = new EnumValue<CellValues>(CellValues.String),
                            StyleIndex = (uint)headerRowStyleIndex
                        };
                        hr.AppendChild(cell);
                    }
                    /* == end Red === */

                    int dateColumnStyleIndex = xAxisColumn.ColumnType == "datetime"
                        ? (useLightText ? StyleDateTimeNormalLight : StyleDateTimeNormal)
                        : (useLightText ? StyleShortDateNormalLight : StyleShortDateNormal);

                    /* == Red: Ticket 5042 == */
                    if (IsOnlyOneData)
                    {
                        if (xAxisColumn.ColumnType == "datetime")
                        {
                            dateColumnStyleIndex = StyleTimeNormal;
                        }
                        else
                        {
                            dateColumnStyleIndex = StyleShortDateNormalLight;
                        }
                    }
                    /* == End Red, Ticket 5042 == */

                    int dataStyleIndex = useLightText ? StyleTextNormalLight : StyleTextNormal;
                    int i = 0;

                    foreach (DataRow row in crossDataTable.Rows)
                    {
                        // create row
                        Row r = sheetData.AppendChild(new Row());

                        Cell cellX = new Cell
                        {
                            CellValue = new CellValue(((DateTime) row[xAxisColumn.DisplayName]).ToOADate()
                                .ToString(CultureInfo.InvariantCulture)),
                            DataType = new EnumValue<CellValues>(CellValues.Number),
                            StyleIndex = (uint) dateColumnStyleIndex
                        };
                        r.AppendChild(cellX);

                        foreach (string s in series)
                        {
                            if (row[s] != DBNull.Value)
                            {
                                string valueString = RemoveNonNumeric(row[s].ToString());
                                if (!String.IsNullOrEmpty(valueString))
                                {
                                    Cell cell = new Cell
                                    {
                                        CellValue = new CellValue(RemoveNonNumeric(row[s].ToString())),
                                        DataType = new EnumValue<CellValues>(CellValues.Number),
                                        StyleIndex = (uint) dataStyleIndex
                                    };
                                    r.AppendChild(cell);
                                }
                                else
                                {
                                    Cell cell = new Cell
                                    {
                                        CellValue = new CellValue("#N/A"),
                                        DataType = new EnumValue<CellValues>(CellValues.Error),
                                        StyleIndex = (uint) dataStyleIndex
                                    };
                                    r.AppendChild(cell);
                                }
                            }
                            else
                            {
                                Cell cell = new Cell
                                {
                                    CellValue = new CellValue("#N/A"),
                                    DataType = new EnumValue<CellValues>(CellValues.Error),
                                    StyleIndex = (uint) dataStyleIndex
                                };
                                r.AppendChild(cell);
                            }
                        }

                        if (displayWarning || displayExceedance)
                        {
                            Cell cell = new Cell
                            {
                                CellValue = new CellValue(""),
                                DataType = new EnumValue<CellValues>(CellValues.String),
                                StyleIndex = StyleTextBoldCentered
                            };
                            r.AppendChild(cell);
                        }
                        if (displayWarning)
                        {
                            Cell cell = new Cell
                            {
                                CellValue = new CellValue(i == 0 || i == crossDataTable.Rows.Count - 1
                                    ? warningValue.Value.ToString(CultureInfo.InvariantCulture)
                                    : "#N/A"),
                                DataType = new EnumValue<CellValues>(i == 0 || i == crossDataTable.Rows.Count - 1
                                    ? CellValues.Number
                                    : CellValues.Error),
                                StyleIndex = (uint) dataStyleIndex
                            };
                            r.AppendChild(cell);
                        }
                        if (displayExceedance)
                        {
                            Cell cell = new Cell
                            {
                                CellValue = new CellValue(i == 0 || i == crossDataTable.Rows.Count - 1
                                    ? exceedanceValue.Value.ToString(CultureInfo.InvariantCulture)
                                    : "#N/A"),
                                DataType = new EnumValue<CellValues>(i == 0 || i == crossDataTable.Rows.Count - 1
                                    ? CellValues.Number
                                    : CellValues.Error),
                                StyleIndex = (uint) dataStyleIndex
                            };
                            r.AppendChild(cell);
                        }

                        /* === Red: Charting Min Values === */
                   
                        if (displayMinWarning)
                        {
                            Cell cell = new Cell
                            {
                                CellValue = new CellValue(i == 0 || i == crossDataTable.Rows.Count - 1
                                    ? warningMinValue.Value.ToString(CultureInfo.InvariantCulture)
                                    : "#N/A"),
                                DataType = new EnumValue<CellValues>(i == 0 || i == crossDataTable.Rows.Count - 1
                                    ? CellValues.Number
                                    : CellValues.Error),
                                StyleIndex = (uint)dataStyleIndex
                            };
                            r.AppendChild(cell);
                        }
                        if (displayMinExceedance)
                        {
                            Cell cell = new Cell
                            {
                                CellValue = new CellValue(i == 0 || i == crossDataTable.Rows.Count - 1
                                    ? exceedanceMinValue.Value.ToString(CultureInfo.InvariantCulture)
                                    : "#N/A"),
                                DataType = new EnumValue<CellValues>(i == 0 || i == crossDataTable.Rows.Count - 1
                                    ? CellValues.Number
                                    : CellValues.Error),
                                StyleIndex = (uint)dataStyleIndex
                            };
                            r.AppendChild(cell);
                        }
                        /* === end Red === */

                        i++;
                    }

                    // 1: Caption, 2: Value, 3: Colour
                    Tuple<string, double, string> warningTuple = displayWarning
                        ? new Tuple<string, double, string>(
                            warningCaption, warningValue.Value, warningColor)
                        : null;
                    Tuple<string, double, string> exceedanceTuple = displayExceedance
                        ? new Tuple<string, double, string>(
                            exceedanceCaption, exceedanceValue.Value, exceedanceColor)
                        : null;

                    /* === Red: Charting Min Color === */
                    Tuple<string, double, string> warningMinTuple = displayMinWarning
                      ? new Tuple<string, double, string>(
                          warningMinCaption, warningMinValue.Value, warningMinColor)
                      : null;
                    Tuple<string, double, string> exceedanceMinTuple = displayMinExceedance
                        ? new Tuple<string, double, string>(
                            exceedanceMinCaption, exceedanceMinValue.Value, exceedanceMinColor)
                        : null;
                    /* === end Red === */

                    InsertChart(wsp, crossDataTable, xAxisColumn.ColumnType, chartType, chartName,
                        chartHeading, chartSubHeading, dataColumn.DisplayName,
                        warningTuple, exceedanceTuple,warningMinTuple,exceedanceMinTuple,listSeriesColors, IsOnlyOneData, highestvalue, lowestvalue, interval);
                }

                // save worksheet
                wsp.Worksheet.Save();
            }

            
        }

        private void InsertChart(WorksheetPart worksheetPart, System.Data.DataTable data, string xAxisDataType,
            int chartType, string sheetTitle, string chartTitle, string chartSubTitle, string valuesAxisTitle,
            Tuple<string, double, string> warningLine, Tuple<string, double, string> exceedanceLine,
            Tuple<string, double, string> warningMinLine, Tuple<string, double, string> exceedanceMinLine, 
            List<Tuple<int, string>> listSeriesColors, bool IsOneDate, string highestvalue = "", string lowestvalue = "", string interval = "")
        {
                bool drawLineChart = false;
                bool drawScatterChart = true;
                bool drawBarChart = false;
                if (chartType == 1)
                {
                    drawLineChart = false;
                    drawScatterChart = false;
                    drawBarChart = true;
                }
                bool drawLineChartLimits = false;

                // Add a new drawing to the worksheet.
                DrawingsPart drawingsPart = worksheetPart.AddNewPart<DrawingsPart>();
                worksheetPart.Worksheet.AppendChild(
                    new Drawing
                    {
                        Id = worksheetPart.GetIdOfPart(drawingsPart)

                    });
                worksheetPart.Worksheet.Save();

                // Add a new chart and set the chart language to English-AU.
                ChartPart chartPart = drawingsPart.AddNewPart<ChartPart>();
                chartPart.ChartSpace = new ChartSpace();
                chartPart.ChartSpace.AppendChild(
                    new EditingLanguage { Val = new StringValue("en-AU") });
                chartPart.ChartSpace.AppendChild(
                    new RoundedCorners { Val = false });

                Charts.Chart chart =
                    chartPart.ChartSpace.AppendChild(
                        new Charts.Chart());

                

                AppendChartTitle(chart, chartTitle, chartSubTitle);

                // Create a new Line chart.
                PlotArea plotArea = chart.AppendChild(new PlotArea());
                Charts.Layout layout = plotArea.AppendChild(new Charts.Layout());

                string xFormatCodeText = "d/m/yyyy";
                switch (xAxisDataType)
                {
                    case "date":
                        xFormatCodeText = "d/m/yyyy";
                        break;
                    case "datetime":
                        xFormatCodeText = "d/m/yyyy\\ h:mm";
                        break;
                }

                int seriesIdx = 0;

                if (drawBarChart)
                {
                    BarChart barChart = plotArea.AppendChild(new BarChart(
                        new BarDirection { Val = BarDirectionValues.Column },
                        new BarGrouping { Val = BarGroupingValues.Clustered },
                        new VaryColors { Val = false }));
                    AppendBarChartDataSeries(barChart, data, xFormatCodeText, sheetTitle, listSeriesColors);
                    seriesIdx += data.Columns.Count;

                    barChart.AppendChild(
                        new DataLabels(
                            new ShowLegendKey { Val = new BooleanValue(false) },
                            new ShowValue { Val = new BooleanValue(false) },
                            new ShowCategoryName { Val = new BooleanValue(false) },
                            new ShowSeriesName { Val = new BooleanValue(false) },
                            new ShowPercent { Val = new BooleanValue(false) },
                            new ShowBubbleSize { Val = new BooleanValue(false) }));
                    barChart.AppendChild(new GapWidth { Val = (UInt16Value)150u });

                    barChart.AppendChild(new AxisId { Val = new UInt32Value(48650112u) });
                    barChart.AppendChild(new AxisId { Val = new UInt32Value(48672768u) });

                    drawLineChartLimits = true;
                }

                if (drawScatterChart)
                {
                    ScatterChart scatterChart = plotArea.AppendChild(new ScatterChart(
                        new ScatterStyle { Val = ScatterStyleValues.LineMarker },
                        new VaryColors { Val = false }));

                    /* == Red 07012020: Ticket 5042 == */
                    if (IsOneDate)
                    {
                        AppendScatterChartDataSeriesOneDate(scatterChart, data, xFormatCodeText, sheetTitle, listSeriesColors);
                    }
                    else
                    {
                        AppendScatterChartDataSeries(scatterChart, data, xFormatCodeText, sheetTitle, listSeriesColors);
                    }
                    /* == End Red, Ticket 5042 == */
                    seriesIdx += data.Columns.Count;

                    scatterChart.AppendChild(
                        new DataLabels(
                            new ShowLegendKey { Val = new BooleanValue(true) },
                            new ShowValue { Val = new BooleanValue(false) },
                            new ShowCategoryName { Val = new BooleanValue(false) },
                            new ShowSeriesName { Val = new BooleanValue(false) },
                            new ShowPercent { Val = new BooleanValue(false) },
                            new ShowBubbleSize { Val = new BooleanValue(false) }));

                    scatterChart.AppendChild(new AxisId { Val = new UInt32Value(48650112u) });
                    scatterChart.AppendChild(new AxisId { Val = new UInt32Value(48672768u) });

                    if (warningLine != null)
                    {
                        AppendScatterChartSingleSeries(scatterChart, data, xFormatCodeText, sheetTitle,
                            seriesIdx, warningLine.Item1, warningLine.Item2, warningLine.Item3);
                        seriesIdx++;
                    }
                    if (exceedanceLine != null)
                    {
                        AppendScatterChartSingleSeries(scatterChart, data, xFormatCodeText, sheetTitle,
                            seriesIdx, exceedanceLine.Item1, exceedanceLine.Item2, exceedanceLine.Item3);
                        seriesIdx++;
                    }


                    /* === Red: Charting === */
                    if (warningMinLine != null)
                    {
                        AppendScatterChartSingleSeries(scatterChart, data, xFormatCodeText, sheetTitle,
                            seriesIdx, warningMinLine.Item1, warningMinLine.Item2, warningMinLine.Item3);
                        seriesIdx++;
                    }
                    if (exceedanceMinLine != null)
                    {
                        AppendScatterChartSingleSeries(scatterChart, data, xFormatCodeText, sheetTitle,
                            seriesIdx, exceedanceMinLine.Item1, exceedanceMinLine.Item2, exceedanceMinLine.Item3);
                        seriesIdx++;
                    }
                    /* === end Red === */
                }

                if (drawLineChart || drawLineChartLimits)
                {
                    LineChart lineChart = plotArea.AppendChild(new LineChart(
                        new Grouping { Val = GroupingValues.Standard },
                        new VaryColors { Val = false }));

                    if (drawLineChart)
                    {
                        AppendLineChartDataSeries(lineChart, data, xFormatCodeText, sheetTitle);
                        seriesIdx += data.Columns.Count;
                    }

                    if (warningLine != null)
                    {
                        AppendLineChartSingleSeries(lineChart, data, xFormatCodeText, sheetTitle,
                            seriesIdx, warningLine.Item1, warningLine.Item2, warningLine.Item3);
                        seriesIdx++;
                    }
                    if (exceedanceLine != null)
                    {
                        AppendLineChartSingleSeries(lineChart, data, xFormatCodeText, sheetTitle,
                            seriesIdx, exceedanceLine.Item1, exceedanceLine.Item2, exceedanceLine.Item3);
                        seriesIdx++;
                    }

                    /* === Red: Charting === */
                    if (warningMinLine != null)
                    {
                        AppendLineChartSingleSeries(lineChart, data, xFormatCodeText, sheetTitle,
                            seriesIdx, warningMinLine.Item1, warningMinLine.Item2, warningMinLine.Item3);
                        seriesIdx++;
                    }
                    if (exceedanceMinLine != null)
                    {
                        AppendLineChartSingleSeries(lineChart, data, xFormatCodeText, sheetTitle,
                            seriesIdx, exceedanceMinLine.Item1, exceedanceMinLine.Item2, exceedanceMinLine.Item3);
                        seriesIdx++;
                    }
                    /* === end Red === */


                    lineChart.AppendChild(
                        new DataLabels(
                            new ShowLegendKey(),
                            new ShowValue { Val = new BooleanValue(false) },
                            new ShowCategoryName { Val = new BooleanValue(false) },
                            new ShowSeriesName { Val = new BooleanValue(false) },
                            new ShowPercent { Val = new BooleanValue(false) },
                            new ShowBubbleSize { Val = new BooleanValue(false) }));

                    lineChart.AppendChild(new ShowMarker { Val = new BooleanValue(true) });

                    lineChart.AppendChild(new Smooth { Val = new BooleanValue(false) });

                    lineChart.AppendChild(new AxisId { Val = new UInt32Value(48650112u) });
                    lineChart.AppendChild(new AxisId { Val = new UInt32Value(48672768u) });
                }

                if (drawBarChart)
                    AppendPlotAreaXDateAxis(plotArea, xFormatCodeText);
                else
                    AppendPlotAreaXValueAxis(plotArea, xFormatCodeText);

                AppendPlotAreaYValueAxis(plotArea, valuesAxisTitle, highestvalue, lowestvalue, interval);

                plotArea.AppendChild(new Charts.ShapeProperties(
                    new NoFill(),
                    new DocumentFormat.OpenXml.Drawing.Outline(
                        new NoFill()),
                    new EffectList()));

                AppendChartLegend(chart);

                chart.AppendChild(new PlotVisibleOnly { Val = new BooleanValue(true) });
                chart.AppendChild(new DisplayBlanksAs { Val = new EnumValue<DisplayBlanksAsValues>(DisplayBlanksAsValues.Gap) });
                chart.AppendChild(new ShowDataLabelsOverMaximum { Val = new BooleanValue(false) });

                // Save the chart part.
                chartPart.ChartSpace.Save();

                // Position the chart on the worksheet using a TwoCellAnchor object.
                drawingsPart.WorksheetDrawing = new WorksheetDrawing();
                TwoCellAnchor twoCellAnchor = drawingsPart.WorksheetDrawing.AppendChild(new TwoCellAnchor());
                twoCellAnchor.AppendChild(new DocumentFormat.OpenXml.Drawing.Spreadsheet.FromMarker(
                    new ColumnId("6"),
                    new ColumnOffset("47625"),
                    new RowId("4"),
                    new RowOffset("28575")));
                twoCellAnchor.AppendChild(new DocumentFormat.OpenXml.Drawing.Spreadsheet.ToMarker(
                    new ColumnId("20"),
                    new ColumnOffset("352425"),
                    new RowId("37"),
                    new RowOffset("28575")));

                // Append a GraphicFrame to the TwoCellAnchor object.
                DocumentFormat.OpenXml.Drawing.Spreadsheet.GraphicFrame graphicFrame =
                    twoCellAnchor.AppendChild(
                        new DocumentFormat.OpenXml.Drawing.Spreadsheet.GraphicFrame());
                graphicFrame.Macro = "";

                graphicFrame.AppendChild(
                    new DocumentFormat.OpenXml.Drawing.Spreadsheet.NonVisualGraphicFrameProperties(
                        new DocumentFormat.OpenXml.Drawing.Spreadsheet.NonVisualDrawingProperties { Id = new UInt32Value(2u), Name = "Chart 1" },
                        new DocumentFormat.OpenXml.Drawing.Spreadsheet.NonVisualGraphicFrameDrawingProperties()));

                graphicFrame.AppendChild(
                    new Transform(
                        new Offset { X = 0L, Y = 0L },
                        new Extents { Cx = 0L, Cy = 0L }));

                graphicFrame.AppendChild(
                    new Graphic(
                        new GraphicData(
                            new ChartReference() { Id = drawingsPart.GetIdOfPart(chartPart) })
                        {
                            Uri = "http://schemas.openxmlformats.org/drawingml/2006/chart"
                        }));

                twoCellAnchor.AppendChild(new ClientData());

                // Save the WorksheetDrawing object.
                drawingsPart.WorksheetDrawing.Save();
        }

private void AppendChartTitle(Charts.Chart chart, string chartTitle, string chartSubTitle)
        {
            // Title
            Paragraph paragraphTitle = new Paragraph(
                new ParagraphProperties(
                    new DefaultRunProperties(
                        new SolidFill(
                            new SchemeColor(
                                new LuminanceModulation { Val = 65000 },
                                new LuminanceOffset { Val = 35000 })
                            {
                                Val = SchemeColorValues.Text1
                            }),
                        new LatinFont { Typeface = "+mn-lt" },
                        new EastAsianFont { Typeface = "+mn-ea" },
                        new ComplexScriptFont { Typeface = "+mn-cs" })
                    {
                        FontSize = 1400,
                        Bold = false,
                        Italic = false,
                        Underline = TextUnderlineValues.None,
                        Strike = TextStrikeValues.NoStrike,
                        Kerning = 1200,
                        Spacing = 0,
                        Baseline = 0
                    }),
                new DocumentFormat.OpenXml.Drawing.Run(
                    new DocumentFormat.OpenXml.Drawing.RunProperties { Language = "en-AU", Bold = true },
                    new DocumentFormat.OpenXml.Drawing.Text { Text = chartTitle }));

            if (!String.IsNullOrEmpty(chartSubTitle))
            {
                paragraphTitle.AppendChild(
                    new DocumentFormat.OpenXml.Drawing.Break(
                        new DocumentFormat.OpenXml.Drawing.RunProperties { Language = "en-AU" }));
                paragraphTitle.AppendChild(
                    new DocumentFormat.OpenXml.Drawing.Run(
                        new DocumentFormat.OpenXml.Drawing.RunProperties { Language = "en-AU", FontSize = 1200 },
                        new DocumentFormat.OpenXml.Drawing.Text { Text = chartSubTitle }));
            }

            chart.AppendChild(
                new Title(
                    new ChartText(
                        new RichText(
                            new BodyProperties
                            {
                                Rotation = 0,
                                UseParagraphSpacing = true,
                                VerticalOverflow = TextVerticalOverflowValues.Ellipsis,
                                Vertical = TextVerticalValues.Horizontal,
                                Wrap = TextWrappingValues.Square,
                                Anchor = TextAnchoringTypeValues.Center,
                                AnchorCenter = true
                            },
                            new ListStyle(),
                            paragraphTitle)),
                    new Charts.Layout(),
                    new Overlay { Val = new BooleanValue(false) },
                    new ChartShapeProperties(
                        new NoFill(),
                        new DocumentFormat.OpenXml.Drawing.Outline(
                            new NoFill()),
                        new EffectList()),
                    new Charts.TextProperties(
                        new BodyProperties
                        {
                            Rotation = 0,
                            UseParagraphSpacing = true,
                            VerticalOverflow = TextVerticalOverflowValues.Ellipsis,
                            Vertical = TextVerticalValues.Horizontal,
                            Wrap = TextWrappingValues.Square,
                            Anchor = TextAnchoringTypeValues.Center,
                            AnchorCenter = true
                        },
                        new ListStyle(),
                        new Paragraph(
                            new ParagraphProperties(
                                new DefaultRunProperties(
                                    new SolidFill(
                                        new SchemeColor(
                                            new LuminanceModulation { Val = 65000 },
                                            new LuminanceOffset { Val = 35000 })
                                        {
                                            Val = SchemeColorValues.Text1
                                        }),
                                    new LatinFont { Typeface = "+mn-lt" },
                                    new EastAsianFont { Typeface = "+mn-ea" },
                                    new ComplexScriptFont { Typeface = "+mn-cs" })
                                {
                                    FontSize = 1400,
                                    Bold = false,
                                    Italic = false,
                                    Underline = TextUnderlineValues.None,
                                    Strike = TextStrikeValues.NoStrike,
                                    Kerning = 1200,
                                    Spacing = 0,
                                    Baseline = 0
                                }),
                            new EndParagraphRunProperties { Language = "en-AU" }))));

            chart.AppendChild(new AutoTitleDeleted { Val = new BooleanValue(false) });
        }

        private void AppendChartLegend(Charts.Chart chart)
        {
            // Add the chart Legend
            Legend legend = chart.AppendChild(
                new Legend(
                    new LegendPosition { Val = new EnumValue<LegendPositionValues>(LegendPositionValues.Bottom) },
                    new Charts.Layout(),
                    new Overlay { Val = new BooleanValue(false) },
                    new ChartShapeProperties(
                        new NoFill(),
                        new DocumentFormat.OpenXml.Drawing.Outline(
                            new NoFill()),
                        new EffectList()),
                    new Charts.TextProperties(
                        new BodyProperties
                        {
                            Rotation = 0,
                            UseParagraphSpacing = true,
                            VerticalOverflow = TextVerticalOverflowValues.Ellipsis,
                            Vertical = TextVerticalValues.Horizontal,
                            Wrap = TextWrappingValues.Square,
                            Anchor = TextAnchoringTypeValues.Center,
                            AnchorCenter = true
                        },
                        new ListStyle(),
                        new Paragraph(
                            new ParagraphProperties(
                                new DefaultRunProperties(
                                    new SolidFill(
                                        new SchemeColor(
                                            new LuminanceModulation { Val = 65000 },
                                            new LuminanceOffset { Val = 35000 })
                                        {
                                            Val = SchemeColorValues.Text1
                                        }),
                                    new LatinFont { Typeface = "+mn-lt" },
                                    new EastAsianFont { Typeface = "+mn-ea" },
                                    new ComplexScriptFont { Typeface = "+mn-cs" })
                                {
                                    FontSize = 900,
                                    Bold = false,
                                    Italic = false,
                                    Underline = TextUnderlineValues.None,
                                    Strike = TextStrikeValues.NoStrike,
                                    Kerning = 1200,
                                    Baseline = 0
                                }),
                            new EndParagraphRunProperties { Language = "en-AU" }))));
        }

        private void AppendPlotAreaXDateAxis(PlotArea plotArea, string xFormatCodeText)
        {
            // Add the Date Axis.
            DateAxis dateAxis = plotArea.AppendChild(
                new DateAxis(
                    new AxisId { Val = new UInt32Value(48650112u) },
                    new Scaling(
                        new Charts.Orientation { Val = Charts.OrientationValues.MinMax }),
                    new Delete { Val = false },
                    new AxisPosition { Val = new EnumValue<AxisPositionValues>(AxisPositionValues.Bottom) },
                    new Charts.NumberingFormat { FormatCode = xFormatCodeText, SourceLinked = true },
                    new MajorTickMark { Val = TickMarkValues.Outside },
                    new MinorTickMark { Val = TickMarkValues.None },
                    new TickLabelPosition { Val = new EnumValue<TickLabelPositionValues>(TickLabelPositionValues.NextTo) },
                    new ChartShapeProperties(
                        new NoFill(),
                        new DocumentFormat.OpenXml.Drawing.Outline(
                            new SolidFill(
                                new SchemeColor(
                                    new LuminanceModulation { Val = 15000 },
                                    new LuminanceOffset { Val = 85000 })
                                {
                                    Val = SchemeColorValues.Text1
                                }),
                            new Round())
                        {
                            Width = 9525,
                            CapType = LineCapValues.Flat,
                            CompoundLineType = CompoundLineValues.Single,
                            Alignment = PenAlignmentValues.Center
                        },
                        new EffectList()),
                    new Charts.TextProperties(
                        new BodyProperties
                        {
                            Rotation = new Int32Value(3000000),
                            UseParagraphSpacing = true,
                            VerticalOverflow = TextVerticalOverflowValues.Ellipsis,
                            Vertical = TextVerticalValues.Horizontal,
                            Wrap = TextWrappingValues.Square,
                            Anchor = TextAnchoringTypeValues.Center,
                            AnchorCenter = true
                        },
                        new ListStyle(),
                        new Paragraph(
                            new ParagraphProperties(
                                new DefaultRunProperties(
                                    new SolidFill(
                                        new SchemeColor(
                                            new LuminanceModulation { Val = 65000 },
                                            new LuminanceOffset { Val = 35000 })
                                        {
                                            Val = SchemeColorValues.Text1
                                        }),
                                    new LatinFont { Typeface = "+mn-lt" },
                                    new EastAsianFont { Typeface = "+mn-ea" },
                                    new ComplexScriptFont { Typeface = "+mn-cs" })
                                {
                                    FontSize = 900,
                                    Bold = false,
                                    Italic = false,
                                    Underline = TextUnderlineValues.None,
                                    Strike = TextStrikeValues.NoStrike,
                                    Kerning = 1200,
                                    Baseline = 0
                                }),
                            new EndParagraphRunProperties() { Language = "en-AU" })),
                    new CrossingAxis { Val = new UInt32Value(48672768U) },
                    new Crosses { Val = CrossesValues.AutoZero },
                    new AutoLabeled { Val = new BooleanValue(true) },
                    new LabelAlignment { Val = LabelAlignmentValues.Center },
                    new LabelOffset { Val = new UInt16Value((ushort)100) },
                    new BaseTimeUnit { Val = TimeUnitValues.Days }));
        }

        private void AppendPlotAreaXValueAxis(PlotArea plotArea, string xFormatCodeText)
        {
            // Add the Value Axis X
            ValueAxis valueAxisX = plotArea.AppendChild(
                new ValueAxis(
                    new AxisId { Val = new UInt32Value(48650112u) },
                    new Scaling(
                        new Charts.Orientation { Val = Charts.OrientationValues.MinMax }),
                    new Delete { Val = false },
                    new AxisPosition { Val = new EnumValue<AxisPositionValues>(AxisPositionValues.Bottom) },
                    new MajorGridlines(
                        new ChartShapeProperties(
                            new DocumentFormat.OpenXml.Drawing.Outline(
                                new SolidFill(
                                    new SchemeColor(
                                        new LuminanceModulation { Val = 15000 },
                                        new LuminanceOffset { Val = 85000 })
                                    {
                                        Val = SchemeColorValues.Text1
                                    }),
                                new Round())
                            {
                                Width = 9525,
                                CapType = LineCapValues.Flat,
                                CompoundLineType = CompoundLineValues.Single,
                                Alignment = PenAlignmentValues.Center
                            },
                            new EffectList())),
                    new Charts.NumberingFormat { FormatCode = xFormatCodeText, SourceLinked = true },
                    new MajorTickMark { Val = TickMarkValues.Inside },
                    new MinorTickMark { Val = TickMarkValues.Inside },
                    new TickLabelPosition
                    {
                        Val = new EnumValue<TickLabelPositionValues>(TickLabelPositionValues.NextTo)
                    },
                    new ChartShapeProperties(
                        new NoFill(),
                        new DocumentFormat.OpenXml.Drawing.Outline(
                            new SolidFill(
                                new SchemeColor(
                                    new LuminanceModulation { Val = 15000 },
                                    new LuminanceOffset { Val = 85000 })
                                {
                                    Val = SchemeColorValues.Text1
                                }),
                            new Round())
                        {
                            Width = 9525,
                            CapType = LineCapValues.Flat,
                            CompoundLineType = CompoundLineValues.Single,
                            Alignment = PenAlignmentValues.Center
                        },
                        new EffectList()),
                    new Charts.TextProperties(
                        new BodyProperties
                        {
                            Rotation = new Int32Value(3000000),
                            UseParagraphSpacing = true,
                            VerticalOverflow = TextVerticalOverflowValues.Ellipsis,
                            Vertical = TextVerticalValues.Horizontal,
                            Wrap = TextWrappingValues.Square,
                            Anchor = TextAnchoringTypeValues.Center,
                            AnchorCenter = true
                        },
                        new ListStyle(),
                        new Paragraph(
                            new ParagraphProperties(
                                new DefaultRunProperties(
                                    new SolidFill(
                                        new SchemeColor(
                                            new LuminanceModulation { Val = 65000 },
                                            new LuminanceOffset { Val = 35000 })
                                        {
                                            Val = SchemeColorValues.Text1
                                        }),
                                    new LatinFont { Typeface = "+mn-lt" },
                                    new EastAsianFont { Typeface = "+mn-ea" },
                                    new ComplexScriptFont { Typeface = "+mn-cs" })
                                {
                                    FontSize = 900,
                                    Bold = false,
                                    Italic = false,
                                    Underline = TextUnderlineValues.None,
                                    Strike = TextStrikeValues.NoStrike,
                                    Kerning = 1200,
                                    Baseline = 0
                                }),
                            new EndParagraphRunProperties() { Language = "en-AU" })),
                    new CrossingAxis { Val = new UInt32Value(48672768U) },
                    new Crosses { Val = CrossesValues.AutoZero },
                    new AutoLabeled { Val = new BooleanValue(true) },
                    new LabelAlignment { Val = LabelAlignmentValues.Center },
                    new LabelOffset { Val = new UInt16Value((ushort)100) },
                    new BaseTimeUnit { Val = TimeUnitValues.Days }));
        }

        private void AppendPlotAreaYValueAxis(PlotArea plotArea, string valuesAxisTitle,
            /*RP Added Ticket 5119*/ 
            string highestvalue = "", string lowestvalue = "", string interval = "")
            /*End Modification*/
        {
            //RP Added Ticket 5119 
            Int32 MaxAxisValue = 0, MinAxisValue = 0, Interval = 0;
            Int32.TryParse(highestvalue, out MaxAxisValue);
            Int32.TryParse(lowestvalue, out MinAxisValue);
            Int32.TryParse(interval, out Interval);
            //End Modification

            // Add the Value Axis.
            ValueAxis valueAxis = plotArea.AppendChild(
                new ValueAxis(new AxisId { Val = new UInt32Value(48672768u) },
                    new Scaling(
                        new Charts.Orientation { Val = Charts.OrientationValues.MinMax }, MaxAxisValue == 0 ? null : new Charts.MaxAxisValue { Val = MaxAxisValue }, MinAxisValue == 0 ? null:  new Charts.MinAxisValue { Val = MinAxisValue }),
                    new Delete { Val = false },
                    new AxisPosition { Val = AxisPositionValues.Left },
                    new MajorGridlines(
                        new ChartShapeProperties(
                            new DocumentFormat.OpenXml.Drawing.Outline(
                                new SolidFill(
                                    new SchemeColor(
                                        new LuminanceModulation { Val = 15000 },
                                        new LuminanceOffset { Val = 85000 })
                                    {
                                        Val = SchemeColorValues.Text1
                                    }),
                                new Round())
                            {
                                Width = 9525,
                                CapType = LineCapValues.Flat,
                                CompoundLineType = CompoundLineValues.Single,
                                Alignment = PenAlignmentValues.Center
                            },
                            new EffectList())),
                    new Title(
                        new ChartText(
                            new RichText(
                                new BodyProperties
                                {
                                    Rotation = -5400000,
                                    UseParagraphSpacing = true,
                                    VerticalOverflow = TextVerticalOverflowValues.Ellipsis,
                                    Vertical = TextVerticalValues.Horizontal,
                                    Wrap = TextWrappingValues.Square,
                                    Anchor = TextAnchoringTypeValues.Center,
                                    AnchorCenter = true
                                },
                                new ListStyle(),
                                new Paragraph(
                                    new ParagraphProperties(
                                        new DefaultRunProperties(
                                            new SolidFill(
                                                new SchemeColor(
                                                    new LuminanceModulation { Val = 65000 },
                                                    new LuminanceOffset { Val = 35000 })
                                                {
                                                    Val = SchemeColorValues.Text1
                                                }),
                                            new LatinFont { Typeface = "+mn-lt" },
                                            new EastAsianFont { Typeface = "+mn-ea" },
                                            new ComplexScriptFont { Typeface = "+mn-cs" })
                                        {
                                            FontSize = 1000,
                                            Bold = false,
                                            Italic = false,
                                            Underline = TextUnderlineValues.None,
                                            Strike = TextStrikeValues.NoStrike,
                                            Kerning = 1200,
                                            Spacing = 0,
                                            Baseline = 0
                                        }),
                                    new DocumentFormat.OpenXml.Drawing.Run(
                                        new DocumentFormat.OpenXml.Drawing.RunProperties { Language = "en-AU" },
                                        new DocumentFormat.OpenXml.Drawing.Text { Text = valuesAxisTitle })))),
                        new Charts.Layout(),
                        new Overlay { Val = new BooleanValue(false) },
                        new ChartShapeProperties(
                            new NoFill(),
                            new DocumentFormat.OpenXml.Drawing.Outline(
                                new NoFill()),
                            new EffectList()),
                        new Charts.TextProperties(
                            new BodyProperties
                            {
                                Rotation = -5400000,
                                UseParagraphSpacing = true,
                                VerticalOverflow = TextVerticalOverflowValues.Ellipsis,
                                Vertical = TextVerticalValues.Horizontal,
                                Wrap = TextWrappingValues.Square,
                                Anchor = TextAnchoringTypeValues.Center,
                                AnchorCenter = true
                            },
                            new ListStyle(),
                            new Paragraph(
                                new ParagraphProperties(
                                    new DefaultRunProperties(
                                        new SolidFill(
                                            new SchemeColor(
                                                new LuminanceModulation { Val = 65000 },
                                                new LuminanceOffset { Val = 35000 })
                                            {
                                                Val = SchemeColorValues.Text1
                                            }),
                                        new LatinFont { Typeface = "+mn-lt" },
                                        new EastAsianFont { Typeface = "+mn-ea" },
                                        new ComplexScriptFont { Typeface = "+mn-cs" })
                                    {
                                        FontSize = 1000,
                                        Bold = false,
                                        Italic = false,
                                        Underline = TextUnderlineValues.None,
                                        Strike = TextStrikeValues.NoStrike,
                                        Kerning = 1200,
                                        Spacing = 0,
                                        Baseline = 0
                                    }),
                                new EndParagraphRunProperties { Language = "en-AU" }))),
                    new Charts.NumberingFormat
                    {
                        FormatCode = new StringValue("General"),
                        SourceLinked = new BooleanValue(true)
                    },
                    new MajorTickMark { Val = TickMarkValues.None },
                    new MinorTickMark { Val = TickMarkValues.None },
                    new TickLabelPosition { Val = TickLabelPositionValues.NextTo },
                    new ChartShapeProperties(
                        new NoFill(),
                        new DocumentFormat.OpenXml.Drawing.Outline(
                            new NoFill()),
                        new EffectList()),
                    new Charts.TextProperties(
                        new BodyProperties
                        {
                            Rotation = -60000000,
                            UseParagraphSpacing = true,
                            VerticalOverflow = TextVerticalOverflowValues.Ellipsis,
                            Vertical = TextVerticalValues.Horizontal,
                            Wrap = TextWrappingValues.Square,
                            Anchor = TextAnchoringTypeValues.Center,
                            AnchorCenter = true
                        },
                        new ListStyle(),
                        new Paragraph(
                            new ParagraphProperties(
                                new DefaultRunProperties(
                                    new SolidFill(
                                        new SchemeColor(
                                            new LuminanceModulation() { Val = 65000 },
                                            new LuminanceOffset() { Val = 35000 })
                                        {
                                            Val = SchemeColorValues.Text1
                                        }),
                                    new LatinFont { Typeface = "+mn-lt" },
                                    new EastAsianFont { Typeface = "+mn-ea" },
                                    new ComplexScriptFont { Typeface = "+mn-cs" })
                                {
                                    FontSize = 900,
                                    Bold = false,
                                    Italic = false,
                                    Underline = TextUnderlineValues.None,
                                    Strike = TextStrikeValues.NoStrike,
                                    Kerning = 1200,
                                    Baseline = 0
                                }),
                            new EndParagraphRunProperties { Language = "en-AU" })),
                    new CrossingAxis { Val = new UInt32Value(48650112U) },
                    new Crosses { Val = new EnumValue<CrossesValues>(CrossesValues.AutoZero) },
                    new CrossBetween { Val = new EnumValue<CrossBetweenValues>(CrossBetweenValues.Between) }));
        }

        private void AppendLineChartDataSeries(LineChart lineChart, System.Data.DataTable data, string xFormatCodeText, string sheetTitle)
        {
            for (uint i = 1; i < data.Columns.Count; i++)
            {
                SeriesText seriesText = new SeriesText(
                    new StringReference(
                        new Charts.Formula { Text = String.Format("'{0}'!{1}1", sheetTitle, GetExcelColumnName((int)i + 1)) },
                        new StringCache(
                            new PointCount { Val = (UInt32Value)1U },
                            new StringPoint(
                                new NumericValue { Text = data.Columns[(int)i].ColumnName })
                            {
                                Index = (UInt32Value)0U
                            })));

                NumberingCache cacheX = new NumberingCache(
                    new FormatCode { Text = xFormatCodeText },
                    new PointCount { Val = (UInt32Value)Convert.ToUInt32(data.Rows.Count) }
                );
                uint k = 0;
                foreach (DataRow row in data.Rows)
                {
                    NumericPoint np = new NumericPoint(
                        new NumericValue { Text = ((DateTime)row[0]).ToOADate().ToString(CultureInfo.InvariantCulture) })
                    {
                        Index = k
                    };
                    cacheX.AppendChild(np);
                    k++;
                }
                NumberReference nrX = new NumberReference(
                    new Charts.Formula { Text = String.Format("'{0}'!A2:A{1}", sheetTitle, data.Rows.Count + 1) },
                    cacheX);
                CategoryAxisData categoryAxisData = new CategoryAxisData(nrX);

                NumberingCache cache = new NumberingCache(
                    new FormatCode { Text = "General" },
                    new PointCount { Val = Convert.ToUInt32(data.Rows.Count) });
                uint j = 0;
                foreach (DataRow row in data.Rows)
                {
                    string value = "#N/A";
                    if (!row.IsNull((int)i))
                    {
                        value = RemoveNonNumeric(row[(int) i].ToString());
                        if (String.IsNullOrEmpty(value))
                            value = "#N/A";
                    }
                    NumericPoint np = new NumericPoint(
                        new NumericValue { Text = value })
                    {
                        Index = j
                    };
                    cache.AppendChild(np);
                    j++;
                }
                string s = String.Format("'{0}'!{1}2:{1}{2}", sheetTitle, GetExcelColumnName((int)i + 1), data.Rows.Count + 1);
                NumberReference nr = new NumberReference(
                    new Charts.Formula { Text = s },
                    cache);
                Charts.Values values = new Charts.Values(nr);

                LineChartSeries lineChartSeries = lineChart.AppendChild(
                    new LineChartSeries(
                        new Index { Val = new UInt32Value(i) },
                        new Order { Val = new UInt32Value(i) },
                        seriesText,
                        new ChartShapeProperties(
                            new DocumentFormat.OpenXml.Drawing.Outline(
                                new SolidFill(GetSchemeColor(new SchemeColor(), (int)i)),
                                new Round())
                            {
                                Width = 19050,
                                CapType = LineCapValues.Round
                            },
                            new EffectList()),
                        new Marker(
                            new Symbol { Val = MarkerStyleValues.Circle },
                            new Size { Val = 5 },
                            new ChartShapeProperties(
                                new SolidFill(GetSchemeColor(new SchemeColor(), (int)i)),
                                new DocumentFormat.OpenXml.Drawing.Outline(
                                    new SolidFill(GetSchemeColor(new SchemeColor(), (int)i)))
                                {
                                    Width = 9525
                                },
                                new EffectList()
                            )),
                        categoryAxisData,
                        values,
                        new Smooth { Val = false }));
            }
        }

        private void AppendScatterChartDataSeries(ScatterChart scatterChart, System.Data.DataTable data, string xFormatCodeText, string sheetTitle, List<Tuple<int, string>> listSeriesColors)
        {
            for (uint i = 1; i < data.Columns.Count; i++)
            {
                SeriesText seriesText = new SeriesText(
                    new StringReference(
                        new Charts.Formula { Text = String.Format("'{0}'!{1}1", sheetTitle, GetExcelColumnName((int)i + 1)) },
                        new StringCache(
                            new PointCount { Val = (UInt32Value)1U },
                            new StringPoint(
                                new NumericValue { Text = data.Columns[(int)i].ColumnName })
                            {
                                Index = (UInt32Value)0U
                            })));

                NumberingCache cacheX = new NumberingCache(
                    new FormatCode { Text = xFormatCodeText },
                    new PointCount { Val = (UInt32Value)Convert.ToUInt32(data.Rows.Count) }
                );
                uint k = 0;
                foreach (DataRow row in data.Rows)
                {
                    NumericPoint np = new NumericPoint(
                        new NumericValue { Text = ((DateTime)row[0]).ToOADate().ToString(CultureInfo.InvariantCulture) })
                    {
                        Index = k
                    };
                    cacheX.AppendChild(np);
                    k++;
                }
                NumberReference nrX = new NumberReference(
                    new Charts.Formula { Text = String.Format("'{0}'!A2:A{1}", sheetTitle, data.Rows.Count + 1) },
                    cacheX);
                XValues xValues = new XValues(nrX);

                NumberingCache cache = new NumberingCache(
                    new FormatCode { Text = "General" },
                    new PointCount { Val = Convert.ToUInt32(data.Rows.Count) });
                uint j = 0;
                foreach (DataRow row in data.Rows)
                {
                    string value = "#N/A";
                    if (!row.IsNull((int)i))
                    {
                        value = RemoveNonNumeric(row[(int)i].ToString());
                        if (String.IsNullOrEmpty(value))
                            value = "#N/A";
                    }
                    NumericPoint np = new NumericPoint(
                        new NumericValue { Text = value })
                    {
                        Index = j
                    };
                    cache.AppendChild(np);
                    j++;
                }
                string s = String.Format("'{0}'!{1}2:{1}{2}", sheetTitle, GetExcelColumnName((int)i + 1), data.Rows.Count + 1);
                NumberReference nr = new NumberReference(
                    new Charts.Formula { Text = s },
                    cache);
                YValues yValues = new YValues(nr);


                /* === Red: Charting, update color of the line === */
                string colorSeries ="";
                if (listSeriesColors != null)
                {
                    foreach (Tuple<int, string> color in listSeriesColors)
                    {
                        if (i == color.Item1)
                        {
                            colorSeries = color.Item2;
                            break;
                        }
                    }
                }
            
              
                //loop below to select a symbol...repeat all to check if e.g. triagle... then the other...
                ScatterChartSeries scatterChartSeries = scatterChart.AppendChild(
                    new ScatterChartSeries(
                        new Index { Val = new UInt32Value(i + 1) },
                        new Order { Val = new UInt32Value(i) },
                        seriesText,
                        new ChartShapeProperties(
                            new DocumentFormat.OpenXml.Drawing.Outline(
                                 colorSeries == "" ? 
                                 new SolidFill(GetSchemeColor(new SchemeColor(), (int)i)) :
                                 new SolidFill(new RgbColorModelHex { Val = colorSeries.ToUpper().Replace("#", "") } ),
                                new Round())
                            {
                                Width = 19050,
                                CapType = LineCapValues.Round
                            },
                            new EffectList()),
                        new Marker(
                            new Symbol { Val = MarkerStyleValues.Circle },
                            new Size { Val = 5 },
                            new ChartShapeProperties(
                                  colorSeries == "" ?
                                  new SolidFill(GetSchemeColor(new SchemeColor(), (int)i)) :
                                  new SolidFill(new RgbColorModelHex { Val = colorSeries.ToUpper().Replace("#", "") }),
                                  new DocumentFormat.OpenXml.Drawing.Outline(
                                    colorSeries == "" ? 
                                    new SolidFill(GetSchemeColor(new SchemeColor(), (int)i)) :
                                    new SolidFill(new RgbColorModelHex { Val =  colorSeries.ToUpper().Replace("#", "") } ))
                                {
                                    Width = 9525
                                },
                                new EffectList()
                            )),
                        new DataLabels(
                            new Delete {  Val = true }
                            ),
                        xValues,
                        yValues,
                        new Smooth { Val = false }));
                /* === end Red ===  */
            }
        }


        /* == Red 04012020: When line chart contains only one date == */
        private void AppendScatterChartDataSeriesOneDate(ScatterChart scatterChart, System.Data.DataTable data, string xFormatCodeText, string sheetTitle, List<Tuple<int, string>> listSeriesColors)
        {
            for (uint i = 1; i < data.Columns.Count; i++)
            {
                SeriesText seriesText = new SeriesText(
                    new StringReference(
                        new Charts.Formula { Text = String.Format("'{0}'!{1}1", sheetTitle, GetExcelColumnName((int)i + 1)) },
                        new StringCache(
                            new PointCount { Val = (UInt32Value)1U },
                            new StringPoint(
                                new NumericValue { Text = data.Columns[(int)i].ColumnName })
                            {
                                Index = (UInt32Value)0U
                            })));

                NumberingCache cacheX = new NumberingCache(
                    new FormatCode { Text = xFormatCodeText },
                    new PointCount { Val = (UInt32Value)Convert.ToUInt32(data.Rows.Count) }
                );
                uint k = 0;
                foreach (DataRow row in data.Rows)
                {
                    NumericPoint np = new NumericPoint(
                        new NumericValue { Text = ((DateTime)row[0]).ToOADate().ToString(CultureInfo.InvariantCulture) })
                    {
                        Index = k
                    };
                    cacheX.AppendChild(np);
                    k++;
                }
                NumberReference nrX = new NumberReference(
                    new Charts.Formula { Text = String.Format("'{0}'!A3:A3", sheetTitle) },
                    cacheX);
                XValues xValues = new XValues(nrX);

                NumberingCache cache = new NumberingCache(
                    new FormatCode { Text = "General" },
                    new PointCount { Val = Convert.ToUInt32(data.Rows.Count) });
                uint j = 0;
                foreach (DataRow row in data.Rows)
                {
                    string value = "#N/A";
                    if (!row.IsNull((int)i))
                    {
                        value = RemoveNonNumeric(row[(int)i].ToString());
                        if (String.IsNullOrEmpty(value))
                            value = "#N/A";
                    }
                    NumericPoint np = new NumericPoint(
                        new NumericValue { Text = value })
                    {
                        Index = j
                    };
                    cache.AppendChild(np);
                    j++;
                }
                string s = String.Format("'{0}'!{1}3:{1}3", sheetTitle, GetExcelColumnName((int)i + 1), data.Rows.Count + 1);
                NumberReference nr = new NumberReference(
                    new Charts.Formula { Text = s },
                    cache);
                YValues yValues = new YValues(nr);


                /* === Red: Charting, update color of the line === */
                string colorSeries = "";
                if (listSeriesColors != null)
                {
                    foreach (Tuple<int, string> color in listSeriesColors)
                    {
                        if (i == color.Item1)
                        {
                            colorSeries = color.Item2;
                            break;
                        }
                    }
                }

                //loop below to select a symbol...repeat all to check if e.g. triagle... then the other...
                ScatterChartSeries scatterChartSeries = scatterChart.AppendChild(
                    new ScatterChartSeries(
                        new Index { Val = new UInt32Value(i + 1) },
                        new Order { Val = new UInt32Value(i) },
                        seriesText,
                        new ChartShapeProperties(
                            new DocumentFormat.OpenXml.Drawing.Outline(
                                 colorSeries == "" ?
                                 new SolidFill(GetSchemeColor(new SchemeColor(), (int)i)) :
                                 new SolidFill(new RgbColorModelHex { Val = colorSeries.ToUpper().Replace("#", "") }),
                                new Round())
                            {
                                Width = 19050,
                                CapType = LineCapValues.Round
                            },
                            new EffectList()),
                        new Marker(
                            new Symbol { Val = MarkerStyleValues.Circle },
                            new Size { Val = 5 },
                            new ChartShapeProperties(
                                  colorSeries == "" ?
                                  new SolidFill(GetSchemeColor(new SchemeColor(), (int)i)) :
                                  new SolidFill(new RgbColorModelHex { Val = colorSeries.ToUpper().Replace("#", "") }),
                                  new DocumentFormat.OpenXml.Drawing.Outline(
                                    colorSeries == "" ?
                                    new SolidFill(GetSchemeColor(new SchemeColor(), (int)i)) :
                                    new SolidFill(new RgbColorModelHex { Val = colorSeries.ToUpper().Replace("#", "") }))
                                  {
                                      Width = 9525
                                  },
                                new EffectList()
                            )),
                        new DataLabels(
                            new Delete { Val = true }
                            ),
                        xValues,
                        yValues,
                        new Smooth { Val = false }));
                /* === end Red ===  */
            }
        }
        /* == End Red == */
        private void AppendBarChartDataSeries(BarChart barChart, System.Data.DataTable data, string xFormatCodeText, string sheetTitle, List<Tuple<int, string>> listSeriesColors)
        {
            for (uint i = 1; i < data.Columns.Count; i++)
            {
                SeriesText seriesText = new SeriesText(
                    new StringReference(
                        new Charts.Formula { Text = String.Format("'{0}'!{1}1", sheetTitle, GetExcelColumnName((int)i + 1)) },
                        new StringCache(
                            new PointCount { Val = (UInt32Value)1U },
                            new StringPoint(
                                new NumericValue { Text = data.Columns[(int)i].ColumnName })
                            {
                                Index = (UInt32Value)0U
                            })));

                NumberingCache cacheX = new NumberingCache(
                    new FormatCode { Text = xFormatCodeText },
                    new PointCount { Val = (UInt32Value)Convert.ToUInt32(data.Rows.Count) }
                );
                uint k = 0;
                foreach (DataRow row in data.Rows)
                {
                    NumericPoint np = new NumericPoint(
                        new NumericValue { Text = ((DateTime)row[0]).ToOADate().ToString(CultureInfo.InvariantCulture) })
                    {
                        Index = k
                    };
                    cacheX.AppendChild(np);
                    k++;
                }
                NumberReference nrX = new NumberReference(
                    new Charts.Formula { Text = String.Format("'{0}'!A2:A{1}", sheetTitle, data.Rows.Count + 1) },
                    cacheX);
                CategoryAxisData categoryAxisData = new CategoryAxisData(nrX);

                NumberingCache cache = new NumberingCache(
                    new FormatCode { Text = "General" },
                    new PointCount { Val = Convert.ToUInt32(data.Rows.Count) });
                uint j = 0;
                foreach (DataRow row in data.Rows)
                {
                    string value = "#N/A";
                    if (!row.IsNull((int)i))
                    { 
                        value = RemoveNonNumeric(row[(int)i].ToString());
                        if (String.IsNullOrEmpty(value))
                            value = "#N/A";
                    }
                    NumericPoint np = new NumericPoint(
                        new NumericValue { Text = value })
                    {
                        Index = j
                    };
                    cache.AppendChild(np);
                    j++;
                }
                string s = String.Format("'{0}'!{1}2:{1}{2}", sheetTitle, GetExcelColumnName((int)i + 1), data.Rows.Count + 1);
                NumberReference nr = new NumberReference(
                    new Charts.Formula { Text = s },
                    cache);
                Charts.Values values = new Charts.Values(nr);

                string colorSeries = "";
                if (listSeriesColors != null)
                {
                    foreach (Tuple<int, string> color in listSeriesColors)
                    {
                        if (i == color.Item1)
                        {
                            colorSeries = color.Item2;
                            break;
                        }
                    }
                }
              
                    BarChartSeries barChartSeries = barChart.AppendChild(
                 new BarChartSeries(
                     new Index { Val = new UInt32Value(i) },
                     new Order { Val = new UInt32Value(i) },
                     seriesText,
                     new ChartShapeProperties(
                         colorSeries == "" ?
                         new SolidFill(GetSchemeColor(new SchemeColor(), (int)i)) :
                         new SolidFill(new RgbColorModelHex { Val = colorSeries.ToUpper().Replace("#", "") } ),
                         new DocumentFormat.OpenXml.Drawing.Outline(
                             new NoFill()),
                         new EffectList()),
                     new InvertIfNegative { Val = false },
                     categoryAxisData,
                     values));
            }
        }

        private void AppendLineChartSingleSeries(LineChart lineChart, System.Data.DataTable data, string xFormatCodeText, string sheetTitle,
            int seriesIdx, string seriesText, double seriesValue, string seriesColor)
        {
            SeriesText warningText = new SeriesText(
                new StringReference(
                    new Charts.Formula { Text = String.Format("'{0}'!{1}1", sheetTitle, GetExcelColumnName(seriesIdx + 2)) }, // +2: empty column between last data and limits
                    new StringCache(
                        new PointCount { Val = (UInt32Value)1U },
                        new StringPoint(
                            new NumericValue { Text = seriesText })
                        {
                            Index = (UInt32Value)0U
                        })));

            NumberingCache cacheX = new NumberingCache(
                new FormatCode { Text = xFormatCodeText },
                new PointCount { Val = (UInt32Value)Convert.ToUInt32(data.Rows.Count) }
            );
            uint k = 0;
            foreach (DataRow row in data.Rows)
            {
                NumericPoint np = new NumericPoint(
                    new NumericValue { Text = ((DateTime)row[0]).ToOADate().ToString(CultureInfo.InvariantCulture) })
                {
                    Index = k
                };
                cacheX.AppendChild(np);
                k++;
            }
            NumberReference nrX = new NumberReference(
                new Charts.Formula { Text = String.Format("'{0}'!A2:A{1}", sheetTitle, data.Rows.Count + 1) },
                cacheX);
            CategoryAxisData categoryAxisData = new CategoryAxisData(nrX);

            NumberingCache cache = new NumberingCache(
                new FormatCode { Text = "General" },
                new PointCount { Val = Convert.ToUInt32(data.Rows.Count) });
            for (uint i = 0; i < data.Rows.Count; i++)
            {
                NumericPoint np = new NumericPoint(
                    new NumericValue
                    {
                        Text = (i == 0 || i == data.Rows.Count - 1)
                            ? seriesValue.ToString(CultureInfo.InvariantCulture)
                            : "#N/A"
                    })
                {
                    Index = i
                };
                cache.AppendChild(np);
            }
            string s = String.Format("'{0}'!{1}2:{1}{2}", sheetTitle, GetExcelColumnName(seriesIdx + 2), data.Rows.Count + 1);
            NumberReference nr = new NumberReference(
                new Charts.Formula { Text = s },
                cache);
            Charts.Values values = new Charts.Values(nr);

            LineChartSeries lineChartSeries = lineChart.AppendChild(
                new LineChartSeries(
                    new Index { Val = (uint)seriesIdx },
                    new Order { Val = (uint)seriesIdx },
                    warningText,
                    new ChartShapeProperties(
                        new DocumentFormat.OpenXml.Drawing.Outline(
                            new SolidFill(new RgbColorModelHex { Val = ColorNameToHexString(seriesColor) }),
                            new PresetDash { Val = PresetLineDashValues.Dash },
                            new Round())
                        {
                            Width = 22225,
                            CapType = LineCapValues.Round
                        },
                        new EffectList()),
                    new Marker(
                        new Symbol { Val = MarkerStyleValues.None }),
                    categoryAxisData,
                    values,
                    new Smooth { Val = false }));
        }

        private void AppendScatterChartSingleSeries(ScatterChart scatterChart, System.Data.DataTable data, string xFormatCodeText, string sheetTitle,
            int seriesIdx, string seriesText, double seriesValue, string seriesColor)
        {
            SeriesText warningText = new SeriesText(
                new StringReference(
                    new Charts.Formula { Text = String.Format("'{0}'!{1}1", sheetTitle, GetExcelColumnName(seriesIdx + 2)) }, // +2: empty column between last data and limits
                    new StringCache(
                        new PointCount { Val = (UInt32Value)1U },
                        new StringPoint(
                            new NumericValue { Text = seriesText })
                        {
                            Index = (UInt32Value)0U
                        })));

            NumberingCache cacheX = new NumberingCache(
                new FormatCode { Text = xFormatCodeText },
                new PointCount { Val = (UInt32Value)Convert.ToUInt32(data.Rows.Count) }
            );
            uint k = 0;
            foreach (DataRow row in data.Rows)
            {
                NumericPoint np = new NumericPoint(
                    new NumericValue { Text = ((DateTime)row[0]).ToOADate().ToString(CultureInfo.InvariantCulture) })
                {
                    Index = k
                };
                cacheX.AppendChild(np);
                k++;
            }
            NumberReference nrX = new NumberReference(
                new Charts.Formula { Text = String.Format("'{0}'!A2:A{1}", sheetTitle, data.Rows.Count + 1) },
                cacheX);
            XValues xValues = new XValues(nrX);

            NumberingCache cache = new NumberingCache(
                new FormatCode { Text = "General" },
                new PointCount { Val = Convert.ToUInt32(data.Rows.Count) });
            for (uint i = 0; i < data.Rows.Count; i++)
            {
                NumericPoint np = new NumericPoint(
                    new NumericValue
                    {
                        Text = (i == 0 || i == data.Rows.Count - 1)
                            ? seriesValue.ToString(CultureInfo.InvariantCulture)
                            : "#N/A"
                    })
                {
                    Index = i
                };
                cache.AppendChild(np);
            }
            string s = String.Format("'{0}'!{1}2:{1}{2}", sheetTitle, GetExcelColumnName(seriesIdx + 2), data.Rows.Count + 1);
            NumberReference nr = new NumberReference(
                new Charts.Formula { Text = s },
                cache);
            YValues yValues = new YValues(nr);

            ScatterChartSeries scatterChartSeries = scatterChart.AppendChild(
                new ScatterChartSeries(
                    new Index { Val = (uint)seriesIdx },
                    new Order { Val = (uint)seriesIdx },
                    warningText,
                    new ChartShapeProperties(
                        new DocumentFormat.OpenXml.Drawing.Outline(
                            new SolidFill(new RgbColorModelHex { Val = ColorNameToHexString(seriesColor) }),
                            new PresetDash { Val = PresetLineDashValues.Dash },
                            new Round())
                        {
                            Width = 22225,
                            CapType = LineCapValues.Round
                        },
                        new EffectList()),
                    new Marker(
                        new Symbol { Val = MarkerStyleValues.None }),
                    xValues,
                    yValues,
                    new Smooth { Val = false }));
        }


        private string GetSortOrderString(OrderBy orderBy,
            Tuple<string, Column, List<Tuple<int, string, string, string, string>>,
                List<Tuple<string, int, string, string>>, bool>[] columnsArray, out string sortDirection,
            out List<int> sortColumnIdList)
        {
            string sortOrder = String.Empty;
            sortColumnIdList = new List<int>();
            sortDirection = "";

            if (orderBy != null)
            {
                orderBy.FirstColumn();
                do
                {
                    Column column = RecordManager.ets_Column_Details(orderBy.ColumnId);
                    if (column != null)
                    {
                        string columnName = column.DisplayName;
                        for (int i = 0; i < columnsArray.Length; i++)
                        {
                            if (column.ColumnID == columnsArray[i].Item2.ColumnID)
                            {
                                columnName = columnsArray[i].Item1;
                                break;
                            }
                        }

                        if ((column.ColumnType == "number") || (column.ColumnType == "calculation"))
                        {
                            sortOrder += String.Format("CONVERT(decimal(38,2), dbo.RemoveNonNumericChar([{0}])) ",
                                columnName);
                        }
                        else if ((column.ColumnType == "date") || (column.ColumnType == "datetime") ||
                                 (column.ColumnType == "time"))
                        {
                            sortOrder += String.Format("CONVERT(datetime, [dbo].[fnRemoveNonDate]([{0}]), 103) ",
                                columnName);
                        }
                        else
                        {
                            sortOrder += String.Format("[{0}] ", columnName);
                        }

                        sortOrder += String.Format("{0}, ", orderBy.Ascending ? "ASC" : "DESC");

                        if (column.ColumnID.HasValue)
                        {
                            if (!sortColumnIdList.Contains(column.ColumnID.Value))
                                sortColumnIdList.Add(column.ColumnID.Value);
                        }
                    }
                } while (orderBy.NextColumn());

                sortOrder += "DBGSystemRecordID ASC";
            }
            else
            {
                sortOrder = "DBGSystemRecordID";
                sortDirection = "DESC";
            }
            return sortOrder;
        }

        private double GetWidth(string font, int fontSize, string text)
        {
            System.Drawing.Font stringFont = new System.Drawing.Font(font, fontSize, System.Drawing.FontStyle.Bold);
            return GetWidth(stringFont, text);
        }

        private double GetWidth(System.Drawing.Font stringFont, string text)
        {
            // This formula is based on this article plus a nudge ( + 0.2M )
            // http://msdn.microsoft.com/en-us/library/documentformat.openxml.spreadsheet.column.width.aspx
            // Truncate(((256 * Solve_For_This + Truncate(128 / 7)) / 256) * 7) = DeterminePixelsOfString

            System.Drawing.Size textSize = System.Windows.Forms.TextRenderer.MeasureText(text, stringFont);
            double width = (textSize.Width / 7d * 256d - 128d / 7d) / 256d;
            width = (double)decimal.Round((decimal)width + 1m, 2);

            return width;
        }

        private void InsertColumnXmlNode(Column column, string text, XmlDocument headerXml)
        {
            string strParentJoinColumnName = "";
            string strChildJoinColumnName = "";
            string strParentTableId = "";
            string strShowViewLink = "";
            string strFieldsToShow = "";

            if (column.TableTableID != null && column.DisplayColumn != "" &&
                (column.ColumnType == "dropdown" || column.ColumnType == "listbox"))
            {
                strParentTableId = column.TableTableID.ToString();

                if (column.LinkedParentColumnID != null & (int)column.TableTableID != -1)
                {
                    Column theLinkedParentColumn = RecordManager.ets_Column_Details(((int)column.LinkedParentColumnID));
                    strParentJoinColumnName = theLinkedParentColumn.SystemName;
                    strChildJoinColumnName = column.SystemName;
                    strFieldsToShow = RecordManager.fnReplaceDisplayColumns(column.DisplayColumn, (int)column.TableTableID, column.ColumnID);
                    strShowViewLink = column.ShowViewLink;
                }
                else
                {
                    if ((int)column.TableTableID == -1)
                    {
                        strFieldsToShow = column.DisplayColumn;
                    }
                }
            }

            XmlNode columnNode = headerXml.CreateNode(XmlNodeType.Element, "Records", null);
            XmlNode node = null;

            node = headerXml.CreateNode(XmlNodeType.Element, "ColumnID", null);
            node.InnerText = column.ColumnID.ToString();
            columnNode.AppendChild(node);
            node = headerXml.CreateNode(XmlNodeType.Element, "DisplayText", null);
            node.InnerText =text;
            columnNode.AppendChild(node);
            node = headerXml.CreateNode(XmlNodeType.Element, "SystemName", null);
            node.InnerText = column.SystemName;
            columnNode.AppendChild(node);
            node = headerXml.CreateNode(XmlNodeType.Element, "FieldsToShow", null);
            node.InnerText = strFieldsToShow;
            columnNode.AppendChild(node);
            node = headerXml.CreateNode(XmlNodeType.Element, "ParentTableID", null);
            node.InnerText = strParentTableId;
            columnNode.AppendChild(node);
            node = headerXml.CreateNode(XmlNodeType.Element, "ParentJoinColumnName", null);
            node.InnerText = strParentJoinColumnName;
            columnNode.AppendChild(node);
            node = headerXml.CreateNode(XmlNodeType.Element, "ChildJoinColumnName", null);
            node.InnerText = strChildJoinColumnName;
            columnNode.AppendChild(node);
            node = headerXml.CreateNode(XmlNodeType.Element, "ShowViewLink", null);
            node.InnerText = strShowViewLink;
            columnNode.AppendChild(node);
            node = headerXml.CreateNode(XmlNodeType.Element, "ColumnType", null);
            node.InnerText = column.ColumnType;
            columnNode.AppendChild(node);

            columnNode.AppendChild(node);
            if (headerXml.DocumentElement != null)
                headerXml.DocumentElement.AppendChild(columnNode);
        }

        private List<Tuple<int, string, string, string, string>> GetCellColourRules(int columnId, List<int>controllingColumnIdList)
        {
            List<Tuple<int, string, string, string, string>> listCellColourRules =
                new List<Tuple<int, string, string, string, string>>();

            System.Data.DataTable dtCellColourRules = Common.DataTableFromText("SELECT * FROM [ColumnColour] WHERE [Context]='columnid' AND [ID] = " + columnId + "");
            if (dtCellColourRules != null)
            {
                foreach (DataRow row in dtCellColourRules.Rows)
                {
                    int controllingColumnId = (int) row["ControllingColumnID"];
                    Column column = RecordManager.ets_Column_Details(controllingColumnId);
                    if (column != null)
                    {
                        string s = (string) row["Value"];
                        int parentRecordId = 0;
                        if (column.ColumnType == "dropdown" && column.DropDownType == "tabledd" &&
                            column.LinkedParentColumnID.HasValue && int.TryParse(s, out parentRecordId))
                        {
                            System.Data.DataTable dtTableTableColumn = Common.DataTableFromText("SELECT SystemName, DisplayName FROM [Column] " +
                                "WHERE TableID = " + column.TableTableID + " AND " +
                                "[DisplayName] LIKE '" + column.DisplayColumn.Replace("[", "").Replace("]", "") + "'");
                            if (dtTableTableColumn.Rows.Count > 0)
                            {
                                Record parentRecord = RecordManager.ets_Record_Detail_Full(parentRecordId, null, false);
                                if (parentRecord != null)
                                    s = RecordManager.GetRecordValue(ref parentRecord, dtTableTableColumn.Rows[0]["SystemName"].ToString());
                            }
                        }
                        Tuple<int, string, string, string, string> rule =
                            new Tuple<int, string, string, string, string>(
                                controllingColumnId, (string) row["Operator"], s,
                                (string) row["Colour"], column.ColumnType);
                        listCellColourRules.Add(rule);
                        if (!controllingColumnIdList.Contains(controllingColumnId))
                            controllingColumnIdList.Add(controllingColumnId);
                    }
                }
            }

            return listCellColourRules;
        }


        private string GetCellColour(string cell, List<Tuple<int, string, string, string, string>> rules, DataRow row, Dictionary<int, string> controllingColumnMap)
        {
            string ret = String.Empty;

            foreach (Tuple<int, string, string, string, string> rule in rules)
            {
                bool ruleResult = false;

                if (controllingColumnMap.ContainsKey(rule.Item1))
                {
                    string sControllingColumnValue = row[controllingColumnMap[rule.Item1]].ToString();
                    string sOperator = rule.Item2;
                    string sRuleValue = rule.Item3;
                    string sColour = rule.Item4;
                    string sColumnType = rule.Item5;

                    bool boolResult = false;
                    int intResult = 0;
                    bool isEmptyValue = false;

                    switch (sColumnType)
                    {
                        case "number":
                            if (!String.IsNullOrEmpty(sOperator))
                            {
                                if (sOperator == "empty")
                                    ruleResult = String.IsNullOrEmpty(sControllingColumnValue);
                                else if (sOperator == "notempty")
                                    ruleResult = !String.IsNullOrEmpty(sControllingColumnValue);
                                else if ((sOperator == "contains") || (sOperator == "notcontains"))
                                {
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(sControllingColumnValue) &&
                                        !String.IsNullOrEmpty(sRuleValue))
                                    {
                                        decimal dControllingColumnValue = 0m;
                                        decimal dRuleValue = 0m;
                                        if (decimal.TryParse(sControllingColumnValue, out dControllingColumnValue) &&
                                            decimal.TryParse(sRuleValue, out dRuleValue))
                                        {
                                            switch (sOperator)
                                            {
                                                case "equals":
                                                    ruleResult = dControllingColumnValue == dRuleValue;
                                                    break;
                                                case "notequal":
                                                    ruleResult = dControllingColumnValue != dRuleValue;
                                                    break;
                                                case "greaterthan":
                                                    ruleResult = dControllingColumnValue > dRuleValue;
                                                    break;
                                                case "greaterthanequal":
                                                    ruleResult = dControllingColumnValue >= dRuleValue;
                                                    break;
                                                case "lessthan":
                                                    ruleResult = dControllingColumnValue < dRuleValue;
                                                    break;
                                                case "lessthanequal":
                                                    ruleResult = dControllingColumnValue <= dRuleValue;
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        default:
                            if (!String.IsNullOrEmpty(sOperator))
                            {
                                if ((sOperator == "empty") || (sOperator == "notempty"))
                                    boolResult = String.IsNullOrEmpty(sControllingColumnValue);
                                else if ((sOperator == "contains") || (sOperator == "notcontains"))
                                {
                                    if (String.IsNullOrEmpty(sControllingColumnValue))
                                        isEmptyValue = true;
                                    else
                                        boolResult = sControllingColumnValue.Contains(sRuleValue);
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(sControllingColumnValue))
                                        isEmptyValue = true;
                                    else
                                        intResult = String.Compare(sControllingColumnValue, sRuleValue,
                                            StringComparison.OrdinalIgnoreCase);
                                }

                                if (!isEmptyValue)
                                {
                                    switch (sOperator)
                                    {
                                        case "equals":
                                            ruleResult = intResult == 0;
                                            break;
                                        case "notequal":
                                            ruleResult = intResult != 0;
                                            break;
                                        case "greaterthan":
                                            ruleResult = intResult > 0;
                                            break;
                                        case "greaterthanequal":
                                            ruleResult = intResult >= 0;
                                            break;
                                        case "lessthan":
                                            ruleResult = intResult < 0;
                                            break;
                                        case "lessthanequal":
                                            ruleResult = intResult <= 0;
                                            break;
                                        case "contains":
                                            ruleResult = boolResult;
                                            break;
                                        case "notcontains":
                                            ruleResult = !boolResult;
                                            break;
                                        case "empty":
                                            ruleResult = boolResult;
                                            break;
                                        case "notempty":
                                            ruleResult = !boolResult;
                                            break;
                                    }
                                }
                            }
                            break;
                    }
                    if (ruleResult)
                    {
                        ret = sColour;
                        break;
                    }
                }
            }

            return ret;
        }


        private List<Tuple<string, int, string, string>> GetCellCheckRules(int columnId, List<int> controllingColumnIdList)
        {
            List<Tuple<string, int, string, string>> listCellCheckRules =
                new List<Tuple<string, int, string, string>>();

            bool isAdvancedWarning = false;
            bool isAdvancedExceedance = false;
            System.Data.DataTable dtCellCheckRules = Common.DataTableFromText("SELECT * FROM [Condition] WHERE [ColumnID] = " + columnId + "");
            if (dtCellCheckRules != null)
            {
                foreach (DataRow row in dtCellCheckRules.Rows)
                {
                    int checkColumnId = (int)row["CheckColumnID"];
                    Column column = RecordManager.ets_Column_Details(checkColumnId);
                    if (column != null)
                    {
                        string s = (string)row["CheckValue"];
                        int parentRecordId = 0;
                        if (column.ColumnType == "dropdown" && column.DropDownType == "tabledd" &&
                            column.LinkedParentColumnID.HasValue && int.TryParse(s, out parentRecordId))
                        {
                            System.Data.DataTable dtTableTableColumn = Common.DataTableFromText("SELECT SystemName, DisplayName FROM [Column] " +
                                                                                                "WHERE TableID = " + column.TableTableID + " AND " +
                                                                                                "[DisplayName] LIKE '" + column.DisplayColumn.Replace("[", "").Replace("]", "") + "'");
                            if (dtTableTableColumn.Rows.Count > 0)
                            {
                                Record parentRecord = RecordManager.ets_Record_Detail_Full(parentRecordId, null, false);
                                s = RecordManager.GetRecordValue(ref parentRecord, dtTableTableColumn.Rows[0]["SystemName"].ToString());
                            }
                        }
                        if (row["ConditionType"].ToString() == "W")
                            isAdvancedWarning = true;
                        else if (row["ConditionType"].ToString() == "E")
                            isAdvancedExceedance = true;

                        Tuple<string, int, string, string> rule =
                            new Tuple<string, int, string, string>(
                                (string)row["ConditionType"], checkColumnId, s,
                                (string)row["CheckFormula"]);
                        listCellCheckRules.Add(rule);
                        if (!controllingColumnIdList.Contains(checkColumnId))
                            controllingColumnIdList.Add(checkColumnId);
                    }
                }

                if (!isAdvancedWarning || !isAdvancedExceedance)
                {
                    Column column = RecordManager.ets_Column_Details(columnId);
                    if (!isAdvancedWarning)
                    {
                        Tuple<string, int, string, string> rule =
                            new Tuple<string, int, string, string>(
                                "W", 0, "", column.ValidationOnWarning);
                        listCellCheckRules.Add(rule);
                    }
                    if (!isAdvancedExceedance)
                    {
                        Tuple<string, int, string, string> rule =
                            new Tuple<string, int, string, string>(
                                "E", 0, "", column.ValidationOnExceedance);
                        listCellCheckRules.Add(rule);
                    }
                }
            }

            return listCellCheckRules;
        }

        private string GetCellComments(string cell, string comment, bool isWarning,
            List<Tuple<string, int, string, string>> rules, DataRow row, Dictionary<int, string> controllingColumnMap)
        {
            string ret = comment;

            string formula = String.Empty;
            foreach (Tuple<string, int, string, string> rule in rules)
            {
                if (rule.Item1 == (isWarning ? "W" : "E") && rule.Item2 == 0)
                {
                    formula = rule.Item4;
                    break;
                }
                else
                {
                    if (controllingColumnMap.ContainsKey(rule.Item2))
                    {
                        string sControllingColumnValue = row[controllingColumnMap[rule.Item2]].ToString();
                        if (rule.Item1 == (isWarning ? "W" : "E") && sControllingColumnValue == rule.Item3)
                        {
                            formula = rule.Item4;
                            break;
                        }
                    }
                }
            }
            if (!String.IsNullOrEmpty(formula))
            {
                string sMin = Common.GetMinFromFormula(formula);
                string sMax = Common.GetMaxFromFormula(formula);
                int iTotalValue = Common.GetNumberOfValue(formula);
                bool bAdvanced = false;

                if (iTotalValue > 2)
                {
                    bAdvanced = true;
                }
                else
                {
                    if (iTotalValue == 2)
                    {
                        if (String.IsNullOrEmpty(sMin) || String.IsNullOrEmpty(sMax))
                        {
                            bAdvanced = true;
                        }
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(sMin) && String.IsNullOrEmpty(sMax))
                        {
                            bAdvanced = true;
                        }
                    }
                }

                ret = cell;

                if (bAdvanced)
                {
                    if (String.IsNullOrEmpty(formula))
                    {
                        ret += " (not set).";
                    }
                    else
                    {
                        ret += " is outside accepted range (" + formula + ").";
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(sMin) && !String.IsNullOrEmpty(sMax))
                    {
                        ret += " is less than " + sMin + " or greater than " + sMax;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(sMin))
                        {
                            ret += " is less than " + sMin;
                        }
                        if (!String.IsNullOrEmpty(sMax))
                        {
                            ret += " is greater than " + sMax;
                        }
                    }
                }
            }

            return ret;
        }

        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (dividend - modulo) / 26;
            }

            return columnName;
        }

        private SchemeColor GetSchemeColor(SchemeColor schemeColor, int seriesNumber)
        {
            switch (seriesNumber % 6)
            {
                case 1:
                    schemeColor.Val = SchemeColorValues.Accent1;
                    break;
                case 2:
                    schemeColor.Val = SchemeColorValues.Accent2;
                    break;
                case 3:
                    schemeColor.Val = SchemeColorValues.Accent3;
                    break;
                case 4:
                    schemeColor.Val = SchemeColorValues.Accent4;
                    break;
                case 5:
                    schemeColor.Val = SchemeColorValues.Accent5;
                    break;
                case 0:
                    schemeColor.Val = SchemeColorValues.Accent6;
                    break;
            }

            if (seriesNumber > 6)
            {
                int n = seriesNumber / 6;
                int modulation = 0;
                int offset = 0;
                switch (n)
                {
                    case 1:
                        modulation = 60000;
                        break;
                    case 2:
                        modulation = 80000;
                        offset = 20000;
                        break;
                    default:
                        modulation = 60000;
                        offset = 40000;
                        break;
                }
                LuminanceModulation luminanceModulation = new LuminanceModulation() { Val = modulation };
                schemeColor.AppendChild(luminanceModulation);
                if (offset != 0)
                {
                    LuminanceOffset luminanceOffset = new LuminanceOffset() { Val = offset };
                    schemeColor.AppendChild(luminanceOffset);
                }
            }

            return schemeColor;
        }

        private string ColorNameToHexString(string color)
        {
            string hex = "000000";
            switch (color.ToLower())
            {
                case "aqua":
                    hex = "00FFFF";
                    break;
                case "black":
                    hex = "000000";
                    break;
                case "blue":
                    hex = "0000FF";
                    break;
                case "fuchsia":
                    hex = "FF00FF";
                    break;
                case "gray":
                    hex = "808080";
                    break;
                case "green":
                    hex = "00FF00";
                    break;
                case "lime":
                    hex = "BFFF00";
                    break;
                case "maroon":
                    hex = "800000";
                    break;
                case "navy":
                    hex = "000080";
                    break;
                case "olive":
                    hex = "808000";
                    break;
                case "orange":
                    hex = "FF7F00";
                    break;
                case "purple":
                    hex = "9F00C5";
                    break;
                case "red":
                    hex = "FF0000";
                    break;
                case "silver":
                    hex = "C0C0C0";
                    break;
                case "teal":
                    hex = "008080";
                    break;
                case "yellow":
                    hex = "FFFF00";
                    break;
            }
            return hex;
        }

        private string RemoveNonNumeric(string s)
        {
            if (String.IsNullOrEmpty(s))
                return s;
            else
            {
                s = s.Trim();
                string neg = "";
                if (s[0] == '-')
                    neg = "-";
                return neg + new string(s.Where(c => char.IsDigit(c) || c == '.').ToArray());
            }
        }

        private bool ContainsNonNumeric(string s)
        {
            if (String.IsNullOrEmpty(s))
                return false;
            else
            {
                s = s.Trim();
                if (s[0] == '-')
                    s = s.Substring(1);
                return s.Any(c => !char.IsDigit(c) && c != '.');
            }
        }

        private void InsertDiagnosticInfo(SheetData sheetData, int tableId, string sortOrder, string sortDirection,
            string whereClauseString, XmlDocument headerXml, Exception exParam)
        {
            Row r1 = sheetData.AppendChild(new Row());
            Cell cell1 = new Cell
            {
                CellValue = new CellValue("Cannot get data. The diagnostic info is below:"),
                DataType = new EnumValue<CellValues>(CellValues.String)
            };
            r1.AppendChild(cell1);
            Row r2 = sheetData.AppendChild(new Row());
            Cell cell2 = new Cell
            {
                CellValue = new CellValue(String.Format("ID {0}", tableId)),
                DataType = new EnumValue<CellValues>(CellValues.String)
            };
            r2.AppendChild(cell2);
            Row r3 = sheetData.AppendChild(new Row());
            Cell cell3 = new Cell
            {
                CellValue = new CellValue(sortOrder + "; " + sortDirection),
                DataType = new EnumValue<CellValues>(CellValues.String)
            };
            r3.AppendChild(cell3);
            Row r4 = sheetData.AppendChild(new Row());
            Cell cell4 = new Cell
            {
                CellValue = new CellValue(whereClauseString),
                DataType = new EnumValue<CellValues>(CellValues.String)
            };
            r4.AppendChild(cell4);
            Row r5 = sheetData.AppendChild(new Row());
            Cell cell5 = new Cell
            {
                CellValue = new CellValue(headerXml.InnerXml),
                DataType = new EnumValue<CellValues>(CellValues.String)
            };
            r5.AppendChild(cell5);
            Row r6 = sheetData.AppendChild(new Row());
            Cell cell6 = new Cell
            {
                CellValue = new CellValue(exParam == null ? "null" : exParam.Message),
                DataType = new EnumValue<CellValues>(CellValues.String)
            };
            r6.AppendChild(cell6);
        }

        private void InsertDiagnosticInfo(SheetData sheetData, int tableId, int reportId)
        {
            Row r1 = sheetData.AppendChild(new Row());
            Cell cell1 = new Cell
            {
                CellValue = new CellValue("Cannot get data. The diagnostic info is below:"),
                DataType = new EnumValue<CellValues>(CellValues.String)
            };
            r1.AppendChild(cell1);
            Row r2 = sheetData.AppendChild(new Row());
            Cell cell2 = new Cell
            {
                CellValue = new CellValue(String.Format("ID {0}", tableId)),
                DataType = new EnumValue<CellValues>(CellValues.String)
            };
            r2.AppendChild(cell2);
            Row r3 = sheetData.AppendChild(new Row());
            Cell cell3 = new Cell
            {
                CellValue = new CellValue(String.Format("Report {0}", reportId)),
                DataType = new EnumValue<CellValues>(CellValues.String)
            };
            r3.AppendChild(cell3);
        }

        internal void CreateInfoWorksheet(string sheetName, string text)
        {
            WorksheetPart wsp = _spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();
            wsp.Worksheet = new Worksheet();
            SheetData sheetData = wsp.Worksheet.AppendChild(new SheetData());

            using (System.IO.StringReader reader = new System.IO.StringReader(text))
            {
                string line = reader.ReadLine();
                Row r = sheetData.AppendChild(new Row());
                Cell cell = new Cell
                {
                    CellValue = new CellValue(line),
                    DataType = new EnumValue<CellValues>(CellValues.String)
                };
                r.AppendChild(cell);
            }

            wsp.Worksheet.Save();

            // create the worksheet to workbook relation
            Sheets sheets = _spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>();
            string relationshipId = _spreadSheet.WorkbookPart.GetIdOfPart(wsp);
            uint sheetId = 1;
            if (sheets.Elements<Sheet>().Any())
            {
                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }
            Sheet sheet = new Sheet
            {
                Id = relationshipId,
                SheetId = sheetId,
                Name = sheetName
            };
            sheets.AppendChild(sheet);
        }
    }

    public class OrderBy
    {
        private class OrderByColumn
        {
            public int ColumnId { get; private set; }
            public bool Ascending { get; private set; }
            public OrderByColumn Next { get; private set; }

            public OrderByColumn(int columnId, bool ascending)
            {
                ColumnId = columnId;
                Ascending = ascending;
                Next = null;
            }

            public void Append(OrderByColumn nextColumn)
            {
                Next = nextColumn;
            }
        }

        private readonly OrderByColumn _root;
        private OrderByColumn _last;
        private OrderByColumn _current;

        public int ColumnId
        {
            get { return _current.ColumnId; }
        }

        public bool Ascending
        {
            get { return _current.Ascending; }
        }

        public OrderBy(int columnId)
        {
            _root = new OrderByColumn(columnId, true);
            _last = _root;
        }

        public OrderBy(int columnId, bool ascending)
        {
            _root = new OrderByColumn(columnId, ascending);
            _last = _root;
        }

        public void ThenBy(int columnId)
        {
            OrderByColumn element = new OrderByColumn(columnId, true);
            _last.Append(element);
            _last = element;
        }

        public void ThenBy(int columnId, bool ascending)
        {
            OrderByColumn element = new OrderByColumn(columnId, ascending);
            _last.Append(element);
            _last = element;
        }

        public void FirstColumn()
        {
            _current = _root;
        }

        public bool NextColumn()
        {
            if (_current.Next == null)
                return false;
            else
            {
                _current = _current.Next;
                return true;
            }
        }
    }

    public class WhereClause
    {
        private class Node
        {
            public int ColumnId { get; private set; }
            public string Operator { get; private set; }
            public string StringValue { get; private set; }
            public DateTime DateTimeValue { get; private set; }
            public WhereClause SubClause { get; private set; }
            public string SubClauseString { get; private set; }
            public string JoinOperator { get; private set; }

            public Node(int columnId, string op, string value)
            {
                ColumnId = columnId;
                Operator = op;
                StringValue = value;
                SubClause = null;
                SubClauseString = string.Empty;
                JoinOperator = "";
            }

            public Node(int columnId, string op, string value, string joinOp)
            {
                ColumnId = columnId;
                Operator = op;
                StringValue = value;
                SubClause = null;
                SubClauseString = string.Empty;
                JoinOperator = joinOp;
            }

            public Node(int columnId, string op, DateTime value)
            {
                ColumnId = columnId;
                Operator = op;
                StringValue = string.Empty;
                DateTimeValue = value;
                SubClause = null;
                SubClauseString = string.Empty;
                JoinOperator = "";
            }

            public Node(int columnId, string op, DateTime value, string joinOp)
            {
                ColumnId = columnId;
                Operator = op;
                StringValue = string.Empty;
                DateTimeValue = value;
                SubClause = null;
                SubClauseString = string.Empty;
                JoinOperator = joinOp;
            }

            public Node(WhereClause subClause)
            {
                ColumnId = 0;
                Operator = string.Empty;
                SubClause = subClause;
                SubClauseString = string.Empty;
                JoinOperator = string.Empty;
            }

            public Node(WhereClause subClause, string joinOp)
            {
                ColumnId = 0;
                Operator = string.Empty;
                SubClause = subClause;
                SubClauseString = string.Empty;
                JoinOperator = joinOp;
            }

            public Node(string subClauseString)
            {
                ColumnId = 0;
                Operator = string.Empty;
                SubClause = null;
                SubClauseString = subClauseString;
                JoinOperator = string.Empty;
            }

            public Node(string subClauseString, string joinOp)
            {
                ColumnId = 0;
                Operator = string.Empty;
                SubClause = null;
                SubClauseString = subClauseString;
                JoinOperator = joinOp;
            }
        }

        private readonly List<Node> _nodes;

        public WhereClause(int columnId, string op, string value)
        {
            Node node = new Node(columnId, op, value);
            _nodes = new List<Node> { node };
        }

        public WhereClause(int columnId, string op, DateTime value)
        {
            Node node = new Node(columnId, op, value);
            _nodes = new List<Node> { node };
        }

        public WhereClause(WhereClause subClause)
        {
            Node node = new Node(subClause);
            _nodes = new List<Node> { node };
        }

        public WhereClause(string subClauseString)
        {
            Node node = new Node(subClauseString);
            _nodes = new List<Node> { node };
        }

        public void And(int columnId, string op, string value)
        {
            Node node = new Node(columnId, op, value, "and");
            _nodes.Add(node);
        }

        public void And(int columnId, string op, DateTime value)
        {
            Node node = new Node(columnId, op, value, "and");
            _nodes.Add(node);
        }

        public void And(WhereClause subClause)
        {
            Node node = new Node(subClause, "and");
            _nodes.Add(node);
        }

        public void And(string subClauseString)
        {
            Node node = new Node(subClauseString, "and");
            _nodes.Add(node);
        }

        public void Or(int columnId, string op, string value)
        {
            Node node = new Node(columnId, op, value, "or");
            _nodes.Add(node);
        }

        public void Or(int columnId, string op, DateTime value)
        {
            Node node = new Node(columnId, op, value, "or");
            _nodes.Add(node);
        }

        public void Or(WhereClause subClause)
        {
            Node node = new Node(subClause, "or");
            _nodes.Add(node);
        }

        public void Or(string subClauseString)
        {
            Node node = new Node(subClauseString, "or");
            _nodes.Add(node);
        }

        public string GetWhereClauseString()
        {
            string filterString = string.Empty;

            foreach (Node node in _nodes)
            {
                if (!String.IsNullOrEmpty(filterString))
                {
                    if (String.IsNullOrEmpty(node.JoinOperator))
                        break;
                    else
                    {
                        filterString += " " + node.JoinOperator + " ";
                    }
                }

                if (node.SubClause != null)
                {
                    filterString += "(" + node.SubClause.GetWhereClauseString() + ")";
                }
                else if (!String.IsNullOrEmpty(node.SubClauseString))
                {
                    filterString += "(" + node.SubClauseString + ")";
                }
                else
                {
                    if (String.IsNullOrEmpty(node.Operator))
                        break;

                    Column column = RecordManager.ets_Column_Details(node.ColumnId);
                    if (column != null)
                    {
                        switch (column.ColumnType)
                        {
                            case "number":
                                if (node.Operator == "empty")
                                    filterString += String.Format("([Record].[{0}] IS NULL OR LEN([Record].[{0}]) = 0)",
                                        column.SystemName);
                                else if (node.Operator == "notempty")
                                    filterString += String.Format("LEN([Record].[{0}]) > 0", column.SystemName);
                                else if ((node.Operator == "contains") || (node.Operator == "notcontains"))
                                {
                                }
                                else
                                {
                                    decimal decimalValue;
                                    if (decimal.TryParse(node.StringValue, out decimalValue))
                                    {
                                        switch (node.Operator)
                                        {
                                            case "equals":
                                                filterString +=
                                                    String.Format(
                                                        "CONVERT(decimal (38, 18), dbo.RemoveNonNumericChar([Record].[{0}])) = {1}",
                                                        column.SystemName, decimalValue);
                                                break;
                                            case "notequal":
                                                filterString +=
                                                    String.Format(
                                                        "CONVERT(decimal (38, 18), dbo.RemoveNonNumericChar([Record].[{0}])) <> {1}",
                                                        column.SystemName, decimalValue);
                                                break;
                                            case "greaterthan":
                                                filterString +=
                                                    String.Format(
                                                        "CONVERT(decimal (38, 18), dbo.RemoveNonNumericChar([Record].[{0}])) > {1}",
                                                        column.SystemName, decimalValue);
                                                break;
                                            case "greaterthanequal":
                                                filterString +=
                                                    String.Format(
                                                        "CONVERT(decimal (38, 18), dbo.RemoveNonNumericChar([Record].[{0}])) >= {1}",
                                                        column.SystemName, decimalValue);
                                                break;
                                            case "lessthan":
                                                filterString +=
                                                    String.Format(
                                                        "CONVERT(decimal (38, 18), dbo.RemoveNonNumericChar([Record].[{0}])) < {1}",
                                                        column.SystemName, decimalValue);
                                                break;
                                            case "lessthanequal":
                                                filterString +=
                                                    String.Format(
                                                        "CONVERT(decimal (38, 18), dbo.RemoveNonNumericChar([Record].[{0}])) <= {1}",
                                                        column.SystemName, decimalValue);
                                                break;
                                        }
                                    }
                                }
                                break;
                            case "date":
                                if (column.SystemName == "DateTimeRecorded")
                                {
                                    if (node.Operator == "empty")
                                        filterString += "[Record].[DateTimeRecorded] IS NULL";
                                    else if (node.Operator == "notempty")
                                        filterString += "[Record].[DateTimeRecorded] IS NOT NULL";
                                    else if ((node.Operator == "contains") || (node.Operator == "notcontains"))
                                    {
                                    }
                                    else
                                    {
                                        DateTime dateTimeValue;
                                        bool isDateTimeValid = true;
                                        if (String.IsNullOrEmpty(node.StringValue))
                                            dateTimeValue = node.DateTimeValue;
                                        else
                                            isDateTimeValid = DateTime.TryParse(node.StringValue, out dateTimeValue);
                                        if (isDateTimeValid)
                                        {
                                            switch (node.Operator)
                                            {
                                                case "equals":
                                                    filterString +=
                                                        String.Format("[Record].[DateTimeRecorded] = '{0:yyyyMMdd}'",
                                                            dateTimeValue);
                                                    break;
                                                case "notequal":
                                                    filterString +=
                                                        String.Format("[Record].[DateTimeRecorded] <> '{0:yyyyMMdd}'",
                                                            dateTimeValue);
                                                    break;
                                                case "greaterthan":
                                                    filterString +=
                                                        String.Format("[Record].[DateTimeRecorded] > '{0:yyyyMMdd}'",
                                                            dateTimeValue);
                                                    break;
                                                case "greaterthanequal":
                                                    filterString +=
                                                        String.Format("[Record].[DateTimeRecorded] >= '{0:yyyyMMdd}'",
                                                            dateTimeValue);
                                                    break;
                                                case "lessthan":
                                                    filterString +=
                                                        String.Format("[Record].[DateTimeRecorded] < '{0:yyyyMMdd}'",
                                                            dateTimeValue);
                                                    break;
                                                case "lessthanequal":
                                                    filterString +=
                                                        String.Format("[Record].[DateTimeRecorded] <= '{0:yyyyMMdd}'",
                                                            dateTimeValue);
                                                    break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (node.Operator == "empty")
                                        filterString +=
                                            String.Format("([Record].[{0}] IS NULL OR LEN([Record].[{0}]) = 0)",
                                                column.SystemName);
                                    else if (node.Operator == "notempty")
                                        filterString += String.Format("LEN([Record].[{0}]) > 0", column.SystemName);
                                    else if ((node.Operator == "contains") || (node.Operator == "notcontains"))
                                    {
                                    }
                                    else
                                    {
                                        DateTime dateTimeValue;
                                        bool isDateTimeValid = true;
                                        if (String.IsNullOrEmpty(node.StringValue))
                                            dateTimeValue = node.DateTimeValue;
                                        else
                                            isDateTimeValid = DateTime.TryParse(node.StringValue, out dateTimeValue);
                                        if (isDateTimeValid)
                                        {
                                            switch (node.Operator)
                                            {
                                                case "equals":
                                                    filterString +=
                                                        String.Format(
                                                            "CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103) = '{1:yyyyMMdd}'",
                                                            column.SystemName, dateTimeValue);
                                                    break;
                                                case "notequal":
                                                    filterString +=
                                                        String.Format(
                                                            "CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103) <> '{1:yyyyMMdd}'",
                                                            column.SystemName, dateTimeValue);
                                                    break;
                                                case "greaterthan":
                                                    filterString +=
                                                        String.Format(
                                                            "CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103) > '{1:yyyyMMdd}'",
                                                            column.SystemName, dateTimeValue);
                                                    break;
                                                case "greaterthanequal":
                                                    filterString +=
                                                        String.Format(
                                                            "CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103) >= '{1:yyyyMMdd}'",
                                                            column.SystemName, dateTimeValue);
                                                    break;
                                                case "lessthan":
                                                    filterString +=
                                                        String.Format(
                                                            "CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103) < '{1:yyyyMMdd}'",
                                                            column.SystemName, dateTimeValue);
                                                    break;
                                                case "lessthanequal":
                                                    filterString +=
                                                        String.Format(
                                                            "CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103) <= '{1:yyyyMMdd}'",
                                                            column.SystemName, dateTimeValue);
                                                    break;
                                            }
                                        }
                                    }
                                }
                                break;
                            case "datetime":
                                if (column.SystemName == "DateTimeRecorded")
                                {
                                    if (node.Operator == "empty")
                                        filterString += "[Record].[DateTimeRecorded] IS NULL";
                                    else if (node.Operator == "notempty")
                                        filterString += "[Record].[DateTimeRecorded] IS NOT NULL";
                                    else if ((node.Operator == "contains") || (node.Operator == "notcontains"))
                                    {
                                    }
                                    else
                                    {
                                        DateTime dateTimeValue;
                                        bool isDateTimeValid = true;
                                        if (String.IsNullOrEmpty(node.StringValue))
                                            dateTimeValue = node.DateTimeValue;
                                        else
                                            isDateTimeValid = DateTime.TryParse(node.StringValue, out dateTimeValue);
                                        if (isDateTimeValid)
                                        {
                                            switch (node.Operator)
                                            {
                                                case "equals":
                                                    filterString +=
                                                        String.Format("[Record].[DateTimeRecorded] = '{0:yyyyMMdd HH:mm:ss.fff}'",
                                                            dateTimeValue);
                                                    break;
                                                case "notequal":
                                                    filterString +=
                                                        String.Format("[Record].[DateTimeRecorded] <> '{0:yyyyMMdd HH:mm:ss.fff}'",
                                                            dateTimeValue);
                                                    break;
                                                case "greaterthan":
                                                    filterString +=
                                                        String.Format("[Record].[DateTimeRecorded] > '{0:yyyyMMdd HH:mm:ss.fff}'",
                                                            dateTimeValue);
                                                    break;
                                                case "greaterthanequal":
                                                    filterString +=
                                                        String.Format("[Record].[DateTimeRecorded] >= '{0:yyyyMMdd HH:mm:ss.fff}'",
                                                            dateTimeValue);
                                                    break;
                                                case "lessthan":
                                                    filterString +=
                                                        String.Format("[Record].[DateTimeRecorded] < '{0:yyyyMMdd HH:mm:ss.fff}'",
                                                            dateTimeValue);
                                                    break;
                                                case "lessthanequal":
                                                    filterString +=
                                                        String.Format("[Record].[DateTimeRecorded] <= '{0:yyyyMMdd HH:mm:ss.fff}'",
                                                            dateTimeValue);
                                                    break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (node.Operator == "empty")
                                        filterString +=
                                            String.Format("([Record].[{0}] IS NULL OR LEN([Record].[{0}]) = 0)",
                                                column.SystemName);
                                    else if (node.Operator == "notempty")
                                        filterString += String.Format("LEN([Record].[{0}]) > 0", column.SystemName);
                                    else if ((node.Operator == "contains") || (node.Operator == "notcontains"))
                                    {
                                    }
                                    else
                                    {
                                        DateTime dateTimeValue;
                                        bool isDateTimeValid = true;
                                        if (String.IsNullOrEmpty(node.StringValue))
                                            dateTimeValue = node.DateTimeValue;
                                        else
                                            isDateTimeValid = DateTime.TryParse(node.StringValue, out dateTimeValue);
                                        if (isDateTimeValid)
                                        {
                                            switch (node.Operator)
                                            {
                                                case "equals":
                                                    filterString +=
                                                        String.Format(
                                                            "CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103) = '{1:yyyyMMdd HH:mm:ss.fff}'",
                                                            column.SystemName, dateTimeValue);
                                                    break;
                                                case "notequal":
                                                    filterString +=
                                                        String.Format(
                                                            "CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103) <> '{1:yyyyMMdd HH:mm:ss.fff}'",
                                                            column.SystemName, dateTimeValue);
                                                    break;
                                                case "greaterthan":
                                                    filterString +=
                                                        String.Format(
                                                            "CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103) > '{1:yyyyMMdd HH:mm:ss.fff}'",
                                                            column.SystemName, dateTimeValue);
                                                    break;
                                                case "greaterthanequal":
                                                    filterString +=
                                                        String.Format(
                                                            "CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103) >= '{1:yyyyMMdd HH:mm:ss.fff}'",
                                                            column.SystemName, dateTimeValue);
                                                    break;
                                                case "lessthan":
                                                    filterString +=
                                                        String.Format(
                                                            "CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103) < '{1:yyyyMMdd HH:mm:ss.fff}'",
                                                            column.SystemName, dateTimeValue);
                                                    break;
                                                case "lessthanequal":
                                                    filterString +=
                                                        String.Format(
                                                            "CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103) <= '{1:yyyyMMdd HH:mm:ss.fff}'",
                                                            column.SystemName, dateTimeValue);
                                                    break;
                                            }
                                        }
                                    }
                                }
                                break;
                            default:
                                if (node.Operator == "empty")
                                    filterString += String.Format("([Record].[{0}] IS NULL OR LEN([Record].[{0}]) = 0)",
                                        column.SystemName);
                                else if (node.Operator == "notempty")
                                    filterString += String.Format("LEN([Record].[{0}]) > 0", column.SystemName);
                                else
                                {
                                    string stringVlaue = node.StringValue;
                                    switch (node.Operator)
                                    {
                                        case "contains":
                                            filterString +=
                                                String.Format("[Record].[{0}] LIKE '%{1}%'", column.SystemName,
                                                    stringVlaue);
                                            break;
                                        case "notcontains":
                                            filterString +=
                                                String.Format("[Record].[{0}] NOT LIKE '%{1}%'", column.SystemName,
                                                    stringVlaue);
                                            break;
                                        case "equals":
                                            filterString +=
                                                String.Format("[Record].[{0}] = '{1}'", column.SystemName, stringVlaue);
                                            break;
                                        case "notequal":
                                            filterString +=
                                                String.Format("[Record].[{0}] <> '{1}'", column.SystemName,
                                                    stringVlaue);
                                            break;
                                        case "greaterthan":
                                            filterString +=
                                                String.Format("[Record].[{0}] > '{1}'", column.SystemName, stringVlaue);
                                            break;
                                        case "greaterthanequal":
                                            filterString +=
                                                String.Format("[Record].[{0}] >= '{1}'", column.SystemName,
                                                    stringVlaue);
                                            break;
                                        case "lessthan":
                                            filterString +=
                                                String.Format("[Record].[{0}] < '{1}'", column.SystemName, stringVlaue);
                                            break;
                                        case "lessthanequal":
                                            filterString +=
                                                String.Format("[Record].[{0}] <= '{1}'", column.SystemName,
                                                    stringVlaue);
                                            break;
                                    }
                                }
                                break;
                        }
                    }
                }
            }

            return filterString;
        }
    }
}
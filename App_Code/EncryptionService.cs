﻿//using eContractor.Droid.Dependencies;
//using eContractor.Services;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
//using Xamarin.Forms;

//[assembly: Dependency(typeof(EncryptionService))]
//namespace eContractor.Droid.Dependencies
//{
    public class EncryptionService //: IEncryptionService
    {
        private const string EncryptionSalt = "dbg12!12345";
        private const string EncryptionPassword = "dbg12!12345";

        public static string Encrypt(string plainValue)
        {
            var salt = Encoding.ASCII.GetBytes(EncryptionSalt);
            var key = new Rfc2898DeriveBytes(EncryptionPassword, salt);

            var algorithm = new RijndaelManaged();
            var bytesForKey = algorithm.KeySize / 8;
            var bytesForIv = algorithm.BlockSize / 8;
            algorithm.Key = key.GetBytes(bytesForKey);
            algorithm.IV = key.GetBytes(bytesForIv);

            byte[] encryptedBytes;
            using (var encryptor = algorithm.CreateEncryptor(algorithm.Key, algorithm.IV))
            {
                var bytesToEncrypt = Encoding.UTF8.GetBytes(plainValue);
                encryptedBytes = InMemoryCrypt(bytesToEncrypt, encryptor);
            }

            return Convert.ToBase64String(encryptedBytes);
        }

        private static byte[] InMemoryCrypt(byte[] data, ICryptoTransform transform)
        {
            var memory = new MemoryStream();
            using (Stream stream = new CryptoStream(memory, transform, CryptoStreamMode.Write))
            {
                stream.Write(data, 0, data.Length);
            }
            return memory.ToArray();
        }

        public static string Decrypt(string encryptedValue)
        {
            var salt = Encoding.ASCII.GetBytes(EncryptionSalt);
            var key = new Rfc2898DeriveBytes(EncryptionPassword, salt);

            var algorithm = new RijndaelManaged();
            var bytesForKey = algorithm.KeySize / 8;
            var bytesForIv = algorithm.BlockSize / 8;
            algorithm.Key = key.GetBytes(bytesForKey);
            algorithm.IV = key.GetBytes(bytesForIv);

            byte[] descryptedBytes;
            using (var decryptor = algorithm.CreateDecryptor(algorithm.Key, algorithm.IV))
            {
                var encryptedBytes = Convert.FromBase64String(encryptedValue);
                descryptedBytes = InMemoryCrypt(encryptedBytes, decryptor);
            }

            return Encoding.UTF8.GetString(descryptedBytes);
        }
    }
//}
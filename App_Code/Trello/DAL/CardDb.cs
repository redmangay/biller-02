﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Trello.Models;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Text;

namespace Trello.DAL
{
    public class CardDb: IDisposable
    {
        protected SqlConnection connection;

        public CardDb(string connectionString)
        {
            connection = new SqlConnection(connectionString);
        }

        public void SaveMultiple(List<Card> cards)
        {
            if (connection.State == ConnectionState.Closed)
                connection.Open();
            using (SqlTransaction trans = connection.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<Member>));
                    XmlWriterSettings settings = new XmlWriterSettings();
                    // Skip generating <?xml ...>
                    settings.OmitXmlDeclaration = true;                    
                                        
                    string membersXML = "";
                    using (SqlCommand cmd = new SqlCommand("TrelloCard_Save", connection, trans))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        foreach (Card card in cards)
                        {
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@RefId", card.Id);
                            cmd.Parameters.AddWithValue("@BoardRefId", card.IdBoard);
                            cmd.Parameters.AddWithValue("@Name", card.Name);
                            cmd.Parameters.AddWithValue("@Description", card.Desc);
                            cmd.Parameters.AddWithValue("@ShortLink", card.ShortLink);
                            cmd.Parameters.AddWithValue("@Due", card.Due);
                            cmd.Parameters.AddWithValue("@DueComplete", card.DueComplete);
                            cmd.Parameters.AddWithValue("@Closed", card.Closed);
                            cmd.Parameters.AddWithValue("@DateLastActivity", card.DateLastActivity);
                            if (card.Members.Count > 0)
                            {
                                using (StringWriter textWriter = new StringWriter())
                                {
                                    using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
                                    {
                                        serializer.Serialize(xmlWriter, card.Members, null);
                                        membersXML = textWriter.ToString();                                        
                                    }
                                }
                            }
                            else
                            {
                                membersXML = "";
                            }
                            cmd.Parameters.AddWithValue("@MembersXML", membersXML);
                            cmd.ExecuteNonQuery();
                        }
                    }
                    trans.Commit();
                }
                catch(Exception ex)
                {

                }
            }
            connection.Close();
        }

        public void Dispose()
        {
            if (connection != null)
            {
                connection.Dispose();
            }
        }
    }
}
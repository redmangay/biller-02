﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using Trello.Models;

namespace Trello.DAL
{
    public class BoardDb : IDisposable
    {
        protected SqlConnection connection;

        public BoardDb(string connectionString)
        {
            connection = new SqlConnection(connectionString);
        }

        public void Save(Board board)
        {
            connection.Execute("TrelloBoard_Save", new
            {
                RefId = board.Id,
                Name = board.Name,
                ShortLink = board.ShortLink,
                CompanyId = board.CompanyId
            }, commandType: System.Data.CommandType.StoredProcedure);
        }

        public Board Get(string refId)
        {
            return connection.Query<Board>("TrelloBoard_Get", new { RefId = refId }, commandType: System.Data.CommandType.StoredProcedure).FirstOrDefault();
        }

        public List<Board> GetList()
        {
            return connection.Query<Board>("TrelloBoard_GetList", commandType: System.Data.CommandType.StoredProcedure).ToList();
        }

        public void SetDateLastActivity(string refId, DateTime dateLastActivity)
        {
            connection.Execute("UPDATE TrelloBoard SET DateLastActivity = @DateLastActivity WHERE RefId = @RefId", new { DateLastActivity = dateLastActivity, RefId = refId });
        }

        public void Dispose()
        {
            if (connection != null)
            {
                connection.Dispose();
            }
        }
    }
}
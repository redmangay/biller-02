﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using Trello.Models;
using Trello.DAL;
using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace Trello
{
    public class TrelloExtractor
    {        
        string appKey = "700ede6f1b94cb7f92946039de874767"; // Will be moved to web.config later
        string userToken = "287759f1b61259e9e948af5d1dadd755917852875a21aca0edfad492a20205c2"; // Will be moved to web.config later

        //string appKey = ConfigurationManager.AppSettings["Trello_AppKey"];
        //string userToken = ConfigurationManager.AppSettings["Trello_UserToken"];
        string connectionString = DBGurus.strGlobalConnectionString;

        public string Execute(string args, out string output)
        {
            output = "";
            ExtractorSettings settings = null;
            try
            {
                settings = JsonConvert.DeserializeObject<ExtractorSettings>(args);
            }
            catch
            {
                output = "Invalid settings for Trello Extractor";
            }
            if (String.IsNullOrEmpty(output))
            {
                try
                { 
                    appKey = settings.AppKey;
                    userToken = settings.AppToken;
                    List<Board> boards = GetBoards();
                    List<Board> existingBoards = new List<Board>();
                    if (boards != null)
                    {
                        using (BoardDb boardDb = new BoardDb(connectionString))
                        {
                            foreach (Board board in boards)
                            {
                                board.CompanyId = settings.CompanyId;
                                boardDb.Save(board);
                            }

                            existingBoards = boardDb.GetList();
                        }

                        foreach (Board board in boards)
                        {
                            Board existingBoard = existingBoards.FirstOrDefault(b => b.Id == board.Id);
                            if (existingBoard != null && existingBoard.DateLastActivity < board.DateLastActivity)
                            {
                                List<Card> cards = GetCards(board.Id, existingBoard.DateLastActivity);
                                if (cards.Any())
                                {
                                    using (CardDb cardDb = new CardDb(connectionString))
                                    {
                                        cardDb.SaveMultiple(cards);
                                    }
                                }
                                using (BoardDb boardDb = new BoardDb(connectionString))
                                {
                                    boardDb.SetDateLastActivity(board.Id, board.DateLastActivity);
                                }
                                Thread.Sleep(3000);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    output = ex.Message;
                }
            }

            return output;
        }

        private List<Board> GetBoards()
        {
            List<Board> boards = null;
            // Get all boards related to the users            
            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                try
                {
                    string json = client.DownloadString(new Uri(String.Format("https://api.trello.com/1/members/me/boards?fields=name,shortLink,dateLastActivity&key={0}&token={1}", appKey, userToken)));
                    boards = JsonConvert.DeserializeObject<List<Board>>(json);
                }
                catch
                {

                }
            }

            return boards;
        }

        private List<Card> GetCards(string boardId, DateTime since)
        {
            List<Card> cards = null;
            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                try
                {
                    string sinceValue = since.CompareTo(DateTime.MinValue) > 0 ? since.ToString("yyyy-MM-dd") : "";
                    string json = client.DownloadString(new Uri(String.Format("https://api.trello.com/1/boards/{0}/cards?since={1}&fields=idBoard,name,desc,due,dueComplete,shortLink,closed,dateLastActivity&members=true&member_fields=username,initials,avatarHash&key={2}&token={3}", boardId, sinceValue, appKey, userToken)));
                    cards = JsonConvert.DeserializeObject<List<Card>>(json);
                }
                catch
                {

                }
            }

            return cards;
        }
    }
}
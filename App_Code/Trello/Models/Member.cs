﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trello.Models
{
    public class Member
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Initials { get; set; }
        public string AvatarHash { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trello.Models
{ 
    public class Board
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ShortLink { get; set; }
        public DateTime DateLastActivity { get; set; }
        public int CompanyId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trello.Models
{
    public class ExtractorSettings
    {
        public string AppKey { get; set; }
        public string AppToken { get; set; }
        public int CompanyId { get; set; }
    }
}
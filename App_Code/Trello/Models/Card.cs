﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trello.Models
{
    public class Card
    {
        public string Id { get; set; }
        public string IdBoard { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public string ShortLink { get; set; }
        public DateTime? Due { get; set; }
        public bool DueComplete { get; set; }
        public bool Closed { get; set; }
        public DateTime DateLastActivity { get; set; }
        public List<Member> Members { get; set; }
    }
}
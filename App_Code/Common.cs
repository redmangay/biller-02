﻿using DocGen.DAL;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;

/// <summary>
/// Summary description for Common
/// </summary>


public struct CommonStruct
{
    public static string DefaultValue_Login_Fixed = "--login--";
}
public struct WarningMsg
{

    public static string MaxtimebetweenRecords = "Max time between Records exceeded";
}
public class Common
{
    public Common()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    //public bool bUseSourceBacthForALS = true;
    public static string DBGEmailPart = "@dbgurus.com.au";
    public static DataTable GetUsersByDashboard(string strUserIDs, int iAccountID)
    {
        if (strUserIDs == "")
            strUserIDs = "-1";

        DataTable dtUserDash = Common.DataTableFromText(@"SELECT U.* ,R.[Role]
                FROM dbo.Account A
                JOIN [UserRole]  UR ON UR.AccountID = A.AccountID 
                JOIN [User] U ON U.UserID = UR.UserID 
                JOIN [Role] R ON UR.RoleID=R.RoleID
                WHERE A.AccountID=" + iAccountID.ToString() +
                      @" AND  U.UserID IN (" + strUserIDs + @") ORDER BY R.[Role],U.FirstName,U.LastName");
        return dtUserDash;

    }
    public static string GetUserIDsForDashboard(int iDocumentID, int iAccountID)
    {
        string strUserIDs = "";
        try
        {

            DataTable dtUsers = Common.DataTableFromText(@"SELECT U.UserID,UR.RoleID
                                FROM dbo.Account A
                                JOIN [UserRole]  UR ON UR.AccountID = A.AccountID 
                                JOIN [User] U ON U.UserID = UR.UserID 
                                WHERE U.IsActive=1 AND  A.AccountID=" + iAccountID.ToString());


            foreach (DataRow dr in dtUsers.Rows)
            {
                int? DashID = DocumentManager.dbg_Dashboard_BestFitting("", int.Parse(dr["UserID"].ToString()), int.Parse(dr["RoleID"].ToString()));

                if (DashID != null && (int)DashID == iDocumentID)
                {
                    strUserIDs = strUserIDs + dr["UserID"].ToString() + ",";
                }
            }



        }
        catch
        {
            //
        }
        if (strUserIDs != "")
            strUserIDs = strUserIDs.Substring(0, strUserIDs.Length - 1);
        return strUserIDs;
    }
    public static DateTime? GetDateTimeFromString(string strDateTime, string strFormat)
    {
        if (strFormat == "" || strFormat == "GB" || strFormat.ToUpper() == "DD/MM/YYYY")
        {
            try
            {
                DateTime dateValue;
                if (DateTime.TryParseExact(strDateTime, DateTimeformats, new CultureInfo("en-GB"), DateTimeStyles.None, out dateValue))
                {
                    return dateValue;
                }
                else
                {
                    //old
                    DateTime? oDateTime = null;
                    System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-GB");
                    if (strDateTime.IndexOf(" ") > 0)
                    {
                        if (strDateTime.Substring(0, strDateTime.IndexOf(" ")).Length < 7)
                        {
                            string strTempDateTime = strDateTime.Substring(0, strDateTime.IndexOf(" ")) + "-" + DateTime.Now.Year.ToString() + " " + strDateTime.Substring(strDateTime.IndexOf(" ") + 1);
                            oDateTime = Convert.ToDateTime(strTempDateTime, culture);
                        }
                        else
                        {
                            if (strDateTime.Length == 16)
                            {
                                //oDateTime = DateTime.ParseExact(strDateTime, "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                                oDateTime = DateTime.ParseExact(strDateTime, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                oDateTime = Convert.ToDateTime(strDateTime, culture);
                            }
                        }
                    }
                    else
                    {
                        oDateTime = Convert.ToDateTime(strDateTime, culture);
                    }
                    return oDateTime;
                }
            }
            catch
            {
                try
                {
                    return Convert.ToDateTime(strDateTime);
                }
                catch
                {
                    //
                }
            }

        }
        return null;

    }

    public static void FindTheAccount()
    {

        try
        {
            string page = Path.GetFileName(HttpContext.Current.Request.Path);
            if (page == "RecordUpload.aspx" || page == "UploadValidation.aspx")
            {
                if (HttpContext.Current.Request.QueryString["TableID"] != null)
                {
                    try
                    {
                        int TableID;
                        TableID = int.Parse(Cryptography.Decrypt(HttpContext.Current.Request.QueryString["TableID"].ToString()));
                        string strVirtualUsersTableID = SystemData.SystemOption_ValueByKey_Account("VirtualUsersTableID", null, null);
                        if (strVirtualUsersTableID != "" && int.Parse(strVirtualUsersTableID) == TableID)
                        {
                            return;
                        }
                    }
                    catch
                    {

                    }
                }

            }
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {

                int iTemp = 0;
                string strPageItemAccountID = "";

                if (strPageItemAccountID == "" && HttpContext.Current.Request.QueryString["TableID"] != null
                    && HttpContext.Current.Request.QueryString["TableID"].ToString() != "")
                {
                    iTemp = 0;
                    if (int.TryParse(HttpContext.Current.Request.QueryString["TableID"].ToString(), out iTemp))
                    {
                        strPageItemAccountID = Common.GetValueFromSQL(@"SELECT AccountID FROM [Table] T 
                              WHERE T.TableID=" + HttpContext.Current.Request.QueryString["TableID"].ToString());
                    }
                    else
                    {
                        strPageItemAccountID = Common.GetValueFromSQL(@"SELECT AccountID FROM [Table] T 
                              WHERE T.TableID=" + Cryptography.Decrypt(HttpContext.Current.Request.QueryString["TableID"].ToString()));
                    }

                }

                if (strPageItemAccountID == "" && HttpContext.Current.Request.QueryString["ColumnID"] != null)
                {
                    strPageItemAccountID = Common.GetValueFromSQL(@"SELECT AccountID FROM [Table] T 
                INNER JOIN [Column] C ON T.TableID=C.TableID  WHERE C.ColumnID=" + Cryptography.Decrypt(HttpContext.Current.Request.QueryString["ColumnID"].ToString()));
                }

                if (strPageItemAccountID == "" && HttpContext.Current.Request.QueryString["DocumentID"] != null)
                {
                    iTemp = 0;
                    if (int.TryParse(HttpContext.Current.Request.QueryString["DocumentID"].ToString(), out iTemp))
                    {
                        strPageItemAccountID = Common.GetValueFromSQL(@"SELECT AccountID FROM [Document]
                            WHERE DocumentID=" + HttpContext.Current.Request.QueryString["DocumentID"].ToString());
                    }
                    else
                    {
                        strPageItemAccountID = Common.GetValueFromSQL(@"SELECT AccountID FROM [Document]
                            WHERE DocumentID=" + Cryptography.Decrypt(HttpContext.Current.Request.QueryString["DocumentID"].ToString()));
                    }

                }

                if (strPageItemAccountID == "" && HttpContext.Current.Request.QueryString["GraphOptionID"] != null)
                {
                    strPageItemAccountID = Common.GetValueFromSQL(@"SELECT AccountID FROM [GraphOption]
                            WHERE GraphOptionID=" + Cryptography.Decrypt(HttpContext.Current.Request.QueryString["GraphOptionID"].ToString()));
                }

                if (strPageItemAccountID == "" && HttpContext.Current.Request.QueryString["MenuID"] != null && HttpContext.Current.Request.QueryString["MenuID"].Length > 0)
                    {
                    strPageItemAccountID = Common.GetValueFromSQL(@"SELECT AccountID FROM [Menu] 
                            WHERE MenuID=" + Cryptography.Decrypt(HttpContext.Current.Request.QueryString["MenuID"].ToString()));
                }

                if (strPageItemAccountID == "" && HttpContext.Current.Request.QueryString["TerminologyID"] != null)
                {
                    strPageItemAccountID = Common.GetValueFromSQL(@"SELECT AccountID FROM [Terminology] 
                            WHERE TerminologyID=" + Cryptography.Decrypt(HttpContext.Current.Request.QueryString["TerminologyID"].ToString()));
                }
                if (strPageItemAccountID == "" && HttpContext.Current.Request.QueryString["UploadID"] != null)
                {
                    strPageItemAccountID = Common.GetValueFromSQL(@"SELECT AccountID FROM [Table] T INNER JOIN [Upload] U ON 
                            T.TableID=U.TableID  WHERE U.UploadID=" + Cryptography.Decrypt(HttpContext.Current.Request.QueryString["UploadID"].ToString()));
                }


                if (strPageItemAccountID == "" && HttpContext.Current.Request.QueryString["AccountID"] != null
                    && Common.HaveAccess(HttpContext.Current.Session["roletype"].ToString(), "1") == false)
                {
                    strPageItemAccountID = Cryptography.Decrypt(HttpContext.Current.Request.QueryString["AccountID"].ToString());
                }


                if (strPageItemAccountID != "")
                {
                    if (int.Parse(HttpContext.Current.Session["AccountID"].ToString()) != int.Parse(strPageItemAccountID))
                    {
                        //different account
                        User loggedUser = (User)HttpContext.Current.Session["User"];
                        if (loggedUser != null)
                        {
                            if (Common.HaveAccess(HttpContext.Current.Session["roletype"].ToString(), "1") == true)
                            {
                                HttpContext.Current.Session["AccountID"] = strPageItemAccountID;
                                try
                                {
                                    HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl, true);
                                    return;
                                }
                                catch
                                {
                                    // return;
                                }
                                return;
                            }

                             //JA 18 JAN 2018                                    
                            else
                            {

                                if (page == "RecordUpload.aspx" && (Common.HaveAccess(HttpContext.Current.Session["roletype"].ToString(), "2") == true))
                                {
                                    try
                                    {
                                        //remove this - MR 07-Dec-2017
                                        int TableID;
                                        TableID = int.Parse(Cryptography.Decrypt(HttpContext.Current.Request.QueryString["TableID"].ToString()));
                                        HttpContext.Current.Session["UserTableID"] = TableID;
                                        HttpContext.Current.Response.Redirect("~/Pages/Record/RecordUpload.aspx");
                                        //HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl, true);
                                    }
                                    catch
                                    {

                                        return;
                                        //HttpContext.Current.Response.Redirect("~/Pages/Record/RecordUpload.aspx");
                                    }
                                }

                            }


                            if (Common.ChangeAccount((int)loggedUser.UserID, int.Parse(strPageItemAccountID), true))
                            {
                                try
                                {
                                    HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl, true);
                                    return;
                                }
                                catch
                                {
                                    HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl, true);
                                    return;
                                }

                            }
                        }

                        HttpContext.Current.Response.Redirect("~/Empty.aspx", true);
                        return;

                    }
                }
            }



        }
        catch (Exception ex)
        {
            //unknown
            //ErrorLog theErrorLog = new ErrorLog(null, "Secure page - wrong account", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            //SystemData.ErrorLog_Insert(theErrorLog);
        }

    }
    public static bool ChangeAccount(int iUserID, int iAccountID, bool bClearSession)
    {


        try
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                //User etUser = (User)HttpContext.Current.Session["User"];
                //string strScreenWidth="";
                //if( HttpContext.Current.Session["ScreenWidth"]!=null)
                //{
                //    strScreenWidth = HttpContext.Current.Session["ScreenWidth"].ToString();
                //}
                User etUser = SecurityManager.User_Details(iUserID);
                if (bClearSession)
                    HttpContext.Current.Session.Clear();

                HttpContext.Current.Session["User"] = etUser;
                Account theAccount = SecurityManager.Account_Details(iAccountID);
                string roletype = "";
                roletype = SecurityManager.GetUserRoleTypeID(iUserID, iAccountID);

                UserRole theUserRole = SecurityManager.GetUserRole(iUserID, iAccountID);
                if (theUserRole == null)
                    return false;
                string strFilesLocation = SystemData.SystemOption_ValueByKey_Account("FilesLocation", null, null);
                string strFilesPhisicalPath = SystemData.SystemOption_ValueByKey_Account("FilesPhisicalPath", null, null);

                if (strFilesLocation != "")
                {
                    HttpContext.Current.Session["FilesLocation"] = strFilesLocation;
                }
                else
                {
                    HttpContext.Current.Session["FilesLocation"] = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath;
                }


                if (strFilesPhisicalPath != "")
                {
                    HttpContext.Current.Session["FilesPhisicalPath"] = strFilesPhisicalPath;
                }
                else
                {
                    HttpContext.Current.Session["FilesPhisicalPath"] = HttpContext.Current.Server.MapPath("~");
                }
                HttpContext.Current.Session["roletype"] = roletype;
                HttpContext.Current.Session["AccountID"] = iAccountID;
                HttpContext.Current.Session["UserRole"] = theUserRole;
                HttpContext.Current.Session["client"] = iAccountID.ToString();
                SecurityManager.User_SessionID_Update((int)etUser.UserID, HttpContext.Current.Session.SessionID.ToString(), (int)iAccountID);
                if ((bool)theUserRole.IsAdvancedSecurity)
                {
                    //Session["STs"] = RecordManager.ets_Table_ByUser_AdvancedSecurity((int)_objUser.UserID, "-1,3,4,5,7,8,9");
                    HttpContext.Current.Session["STs"] = RecordManager.ets_Table_ByUser_AdvancedSecurity((int)theUserRole.RoleID);
                    if (HttpContext.Current.Session["roletype"].ToString() != Common.UserRoleType.OwnData)
                    {
                        HttpContext.Current.Session["roletype"] = Common.UserRoleType.ReadOnly;
                    }
                }
                else
                {
                    HttpContext.Current.Session["STs"] = "";
                    if (Common.HaveAccess(HttpContext.Current.Session["roletype"].ToString(), Common.UserRoleType.None))
                    {
                        HttpContext.Current.Session["STs"] = "-1";
                    }
                }

                try
                {
                    HttpContext.Current.Session["DoNotAllow"] = null;

                    if (SecurityManager.IsRecordsExceeded(iAccountID))
                    {
                        HttpContext.Current.Session["DoNotAllow"] = "true";
                    }


                }
                catch
                {
                    //
                }

                HttpContext.Current.Session["GridPageSize"] = SystemData.SystemOption_ValueByKey_Account("GridPageSize", iAccountID, null);

                HttpContext.Current.Session["DoNotAllow"] = null;

                if (SecurityManager.IsRecordsExceeded(iAccountID))
                {
                    HttpContext.Current.Session["DoNotAllow"] = "true";
                }
                //if (strScreenWidth != "")
                //    HttpContext.Current.Session["ScreenWidth"] = strScreenWidth;

                if (theAccount != null && (bool)theAccount.IsActive)
                {
                    //HttpContext.Current.Session["tdbmsg"] = "Your are now in "+theAccount.AccountName+" account.";
                    return true;
                }

            }


        }
        catch
        {
            //
        }


        return false;
    }
    public static bool HasRecord(string strSQL)
    {
        if (!string.IsNullOrEmpty(strSQL))
        {
            string strRecordValue = GetValueFromSQL(strSQL);
            if (!string.IsNullOrEmpty(strRecordValue))
                return true;

        }
        return false;
    }

    //Import Template
    public static string LongDateWithTimeStringFormat = @"dd\/MM\/yyyy h\:mm tt";

    public static string ToolTip_Today = @"You can use TODAY keyword; e.g TODAY - 10, TODAY, TODAY + 20 etc.";

    public static string BeforeComma(string sX)
    {
        string sR = "";
        if (sX.IndexOf(",") > 0)
        {
            sR = sX.Substring(0, sX.IndexOf(","));
        }
        else
        {
            sR = sX;
        }
        return sR;
    }
    public static string AferComma(string sX)
    {
        string sR = "";
        if (sX.IndexOf(",") > 0)
        {
            sR = sX.Substring(sX.IndexOf(",") + 1);
        }
        else
        {
            sR = sX;
        }
        return sR;
    }

    public static bool SO_ImportTemplateAsDefault(int? iAccountID, int? iTableID)
    {
        string strOptionValue = SystemData.SystemOption_ValueByKey_Account("Import Template As Default", iAccountID, iTableID);
        if (strOptionValue != "" && strOptionValue.ToLower() == "no")
        {
            return false;
        }
        return true;
    }

    public static bool SO_ShowExceedances(int? iAccountID, int? iTableID)
    {
        string strOptionValue = SystemData.SystemOption_ValueByKey_Account("Show Exceedances", iAccountID, iTableID);
        if (strOptionValue != "" && strOptionValue.ToLower() == "no")
        {
            return false;
        }
        return true;
    }

    //END of Import Template 

    public static bool SO_SearchAllifToIsNull(int? iAccountID, int? iTableID)
    {

        string strOptionValue = SystemData.SystemOption_ValueByKey_Account("Use Lower Limit As Minimum When Upper Empty", iAccountID, iTableID);
        if (strOptionValue != "" && strOptionValue.ToLower() == "yes")
        {
            return true;
        }
        return false;
    }
    public static string SO_ViewAlignmentDefault(int? iAccountID, int? iTableID)
    {

        string strOptionValue = SystemData.SystemOption_ValueByKey_Account("View Alignment Default", iAccountID, iTableID);
        if (strOptionValue != "")
        {
            return strOptionValue;
        }
        return "Auto";
    }
    public static bool SO_ShowRecordFirstLastButtons(int? iAccountID, int? iTableID)
    {
        string strOptionValue = SystemData.SystemOption_ValueByKey_Account("Show Record First-Last Buttons", iAccountID, iTableID);
        if (strOptionValue != "" && strOptionValue.ToLower() == "yes")
        {
            return true;
        }
        return false;
    }


    public static string GetDatabaseName()
    {
        string strDBName = "";

        strDBName = Common.GetValueFromSQL("SELECT DB_NAME()");

        return strDBName;
    }
    public static string GetValidFileName(string strFileName)
    {
        string invalidChars = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
        foreach (char c in invalidChars)
        {
            strFileName = strFileName.Replace(c.ToString(), "_"); // or with "."
        }
        return strFileName;
    }
    public static string RecordExceededMessage = @"You have exceeded the number of records allowed. Please contact DB Gurus if you wish to extend your plan. Alternatively you can permanently delete records.";
    public static string ReturnDateStringFromToken(string strDateToken)
    {
        string strTempDateToken = strDateToken.Trim().ToLower();
        if (strTempDateToken == "today")
        {
            return DateTime.Today.ToShortDateString();
        }

        if (strTempDateToken.IndexOf("today") > -1)
        {
            strTempDateToken = strTempDateToken.Replace("today", "");

            if (strTempDateToken.IndexOf("+") > -1)
            {
                strTempDateToken = strTempDateToken.Replace("+", "");
                strTempDateToken = strTempDateToken.Trim();
                int iTotalDayAdd = 0;

                if (int.TryParse(strTempDateToken, out iTotalDayAdd))
                {
                    return DateTime.Today.AddDays(iTotalDayAdd).ToShortDateString();
                }
            }

            if (strTempDateToken.IndexOf("-") > -1)
            {
                strTempDateToken = strTempDateToken.Replace("-", "");
                strTempDateToken = strTempDateToken.Trim();
                int iTotalDayAdd = 0;

                if (int.TryParse(strTempDateToken, out iTotalDayAdd))
                {
                    return DateTime.Today.AddDays(-iTotalDayAdd).ToShortDateString();
                }
            }

        }



        return strDateToken;
    }

    public static string ReturnDateTimeStringFromToken(string strDateTimeToken)
    {
        string strTempDateToken = strDateTimeToken.Trim().ToLower();
        if (strTempDateToken == "today")
        {
            return DateTime.Now.ToString();
        }


        if (strTempDateToken.IndexOf("today") > -1)
        {
            strTempDateToken = strTempDateToken.Replace("today", "");

            if (strTempDateToken.IndexOf("+") > -1)
            {
                strTempDateToken = strTempDateToken.Replace("+", "");
                strTempDateToken = strTempDateToken.Trim();
                int iTotalDayAdd = 0;

                if (int.TryParse(strTempDateToken, out iTotalDayAdd))
                {
                    return DateTime.Now.AddDays(iTotalDayAdd).ToString();
                }
            }

            if (strTempDateToken.IndexOf("-") > -1)
            {
                strTempDateToken = strTempDateToken.Replace("-", "");
                strTempDateToken = strTempDateToken.Trim();
                int iTotalDayAdd = 0;

                if (int.TryParse(strTempDateToken, out iTotalDayAdd))
                {
                    return DateTime.Now.AddDays(-iTotalDayAdd).ToString();
                }
            }

        }

        DateTime dt;
        if (DateTime.TryParse(strDateTimeToken, out dt))
        {
            string dd = dt.ToShortDateString();
            string tt;

            if (strDateTimeToken.Length > 8)
                tt = dt.ToLongTimeString();
            else
                tt = DateTime.Now.ToLongTimeString();

            strDateTimeToken = dd + " " + tt;
        }


        return strDateTimeToken;

    }

    public static string EncodeTo64(string toEncode)
    {
        byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
        string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
        return returnValue;
    }

    public static string DecodeFrom64(string encodedData)
    {
        byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
        string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
        return returnValue;
    }

    public static int DaysBetween(DateTime d1, DateTime d2)
    {
        TimeSpan span = d2.Subtract(d1);
        return (int)span.TotalDays;
    }

    public static string[] Dateformats = {"d/M/yyyy",
                                    "dd/MM/yyyy","dd/M/yyyy","d/M/yy","dd/MM/yyyy","dd/M/yy","d/MM/yyyy","d/MM/yy"};

    public static string[] DateTimeformats = {"d/M/yyyy HH:mm","dd/MM/yyyy HH:mm","dd/M/yyyy HH:mm","d/M/yy HH:mm","dd/MM/yyyy HH:mm","d/MM/yyyy HH:mm","d/MM/yy HH:mm",
                                             "d/M/yyyy HH:mm:ss","dd/MM/yyyy HH:mm:ss","dd/M/yyyy HH:mm:ss","d/M/yy HH:mm:ss","dd/MM/yyyy HH:mm:ss",
                                             "d/MM/yyyy HH:mm:ss","d/MM/yy HH:mm:ss",
                                             "d/M/yyyy HH:mm tt","dd/MM/yyyy HH:mm tt","dd/M/yyyy HH:mm tt","d/M/yy HH:mm tt","dd/MM/yyyy HH:mm tt",
                                             "d/MM/yyyy HH:mm tt","d/MM/yy HH:mm tt",
                                             "d/M/yyyy HH:mm:ss tt","dd/MM/yyyy HH:mm:ss tt","dd/M/yyyy HH:mm:ss tt","d/M/yy HH:mm:ss","dd/MM/yyyy HH:mm:ss tt",
                                             "d/MM/yyyy HH:mm:ss  tt","d/MM/yy HH:mm:ss tt",
                                             "d/M/yyyy H:mm","dd/MM/yyyy H:mm","dd/M/yyyy H:mm","d/M/yy H:mm","dd/MM/yyyy H:mm","d/MM/yyyy H:mm","d/MM/yy H:mm",
                                             "d/M/yyyy H:mm:ss","dd/MM/yyyy H:mm:ss","dd/M/yyyy H:mm:ss","d/M/yy H:mm:ss","dd/MM/yyyy H:mm:ss",
                                             "d/MM/yyyy H:mm:ss","d/MM/yy H:mm:ss",
                                             "d/M/yyyy H:mm tt","dd/MM/yyyy H:mm tt","dd/M/yyyy H:mm tt","d/M/yy H:mm tt","dd/MM/yyyy H:mm tt",
                                             "d/MM/yyyy H:mm tt","d/MM/yy H:mm tt",
                                             "d/M/yyyy H:mm:ss tt","dd/MM/yyyy H:mm:ss tt","dd/M/yyyy H:mm:ss tt","d/M/yy H:mm:ss","dd/MM/yyyy H:mm:ss tt",
                                             "d/MM/yyyy H:mm:ss  tt","d/MM/yy H:mm:ss tt",
                                             "d/M/yyyy HH:m","dd/MM/yyyy HH:m","dd/M/yyyy HH:m","d/M/yy HH:m","dd/MM/yyyy HH:m","d/MM/yyyy HH:m","d/MM/yy HH:m",
                                             "d/M/yyyy HH:m:ss","dd/MM/yyyy HH:m:ss","dd/M/yyyy HH:m:ss","d/M/yy HH:m:ss","dd/MM/yyyy HH:m:ss",
                                             "d/MM/yyyy HH:m:ss","d/MM/yy HH:m:ss",
                                             "d/M/yyyy HH:m tt","dd/MM/yyyy HH:m tt","dd/M/yyyy HH:m tt","d/M/yy HH:m tt","dd/MM/yyyy HH:m tt",
                                             "d/MM/yyyy HH:m tt","d/MM/yy HH:m tt",
                                             "d/M/yyyy HH:m:ss tt","dd/MM/yyyy HH:m:ss tt","dd/M/yyyy HH:m:ss tt","d/M/yy HH:m:ss","dd/MM/yyyy HH:m:ss tt",
                                             "d/MM/yyyy HH:m:ss  tt","d/MM/yy HH:m:ss tt",
                                             "d/M/yyyy H:m","dd/MM/yyyy H:m","dd/M/yyyy H:m","d/M/yy H:m","dd/MM/yyyy H:m","d/MM/yyyy H:m","d/MM/yy H:m",
                                             "d/M/yyyy H:m:ss","dd/MM/yyyy H:m:ss","dd/M/yyyy H:m:ss","d/M/yy H:m:ss","dd/MM/yyyy H:m:ss",
                                             "d/MM/yyyy H:m:ss","d/MM/yy H:m:ss",
                                             "d/M/yyyy H:m tt","dd/MM/yyyy H:m tt","dd/M/yyyy H:m tt","d/M/yy H:m tt","dd/MM/yyyy H:m tt",
                                             "d/MM/yyyy H:m tt","d/MM/yy H:m tt",
                                             "d/M/yyyy H:m:ss tt","dd/MM/yyyy H:m:ss tt","dd/M/yyyy H:m:ss tt","d/M/yy H:m:ss","dd/MM/yyyy H:m:ss tt",
                                             "d/MM/yyyy H:m:ss  tt","d/MM/yy H:m:ss tt"};

    public static string DemoEmail = "demo@carbonmonitoring.com.au";
    public static string DemoReadyOnlyMsg = "Demo user is read only.";
    public static string MenuDividerText = "---";
    //public static int _iMaxRecordsExport = 5;
    public static int MinSTDEVRecords = 3;
    public static int MaxGraphRecords = 200; //1000
    public static int MaxRowForListBoxTable = 1000; //1000
    public static string NumberRegExDC = @"\[(.*?)\]"; //@"^.*?\([^\d]*(\d+)[^\d]*\).*$";
    public static string AusMobileRegEx = @"^04[0-9]{2}\s?([0-9]{3}\s?[0-9]{3}|[0-9]{2}\s?[0-9]{2}\s?[0-9]{2})$";
    //@"/^(?:\+?61|0)4(?:[01]\d{3}|(?:2[1-9]|3[0-57-9]|4[7-9]|5[0-15-9]|6[679]|7[3-8]|8[1478]|9[07-9])\d{2}|(?:20[2-9]|444|52[0-6]|68[3-9]|70[0-7]|79[01]|820|890|91[0-4])\d|(?:200[0-3]|201[01]|8984))\d{4}$/";  // @"^((61|\+61)?\s?)04[0-9]{2}\s?([0-9]{3}\s?[0-9]{3}|[0-9]{2}\s?[0-9]{2}\s?[0-9]{2})$";
    public static int MaxRecordsExport(int? iAccountID, int? iTableID)
    {


        return int.Parse(SystemData.SystemOption_ValueByKey_Account("MaxRecordsExport", iAccountID, iTableID));


    }

    public static string GetUpdatedFullURLWithQueryString(string strFullURL, string theQueryString, string theNewValue)
    {
        var nameValues = HttpUtility.ParseQueryString(strFullURL);
        nameValues.Set(theQueryString, theNewValue);
        string strReturnURL = nameValues.ToString();
        strReturnURL = HttpContext.Current.Server.UrlDecode(strReturnURL);

        return strReturnURL;
    }
    public static string GetUpdatedFullURLRemoveQueryString(string strFullURL, string theRemoveQueryString)
    {
        try
        {
            if (strFullURL.IndexOf('?') > -1)
            {
                string strBase = strFullURL.Substring(0, strFullURL.IndexOf('?') + 1);
                var nameValues = HttpUtility.ParseQueryString(strFullURL.Substring(strFullURL.IndexOf('?') + 1));
                nameValues.Remove(theRemoveQueryString);
                string strReturnURL = nameValues.ToString();
                strReturnURL = HttpContext.Current.Server.UrlDecode(strReturnURL);

                return strBase + strReturnURL;
            }

        }
        catch
        {
            //
        }

        return strFullURL;
    }

    public static bool IsThisDouble(string strValue)
    {
        try
        {
            if (strValue.ToLower() == "nan")
            {
                return false;
            }

            double dTest = double.Parse(strValue);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static string MakeDecimal(string strValue)
    {
        if (strValue.IndexOf(".", 0) == -1)
        {
            return strValue + ".0";
        }
        return strValue;
    }


    public static string FixCrLfAndOtherNonPrint(string value)
    {

        if (string.IsNullOrEmpty(value)) { return string.Empty; }


        return value.Replace(Environment.NewLine, "<br />").Replace("\n", "<br/>").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
    }


    public static DateTime PreviousMonday(DateTime dt)
    {
        var dateDayOfWeek = (int)dt.DayOfWeek;
        if (dateDayOfWeek == 0)
        {
            dateDayOfWeek = dateDayOfWeek + 7;
        }
        var alterNumber = dateDayOfWeek - ((dateDayOfWeek * 2) - 1);

        return dt.AddDays(alterNumber);
    }


    public static string GetSystemFromDisplay(string strDisplay, int iTableID)
    {

        if (strDisplay != "")
        {


            DataTable dtTemp = Common.DataTableFromText("SELECT SystemName,DisplayName,ColumnType FROM [Column] WHERE TableID=" + iTableID.ToString());

            foreach (DataRow dr in dtTemp.Rows)
            {
                if (dr["ColumnType"].ToString() == "number" || dr["ColumnType"].ToString() == "dropdown")
                {
                    strDisplay = strDisplay.Replace("[" + dr["DisplayName"].ToString() + "]", "CAST(dbo.RemoveNonNumericChar([" + dr["SystemName"].ToString() + "]) as decimal(20,10))");
                }
                else if (dr["ColumnType"].ToString() == "date")
                {
                    strDisplay = strDisplay.Replace("[" + dr["DisplayName"].ToString() + "]", "[" + dr["SystemName"].ToString() + "]");
                }
                else
                {
                    strDisplay = strDisplay.Replace("[" + dr["DisplayName"].ToString() + "]", "[" + dr["SystemName"].ToString() + "]");
                }

            }



            //Work with 1 top level Parent tables.
            DataTable dtPT = Common.DataTableFromText("SELECT distinct ParentTableID FROM TableChild WHERE ChildTableID=" + iTableID.ToString()); //AND DetailPageType<>'not'

            if (dtPT.Rows.Count > 0)
            {
                foreach (DataRow dr in dtPT.Rows)
                {
                    DataTable dtPColumns = Common.DataTableFromText(@"SELECT distinct TableName + ':' + DisplayName AS DP,TableName + ':' +  [Column].SystemName AS SP,ColumnType
                                            FROM [Column] INNER JOIN [Table]
                                        ON [Column].TableID=[Table].TableID WHERE IsStandard=0 AND TableTableID IS NULL AND  [Column].TableID=" + dr["ParentTableID"].ToString());
                    foreach (DataRow drp in dtPColumns.Rows)
                    {
                        if (drp["ColumnType"].ToString() == "number" || drp["ColumnType"].ToString() == "dropdown")
                        {
                            strDisplay = strDisplay.Replace("[" + drp["DP"].ToString() + "]", "CAST(dbo.RemoveNonNumericChar([" + drp["SP"].ToString() + "]) as decimal(20,10))");
                        }
                        else if (drp["ColumnType"].ToString() == "date")
                        {
                            strDisplay = strDisplay.Replace("[" + drp["DP"].ToString() + "]", "[" + drp["SP"].ToString() + "]");
                        }
                        else
                        {
                            strDisplay = strDisplay.Replace("[" + drp["DP"].ToString() + "]", "[" + drp["SP"].ToString() + "]");
                        }

                    }
                }
            }

        }

        return strDisplay;

    }


    public static string GetDisplayFromSystem(string strSystem, int iTableID)
    {

        if (strSystem != "")
        {
            DataTable dtTemp = Common.DataTableFromText("SELECT SystemName,DisplayName FROM [Column] WHERE TableID=" + iTableID.ToString());

            foreach (DataRow dr in dtTemp.Rows)
            {
                strSystem = strSystem.Replace("CAST(dbo.RemoveNonNumericChar([" + dr["SystemName"].ToString() + "]) as decimal(20,10))", "[" + dr["DisplayName"].ToString() + "]");

                // strSystem = strSystem.Replace("CONVERT(Datetime,[" + dr["SystemName"].ToString() + "],103)", "[" + dr["DisplayName"].ToString() + "]");

                strSystem = strSystem.Replace("[" + dr["SystemName"].ToString() + "]", "[" + dr["DisplayName"].ToString() + "]");

            }


            //Work with 1 top level Parent tables.
            DataTable dtPT = Common.DataTableFromText("SELECT distinct ParentTableID FROM TableChild WHERE ChildTableID=" + iTableID.ToString()); //AND DetailPageType<>'not'
            if (dtPT.Rows.Count > 0)
            {
                foreach (DataRow dr in dtPT.Rows)
                {
                    DataTable dtPColumns = Common.DataTableFromText(@"SELECT distinct TableName + ':' + DisplayName AS DP,TableName + ':' +  [Column].SystemName AS SP,ColumnType
                                            FROM [Column] INNER JOIN [Table]
                                        ON [Column].TableID=[Table].TableID WHERE IsStandard=0 AND TableTableID IS NULL AND  [Column].TableID=" + dr["ParentTableID"].ToString());
                    foreach (DataRow drp in dtPColumns.Rows)
                    {

                        strSystem = strSystem.Replace("CAST(dbo.RemoveNonNumericChar([" + drp["SP"].ToString() + "]) as decimal(20,10))", "[" + drp["DP"].ToString() + "]");

                        strSystem = strSystem.Replace("[" + drp["SP"].ToString() + "]", "[" + drp["DP"].ToString() + "]");

                    }
                }

            }



        }

        return strSystem;

    }

    public static string UpdateURL(string strOriURL, string sQS_K, string sQS_V, bool? bRemoveKey)
    {
        bool bFoundKey = false;
        string strNewURL = "";
        try
        {
            strNewURL = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path) + "?";
            string sValue = "";
            string sAnd = "";
            int i = 0;
            foreach (String key in System.Web.HttpContext.Current.Request.QueryString.AllKeys)
            {
                string sKey = key;
                if (i > 0)
                {
                    sAnd = "&";
                }
                else
                {
                    sAnd = "";
                }
                sValue = System.Web.HttpContext.Current.Request.QueryString[key].ToString();
                if (sQS_K.ToLower() == key.ToLower())
                {
                    bFoundKey = true;
                    sValue = sQS_V;
                    if (bRemoveKey != null && (bool)bRemoveKey)
                    {
                        sKey = "";
                    }
                }
                if (sKey == "")
                {
                    //no add & no increase of i;
                }
                else
                {
                    strNewURL += sAnd + sKey + "=" + sValue;
                    i += 1;
                }

            }
            if (bFoundKey == false)
            {
                if (i > 0)
                {
                    sAnd = "&";
                }
                else
                {
                    sAnd = "";
                }
                strNewURL += sAnd + sQS_K + "=" + sQS_V;
            }
            strOriURL = strNewURL;
        }
        catch
        {
            //
        }


        return strOriURL;
    }
    public static string EvaluateCalculationFormula(string expression)
    {
        try
        {
            var loDataTable = new DataTable();
            var loDataColumn = new DataColumn("Eval", typeof(double), expression);
            loDataTable.Columns.Add(loDataColumn);
            loDataTable.Rows.Add(0);
            return ((double)(loDataTable.Rows[0]["Eval"])).ToString();
        }
        catch
        {
            return "";
        }

    }
    public static string GetCalculationSystemNameOnly(string strSystem, int iTableID)
    {

        if (strSystem != "")
        {
            DataTable dtTemp = Common.DataTableFromText("SELECT SystemName,DisplayName FROM [Column] WHERE TableID=" + iTableID.ToString());

            foreach (DataRow dr in dtTemp.Rows)
            {
                strSystem = strSystem.Replace("CAST(dbo.RemoveNonNumericChar([" + dr["SystemName"].ToString() + "]) as decimal(20,10))", "[" + dr["SystemName"].ToString() + "]");

            }


            //Work with 1 top level Parent tables.
            DataTable dtPT = Common.DataTableFromText("SELECT distinct ParentTableID FROM TableChild WHERE ChildTableID=" + iTableID.ToString()); //AND DetailPageType<>'not'
            if (dtPT.Rows.Count > 0)
            {
                foreach (DataRow dr in dtPT.Rows)
                {
                    DataTable dtPColumns = Common.DataTableFromText(@"SELECT distinct TableName + ':' + DisplayName AS DP,TableName + ':' +  [Column].SystemName AS SP,ColumnType
                                            FROM [Column] INNER JOIN [Table]
                                        ON [Column].TableID=[Table].TableID WHERE IsStandard=0 AND TableTableID IS NULL AND  [Column].TableID=" + dr["ParentTableID"].ToString());
                    foreach (DataRow drp in dtPColumns.Rows)
                    {

                        strSystem = strSystem.Replace("CAST(dbo.RemoveNonNumericChar([" + drp["SP"].ToString() + "]) as decimal(20,10))", "[" + drp["SP"].ToString() + "]");


                    }
                }

            }



        }

        return strSystem;

    }


    public static int GetIntColorFromName(string strColorName)
    {
        try
        {
            switch (strColorName)
            {
                case "Aqua":
                    return 0x00FFFF;
                case "Black":
                    return 0x000000;
                case "Blue":
                    return 0x0000FF;
                case "Fuchsia":
                    return 0xFF00FF;
                case "Gray":
                    return 0x808080;
                case "Green":
                    return 0x008000;
                case "Lime":
                    return 0x00FF00;
                case "Maroon":
                    return 0x800000;
                case "Navy":
                    return 0x000080;
                case "Olive":
                    return 0x808000;
                case "Orange":
                    return 0xFFA500;
                case "Purple":
                    return 0x800080;
                case "Red":
                    return 0xFF0000;
                case "Silver":
                    return 0xC0C0C0;
                case "Teal":
                    return 0x008080;
                case "Yellow":
                    return 0xFFFF00;
                default:
                    return int.Parse(strColorName.Replace("#", ""), System.Globalization.NumberStyles.HexNumber);

            }
        }
        catch (Exception ex)
        {
            return -1;
        }


    }

    public int DefaultTextWidth = 22;//200/22=9px for each word
    public int DefaultTextHeight = 1;//18*1
    public enum NumberType
    {
        Normal = 1,
        Constant = 2,
        Calculated = 3,
        Average = 4
    }


    public static class UserRoleType
    {
        public static string GOD = "1";
        public static string Administrator = "2";
        //public static string EditRecordSite = "3";
        public static string AddEditRecord = "4";
        public static string ReadOnly = "5";
        public static string None = "6";
        public static string AddRecord = "7";
        public static string OwnData = "8";
        public static string EditOwnViewOther = "9";

    }



    //public static string GetMinVaue(string strFormula)
    //{
    //    strFormula = strFormula.ToLower();
    //    strFormula = strFormula.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ");
    //    strFormula = strFormula.Replace("  ", " ");
    //    strFormula = strFormula.Replace("value >=", "value>=");
    //    strFormula = strFormula.Replace("value > =", "value>=");

    //    if (strFormula.IndexOf("value>=") > -1)
    //    {
    //        //we have MIN
    //        string strMin = strFormula.Substring(strFormula.IndexOf("value>=") + 7);
    //        strMin = strMin.Trim();
    //        if (strMin.IndexOf(" ") > -1)
    //        {
    //            return strMin.Substring(0, strMin.IndexOf(" "));
    //        }
    //        else
    //        {
    //            return strMin;
    //        }

    //    }


    //    return "";
    //}


    public static string GetMaxFromFormula(string strFormula)
    {
        strFormula = strFormula.ToLower();
        strFormula = strFormula.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ");
        strFormula = strFormula.Replace("  ", " ");
        strFormula = strFormula.Replace("value <=", "value<=");
        strFormula = strFormula.Replace("value < =", "value<=");

        if (strFormula.IndexOf("value<=") > -1)
        {
            //we have max
            string strMax = strFormula.Substring(strFormula.IndexOf("value<=") + 7);
            strMax = strMax.Trim();
            if (strMax.IndexOf(" ") > -1)
            {
                return strMax.Substring(0, strMax.IndexOf(" "));
            }
            else
            {
                return strMax;
            }

        }


        return "";
    }

    //public static string GetMaxVaue(string strFormula)
    //{
    //    strFormula = strFormula.ToLower();
    //    strFormula = strFormula.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ");
    //    strFormula = strFormula.Replace("  ", " ");
    //    strFormula = strFormula.Replace("value <=", "value<=");
    //    strFormula = strFormula.Replace("value < =", "value<=");

    //    if (strFormula.IndexOf("value<=") > -1)
    //    {
    //        //we have Max
    //        string strMax = strFormula.Substring(strFormula.IndexOf("value<=") + 7);
    //        strMax = strMax.Trim();
    //        if (strMax.IndexOf(" ") > -1)
    //        {
    //            return strMax.Substring(0, strMax.IndexOf(" "));
    //        }
    //        else
    //        {
    //            return strMax;
    //        }

    //    }

    //    return "";
    //}


    public static string GetFullFormula(string strMin, string strMax, string strFull)
    {
        if (strMin != "" || strMax != "")
        {
            string strFormula = "";
            if (strMin != "")
            {
                strFormula = "value>=" + strMin;
            }

            if (strMax != "")
            {
                strFormula = "value<=" + strMax;
            }

            if (strMin != ""
            && strMax != "")
            {
                strFormula = "value>=" + strMin + " AND " + "value<=" + strMax;
            }

            return strFormula;

        }
        else
        {
            return strFull;
        }


    }
    public static string GetMinFromFormula(string strFormula)
    {
        strFormula = strFormula.ToLower();
        strFormula = strFormula.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ");
        strFormula = strFormula.Replace("  ", " ");
        strFormula = strFormula.Replace("value >=", "value>=");
        strFormula = strFormula.Replace("value > =", "value>=");

        if (strFormula.IndexOf("value>=") > -1)
        {
            //we have Min
            string strMin = strFormula.Substring(strFormula.IndexOf("value>=") + 7);
            strMin = strMin.Trim();
            if (strMin.IndexOf(" ") > -1)
            {
                return strMin.Substring(0, strMin.IndexOf(" "));
            }
            else
            {
                return strMin;
            }

        }

        return "";
    }

    public static void ShowFromula(string strFormula, ref TextBox txtMin, ref TextBox txtMax, ref TextBox txtFullFormula, ref bool bAdvanced)
    {
        string strMin = "";
        string strMax = "";
        strMin = Common.GetMinFromFormula(strFormula);
        strMax = Common.GetMaxFromFormula(strFormula);

        int iTotalValue = Common.GetNumberOfValue(strFormula);
        bAdvanced = false;

        if (iTotalValue > 2)
        {

            bAdvanced = true;
        }
        else
        {
            if (iTotalValue > 1)
            {
                if (strMin != "" && strMax != "")
                {

                }
                else
                {
                    bAdvanced = true;
                }
            }
            else
            {
                if (strMin != "" || strMax != "")
                {

                }
                else
                {
                    bAdvanced = true;
                }

            }
        }

        if (bAdvanced)
        {
            txtFullFormula.Text = strFormula;
        }
        else
        {
            txtMin.Text = strMin;
            txtMax.Text = strMax;
        }

    }

    public static string GetFromulaMsg(string sType, string strDisplayName, string strFormula)
    {
        string strReturnMessage = "";
        string strMin = "";
        string strMax = "";
        strMin = Common.GetMinFromFormula(strFormula);
        strMax = Common.GetMaxFromFormula(strFormula);

        int iTotalValue = Common.GetNumberOfValue(strFormula);
        bool bAdvanced = false;

        if (iTotalValue > 2)
        {

            bAdvanced = true;
        }
        else
        {
            if (iTotalValue > 1)
            {
                if (strMin != "" && strMax != "")
                {

                }
                else
                {
                    bAdvanced = true;
                }
            }
            else
            {
                if (strMin != "" || strMax != "")
                {

                }
                else
                {
                    bAdvanced = true;
                }

            }
        }

        switch (sType)
        {
            case "i":
                strReturnMessage = "INVALID: ";
                break;
            case "e":
                strReturnMessage = "EXCEEDANCE: ";
                break;
            case "w":
                strReturnMessage = "WARNING: ";
                break;
        }

        if (strReturnMessage != "")
            strReturnMessage = strReturnMessage + strDisplayName + " ";

        if (strFormula == "---advanced---")
        {
            strReturnMessage = strReturnMessage + "Value outside accepted range.";
        }
        else if (bAdvanced)
        {
            if (strFormula == "")
            {
                strReturnMessage = strReturnMessage + "Not set.";
            }
            else
            {
                strReturnMessage = strReturnMessage + "Value outside accepted range(" + strFormula + ").";
            }

        }
        else
        {
            if (sType == "")
            {
                if (strMin == "")
                    strMin = "Not set";
                if (strMax == "")
                    strMax = "Not set";
            }

            if (strMin != "" && strMax != "")
            {
                if (sType == "")
                {
                    strReturnMessage = "Value less than: " + strMin + "<br/> Value greater than: " + strMax;
                }
                else
                {
                    strReturnMessage = strReturnMessage + "is less than " + strMin + " or greater than " + strMax;
                }

            }
            else
            {
                if (strMin != "")
                {
                    strReturnMessage = strReturnMessage + "is less than " + strMin;
                }
                if (strMax != "")
                {
                    strReturnMessage = strReturnMessage + "is greater than " + strMax;
                }
            }


        }
        return strReturnMessage;
    }
    public static int GetNumberOfValue(string strFormula)
    {

        strFormula = strFormula.ToLower();

        if (strFormula.IndexOf("value") > -1)
        {
            strFormula = strFormula.Substring(strFormula.IndexOf("value") + 5);
            if (strFormula.IndexOf("value") > -1)
            {
                strFormula = strFormula.Substring(strFormula.IndexOf("value") + 5);

                if (strFormula.IndexOf("value") > -1)
                {
                    return 3;
                }
                else
                {
                    return 2;
                }
            }
            else
            {
                return 1;
            }
        }

        return 0;
    }




    public static void PopulateAdminDropDown(ref DropDownList ddl)
    {


        System.Web.UI.WebControls.ListItem liAudit = new System.Web.UI.WebControls.ListItem("Audit", "Audit");
        ddl.Items.Insert(0, liAudit);

        System.Web.UI.WebControls.ListItem liBatches = new System.Web.UI.WebControls.ListItem("Batches", "Batches");
        ddl.Items.Insert(1, liBatches);

        System.Web.UI.WebControls.ListItem liContents = new System.Web.UI.WebControls.ListItem("Contents", "Contents");
        ddl.Items.Insert(2, liContents);

        //System.Web.UI.WebControls.ListItem liDocuments = new System.Web.UI.WebControls.ListItem("Documents and Reports", "Documents");
        //ddl.Items.Insert(3, liDocuments);

        System.Web.UI.WebControls.ListItem liNotifications = new System.Web.UI.WebControls.ListItem("Notifications", "Notifications");
        ddl.Items.Insert(3, liNotifications);

        System.Web.UI.WebControls.ListItem liTableList = new System.Web.UI.WebControls.ListItem("Tables", "TableList");
        ddl.Items.Insert(4, liTableList);

        System.Web.UI.WebControls.ListItem liSensors = new System.Web.UI.WebControls.ListItem("Sensors", "Sensors");
        ddl.Items.Insert(5, liSensors);

        //System.Web.UI.WebControls.ListItem liLocationList = new System.Web.UI.WebControls.ListItem( "Locations","LocationList");
        //ddl.Items.Insert(6, liLocationList);

        System.Web.UI.WebControls.ListItem liUsers = new System.Web.UI.WebControls.ListItem("Users", "Users");
        ddl.Items.Insert(6, liUsers);

        System.Web.UI.WebControls.ListItem liWorkFlow = new System.Web.UI.WebControls.ListItem("WorkFlows", "WorkFlows");
        ddl.Items.Insert(7, liWorkFlow);



    }
    public static string GetNavigateURL(string strValue, int iAccontID)
    {
        switch (strValue.Trim())
        {
            //case "Account":
            //    return "~/Pages/Security/AccountDetail.aspx?mode=" + Cryptography.Encrypt("edit") + "&accountid=" + Cryptography.Encrypt(iAccontID.ToString()) ;

            case "Batches":
                return "~/Pages/Record/Batches.aspx?menu=" + Cryptography.Encrypt("yes") + "&TableID=" + Cryptography.Encrypt("-1");

            case "Contents":
                return "~/Pages/SystemData/Content.aspx";

            case "Audit":
                return "~/Pages/Document/AuditReport.aspx";

            case "Notifications":
                return "~/Pages/Record/Notification.aspx";

            case "TableList":
                return "~/Pages/Record/TableList.aspx";

            case "Sensors":
                return "~/Pages/Instrument/Sensor.aspx";

            //case "LocationList":
            //    return "~/Pages/Site/LocationList.aspx?MenuID=" +   Cryptography.Encrypt("-1");

            case "Users":
                return "~/Pages/User/List.aspx";



            default:
                return "#";

        }
    }

    public static string CompareOperatorErrorMsg(string strCompareOperator)
    {
        string strUserFriendlyMsg = "should be equal to";
        switch (strCompareOperator)
        {
            case "Equal":
                strUserFriendlyMsg = "should be equal to";
                break;
            case "DataTypeCheck":
                strUserFriendlyMsg = "should be same data type of";
                break;
            case "GreaterThan":
                strUserFriendlyMsg = "should be greater than";
                break;
            case "GreaterThanEqual":
                strUserFriendlyMsg = "should be greater than or equal to";
                break;
            case "LessThan":
                strUserFriendlyMsg = "should be less than";
                break;
            case "LessThanEqual":
                strUserFriendlyMsg = "should be less than or equal to";
                break;
            case "NotEqual":
                strUserFriendlyMsg = "should not be equal to";
                break;
            default:
                strUserFriendlyMsg = "should be equal to";
                break;

        }

        return strUserFriendlyMsg;

    }

    public static bool IsEmailFormatOK(string inputEmail)
    {
        if (inputEmail == null || inputEmail.Length == 0)
        {
            return false;
        }

        const string expression = "^([a-zA-Z0-9_\\-\\.])+@(([0-2]?[0-5]?[0-5]\\.[0-2]?[0-5]?[0-5]\\.[0-2]?[0-5]?[0-5]\\.[0-2]?[0-5]?[0-5])|((([a-zA-Z0-9\\-])+\\.)+([a-zA-Z\\-])+))$";

        Regex regex = new Regex(expression);
        return regex.IsMatch(inputEmail);
    }

    public static List<string> GetColumnStringListFromTable(DataTable theDatatable)
    {

        List<string> list = new List<string>();

        for (int i = 0; i < theDatatable.Columns.Count; i++)
        {
            list.Add(theDatatable.Columns[i].ColumnName);

        }


        return list;

    }
    public static ListItemCollection GetColumnsFromTable(DataTable theDatatable)
    {

        ListItemCollection list = new ListItemCollection();
        // ArrayList list = new ArrayList();
        string sItem = "";
        for (int i = 0; i < theDatatable.Columns.Count; i++)
        {
            string sColumn = theDatatable.Columns[i].ColumnName;
            string sValue = "";
            if (theDatatable.Rows.Count > 0)
                sValue = theDatatable.Rows[0][i].ToString();
            if (sValue.Length > 20) sValue = String.Concat(sValue.Substring(0, 20), "...");
            if (sValue.Length > 0)
                sItem = string.Concat(sColumn, " (e.g. ", sValue, ")");
            else
                sItem = sColumn;
            list.Add(new System.Web.UI.WebControls.ListItem(sItem, sColumn));
        }


        return list;

    }

    public static ListItemCollection GetColumnsFromTableNoEG(DataTable theDatatable)
    {

        ListItemCollection list = new ListItemCollection();
        // ArrayList list = new ArrayList();
        string sItem = "";
        for (int i = 0; i < theDatatable.Columns.Count; i++)
        {
            string sColumn = theDatatable.Columns[i].ColumnName;
            string sValue = "";
            if (theDatatable.Rows.Count > 0)
                sValue = theDatatable.Rows[0][i].ToString();
            if (sValue.Length > 20) sValue = String.Concat(sValue.Substring(0, 20), "...");
            if (sValue.Length > 0)
                sItem = string.Concat(sColumn, " (e.g. ", sValue, ")");
            else
                sItem = sColumn;
            list.Add(new System.Web.UI.WebControls.ListItem(sItem, sColumn));
        }


        return list;

    }
    public static List<string> GetTokensFromTable(DataTable theDatatable)
    {

        List<string> list = new List<string>();

        for (int i = 0; i < theDatatable.Columns.Count; i++)
        {
            list.Add("[" + theDatatable.Columns[i].ColumnName + "]");
        }


        return list;

    }

    static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
    public static string SizeSuffix(double value)
    {
        int mag = (int)Math.Log(value, 1024);
        decimal adjustedSize = (decimal)value / (1 << (mag * 10));

        return string.Format("{0:n1} {1}", adjustedSize, SizeSuffixes[mag]);
    }

    public static void SendSingleEmail(string strTo, string strHeading, string strBody, ref string strError)
    {



        strError = "";

        string strEmail = SystemData.SystemOption_ValueByKey_Account("EmailFrom", null, null);
        string strEmailServer = SystemData.SystemOption_ValueByKey_Account("EmailServer", null, null);
        string strEmailUserName = SystemData.SystemOption_ValueByKey_Account("EmailUserName", null, null);
        string strEmailPassword = SystemData.SystemOption_ValueByKey_Account("EmailPassword", null, null);
        string strEnableSSL = SystemData.SystemOption_ValueByKey_Account("EnableSSL", null, null);
        string strSmtpPort = SystemData.SystemOption_ValueByKey_Account("SmtpPort", null, null);




        //strBody = strBody.Replace("[Table]", theTable.TableName);



        MailMessage msg = new MailMessage();
        msg.From = new MailAddress(strEmail);


        msg.Subject = strHeading;

        msg.IsBodyHtml = true;

        msg.Body = strBody;


        SmtpClient smtpClient = new SmtpClient(strEmailServer);
        smtpClient.Timeout = 99999;
        smtpClient.Credentials = new System.Net.NetworkCredential(strEmailUserName, strEmailPassword);
        smtpClient.EnableSsl = bool.Parse(strEnableSSL);
        smtpClient.Port = int.Parse(strSmtpPort);

        msg.To.Clear();
        msg.To.Add(strTo);

        try
        {



#if (!DEBUG)
            smtpClient.Send(msg);
#endif


        }
        catch (Exception ex)
        {

            strError = "Server could not send this email, please try again.";
            ErrorLog theErrorLog = new ErrorLog(null, "SendSingleEmail--" + strError, ex.Message, ex.StackTrace, DateTime.Now, "Common.SendSingleEmail");
            SystemData.ErrorLog_Insert(theErrorLog);
        }





    }


    //public static void SendSingleEmail(string strTo, Content theContent, ref string strError)
    //{

    //    if (theContent == null)
    //    {
    //        return;
    //    }

    //    strError = "";

    //    string strEmail = SystemData.SystemOption_ValueByKey("EmailFrom");
    //    string strEmailServer = SystemData.SystemOption_ValueByKey("EmailServer");
    //    string strEmailUserName = SystemData.SystemOption_ValueByKey("EmailUserName");
    //    string strEmailPassword = SystemData.SystemOption_ValueByKey("EmailPassword");
    //    string strWarningSMSEMail = SystemData.SystemOption_ValueByKey("WarningSMSEmail");
    //    string strEnableSSL = SystemData.SystemOption_ValueByKey("EnableSSL");
    //    string strSmtpPort = SystemData.SystemOption_ValueByKey("SmtpPort");



    //    string strBody = theContent.ContentP;


    //    //strBody = strBody.Replace("[Table]", theTable.TableName);



    //    MailMessage msg = new MailMessage();
    //    msg.From = new MailAddress(strEmail);


    //    msg.Subject = theContent.Heading;

    //    msg.IsBodyHtml = true;

    //    msg.Body = strBody;


    //    SmtpClient smtpClient = new SmtpClient(strEmailServer);
    //    smtpClient.Timeout = 99999;
    //    smtpClient.Credentials = new System.Net.NetworkCredential(strEmailUserName, strEmailPassword);
    //    smtpClient.EnableSsl = bool.Parse(strEnableSSL);
    //    smtpClient.Port = int.Parse(strSmtpPort);

    //    msg.To.Clear();
    //    msg.To.Add(strTo);

    //    try
    //    {



    //        #if (!DEBUG)
    //         smtpClient.Send(msg);
    //        #endif



    //         if (System.Web.HttpContext.Current.Session["AccountID"] != null)
    //         {

    //             SecurityManager.Account_SMS_Email_Count(int.Parse(System.Web.HttpContext.Current.Session["AccountID"].ToString()), true, null, null, null);
    //         }


    //         if (System.Web.HttpContext.Current.Session["AccountID"] != null)
    //         {
    //             if (msg.To.Count > 0)
    //             {
    //                 Guid guidNew = Guid.NewGuid();
    //                 string strEmailUID = guidNew.ToString();

    //                 EmailLog theEmailLog = new EmailLog(null, int.Parse(System.Web.HttpContext.Current.Session["AccountID"].ToString()), msg.Subject,
    //                   msg.To[0].ToString(), DateTime.Now, null,
    //                   null,
    //                   theContent.ContentKey, msg.Body);

    //                 theEmailLog.EmailUID = strEmailUID;
    //                 EmailAndIncoming.dbg_EmailLog_Insert(theEmailLog, null, null);


    //             }
    //         }

    //    }
    //    catch (Exception)
    //    {

    //        strError = "Server could not send this email, please try again.";
    //    }





    //}

    public static bool IsResponsive()
    {
        string strResponsive = "";
        if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session["AccountID"] != null)
        {
            strResponsive = Common.GetValueFromSQL("SELECT MasterPage FROM Account WHERE AccountID=" + System.Web.HttpContext.Current.Session["AccountID"].ToString() + " AND MasterPage LIKE '%Responsive%'");

        }
        bool bResponsive = false;
        if (strResponsive != "")
        {
            bResponsive = true;
        }
        return bResponsive;
    }
    public static string ReplaceDataFiledByValue(DataTable theDatatable, string strContent)
    {

        for (int i = 0; i < theDatatable.Columns.Count; i++)
        {

            strContent = strContent.Replace("[" + theDatatable.Columns[i].ColumnName + "]", theDatatable.Rows[0][i].ToString());

        }

        return strContent;
    }


    //public static void GenerateWORDDoc(string documentFileName,DataTable theDatatable, out string error)
    //{
    //    error = String.Empty;

    //    using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open(documentFileName, true))
    //    {
    //        string path = System.IO.Path.GetDirectoryName(documentFileName);
    //        MainDocumentPart mainPart = wordprocessingDocument.MainDocumentPart;
    //        DocumentFormat.OpenXml.Wordprocessing.Document document = mainPart.Document;
    //        IEnumerable<Text> texts = document.Body.Descendants<Text>();
    //        string strPreValue = "";
    //        foreach (Text text in texts)
    //        {
    //            //Console.WriteLine(text.Text);

    //            for (int i = 0; i < theDatatable.Columns.Count; i++)
    //            {

    //                if (text.Text == "[" + theDatatable.Columns[i].ColumnName + "]")
    //                    text.Text = theDatatable.Rows[0][i].ToString();

    //                if (strPreValue == "[")
    //                {
    //                    if ("[" + text.Text + "]" == "[" + theDatatable.Columns[i].ColumnName + "]")
    //                        text.Text = theDatatable.Rows[0][i].ToString();
    //                }
    //            }

    //            strPreValue = text.Text;

    //            if (text.Text == "[")
    //                text.Text = "";

    //            if (text.Text == "]")
    //                text.Text = "";

    //        }
    //    }
    //    //Console.ReadLine();
    //}


    //oliver <begin> Ticket 1451
    public static void GenerateWORDDoc2(string documentFileName, DataTable theDatatable, out string error)
    {
        error = String.Empty;


        using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open(documentFileName, true))
        {
            string path = System.IO.Path.GetDirectoryName(documentFileName);
            MainDocumentPart mainPart = wordprocessingDocument.MainDocumentPart;
            DocumentFormat.OpenXml.Wordprocessing.Document document = mainPart.Document;
            IEnumerable<Paragraph> paragraphs = document.Body.Descendants<Paragraph>();
            foreach (Paragraph paragraph in paragraphs)
            {
                IEnumerable<Run> runs = from x in paragraph.Descendants<Run>()
                                        where (from y in x.Descendants<Text>()
                                               where y.Text.Contains('«')
                                               select y).Any()
                                        select x;

                foreach (Run run in runs)
                {
                    Text text = run.Descendants<Text>().FirstOrDefault();
                    int startPos = text.Text.IndexOf('«');
                    int endPos = text.Text.IndexOf('»', startPos);
                    if (endPos != -1)
                    {
                        //Console.WriteLine(text.Text.Substring(startPos, endPos - startPos + 1));
                        string newText = null;
                        if (TryReplace2(text.Text, out newText, theDatatable))
                            text.Text = newText;
                    }
                    else
                    {
                        List<Run> runsToDelete = new List<Run>();
                        string s = text.Text;
                        Run nextRun = run;
                        while (nextRun != null)
                        {
                            nextRun = nextRun.NextSibling<Run>();
                            if (nextRun != null)
                            {
                                runsToDelete.Add(nextRun);
                                Text nextText = nextRun.Descendants<Text>().FirstOrDefault();
                                if (nextText != null)
                                {
                                    s = s + nextText.Text;
                                    if (nextText.Text.IndexOf('»', startPos) != -1)
                                        break;
                                }
                            }
                        }
                        //Console.WriteLine(s);
                        string newText = null;
                        if (TryReplace2(s, out newText, theDatatable))
                        {
                            text.Text = newText;
                            foreach (Run x in runsToDelete)
                                paragraph.RemoveChild<Run>(x);
                        }
                    }
                }
            }
        }

    }
    //oliver <end>

    //oliver <begin> Ticket 1451
    static private bool TryReplace2(string oldText, out string newText, DataTable theDatatable)
    {
        newText = string.Empty;

        for (int i = 0; i < theDatatable.Columns.Count; i++)
        {

            if (oldText.Contains("«" + theDatatable.Columns[i].ColumnName + "»"))
            {
                newText = oldText.Replace("«" + theDatatable.Columns[i].ColumnName + "»", theDatatable.Rows[0][i].ToString());
                return true;
            }
        }

        return false;

    }
    //oliver <end> Ticket 1451

    public static void GenerateWORDDoc(string documentFileName, DataTable theDatatable, out string error)
    {
        error = String.Empty;


        using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open(documentFileName, true))
        {
            string path = System.IO.Path.GetDirectoryName(documentFileName);
            MainDocumentPart mainPart = wordprocessingDocument.MainDocumentPart;
            DocumentFormat.OpenXml.Wordprocessing.Document document = mainPart.Document;
            IEnumerable<Paragraph> paragraphs = document.Body.Descendants<Paragraph>();
            foreach (Paragraph paragraph in paragraphs)
            {
                IEnumerable<Run> runs = from x in paragraph.Descendants<Run>()
                                        where (from y in x.Descendants<Text>()
                                               where y.Text.Contains('[')
                                               select y).Any()
                                        select x;

                foreach (Run run in runs)
                {
                    Text text = run.Descendants<Text>().FirstOrDefault();
                    int startPos = text.Text.IndexOf('[');
                    int endPos = text.Text.IndexOf(']', startPos);
                    if (endPos != -1)
                    {
                        //Console.WriteLine(text.Text.Substring(startPos, endPos - startPos + 1));
                        string newText = null;
                        if (TryReplace(text.Text, out newText, theDatatable))
                            text.Text = newText;
                    }
                    else
                    {
                        List<Run> runsToDelete = new List<Run>();
                        string s = text.Text;
                        Run nextRun = run;
                        while (nextRun != null)
                        {
                            nextRun = nextRun.NextSibling<Run>();
                            if (nextRun != null)
                            {
                                runsToDelete.Add(nextRun);
                                Text nextText = nextRun.Descendants<Text>().FirstOrDefault();
                                if (nextText != null)
                                {
                                    s = s + nextText.Text;
                                    if (nextText.Text.IndexOf(']', startPos) != -1)
                                        break;
                                }
                            }
                        }
                        //Console.WriteLine(s);
                        string newText = null;
                        if (TryReplace(s, out newText, theDatatable))
                        {
                            text.Text = newText;
                            foreach (Run x in runsToDelete)
                                paragraph.RemoveChild<Run>(x);
                        }
                    }
                }
            }
        }

    }


    static private bool TryReplace(string oldText, out string newText, DataTable theDatatable)
    {
        newText = string.Empty;

        for (int i = 0; i < theDatatable.Columns.Count; i++)
        {

            if (oldText.Contains("[" + theDatatable.Columns[i].ColumnName + "]"))
            {
                newText = oldText.Replace("[" + theDatatable.Columns[i].ColumnName + "]", theDatatable.Rows[0][i].ToString());
                return true;
            }
        }

        return false;

    }

    public static string ReplaceDataFiledByValueWORD(DataTable theDatatable, string strContent)
    {

        for (int i = 0; i < theDatatable.Columns.Count; i++)
        {

            strContent = strContent.Replace("[" + theDatatable.Columns[i].ColumnName + "]", theDatatable.Rows[0][i].ToString());

        }

        return strContent;
    }
    public static string StripTagsCharArray(string HTMLText)
    {
        string strR = "";
        using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand comm = new SqlCommand("dbo.udf_StripHTML", conn))
            {
                comm.CommandType = CommandType.StoredProcedure;

                SqlParameter p1 = new SqlParameter("@HTMLText", SqlDbType.VarChar);
                // You can call the return value parameter anything, .e.g. "@Result".
                SqlParameter p2 = new SqlParameter("@Result", SqlDbType.VarChar);

                p1.Direction = ParameterDirection.Input;
                p2.Direction = ParameterDirection.ReturnValue;

                p1.Value = HTMLText;

                comm.Parameters.Add(p1);
                comm.Parameters.Add(p2);

                conn.Open();

                try
                {
                    comm.ExecuteNonQuery();

                    if (p2.Value != DBNull.Value)
                        strR = (string)p2.Value;
                }
                catch
                {

                }
                conn.Close();
                conn.Dispose();

            }
        }
        return strR;
    }
    //public static string StripTagsCharArray(string source)
    //{
    //    char[] array = new char[source.Length];
    //    int arrayIndex = 0;
    //    bool inside = false;

    //    for (int i = 0; i < source.Length; i++)
    //    {
    //        char let = source[i];
    //        if (let == '<')
    //        {
    //            inside = true;
    //            continue;
    //        }
    //        if (let == '>')
    //        {
    //            inside = false;
    //            continue;
    //        }
    //        if (!inside)
    //        {
    //            array[arrayIndex] = let;
    //            arrayIndex++;
    //        }
    //    }
    //    return new string(array, 0, arrayIndex);
    //}









    //Then you can  override Set function, eg :

    //public override void SetCompanyDetail()
    //    {
    //        //base.SetCompanyDetail();
    //        if (DataSource.CompanyDetailSpecified)
    //        {
    //            string strCompanyDetail = DBGurus.StripTagsCharArray(DataSource.CompanyDetail);
    //            string param = string.Format("<key><cv><c>CompanyID<%2fc><v>{0}<%2fv><%2fcv><%2fkey>",DataSource.CompanyID);
    //            string sLink = string.Format("<a href=\"javascript:void()\" onclick=\"SelectCompany('{0}')\" > more >></a>", ViewButton.ClientID);
    //            if (strCompanyDetail.Length > 500)
    //            {
    //                string sCompanyDetail = strCompanyDetail.Substring(500);
    //                sCompanyDetail = sCompanyDetail.Substring(0, sCompanyDetail.IndexOf(" "));
    //                strCompanyDetail = strCompanyDetail.Substring(0, 500) + sCompanyDetail + "...";
    //            }

    //            CompanyDetail.Text = string.Format("{0} {1}", strCompanyDetail, sLink);
    //        }

    //    }












    public static byte[] ReadFile(string sPath)
    {
        //Initialize byte array with a null value initially.
        byte[] data = null;

        //Use FileInfo object to get file size.
        FileInfo fInfo = new FileInfo(sPath);
        long numBytes = fInfo.Length;

        //Open FileStream to read file
        FileStream fStream = new FileStream(sPath, FileMode.Open,
                                                FileAccess.Read);

        //Use BinaryReader to read file stream into byte array.
        BinaryReader br = new BinaryReader(fStream);

        //When you use BinaryReader, you need to 

        //supply number of bytes to read from file.
        //In this case we want to read entire file. 

        //So supplying total number of bytes.
        data = br.ReadBytes((int)numBytes);
        return data;
    }


    public static bool IsIn(string sEach, string sAll)
    {
        foreach (string sE in sAll.Split(','))
        {
            if (sE.Length > 0)
            {
                if (sE == sEach)
                {
                    return true;
                }
            }
        }
        return false;
    }
    public static bool HaveAccess(string strUserRoleTypes, string strAllowedRoleTypes)
    {
        foreach (string strRoleType in strAllowedRoleTypes.Split(','))
        {
            if (strRoleType.Length > 0)
            {
                if (strUserRoleTypes.Contains(strRoleType))
                {
                    return true;
                }
            }
        }
        return false;
    }

    //public static bool IsUserInThisRoleTypes(User ObjUser,string strRoleTypes)
    //{
    //    bool kq = false;
    //    int iTN = 0;

    //    List<UserRole> uroles = SecurityManager.UserRole_Select(null, ObjUser.UserID, null, null, null, "", "", null, null, ref iTN);


    //    foreach (UserRole item in uroles)
    //    {
    //        if (strRoleTypes.Contains(item.RoleType))
    //        {
    //            kq = true;
    //        }
    //    }

    //    ObjUser = null;

    //    return kq;
    //}


    public static string TrafficLightURL(Column theTrafficLightColumn, string strTLValue, string strXML)
    {


        string strImageURL = "";

        try
        {

            if (strTLValue != "" && strXML != "")
            {


                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(strXML);

                XmlTextReader r = new XmlTextReader(new StringReader(xmlDoc.OuterXml));

                DataSet ds = new DataSet();
                ds.ReadXml(r);

                bool bhasCompare = false;
                if (ds.Tables[0] != null)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (dr[0].ToString().IndexOf("____") > -1)
                        {
                            bhasCompare = true;
                            break;
                        }
                    }

                }

                if (bhasCompare)
                {

                    if (ds.Tables[0] != null)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            string strEachValue = dr[0].ToString();

                            string strLowerTL = "";
                            string strUpperTL = "";

                            if (strEachValue.IndexOf("____") > -1)
                            {
                                strLowerTL = strEachValue.Substring(0, strEachValue.IndexOf("____"));
                                strUpperTL = strEachValue.Substring(strEachValue.IndexOf("____") + 4);
                            }

                            if (theTrafficLightColumn.ColumnType == "number")
                            {
                                if (strLowerTL != "" && strUpperTL != "")
                                {
                                    if (int.Parse(strTLValue) >= int.Parse(strLowerTL)
                                        && int.Parse(strTLValue) <= int.Parse(strUpperTL))
                                    {
                                        strImageURL = dr[1].ToString();
                                        break;
                                    }
                                }
                                else if (strLowerTL == "" && strUpperTL != "")
                                {
                                    if (int.Parse(strTLValue) <= int.Parse(strUpperTL))
                                    {
                                        strImageURL = dr[1].ToString();
                                        break;
                                    }
                                }
                                else if (strLowerTL != "" && strUpperTL == "")
                                {
                                    if (int.Parse(strTLValue) >= int.Parse(strLowerTL))
                                    {
                                        strImageURL = dr[1].ToString();
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                //date

                                DateTime? dtTLLowwer = null;
                                DateTime? dtTLUpper = null;
                                DateTime dtTLValue;
                                DateTime.TryParseExact(strTLValue.Trim(), Common.Dateformats, new CultureInfo("en-GB"),
                                         DateTimeStyles.None, out dtTLValue);
                                if (strLowerTL != "")
                                {
                                    DateTime dtTemp;
                                    if (DateTime.TryParseExact(strLowerTL.Trim(), Common.Dateformats, new CultureInfo("en-GB"),
                                         DateTimeStyles.None, out dtTemp))
                                    {
                                        dtTLLowwer = dtTemp;
                                    }
                                }

                                if (strUpperTL != "")
                                {
                                    DateTime dtTemp;
                                    if (DateTime.TryParseExact(strUpperTL.Trim(), Common.Dateformats, new CultureInfo("en-GB"),
                                         DateTimeStyles.None, out dtTemp))
                                    {
                                        dtTLUpper = dtTemp;
                                    }
                                }

                                if (dtTLLowwer != null && dtTLUpper != null)
                                {
                                    if (dtTLValue >= dtTLLowwer
                                        && dtTLValue <= dtTLUpper)
                                    {
                                        strImageURL = dr[1].ToString();
                                        break;
                                    }
                                }
                                if (dtTLLowwer == null && dtTLUpper != null)
                                {
                                    if (dtTLValue <= dtTLUpper)
                                    {
                                        strImageURL = dr[1].ToString();
                                        break;
                                    }
                                }
                                if (dtTLLowwer != null && dtTLUpper == null)
                                {
                                    if (dtTLValue >= dtTLLowwer)
                                    {
                                        strImageURL = dr[1].ToString();
                                        break;
                                    }
                                }


                            }

                        }
                    }


                }
                else
                {
                    if (ds.Tables[0] != null)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if (dr[0].ToString().ToLower() == strTLValue.ToLower())
                            {
                                strImageURL = dr[1].ToString();
                                break;
                            }
                        }

                    }
                }


            }

        }
        catch
        {
            //
        }


        return strImageURL;

    }

    //By JV - Look up for the link record id base on user input
    public static string GetLinkedIDFromDisplayText(int nTableTableID, string strDisplayColumn, string strLinkValue)
    {
        strLinkValue = strLinkValue.Replace("'", "''"); //Red: lets use this to lessen sql process
        DataTable dtData = Common.spGetLinkedRecordIDnDisplayText(strDisplayColumn, null, nTableTableID, null, "", "", strLinkValue);
        if (dtData != null && dtData.Rows.Count > 0)
        {
            DataRow[] dtRow = dtData.Select("DisplayText = '" + strLinkValue + "'");
            if (dtRow != null && dtRow.Length > 0)
            {
                string strlinkValRecordID = dtRow[0][0].ToString();
                return strlinkValRecordID;
            }
        }
        return "";
    }

    public static string GetLinkedDisplayText(string sDisplayColumn, int nTableTableID, int? nMaxRow,
        string WhereEqual, string DisplayPredict)
    {
        string strDisplayText = "";

        DataTable dtData = Common.spGetLinkedRecordIDnDisplayText(sDisplayColumn, null, nTableTableID, null, WhereEqual, "", "");//3611
        if (dtData != null && dtData.Rows.Count > 0)
        {
            if (dtData.Rows[0][1] != DBNull.Value)
            {
                strDisplayText = dtData.Rows[0][1].ToString();
            }
        }
        return strDisplayText;
    }


    public static DataTable spForSpecificDataToDropDown(int? iRecordID, string nTableIDOne, string nTableIDTwo, string ColumnIDONe,
        string ColumnIDTwo, string sWhere, string strSearch, string UserID)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("sp_Renzo_ForSpecificDataToDropDown", connection)) //spExportAllTables
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = DBGurus.GetCommandTimeout();

                if (iRecordID != null)
                    command.Parameters.Add(new SqlParameter("@RecordID", iRecordID));

                if (nTableIDOne != "")
                    command.Parameters.Add(new SqlParameter("@TableIDOne", int.Parse(nTableIDOne)));
                if (nTableIDTwo.ToString() != "")
                    command.Parameters.Add(new SqlParameter("@TableIDTwo", nTableIDTwo));
                if (ColumnIDONe != "")
                    command.Parameters.Add(new SqlParameter("@ColumnIDOne", int.Parse(ColumnIDONe)));
                if (ColumnIDTwo != "")
                    command.Parameters.Add(new SqlParameter("@ColumnIDTwo", int.Parse(ColumnIDTwo)));
                if (sWhere != "")
                    command.Parameters.Add(new SqlParameter("@swhere", sWhere));
                if (UserID != "")
                    command.Parameters.Add(new SqlParameter("@UserID", int.Parse(UserID)));
                if (strSearch != "")
                    command.Parameters.Add(new SqlParameter("@Search", strSearch));

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();


                connection.Open();
                try
                {
                    da.Fill(dt);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();

                return dt;
            }
        }
    }



    public static DataTable spGetLinkedRecordIDnDisplayText(string sDisplayColumn, int? nColumnID, int nTableTableID, int? nMaxRow,
       string WhereEqualTextSearch, string WhereEqualParentSearch, string DisplayPredict)
    {
        //pass also the displaycolumn and the columnid
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("spGetLinkedRecordIDnDisplayText", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = DBGurus.GetCommandTimeout();


                command.Parameters.Add(new SqlParameter("@sDisplayColumn", sDisplayColumn));
                command.Parameters.Add(new SqlParameter("@nTableTableID", nTableTableID));

                if (nColumnID != null)
                    command.Parameters.Add(new SqlParameter("@nColumnID", nColumnID));


                if (WhereEqualTextSearch != "")
                    command.Parameters.Add(new SqlParameter("@WhereEqualTextSearch", WhereEqualTextSearch));

                if (WhereEqualParentSearch != "")
                    command.Parameters.Add(new SqlParameter("@WhereEqualParentSearch", WhereEqualParentSearch));

                if (DisplayPredict != "")
                    command.Parameters.Add(new SqlParameter("@DisplayPredict", DisplayPredict));

                if (nMaxRow != null)
                    command.Parameters.Add(new SqlParameter("@nMaxRow", nMaxRow));

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();


                connection.Open();
                try
                {
                    da.Fill(dt);
                }
                catch (Exception ex)
                {
                    //ErrorLog theErrorLog = new ErrorLog(null, "ets_SP_Load DDl", ex.Message, ex.StackTrace, DateTime.Now, "");
                    //SystemData.ErrorLog_Insert(theErrorLog);
                }
                connection.Close();
                connection.Dispose();

                return dt;
            }
        }
    }


    public static string GetSQLOperator(string strTextOperator)
    {
        string strSQLOperator = "=";
        switch (strTextOperator.ToLower())
        {
            case "equals":
                strSQLOperator = "=";
                break;
            case "equal":
                strSQLOperator = "=";
                break;
            case "greaterthan":
                strSQLOperator = ">";
                break;
            case "greaterthanequal":
                strSQLOperator = ">=";
                break;
            case "lessthan":
                strSQLOperator = "<";
                break;
            case "lessthanequal":
                strSQLOperator = "<=";
                break;
            case "notequal":
                strSQLOperator = "<>";
                break;
            case "contains":
                strSQLOperator = "IN";
                break;
            case "notcontains":
                strSQLOperator = "NOT IN";
                break;
            default:
                strSQLOperator = "=";
                break;

        }
        return strSQLOperator;
    }

    public static bool IsDataValidCommon(string sColumnType, string sData, string strOperator, string sComparevalue)
    {
        try
        {

            if (sData == "")
            {
                if (strOperator.ToLower() == "empty")
                {
                    return true;
                }
                if (strOperator.ToLower() == "notempty")
                {
                    return false;
                }
            }
            else
            {
                if (strOperator.ToLower() == "empty")
                {
                    return false;
                }
                if (strOperator.ToLower() == "notempty")
                {
                    return true;
                }
            }


            if (sColumnType == "number" || sColumnType == "calculation")
            {
                if (sColumnType == "calculation")
                {
                    if (sData != "" && sData.IndexOf("year") > -1 && sData.IndexOf(" ") > -1)
                    {
                        sData = sData.Substring(0, sData.IndexOf(" "));
                    }
                }


                double dTemp = 0;
                double dData;
                double dComparevalue;
                if (double.TryParse(sData, out dTemp))
                {
                    dData = dTemp;
                }
                else
                {
                    return false;
                }

                if (double.TryParse(sComparevalue, out dTemp))
                {
                    dComparevalue = dTemp;
                }
                else
                {
                    return false;
                }

                bool bResult = false;
                switch (strOperator.ToLower())
                {
                    case "equals":
                    case "equal":
                        bResult = dData == dComparevalue;
                        break;
                    case "greaterthan":
                        bResult = dData > dComparevalue;
                        break;
                    case "greaterthanequal":
                        bResult = dData >= dComparevalue;
                        break;
                    case "lessthan":
                        bResult = dData < dComparevalue;
                        break;
                    case "lessthanequal":
                        bResult = dData <= dComparevalue;
                        break;
                    case "notequal":
                        bResult = dData != dComparevalue;
                        break;
                    default:
                        bResult = dData == dComparevalue;
                        break;

                }

                return bResult;
            }
            else if (sColumnType == "datetime" || sColumnType == "date" || sColumnType == "time")
            {
                DateTime dTemp;
                DateTime dData;
                DateTime dComparevalue;
                if (DateTime.TryParse(sData, out dTemp))
                {
                    dData = dTemp;
                }
                else
                {
                    return false;
                }

                if (DateTime.TryParse(sComparevalue, out dTemp))
                {
                    dComparevalue = dTemp;
                }
                else
                {
                    return false;
                }

                bool bResult = false;

                switch (strOperator.ToLower())
                {
                    case "equals":
                    case "equal":
                        bResult = dData == dComparevalue;
                        break;
                    case "greaterthan":
                        bResult = dData > dComparevalue;
                        break;
                    case "greaterthanequal":
                        bResult = dData >= dComparevalue;
                        break;
                    case "lessthan":
                        bResult = dData < dComparevalue;
                        break;
                    case "lessthanequal":
                        bResult = dData <= dComparevalue;
                        break;
                    case "notequal":
                        bResult = dData != dComparevalue;
                        break;
                    default:
                        bResult = dData == dComparevalue;
                        break;

                }

                return bResult;
            }
            else
            {
                sData = sData.ToLower();
                sComparevalue = sComparevalue.ToLower();
                bool bResult = false;
                switch (strOperator.ToLower())
                {
                    case "equals":
                    case "equal":
                        bResult = sData == sComparevalue;
                        break;
                    case "greaterthan":
                        bResult = sData.Length > sComparevalue.Length;
                        break;
                    case "greaterthanequal":
                        bResult = sData.Length >= sComparevalue.Length;
                        break;
                    case "lessthan":
                        bResult = sData.Length < sComparevalue.Length;
                        break;
                    case "lessthanequal":
                        bResult = sData.Length <= sComparevalue.Length;
                        break;
                    case "notequal":
                        bResult = sData != sComparevalue;
                        break;
                    case "contains":
                        //listbox,dropdown,

                        bResult = sData.IndexOf(sComparevalue) > -1;
                        break;
                    case "notcontains":
                        //listbox,dropdown,
                        bResult = sData.IndexOf(sComparevalue) == -1;
                        break;
                    default:
                        bResult = sData == sComparevalue;
                        break;

                }
                return bResult;
            }



        }
        catch
        {
            //
        }

        return false;
    }


    public static DataTable DataTableFromText(string strCommandText)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("dbg_DataTable_FromSQL", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = DBGurus.GetCommandTimeout();

                command.Parameters.Add(new SqlParameter("@sSQL", strCommandText));

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();

                connection.Open();
                try
                {
                    da.Fill(dt);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();

                return dt;


            }

        }


    }




    public static void AddOneSysRecord(int iTableID, int iEnteredBy, string strSystemName, string strValue)
    {
        ExecuteText("INSERT INTO Record (TableID,EnteredBy," + strSystemName.Trim().ToUpper() + ") VALUES (" + iTableID.ToString() + "," + iEnteredBy.ToString() + ",'" + strValue.Replace("'", "''") + "')");
    }



    public static int ExecuteText(string strCommandText)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand())
            {
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "dbg_EXEC_SQL";
                command.CommandTimeout = DBGurus.GetCommandTimeout();
                command.Parameters.Add(new SqlParameter("@sSQL", strCommandText));


                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //
                }

                connection.Close();
                connection.Dispose();

                return 1;

            }
        }
    }
    public static long GetObjectSize(object o)
    {


        long size = 0;

        using (Stream s = new MemoryStream())
        {
            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
                formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            formatter.Serialize(s, o);
            size = s.Length;
        }

        return size;
    }
    //public static string GetValueFromSQL(string strSQL)
    //{

    //    string strValue = "";


    //    using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
    //    {
    //        using (SqlCommand command = new SqlCommand(strSQL, connection))
    //        {
    //            command.CommandType = CommandType.Text;

    //            connection.Open();
    //            try
    //            {
    //                using (SqlDataReader reader = command.ExecuteReader())
    //                {
    //                    while (reader.Read())
    //                    {
    //                        if (reader[0] != DBNull.Value)
    //                        {
    //                            strValue = reader[0].ToString();
    //                            break;
    //                        }
    //                    }
    //                }                
    //            }
    //            catch
    //            {
    //               //
    //            }
    //            connection.Close();
    //            connection.Dispose();
    //        }
    //    }

    //    return strValue;

    //}



    public static string GetValueFromSQL(string strSQL)
    {

        string strValue = "";


        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("dbg_DataTable_FromSQL", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = DBGurus.GetCommandTimeout();

                command.Parameters.Add(new SqlParameter("@sSQL", strSQL));

                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader[0] != DBNull.Value)
                            {
                                strValue = reader[0].ToString();
                                break;
                            }
                        }
                    }
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();
            }
        }

        return strValue;

    }



    public static string Get_Comma_Sep_IDs(string sIDName, string sTableName, string sWHERE)
    {

        string strValue = "";


        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Get_Comma_Sep_IDs", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@sIDName", sIDName));
                command.Parameters.Add(new SqlParameter("@sTableName", sTableName));
                command.Parameters.Add(new SqlParameter("@sWHERE", sWHERE));

                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            if (reader[0] != DBNull.Value)
                            {
                                strValue = reader[0].ToString();
                                break;
                            }
                        }
                    }
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();
            }
        }

        return strValue;

    }



    public static bool IsDataValid(string strData, string strExpression)
    {
        if (strData == null || strData.Length == 0)
        {
            return false;
        }

        Regex regex = new Regex(strExpression);
        return regex.IsMatch(strData);
    }


    public static string RemoveSpecialCharacters(string str)
    {

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.Length; i++)
        {
            if ((int)str[i] >= 32 && (int)str[i] <= 127)
            {
                //63=?,35=#,46=.,91=[,93=],37=%
                if ((int)str[i] != 63 && (int)str[i] != 35 && (int)str[i] != 46
                     && (int)str[i] != 91 && (int)str[i] != 93) //&& (int)str[i] != 37
                {
                    sb.Append(str[i]);
                }
                if ((int)str[i] == 91)
                {
                    sb.Append("(");
                }
                if ((int)str[i] == 93)
                {
                    sb.Append(")");
                }
            }
        }

        return sb.ToString().Trim();
    }

    public static bool isNumeric(string N)
    {
        bool YesNumeric = false;

        if (N != null)
        {
            for (int i = 0; i < N.Length; i++)
            {

                if (char.IsNumber(N[i]))
                    YesNumeric = true;
            }

            if (YesNumeric == true)
            {
                return true;
            }
        }

        return false;
    }


    public static bool isAlpha(string N)
    {
        bool YesNumeric = false;
        bool YesAlpha = false;
        bool BothStatus = false;


        for (int i = 0; i < N.Length; i++)
        {
            if (char.IsLetter(N[i]))
                YesAlpha = true;

            if (char.IsNumber(N[i]))
                YesNumeric = true;
        }

        if (YesAlpha == true && YesNumeric == false)
        {
            BothStatus = true;
        }

        return BothStatus;
    }

    public static bool isAlphaNumeric(string N)
    {
        bool YesNumeric = false;
        bool YesAlpha = false;
        bool BothStatus = false;


        for (int i = 0; i < N.Length; i++)
        {
            if (char.IsLetter(N[i]))
                YesAlpha = true;

            if (char.IsNumber(N[i]))
                YesNumeric = true;
        }

        if (YesAlpha == true && YesNumeric == true)
        {
            BothStatus = true;
        }
        else
        {
            BothStatus = false;
        }
        return BothStatus;
    }



    public static bool HasSymbols(string str)
    {

        StringBuilder sb = new StringBuilder();
        bool bOnlyOneDot = false;
        bool bOnlyOneMinus = false;
        for (int i = 0; i < str.Length; i++)
        {
            bool bSymbol = true;
            if ((int)str[i] >= 48 && (int)str[i] <= 57)
            {

                sb.Append(str[i]);
                bSymbol = false;
            }
            if ((int)str[i] == 46 && bOnlyOneDot == false)
            {
                sb.Append(str[i]);
                bOnlyOneDot = true;
                bSymbol = false;
            }
            if ((int)str[i] == 45 && bOnlyOneMinus == false && sb.Length == 0)
            {
                sb.Append(str[i]);
                bOnlyOneMinus = true;
                bSymbol = false;
            }
            if (bSymbol)
            {
                return true;
            }
        }

        return false;
    }
    public static string IgnoreSymbols(string str)
    {
        //Red Ticket 2188 - to set the string to zero if all are alpha and special char
        if (!isNumeric(str))
        {
            str = "0";
        }

        StringBuilder sb = new StringBuilder();
        bool bOnlyOneDot = false;
        bool bOnlyOneMinus = false;
        for (int i = 0; i < str.Length; i++)
        {
            if ((int)str[i] >= 48 && (int)str[i] <= 57)
            {

                sb.Append(str[i]);

            }
            if ((int)str[i] == 46 && bOnlyOneDot == false)
            {
                sb.Append(str[i]);
                bOnlyOneDot = true;
            }
            if ((int)str[i] == 45 && bOnlyOneMinus == false && sb.Length == 0)
            {
                sb.Append(str[i]);
                bOnlyOneMinus = true;
            }
        }

        return sb.ToString();
    }

    public static byte[] ResizeImageFile(byte[] imageFile, int targetSize)
    {
        using (System.Drawing.Image oldImage = System.Drawing.Image.FromStream(new MemoryStream(imageFile)))
        {
            Size newSize = CalculateDimensions(oldImage.Size, targetSize);
            using (Bitmap newImage = new Bitmap(newSize.Width, newSize.Height, PixelFormat.Format24bppRgb))
            {
                using (Graphics canvas = Graphics.FromImage(newImage))
                {
                    canvas.SmoothingMode = SmoothingMode.AntiAlias;
                    canvas.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    canvas.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    canvas.DrawImage(oldImage, new Rectangle(new Point(0, 0), newSize));
                    MemoryStream m = new MemoryStream();
                    newImage.Save(m, ImageFormat.Jpeg);
                    return m.GetBuffer();
                }
            }
        }
    }


    private static Size CalculateDimensions(Size oldSize, int targetSize)
    {
        Size newSize = new Size();
        if (oldSize.Height > oldSize.Width)
        {
            newSize.Width = (int)(oldSize.Width * ((float)targetSize / (float)oldSize.Height));
            newSize.Height = targetSize;
        }
        else
        {
            newSize.Width = targetSize;
            newSize.Height = (int)(oldSize.Height * ((float)targetSize / (float)oldSize.Width));
        }
        return newSize;
    }

    public static string MobileContentByKey(string key)
    {
        string strContent = "";
        using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            SqlCommand cmd = new SqlCommand(String.Format("SELECT TOP 1 Content FROM Content WHERE ContentKey='{0}' AND AccountID is null", key), conn);
            conn.Open();
            strContent = Convert.ToString(cmd.ExecuteScalar());
            conn.Close();
            conn.Dispose();
        }
        return strContent;
    }




    public static void PutListValues_Text(string strDropdownValues, ref  ListBox lb)
    {
        lb.Items.Clear();
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        string strValue = "";
        string strText = "";

        foreach (string s in result)
        {
            //ListItem liTemp = new ListItem(s, s.ToLower());
            strValue = "";
            strText = "";
            if (s.IndexOf(",") > -1)
            {
                strValue = s.Substring(0, s.IndexOf(","));
                strText = s.Substring(strValue.Length + 1);
                if (strValue != "" && strText != "")
                {
                    System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(strText, strValue);
                    lb.Items.Add(liTemp);
                }
            }
        }


    }

    public static void SetListValues(string strDBValues, ref  ListBox lb)
    {
        if (strDBValues != "")
        {
            string[] strSS = strDBValues.Split(',');
            foreach (string SS in strSS)
            {
                try
                {
                    if (SS != "")
                        lb.Items.FindByValue(SS).Selected = true;
                }
                catch
                {
                    //
                }
            }
        }

    }

    public static string GetListTableDisplay(string sDisplayColumn, int nTableTableID, string WhereEqual, string DisplayPredict)
    {
        string strDisplayText = "";

        DataTable dtData = Common.spGetLinkedRecordIDnDisplayText(sDisplayColumn, null, nTableTableID, null, WhereEqual, "", ""); //3611
        if (dtData != null && dtData.Rows.Count > 0)
        {

            foreach (DataRow dr in dtData.Rows)
            {
                if (dr[1] != DBNull.Value)
                {
                    strDisplayText = strDisplayText + "," + dr[1].ToString();
                }
            }
            if (strDisplayText.Length > 1)
            {
                strDisplayText = strDisplayText.Substring(1);
            }
        }

        return strDisplayText;
    }

    public static void SetListValues_ForTable(string strDBValues, ref  ListBox lb, int iTableTableID, int? iLinkedParentColumnID, string strDisplayColumn)
    {
        if (strDBValues != "")
        {
            //it's a new dev so iLinkedParentColumnID must be RecordID

            DataTable dtParents = Common.spGetLinkedRecordIDnDisplayText(strDisplayColumn, null, iTableTableID, Common.MaxRowForListBoxTable, "", "", ""); //3611


            lb.Items.Clear();

            string[] strSS = strDBValues.Split(',');



            foreach (DataRow dr in dtParents.Rows)
            {

                foreach (string SS in strSS)
                {
                    if (SS == dr[0].ToString())
                    {
                        System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString());
                        lb.Items.Add(liTemp);
                    }
                }


            }

            foreach (DataRow dr in dtParents.Rows)
            {
                if (lb.Items.FindByValue(dr[0].ToString()) == null)
                {
                    System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString());
                    lb.Items.Add(liTemp);
                }
            }
            foreach (string SS in strSS)
            {

                if (SS != "" & lb.Items.FindByValue(SS) != null)
                {
                    lb.Items.FindByValue(SS).Selected = true;
                }


            }



        }

    }


    public static void SetListValues_Text(string strDBValues, ref  ListBox lb, string strDropdownValues)
    {
        if (strDBValues != "")
        {
            lb.Items.Clear();

            string[] strSS = strDBValues.Split(',');

            string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            string strValue = "";
            string strText = "";
            foreach (string s in result)
            {
                strValue = "";
                strText = "";
                if (s.IndexOf(",") > -1)
                {
                    strValue = s.Substring(0, s.IndexOf(","));
                    strText = s.Substring(strValue.Length + 1);
                }

                foreach (string SS in strSS)
                {
                    if (SS == strValue)
                    {
                        System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(strText, strValue);
                        lb.Items.Add(liTemp);
                    }
                }


            }

            foreach (string s in result)
            {
                strValue = "";
                strText = "";

                if (s.IndexOf(",") > -1)
                {
                    strValue = s.Substring(0, s.IndexOf(","));
                    strText = s.Substring(strValue.Length + 1);
                }

                if (lb.Items.FindByValue(strValue) == null)
                {
                    System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(strText, strValue);
                    lb.Items.Add(liTemp);
                }
            }
            foreach (string SS in strSS)
            {
                try
                {
                    if (SS != "")
                        lb.Items.FindByValue(SS).Selected = true;
                }
                catch
                {
                    //
                }
            }



        }

    }

    public static void SetListValues(string strDBValues, ref  ListBox lb, string strDropdownValues)
    {
        if (strDBValues != "")
        {
            lb.Items.Clear();

            string[] strSS = strDBValues.Split(',');

            string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string s in result)
            {
                foreach (string SS in strSS)
                {
                    if (SS == s)
                    {
                        System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(s, s);
                        lb.Items.Add(liTemp);
                    }
                }

            }

            foreach (string s in result)
            {
                if (lb.Items.FindByValue(s) == null)
                {
                    System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(s, s);
                    lb.Items.Add(liTemp);
                }

            }

            foreach (string SS in strSS)
            {
                try
                {
                    if (SS != "")
                        lb.Items.FindByValue(SS).Selected = true;
                }
                catch
                {
                    //
                }
            }
        }

    }


    public static void PutListValues(string strDropdownValues, ref  ListBox lb)
    {
        lb.Items.Clear();
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        foreach (string s in result)
        {
            System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(s, s);
            lb.Items.Add(liTemp);
        }


    }
    public static string GetListValues(ListBox lb)
    {
        string strSelectedValues = "";

        foreach (System.Web.UI.WebControls.ListItem item in lb.Items)
        {
            if (item.Selected)
            {
                strSelectedValues = strSelectedValues + item.Value + ",";
            }
        }

        if (strSelectedValues != "")
            strSelectedValues = strSelectedValues.Substring(0, strSelectedValues.Length - 1);

        return strSelectedValues;
    }

    public static string GetDisplayTextFromColumnAndValue(Column theColumn, string strDBValue)
    {
        if (strDBValue.Trim() == "" || theColumn == null)
            return strDBValue;

        string strDisplayText = strDBValue;

        try
        {
            if (theColumn.TableTableID != null && theColumn.DisplayColumn != "" && theColumn.LinkedParentColumnID != null
                && (int)theColumn.TableTableID > -1)
            {
                strDisplayText = Common.GetLinkedDisplayText(theColumn.DisplayColumn, (int)theColumn.TableTableID, null, " AND Record.RecordID=" + strDBValue, "");

            }
            else if (theColumn.TableTableID != null && theColumn.DisplayColumn != "" && (int)theColumn.TableTableID == -1)
            {
                strDisplayText = RecordManager.fnGetSystemUserDisplayText(theColumn.DisplayColumn, strDBValue);
            }
            else if (theColumn.DropdownValues != "" && (theColumn.ColumnType == "dropdown" || theColumn.ColumnType == "listbox"))
            {
                strDisplayText = Common.GetTextFromValue(theColumn.DropdownValues, strDBValue);
            }
            else
            {
                strDisplayText = strDBValue;
            }

        }
        catch
        {
            strDisplayText = strDBValue;
        }

        return strDisplayText;
    }
    public static string GetTextFromValue(string strDropdownValues, string strDBValue)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        string strValue = "";
        string strText = "";

        foreach (string s in result)
        {
            //ListItem liTemp = new ListItem(s, s.ToLower());
            strValue = "";
            strText = "";
            if (s.IndexOf(",") > -1)
            {
                strValue = s.Substring(0, s.IndexOf(","));
                strText = s.Substring(strValue.Length + 1);
                if (strValue != "" && strText != "")
                {
                    if (strDBValue.ToLower() == strValue.ToLower())
                    {
                        return strText;
                    }
                }
            }
        }
        return strDBValue;

    }

    public static void GetCheckTcikedUnTicked(string strDropdownValues, ref string strTrue, ref string strFalse)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        int i = 0;
        foreach (string s in result)
        {
            //ListItem liTemp = new ListItem(s, s.ToLower());

            if (i == 0)
            {
                strTrue = s;
            }
            else if (i == 1)
            {
                strFalse = s;
            }
            i = i + 1;
        }

    }


    public static void PutCheckBoxListValues(string strDropdownValues, ref  CheckBoxList lb)
    {
        lb.Items.Clear();
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        foreach (string s in result)
        {
            System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(s, s);
            lb.Items.Add(liTemp);
        }

    }

    public static void PutCheckBoxListValues_Text(string strDropdownValues, ref  CheckBoxList lb)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        string strValue = "";
        string strText = "";
        lb.Items.Clear();
        foreach (string s in result)
        {
            //ListItem liTemp = new ListItem(s, s.ToLower());
            strValue = "";
            strText = "";
            if (s.IndexOf(",") > -1)
            {
                strValue = s.Substring(0, s.IndexOf(","));
                strText = s.Substring(strValue.Length + 1);
                if (strValue != "" && strText != "")
                {
                    System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(strText, strValue);
                    lb.Items.Add(liTemp);
                }
            }
        }


    }
    public static void PutCheckBoxList_ForTable(int iTableTableID, int? iLinkedParentColumnID, string strDisplayColumn, ref  CheckBoxList lb)
    {
        lb.Items.Clear();
        DataTable dtParents = Common.spGetLinkedRecordIDnDisplayText(strDisplayColumn, null, iTableTableID, Common.MaxRowForListBoxTable, "", "", ""); //3611
        foreach (DataRow dr in dtParents.Rows)
        {
            System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString());
            lb.Items.Add(liTemp);
        }

    }
    public static string GetCheckBoxValue(string strDropdownValues, ref  System.Web.UI.WebControls.CheckBox chk)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        int i = 0;
        foreach (string s in result)
        {
            if (i == 0)
            {
                if (chk.Checked)
                {
                    return s;
                }
            }
            if (i == 1)
            {
                if (chk.Checked == false)
                {
                    return s;
                }
            }
            i = i + 1;
        }
        return "";
    }


    public static string GetCheckBoxListValues(CheckBoxList lb)
    {
        string strSelectedValues = "";

        foreach (System.Web.UI.WebControls.ListItem item in lb.Items)
        {
            if (item.Selected)
            {
                strSelectedValues = strSelectedValues + item.Value + ",";
            }
        }

        if (strSelectedValues != "")
            strSelectedValues = strSelectedValues.Substring(0, strSelectedValues.Length - 1);
        return strSelectedValues;
    }


    public static void SetCheckBoxValue(string strDropdownValues, string strValue, ref  System.Web.UI.WebControls.CheckBox chk)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        int i = 0;
        foreach (string s in result)
        {
            if (i == 0)
            {
                if (s.ToLower() == strValue.ToLower())
                {
                    chk.Checked = true;
                }
            }
            if (i == 1)
            {
                if (s.ToLower() == strValue.ToLower())
                {
                    chk.Checked = false;
                }
            }
            i = i + 1;
        }


    }
    public static void PutCheckBoxDefault(string strDropdownValues, ref  System.Web.UI.WebControls.CheckBox chk)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        int i = 0;
        foreach (string s in result)
        {
            if (i == 2)
            {
                if (s.ToLower() == "yes")
                {
                    chk.Checked = true;
                }
            }
            i = i + 1;
        }


    }



    public static string GetDDLValueFromText(string strDropdownValues, string strSearchText)
    {

        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        string strValue = "";
        string strText = "";

        foreach (string s in result)
        {
            //ListItem liTemp = new ListItem(s, s.ToLower());
            strValue = "";
            strText = "";
            if (s.IndexOf(",") > -1)
            {
                strValue = s.Substring(0, s.IndexOf(","));
                strText = s.Substring(strValue.Length + 1);
                if (strValue != "" && strText != "")
                {
                    if (strText.ToLower() == strSearchText.ToLower())
                    {
                        return strValue;
                    }
                }
            }
        }

        return "";

    }

    public static string GetTextFromTableForList(int iTableTableID, int? iLinkedParentColumnID, string strDisplayColumn, string strMainValue)
    {

        //it's a new dev so iLinkedParentColumnID must be RecordID

        DataTable dtParents = Common.spGetLinkedRecordIDnDisplayText(strDisplayColumn, null, iTableTableID, Common.MaxRowForListBoxTable, "", "", ""); //3611

        string[] values = strMainValue.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);


        string strTotalText = "";
        foreach (DataRow dr in dtParents.Rows)
        {
            foreach (string v in values)
            {
                if (dr[0].ToString() == v)
                {
                    strTotalText = strTotalText + dr[1].ToString() + ",";
                }
            }
        }
        if (strTotalText.Length > 0)
            strTotalText = strTotalText.Substring(0, strTotalText.Length - 1);

        return strTotalText;
    }



    public static void PutDDLValues(string strDropdownValues, ref  DropDownList ddl)
    {
        ddl.Items.Clear();
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        foreach (string s in result)
        {
            System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(s, s);
            ddl.Items.Add(liTemp);
        }

        string defaultListItem = "--Please Select--";
        if (ddl.TemplateControl != null && (ddl.TemplateControl.ID == "rlOne" || ddl.TemplateControl.ID.Contains("ctList")))
        {
            defaultListItem = "--Show All--";
        }

        System.Web.UI.WebControls.ListItem liSelect = new System.Web.UI.WebControls.ListItem(defaultListItem, "");
        ddl.Items.Insert(0, liSelect);

    }

    public static void PutList_FromTable(int iTableTableID, int? iLinkedParentColumnID, string strDisplayColumn, ref  ListBox lb)
    {
        //it's a new dev so iLinkedParentColumnID must be RecordID
        DataTable dtParents = Common.spGetLinkedRecordIDnDisplayText(strDisplayColumn, null, iTableTableID, Common.MaxRowForListBoxTable, "", "", ""); //3611

        lb.Items.Clear();
        foreach (DataRow dr in dtParents.Rows)
        {
            System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString());
            lb.Items.Add(liTemp);
        }

    }

    public static string GetDatestringFromD(string strDate)
    {
        string strDateTime = "";
        if (strDate.Trim() == "")
        {
            // strDateTime = DateTime.Now.ToShortDateString();// +" 12:00:00 AM";
        }
        else
        {
            DateTime dtTemp;
            if (DateTime.TryParseExact(strDate.Trim(), Common.Dateformats, new CultureInfo("en-GB"), DateTimeStyles.None, out dtTemp))
            {
                strDate = dtTemp.ToShortDateString();
            }
            strDateTime = strDate;// +" " + "12:00:00 AM";
        }
        return strDateTime;

    }
    public static string GetDateTimeFromDnT(string strDate, string strTime)
    {
        string strDateTime = "";
        if (strDate.Trim() == "")
        {
            // strDateTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
        }
        else
        {

            DateTime dtTemp;
            if (DateTime.TryParseExact(strDate.Trim(), Common.Dateformats, new CultureInfo("en-GB"),
                DateTimeStyles.None, out dtTemp))
            {
                strDate = dtTemp.ToShortDateString();
            }

            string strTimePart = "";

            if (strTime == "")
            {
                strTimePart = " 12:00:00 AM";
            }
            else
            {
                if (strTime.ToLower().IndexOf(":am") > 0)
                {
                    strTimePart = strTime.ToLower().Replace(":am", ":00 AM");
                }
                else
                {
                    strTimePart = strTime.ToLower().Replace(":pm", ":00 PM");
                }
            }


            strDateTime = strDate + " " + strTimePart;
        }
        return strDateTime;


    }
    public static void PutDDLValue_Text(string strDropdownValues, ref  DropDownList ddl)
    {
        ddl.Items.Clear();
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        string strValue = "";
        string strText = "";

        foreach (string s in result)
        {
            //ListItem liTemp = new ListItem(s, s.ToLower());
            strValue = "";
            strText = "";
            if (s.IndexOf(",") > -1)
            {
                strValue = s.Substring(0, s.IndexOf(","));
                strText = s.Substring(strValue.Length + 1);
                if (strValue != "" && strText != "")
                {
                    System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(strText, strValue);
                    ddl.Items.Add(liTemp);
                }
            }
        }

        System.Web.UI.WebControls.ListItem liSelect = new System.Web.UI.WebControls.ListItem("--Please Select--", "");
        ddl.Items.Insert(0, liSelect);

    }

    public static void PutRadioList(string strDropdownValues, ref  RadioButtonList rl)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        rl.Items.Clear();
        foreach (string s in result)
        {
            //ListItem liTemp = new ListItem(s, s.ToLower());
            System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(s + "&nbsp;&nbsp;", s);
            rl.Items.Add(liTemp);
        }

    }


    public static void PutRadioListValue_Text(string strDropdownValues, ref  RadioButtonList rl)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        rl.Items.Clear();
        string strValue = "";
        string strText = "";

        foreach (string s in result)
        {
            //ListItem liTemp = new ListItem(s, s.ToLower());
            strValue = "";
            strText = "";
            if (s.IndexOf(",") > -1)
            {
                strValue = s.Substring(0, s.IndexOf(","));
                strText = s.Substring(strValue.Length + 1);
                if (strValue != "" && strText != "")
                {
                    System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(strText + "&nbsp;&nbsp;", strValue);
                    rl.Items.Add(liTemp);
                }
            }
        }


    }




    public static void SetCheckBoxListValues(string strDBValues, ref  CheckBoxList lb, string strDropdownValues)
    {


        if (strDBValues != "")
        {

            lb.Items.Clear();


            string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);


            string[] strSS = strDBValues.Split(',');

            foreach (string s in result)
            {

                foreach (string SS in strSS)
                {
                    if (SS == s)
                    {
                        System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(s, s);
                        lb.Items.Add(liTemp);
                    }
                }
            }

            foreach (string s in result)
            {

                if (lb.Items.FindByValue(s) == null)
                {
                    System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(s, s);
                    lb.Items.Add(liTemp);
                }

            }

            foreach (string SS in strSS)
            {
                try
                {
                    if (SS != "")
                        lb.Items.FindByValue(SS).Selected = true;
                }
                catch
                {
                    //
                }
            }

            foreach (System.Web.UI.WebControls.ListItem li in lb.Items)
            {
                li.Attributes.Add("DataValue", li.Value);
            }
        }

    }


    public static void SetCheckBoxListValues_ForTable(string strDBValues, ref  CheckBoxList lb, int iTableTableID,
       int? iLinkedParentColumnID, string strDisplayColumn)
    {


        if (strDBValues != "")
        {
            //it's a new dev so iLinkedParentColumnID must be RecordID

            DataTable dtParents = Common.spGetLinkedRecordIDnDisplayText(strDisplayColumn, null, iTableTableID, Common.MaxRowForListBoxTable, "", "", ""); //3611


            lb.Items.Clear();

            string[] strSS = strDBValues.Split(',');

            foreach (DataRow dr in dtParents.Rows)
            {

                foreach (string SS in strSS)
                {
                    if (SS == dr[0].ToString())
                    {
                        System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString());
                        lb.Items.Add(liTemp);
                    }
                }


            }



            foreach (DataRow dr in dtParents.Rows)
            {




                if (lb.Items.FindByValue(dr[0].ToString()) == null)
                {
                    System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(dr[1].ToString(), dr[0].ToString());
                    lb.Items.Add(liTemp);
                }

            }

            foreach (string SS in strSS)
            {
                try
                {
                    if (SS != "")
                        lb.Items.FindByValue(SS).Selected = true;
                }
                catch
                {
                    //
                }
            }

            foreach (System.Web.UI.WebControls.ListItem li in lb.Items)
            {
                li.Attributes.Add("DataValue", li.Value);
            }
        }

    }

    public static void SetCheckBoxListValues_Text(string strDBValues, ref  CheckBoxList lb, string strDropdownValues)
    {


        if (strDBValues != "")
        {

            lb.Items.Clear();


            string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            string strValue = "";
            string strText = "";

            string[] strSS = strDBValues.Split(',');

            foreach (string s in result)
            {
                strValue = "";
                strText = "";

                if (s.IndexOf(",") > -1)
                {
                    strValue = s.Substring(0, s.IndexOf(","));
                    strText = s.Substring(strValue.Length + 1);
                }

                foreach (string SS in strSS)
                {
                    if (SS == strValue)
                    {
                        System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(strText, strValue);
                        lb.Items.Add(liTemp);
                    }
                }
            }

            foreach (string s in result)
            {


                strValue = "";
                strText = "";

                if (s.IndexOf(",") > -1)
                {
                    strValue = s.Substring(0, s.IndexOf(","));
                    strText = s.Substring(strValue.Length + 1);
                }

                if (lb.Items.FindByValue(strValue) == null)
                {
                    System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(strText, strValue);
                    lb.Items.Add(liTemp);
                }

            }

            foreach (string SS in strSS)
            {
                try
                {
                    if (SS != "")
                        lb.Items.FindByValue(SS).Selected = true;
                }
                catch
                {
                    //
                }
            }

            foreach (System.Web.UI.WebControls.ListItem li in lb.Items)
            {
                li.Attributes.Add("DataValue", li.Value);
            }
        }

    }


    public static string GetTextFromValueForList(string strDropdownValues, string strMainValue)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        string[] values = strMainValue.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

        string strValue = "";
        string strText = "";
        string strTotalText = "";
        foreach (string s in result)
        {
            strValue = "";
            strText = "";
            if (s.IndexOf(",") > -1)
            {
                strValue = s.Substring(0, s.IndexOf(","));
                strText = s.Substring(strValue.Length + 1);

                foreach (string v in values)
                {
                    if (strValue == v)
                    {
                        strTotalText = strTotalText + strText + ",";
                    }
                }


            }
        }
        if (strTotalText.Length > 0)
            strTotalText = strTotalText.Substring(0, strTotalText.Length - 1);

        return strTotalText;
    }

    public static string GetImageFromValueForDD(string strDropdownValues, string strMainValue, string _strFilesLocation, string strHeight)
    {
        OptionImageList theOptionImageList = JSONField.GetTypedObject<OptionImageList>(strDropdownValues);
        foreach (OptionImage aOptionImage in theOptionImageList.ImageList)
        {
            if (aOptionImage.Value == strMainValue)
            {
                if (strHeight != "")
                {
                    strHeight = " height='" + strHeight + "'";
                }

                return "<img " + strHeight + " src='" + _strFilesLocation + "/UserFiles/AppFiles/" + aOptionImage.UniqueFileName + "' title='" + aOptionImage.Value + "' />";
            }

        }
        return strMainValue;
    }
    public static void PutRadioListValue_Image(string strDropdownValues, ref  RadioButtonList rl, string _strFilesLocation)
    {
        //string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        OptionImageList theOptionImageList = JSONField.GetTypedObject<OptionImageList>(strDropdownValues);
        rl.Items.Clear();
        foreach (OptionImage aOptionImage in theOptionImageList.ImageList)
        {
            System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem("<img src='" + _strFilesLocation + "/UserFiles/AppFiles/" + aOptionImage.UniqueFileName + "' title='" + aOptionImage.Value + "' />" + "&nbsp;&nbsp;", aOptionImage.Value);
            rl.Items.Add(liTemp);
        }

    }

    //XXXFILES501
    public static string filePath = "/FileSite/UserFiles/AppFiles/";
    public static bool IsExceedance = false;
    public static bool IsWarning = false;

    public static void PutRadioImageInto_DDL(string strDropdownValues, ref  DropDownList ddl)
    {

        OptionImageList theOptionImageList = JSONField.GetTypedObject<OptionImageList>(strDropdownValues);
        ddl.Items.Clear();
        foreach (OptionImage aOptionImage in theOptionImageList.ImageList)
        {
            System.Web.UI.WebControls.ListItem liTemp = new System.Web.UI.WebControls.ListItem(aOptionImage.FileName, aOptionImage.Value);
            ddl.Items.Add(liTemp);
        }

        //Red 3715_2
        if (!ddl.TemplateControl.ClientID.Contains("rlOne_cbcSearch"))
        {
            System.Web.UI.WebControls.ListItem liSelect = new System.Web.UI.WebControls.ListItem("--Please Select--", "");
            ddl.Items.Insert(0, liSelect);

        }

    }

    public static DataTable CreateVirtualRecord(Record theRecord)
    {
        DataTable vRecord = new DataTable();
        vRecord.Columns.Add("SystemName", typeof(System.String));
        vRecord.Columns.Add("VValue", typeof(System.String));

        DataTable dtColumns = Common.DataTableFromText("SELECT SystemName FROM [Column] WHERE TableID="
            + theRecord.TableID);

        foreach (DataRow dr in dtColumns.Rows)
        {
            vRecord.Rows.Add(dr[0].ToString(), RecordManager.GetRecordValue(ref theRecord, dr[0].ToString()));
        }

        return vRecord;
    }
    public static DataTable CreateVirtualTable(Record record)
    {
        DataTable virtualTable = new DataTable();
        virtualTable.Columns.Add("RecordID", typeof(System.Int32));
        virtualTable.Columns.Add("V001", typeof(System.String));
        virtualTable.Columns.Add("V002", typeof(System.String));
        virtualTable.Columns.Add("V003", typeof(System.String));
        virtualTable.Columns.Add("V004", typeof(System.String));
        virtualTable.Columns.Add("V005", typeof(System.String));
        virtualTable.Columns.Add("V006", typeof(System.String));
        virtualTable.Columns.Add("V007", typeof(System.String));
        virtualTable.Columns.Add("V008", typeof(System.String));
        virtualTable.Columns.Add("V009", typeof(System.String));
        virtualTable.Columns.Add("V010", typeof(System.String));
        virtualTable.Columns.Add("V011", typeof(System.String));
        virtualTable.Columns.Add("V012", typeof(System.String));
        virtualTable.Columns.Add("V013", typeof(System.String));
        virtualTable.Columns.Add("V014", typeof(System.String));
        virtualTable.Columns.Add("V015", typeof(System.String));
        virtualTable.Columns.Add("V016", typeof(System.String));
        virtualTable.Columns.Add("V017", typeof(System.String));
        virtualTable.Columns.Add("V018", typeof(System.String));
        virtualTable.Columns.Add("V019", typeof(System.String));
        virtualTable.Columns.Add("V020", typeof(System.String));
        virtualTable.Columns.Add("V021", typeof(System.String));
        virtualTable.Columns.Add("V022", typeof(System.String));
        virtualTable.Columns.Add("V023", typeof(System.String));
        virtualTable.Columns.Add("V024", typeof(System.String));
        virtualTable.Columns.Add("V025", typeof(System.String));
        virtualTable.Columns.Add("V026", typeof(System.String));
        virtualTable.Columns.Add("V027", typeof(System.String));
        virtualTable.Columns.Add("V028", typeof(System.String));
        virtualTable.Columns.Add("V029", typeof(System.String));
        virtualTable.Columns.Add("V030", typeof(System.String));
        virtualTable.Columns.Add("V031", typeof(System.String));
        virtualTable.Columns.Add("V032", typeof(System.String));
        virtualTable.Columns.Add("V033", typeof(System.String));
        virtualTable.Columns.Add("V034", typeof(System.String));
        virtualTable.Columns.Add("V035", typeof(System.String));
        virtualTable.Columns.Add("V036", typeof(System.String));
        virtualTable.Columns.Add("V037", typeof(System.String));
        virtualTable.Columns.Add("V038", typeof(System.String));
        virtualTable.Columns.Add("V039", typeof(System.String));
        virtualTable.Columns.Add("V040", typeof(System.String));
        virtualTable.Columns.Add("V041", typeof(System.String));
        virtualTable.Columns.Add("V042", typeof(System.String));
        virtualTable.Columns.Add("V043", typeof(System.String));
        virtualTable.Columns.Add("V044", typeof(System.String));
        virtualTable.Columns.Add("V045", typeof(System.String));
        virtualTable.Columns.Add("V046", typeof(System.String));
        virtualTable.Columns.Add("V047", typeof(System.String));
        virtualTable.Columns.Add("V048", typeof(System.String));
        virtualTable.Columns.Add("V049", typeof(System.String));
        virtualTable.Columns.Add("V050", typeof(System.String));
        virtualTable.Columns.Add("V051", typeof(System.String));
        virtualTable.Columns.Add("V052", typeof(System.String));
        virtualTable.Columns.Add("V053", typeof(System.String));
        virtualTable.Columns.Add("V054", typeof(System.String));
        virtualTable.Columns.Add("V055", typeof(System.String));
        virtualTable.Columns.Add("V056", typeof(System.String));
        virtualTable.Columns.Add("V057", typeof(System.String));
        virtualTable.Columns.Add("V058", typeof(System.String));
        virtualTable.Columns.Add("V059", typeof(System.String));
        virtualTable.Columns.Add("V060", typeof(System.String));
        virtualTable.Columns.Add("V061", typeof(System.String));
        virtualTable.Columns.Add("V062", typeof(System.String));
        virtualTable.Columns.Add("V063", typeof(System.String));
        virtualTable.Columns.Add("V064", typeof(System.String));
        virtualTable.Columns.Add("V065", typeof(System.String));
        virtualTable.Columns.Add("V066", typeof(System.String));
        virtualTable.Columns.Add("V067", typeof(System.String));
        virtualTable.Columns.Add("V068", typeof(System.String));
        virtualTable.Columns.Add("V069", typeof(System.String));
        virtualTable.Columns.Add("V070", typeof(System.String));
        virtualTable.Columns.Add("V071", typeof(System.String));
        virtualTable.Columns.Add("V072", typeof(System.String));
        virtualTable.Columns.Add("V073", typeof(System.String));
        virtualTable.Columns.Add("V074", typeof(System.String));
        virtualTable.Columns.Add("V075", typeof(System.String));
        virtualTable.Columns.Add("V076", typeof(System.String));
        virtualTable.Columns.Add("V077", typeof(System.String));
        virtualTable.Columns.Add("V078", typeof(System.String));
        virtualTable.Columns.Add("V079", typeof(System.String));
        virtualTable.Columns.Add("V080", typeof(System.String));
        virtualTable.Columns.Add("V081", typeof(System.String));
        virtualTable.Columns.Add("V082", typeof(System.String));
        virtualTable.Columns.Add("V083", typeof(System.String));
        virtualTable.Columns.Add("V084", typeof(System.String));
        virtualTable.Columns.Add("V085", typeof(System.String));
        virtualTable.Columns.Add("V086", typeof(System.String));
        virtualTable.Columns.Add("V087", typeof(System.String));
        virtualTable.Columns.Add("V088", typeof(System.String));
        virtualTable.Columns.Add("V089", typeof(System.String));
        virtualTable.Columns.Add("V090", typeof(System.String));
        virtualTable.Columns.Add("V091", typeof(System.String));
        virtualTable.Columns.Add("V092", typeof(System.String));
        virtualTable.Columns.Add("V093", typeof(System.String));
        virtualTable.Columns.Add("V094", typeof(System.String));
        virtualTable.Columns.Add("V095", typeof(System.String));
        virtualTable.Columns.Add("V096", typeof(System.String));
        virtualTable.Columns.Add("V097", typeof(System.String));
        virtualTable.Columns.Add("V098", typeof(System.String));
        virtualTable.Columns.Add("V099", typeof(System.String));
        virtualTable.Columns.Add("V100", typeof(System.String));
        virtualTable.Columns.Add("V101", typeof(System.String));
        virtualTable.Columns.Add("V102", typeof(System.String));
        virtualTable.Columns.Add("V103", typeof(System.String));
        virtualTable.Columns.Add("V104", typeof(System.String));
        virtualTable.Columns.Add("V105", typeof(System.String));
        virtualTable.Columns.Add("V106", typeof(System.String));
        virtualTable.Columns.Add("V107", typeof(System.String));
        virtualTable.Columns.Add("V108", typeof(System.String));
        virtualTable.Columns.Add("V109", typeof(System.String));
        virtualTable.Columns.Add("V110", typeof(System.String));
        virtualTable.Columns.Add("V111", typeof(System.String));
        virtualTable.Columns.Add("V112", typeof(System.String));
        virtualTable.Columns.Add("V113", typeof(System.String));
        virtualTable.Columns.Add("V114", typeof(System.String));
        virtualTable.Columns.Add("V115", typeof(System.String));
        virtualTable.Columns.Add("V116", typeof(System.String));
        virtualTable.Columns.Add("V117", typeof(System.String));
        virtualTable.Columns.Add("V118", typeof(System.String));
        virtualTable.Columns.Add("V119", typeof(System.String));
        virtualTable.Columns.Add("V120", typeof(System.String));
        virtualTable.Columns.Add("V121", typeof(System.String));
        virtualTable.Columns.Add("V122", typeof(System.String));
        virtualTable.Columns.Add("V123", typeof(System.String));
        virtualTable.Columns.Add("V124", typeof(System.String));
        virtualTable.Columns.Add("V125", typeof(System.String));
        virtualTable.Columns.Add("V126", typeof(System.String));
        virtualTable.Columns.Add("V127", typeof(System.String));
        virtualTable.Columns.Add("V128", typeof(System.String));
        virtualTable.Columns.Add("V129", typeof(System.String));
        virtualTable.Columns.Add("V130", typeof(System.String));
        virtualTable.Columns.Add("V131", typeof(System.String));
        virtualTable.Columns.Add("V132", typeof(System.String));
        virtualTable.Columns.Add("V133", typeof(System.String));
        virtualTable.Columns.Add("V134", typeof(System.String));
        virtualTable.Columns.Add("V135", typeof(System.String));
        virtualTable.Columns.Add("V136", typeof(System.String));
        virtualTable.Columns.Add("V137", typeof(System.String));
        virtualTable.Columns.Add("V138", typeof(System.String));
        virtualTable.Columns.Add("V139", typeof(System.String));
        virtualTable.Columns.Add("V140", typeof(System.String));
        virtualTable.Columns.Add("V141", typeof(System.String));
        virtualTable.Columns.Add("V142", typeof(System.String));
        virtualTable.Columns.Add("V143", typeof(System.String));
        virtualTable.Columns.Add("V144", typeof(System.String));
        virtualTable.Columns.Add("V145", typeof(System.String));
        virtualTable.Columns.Add("V146", typeof(System.String));
        virtualTable.Columns.Add("V147", typeof(System.String));
        virtualTable.Columns.Add("V148", typeof(System.String));
        virtualTable.Columns.Add("V149", typeof(System.String));
        virtualTable.Columns.Add("V150", typeof(System.String));
        virtualTable.Columns.Add("V151", typeof(System.String));
        virtualTable.Columns.Add("V152", typeof(System.String));
        virtualTable.Columns.Add("V153", typeof(System.String));
        virtualTable.Columns.Add("V154", typeof(System.String));
        virtualTable.Columns.Add("V155", typeof(System.String));
        virtualTable.Columns.Add("V156", typeof(System.String));
        virtualTable.Columns.Add("V157", typeof(System.String));
        virtualTable.Columns.Add("V158", typeof(System.String));
        virtualTable.Columns.Add("V159", typeof(System.String));
        virtualTable.Columns.Add("V160", typeof(System.String));
        virtualTable.Columns.Add("V161", typeof(System.String));
        virtualTable.Columns.Add("V162", typeof(System.String));
        virtualTable.Columns.Add("V163", typeof(System.String));
        virtualTable.Columns.Add("V164", typeof(System.String));
        virtualTable.Columns.Add("V165", typeof(System.String));
        virtualTable.Columns.Add("V166", typeof(System.String));
        virtualTable.Columns.Add("V167", typeof(System.String));
        virtualTable.Columns.Add("V168", typeof(System.String));
        virtualTable.Columns.Add("V169", typeof(System.String));
        virtualTable.Columns.Add("V170", typeof(System.String));
        virtualTable.Columns.Add("V171", typeof(System.String));
        virtualTable.Columns.Add("V172", typeof(System.String));
        virtualTable.Columns.Add("V173", typeof(System.String));
        virtualTable.Columns.Add("V174", typeof(System.String));
        virtualTable.Columns.Add("V175", typeof(System.String));
        virtualTable.Columns.Add("V176", typeof(System.String));
        virtualTable.Columns.Add("V177", typeof(System.String));
        virtualTable.Columns.Add("V178", typeof(System.String));
        virtualTable.Columns.Add("V179", typeof(System.String));
        virtualTable.Columns.Add("V180", typeof(System.String));
        virtualTable.Columns.Add("V181", typeof(System.String));
        virtualTable.Columns.Add("V182", typeof(System.String));
        virtualTable.Columns.Add("V183", typeof(System.String));
        virtualTable.Columns.Add("V184", typeof(System.String));
        virtualTable.Columns.Add("V185", typeof(System.String));
        virtualTable.Columns.Add("V186", typeof(System.String));
        virtualTable.Columns.Add("V187", typeof(System.String));
        virtualTable.Columns.Add("V188", typeof(System.String));
        virtualTable.Columns.Add("V189", typeof(System.String));
        virtualTable.Columns.Add("V190", typeof(System.String));
        virtualTable.Columns.Add("V191", typeof(System.String));
        virtualTable.Columns.Add("V192", typeof(System.String));
        virtualTable.Columns.Add("V193", typeof(System.String));
        virtualTable.Columns.Add("V194", typeof(System.String));
        virtualTable.Columns.Add("V195", typeof(System.String));
        virtualTable.Columns.Add("V196", typeof(System.String));
        virtualTable.Columns.Add("V197", typeof(System.String));
        virtualTable.Columns.Add("V198", typeof(System.String));
        virtualTable.Columns.Add("V199", typeof(System.String));
        virtualTable.Columns.Add("V200", typeof(System.String));
        virtualTable.Columns.Add("V201", typeof(System.String));
        virtualTable.Columns.Add("V202", typeof(System.String));
        virtualTable.Columns.Add("V203", typeof(System.String));
        virtualTable.Columns.Add("V204", typeof(System.String));
        virtualTable.Columns.Add("V205", typeof(System.String));
        virtualTable.Columns.Add("V206", typeof(System.String));
        virtualTable.Columns.Add("V207", typeof(System.String));
        virtualTable.Columns.Add("V208", typeof(System.String));
        virtualTable.Columns.Add("V209", typeof(System.String));
        virtualTable.Columns.Add("V210", typeof(System.String));
        virtualTable.Columns.Add("V211", typeof(System.String));
        virtualTable.Columns.Add("V212", typeof(System.String));
        virtualTable.Columns.Add("V213", typeof(System.String));
        virtualTable.Columns.Add("V214", typeof(System.String));
        virtualTable.Columns.Add("V215", typeof(System.String));
        virtualTable.Columns.Add("V216", typeof(System.String));
        virtualTable.Columns.Add("V217", typeof(System.String));
        virtualTable.Columns.Add("V218", typeof(System.String));
        virtualTable.Columns.Add("V219", typeof(System.String));
        virtualTable.Columns.Add("V220", typeof(System.String));
        virtualTable.Columns.Add("V221", typeof(System.String));
        virtualTable.Columns.Add("V222", typeof(System.String));
        virtualTable.Columns.Add("V223", typeof(System.String));
        virtualTable.Columns.Add("V224", typeof(System.String));
        virtualTable.Columns.Add("V225", typeof(System.String));
        virtualTable.Columns.Add("V226", typeof(System.String));
        virtualTable.Columns.Add("V227", typeof(System.String));
        virtualTable.Columns.Add("V228", typeof(System.String));
        virtualTable.Columns.Add("V229", typeof(System.String));
        virtualTable.Columns.Add("V230", typeof(System.String));
        virtualTable.Columns.Add("V231", typeof(System.String));
        virtualTable.Columns.Add("V232", typeof(System.String));
        virtualTable.Columns.Add("V233", typeof(System.String));
        virtualTable.Columns.Add("V234", typeof(System.String));
        virtualTable.Columns.Add("V235", typeof(System.String));
        virtualTable.Columns.Add("V236", typeof(System.String));
        virtualTable.Columns.Add("V237", typeof(System.String));
        virtualTable.Columns.Add("V238", typeof(System.String));
        virtualTable.Columns.Add("V239", typeof(System.String));
        virtualTable.Columns.Add("V240", typeof(System.String));
        virtualTable.Columns.Add("V241", typeof(System.String));
        virtualTable.Columns.Add("V242", typeof(System.String));
        virtualTable.Columns.Add("V243", typeof(System.String));
        virtualTable.Columns.Add("V244", typeof(System.String));
        virtualTable.Columns.Add("V245", typeof(System.String));
        virtualTable.Columns.Add("V246", typeof(System.String));
        virtualTable.Columns.Add("V247", typeof(System.String));
        virtualTable.Columns.Add("V248", typeof(System.String));
        virtualTable.Columns.Add("V249", typeof(System.String));
        virtualTable.Columns.Add("V250", typeof(System.String));
        virtualTable.Columns.Add("V251", typeof(System.String));
        virtualTable.Columns.Add("V252", typeof(System.String));
        virtualTable.Columns.Add("V253", typeof(System.String));
        virtualTable.Columns.Add("V254", typeof(System.String));
        virtualTable.Columns.Add("V255", typeof(System.String));
        virtualTable.Columns.Add("V256", typeof(System.String));
        virtualTable.Columns.Add("V257", typeof(System.String));
        virtualTable.Columns.Add("V258", typeof(System.String));
        virtualTable.Columns.Add("V259", typeof(System.String));
        virtualTable.Columns.Add("V260", typeof(System.String));
        virtualTable.Columns.Add("V261", typeof(System.String));
        virtualTable.Columns.Add("V262", typeof(System.String));
        virtualTable.Columns.Add("V263", typeof(System.String));
        virtualTable.Columns.Add("V264", typeof(System.String));
        virtualTable.Columns.Add("V265", typeof(System.String));
        virtualTable.Columns.Add("V266", typeof(System.String));
        virtualTable.Columns.Add("V267", typeof(System.String));
        virtualTable.Columns.Add("V268", typeof(System.String));
        virtualTable.Columns.Add("V269", typeof(System.String));
        virtualTable.Columns.Add("V270", typeof(System.String));
        virtualTable.Columns.Add("V271", typeof(System.String));
        virtualTable.Columns.Add("V272", typeof(System.String));
        virtualTable.Columns.Add("V273", typeof(System.String));
        virtualTable.Columns.Add("V274", typeof(System.String));
        virtualTable.Columns.Add("V275", typeof(System.String));
        virtualTable.Columns.Add("V276", typeof(System.String));
        virtualTable.Columns.Add("V277", typeof(System.String));
        virtualTable.Columns.Add("V278", typeof(System.String));
        virtualTable.Columns.Add("V279", typeof(System.String));
        virtualTable.Columns.Add("V280", typeof(System.String));
        virtualTable.Columns.Add("V281", typeof(System.String));
        virtualTable.Columns.Add("V282", typeof(System.String));
        virtualTable.Columns.Add("V283", typeof(System.String));
        virtualTable.Columns.Add("V284", typeof(System.String));
        virtualTable.Columns.Add("V285", typeof(System.String));
        virtualTable.Columns.Add("V286", typeof(System.String));
        virtualTable.Columns.Add("V287", typeof(System.String));
        virtualTable.Columns.Add("V288", typeof(System.String));
        virtualTable.Columns.Add("V289", typeof(System.String));
        virtualTable.Columns.Add("V290", typeof(System.String));
        virtualTable.Columns.Add("V291", typeof(System.String));
        virtualTable.Columns.Add("V292", typeof(System.String));
        virtualTable.Columns.Add("V293", typeof(System.String));
        virtualTable.Columns.Add("V294", typeof(System.String));
        virtualTable.Columns.Add("V295", typeof(System.String));
        virtualTable.Columns.Add("V296", typeof(System.String));
        virtualTable.Columns.Add("V297", typeof(System.String));
        virtualTable.Columns.Add("V298", typeof(System.String));
        virtualTable.Columns.Add("V299", typeof(System.String));
        virtualTable.Columns.Add("V300", typeof(System.String));
        virtualTable.Columns.Add("V301", typeof(System.String));
        virtualTable.Columns.Add("V302", typeof(System.String));
        virtualTable.Columns.Add("V303", typeof(System.String));
        virtualTable.Columns.Add("V304", typeof(System.String));
        virtualTable.Columns.Add("V305", typeof(System.String));
        virtualTable.Columns.Add("V306", typeof(System.String));
        virtualTable.Columns.Add("V307", typeof(System.String));
        virtualTable.Columns.Add("V308", typeof(System.String));
        virtualTable.Columns.Add("V309", typeof(System.String));
        virtualTable.Columns.Add("V310", typeof(System.String));
        virtualTable.Columns.Add("V311", typeof(System.String));
        virtualTable.Columns.Add("V312", typeof(System.String));
        virtualTable.Columns.Add("V313", typeof(System.String));
        virtualTable.Columns.Add("V314", typeof(System.String));
        virtualTable.Columns.Add("V315", typeof(System.String));
        virtualTable.Columns.Add("V316", typeof(System.String));
        virtualTable.Columns.Add("V317", typeof(System.String));
        virtualTable.Columns.Add("V318", typeof(System.String));
        virtualTable.Columns.Add("V319", typeof(System.String));
        virtualTable.Columns.Add("V320", typeof(System.String));
        virtualTable.Columns.Add("V321", typeof(System.String));
        virtualTable.Columns.Add("V322", typeof(System.String));
        virtualTable.Columns.Add("V323", typeof(System.String));
        virtualTable.Columns.Add("V324", typeof(System.String));
        virtualTable.Columns.Add("V325", typeof(System.String));
        virtualTable.Columns.Add("V326", typeof(System.String));
        virtualTable.Columns.Add("V327", typeof(System.String));
        virtualTable.Columns.Add("V328", typeof(System.String));
        virtualTable.Columns.Add("V329", typeof(System.String));
        virtualTable.Columns.Add("V330", typeof(System.String));
        virtualTable.Columns.Add("V331", typeof(System.String));
        virtualTable.Columns.Add("V332", typeof(System.String));
        virtualTable.Columns.Add("V333", typeof(System.String));
        virtualTable.Columns.Add("V334", typeof(System.String));
        virtualTable.Columns.Add("V335", typeof(System.String));
        virtualTable.Columns.Add("V336", typeof(System.String));
        virtualTable.Columns.Add("V337", typeof(System.String));
        virtualTable.Columns.Add("V338", typeof(System.String));
        virtualTable.Columns.Add("V339", typeof(System.String));
        virtualTable.Columns.Add("V340", typeof(System.String));
        virtualTable.Columns.Add("V341", typeof(System.String));
        virtualTable.Columns.Add("V342", typeof(System.String));
        virtualTable.Columns.Add("V343", typeof(System.String));
        virtualTable.Columns.Add("V344", typeof(System.String));
        virtualTable.Columns.Add("V345", typeof(System.String));
        virtualTable.Columns.Add("V346", typeof(System.String));
        virtualTable.Columns.Add("V347", typeof(System.String));
        virtualTable.Columns.Add("V348", typeof(System.String));
        virtualTable.Columns.Add("V349", typeof(System.String));
        virtualTable.Columns.Add("V350", typeof(System.String));
        virtualTable.Columns.Add("V351", typeof(System.String));
        virtualTable.Columns.Add("V352", typeof(System.String));
        virtualTable.Columns.Add("V353", typeof(System.String));
        virtualTable.Columns.Add("V354", typeof(System.String));
        virtualTable.Columns.Add("V355", typeof(System.String));
        virtualTable.Columns.Add("V356", typeof(System.String));
        virtualTable.Columns.Add("V357", typeof(System.String));
        virtualTable.Columns.Add("V358", typeof(System.String));
        virtualTable.Columns.Add("V359", typeof(System.String));
        virtualTable.Columns.Add("V360", typeof(System.String));
        virtualTable.Columns.Add("V361", typeof(System.String));
        virtualTable.Columns.Add("V362", typeof(System.String));
        virtualTable.Columns.Add("V363", typeof(System.String));
        virtualTable.Columns.Add("V364", typeof(System.String));
        virtualTable.Columns.Add("V365", typeof(System.String));
        virtualTable.Columns.Add("V366", typeof(System.String));
        virtualTable.Columns.Add("V367", typeof(System.String));
        virtualTable.Columns.Add("V368", typeof(System.String));
        virtualTable.Columns.Add("V369", typeof(System.String));
        virtualTable.Columns.Add("V370", typeof(System.String));
        virtualTable.Columns.Add("V371", typeof(System.String));
        virtualTable.Columns.Add("V372", typeof(System.String));
        virtualTable.Columns.Add("V373", typeof(System.String));
        virtualTable.Columns.Add("V374", typeof(System.String));
        virtualTable.Columns.Add("V375", typeof(System.String));
        virtualTable.Columns.Add("V376", typeof(System.String));
        virtualTable.Columns.Add("V377", typeof(System.String));
        virtualTable.Columns.Add("V378", typeof(System.String));
        virtualTable.Columns.Add("V379", typeof(System.String));
        virtualTable.Columns.Add("V380", typeof(System.String));
        virtualTable.Columns.Add("V381", typeof(System.String));
        virtualTable.Columns.Add("V382", typeof(System.String));
        virtualTable.Columns.Add("V383", typeof(System.String));
        virtualTable.Columns.Add("V384", typeof(System.String));
        virtualTable.Columns.Add("V385", typeof(System.String));
        virtualTable.Columns.Add("V386", typeof(System.String));
        virtualTable.Columns.Add("V387", typeof(System.String));
        virtualTable.Columns.Add("V388", typeof(System.String));
        virtualTable.Columns.Add("V389", typeof(System.String));
        virtualTable.Columns.Add("V390", typeof(System.String));
        virtualTable.Columns.Add("V391", typeof(System.String));
        virtualTable.Columns.Add("V392", typeof(System.String));
        virtualTable.Columns.Add("V393", typeof(System.String));
        virtualTable.Columns.Add("V394", typeof(System.String));
        virtualTable.Columns.Add("V395", typeof(System.String));
        virtualTable.Columns.Add("V396", typeof(System.String));
        virtualTable.Columns.Add("V397", typeof(System.String));
        virtualTable.Columns.Add("V398", typeof(System.String));
        virtualTable.Columns.Add("V399", typeof(System.String));
        virtualTable.Columns.Add("V400", typeof(System.String));
        virtualTable.Columns.Add("V401", typeof(System.String));
        virtualTable.Columns.Add("V402", typeof(System.String));
        virtualTable.Columns.Add("V403", typeof(System.String));
        virtualTable.Columns.Add("V404", typeof(System.String));
        virtualTable.Columns.Add("V405", typeof(System.String));
        virtualTable.Columns.Add("V406", typeof(System.String));
        virtualTable.Columns.Add("V407", typeof(System.String));
        virtualTable.Columns.Add("V408", typeof(System.String));
        virtualTable.Columns.Add("V409", typeof(System.String));
        virtualTable.Columns.Add("V410", typeof(System.String));
        virtualTable.Columns.Add("V411", typeof(System.String));
        virtualTable.Columns.Add("V412", typeof(System.String));
        virtualTable.Columns.Add("V413", typeof(System.String));
        virtualTable.Columns.Add("V414", typeof(System.String));
        virtualTable.Columns.Add("V415", typeof(System.String));
        virtualTable.Columns.Add("V416", typeof(System.String));
        virtualTable.Columns.Add("V417", typeof(System.String));
        virtualTable.Columns.Add("V418", typeof(System.String));
        virtualTable.Columns.Add("V419", typeof(System.String));
        virtualTable.Columns.Add("V420", typeof(System.String));
        virtualTable.Columns.Add("V421", typeof(System.String));
        virtualTable.Columns.Add("V422", typeof(System.String));
        virtualTable.Columns.Add("V423", typeof(System.String));
        virtualTable.Columns.Add("V424", typeof(System.String));
        virtualTable.Columns.Add("V425", typeof(System.String));
        virtualTable.Columns.Add("V426", typeof(System.String));
        virtualTable.Columns.Add("V427", typeof(System.String));
        virtualTable.Columns.Add("V428", typeof(System.String));
        virtualTable.Columns.Add("V429", typeof(System.String));
        virtualTable.Columns.Add("V430", typeof(System.String));
        virtualTable.Columns.Add("V431", typeof(System.String));
        virtualTable.Columns.Add("V432", typeof(System.String));
        virtualTable.Columns.Add("V433", typeof(System.String));
        virtualTable.Columns.Add("V434", typeof(System.String));
        virtualTable.Columns.Add("V435", typeof(System.String));
        virtualTable.Columns.Add("V436", typeof(System.String));
        virtualTable.Columns.Add("V437", typeof(System.String));
        virtualTable.Columns.Add("V438", typeof(System.String));
        virtualTable.Columns.Add("V439", typeof(System.String));
        virtualTable.Columns.Add("V440", typeof(System.String));
        virtualTable.Columns.Add("V441", typeof(System.String));
        virtualTable.Columns.Add("V442", typeof(System.String));
        virtualTable.Columns.Add("V443", typeof(System.String));
        virtualTable.Columns.Add("V444", typeof(System.String));
        virtualTable.Columns.Add("V445", typeof(System.String));
        virtualTable.Columns.Add("V446", typeof(System.String));
        virtualTable.Columns.Add("V447", typeof(System.String));
        virtualTable.Columns.Add("V448", typeof(System.String));
        virtualTable.Columns.Add("V449", typeof(System.String));
        virtualTable.Columns.Add("V450", typeof(System.String));
        virtualTable.Columns.Add("V451", typeof(System.String));
        virtualTable.Columns.Add("V452", typeof(System.String));
        virtualTable.Columns.Add("V453", typeof(System.String));
        virtualTable.Columns.Add("V454", typeof(System.String));
        virtualTable.Columns.Add("V455", typeof(System.String));
        virtualTable.Columns.Add("V456", typeof(System.String));
        virtualTable.Columns.Add("V457", typeof(System.String));
        virtualTable.Columns.Add("V458", typeof(System.String));
        virtualTable.Columns.Add("V459", typeof(System.String));
        virtualTable.Columns.Add("V460", typeof(System.String));
        virtualTable.Columns.Add("V461", typeof(System.String));
        virtualTable.Columns.Add("V462", typeof(System.String));
        virtualTable.Columns.Add("V463", typeof(System.String));
        virtualTable.Columns.Add("V464", typeof(System.String));
        virtualTable.Columns.Add("V465", typeof(System.String));
        virtualTable.Columns.Add("V466", typeof(System.String));
        virtualTable.Columns.Add("V467", typeof(System.String));
        virtualTable.Columns.Add("V468", typeof(System.String));
        virtualTable.Columns.Add("V469", typeof(System.String));
        virtualTable.Columns.Add("V470", typeof(System.String));
        virtualTable.Columns.Add("V471", typeof(System.String));
        virtualTable.Columns.Add("V472", typeof(System.String));
        virtualTable.Columns.Add("V473", typeof(System.String));
        virtualTable.Columns.Add("V474", typeof(System.String));
        virtualTable.Columns.Add("V475", typeof(System.String));
        virtualTable.Columns.Add("V476", typeof(System.String));
        virtualTable.Columns.Add("V477", typeof(System.String));
        virtualTable.Columns.Add("V478", typeof(System.String));
        virtualTable.Columns.Add("V479", typeof(System.String));
        virtualTable.Columns.Add("V480", typeof(System.String));
        virtualTable.Columns.Add("V481", typeof(System.String));
        virtualTable.Columns.Add("V482", typeof(System.String));
        virtualTable.Columns.Add("V483", typeof(System.String));
        virtualTable.Columns.Add("V484", typeof(System.String));
        virtualTable.Columns.Add("V485", typeof(System.String));
        virtualTable.Columns.Add("V486", typeof(System.String));
        virtualTable.Columns.Add("V487", typeof(System.String));
        virtualTable.Columns.Add("V488", typeof(System.String));
        virtualTable.Columns.Add("V489", typeof(System.String));
        virtualTable.Columns.Add("V490", typeof(System.String));
        virtualTable.Columns.Add("V491", typeof(System.String));
        virtualTable.Columns.Add("V492", typeof(System.String));
        virtualTable.Columns.Add("V493", typeof(System.String));
        virtualTable.Columns.Add("V494", typeof(System.String));
        virtualTable.Columns.Add("V495", typeof(System.String));
        virtualTable.Columns.Add("V496", typeof(System.String));
        virtualTable.Columns.Add("V497", typeof(System.String));
        virtualTable.Columns.Add("V498", typeof(System.String));
        virtualTable.Columns.Add("V499", typeof(System.String));
        virtualTable.Columns.Add("V500", typeof(System.String));
        DataRow virtualRow = virtualTable.NewRow();
        virtualRow["V001"] = record.V001;
        virtualRow["V001"] = record.V001;
        virtualRow["V002"] = record.V002;
        virtualRow["V003"] = record.V003;
        virtualRow["V004"] = record.V004;
        virtualRow["V005"] = record.V005;
        virtualRow["V006"] = record.V006;
        virtualRow["V007"] = record.V007;
        virtualRow["V008"] = record.V008;
        virtualRow["V009"] = record.V009;
        virtualRow["V010"] = record.V010;
        virtualRow["V011"] = record.V011;
        virtualRow["V012"] = record.V012;
        virtualRow["V013"] = record.V013;
        virtualRow["V014"] = record.V014;
        virtualRow["V015"] = record.V015;
        virtualRow["V016"] = record.V016;
        virtualRow["V017"] = record.V017;
        virtualRow["V018"] = record.V018;
        virtualRow["V019"] = record.V019;
        virtualRow["V020"] = record.V020;
        virtualRow["V021"] = record.V021;
        virtualRow["V022"] = record.V022;
        virtualRow["V023"] = record.V023;
        virtualRow["V024"] = record.V024;
        virtualRow["V025"] = record.V025;
        virtualRow["V026"] = record.V026;
        virtualRow["V027"] = record.V027;
        virtualRow["V028"] = record.V028;
        virtualRow["V029"] = record.V029;
        virtualRow["V030"] = record.V030;
        virtualRow["V031"] = record.V031;
        virtualRow["V032"] = record.V032;
        virtualRow["V033"] = record.V033;
        virtualRow["V034"] = record.V034;
        virtualRow["V035"] = record.V035;
        virtualRow["V036"] = record.V036;
        virtualRow["V037"] = record.V037;
        virtualRow["V038"] = record.V038;
        virtualRow["V039"] = record.V039;
        virtualRow["V040"] = record.V040;
        virtualRow["V041"] = record.V041;
        virtualRow["V042"] = record.V042;
        virtualRow["V043"] = record.V043;
        virtualRow["V044"] = record.V044;
        virtualRow["V045"] = record.V045;
        virtualRow["V046"] = record.V046;
        virtualRow["V047"] = record.V047;
        virtualRow["V048"] = record.V048;
        virtualRow["V049"] = record.V049;
        virtualRow["V050"] = record.V050;
        virtualRow["V051"] = record.V051;
        virtualRow["V052"] = record.V052;
        virtualRow["V053"] = record.V053;
        virtualRow["V054"] = record.V054;
        virtualRow["V055"] = record.V055;
        virtualRow["V056"] = record.V056;
        virtualRow["V057"] = record.V057;
        virtualRow["V058"] = record.V058;
        virtualRow["V059"] = record.V059;
        virtualRow["V060"] = record.V060;
        virtualRow["V061"] = record.V061;
        virtualRow["V062"] = record.V062;
        virtualRow["V063"] = record.V063;
        virtualRow["V064"] = record.V064;
        virtualRow["V065"] = record.V065;
        virtualRow["V066"] = record.V066;
        virtualRow["V067"] = record.V067;
        virtualRow["V068"] = record.V068;
        virtualRow["V069"] = record.V069;
        virtualRow["V070"] = record.V070;
        virtualRow["V071"] = record.V071;
        virtualRow["V072"] = record.V072;
        virtualRow["V073"] = record.V073;
        virtualRow["V074"] = record.V074;
        virtualRow["V075"] = record.V075;
        virtualRow["V076"] = record.V076;
        virtualRow["V077"] = record.V077;
        virtualRow["V078"] = record.V078;
        virtualRow["V079"] = record.V079;
        virtualRow["V080"] = record.V080;
        virtualRow["V081"] = record.V081;
        virtualRow["V082"] = record.V082;
        virtualRow["V083"] = record.V083;
        virtualRow["V084"] = record.V084;
        virtualRow["V085"] = record.V085;
        virtualRow["V086"] = record.V086;
        virtualRow["V087"] = record.V087;
        virtualRow["V088"] = record.V088;
        virtualRow["V089"] = record.V089;
        virtualRow["V090"] = record.V090;
        virtualRow["V091"] = record.V091;
        virtualRow["V092"] = record.V092;
        virtualRow["V093"] = record.V093;
        virtualRow["V094"] = record.V094;
        virtualRow["V095"] = record.V095;
        virtualRow["V096"] = record.V096;
        virtualRow["V097"] = record.V097;
        virtualRow["V098"] = record.V098;
        virtualRow["V099"] = record.V099;
        virtualRow["V100"] = record.V100;
        virtualRow["V101"] = record.V101;
        virtualRow["V102"] = record.V102;
        virtualRow["V103"] = record.V103;
        virtualRow["V104"] = record.V104;
        virtualRow["V105"] = record.V105;
        virtualRow["V106"] = record.V106;
        virtualRow["V107"] = record.V107;
        virtualRow["V108"] = record.V108;
        virtualRow["V109"] = record.V109;
        virtualRow["V110"] = record.V110;
        virtualRow["V111"] = record.V111;
        virtualRow["V112"] = record.V112;
        virtualRow["V113"] = record.V113;
        virtualRow["V114"] = record.V114;
        virtualRow["V115"] = record.V115;
        virtualRow["V116"] = record.V116;
        virtualRow["V117"] = record.V117;
        virtualRow["V118"] = record.V118;
        virtualRow["V119"] = record.V119;
        virtualRow["V120"] = record.V120;
        virtualRow["V121"] = record.V121;
        virtualRow["V122"] = record.V122;
        virtualRow["V123"] = record.V123;
        virtualRow["V124"] = record.V124;
        virtualRow["V125"] = record.V125;
        virtualRow["V126"] = record.V126;
        virtualRow["V127"] = record.V127;
        virtualRow["V128"] = record.V128;
        virtualRow["V129"] = record.V129;
        virtualRow["V130"] = record.V130;
        virtualRow["V131"] = record.V131;
        virtualRow["V132"] = record.V132;
        virtualRow["V133"] = record.V133;
        virtualRow["V134"] = record.V134;
        virtualRow["V135"] = record.V135;
        virtualRow["V136"] = record.V136;
        virtualRow["V137"] = record.V137;
        virtualRow["V138"] = record.V138;
        virtualRow["V139"] = record.V139;
        virtualRow["V140"] = record.V140;
        virtualRow["V141"] = record.V141;
        virtualRow["V142"] = record.V142;
        virtualRow["V143"] = record.V143;
        virtualRow["V144"] = record.V144;
        virtualRow["V145"] = record.V145;
        virtualRow["V146"] = record.V146;
        virtualRow["V147"] = record.V147;
        virtualRow["V148"] = record.V148;
        virtualRow["V149"] = record.V149;
        virtualRow["V150"] = record.V150;
        virtualRow["V151"] = record.V151;
        virtualRow["V152"] = record.V152;
        virtualRow["V153"] = record.V153;
        virtualRow["V154"] = record.V154;
        virtualRow["V155"] = record.V155;
        virtualRow["V156"] = record.V156;
        virtualRow["V157"] = record.V157;
        virtualRow["V158"] = record.V158;
        virtualRow["V159"] = record.V159;
        virtualRow["V160"] = record.V160;
        virtualRow["V161"] = record.V161;
        virtualRow["V162"] = record.V162;
        virtualRow["V163"] = record.V163;
        virtualRow["V164"] = record.V164;
        virtualRow["V165"] = record.V165;
        virtualRow["V166"] = record.V166;
        virtualRow["V167"] = record.V167;
        virtualRow["V168"] = record.V168;
        virtualRow["V169"] = record.V169;
        virtualRow["V170"] = record.V170;
        virtualRow["V171"] = record.V171;
        virtualRow["V172"] = record.V172;
        virtualRow["V173"] = record.V173;
        virtualRow["V174"] = record.V174;
        virtualRow["V175"] = record.V175;
        virtualRow["V176"] = record.V176;
        virtualRow["V177"] = record.V177;
        virtualRow["V178"] = record.V178;
        virtualRow["V179"] = record.V179;
        virtualRow["V180"] = record.V180;
        virtualRow["V181"] = record.V181;
        virtualRow["V182"] = record.V182;
        virtualRow["V183"] = record.V183;
        virtualRow["V184"] = record.V184;
        virtualRow["V185"] = record.V185;
        virtualRow["V186"] = record.V186;
        virtualRow["V187"] = record.V187;
        virtualRow["V188"] = record.V188;
        virtualRow["V189"] = record.V189;
        virtualRow["V190"] = record.V190;
        virtualRow["V191"] = record.V191;
        virtualRow["V192"] = record.V192;
        virtualRow["V193"] = record.V193;
        virtualRow["V194"] = record.V194;
        virtualRow["V195"] = record.V195;
        virtualRow["V196"] = record.V196;
        virtualRow["V197"] = record.V197;
        virtualRow["V198"] = record.V198;
        virtualRow["V199"] = record.V199;
        virtualRow["V200"] = record.V200;
        virtualRow["V201"] = record.V201;
        virtualRow["V202"] = record.V202;
        virtualRow["V203"] = record.V203;
        virtualRow["V204"] = record.V204;
        virtualRow["V205"] = record.V205;
        virtualRow["V206"] = record.V206;
        virtualRow["V207"] = record.V207;
        virtualRow["V208"] = record.V208;
        virtualRow["V209"] = record.V209;
        virtualRow["V210"] = record.V210;
        virtualRow["V211"] = record.V211;
        virtualRow["V212"] = record.V212;
        virtualRow["V213"] = record.V213;
        virtualRow["V214"] = record.V214;
        virtualRow["V215"] = record.V215;
        virtualRow["V216"] = record.V216;
        virtualRow["V217"] = record.V217;
        virtualRow["V218"] = record.V218;
        virtualRow["V219"] = record.V219;
        virtualRow["V220"] = record.V220;
        virtualRow["V221"] = record.V221;
        virtualRow["V222"] = record.V222;
        virtualRow["V223"] = record.V223;
        virtualRow["V224"] = record.V224;
        virtualRow["V225"] = record.V225;
        virtualRow["V226"] = record.V226;
        virtualRow["V227"] = record.V227;
        virtualRow["V228"] = record.V228;
        virtualRow["V229"] = record.V229;
        virtualRow["V230"] = record.V230;
        virtualRow["V231"] = record.V231;
        virtualRow["V232"] = record.V232;
        virtualRow["V233"] = record.V233;
        virtualRow["V234"] = record.V234;
        virtualRow["V235"] = record.V235;
        virtualRow["V236"] = record.V236;
        virtualRow["V237"] = record.V237;
        virtualRow["V238"] = record.V238;
        virtualRow["V239"] = record.V239;
        virtualRow["V240"] = record.V240;
        virtualRow["V241"] = record.V241;
        virtualRow["V242"] = record.V242;
        virtualRow["V243"] = record.V243;
        virtualRow["V244"] = record.V244;
        virtualRow["V245"] = record.V245;
        virtualRow["V246"] = record.V246;
        virtualRow["V247"] = record.V247;
        virtualRow["V248"] = record.V248;
        virtualRow["V249"] = record.V249;
        virtualRow["V250"] = record.V250;
        virtualRow["V251"] = record.V251;
        virtualRow["V252"] = record.V252;
        virtualRow["V253"] = record.V253;
        virtualRow["V254"] = record.V254;
        virtualRow["V255"] = record.V255;
        virtualRow["V256"] = record.V256;
        virtualRow["V257"] = record.V257;
        virtualRow["V258"] = record.V258;
        virtualRow["V259"] = record.V259;
        virtualRow["V260"] = record.V260;
        virtualRow["V261"] = record.V261;
        virtualRow["V262"] = record.V262;
        virtualRow["V263"] = record.V263;
        virtualRow["V264"] = record.V264;
        virtualRow["V265"] = record.V265;
        virtualRow["V266"] = record.V266;
        virtualRow["V267"] = record.V267;
        virtualRow["V268"] = record.V268;
        virtualRow["V269"] = record.V269;
        virtualRow["V270"] = record.V270;
        virtualRow["V271"] = record.V271;
        virtualRow["V272"] = record.V272;
        virtualRow["V273"] = record.V273;
        virtualRow["V274"] = record.V274;
        virtualRow["V275"] = record.V275;
        virtualRow["V276"] = record.V276;
        virtualRow["V277"] = record.V277;
        virtualRow["V278"] = record.V278;
        virtualRow["V279"] = record.V279;
        virtualRow["V280"] = record.V280;
        virtualRow["V281"] = record.V281;
        virtualRow["V282"] = record.V282;
        virtualRow["V283"] = record.V283;
        virtualRow["V284"] = record.V284;
        virtualRow["V285"] = record.V285;
        virtualRow["V286"] = record.V286;
        virtualRow["V287"] = record.V287;
        virtualRow["V288"] = record.V288;
        virtualRow["V289"] = record.V289;
        virtualRow["V290"] = record.V290;
        virtualRow["V291"] = record.V291;
        virtualRow["V292"] = record.V292;
        virtualRow["V293"] = record.V293;
        virtualRow["V294"] = record.V294;
        virtualRow["V295"] = record.V295;
        virtualRow["V296"] = record.V296;
        virtualRow["V297"] = record.V297;
        virtualRow["V298"] = record.V298;
        virtualRow["V299"] = record.V299;
        virtualRow["V300"] = record.V300;
        virtualRow["V301"] = record.V301;
        virtualRow["V302"] = record.V302;
        virtualRow["V303"] = record.V303;
        virtualRow["V304"] = record.V304;
        virtualRow["V305"] = record.V305;
        virtualRow["V306"] = record.V306;
        virtualRow["V307"] = record.V307;
        virtualRow["V308"] = record.V308;
        virtualRow["V309"] = record.V309;
        virtualRow["V310"] = record.V310;
        virtualRow["V311"] = record.V311;
        virtualRow["V312"] = record.V312;
        virtualRow["V313"] = record.V313;
        virtualRow["V314"] = record.V314;
        virtualRow["V315"] = record.V315;
        virtualRow["V316"] = record.V316;
        virtualRow["V317"] = record.V317;
        virtualRow["V318"] = record.V318;
        virtualRow["V319"] = record.V319;
        virtualRow["V320"] = record.V320;
        virtualRow["V321"] = record.V321;
        virtualRow["V322"] = record.V322;
        virtualRow["V323"] = record.V323;
        virtualRow["V324"] = record.V324;
        virtualRow["V325"] = record.V325;
        virtualRow["V326"] = record.V326;
        virtualRow["V327"] = record.V327;
        virtualRow["V328"] = record.V328;
        virtualRow["V329"] = record.V329;
        virtualRow["V330"] = record.V330;
        virtualRow["V331"] = record.V331;
        virtualRow["V332"] = record.V332;
        virtualRow["V333"] = record.V333;
        virtualRow["V334"] = record.V334;
        virtualRow["V335"] = record.V335;
        virtualRow["V336"] = record.V336;
        virtualRow["V337"] = record.V337;
        virtualRow["V338"] = record.V338;
        virtualRow["V339"] = record.V339;
        virtualRow["V340"] = record.V340;
        virtualRow["V341"] = record.V341;
        virtualRow["V342"] = record.V342;
        virtualRow["V343"] = record.V343;
        virtualRow["V344"] = record.V344;
        virtualRow["V345"] = record.V345;
        virtualRow["V346"] = record.V346;
        virtualRow["V347"] = record.V347;
        virtualRow["V348"] = record.V348;
        virtualRow["V349"] = record.V349;
        virtualRow["V350"] = record.V350;
        virtualRow["V351"] = record.V351;
        virtualRow["V352"] = record.V352;
        virtualRow["V353"] = record.V353;
        virtualRow["V354"] = record.V354;
        virtualRow["V355"] = record.V355;
        virtualRow["V356"] = record.V356;
        virtualRow["V357"] = record.V357;
        virtualRow["V358"] = record.V358;
        virtualRow["V359"] = record.V359;
        virtualRow["V360"] = record.V360;
        virtualRow["V361"] = record.V361;
        virtualRow["V362"] = record.V362;
        virtualRow["V363"] = record.V363;
        virtualRow["V364"] = record.V364;
        virtualRow["V365"] = record.V365;
        virtualRow["V366"] = record.V366;
        virtualRow["V367"] = record.V367;
        virtualRow["V368"] = record.V368;
        virtualRow["V369"] = record.V369;
        virtualRow["V370"] = record.V370;
        virtualRow["V371"] = record.V371;
        virtualRow["V372"] = record.V372;
        virtualRow["V373"] = record.V373;
        virtualRow["V374"] = record.V374;
        virtualRow["V375"] = record.V375;
        virtualRow["V376"] = record.V376;
        virtualRow["V377"] = record.V377;
        virtualRow["V378"] = record.V378;
        virtualRow["V379"] = record.V379;
        virtualRow["V380"] = record.V380;
        virtualRow["V381"] = record.V381;
        virtualRow["V382"] = record.V382;
        virtualRow["V383"] = record.V383;
        virtualRow["V384"] = record.V384;
        virtualRow["V385"] = record.V385;
        virtualRow["V386"] = record.V386;
        virtualRow["V387"] = record.V387;
        virtualRow["V388"] = record.V388;
        virtualRow["V389"] = record.V389;
        virtualRow["V390"] = record.V390;
        virtualRow["V391"] = record.V391;
        virtualRow["V392"] = record.V392;
        virtualRow["V393"] = record.V393;
        virtualRow["V394"] = record.V394;
        virtualRow["V395"] = record.V395;
        virtualRow["V396"] = record.V396;
        virtualRow["V397"] = record.V397;
        virtualRow["V398"] = record.V398;
        virtualRow["V399"] = record.V399;
        virtualRow["V400"] = record.V400;
        virtualRow["V401"] = record.V401;
        virtualRow["V402"] = record.V402;
        virtualRow["V403"] = record.V403;
        virtualRow["V404"] = record.V404;
        virtualRow["V405"] = record.V405;
        virtualRow["V406"] = record.V406;
        virtualRow["V407"] = record.V407;
        virtualRow["V408"] = record.V408;
        virtualRow["V409"] = record.V409;
        virtualRow["V410"] = record.V410;
        virtualRow["V411"] = record.V411;
        virtualRow["V412"] = record.V412;
        virtualRow["V413"] = record.V413;
        virtualRow["V414"] = record.V414;
        virtualRow["V415"] = record.V415;
        virtualRow["V416"] = record.V416;
        virtualRow["V417"] = record.V417;
        virtualRow["V418"] = record.V418;
        virtualRow["V419"] = record.V419;
        virtualRow["V420"] = record.V420;
        virtualRow["V421"] = record.V421;
        virtualRow["V422"] = record.V422;
        virtualRow["V423"] = record.V423;
        virtualRow["V424"] = record.V424;
        virtualRow["V425"] = record.V425;
        virtualRow["V426"] = record.V426;
        virtualRow["V427"] = record.V427;
        virtualRow["V428"] = record.V428;
        virtualRow["V429"] = record.V429;
        virtualRow["V430"] = record.V430;
        virtualRow["V431"] = record.V431;
        virtualRow["V432"] = record.V432;
        virtualRow["V433"] = record.V433;
        virtualRow["V434"] = record.V434;
        virtualRow["V435"] = record.V435;
        virtualRow["V436"] = record.V436;
        virtualRow["V437"] = record.V437;
        virtualRow["V438"] = record.V438;
        virtualRow["V439"] = record.V439;
        virtualRow["V440"] = record.V440;
        virtualRow["V441"] = record.V441;
        virtualRow["V442"] = record.V442;
        virtualRow["V443"] = record.V443;
        virtualRow["V444"] = record.V444;
        virtualRow["V445"] = record.V445;
        virtualRow["V446"] = record.V446;
        virtualRow["V447"] = record.V447;
        virtualRow["V448"] = record.V448;
        virtualRow["V449"] = record.V449;
        virtualRow["V450"] = record.V450;
        virtualRow["V451"] = record.V451;
        virtualRow["V452"] = record.V452;
        virtualRow["V453"] = record.V453;
        virtualRow["V454"] = record.V454;
        virtualRow["V455"] = record.V455;
        virtualRow["V456"] = record.V456;
        virtualRow["V457"] = record.V457;
        virtualRow["V458"] = record.V458;
        virtualRow["V459"] = record.V459;
        virtualRow["V460"] = record.V460;
        virtualRow["V461"] = record.V461;
        virtualRow["V462"] = record.V462;
        virtualRow["V463"] = record.V463;
        virtualRow["V464"] = record.V464;
        virtualRow["V465"] = record.V465;
        virtualRow["V466"] = record.V466;
        virtualRow["V467"] = record.V467;
        virtualRow["V468"] = record.V468;
        virtualRow["V469"] = record.V469;
        virtualRow["V470"] = record.V470;
        virtualRow["V471"] = record.V471;
        virtualRow["V472"] = record.V472;
        virtualRow["V473"] = record.V473;
        virtualRow["V474"] = record.V474;
        virtualRow["V475"] = record.V475;
        virtualRow["V476"] = record.V476;
        virtualRow["V477"] = record.V477;
        virtualRow["V478"] = record.V478;
        virtualRow["V479"] = record.V479;
        virtualRow["V480"] = record.V480;
        virtualRow["V481"] = record.V481;
        virtualRow["V482"] = record.V482;
        virtualRow["V483"] = record.V483;
        virtualRow["V484"] = record.V484;
        virtualRow["V485"] = record.V485;
        virtualRow["V486"] = record.V486;
        virtualRow["V487"] = record.V487;
        virtualRow["V488"] = record.V488;
        virtualRow["V489"] = record.V489;
        virtualRow["V490"] = record.V490;
        virtualRow["V491"] = record.V491;
        virtualRow["V492"] = record.V492;
        virtualRow["V493"] = record.V493;
        virtualRow["V494"] = record.V494;
        virtualRow["V495"] = record.V495;
        virtualRow["V496"] = record.V496;
        virtualRow["V497"] = record.V497;
        virtualRow["V498"] = record.V498;
        virtualRow["V499"] = record.V499;
        virtualRow["V500"] = record.V500;
        virtualTable.Rows.Add(virtualRow);

        return virtualTable;
    }
}



public class Cryptography
{

    #region Fields



    private static byte[] key = { };

    private static byte[] IV = { 38, 55, 206, 48, 28, 64, 20, 16 };

    //private static string stringKey = "!5663a#KN";

    //private static string stringKey = HttpContext.Current.Session.SessionID;

    private static string stringKey = "dbg12!12345";
    #endregion



    #region Public Methods






    public static string Encrypt(string text)
    {
        //HttpContext.Current.Session.SessionID

        try
        {
            if (text == "")
                return "";

            key = Encoding.UTF8.GetBytes(stringKey.Substring(0, 8));



            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            Byte[] byteArray = Encoding.UTF8.GetBytes(text);



            MemoryStream memoryStream = new MemoryStream();

            CryptoStream cryptoStream = new CryptoStream(memoryStream,

                des.CreateEncryptor(key, IV), CryptoStreamMode.Write);



            cryptoStream.Write(byteArray, 0, byteArray.Length);

            cryptoStream.FlushFinalBlock();


            string strValue = Convert.ToBase64String(memoryStream.ToArray());
            //StringWriter writer = new StringWriter();
            //HttpContext.Current.Server.UrlEncode(strValue, writer);
            System.Web.HttpUtility.UrlEncode(strValue);
            //return writer.ToString();

            return strValue;

        }

        catch (Exception ex)
        {

            // Handle Exception Here

        }



        return string.Empty;

    }



    public static string Decrypt(string text)
    {

        try
        {
            if (text.Trim() == "")
                return string.Empty;

            text = text.Replace(' ', '+');
            key = Encoding.UTF8.GetBytes(stringKey.Substring(0, 8));



            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            Byte[] byteArray = Convert.FromBase64String(text);



            MemoryStream memoryStream = new MemoryStream();

            CryptoStream cryptoStream = new CryptoStream(memoryStream,

                des.CreateDecryptor(key, IV), CryptoStreamMode.Write);



            cryptoStream.Write(byteArray, 0, byteArray.Length);

            cryptoStream.FlushFinalBlock();



            return Encoding.UTF8.GetString(memoryStream.ToArray());

        }

        catch (Exception ex)
        {

            // Handle Exception Here

        }



        return string.Empty;

    }



    public static string EncryptStatic(string text)
    {
        //HttpContext.Current.Session.SessionID

        try
        {

            key = Encoding.UTF8.GetBytes("tyu367xj");



            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            Byte[] byteArray = Encoding.UTF8.GetBytes(text);



            MemoryStream memoryStream = new MemoryStream();

            CryptoStream cryptoStream = new CryptoStream(memoryStream,

                des.CreateEncryptor(key, IV), CryptoStreamMode.Write);



            cryptoStream.Write(byteArray, 0, byteArray.Length);

            cryptoStream.FlushFinalBlock();


            string strValue = Convert.ToBase64String(memoryStream.ToArray());
            StringWriter writer = new StringWriter();
            HttpContext.Current.Server.UrlEncode(strValue, writer);

            return writer.ToString();

        }

        catch (Exception ex)
        {

            // Handle Exception Here

        }



        return string.Empty;

    }

    public static string DecryptStatic(string text)
    {

        try
        {
            text = text.Replace(' ', '+');
            key = Encoding.UTF8.GetBytes("tyu367xj");



            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            Byte[] byteArray = Convert.FromBase64String(text);



            MemoryStream memoryStream = new MemoryStream();

            CryptoStream cryptoStream = new CryptoStream(memoryStream,

                des.CreateDecryptor(key, IV), CryptoStreamMode.Write);



            cryptoStream.Write(byteArray, 0, byteArray.Length);

            cryptoStream.FlushFinalBlock();



            return Encoding.UTF8.GetString(memoryStream.ToArray());

        }

        catch (Exception ex)
        {

            // Handle Exception Here

        }



        return string.Empty;

    }

    #endregion

}



﻿using GenericParsing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Xml;
using ExCSS;

//using EMD;

/// <summary>
/// Summary description for SuperMethod
/// </summary>
/// 


public partial class UploadManager
{
    public static void UploadCSV(int? iUserID, Table theTable, string strBatchDescription,
  string strOriginalFileName, Guid? guidNew, string strImportFolder, out string strMsg, out int iBatchID,
     string strFileExtension, string strSelectedSheet, int? iAccountID,
     bool? bAllowDataUpload, int? iImportTemplateID, int? iSourceBatchID, List<object> objList)
    {

        //bool bHas_SystemName_2nd = false;      
        string strDBName = Common.GetDatabaseName();
        iBatchID = -1;
        strMsg = "";


        try
        {
            if (theTable != null && iAccountID != null && theTable.AccountID != null && theTable.AccountID != iAccountID)
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session["AccountID"] != null)
                {
                    iAccountID = int.Parse(HttpContext.Current.Session["AccountID"].ToString());//theTable.AccountID;
                }
                else
                {
                    iAccountID = theTable.AccountID;
                }
            }


            //string strImportHeaderName = "ImportHeaderName";
            Batch theSourceBatch = null;
            if (iSourceBatchID != null)
            {
                theSourceBatch = UploadManager.ets_Batch_Details((int)iSourceBatchID);
                if (theSourceBatch == null)
                {
                    return;
                }


                if (theTable == null)
                    theTable = RecordManager.ets_Table_Details((int)theSourceBatch.TableID);


                if (iUserID == null)
                    iUserID = theSourceBatch.UserIDUploaded;

                if (iAccountID == null && theTable != null)
                {
                    if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session["AccountID"] != null)
                    {
                        iAccountID = int.Parse(HttpContext.Current.Session["AccountID"].ToString());//theTable.AccountID;
                    }
                    else
                    {
                        iAccountID = theTable.AccountID;
                    }
                }


                // int.Parse(HttpContext.Current.Session["AccountID"].ToString());// theTable.AccountID;

                if (iAccountID == null && theSourceBatch != null && theSourceBatch.AccountID != null)
                    iAccountID = theSourceBatch.AccountID;// int.Parse(HttpContext.Current.Session["AccountID"].ToString());//theSourceBatch.AccountID;
            }

            if (iAccountID == null && HttpContext.Current != null && HttpContext.Current.Session != null
                && HttpContext.Current.Session["AccountID"] != null)
            {
                iAccountID = int.Parse(HttpContext.Current.Session["AccountID"].ToString());//
            }




            if (iUserID == null && iAccountID != null)
            {
                int? iAutoUserID = int.Parse(SystemData.SystemOption_ValueByKey_Account("AutoUploadUserID", null, null));
                iUserID = iAutoUserID;
                if (iUserID == null)
                {
                    User theAccountHolder = SecurityManager.User_AccountHolder((int)iAccountID);
                    if (theAccountHolder != null && theAccountHolder.UserID != null)
                    {
                        iUserID = theAccountHolder.UserID;
                    }
                }
            }

            //if (strFileExtension.ToLower()!="virtual" && iImportTemplateID == null && theTable.DefaultImportTemplateID != null
            //    && (iSourceBatchID != null || iUserID == iAutoUserID)) 
            //{
            //    iImportTemplateID = theTable.DefaultImportTemplateID;

            //}

            //if (strFileExtension.ToLower()=="virtual" && iImportTemplateID == null
            //    && theTable.DefaultImportTemplateID != null && strImportFolder!="") //ALS Labdata only
            //{
            //    iImportTemplateID = theTable.DefaultImportTemplateID;
            //}


            if (iImportTemplateID == null)
            {
                iImportTemplateID = ImportManager.GetDefaultImportTemplate((int)theTable.TableID);
            }

            bool bCheckIgnoreMidnight = false;

            //JA 20 JAN 2017 
            //string strIgnoreMidnight = SystemData.SystemOption_ValueByKey_Account("Time Calculation Ignore Midnight", (int)theTable.AccountID, theTable.TableID);
            //int.Parse(HttpContext.Current.Session["AccountID"].ToString()) can be null - MR
            string strIgnoreMidnight = SystemData.SystemOption_ValueByKey_Account("Time Calculation Ignore Midnight", iAccountID, theTable.TableID);


            if (strIgnoreMidnight != "" && strIgnoreMidnight.ToString().ToLower() == "yes")
            {
                bCheckIgnoreMidnight = true;
            }


            ImportTemplate theImportTemplate = null;
            string strUniqueColumnIDSys = "";
            string strUniqueColumnID2Sys = "";

            if (theTable.UniqueColumnID != null)
                strUniqueColumnIDSys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE ColumnID=" + theTable.UniqueColumnID.ToString());

            if (theTable.UniqueColumnID2 != null)
                strUniqueColumnID2Sys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE ColumnID=" + theTable.UniqueColumnID2.ToString());

            //JA 20 2017
            //string strShowExceedances = SystemData.SystemOption_ValueByKey_Account("Show Exceedances", theTable.AccountID, theTable.TableID);
            string strShowExceedances = SystemData.SystemOption_ValueByKey_Account("Show Exceedances", iAccountID, theTable.TableID);


            bool bShowExceedances = false;

            if (strShowExceedances != "" && strShowExceedances.ToLower() == "yes")
            {
                bShowExceedances = true;
            }

            if (iImportTemplateID != null)
                theImportTemplate = ImportManager.dbg_ImportTemplate_Detail((int)iImportTemplateID);

            //if (theImportTemplate != null)
            //{
            //    theTable.ImportColumnHeaderRow = theImportTemplate.ImportColumnHeaderRow;
            //    theTable.ImportDataStartRow = theImportTemplate.ImportDataStartRow;
            //}

            //lets get date and time column name
            //            string strDateTimeColumnName = "";
            //            bool bIsImportPositional = false;
            //            if (theImportTemplate != null && theImportTemplate.IsImportPositional != null && (bool)theImportTemplate.IsImportPositional)
            //            {
            //                bIsImportPositional = true;
            //            }

            //            if (bIsImportPositional == false)
            //            {
            //                if (iImportTemplateID != null)
            //                {
            //                    strDateTimeColumnName = Common.GetValueFromSQL(@"SELECT      ITI.Name_OnImport FROM ImportTemplateItem ITI INNER JOIN [Column] C
            //                            ON ITI.ColumnID=C.ColumnID WHERE ITI.ImportTemplateID=" + iImportTemplateID.ToString() + @"                   
            //                    AND C.SystemName='DateTimeRecorded' AND C.TableID=" + theTable.TableID.ToString());

            //                }

            ////                if (strDateTimeColumnName == "")
            ////                {
            ////                    strDateTimeColumnName = Common.GetValueFromSQL(@"SELECT      Name_OnImport 
            ////                    FROM  [Column]
            ////                    WHERE SystemName='DateTimeRecorded' AND TableID=" + theTable.TableID.ToString());

            ////                }




            //                if (strDateTimeColumnName != "")
            //                {

            //                    if (strDateTimeColumnName.IndexOf(",") > 0)
            //                    {
            //                        strDateRecordedColumnName = strDateTimeColumnName.Substring(0, strDateTimeColumnName.IndexOf(","));
            //                        strTimeSamledColumnName = strDateTimeColumnName.Substring(strDateTimeColumnName.IndexOf(",") + 1);
            //                    }
            //                    else
            //                    {
            //                        //bIsDateSingleColumn = true;
            //                        strDateRecordedColumnName = strDateTimeColumnName;
            //                        strTimeSamledColumnName = "";
            //                    }
            //                }
            //            }
            //            else
            //            {


            //                DataTable dtDateTimeColumnName = Common.DataTableFromText(@"SELECT      
            //                        PositionOnImport FROM  [ImportTemplateItem] ITI JOIN [Column] C ON C.ColumnID=ITI.ColumnID
            //                        JOIN [Table] T ON C.TableID=T.TableID
            //                        WHERE SystemName='DateTimeRecorded' AND C.TableID=" + theTable.TableID.ToString()
            //                           + " AND ITI.ImportTemplateID="+ iImportTemplateID.ToString());
            //                if (dtDateTimeColumnName.Rows.Count > 0 && dtDateTimeColumnName.Rows[0][0] != DBNull.Value)
            //                {

            //                    //if (dtDateTimeColumnName.Rows[0]["IsDateSingleColumn"].ToString().ToLower() == "true")
            //                    //{
            //                    //    bIsDateSingleColumn = true;
            //                    //}
            //                    string strPositionOnImport = dtDateTimeColumnName.Rows[0]["PositionOnImport"].ToString();
            //                    string sD = "";
            //                    string sT = "";
            //                    if (strPositionOnImport.IndexOf(",") > 0)
            //                    {
            //                        sD = strPositionOnImport.Substring(0, strPositionOnImport.IndexOf(","));
            //                        sT = strPositionOnImport.Substring(strPositionOnImport.IndexOf(",") + 1);
            //                    }
            //                    else
            //                    {
            //                        //bIsDateSingleColumn = true;
            //                        sD = strPositionOnImport;
            //                        sT = "";
            //                    }
            //                    iDatePosition = int.Parse(sD);
            //                    if (sT != "")
            //                        iTimePosition = int.Parse(sT);
            //                }

            //            }



            if (objList != null)
            {
                try
                {
                    foreach (object obj in objList)
                    {
                        if (obj.GetType().Name == "ImportTemplate")
                        {
                            ImportTemplate tempImportTemplate = (ImportTemplate)obj;
                            if (tempImportTemplate != null)
                            {
                                if (tempImportTemplate.TempImportColumnHeaderRow != null)
                                    theImportTemplate.ImportColumnHeaderRow = tempImportTemplate.TempImportColumnHeaderRow;

                                if (tempImportTemplate.TempImportDataStartRow != null)
                                    theImportTemplate.ImportDataStartRow = tempImportTemplate.TempImportDataStartRow;
                            }
                        }
                    }
                }
                catch
                {
                    //
                }

            }



            DataTable dtImportFileTable;



            dtImportFileTable = null;
            List<string> listIgnoredColumns = new List<string>();


            string strTemp = "";

            int z = 0;

            string strFileUniqueNameCM = "";
            if (iSourceBatchID == null)
            {

                string strFileUniqueName = guidNew.ToString() + strFileExtension;
                strFileUniqueNameCM = strFileUniqueName;
                bool isPreprocessed = false;

                if ((theImportTemplate != null) && (theImportTemplate.FileFormat == "other") && !String.IsNullOrEmpty(theImportTemplate.SPProcessFile))
                {
                    dtImportFileTable = UploadManager.GetPreProcessorCSharpImportFileTable(strImportFolder, strFileUniqueName, theImportTemplate.SPProcessFile, out strMsg);
                    theImportTemplate.ImportColumnHeaderRow = 0;
                    theImportTemplate.ImportDataStartRow = 1;
                    isPreprocessed = true;
                }
                else
                {
                    switch (strFileExtension.ToLower())
                    {
                        case ".dbf":
                            dtImportFileTable = UploadManager.GetImportFileTableFromDBF(strImportFolder, strFileUniqueName, ref strMsg);
                            z = 0;
                            break;

                        case ".txt":
                            dtImportFileTable = UploadManager.GetImportFileTableFromText(strImportFolder, strFileUniqueName, ref strMsg);
                            z = 0;
                            break;
                        case ".csv":
                            dtImportFileTable = UploadManager.GetImportFileTableFromCSV(strImportFolder, strFileUniqueName, ref strMsg);
                            z = 0;
                            break;
                        case ".xls":
                            //RP Modified Ticket 4643 - Invalid Columns Showing Blanks - (RemoveF = true)
                            dtImportFileTable = OfficeManager.GetImportFileTableFromXLSX(strImportFolder, strFileUniqueName, strSelectedSheet, false);
                            break;
                        case ".xlsx":
                            //RP Modified Ticket 4643 - Invalid Columns Showing Blanks - (RemoveF = true)
                            dtImportFileTable = OfficeManager.GetImportFileTableFromXLSX(strImportFolder, strFileUniqueName, strSelectedSheet, false);
                            break;

                        case ".xml":
                            dtImportFileTable = UploadManager.GetImportFileTableFromXML(strImportFolder, strFileUniqueName);
                            //"ImportHeaderName" = "DisplayName";

                            break;

                        case "virtual":
                            //dtImportFileTable = UploadManager.GetVirtualImportFileTable(BitConverter.ToInt32(((Guid)guidNew).ToByteArray(), 8),
                            //    theTable.TableID.Value,
                            //    BitConverter.ToInt32(((Guid)guidNew).ToByteArray(), 0));
                            dtImportFileTable = MappedImport.GetVirtualImportFileTable(
                                BitConverter.ToInt32(((Guid)guidNew).ToByteArray(), 12),
                                BitConverter.ToInt32(((Guid)guidNew).ToByteArray(), 8),
                                theTable.TableID.Value,
                                BitConverter.ToInt32(((Guid)guidNew).ToByteArray(), 0));
                                
                                theImportTemplate.ImportColumnHeaderRow = 1;
                            theImportTemplate.ImportDataStartRow = 2;

                            //if (theImportTemplate != null)
                            //{

                            //    theImportTemplate.ImportColumnHeaderRow = null;
                            //    theImportTemplate.ImportDataStartRow = null;
                            //}

                            //switch (strSelectedSheet)
                            //{
                            //    case "Novecom Meteo":
                            //        EMD.NovecomMeteoData meteoDataSource = new NovecomMeteoData();
                            //        dtImportFileTable = meteoDataSource.GetNovecomMeteoData(
                            //            EMD.NovecomMeteoData.NovecomMeteoDataType.General, null);
                            //        isPreprocessed = true;
                            //        break;
                            //    default:
                            //        dtImportFileTable = UploadManager.GetVirtualImportFileTable(BitConverter.ToInt32(((Guid)guidNew).ToByteArray(), 8),
                            //            theTable.TableID.Value,
                            //            BitConverter.ToInt32(((Guid)guidNew).ToByteArray(), 0));
                            //        theImportTemplate.ImportColumnHeaderRow = 1;
                            //        theImportTemplate.ImportDataStartRow = 2;
                            //        break;
                            //}
                            break;

                        case "preprocessorsql":
                            dtImportFileTable = UploadManager.GetPreProcessorSQLImportFileTable(BitConverter.ToInt32(((Guid)guidNew).ToByteArray(), 0), theImportTemplate.SPProcessFile, out strMsg);
                            theImportTemplate.ImportColumnHeaderRow = 0;
                            theImportTemplate.ImportDataStartRow = 1;
                            isPreprocessed = true;
                            break;
                        default:
                            dtImportFileTable = UploadManager.GetImportFileTableFromText(strImportFolder, strFileUniqueName, ref strMsg);
                            z = 0;
                            break;
                    }
                }

                if (strMsg != "")
                {

                    return;

                }

                //Update the file DataTable at the start point by ImportTemplate.DataTableStartCustomMethod
                try
                {
                    if (theImportTemplate != null && dtImportFileTable != null)
                    {
                        string strDataTableStartCustomMethod = Common.GetValueFromSQL("SELECT DataTableStartCustomMethod FROM [ImportTemplate] WHERE ImportTemplateID=" + theImportTemplate.ImportTemplateID.ToString());
                        if (!string.IsNullOrEmpty(strDataTableStartCustomMethod))
                        {
                            List<object> roList = new List<object>();
                            List<object> ObjList = new List<object>();
                            ObjList.Add(dtImportFileTable);
                            roList = CustomMethod.DotNetMethod(strDataTableStartCustomMethod, ObjList);
                            if (roList != null)
                            {
                                foreach (object obj in roList)
                                {
                                    if (obj.GetType().Name == "DataTable")
                                    {
                                        dtImportFileTable = (DataTable)obj;
                                    }
                                }

                            }
                        }

                    }

                }
                catch (Exception ex)
                {
                    //catch MR
                }




                if ((strFileExtension.ToLower() != ".dbf") && !isPreprocessed)
                {
                    if (theImportTemplate.ImportColumnHeaderRow == null)
                        theImportTemplate.ImportColumnHeaderRow = 1;
                    if (theImportTemplate.ImportDataStartRow == null)
                        theImportTemplate.ImportDataStartRow = 2;

                    if (theImportTemplate.ImportColumnHeaderRow != null)
                    {
                        if (dtImportFileTable == null)
                        {
                            strMsg = "An error hass occured reading the upload file. Kindly check the upload file template";
                            return;
                        }

                        //if ((int)theTable.ImportColumnHeaderRow > 1)
                        //{
                        if (dtImportFileTable.Rows.Count >= (int)theImportTemplate.ImportColumnHeaderRow)
                        {
                            for (int i = 0; i <= dtImportFileTable.Columns.Count - 1; i++)
                            {
                                if (dtImportFileTable.Rows[(int)theImportTemplate.ImportColumnHeaderRow - 1][i].ToString() == "")
                                {
                                    //do nothing for it
                                    if (strFileExtension.ToLower() == ".csv")
                                    {
                                        try
                                        {
                                            dtImportFileTable.Columns[i].ColumnName = "Column" + (i + 1).ToString();
                                        }
                                        catch
                                        {
                                            //
                                        }
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        dtImportFileTable.Columns[i].ColumnName = dtImportFileTable.Rows[(int)theImportTemplate.ImportColumnHeaderRow - 1][i].ToString();
                                    }
                                    catch (Exception ex)
                                    {
                                        if (ex.Message.IndexOf("already belongs to this DataTable") > -1)
                                        {
                                            for (int j = 1; j < 20; j++)
                                            {
                                                bool bOK = true;
                                                foreach (DataColumn dc in dtImportFileTable.Columns)
                                                {
                                                    if (dc.ColumnName == dtImportFileTable.Rows[(int)theImportTemplate.ImportColumnHeaderRow - 1][i].ToString() + j.ToString())
                                                    {
                                                        bOK = false;
                                                    }
                                                }

                                                if (bOK)
                                                {
                                                    dtImportFileTable.Columns[i].ColumnName = dtImportFileTable.Rows[(int)theImportTemplate.ImportColumnHeaderRow - 1][i].ToString() + j.ToString();
                                                    dtImportFileTable.AcceptChanges();
                                                    break;
                                                }

                                            }
                                        }
                                    }
                                }

                            }
                            dtImportFileTable.AcceptChanges();
                        }
                        //}

                    }



                    if (theImportTemplate.ImportDataStartRow != null)
                    {
                        for (int i = 1; i <= (int)theImportTemplate.ImportDataStartRow - 1; i++)
                        {
                            dtImportFileTable.Rows.RemoveAt(0);

                        }
                        dtImportFileTable.AcceptChanges();
                    }

                    int xy = 0;
                    foreach (DataColumn dc in dtImportFileTable.Columns)
                    {
                        dtImportFileTable.Columns[xy].ColumnName = Common.RemoveSpecialCharacters(dc.ColumnName);
                        xy = xy + 1;
                    }
                    dtImportFileTable.AcceptChanges();
                }
            }





            //if(strFileExtension.ToLower()=="virtual" && theImportTemplate!=null)
            //{
            //    DataTable dtIT_Columns = RecordManager.ets_Table_Columns_Import((int)theTable.TableID, iImportTemplateID);
            //    //DataTable dtNoI_Columns = RecordManager.ets_Table_Columns_Import((int)theTable.TableID, null);

            //    //need Alex
            //    for (int i = 0; i <= dtImportFileTable.Columns.Count - 1; i++)
            //    {
            //        //foreach (DataRow drNoI in dtNoI_Columns.Rows)
            //        //{

            //            //if (dtImportFileTable.Columns[i].ColumnName.ToLower() ==
            //            //   drNoI["Name_OnImport"].ToString().ToLower())
            //            //{
            //                //foreach (DataRow drIT in dtIT_Columns.Rows)
            //                //{
            //                //    if (int.Parse(drNoI["ColumnID"].ToString()) == int.Parse(drIT["ColumnID"].ToString()))
            //                //    {
            //                //        dtImportFileTable.Columns[i].ColumnName = drIT["Name_OnImport"].ToString();
            //                //        dtImportFileTable.AcceptChanges();
            //                //        break;
            //                //    }
            //                //}
            //                //break;
            //            //}
            //        //}
            //    }

            //}




            DataTable dtRecordTypleColumns;
            DataTable dtColumnsAll = RecordManager.ets_Table_Columns_All((int)theTable.TableID);



            //string strListOfNoNeedColumns = "";
            bool bHasValidationOnEntry = false;
            bool bHasValidationOnWarning = false;
            bool bHasCheckUnlikelyValue = false;
            bool bHasRecordIDUploadedColumn = false;

            bool bNeedAcceptChange = false;
            foreach (DataRow drC in dtColumnsAll.Rows)
            {
                if (drC["TextType"] != DBNull.Value &&
                    (drC["TextType"].ToString() == "d" || drC["TextType"].ToString() == "t"))
                {

                }
                else
                {
                    bNeedAcceptChange = true;
                    drC["Calculation"] = Common.GetCalculationSystemNameOnly(drC["Calculation"].ToString(), (int)theTable.TableID);
                }

                if ((drC["ValidationOnEntry"] != DBNull.Value && drC["ValidationOnEntry"].ToString().Length > 0)
                    || drC["ConV"] != DBNull.Value)
                {
                    bHasValidationOnEntry = true;
                }
                if ((drC["ValidationOnWarning"] != DBNull.Value && drC["ValidationOnWarning"].ToString().Length > 0)
                    || drC["ConW"] != DBNull.Value)
                {
                    bHasValidationOnWarning = true;
                }
                if (drC["CheckUnlikelyValue"] != DBNull.Value && bool.Parse(drC["CheckUnlikelyValue"].ToString()))
                {
                    bHasCheckUnlikelyValue = true;
                }
            }

            int iTN = 0;
            DataTable dtIti = ImportManager.dbg_ImportTemplateItem_Select(theImportTemplate.ImportTemplateID, null,
                "", "", "", null, null, ref iTN);
            foreach (DataRow iti in dtIti.Rows)
            {
                if (!iti.IsNull("ColumnID"))
                {
                    int templateColumnId = (int)iti["ColumnID"];
                    Column templateColumn = RecordManager.ets_Column_Details(templateColumnId);
                    if (templateColumn != null)
                    {
                        if (templateColumn.SystemName == "RecordID")
                        {
                            if (theTable.UniqueColumnID == templateColumnId ||
                                theTable.UniqueColumnID2 == templateColumnId)
                            {
                                bHasRecordIDUploadedColumn = true;
                                break;
                            }
                        }
                    }
                }
            }

            if (bNeedAcceptChange)
                dtColumnsAll.AcceptChanges();



            //if (iImportTemplateID != null)
            //{
            //strImportHedaderName = "ImportHeaderName";
            dtRecordTypleColumns = RecordManager.ets_Table_Columns_Import((int)theTable.TableID, iImportTemplateID);
            //}
            //else if (strImpdortHeaderName == "DisplayName")
            //{
            //    dtRecordTypleColumns = RecordManager.ets_Table_Columns_DisplayName((int)theTable.TableID);
            //}
            //else
            //{
            //    dtRecordTypleColumns = RecordManager.ets_Table_Columns_Import((int)theTable.TableID, null);
            //}
            dtRecordTypleColumns.Columns.Add("FileColumnName_Import");
            dtRecordTypleColumns.Columns.Add("SystemName_2nd");
            dtRecordTypleColumns.AcceptChanges();

            if (iSourceBatchID == null)
            {
                if (dtImportFileTable.Rows.Count > 0)
                {
                    try
                    {
                        dtImportFileTable = dtImportFileTable.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare(field.ToString().Trim(), string.Empty) == 0)).CopyToDataTable();
                    }
                    catch
                    {
                        //
                    }

                }



                //for (int r = 0; r < dtImportFileTable.Columns.Count; r++)
                //{
                //    bool bIsFound = false;
                //    for (int i = 0; i < dtRecordTypleColumns.Rows.Count; i++)
                //    {
                //        if (Common.RemoveSpecialCharacters(dtImportFileTable.Columns[r].ColumnName.Trim().ToLower()) ==
                //            Common.RemoveSpecialCharacters(dtRecordTypleColumns.Rows[i][strImportHedaderName].ToString().Trim().ToLower()))
                //        {
                //            bIsFound = true;
                //            break;
                //        }
                //    }
                //    if (bIsFound == false)
                //    {
                //        if (dtImportFileTable.Columns[r].ColumnName.ToLower() != strTimeSamledColumnName.ToLower() && dtImportFileTable.Columns[r].ColumnName.ToLower() != strDateRecordedColumnName.ToLower())
                //        {
                //            strListOfNoNeedColumns += dtImportFileTable.Columns[r].ColumnName + ",";
                //        }
                //    }
                //}

                //if (strFileExtension == ".txt")
                //{
                //    strListOfNoNeedColumns = "";
                //}

                //List<string> strRemoveIndexes = strListOfNoNeedColumns.Split(',').Where(s => (!String.IsNullOrEmpty(s))).ToList();


                //foreach (string item in strRemoveIndexes)
                //{
                //    try
                //    {
                //        dtImportFileTable.Columns.Remove(item);
                //    }
                //    catch
                //    {
                //        //
                //    }
                //}


                //string strListOfMissingColumns = "";
                //for (int i = 0; i < dtRecordTypleColumns.Rows.Count; i++)
                //{


                //    bool bMissingColumnFound = false;

                //    for (int ic = 0; ic < dtImportFileTable.Columns.Count; ic++)
                //    {
                //        if (Common.RemoveSpecialCharacters(dtImportFileTable.Columns[ic].ColumnName.Trim().ToLower()) ==
                //    Common.RemoveSpecialCharacters(dtRecordTypleColumns.Rows[i][strImportHdeaderName].ToString().Trim().ToLower()))
                //        {
                //            bMissingColumnFound = true;
                //            break;
                //        }
                //    }

                //    if (bMissingColumnFound == false)
                //    {
                //        strListOfMissingColumns += dtRecordTypleColumns.Rows[i][strImportHeadderName].ToString() + ",";
                //    }
                //}

                //if (strFileExtension == ".txt")
                //{
                //    strListOfMissingColumns = "";
                //}
                //if (strListOfMissingColumns.Length > 0)
                //{
                //    List<string> strMissingColumns = strListOfMissingColumns.Split(',').Where(s => (!String.IsNullOrEmpty(s))).ToList();
                //    foreach (string item in strMissingColumns)
                //    {
                //        try
                //        {
                //            dtImportFileTable.Columns.Add(item);
                //        }
                //        catch
                //        {
                //            //
                //        }
                //    }

                //}

                dtImportFileTable.AcceptChanges();

                //IDnText aIDnText

                List<IDnText> lstFileColumn_Sys = new List<IDnText>();

                for (int r = 0; r < dtImportFileTable.Columns.Count; r++)
                {
                    if (theImportTemplate != null && theImportTemplate.IsImportPositional != null && (bool)theImportTemplate.IsImportPositional)
                    {
                        bool isImportFileColumnFound = false;
                        for (int i = 0; i < dtRecordTypleColumns.Rows.Count; i++)
                        {
                            int iPositon = 0;
                            if (dtRecordTypleColumns.Rows[i]["PositionOnImport"] != DBNull.Value && dtRecordTypleColumns.Rows[i]["PositionOnImport"].ToString().IndexOf(",") == -1)
                            {
                                if (int.TryParse(dtRecordTypleColumns.Rows[i]["PositionOnImport"].ToString(), out iPositon))
                                {
                                    if (r == (iPositon - 1))
                                    {
                                        //dtImportFileTable.Columns[r].ColumnName = dtRecordTypleColumns.Rows[i]["SystemName"].ToString();
                                        dtRecordTypleColumns.Rows[i]["FileColumnName_Import"] = dtImportFileTable.Columns[r].ColumnName;
                                        //lstFileColumn_Sys.Add(new IDnText(dtRecordTypleColumns.Rows[i]["SystemName"].ToString(), dtImportFileTable.Columns[r].ColumnName));
                                        isImportFileColumnFound = true;
                                        //break;
                                    }
                                }
                            }

                            if (dtRecordTypleColumns.Rows[i]["PositionOnImport"] != DBNull.Value && dtRecordTypleColumns.Rows[i]["PositionOnImport"].ToString().IndexOf(",") > -1)
                            {
                                string strBeforeComma = Common.BeforeComma(dtRecordTypleColumns.Rows[i]["PositionOnImport"].ToString());
                                int iBeforeCommaPosition = 0;
                                if (int.TryParse(strBeforeComma, out iBeforeCommaPosition))
                                {
                                    if (r == (iBeforeCommaPosition - 1))
                                    {
                                        //dtImportFileTable.Columns[r].ColumnName = dtRecordTypleColumns.Rows[i]["SystemName"].ToString();
                                        dtRecordTypleColumns.Rows[i]["FileColumnName_Import"] = dtImportFileTable.Columns[r].ColumnName;
                                        //lstFileColumn_Sys.Add(new IDnText(dtRecordTypleColumns.Rows[i]["SystemName"].ToString(), dtImportFileTable.Columns[r].ColumnName));

                                        string strAfterComma = Common.AferComma(dtRecordTypleColumns.Rows[i]["PositionOnImport"].ToString());
                                        int iAfterCommaPosition = 0;
                                        if (int.TryParse(strAfterComma, out iAfterCommaPosition))
                                        {

                                            if (dtImportFileTable.Columns.Count >= iAfterCommaPosition)
                                            {
                                                dtRecordTypleColumns.Rows[i]["SystemName_2nd"] = dtImportFileTable.Columns[iAfterCommaPosition - 1].ColumnName;
                                                //bHas_SystemName_2nd = true;
                                            }

                                            //time column
                                        }
                                        isImportFileColumnFound = true;
                                        //break;
                                    }
                                }
                            }
                        }
                        if (!isImportFileColumnFound)
                        {
                            listIgnoredColumns.Add(String.Format("#{0} ({1})", r + 1, dtImportFileTable.Columns[r].ColumnName));
                        }

                    }
                    else
                    {
                        bool isImportFileColumnFound = false;
                        for (int i = 0; i < dtRecordTypleColumns.Rows.Count; i++)
                        {

                            if (dtRecordTypleColumns.Rows[i]["ImportHeaderName"].ToString().Trim().IndexOf(",") == -1)
                            {

                                if (Common.RemoveSpecialCharacters(dtImportFileTable.Columns[r].ColumnName.Trim().ToLower()) ==
                                                                                    Common.RemoveSpecialCharacters(dtRecordTypleColumns.Rows[i]["ImportHeaderName"].ToString().Trim().ToLower()))
                                {
                                    try
                                    {
                                        //over write if this is used by any other 2nd column, need to find and change those SystemName_2nd
                                        //dtImportFileTable.Columns[r].ColumnName = dtRecordTypleColumns.Rows[i]["SystemName"].ToString();
                                        dtRecordTypleColumns.Rows[i]["FileColumnName_Import"] = dtImportFileTable.Columns[r].ColumnName;
                                        isImportFileColumnFound = true;
                                        //lstFileColumn_Sys.Add(new IDnText(dtRecordTypleColumns.Rows[i]["SystemName"].ToString(), dtImportFileTable.Columns[r].ColumnName));

                                        //break;
                                    }
                                    catch
                                    {
                                        //
                                    }
                                }

                            }
                            else
                            {
                                //2 columns
                                string strBeforeComma = Common.BeforeComma(dtRecordTypleColumns.Rows[i]["ImportHeaderName"].ToString());
                                if (Common.RemoveSpecialCharacters(dtImportFileTable.Columns[r].ColumnName.Trim().ToLower()) ==
                                                                                    Common.RemoveSpecialCharacters(strBeforeComma.Trim().ToLower()))
                                {
                                    try
                                    {
                                        //over write if this is used by any other 2nd column, need to find and change those SystemName_2nd
                                        //dtImportFileTable.Columns[r].ColumnName = dtRecordTypleColumns.Rows[i]["SystemName"].ToString();
                                        dtRecordTypleColumns.Rows[i]["FileColumnName_Import"] = dtImportFileTable.Columns[r].ColumnName;

                                        //lstFileColumn_Sys.Add(new IDnText(dtRecordTypleColumns.Rows[i]["SystemName"].ToString(), dtImportFileTable.Columns[r].ColumnName));

                                        string strAfterComma = Common.AferComma(dtRecordTypleColumns.Rows[i]["ImportHeaderName"].ToString());
                                        dtRecordTypleColumns.Rows[i]["ImportHeaderName"] = dtImportFileTable.Columns[r].ColumnName;
                                        if (strAfterComma != "")
                                        {
                                            dtRecordTypleColumns.Rows[i]["SystemName_2nd"] = strAfterComma;
                                            //bHas_SystemName_2nd = true;
                                        }

                                        isImportFileColumnFound = true;
                                        //break;
                                    }
                                    catch
                                    {
                                        //
                                    }
                                }
                            }
                        }
                        if (!isImportFileColumnFound)
                        {
                            listIgnoredColumns.Add(dtImportFileTable.Columns[r].ColumnName);
                        }
                    }
                }

                //if(lstFileColumn_Sys.Count>0)
                //{
                //    for (int r = 0; r < dtImportFileTable.Columns.Count; r++)
                //    {
                //        foreach(IDnText aFileColumn in lstFileColumn_Sys)
                //        {
                //            if(dtImportFileTable.Columns[r].ColumnName==aFileColumn.Text)
                //            {
                //                dtImportFileTable.Columns[r].ColumnName = aFileColumn.ID;
                //            }
                //        }
                //    }
                //}
                //if(bHas_SystemName_2nd)
                //{
                //    for (int i = 0; i < dtRecordTypleColumns.Rows.Count; i++)
                //    {
                //        if( dtRecordTypleColumns.Rows[i]["SystemName_2nd"].ToString()!="")
                //        {
                //            foreach(IDnText aFileColumn in lstFileColumn_Sys)
                //            {
                //                if (dtRecordTypleColumns.Columns[i].ColumnName == aFileColumn.Text)
                //                {
                //                    dtRecordTypleColumns.Columns[i].ColumnName = aFileColumn.ID;
                //                }
                //            }
                //        } 
                //    }
                //}
                //dtImportFileTable.AcceptChanges();
                dtRecordTypleColumns.AcceptChanges();
            }//SourceBatcID NULL


            if (theSourceBatch != null)
            {

                int iTN_Temp = 0;

                dtImportFileTable = ets_TempRecord_List((int)theSourceBatch.TableID, (int)theSourceBatch.BatchID, null, null, null, null, null, "", "", null, null,
                    ref iTN_Temp, ref iTN_Temp, "", null, "system");


                //dtRecordTypleColumns.ToString();

                if (dtImportFileTable == null)
                    return;

                if (dtImportFileTable.Rows.Count == 0)
                    return;

                strBatchDescription = theSourceBatch.BatchDescription;
                strOriginalFileName = "NA";
                if (guidNew == null)
                    guidNew = Guid.NewGuid(); //may be we can remove this
            }

            for (int r = 0; r < dtImportFileTable.Columns.Count; r++)
            {
                for (int i = 0; i < dtRecordTypleColumns.Rows.Count; i++)
                {
                    if (Common.RemoveSpecialCharacters(dtImportFileTable.Columns[r].ColumnName.Trim().ToLower()) ==
                        Common.RemoveSpecialCharacters(dtRecordTypleColumns.Rows[i]["SystemName"].ToString().Trim().ToLower()))
                    {
                        if (dtRecordTypleColumns.Rows[i]["FileColumnName_Import"].ToString() == "")
                            dtRecordTypleColumns.Rows[i]["FileColumnName_Import"] = dtRecordTypleColumns.Rows[i]["SystemName"].ToString();

                    }
                }
            }
            dtRecordTypleColumns.AcceptChanges();


            //now dtCSV is ready to be imported into Batch & TempRecord

            //JA 20 JAN 2017  int.Parse(HttpContext.Current.Session["AccountID"].ToString())
            //Batch newBatch = new Batch(null, (int)theTable.TableID,
            //    strBatchDescription.Trim() == "" ? strOriginalFileName : strBatchDescription.Trim(),
            //    strOriginalFileName, null, guidNew, iUserID, theTable.AccountID);
            Batch newBatch = new Batch(null, (int)theTable.TableID,
                strBatchDescription.Trim() == "" ? strOriginalFileName : strBatchDescription.Trim(),
                strOriginalFileName, null, guidNew, iUserID, iAccountID);


            //if (bAllowDataUpload.HasValue)
            //{
            //    newBatch.AllowDataUpdate = bAllowDataUpload;
            //}
            //else
            //{
            //    if (Common.GetDatabaseName() == "thedatabase_emd")
            //    {
            //        newBatch.AllowDataUpdate = theTable.IsDataUpdateAllowed;
            //    }

            //}


            newBatch.ImportTemplateID = iImportTemplateID;
            if (newBatch.SourceBatchID == null)
                newBatch.SourceBatchID = iSourceBatchID;

            if (listIgnoredColumns.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (string s in listIgnoredColumns)
                {
                    sb.AppendLine(s.Replace("\r\n", " ").Replace("\n\r", " ").Replace("\r", " ").Replace("\n", " "));
                }
                newBatch.IgnoredColumns = sb.ToString();
            }
            iBatchID = UploadManager.ets_Batch_Insert(newBatch);

            //bool bHasParentMatching = false;
            bool bHasParent = false;
            string strHasParent = Common.GetValueFromSQL("SELECT ColumnID FROM [Column] WHERE TableID=" + theTable.TableID.ToString() + " AND TableTableID  is not NULL AND DisplayColumn is not NULL AND ColumnType='dropdown' ");

            bNeedAcceptChange = false;
            if (strHasParent != "")
            {
                bHasParent = true;
                dtRecordTypleColumns.Columns.Add("ParentColumnSystemName", typeof(String));
                dtRecordTypleColumns.AcceptChanges();
                foreach (DataRow drC in dtRecordTypleColumns.Rows)
                {
                    if ((drC["ColumnType"].ToString() == "dropdown") &&
                            (drC["DropDownType"].ToString() == "tabledd" || drC["DropDownType"].ToString() == "table") &&
                            (drC["TableTableID"] != DBNull.Value) && (drC["DisplayColumn"] != DBNull.Value))
                    {
                        bool bParentImportColumnID = false;
                        if (iImportTemplateID != null)
                        {
                            string strParentImportColumnID = Common.GetValueFromSQL("SELECT ParentImportColumnID FROM ImportTemplateItem WHERE ImportTemplateID="
                                + iImportTemplateID.ToString() + " AND ParentImportColumnID IS NOT NULL AND ColumnID=" + drC["ColumnID"].ToString());
                            if (strParentImportColumnID != "" && strParentImportColumnID.Length > 0)
                            {
                                bParentImportColumnID = true;
                            }
                        }

                        // || theTable.TableName.IndexOf("EMD_Test") > -1
                        if (bParentImportColumnID == false)// when bParentImportColumnID==true we use  [dbo].[spAdjustTempRecordLinkedValueOnImport]
                        {
                            string lookupColumnName = drC["DisplayColumn"].ToString();

                            string lookupSystemName = "";
                            if (strFileExtension.ToLower() == "virtual" && strImportFolder != "")
                            {
                                if (lookupColumnName == "[Site Name]")
                                {
                                    lookupSystemName = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + drC["TableTableID"].ToString()
                                        + " AND (DisplayName='Site Name On Import File' OR DisplayTextSummary='Site Name On Import File')");
                                }
                            }

                            if (lookupSystemName.Trim() == "")
                            {
                                lookupSystemName = Common.GetValueFromSQL("SELECT dbo.fnReplaceDisplayColumns_NoAlias(" + drC["ColumnID"].ToString() + ")");
                            }

                            if (lookupSystemName != "")
                            {
                                drC["ParentColumnSystemName"] = lookupSystemName;
                                bNeedAcceptChange = true;
                            }
                        }

                    }
                }

            }







            if (bNeedAcceptChange)
                dtRecordTypleColumns.AcceptChanges();

            HashSet<int> columnsForValidityCheck = null;
            HashSet<int> columnsForWarningCheck = null;
            HashSet<int> columnsForExceedanceCheck = null;

            DataColumnCollection dccIFT = dtImportFileTable.Columns;
            for (int r = z; r < dtImportFileTable.Rows.Count; r++)
            {
                TempRecord newTempRecord = new TempRecord();
                //JA 20 JAN 2017 int.Parse(HttpContext.Current.Session["AccountID"].ToString())
                //newTempRecord.AccountID = theTable.AccountID;

                newTempRecord.AccountID = iAccountID;// int.Parse(HttpContext.Current.Session["AccountID"].ToString());

                newTempRecord.BatchID = iBatchID;
                newTempRecord.TableID = (int)theTable.TableID;
                newTempRecord.DateFormat = theTable.DateFormat;
                //bool bIsBlank = false;
                string strRejectReason = "";
                string strWarningReason = "";
                string strExceedanceReason = "";

                foreach (DataColumn dc in dtImportFileTable.Columns)
                {
                    string strColumnName = "";
                    strColumnName = dc.ColumnName;
                    string strFileCellToColumnRow = "";
                    //if (strColumnName.ToLower() != strTimeSamledColumnName.ToLower())
                    //{


                    //if (dc.ColumnName.ToUpper() == "DATETIMERECORDED")
                    //{
                    //    //newTempRecord.DateFormat = theTable.DateFormat;
                    //    try
                    //    {
                    //        strFileCellToColumnRow = dtImportFileTable.Rows[r][dc.ColumnName].ToString();


                    //        //if (strFileExtension == ".csv")
                    //        //{
                    //        //    if (strTimeSamledColumnName == "")
                    //        //    {
                    //        //        UploadManager.MakeTheTempRecord(ref newTempRecord, strColumnName, dtImportFileTable.Rows[r][dc.ColumnName].ToString());
                    //        //    }
                    //        //    else
                    //        //    {

                    //        //        UploadManager.MakeTheTempRecord(ref newTempRecord, strColumnName, dtImportFileTable.Rows[r][dc.ColumnName].ToString() + " " + dtImportFileTable.Rows[r][strTimeSamledColumnName].ToString());
                    //        //    }

                    //        //}
                    //        //else if (strFileExtension == ".xml" || iSourceBatchID != null)
                    //        //{
                    //        //    UploadManager.MakeTheTempRecord(ref newTempRecord, strColumnName, dtImportFileTable.Rows[r][dc.ColumnName].ToString());
                    //        //}
                    //        //else
                    //        //{
                    //        //    string strDateTimeTemp = "";
                    //        //    if (strTimeSamledColumnName == "")
                    //        //    {
                    //        //        UploadManager.MakeTheTempRecord(ref newTempRecord, strColumnName, dtImportFileTable.Rows[r][dc.ColumnName].ToString().Substring(0));
                    //        //    }
                    //        //    else
                    //        //    {
                    //        //        strDateTimeTemp = dtImportFileTable.Rows[r][strTimeSamledColumnName].ToString();

                    //        //        if (dtImportFileTable.Rows[r][strTimeSamledColumnName].ToString().Length > 10)
                    //        //        {
                    //        //            strDateTimeTemp = strDateTimeTemp.Substring(11);
                    //        //        }
                    //        //        if (dtImportFileTable.Rows[r][dc.ColumnName].ToString().Trim() != "")
                    //        //        {
                    //        //            if (dtImportFileTable.Rows[r][dc.ColumnName].ToString().IndexOf(" ") != -1)
                    //        //            {
                    //        //                UploadManager.MakeTheTempRecord(ref newTempRecord, strColumnName, dtImportFileTable.Rows[r][dc.ColumnName].ToString().Substring(0, dtImportFileTable.Rows[r][dc.ColumnName].ToString().IndexOf(" ")) + " " + strDateTimeTemp);
                    //        //            }
                    //        //            else
                    //        //            {
                    //        //                UploadManager.MakeTheTempRecord(ref newTempRecord, strColumnName, dtImportFileTable.Rows[r][dc.ColumnName].ToString().Substring(0) + " " + strDateTimeTemp);
                    //        //            }
                    //        //        }
                    //        //    }
                    //        //}
                    //    }
                    //    catch
                    //    {
                    //        newTempRecord.DateTimeRecorded = DateTime.Now;
                    //    }

                    //}
                    //else
                    //{
                    //    UploadManager.MakeTheTempRecord(ref newTempRecord, strColumnName, dtImportFileTable.Rows[r][dc.ColumnName].ToString());
                    //}


                    for (int i = 0; i < dtRecordTypleColumns.Rows.Count; i++)
                    {
                        if (dc.ColumnName.Trim().ToLower() ==
                           dtRecordTypleColumns.Rows[i]["FileColumnName_Import"].ToString().Trim().ToLower())
                        {
                            strFileCellToColumnRow = dtImportFileTable.Rows[r][dc.ColumnName].ToString();
                            if (dtRecordTypleColumns.Rows[i]["SystemName_2nd"].ToString() != "")
                            {
                                string strConcatenate = "";
                                if (dccIFT.Contains(dtRecordTypleColumns.Rows[i]["SystemName_2nd"].ToString()))
                                {
                                    if (strFileCellToColumnRow.IndexOf(" ") > 0)
                                    {
                                        strFileCellToColumnRow = strFileCellToColumnRow.Substring(0, strFileCellToColumnRow.IndexOf(" ") + 1);
                                    }

                                    string str2ndValue = dtImportFileTable.Rows[r][dtRecordTypleColumns.Rows[i]["SystemName_2nd"].ToString()].ToString();
                                    if (dtRecordTypleColumns.Rows[i]["ColumnType"].ToString() == "datetime")
                                    {
                                        strConcatenate = " ";
                                        DateTime? dtt = Common.GetDateTimeFromString(str2ndValue, newTempRecord.DateFormat);// Convert.ToDateTime(dtImportFileTable.Rows[r][dc.ColumnName].ToString());
                                        if (dtt != null)
                                        {

                                            str2ndValue = ((DateTime)dtt).ToString("HH:mm:ss");
                                        }

                                    }
                                    strFileCellToColumnRow = strFileCellToColumnRow + strConcatenate + str2ndValue;
                                }
                            }
                            if (strFileCellToColumnRow != "")
                            {
                                try
                                {
                                    if (dtRecordTypleColumns.Rows[i]["SystemName"].ToString() == "RecordID")
                                    {
                                        newTempRecord.TempRecordIDUploaded = strFileCellToColumnRow;
                                    }
                                    else
                                    {
                                        if (dtRecordTypleColumns.Rows[i]["ColumnType"].ToString() == "datetime")
                                        {
                                            DateTime? dtt = Common.GetDateTimeFromString(strFileCellToColumnRow,
                                                newTempRecord
                                                    .DateFormat); // Convert.ToDateTime(dtImportFileTable.Rows[r][dc.ColumnName].ToString());
                                            if (dtt != null)
                                            {
                                                if (dtRecordTypleColumns.Rows[i]["ColumnType"].ToString().ToUpper() ==
                                                    "DATETIMERECORDED")
                                                {
                                                    newTempRecord.DateTimeRecorded = dtt;
                                                }
                                                else
                                                {
                                                    UploadManager.MakeTheTempRecord(ref newTempRecord,
                                                        dtRecordTypleColumns.Rows[i]["SystemName"].ToString(),
                                                        ((DateTime) dtt).ToString("dd/MM/yyyy HH:mm:ss"));
                                                }
                                            }
                                            else
                                            {
                                                strRejectReason =
                                                    strRejectReason +
                                                    TheDatabase.GetInvalid_msg(
                                                        dtRecordTypleColumns.Rows[i]["DisplayName"].ToString());
                                            }

                                        }
                                        else if (dtRecordTypleColumns.Rows[i]["ColumnType"].ToString() == "date"
                                                 || dtRecordTypleColumns.Rows[i]["ColumnType"].ToString() == "time"
                                        )
                                        {
                                            //
                                        }
                                        else
                                        {
                                            UploadManager.MakeTheTempRecord(ref newTempRecord,
                                                dtRecordTypleColumns.Rows[i]["SystemName"].ToString(),
                                                strFileCellToColumnRow);
                                        }
                                    }
                                }
                                catch
                                {
                                    strRejectReason = strRejectReason + TheDatabase.GetInvalid_msg(dtRecordTypleColumns.Rows[i]["DisplayName"].ToString());
                                }
                            }



                            if (dtImportFileTable.Rows[r][dc.ColumnName].ToString().Length == 0)
                            {
                                if (dtRecordTypleColumns.Rows[i]["DefaultValue"].ToString() != "")
                                {
                                    if (dtRecordTypleColumns.Rows[i]["ColumnType"].ToString().Trim().ToLower() == "datetime"
                                        || dtRecordTypleColumns.Rows[i]["ColumnType"].ToString().Trim().ToLower() == "date"
                                        || dtRecordTypleColumns.Rows[i]["ColumnType"].ToString().Trim().ToLower() == "time")
                                    {
                                        //dtImportFileTable.Rows[r][dc.ColumnName] = DateTime.Now;
                                        UploadManager.MakeTheTempRecord(ref newTempRecord, dtRecordTypleColumns.Rows[i]["SystemName"].ToString(), DateTime.Now);
                                    }
                                    else
                                    {
                                        //dtImportFileTable.Rows[r][dc.ColumnName] = dtRecordTypleColumns.Rows[i]["DefaultValue"].ToString();
                                        UploadManager.MakeTheTempRecord(ref newTempRecord, dtRecordTypleColumns.Rows[i]["SystemName"].ToString(), dtRecordTypleColumns.Rows[i]["DefaultValue"].ToString());
                                    }
                                }
                            }
                        }
                    }


                    //dtImportFileTable.AcceptChanges();
                    for (int i = 0; i < dtRecordTypleColumns.Rows.Count; i++)
                    {
                        if (dc.ColumnName.Trim().ToLower() ==
                            dtRecordTypleColumns.Rows[i]["FileColumnName_Import"].ToString().Trim().ToLower())
                        {

                            //if (dtImportFileTable.Rows[r][dc.ColumnName].ToString().Length == 0)
                            //{
                            //    if (dtRecordTypleColumns.Rows[i]["DefaultValue"].ToString() != "")
                            //    {
                            //        if (dtRecordTypleColumns.Rows[i]["ColumnType"].ToString().Trim().ToLower() == "datetime"
                            //            || dtRecordTypleColumns.Rows[i]["ColumnType"].ToString().Trim().ToLower() == "date"
                            //            || dtRecordTypleColumns.Rows[i]["ColumnType"].ToString().Trim().ToLower() == "time")
                            //        {
                            //            dtImportFileTable.Rows[r][dc.ColumnName] = DateTime.Now;
                            //            UploadManager.MakeTheTempRecord(ref newTempRecord, strColumnName, DateTime.Now);
                            //        }
                            //        else
                            //        {

                            //            dtImportFileTable.Rows[r][dc.ColumnName] = dtRecordTypleColumns.Rows[i]["DefaultValue"].ToString();
                            //            UploadManager.MakeTheTempRecord(ref newTempRecord, strColumnName, dtRecordTypleColumns.Rows[i]["DefaultValue"].ToString());
                            //        }
                            //        dtImportFileTable.AcceptChanges();
                            //    }
                            //}


                            if (dtImportFileTable.Rows[r][dc.ColumnName].ToString().Length > 0)
                            {
                                //Validation for ticket 2423. By JV
                                if (dtRecordTypleColumns.Rows[i]["ColumnType"].ToString() == "image")
                                {
                                    strRejectReason = strRejectReason + TheDatabase.GetInvalid_msg(dtRecordTypleColumns.Rows[i]["DisplayName"].ToString());
                                }
                                //=================================

                                if (dtRecordTypleColumns.Rows[i]["ColumnType"].ToString() == "date")
                                {
                                    if (dtImportFileTable.Rows[r][dc.ColumnName].ToString() != "")
                                    {
                                        try
                                        {
                                            DateTime? dtt = Common.GetDateTimeFromString(dtImportFileTable.Rows[r][dc.ColumnName].ToString(), newTempRecord.DateFormat);// Convert.ToDateTime(dtImportFileTable.Rows[r][dc.ColumnName].ToString());

                                            if (dtt != null)
                                            {
                                                UploadManager.MakeTheTempRecord(ref newTempRecord, dtRecordTypleColumns.Rows[i]["SystemName"].ToString(), ((DateTime)dtt).ToString("dd/MM/yyyy"));
                                            }
                                            else
                                            {
                                                strRejectReason = strRejectReason + TheDatabase.GetInvalid_msg(dtRecordTypleColumns.Rows[i]["DisplayName"].ToString());
                                            }

                                        }
                                        catch
                                        {

                                        }
                                    }
                                }

                                if (dtRecordTypleColumns.Rows[i]["ColumnType"].ToString() == "time")
                                {
                                    if (dtImportFileTable.Rows[r][dc.ColumnName].ToString() != "")
                                    {
                                        try
                                        {
                                            //Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + dtImportFileTable.Rows[r][dc.ColumnName].ToString());
                                            DateTime? dtt = Common.GetDateTimeFromString(DateTime.Now.ToShortDateString() + " " + dtImportFileTable.Rows[r][dc.ColumnName].ToString(), newTempRecord.DateFormat);// Convert.ToDateTime(dtImportFileTable.Rows[r][dc.ColumnName].ToString());

                                            if (dtt != null)
                                            {
                                                UploadManager.MakeTheTempRecord(ref newTempRecord, dtRecordTypleColumns.Rows[i]["SystemName"].ToString(), ((DateTime)dtt).ToString("HH:mm:ss"));
                                            }
                                            else
                                            {
                                                strRejectReason = strRejectReason + TheDatabase.GetInvalid_msg(dtRecordTypleColumns.Rows[i]["DisplayName"].ToString());
                                            }

                                        }
                                        catch
                                        {

                                        }
                                    }
                                }

                                if (bHasParent == true && (dtRecordTypleColumns.Rows[i]["ColumnType"].ToString() == "dropdown") &&
                                    (dtRecordTypleColumns.Rows[i]["DropDownType"].ToString() == "tabledd"
                                    || dtRecordTypleColumns.Rows[i]["DropDownType"].ToString() == "table") &&
                                    (dtRecordTypleColumns.Rows[i]["TableTableID"] != DBNull.Value) &&
                                    (dtRecordTypleColumns.Rows[i]["DisplayColumn"] != DBNull.Value) && (dtRecordTypleColumns.Columns.Contains("ParentColumnSystemName")
                                    && dtRecordTypleColumns.Rows[i]["ParentColumnSystemName"] != DBNull.Value && dtRecordTypleColumns.Rows[i]["ParentColumnSystemName"].ToString().Length > 0))
                                {
                                    if (dtImportFileTable.Rows[r][dc.ColumnName].ToString() != "")
                                    {

                                        string strRecordIDSQL = "SELECT RecordID FROM [Record] WHERE IsActive=1 AND TableID=" + dtRecordTypleColumns.Rows[i]["TableTableID"]
                                             + " AND CHARINDEX (';' +'" + dtImportFileTable.Rows[r][dc.ColumnName].ToString().Replace("'", "''").Trim()
                                                                 + "'+ ';',';' + "
                                                                 + dtRecordTypleColumns.Rows[i]["ParentColumnSystemName"] + " + ';')>0";


                                        //+ " AND " + dtRecordTypleColumns.Rows[i]["ParentColumnSystemName"] + "='" + dtImportFileTable.Rows[r][dc.ColumnName].ToString() + "'";


                                        string strParentRecordID = Common.GetValueFromSQL(strRecordIDSQL);
                                        if (strParentRecordID != "")
                                        {
                                            UploadManager.MakeTheTempRecord(ref newTempRecord, dtRecordTypleColumns.Rows[i]["SystemName"].ToString(), strParentRecordID);
                                        }
                                        else
                                        {
                                            strRejectReason = strRejectReason + dtRecordTypleColumns.Rows[i]["DisplayName"].ToString() + "(" + dtImportFileTable.Rows[r][dc.ColumnName].ToString() + ") not found.";
                                        }
                                    }
                                }
                            }

                            if (dtRecordTypleColumns.Rows[i]["Importance"].ToString().ToLower() == "m")
                            {
                                if (dtImportFileTable.Rows[r][dc.ColumnName].ToString() == "")
                                {
                                    strRejectReason = strRejectReason + "MANDATORY:" + dtRecordTypleColumns.Rows[i]["DisplayName"].ToString() + ".";
                                }
                            }

                            if (bHasRecordIDUploadedColumn && dtRecordTypleColumns.Rows[i]["SystemName"].ToString() == "RecordID")
                            {
                                int recordToUpdateId = 0;
                                if (!int.TryParse(dtImportFileTable.Rows[r][dc.ColumnName].ToString(),
                                    out recordToUpdateId))
                                {
                                    strRejectReason = strRejectReason + "Invalid Record ID";
                                }
                                else
                                {
                                    Record recordToUpdate  = RecordManager.ets_Record_Detail_Full(recordToUpdateId, theTable.TableID, false);
                                    if (recordToUpdate == null)
                                    {
                                        strRejectReason = strRejectReason + "Record ID not found";
                                    }
                                }
                            }

                            //break;
                        }// END of dc.ColumnName.ToLower() ===dtRecordTypleColumns.Rows[i]["SystemName"].ToString().Trim().ToLower()
                    }

                    //}
                }


                //KG 31/10/17 Ticket 3277 check if all mandatory fields exists in the template
                for (int i = 0; i < dtRecordTypleColumns.Rows.Count; i++)
                {
                    bool bCheckIfExisting = false;
                    if (dtRecordTypleColumns.Rows[i]["Importance"].ToString().ToLower() == "m")
                    {
                        foreach (DataColumn dc in dtImportFileTable.Columns)
                        {
                            if (dc.ColumnName.Trim().ToLower() == dtRecordTypleColumns.Rows[i]["FileColumnName_Import"].ToString().Trim().ToLower())
                            {
                                bCheckIfExisting = true;
                                break;
                            }
                        }

                        if (!bCheckIfExisting)
                            strRejectReason = strRejectReason + "MANDATORY:" + dtRecordTypleColumns.Rows[i]["DisplayName"].ToString() + ".";
                    }
                }


                if (newTempRecord.DateTimeRecorded == null)
                {
                    newTempRecord.DateTimeRecorded = DateTime.Now;

                }

                bool bIsAnyCalculationField = false;

                for (int i = 0; i < dtColumnsAll.Rows.Count; i++)
                {

                    if (dtColumnsAll.Rows[i]["ColumnType"].ToString() == "calculation"
                                               && dtColumnsAll.Rows[i]["Calculation"] != DBNull.Value
                                               && dtColumnsAll.Rows[i]["Calculation"].ToString() != "")
                    {
                        bIsAnyCalculationField = true;

                    }

                    if (dtColumnsAll.Rows[i]["ColumnType"].ToString().Trim().ToLower() == "number")
                    {
                        if (dtColumnsAll.Rows[i]["NumberType"] != null &&
                            dtColumnsAll.Rows[i]["NumberType"].ToString() == "8")
                        {
                            string strValue = "1";
                            try
                            {
                                string strMax = "";

                                if (r == z)
                                {
                                    strMax = Common.GetValueFromSQL("SELECT MAX(CONVERT(INT," + dtColumnsAll.Rows[i]["SystemName"].ToString() + ")) FROM Record WHERE IsNumeric(" + dtColumnsAll.Rows[i]["SystemName"].ToString() + ")=1 and TableID=" + theTable.TableID.ToString());
                                }
                                else
                                {
                                    strMax = Common.GetValueFromSQL("SELECT MAX(CONVERT(INT," + dtColumnsAll.Rows[i]["SystemName"].ToString() + ")) FROM TempRecord WHERE IsNumeric(" + dtColumnsAll.Rows[i]["SystemName"].ToString() + ")=1 and  TableID=" + theTable.TableID.ToString());
                                }
                                if (strMax == "")
                                {
                                    strValue = "1";
                                }
                                else
                                {
                                    strValue = (int.Parse(strMax) + 1).ToString();
                                }
                            }
                            catch
                            {
                                strValue = "1";
                            }
                            UploadManager.MakeTheTempRecord(ref newTempRecord, dtColumnsAll.Rows[i]["SystemName"].ToString(), strValue);

                        }
                        //update constant field 
                        if (dtColumnsAll.Rows[i]["Constant"] != DBNull.Value && dtColumnsAll.Rows[i]["Constant"].ToString() != ""
                        && dtColumnsAll.Rows[i]["NumberType"] != null && dtColumnsAll.Rows[i]["NumberType"].ToString() == "2")
                        {

                            UploadManager.MakeTheTempRecord(ref newTempRecord, dtColumnsAll.Rows[i]["SystemName"].ToString(), dtColumnsAll.Rows[i]["Constant"].ToString());

                        }
                    }
                }

                //KG 17/6/2017 Ticket 2752 - this should be done per record before checking validity and exceedance
                if (theImportTemplate != null)
                {
                    DataTable dtMatchingField = Common.DataTableFromText("SELECT ColumnID,ParentImportColumnID FROM ImportTemplateItem WHERE ImportTemplateID=" + theImportTemplate.ImportTemplateID.ToString() + " AND ParentImportColumnID IS NOT NULL ORDER BY ColumnIndex");

                    if (dtMatchingField != null && dtMatchingField.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtMatchingField.Rows)
                        {
                            Column theColumn = RecordManager.ets_Column_Details(int.Parse(dr["ColumnID"].ToString()));
                            if (theColumn != null && theColumn.TableTableID != null)
                            {
                                Column theParentImportColumn = RecordManager.ets_Column_Details(int.Parse(dr["ParentImportColumnID"].ToString()));
                                if (theParentImportColumn != null)
                                {
                                    string strCheckValue = UploadManager.GetTempRecordValue(ref newTempRecord, theColumn.SystemName);

                                    //Red: ditch the single quote
                                    // JB backed this out because it did not work and caused problems importing times into JC
                                    //if (strCheckValue.Contains("'"))
                                    //{
                                    //    strCheckValue = strCheckValue.Replace("'", "''");
                                    //}

                                    string parentID = Common.GetValueFromSQL("SELECT RecordID" +
                                                    " FROM [Record]" +
                                                    " WHERE TableID =" + (int)theColumn.TableTableID +
                                                    " AND IsActive = 1 " +
                                                    " AND " + theParentImportColumn.SystemName + " IS NOT NULL" +
                                                    " AND LEN(" + theParentImportColumn.SystemName + ") > 0" +
                                                    " AND CHARINDEX (';' + RTRIM(LTRIM('" + strCheckValue +
                                                        "')) + ';', ';' + RTRIM(LTRIM(REPLACE(REPLACE(" + theParentImportColumn.SystemName + ", CHAR(10), ''), CHAR(13), ''))) + ';')> 0");


                                    if (parentID == "")
                                    {
                                        //KG Ticket 3560 16/5/2018 - also look at 'Site Name On Import File' field
                                        string lookupSiteSystemName = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theColumn.TableTableID.ToString()
                                                                 + " AND (DisplayName='Site Name On Import File' OR DisplayTextSummary='Site Name On Import File')");

                                        string lookupParentID = Common.GetValueFromSQL("SELECT RecordID" +
                                                                                            " FROM [Record]" +
                                                                                            " WHERE TableID =" + (int)theColumn.TableTableID +
                                                                                            " AND IsActive = 1 " +
                                                                                            " AND " + lookupSiteSystemName + " IS NOT NULL" +
                                                                                            " AND LEN(" + lookupSiteSystemName + ") > 0" +
                                                                                            " AND CHARINDEX (';' + RTRIM(LTRIM('" + strCheckValue +
                                                                                                "')) + ';', ';' + RTRIM(LTRIM(REPLACE(REPLACE(" + lookupSiteSystemName + ", CHAR(10), ''), CHAR(13), ''))) + ';')> 0");
                                        if (lookupParentID == "")
                                            strRejectReason = strRejectReason + theColumn.DisplayName + "(" + strCheckValue + ") not found.";
                                        else
                                            UploadManager.MakeTheTempRecord(ref newTempRecord, theColumn.SystemName, lookupParentID);
                                    }
                                    else
                                        UploadManager.MakeTheTempRecord(ref newTempRecord, theColumn.SystemName, parentID);
                                }
                            }
                        }
                    }
                }

                if (bIsAnyCalculationField)
                {
                    for (int i = 0; i < dtColumnsAll.Rows.Count; i++)
                    {
                        //perform calculation for this column (if found)
                        if (dtColumnsAll.Rows[i]["ColumnType"].ToString() == "calculation"
                                                    && dtColumnsAll.Rows[i]["Calculation"] != DBNull.Value
                                                    && dtColumnsAll.Rows[i]["Calculation"].ToString() != "")
                        {
                            string strValue = "";
                            if (dtColumnsAll.Rows[i]["TextType"] != DBNull.Value
                            && dtColumnsAll.Rows[i]["TextType"].ToString().ToLower() == "d")
                            {
                                //datetime calculation
                                string strCalculation = dtColumnsAll.Rows[i]["Calculation"].ToString();

                                try
                                {
                                    strValue = TheDatabaseS.GetDateCalculationResult(ref dtColumnsAll, strCalculation, null, null, null,
                                   dtColumnsAll.Rows[i]["DateCalculationType"] == DBNull.Value ? "" : dtColumnsAll.Rows[i]["DateCalculationType"].ToString(),
                                   newTempRecord, theTable, bCheckIgnoreMidnight);
                                }
                                catch
                                {
                                    //
                                }
                            }
                            else if (dtColumnsAll.Rows[i]["TextType"] != DBNull.Value
                           && dtColumnsAll.Rows[i]["TextType"].ToString().ToLower() == "t")
                            {
                                try
                                {
                                    string strFormula = Common.GetCalculationSystemNameOnly(dtColumnsAll.Rows[i]["Calculation"].ToString(), (int)theTable.TableID);
                                    //string strFormula = Common.GetCalculationSystemNameOnly(_dtColumnsDetail.Rows[i]["Calculation"].ToString(), (int)_theTable.TableID);

                                    Column theColumn = RecordManager.ets_Column_Details(int.Parse(dtColumnsAll.Rows[i]["ColumnID"].ToString()));
                                    strValue = TheDatabaseS.GetTextCalculationResult(ref dtColumnsAll, strFormula, null, null, null,
                                        newTempRecord, theTable, theColumn);
                                }
                                catch
                                {
                                    //
                                }
                            }
                            else
                            {
                                //number calculation
                                try
                                {
                                    //string strFormula = Common.GetCalculationSystemNameOnly(dtColumnsAll.Rows[i]["Calculation"].ToString(), (int)theTable.TableID);
                                    Column theColumn = RecordManager.ets_Column_Details(int.Parse(dtColumnsAll.Rows[i]["ColumnID"].ToString()));
                                    strValue = TheDatabaseS.GetCalculationResult(ref dtColumnsAll, dtColumnsAll.Rows[i]["Calculation"].ToString(), null, null, null,
                                        newTempRecord, theTable, theColumn);
                                }
                                catch
                                {
                                    //
                                }
                            }

                            if (strValue != "")
                            {
                                UploadManager.MakeTheTempRecord(ref newTempRecord, dtColumnsAll.Rows[i]["SystemName"].ToString(), strValue);
                            }

                        }

                    }

                }


                if (columnsForValidityCheck == null)
                {
                    columnsForValidityCheck = new HashSet<int>();
                    foreach (DataRow col in dtColumnsAll.Rows)
                    {
                        DataTable dt =
                            UploadWorld.ets_AdvancedCondition_Select((int) col["ColumnID"], "notification", "V");
                        if (dt != null && dt.Rows.Count > 0)
                            columnsForValidityCheck.Add((int) col["ColumnID"]);
                    }
                }
                if (columnsForWarningCheck == null)
                {
                    columnsForWarningCheck = new HashSet<int>();
                    foreach (DataRow col in dtColumnsAll.Rows)
                    {
                        DataTable dt =
                            UploadWorld.ets_AdvancedCondition_Select((int)col["ColumnID"], "notification", "W");
                        if (dt != null && dt.Rows.Count > 0)
                            columnsForWarningCheck.Add((int)col["ColumnID"]);
                    }
                }
                if (columnsForExceedanceCheck == null)
                {
                    columnsForExceedanceCheck = new HashSet<int>();
                    foreach (DataRow col in dtColumnsAll.Rows)
                    {
                        DataTable dt =
                            UploadWorld.ets_AdvancedCondition_Select((int)col["ColumnID"], "notification", "E");
                        if (dt != null && dt.Rows.Count > 0)
                            columnsForExceedanceCheck.Add((int)col["ColumnID"]);
                    }
                }

                for (int i = 0; i < dtColumnsAll.Rows.Count; i++)
                {
                    bool bEachColumnExceedance = false;
                    if (columnsForValidityCheck.Contains((int) dtColumnsAll.Rows[i]["ColumnID"]))
                    {
                        DataTable dt = UploadWorld.ets_AdvancedCondition_Select((int) dtColumnsAll.Rows[i]["ColumnID"],
                            "notification", "V");
                        if (!IsDataValidAdvanced((int) dtColumnsAll.Rows[i]["ColumnID"], newTempRecord, dt, ref strTemp))
                        {
                            strRejectReason =
                                strRejectReason +
                                TheDatabase.GetInvalid_msg(dtColumnsAll.Rows[i]["DisplayName"].ToString());
                        }
                    }
                    else
                    {
                        if (bHasValidationOnEntry)
                        {
                            string strValue = UploadManager.GetTempRecordValue(ref newTempRecord,
                                dtColumnsAll.Rows[i]["SystemName"].ToString());
                            if (!string.IsNullOrEmpty(strValue))
                            {
                                string strFormulaV = "";

                                //if (dtColumnsAll.Rows[i]["ConV"] != DBNull.Value)
                                //{
                                //    Column theCheckColumn =
                                //        RecordManager.ets_Column_Details(int.Parse(dtColumnsAll.Rows[i]["ConV"]
                                //            .ToString()));
                                //    if (theCheckColumn != null)
                                //    {
                                //        string strCheckValue = UploadManager.GetTempRecordValue(ref newTempRecord,
                                //            theCheckColumn.SystemName);
                                //        strFormulaV = UploadWorld.Condition_GetFormula(
                                //            int.Parse(dtColumnsAll.Rows[i]["ColumnID"].ToString()),
                                //            theCheckColumn.ColumnID,
                                //            "V", strCheckValue);
                                //    }
                                //}
                                //else
                                //{
                                if (dtColumnsAll.Rows[i]["ValidationOnEntry"] != DBNull.Value &&
                                    dtColumnsAll.Rows[i]["ValidationOnEntry"].ToString().Length > 0)
                                {
                                    strFormulaV = dtColumnsAll.Rows[i]["ValidationOnEntry"].ToString();
                                }
                                //}

                                if (strFormulaV != "" && !UploadManager.IsDataValid(strValue, strFormulaV, ref strTemp,
                                        dtColumnsAll.Rows[i]["ColumnType"].ToString()))
                                {
                                    strRejectReason =
                                        strRejectReason +
                                        TheDatabase.GetInvalid_msg(dtColumnsAll.Rows[i]["DisplayName"].ToString());
                                }
                            }
                        }
                    }

                    if (columnsForExceedanceCheck.Contains((int)dtColumnsAll.Rows[i]["ColumnID"]))
                    {
                        DataTable dt = UploadWorld.ets_AdvancedCondition_Select((int)dtColumnsAll.Rows[i]["ColumnID"],
                            "notification", "E");
                        if (!IsDataValidAdvanced((int)dtColumnsAll.Rows[i]["ColumnID"], newTempRecord, dt, ref strTemp))
                        {
                            strExceedanceReason = strExceedanceReason +
                                                  TheDatabase.GetExceedance_msg(dtColumnsAll.Rows[i]["DisplayName"]
                                                      .ToString());
                            bEachColumnExceedance = true;
                        }
                    }
                    else
                    {
                        if (bShowExceedances)
                        {
                            if (dtColumnsAll.Rows[i]["ValidationOnExceedance"] != DBNull.Value)
                                //|| dtColumnsAll.Rows[i]["ConE"] != DBNull.Value)
                            {
                                string strValue = UploadManager.GetTempRecordValue(ref newTempRecord,
                                    dtColumnsAll.Rows[i]["SystemName"].ToString());
                                if (!string.IsNullOrEmpty(strValue))
                                {
                                    string strFormulaE = "";

                                    //if (dtColumnsAll.Rows[i]["ConE"] != DBNull.Value)
                                    //{
                                    //    Column theCheckColumn =
                                    //        RecordManager.ets_Column_Details(
                                    //            int.Parse(dtColumnsAll.Rows[i]["ConE"].ToString()));
                                    //    if (theCheckColumn != null)
                                    //    {
                                    //        string strCheckValue = UploadManager.GetTempRecordValue(ref newTempRecord,
                                    //            theCheckColumn.SystemName);
                                    //        strFormulaE = UploadWorld.Condition_GetFormula(
                                    //            int.Parse(dtColumnsAll.Rows[i]["ColumnID"].ToString())
                                    //            , theCheckColumn.ColumnID,
                                    //            "E", strCheckValue);
                                    //    }
                                    //}
                                    //else
                                    //{
                                    if (dtColumnsAll.Rows[i]["ValidationOnExceedance"] != DBNull.Value &&
                                        dtColumnsAll.Rows[i]["ValidationOnExceedance"].ToString().Length > 0)
                                    {
                                        strFormulaE = dtColumnsAll.Rows[i]["ValidationOnExceedance"].ToString();
                                    }
                                    //}

                                    if (strFormulaE != "" && !UploadManager.IsDataValid(strValue, strFormulaE,
                                            ref strTemp, dtColumnsAll.Rows[i]["ColumnType"].ToString()))
                                    {
                                        strExceedanceReason =
                                            strExceedanceReason +
                                            TheDatabase.GetExceedance_msg(
                                                dtColumnsAll.Rows[i]["DisplayName"].ToString());
                                        bEachColumnExceedance = true;
                                    }
                                }
                            }
                        }
                    }

                    if (!bEachColumnExceedance)
                    {
                        if (columnsForWarningCheck.Contains((int)dtColumnsAll.Rows[i]["ColumnID"]))
                        {
                            DataTable dt = UploadWorld.ets_AdvancedCondition_Select((int)dtColumnsAll.Rows[i]["ColumnID"],
                                "notification", "W");
                            if (!IsDataValidAdvanced((int)dtColumnsAll.Rows[i]["ColumnID"], newTempRecord, dt, ref strTemp))
                            {
                                strWarningReason = strWarningReason +
                                                   TheDatabase.GetWarning_msg(dtColumnsAll.Rows[i]["DisplayName"]
                                                       .ToString());
                            }
                        }
                        else
                        {
                            if (bHasValidationOnWarning)
                            {
                                if (dtColumnsAll.Rows[i]["ValidationOnWarning"] != DBNull.Value)
                                    //|| dtColumnsAll.Rows[i]["ConW"] != DBNull.Value)
                                {
                                    string strValue = UploadManager.GetTempRecordValue(ref newTempRecord,
                                        dtColumnsAll.Rows[i]["SystemName"].ToString());
                                    if (!string.IsNullOrEmpty(strValue))
                                    {
                                        string strFormulaW = "";

                                        //if (dtColumnsAll.Rows[i]["ConW"] != DBNull.Value)
                                        //{
                                        //    Column theCheckColumn =
                                        //        RecordManager.ets_Column_Details(
                                        //            int.Parse(dtColumnsAll.Rows[i]["ConW"].ToString()));
                                        //    if (theCheckColumn != null)
                                        //    {
                                        //        string strCheckValue = UploadManager.GetTempRecordValue(
                                        //            ref newTempRecord,
                                        //            theCheckColumn.SystemName);
                                        //        strFormulaW = UploadWorld.Condition_GetFormula(
                                        //            int.Parse(dtColumnsAll.Rows[i]["ColumnID"].ToString()),
                                        //            theCheckColumn.ColumnID,
                                        //            "W", strCheckValue);
                                        //    }
                                        //}
                                        //else
                                        //{
                                        if (dtColumnsAll.Rows[i]["ValidationOnWarning"] != DBNull.Value &&
                                            dtColumnsAll.Rows[i]["ValidationOnWarning"].ToString().Length > 0)
                                        {
                                            strFormulaW = dtColumnsAll.Rows[i]["ValidationOnWarning"].ToString();
                                        }
                                        //}

                                        if (strFormulaW != "" && !UploadManager.IsDataValid(strValue, strFormulaW,
                                                ref strTemp, dtColumnsAll.Rows[i]["ColumnType"].ToString()))
                                        {
                                            strWarningReason =
                                                strWarningReason +
                                                TheDatabase.GetWarning_msg(dtColumnsAll.Rows[i]["DisplayName"]
                                                    .ToString());
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //check SD

                    if (bHasCheckUnlikelyValue)
                    {
                        string strData = UploadManager.GetTempRecordValue(ref newTempRecord, dtColumnsAll.Rows[i]["SystemName"].ToString());
                        if (strData != "" && bool.Parse(dtColumnsAll.Rows[i]["CheckUnlikelyValue"].ToString()))
                        {
                            int? iCount = RecordManager.ets_Table_GetCount((int)theTable.TableID, dtColumnsAll.Rows[i]["SystemName"].ToString(), -1);

                            if (iCount >= Common.MinSTDEVRecords)
                            {
                                string strRecordedate;
                                if (dtColumnsAll.Rows[i]["IgnoreSymbols"].ToString().ToLower() == "true")
                                {
                                    strRecordedate = Common.IgnoreSymbols(strData);
                                }
                                else
                                {
                                    strRecordedate = strData;
                                }

                                double? dAVG = RecordManager.ets_Table_GetAVG((int)theTable.TableID, dtColumnsAll.Rows[i]["SystemName"].ToString(), -1);

                                double? dSTDEV = RecordManager.ets_Table_GetSTDEV((int)theTable.TableID, dtColumnsAll.Rows[i]["SystemName"].ToString(), -1);

                                double dRecordedate = double.Parse(strRecordedate);
                                if (dAVG != null && dSTDEV != null)
                                {
                                    dSTDEV = dSTDEV * 3;
                                    if (dRecordedate > (dAVG + dSTDEV) || dRecordedate < (dAVG - dSTDEV))
                                    {
                                        //deviation happaned
                                        strWarningReason = strWarningReason + TheDatabase.GetWarningUnlikely_msg(dtColumnsAll.Rows[i]["DisplayName"].ToString());
                                    }

                                }
                            }
                        }
                    }
                }



                //KG 14/12/17 Ticket 2572
                if (theTable.MaxTimeBetweenRecords != null && theTable.MaxTimeBetweenRecordsUnit != null)
                {
                    if (!RecordManager.IsTimeBetweenRecordOK((int)theTable.TableID, theTable.MaxTimeBetweenRecords, theTable.MaxTimeBetweenRecordsUnit, -1, (DateTime)newTempRecord.DateTimeRecorded))
                    {
                        strWarningReason = strWarningReason + " WARNING: " + WarningMsg.MaxtimebetweenRecords + "!";
                    }
                }

                strRejectReason = strRejectReason.Trim();
                strWarningReason = strWarningReason.Trim();
                strExceedanceReason = strExceedanceReason.Trim();


                if (strRejectReason.Length > 0)
                {
                    newTempRecord.RejectReason = strRejectReason.Trim();
                }

                if (strWarningReason.Length > 0)
                {
                    newTempRecord.WarningReason = strWarningReason.Trim();
                }

                if (strExceedanceReason.Length > 0)
                {
                    newTempRecord.WarningReason = newTempRecord.WarningReason == "" ? strExceedanceReason.Trim() : newTempRecord.WarningReason + " " + strExceedanceReason.Trim();

                }


                int iTempRecordID = UploadManager.ets_TempRecord_Insert(newTempRecord);


            }

            ///KG 17/6/2017 Ticket 2752 - comment this out to link the parent record before creating temp record
            //if (theImportTemplate != null)
            //{
            //    DataTable dtMatchingField = Common.DataTableFromText("SELECT ColumnID,ParentImportColumnID FROM ImportTemplateItem WHERE ImportTemplateID=" + theImportTemplate.ImportTemplateID.ToString() + " AND ParentImportColumnID IS NOT NULL ORDER BY ColumnIndex");

            //    if (dtMatchingField != null && dtMatchingField.Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in dtMatchingField.Rows)
            //        {
            //            Column theColumn = RecordManager.ets_Column_Details(int.Parse(dr["ColumnID"].ToString()));
            //            if (theColumn != null && theColumn.TableTableID != null)
            //            {
            //                Column theParentImportColumn = RecordManager.ets_Column_Details(int.Parse(dr["ParentImportColumnID"].ToString()));
            //                if (theParentImportColumn != null)
            //                {
            //                    spAdjustTempRecordLinkedValueOnImport(iBatchID, theColumn.SystemName, (int)theColumn.TableTableID, theParentImportColumn.SystemName);
            //                }
            //            }
            //        }
            //    }
            //}


            // if ((strUniqueColumnIDSys != "") && (!theTable.IsDataUpdateAllowed.HasValue || !theTable.IsDataUpdateAllowed.Value))

            if (strUniqueColumnIDSys != "")//to avoid a process if not needed
            {
                //if ((strUniqueColumnIDSys != "") && 
                //    (newBatch.AllowDataUpdate == null || 
                //                (newBatch.AllowDataUpdate != null && (bool)newBatch.AllowDataUpdate==false)))
                //{
                //RecordManager.ets_Batch_Duplicate(iBatchID, strUniqueColumnIDSys, strUniqueColumnID2Sys);
                //}
                //else
                //{
                //RecordManager.ets_Batch_Duplicate_Self(iBatchID, strUniqueColumnIDSys, strUniqueColumnID2Sys);
                RecordManager.ets_Batch_Duplicate_UniqueKey(iBatchID);
                //}
            }



            //if (theTable.MaxTimeBetweenRecords != null && theTable.MaxTimeBetweenRecordsUnit != null)
            //{
            //    //NEED JB's HELP
            //}

            if (!string.IsNullOrEmpty(theImportTemplate.SPAfterUpload) && theImportTemplate.SPAfterUpload.Length > 0)
            {
                SPAfterUpload(iBatchID, theImportTemplate.SPAfterUpload);
            }

            UploadManager.Batch_Validation_Count_Update(iBatchID);
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Upload CSV", ex.Message, ex.StackTrace, DateTime.Now, strOriginalFileName);
            SystemData.ErrorLog_Insert(theErrorLog);

            if (ex.Message.IndexOf("DateTime") > -1)
            {
                strMsg = "Date Recorded data are not valid, please review the file data.";
            }
            else if (ex.Message.IndexOf("recognized") > -1)
            {
                strMsg = "Unknown error occurred please review your import data.";
            }
            //else if (ex.Message.IndexOf(strTimeSamledColumnName) > -1)
            //{
            //    strMsg = "The file must have a Time Recorded column just after Date Recorded column.";
            //}
            else
            {
                strMsg = "UNKNOWN:" + ex.Message + ex.StackTrace;
                //SystemData.ErrorLog_Insert(theErrorLog);
            }

        }


    }

    public static void UploadCSVRaw(int? iUserID, Table theTable, string strBatchDescription,
     string strOriginalFileName, Guid? guidNew, string strImportFolder, out string strMsg, out int iBatchID,
        string strFileExtension, string strSelectedSheet, int? iAccountID, int? iSourceBatchID)
    {

        string strDBName = Common.GetDatabaseName();
        iBatchID = -1;
        strMsg = "";


        try
        {
            if (theTable != null && iAccountID != null && theTable.AccountID != null && theTable.AccountID != iAccountID)
            {
                if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session["AccountID"] != null)
                {
                    iAccountID = int.Parse(HttpContext.Current.Session["AccountID"].ToString());//theTable.AccountID;
                }
                else
                {
                    iAccountID = theTable.AccountID;
                }
            }
            //JA 20 JAN 2017 int.Parse(HttpContext.Current.Session["AccountID"].ToString())
            //iAccountID = theTable.AccountID;
            //iAccountID = int.Parse(HttpContext.Current.Session["AccountID"].ToString());

            Batch theSourceBatch = null;
            if (iSourceBatchID != null)
            {
                theSourceBatch = UploadManager.ets_Batch_Details((int)iSourceBatchID);
                if (theSourceBatch == null)
                {
                    return;
                }

                if (theTable == null)
                    theTable = RecordManager.ets_Table_Details((int)theSourceBatch.TableID);

                if (iUserID == null)
                    iUserID = theSourceBatch.UserIDUploaded;

                if (iAccountID == null && theTable != null)
                {
                    if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session["AccountID"] != null)
                    {
                        iAccountID = int.Parse(HttpContext.Current.Session["AccountID"].ToString());//theTable.AccountID;
                    }
                    else
                    {
                        iAccountID = theTable.AccountID;
                    }
                }
                //JA 20 JAN 2017 int.Parse(HttpContext.Current.Session["AccountID"].ToString())
                //iAccountID = theTable.AccountID;
                //iAccountID = int.Parse(HttpContext.Current.Session["AccountID"].ToString());

                if (iAccountID == null)
                    iAccountID = theSourceBatch.AccountID;
            }

            DataTable dtImportFileTable = null;

            int z = 0;

            if (iSourceBatchID == null)
            {
                string strFileUniqueName = guidNew.ToString() + strFileExtension;

                switch (strFileExtension.ToLower())
                {
                    case ".dbf":
                        dtImportFileTable = UploadManager.GetImportFileTableFromDBF(strImportFolder, strFileUniqueName, ref strMsg);
                        z = 0;
                        break;
                    case ".txt":
                        dtImportFileTable = UploadManager.GetImportFileTableFromText(strImportFolder, strFileUniqueName, ref strMsg);
                        z = 0;
                        break;
                    case ".csv":
                        dtImportFileTable = UploadManager.GetImportFileTableFromCSV(strImportFolder, strFileUniqueName, ref strMsg);
                        z = 0;
                        break;
                    case ".xls":
                        dtImportFileTable = OfficeManager.GetImportFileTableFromXLSX(strImportFolder, strFileUniqueName, strSelectedSheet, false);
                        break;
                    case ".xlsx":
                        dtImportFileTable = OfficeManager.GetImportFileTableFromXLSX(strImportFolder, strFileUniqueName, strSelectedSheet, false);
                        break;
                    case ".xml":
                        dtImportFileTable = UploadManager.GetImportFileTableFromXML(strImportFolder, strFileUniqueName);
                        break;
                    default:
                        dtImportFileTable = UploadManager.GetImportFileTableFromText(strImportFolder, strFileUniqueName, ref strMsg);
                        z = 0;
                        break;
                }

                if (strMsg != "")
                {
                    return;
                }
            }
            //JA 20 JAN 2017 int.Parse(HttpContext.Current.Session["AccountID"].ToString())
            //Batch newBatch = new Batch(null, (int)theTable.TableID,
            //    "[PRE] " + (strBatchDescription.Trim() == "" ? strOriginalFileName : strBatchDescription.Trim()),
            //    strOriginalFileName, null, guidNew, iUserID, theTable.AccountID);


            if (iUserID == null && iAccountID != null)
            {
                int? iAutoUserID = int.Parse(SystemData.SystemOption_ValueByKey_Account("AutoUploadUserID", null, null));
                iUserID = iAutoUserID;
                if (iUserID == null)
                {
                    User theAccountHolder = SecurityManager.User_AccountHolder((int)iAccountID);
                    if (theAccountHolder != null && theAccountHolder.UserID != null)
                    {
                        iUserID = theAccountHolder.UserID;
                    }
                }
            }

            Batch newBatch = new Batch(null, (int)theTable.TableID,
                "[PRE] " + (strBatchDescription.Trim() == "" ? strOriginalFileName : strBatchDescription.Trim()),
                strOriginalFileName, null, guidNew, iUserID, iAccountID);


            // newBatch.ImportTemplateID = -1;
            newBatch.IsIntermediate = true;
            newBatch.SourceColumnsCount = dtImportFileTable.Columns.Count;

            if (newBatch.SourceBatchID == null)
                newBatch.SourceBatchID = iSourceBatchID;

            iBatchID = UploadManager.ets_Batch_Insert(newBatch);

            DataColumnCollection dccIFT = dtImportFileTable.Columns;


            for (int r = z; r < dtImportFileTable.Rows.Count; r++)
            {
                TempRecord newTempRecord = new TempRecord();

                //JA 20 JAN 2017 int.Parse(HttpContext.Current.Session["AccountID"].ToString())
                //newTempRecord.AccountID = theTable.AccountID;
                newTempRecord.AccountID = iAccountID;// int.Parse(HttpContext.Current.Session["AccountID"].ToString());

                newTempRecord.BatchID = iBatchID;
                newTempRecord.TableID = (int)theTable.TableID;
                //newTempRecord.DateFormat = theTable.DateFormat;

                for (int c = 0; c < dtImportFileTable.Columns.Count; c++)
                {
                    newTempRecord.DateTimeRecorded = DateTime.Now;
                    UploadManager.MakeTheTempRecord(ref newTempRecord, "V" + String.Format("{0:000}", c + 1), dtImportFileTable.Rows[r][c].ToString());
                }
                int iTempRecordID = UploadManager.ets_TempRecord_Insert(newTempRecord);
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Upload CSV Raw", ex.Message, ex.StackTrace, DateTime.Now, strOriginalFileName);
            SystemData.ErrorLog_Insert(theErrorLog);

            strMsg = "Upload CSV Raw:" + ex.Message + ex.StackTrace;
        }
    }
}


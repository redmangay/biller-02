﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DocumentFormat.OpenXml.Wordprocessing;

/// <summary>
/// Summary description for MappedImport
/// </summary>
public class MappedImport
{
    public MappedImport()
    {
    }

    public static string GetMappingSP(int key)
    {
        switch (key)
        {
            case 1:
                return "dbg_Import_Mapping";
            case 2:
                return "dbg_Import_Mapping_Citect";
            default:
                return string.Empty;
        }
    }

    public static string GetMappingUpdateBatchSP(int key)
    {
        switch (key)
        {
            case 1:
                return "dbg_Import_Mapping_Update_Batch";
            case 2:
                return "dbg_Import_Mapping_Update_Batch_Citect";
            default:
                return string.Empty;
        }
    }

    public static void NotificationMessage(int key, Batch batch, List<int> children)
    {
        switch (key)
        {
            case 1:
                ALSCommon.ALSNotificationMessage(batch, children);
                break;
            case 2:
                Citect.CitectNotificationMessage(batch, children);
                break;
        }
    }

    public static DataTable GetVirtualImportFileTable(int key, int tableID, int targetTableID, int batchID)
    {
        switch (key)
        {
            case 1:
                return ALSCommon.GetVirtualImportFileTableALS(tableID, targetTableID, batchID);
            case 2:
                return Citect.GetVirtualImportFileTableCitect(tableID, targetTableID, batchID);
            default:
                return null;
        }
    }


}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TableRecordList
/// </summary>
public class TableRecordList
{
    public int Total { get; set; }
    public DataTable Data { get; set; } 
}
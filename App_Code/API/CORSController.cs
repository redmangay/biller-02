﻿using System.Net.Http;
using System.Web.Http;

public class CORSController : ApiController
{
    [Route("api/cors")]
    [HttpGet]
    public string GetResourceAsString([FromUri]string url)
    {
        string content = "";
        try
        {           
            HttpClient client = new HttpClient();
            content = client.GetStringAsync(url).Result;
        }
        catch//(Exception ex)
        {
            //string s = ex.ToString();
        }
        return content;
    }
}

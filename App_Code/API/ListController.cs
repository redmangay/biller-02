﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Description;

public class ListController : ApiController
{


    [Route("api/accountinviteinfo")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(TableRecordList))]
    public HttpResponseMessage accountinviteinfo()
    {
        try
        {
             var query = Request.GetQueryNameValuePairs();
             string strTaskID = query.FirstOrDefault(q => q.Key == "TaskID").Value; 

            TableRecordList list = new TableRecordList();
            DataTable dtListOfAccountInvite = Common.DataTableFromText("SELECT * FROM [AccountInvite] WHERE TaskRecordID=" + strTaskID + " ORDER BY AccountInviteID DESC");
            list.Total = dtListOfAccountInvite.Rows.Count;
            list.Data = dtListOfAccountInvite;
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }
        catch
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error.");
        }

    }

    [Route("api/externalcontractor")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(TableRecordList))]
    public HttpResponseMessage externalcontractor()
    {
        try
        {
            var query = Request.GetQueryNameValuePairs();
            string strAccountID = query.FirstOrDefault(q => q.Key == "AccountID").Value;

            TableRecordList list = new TableRecordList();
            DataTable dtListOfContractor = Common.DataTableFromText("SELECT DISTINCT U.* FROM [AccountInvite] AI JOIN [User] U ON AI.InvitedUserID=U.UserID WHERE AI.AccountID=" + strAccountID);
            list.Total = dtListOfContractor.Rows.Count;
            list.Data = dtListOfContractor;
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }
        catch
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error.");
        }

    }

    [Route("api/usersbyaccount")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(TableRecordList))]
    public HttpResponseMessage usersbyaccount()
    {
        try
        {
            var query = Request.GetQueryNameValuePairs();
            string strAccountID = query.FirstOrDefault(q => q.Key == "AccountID").Value;

            TableRecordList list = new TableRecordList();
            DataTable dtListOfUsers = Common.DataTableFromText(@"SELECT DISTINCT U.*
                                                                        FROM [User] U
                                                                        JOIN [UserRole]  UR ON U.UserID = UR.UserID 
                                                                        WHERE U.IsActive=1 AND UR.AccountID=" + strAccountID);
            list.Total = dtListOfUsers.Rows.Count;
            list.Data = dtListOfUsers;
            return Request.CreateResponse(HttpStatusCode.OK, list);
        }
        catch
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error.");
        }

    }




    [Route("api/logininfo")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(TableRecordList))]
    public HttpResponseMessage LoginInfo()
    {

        try
        {

           // var query = Request.GetQueryNameValuePairs();
            //string strUserEmail = query.FirstOrDefault(q => q.Key == "email").Value; 
            var strUserEmail = Thread.CurrentPrincipal.Identity.Name;

            string strUserID = Common.GetValueFromSQL("SELECT UserID FROM [User] WHERE Email='" + strUserEmail + "'");

            if (strUserID != "")
            {
                //get the new account's ID
                
                DataTable dtLogin = new DataTable();
                dtLogin.Columns.Add("FullName");
                dtLogin.Columns.Add("Phone");
                dtLogin.Columns.Add("Email");
                dtLogin.Columns.Add("Active");
                dtLogin.Columns.Add("Password");
                dtLogin.Columns.Add("Rate");
                dtLogin.Columns.Add("UserId");
                dtLogin.Columns.Add("DBGSystemRecordID");
                dtLogin.Columns.Add("AttachmentsID");
                dtLogin.Columns.Add("CheckListItemID");
                dtLogin.Columns.Add("ChecklistItemAttachmentID");
                dtLogin.Columns.Add("ClientID");
                dtLogin.Columns.Add("ContractorID");
                dtLogin.Columns.Add("InvoiceItemDetailID");
                dtLogin.Columns.Add("InvoicesID");
                dtLogin.Columns.Add("JobID");
                dtLogin.Columns.Add("JobAttachmentID");
                dtLogin.Columns.Add("JobTimeID");
                dtLogin.Columns.Add("ReceiptsID");
                dtLogin.Columns.Add("ChecklistTableID");
                dtLogin.Columns.Add("AccountID");
                dtLogin.Columns.Add("ChecklistTemplateTableID");
                dtLogin.Columns.Add("ChecklistTemplateItemTableID");
                dtLogin.Columns.Add("FilesLocation");
                dtLogin.Columns.Add("FilesPhisicalPath");
                dtLogin.Columns.Add("ProfilePicture");
                dtLogin.Columns.Add("CommentsTableID");
                dtLogin.Columns.Add("QuoteTableID");
                dtLogin.Columns.Add("QuoteItemTableID");
                dtLogin.Columns.Add("OfferTableID");
                dtLogin.Columns.Add("AccountName");
                dtLogin.Columns.Add("IsPrimaryAccount");
                dtLogin.Columns.Add("RoleID");
                dtLogin.Columns.Add("IsExpired");
                dtLogin.Columns.Add("IsUpgraded");
                dtLogin.Columns.Add("IsEmailConfirmed");

                DataTable dtUserRole = Common.DataTableFromText("SELECT * FROM [UserRole] WHERE Userid=" + strUserID);
                foreach(DataRow drUR in dtUserRole.Rows)
                {
                    int? iAccountID = int.Parse(drUR["AccountID"].ToString());

                    Account account = SecurityManager.Account_Details(iAccountID.GetValueOrDefault());

                    DataRow drUserInfo = dtLogin.NewRow();
                    drUserInfo["FilesLocation"] = SystemData.SystemOption_ValueByKey_Account("FilesLocation", null, null);
                    drUserInfo["FilesPhisicalPath"] = SystemData.SystemOption_ValueByKey_Account("FilesPhisicalPath", null, null);

                    drUserInfo["UserId"] = strUserID;

                    User theUser = SecurityManager.User_Details(int.Parse(strUserID));

                    string strAccountID = iAccountID.ToString();

                    drUserInfo["ProfilePicture"] = theUser.ProfilePicture;// Common.GetValueFromSQL("SELECT ProfilePicture FROM [User] WHERE UserId='" + strUserID + "'");

                    drUserInfo["IsEmailConfirmed"] = theUser.IsEmailConfirmed;

                    drUserInfo["AccountID"] = strAccountID;
                    Account theAccount = SecurityManager.Account_Details(int.Parse(strAccountID));
                    drUserInfo["IsExpired"] = "False";
                    if (theAccount.AccountTypeID==1)
                    {
                        if(theAccount.DateAdded.Value.AddMonths(1)<DateTime.Now)
                        {
                            drUserInfo["IsExpired"] = "True";
                        }
                    }


                    DataTable dtBilling = Common.DataTableFromText("SELECT * FROM [Billing] WHERE PaymentSuccess = 1 AND AccountID =" + strAccountID);
                    drUserInfo["IsUpgraded"] = "False";
                    if (dtBilling != null)
                    {
                        if (dtBilling.Rows.Count > 0)
                        {
                            drUserInfo["IsUpgraded"] = "True";
                        }
                    }

                    string strAttachmentsTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Attachment' AND AccountID=" + strAccountID + " AND IsActive=1");
                    string strCheckListItemTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Checklist Item' AND AccountID=" + strAccountID + " AND IsActive=1");
                    string strChecklistItemAttachmentTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Checklist Item Attachment' AND AccountID=" + strAccountID + " AND IsActive=1");

                    string strClientTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Client' AND AccountID=" + strAccountID + " AND IsActive=1");
                    //string strContractorTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Contractor' AND AccountID=" + strAccountID + " AND IsActive=1");
                    string strInvoiceItemDetailTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Invoice Item' AND AccountID=" + strAccountID + " AND IsActive=1");

                    string strInvoicesTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Invoice' AND AccountID=" + strAccountID + " AND IsActive=1");
                    string strJobTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Task' AND AccountID=" + strAccountID + " AND IsActive=1");
                    string strJobAttachmentTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Job Attachment' AND AccountID=" + strAccountID + " AND IsActive=1");
                    string strJobTimeTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Time' AND AccountID=" + strAccountID + " AND IsActive=1");
                    string strReceiptsTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Receipt' AND AccountID=" + strAccountID + " AND IsActive=1");
                    string strChecklistTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Checklist' AND AccountID=" + strAccountID + " AND IsActive=1");

                    string strChecklistTemplateTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Checklist Template' AND AccountID=" + strAccountID + " AND IsActive=1");
                    string strChecklistTemplateItemTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Checklist Template Item' AND AccountID=" + strAccountID + " AND IsActive=1");
                    string strCommentsTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Comment' AND AccountID=" + strAccountID + " AND IsActive=1");
                    string strQuoteTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Quote' AND AccountID=" + strAccountID + " AND IsActive=1");
                    string strQuoteItemTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Quote Item' AND AccountID=" + strAccountID + " AND IsActive=1");
                    string strOfferTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Offer' AND AccountID=" + strAccountID + " AND IsActive=1");



                    //ChecklistTableID
                    //If any user change the table name or have different language then we will be using system option.


                    int iTemp = 0;
                    if (int.TryParse(strAttachmentsTableID, out iTemp) == false)
                        strAttachmentsTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Attachments TableID", iAccountID, null);
                    if (int.TryParse(strCheckListItemTableID, out iTemp) == false)
                        strCheckListItemTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Check List Item TableID", iAccountID, null);
                    if (int.TryParse(strChecklistItemAttachmentTableID, out iTemp) == false)
                        strChecklistItemAttachmentTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Checklist Item Attachment TableID", iAccountID, null);
                    //if (int.TryParse(strContractorTableID, out iTemp) == false)
                    //    strContractorTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Contractor TableID", iAccountID, null);
                    if (int.TryParse(strInvoiceItemDetailTableID, out iTemp) == false)
                        strInvoiceItemDetailTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Invoice Item Detail TableID", iAccountID, null);
                    if (int.TryParse(strInvoicesTableID, out iTemp) == false)
                        strInvoicesTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Invoices TableID", iAccountID, null);
                    if (int.TryParse(strJobTableID, out iTemp) == false)
                        strJobTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Job TableID", iAccountID, null);
                    if (int.TryParse(strJobAttachmentTableID, out iTemp) == false)
                        strJobAttachmentTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Job Attachment TableID", iAccountID, null);
                    if (int.TryParse(strJobTimeTableID, out iTemp) == false)
                        strJobTimeTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Job Time TableID", iAccountID, null);
                    if (int.TryParse(strReceiptsTableID, out iTemp) == false)
                        strReceiptsTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Receipts TableID", iAccountID, null);

                    if (int.TryParse(strChecklistTableID, out iTemp) == false)
                        strChecklistTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Checklist TableID", iAccountID, null);

                    if (int.TryParse(strChecklistTemplateTableID, out iTemp) == false)
                        strChecklistTemplateTableID = SystemData.SystemOption_ValueByKey_Account("OnTask Checklist Template TableID", iAccountID, null);

                    if (int.TryParse(strChecklistTemplateItemTableID, out iTemp) == false)
                        strChecklistTemplateItemTableID = SystemData.SystemOption_ValueByKey_Account("OnTask Checklist Template Item TableID", iAccountID, null);



                    drUserInfo["AttachmentsID"] = strAttachmentsTableID;
                    drUserInfo["CheckListItemID"] = strCheckListItemTableID;
                    drUserInfo["ChecklistItemAttachmentID"] = strChecklistItemAttachmentTableID;
                    drUserInfo["ClientID"] = strClientTableID;
                    //drUserInfo["ContractorID"] = strContractorTableID;
                    drUserInfo["InvoiceItemDetailID"] = strInvoiceItemDetailTableID;
                    drUserInfo["InvoicesID"] = strInvoicesTableID;
                    drUserInfo["JobID"] = strJobTableID;
                    drUserInfo["JobAttachmentID"] = strJobAttachmentTableID;
                    drUserInfo["JobTimeID"] = strJobTimeTableID;
                    drUserInfo["ReceiptsID"] = strReceiptsTableID;
                    drUserInfo["ChecklistTableID"] = strChecklistTableID;

                    drUserInfo["ChecklistTemplateTableID"] = strChecklistTemplateTableID;
                    drUserInfo["ChecklistTemplateItemTableID"] = strChecklistTemplateItemTableID;

                    drUserInfo["CommentsTableID"] = strCommentsTableID;
                    drUserInfo["QuoteTableID"] = strQuoteTableID;
                    drUserInfo["QuoteItemTableID"] = strQuoteItemTableID;
                    drUserInfo["OfferTableID"] = strOfferTableID;
                    drUserInfo["AccountName"] = account.AccountName;
                    drUserInfo["IsPrimaryAccount"] = drUR["IsPrimaryAccount"].ToString();
                    drUserInfo["RoleID"] = drUR["RoleID"].ToString();

                    //DataTable dtUserRecord = Common.DataTableFromText("SELECT RecordID, V001 AS FullName,V003 AS Phone,V005 AS Active,V007 AS Rate FROM [Record] WHERE TableID="+strContractorTableID+" AND V008='"+strUserID+"'");

                    //if(dtUserRecord!=null && dtUserRecord.Rows.Count>0)
                    //{
                    //    foreach(DataRow dr in dtUserRecord.Rows)
                    //    {
                    //        drUserInfo["DBGSystemRecordID"]= dr["RecordID"].ToString();
                    //        drUserInfo["FullName"]= dr["FullName"].ToString();
                    //        drUserInfo["Phone"]= dr["Phone"].ToString();
                    //        drUserInfo["Active"]= dr["Active"].ToString();
                    //        drUserInfo["Rate"]= dr["Rate"].ToString();

                    //        break;
                    //    }
                    //}


                    //drUserInfo["DBGSystemRecordID"] = dr["RecordID"].ToString();
                    drUserInfo["FullName"] = theUser.FirstName + " " + theUser.LastName;
                    drUserInfo["Phone"] = theUser.PhoneNumber;
                    drUserInfo["Active"] = theUser.IsActive.ToString();
                    drUserInfo["Rate"] = "";
                    if (!string.IsNullOrEmpty(theUser.Attributes))
                    {
                        UserAttributes_ontask userAttributes = DocGen.DAL.JSONField.GetTypedObject<UserAttributes_ontask>(theUser.Attributes);
                        if (userAttributes != null && userAttributes.Rate != null)
                        {
                            drUserInfo["Rate"] = userAttributes.Rate.ToString();
                        }

                    }

                    dtLogin.Rows.Add(drUserInfo);

                }




                

                TableRecordList list = new TableRecordList();
                list.Total = dtLogin.Rows.Count;
                list.Data = dtLogin;
                return Request.CreateResponse(HttpStatusCode.OK, list);

            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Email not found.");
            }
        
        }
        catch
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error.");
        }

    }







    [Route("api/CheckEmailExists")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(TableRecordList))]
    public HttpResponseMessage CheckEmailExists()
    {

        try
        {

            var query = Request.GetQueryNameValuePairs();
            string strUserEmail = query.FirstOrDefault(q => q.Key == "email").Value; 
            
            string strUserID = Common.GetValueFromSQL("SELECT UserID FROM [User] WHERE Email='" + strUserEmail + "'");

            if (strUserID != "")
            {
                //get the new account's ID
                int? iAccountID = SecurityManager.GetPrimaryAccountID(int.Parse(strUserID));
                DataTable dtLogin = new DataTable();
                dtLogin.Columns.Add("UserId");               
                DataRow drUserInfo = dtLogin.NewRow();
                drUserInfo["UserId"] = strUserID;                
                dtLogin.Rows.Add(drUserInfo);

                TableRecordList list = new TableRecordList();
                list.Total = dtLogin.Rows.Count;
                list.Data = dtLogin;
                return Request.CreateResponse(HttpStatusCode.NotAcceptable, list);

            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, "Email not found.");
            }

        }
        catch
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error.");
        }

    }




    [Route("api/GetContentID")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage GetContentID()
    {

        try
        {          

            var query = Request.GetQueryNameValuePairs();
            string sAccountID = query.FirstOrDefault(q => q.Key == "AccountID").Value;
            string sContentKey = query.FirstOrDefault(q => q.Key == "ContentKey").Value;

            int? iAccountID=null;
            if (!string.IsNullOrEmpty(sAccountID))
            {
                iAccountID = int.Parse(sAccountID);
            }

            Content theContentEmail = SystemData.Content_Details_ByKey(sContentKey, null);
            return Request.CreateResponse(HttpStatusCode.OK, theContentEmail.ContentID.ToString());

        }
        catch
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error.");
        }

    }


    [Route("api/GetRecordDetailURL")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage GetRecordDetailURL()
    {

        try
        {
            //sPass = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(sPass));
            //if(Thread.CurrentPrincipal.Identity)

            var query = Request.GetQueryNameValuePairs();
            string sSite = query.FirstOrDefault(q => q.Key == "Site").Value;
            string sPass = query.FirstOrDefault(q => q.Key == "Pass").Value;
            string sRecordID = query.FirstOrDefault(q => q.Key == "RecordID").Value;

            //sPass = System.Web.HttpUtility.UrlDecode(sPass);

            sPass = EncryptionService.Decrypt(sPass);
            string xml = @"<root>" +
                      " <email>" + System.Web.HttpUtility.HtmlEncode(Cryptography.Encrypt(Thread.CurrentPrincipal.Identity.Name)) + "</email>" +
                      " <pass>" + System.Web.HttpUtility.HtmlEncode(Cryptography.Encrypt(sPass)) + "</pass>" +
                     "</root>";
            SearchCriteria theSC = new SearchCriteria(null, xml);
            int iSC = SystemData.SearchCriteria_Insert(theSC);
            string strTableID = Common.GetValueFromSQL("SELECT TableID FROM [Record] WHERE RecordID=" + sRecordID);
            string strURL = sSite + "/Pages/Record/RecordDetail.aspx?mode=ap70n78xI6o=&SearchCriteriaID=" 
                + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt(strTableID)
                + "&Recordid=" + Cryptography.Encrypt(sRecordID) + "&autologin=" + Cryptography.Encrypt(iSC.ToString())
                + "&viewon=" + Cryptography.Encrypt("app");



            return Request.CreateResponse(HttpStatusCode.OK, strURL);

        }
        catch
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error.");
        }

    }

    [Route("api/tables/{tableId:int}/allcolumnrecords")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(TableRecordList))]
    public HttpResponseMessage AllColumnRecords( int tableId)
    {
        var username = Thread.CurrentPrincipal.Identity.Name;
        User user = SecurityManager.User_ByEmail(username);
        bool hasPermission = user != null && user.IsActive.HasValue && user.IsActive.Value;
        TableRights rights = user != null ? SecurityManager.GetUserRightsOnTable(user.UserID.Value, tableId) : null;
        hasPermission = hasPermission && rights != null && (rights.CanSelect == "Y" || rights.CanSelect == "O");

        Table theTable = RecordManager.ets_Table_Details(tableId);
        int accountId = (int)theTable.AccountID;

        if (hasPermission == false)
        {
            string roletype = SecurityManager.GetUserRoleTypeID((int)user.UserID, accountId);
            if (roletype == "2")
            {
                hasPermission = true;
            }
        }



        if (hasPermission)
        {
            var query = Request.GetQueryNameValuePairs();
            
            
            string sTextSearch = GetNullableString(query.FirstOrDefault(q => q.Key == "sTextSearch").Value);
            

            try
            {
                TableRecordList list = new TableRecordList();

                int iTN = 0;
                int _iTotalDynamicColumns = 0;
                string strReturnSQL="", sReturnHeaderSQL="";
                Exception exParam = null;
                DataTable dtDataSource = RecordManager.ets_Record_List(tableId,
               null,true,null, null, null,
               "", "", 0, 0, ref iTN, ref _iTotalDynamicColumns, "allcolumns", "", sTextSearch,
              null, null, "", "", "", null,
              ref strReturnSQL, ref sReturnHeaderSQL, ref exParam, false);

                list.Total = iTN;
                list.Data = dtDataSource;

               
                
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
            }
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied");
        }
    }




    [Route("api/accounts/{accountId:int}/tables/{tableId:int}/records")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(TableRecordList))]
    public HttpResponseMessage ListRecord(int accountId, int tableId)
    {
        var username = Thread.CurrentPrincipal.Identity.Name;
        User user = SecurityManager.User_ByEmail(username);               
        bool hasPermission = user != null && user.IsActive.HasValue && user.IsActive.Value;
        TableRights rights = user != null ? SecurityManager.GetUserRightsOnTable(user.UserID.Value, tableId) : null;
        hasPermission = hasPermission && rights != null && (rights.CanSelect == "Y" || rights.CanSelect == "O");

        if(hasPermission==false)
        {
            string roletype = SecurityManager.GetUserRoleTypeID((int)user.UserID, accountId);
            if(roletype=="2")
            {
                hasPermission = true;
            }
        }
        


        if (hasPermission)
        {            
            var query = Request.GetQueryNameValuePairs();
            string dateFormat = "yyyy-MM-dd";
            int? nEnteredBy = GetNullableInt(query.FirstOrDefault(q => q.Key == "nEnteredBy").Value);
            bool? bIsActive = GetNullableBool(query.FirstOrDefault(q => q.Key == "bIsActive").Value);
            bool? bHasWarningResults = GetNullableBool(query.FirstOrDefault(q => q.Key == "bHasWarningResults").Value);
            DateTime? dDateFrom = GetNullableDate(query.FirstOrDefault(q => q.Key == "dDateFrom").Value, dateFormat);
            DateTime? dDateTo = GetNullableDate(query.FirstOrDefault(q => q.Key == "dDateTo").Value, dateFormat);
            string sOrder = GetNullableString(query.FirstOrDefault(q => q.Key == "sOrder").Value);
            int? nStartRow = GetNullableInt(query.FirstOrDefault(q => q.Key == "nStartRow").Value);
            int? nMaxRows = GetNullableInt(query.FirstOrDefault(q => q.Key == "nMaxRows").Value);
            string sType = GetNullableString(query.FirstOrDefault(q => q.Key == "sType").Value);
            string sNumericSearch = GetNullableString(query.FirstOrDefault(q => q.Key == "sNumericSearch").Value);
            string sTextSearch = GetNullableString(query.FirstOrDefault(q => q.Key == "sTextSearch").Value);
            DateTime? dDateAddedFrom = GetNullableDate(query.FirstOrDefault(q => q.Key == "dDateAddedFrom").Value, dateFormat);
            DateTime? dDateAddedTo = GetNullableDate(query.FirstOrDefault(q => q.Key == "dDateAddedTo").Value, dateFormat);
            string sParentColumnSortSQL = GetNullableString(query.FirstOrDefault(q => q.Key == "sParentColumnSortSQL").Value);
            string sHeaderSQL = GetNullableString(query.FirstOrDefault(q => q.Key == "sHeaderSQL").Value);
            string sViewName = GetNullableString(query.FirstOrDefault(q => q.Key == "sViewName").Value);
            int? nViewID = GetNullableInt(query.FirstOrDefault(q => q.Key == "nViewID").Value);

            try
            {
                TableRecordList list = new TableRecordList();
                DataSet ds = new DataSet();
                using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("ets_record_list", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@nTableID", tableId);
                    cmd.Parameters.AddWithValue("@nEnteredBy", nEnteredBy);
                    cmd.Parameters.AddWithValue("@bIsActive", bIsActive);
                    cmd.Parameters.AddWithValue("@bHasWarningResults", bHasWarningResults);
                    cmd.Parameters.AddWithValue("@dDateFrom", dDateFrom);
                    cmd.Parameters.AddWithValue("@dDateTo", dDateTo);
                    cmd.Parameters.AddWithValue("@sOrder", sOrder);
                    cmd.Parameters.AddWithValue("@nStartRow", nStartRow);
                    cmd.Parameters.AddWithValue("@nMaxRows", nMaxRows);
                    cmd.Parameters.AddWithValue("@sType", sType);
                    cmd.Parameters.AddWithValue("@sNumericSearch", sNumericSearch);
                    cmd.Parameters.AddWithValue("@sTextSearch", sTextSearch);
                    cmd.Parameters.AddWithValue("@dDateAddedFrom", dDateAddedFrom);
                    cmd.Parameters.AddWithValue("@dDateAddedTo", dDateAddedTo);
                    cmd.Parameters.AddWithValue("@sParentColumnSortSQL", sParentColumnSortSQL);
                    cmd.Parameters.AddWithValue("@sHeaderSQL", sHeaderSQL);
                    cmd.Parameters.AddWithValue("@sViewName", sViewName);
                    cmd.Parameters.AddWithValue("@nViewID", nViewID);
                    cmd.Parameters.AddWithValue("@bReturnIsActive", true);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Open();
                    da.Fill(ds);
                    conn.Close();
                }
                if (ds.Tables.Count == 2)
                {
                    list.Total = Convert.ToInt32(ds.Tables[1].Rows[0][0]);
                    list.Data = ds.Tables[0];
                }
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
            }
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied");
        }
    }

    [Route("api/accounts/{accountId:int}/tables/{tableId:int}/structure")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(List<TableField>))]
    public HttpResponseMessage TableStructure(int accountId, int tableId)
    {
        var username = Thread.CurrentPrincipal.Identity.Name;
        User user = SecurityManager.User_ByEmail(username);
        bool hasPermission = user != null && user.IsActive.HasValue && user.IsActive.Value;
        TableRights rights = user != null ? SecurityManager.GetUserRightsOnTable(user.UserID.Value, tableId) : null;
        hasPermission = hasPermission && rights != null && (rights.CanAccessAdminMenu == "Y");


        if (hasPermission == false)
        {
            string roletype = SecurityManager.GetUserRoleTypeID((int)user.UserID, accountId);
            if (roletype == "2")
            {
                hasPermission = true;
            }
        }


        if (hasPermission)
        {
            try
            {
                List<TableField> fields = new List<TableField>();

                using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("dev_TableStructure", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TableID", tableId);
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            fields.Add(new TableField()
                            {
                                SystemName = (string)reader["SystemName"],
                                DisplayName = (string)reader["DisplayName"],
                                ColumnType = (string)reader["ColumnType"],
                                DropDownType = (string)reader["DropDownType"],
                                DropDownTableId = reader["Dropdown Table ID"] != DBNull.Value ? (int)reader["Dropdown Table ID"] : (int?)null
                            });
                        }
                        reader.Close();
                    }
                    conn.Close();
                }
                return Request.CreateResponse(HttpStatusCode.OK, fields);
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
            }
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied");
        }
    }



    [Route("api/getuser")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(UserField))]
    public HttpResponseMessage getuser()
    {

        try
        {

            var query = Request.GetQueryNameValuePairs();
            string sUserID = query.FirstOrDefault(q => q.Key == "userid").Value;
            UserField theUserField = new UserField();

            if (!string.IsNullOrEmpty(sUserID))
            {
                User theUser = SecurityManager.User_Details(int.Parse(sUserID));
                if (theUser != null)
                {
                    theUserField.UserID = (int)theUser.UserID;
                    theUserField.Email = theUser.Email;
                    if (string.IsNullOrEmpty(theUser.FirstName))
                        theUser.FirstName = "";
                    if (string.IsNullOrEmpty(theUser.LastName))
                        theUser.LastName = "";

                    theUserField.FullName = theUser.FirstName + " " + theUser.LastName;
                    theUserField.PhoneNumber = theUser.PhoneNumber;
                    theUserField.ProfilePicture = theUser.ProfilePicture;
                    //theUserField.RecordID = int.Parse(sRecordID);
                }
            }




            return Request.CreateResponse(HttpStatusCode.OK, theUserField);

        }
        catch
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error.");
        }

    }





    //[Route("api/getuserfromrecordid")]
    //[HttpGet]
    //[BasicAuthentication]
    //[ResponseType(typeof(UserField))]
    //public HttpResponseMessage getuserfromrecordid()
    //{

    //    try
    //    {            

    //        var query = Request.GetQueryNameValuePairs();           
    //        string sRecordID = query.FirstOrDefault(q => q.Key == "recordid").Value;
    //        string sUserID = Common.GetValueFromSQL("SELECT V008 FROM [Record]   WHERE RecordID =" + sRecordID);
    //        UserField theUserField = new UserField();

    //        if (!string.IsNullOrEmpty(sUserID))
    //        {
    //            User theUser = SecurityManager.User_Details(int.Parse(sUserID));
    //            if(theUser!=null)
    //            {
    //                theUserField.UserID = (int)theUser.UserID;
    //                theUserField.Email = theUser.Email;
    //                if (string.IsNullOrEmpty(theUser.FirstName))
    //                    theUser.FirstName = "";
    //                if (string.IsNullOrEmpty(theUser.LastName))
    //                    theUser.LastName = "";

    //                theUserField.FullName = theUser.FirstName + " " + theUser.LastName;
    //                theUserField.PhoneNumber = theUser.PhoneNumber;
    //                theUserField.ProfilePicture = theUser.ProfilePicture;
    //                theUserField.RecordID = int.Parse(sRecordID);
    //            }
    //        }




    //        return Request.CreateResponse(HttpStatusCode.OK, theUserField);

    //    }
    //    catch
    //    {
    //        return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error.");
    //    }

    //}






    [Route("api/accounts/{accountId:int}/createbilling")]
    [HttpPost]
    [BasicAuthentication]
    [ResponseType(typeof(int))]
    public HttpResponseMessage createbilling(int accountId)
    {
        int newId = 0;
        var username = Thread.CurrentPrincipal.Identity.Name;
        User user = SecurityManager.User_ByEmail(username);
        bool hasPermission = false;
       
        
        string roletype = SecurityManager.GetUserRoleTypeID((int)user.UserID, accountId);
        if (roletype == "2")
        {
            hasPermission = true;
        }
       
        if (hasPermission)
        {
            try
            {
                var query = Request.GetQueryNameValuePairs();
                string sPaymentMethod = query.FirstOrDefault(q => q.Key == "PaymentMethod").Value;
                DataTable dtUsers = Common.DataTableFromText("SELECT U.UserID FROM [UserRole] UR JOIN [User] U ON UR.UserID=U.UserID WHERE U.IsActive=1  and UR.IsPrimaryAccount=1 and UR.AccountID=" + accountId.ToString());

                string sBillingID = BillingAPI.GetActiveBillingID(accountId);

                if (sBillingID == "")
                {
                    BillingAPI.Billing newBilling = new BillingAPI.Billing();
                    newBilling.AccountID = accountId;
                    newBilling.Amount = BillingAPI.TotalChargeAmount(newBilling.AccountID, dtUsers.Rows.Count, null, null);
                    newBilling.BillingDate = DateTime.Now;
                    newBilling.Currency = BillingAPI.GetAccountCurrency(newBilling.AccountID);
                    newBilling.NoOfUsers = dtUsers.Rows.Count;
                    newBilling.Remarks = "Updated subscription(" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ").";
                    newBilling.PaymentDate = DateTime.Now;
                    newBilling.PaymentMethod = sPaymentMethod;
                    newBilling.PaymentSuccess = true;
                    newBilling.TargetAccountTypeID = 2;
                    //newBilling.
                    Int32 billingid = BillingAPI.CreateBilling(newBilling);

                    Account theAccount = SecurityManager.Account_Details(accountId);
                    theAccount.AccountTypeID = 2;
                    SecurityManager.Account_Update(theAccount);



                    newId =(int) billingid;
                }
                else
                {
                    BillingAPI.Billing editBilling = BillingAPI.GetBilling(int.Parse(sBillingID));
                    editBilling.Amount = BillingAPI.TotalChargeAmount(editBilling.AccountID, dtUsers.Rows.Count, null, null);
                    editBilling.BillingDate = DateTime.Now;
                    editBilling.PaymentMethod = sPaymentMethod;
                    editBilling.NoOfUsers = dtUsers.Rows.Count;
                    editBilling.PaymentDate = DateTime.Now;                    
                    editBilling.PaymentSuccess = true;
                    editBilling.TargetAccountTypeID = 2;
                    //editBilling.Remarks = editBilling.Remarks + "Updated subscription(" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ").";
                    BillingAPI.UpdateBilling(editBilling);

                    Account theAccount = SecurityManager.Account_Details(accountId);
                    theAccount.AccountTypeID = 2;
                    SecurityManager.Account_Update(theAccount);


                    newId = (int)editBilling.BillingID;
                }                

                return Request.CreateResponse(HttpStatusCode.OK, newId);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied");
        }
    }



    [Route("api/accounts/{accountId:int}/updatebilling")]
    [HttpPost]
    [BasicAuthentication]
    [ResponseType(typeof(int))]
    public HttpResponseMessage updatebilling(int accountId)
    {
        int newId = 0;
        var username = Thread.CurrentPrincipal.Identity.Name;
        User user = SecurityManager.User_ByEmail(username);
        bool hasPermission = false;


        string roletype = SecurityManager.GetUserRoleTypeID((int)user.UserID, accountId);
        if (roletype == "2")
        {
            hasPermission = true;
        }

        if (hasPermission)
        {
            try
            {
                var query = Request.GetQueryNameValuePairs();
                string sIsCancel = query.FirstOrDefault(q => q.Key == "IsCancel").Value;
                DataTable dtUsers = Common.DataTableFromText("SELECT U.UserID FROM [UserRole] UR JOIN [User] U ON UR.UserID=U.UserID WHERE U.IsActive=1  and UR.IsPrimaryAccount=1 and UR.AccountID=" + accountId.ToString());

                

                string sBillingID = Common.GetValueFromSQL(@"SELECT BillingID FROM Billing WHERE AccountID = " + accountId.ToString() +
            @"  AND (PaymentSuccess=1 OR PaymentSuccess IS NULL) ORDER BY BillingID DESC");

                if (sBillingID == "")
                {
                   
                }
                else
                {
                    if(sIsCancel.ToLower()=="true")
                    {
                        BillingAPI.Billing editBilling = BillingAPI.GetBilling(int.Parse(sBillingID));
                        editBilling.Amount = BillingAPI.TotalChargeAmount(editBilling.AccountID, dtUsers.Rows.Count, null, null);
                        editBilling.BillingDate = DateTime.Now;
                        editBilling.NoOfUsers = dtUsers.Rows.Count;
                        editBilling.PaymentDate = DateTime.Now;
                        editBilling.PaymentSuccess = false;
                        editBilling.TargetAccountTypeID = 1;
                        //editBilling.Remarks = editBilling.Remarks + "Updated subscription(" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ").";
                        BillingAPI.UpdateBilling(editBilling);
                        newId = (int)editBilling.BillingID;
                    }
                    
                }

                return Request.CreateResponse(HttpStatusCode.OK, newId);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied");
        }
    }



    [Route("api/accounts/{accountId:int}/tables/{tableId:int}/records")]
    [HttpPost]
    [BasicAuthentication]
    [ResponseType(typeof(int))]
    public HttpResponseMessage InsertRecord(int accountId, int tableId, [FromBody]Dictionary<string, string> fieldValues) {
        int newId = 0;
        var username = Thread.CurrentPrincipal.Identity.Name;
        User user = SecurityManager.User_ByEmail(username);
        bool hasPermission = user != null && user.IsActive.HasValue && user.IsActive.Value;
        TableRights rights = user != null ? SecurityManager.GetUserRightsOnTable(user.UserID.Value, tableId) : null;
        hasPermission = hasPermission && rights != null && (rights.CanInsert == "Y");

        if (hasPermission == false)
        {
            string roletype = SecurityManager.GetUserRoleTypeID((int)user.UserID, accountId);
            if (roletype == "2")
            {
                hasPermission = true;
            }
        }


        if (hasPermission)
        {
            try
            {
                int userId = SecurityManager.User_ByEmail(username).UserID.Value;
                using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("ets_Record_Insert", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "@nNewID",
                        DbType = DbType.Int32,
                        Direction = ParameterDirection.Output
                    });

                    cmd.Parameters.AddWithValue("@TableID", tableId);
                    cmd.Parameters.AddWithValue("@DateTimeRecorded", DateTime.Now);
                    cmd.Parameters.AddWithValue("@EnteredBy", userId);

                    foreach (string fieldName in fieldValues.Keys)
                    {
                        cmd.Parameters.AddWithValue(String.Format("@{0}", fieldName), fieldValues[fieldName]);
                    }
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();

                    newId = Convert.ToInt32(cmd.Parameters["@nNewID"].Value);
                }
                return Request.CreateResponse(HttpStatusCode.OK, newId);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied");
        }
    }


    [Route("api/accounts/{accountId:int}/tables/{tableId:int}/{client}/records")]
    [HttpPost]
    [BasicAuthentication]
    [ResponseType(typeof(int))]
    public HttpResponseMessage InsertRecord(int accountId, int tableId, string client, [FromBody]Dictionary<string, string> fieldValues)
    {
        int newId = 0;
        var username = Thread.CurrentPrincipal.Identity.Name;
        User user = SecurityManager.User_ByEmail(username);
        bool hasPermission = user != null && user.IsActive.HasValue && user.IsActive.Value;
        TableRights rights = user != null ? SecurityManager.GetUserRightsOnTable(user.UserID.Value, tableId) : null;
        hasPermission = hasPermission && rights != null && (rights.CanInsert == "Y");

        if (hasPermission == false)
        {
            string roletype = SecurityManager.GetUserRoleTypeID((int)user.UserID, accountId);
            if (roletype == "2")
            {
                hasPermission = true;
            }
        }


        if (hasPermission)
        {
            try
            {
                int userId = SecurityManager.User_ByEmail(username).UserID.Value;
                using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("ets_Record_Insert", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "@nNewID",
                        DbType = DbType.Int32,
                        Direction = ParameterDirection.Output
                    });

                    cmd.Parameters.AddWithValue("@TableID", tableId);
                    cmd.Parameters.AddWithValue("@DateTimeRecorded", DateTime.Now);
                    cmd.Parameters.AddWithValue("@EnteredBy", userId);

                    foreach (string fieldName in fieldValues.Keys)
                    {
                        cmd.Parameters.AddWithValue(String.Format("@{0}", fieldName), fieldValues[fieldName]);
                    }
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();

                    newId = Convert.ToInt32(cmd.Parameters["@nNewID"].Value);

                }

                /* == send notification on add record == */
                /* the same MethodName but may have different code for different client of process */
                try
                {
                    string methodMessage = String.Empty;
                    List<object> objList = new List<object>();
                    GenericParam theGenericParam = new GenericParam();
                    theGenericParam.TableID = tableId;
                    theGenericParam.AccountID = accountId;
                    theGenericParam.RecordID = newId;
                    objList.Add(theGenericParam);
                    List<object> roList = new List<object>();
                    roList = CustomMethod.DotNetMethod("api_add_data_notification", objList, client);
                    if (roList != null && roList.Count > 0)
                    {
                        foreach (object obj in roList)
                        {
                            if (obj.GetType().Name == "String")
                            {
                                methodMessage = (string)obj;

                                ErrorLog theErrorLog = new ErrorLog(null, "api_insert_record", methodMessage, "api_insert_record", DateTime.Now, "ListController/InsertRecord");
                                SystemData.ErrorLog_Insert(theErrorLog);
                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "api_insert_record", ex.Message, ex.StackTrace, DateTime.Now, "ListController/InsertRecord");
                    SystemData.ErrorLog_Insert(theErrorLog);
                }


                return Request.CreateResponse(HttpStatusCode.OK, newId);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied");
        }
    }



    [Route("api/task/{taskrecordid:int}/template/{templaterecordid:int}/AddChecklistTemplateToTask")]
    [HttpPost]
    [BasicAuthentication]
    [ResponseType(typeof(int))]
    public HttpResponseMessage AddChecklistTemplateToTask(int taskrecordid, int templaterecordid)
    {
        //int newId = 0;
        var username = Thread.CurrentPrincipal.Identity.Name;
        User user = SecurityManager.User_ByEmail(username);
        bool hasPermission = user != null && user.IsActive.HasValue && user.IsActive.Value;

        Record theTaskRecord = RecordManager.ets_Record_Detail_Full(taskrecordid, null, false);
        Record theTemplateRecord = RecordManager.ets_Record_Detail_Full(templaterecordid, null, false);

        string sUserAccountID = Common.GetValueFromSQL("SELECT AccountID FROM [Table] WHERE TableID="+ theTaskRecord.TableID.ToString());
        //string sTemplateAccountID = Common.GetValueFromSQL("SELECT AccountID FROM [Table] WHERE TableID=" + theTemplateRecord.TableID.ToString());

        TableRights rights = user != null ? SecurityManager.GetUserRightsOnTable(user.UserID.Value,(int) theTaskRecord.TableID) : null;
        hasPermission = hasPermission && rights != null && (rights.CanInsert == "Y");

        if (hasPermission == false)
        {
            string roletype = SecurityManager.GetUserRoleTypeID((int)user.UserID, int.Parse(sUserAccountID));
            if (roletype == "2")
            {
                hasPermission = true;
            }
        }





        if (hasPermission)
        {
            try
            {
                string sUser_ChecklistTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Checklist' AND AccountID="
                                     + sUserAccountID + " AND IsActive=1");

                string sUser_ChecklistItemTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Checklist Item' AND AccountID="
                     + sUserAccountID + " AND IsActive=1");

                string sUser_AttachmentTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Attachment' AND AccountID="
                     + sUserAccountID + " AND IsActive=1");
                string sUser_CheckListTemplateItemTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Checklist Template Item' AND AccountID="
                     + sUserAccountID + " AND IsActive=1");

                var query = Request.GetQueryNameValuePairs();
                string sCheckListName = GetNullableString(query.FirstOrDefault(q => q.Key == "checklistname").Value);
                string sSignatureRequired = GetNullableString(query.FirstOrDefault(q => q.Key == "signaturerequired").Value);

                if (string.IsNullOrEmpty(sCheckListName))
                    sCheckListName = theTemplateRecord.V001;
                if (string.IsNullOrEmpty(sSignatureRequired))
                    sSignatureRequired = theTemplateRecord.V004;

                Record rUser_CheckList = new Record();
                rUser_CheckList.TableID = int.Parse(sUser_ChecklistTableID);
                rUser_CheckList.EnteredBy = user.UserID;
                rUser_CheckList.DateTimeRecorded = DateTime.Now;
                rUser_CheckList.V001 = Guid.NewGuid().ToString();
                rUser_CheckList.V002 = sCheckListName;
                rUser_CheckList.V005 = theTemplateRecord.V009;
                rUser_CheckList.V003 = theTaskRecord.RecordID.ToString();
                rUser_CheckList.V006 = sSignatureRequired;

                rUser_CheckList.RecordID = RecordManager.ets_Record_Insert(rUser_CheckList);

                //now let's add multiple checklist item
                DataTable dtTemplate_CheckListItemRecord = Common.DataTableFromText("SELECT RecordID FROM [Record]  WHERE  IsActive=1 AND TableID="
                    + sUser_CheckListTemplateItemTableID + " AND V001='" + theTemplateRecord.RecordID.ToString() + "'");
                if (dtTemplate_CheckListItemRecord != null && dtTemplate_CheckListItemRecord.Rows.Count > 0)
                {
                    //we have checklist item
                    foreach (DataRow drTemplate_CLItemRecord in dtTemplate_CheckListItemRecord.Rows)
                    {
                        Record rU_CLIRecord = new Record();// 
                        Record rT_rU_CLIRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drTemplate_CLItemRecord[0].ToString()), null, false);
                        if (rU_CLIRecord != null)
                        {
                            rU_CLIRecord.TableID = int.Parse(sUser_ChecklistItemTableID);
                            rU_CLIRecord.EnteredBy = user.UserID;
                            rU_CLIRecord.V011 = Guid.NewGuid().ToString();
                            rU_CLIRecord.DateTimeRecorded = DateTime.Now;
                            rU_CLIRecord.V002 = rT_rU_CLIRecord.V003;
                            rU_CLIRecord.V004 = rT_rU_CLIRecord.V002;
                            rU_CLIRecord.V014 = rUser_CheckList.RecordID.ToString();
                            rU_CLIRecord.RecordID = RecordManager.ets_Record_Insert(rU_CLIRecord);

                            //check attachements(checklist item)
                            DataTable dtTemplate_CLI_A_Record = Common.DataTableFromText("SELECT RecordID FROM [Record]  WHERE  IsActive=1 AND TableID=" + sUser_AttachmentTableID
                                + " AND V010='" + drTemplate_CLItemRecord[0].ToString() + "'");
                            if (dtTemplate_CLI_A_Record != null && dtTemplate_CLI_A_Record.Rows.Count > 0)
                            {
                                //we have checklist item attachment
                                foreach (DataRow drTemplate_CLI_A_Record in dtTemplate_CLI_A_Record.Rows)
                                {
                                    Record rU_CLI_A_Record = new Record();
                                    Record rT_CLI_A_Record = RecordManager.ets_Record_Detail_Full(int.Parse(drTemplate_CLI_A_Record[0].ToString()), null, false);
                                    if (rU_CLI_A_Record != null)
                                    {
                                        rU_CLI_A_Record.RecordID = null;
                                        rU_CLI_A_Record.TableID = int.Parse(sUser_AttachmentTableID);
                                        rU_CLI_A_Record.EnteredBy = user.UserID;
                                        rU_CLI_A_Record.V001 = Guid.NewGuid().ToString();
                                        rU_CLI_A_Record.DateTimeRecorded = DateTime.Now;
                                        rU_CLI_A_Record.V009 = rU_CLIRecord.RecordID.ToString();

                                        rU_CLI_A_Record.V002 = rT_CLI_A_Record.V002;
                                        rU_CLI_A_Record.V003 = rT_CLI_A_Record.V003;
                                        rU_CLI_A_Record.V004 = rT_CLI_A_Record.V004;
                                        rU_CLI_A_Record.V007 = rT_CLI_A_Record.V007;

                                        rU_CLI_A_Record.RecordID = RecordManager.ets_Record_Insert(rU_CLI_A_Record);


                                    }
                                }
                            }


                            ////check comment(checklist item)
                            //DataTable dtTemplate_CLI_Comment_Record = Common.DataTableFromText("SELECT RecordID FROM [Record]  WHERE  IsActive=1 AND TableID=" +
                            //    sTemplate_CommentTableID + " AND V003='" + drTemplate_CLItemRecord[0].ToString() + "'");
                            //if (dtTemplate_CLI_Comment_Record != null && dtTemplate_CLI_Comment_Record.Rows.Count > 0)
                            //{
                            //    //we have checklist item comments
                            //    foreach (DataRow drTemplate_CLI_Comment_Record in dtTemplate_CLI_Comment_Record.Rows)
                            //    {
                            //        Record rR_CLI_Comment_Record = RecordManager.ets_Record_Detail_Full(int.Parse(drTemplate_CLI_Comment_Record[0].ToString()), null, false);
                            //        if (rR_CLI_Comment_Record != null)
                            //        {
                            //            rR_CLI_Comment_Record.RecordID = null;
                            //            rR_CLI_Comment_Record.TableID = int.Parse(sUser_CommentTableID);
                            //            rR_CLI_Comment_Record.EnteredBy = theAccountInvite.InvitedUserID;
                            //            rR_CLI_Comment_Record.V006 = Guid.NewGuid().ToString();
                            //            rR_CLI_Comment_Record.DateTimeRecorded = DateTime.Now;
                            //            rR_CLI_Comment_Record.V004 = "";//?? somehow Template record has Template contrator here
                            //            rR_CLI_Comment_Record.V003 = rU_CLIRecord.RecordID.ToString();
                            //            rR_CLI_Comment_Record.RecordID = RecordManager.ets_Record_Insert(rR_CLI_Comment_Record);

                            //        }
                            //    }
                            //}
                        }
                    }
                }


                return Request.CreateResponse(HttpStatusCode.OK, rUser_CheckList.RecordID.ToString());
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied");
        }







    }








    [Route("api/accounts/{accountId:int}/tables/{tableId:int}/records/{recordId:int}")]
    [HttpPut]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage UpdateRecord(int accountId, int tableId, int recordId, [FromBody]Dictionary<string, string> fieldValues)
    {
        var username = Thread.CurrentPrincipal.Identity.Name;
        User user = SecurityManager.User_ByEmail(username);
        bool hasPermission = user != null && user.IsActive.HasValue && user.IsActive.Value;
        TableRights rights = user != null ? SecurityManager.GetUserRightsOnTable(user.UserID.Value, tableId) : null;
        hasPermission = hasPermission && rights != null && (rights.CanUpdate == "Y" || rights.CanUpdate == "O");

        if (hasPermission == false)
        {
            string roletype = SecurityManager.GetUserRoleTypeID((int)user.UserID, accountId);
            if (roletype == "2")
            {
                hasPermission = true;
            }
        }


        if (hasPermission)
        {
            try
            {
                int userId = SecurityManager.User_ByEmail(username).UserID.Value;
                int rowEffected = 0;
                using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("ets_Record_Update", conn);
                    cmd.CommandType = CommandType.StoredProcedure;                    
                    cmd.Parameters.AddWithValue("@TableID", tableId);
                    cmd.Parameters.AddWithValue("@RecordID", recordId);
                    cmd.Parameters.AddWithValue("@DateTimeRecorded", DateTime.Now);
                    cmd.Parameters.AddWithValue("@EnteredBy", userId);
                    
                    foreach (string fieldName in fieldValues.Keys)
                    {
                        cmd.Parameters.AddWithValue(String.Format("@{0}", fieldName), fieldValues[fieldName]);
                    }
                    conn.Open();
                    rowEffected = cmd.ExecuteNonQuery();
                    conn.Close();
                }
                return Request.CreateResponse(HttpStatusCode.OK, "Record was updated");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied");
        }
    }

    [Route("api/accounts/{accountId:int}/tables/{tableId:int}/records/{recordId:int}")]
    [HttpDelete]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage DeleteRecord(int accountId, int tableId, int recordId)
    {
        var username = Thread.CurrentPrincipal.Identity.Name;
        User user = SecurityManager.User_ByEmail(username);
        bool hasPermission = user != null && user.IsActive.HasValue && user.IsActive.Value;
        TableRights rights = user != null ? SecurityManager.GetUserRightsOnTable(user.UserID.Value, tableId) : null;
        hasPermission = hasPermission && rights != null && (rights.CanDelete == "Y" || rights.CanDelete == "O");

        if (hasPermission == false)
        {
            string roletype = SecurityManager.GetUserRoleTypeID((int)user.UserID, accountId);
            if (roletype == "2")
            {
                hasPermission = true;
            }
        }

        if (hasPermission)
        {
            try
            {
                DataSet ds = new DataSet();
                using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("ets_Record_Delete", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@sRecordIDs", recordId.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@nUserID", user.UserID.Value.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@nTableID", tableId.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@bDeleteAll", false));
                    cmd.Parameters.Add(new SqlParameter("@bPermanentDelete", false));
                    cmd.Parameters.Add(new SqlParameter("@sDeleteReason", "eContractor API delete record"));
                    

                    cmd.CommandTimeout = 0;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    

                    conn.Open();
                    try
                    {
                        da.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "Record Delete", ex.Message, ex.StackTrace, DateTime.Now, "");
                        SystemData.ErrorLog_Insert(theErrorLog);

                    }
                    conn.Close();
                    conn.Dispose();
                }
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Record was deleted");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "Record not found");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied");
        }
    }



    [Route("api/accounts/{accountId:int}/tables/{tableId:int}/completetaskbyreceiver/{receivertaskrecordId:int}")]
    [HttpPut]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage completetaskbyreceiver(int accountId, int tableId, int receivertaskrecordId)
    {
        var username = Thread.CurrentPrincipal.Identity.Name;
        User user = SecurityManager.User_ByEmail(username);
        bool hasPermission = user != null && user.IsActive.HasValue && user.IsActive.Value;
        TableRights rights = user != null ? SecurityManager.GetUserRightsOnTable(user.UserID.Value, tableId) : null;
        hasPermission = hasPermission && rights != null && (rights.CanUpdate == "Y" || rights.CanUpdate == "O");
      
        if (hasPermission == false)
        {
            string roletype = SecurityManager.GetUserRoleTypeID((int)user.UserID, accountId);
            if (roletype == "2")
            {
                hasPermission = true;
            }
        }


        if (hasPermission)
        {
            try
            {
                
                Record theReceiverTask = RecordManager.ets_Record_Detail_Full(receivertaskrecordId, null, false);
                if(theReceiverTask!=null)
                {
                    theReceiverTask.V011 = "1";
                    theReceiverTask.V033 = "Yes";
                    theReceiverTask.V023 = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                    RecordManager.ets_Record_Update(theReceiverTask,true);

                    //find the original task and update it
                    if(!string.IsNullOrEmpty(theReceiverTask.V037))
                    {
                        Record theOriginalTask = RecordManager.ets_Record_Detail_Full(int.Parse(theReceiverTask.V037), null, false);
                        if(theOriginalTask!=null)
                        {
                            theOriginalTask.V011 = theReceiverTask.V011;
                            theOriginalTask.V033 = theReceiverTask.V033;
                            theOriginalTask.V023 = theReceiverTask.V023;
                            RecordManager.ets_Record_Update(theOriginalTask, true);
                        }

                    }


                }

                return Request.CreateResponse(HttpStatusCode.OK, "Record was updated");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied");
        }
    }


    [Route("api/accounts/{accountId:int}/tables/{tableId:int}/completetaskbysender/{sendertaskrecordId:int}")]
    [HttpPut]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage completetaskbysender(int accountId, int tableId, int sendertaskrecordId)
    {
        var username = Thread.CurrentPrincipal.Identity.Name;
        User user = SecurityManager.User_ByEmail(username);
        bool hasPermission = user != null && user.IsActive.HasValue && user.IsActive.Value;
        TableRights rights = user != null ? SecurityManager.GetUserRightsOnTable(user.UserID.Value, tableId) : null;
        hasPermission = hasPermission && rights != null && (rights.CanUpdate == "Y" || rights.CanUpdate == "O");

        if (hasPermission == false)
        {
            string roletype = SecurityManager.GetUserRoleTypeID((int)user.UserID, accountId);
            if (roletype == "2")
            {
                hasPermission = true;
            }
        }


        if (hasPermission)
        {
            try
            {
                var query = Request.GetQueryNameValuePairs();
                string strReceiverEmail = query.FirstOrDefault(q => q.Key == "ReceiverEmail").Value;

                string sReceiverAccountID = Common.GetValueFromSQL(@"SELECT A.AccountID
                                                        FROM dbo.Account A
                                                        JOIN [UserRole]  UR ON UR.AccountID = A.AccountID 
                                                        JOIN [User] U ON U.UserID = UR.UserID 
                                                        WHERE U.Email='"+ strReceiverEmail + "'");

                Record theSenderTask = RecordManager.ets_Record_Detail_Full(sendertaskrecordId, null, false);
                if (theSenderTask != null)
                {
                    theSenderTask.V011 = "1";
                    theSenderTask.V033 = "Yes";
                    theSenderTask.V023 = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                    RecordManager.ets_Record_Update(theSenderTask, true);

                    //find the receiver task and update it
                                        

                    string sReceiverTaskID = Common.GetValueFromSQL(@"SELECT R.RecordID FROM [Record] R JOIN [Table] T ON R.TableID=T.TableID WHERE
                        T.AccountID = "+ sReceiverAccountID + " AND R.v037 = '"+ theSenderTask.RecordID + "'");
                    if (!string.IsNullOrEmpty(sReceiverTaskID))
                    {
                        Record theReceiverTask = RecordManager.ets_Record_Detail_Full(int.Parse(sReceiverTaskID), null, false);
                        if (theReceiverTask != null)
                        {
                            theReceiverTask.V011 = theSenderTask.V011;
                            theReceiverTask.V033 = theSenderTask.V033;
                            theReceiverTask.V023 = theSenderTask.V023;
                            RecordManager.ets_Record_Update(theReceiverTask, true);
                        }
                    }                      
                    
                }

                return Request.CreateResponse(HttpStatusCode.OK, "Record was updated");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied");
        }
    }


    #region Helper methods
    private string GetNullableString(string input)
    {
        return String.IsNullOrEmpty(input) ? null : input;
    }

    private int? GetNullableInt(string input)
    {
        int value;
        if (!String.IsNullOrEmpty(input))
        {
            if (!Int32.TryParse(input, out value))
            {
                return (int?)null;
            }
            else
            {
                return value;
            }
        }
        else
        {
            return (int?)null;
        }
    }

    private DateTime? GetNullableDate(string input, string format)
    {
        DateTime value;
        if (!String.IsNullOrEmpty(input))
        {
            if (!DateTime.TryParseExact(input, format, null, System.Globalization.DateTimeStyles.None, out value))
            {
                return (DateTime?)null;
            }
            else
            {
                return value;
            }
        }
        else
        {
            return (DateTime?)null;
        }
    }

    private bool? GetNullableBool(string input)
    {
        bool? value = (bool?)null;
        if (!String.IsNullOrEmpty(input))
        {
            switch (input.ToLower())
            {
                case "0":
                case "false":
                case "no":
                    value = false;
                    break;
                case "1":
                case "true":
                case "yes":
                    value = true;
                    break;
            }
        }
        return value;
    }
    #endregion
}

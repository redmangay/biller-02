﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Text;
using System.Threading;
using System.Security.Principal;

/// <summary>
/// Summary description for BasicAuthenticationAttribute
/// </summary>
public class BasicAuthenticationAttribute: AuthorizationFilterAttribute
{
    public override void OnAuthorization(HttpActionContext actionContext)
    {
        if(actionContext.Request.Headers.Authorization == null)
        {
            actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
        }
        else
        {
            string token = actionContext.Request.Headers.Authorization.Parameter;
            try
            {
                string[] data = Encoding.UTF8.GetString(Convert.FromBase64String(token)).Split(':');
                if (data.Length == 2)
                {
                    string username = data[0];
                    string password = data[1];

                    User user = SecurityManager.User_LoginByEmail(username, password);                    
                    if (user != null && user.IsActive.HasValue && user.IsActive.Value)
                    {

                        int? iAccountID = SecurityManager.GetPrimaryAccountID((int)user.UserID);
                        Account theAccount = SecurityManager.Account_Details((int)iAccountID);

                        if((bool)theAccount.IsActive)
                        {
                            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(username), null);
                        }
                        else
                        {
                            actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.NotAcceptable);
                        }

                       
                    }
                    else
                    {
                        actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                else
                {
                    actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
                }
            }
            catch(FormatException formatEx)
            {
                actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
            }
        }
    }
}
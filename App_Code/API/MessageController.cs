﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using FirebaseNet;
using FirebaseNet.Messaging;
using FCMMessage = FirebaseNet.Messaging.Message;

/// <summary>
/// Summary description for MessageController
/// </summary>
public class MessageController : ApiController
{
    [Route("api/accounts/{accountId:int}/messages")]
    [HttpPost]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage Send(int accountId, [FromBody]MessageSendingRequest msRequest)
    {
        bool validRequest = true;
        string messageBody = msRequest.Body;
        string messageSubject = msRequest.Subject;
        Record record = null;

        if (msRequest != null)
        {
            string[] validDeliveryMethods = new string[] { "S", "E" };   //E is for email, S is for SMS
            string[] validMsgTypes = new string[] { "M", "E", "W" };     //M is a general message; E is for an exceedance message; W is for a warning message
            validRequest = validDeliveryMethods.Contains(msRequest.DeliveryMethod) && validMsgTypes.Contains(msRequest.MessageType) && !String.IsNullOrEmpty(msRequest.Subject);
            if (validRequest)
            {
                switch (msRequest.DeliveryMethod)
                {
                    case "E":
                        // Other party must be an email address
                        Regex rEmail = new Regex(@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                                        @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$");
                        validRequest = rEmail.IsMatch(msRequest.OtherParty);

                        break;
                    case "S":
                        // Other party must be a phone number
                        validRequest = Regex.IsMatch(msRequest.OtherParty, "[0-9]+");
                        break;
                }
            }

            // If still be a valid request, then we check for user permission on Record (if presented)
            if (validRequest && msRequest.RecordId > 0)
            {
                try
                {
                    // Get record data
                    record = RecordManager.ets_Record_Detail_Full(msRequest.RecordId, null, false);
                    if (record != null)
                    {
                        // Get message body based on record data
                        //var username = Thread.CurrentPrincipal.Identity.Name;
                        //User user = SecurityManager.User_ByEmail(username);
                        //bool hasPermission = user != null && user.IsActive.HasValue && user.IsActive.Value;
                        //TableRights rights = user != null ? SecurityManager.GetUserRightsOnTable(user.UserID.Value, record.TableID.Value) : null;
                        //hasPermission = hasPermission && rights != null && (rights.CanSelect == "Y");

                        bool hasPermission = true;
                        if (hasPermission)
                        {
                            if (msRequest.ContentId > 0)
                            {
                                // If Content ID is presented then we get template from database
                                messageBody = GetMessageBody(msRequest.ContentId, msRequest.RecordId, out messageSubject);
                                if (string.IsNullOrEmpty(messageSubject)) messageSubject = msRequest.Subject;
                                if (string.IsNullOrEmpty(messageBody)) messageBody = msRequest.Body;
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied. You don't have permission to access to the specified record");
                        }
                    }
                    else
                    {
                        // If record could not be found, then return bad request message                    
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Record could not be found");
                    }
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error Email Data Population: " + ex.Message);
                }
            }

            validRequest = validRequest && !String.IsNullOrEmpty(messageBody) && (msRequest.DeliveryMethod == "E" || messageBody.Length <= 256);            
        }
        else
        {
            validRequest = false;
        }
        //Generate PDF file here and add to attachment

        string filesPhysicalRoot = SystemData.SystemOption_ValueByKey_Account("FilesPhisicalPath", null, null);
        string container = string.Empty;
        string pdfFileName = string.Empty;
        string htmlFileName = string.Empty;
       
        MailMessage msg = new MailMessage();
        if (msRequest.Invoice != null)
        {
            try
            {
                container = "InvoiceFiles";
                pdfFileName = String.Format(@"{0}\UserFiles\{1}\Invoice-{2}.pdf", filesPhysicalRoot, container, DateTime.Now.ToString("yyyyMMddhhmmss"));
                htmlFileName = String.Format(@"{0}\UserFiles\{1}\InvoiceTemplate.htm", filesPhysicalRoot, container);

                CreatePDF(msRequest, htmlFileName, pdfFileName);
                msg.Attachments.Add(new Attachment(pdfFileName));
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error PDF Creation: " + ex.Message);
            }
            
        }

        if (validRequest)
        {
            
            // Send message
            Message theMessage = new Message(null, record.RecordID, record.TableID, accountId, DateTime.Now, msRequest.MessageType, msRequest.DeliveryMethod, null, msRequest.OtherParty, messageSubject, messageBody, null, "");
            string errorMessage = "";
            DBGurus.SendEmail(msRequest.MessageType,
                msRequest.DeliveryMethod == "E" ? true : (bool?)null,
                msRequest.DeliveryMethod == "S" ? true : (bool?)null,
                messageSubject,
                messageBody,
                "", // sFrom               
                msRequest.OtherParty,   // sTo
                "", // sCC
                "", // sBCC
                msg.Attachments, // attFiles
                theMessage,
                out errorMessage
                );

            return String.IsNullOrEmpty(errorMessage) ? Request.CreateResponse(HttpStatusCode.OK, "Message was sent") : Request.CreateResponse(HttpStatusCode.InternalServerError, errorMessage);
        }
        else
        {
            // Return error response
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid request");
        }
    }


    [Route("api/accounts/{accountId:int}/sendquote")]
    [HttpPost]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage SendQuote(int accountId, [FromBody]MessageSendingRequest msRequest)
    {

        try
        {
            bool validRequest = true;
            string messageBody = msRequest.Body;
            string messageSubject = msRequest.Subject;
            Record record = null;

            if (msRequest != null)
            {
                string[] validDeliveryMethods = new string[] { "S", "E" };   //E is for email, S is for SMS
                string[] validMsgTypes = new string[] { "M", "E", "W" };     //M is a general message; E is for an exceedance message; W is for a warning message

                validRequest = validDeliveryMethods.Contains(msRequest.DeliveryMethod) && validMsgTypes.Contains(msRequest.MessageType) && !String.IsNullOrEmpty(msRequest.Subject);
                if (validRequest)
                {
                    switch (msRequest.DeliveryMethod)
                    {
                        case "E":
                            // Other party must be an email address
                            Regex rEmail = new Regex(@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                                            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$");
                            msRequest.OtherParty = msRequest.OtherParty.Trim();
                            validRequest = rEmail.IsMatch(msRequest.OtherParty);

                            break;
                        case "S":
                            // Other party must be a phone number
                            validRequest = Regex.IsMatch(msRequest.OtherParty, "[0-9]+");
                            break;
                    }
                }

                // If still be a valid request, then we check for user permission on Record (if presented)
                if (validRequest && msRequest.RecordId > 0)
                {
                    try
                    {
                        // Get record data
                        record = RecordManager.ets_Record_Detail_Full(msRequest.RecordId, null, false);
                        if (record != null)
                        {
                            bool hasPermission = true;
                            if (hasPermission)
                            {
                                if (msRequest.ContentId > 0)
                                {
                                    // If Content ID is presented then we get template from database
                                    messageBody = GetMessageBody(msRequest.ContentId, msRequest.RecordId, out messageSubject);
                                    string strAcceptURL = TheDatabase.SiteRoot() + "/QuoteAccept.aspx?ID=" + Cryptography.Encrypt(accountId.ToString()) + "&RecordID=" + Cryptography.Encrypt(record.RecordID.ToString())
                                                         + "&Email=" + Cryptography.Encrypt(msRequest.OtherParty) + "&senttime=" + Cryptography.Encrypt(DateTime.Now.ToString());
                                    messageBody = messageBody.Replace("[Accept]", strAcceptURL + "&status=accept");
                                    messageBody = messageBody.Replace("[Decline]", strAcceptURL + "&status=decline");
                                    if (string.IsNullOrEmpty(messageSubject)) messageSubject = msRequest.Subject;
                                    if (string.IsNullOrEmpty(messageBody)) messageBody = msRequest.Body;
                                }
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied. You don't have permission to access to the specified record");
                            }
                        }
                        else
                        {
                            // If record could not be found, then return bad request message                    
                            return Request.CreateResponse(HttpStatusCode.BadRequest, "Record could not be found");
                        }
                    }
                    catch (Exception ex)
                    {
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error Email Data Population: " + ex.Message);
                    }
                }

                validRequest = validRequest && !String.IsNullOrEmpty(messageBody) && (msRequest.DeliveryMethod == "E" || messageBody.Length <= 256);
            }
            else
            {
                validRequest = false;
            }
            //Generate PDF file here and add to attachment

            string filesPhysicalRoot = SystemData.SystemOption_ValueByKey_Account("FilesPhisicalPath", null, null);
            string container = string.Empty;
            string pdfFileName = string.Empty;
            string htmlFileName = string.Empty;
            container = "QuoteFiles";
            pdfFileName = String.Format(@"{0}\UserFiles\{1}\Quote-{2}.pdf", filesPhysicalRoot, container, DateTime.Now.ToString("yyyyMMddhhmmss"));
            htmlFileName = String.Format(@"{0}\UserFiles\{1}\QuoteTemplate.htm", filesPhysicalRoot, container);

            MailMessage msg = new MailMessage();
            if (msRequest.Invoice != null)
            {
                try
                {
                    CreatePDF(msRequest, htmlFileName, pdfFileName);
                    msg.Attachments.Add(new Attachment(pdfFileName));
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error PDF Creation: " + ex.Message);
                }
            }

            if (validRequest)
            {

                // Send message
                Message theMessage = new Message(null, record.RecordID, record.TableID, accountId, DateTime.Now, msRequest.MessageType, msRequest.DeliveryMethod, null, msRequest.OtherParty, messageSubject, messageBody, null, "");
                string errorMessage = "";
                DBGurus.SendEmail(msRequest.MessageType,
                    msRequest.DeliveryMethod == "E" ? true : (bool?)null,
                    msRequest.DeliveryMethod == "S" ? true : (bool?)null,
                    messageSubject,
                    messageBody,
                    "", // sFrom               
                    msRequest.OtherParty,   // sTo
                    "", // sCC
                    "", // sBCC
                    msg.Attachments, // attFiles
                    theMessage,
                    out errorMessage
                    );

                return String.IsNullOrEmpty(errorMessage) ? Request.CreateResponse(HttpStatusCode.OK, "Message was sent") : Request.CreateResponse(HttpStatusCode.InternalServerError, errorMessage);
            }
            else
            {
                // Return error response
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid request");
            }
        }
        catch(Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "OnTask - SendQuote API", ex.Message, ex.StackTrace, DateTime.Now, "");
            SystemData.ErrorLog_Insert(theErrorLog);
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid request");
        }


        
    }


    [Route("api/accounts/{accountId:int}/sendcomment")]
    [HttpPost]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public  HttpResponseMessage SenComment(int accountId, [FromBody]FCMMessage msRequest)
    {
        if(msRequest == null) return Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid request");
        try
        {
            string key = SystemData.SystemOption_ValueByKey_Account("FCMServerKey", null, null);
            FCMClient client = new FCMClient(key);
            var result = client.SendMessageAsync(msRequest);
        }
        catch(Exception ex)
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error: " + ex.Message);
        }
        return Request.CreateResponse(HttpStatusCode.OK, "OK");
    }

    #region Helper methods
    public string GetMessageBody(int contentId, int recordId, out string subject)
    {
        string body = "";
        subject = "";
        string templateBody = "";
        string templateSubject = "";
        Dictionary<string, object> mergeData = new Dictionary<string, object>();
        using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            SqlCommand cmd = new SqlCommand("ets_GetContentTemplateAndRecordData", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ContentId", contentId);
            cmd.Parameters.AddWithValue("@RecordId", recordId);
            conn.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                {
                    templateBody = Convert.ToString(reader[0]);
                    templateSubject = Convert.ToString(reader[1]);

                }
                if (reader.NextResult())
                {
                    if (reader.Read())
                    {
                        int fieldCount = reader.FieldCount;
                        for (int i = 0; i < fieldCount; i++)
                        {
                            mergeData.Add(reader.GetName(i), reader[i]);
                        }
                    }
                }
            }
            conn.Close();
        }

        if (!String.IsNullOrEmpty(templateBody))
        {
            if (mergeData.Count > 0)
            {
                body = MergeMessageBody(templateBody, mergeData);
            }
            else
            {
                body = templateBody;
            }
        }
        if (!String.IsNullOrEmpty(templateSubject))
        {
            if (mergeData.Count > 0)
            {
                subject = MergeMessageBody(templateSubject, mergeData);
            }
            else
            {
                subject = templateSubject;
            }
        }
        return body;
    }

    public string MergeMessageBody(string template, Dictionary<string, object> data)
    {
        string body = template;
        string key = "";
        string value = "";
        Regex rField = new Regex(@"\[([^\]]+)\]");
        MatchCollection mcField = rField.Matches(template);
        foreach (Match mField in mcField)
        {
            // Get field name
            key = mField.Groups[1].Value;

            // Get suitable data from dictionary
            if (data.ContainsKey(key))
            {
                object piece = data[key] ?? "";

                if (piece is DateTime)
                {
                    value = ((DateTime)piece).ToString("dd MMM yyyy");
                }
                else if (piece is bool)
                {
                    value = (bool)piece ? "Yes" : "No";
                }
                else
                {
                    value = Convert.ToString(piece);
                }

                // Replace merge field with real data
                body = body.Replace(mField.Value, value);
            }
        }
        return body;
    }

    protected void CreatePDF(MessageSendingRequest obj, string htmlFileName, string pdfFileName)
    {
        try
        {
            string strHtml = string.Empty;
            //reading html code from html file
            FileStream fsHTMLDocument = new FileStream(htmlFileName, FileMode.Open, FileAccess.Read);
            StreamReader srHTMLDocument = new StreamReader(fsHTMLDocument);
            strHtml = srHTMLDocument.ReadToEnd();
            srHTMLDocument.Close();
            strHtml = strHtml.Replace("\r\n", "");
            strHtml = strHtml.Replace("\0", "");
            //REPLACE template with data
            strHtml = strHtml.Replace("{InvoiceNo}", obj.Invoice.InvoiceNumber);
            strHtml = strHtml.Replace("{Date}", obj.Invoice.InvoiceDate);
            strHtml = strHtml.Replace("{To}", obj.Invoice.InvoiceTo);
            strHtml = strHtml.Replace("{From}", obj.Invoice.InvoiceFrom);
            strHtml = strHtml.Replace("{ABN}", obj.Invoice.ABN);
            strHtml = strHtml.Replace("{InvoiceFor}", obj.Invoice.InvoiceFor);
            strHtml = strHtml.Replace("{InvoiceNote}", obj.Invoice.InvoiceNote);
            strHtml = strHtml.Replace("{InvoiceFooter}", obj.Invoice.InvoiceFooter);
            //Add Item
            string strItems = string.Empty;
            double dNet = 0;
            double dTax = 0;
            double dGross = 0;
            if (obj.Invoice.Items.Count > 0)
            {
                foreach (InvoiceItem item in obj.Invoice.Items)
                {
                    double iNet = 0;
                    double iTax = 0;
                    double iGross = 0;
                    if (double.TryParse(item.Net, out iNet)) dNet += iNet;
                    if (double.TryParse(item.Tax, out iTax)) dTax += iTax;
                    if (double.TryParse(item.Gross, out iGross)) dGross += iGross;
                    strItems += string.Format(@"<tr style='color:#0071BC'>
                                                <td style='text-align:left'>{0}</td>
                                                <td style='text-align:right'>{1}</td>
                                                <td style='text-align:right'>{2}</td>
                                                <td style='text-align:right'>{3}</td>
                                                </tr>",
                                                item.Item,
                                                iNet.ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-au")),
                                                iTax.ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-au")),
                                                iGross.ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-au")));
                }
            }
            strItems += string.Format(@"<tr>
                                        <td style='text-align:left'>{0}</td>
                                        <td style='text-align:right'>{1}</td>
                                        <td style='text-align:right'>{2}</td>
                                        <td style='text-align:right'>{3}</td>
                                        </tr>",
                                        "Total",
                                        dNet.ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-au")),
                                        dTax.ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-au")),
                                        dGross.ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-au")));
            strHtml = strHtml.Replace("{Items}", strItems);

            CreatePDFFromHTMLFile(strHtml, pdfFileName);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void CreatePDFFromHTMLFile(string HtmlStream, string FileName)
    {
        try
        {
            string TargetFile = FileName;
            HtmlToPdfBuilder builder = new HtmlToPdfBuilder(iTextSharp.text.PageSize.A4);
            HtmlPdfPage first = builder.AddPage();
            first.AppendHtml(HtmlStream);

            byte[] file = builder.RenderPdf();
            File.WriteAllBytes(TargetFile.ToString(), file);

            iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(TargetFile.ToString());

            reader.Close();


        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}
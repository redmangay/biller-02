﻿//RP Added Ticket 3605
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Stripe;
//using Newtonsoft.Json;
using DocGen.DAL;

/// <summary>
/// Summary description for BillingAPI
/// </summary>
/// 

namespace DBGPayment.DBGPaypal
{

    public class ShippingAmount : JSONField
    {
        public string currency_code { get; set; }
        public string value { get; set; }
    }

    public class Name : JSONField
    {
        public string given_name { get; set; }
        public string surname { get; set; }
        public string full_name { get; set; }
    }


    public class Address : JSONField
    {
        public string address_line_1 { get; set; }
        public string address_line_2 { get; set; }
        public string admin_area_2 { get; set; }
        public string admin_area_1 { get; set; }
        public string postal_code { get; set; }
        public string country_code { get; set; }
    }

    public class ShippingAddress : JSONField
    {
        public Name name { get; set; }
        public Address address { get; set; }
    }

    public class Subscriber : JSONField
    {
        public Name name { get; set; }
        public string email_address { get; set; }
        public ShippingAddress shipping_address { get; set; }
    }

    public class PaymentMethod : JSONField
    {
        public string payer_selected { get; set; }
        public string payee_preferred { get; set; }
    }

    public class ApplicationContext : JSONField
    {
        public string brand_name { get; set; }
        public string locale { get; set; }
        public string shipping_preference { get; set; }
        public string user_action { get; set; }
        public PaymentMethod payment_method { get; set; }
        public string return_url { get; set; }
        public string cancel_url { get; set; }
    }

    public class Subscription : JSONField
    {
        public string id { get; set; }
        public string status { get; set; }
        public DateTime? status_update_time { get; set; }
        public string plan_id { get; set; }
        //public DateTime? start_time { get; set; }
        public string start_time { get; set; }
        public string quantity { get; set; }
        public ShippingAmount shipping_amount { get; set; }
        public Subscriber subscriber { get; set; }
        public bool auto_renewal { get; set; }
        public ApplicationContext application_context { get; set; }
        public DateTime? create_time { get; set; }
        public List<Link> links { get; set; }
    }


    public class Link : JSONField
    {
        public string href { get; set; }
        public string rel { get; set; }
        public string method { get; set; }
    }

    public class Product : JSONField
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string type { get; set; }
        public string category { get; set; }
        public string image_url { get; set; }
        public string home_url { get; set; }
        public DateTime? create_time { get; set; }
        public DateTime? update_time { get; set; }
        public List<Link> links { get; set; }
    }
    public class PlanList : JSONField
    {
        public string total_items { get; set; }
        public string total_pages { get; set; }
        public List<Plan> plans { get; set; }
        public List<Link> links { get; set; }
    }
    public class ProductList : JSONField
    {
        public List<Product> products { get; set; }
        public int total_items { get; set; }
        public int total_pages { get; set; }
        public List<Link> links { get; set; }
    }

    public class Frequency : JSONField
    {
        public string interval_unit { get; set; }
        public int interval_count { get; set; }
    }

    public class FixedPrice : JSONField
    {
        public string value { get; set; }
        public string currency_code { get; set; }
    }

    public class PricingScheme : JSONField
    {
        public FixedPrice fixed_price { get; set; }
    }

    public class BillingCycle : JSONField
    {
        public Frequency frequency { get; set; }
        public string tenure_type { get; set; }
        public int sequence { get; set; }
        public int total_cycles { get; set; }
        public PricingScheme pricing_scheme { get; set; }
    }

    public class SetupFee : JSONField
    {
        public string value { get; set; }
        public string currency_code { get; set; }
    }

    public class PaymentPreferences : JSONField
    {
        public string service_type { get; set; }
        public bool auto_bill_outstanding { get; set; }
        public SetupFee setup_fee { get; set; }
        public string setup_fee_failure_action { get; set; }
        public int payment_failure_threshold { get; set; }
    }

    public class Taxes : JSONField
    {
        public string percentage { get; set; }
        public bool inclusive { get; set; }
    }


    public class Plan : JSONField
    {
        public string id { get; set; }
        public string product_id { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public string description { get; set; }
        public List<BillingCycle> billing_cycles { get; set; }
        public PaymentPreferences payment_preferences { get; set; }
        public Taxes taxes { get; set; }
        public bool quantity_supported { get; set; }
        public DateTime? create_time { get; set; }
        public DateTime? update_time { get; set; }
        public List<Link> links { get; set; }
    }


}



public class BillingAPI
{

    public static string PaypalAuthInfo()
    {
        return Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(PaypalClientID() + ":" + PaypalSecret())); 
    }
    public static string PaypalClientID()
    {
        string sPaypalClientID = "";

        string strPayPalLive = SystemData.SystemOption_ValueByKey_Account("PayPalLive", null, null);

        if (bool.Parse(strPayPalLive))
        {
            sPaypalClientID = SystemData.SystemOption_ValueByKey_Account("OnTask Paypal Client ID", null, null);
        }
        else
        {
            sPaypalClientID = SystemData.SystemOption_ValueByKey_Account("OnTask Paypal Sandbox Client ID", null, null);
        }

        return sPaypalClientID;
    }


    public static string PaypalSecret()
    {
        string sPaypalSecret = "";

        string strPayPalLive = SystemData.SystemOption_ValueByKey_Account("PayPalLive", null, null);

        if (bool.Parse(strPayPalLive))
        {
            sPaypalSecret = SystemData.SystemOption_ValueByKey_Account("OnTask Paypal Secret", null, null);
        }
        else
        {
            sPaypalSecret = SystemData.SystemOption_ValueByKey_Account("OnTask Paypal Sandbox Secret", null, null);
        }

        return sPaypalSecret;
    }


    public static string BillingIDbySubscriptionID(string sSubscriptionID)
    {
        return Common.GetValueFromSQL("SELECT BillingID FROM [Billing] WHERE SubscriptionID='"+ sSubscriptionID + "' ORDER BY BillingID DESC");
    }
    public static string PaypalYearlyPlanID()
    {
        string sPlanID = "";

        string strPayPalLive = SystemData.SystemOption_ValueByKey_Account("PayPalLive", null, null);

        if (bool.Parse(strPayPalLive))
        {
            sPlanID = SystemData.SystemOption_ValueByKey_Account("OnTask Yearly Paypal PlanID", null, null);
        }
        else
        {
            sPlanID = SystemData.SystemOption_ValueByKey_Account("OnTask Yearly Sandbox Paypal PlanID", null, null);
        }

        return sPlanID;
    }

    public static string LatestSubscriptionID(int iAccountID)
    {
        return Common.GetValueFromSQL("SELECT SubscriptionID FROM [Billing] WHERE AccountID="+iAccountID.ToString()+"  ORDER BY BillingID DESC");
    }


    public static string PaypalMonthlyPlanID()
    {
        string sPlanID = "";

        string strPayPalLive = SystemData.SystemOption_ValueByKey_Account("PayPalLive", null, null);

        if (bool.Parse(strPayPalLive))
        {
            sPlanID = SystemData.SystemOption_ValueByKey_Account("OnTask Monthly Paypal PlanID", null, null);
        }
        else
        {
            sPlanID = SystemData.SystemOption_ValueByKey_Account("OnTask Monthly Sandbox Paypal PlanID", null, null);
        }

        return sPlanID;
    }


    public static string PaypalProductID()
    {
        string sProductID = "";

        string strPayPalLive = SystemData.SystemOption_ValueByKey_Account("PayPalLive", null, null);

        if (bool.Parse(strPayPalLive))
        {
            sProductID= SystemData.SystemOption_ValueByKey_Account("OnTask Paypal Product ID", null, null);
        }
        else
        {
            sProductID = SystemData.SystemOption_ValueByKey_Account("OnTask Sandbox Paypal Product ID", null, null);
        }

        return sProductID;
    }
    public static string PaypalMonthlyInterval()
    {
        string sInterval = "DAY";

        string strPayPalLive = SystemData.SystemOption_ValueByKey_Account("PayPalLive", null, null);

        if (bool.Parse(strPayPalLive))
        {
            sInterval = "MONTH";
        }        

        return sInterval;
    }

    public static string PaypalYearlyInterval()
    {
        string sInterval = "DAY";

        string strPayPalLive = SystemData.SystemOption_ValueByKey_Account("PayPalLive", null, null);

        if (bool.Parse(strPayPalLive))
        {
            sInterval = "YEAR";
        }

        return sInterval;
    }


    public static string StripeMonthlyPlanID()
    {
        return SystemData.SystemOption_ValueByKey_Account("OnTask Monthly Stripe PlanID", null, null);
    }
    public static string StripeYearlyPlanID()
    {
        return SystemData.SystemOption_ValueByKey_Account("OnTask Yearly Stripe PlanID", null, null);
    }


    public static string PaypalAccountPlanID(int iAccountID)
    {
        string sPlanID = "";

        if (HttpContext.Current.Session["IsYearlySubscription"] != null)
        {
            if (HttpContext.Current.Session["IsYearlySubscription"].ToString().ToLower() == "true")
            {
                return PaypalYearlyPlanID();
            }
            else
            {
                return PaypalMonthlyPlanID();
            }
        }

        string sValue = Common.GetValueFromSQL("SELECT TOP 1 IsYearlySubscription FROM [Billing] WHERE AccountID=" + iAccountID.ToString() + " ORDER BY BillingID DESC");
        if (!string.IsNullOrEmpty(sValue) && bool.Parse(sValue) == true)
        {
            sPlanID = PaypalYearlyPlanID();
        }
        else
        {
            sPlanID = PaypalMonthlyPlanID();
        }
        return sPlanID;
    }



    public static string StripeAccountPlanID(int iAccountID)
    {
        string sPlanID = "";

        if(HttpContext.Current.Session["IsYearlySubscription"] !=null)
        {
            if(HttpContext.Current.Session["IsYearlySubscription"].ToString().ToLower() == "true")
            {
                return StripeYearlyPlanID();
            }
            else
            {
                return StripeMonthlyPlanID();
            }
        }

        string sValue = Common.GetValueFromSQL("SELECT TOP 1 IsYearlySubscription FROM [Billing] WHERE AccountID="+iAccountID.ToString()+" ORDER BY BillingID DESC");
        if(!string.IsNullOrEmpty(sValue) && bool.Parse(sValue)==true)
        {
            sPlanID = StripeYearlyPlanID();
        }
        else
        {
            sPlanID=StripeMonthlyPlanID();
        }
        return sPlanID;
    }

    public static bool IsYearlySubscription(int iAccountID)
    {
        bool bYearly = false;
        string sIsYearly = Common.GetValueFromSQL("SELECT TOP 1 IsYearlySubscription FROM [Billing] WHERE AccountID=" + iAccountID.ToString() + " ORDER BY BillingID DESC");

        if (!string.IsNullOrEmpty(sIsYearly) && bool.Parse(sIsYearly))
        {
            bYearly = true;
        }
        return bYearly;
    }
    public static int AmountPerUser(int iAccountID,int? iAccountTypeID,bool? bIsYearly)
    {
        int iValue = 0;
        string sValue = "0";
        if (iAccountTypeID == null)
        {
            sValue = Common.GetValueFromSQL(@" SELECT CostPerMonth FROM AccountType ATY JOIN [Account] A 
                        ON ATY.AccountTypeID=A.AccountTypeID WHERE A.AccountID=" + iAccountID.ToString());
        }
        else
        {
            sValue = Common.GetValueFromSQL("SELECT CostPerMonth FROM AccountType WHERE AccountTypeID=" + iAccountTypeID.ToString());
        }

        if (!string.IsNullOrEmpty(sValue))
        {
            iValue = (int)double.Parse(sValue);
            string sCountry = Common.GetValueFromSQL("SELECT LD.[Value] FROM [Account] A JOIN LookupData LD ON A.CountryID=LD.LookupDataID WHERE AccountID=" + iAccountID.ToString());
            if(string.IsNullOrEmpty(sCountry) || (!string.IsNullOrEmpty(sCountry) && sCountry.Trim().ToLower()== "australia"))
            {
                iValue = iValue + (int)(iValue * 0.1);
            }
        }
        if ((bIsYearly==null && IsYearlySubscription(iAccountID)) || (bIsYearly!=null && bIsYearly==true))
        {
            iValue =(int) (iValue * 12 * 0.8);//20% for yearly
        }
        return iValue;
    }
    public static int UsersPerAccount(int iAccountID)
    {
        int iValue = 0;

        string sValue = Common.GetValueFromSQL("SELECT COUNT(U.UserID) FROM [UserRole] UR JOIN [User] U ON UR.UserID=U.UserID WHERE U.IsActive=1  and UR.IsPrimaryAccount=1 and UR.AccountID=" + iAccountID.ToString());
        if(!string.IsNullOrEmpty(sValue))
        {
            iValue = int.Parse(sValue);
        }
        return iValue;
    }

    public static string GetAccountCountryName(int countryid)
    {       
        string sValue = Common.GetValueFromSQL("SELECT [Value] FROM LookupData WHERE LookupDataID=" + countryid.ToString());
        if (!string.IsNullOrEmpty(sValue))
        {
            sValue = "Australia";
        }
        return sValue;
    }
    public static int AustraliaLookupID()
    {
        int iValue = 0;

        string sValue = Common.GetValueFromSQL("SELECT LookupDataID FROM LookupData WHERE LookupTypeID=-1 AND [Value]='Australia'");
        if (!string.IsNullOrEmpty(sValue))
        {
            iValue = int.Parse(sValue);
        }
        return iValue;
    }
    public static int AmountPerAccountType(int accounttypeid)
    {
        int iValue = 0;
        string sValue = Common.GetValueFromSQL(@" SELECT CostPerMonth FROM AccountType WHERE AccountTypeID=" 
                        + accounttypeid.ToString());
        if (!string.IsNullOrEmpty(sValue))
        {
            iValue = (int)double.Parse(sValue);
        }
        return iValue;
    }
    public class Billing
    {
        public Int32 BillingID { get; set; }
        public Int32 AccountID { get; set; }
        public DateTime? BillingDate { get; set; }
        public Int32 Amount { get; set; }
        public String Currency { get; set; }
        public Int32 NoOfUsers { get; set; }
        public String Remarks { get; set; }
        public String PaymentMethod { get; set; }
        public Boolean PaymentSuccess { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? DateAdded { get; set; }
        public DateTime? DateUpdated { get; set; }
        public bool? IsYearlySubscription { get; set; }
        public string SubscriptionID { get; set; }

        public int? TargetAccountTypeID { get; set; }
    }


    public class BillingDetail
    {
        public int BillingDetailID { get; set; }      
        public Int32 BillingID { get; set; }
        public int UserID { get; set; }
        public DateTime? DateAdded { get; set; }
        public DateTime? DateUpdated { get; set; }
    }



    public static StripeInvoice GetStripeUpcomingBilling(int iAccountID)
    {
        Billing newBilling = new Billing();
        try
        {
            
            string secretKey = SystemData.SystemOption_ValueByKey_Account("StripeSecretKey", null, null);

            StripeCustomerService stripeService = new StripeCustomerService(secretKey);
            var options = new StripeCustomerListOptions();
            options.Limit = 10000;
            var customers = stripeService.List(options);
            //string sTest = "";
            string sSubscriptionID = "";
            string sCustomerID = "";
            foreach (var customer in customers)
            {
                sCustomerID = customer.Id;
                System.Collections.Generic.Dictionary<string, string> cusMetaData = customer.Metadata;
                if (cusMetaData != null && cusMetaData["AccountID"].ToString() == iAccountID.ToString())
                {
                    StripeList<StripeSubscription> lstSubscription = customer.Subscriptions;
                    foreach (StripeSubscription eachSubscription in lstSubscription)
                    {
                        sSubscriptionID = eachSubscription.Id;
                        break;
                    }
                }
                if(sSubscriptionID!="")
                {
                    break;
                }
            }
                             
            var serviceUpInvoice = new StripeInvoiceService(secretKey);
            var options_up_invoice = new StripeUpcomingInvoiceOptions
            {
                SubscriptionId =sSubscriptionID,
            };

            StripeInvoice upcoming = serviceUpInvoice.Upcoming(sCustomerID, options_up_invoice);
            return upcoming;

        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "OnTask BillingAPI.UpdateSubscription", ex.Message, ex.StackTrace, DateTime.Now, "BillingAPI.UpdateSubscription");
            SystemData.ErrorLog_Insert(theErrorLog);
        }
        return null;
    }




    public static void AdjustUsersForAccountType(int iOldAccountTypeID,int iNewAccountTypeID, int iAccountID)
    {
        if (iNewAccountTypeID < iOldAccountTypeID)
        {
            //what to do?
            try
            {

                string sMaxUserAllowed = Common.GetValueFromSQL("SELECT MaxUsers FROM [AccountType] WHERE AccountTypeID=" + iNewAccountTypeID.ToString());
                string sNoOfUsers = Common.GetValueFromSQL(@"SELECT COUNT(U.UserID) FROM [User] U JOIN [UserRole] UR 
                                            ON U.UserID=UR.UserID WHERE AccountID=" + iAccountID.ToString() + @" AND UR.IsPrimaryAccount=1 AND U.IsActive=1");
                int iMaxUserAllowed = int.Parse(sMaxUserAllowed);
                int iNoOfUsers = int.Parse(sNoOfUsers);

                if (iNoOfUsers > iMaxUserAllowed)
                {
                    int iInActiveUser = iNoOfUsers - iMaxUserAllowed;
                    if (iInActiveUser > 0)
                    {
                        Common.ExecuteText(@"UPDATE [User] SET IsActive=0 WHERE UserID IN 
                                    (SELECT top " + iInActiveUser + @" U.UserID FROM [User] U JOIN [UserRole] UR ON U.UserID=UR.UserID
                                    WHERE AccountID=" + iAccountID.ToString() + @"  AND U.IsActive=1 AND UR.IsPrimaryAccount=1 AND IsAccountHolder=0 ORDER BY U.UserID DESC)");
                    }

                }

            }
            catch (Exception ex)
            {

            }

        }
    }


    public static void CancelStripeSubscription(int iAccountID,User theUser)
    {
        try
        {
            string secretKey = SystemData.SystemOption_ValueByKey_Account("StripeSecretKey", null, null);
            Account theAccount = SecurityManager.Account_Details(iAccountID);
            StripeCustomerService stripeService = new StripeCustomerService(secretKey);
            var options = new StripeCustomerListOptions();
            options.Limit = 10000;
            var customers = stripeService.List(options);
            //string sTest = "";
            string sSubscriptionID = "";
            string sCustomerID = "";
            foreach (var customer in customers)
            {
                if (customer.Email.ToLower() == theUser.Email.ToLower())
                {
                    System.Collections.Generic.Dictionary<string, string> cusMetaData = customer.Metadata;
                    if (cusMetaData != null && cusMetaData["AccountID"] == theAccount.AccountID.ToString())
                    {
                        StripeList<StripeSubscription> lstSubscription = customer.Subscriptions;
                        sCustomerID = customer.Id;
                        foreach (StripeSubscription eachSubscription in lstSubscription)
                        {
                            sSubscriptionID = eachSubscription.Id;
                            break;
                        }
                    }
                }
            }
            var serviceSub = new StripeSubscriptionService(secretKey);
            serviceSub.Cancel(sSubscriptionID);
        }
        catch(Exception ex)
        {

        }
        


    }
    public static void UpdateStripeSubscription(int iAccountID,int? iAccountTypeID)
    {
        try
        {
            string secretKey = SystemData.SystemOption_ValueByKey_Account("StripeSecretKey", null, null);

            StripeCustomerService stripeService = new StripeCustomerService(secretKey);
            var options = new StripeCustomerListOptions();
            options.Limit = 10000;
            var customers = stripeService.List(options);
            //string sTest = "";
            string sSubscriptionID = "";
            string sCustomerID = "";
            foreach (var customer in customers)
            {
               
                System.Collections.Generic.Dictionary<string, string> cusMetaData = customer.Metadata;
                if (cusMetaData != null && cusMetaData["AccountID"].ToString() == iAccountID.ToString())
                {
                    sCustomerID = customer.Id;
                    StripeList<StripeSubscription> lstSubscription = customer.Subscriptions;
                    foreach (StripeSubscription eachSubscription in lstSubscription)
                    {
                        sSubscriptionID = eachSubscription.Id;
                        break;
                    }
                }
                if (sSubscriptionID != "")
                {
                    break;
                }
            }


            var service = new StripeSubscriptionService(secretKey);
            StripeSubscription theSubscription = new StripeSubscription();
            int iPlanQuantity = BillingAPI.TotalChargeAmount((int)iAccountID, null, iAccountTypeID, null) * 100;
            if (string.IsNullOrEmpty(sSubscriptionID))
            {
                //this customer must have downgraded account and deleted his subscription so now lets create a new fucking subscription for him/her

                var itemsC = new List<StripeSubscriptionItemOption> {
                    new StripeSubscriptionItemOption {
                        PlanId = StripeAccountPlanID(iAccountID),
                        Quantity=iPlanQuantity

                    }
                };

                var optionsSubCreate = new StripeSubscriptionCreateOptions
                {
                    Items = itemsC,
                    //Source = Request.Form["stripeToken"].ToString(),
                    Metadata = new Dictionary<string, string>()
                        {
                             { "AccountID", iAccountID.ToString() }//,
                        }
                };

                theSubscription = service.Create(sCustomerID, optionsSubCreate);//  "cus_4fdAW5ftNQow1a"
                sSubscriptionID = theSubscription.Id;
                //string sTestString = "";
            }
            else
            {
                theSubscription = service.Get(sSubscriptionID);

                var items = new List<StripeSubscriptionItemUpdateOption> {
                    new StripeSubscriptionItemUpdateOption {
                        Id = theSubscription.Items.Data[0].Id,
                        PlanId=StripeAccountPlanID(iAccountID),
                        Quantity=iPlanQuantity
                    }
                };

                var Sub_options = new StripeSubscriptionUpdateOptions
                {
                    Items = items,
                    Metadata = new Dictionary<string, string>()
                        {
                            { "AccountID", iAccountID.ToString() }//,
                        }
                };
                theSubscription = service.Update(theSubscription.Id, Sub_options);

            }




            //update local billingid by upcoming invoice

            var serviceUpInvoice = new StripeInvoiceService(secretKey);
            var options_up_invoice = new StripeUpcomingInvoiceOptions
            {
                SubscriptionId = theSubscription.Id,
            };

            StripeInvoice upcoming = serviceUpInvoice.Upcoming(theSubscription.CustomerId, options_up_invoice);
            int iAmountDue = upcoming.AmountDue;

            //now get the local 

            string sBillingID =GetActiveBillingID(iAccountID);

            if(sBillingID=="")
            {
                BillingAPI.Billing newBilling = new BillingAPI.Billing();
                newBilling.AccountID = iAccountID;
                newBilling.Amount = iAmountDue/100;        
                newBilling.BillingDate = upcoming.Date;
                newBilling.Currency =GetAccountCurrency(iAccountID); 
                newBilling.NoOfUsers = UsersPerAccount(iAccountID);// dtUsers.Rows.Count;
                newBilling.Remarks = "Updated subscription(" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ").";
                //newBilling.PaymentDate = DateTime.Now;
                newBilling.PaymentMethod = "S";
                //newBilling.PaymentSuccess = true;

                //newBilling.
                Int32 billingid = BillingAPI.CreateBilling(newBilling);
            }
            else
            {
                BillingAPI.Billing editBilling = BillingAPI.GetBilling(int.Parse(sBillingID));
                editBilling.Amount = iAmountDue/100;
                editBilling.BillingDate = upcoming.Date;
                editBilling.PaymentMethod = "S";
                editBilling.NoOfUsers = UsersPerAccount(iAccountID);
                editBilling.Remarks = editBilling.Remarks + "Updated subscription("+DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() +").";
                BillingAPI.UpdateBilling(editBilling);
                
            }

        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "OnTask BillingAPI.UpdateSubscription", ex.Message, ex.StackTrace, DateTime.Now, "BillingAPI.UpdateSubscription");
            SystemData.ErrorLog_Insert(theErrorLog);
        }
    }

    public static string GetActiveBillingID(int iAccountID)
    {
      
        string sBillingID = Common.GetValueFromSQL(@"SELECT BillingID FROM Billing WHERE AccountID = "+iAccountID.ToString()+
            @"  AND (PaymentSuccess=0 OR PaymentSuccess IS NULL) ORDER BY BillingID DESC");

        //if(IsYearlySubscription(iAccountID))
        //{
        //    sBillingID = Common.GetValueFromSQL(@"SELECT BillingID FROM [Billing] WHERE  YEAR(GETDATE())=YEAR(BillingDate) 
        //                         AND  (PaymentSuccess=0 OR PaymentSuccess IS NULL) AND AccountID=" + iAccountID.ToString());
        //    if(string.IsNullOrEmpty(sBillingID))
        //    {
        //        //we should create a new billing
        //    }
        //}
        //else
        //{
        //    sBillingID = Common.GetValueFromSQL(@"SELECT BillingID FROM [Billing] WHERE  YEAR(GETDATE())=YEAR(BillingDate) 
        //                        AND MONTH(GETDATE())=MONTH(BillingDate) AND  (PaymentSuccess=0 OR PaymentSuccess IS NULL) AND AccountID=" + iAccountID.ToString());

        //    if (string.IsNullOrEmpty(sBillingID))
        //    {
        //        //we should create a new billing
        //    }
        //}

        return sBillingID;
    }
    public static string GetAccountPaymentMethod(int iAccountID)
    {
        string sPaymentMethod = Common.GetValueFromSQL("SELECT top 1 PaymentMethod FROM [Billing] WHERE AccountID=" + iAccountID.ToString() + " ORDER BY BillingID desc");
        
        return sPaymentMethod;
    }


    public static void UpdateSubscription(int iAccountID,int? iAccountTypeID)
    {
        string sPaymentMethod = GetAccountPaymentMethod(iAccountID);

        if(sPaymentMethod.ToLower()=="s")
        {
            BillingAPI.UpdateStripeSubscription(iAccountID, iAccountTypeID);
        }
        else if (sPaymentMethod.ToLower() == "p")
        {
            //paypal
        }
        else
        {
            //only update the billing table
        }
    }

    public static int OnTaskBillingUpdate()
    {
        //check list of account 

        DataTable dtAccounts = Common.DataTableFromText(@"SELECT AccountID,AccountTypeID,ExpiryDate,DateAdded FROM [Account]
                WHERE IsActive=1 and (AccountTypeID<>1 OR (AccountTypeID=1 AND GETDATE()>ExpiryDate)) AND 
                AccountID NOT IN(SELECT AccountID FROM [Billing] WHERE  YEAR(GETDATE())=YEAR(BillingDate) AND MONTH(GETDATE())=MONTH(BillingDate))");

        if(dtAccounts!=null && dtAccounts.Rows.Count>0)
        {
            foreach(DataRow drAccount in dtAccounts.Rows)
            {
                bool bIsYearly = false;

                string sLastBillingID = Common.GetValueFromSQL("SELECT TOP 1 BillingID FROM [Billing] WHERE AccountID="+ drAccount["AccountID"].ToString() + " ORDER BY BillingID DESC");

                if(!string.IsNullOrEmpty(sLastBillingID))
                {
                    Billing lastBilling = GetBilling(int.Parse(sLastBillingID));
                    if(lastBilling!=null && lastBilling.BillingDate!=null)
                    {
                        if(lastBilling.IsYearlySubscription!=null && (bool)lastBilling.IsYearlySubscription)
                        {
                            if (lastBilling.BillingDate.Value.Year == DateTime.Now.Year)
                            {
                                continue;
                            }

                            if (lastBilling.BillingDate.Value.Month !=DateTime.Now.Month)
                            {
                                continue;
                            }
                            else
                            {
                                bIsYearly = true;
                            }
                        }                        
                    }
                }

                DataTable dtUsers = Common.DataTableFromText("SELECT U.UserID FROM [UserRole] UR JOIN [User] U ON UR.UserID=U.UserID WHERE U.IsActive=1  and UR.IsPrimaryAccount=1 and UR.AccountID=" + drAccount["AccountID"].ToString());
                if(dtUsers!=null && dtUsers.Rows.Count>0)
                {

                    Billing newBilling = new Billing();
                    newBilling.AccountID = int.Parse(drAccount["AccountID"].ToString());
                    newBilling.Currency = GetAccountCurrency(newBilling.AccountID);
                    newBilling.Amount = TotalChargeAmount(newBilling.AccountID, dtUsers.Rows.Count,null,null);
                    newBilling.BillingDate = DateTime.Now;
                    newBilling.PaymentMethod = GetAccountPaymentMethod((int)newBilling.AccountID);
                    newBilling.SubscriptionID = LatestSubscriptionID(newBilling.AccountID);
                    if ( !string.IsNullOrEmpty(newBilling.PaymentMethod) && newBilling.PaymentMethod.ToLower() == "s")
                    {
                        StripeInvoice upcomingStripeInvoice = GetStripeUpcomingBilling((int)newBilling.AccountID);
                        if(upcomingStripeInvoice!=null)
                        {
                            newBilling.Amount = upcomingStripeInvoice.AmountDue;
                            newBilling.BillingDate = upcomingStripeInvoice.Date;
                        }
                    }
                    
                    newBilling.NoOfUsers = dtUsers.Rows.Count;
                    newBilling.IsYearlySubscription = bIsYearly;
                    //newBilling.
                    Int32 billingid = CreateBilling(newBilling);
                    ////create billing detail
                    //foreach(DataRow drUser in dtUsers.Rows)
                    //{
                    //    BillingDetail newBillingDetail = new BillingDetail();
                    //    newBillingDetail.BillingID = billingid;
                    //    newBillingDetail.UserID = int.Parse(drUser["UserID"].ToString());
                    //    Ontask_BillingDetail_Insert(newBillingDetail);
                    //}
                }
            }
        }

        //do we need this? 
        //DataTable dtMonthAccount = Common.DataTableFromText("SELECT AccountID FROM [Billing] WHERE  YEAR(GETDATE())=YEAR(BillingDate) AND MONTH(GETDATE())=MONTH(BillingDate)");
        //if (dtMonthAccount != null && dtMonthAccount.Rows.Count > 0)
        //{
        //    foreach (DataRow drMonthAccount in dtMonthAccount.Rows)
        //    {
        //        DataTable dtMissingUsers = Common.DataTableFromText(@"SELECT U.UserID FROM [Account] A JOIN [UserRole] UR ON A.AccountID=UR.AccountID
        //                    JOIN [User] U ON U.UserID=UR.UserID WHERE A.AccountID=" + drMonthAccount["AccountID"].ToString() + @" AND U.UserID NOT IN 
        //                    (SELECT BD.UserID FROM [Billing] B JOIN [BillingDetail] BD ON B.BillingID=BD.UserID 
        //                    WHERE  A.IsActive=1 AND U.IsActive=1 AND YEAR(GETDATE())=YEAR(BillingDate) AND MONTH(GETDATE())=MONTH(BillingDate) 
        //                    AND B.AccountID=" + drMonthAccount["AccountID"].ToString() + @")");
        //        if(dtMissingUsers!=null && dtMissingUsers.Rows.Count>0)
        //        {
        //            //need to add a new billing
        //            Billing newBilling = new Billing();
        //            newBilling.AccountID = int.Parse(drMonthAccount["AccountID"].ToString());
        //            newBilling.Amount = MonthlyCharge(newBilling.AccountID, dtMissingUsers.Rows.Count);
        //            newBilling.BillingDate = DateTime.Now;
        //            newBilling.Currency = GetAccountCurrency(newBilling.AccountID);
        //            newBilling.NoOfUsers = dtMissingUsers.Rows.Count;
        //            //newBilling.
        //            Int32 billingid = CreateBilling(newBilling);
        //            //create billing detail
        //            foreach (DataRow drUser in dtMissingUsers.Rows)
        //            {
        //                BillingDetail newBillingDetail = new BillingDetail();
        //                newBillingDetail.BillingID = billingid;
        //                newBillingDetail.UserID = int.Parse(drUser["UserID"].ToString());
        //                Ontask_BillingDetail_Insert(newBillingDetail);
        //            }
        //        }

        //    }

        //}

            //


            return -1;
    }

    public static string GetAccountCurrency(int iAccountID)
    {
        string sCurrency = "AUD";
        string sCountryID = Common.GetValueFromSQL("SELECT CountryID FROM [Account] WHERE AccountID=" + iAccountID.ToString());
        if(!string.IsNullOrEmpty(sCountryID))
        {
            if(GetAccountCountryName(int.Parse(sCountryID)).ToLower()!= "australia")
            {
                sCurrency = "USD";
            }
        }
        return sCurrency;
    }
    public static int TotalChargeAmount(int iAccountID, int? iNoOfUsers,int? iAccountTypeID,bool? bIsYearly)
    {
        int iTotal = 0;

        if(iNoOfUsers==null)
        {
            iNoOfUsers = 0;
            string sNoOfUsers = Common.GetValueFromSQL("SELECT count(U.UserID) FROM [UserRole] UR JOIN [User] U ON UR.UserID=U.UserID WHERE U.IsActive=1 and UR.AccountID="+ iAccountID.ToString());
            if(!string.IsNullOrEmpty(sNoOfUsers))
            {
                iNoOfUsers = int.Parse(sNoOfUsers);
            }
        }
        iTotal =((int) iNoOfUsers) * AmountPerUser(iAccountID, iAccountTypeID,bIsYearly);

        string sCurrency= GetAccountCurrency(iAccountID);

        //if(sCurrency.ToLower()=="aud")
        //{
        //    iTotal = iTotal * (11/10);
        //}

        return iTotal;

    }
    public static Int32 CreateBilling(Billing billing)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Ontask_Billing_Insert", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@BillingID", billing.BillingID)).Direction = ParameterDirection.InputOutput;
                command.Parameters.Add(new SqlParameter("@AccountID", billing.AccountID));
                command.Parameters.Add(new SqlParameter("@BillingDate", billing.BillingDate==null?DateTime.Now: billing.BillingDate));
                command.Parameters.Add(new SqlParameter("@Amount", billing.Amount));
                command.Parameters.Add(new SqlParameter("@sCurrency", billing.Currency));
                command.Parameters.Add(new SqlParameter("@NoOfUsers", billing.NoOfUsers));
                command.Parameters.Add(new SqlParameter("@Remarks", billing.Remarks));

                if (!string.IsNullOrEmpty(billing.PaymentMethod))
                    command.Parameters.Add(new SqlParameter("@PaymentMethod", billing.PaymentMethod));

                command.Parameters.Add(new SqlParameter("@PaymentSuccess", billing.PaymentSuccess));

                command.Parameters.Add(new SqlParameter("@dPaymentDate", billing.PaymentDate == DateTime.MinValue ? null : billing.PaymentDate));
                command.Parameters.Add(new SqlParameter("@bIsYearlySubscription", billing.IsYearlySubscription));
                command.Parameters.Add(new SqlParameter("@sSubscriptionID", billing.SubscriptionID));
                command.Parameters.Add(new SqlParameter("@TargetAccountTypeID", billing.TargetAccountTypeID));

                if (connection.State == ConnectionState.Closed)
                    connection.Open();
                Int32 billingid = 0;// or null, need to talk with Ricky  
                try
                {
                    command.ExecuteNonQuery();
                    billingid = (Int32)command.Parameters["@BillingID"].Value;
                }
                catch
                {

                }                
                connection.Close();
                connection.Dispose();
                return billingid;
            }
        }
    }

    public static void UpdateBilling(Billing billing)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("OnTask_Billing_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nBillingID", billing.BillingID));
                command.Parameters.Add(new SqlParameter("@nAccountID", billing.AccountID));
                command.Parameters.Add(new SqlParameter("@dBillingDate", billing.BillingDate));
                command.Parameters.Add(new SqlParameter("@nAmount", billing.Amount));
                command.Parameters.Add(new SqlParameter("@sCurrency", billing.Currency));
                command.Parameters.Add(new SqlParameter("@nNoOfUsers", billing.NoOfUsers));
                command.Parameters.Add(new SqlParameter("@sRemarks", billing.Remarks));
                command.Parameters.Add(new SqlParameter("@sPaymentMethod", billing.PaymentMethod));
                command.Parameters.Add(new SqlParameter("@bPaymentSuccess", billing.PaymentSuccess));
                command.Parameters.Add(new SqlParameter("@dPaymentDate", billing.PaymentDate == DateTime.MinValue ? null : billing.PaymentDate));
                command.Parameters.Add(new SqlParameter("@bIsYearlySubscription", billing.IsYearlySubscription));
                command.Parameters.Add(new SqlParameter("@sSubscriptionID", billing.SubscriptionID));
                command.Parameters.Add(new SqlParameter("@TargetAccountTypeID", billing.TargetAccountTypeID));

                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {

                }
                                
                connection.Close();
                connection.Dispose();
            }
        }
    }

  

    public static Billing GetBilling(Int32 billingid)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Ontask_Billing_Select", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@BillingID", billingid));
                

                if (connection.State == ConnectionState.Closed)
                    connection.Open();
                Billing billing = new Billing();
                try
                {
                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(command);
                    da.Fill(dt);

                    if (dt.Rows.Count < 0)
                        return null;
                    else
                    {

                        if (dt.Rows[0]["BillingID"] != null)
                            billing.BillingID = System.Int32.Parse(dt.Rows[0]["BillingID"].ToString());// (Int32)dt.Rows[0]["BillingID"]; // 
                        if (dt.Rows[0]["AccountID"] != null)
                            billing.AccountID = System.Int32.Parse(dt.Rows[0]["AccountID"].ToString());// (Int32)dt.Rows[0]["AccountID"];// System.Int32.Parse(dt.Rows[0]["AccountID"].ToString());//
                        if (dt.Rows[0]["BillingDate"] != null && dt.Rows[0]["BillingDate"].ToString() != "")
                            billing.BillingDate = (DateTime)dt.Rows[0]["BillingDate"];
                        if (dt.Rows[0]["Amount"] != null)
                            billing.Amount = (int)double.Parse(dt.Rows[0]["Amount"].ToString());
                        if (dt.Rows[0]["Currency"] != null)
                            billing.Currency = dt.Rows[0]["Currency"].ToString();
                        if (dt.Rows[0]["NoOfUsers"] != null)
                            billing.NoOfUsers = System.Int32.Parse(dt.Rows[0]["NoOfUsers"].ToString()); //(Int32)dt.Rows[0]["NoOfUsers"];// System.Int32.Parse(dt.Rows[0]["NoOfUsers"].ToString()); //
                        if (dt.Rows[0]["Remarks"] != null)
                            billing.Remarks = dt.Rows[0]["Remarks"].ToString();
                        if (dt.Rows[0]["PaymentMethod"] != null)
                            billing.PaymentMethod = dt.Rows[0]["PaymentMethod"].ToString();
                        if (dt.Rows[0]["PaymentSuccess"] != null)
                            billing.PaymentSuccess = (Boolean)dt.Rows[0]["PaymentSuccess"];
                        if (dt.Rows[0]["PaymentDate"] != null)
                            billing.PaymentDate = dt.Rows[0]["PaymentDate"] == DBNull.Value ? null : (DateTime?)dt.Rows[0]["PaymentDate"];
                        if (dt.Rows[0]["DateAdded"] != null)
                            billing.DateAdded = (DateTime)dt.Rows[0]["DateAdded"];
                        if (dt.Rows[0]["DateUpdated"] != null)
                            billing.DateUpdated = (DateTime)dt.Rows[0]["DateUpdated"];
                        if (dt.Rows[0]["IsYearlySubscription"].ToString() != "")
                            billing.IsYearlySubscription = (bool?)dt.Rows[0]["IsYearlySubscription"];
                        if (dt.Rows[0]["TargetAccountTypeID"].ToString() != "")
                            billing.TargetAccountTypeID = (int?)dt.Rows[0]["TargetAccountTypeID"];

                        billing.SubscriptionID = dt.Rows[0]["SubscriptionID"].ToString();
                    }
                }
                catch(Exception ex)
                {
                    //
                    ErrorLog theErrorLog = new ErrorLog(null, "OnTask Getbilling", ex.Message, ex.StackTrace, DateTime.Now, "BillingAPI.GetBilling");
                    SystemData.ErrorLog_Insert(theErrorLog);
                }
                connection.Close();
                connection.Dispose();
                return billing;
            }
        }

    }

    //public static DataTable Ontask_BillingDetail_List_ByBillingID(int nBillingID)
    //{
    //    DataTable dtBilling = new DataTable();
    //    using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
    //    {
    //        using (SqlCommand command = new SqlCommand("Ontask_BillingDetail_List_ByBillingID", connection))
    //        {
    //            command.CommandType = CommandType.StoredProcedure;
    //            command.Parameters.Add(new SqlParameter("@nBillingID", nBillingID));

    //            if (connection.State == ConnectionState.Closed)
    //                connection.Open();

    //            try
    //            {
    //                using (SqlDataAdapter da = new SqlDataAdapter(command))
    //                {
    //                    da.Fill(dtBilling);
    //                }
    //            }
    //            catch
    //            {
    //                //
    //            }

    //            connection.Close();
    //            connection.Dispose();
    //        }
    //    }

    //    return dtBilling;
    //}

    public static DataTable BillingHistory(Int32 AccountID)
    {
        DataTable dtBilling = new DataTable();
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Ontask_Billing_Select_History", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@AccountID", AccountID));

                if (connection.State == ConnectionState.Closed) 
                    connection.Open();

                try
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(command))
                    {
                        da.Fill(dtBilling);
                    }
                }
                catch
                {
                    //
                }

                connection.Close();
                connection.Dispose();
            }
        }

        return dtBilling;
    }

    public static Boolean CheckBilling(Int32 AccountID)
    {
        Boolean haspending = false;
        DataTable dtBilling = new DataTable();
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Ontask_Billing_CheckBilling", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@AccountID", AccountID));
                command.Parameters.Add(new SqlParameter("@HasPendingBilling", false)).Direction = ParameterDirection.InputOutput;

                if (connection.State == ConnectionState.Closed)
                    connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    haspending = (Boolean)command.Parameters["@HasPendingBilling"].Value;
                }
                catch
                {
                    //
                }
                
                connection.Close();
                connection.Dispose();
            }
        }

        return haspending;
    }


    public static Int32 GetCostPerMonh(Int32 accounttypeid)
    {
        Int32 costpermonth = 0;
        DataTable dt = Common.DataTableFromText("SELECT CostPerMonth FROM AccountType WHERE AccountTypeID=" + accounttypeid.ToString());
        if (dt.Rows.Count != 0)
        {
            costpermonth = Decimal.ToInt32((Decimal)dt.Rows[0]["CostPerMonth"]);
        }

        return costpermonth;
    }


    //public static string GetPendingBillingIDs(int iAccountID)
    //{
    //    string sBillingIDs = "";
    //    DataTable dtPendingBill = Common.DataTableFromText("SELECT BillingID FROM [Billing] WHERE AccountID=" + iAccountID.ToString() + " AND PaymentSuccess=0");
    //    if (dtPendingBill != null && dtPendingBill.Rows.Count > 0)
    //    {
    //        foreach (DataRow drPendingBill in dtPendingBill.Rows)
    //        {
    //            sBillingIDs = sBillingIDs + "," + drPendingBill["BillingID"].ToString();
    //        }
    //    }
    //    if (sBillingIDs.Length > 2)
    //    {
    //        sBillingIDs = sBillingIDs.Substring(1);
    //    }
    //    return sBillingIDs;
    //}
    public static String GetCurrency(String country)
    {
        if (country.Contains("Australia"))
        {
            return "AUD";
        }
        else
        {
            return "USD";
        }
    }

}







//public static int Ontask_BillingDetail_Insert(BillingDetail p_BillingDetail)
//{

//    using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
//    {
//        using (SqlCommand command = new SqlCommand("Ontask_BillingDetail_Insert", connection))
//        {

//            command.CommandType = CommandType.StoredProcedure;


//            SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int);
//            pRV.Direction = ParameterDirection.Output;

//            command.Parameters.Add(pRV);
//            command.Parameters.Add(new SqlParameter("@nBillingID", p_BillingDetail.BillingID));
//            command.Parameters.Add(new SqlParameter("@nUserID", p_BillingDetail.UserID));              

//            connection.Open();
//            try
//            {
//                command.ExecuteNonQuery();
//                connection.Close();
//                connection.Dispose();
//                return int.Parse(pRV.Value.ToString());
//            }
//            catch
//            {
//                connection.Close();
//                connection.Dispose();

//            }
//            return -1;
//        }

//    }


//}



//public static int Ontask_BillingDetail_Update(BillingDetail p_BillingDetail)
//{

//    using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
//    {
//        using (SqlCommand command = new SqlCommand("Ontask_BillingDetail_Update", connection))
//        {
//            command.CommandType = CommandType.StoredProcedure;



//            command.Parameters.Add(new SqlParameter("@nBillingDetailID", p_BillingDetail.BillingDetailID));
//            command.Parameters.Add(new SqlParameter("@nBillingID", p_BillingDetail.BillingID));
//            command.Parameters.Add(new SqlParameter("@nUserID", p_BillingDetail.UserID));


//            int i = 1;
//            connection.Open();
//            try
//            {
//                command.ExecuteNonQuery();
//            }
//            catch
//            {
//                i = -1;
//            }

//            connection.Close();
//            connection.Dispose();

//            return i;

//        }
//    }
//}




//public static int Ontask_BillingDetail_Delete(int nBillingDetailID)
//{
//    using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
//    {
//        using (SqlCommand command = new SqlCommand("Ontask_BillingDetail_Delete", connection))
//        {

//            command.CommandType = CommandType.StoredProcedure;
//            command.Parameters.Add(new SqlParameter("@nBillingDetailID ", nBillingDetailID));

//            int i = 1;
//            connection.Open();
//            try
//            {
//                command.ExecuteNonQuery();
//            }
//            catch
//            {
//                i = -1;
//            }

//            connection.Close();
//            connection.Dispose();

//            return i;

//        }
//    }
//}


//public static BillingDetail Ontask_BillingDetail_Detail(int nBillingDetailID)
//{


//    using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
//    {

//        using (SqlCommand command = new SqlCommand("Ontask_BillingDetail_Detail", connection))
//        {
//            command.CommandType = CommandType.StoredProcedure;

//            command.Parameters.Add(new SqlParameter("@nBillingDetailID", nBillingDetailID));

//            connection.Open();

//            try
//            {
//                using (SqlDataReader reader = command.ExecuteReader())
//                {
//                    while (reader.Read())
//                    {
//                        BillingDetail temp = new BillingDetail();

//                        temp.BillingDetailID = (int)reader["BillingDetailID"];
//                        temp.BillingID = (Int32)reader["BillingID"];
//                        temp.UserID = (int)reader["UserID"];
//                        temp.DateAdded = (DateTime)reader["DateAdded"];
//                        temp.DateUpdated = (DateTime)reader["DateUpdated"];

//                        connection.Close();
//                        connection.Dispose();
//                        return temp;
//                    }

//                }

//            }
//            catch
//            {

//            }
//            connection.Close();
//            connection.Dispose();

//            return null;

//        }

//    }

//}



//public static DataTable Ontask_BillingDetail_List_ByBillingID(int nBillingID)
//{

//    using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
//    {
//        using (SqlCommand command = new SqlCommand("Ontask_BillingDetail_List_ByBillingID", connection))
//        {
//            command.CommandType = CommandType.StoredProcedure;

//             command.Parameters.Add(new SqlParameter("@nBillingID ", nBillingID));                

//            SqlDataAdapter da = new SqlDataAdapter();
//            da.SelectCommand = command;
//            DataTable dt = new DataTable();
//            System.Data.DataSet ds = new System.Data.DataSet();

//            connection.Open();
//            try
//            {
//                da.Fill(ds);
//            }
//            catch
//            {
//                //
//            }
//            connection.Close();
//            connection.Dispose();

//            if (ds.Tables.Count > 0)
//            {
//                return ds.Tables[0];
//            }
//            {
//                return null;
//            }

//        }
//    }
//}

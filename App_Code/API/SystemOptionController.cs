﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Description;
using System.Net.Mail;
using System.Linq;
using System.Web;
public class SystemOptionController : ApiController
{


    [Route("api/accountactivestatus")]
    [HttpGet]   
    [ResponseType(typeof(string))]
    public HttpResponseMessage accountactivestatus()
    {
        try
        {
            var query = Request.GetQueryNameValuePairs();
            string stremail = query.FirstOrDefault(q => q.Key == "email").Value;

            string sIsActive = Common.GetValueFromSQL(@"SELECT A.IsActive
                    FROM dbo.Account A
                    JOIN [UserRole]  UR ON UR.AccountID = A.AccountID 
                    JOIN [User] U ON U.UserID = UR.UserID 
                    WHERE UR.IsAccountHolder=1 AND U.Email='"+ stremail +@"'");
            
            return Request.CreateResponse(HttpStatusCode.OK, sIsActive);
        }
        catch (Exception ex)
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error-" + ex.Message + "--" + ex.StackTrace);
        }
    }





    [Route("api/accounts/{accountId:int}/systemoption")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage GetSystemOption(int accountId, [FromUri]string optionKey, [FromUri]int? tableId)
    {
        string optionValue = null;
        if (!String.IsNullOrEmpty(optionKey))
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("SystemOption_Detail_Key_Account", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@sOptionKey", optionKey);
                    cmd.Parameters.AddWithValue("@nAccountID", accountId);
                    if (tableId.HasValue && tableId.Value > 0)
                    {
                        cmd.Parameters.AddWithValue("@nTableID", tableId);
                    }
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow))
                    {
                        if (reader.Read())
                        {
                            optionValue = Convert.ToString(reader["OptionValue"]);
                        }
                        reader.Close();
                    }
                    conn.Close();
                }                
                return Request.CreateResponse(optionValue != null ? HttpStatusCode.OK : HttpStatusCode.NoContent, optionValue);
            }
            catch {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
            }
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Option key is empty");
        }        
    }

    [Route("api/accounts/{accountId:int}/lastinvoicenumber")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage GetLastInvoiceNumber(int accountId)
    {
        try
        {
            var query = Request.GetQueryNameValuePairs();
            string strtableid = query.FirstOrDefault(q => q.Key == "tableid").Value;
            int tableid = int.Parse(strtableid);
            string sSQL = string.Format("SELECT MAX(CAST(V001 as INT)) FROM Record where TableID = {0} AND IsActive = 1 AND ISNUMERIC(V001) = 1", tableid);

            string maxValue = Common.GetValueFromSQL(sSQL);
            if (string.IsNullOrEmpty(maxValue)) maxValue = "0000"; //set default
            return Request.CreateResponse(maxValue != null ? HttpStatusCode.OK : HttpStatusCode.NoContent, maxValue);
        }
        catch
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
        }
    }



    [Route("api/task/{taskid:int}/nextavailabletaskname")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage NextAvailableTaskName(int taskid)
    {
        try
        {
            string stNextAvailableTaskName = Common.GetValueFromSQL("select dbo.ontask_fnNextAvailableTaskName(" + taskid.ToString() + ")");           
            return Request.CreateResponse(HttpStatusCode.OK, stNextAvailableTaskName);
        }
        catch(Exception ex)
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error-" + ex.Message + "--" + ex.StackTrace);
        }
    }




    [Route("api/accounts/{accountId:int}/location")]
    [HttpPost]
    [BasicAuthentication]
    [ResponseType(typeof(int))]
    public HttpResponseMessage InsertUserLocation(int accountId, [FromBody]LocationField location)
    {
        int newId = 0;

       
        try
        {
            //let's update user last locaiton
            if(!string.IsNullOrEmpty( location.UserID) && !string.IsNullOrEmpty(location.Latitude) && !string.IsNullOrEmpty(location.Longitude))
            {
                User theUser = SecurityManager.User_Details(int.Parse(location.UserID));
                if(theUser!=null)
                {
                    UserAttributes_ontask userAttributes = new UserAttributes_ontask();
                    if (!string.IsNullOrEmpty(theUser.Attributes))
                    {
                        userAttributes = DocGen.DAL.JSONField.GetTypedObject<UserAttributes_ontask>(theUser.Attributes);
                    }
                    LocationColumn userLocation = new LocationColumn();
                    if (!string.IsNullOrEmpty(userAttributes.LastSeenLocation))
                    {
                        userLocation= DocGen.DAL.JSONField.GetTypedObject<LocationColumn>(userAttributes.LastSeenLocation);
                    }
                   
                    userLocation.Latitude = location.Latitude;
                    userLocation.Longitude = location.Longitude;
                    userAttributes.LastSeenLocation = userLocation.GetJSONString();
                    userAttributes.LastSeenTime = DateTime.Now;
                    theUser.Attributes = userAttributes.GetJSONString();
                    SecurityManager.User_Update(theUser);
                }
            }
        }
        catch(Exception exu)
        {
            //
        }

        try
        {




            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                SqlCommand cmd = new SqlCommand("UserLocation_Insert", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter()
                {
                    ParameterName = "@nNewID",
                    DbType = DbType.Int32,
                    Direction = ParameterDirection.Output
                });

                cmd.Parameters.AddWithValue("@nUserID", location.UserID);
                cmd.Parameters.AddWithValue("@nLatitude", location.Latitude);
                cmd.Parameters.AddWithValue("@nLongitude", location.Longitude);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                newId = (cmd.Parameters["@nNewID"].Value != System.DBNull.Value) ? Convert.ToInt32(cmd.Parameters["@nNewID"].Value) : 0;
            }
            return Request.CreateResponse(HttpStatusCode.OK, newId);
        }
        catch (Exception ex)
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
        }

    }

    [Route("api/accounts/{accountId:int}/forwardtask")]
    [HttpPost]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage ForwardTask(int accountId, [FromBody]AccountInviteField info)
    {
        try
        {
           
            int iAccountInviteID= InvoiceManager.ForwardTask(accountId, info);

            return Request.CreateResponse(HttpStatusCode.OK, iAccountInviteID.ToString());
        }
        catch
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
        }

    }

    [Route("api/accounts/{accountId:int}/cancelforwardtask")]
    [HttpPost]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage CancelForwardTask(int accountId, [FromBody]AccountInviteField info)
    {
        try
        {

            int iAccountInviteID = InvoiceManager.CancelForwardTask(accountId, info);

            return Request.CreateResponse(HttpStatusCode.OK, iAccountInviteID.ToString());
        }
        catch
        {
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
        }

    }


    //[Route("api/accounts/{accountId:int}/signup")]
    //[HttpPost]
    //[BasicAuthentication]
    //[ResponseType(typeof(string))]
    //public HttpResponseMessage CreateContractor(int accountId, [FromBody]SignUpField info)
    //{
    //    string strUserID = "";
    //    int _iAccountType = 2;
    //    if (!String.IsNullOrEmpty(info.Email))
    //    {
    //        try
    //        {
    //            string strPassword = info.Password;
    //            string strFirstName = info.FullName.Substring(0, info.FullName.IndexOf(" "));
    //            string strLastName = info.FullName.Replace(strFirstName + " ", "");
    //            string strEmail = info.Email;

    //            strUserID = SecurityManager.eContractorSignUp(accountId, _iAccountType, strEmail, strPassword, strFirstName, strLastName);
    //            if (string.IsNullOrEmpty(strUserID))
    //                return Request.CreateResponse(HttpStatusCode.NoContent, strUserID);

    //            else if (strUserID == "-1")
    //                return Request.CreateResponse(HttpStatusCode.BadRequest, "Email is already exist");

    //            else
    //            {
    //                //Add new contractor to Record table that link to User
    //                int tableId = 5062; //eContractor table
    //                Dictionary<string, string> fieldValues = new Dictionary<string, string>();
    //                fieldValues.Add("V001", info.FullName);
    //                fieldValues.Add("V003", "");
    //                fieldValues.Add("V004", strEmail);
    //                fieldValues.Add("V005", "True");
    //                fieldValues.Add("V006", strPassword);
    //                fieldValues.Add("V008", strUserID);

    //                int strContractorId = InsertContractor(tableId, fieldValues);
    //                if(strContractorId > 0 )
    //                    return Request.CreateResponse(HttpStatusCode.OK, strContractorId.ToString());
    //                else
    //                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Cannot add contractor");
    //            }
    //        }
    //        catch
    //        {
    //            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
    //        }
    //    }
    //    else
    //    {
    //        return Request.CreateResponse(HttpStatusCode.BadRequest, "Email is empty");
    //    }
    //}



    [Route("api/resendconfirmationemail")]
    [HttpPost]
    //[BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage resendconfirmationemail()
    {
        try
        {
            var query = Request.GetQueryNameValuePairs();
            string strEmail =  query.FirstOrDefault(q => q.Key == "email").Value;
            string strUserID = Common.GetValueFromSQL("SELECT UserID FROM [User] WHERE [Email] = '" + strEmail.Replace("'", "''") + "'");
            
            if(string.IsNullOrEmpty(strUserID))
            {
                strEmail = Cryptography.Decrypt(strEmail);
                strUserID = Common.GetValueFromSQL("SELECT UserID FROM [User] WHERE [Email] = '" + strEmail.Replace("'", "''") + "'");
            }
            
            int? iAccountID = SecurityManager.GetPrimaryAccountID(int.Parse(strUserID));
            User theUser = SecurityManager.User_Details(int.Parse(strUserID));
            SendSignUpEmail(strEmail, theUser.FirstName, (int)iAccountID);
            return Request.CreateResponse(HttpStatusCode.OK, "OK");

        }
        catch
        {

            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
        }

    }





    //[Route("api/resendconfirmationemail")]
    //[HttpPost]
    ////[BasicAuthentication]
    //[ResponseType(typeof(string))]
    //public HttpResponseMessage resendconfirmationemail()
    //{
    //    try
    //    {
    //        var query = Request.GetQueryNameValuePairs();
    //        string strEmail = query.FirstOrDefault(q => q.Key == "email").Value;
    //        string strUserID = Common.GetValueFromSQL("SELECT UserID FROM [User] WHERE [Email] = '" + strEmail.Replace("'", "''") + "'");
    //        int? iAccountID = SecurityManager.GetPrimaryAccountID(int.Parse(strUserID));
    //        User theUser = SecurityManager.User_Details(int.Parse(strUserID));
    //        SendSignUpEmail(strEmail, theUser.FirstName, (int)iAccountID);
    //        return Request.CreateResponse(HttpStatusCode.OK, "OK");

    //    }
    //    catch
    //    {

    //        return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
    //    }

    //}

    [Route("api/accounts/{accountId:int}/ecsignup")]
    [HttpPost]
    //[BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage CreateEContractor(int accountId, [FromBody]SignUpField info)
    {
        //At the time of signup there is no accountId, pass -1 in this case
        //pass an AccountID when you wnat to add a user to a specific account. e.g Invite a new user i think.

        string strUserID = "";
        int _iAccountType = 1;
        if (!String.IsNullOrEmpty(info.Email))
        {
            try
            {
                string strPassword = info.Password;
                string strFirstName = info.FullName.Substring(0, info.FullName.IndexOf(" "));
                string strLastName = info.FullName.Replace(strFirstName + " ", "");
                string strEmail = info.Email;
                string strPhoneNumber = "";
                if (!string.IsNullOrEmpty(info.PhoneNumber))
                {
                    strPhoneNumber = info.PhoneNumber;
                }

                //Do they already have a login (User record) 
                //bool bAddAcontratorRecord = false;
                strUserID = Common.GetValueFromSQL("SELECT UserID FROM [User] WHERE [Email] = '" + strEmail.Replace("'","''")+ "'");
                if (strUserID=="")
                {
                    //create a new account for this user                    
                    string strAccountName = info.FullName.Replace(" ",""); 
       
                    // a clean eampty account.
                    strUserID = SecurityManager.CommonSignUp(strAccountName, _iAccountType,
                     strEmail, strPassword, strFirstName, strLastName, strPhoneNumber);

                    //get the new account's ID
                    int? iAccountID = SecurityManager.GetPrimaryAccountID(int.Parse(strUserID));

                    string strTemplateAccountID = SystemData.SystemOption_ValueByKey_Account("TemplateAccountID", null, null).ToLower();

                    //Call the SP eContractor_Copy_Account
                    RecordManager.eContractor_Copy_Account(int.Parse(strTemplateAccountID), (int)iAccountID, int.Parse(strUserID));

                    accountId = (int)iAccountID;
                    //bAddAcontratorRecord = true;

                    //Account theAccount = SecurityManager.Account_Details(accountId);
                    //theAccount.IsActive = true;//Wait for email validation
                    //theAccount.Comment = theAccount.AccountID.ToString();
                    //SecurityManager.Account_Update(theAccount);


                    SendSignUpEmail(strEmail, strFirstName, (int)iAccountID);
                }
                else
                {
                    if(accountId==-1)
                    {
                        //we need to send them an email to remind them how to sign in. @Lan
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Email already exist");
                    }
                    else
                    {
                        //we want to add this user to the passed(accountId) Account

                        //check if this user is already added in this account
                        string strUserRoleID = Common.GetValueFromSQL("SELECT UserRoleID FROM [UserRole] WHERE UserID=" + strUserID + " AND AccountID=" + accountId.ToString());

                        if(strUserRoleID=="")
                        {
                            //Add the user to the UserRole table so that this user can have access to the Account
                            string strAddEditRoleID = Common.GetValueFromSQL("SELECT RoleID FROM [Role]  WHERE RoleType='4' AND AccountID=" + accountId.ToString());
                            UserRole newUserRole = new UserRole(null, int.Parse(strUserID), int.Parse(strAddEditRoleID), null, null);
                            newUserRole.AccountID = accountId;
                            newUserRole.IsPrimaryAccount = false;
                            newUserRole.IsAccountHolder = false;
                            int iUserRoleID = SecurityManager.UserRole_Insert(newUserRole);
                            //bAddAcontratorRecord = true;
                        }
                        else
                        {
                            //we need to send them an email to remind them how to sign in. @Lan
                            return Request.CreateResponse(HttpStatusCode.BadRequest, "Email already exist");
                        }

                    }
                }

                //if (bAddAcontratorRecord)
                //{
                //    //Link the UserID to the Contrator virtual table

                //    int tableId = -1;// 5062; //eContractor table
                //    //string strContractorTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Contractor' AND AccountID=" 
                //    //    + accountId.ToString() + " AND IsActive=1");
                //    int iTemp = 0;
                //    //if (int.TryParse(strContractorTableID, out iTemp) == false)
                //    //    strContractorTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Contractor TableID", accountId, null);


                //    //if (strContractorTableID != "")
                //    //    tableId = int.Parse(strContractorTableID);



                //    //Record theReocrd = new Record();
                //    //theReocrd.TableID = tableId;
                //    //RecordManager.MakeTheRecord(ref theReocrd, "V001", info.FullName);
                //    //RecordManager.MakeTheRecord(ref theReocrd, "V003", strPhoneNumber);
                //    //RecordManager.MakeTheRecord(ref theReocrd, "V004", strEmail);
                //    //RecordManager.MakeTheRecord(ref theReocrd, "V005", "True");
                //    //RecordManager.MakeTheRecord(ref theReocrd, "V006", strPassword);
                //    //RecordManager.MakeTheRecord(ref theReocrd, "V008", strUserID);
                //    //RecordManager.MakeTheRecord(ref theReocrd, "V011", Guid.NewGuid().ToString());

                //    //theReocrd.EnteredBy = int.Parse(strUserID);
                //    //int strContractorId=RecordManager.ets_Record_Insert(theReocrd);

                //    ////int strContractorId = InsertContractor(tableId, fieldValues);
                //    //if (strContractorId > 0)
                //    //    return Request.CreateResponse(HttpStatusCode.OK, strContractorId.ToString());
                //    //else
                //    //    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Cannot add contractor");
                //}
                return Request.CreateResponse(HttpStatusCode.OK, strUserID.ToString());
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
            }
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Email is empty");
        }
    }



    [Route("api/updateuser")]
    [HttpPost]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage UpdateUser([FromBody]UserField info)
    {
        //At the time of signup there is no accountId, pass -1 in this case
        //pass an AccountID when you wnat to add a user to a specific account. e.g Invite a new user i think.

         if (info.UserID>0)
         {
             User theUser = SecurityManager.User_Details(info.UserID);
             if(theUser!=null)
             {
                 if(info.FullName.IndexOf(" ")>0)
                 {
                     theUser.FirstName = info.FullName.Substring(0, info.FullName.IndexOf(" "));
                     theUser.LastName = info.FullName.Replace(theUser.FirstName + " ", "");
                 }
                 else
                 {
                     theUser.FirstName = info.FullName;
                 }
                 theUser.Email = info.Email;
                 theUser.PhoneNumber = info.PhoneNumber;
                 theUser.ProfilePicture = info.ProfilePicture;                

                 SecurityManager.User_Update(theUser);

                 //if (info.RecordID > 0)
                 //{
                 //    Record theContractorRecord = RecordManager.ets_Record_Detail_Full(info.RecordID, null, false);
                 //    if(theContractorRecord!=null)
                 //    {
                 //        theContractorRecord.V003 = info.PhoneNumber;
                 //        RecordManager.ets_Record_Update(theContractorRecord, true);
                 //    }
                 //}



                 return Request.CreateResponse(HttpStatusCode.OK, theUser.UserID.ToString());
             }
             else
             {
                 return Request.CreateResponse(HttpStatusCode.BadRequest, "User not found.");
             }
         }
         else
         {
             return Request.CreateResponse(HttpStatusCode.BadRequest, "Please send UserID.");
         }
        
    }





    [Route("api/PostUserImage")]
    [HttpPost]
    [BasicAuthentication]
    [ResponseType(typeof(string))]
    public HttpResponseMessage PostUserImage() // public async Task<HttpResponseMessage> PostUserImage()
    {
        Dictionary<string, object> dict = new Dictionary<string, object>();
        try
        {
            var httpRequest = HttpContext.Current.Request;
            string strFileNameTemp="";
            foreach (string file in httpRequest.Files)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                var postedFile = httpRequest.Files[file];
                if (postedFile != null && postedFile.ContentLength > 0)                    //int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB
                { 
                    IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                    var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                    var extension = ext.ToLower();
                    if (!AllowedFileExtensions.Contains(extension))
                    {

                        var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

                        dict.Add("error", message);
                        return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                    }                   
                    else
                    {                      
                        strFileNameTemp = Guid.NewGuid().ToString() + extension;
                       string _strFilesPhisicalPath= SystemData.SystemOption_ValueByKey_Account("FilesPhisicalPath", null, null);
                       string filePath = _strFilesPhisicalPath + "\\UserFiles\\AppFiles\\" + strFileNameTemp;
                        postedFile.SaveAs(filePath);
                    }
                }
                return Request.CreateErrorResponse(HttpStatusCode.Created, strFileNameTemp);
               
            }
            var res = string.Format("Please Upload a image.");
            dict.Add("error", res);
            return Request.CreateResponse(HttpStatusCode.NotFound, dict);
        }
        catch (Exception ex)
        {
            var res = string.Format("some Message");
            dict.Add("error", res);
            return Request.CreateResponse(HttpStatusCode.NotFound, dict);
        }
    }



    //else if (postedFile.ContentLength > MaxContentLength)
    //{

    //    var message = string.Format("Please Upload a file upto 1 mb.");

    //    dict.Add("error", message);
    //    return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
    //}


    #region Helper

    public void SendSignUpEmail(string strEmail, string strFirstName, int iAccountID)
    {
        try
        {
            string strEmailFrom = SystemData.SystemOption_ValueByKey_Account("EmailFrom", null, null);
            string strEmailServer = SystemData.SystemOption_ValueByKey_Account("EmailServer", null, null);
            string strEmailUserName = SystemData.SystemOption_ValueByKey_Account("EmailUserName", null, null);
            string strEmailPassword = SystemData.SystemOption_ValueByKey_Account("EmailPassword", null, null);

            //send email to user
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(strEmailFrom);

            //this might be different content for eContractor account
            Content theContent2 = SystemData.Content_Details_ByKey("eContractor Sign Up email", null);


            msg.Subject = theContent2.Heading;
            msg.IsBodyHtml = true;


            theContent2.ContentP = theContent2.ContentP.Replace("[First Name]", strFirstName);
            theContent2.ContentP = theContent2.ContentP.Replace("[Email]", strEmail);
            //theContent2.ContentP = theContent2.ContentP.Replace("[Password]", strPassword);
            Account theAccount = SecurityManager.Account_Details(iAccountID);

            string strUserID = Common.GetValueFromSQL("SELECT UserID FROM [User] WHERE [Email] = '" + strEmail.Replace("'", "''") + "'");
            if (theAccount != null)
            {
                //string strConfirmEmailURL = "https://ontask.thedatabase.net" +
                //    "/Email.aspx?key=" + Cryptography.Encrypt(theAccount.AccountID.ToString()) +
                //    "&key2=" + Cryptography.Encrypt(theAccount.DateUpdated.ToString());

                string strConfirmEmailURL = "https://ontask.thedatabase.net" +
                    "/Email.aspx?uid=" + Cryptography.Encrypt(strUserID);

                theContent2.ContentP = theContent2.ContentP.Replace("[ConfirmMyEmai]", strConfirmEmailURL);
            }



            msg.Body = theContent2.ContentP;// Sb.ToString();

            SmtpClient smtpClient2 = new SmtpClient(strEmailServer);
            smtpClient2.Timeout = 99999;
            smtpClient2.Credentials = new System.Net.NetworkCredential(strEmailUserName, strEmailPassword);
            smtpClient2.Port = DBGurus.StringToInt(SystemData.SystemOption_ValueByKey_Account("SmtpPort", null, null));
            smtpClient2.EnableSsl = Convert.ToBoolean(SystemData.SystemOption_ValueByKey_Account("EnableSSL", null, null));

            msg.To.Add(strEmail);

            msg.Bcc.Add("info@dbgurus.com.au");
            smtpClient2.Send(msg);  
        }
        catch
        {
            //
        }
    }

    //public int InsertContractor(int tableId, Dictionary<string, string> fieldValues)
    //{
    //    int newId = 0;
    //    var username = Thread.CurrentPrincipal.Identity.Name;
    //    try
    //    {
    //        int userId = SecurityManager.User_ByEmail(username).UserID.Value;
    //        using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
    //        {
    //            SqlCommand cmd = new SqlCommand("ets_Record_Insert", conn);
    //            cmd.CommandType = CommandType.StoredProcedure;
    //            cmd.Parameters.Add(new SqlParameter()
    //            {
    //                ParameterName = "@nNewID",
    //                DbType = DbType.Int32,
    //                Direction = ParameterDirection.Output
    //            });

    //            cmd.Parameters.AddWithValue("@TableID", tableId);
    //            cmd.Parameters.AddWithValue("@DateTimeRecorded", DateTime.Now);
    //            cmd.Parameters.AddWithValue("@EnteredBy", userId);

    //            foreach (string fieldName in fieldValues.Keys)
    //            {
    //                cmd.Parameters.AddWithValue(String.Format("@{0}", fieldName), fieldValues[fieldName]);
    //            }
    //            conn.Open();
    //            cmd.ExecuteNonQuery();
    //            conn.Close();

    //            newId = Convert.ToInt32(cmd.Parameters["@nNewID"].Value);
    //        }
    //        return newId;
    //    }
    //    catch (Exception)
    //    {
    //        return -1;
    //    }

    //}
    #endregion
}

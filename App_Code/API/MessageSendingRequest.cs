﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MessageSendingRequest
/// </summary>
public class MessageSendingRequest
{
    public int RecordId { get; set; }
    public string MessageType { get; set; }
    public string DeliveryMethod { get; set; }
    public string OtherParty { get; set; }
    public string Subject { get; set; }
    public string Body { get; set; }
    public int ContentId { get; set; }
    public InvoiceAttachment Invoice { get; set; }
}
public class InvoiceAttachment
{
    public string InvoiceNumber { get; set; }
    public string InvoiceDate { get; set; }
    public string InvoiceTo { get; set; }
    public string InvoiceFrom { get; set; }
    public string InvoiceFor { get; set; }
    public string InvoiceNote { get; set; }
    public string InvoiceFooter { get; set; }
    public string ABN { get; set; }
    public List<InvoiceItem> Items { get; set; }
}

public class InvoiceItem
{
    public string Item { get; set; }
    public string Net { get; set; }
    public string Tax { get; set; }
    public string Gross { get; set; }
}
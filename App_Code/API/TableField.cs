﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TableField
/// </summary>
public class TableField
{
    public string SystemName { get; set; }
    public string DisplayName { get; set; }
    public string ColumnType { get; set; }
    public string DropDownType { get; set; }
    public int? DropDownTableId { get; set; }
}
public class LocationField
{
    public string UserID { get; set; }
    public string Latitude { get; set; }
    public string Longitude { get; set; }
    
}
public class SignUpField
{
    public string FullName { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
    public string PhoneNumber { get; set; }

}

public class UserField
{
    public int UserID { get; set; }
    public string FullName { get; set; }
    public string Email { get; set; }   
    public string PhoneNumber { get; set; } 
    public string ProfilePicture { get; set; }

    //public int RecordID { get; set; }
    //public Object ProfilePictureObject { get; set; }

}

public class AccountInviteField
{
    public string FromUserID { get; set; }
    public string TaskID { get; set; }
    public string ToFullName { get; set; }
    public string ToEmailAddress { get; set; }
    public string ToMobile { get; set; }
    public string AddAsTeamMember { get; set; }
    public string InviteType { get; set; } //N=New User, C=Current account, E=External
    public string FromAccountID { get; set; }
    public string ToUserID { get; set; }

    public string AccountInviteID { get; set; }

}
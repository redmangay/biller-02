﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Description;

/// <summary>
/// Summary description for MelbourneAnglican
/// </summary>
/// 
public class LastUpdatedwlttwData
{
    public DataTable LayoutData { get; set; }
    public DataTable ServiceData { get; set; }
  
}
//public class wlttwData
//{
//    public int? AccountID { get; set; }
//    public int? ServiceTableID { get; set; }
//    public int? LayoutTableID { get; set; }
//    public int? UserID { get; set; }

//}
public class wlttwController : ApiController
{



  

    //[Route("api/getservertime")]
    //[HttpGet]
    //[BasicAuthentication]
    //[ResponseType(typeof(DateTime))]
    //public HttpResponseMessage GetServerTime()
    //{
    //     return Request.CreateResponse(HttpStatusCode.OK, DateTime.Now);

    //}

    [Route("api/wlttwdata")]
    [HttpGet]
    [BasicAuthentication]
    [ResponseType(typeof(LastUpdatedwlttwData))]
    public HttpResponseMessage wlttwdata()
    {
        try
        {
            var query = Request.GetQueryNameValuePairs();

            var strUserEmail = Thread.CurrentPrincipal.Identity.Name;

            string strUserID = Common.GetValueFromSQL("SELECT UserID FROM [User] WHERE Email='" + strUserEmail + "'");
            int? iAccountID = SecurityManager.GetPrimaryAccountID(int.Parse(strUserID));
            string strServiceTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE (SystemName='Prayer' OR TableName='Prayer') AND AccountID=" +
                   iAccountID.ToString() + " AND IsActive=1");

           int iServiceTableID = int.Parse(strServiceTableID.Trim());

            string strLayoutTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE (SystemName='Layout' OR TableName='Layout') AND AccountID=" + iAccountID.ToString() + " AND IsActive=1");
            int iLayoutTableID = int.Parse(strLayoutTableID.Trim());
            //string sTextSearch = GetNullableString(query.FirstOrDefault(q => q.Key == "sTextSearch").Value);
            string sTextSearch = "";
            string sLastUpdated = GetNullableString(query.FirstOrDefault(q => q.Key == "LastUpdated").Value);

            int iTN = 0;
            int _iTotalDynamicColumns = 0;
            string strReturnSQL = "", sReturnHeaderSQL = "";
            Exception exParam = null;
            bool bMainLayoutUpdated = false;



            var username = Thread.CurrentPrincipal.Identity.Name;
            User user = SecurityManager.User_ByEmail(username);
            bool hasPermission = user != null && user.IsActive.HasValue && user.IsActive.Value;
            TableRights rights = user != null ? SecurityManager.GetUserRightsOnTable(user.UserID.Value, iServiceTableID) : null;
            hasPermission = hasPermission && rights != null && (rights.CanSelect == "Y" || rights.CanSelect == "O");

            //int accountId = int.Parse(strAccountID);

            if (hasPermission == false)
            {
                string roletype = SecurityManager.GetUserRoleTypeID((int)user.UserID, (int)iAccountID);
                if (roletype == "2")
                {
                    hasPermission = true;
                }
            }

            LastUpdatedwlttwData list = new LastUpdatedwlttwData();

            if (hasPermission)
            {
                if (!string.IsNullOrEmpty(sLastUpdated))
                {
                    //sTextSearch = " Record.DateUpdated>CONVERT(datetime,'20/01/2019 16:03:07',103)'";
                    //Record.V005='Main' AND 
                    sTextSearch = " Record.DateUpdated>CONVERT(datetime,'" + DateTime.Parse(sLastUpdated).ToString("dd/MM/yyyy HH:mm:ss") + "',103)";
                }

                DataTable dtLayoutDataAll = RecordManager.ets_Record_List(iLayoutTableID,
                              null, true, null, null, null,
                              "", "", 0, 0, ref iTN, ref _iTotalDynamicColumns, "allcolumns", "", sTextSearch,
                             null, null, "", "", "", null,
                             ref strReturnSQL, ref sReturnHeaderSQL, ref exParam, false);

                if (dtLayoutDataAll != null && dtLayoutDataAll.Rows.Count > 0)
                {
                    foreach (DataRow drLayout in dtLayoutDataAll.Rows)
                    {
                        if (drLayout["Layout Name"].ToString().ToLower() == "main")
                        {
                            bMainLayoutUpdated = true;
                        }
                    }

                }



                iTN = 0;
                _iTotalDynamicColumns = 0;
                strReturnSQL = "";
                sReturnHeaderSQL = "";
                exParam = null;


                if (bMainLayoutUpdated)
                {
                    //get all service records
                    sTextSearch = "";
                }
                else
                {
                    sTextSearch = " Record.DateUpdated>CONVERT(datetime,'" + DateTime.Parse(sLastUpdated).ToString("dd/MM/yyyy HH:mm:ss") + "',103)";

                }

                DataTable dtServiceDataAll = RecordManager.ets_Record_List(iServiceTableID,
                               null, true, null, null, null,
                               "", "", 0, 0, ref iTN, ref _iTotalDynamicColumns, "allcolumns", "", sTextSearch,
                              null, null, "", "", "", null,
                              ref strReturnSQL, ref sReturnHeaderSQL, ref exParam, false);


                DataTable dtService = new DataTable();
                dtService.Columns.Add("Date");
                dtService.Columns.Add("FullContent");
                dtService.Columns.Add("DateAdded");
                dtService.Columns.Add("DateUpdated");


                DataTable dtLayout = new DataTable();
                dtLayout.Columns.Add("LayoutName");
                dtLayout.Columns.Add("Template");
                dtLayout.Columns.Add("Stylesheet");
                dtLayout.Columns.Add("DateAdded");
                dtLayout.Columns.Add("DateUpdated");




                if (dtServiceDataAll != null && dtServiceDataAll.Rows.Count > 0)
                {
                    string sOrigLayoutTemlate = Common.GetValueFromSQL("SELECT V002 FROM [Record] WHERE TableID=" + strLayoutTableID + " AND IsActive=1 AND V001='Main'");
                    string sOrigLayoutStyle = Common.GetValueFromSQL("SELECT V005 FROM [Record] WHERE TableID=" + strLayoutTableID + " AND IsActive=1 AND V001='Main'");
                    sOrigLayoutStyle = "<style type='text/css'>" + sOrigLayoutStyle + " </style>";
                    foreach (DataRow drService in dtServiceDataAll.Rows)
                    {
                        string sEachLayoutTemplate = sOrigLayoutStyle + sOrigLayoutTemlate;
                      
                        //sEachLayoutTemplate = sEachLayoutTemplate.Replace("[Date]", drService["Date"].ToString());
                        
                        foreach (DataColumn dc in dtServiceDataAll.Columns)
                        {
                            
                            sEachLayoutTemplate = sEachLayoutTemplate.Replace("["+ dc.ColumnName+ "]", drService[dc.ColumnName].ToString());
                        }

                        DataRow drOneService = dtService.NewRow();
                        drOneService["Date"] = drService["Date"].ToString();
                        drOneService["FullContent"] = sEachLayoutTemplate;
                        drOneService["DateAdded"] = drService["DBGDateAdded"].ToString();
                        drOneService["DateUpdated"] = drService["DBGDateUpdated"].ToString();
                        dtService.Rows.Add(drOneService);

                    }
                }

                dtService.AcceptChanges();


                if (dtLayoutDataAll != null && dtLayoutDataAll.Rows.Count > 0)
                {
                    foreach (DataRow drLayout in dtLayoutDataAll.Rows)
                    {
                        DataRow drOneLayout = dtLayout.NewRow();
                        drOneLayout["LayoutName"] = drLayout["Layout Name"].ToString();
                        drOneLayout["Template"] = drLayout["Template"].ToString();
                        drOneLayout["Stylesheet"] = drLayout["Stylesheet"].ToString();
                        drOneLayout["DateAdded"] = drLayout["DBGDateAdded"].ToString();
                        drOneLayout["DateUpdated"] = drLayout["DBGDateUpdated"].ToString();
                        dtLayout.Rows.Add(drOneLayout);
                    }
                }

                dtLayout.AcceptChanges();


                list.ServiceData = dtService;
                list.LayoutData = dtLayout;


            }



            return Request.CreateResponse(HttpStatusCode.OK, list);
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "api/wlttwdata", ex.Message, ex.StackTrace, DateTime.Now, "");
            SystemData.ErrorLog_Insert(theErrorLog);

            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Error." + ex.Message + ex.StackTrace);
        }

    }


   


    private string GetNullableString(string input)
    {
        return String.IsNullOrEmpty(input) ? null : input;
    }


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TableRights
/// </summary>
public class TableRights
{    
    public string CanInsert { get; set; }
    public string CanUpdate { get; set; }
    public string CanDelete { get; set; }
    public string CanSelect { get; set; }
    public string CanAccessAdminMenu { get; set; }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

public class ImageController : ApiController
{
    [Route("api/accounts/{accountId:int}/files")]
    [HttpPost]
    [BasicAuthentication]
    public HttpResponseMessage UploadFile(int accountId)
    {
        if (Request.Content.IsMimeMultipartContent())
        {
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count == 1)
            {
                var postedFile = httpRequest.Files[0];
                if (postedFile.FileName.Contains("."))
                {
                    string[] notAllowedExtensions = new string[] { ".exe", ".bat" };
                    string[] imageExtensions = new string[] { ".jpg", ".jpeg", ".png", ".gif" };
                    bool isAllowedFileType = !notAllowedExtensions.Contains(postedFile.FileName.Substring(postedFile.FileName.LastIndexOf(".")));

                    if (isAllowedFileType)
                    {
                        bool isImage = imageExtensions.Contains(postedFile.FileName.Substring(postedFile.FileName.LastIndexOf(".")));
                        Guid uniqueId = Guid.NewGuid();
                        string filesPhysicalRoot = SystemData.SystemOption_ValueByKey_Account("FilesPhisicalPath", null, null);
                        string filesDomain = SystemData.SystemOption_ValueByKey_Account("FilesLocation", null, null);
                        string container = isImage ? "AppFiles" : "AppFiles";
                        string savePath = String.Format(@"{0}\UserFiles\{1}\{2}-{3}", filesPhysicalRoot, container, uniqueId, postedFile.FileName);
                        string thumbPath = String.Format(@"{0}\UserFiles\{1}\thumb-{2}-{3}", filesPhysicalRoot, container, uniqueId, postedFile.FileName);
                        string publicUrl = String.Format(@"{0}/UserFiles/{1}/{2}-{3}", filesDomain, container, uniqueId, postedFile.FileName);
                        string strFileName = uniqueId + "-" + postedFile.FileName;
                        //string savePath = String.Format(@"D:\{0}-{1}",  uniqueId, postedFile.FileName);
                        //string thumbPath = String.Format(@"D:\thumb-{0}-{1}",  uniqueId, postedFile.FileName);
                        //string publicUrl = String.Format(@"{0}/UserFiles/{1}/{2}-{3}", filesDomain, container, uniqueId, postedFile.FileName);
                        if (isImage)
                        {
                            try
                            {
                                //Make sure that the file is a real image
                                using (var bitmap = new System.Drawing.Bitmap(postedFile.InputStream))
                                {
                                    try
                                    {
                                        bitmap.Save(savePath);
                                        SaveThumbnail(bitmap, thumbPath);
                                        result = Request.CreateResponse(HttpStatusCode.Created, strFileName);
                                    }
                                    catch
                                    {
                                        result = Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
                                    }
                                }
                            }
                            catch
                            {
                                result = Request.CreateResponse(HttpStatusCode.BadRequest, "Invalid image file");
                            }
                        }
                        else
                        {
                            // If uploaded is not image
                            try
                            {
                                postedFile.SaveAs(savePath);
                                result = Request.CreateResponse(HttpStatusCode.Created, strFileName);
                            }
                            catch
                            {
                                result = Request.CreateResponse(HttpStatusCode.InternalServerError, "Error");
                            }
                        }
                    }
                    else
                    {
                        result = Request.CreateResponse(HttpStatusCode.BadRequest, "File type is not allowed");
                    }                                         
                }                
            }
            
            if (result == null)
            {
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return result;
        }
        else
        {
            return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
        }        
    }
    #region Helper methods
    private void SaveThumbnail(System.Drawing.Bitmap bitmap, string thumbPath)
    {
        try
        {
            float ratio = 1;
            float minSize = Math.Min(75, 75);

            if (bitmap.Width > bitmap.Height)
            {
                ratio = minSize / (float)bitmap.Width;
            }
            else
            {
                ratio = minSize / (float)bitmap.Height;
            }

            SizeF newSize = new SizeF(bitmap.Width * ratio, bitmap.Height * ratio);
            Bitmap target = new Bitmap((int)newSize.Width, (int)newSize.Height);

            using (Graphics graphics = Graphics.FromImage(target))
            {
                graphics.CompositingQuality = CompositingQuality.HighSpeed;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.DrawImage(bitmap, 0, 0, newSize.Width, newSize.Height);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    target.Save(thumbPath);
                }
            }
        }
        catch (Exception) { }
    }

    
    #endregion
}

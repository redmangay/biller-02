﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
//using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web;
/// <summary>
/// Summary description for TheDatabase
/// </summary>
/// 


public static class EMD_Standardised_Field_Table
{
    public static string Sample_Type = "Sample Type";
    public static string Analyte_Name = "Analyte Name";
    public static string Decimals = "Decimals";
    public static string Ignore_Symbols = "Ignore Symbols";
    public static string Show_Graph = "Show Graph";

}


public static class TheDatabaseS
{
    public static string SystemNameFromColumnID(int iColumnID)
    {
        string strSystemName = "";
        strSystemName = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE ColumnID=" + iColumnID.ToString());
        return strSystemName;
    }

    public static void PopulateMenuDDL(ref DropDownList ddlShowUnder)
    {



        ddlShowUnder.Items.Clear();

        DataTable dtTopLevel = Common.DataTableFromText(@"SELECT MenuID,Menu FROM Menu WHERE IsActive=1 AND 
                AccountID=" + System.Web.HttpContext.Current.Session["AccountID"].ToString() +
                            @" AND ParentMenuID IS  NULL   AND(TableID IS NULL AND DocumentID IS NULL AND ExternalPageLink IS NULL AND Menu<>'---') ORDER BY DisplayOrder");

        foreach (DataRow drTop in dtTopLevel.Rows)
        {
            ListItem liItem = new ListItem(drTop["Menu"].ToString(), drTop["MenuID"].ToString());
            ddlShowUnder.Items.Add(liItem);

            PopulateSubMenu(ref ddlShowUnder, int.Parse(drTop["MenuID"].ToString()), 0);

        }
        //add top level in UI please
        //ListItem liTop = new ListItem("-- Top Level --", "");
        //ddlShowUnder.Items.Insert(0, liTop);
    }



    static void PopulateSubMenu(ref DropDownList ddlShowUnder, int iParentMenuID, int iLD)
    {
        DataTable dtSubMenu = Common.DataTableFromText(@"SELECT MenuID,Menu FROM Menu WHERE IsActive=1 AND 
                AccountID=" + System.Web.HttpContext.Current.Session["AccountID"].ToString() + @" AND ParentMenuID=" + iParentMenuID.ToString()
                            + @"  AND (TableID IS NULL AND DocumentID IS NULL AND ExternalPageLink IS NULL AND Menu<>'---')  ORDER BY DisplayOrder");

        iLD = iLD + 1;
        string strLD = "";
        for (int i = 1; i <= iLD; i++)
        {
            strLD = strLD + "-";
        }

        foreach (DataRow drSubMenu in dtSubMenu.Rows)
        {
            ListItem liItem = new ListItem(strLD + drSubMenu["Menu"].ToString(), drSubMenu["MenuID"].ToString());
            ddlShowUnder.Items.Add(liItem);
            PopulateSubMenu(ref ddlShowUnder, int.Parse(drSubMenu["MenuID"].ToString()), iLD);
        }
    }


    public static string spGetParentRecordID(int nChildRecordID)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("spGetParentRecordID", connection))
            {

                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nChildRecordID", nChildRecordID));


                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                System.Data.DataSet ds = new System.Data.DataSet();


                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();


                if (ds == null) return "";


                if (ds.Tables[0].Rows[0][0] == DBNull.Value)
                {
                    return "";
                }
                else
                {
                    return ds.Tables[0].Rows[0][0].ToString();
                }



            }
        }
    }



    public static string spGetValueFromRelatedTable(int nParentRecordID, int nRequiredTableID, string sRequiredColumnName)
    {

        try
        {

            using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                using (SqlCommand command = new SqlCommand("spGetValueFromRelatedTable", connection))
                {

                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.Add(new SqlParameter("@nParentRecordID", nParentRecordID));
                    command.Parameters.Add(new SqlParameter("@nRequiredTableID", nRequiredTableID));
                    command.Parameters.Add(new SqlParameter("@sRequiredColumnName", sRequiredColumnName));


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = command;
                    DataTable dt = new DataTable();
                    System.Data.DataSet ds = new System.Data.DataSet();


                    connection.Open();
                    try
                    {
                        da.Fill(ds);
                    }
                    catch
                    {
                        //
                    }
                    connection.Close();
                    connection.Dispose();


                    if (ds == null) return "";

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0][0] == DBNull.Value)
                        {
                            return "";
                        }
                        else
                        {
                            return ds.Tables[0].Rows[0][0].ToString();
                        }
                    }


                    return "";

                }
            }
        }
        catch
        {
            return "";
        }
    }


    public static int spUpdateRelatedTable(int nParentRecordID, int nRequiredTableID, string sRequiredColumnName, string sValue)
    {



        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("spUpdateRelatedTable", connection))
            {

                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nParentRecordID", nParentRecordID));
                command.Parameters.Add(new SqlParameter("@nRequiredTableID", nRequiredTableID));
                command.Parameters.Add(new SqlParameter("@sRequiredColumnName", sRequiredColumnName));
                command.Parameters.Add(new SqlParameter("@sValue", sValue));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;



            }
        }
    }


    public static int Column_ReplaceDisplayColumn(int TableID, string OldColumnName, string NewColumnName)
    {



        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Column_ReplaceDisplayColumn", connection))
            {

                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@TableID", TableID));
                command.Parameters.Add(new SqlParameter("@OldColumnName", OldColumnName));
                command.Parameters.Add(new SqlParameter("@NewColumnName", NewColumnName));


                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;



            }
        }
    }



    public static int Table_TableNameRename(int TableID, string OldName, string NewName)
    {



        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Table_TableNameRename", connection))
            {

                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@TableID", TableID));
                command.Parameters.Add(new SqlParameter("@OldName", OldName));
                command.Parameters.Add(new SqlParameter("@NewName", NewName));


                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;



            }
        }
    }



    public static DataTable spExportAllTables(int? nTableID)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_ExportRelatedData", connection)) //spExportAllTables
            {
                command.CommandType = CommandType.StoredProcedure;

                command.CommandTimeout = DBGurus.GetCommandTimeout();

                if (nTableID != null)
                    command.Parameters.Add(new SqlParameter("@ParentTableTableID", nTableID));



                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                System.Data.DataSet ds = new System.Data.DataSet();
                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();


                if (ds == null) return null;

                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
                {
                    return null;
                }
            }
        }
    }


    // This presumes that weeks start with Monday.
    // Week 1 is the 1st week of the year with a Thursday in it.
    public static int GetIso8601WeekOfYear(DateTime time)
    {
        // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
        // be the same week# as whatever Thursday, Friday or Saturday are,
        // and we always get those right
        DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
        if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
        {
            time = time.AddDays(3);
        }

        // Return the week of our adjusted day
        return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
    }

    public static int spAuditRawToAudit(int ProcessRecordID)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("spAuditRawToAudit", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@ProcessRecordID ", ProcessRecordID));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;

            }
        }
    }

    public static string EscapeStringValue(string s)
    {
        if (s == null || s.Length == 0)
        {
            return "";
        }

        char c = '\0';
        int i;
        int len = s.Length;
        System.Text.StringBuilder sb = new System.Text.StringBuilder(len + 4);
        String t;

        for (i = 0; i < len; i += 1)
        {
            c = s[i];
            switch (c)
            {
                case '\\':
                case '"':
                    sb.Append('\\');
                    sb.Append(c);
                    break;
                case '/':
                    sb.Append('\\');
                    sb.Append(c);
                    break;
                case '\b':
                    sb.Append("\\b");
                    break;
                case '\t':
                    sb.Append("\\t");
                    break;
                case '\n':
                    sb.Append("\\n");
                    break;
                case '\f':
                    sb.Append("\\f");
                    break;
                case '\r':
                    sb.Append("\\r");
                    break;
                default:
                    if (c < ' ')
                    {
                        t = "000" + String.Format("X", c);
                        sb.Append("\\u" + t.Substring(t.Length - 4));
                    }
                    else
                    {
                        sb.Append(c);
                    }
                    break;
            }
        }
        return sb.ToString();
    }

    public static string GetCalculationFormula(int iTableID, string strFormula, DataTable dtSys)
    {
        strFormula = strFormula.ToLower();

        if (dtSys == null)
            dtSys = Common.DataTableFromText("SELECT SystemName FROM [Column] WHERE TableID=" + iTableID.ToString());

        foreach (DataRow dr in dtSys.Rows)
        {
            if (strFormula.IndexOf("removenonnumericchar") > -1)
            {
                strFormula = strFormula.Replace("dbo.removenonnumericchar([" + dr["SystemName"].ToString().ToLower() + "])", "ISNULL( NULLIF(dbo.RemoveNonNumericChar([" + dr["SystemName"].ToString().ToLower() + "]),''),0)");
            }
            else
            {

                strFormula = strFormula.Replace("[" + dr["SystemName"].ToString().ToLower() + "]", "ISNULL([" + dr["SystemName"].ToString().ToLower() + "],0)");
            }
        }


        return strFormula;

    }




    public static double ToJulianDate(this DateTime date)
    {
        //return date.ToOADate() + 2415018.5;
        //return date.ToJulianDate() ;
        return date.ToOADate();
    }

    public static List<int> ListIDsByCOALESCE(int iTableID)
    {

        try
        {


            using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                using (SqlCommand command = new SqlCommand("Get_RecordIDS_BY_TableID", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@nTableID", iTableID));
                    command.CommandTimeout = DBGurus.GetCommandTimeout();


                    List<int> lstIDs = new List<int>();

                    string strAllIDs = "";

                    connection.Open();

                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                strAllIDs = (string)reader[0];
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //
                    }

                    connection.Close();
                    connection.Dispose();
                    var elements = strAllIDs.Split(new[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);

                    // To Loop through
                    foreach (string items in elements)
                    {
                        lstIDs.Add(int.Parse(items));
                    }


                    return lstIDs;

                }
            }

        }
        catch
        {
            return null;
        }
    }

    public static List<int> ListOfIDs(string strSQL)
    {

        try
        {
            using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                using (SqlCommand command = new SqlCommand(strSQL, connection))
                {
                    command.CommandType = CommandType.Text;

                    connection.Open();
                    List<int> lstIDs = new List<int>();

                    try
                    {

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                lstIDs.Add((int)reader[0]);
                            }
                        }

                    }
                    catch
                    {
                        lstIDs = null;
                    }

                    connection.Close();
                    connection.Dispose();

                    return lstIDs;

                }
            }
        }
        catch
        {
            return null;
        }



    }
    //    public static string GetCalculationResult(DataTable _dtColumnsAll, string strCalculation, int? _iRecordID,Record theRecord, int? iParentRecordID,
    //        TempRecord theTempRecord)
    //    {
    //        if (theRecord == null && _iRecordID == null && theTempRecord==null)
    //            return "";


    //        string strResult = "";
    //        string strCalculationOrg = strCalculation;
    //        try
    //        {


    //            if (_iRecordID != null)
    //                theRecord = RecordManager.ets_Record_Detail_Full((int)_iRecordID);
    //            //DataTable _dtColumnsAll = RecordManager.ets_Table_Columns_All((int)theRecord.TableID, null, null);

    //            for (int j = 0; j < _dtColumnsAll.Rows.Count; j++)
    //            {
    //                if (strCalculation.ToUpper().IndexOf("[" + _dtColumnsAll.Rows[j]["SystemName"].ToString().ToUpper() + "]") > -1)
    //                {
    //                    string strValue = "";

    //                    if (theRecord!=null)
    //                    {
    //                        strValue = RecordManager.GetRecordValue(ref theRecord, _dtColumnsAll.Rows[j]["SystemName"].ToString());
    //                    }
    //                    else
    //                    {
    //                        strValue = UploadManager.GetTempRecordValue(ref theTempRecord, _dtColumnsAll.Rows[j]["SystemName"].ToString());
    //                    }


    //                    if (!string.IsNullOrEmpty(strValue))
    //                    {

    //                        if (strValue.IndexOf(" ") > -1)
    //                        {
    //                            strValue = strValue.Substring(0, strValue.IndexOf(" "));
    //                            strValue = strValue.Trim();

    //                        }
    //                        strValue = Common.IgnoreSymbols(strValue);
    //                        strCalculation = strCalculation.ToUpper().Replace("[" + _dtColumnsAll.Rows[j]["SystemName"].ToString().ToUpper() + "]", strValue);
    //                    }
    //                }
    //            }

    //            if (iParentRecordID == null && theTempRecord==null && strCalculation.IndexOf("[") > -1 && strCalculation.IndexOf(":") > -1)
    //            {
    //                string strParentTableName = strCalculation.Substring(strCalculation.IndexOf("[") + 1, strCalculation.IndexOf(":") - 1);

    //                if (strParentTableName != "")
    //                {

    //                    Table theTable = RecordManager.ets_Table_Details((int)theRecord.TableID);
    //                    string strParentSys = Common.GetValueFromSQL(@" SELECT SystemName FROM [Column] WHERE TableID=" + theTable.TableID.ToString() + @" AND TableTableID=(SELECT Top 1 TableID FROM [Table] WHERE IsActive=1
    //                                AND AccountID=" + theTable.AccountID.ToString() + @"  and LinkedParentColumnID IS NOT NULL 
    //                                    AND (columntype='dropdown' or columntype='listbox')
    //                                    AND TableName='" + strParentTableName.Replace("'", "''") + "')");


    //                    if (strParentSys != "")
    //                    {
    //                        string strParentID = RecordManager.GetRecordValue(ref theRecord, strParentSys);
    //                        if (strParentID != "")
    //                        {
    //                            int iTemp = 0;
    //                            int.TryParse(strParentID, out iTemp);
    //                            if (iTemp > 0)
    //                            {
    //                                iParentRecordID = iTemp;
    //                            }

    //                        }
    //                    }
    //                }

    //            }

    //            if (iParentRecordID != null)
    //            {
    //                //string strParentTableID = Common.GetValueFromSQL("SELECT TableID FROM [Record] WHERE RecordID=" + iParentRecordID.ToString());
    //                Record theParentRecord = RecordManager.ets_Record_Detail_Full((int)iParentRecordID);

    //                Table theParnetTable = RecordManager.ets_Table_Details((int)theParentRecord.TableID);
    //                DataTable dtRecordTypleColumlnsP = RecordManager.ets_Table_Columns_All((int)theParnetTable.TableID);

    //                for (int j = 0; j < dtRecordTypleColumlnsP.Rows.Count; j++)
    //                {
    //                    if (strCalculation.ToUpper().IndexOf("[" + theParnetTable.TableName.ToUpper() + ":" + dtRecordTypleColumlnsP.Rows[j]["SystemName"].ToString().ToUpper() + "]") > -1)
    //                    {
    //                        string strValue = RecordManager.GetRecordValue(ref theParentRecord, dtRecordTypleColumlnsP.Rows[j]["SystemName"].ToString());
    //                        if (!string.IsNullOrEmpty(strValue))
    //                        {
    //                            //lets convert it to datetime

    //                            if (strValue.IndexOf(" ") > -1)
    //                            {
    //                                strValue = strValue.Substring(0, strValue.IndexOf(" "));
    //                                strValue = strValue.Trim();

    //                            }
    //                            strValue = Common.IgnoreSymbols(strValue);
    //                            strCalculation = strCalculation.ToUpper().Replace("[" + theParnetTable.TableName.ToUpper() + ":" + dtRecordTypleColumlnsP.Rows[j]["SystemName"].ToString().ToUpper() + "]", strValue);
    //                        }
    //                    }

    //                }
    //            }

    //            strCalculation = strCalculation.Replace("[", "");
    //            strCalculation = strCalculation.Replace("]", "");


    //            if (strCalculation.ToLower().IndexOf("RemoveNonNumericChar(v".ToLower()) > -1
    //                || strCalculation.ToLower() == strCalculationOrg.ToLower())
    //            {

    //                return "";
    //            }
    //            else
    //            {
    //                strResult = Common.GetValueFromSQL("SELECT " + strCalculation);
    //            }

    //        }
    //        catch
    //        {
    //            //
    //        }

    //        return strResult;
    //    }

//    public static string GetCalculationResult(ref DataTable _dtColumnsAll, string strCalculation, int? _iRecordID, Record theRecord, int? iParentRecordID,
//      TempRecord theTempRecord, Table theTable, Column theColumn)
//    {
//        if (theRecord == null && _iRecordID == null && theTempRecord == null)
//            return "";


//        string strResult = "";
//        string strCalculationOrg = strCalculation;
//        try
//        {


//            if (_iRecordID != null)
//                theRecord = RecordManager.ets_Record_Detail_Full((int)_iRecordID, null, false);
//            //DataTable _dtColumnsAll = RecordManager.ets_Table_Columns_All((int)theRecord.TableID, null, null);

//            for (int j = 0; j < _dtColumnsAll.Rows.Count; j++)
//            {
//                //KG 9/5/17 Ticket 3166 - populate the iParentRecordID variable if it's empty so the calculation will work
//                if (iParentRecordID == null)
//                {
//                    if (_dtColumnsAll.Rows[j]["LinkedParentColumnID"].ToString() != "" && theTempRecord != null)
//                    {
//                        string strLinkedParentColumnID = UploadManager.GetTempRecordValue(ref theTempRecord, _dtColumnsAll.Rows[j]["SystemName"].ToString());
//                        if (strLinkedParentColumnID != "")
//                        {
//                            iParentRecordID = int.Parse(strLinkedParentColumnID);
//                        }
//                    }
//                }

//                if (strCalculation.ToUpper().IndexOf("[" + _dtColumnsAll.Rows[j]["SystemName"].ToString().ToUpper() + "]") > -1)
//                {
//                    string strValue = "";

//                    if (theRecord != null)
//                    {
//                        strValue = RecordManager.GetRecordValue(ref theRecord, _dtColumnsAll.Rows[j]["SystemName"].ToString());
//                    }
//                    else
//                    {
//                        strValue = UploadManager.GetTempRecordValue(ref theTempRecord, _dtColumnsAll.Rows[j]["SystemName"].ToString());
//                    }


//                    if (string.IsNullOrEmpty(strValue))
//                    {
//                        if (theColumn.CalculationNullAsZero.HasValue && theColumn.CalculationNullAsZero.Value)
//                            strValue = "0";
//                        else
//                            return "";
//                    }

//                    if (strValue.IndexOf(" ") > -1)
//                    {
//                        strValue = strValue.Substring(0, strValue.IndexOf(" "));
//                        strValue = strValue.Trim();

//                    }
//                    //KG 28/11/17 Ticket 3101 - Allow non-numeric values in formula
//                    if (_dtColumnsAll.Rows[j]["ColumnType"].ToString() == "number" ||
//                        _dtColumnsAll.Rows[j]["ColumnType"].ToString() == "calculation" || theColumn.TextType == "n")
//                    {
//                        if (strValue.StartsWith("<") && _dtColumnsAll.Rows[j]["IgnoreSymbolMode"] != DBNull.Value)
//                        {
//                            strValue = RecordManager.AdjustSpecialValue(strValue,
//                                _dtColumnsAll.Rows[j]["IgnoreSymbolMode"] != DBNull.Value
//                                    ? _dtColumnsAll.Rows[j]["IgnoreSymbolMode"].ToString()
//                                    : null,
//                                _dtColumnsAll.Rows[j]["IgnoreSymbolConstant"] != DBNull.Value
//                                    ? _dtColumnsAll.Rows[j]["IgnoreSymbolConstant"].ToString()
//                                    : null);
//                        }
//                        else
//                            strValue = Common.IgnoreSymbols(strValue);
//                    }
//                    else
//                        strValue = "'" + strValue + "'";

//                    strCalculation = strCalculation.ToUpper().Replace("[" + _dtColumnsAll.Rows[j]["SystemName"].ToString().ToUpper() + "]", strValue);

//                }
//            }

//            if (iParentRecordID == null && theTempRecord == null && strCalculation.IndexOf("[") > -1 && strCalculation.IndexOf(":") > -1)
//            {
//                string strParentTableName = strCalculation.Substring(strCalculation.IndexOf("[") + 1, strCalculation.IndexOf(":") - strCalculation.IndexOf("[") - 1);

//                if (strParentTableName != "")
//                {

//                    if (theTable == null)
//                        theTable = RecordManager.ets_Table_Details((int)theRecord.TableID);

//                    string strParentSys = Common.GetValueFromSQL(@" SELECT SystemName FROM [Column] WHERE TableID=" + theTable.TableID.ToString() + @" AND TableTableID=(SELECT Top 1 TableID FROM [Table] WHERE IsActive=1
//                                AND AccountID=" + theTable.AccountID.ToString() + @"  and LinkedParentColumnID IS NOT NULL 
//                                    AND (columntype='dropdown' or columntype='listbox')
//                                    AND TableName='" + strParentTableName.Replace("'", "''") + "')");


//                    if (strParentSys != "")
//                    {
//                        string strParentID = RecordManager.GetRecordValue(ref theRecord, strParentSys);
//                        if (strParentID != "")
//                        {
//                            int iTemp = 0;
//                            int.TryParse(strParentID, out iTemp);
//                            if (iTemp > 0)
//                            {
//                                iParentRecordID = iTemp;
//                            }

//                        }
//                    }
//                }

//            }

//            if (iParentRecordID != null)
//            {
//                //string strParentTableID = Common.GetValueFromSQL("SELECT TableID FROM [Record] WHERE RecordID=" + iParentRecordID.ToString());
//                Record theParentRecord = RecordManager.ets_Record_Detail_Full((int)iParentRecordID, null, false);

//                Table theParnetTable = RecordManager.ets_Table_Details((int)theParentRecord.TableID);
//                DataTable dtRecordTypleColumlnsP = RecordManager.ets_Table_Columns_All((int)theParnetTable.TableID);

//                for (int j = 0; j < dtRecordTypleColumlnsP.Rows.Count; j++)
//                {
//                    if (strCalculation.ToUpper().IndexOf("[" + theParnetTable.TableName.ToUpper() + ":" + dtRecordTypleColumlnsP.Rows[j]["SystemName"].ToString().ToUpper() + "]") > -1)
//                    {
//                        string strValue = RecordManager.GetRecordValue(ref theParentRecord, dtRecordTypleColumlnsP.Rows[j]["SystemName"].ToString());
//                        if (string.IsNullOrEmpty(strValue))
//                        {
//                            strValue = "0";
//                        }
//                        //lets convert it to datetime

//                        if (strValue.IndexOf(" ") > -1)
//                        {
//                            strValue = strValue.Substring(0, strValue.IndexOf(" "));
//                            strValue = strValue.Trim();

//                        }
//                        //KG 28/11/17 Ticket 3101 - Allow non-numeric values in formula
//                        //if (_dtColumnsAll.Rows[j]["ColumnType"].ToString() == "number" || _dtColumnsAll.Rows[j]["ColumnType"].ToString() == "calculation")
//                        //    strValue = Common.IgnoreSymbols(strValue);
//                        //else
//                        //    strValue = "'" + strValue + "'";
//                        strValue = Common.IgnoreSymbols(strValue);
//                        strCalculation = strCalculation.ToUpper().Replace("[" + theParnetTable.TableName.ToUpper() + ":" + dtRecordTypleColumlnsP.Rows[j]["SystemName"].ToString().ToUpper() + "]", strValue);

//                    }

//                }
//            }

//            strCalculation = strCalculation.Replace("[", "");
//            strCalculation = strCalculation.Replace("]", "");


//            if (strCalculation.ToLower().IndexOf("RemoveNonNumericChar(v".ToLower()) > -1
//                || strCalculation.ToLower() == strCalculationOrg.ToLower())
//            {

//                return "";
//            }
//            else if (strCalculation.ToLower().IndexOf("v".ToLower()) > -1)
//            {
//                return "";
//            }
//            else
//            {
//                //strResult = Common.GetValueFromSQL("SELECT " + strCalculation);
//                //strResult = Common.EvaluateCalculationFormula(strCalculation);

//                //Red Ticket 2567 10-05-2017 -- meantime this is just for IF statement, we can expand this to advanced query
//                if (strCalculation.ToLower().IndexOf("else select".ToLower()) > -1)
//                {
//                    //Calculation SQL query
//                    strResult = Common.GetValueFromSQL(strCalculation);

//                }
//                else
//                {
//                    strResult = Common.EvaluateCalculationFormula(strCalculation);
//                }
//                //end Red
//                if (theColumn.RoundNumber != null)
//                {
//                    strResult = Math.Round(double.Parse(Common.IgnoreSymbols(strResult)), (int)theColumn.RoundNumber).ToString("N" + theColumn.RoundNumber.ToString());
//                }
//            }

//        }
//        catch
//        {
//            //
//        }
//        if (strResult.ToLower() == "nan")
//        {
//            strResult = "";
//        }
//        return strResult;
//    }





    public static string GetCalculationResult(ref DataTable _dtColumnsAll, string strCalculation, int? _iRecordID, Record theRecord, int? iParentRecordID,
    TempRecord theTempRecord, Table theTable, Column theColumn)
    {
        if (theRecord == null && _iRecordID == null && theTempRecord == null)
            return "";


        string strResult = "";
        string strCalculationOrg = strCalculation;
        try
        {


            if (_iRecordID != null)
                theRecord = RecordManager.ets_Record_Detail_Full((int)_iRecordID, null, false);

            Regex rx = new Regex(@"\[([^]]*)\]");
            MatchCollection matches = rx.Matches(strCalculation);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    string key = match.Groups[1].Value;

                    if (key.ToUpper().IndexOf("V") == -1)
                    {
                        continue;
                    }

                    if(key.IndexOf(":")>-1)
                    {
                        //parent table
                        string strParentTableName = key.Substring(0, key.IndexOf(":"));
                        string strParentSystemName = key.Substring(key.IndexOf(":") + 1);

                        string strThisTableParentSys = Common.GetValueFromSQL(@" SELECT SystemName FROM [Column] WHERE TableID=" + theTable.TableID.ToString() + @" AND TableTableID=(SELECT Top 1 TableID FROM [Table] WHERE IsActive=1
                                AND AccountID=" + theTable.AccountID.ToString() + @"  and LinkedParentColumnID IS NOT NULL 
                                    AND (columntype='dropdown' or columntype='listbox')
                                    AND TableName='" + strParentTableName.Replace("'", "''") + "')");

                        if (strThisTableParentSys != "")
                        {
                            string strParentRecordID = RecordManager.GetRecordValue(ref theRecord, strThisTableParentSys);
                            if (strParentRecordID != "")
                            {
                                int iTemp = 0;
                                int.TryParse(strParentRecordID, out iTemp);
                                if (iTemp > 0)
                                {
                                    try
                                    {
                                        string strParentValue = Common.GetValueFromSQL("SELECT " + strParentSystemName + " FROM [Record] WHERE RecordID=" + strParentRecordID);
                                        if (string.IsNullOrEmpty(strParentValue))
                                            {
                                                strParentValue = "0";
                                            }
                                         //lets convert it to datetime

                                            if (strParentValue.IndexOf(" ") > -1)
                                            {
                                                strParentValue = strParentValue.Substring(0, strParentValue.IndexOf(" "));
                                                strParentValue = strParentValue.Trim();

                                            }
                                            strParentValue = Common.IgnoreSymbols(strParentValue);
                                            strCalculation = strCalculation.ToUpper().Replace("[" + strParentTableName.ToUpper() + ":" + strParentSystemName.ToUpper() + "]", strParentValue);

                                    }
                                    catch
                                    {
                                        //
                                    }
                                }
                            }
                        }                        
                    }
                    else
                    {
                        //main record
                        if (strCalculation.ToUpper().IndexOf("[" + key.ToString().ToUpper() + "]") > -1)
                        {
                            string strValue = "";

                            if (theRecord != null)
                            {
                                strValue = RecordManager.GetRecordValue(ref theRecord, key.ToString());
                            }
                            else
                            {
                                strValue = UploadManager.GetTempRecordValue(ref theTempRecord, key.ToString());
                            }

                            if (string.IsNullOrEmpty(strValue))
                            {
                                if (theColumn.CalculationNullAsZero.HasValue && theColumn.CalculationNullAsZero.Value)
                                    strValue = "0";
                                else
                                    return "";
                            }

                            if (strValue.IndexOf(" ") > -1)
                            {
                                strValue = strValue.Substring(0, strValue.IndexOf(" "));
                                strValue = strValue.Trim();
                            }


                            string strColumnID=Common.GetValueFromSQL("SELECT ColumnID FROM [Column] WHERE TableID="+theTable.TableID.ToString()
                                +" AND SystemName='"+key.ToString()+"'");

                            if(!string.IsNullOrEmpty(strColumnID))
                            {
                                Column theCalColumn=RecordManager.ets_Column_Details(int.Parse(strColumnID));
                                if(theCalColumn!=null)
                                {
                                    if(theCalColumn.ColumnType=="number" || theCalColumn.ColumnType=="calculation" || theCalColumn.TextType=="n")
                                    {
                                        if (strValue.StartsWith("<") && theCalColumn.IgnoreSymbolMode != null)
                                        {
                                            strValue = RecordManager.AdjustSpecialValue(strValue,
                                                theCalColumn.IgnoreSymbolMode!= null
                                                    ? theCalColumn.IgnoreSymbolMode.ToString()
                                                    : null,
                                                theCalColumn.IgnoreSymbolConstant != null
                                                    ? theCalColumn.IgnoreSymbolConstant.ToString()
                                                    : null);
                                        }
                                        else
                                        {
                                             strValue = Common.IgnoreSymbols(strValue);
                                        }
                                    }
                                    else
                                    {
                                        strValue = "'" + strValue + "'";
                                    }
                                }
                            }

                            strCalculation = strCalculation.ToUpper().Replace("[" + key.ToString().ToUpper() + "]", strValue);

                        }
                    }
                }
            }



            strCalculation = strCalculation.Replace("[", "");
            strCalculation = strCalculation.Replace("]", "");


            if (strCalculation.ToLower().IndexOf("RemoveNonNumericChar(v".ToLower()) > -1
                || strCalculation.ToLower() == strCalculationOrg.ToLower())
            {

                return "";
            }
            else if (strCalculation.ToLower().IndexOf("v".ToLower()) > -1)
            {
                return "";
            }
            else
            {
                //strResult = Common.GetValueFromSQL("SELECT " + strCalculation);
                //strResult = Common.EvaluateCalculationFormula(strCalculation);

                //Red Ticket 2567 10-05-2017 -- meantime this is just for IF statement, we can expand this to advanced query
                if (strCalculation.ToLower().IndexOf("else select".ToLower()) > -1)
                {
                    //Calculation SQL query
                    strResult = Common.GetValueFromSQL(strCalculation);

                }
                else
                {
                    strResult = Common.EvaluateCalculationFormula(strCalculation);
                }
                //end Red
                if (theColumn.RoundNumber != null)
                {
                    strResult = Math.Round(double.Parse(Common.IgnoreSymbols(strResult)), (int)theColumn.RoundNumber).ToString("N" + theColumn.RoundNumber.ToString());
                }
            }

        }
        catch
        {
            //
        }
        if (strResult.ToLower() == "nan")
        {
            strResult = "";
        }
        return strResult;
    }



    public static string GetTextCalculationResult(ref DataTable _dtColumnsAll, string strCalculation, int? _iRecordID, Record theRecord, int? iParentRecordID,
   TempRecord theTempRecord, Table theTable, Column theColumn)
    {
        if (theRecord == null && _iRecordID == null && theTempRecord == null)
            return "";


        string strResult = "";
        string strCalculationOrg = strCalculation;
        try
        {


            if (_iRecordID != null)
                theRecord = RecordManager.ets_Record_Detail_Full((int)_iRecordID, null, false);

            Regex rx = new Regex(@"\[([^]]*)\]");
            MatchCollection matches = rx.Matches(strCalculation);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    string key = match.Groups[1].Value;
                    if (key.IndexOf(":") > -1)
                    {
                        //parent table
                        string strParentTableName = key.Substring(0, key.IndexOf(":"));
                        string strParentSystemName = key.Substring(key.IndexOf(":") + 1);

                        string strThisTableParentSys = Common.GetValueFromSQL(@" SELECT SystemName FROM [Column] WHERE TableID=" + theTable.TableID.ToString() + @" AND TableTableID=(SELECT Top 1 TableID FROM [Table] WHERE IsActive=1
                                AND AccountID=" + theTable.AccountID.ToString() + @"  and LinkedParentColumnID IS NOT NULL 
                                    AND (columntype='dropdown' or columntype='listbox')
                                    AND TableName='" + strParentTableName.Replace("'", "''") + "')");

                        if (strThisTableParentSys != "")
                        {
                            string strParentRecordID = RecordManager.GetRecordValue(ref theRecord, strThisTableParentSys);
                            if (strParentRecordID != "")
                            {
                                int iTemp = 0;
                                int.TryParse(strParentRecordID, out iTemp);
                                if (iTemp > 0)
                                {
                                    try
                                    {
                                        string strParentValue = Common.GetValueFromSQL("SELECT " + strParentSystemName + " FROM [Record] WHERE RecordID=" + strParentRecordID);
                                        if (string.IsNullOrEmpty(strParentValue))
                                        {
                                            strParentValue = "";
                                        }                                        
                                        strCalculation = strCalculation.ToUpper().Replace("[" + strParentTableName.ToUpper() + ":" + strParentSystemName.ToUpper() + "]", strParentValue);

                                    }
                                    catch
                                    {
                                        //
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //main record
                        if (strCalculation.ToUpper().IndexOf("[" + key.ToString().ToUpper() + "]") > -1)
                        {
                            string strValue = "";

                            if (theRecord != null)
                            {
                                strValue = RecordManager.GetRecordValue(ref theRecord, key.ToString());
                            }
                            else
                            {
                                strValue = UploadManager.GetTempRecordValue(ref theTempRecord, key.ToString());
                            }

                            if (string.IsNullOrEmpty(strValue))
                            {
                                if (theColumn.CalculationNullAsZero.HasValue && theColumn.CalculationNullAsZero.Value)
                                    strValue = "";
                                else
                                    return "";
                            }                                                      


                            strCalculation = strCalculation.ToUpper().Replace("[" + key.ToString().ToUpper() + "]", strValue);

                        }
                    }
                }
            }



            strCalculation = strCalculation.Replace("[", "");
            strCalculation = strCalculation.Replace("]", "");


            if (strCalculation.ToLower().IndexOf("RemoveNonNumericChar(v".ToLower()) > -1
                || strCalculation.ToLower() == strCalculationOrg.ToLower())
            {

                return "";
            }
            else if (strCalculation.ToLower().IndexOf("v".ToLower()) > -1)
            {
                return "";
            }
            else
            {

                strResult = strCalculation;                
            }

        }
        catch
        {
            //
        }
        if (strResult.ToLower() == "nan")
        {
            strResult = "";
        }
        return strResult;
    }






    public static string GetDateCalculationResult(ref DataTable _dtColumnsAll, string strCalculation, int? _iRecordID, Record theRecord, int? iParentRecordID, string strDateCalculationType,
      TempRecord theTempRecord, Table theTable, bool? bCheckIgnoreMidnight)
    {
        if (theRecord == null && _iRecordID == null && theTempRecord == null)
            return "";


        string strResult = "";
        // bool bCheckIgnoreMidnight = false;

        try
        {

            if (_iRecordID != null)
                theRecord = RecordManager.ets_Record_Detail_Full((int)_iRecordID, null, false);

            if (theTable == null)
                theTable = RecordManager.ets_Table_Details(theRecord != null ? (int)theRecord.TableID : (int)theTempRecord.TableID);


            if (bCheckIgnoreMidnight == null)
            {
                bCheckIgnoreMidnight = false;
                string strIgnoreMidnight = SystemData.SystemOption_ValueByKey_Account("Time Calculation Ignore Midnight", (int)theTable.AccountID, theTable.TableID);

                if (strIgnoreMidnight != "" && strIgnoreMidnight.ToString().ToLower() == "yes")
                {
                    bCheckIgnoreMidnight = true;
                }
            }



            //DataTable _dtColumnsAll = RecordManager.ets_Table_Columns_All((int)theRecord.TableID, null, null);

            string strResultFormat = "day";
            if (strDateCalculationType != "")
                strResultFormat = strDateCalculationType;
            //if (_dtRecordTypleColumlns.Rows[i]["DateCalculationType"] != DBNull.Value)
            //    strResultFormat = _dtRecordTypleColumlns.Rows[i]["DateCalculationType"].ToString().ToLower();

            //check if it has "time"

            bool bHasTime = false;

            for (int j = 0; j < _dtColumnsAll.Rows.Count; j++)
            {
                if (strCalculation.ToUpper().IndexOf("[" + _dtColumnsAll.Rows[j]["SystemName"].ToString().ToUpper() + "]") > -1)
                {
                    if (_dtColumnsAll.Rows[j]["ColumnType"].ToString() == "time")
                    {
                        bHasTime = true;
                    }
                }
            }



            Regex rx = new Regex(@"\[([^]]*)\]");
            MatchCollection matches = rx.Matches(strCalculation);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    string key = match.Groups[1].Value;

                    if(key.ToUpper().IndexOf("V")==-1)
                    {
                        continue;
                    }
                    if (key.IndexOf(":") > -1)
                    {
                        //parent table
                        string strParentTableName = key.Substring(0, key.IndexOf(":"));
                        string strParentSystemName = key.Substring(key.IndexOf(":") + 1);

                        string strThisTableParentSys = Common.GetValueFromSQL(@" SELECT SystemName FROM [Column] WHERE TableID=" + theTable.TableID.ToString() + @" AND TableTableID=(SELECT Top 1 TableID FROM [Table] WHERE IsActive=1
                                AND AccountID=" + theTable.AccountID.ToString() + @"  and LinkedParentColumnID IS NOT NULL 
                                    AND (columntype='dropdown' or columntype='listbox')
                                    AND TableName='" + strParentTableName.Replace("'", "''") + "')");

                        if (strThisTableParentSys != "")
                        {
                            string strParentRecordID = RecordManager.GetRecordValue(ref theRecord, strThisTableParentSys);
                            if (strParentRecordID != "")
                            {
                                int iTemp = 0;
                                int.TryParse(strParentRecordID, out iTemp);
                                if (iTemp > 0)
                                {
                                    try
                                    {
                                        string strParentValue = Common.GetValueFromSQL("SELECT " + strParentSystemName + " FROM [Record] WHERE RecordID=" + strParentRecordID);

                                        if (!string.IsNullOrEmpty(strParentValue))
                                        {
                                            //lets convert it to datetime
                                            string strParentTableID = Common.GetValueFromSQL("SELECT TableID FROM [Record] WHERE RecordID="+ strParentRecordID);
                                            string strParentColumnID = Common.GetValueFromSQL("SELECT ColumnID FROM [Column] WHERE TableID=" + strParentTableID
                                + " AND SystemName='" + strParentSystemName + "'");
                                            DateTime dtTempDateTime = DateTime.Today;
                                            Column theCalParentColumn = RecordManager.ets_Column_Details(int.Parse(strParentColumnID));
                                            try
                                            {
                                                dtTempDateTime = DateTime.Parse(strParentValue);

                                                if ((bool)bCheckIgnoreMidnight && dtTempDateTime.TimeOfDay.Ticks == 0)
                                                {
                                                    if (theCalParentColumn != null && theCalParentColumn.ColumnType != "date")
                                                        return "";
                                                }

                                                if (theCalParentColumn.ColumnType == "time")
                                                {
                                                    strParentValue = (TheDatabaseS.ToJulianDate(dtTempDateTime) - TheDatabaseS.ToJulianDate(DateTime.Today)).ToString();
                                                }
                                                else
                                                {
                                                    strParentValue = TheDatabaseS.ToJulianDate(dtTempDateTime).ToString();
                                                }

                                                strCalculation = strCalculation.ToUpper().Replace("[" + strParentTableName.ToUpper() + ":" + strParentSystemName.ToUpper() + "]", strParentValue);

                                            }
                                            catch
                                            {

                                            }
                                            //strParentValue = TheDatabaseS.ToJulianDate(dtTempDateTime).ToString();
                                            //strCalculation = strCalculation.ToUpper().Replace("[" + strParentTableName.ToUpper() + ":" + strParentSystemName.ToUpper() + "]", strParentValue);

                                        }                                      

                                    }
                                    catch
                                    {
                                        //
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //main record
                        if (strCalculation.ToUpper().IndexOf("[" + key.ToString().ToUpper() + "]") > -1)
                        {
                            string strValue = "";

                            if (theRecord != null)
                            {
                                strValue = RecordManager.GetRecordValue(ref theRecord, key.ToString());
                            }
                            else
                            {
                                strValue = UploadManager.GetTempRecordValue(ref theTempRecord, key.ToString());
                            }
                            
                            string strColumnID = Common.GetValueFromSQL("SELECT ColumnID FROM [Column] WHERE TableID=" + theTable.TableID.ToString()
                                + " AND SystemName='" + key.ToString() + "'");

                            if (!string.IsNullOrEmpty(strColumnID))
                            {
                                Column theCalColumn = RecordManager.ets_Column_Details(int.Parse(strColumnID));
                                if (theCalColumn != null)
                                {
                                    if (theCalColumn.ColumnType == "number" || theCalColumn.ColumnType == "calculation" || theCalColumn.TextType == "n")
                                    {
                                        strCalculation = strCalculation.ToUpper().Replace("[" + theCalColumn.SystemName.ToString().ToUpper() + "]", strValue);
                                        strCalculation = strCalculation.ToUpper().Replace("CAST(DBO.REMOVENONNUMERICCHAR(", "");
                                        strCalculation = strCalculation.ToUpper().Replace(") AS DECIMAL(20,10))", "");
                                        continue;
                                    }
                                    if (bHasTime)
                                    {
                                        if (strValue.IndexOf(" ") > -1)
                                        {
                                            strValue = strValue.Substring(strValue.IndexOf(" "));
                                            strValue = strValue.Trim();
                                        }
                                    }
                                    DateTime dtTempDateTime = DateTime.Today;
                                    try
                                    {
                                        if(!string.IsNullOrEmpty(strValue))
                                        {
                                            dtTempDateTime = DateTime.Parse(strValue);

                                            if ((bool)bCheckIgnoreMidnight && dtTempDateTime.TimeOfDay.Ticks == 0)
                                            {
                                                if (theCalColumn.ColumnType != "date")
                                                    return "";
                                            }
                                            if (theCalColumn.ColumnType == "time")
                                            {
                                                strValue = (TheDatabaseS.ToJulianDate(dtTempDateTime) - TheDatabaseS.ToJulianDate(DateTime.Today)).ToString();
                                            }
                                            else
                                            {
                                                strValue = TheDatabaseS.ToJulianDate(dtTempDateTime).ToString();
                                            }

                                            strCalculation = strCalculation.ToUpper().Replace("[" + key.ToString().ToUpper() + "]", strValue);

                                        }
                                    }
                                    catch
                                    {
                                        //
                                    }
                                    

                                }
                            }

                            

                        }
                    }
                }
            }

            
            if (strCalculation.IndexOf("[") > -1)
            {
                //we have number here
                string regex = Common.NumberRegExDC; //@"\[(.*?)\]";  // Common.NumberRegExDC; //@"^.*?\([^\d]*(\d+)[^\d]*\).*$"
                string text = strCalculation;
                double num;
                foreach (Match match in Regex.Matches(text, regex))
                {
                    string strEachNumber = match.Groups[1].Value;
                    string strEachJulianNumber = strEachNumber;


                    if (double.TryParse(strEachJulianNumber, out num))
                    {
                        // It's a number!
                    }
                    else
                    {
                        continue;
                    }

                    switch (strResultFormat)
                    {
                        case "datetime":
                            strEachJulianNumber = (double.Parse(strEachNumber) / (24 * 60)).ToString(); //minute

                            break;
                        case "date":
                            strEachJulianNumber = strEachNumber; //day
                            break;
                        case "time":
                            strEachJulianNumber = (double.Parse(strEachNumber) / (24 * 60)).ToString(); //minute
                            break;
                        case "minute":
                            strEachJulianNumber = (double.Parse(strEachNumber) / (24 * 60)).ToString(); //minute
                            break;
                        case "hour":
                            strEachJulianNumber = (double.Parse(strEachNumber) / (24)).ToString(); //hour
                            break;
                        case "day":
                            strEachJulianNumber = strEachNumber; //day
                            break;

                        case "ymd":
                            strEachJulianNumber = strEachNumber; //day



                            break;

                        case "ym":
                            strEachJulianNumber = strEachNumber; //day
                            break;

                        default:
                            strEachJulianNumber = strEachNumber; //day
                            break;
                    }

                    strCalculation = strCalculation.Replace("[" + strEachNumber + "]", strEachJulianNumber);
                }

            }


            if (strCalculation.ToLower().IndexOf("[v") > -1)
            {
                return "";
            }

            strCalculation = strCalculation.Replace("[", "");
            strCalculation = strCalculation.Replace("]", "");

            //string strJulianValue = Common.GetValueFromSQL("SELECT " + strCalculation);
            string strJulianValue = Common.EvaluateCalculationFormula(strCalculation);

            //implement Result Format

            if (strResultFormat != "")
            {

                strResult = strJulianValue;
                switch (strResultFormat)
                {
                    case "datetime":

                        DateTime dtResult = DateTime.FromOADate(double.Parse(strResult));
                        strResult = dtResult.ToString();
                        break;
                    case "date":
                        DateTime dResult = DateTime.FromOADate(double.Parse(strResult));
                        strResult = dResult.ToShortDateString();
                        break;
                    case "time":
                        string strOriginal = strResult;
                        DateTime tResult = DateTime.FromOADate(double.Parse(strResult));
                        string strDay = "";
                        if (strResult.IndexOf(".") > -1)
                        {
                            strDay = strResult.Substring(0, strResult.IndexOf("."));
                        }
                        else
                        {
                            strDay = strResult;
                        }
                        if (strDay == "0")
                        {
                            strDay = "";
                        }
                        else if (strDay == "-0")
                        {
                            strDay = "-";
                        }
                        else if (strDay == "1")
                        {
                            strDay = "1 day ";
                        }
                        else
                        {

                            strDay = strDay + " days ";
                        }


                        string strTimePart = tResult.ToLongTimeString();

                        strResult = strDay + strTimePart;
                        break;
                    case "minute":
                        double dMinutes = double.Parse(strJulianValue) * 24 * 60;
                        strResult = (Math.Round(dMinutes, 2)).ToString("N6");
                        break;
                    case "hour":
                        double dHours = double.Parse(strJulianValue) * 24;
                        strResult = (Math.Round(dHours, 2)).ToString("N6");
                        break;
                    case "day":
                        double dDays = double.Parse(strJulianValue);
                        strResult = (Math.Round(dDays, 2)).ToString("N6");
                        break;
                    case "ymd":

                        double totalDays = double.Parse(strJulianValue);
                        string totalYears = Math.Truncate(totalDays / 365).ToString();
                        string totalMonths = Math.Truncate((totalDays % 365) / 30).ToString();
                        string remainingDays = Math.Truncate((totalDays % 365) % 30).ToString();
                        strResult = totalYears + " years " + totalMonths + " months " + remainingDays + " days";
                        break;

                    case "ym":

                        double totalDays2 = double.Parse(strJulianValue);
                        string totalYears2 = Math.Truncate(totalDays2 / 365).ToString();
                        string totalMonths2 = Math.Truncate((totalDays2 % 365) / 30).ToString();

                        strResult = totalYears2 + " years " + totalMonths2 + " months";
                        break;

                    default:
                        break;
                }

            }
        }
        catch
        {
            //
        }
        if (strResult.ToLower() == "nan")
        {
            strResult = "";
        }

        return strResult;

    }
    
    
    


}
public class TheDatabase
{
    public TheDatabase()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static int MondaysInMonth(DateTime thisMonth)
    {
        int mondays = 0;
        int month = thisMonth.Month;
        int year = thisMonth.Year;
        int daysThisMonth = DateTime.DaysInMonth(year, month);
        DateTime beginingOfThisMonth = new DateTime(year, month, 1);
        for (int i = 0; i < daysThisMonth; i++)
            if (beginingOfThisMonth.AddDays(i).DayOfWeek == DayOfWeek.Monday)
                mondays++;
        return mondays;
    }
    public static bool IsDataInFilterRange(string strColumnType, string strNumberType, string strRecordValue,
       string strFilterValue, string strCompareOperator)
    {


        try
        {
            if (strCompareOperator == "")
                strCompareOperator = "=";

            if (strCompareOperator == "empty")
            {
                if (strRecordValue == "")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            if (strCompareOperator == "notempty")
            {
                if (strRecordValue != "")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            if (strRecordValue == "")
            {
                return false;
            }

            strColumnType = strColumnType.ToLower();
            if (strColumnType == "number")
            {
                if (strFilterValue.IndexOf("____") > -1)
                {
                    int iRecordValue = int.Parse(strRecordValue);
                    string strLowerLimit = strFilterValue.Substring(0, strFilterValue.IndexOf("____"));
                    string strUpperLimit = strFilterValue.Substring(strFilterValue.IndexOf("____") + 4);
                    if (strLowerLimit != "" && strUpperLimit != "")
                    {

                        if (iRecordValue >= int.Parse(strLowerLimit) && iRecordValue <= int.Parse(strUpperLimit))
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (strLowerLimit != "")
                        {
                            if (strCompareOperator == "=")
                            {
                                if (iRecordValue == int.Parse(strLowerLimit))
                                {
                                    return true;
                                }
                            }
                            if (strCompareOperator == ">")
                            {
                                if (iRecordValue > int.Parse(strLowerLimit))
                                {
                                    return true;
                                }
                            }
                            if (strCompareOperator == "<")
                            {
                                if (iRecordValue < int.Parse(strLowerLimit))
                                {
                                    return true;
                                }
                            }

                            if (strCompareOperator == "<>")
                            {
                                if (iRecordValue != int.Parse(strLowerLimit))
                                {
                                    return true;
                                }
                            }
                        }
                        else if (strUpperLimit != "")
                        {
                            if (iRecordValue <= int.Parse(strUpperLimit))
                            {
                                return true;
                            }
                        }

                    }

                }
            }
            else if (strColumnType == "datetime" || strColumnType == "date" || strColumnType == "time")
            {
                if (strFilterValue.IndexOf("____") > -1)
                {
                    DateTime dtRecordValue = DateTime.Parse(strRecordValue);
                    string strLowerLimit = strFilterValue.Substring(0, strFilterValue.IndexOf("____"));
                    string strUpperLimit = strFilterValue.Substring(strFilterValue.IndexOf("____") + 4);
                    strLowerLimit = Common.ReturnDateStringFromToken(strLowerLimit);
                    strUpperLimit = Common.ReturnDateStringFromToken(strUpperLimit);
                    if (strLowerLimit != "" && strUpperLimit != "")
                    {

                        if (dtRecordValue >= DateTime.Parse(strLowerLimit) && dtRecordValue <= DateTime.Parse(strUpperLimit))
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (strLowerLimit != "")
                        {
                            if (strCompareOperator == "=")
                            {
                                if (dtRecordValue == DateTime.Parse(strLowerLimit))
                                {
                                    return true;
                                }
                            }
                            if (strCompareOperator == ">")
                            {
                                if (dtRecordValue > DateTime.Parse(strLowerLimit))
                                {
                                    return true;
                                }
                            }
                            if (strCompareOperator == "<")
                            {
                                if (dtRecordValue < DateTime.Parse(strLowerLimit))
                                {
                                    return true;
                                }
                            }

                            if (strCompareOperator == "<>")
                            {
                                if (dtRecordValue != DateTime.Parse(strLowerLimit))
                                {
                                    return true;
                                }
                            }

                        }
                        else if (strUpperLimit != "")
                        {
                            if (dtRecordValue <= DateTime.Parse(strUpperLimit))
                            {
                                return true;
                            }
                        }

                    }

                }
            }
            else
            {
                if (strCompareOperator == "<>")
                {
                    if (strRecordValue != strFilterValue)
                    {
                        return true;
                    }
                }
                else
                {
                    if (strRecordValue == strFilterValue)
                    {
                        return true;
                    }
                }


            }
        }
        catch
        {
            return false;
        }



        return false;
    }

    public static void Load_Session_Current()
    {
        string strLoadSession = "";
        strLoadSession = TheDatabase.TDB_FilesLocation;
        strLoadSession = TheDatabase.TDB_FilesPhisicalPath;
    }
    public static string TDB_FilesLocation
    {
        get
        {
            try
            {
                if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null &&
                    System.Web.HttpContext.Current.Session["FilesLocation"] != null)
                {
                    return System.Web.HttpContext.Current.Session["FilesLocation"].ToString();
                }
                else
                {
                    string strValue = SystemData.SystemOption_ValueByKey_Account("FilesLocation", null, null);
                    if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null)
                    {
                        System.Web.HttpContext.Current.Session["FilesLocation"] = strValue;
                    }

                    return strValue;
                }
            }
            catch
            {
                //
            }

            return "https://files.thedatabase.net";
            //default
        }


    }

    public static string TDB_FilesPhisicalPath
    {
        get
        {
            try
            {
                if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null &&
                    System.Web.HttpContext.Current.Session["FilesPhisicalPath"] != null)
                {
                    return System.Web.HttpContext.Current.Session["FilesPhisicalPath"].ToString();
                }
                else
                {
                    string strValue = SystemData.SystemOption_ValueByKey_Account("FilesPhisicalPath", null, null);
                    if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null)
                    {
                        System.Web.HttpContext.Current.Session["FilesPhisicalPath"] = strValue;
                    }

                    return strValue;
                }
            }
            catch
            {
                //
            }

            return @"C:\HostingSpaces\thedatabase\files.thedatabase.net\wwwroot";
            //default
        }


    }


    public static string Files_URL_ImageSection
    {
        get
        {

            return TDB_FilesLocation + "/UserFiles/ImageSection/";

        }
    }
    public static string Files_Folder_ImageSection
    {
        get
        {

            return TDB_FilesPhisicalPath + "\\UserFiles\\ImageSection";

        }
    }

    public static string RecordToXML(Record theRecord, DataTable _dtColumnsAll)
    {
        string strReturn = "";
        try
        {
            if (_dtColumnsAll == null)
                _dtColumnsAll = RecordManager.ets_Table_Columns_All((int)theRecord.TableID);

            string strXML = "<TheRecord>";
            for (int i = 0; i < _dtColumnsAll.Rows.Count; i++)
            {
                //_dtColumnsAll.Rows[i]["SystemName"].ToString()
                strXML += "<EachColumn>";
                strXML += "<ColumnID>" + _dtColumnsAll.Rows[i]["ColumnID"].ToString() + "</ColumnID>";
                strXML += "<SystemName>" + _dtColumnsAll.Rows[i]["SystemName"].ToString() + "</SystemName>";
                strXML += "<CurrentValue>" + RecordManager.GetRecordValue(ref theRecord, _dtColumnsAll.Rows[i]["SystemName"].ToString()) + "</CurrentValue>";
                strXML += "</EachColumn>";
            }
            strXML += "</TheRecord>";
            strReturn = strXML;
        }
        catch
        {
            //
        }
        return strReturn;
    }

    public static DataTable XMLtoDataTableDetail(string strXML, int? iTableID)
    {
        DataTable _dtSysWithValue = null; ;
        try
        {
            DataTable dtDetail = RecordManager.ets_Table_Columns_Detail((int)iTableID);
            DataSet ds = new DataSet();
            StringReader sr = new StringReader(strXML);
            ds.ReadXml(sr);
            DataTable dtRecord = ds.Tables[0];
            _dtSysWithValue = new DataTable();

            foreach (DataRow drR in dtDetail.Rows)
            {
                foreach (DataRow dr in dtRecord.Rows)
                {
                    if (drR["SystemName"].ToString() == dr["SystemName"].ToString())
                    {
                        _dtSysWithValue.Columns.Add(dr["SystemName"].ToString());
                    }
                }
            }

            _dtSysWithValue.Columns.Add("WarningResults");
            _dtSysWithValue.Columns.Add("validationresults");
            _dtSysWithValue.Rows.Add();


            foreach (DataRow dr in dtRecord.Rows)
            {
                if (_dtSysWithValue.Columns.Contains(dr["SystemName"].ToString()))
                    _dtSysWithValue.Rows[0][dr["SystemName"].ToString()] = dr["CurrentValue"].ToString();
            }
            _dtSysWithValue.AcceptChanges();
        }
        catch
        {
            //
        }
        return _dtSysWithValue;
    }

    public static Record XMLtoRecord(string strXML, DataTable _dtColumnsAll, int? iTableID)
    {
        Record theRecord = null;

        try
        {
            if (_dtColumnsAll == null)
                _dtColumnsAll = RecordManager.ets_Table_Columns_All((int)iTableID);


            DataSet ds = new DataSet();
            StringReader sr = new StringReader(strXML);
            ds.ReadXml(sr);
            DataTable dtRecord = ds.Tables[0];

            foreach (DataRow dr in dtRecord.Rows)
            {
                RecordManager.MakeTheRecord(ref theRecord, dr["SystemName"].ToString(), dr["CurrentValue"].ToString());
            }

        }
        catch
        {
            //
        }
        return theRecord;
    }

    public static string SP_ControlValueChangeService(string spName, bool AddMode, int? RecordID, int? UserID, int? TableID,
       string xmlRecord, ref string xmlUpdatedRecord)
    {
        string strReturnMessage = "";
        SearchCriteria theSC = new SearchCriteria(null, xmlRecord);
        int? iSC = SystemData.SearchCriteria_Insert(theSC);


        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {

            using (SqlCommand command = new SqlCommand(spName, connection))
            {
                command.CommandType = CommandType.StoredProcedure;


                SqlParameter pReturnSCid = new SqlParameter("@ReturnRecordInSeachCriteria", SqlDbType.Int);
                pReturnSCid.Direction = ParameterDirection.Output;

                command.Parameters.Add(pReturnSCid);



                command.Parameters.Add(new SqlParameter("@AddMode", AddMode));
                if (RecordID != null)
                    command.Parameters.Add(new SqlParameter("@RecordID", RecordID));
                if (UserID != null)
                    command.Parameters.Add(new SqlParameter("@UserID", UserID));
                if (TableID != null)
                    command.Parameters.Add(new SqlParameter("@TableID", TableID));

                if (iSC != null)
                    command.Parameters.Add(new SqlParameter("@RecordInSeachCriteria", iSC));
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    iSC = int.Parse(pReturnSCid.Value.ToString());
                }
                catch
                {

                }


                connection.Close();
                connection.Dispose();

                SearchCriteria theSearchCriteria = SystemData.SearchCriteria_Detail((int)iSC);

                if (theSearchCriteria != null)
                {

                    System.Xml.XmlDocument xmlSC_Doc = new System.Xml.XmlDocument();

                    xmlSC_Doc.Load(new StringReader(theSearchCriteria.SearchText));


                    strReturnMessage = xmlSC_Doc.FirstChild["message"].InnerText;
                    xmlUpdatedRecord = xmlSC_Doc.FirstChild["xmlUpdatedRecord"].InnerXml;
                }

                return strReturnMessage;
            }


        }

    }
    public static DataTable GetDataRetrieverByTable(int iTableID)
    {
        return Common.DataTableFromText(@" SELECT DocTemplateID,SPName,FileName FROM DocTemplate INNER JOIN DataRetriever
                            ON DocTemplate.DataRetrieverID=DataRetriever.DataRetrieverID
                            WHERE TableID=" + iTableID.ToString());
    }
    public static DataTable spBulkExportColumns(int TableID)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("spBulkExportColumns", connection)) //spExportAllTables
            {
                command.CommandType = CommandType.StoredProcedure;

                command.CommandTimeout = DBGurus.GetCommandTimeout();


                command.Parameters.Add(new SqlParameter("@TableID", TableID));



                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                System.Data.DataSet ds = new System.Data.DataSet();
                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();


                if (ds == null) return null;

                if (ds.Tables.Count > 0)
                {
                    for (int i = 1; i < ds.Tables.Count; i++)
                    {
                        if (ds.Tables[i] != null && ds.Tables[i].Rows.Count > 0)
                            ds.Tables[0].Merge(ds.Tables[i]);
                    }

                    return ds.Tables[0];
                }
                return null;
            }
        }
    }


    public static DataTable spBulkExportData(string TableID, string RecordList, string ColumnList, int iResultType)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("spBulkExportData", connection)) //spExportAllTables
            {
                command.CommandType = CommandType.StoredProcedure;

                command.CommandTimeout = DBGurus.GetCommandTimeout();

                if (TableID != "")
                    command.Parameters.Add(new SqlParameter("@TableID", TableID));
                if (RecordList != "")
                    command.Parameters.Add(new SqlParameter("@RecordList", RecordList));
                if (ColumnList != "")
                    command.Parameters.Add(new SqlParameter("@ColumnList", ColumnList));

                command.Parameters.Add(new SqlParameter("@ResultType", iResultType));


                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                System.Data.DataSet ds = new System.Data.DataSet();
                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();


                if (ds == null) return null;

                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
                {
                    return null;
                }
            }
        }
    }

    public static string GetCode(Control _control, string strValidationGroup)
    {
        // building helper
        StringBuilder _output = new StringBuilder();

        // the logic of scanning
        if (_control.GetType().GetProperty("ValidationGroup") != null && !string.IsNullOrEmpty(_control.ID))
        {
            // the desired code
            _output.AppendFormat("{0}.{1} = {2};", _control.ID, "ValidationGroup", strValidationGroup);
            _output.AppendLine();
        }

        // recursive search within children
        _output.Append(GetCode(_control.Controls, strValidationGroup));

        // outputting
        return _output.ToString();
    }

    public static string GetCode(ControlCollection _collection, string strValidationGroup)
    {
        // building helper
        StringBuilder _output = new StringBuilder();
        foreach (Control _control in _collection)
        {
            // get code for each child
            _output.Append(GetCode(_control, strValidationGroup));
        }
        // outputting
        return _output.ToString();
    }

    public static void SetValidationGroup(ControlCollection _collection, string strValidationGroup)
    {
        foreach (Control _control in _collection)
        {
            // set each child
            SetValidationGroup(_control, strValidationGroup);
        }

    }
    public static void SetValidationGroup(Control _control, string strValidationGroup)
    {

        // the logic of scanning
        if (_control.GetType().GetProperty("ValidationGroup") != null && !string.IsNullOrEmpty(_control.ID))
        {
            _control.GetType().GetProperty("ValidationGroup").SetValue(_control, strValidationGroup);

        }
        // recursive search within children
        SetValidationGroup(_control.Controls, strValidationGroup);

    }


    public static void SetCssClass(Control _control, string strCssClass)
    {

        // the logic of scanning
        if (_control.GetType().GetProperty("CssClass") != null && !string.IsNullOrEmpty(_control.ID))
        {
            //_control.GetType().GetProperty("CssClass").SetValue(_control, strCssClass);
            if (_control is TextBox)
            {
                (_control as TextBox).CssClass = "form-control";
                (_control as TextBox).Style.Add("width", "100%");
            }
            if (_control is DropDownList)
            {
                (_control as DropDownList).CssClass = "form-control";
                (_control as DropDownList).Style.Add("width", "100%");
            }
            if (_control is CheckBox)
            {
                (_control as CheckBox).CssClass = "form-control";
            }
            if (_control is RadioButtonList)
            {
                (_control as RadioButtonList).CssClass = "form-control";
            }
            if (_control is ListBox)
            {
                (_control as ListBox).CssClass = "form-control";
                (_control as ListBox).Style.Add("width", "100%");
                (_control as ListBox).Style.Add("max-width", "100%");
            }
            if (_control is CheckBoxList)
            {
                (_control as CheckBoxList).CssClass = "form-control";
                (_control as CheckBoxList).Style.Add("width", "100%");
                (_control as CheckBoxList).Style.Add("max-width", "100%");
            }

        }
        // recursive search within children
        SetCssClass(_control.Controls, strCssClass);

    }
    public static void SetCssClass(ControlCollection _collection, string strCssClass)
    {
        foreach (Control _control in _collection)
        {
            // set each child
            SetCssClass(_control, strCssClass);
        }

    }
    public static void SetControlProperty(Control _control, string strProperty, string strValue)
    {
        try
        {
            // the logic of scanning
            if (_control.GetType().GetProperty(strProperty) != null && !string.IsNullOrEmpty(_control.ID))
            {
                if (_control.GetType().GetProperty(strProperty).PropertyType == typeof(string))
                {
                    _control.GetType().GetProperty(strProperty).SetValue(_control, strValue);
                }
                else if (_control.GetType().GetProperty(strProperty).PropertyType == typeof(bool))
                {
                    _control.GetType().GetProperty(strProperty).SetValue(_control, bool.Parse(strValue));
                }
                else
                {
                    _control.GetType().GetProperty(strProperty).SetValue(_control, strValue);
                }


            }
        }
        catch
        {
            //
        }


    }
    public static void ChangeControlName(ControlCollection _collection, string strIDExtra)
    {
        foreach (Control _control in _collection)
        {
            // set each child
            ChangeControlName(_control, strIDExtra);
        }

    }
    public static void ChangeControlName(Control _control, string strIDExtra)
    {

        // the logic of scanning
        if (_control.GetType().GetProperty("ID") != null && !string.IsNullOrEmpty(_control.ID))
        {
            _control.GetType().GetProperty("ID").SetValue(_control, _control.ID + strIDExtra);

        }
        // recursive search within children
        ChangeControlName(_control.Controls, strIDExtra);

    }

    public static void CausesValidationFalse(ControlCollection _collection)
    {
        foreach (Control _control in _collection)
        {
            // set each child
            CausesValidationFalse(_control);
        }

    }
    public static void CausesValidationFalse(Control _control)
    {

        // the logic of scanning
        if (_control.GetType().GetProperty("CausesValidation") != null && !string.IsNullOrEmpty(_control.ID))
        {
            _control.GetType().GetProperty("CausesValidation").SetValue(_control, false);

        }
        // recursive search within children
        CausesValidationFalse(_control.Controls);

    }

    public static void SetEnabled(ControlCollection _collection, bool bEnabled)
    {
        foreach (Control _control in _collection)
        {
            // set each child
            SetEnabled(_control, bEnabled);
        }

    }
    public static void SetEnabled(Control _control, bool bEnabled)
    {

        // the logic of scanning
        if (_control.GetType().GetProperty("Enabled") != null && !string.IsNullOrEmpty(_control.ID))
        {
            _control.GetType().GetProperty("Enabled").SetValue(_control, bEnabled);

        }
        // recursive search within children
        SetEnabled(_control.Controls, bEnabled);

    }

    public static string SiteRoot()
    {
        string strRoot = "";
        try
        {
            //HttpContext.Current.Request.Url.Scheme
            strRoot = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority
                + HttpContext.Current.Request.ApplicationPath;
        }
        catch
        {
            try
            {
                string strDB = Common.GetDatabaseName();
                strDB = strDB.Replace("thedatabase_", "");
                strRoot = "https://" + strDB + ".thedatabase.net";
            }
            catch
            {
                strRoot = "https://dev.thedatabase.net";
            }
          

        }
        return strRoot;
    }


    public static Control CopyControls(Control target, Control source)
    {
        foreach (Control c in source.Controls)
        {
            Control cc = Activator.CreateInstance(c.GetType()) as Control;
            cc = Clone(cc, c) as Control;
            cc = CopyControls(cc, c);
            target.Controls.Add(cc);
            if (c.Controls.Count != 0)
            {
                Control d = new Control();
                d = CopyControls(d, c);
                target.Controls.Add(d);
            }
        }
        return target;
    }
    public static object Clone(object target, object source)
    {
        if (source == null)
            throw new ArgumentNullException("Source");
        if (source.GetType() != target.GetType())
            throw new ArgumentException("Type Mismatch");
        foreach (PropertyInfo p in source.GetType().GetProperties())
        {
            if (p.CanRead && p.CanWrite)
                p.SetValue(target, p.GetValue(source, p.GetIndexParameters()), p.GetIndexParameters());
        }
        return target;
    }
    public static void SetReadOnly(ControlCollection _collection, bool bRO)
    {
        foreach (Control _control in _collection)
        {
            // set each child
            SetReadOnly(_control, bRO);
        }

    }
    public static void SetReadOnly(Control _control, bool bRO)
    {

        // the logic of scanning 
        
        if (_control.GetType().GetProperty("Enabled") != null && !string.IsNullOrEmpty(_control.ID))
            _control.GetType().GetProperty("Enabled").SetValue(_control, !bRO);
       
        // recursive search within children
        SetReadOnly(_control.Controls, bRO);

    }


    public static bool IsRecordDuplicate(Record theRecord, string strUniqueColumnIDSys, string strUniqueColumnID2Sys, int iRecordID)
    {
        if (strUniqueColumnIDSys != "" || strUniqueColumnID2Sys != "")
        {
            string strUniqueColumnIDValue = "";
            string strUniqueColumnID2Value = "";
            if (strUniqueColumnIDSys != "")
                strUniqueColumnIDValue = RecordManager.GetRecordValue(ref theRecord, strUniqueColumnIDSys);

            if (strUniqueColumnID2Sys != "")
                strUniqueColumnID2Value = RecordManager.GetRecordValue(ref theRecord, strUniqueColumnID2Sys);

            if (RecordManager.ets_Record_IsDuplicate_Entry((int)theRecord.TableID, iRecordID, strUniqueColumnIDSys, strUniqueColumnIDValue,
                strUniqueColumnID2Sys, strUniqueColumnID2Value))
            {

                return true;
            }

        }
        return false;
    }
    public static void PerformAllValidation(ref Record theRecord, ref DataTable dtValidWarning,
       bool bAddToGrid, bool bSendEmail, DataTable _dtColumnsAll, ref string _strValidationError, ref string _strInValidResults,
        bool _bShowExceedances, ref string _strExceedanceResults, int _iSessionAccountID, string _strURL, ref string _strExceedanceEmailFullBody,
        ref string _strExceedanceSMSFullBody, ref int _iExceedanceColumnCount, ref string _strWarningResults, ref string _strWarningEmailFullBody,
        ref string _strWarningSMSFullBody, ref int _iWarningColumnCount)
    {
        bool bEachColumnExceedance = false;
        bool bEachColumnWarning = false;
        bool bEachColumnInValid = false;
        string strTemp = "";

        //KG 14-8-17 Ticket 3000
        string strSampleDateValue = "";
        string strSampleSite = "";

        if (bSendEmail)
            bAddToGrid = false;//in case

        for (int i = 0; i < _dtColumnsAll.Rows.Count; i++)
        {
            //KG 14-8-17 Ticket 3000
            if (_dtColumnsAll.Rows[i]["DisplayName"].ToString() == "Date/Time Sampled" || _dtColumnsAll.Rows[i]["DisplayName"].ToString() == "Date / Time Sampled" || _dtColumnsAll.Rows[i]["DisplayName"].ToString() == "Date Sampled")
                strSampleDateValue = RecordManager.GetRecordValue(ref theRecord, _dtColumnsAll.Rows[i]["SystemName"].ToString());
            else if (_dtColumnsAll.Rows[i]["DisplayName"].ToString() == "Sample Site" && _dtColumnsAll.Rows[i]["ColumnType"].ToString() == "dropdown" && _dtColumnsAll.Rows[i]["DropDownType"].ToString() != "values")
            {
                string strColumnID = "";
                string strSampleSiteID = RecordManager.GetRecordValue(ref theRecord, _dtColumnsAll.Rows[i]["SystemName"].ToString());
                if (strSampleSiteID != "")
                {
                    Record theLinkedRecord = RecordManager.ets_Record_Detail_Full(int.Parse(strSampleSiteID), null, false);
                    if (_dtColumnsAll.Rows[i]["DisplayColumn"] != null)
                    {
                        strColumnID = Common.GetValueFromSQL("SELECT [ColumnID] FROM [Column] WHERE TableID=" + theLinkedRecord.TableID + " and DisplayName ='" + _dtColumnsAll.Rows[i]["DisplayColumn"].ToString().Replace("[", "").Replace("]", "") + "'");
                    }
                    Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(strColumnID));
                    strSampleSite = RecordManager.GetRecordValue(ref theLinkedRecord, theLinkedColumn.SystemName).Replace("'", "''");
                }
            }
            //ALL Validation
            bEachColumnExceedance = false;
            bEachColumnWarning = false;
            bEachColumnInValid = false;
            string strValue = RecordManager.GetRecordValue(ref theRecord, _dtColumnsAll.Rows[i]["SystemName"].ToString());
            if (!string.IsNullOrEmpty(strValue))
            {
                bool bValidationCanIgnore = false;
                if (_dtColumnsAll.Rows[i]["ValidationCanIgnore"] != DBNull.Value && (bool)_dtColumnsAll.Rows[i]["ValidationCanIgnore"])
                {
                    bValidationCanIgnore = true;
                }


                string strFormulaV = "";

                if (_dtColumnsAll.Rows[i]["AdvConV"] != DBNull.Value)
                {
                    strFormulaV = "---advanced---";
                }
                //else if (_dtColumnsAll.Rows[i]["ConV"] != DBNull.Value)
                //{
                //    Column theCheckColumn = RecordManager.ets_Column_Details(int.Parse(_dtColumnsAll.Rows[i]["ConV"].ToString()));
                //    if (theCheckColumn != null)
                //    {
                //        string strCheckValue = RecordManager.GetRecordValue(ref theRecord, theCheckColumn.SystemName);
                //        strFormulaV = UploadWorld.Condition_GetFormula(int.Parse(_dtColumnsAll.Rows[i]["ColumnID"].ToString()), theCheckColumn.ColumnID,
                //            "V", strCheckValue);
                //    }
                //}
                else
                {
                    if (_dtColumnsAll.Rows[i]["ValidationOnEntry"] != DBNull.Value && _dtColumnsAll.Rows[i]["ValidationOnEntry"].ToString().Length > 0)
                    {
                        strFormulaV = _dtColumnsAll.Rows[i]["ValidationOnEntry"].ToString();
                    }
                }

                if (strFormulaV != "" && _bShowExceedances)
                {
                    if (strFormulaV == "---advanced---")
                    {
                        int columnId = int.Parse(_dtColumnsAll.Rows[i]["ColumnID"].ToString());
                        DataTable dt = UploadWorld.ets_AdvancedCondition_Select(columnId, "notification", "V");
                        string sError = String.Empty;
                        bEachColumnInValid = !UploadManager.IsDataValidAdvanced(columnId, theRecord, dt, ref sError);
                    }
                    else
                    {
                        bEachColumnInValid = !UploadManager.IsDataValid(strValue, strFormulaV, ref _strValidationError,
                            _dtColumnsAll.Rows[i]["ColumnType"].ToString());
                    }

                    if (bEachColumnInValid)
                    {
                        if (bValidationCanIgnore)
                        {
                            _strWarningResults = _strWarningResults +
                                                 TheDatabase.GetInvalidIgnored_msg(_dtColumnsAll.Rows[i]["DisplayName"]
                                                     .ToString());
                        }
                        else
                        {
                            _strInValidResults = _strInValidResults +
                                                 TheDatabase.GetInvalid_msg(_dtColumnsAll.Rows[i]["DisplayName"]
                                                     .ToString());
                        }
                        if (bAddToGrid)
                        {
                            dtValidWarning.Rows.Add(_dtColumnsAll.Rows[i]["ColumnID"].ToString(), "i", "no",
                                Common.GetFromulaMsg("i", _dtColumnsAll.Rows[i]["DisplayName"].ToString(),
                                    strFormulaV) //    "Invalid data - " + _dtColumnsAll.Rows[i]["DisplayName"].ToString()
                                , strFormulaV, strValue);
                        }
                    }
                }
                //}

                //bEachColumnExceedance = false;
                if (_bShowExceedances && bEachColumnInValid == false)
                {

                    string strFormulaE = "";

                    if (_dtColumnsAll.Rows[i]["AdvConE"] != DBNull.Value)
                    {
                        strFormulaE = "---advanced---";
                    }
                    //else if (_dtColumnsAll.Rows[i]["ConE"] != DBNull.Value)
                    //{
                    //    Column theCheckColumn = RecordManager.ets_Column_Details(int.Parse(_dtColumnsAll.Rows[i]["ConE"].ToString()));
                    //    if (theCheckColumn != null)
                    //    {
                    //        string strCheckValue = RecordManager.GetRecordValue(ref theRecord, theCheckColumn.SystemName);
                    //        strFormulaE = UploadWorld.Condition_GetFormula(int.Parse(_dtColumnsAll.Rows[i]["ColumnID"].ToString()), theCheckColumn.ColumnID,
                    //            "E", strCheckValue);
                    //    }
                    //}
                    else
                    {
                        if (_dtColumnsAll.Rows[i]["ValidationOnExceedance"] != DBNull.Value && _dtColumnsAll.Rows[i]["ValidationOnExceedance"].ToString().Length > 0)
                        {
                            strFormulaE = _dtColumnsAll.Rows[i]["ValidationOnExceedance"].ToString();
                        }
                    }


                    if (strFormulaE != "")
                    {
                        if (strFormulaE == "---advanced---")
                        {
                            int columnId = int.Parse(_dtColumnsAll.Rows[i]["ColumnID"].ToString());
                            DataTable dt = UploadWorld.ets_AdvancedCondition_Select(columnId, "notification", "E");
                            string sError = String.Empty;
                            bEachColumnExceedance =
                                !UploadManager.IsDataValidAdvanced(columnId, theRecord, dt, ref sError);
                        }
                        else
                            bEachColumnExceedance = !UploadManager.IsDataValid(strValue, strFormulaE,
                                ref _strValidationError, _dtColumnsAll.Rows[i]["ColumnType"].ToString());

                        if (bEachColumnExceedance)
                        {
                            _strExceedanceResults = _strExceedanceResults + TheDatabase.GetExceedance_msg(_dtColumnsAll.Rows[i]["DisplayName"].ToString());
                            //_bDataExceedance = true;
                            if (bAddToGrid)
                            {
                                dtValidWarning.Rows.Add(_dtColumnsAll.Rows[i]["ColumnID"].ToString(), "e", "yes",
                                  Common.GetFromulaMsg("e", _dtColumnsAll.Rows[i]["DisplayName"].ToString(), strFormulaE)//  "EXCEEDANCE: " + _dtColumnsAll.Rows[i]["DisplayName"].ToString() + " –  Value outside accepted range."
                                    , strFormulaE, strValue);
                            }

                            if (bSendEmail)
                            {
                                //KG 14-8-17 Ticket 3000
                                if (strSampleDateValue == "")
                                    strSampleDateValue = theRecord.DateTimeRecorded.ToString();

                                RecordManager.BuildDataExceedanceSMSandEmail(int.Parse(_dtColumnsAll.Rows[i]["ColumnID"].ToString()), strValue, strSampleSite, strSampleDateValue,
                                    ref strTemp, _iSessionAccountID, _strURL, ref _strExceedanceEmailFullBody, ref _strExceedanceSMSFullBody, ref _iExceedanceColumnCount);

                            }
                        }

                    }
                }

                if (bEachColumnExceedance == false && bEachColumnInValid == false)
                {

                    string strFormulaW = "";

                    if (_dtColumnsAll.Rows[i]["AdvConW"] != DBNull.Value)
                    {
                        strFormulaW = "---advanced---";
                    }
                    //else if (_dtColumnsAll.Rows[i]["ConW"] != DBNull.Value)
                    //{
                    //    Column theCheckColumn = RecordManager.ets_Column_Details(int.Parse(_dtColumnsAll.Rows[i]["ConW"].ToString()));
                    //    if (theCheckColumn != null)
                    //    {
                    //        string strCheckValue = RecordManager.GetRecordValue(ref theRecord, theCheckColumn.SystemName);
                    //        strFormulaW = UploadWorld.Condition_GetFormula(int.Parse(_dtColumnsAll.Rows[i]["ColumnID"].ToString()), theCheckColumn.ColumnID,
                    //            "W", strCheckValue);
                    //    }
                    //}
                    else
                    {
                        if (_dtColumnsAll.Rows[i]["ValidationOnWarning"] != DBNull.Value && _dtColumnsAll.Rows[i]["ValidationOnWarning"].ToString().Length > 0)
                        {
                            strFormulaW = _dtColumnsAll.Rows[i]["ValidationOnWarning"].ToString();
                        }
                    }


                    if (strFormulaW != "" && _bShowExceedances)
                    {
                        if (strFormulaW == "---advanced---")
                        {
                            int columnId = int.Parse(_dtColumnsAll.Rows[i]["ColumnID"].ToString());
                            DataTable dt = UploadWorld.ets_AdvancedCondition_Select(columnId, "notification", "W");
                            string sError = String.Empty;
                            bEachColumnWarning = !UploadManager.IsDataValidAdvanced(columnId, theRecord, dt, ref sError);
                        }
                        else
                        {
                            bEachColumnWarning = !UploadManager.IsDataValid(strValue, strFormulaW,
                                ref _strValidationError, _dtColumnsAll.Rows[i]["ColumnType"].ToString());
                        }

                        if (bEachColumnWarning)
                        {
                            _strWarningResults = _strWarningResults +
                                                 TheDatabase.GetWarning_msg(_dtColumnsAll.Rows[i]["DisplayName"]
                                                     .ToString());
                            //_bDataWarning = true;
                            if (bAddToGrid)
                            {
                                dtValidWarning.Rows.Add(_dtColumnsAll.Rows[i]["ColumnID"].ToString(), "w", "no",
                                    Common.GetFromulaMsg("w", _dtColumnsAll.Rows[i]["DisplayName"].ToString(),
                                        strFormulaW) // "WARNING: " + _dtColumnsAll.Rows[i]["DisplayName"].ToString() + " – Value outside accepted range."
                                    , strFormulaW, strValue);
                            }

                            if (bSendEmail)
                            {
                                //KG 14-8-17 Ticket 3000
                                if (strSampleDateValue == "")
                                    strSampleDateValue = theRecord.DateTimeRecorded.ToString();

                                RecordManager.BuildDataWanrningSMSandEmail(
                                    int.Parse(_dtColumnsAll.Rows[i]["ColumnID"].ToString()), strValue, strSampleSite,
                                    strSampleDateValue,
                                    ref strTemp, _iSessionAccountID, _strURL, ref _strWarningEmailFullBody,
                                    ref _strWarningSMSFullBody, ref _iWarningColumnCount);
                            }
                        }
                    }
                }
            }
        }
    }
    public static string GetAdjustValidationNotification(string strNotifications, string strInvalidRecordIDs, string strValidRecordIDs, Column editColumn)
    {
        if (strInvalidRecordIDs != "")
        {
            strNotifications = strNotifications + "Validation Adjusted:" + editColumn.DisplayColumn + " " + (strInvalidRecordIDs.Split(',').Length - 1).ToString() + " invalid records";
            //strNotifications = strNotifications + " and " + (strValidRecordIDs.Split(',').Length - 1).ToString() + " records became valid from invalid.";
        }
        //else if (strInvalidRecordIDs != "")
        //{
        //    strNotifications = strNotifications + "Validation Adjusted:" + editColumn.DisplayColumn + " " + (strInvalidRecordIDs.Split(',').Length - 1).ToString() + " invalid records.";
        //}
        //else if (strInvalidRecordIDs != "" && strValidRecordIDs != "")
        //{
        //    strNotifications = strNotifications + "Validation Adjusted:" + editColumn.DisplayColumn + " " + (strValidRecordIDs.Split(',').Length - 1).ToString() + " records became valid from invalid";
        //}

        return strNotifications;
    }

    public static string GetInvalid_msg(string strDisplayName)
    {
        return "INVALID: " + strDisplayName + ".";
    }
    public static string GetInvalidIgnored_msg(string strDisplayName)
    {
        return "INVALID (and ignored): " + strDisplayName + ".";
    }

    public static string GetWarning_msg(string strDisplayName)
    {
        return "WARNING: " + strDisplayName + " – Value outside accepted range.";
    }
    public static string GetWarningUnlikely_msg(string strDisplayName)
    {
        return "WARNING: " + strDisplayName + " – Unlikely data – outside 3 standard deviations.";
    }
    public static string GetExceedance_msg(string strDisplayName)
    {
        return "EXCEEDANCE: " + strDisplayName + " – Value outside accepted range.";
    }

    public static bool HasInvalidIgnored_msg(string strWarningResults, string strDisplayName, string strType)
    {
        string strFullMsg = GetInvalidIgnored_msg(strDisplayName);
        return strWarningResults.IndexOf(strFullMsg.Substring(0, (strFullMsg.Length - 1))) > -1;
    }

    public static bool HasInvalid_msg(string strWarningResults, string strDisplayName, string strType)
    {
        string strFullMsg = GetInvalid_msg(strDisplayName);
        return strWarningResults.IndexOf(strFullMsg.Substring(0, (strFullMsg.Length - 1))) > -1;
    }

    public static bool HasExceedance_msg(string strWarningResults, string strDisplayName, string strType)
    {
        string strFullMsg = GetExceedance_msg(strDisplayName);

        if (strType == "")
        {
            return strWarningResults.IndexOf("EXCEEDANCE: " + strDisplayName) > -1;
        }
        if (strType == "l")
        {
            return strWarningResults.IndexOf(strFullMsg.Substring(0, (strFullMsg.Length - 5))) > -1;
        }

        return false;
    }

    public static bool HasWarning_msg(string strWarningResults, string strDisplayName, string strType)
    {
        string strFullMsg = GetWarning_msg(strDisplayName);

        if (strType == "")
        {
            return strWarningResults.IndexOf("WARNING: " + strDisplayName) > -1;
        }
        if (strType == "l")
        {
            return strWarningResults.IndexOf(strFullMsg.Substring(0, (strFullMsg.Length - 5))) > -1;
        }

        return false;
    }

    public static bool HasWarningUnlikely_msg(string strWarningResults, string strDisplayName, string strType)
    {
        string strFullMsg = GetWarningUnlikely_msg(strDisplayName);

        if (strType == "")
        {
            return strWarningResults.IndexOf("WARNING: " + strDisplayName) > -1;
        }
        if (strType == "l")
        {
            return strWarningResults.IndexOf(strFullMsg.Substring(0, (strFullMsg.Length - 5))) > -1;
        }

        return false;
    }

    public static void PutCheckBoxListValues(string strDropdownValues, ref CheckBoxList lb)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        foreach (string s in result)
        {
            ListItem liTemp = new ListItem(s, s);
            lb.Items.Add(liTemp);
        }

    }


    public static void SetCheckBoxListValues(string strDBValues, ref CheckBoxList lb, string strDropdownValues)
    {


        if (strDBValues != "")
        {

            lb.Items.Clear();


            string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);


            string[] strSS = strDBValues.Split(',');

            foreach (string s in result)
            {

                foreach (string SS in strSS)
                {
                    if (SS == s)
                    {
                        ListItem liTemp = new ListItem(s, s);
                        lb.Items.Add(liTemp);
                    }
                }
            }

            foreach (string s in result)
            {

                if (lb.Items.FindByValue(s) == null)
                {
                    ListItem liTemp = new ListItem(s, s);
                    lb.Items.Add(liTemp);
                }

            }

            foreach (string SS in strSS)
            {
                try
                {
                    if (SS != "")
                        lb.Items.FindByValue(SS).Selected = true;
                }
                catch
                {
                    //
                }
            }

            foreach (ListItem li in lb.Items)
            {
                li.Attributes.Add("DataValue", li.Value);
            }
        }

    }


    public static void SetCheckBoxListValues_Text(string strDBValues, ref CheckBoxList lb, string strDropdownValues)
    {


        if (strDBValues != "")
        {

            lb.Items.Clear();


            string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            string strValue = "";
            string strText = "";

            string[] strSS = strDBValues.Split(',');

            foreach (string s in result)
            {
                strValue = "";
                strText = "";

                if (s.IndexOf(",") > -1)
                {
                    strValue = s.Substring(0, s.IndexOf(","));
                    strText = s.Substring(strValue.Length + 1);
                }

                foreach (string SS in strSS)
                {
                    if (SS == strValue)
                    {
                        ListItem liTemp = new ListItem(strText, strValue);
                        lb.Items.Add(liTemp);
                    }
                }
            }

            foreach (string s in result)
            {


                strValue = "";
                strText = "";

                if (s.IndexOf(",") > -1)
                {
                    strValue = s.Substring(0, s.IndexOf(","));
                    strText = s.Substring(strValue.Length + 1);
                }

                if (lb.Items.FindByValue(strValue) == null)
                {
                    ListItem liTemp = new ListItem(strText, strValue);
                    lb.Items.Add(liTemp);
                }

            }

            foreach (string SS in strSS)
            {
                try
                {
                    if (SS != "")
                        lb.Items.FindByValue(SS).Selected = true;
                }
                catch
                {
                    //
                }
            }

            foreach (ListItem li in lb.Items)
            {
                li.Attributes.Add("DataValue", li.Value);
            }
        }

    }

    public static void PutListValues_Text(string strDropdownValues, ref ListBox lb)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        string strValue = "";
        string strText = "";

        foreach (string s in result)
        {
            //ListItem liTemp = new ListItem(s, s.ToLower());
            strValue = "";
            strText = "";
            if (s.IndexOf(",") > -1)
            {
                strValue = s.Substring(0, s.IndexOf(","));
                strText = s.Substring(strValue.Length + 1);
                if (strValue != "" && strText != "")
                {
                    ListItem liTemp = new ListItem(strText, strValue);
                    lb.Items.Add(liTemp);
                }
            }
        }


    }

    public static void SetListValues(string strDBValues, ref ListBox lb)
    {
        if (strDBValues != "")
        {
            string[] strSS = strDBValues.Split(',');
            foreach (string SS in strSS)
            {
                try
                {
                    if (SS != "")
                        lb.Items.FindByValue(SS).Selected = true;
                }
                catch
                {
                    //
                }
            }
        }

    }

    public static void PutListValues(string strDropdownValues, ref ListBox lb)
    {
        lb.Items.Clear();
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        foreach (string s in result)
        {
            ListItem liTemp = new ListItem(s, s);
            lb.Items.Add(liTemp);
        }


    }

    public static string GetListValues(ListBox lb)
    {
        string strSelectedValues = "";

        foreach (ListItem item in lb.Items)
        {
            if (item.Selected)
            {
                strSelectedValues = strSelectedValues + item.Value + ",";
            }
        }

        if (strSelectedValues != "")
            strSelectedValues = strSelectedValues.Substring(0, strSelectedValues.Length - 1);
        return strSelectedValues;
    }

    public static string GetCheckBoxValue(string strDropdownValues, ref CheckBox chk)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        int i = 0;
        foreach (string s in result)
        {
            if (i == 0)
            {
                if (chk.Checked)
                {
                    return s;
                }
            }
            if (i == 1)
            {
                if (chk.Checked == false)
                {
                    return s;
                }
            }
            i = i + 1;
        }
        return "";
    }

    public static void SetCheckBoxValue(string strDropdownValues, string strValue, ref CheckBox chk)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        int i = 0;
        foreach (string s in result)
        {
            if (i == 0)
            {
                if (s.ToLower() == strValue.ToLower())
                {
                    chk.Checked = true;
                }
            }
            if (i == 1)
            {
                if (s.ToLower() == strValue.ToLower())
                {
                    chk.Checked = false;
                }
            }
            i = i + 1;
        }


    }
    public static void PutCheckBoxDefault(string strDropdownValues, ref CheckBox chk)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        int i = 0;
        foreach (string s in result)
        {
            if (i == 2)
            {
                if (s.ToLower() == "yes")
                {
                    chk.Checked = true;
                }
            }
            i = i + 1;
        }


    }

    public static void PutRadioList(string strDropdownValues, ref RadioButtonList rl)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        rl.Items.Clear();
        foreach (string s in result)
        {
            //ListItem liTemp = new ListItem(s, s.ToLower());
            ListItem liTemp = new ListItem(s + "&nbsp;&nbsp;", s);
            rl.Items.Add(liTemp);
        }

    }

    public static void PutRadioListValue_Text(string strDropdownValues, ref RadioButtonList rl)
    {
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        rl.Items.Clear();
        string strValue = "";
        string strText = "";

        foreach (string s in result)
        {
            //ListItem liTemp = new ListItem(s, s.ToLower());
            strValue = "";
            strText = "";
            if (s.IndexOf(",") > -1)
            {
                strValue = s.Substring(0, s.IndexOf(","));
                strText = s.Substring(strValue.Length + 1);
                if (strValue != "" && strText != "")
                {
                    ListItem liTemp = new ListItem(strText + "&nbsp;&nbsp;", strValue);
                    rl.Items.Add(liTemp);
                }
            }
        }


    }

    public static void PutCheckboxIntoDDL(string strDropdownValues, ref DropDownList ddl)
    {
        PutDDLValues(strDropdownValues, ref ddl);
        if (ddl.Items.Count > 1)
        {
            ddl.Items.RemoveAt(ddl.Items.Count - 1);// remove the defaule part
        }
    }
    public static void PutDDLValues(string strDropdownValues, ref DropDownList ddl)
    {
        ddl.Items.Clear();

        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        foreach (string s in result)
        {
            //ListItem liTemp = new ListItem(s, s.ToLower());
            ListItem liTemp = new ListItem(s, s);
            ddl.Items.Add(liTemp);
        }

        string defaultListItem = "--Please Select--";
        if (ddl.TemplateControl.ID.Contains("cbcSearch"))
        {
            defaultListItem = "--Show All--";

        }

        //Red 3715_2
        if (!ddl.TemplateControl.ClientID.Contains("rlOne_cbcSearch"))
        {
            ListItem liSelect = new ListItem(defaultListItem, "");
            ddl.Items.Insert(0, liSelect);
        }

    }


    public static void PutDDLValue_Text(string strDropdownValues, ref DropDownList ddl)
    {
        ddl.Items.Clear();
        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        string strValue = "";
        string strText = "";

        foreach (string s in result)
        {
            //ListItem liTemp = new ListItem(s, s.ToLower());
            strValue = "";
            strText = "";
            if (s.IndexOf(",") > -1)
            {
                strValue = s.Substring(0, s.IndexOf(","));
                strText = s.Substring(strValue.Length + 1);
                if (strValue != "" && strText != "")
                {
                    ListItem liTemp = new ListItem(strText, strValue);
                    ddl.Items.Add(liTemp);
                }
            }
        }

        string defaultListItem = "--Please Select--";

        if (ddl.TemplateControl.ID.Contains("cbcSearch"))
        {
            defaultListItem = "--Show All--";
        }

        //Red 3715_2
        if (!ddl.TemplateControl.ClientID.Contains("rlOne_cbcSearch"))
        {
            ListItem liSelect = new ListItem(defaultListItem, "");
            ddl.Items.Insert(0, liSelect);
        }

    }

    public static int Account24769_spAddNewMedication(int PatientID, int NewMedicationID)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Account24769.spAddNewMedication", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@PatientID", PatientID));
                command.Parameters.Add(new SqlParameter("@NewMedicationID", NewMedicationID));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;


            }
        }
    }


}
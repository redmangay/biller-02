﻿using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;
using Exception = System.Exception;
using ss = DocumentFormat.OpenXml.Spreadsheet;

//using NPOI.SS;
namespace DBG.Common
{


    public class ExportUtil
    {


        public static string StripTagsCharArray(string HTMLText)
        {
            string strR = "";
            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                using (SqlCommand comm = new SqlCommand("dbo.udf_StripHTML", conn))
                {
                    comm.CommandType = CommandType.StoredProcedure;

                    SqlParameter p1 = new SqlParameter("@HTMLText", SqlDbType.VarChar);
                    // You can call the return value parameter anything, .e.g. "@Result".
                    SqlParameter p2 = new SqlParameter("@Result", SqlDbType.VarChar);

                    p1.Direction = ParameterDirection.Input;
                    p2.Direction = ParameterDirection.ReturnValue;

                    p1.Value = HTMLText;

                    comm.Parameters.Add(p1);
                    comm.Parameters.Add(p2);

                    conn.Open();

                    try
                    {
                        comm.ExecuteNonQuery();

                        if (p2.Value != DBNull.Value)
                            strR = (string)p2.Value;
                    }
                    catch
                    {

                    }
                    conn.Close();
                    conn.Dispose();

                }
            }
            return strR;
        }

        public static void ExportToExcel2(DataTable sourceTable, string fileName)
        {
            ExportToExcel2(sourceTable, "Sheet 1", fileName);
        }

        public static void ExportToExcel2(DataTable sourceTable, string sheetName, string fileName)
        {
            HSSFWorkbook workbook = new HSSFWorkbook();
            MemoryStream memoryStream = new MemoryStream();
            HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet(String.IsNullOrEmpty(sheetName) ? "Sheet 1" : sheetName);
            HSSFRow headerRow = (HSSFRow)sheet.CreateRow(0);

            Dictionary<int, string> dicCellType = new Dictionary<int, string>();
            //HSSFCellStyle boldStyle =(HSSFCellStyle) workbook.CreateCellStyle();
            var boldStyle = workbook.CreateCellStyle();
            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.BOLD;
            boldStyle.SetFont(boldFont);


            boldStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            boldStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.MEDIUM;
            boldStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            boldStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;

            boldStyle.TopBorderColor = HSSFColor.BLACK.index;
            boldStyle.RightBorderColor = HSSFColor.BLACK.index;
            boldStyle.BottomBorderColor = HSSFColor.BLACK.index;
            boldStyle.LeftBorderColor = HSSFColor.BLACK.index;

            boldStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
            NPOI.HSSF.Record.PaletteRecord palette = new NPOI.HSSF.Record.PaletteRecord();
            HSSFPalette p = new HSSFPalette(palette);

            //p.AddColor(219, 229, 241);

            //boldStyle.FillForegroundColor = ((HSSFColor)p.FindSimilarColor(20, 230, 230)).GetIndex();

            boldStyle.FillForegroundColor = ((HSSFColor)setColor(workbook, 219, 229, 241)).GetIndex();


            // handling header.
            string[] headerColumnTypeSplit;
            string tmpColumnName;
            foreach (DataColumn column in sourceTable.Columns)
            {
                HSSFCell headerCell = (HSSFCell)headerRow.CreateCell(column.Ordinal);
                headerCell.CellStyle = boldStyle;

                //oliver <begin> Ticket 1461
                headerColumnTypeSplit = column.ToString().Split('-');
                if (headerColumnTypeSplit.Length > 1)
                {
                    if (headerColumnTypeSplit[1].ToLower() == "date")
                    {
                        tmpColumnName = column.ColumnName.ToString().Replace("-Date", " (Date)");
                        headerCell.SetCellValue(tmpColumnName);
                    }
                    if (headerColumnTypeSplit[1].ToLower() == "time")
                    {
                        tmpColumnName = column.ColumnName.ToString().Replace("-Time", " (Time)");
                        headerCell.SetCellValue(tmpColumnName);
                    }
                }
                else
                {
                    headerCell.SetCellValue(column.ColumnName);
                }
                //oliver <end>

                //headerCell.SetCellValue(column.ColumnName);
                dicCellType.Add(column.Ordinal, column.DataType.ToString());
            }

            // handling value.
            int rowIndex = 1;
            foreach (DataRow row in sourceTable.Rows)
            {
                HSSFRow dataRow = (HSSFRow)sheet.CreateRow(rowIndex);
                foreach (DataColumn column in sourceTable.Columns)
                {
                    HSSFCell dataCell = (HSSFCell)dataRow.CreateCell(column.Ordinal);

                    switch (dicCellType[column.Ordinal])
                    {
                        case "System.Byte":
                        case "System.Short":
                        case "System.Int16":
                        case "System.Int32":
                        case "System.Int64":
                        case "System.Double":
                        case "System.Decimal":
                            if (row[column] != DBNull.Value)
                            {
                                dataCell.SetCellValue(Convert.ToDouble(row[column]));
                            }
                            break;
                        case "System.Bolean":
                            dataCell.SetCellType(NPOI.SS.UserModel.CellType.BOOLEAN);
                            dataCell.SetCellValue(Convert.ToString(row[column]));
                            break;
                        //case "System.DateTime":
                        //    if(row[column] != DBNull.Value)
                        //        dataCell.SetCellValue(Convert.ToDateTime(row[column]).ToString("dd/MM/yyyy hh:mm:ss"));
                        //    break;
                        default:
                            try
                            {
                                string strF = Convert.ToString(row[column]);

                                if (strF.Length > 32000)
                                {
                                    strF = StripTagsCharArray(strF);
                                }
                                if (strF.Length > 32000)
                                {
                                    strF = strF.Substring(0, 32000);
                                }
                                dataCell.SetCellValue(strF);
                            }
                            catch
                            {
                                //
                            }

                            break;
                    }
                }
                rowIndex++;
            }
            for (int i = 0; i < sourceTable.Columns.Count; i++)
            {
                sheet.AutoSizeColumn(i);
                try
                {
                    sheet.SetColumnWidth(i, Convert.ToInt32(sheet.GetColumnWidth(i) * 1.1));
                }
                catch
                {
                    //sheet.SetColumnWidth(i, 255);
                }
            }
            workbook.Write(memoryStream);
            memoryStream.Flush();

            //HttpResponse response = HttpContext.Current.Response;
            //response.ContentType = "application/vnd.ms-excel";
            //response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            //response.Clear();

            //response.BinaryWrite(memoryStream.GetBuffer());
            //response.End();

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";

            HttpContext.Current.Response.BinaryWrite(memoryStream.GetBuffer());
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
            return;

        }

        public static void ExportToExcel(DataTable sourceTable, string fileName, DataTable dtExportTemplate)
        {
            //Uncommented by RP - Ticket 4956 - Upload from Spreadsheet Object Error
            ExportToExcel(sourceTable, "Sheet 1", fileName, dtExportTemplate);
            //Commented by RP - Ticket 4956 - Upload from Spreadsheet Object Error
            //ExportToXLSX(sourceTable, "Sheet 1", fileName, dtExportTemplate);
        }

        public static bool isNumeric(string N)
        {
            bool YesNumeric = false;


            for (int i = 0; i < N.Length; i++)
            {

                if (char.IsNumber(N[i]))
                    YesNumeric = true;
            }

            if (YesNumeric == true)
            {
                return true;
            }

            return false;
        }

        public static bool HasSymbols(string str)
        {

            StringBuilder sb = new StringBuilder();
            bool bOnlyOneDot = false;
            bool bOnlyOneMinus = false;
            for (int i = 0; i < str.Length; i++)
            {
                bool bSymbol = true;
                if ((int)str[i] >= 48 && (int)str[i] <= 57)
                {

                    sb.Append(str[i]);
                    bSymbol = false;
                }
                if ((int)str[i] == 46 && bOnlyOneDot == false)
                {
                    sb.Append(str[i]);
                    bOnlyOneDot = true;
                    bSymbol = false;
                }
                if ((int)str[i] == 45 && bOnlyOneMinus == false && sb.Length == 0)
                {
                    sb.Append(str[i]);
                    bOnlyOneMinus = true;
                    bSymbol = false;
                }
                if (bSymbol)
                {
                    return true;
                }
            }

            return false;
        }
        public static string IgnoreSymbols(string str)
        {
           
            if (!isNumeric(str))
            {
                str = "0";
            }

            StringBuilder sb = new StringBuilder();
            bool bOnlyOneDot = false;
            bool bOnlyOneMinus = false;
            for (int i = 0; i < str.Length; i++)
            {
                if ((int)str[i] >= 48 && (int)str[i] <= 57)
                {

                    sb.Append(str[i]);

                }
                if ((int)str[i] == 46 && bOnlyOneDot == false)
                {
                    sb.Append(str[i]);
                    bOnlyOneDot = true;
                }
                if ((int)str[i] == 45 && bOnlyOneMinus == false && sb.Length == 0)
                {
                    sb.Append(str[i]);
                    bOnlyOneMinus = true;
                }
            }

            return sb.ToString();
        }

        public static void ExportToExcel(DataTable sourceTable, string sheetName, string fileName, DataTable dtExportTemplate)
        {
            /*
             History:
             Red 24102018: Updated the export of Calculation: Number type of field to number format not text.

             
             */
            HSSFWorkbook workbook = new HSSFWorkbook();
            MemoryStream memoryStream = new MemoryStream();
            HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet(String.IsNullOrEmpty(sheetName) ? "Sheet 1" : sheetName);
            HSSFRow headerRow = (HSSFRow)sheet.CreateRow(0);

            Dictionary<int, string> dicCellType = new Dictionary<int, string>();
            //HSSFCellStyle boldStyle =(HSSFCellStyle) workbook.CreateCellStyle();
            var boldStyle = workbook.CreateCellStyle();
            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.BOLD;
            boldStyle.SetFont(boldFont);


            boldStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            boldStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.MEDIUM;
            boldStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            boldStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;

            boldStyle.TopBorderColor = HSSFColor.BLACK.index;
            boldStyle.RightBorderColor = HSSFColor.BLACK.index;
            boldStyle.BottomBorderColor = HSSFColor.BLACK.index;
            boldStyle.LeftBorderColor = HSSFColor.BLACK.index;

            boldStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
            NPOI.HSSF.Record.PaletteRecord palette = new NPOI.HSSF.Record.PaletteRecord();
            HSSFPalette p = new HSSFPalette(palette);

            //Red Old Ticket 1461 - Not done
            ICellStyle _doubleCellStyle = workbook.CreateCellStyle();
            _doubleCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("#,##0.###");

            ICellStyle _intCellStyle = workbook.CreateCellStyle();
            _intCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("#,##0");

            //ICellStyle _boolCellStyle = workbook.CreateCellStyle();
            //_boolCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("BOOLEAN");

            ICellStyle _dateCellStyle = workbook.CreateCellStyle();
            _dateCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("dd/MM/yyyy");

            ICellStyle _dateTimeCellStyle = workbook.CreateCellStyle();
            _dateTimeCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("dd/MM/yyyy HH:mm:ss");

            ICellStyle _timeCellStyle = workbook.CreateCellStyle();
            _timeCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("hh:mm:ss");

            //p.AddColor(219, 229, 241);

            //boldStyle.FillForegroundColor = ((HSSFColor)p.FindSimilarColor(20, 230, 230)).GetIndex();

            boldStyle.FillForegroundColor = ((HSSFColor)setColor(workbook, 219, 229, 241)).GetIndex();


            // handling header.
            foreach (DataColumn column in sourceTable.Columns)
            {
                HSSFCell headerCell = (HSSFCell)headerRow.CreateCell(column.Ordinal);
                headerCell.CellStyle = boldStyle;

                if (column.ColumnName.IndexOf("-TTIIMMEE") > -1)
                {

                    column.ColumnName = column.ColumnName.ToString().Replace("-TTIIMMEE", (" (Time)"));
                }

                if (column.ColumnName.IndexOf("-DDAATTEE") > -1)
                {

                    column.ColumnName = column.ColumnName.ToString().Replace("-DDAATTEE", (" (Date)"));
                }

                headerCell.SetCellValue(column.ColumnName);
                dicCellType.Add(column.Ordinal, column.DataType.ToString());

            }

            // handling value.
            int rowIndex = 1;
            foreach (DataRow row in sourceTable.Rows)
            {
                HSSFRow dataRow = (HSSFRow)sheet.CreateRow(rowIndex);
                foreach (DataColumn column in sourceTable.Columns)
                {
                    HSSFCell dataCell = (HSSFCell)dataRow.CreateCell(column.Ordinal);
                    try
                    {
                                            
                        switch (dicCellType[column.Ordinal])
                        {
                            case "System.Byte":
                            case "System.Short":
                            case "System.Int16":
                            case "System.Int32":
                            case "System.Int64":
                            case "System.Double":
                            case "System.Decimal":
                                if (row[column] != DBNull.Value)
                                {
                                    dataCell.SetCellValue(Convert.ToDouble(row[column]));
                                }
                                break;
                            case "System.Bolean":
                                dataCell.SetCellType(NPOI.SS.UserModel.CellType.BOOLEAN);
                                dataCell.SetCellValue(Convert.ToString(row[column]));
                                break;

                            default:
                                try
                                {
                                    string strF = Convert.ToString(row[column]);

                                    if (strF.Length > 32000)
                                    {
                                        strF = StripTagsCharArray(strF);
                                    }
                                    if (strF.Length > 32000)
                                    {
                                        strF = strF.Substring(0, 32000);
                                    }

                                    if (dtExportTemplate != null)
                                    {
                                        double dtrF;
                                        foreach (DataRow drow in dtExportTemplate.Rows)
                                        {
                                            /* all should be equals; 
                                             * only if it's a date and time that is separate; 
                                             * that is the only reason to not equal */
                                            if (drow["ExportHeaderName"].ToString() == column.ColumnName)
                                            {
                                                if (drow["ColumnType"].ToString() == "number" || (drow["ColumnType"].ToString() == "calculation"
                                                    && drow["TextType"].ToString() == "n")
                                                    //&& HasSymbols(row[column].ToString()) == false
                                                    )
                                                {
                                                    if (row[column].ToString() != "")
                                                    {
                                                        try
                                                        {
                                                            bool IsRound = false;
                                                            int iRound = int.Parse(drow["RoundNumber"].ToString());
                                                            if (drow["IsRound"] != DBNull.Value)
                                                            {
                                                                if (drow["IsRound"].ToString().ToLower() == "true")
                                                                {
                                                                    IsRound = true;
                                                                }
                                                                else
                                                                {
                                                                    iRound = 0;
                                                                }
                                                            }

                                                            // Preserve leading symbol if any
                                                            string symbol = "";
                                                            char c = row[column].ToString()[0];
                                                            if ((c < '0' || c > '9') && (c != '-'))
                                                                symbol = c.ToString();

                                                            //Ticket 4225 - Decimal places - need to retain the data by JV
                                                            //Original data should not be manipulated. Data should show original number regardless of decimal places.
                                                            //Decimal places should only be applied on display (RecordList).

                                                            /*Red 27102018 corrected (as per Jon/Simon adviced): 
                                                             * we should export what we are 
                                                             * seeing in the view (RecordList) 
                                                             * i.e. configured decimal places of the field; 
                                                             * not the original data entered or uploaded */

                                                            dtrF = double.Parse(IgnoreSymbols(row[column].ToString()));
                                                            if (IsRound)
                                                            {
                                                                dtrF = Math.Round(double.Parse(IgnoreSymbols(row[column].ToString())), iRound);
                                                            }

                                                            if (String.IsNullOrEmpty(symbol))
                                                            {
                                                                dataCell.SetCellValue(dtrF);
                                                                string number = dtrF.ToString();
                                                                int length = 0;
                                                                if (number.IndexOf(".") > -1)
                                                                {
                                                                    length = number.Substring(number.IndexOf(".") + 1).Length;
                                                                }
                                                                string decimalPlaces = "";

                                                                if (length > 0)
                                                                {
                                                                    /* append zero (0) if decimal value in the data is 
                                                                     * less than the set decimal places 
                                                                     * e.g. configured decimal places is 2; 
                                                                     * data is 1234.3 
                                                                     * export should be 1234.30 */
                                                                    if (length < iRound)
                                                                    {
                                                                        length = iRound;
                                                                    }
                                                                    /* get how many decimal places to set */
                                                                    for (int i = 0; i < length; i++)
                                                                    {
                                                                        decimalPlaces += "0";
                                                                    }
                                                                    decimalPlaces = "." + decimalPlaces;
                                                                }
                                                                /* if data has no decimal places in it but configured the decimal places 
                                                                 * e.g. data is 1234 
                                                                 * configured 2 decimal places
                                                                 * export should be 1234.00 */
                                                                else if (iRound > 0)
                                                                {
                                                                    for (int i = 0; i < iRound; i++)
                                                                    {
                                                                        decimalPlaces += "0";
                                                                    }
                                                                    decimalPlaces = "." + decimalPlaces;

                                                                }

                                                                /* set the excel cell formal for the value */
                                                                ICellStyle _customerNumberStyle = workbook.CreateCellStyle();

                                                                /* Not Round off, but value has decimal places */
                                                                _customerNumberStyle.DataFormat = workbook.CreateDataFormat().GetFormat("###0" + decimalPlaces);

                                                                /* if Round off, set the comma thousand separator */
                                                                if (IsRound)
                                                                {
                                                                    _customerNumberStyle.DataFormat = workbook.CreateDataFormat().GetFormat("#,##0" + decimalPlaces);
                                                                }
                                                                dataCell.CellStyle = _customerNumberStyle;
                                                            }
                                                            else
                                                            {
                                                                dataCell.SetCellValue(row[column].ToString());
                                                            }
                                                        }
                                                        catch
                                                        {
                                                            //Red
                                                        }
                                                    }
                                                }
                                                else if (drow["ColumnType"].ToString() == "date")
                                                {
                                                    if (row[column].ToString() != "")
                                                    {
                                                        try
                                                        {
                                                            DateTime dtDate = Convert.ToDateTime(row[column]);
                                                            dataCell.SetCellValue(dtDate);
                                                            dataCell.CellStyle = _dateCellStyle;
                                                        }
                                                        catch
                                                        {
                                                            //Red
                                                        }
                                                    }
                                                }
                                                else if (drow["ColumnType"].ToString() == "datetime")
                                                {
                                                    if (row[column].ToString() != "")
                                                    {
                                                        try
                                                        {
                                                            DateTime dtDate = Convert.ToDateTime(row[column]);
                                                            dataCell.SetCellValue(dtDate);
                                                            dataCell.CellStyle = _dateTimeCellStyle;
                                                        }
                                                        catch
                                                        {
                                                            //Red
                                                        }
                                                    }
                                                }
                                                else if (drow["ColumnType"].ToString() == "time")
                                                {
                                                    if (row[column].ToString() != "")
                                                    {
                                                        try
                                                        {
                                                            DateTime dtDate = Convert.ToDateTime(row[column]);
                                                            dataCell.SetCellValue(dtDate);
                                                            dataCell.CellStyle = _timeCellStyle;
                                                        }
                                                        catch
                                                        {
                                                            //Red
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    dataCell.SetCellValue(strF);
                                                }
                                            }
                                            else if (column.ColumnName.IndexOf(" (Date)") > -1)
                                            {
                                                if (drow["ColumnType"].ToString() == "datetime")
                                                {
                                                    DateTime dtDate = Convert.ToDateTime(row[column]);
                                                    dataCell.SetCellValue(dtDate);
                                                    dataCell.CellStyle = _dateCellStyle;
                                                }
                                            }
                                            else if (column.ColumnName.IndexOf(" (Time)") > -1)
                                            {
                                                if (drow["ColumnType"].ToString() == "datetime")
                                                {
                                                     DateTime dtDate = Convert.ToDateTime(row[column]);
                                                    dataCell.SetCellValue(dtDate);
                                                    dataCell.CellStyle = _timeCellStyle;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        dataCell.SetCellValue(strF);
                                    }
                                }
                                catch
                                {
                                    //Red
                                }

                                break;
                        }
                    }
                    catch (Exception ex)
                    {

                        ErrorLog theErrorLog = new ErrorLog(null, "ExportTemplate: Output Excel", ex.Message, ex.StackTrace, DateTime.Now, "App_Code: ExportUtil");
                        SystemData.ErrorLog_Insert(theErrorLog);
                    }



                }
                rowIndex++;
            }


            for (int i = 0; i < sourceTable.Columns.Count; i++)
            {
                sheet.AutoSizeColumn(i);
                try
                {
                    sheet.SetColumnWidth(i, Convert.ToInt32(sheet.GetColumnWidth(i) * 1.1));
                }
                catch
                {
                    //sheet.SetColumnWidth(i, 255);
                }
            }


            workbook.Write(memoryStream);
            memoryStream.Flush();

            //RP Added Ticket 5070
            HttpCookie cookie = new HttpCookie("ExportReport");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            HttpContext.Current.Response.AppendCookie(cookie);
            //End Modification

            HttpResponse response = HttpContext.Current.Response;
            response.ContentType = "application/vnd.ms-excel";
            response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            response.Clear();

            response.BinaryWrite(memoryStream.GetBuffer());
            response.End();

        }



        public static HSSFColor setColor(HSSFWorkbook workbook, byte r, byte g, byte b)
        {
            HSSFPalette palette = workbook.GetCustomPalette();
            HSSFColor hssfColor = null;
            try
            {
                hssfColor = palette.FindColor(r, g, b);
                if (hssfColor == null)
                {
                    palette.SetColorAtIndex(HSSFColor.LAVENDER.index, r, g, b);
                    hssfColor = palette.GetColor(HSSFColor.LAVENDER.index);
                }
            }
            catch (Exception e)
            {
                //logger.error(e);
            }

            return hssfColor;
        }

        //RP DELETED - Ticket 4956 - Upload From Spreadsheet Object Error
        //public static void ExportToXLSX(DataTable sourceTable, string sheetName, string fileName, DataTable dtExportTemplate)
        //{
        //    Dictionary<int, string> dictCellType = new Dictionary<int, string>();

        //    UInt32Value lastCellFormat = 0;
        //    CellFormats cellFormats = InitCellFormats(out lastCellFormat);
        //    const int StyleTextNormal = 0;
        //    const int StyleTextBoldCentered = 1;
        //    const int StyleTextNormalRight = 2;
        //    const int StyleShortDateNormal = 3;
        //    const int StyleShortTimeNormal = 4;
        //    const int StyleShortDateTimeNormal = 5;
        //    const int StyleShortDateTimeNoSecondsNormal = 6;
        //    const int StyleShortTimeNoSecondsNormal = 7;
        //    const int StyleNumber0 = 8;
        //    const int StyleNumber2 = 9;

        //    UInt32Value lastNumberingFormat = 164U;
        //    NumberingFormats numberingFormats = InitNumberingFormats(out lastNumberingFormat);

        //    Dictionary<string, UInt32Value> cellFormatDictionary = new Dictionary<string, UInt32Value>();

        //    using (MemoryStream memoryStream = new MemoryStream())
        //    {
        //        using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Create(memoryStream,
        //            SpreadsheetDocumentType.Workbook))
        //        {
        //            spreadSheet.AddWorkbookPart();

        //            WorkbookStylesPart wbsp = spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();

        //            spreadSheet.WorkbookPart.Workbook = new Workbook();
        //            WorksheetPart wsp = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();

        //            wsp.Worksheet = new Worksheet();

        //            ss.Columns columns = new ss.Columns();
        //            for (int i = 0; i < sourceTable.Columns.Count; i++)
        //            {
        //                string columnName = sourceTable.Columns[i].ColumnName;
        //                if (sourceTable.Columns[i].ColumnName.IndexOf("-TTIIMMEE", StringComparison.Ordinal) > -1)
        //                {
        //                    columnName = sourceTable.Columns[i].ColumnName.ToString().Replace("-TTIIMMEE", (" (Time)"));
        //                }

        //                if (sourceTable.Columns[i].ColumnName.IndexOf("-DDAATTEE", StringComparison.Ordinal) > -1)
        //                {
        //                    columnName = sourceTable.Columns[i].ColumnName.ToString().Replace("-DDAATTEE", (" (Date)"));
        //                }

        //                columns.AppendChild(new ss.Column
        //                {
        //                    Min = (UInt32) i + 1,
        //                    Max = (UInt32) i + 1,
        //                    Width = GetWidth("Calibri", 11, columnName),
        //                    CustomWidth = true
        //                });

        //                dictCellType.Add(sourceTable.Columns[i].Ordinal, sourceTable.Columns[i].DataType.ToString());
        //            }

        //            wsp.Worksheet.AppendChild(columns);

        //            SheetData sheetData = wsp.Worksheet.AppendChild(new SheetData());

        //            Row hr = sheetData.AppendChild(new Row());
        //            foreach (DataColumn column in sourceTable.Columns)
        //            {
        //                string columnName = column.ColumnName;
        //                if (column.ColumnName.IndexOf("-TTIIMMEE", StringComparison.Ordinal) > -1)
        //                {
        //                    columnName = column.ColumnName.ToString().Replace("-TTIIMMEE", (" (Time)"));
        //                }

        //                if (column.ColumnName.IndexOf("-DDAATTEE", StringComparison.Ordinal) > -1)
        //                {
        //                    columnName = column.ColumnName.ToString().Replace("-DDAATTEE", (" (Date)"));
        //                }

        //                Cell cell = new Cell
        //                {
        //                    CellValue = new ss.CellValue(columnName),
        //                    DataType = new EnumValue<CellValues>(CellValues.String),
        //                    StyleIndex = StyleTextBoldCentered
        //                };
        //                hr.AppendChild(cell);
        //            }

        //            foreach (DataRow row in sourceTable.Rows)
        //            {
        //                Row r = sheetData.AppendChild(new Row());

        //                foreach (DataColumn column in sourceTable.Columns)
        //                {
        //                    Cell cell = new Cell();
        //                    try
        //                    {
        //                        switch (dictCellType[column.Ordinal])
        //                        {
        //                            case "System.Byte":
        //                            case "System.Short":
        //                            case "System.Int16":
        //                            case "System.Int32":
        //                            case "System.Int64":
        //                            case "System.Double":
        //                            case "System.Decimal":
        //                                if (row[column] != DBNull.Value)
        //                                {
        //                                    cell.CellValue = new ss.CellValue(row[column].ToString());
        //                                    cell.DataType = new EnumValue<CellValues>(CellValues.Number);
        //                                }
        //                                break;
        //                            case "System.Boolean":
        //                                cell.CellValue = new ss.CellValue((bool) row[column] ? "Yes" : "No");
        //                                cell.DataType = new EnumValue<CellValues>(CellValues.String);
        //                                break;

        //                            default:
        //                                try
        //                                {
        //                                    string strF = Convert.ToString(row[column]);

        //                                    if (strF.Length > 32000)
        //                                    {
        //                                        strF = StripTagsCharArray(strF);
        //                                    }

        //                                    if (strF.Length > 32000)
        //                                    {
        //                                        strF = strF.Substring(0, 32000);
        //                                    }

        //                                    if (dtExportTemplate != null)
        //                                    {
        //                                        foreach (DataRow dataRow in dtExportTemplate.Rows)
        //                                        {
        //                                            /* all should be equals; 
        //                                             * only if it's a date and time that is separate; 
        //                                             * that is the only reason to not equal */
        //                                            if (dataRow["ExportHeaderName"].ToString() == column.ColumnName)
        //                                            {
        //                                                if (dataRow["ColumnType"].ToString() == "number" ||
        //                                                    (dataRow["ColumnType"].ToString() == "calculation"
        //                                                     && dataRow["TextType"].ToString() == "n"))
        //                                                {
        //                                                    if (row[column].ToString() != "")
        //                                                    {
        //                                                        try
        //                                                        {
        //                                                            bool isRound = false;
        //                                                            int iRound =
        //                                                                int.Parse(dataRow["RoundNumber"].ToString());
        //                                                            if (dataRow["IsRound"] != DBNull.Value)
        //                                                            {
        //                                                                if (dataRow["IsRound"].ToString().ToLower() ==
        //                                                                    "true")
        //                                                                {
        //                                                                    isRound = true;
        //                                                                }
        //                                                                else
        //                                                                {
        //                                                                    iRound = 0;
        //                                                                }
        //                                                            }

        //                                                            // Preserve leading symbols if any
        //                                                            string symbol = "";
        //                                                            int i = 0;
        //                                                            int j = row[column].ToString().Length;
        //                                                            while (i < j)
        //                                                            {
        //                                                                char c = row[column].ToString()[i++];
        //                                                                if ((c < '0' || c > '9') && (c != '-'))
        //                                                                    symbol += c;
        //                                                                else
        //                                                                    break;
        //                                                            }

        //                                                            //Ticket 4225 - Decimal places - need to retain the data by JV
        //                                                            //Original data should not be manipulated. Data should show original number regardless of decimal places.
        //                                                            //Decimal places should only be applied on display (RecordList).

        //                                                            /*Red 27102018 corrected (as per Jon/Simon adviced): 
        //                                                             * we should export what we are 
        //                                                             * seeing in the view (RecordList) 
        //                                                             * i.e. configured decimal places of the field; 
        //                                                             * not the original data entered or uploaded */

        //                                                            double dtrF =
        //                                                                double.Parse(
        //                                                                    IgnoreSymbols(row[column].ToString()));
        //                                                            if (isRound)
        //                                                            {
        //                                                                dtrF = Math.Round(
        //                                                                    double.Parse(
        //                                                                        IgnoreSymbols(row[column].ToString())),
        //                                                                    iRound);
        //                                                            }

        //                                                            if (String.IsNullOrEmpty(symbol))
        //                                                            {
        //                                                                cell.CellValue = new ss.CellValue(dtrF.ToString(CultureInfo.InvariantCulture));
        //                                                                cell.DataType = new EnumValue<CellValues>(CellValues.Number);
        //                                                                if (isRound)
        //                                                                {
        //                                                                    switch (iRound)
        //                                                                    {
        //                                                                        case 0:
        //                                                                            cell.StyleIndex = StyleNumber0;
        //                                                                            break;
        //                                                                        case 2:
        //                                                                            cell.StyleIndex = StyleNumber2;
        //                                                                            break;
        //                                                                        default:
        //                                                                            string key = String.Format("N{0}",
        //                                                                                iRound);
        //                                                                            if (!cellFormatDictionary
        //                                                                                .ContainsKey(key))
        //                                                                            {
        //                                                                                ++lastNumberingFormat;
        //                                                                                ++lastCellFormat;
        //                                                                                numberingFormats.AppendChild(
        //                                                                                    new NumberingFormat
        //                                                                                    {
        //                                                                                        NumberFormatId =
        //                                                                                            lastNumberingFormat,
        //                                                                                        FormatCode =
        //                                                                                            "#,##0." + new String(
        //                                                                                                '0', iRound)
        //                                                                                    });
        //                                                                                cellFormats.AppendChild(
        //                                                                                    new
        //                                                                                        CellFormat()
        //                                                                                        {
        //                                                                                            NumberFormatId =
        //                                                                                                lastNumberingFormat,
        //                                                                                            FontId =
        //                                                                                                (UInt32Value)
        //                                                                                                0U,
        //                                                                                            FillId =
        //                                                                                                (UInt32Value)
        //                                                                                                0U,
        //                                                                                            BorderId =
        //                                                                                                (UInt32Value)
        //                                                                                                0U,
        //                                                                                            FormatId =
        //                                                                                                (UInt32Value)
        //                                                                                                0U,
        //                                                                                            ApplyNumberFormat =
        //                                                                                                true
        //                                                                                        });

        //                                                                                cellFormatDictionary.Add(
        //                                                                                    key,
        //                                                                                    lastCellFormat);
        //                                                                            }

        //                                                                            cell.StyleIndex =
        //                                                                                cellFormatDictionary[key];
        //                                                                            break;
        //                                                                    }
        //                                                                }
        //                                                            }
        //                                                            else
        //                                                            {
        //                                                                string s = row[column].ToString();
        //                                                                string fmt = "#,##0";
        //                                                                if (isRound && iRound > 0)
        //                                                                {
        //                                                                    fmt += "." + new String('0', iRound);
        //                                                                    s = symbol + dtrF.ToString(fmt);
        //                                                                }
        //                                                                cell.CellValue = new ss.CellValue(s);
        //                                                                cell.DataType = new EnumValue<CellValues>(CellValues.String);
        //                                                                cell.StyleIndex = StyleTextNormalRight;
        //                                                            }
        //                                                        }
        //                                                        catch (Exception ex)
        //                                                        {
        //                                                            ErrorLog theErrorLog = new ErrorLog(null,
        //                                                                "ExportTemplate: Output \"Number\" Value to XLSX",
        //                                                                ex.Message, ex.StackTrace, DateTime.Now,
        //                                                                "App_Code: ExportUtil");
        //                                                            SystemData.ErrorLog_Insert(theErrorLog);
        //                                                        }
        //                                                    }
        //                                                }
        //                                                else if (dataRow["ColumnType"].ToString() == "date")
        //                                                {
        //                                                    if (row[column].ToString() != "")
        //                                                    {
        //                                                        try
        //                                                        {
        //                                                            cell.CellValue = new ss.CellValue(
        //                                                                (DateTime.Parse(row[column].ToString()))
        //                                                                .ToOADate().ToString(CultureInfo
        //                                                                    .InvariantCulture));
        //                                                            cell.DataType =
        //                                                                new EnumValue<CellValues>(CellValues.Number);
        //                                                            cell.StyleIndex = StyleShortDateNormal;
        //                                                        }
        //                                                        catch (Exception ex)
        //                                                        {
        //                                                            ErrorLog theErrorLog = new ErrorLog(null,
        //                                                                "ExportTemplate: Output \"Date\" Value to XLSX",
        //                                                                ex.Message, ex.StackTrace, DateTime.Now,
        //                                                                "App_Code: ExportUtil");
        //                                                            SystemData.ErrorLog_Insert(theErrorLog);
        //                                                        }
        //                                                    }
        //                                                }
        //                                                else if (dataRow["ColumnType"].ToString() == "datetime")
        //                                                {
        //                                                    if (row[column].ToString() != "")
        //                                                    {
        //                                                        try
        //                                                        {
        //                                                            cell.CellValue = new ss.CellValue(
        //                                                                (DateTime.Parse(row[column].ToString()))
        //                                                                .ToOADate().ToString(CultureInfo
        //                                                                    .InvariantCulture));
        //                                                            cell.DataType =
        //                                                                new EnumValue<CellValues>(CellValues.Number);
        //                                                            if (dataRow["HideTimeSecond"] != DBNull.Value && (bool)dataRow["HideTimeSecond"])
        //                                                                cell.StyleIndex = StyleShortDateTimeNoSecondsNormal;
        //                                                            else
        //                                                                cell.StyleIndex = StyleShortDateTimeNormal;
        //                                                        }
        //                                                        catch (Exception ex)
        //                                                        {
        //                                                            ErrorLog theErrorLog = new ErrorLog(null,
        //                                                                "ExportTemplate: Output \"DateTime\" Value to XLSX",
        //                                                                ex.Message, ex.StackTrace, DateTime.Now,
        //                                                                "App_Code: ExportUtil");
        //                                                            SystemData.ErrorLog_Insert(theErrorLog);
        //                                                        }
        //                                                    }
        //                                                }
        //                                                else if (dataRow["ColumnType"].ToString() == "time")
        //                                                {
        //                                                    if (row[column].ToString() != "")
        //                                                    {
        //                                                        try
        //                                                        {
        //                                                            cell.CellValue = new ss.CellValue(
        //                                                                (DateTime.Parse(row[column].ToString()))
        //                                                                .ToOADate().ToString(CultureInfo
        //                                                                    .InvariantCulture));
        //                                                            cell.DataType =
        //                                                                new EnumValue<CellValues>(CellValues.Number);
        //                                                            if (dataRow["HideTimeSecond"] != DBNull.Value && (bool)dataRow["HideTimeSecond"])
        //                                                                cell.StyleIndex = StyleShortTimeNoSecondsNormal;
        //                                                            else
        //                                                                cell.StyleIndex = StyleShortTimeNormal;
        //                                                        }
        //                                                        catch (Exception ex)
        //                                                        {
        //                                                            ErrorLog theErrorLog = new ErrorLog(null,
        //                                                                "ExportTemplate: Output \"Time\" Value to XLSX",
        //                                                                ex.Message, ex.StackTrace, DateTime.Now,
        //                                                                "App_Code: ExportUtil");
        //                                                            SystemData.ErrorLog_Insert(theErrorLog);
        //                                                        }
        //                                                    }
        //                                                }
        //                                                else
        //                                                {
        //                                                    cell.CellValue = new ss.CellValue(strF);
        //                                                    cell.DataType =
        //                                                        new EnumValue<CellValues>(CellValues.String);
        //                                                }
        //                                            }
        //                                            else if (dataRow["ExportHeaderName"] + "-DDAATTEE" == column.ColumnName)
        //                                            {
        //                                                if (dataRow["ColumnType"].ToString() == "datetime")
        //                                                {
        //                                                    cell.CellValue = new ss.CellValue((DateTime.Parse(row[column].ToString()))
        //                                                        .ToOADate().ToString(CultureInfo.InvariantCulture));
        //                                                    cell.DataType =
        //                                                        new EnumValue<CellValues>(CellValues.Number);
        //                                                    cell.StyleIndex = StyleShortDateNormal;
        //                                                }
        //                                            }
        //                                            else if (dataRow["ExportHeaderName"] + "-TTIIMMEE" == column.ColumnName)
        //                                            {
        //                                                if (dataRow["ColumnType"].ToString() == "datetime")
        //                                                {
        //                                                    cell.CellValue = new ss.CellValue((DateTime.Parse(row[column].ToString()))
        //                                                        .ToOADate().ToString(CultureInfo.InvariantCulture));
        //                                                    cell.DataType =
        //                                                        new EnumValue<CellValues>(CellValues.Number);
        //                                                    if (dataRow["HideTimeSecond"] != DBNull.Value && (bool)dataRow["HideTimeSecond"])
        //                                                        cell.StyleIndex = StyleShortTimeNoSecondsNormal;
        //                                                    else
        //                                                        cell.StyleIndex = StyleShortTimeNormal;
        //                                                }
        //                                            }
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        cell.CellValue = new ss.CellValue(strF);
        //                                        cell.DataType = new EnumValue<CellValues>(CellValues.String);
        //                                    }
        //                                }
        //                                catch (Exception ex)
        //                                {
        //                                    ErrorLog theErrorLog = new ErrorLog(null, "ExportTemplate: Output XLSX",
        //                                        ex.Message, ex.StackTrace, DateTime.Now, "App_Code: ExportUtil");
        //                                    SystemData.ErrorLog_Insert(theErrorLog);
        //                                }

        //                                break;
        //                        }

        //                        r.AppendChild(cell);
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        ErrorLog theErrorLog = new ErrorLog(null, "Export: Output XLSX", ex.Message,
        //                            ex.StackTrace, DateTime.Now, "App_Code: ExportUtil");
        //                        SystemData.ErrorLog_Insert(theErrorLog);
        //                    }
        //                }
        //            }

        //            wbsp.Stylesheet = CreateStyleSheet(numberingFormats, cellFormats);
        //            //wbsp.Stylesheet.Save();

        //            // save worksheet
        //            wsp.Worksheet.Save();

        //            // create the worksheet to workbook relation
        //            spreadSheet.WorkbookPart.Workbook.AppendChild(new Sheets());
        //            spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>().AppendChild(new Sheet()
        //            {
        //                Id = spreadSheet.WorkbookPart.GetIdOfPart(wsp),
        //                SheetId = 1,
        //                Name = String.IsNullOrEmpty(sheetName) ? "Sheet 1" : sheetName
        //            });

        //            spreadSheet.WorkbookPart.Workbook.Save();
        //        }

        //        //Guid uniqueid = new Guid();
        //        //String exportname = System.Web.HttpContext.Current.Session["FilesPhisicalPath"].ToString();
        //        //exportname = exportname + "\\UserFiles\\AppFiles" + "\\" + uniqueid.ToString();
        //        //System.IO.FileStream fs = new FileStream(exportname, FileMode.Open, FileAccess.ReadWrite);
        //        //memoryStream.CopyTo(fs);

        //        HttpResponse response = HttpContext.Current.Response;
        //        response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //        response.AppendHeader("content-disposition", string.Format("attachment;filename={0}", fileName));
        //        memoryStream.Position = 0;
        //        memoryStream.CopyTo(response.OutputStream);

        //        /* == Red 09092019: Append cookie */
        //        HttpCookie cookie = new HttpCookie("ExportReport");
        //        cookie.Value = "Flag";
        //        cookie.Expires = DateTime.Now.AddDays(1);
        //        HttpContext.Current.Response.AppendCookie(cookie);
        //        /* == End Red == */

        //        response.Flush();
        //        response.End();
        //    }
        //}


        public static void ExportToXLSX(DataTable sourceTable, string sheetName, string fileName, DataTable dtExportTemplate)
        {
            Dictionary<int, string> dictCellType = new Dictionary<int, string>();

            UInt32Value lastCellFormat = 0;
            CellFormats cellFormats = InitCellFormats(out lastCellFormat);
            const int StyleTextNormal = 0;
            const int StyleTextBoldCentered = 1;
            const int StyleTextNormalRight = 2;
            const int StyleShortDateNormal = 3;
            const int StyleShortTimeNormal = 4;
            const int StyleShortDateTimeNormal = 5;
            const int StyleShortDateTimeNoSecondsNormal = 6;
            const int StyleShortTimeNoSecondsNormal = 7;
            const int StyleNumber0 = 8;
            const int StyleNumber2 = 9;

            UInt32Value lastNumberingFormat = 164U;
            NumberingFormats numberingFormats = InitNumberingFormats(out lastNumberingFormat);

            Dictionary<string, UInt32Value> cellFormatDictionary = new Dictionary<string, UInt32Value>();

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Create(memoryStream,
                    SpreadsheetDocumentType.Workbook))
                {
                    spreadSheet.AddWorkbookPart();

                    WorkbookStylesPart wbsp = spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();

                    spreadSheet.WorkbookPart.Workbook = new Workbook();
                    WorksheetPart wsp = spreadSheet.WorkbookPart.AddNewPart<WorksheetPart>();

                    wsp.Worksheet = new Worksheet();

                    ss.Columns columns = new ss.Columns();
                    for (int i = 0; i < sourceTable.Columns.Count; i++)
                    {
                        string columnName = sourceTable.Columns[i].ColumnName;
                        if (sourceTable.Columns[i].ColumnName.IndexOf("-TTIIMMEE", StringComparison.Ordinal) > -1)
                        {
                            columnName = sourceTable.Columns[i].ColumnName.ToString().Replace("-TTIIMMEE", (" (Time)"));
                        }

                        if (sourceTable.Columns[i].ColumnName.IndexOf("-DDAATTEE", StringComparison.Ordinal) > -1)
                        {
                            columnName = sourceTable.Columns[i].ColumnName.ToString().Replace("-DDAATTEE", (" (Date)"));
                        }

                        columns.AppendChild(new ss.Column
                        {
                            Min = (UInt32)i + 1,
                            Max = (UInt32)i + 1,
                            Width = GetWidth("Calibri", 11, columnName),
                            CustomWidth = true
                        });

                        dictCellType.Add(sourceTable.Columns[i].Ordinal, sourceTable.Columns[i].DataType.ToString());
                    }

                    wsp.Worksheet.AppendChild(columns);

                    SheetData sheetData = wsp.Worksheet.AppendChild(new SheetData());

                    Row hr = sheetData.AppendChild(new Row());
                    foreach (DataColumn column in sourceTable.Columns)
                    {
                        string columnName = column.ColumnName;
                        if (column.ColumnName.IndexOf("-TTIIMMEE", StringComparison.Ordinal) > -1)
                        {
                            columnName = column.ColumnName.ToString().Replace("-TTIIMMEE", (" (Time)"));
                        }

                        if (column.ColumnName.IndexOf("-DDAATTEE", StringComparison.Ordinal) > -1)
                        {
                            columnName = column.ColumnName.ToString().Replace("-DDAATTEE", (" (Date)"));
                        }

                        Cell cell = new Cell
                        {
                            CellValue = new ss.CellValue(columnName),
                            DataType = new EnumValue<CellValues>(CellValues.String),
                            StyleIndex = StyleTextBoldCentered
                        };
                        hr.AppendChild(cell);
                    }

                    foreach (DataRow row in sourceTable.Rows)
                    {
                        Row r = sheetData.AppendChild(new Row());

                        foreach (DataColumn column in sourceTable.Columns)
                        {
                            Cell cell = new Cell();
                            try
                            {
                                switch (dictCellType[column.Ordinal])
                                {
                                    case "System.Byte":
                                    case "System.Short":
                                    case "System.Int16":
                                    case "System.Int32":
                                    case "System.Int64":
                                    case "System.Double":
                                    case "System.Decimal":
                                        if (row[column] != DBNull.Value)
                                        {
                                            cell.CellValue = new ss.CellValue(row[column].ToString());
                                            cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                        }
                                        break;
                                    case "System.Boolean":
                                        cell.CellValue = new ss.CellValue((bool)row[column] ? "Yes" : "No");
                                        cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                        break;

                                    default:
                                        try
                                        {
                                            string strF = Convert.ToString(row[column]);

                                            if (strF.Length > 32000)
                                            {
                                                strF = StripTagsCharArray(strF);
                                            }

                                            if (strF.Length > 32000)
                                            {
                                                strF = strF.Substring(0, 32000);
                                            }

                                            if (dtExportTemplate != null)
                                            {
                                                foreach (DataRow dataRow in dtExportTemplate.Rows)
                                                {
                                                    /* all should be equals; 
                                                     * only if it's a date and time that is separate; 
                                                     * that is the only reason to not equal */
                                                    if (dataRow["ExportHeaderName"].ToString() == column.ColumnName)
                                                    {
                                                        if (dataRow["ColumnType"].ToString() == "number" ||
                                                            (dataRow["ColumnType"].ToString() == "calculation"
                                                             && dataRow["TextType"].ToString() == "n"))
                                                        {
                                                            if (row[column].ToString() != "")
                                                            {
                                                                try
                                                                {
                                                                    bool isRound = false;
                                                                    int iRound =
                                                                        int.Parse(dataRow["RoundNumber"].ToString());
                                                                    if (dataRow["IsRound"] != DBNull.Value)
                                                                    {
                                                                        if (dataRow["IsRound"].ToString().ToLower() ==
                                                                            "true")
                                                                        {
                                                                            isRound = true;
                                                                        }
                                                                        else
                                                                        {
                                                                            iRound = 0;
                                                                        }
                                                                    }

                                                                    // Preserve leading symbols if any
                                                                    string symbol = "";
                                                                    int i = 0;
                                                                    int j = row[column].ToString().Length;
                                                                    while (i < j)
                                                                    {
                                                                        char c = row[column].ToString()[i++];
                                                                        if ((c < '0' || c > '9') && (c != '-'))
                                                                            symbol += c;
                                                                        else
                                                                            break;
                                                                    }

                                                                    //Ticket 4225 - Decimal places - need to retain the data by JV
                                                                    //Original data should not be manipulated. Data should show original number regardless of decimal places.
                                                                    //Decimal places should only be applied on display (RecordList).

                                                                    /*Red 27102018 corrected (as per Jon/Simon adviced): 
                                                                     * we should export what we are 
                                                                     * seeing in the view (RecordList) 
                                                                     * i.e. configured decimal places of the field; 
                                                                     * not the original data entered or uploaded */

                                                                    double dtrF =
                                                                        double.Parse(
                                                                            IgnoreSymbols(row[column].ToString()));
                                                                    if (isRound)
                                                                    {
                                                                        dtrF = Math.Round(
                                                                            double.Parse(
                                                                                IgnoreSymbols(row[column].ToString())),
                                                                            iRound);
                                                                    }

                                                                    if (String.IsNullOrEmpty(symbol))
                                                                    {
                                                                        cell.CellValue = new ss.CellValue(dtrF.ToString(CultureInfo.InvariantCulture));
                                                                        cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                                                        if (isRound)
                                                                        {
                                                                            switch (iRound)
                                                                            {
                                                                                case 0:
                                                                                    cell.StyleIndex = StyleNumber0;
                                                                                    break;
                                                                                case 2:
                                                                                    cell.StyleIndex = StyleNumber2;
                                                                                    break;
                                                                                default:
                                                                                    string key = String.Format("N{0}",
                                                                                        iRound);
                                                                                    if (!cellFormatDictionary
                                                                                        .ContainsKey(key))
                                                                                    {
                                                                                        ++lastNumberingFormat;
                                                                                        ++lastCellFormat;
                                                                                        numberingFormats.AppendChild(
                                                                                            new NumberingFormat
                                                                                            {
                                                                                                NumberFormatId =
                                                                                                    lastNumberingFormat,
                                                                                                FormatCode =
                                                                                                    "#,##0." + new String(
                                                                                                        '0', iRound)
                                                                                            });
                                                                                        cellFormats.AppendChild(
                                                                                            new
                                                                                                CellFormat()
                                                                                            {
                                                                                                NumberFormatId =
                                                                                                        lastNumberingFormat,
                                                                                                FontId =
                                                                                                        (UInt32Value)
                                                                                                        0U,
                                                                                                FillId =
                                                                                                        (UInt32Value)
                                                                                                        0U,
                                                                                                BorderId =
                                                                                                        (UInt32Value)
                                                                                                        0U,
                                                                                                FormatId =
                                                                                                        (UInt32Value)
                                                                                                        0U,
                                                                                                ApplyNumberFormat =
                                                                                                        true
                                                                                            });

                                                                                        cellFormatDictionary.Add(
                                                                                            key,
                                                                                            lastCellFormat);
                                                                                    }

                                                                                    cell.StyleIndex =
                                                                                        cellFormatDictionary[key];
                                                                                    break;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        string s = row[column].ToString();
                                                                        string fmt = "#,##0";
                                                                        if (isRound && iRound > 0)
                                                                        {
                                                                            fmt += "." + new String('0', iRound);
                                                                            s = symbol + dtrF.ToString(fmt);
                                                                        }
                                                                        cell.CellValue = new ss.CellValue(s);
                                                                        cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                                                        cell.StyleIndex = StyleTextNormalRight;
                                                                    }
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    ErrorLog theErrorLog = new ErrorLog(null,
                                                                        "ExportTemplate: Output \"Number\" Value to XLSX",
                                                                        ex.Message, ex.StackTrace, DateTime.Now,
                                                                        "App_Code: ExportUtil");
                                                                    SystemData.ErrorLog_Insert(theErrorLog);
                                                                }
                                                            }
                                                        }
                                                        else if (dataRow["ColumnType"].ToString() == "date")
                                                        {
                                                            if (row[column].ToString() != "")
                                                            {
                                                                try
                                                                {
                                                                    cell.CellValue = new ss.CellValue(
                                                                        (DateTime.Parse(row[column].ToString()))
                                                                        .ToOADate().ToString(CultureInfo
                                                                            .InvariantCulture));
                                                                    cell.DataType =
                                                                        new EnumValue<CellValues>(CellValues.Number);
                                                                    cell.StyleIndex = StyleShortDateNormal;
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    ErrorLog theErrorLog = new ErrorLog(null,
                                                                        "ExportTemplate: Output \"Date\" Value to XLSX",
                                                                        ex.Message, ex.StackTrace, DateTime.Now,
                                                                        "App_Code: ExportUtil");
                                                                    SystemData.ErrorLog_Insert(theErrorLog);
                                                                }
                                                            }
                                                        }
                                                        else if (dataRow["ColumnType"].ToString() == "datetime")
                                                        {
                                                            if (row[column].ToString() != "")
                                                            {
                                                                try
                                                                {
                                                                    cell.CellValue = new ss.CellValue(
                                                                        (DateTime.Parse(row[column].ToString()))
                                                                        .ToOADate().ToString(CultureInfo
                                                                            .InvariantCulture));
                                                                    cell.DataType =
                                                                        new EnumValue<CellValues>(CellValues.Number);
                                                                    if (dataRow["HideTimeSecond"] != DBNull.Value && (bool)dataRow["HideTimeSecond"])
                                                                        cell.StyleIndex = StyleShortDateTimeNoSecondsNormal;
                                                                    else
                                                                        cell.StyleIndex = StyleShortDateTimeNormal;
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    ErrorLog theErrorLog = new ErrorLog(null,
                                                                        "ExportTemplate: Output \"DateTime\" Value to XLSX",
                                                                        ex.Message, ex.StackTrace, DateTime.Now,
                                                                        "App_Code: ExportUtil");
                                                                    SystemData.ErrorLog_Insert(theErrorLog);
                                                                }
                                                            }
                                                        }
                                                        else if (dataRow["ColumnType"].ToString() == "time")
                                                        {
                                                            if (row[column].ToString() != "")
                                                            {
                                                                try
                                                                {
                                                                    cell.CellValue = new ss.CellValue(
                                                                        (DateTime.Parse(row[column].ToString()))
                                                                        .ToOADate().ToString(CultureInfo
                                                                            .InvariantCulture));
                                                                    cell.DataType =
                                                                        new EnumValue<CellValues>(CellValues.Number);
                                                                    if (dataRow["HideTimeSecond"] != DBNull.Value && (bool)dataRow["HideTimeSecond"])
                                                                        cell.StyleIndex = StyleShortTimeNoSecondsNormal;
                                                                    else
                                                                        cell.StyleIndex = StyleShortTimeNormal;
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    ErrorLog theErrorLog = new ErrorLog(null,
                                                                        "ExportTemplate: Output \"Time\" Value to XLSX",
                                                                        ex.Message, ex.StackTrace, DateTime.Now,
                                                                        "App_Code: ExportUtil");
                                                                    SystemData.ErrorLog_Insert(theErrorLog);
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cell.CellValue = new ss.CellValue(strF);
                                                            cell.DataType =
                                                                new EnumValue<CellValues>(CellValues.String);
                                                        }
                                                    }
                                                    else if (dataRow["ExportHeaderName"] + "-DDAATTEE" == column.ColumnName)
                                                    {
                                                        if (dataRow["ColumnType"].ToString() == "datetime")
                                                        {
                                                            cell.CellValue = new ss.CellValue((DateTime.Parse(row[column].ToString()))
                                                                .ToOADate().ToString(CultureInfo.InvariantCulture));
                                                            cell.DataType =
                                                                new EnumValue<CellValues>(CellValues.Number);
                                                            cell.StyleIndex = StyleShortDateNormal;
                                                        }
                                                    }
                                                    else if (dataRow["ExportHeaderName"] + "-TTIIMMEE" == column.ColumnName)
                                                    {
                                                        if (dataRow["ColumnType"].ToString() == "datetime")
                                                        {
                                                            cell.CellValue = new ss.CellValue((DateTime.Parse(row[column].ToString()))
                                                                .ToOADate().ToString(CultureInfo.InvariantCulture));
                                                            cell.DataType =
                                                                new EnumValue<CellValues>(CellValues.Number);
                                                            if (dataRow["HideTimeSecond"] != DBNull.Value && (bool)dataRow["HideTimeSecond"])
                                                                cell.StyleIndex = StyleShortTimeNoSecondsNormal;
                                                            else
                                                                cell.StyleIndex = StyleShortTimeNormal;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                cell.CellValue = new ss.CellValue(strF);
                                                cell.DataType = new EnumValue<CellValues>(CellValues.String);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog theErrorLog = new ErrorLog(null, "ExportTemplate: Output XLSX",
                                                ex.Message, ex.StackTrace, DateTime.Now, "App_Code: ExportUtil");
                                            SystemData.ErrorLog_Insert(theErrorLog);
                                        }

                                        break;
                                }

                                r.AppendChild(cell);
                            }
                            catch (Exception ex)
                            {
                                ErrorLog theErrorLog = new ErrorLog(null, "Export: Output XLSX", ex.Message,
                                    ex.StackTrace, DateTime.Now, "App_Code: ExportUtil");
                                SystemData.ErrorLog_Insert(theErrorLog);
                            }
                        }
                    }

                    wbsp.Stylesheet = CreateStyleSheet(numberingFormats, cellFormats);
                    //wbsp.Stylesheet.Save();

                    // save worksheet
                    wsp.Worksheet.Save();

                    // create the worksheet to workbook relation
                    spreadSheet.WorkbookPart.Workbook.AppendChild(new Sheets());
                    spreadSheet.WorkbookPart.Workbook.GetFirstChild<Sheets>().AppendChild(new Sheet()
                    {
                        Id = spreadSheet.WorkbookPart.GetIdOfPart(wsp),
                        SheetId = 1,
                        Name = String.IsNullOrEmpty(sheetName) ? "Sheet 1" : sheetName
                    });

                    spreadSheet.WorkbookPart.Workbook.Save();
                }

                Guid uniqueid = new Guid();
                String exportname = System.Web.HttpContext.Current.Session["FilesPhisicalPath"].ToString();
                exportname = exportname + "\\UserFiles\\AppFiles" + "\\" + "TESTER" + uniqueid.ToString();
                System.IO.FileStream fs = new FileStream(exportname, FileMode.Open, FileAccess.ReadWrite);
                memoryStream.CopyTo(fs);

                HttpResponse response = HttpContext.Current.Response;
                response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                response.AppendHeader("content-disposition", string.Format("attachment;filename={0}", fileName));
                memoryStream.Position = 0;
                memoryStream.CopyTo(response.OutputStream);

                /* == Red 09092019: Append cookie */
                HttpCookie cookie = new HttpCookie("ExportReport");
                cookie.Value = "Flag";
                cookie.Expires = DateTime.Now.AddDays(1);
                HttpContext.Current.Response.AppendCookie(cookie);
                /* == End Red == */

                response.Flush();
                response.End();
            }
        }


        private static NumberingFormats InitNumberingFormats(out UInt32Value lastFixedStyle)
        {
            lastFixedStyle = 165U;
            return new NumberingFormats(
                new NumberingFormat
                {
                    NumberFormatId = 165U,
                    FormatCode = "dd/MM/yyyy HH:mm:ss"
                });
        }

        private static CellFormats InitCellFormats(out UInt32Value lastFixedStyle)
        {
            lastFixedStyle = 9U;
            return new CellFormats(
                new CellFormat() // Normal: 0
                {
                    NumberFormatId = (UInt32Value) 0U,
                    FontId = (UInt32Value) 0U,
                    FillId = (UInt32Value) 0U,
                    BorderId = (UInt32Value) 0U,
                    FormatId = (UInt32Value) 0U
                },
                new CellFormat(new Alignment()
                        {Horizontal = HorizontalAlignmentValues.Center}) // Normal Bold Centered: 1
                    {
                        NumberFormatId = (UInt32Value) 0U,
                        FontId = (UInt32Value) 1U, // bold
                        FillId = (UInt32Value) 0U,
                        BorderId = (UInt32Value) 0U,
                        FormatId = (UInt32Value) 0U,
                        ApplyFont = true,
                        ApplyAlignment = true
                    },
                new CellFormat(new Alignment()
                        { Horizontal = HorizontalAlignmentValues.Right }) // Normal Right: 2
                    {
                        NumberFormatId = (UInt32Value)0U,
                        FontId = (UInt32Value)0U,
                        FillId = (UInt32Value)0U,
                        BorderId = (UInt32Value)0U,
                        FormatId = (UInt32Value)0U,
                        ApplyAlignment = true
                    },
                new CellFormat() // Short Date: 3
                {
                    NumberFormatId = (UInt32Value) 14U, // dd/mm/yyyy
                    FontId = (UInt32Value) 0U,
                    FillId = (UInt32Value) 0U,
                    BorderId = (UInt32Value) 0U,
                    FormatId = (UInt32Value) 0U,
                    ApplyNumberFormat = true
                },
                new CellFormat() // Short Time: 4
                {
                    NumberFormatId = (UInt32Value) 21U, // H:mm:ss
                    FontId = (UInt32Value) 0U,
                    FillId = (UInt32Value) 0U,
                    BorderId = (UInt32Value) 0U,
                    FormatId = (UInt32Value) 0U,
                    ApplyNumberFormat = true
                },
                new CellFormat() // Sort Date Time: 5
                {
                    NumberFormatId = (UInt32Value) 165U, // d/m/yy h:mm:ss
                    FontId = (UInt32Value) 0U,
                    FillId = (UInt32Value) 0U,
                    BorderId = (UInt32Value) 0U,
                    FormatId = (UInt32Value) 0U,
                    ApplyNumberFormat = true
                },
                new CellFormat() // Sort Date Time no seconds: 6
                {
                    NumberFormatId = (UInt32Value)22U, // d/m/yy h:mm
                    FontId = (UInt32Value)0U,
                    FillId = (UInt32Value)0U,
                    BorderId = (UInt32Value)0U,
                    FormatId = (UInt32Value)0U,
                    ApplyNumberFormat = true
                },
                new CellFormat() // Sort Time no seconds: 7
                {
                    NumberFormatId = (UInt32Value)20U, // h:mm
                    FontId = (UInt32Value)0U,
                    FillId = (UInt32Value)0U,
                    BorderId = (UInt32Value)0U,
                    FormatId = (UInt32Value)0U,
                    ApplyNumberFormat = true
                },
                new CellFormat() // Number 0: 8
                {
                    NumberFormatId = (UInt32Value) 3U, // #,##0
                    FontId = (UInt32Value) 0U,
                    FillId = (UInt32Value) 0U,
                    BorderId = (UInt32Value) 0U,
                    FormatId = (UInt32Value) 0U,
                    ApplyNumberFormat = true
                },
                new CellFormat() // Number 0.00: 9
                {
                    NumberFormatId = (UInt32Value) 4U, // #,##0.00
                    FontId = (UInt32Value) 0U,
                    FillId = (UInt32Value) 0U,
                    BorderId = (UInt32Value) 0U,
                    FormatId = (UInt32Value) 0U,
                    ApplyNumberFormat = true
                }
            );
        }

        private static Stylesheet CreateStyleSheet(NumberingFormats numberingFormats, CellFormats cellFormats)
        {
            Stylesheet workBookStylesheet = new Stylesheet();

            Fonts fonts = new Fonts(
                new Font() // all fields are optional
                {
                    FontSize = new ss.FontSize() { Val = 11D },
                    Color = new Color() { Theme = (UInt32Value)1U },
                    FontName = new FontName() { Val = "Calibri" },
                    FontFamilyNumbering = new FontFamilyNumbering() { Val = 2 },
                    FontCharSet = new FontCharSet() { Val = 1 }, // Default charset
                    FontScheme = new ss.FontScheme() { Val = FontSchemeValues.Minor }
                },
                new Font() // "Bold" is required, other are optional
                {
                    Bold = new Bold(),
                    FontSize = new ss.FontSize() { Val = 11D },
                    Color = new Color() { Theme = (UInt32Value)1U },
                    FontName = new FontName() { Val = "Calibri" },
                    FontFamilyNumbering = new FontFamilyNumbering() { Val = 2 },
                    FontCharSet = new FontCharSet() { Val = 1 }, // Default charset
                    FontScheme = new ss.FontScheme() { Val = FontSchemeValues.Minor }

                });

            Fills fills = new Fills(
                new Fill( // all fields are optional
                    new PatternFill() { PatternType = PatternValues.None })
            );

            Borders borders = new Borders(
                new Border( // all fields are optional
                    new LeftBorder(),
                    new RightBorder(),
                    new TopBorder(),
                    new BottomBorder(),
                    new DiagonalBorder())
            );

            CellStyleFormats cellStyleFormats = new CellStyleFormats(
                new CellFormat()
                {
                    NumberFormatId = (UInt32Value)0U,
                    FontId = (UInt32Value)0U,
                    FillId = (UInt32Value)0U,
                    BorderId = (UInt32Value)0U
                }
            );

            workBookStylesheet.Append(
                numberingFormats,
                fonts,
                fills,
                borders,
                cellStyleFormats,
                cellFormats);

            return workBookStylesheet;
        }

        private static double GetWidth(string font, int fontSize, string text)
        {
            System.Drawing.Font stringFont = new System.Drawing.Font(font, fontSize, System.Drawing.FontStyle.Bold);
            return GetWidth(stringFont, text);
        }

        private static double GetWidth(System.Drawing.Font stringFont, string text)
        {
            // This formula is based on this article plus a nudge ( + 0.2M )
            // http://msdn.microsoft.com/en-us/library/documentformat.openxml.spreadsheet.column.width.aspx
            // Truncate(((256 * Solve_For_This + Truncate(128 / 7)) / 256) * 7) = DeterminePixelsOfString

            System.Drawing.Size textSize = System.Windows.Forms.TextRenderer.MeasureText(text, stringFont);
            double width = (((textSize.Width / 7d) * 256) - 128d / 7) / 256;
            width = (double)decimal.Round((decimal)width + 1M, 2);

            return width;
        }
    }
}
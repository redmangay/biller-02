﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xero
{
    public class XeroInvoice
    {
        public int XeroInvoiceID { get; set; }
        public string RefID { get; set; }
        public string InvoiceNumber { get; set; }
        public int XeroContactID { get; set; }
        public string Type { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DueDate { get; set; }
        public string Status { get; set; }
        public string CurrencyCode { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TotalTax { get; set; }
        public decimal Total { get; set; }
        public decimal AmountDue { get; set; }
        public decimal AmountPaid { get; set; }
        public DateTime UpdatedDateUTC { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}
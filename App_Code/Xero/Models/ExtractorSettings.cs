﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xero
{
    public class ExtractorSettings
    {
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public string KeyFile { get; set; }
        public string KeyFilePassword { get; set; }
    }
}
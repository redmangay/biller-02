﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Xero.Api;
using Xero.Api.Core;
using Xero.Api.Core.Model;
using Xero.Api.Infrastructure.OAuth;
using Xero.Api.Serialization;

namespace Xero
{
    public class XeroExtractor
    {
        string connectionString = DBGurus.strGlobalConnectionString;
        ExtractorSettings settings = null;

        public string Execute(string args, out string output)
        {
            // https://developer.xero.com/myapps (Create app and get ConsumerKey, ConsumerSecret)
            // https://developer.xero.com/documentation/api-guides/create-publicprivate-key
            StringBuilder sb = new StringBuilder();
            output = "";
            try
            {
                settings = JsonConvert.DeserializeObject<ExtractorSettings>(args);
            }
            catch
            {
                                      
            }
            if(settings == null)
            {
                output = "Invalid settings for Xero Extractor";
            }
            else
            {
                XeroCoreApi client = new XeroCoreApi(
                    "https://api.xero.com/api.xro/2.0/",
                    new PrivateAuthenticator(HttpContext.Current.Server.MapPath(settings.KeyFile), settings.KeyFilePassword),
                    new Consumer(settings.ConsumerKey, settings.ConsumerSecret),
                    null,
                    new DefaultMapper(),
                    new DefaultMapper());

                try
                {
                    using (XeroInvoiceDb invoiceDb = new XeroInvoiceDb(this.connectionString))
                    {
                        DateTime lastUpdated = invoiceDb.GetLastUpdatedDate();
                        IEnumerable<Xero.Api.Core.Model.Invoice> invoices = client.Invoices.ModifiedSince(lastUpdated).Find();
                        foreach (var invoice in invoices)
                        {
                            invoiceDb.Save(invoice);
                        }
                        sb.Append("Invoices: " + invoices.Count().ToString());
                    }
                }
                catch (Exception ex)
                {
                    output = ex.Message;
                }
            }
            return sb.ToString();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;

namespace Xero
{
    public class XeroInvoiceDb : IDisposable
    {
        protected SqlConnection connection;

        public XeroInvoiceDb(string connectionString)
        {
            connection = new SqlConnection(connectionString);
        }

        public void Save(Xero.Api.Core.Model.Invoice invoice)
        {            
            connection.Execute("XeroInvoice_Save", new
            {
                RefID = invoice.Id,
                InvoiceNumber = invoice.Number,
                ContactRefID = invoice.Contact.Id,
                ContactName = invoice.Contact.Name,
                Type = invoice.Type,
                InvoiceDate = invoice.Date,
                DueDate = invoice.DueDate,
                Status = invoice.Status,
                CurrencyCode = invoice.CurrencyCode,
                SubTotal = invoice.SubTotal,
                TotalTax = invoice.TotalTax,
                Total = invoice.Total,
                AmountDue = invoice.AmountDue,
                AmountPaid = invoice.AmountPaid,
                UpdatedDateUTC = invoice.UpdatedDateUtc
            }, commandType: System.Data.CommandType.StoredProcedure);
        }

        public DateTime GetLastUpdatedDate()
        {
            return connection.ExecuteScalar<DateTime>("XeroInvoice_GetLastUpdatedDate", commandType: System.Data.CommandType.StoredProcedure);
        }

        public void Dispose()
        {
            if (connection != null)
            {
                connection.Dispose();
            }
        }
    }
}
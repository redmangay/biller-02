﻿using System;
using System.Collections.Generic;
using System.Text;

public class JSONObject : IJSONItem
{
    private string name;
    private List<IJSONItem> items = new List<IJSONItem>();

    public JSONObject()
    {
    }

    public JSONObject(string name)
    {
        this.name = name;
    }

    public JSONObject(IJSONItem item)
    {
        items.Add(item);
    }

    public JSONObject(string name, IJSONItem item)
    {
        this.name = name;
        items.Add(item);
    }

    public JSONObject(List<IJSONItem> items)
    {
        this.items = items;
    }

    public JSONObject(string name, List<IJSONItem> items)
    {
        this.name = name;
        this.items = items;
    }

    public JSONObject AddString(string name, object value)
    {
        items.Add(new JSONValue(name, value));
        return this;
    }

    public JSONObject AddBoolean(string name, object value, bool defaultValueIfNull = false)
    {
        if (value == null || value is DBNull)
        {
            items.Add(new JSONValue(name, defaultValueIfNull));
        }
        else
        {
            items.Add(new JSONValue(name, value));
        }

        return this;
    }

    public JSONObject AddDouble(string name, object value, int decimalPlaces, bool showThousands = true)
    {
        double dbl;

        if (value == null || value is DBNull)
        {
            items.Add(new JSONValue(name, ""));
        }
        else if (Double.TryParse(value.ToString(), out dbl))
        {
            if (showThousands)
            {
                items.Add(new JSONValue(name, dbl.ToString("N" + decimalPlaces)));
            }
            else
            {
                items.Add(new JSONValue(name, dbl.ToString("F" + decimalPlaces)));
            }
        }
        else
        {
            items.Add(new JSONValue(name, value));
        }

        return this;
    }

    public JSONObject AddInteger(string name, object value)
    {
        int val;

        if (value == null || value is DBNull)
        {
            items.Add(new JSONValue(name, ""));
        }
        else if (Int32.TryParse(value.ToString(), out val))
        {
            items.Add(new JSONValue(name, val.ToString()));
        }
        else
        {
            items.Add(new JSONValue(name, value));
        }

        return this;
    }

    public JSONObject AddDate(string name, object value, string format = "dd/MM/yyyy")
    {
        return AddDateTime(name, value, format);
    }

    public JSONObject AddTime(string name, object value, string format = "HH:mm")
    {
        return AddDateTime(name, value, format);
    }

    public JSONObject AddDateTime(string name, object value, string format = "dd/MM/yyyy HH:mm")
    {
        DateTime dateTime;

        if (value == null || value is DBNull)
        {
            items.Add(new JSONValue(name, ""));
        }
        else if (DateTime.TryParse(value.ToString(), out dateTime))
        {
            items.Add(new JSONValue(name, dateTime.ToString(format)));
        }
        else
        {
            items.Add(new JSONValue(name, value));
        }

        return this;
    }

    public JSONObject AddList(JSONList list)
    {
        items.Add(list);
        return this;
    }

    public JSONObject AddObject(JSONObject obj)
    {
        items.Add(obj);
        return this;
    }

    public override string ToString()
    {
        StringBuilder output = new StringBuilder();

        if (items.Count == 0)
        {
            // Assume that it is just a string being added to the list, not an object.
            return "\"" + name + "\"";
        }

        if (!String.IsNullOrEmpty(name))
        {
            output.Append("\"" + name + "\": ");
        }

        output.Append("{");
        bool firstObj = true;

        foreach (IJSONItem item in items)
        {
            if (!firstObj)
            {
                output.Append(",");
            }

            output.Append(item.ToString());
            firstObj = false;
        }

        output.Append("}");
        return output.ToString();
    }
}
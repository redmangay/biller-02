﻿using DocGen.DAL;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
//using System.Linq;
//using ChartDirector;
//using DocGen.DAL;
//using System.Web.UI.WebControls;
/// <summary>
/// Summary description for CustomMethod_boyne
/// </summary>
public class CustomMethod_boyne
{
    public CustomMethod_boyne()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public static List<object> DotNetMethod(string sMethodName, List<object> objList)
    {

        List<object> roList = new List<object>();
        try
        {
            switch (sMethodName.ToLower())
            {

                case "boyne_save_idea_to_project":
                    roList = boyne_save_idea_to_project(objList);
                    return roList;
                case "boyne_filter_forecast_actual_cost_grid":
                    roList = boyne_filter_forecast_actual_cost_grid(objList);
                    return roList;

            }

            return roList;
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "DotNetMethod" + sMethodName, ex.Message, ex.StackTrace, DateTime.Now, "App_Code");
            SystemData.ErrorLog_Insert(theErrorLog);
            if (roList != null)
                roList.Add(ex);
        }
        return null;
    }

    public static List<object> boyne_save_idea_to_project(List<object> objList)
    {
        Service theService = null;
        Panel pnlFullDetailPage = null;
        HiddenField hfRecordDetailSCid = null;

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
            }
            else if (obj.GetType().Name == "Panel")
            {
                pnlFullDetailPage = (Panel)obj;
                hfRecordDetailSCid = (HiddenField)pnlFullDetailPage.FindControl("hfRecordDetailSCid");
            }
        }

        string projectRecordID = Common.GetValueFromSQL("SELECT Max(RecordID) FROM Record WHERE TableID = 6843");

        string strEditVisitURL = "Pages/Record/RecordDetail.aspx?mode=" + Cryptography.Encrypt("edit") + "&SearchCriteriaID=" + Cryptography.Encrypt(hfRecordDetailSCid.Value) + "&TableID=" + Cryptography.Encrypt("6843") + "&stackhault=yes";

        System.Web.HttpContext.Current.Response.Redirect(System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath + strEditVisitURL + "&RecordID=" + Cryptography.Encrypt(projectRecordID), true);

        return null;
    }

    public static List<object> boyne_filter_forecast_actual_cost_grid(List<object> objList)
    {
        List<object> roList = new List<object>();
        Service theService = null;
        Panel pnlFullDetailPage = null;
        HiddenField hfRecordDetailSCid = null;
        System.Web.UI.Page thePage = null;


        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
            }
            else if (obj.GetType().Name == "Panel")
            {
                pnlFullDetailPage = (Panel)obj;
                hfRecordDetailSCid = (HiddenField)pnlFullDetailPage.FindControl("hfRecordDetailSCid");
            }
            else if (obj.GetType().Name == "pages_record_recorddetail_aspx")
            {
                thePage = (System.Web.UI.Page)obj;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
        if (hfRecordID.Value == "-1")
        {
            return null;//this is not applicable at the time of "ADD Record"
        }
        string sRecordID = hfRecordID.Value;
        if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Init")
        {
            TextBox txtStartYear = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV036");
            TextBox txtEndYear = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV037");

            txtStartYear.AutoPostBack = true;
            txtEndYear.AutoPostBack = true;

            txtStartYear.Attributes["onchange"] = "confirmBeforeLeaving = false;";
            txtEndYear.Attributes["onchange"] = "confirmBeforeLeaving = false;";

            txtStartYear.Attributes.Add("serviceid", theService.ServiceID.ToString());
            ControlEvent sControlEvent = new ControlEvent(txtStartYear, txtStartYear.ID, "Service_Control_TextChanged");

            txtEndYear.Attributes.Add("serviceid", theService.ServiceID.ToString());
            ControlEvent eControlEvent = new ControlEvent(txtEndYear, txtEndYear.ID, "Service_Control_TextChanged");


            List<ControlEvent> lstControlEvent = new List<ControlEvent>();
            lstControlEvent.Add(sControlEvent);
            lstControlEvent.Add(eControlEvent);

            theService.Temp_ControlEventList = lstControlEvent;
            roList.Add(theService);



            return roList;

        }
        else if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_PreRender")
        {
            if (!thePage.IsPostBack)
            {
                TextBox txtStartYear = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV036");
                TextBox txtEndYear = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV037");
                System.Web.UI.UserControl theForecastGrid = (System.Web.UI.UserControl)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "columnCTListV035");
                System.Web.UI.UserControl theActualGrid = (System.Web.UI.UserControl)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "columnCTListV038");
                string sCustomFilter = "";
                if (txtStartYear.Text != "" && txtEndYear.Text != "")
                {
                    sCustomFilter = " AND ( dbo.RemoveNonNumericChar(Record.V002)<>'' AND  dbo.RemoveNonNumericChar(Record.V002) >= CONVERT(decimal(38,2)," + txtStartYear.Text + ") AND dbo.RemoveNonNumericChar(Record.V002) <= CONVERT(decimal(38,2)," + txtEndYear.Text + ")) ";
                }
                else if (txtStartYear.Text != "" && txtEndYear.Text == "")
                {
                    sCustomFilter = " AND ( dbo.RemoveNonNumericChar(Record.V002)<>'' AND  dbo.RemoveNonNumericChar(Record.V002) >= CONVERT(decimal(38,2)," + txtStartYear.Text + ")) ";
                }
                else if (txtStartYear.Text == "" && txtEndYear.Text != "")
                {
                    sCustomFilter = " AND ( dbo.RemoveNonNumericChar(Record.V002)<>''  AND dbo.RemoveNonNumericChar(Record.V002) <= CONVERT(decimal(38,2)," + txtEndYear.Text + ")) ";
                }
                string origForeCastTextSearchParent = (string)theForecastGrid.GetType().GetProperty("TextSearchParent").GetValue(theForecastGrid);
                string origActualTextSearchParent = (string)theActualGrid.GetType().GetProperty("TextSearchParent").GetValue(theActualGrid);
                theForecastGrid.GetType().GetProperty("TextSearchParent").SetValue(theForecastGrid, origForeCastTextSearchParent + sCustomFilter);
                theActualGrid.GetType().GetProperty("TextSearchParent").SetValue(theActualGrid, origActualTextSearchParent + sCustomFilter);
                theForecastGrid.GetType().GetProperty("RefreshMe").SetValue(theForecastGrid, "b");
                theActualGrid.GetType().GetProperty("RefreshMe").SetValue(theActualGrid, "b");
            }
        }
        else if (thePageLifeCycleService.ServerControlEvent == "Service_Control_TextChanged")
        {
            //Pages_UserControl_RecordList theForecastGrid = (Pages_UserControl_RecordList)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "columnCTListV035");
            // System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;
            System.Web.UI.UserControl theForecastGrid = (System.Web.UI.UserControl)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "columnCTListV035");
            System.Web.UI.UserControl theActualGrid = (System.Web.UI.UserControl)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "columnCTListV038");

            TextBox txtStartYear = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV036");
            TextBox txtEndYear = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV037");

            string sCustomFilter = "";
            Record theReocrd = RecordManager.ets_Record_Detail_Full(int.Parse(sRecordID), null, true);
            if (txtStartYear.Text != "" && txtEndYear.Text != "")
            {
                //create child record if needed
                try
                {
                    int? iEnterdByUserID = null;
                    if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null)
                    {
                        if (System.Web.HttpContext.Current.Session["User"] != null)
                        {
                            User theUser = (User)System.Web.HttpContext.Current.Session["User"];
                            if (theUser != null)
                                iEnterdByUserID = theUser.UserID;
                        }
                    }

                    if (iEnterdByUserID == null)
                    {

                        if (theReocrd != null)
                        {
                            iEnterdByUserID = theReocrd.EnteredBy;
                        }
                    }

                    for (int i = int.Parse(txtStartYear.Text); i <= int.Parse(txtEndYear.Text); i++)
                    {
                        string sHasYear = Common.GetValueFromSQL("SELECT RecordID FROM [Record] WHERE IsActive=1 AND TableID=6852 AND V001='" + sRecordID + "' AND V002='" + i.ToString() + "'");

                        if (string.IsNullOrEmpty(sHasYear))
                        {
                            Record newRecord = new Record();
                            newRecord.TableID = 6852;
                            newRecord.EnteredBy = iEnterdByUserID;
                            newRecord.DateTimeRecorded = DateTime.Now;
                            newRecord.V001 = sRecordID;//link with the Parent
                            newRecord.V002 = i.ToString();//Year value
                            RecordManager.ets_Record_Insert(newRecord);
                        }

                        sHasYear = Common.GetValueFromSQL("SELECT RecordID FROM [Record] WHERE IsActive=1 AND TableID=6862 AND V001='" + sRecordID + "' AND V002='" + i.ToString() + "'");

                        if (string.IsNullOrEmpty(sHasYear))
                        {
                            Record newRecord = new Record();
                            newRecord.TableID = 6862;
                            newRecord.EnteredBy = iEnterdByUserID;
                            newRecord.DateTimeRecorded = DateTime.Now;
                            newRecord.V001 = sRecordID;//link with the Parent
                            newRecord.V002 = i.ToString();//Year value
                            RecordManager.ets_Record_Insert(newRecord);
                        }
                    }


                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "CustomMethod_dev/Boyne Service", ex.Message, ex.StackTrace, DateTime.Now, "CustomMethod_dev/boyne_filter_forecast_actual_cost_grid");
                    SystemData.ErrorLog_Insert(theErrorLog);
                }
            }


            if (txtStartYear.Text != "" && txtEndYear.Text != "")
            {
                sCustomFilter = " AND ( dbo.RemoveNonNumericChar(Record.V002)<>'' AND  dbo.RemoveNonNumericChar(Record.V002) >= CONVERT(decimal(38,2)," + txtStartYear.Text + ") AND dbo.RemoveNonNumericChar(Record.V002) <= CONVERT(decimal(38,2)," + txtEndYear.Text + ")) ";
            }
            else if (txtStartYear.Text != "" && txtEndYear.Text == "")
            {
                sCustomFilter = " AND ( dbo.RemoveNonNumericChar(Record.V002)<>'' AND  dbo.RemoveNonNumericChar(Record.V002) >= CONVERT(decimal(38,2)," + txtStartYear.Text + ")) ";
            }
            else if (txtStartYear.Text == "" && txtEndYear.Text != "")
            {
                sCustomFilter = " AND ( dbo.RemoveNonNumericChar(Record.V002)<>''  AND dbo.RemoveNonNumericChar(Record.V002) <= CONVERT(decimal(38,2)," + txtEndYear.Text + ")) ";
            }
            string origForeCastTextSearchParent = (string)theForecastGrid.GetType().GetProperty("TextSearchParent").GetValue(theForecastGrid);
            string origActualTextSearchParent = (string)theActualGrid.GetType().GetProperty("TextSearchParent").GetValue(theActualGrid);


            theForecastGrid.GetType().GetProperty("TextSearchParent").SetValue(theForecastGrid, origForeCastTextSearchParent + sCustomFilter);
            theActualGrid.GetType().GetProperty("TextSearchParent").SetValue(theActualGrid, origActualTextSearchParent + sCustomFilter);



            theForecastGrid.GetType().GetProperty("RefreshMe").SetValue(theForecastGrid, "b");
            theActualGrid.GetType().GetProperty("RefreshMe").SetValue(theActualGrid, "b");

            if (theReocrd != null)
            {
                theReocrd.V036 = txtStartYear.Text;
                theReocrd.V037 = txtEndYear.Text;
                RecordManager.ets_Record_Update(theReocrd, true);
            }

            string sTest = theForecastGrid.ClientID;
        }

        return null;
    }



}
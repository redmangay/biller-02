﻿using DocGen.DAL;
using System;
using System.Collections.Generic;
using System.Data;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
//using ChartDirector;
//using DocGen.DAL;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for CustomMethod_dev 
/// </summary>
public class CustomMethod_dev
{
	public CustomMethod_dev()
	{
		// 
		// TODO: Add constructor logic here
		//
	}
    public static List<object> test_custom_usercontrol_custom(List<object> objList)
    {
        if (objList != null && objList.Count > 0)
        {
            UserControl aUserControl = (UserControl)objList[0];
            if (aUserControl != null)
            {
                aUserControl.GetType().GetProperty("TextProperty").SetValue(aUserControl, " custom Methods");
            }
        }
        return null;
    }
    public static List<object> DotNetMethod(string sMethodName, List<object> objList)
    {
        List<object> roList = new List<object>();
        try
        {

            switch (sMethodName.ToLower())
            {
                //case "econtractor_mapsection_service":
                //    roList = econtractor_mapsection_service(objList);
                //    return roList;

                case "cs_test_method_join":
                    roList = cs_test_method_join(objList);
                    return roList;
                case "cs_test_method_EMPTY":
                    roList = cs_test_method_EMPTY(objList);
                    return roList;              
                case "testing_new_site":
                    roList = testing_new_site(objList);
                    return roList;
                case "windtech_searchbox_disbursement_date":
                    roList = windtech_searchbox_disbursement_date(objList);
                    return roList;
                case "windtech_searchbox_timesheet_date":
                    roList = windtech_searchbox_timesheet_date(objList);
                    return roList;
                case "windtech_billing_top":
                    roList = windtech_billing_top(objList);
                    return roList;

                case "windtech_billingaddress_hide_things":
                    roList = windtech_billingaddress_hide_things(objList);
                    return roList;
                case "windtech_billpayment_baddebt_entry":
                    roList = windtech_billpayment_baddebt_entry(objList);
                    return roList;
                case "windtech_billpayment_baddebt_entry_v2":
                    roList = windtech_billpayment_baddebt_entry_v2(objList);
                    return roList;
                case "tdb_columntype_number_id_62667":
                    roList = tdb_columntype_number_id_62667(objList);
                    return roList;
                case "tdb_columntype_number_id_71666":
                    roList = tdb_columntype_number_id_71666(objList);
                    return roList;
                case "mr_common_service_test":
                    roList = MR_Common_Service_Test(objList);
                    return roList;
                case "test_custom_usercontrol_custom":
                    roList = test_custom_usercontrol_custom(objList);
                    return roList;
                case "uhaqm_network_data_read_datetime":
                    roList = uhaqm_network_data_read_datetime(objList);
                    return roList;
                case "boyne_save_idea_to_project":
                    roList = boyne_save_idea_to_project(objList);
                    return roList;
                case "boyne_filter_forecast_actual_cost_grid":
                    roList = boyne_filter_forecast_actual_cost_grid(objList);
                    return roList;
                case "local_test_button_service":
                    roList = local_test_button_service(objList);
                    return roList;
            }

            return roList;
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "DotNetMethod/SP" + sMethodName, ex.Message, ex.StackTrace, DateTime.Now, "App_Code");
            SystemData.ErrorLog_Insert(theErrorLog);
            if (roList != null)
                roList.Add(ex);
        }
        return null;
    }
    public static List<object> uhaqm_network_data_read_datetime(List<object> objList)
    {
        List<object> roList = new List<object>();

        DataTable dtImportFileTable = null;

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "DataTable")
            {
                dtImportFileTable = (DataTable)obj;
            }
        }


        if (dtImportFileTable != null && dtImportFileTable.Rows[0][0] != DBNull.Value
            && dtImportFileTable.Rows[0][0].ToString() != "")
        {
            string strThisFileDateTime = dtImportFileTable.Rows[0][0].ToString();
            if (strThisFileDateTime != "")
            {
                strThisFileDateTime = strThisFileDateTime.Substring(strThisFileDateTime.IndexOf(":") + 1).Trim();

                dtImportFileTable.Columns.Add("Date Time Sampled");
                foreach (DataRow dr in dtImportFileTable.Rows)
                {
                    dr["Date Time Sampled"] = strThisFileDateTime;//putting datatime value in all rows for this column
                }
                if (dtImportFileTable.Rows.Count > 3)
                {
                    dtImportFileTable.Rows[2]["Date Time Sampled"] = "Date Time Sampled";//puttng "Date Time Sampled" in line 3 as this line is the column header.
                }
                dtImportFileTable.AcceptChanges();
            }


            roList.Add(dtImportFileTable);
            return roList;

        }

        return null;
    }
    public static List<object> MR_Common_Service_Test(List<object> objList)
    {
        UpdatePanel upDetailDynamic=null;
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;
                upDetailDynamic = (UpdatePanel)pnlFullDetailPage.FindControl("upDetailDynamic");               
                break;
            }
        }
    
        DataTable dtColumnsDetail = RecordManager.ets_Table_Columns_Detail((int)theService.TableID);      
        for (int i = 0; i < dtColumnsDetail.Rows.Count; i++)
        {
            if(dtColumnsDetail.Rows[i]["SystemName"].ToString().ToLower()=="v010")
            {
                ScriptManager.RegisterStartupScript(upDetailDynamic, upDetailDynamic.GetType(), 
                    "AutoCompleteJS" + theService.Temp_DynamicPartName + i.ToString(), "", true);               
                break;
            }
        }                

        return null;
    }


    public static List<object> boyne_save_idea_to_project(List<object> objList)
    {
        Service theService = null;
        Panel pnlFullDetailPage = null;
        HiddenField hfRecordDetailSCid = null;

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
            }
            else if (obj.GetType().Name == "Panel")
            {
                pnlFullDetailPage = (Panel)obj;
                hfRecordDetailSCid = (HiddenField)pnlFullDetailPage.FindControl("hfRecordDetailSCid");
            }
        }

        string projectRecordID = Common.GetValueFromSQL("SELECT Max(RecordID) FROM Record WHERE TableID = 6843");

        string strEditVisitURL = "Pages/Record/RecordDetail.aspx?mode=" + Cryptography.Encrypt("edit") + "&SearchCriteriaID=" + Cryptography.Encrypt(hfRecordDetailSCid.Value) + "&TableID=" + Cryptography.Encrypt("6843") + "&stackhault=yes";

        System.Web.HttpContext.Current.Response.Redirect(System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath + strEditVisitURL + "&RecordID=" + Cryptography.Encrypt(projectRecordID), true);

        return null;
    }

    public static List<object> boyne_filter_forecast_actual_cost_grid(List<object> objList)
    {
        List<object> roList = new List<object>();
        Service theService = null;
        Panel pnlFullDetailPage = null;
        HiddenField hfRecordDetailSCid = null;
        System.Web.UI.Page thePage = null;


        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
            }
            else if (obj.GetType().Name == "Panel")
            {
                pnlFullDetailPage = (Panel)obj;
                hfRecordDetailSCid = (HiddenField)pnlFullDetailPage.FindControl("hfRecordDetailSCid");
            }
            else if (obj.GetType().Name == "pages_record_recorddetail_aspx")
            {
               thePage = (System.Web.UI.Page)obj;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
        if (hfRecordID.Value == "-1")
        {
            return null;//this is not applicable at the time of "ADD Record"
        }
        string sRecordID = hfRecordID.Value;
        if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Init")
        {
            TextBox txtStartYear = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV036");
            TextBox txtEndYear = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV037");

            txtStartYear.AutoPostBack = true;
            txtEndYear.AutoPostBack = true;

            txtStartYear.Attributes["onchange"] = "confirmBeforeLeaving = false;";
            txtEndYear.Attributes["onchange"] = "confirmBeforeLeaving = false;";

            txtStartYear.Attributes.Add("serviceid", theService.ServiceID.ToString());
            ControlEvent sControlEvent = new ControlEvent(txtStartYear, txtStartYear.ID, "Service_Control_TextChanged");

            txtEndYear.Attributes.Add("serviceid", theService.ServiceID.ToString());
            ControlEvent eControlEvent = new ControlEvent(txtEndYear, txtEndYear.ID, "Service_Control_TextChanged");


            List<ControlEvent> lstControlEvent = new List<ControlEvent>();
            lstControlEvent.Add(sControlEvent);
            lstControlEvent.Add(eControlEvent);

            theService.Temp_ControlEventList = lstControlEvent;
            roList.Add(theService);
                                  


            return roList;

        }
        else if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_PreRender")
        {
            if (!thePage.IsPostBack)
            {
                TextBox txtStartYear = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV036");
                TextBox txtEndYear = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV037");
                System.Web.UI.UserControl theForecastGrid = (System.Web.UI.UserControl)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "columnCTListV035");
                System.Web.UI.UserControl theActualGrid = (System.Web.UI.UserControl)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "columnCTListV038");
                string sCustomFilter = "";
                if (txtStartYear.Text != "" && txtEndYear.Text != "")
                {
                    sCustomFilter = " AND ( dbo.RemoveNonNumericChar(Record.V002)<>'' AND  dbo.RemoveNonNumericChar(Record.V002) >= CONVERT(decimal(38,2)," + txtStartYear.Text + ") AND dbo.RemoveNonNumericChar(Record.V002) <= CONVERT(decimal(38,2)," + txtEndYear.Text + ")) ";
                }
                else if (txtStartYear.Text != "" && txtEndYear.Text == "")
                {
                    sCustomFilter = " AND ( dbo.RemoveNonNumericChar(Record.V002)<>'' AND  dbo.RemoveNonNumericChar(Record.V002) >= CONVERT(decimal(38,2)," + txtStartYear.Text + ")) ";
                }
                else if (txtStartYear.Text == "" && txtEndYear.Text != "")
                {
                    sCustomFilter = " AND ( dbo.RemoveNonNumericChar(Record.V002)<>''  AND dbo.RemoveNonNumericChar(Record.V002) <= CONVERT(decimal(38,2)," + txtEndYear.Text + ")) ";
                }
                string origForeCastTextSearchParent = (string)theForecastGrid.GetType().GetProperty("TextSearchParent").GetValue(theForecastGrid);
                string origActualTextSearchParent = (string)theActualGrid.GetType().GetProperty("TextSearchParent").GetValue(theActualGrid);
                theForecastGrid.GetType().GetProperty("TextSearchParent").SetValue(theForecastGrid, origForeCastTextSearchParent + sCustomFilter);
                theActualGrid.GetType().GetProperty("TextSearchParent").SetValue(theActualGrid, origActualTextSearchParent + sCustomFilter);
                theForecastGrid.GetType().GetProperty("RefreshMe").SetValue(theForecastGrid, "b");
                theActualGrid.GetType().GetProperty("RefreshMe").SetValue(theActualGrid, "b");
            }
        }
        else if (thePageLifeCycleService.ServerControlEvent == "Service_Control_TextChanged")
        {
            //Pages_UserControl_RecordList theForecastGrid = (Pages_UserControl_RecordList)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "columnCTListV035");
            // System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;
            System.Web.UI.UserControl theForecastGrid = (System.Web.UI.UserControl)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "columnCTListV035");
            System.Web.UI.UserControl theActualGrid = (System.Web.UI.UserControl)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "columnCTListV038");

            TextBox txtStartYear = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV036");
            TextBox txtEndYear = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV037");

            string sCustomFilter = "";
            Record theReocrd = RecordManager.ets_Record_Detail_Full(int.Parse(sRecordID), null, true);
            if (txtStartYear.Text != "" && txtEndYear.Text != "")
            {
                //create child record if needed
                try
                {
                    int? iEnterdByUserID = null;
                    if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null)
                    {
                        if(System.Web.HttpContext.Current.Session["User"]!=null)
                        {
                            User theUser = (User)System.Web.HttpContext.Current.Session["User"];
                            if (theUser != null)
                                iEnterdByUserID = theUser.UserID;
                        }
                    }

                    if(iEnterdByUserID==null)
                    {
                       
                        if(theReocrd!=null)
                        {
                            iEnterdByUserID = theReocrd.EnteredBy;
                        }
                    }

                    for (int i = int.Parse(txtStartYear.Text); i <= int.Parse(txtEndYear.Text); i++)
                    {
                        string sHasYear = Common.GetValueFromSQL("SELECT RecordID FROM [Record] WHERE IsActive=1 AND TableID=6852 AND V001='" + sRecordID + "' AND V002='" + i.ToString() + "'");

                        if(string.IsNullOrEmpty(sHasYear))
                        {
                            Record newRecord = new Record();
                            newRecord.TableID = 6852;
                            newRecord.EnteredBy = iEnterdByUserID;
                            newRecord.DateTimeRecorded = DateTime.Now;
                            newRecord.V001 = sRecordID;//link with the Parent
                            newRecord.V002 = i.ToString();//Year value
                            RecordManager.ets_Record_Insert(newRecord);
                        }

                        sHasYear = Common.GetValueFromSQL("SELECT RecordID FROM [Record] WHERE IsActive=1 AND TableID=6862 AND V001='" + sRecordID + "' AND V002='" + i.ToString() + "'");

                        if (string.IsNullOrEmpty(sHasYear))
                        {
                            Record newRecord = new Record();
                            newRecord.TableID = 6862;
                            newRecord.EnteredBy = iEnterdByUserID;
                            newRecord.DateTimeRecorded = DateTime.Now;
                            newRecord.V001 = sRecordID;//link with the Parent
                            newRecord.V002 = i.ToString();//Year value
                            RecordManager.ets_Record_Insert(newRecord);
                        }
                    }


                }
                catch(Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "CustomMethod_dev/Boyne Service", ex.Message, ex.StackTrace, DateTime.Now, "CustomMethod_dev/boyne_filter_forecast_actual_cost_grid");
                    SystemData.ErrorLog_Insert(theErrorLog);
                }
            }


            if (txtStartYear.Text!="" && txtEndYear.Text!="")
            {
                sCustomFilter = " AND ( dbo.RemoveNonNumericChar(Record.V002)<>'' AND  dbo.RemoveNonNumericChar(Record.V002) >= CONVERT(decimal(38,2),"+ txtStartYear.Text + ") AND dbo.RemoveNonNumericChar(Record.V002) <= CONVERT(decimal(38,2),"+ txtEndYear.Text + ")) ";
            }
            else if (txtStartYear.Text != "" && txtEndYear.Text == "")
            {
                sCustomFilter = " AND ( dbo.RemoveNonNumericChar(Record.V002)<>'' AND  dbo.RemoveNonNumericChar(Record.V002) >= CONVERT(decimal(38,2)," + txtStartYear.Text + ")) ";
            }
            else if (txtStartYear.Text == "" && txtEndYear.Text != "")
            {
                sCustomFilter = " AND ( dbo.RemoveNonNumericChar(Record.V002)<>''  AND dbo.RemoveNonNumericChar(Record.V002) <= CONVERT(decimal(38,2)," + txtEndYear.Text + ")) ";
            }
            string origForeCastTextSearchParent =(string) theForecastGrid.GetType().GetProperty("TextSearchParent").GetValue(theForecastGrid);
            string origActualTextSearchParent = (string)theActualGrid.GetType().GetProperty("TextSearchParent").GetValue(theActualGrid);


            theForecastGrid.GetType().GetProperty("TextSearchParent").SetValue(theForecastGrid, origForeCastTextSearchParent + sCustomFilter);
            theActualGrid.GetType().GetProperty("TextSearchParent").SetValue(theActualGrid, origActualTextSearchParent + sCustomFilter);

           

            theForecastGrid.GetType().GetProperty("RefreshMe").SetValue(theForecastGrid, "b");
            theActualGrid.GetType().GetProperty("RefreshMe").SetValue(theActualGrid, "b");

            if(theReocrd!=null)
            {
                theReocrd.V036 = txtStartYear.Text;
                theReocrd.V037 = txtEndYear.Text;
                RecordManager.ets_Record_Update(theReocrd,true);
            }

            string sTest = theForecastGrid.ClientID;
        }

        return null;
    }



    public static List<object> local_test_button_service(List<object> objList)
    {
        List<object> roList = new List<object>();
        Service theService = null;
        Panel pnlFullDetailPage = null;
        HiddenField hfRecordDetailSCid = null;
        System.Web.UI.Page thePage = null;


        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
            }
            else if (obj.GetType().Name == "Panel")
            {
                pnlFullDetailPage = (Panel)obj;
                hfRecordDetailSCid = (HiddenField)pnlFullDetailPage.FindControl("hfRecordDetailSCid");
            }
            else if (obj.GetType().Name == "pages_record_recorddetail_aspx")
            {
                thePage = (System.Web.UI.Page)obj;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
        if (hfRecordID.Value == "-1")
        {
            return null;//this is not applicable at the time of "ADD Record"
        }
        string sRecordID = hfRecordID.Value;
        Record theRecord = RecordManager.ets_Record_Detail_Full(int.Parse(sRecordID), null, true);
        if(theRecord!=null)
        {
            theRecord.V002 = DateTime.Now.ToLongTimeString();
            RecordManager.ets_Record_Update(theRecord, null);
        }

        return null;
    }
    //public static List<object> econtractor_mapsection_service(List<object> objList)
    //{
    //    Panel pnlFullMap = null;
    //    Panel pnlCustomSearch = null;
    //    Service theService = null;
    //    Page thePage = null;
    //    //MapSectionDetail mapDetail = null;
    //    string sAccountID = System.Web.HttpContext.Current.Session["AccountID"].ToString();

    //    foreach (object obj in objList)
    //    {
    //        if (obj.GetType().Name == "pages_docgen_eachmap_aspx")
    //        {
    //            thePage = (Page)obj;
    //            break;
    //        }
    //    }

    //    foreach (object obj in objList)
    //    {
    //        if (obj.GetType().Name == "Service")
    //        {
    //            theService = (Service)obj;
    //            break;
    //        }
    //    }

    //    //foreach (object obj in objList)
    //    //{
    //    //    if (obj.GetType().Name == "MapSectionDetail")
    //    //    {
    //    //        mapDetail = (MapSectionDetail)obj;
    //    //        mapDetail.SearchColumnID
    //    //        break;
    //    //    }
    //    //}

    //    foreach (object obj in objList)
    //    {
    //        if (obj.GetType().Name == "Panel")
    //        {
    //            pnlFullMap = (Panel)obj;
    //            pnlCustomSearch = (Panel)pnlFullMap.FindControl("pnlCustomSearch");

    //            LinkButton lnkMapSearch = (LinkButton)pnlFullMap.FindControl("lnkMapSearch");
    //            if (lnkMapSearch != null)
    //                lnkMapSearch.OnClientClick = "ShowEContratorLocations();return false;";
    //            CheckBox chkShowDisRings = (CheckBox)pnlFullMap.FindControl("chkShowDisRings");
    //            if (chkShowDisRings != null)
    //                chkShowDisRings.Visible = false;
    //            HtmlTable trMapSearch = (HtmlTable)pnlFullMap.FindControl("trMapSearch");
    //            if (trMapSearch != null)
    //                trMapSearch.Visible = true;

    //            break;
    //        }
    //    }
    //    if (pnlCustomSearch!=null)
    //    {
    //        HtmlTable subTable = new HtmlTable();
    //        HtmlTableRow subRow = new HtmlTableRow();

    //        HtmlTableCell celld = new HtmlTableCell();
    //        HtmlTableCell celldc = new HtmlTableCell();

    //        HtmlTableCell cell1 = new HtmlTableCell();
    //        HtmlTableCell cell2 = new HtmlTableCell();
    //        HtmlTableCell cell3 = new HtmlTableCell();
    //        subTable.Rows.Add(subRow);

    //        subRow.Cells.Add(celld);
    //        subRow.Cells.Add(celldc);

    //        subRow.Cells.Add(cell1);
    //        subRow.Cells.Add(cell2);
    //        subRow.Cells.Add(cell3);
    //        TextBox txtClient = new TextBox();
    //        TextBox txtJob = new TextBox();
    //        TextBox txtContrator = new TextBox();

    //        TextBox txtDate = new TextBox();

    //        txtClient.ID = "txtClient";
    //        txtJob.ID = "txtJob";
    //        txtContrator.ID = "txtContrator";
    //        txtDate.ID = "txtDate";

    //        if(thePage.IsPostBack==false)
    //        {
    //            txtDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
    //        }

    //        txtClient.ClientIDMode = ClientIDMode.Static;
    //        txtJob.ClientIDMode = ClientIDMode.Static;
    //        txtContrator.ClientIDMode = ClientIDMode.Static;
    //        txtDate.ClientIDMode = ClientIDMode.Static;


    //        txtClient.Attributes.Add("placeholder", "Client");
    //        txtJob.Attributes.Add("placeholder", "Job");
    //        txtContrator.Attributes.Add("placeholder", "Contrator");
    //        txtDate.Attributes.Add("placeholder", "dd/mm/yyyy");

    //        txtClient.CssClass = "NormalTextBox";
    //        txtJob.CssClass = "NormalTextBox";
    //        txtContrator.CssClass = "NormalTextBox";
    //        txtDate.CssClass = "NormalTextBox";

    //        txtClient.Width = 100;
    //        txtJob.Width = 100;
    //        txtContrator.Width = 100;
    //        txtDate.Width = 100;

    //        txtClient.Attributes.Add("onblur", "ShowEContratorLocations()");
    //        txtJob.Attributes.Add("onblur", "ShowEContratorLocations()");
    //        txtContrator.Attributes.Add("onblur", "ShowEContratorLocations()");

    //        txtDate.Attributes.Add("onblur", "ShowEContratorLocations()");
    //        txtDate.Attributes.Add("onchange", "ShowEContratorLocations()");
    //        txtDate.ReadOnly = true;

    //        ImageButton ibCal = new ImageButton();
    //        ibCal.ID = "ibCal";
    //        ibCal.ClientIDMode = ClientIDMode.Static;
    //        ibCal.ImageUrl = "~/Images/Calendar.png";
    //        ibCal.AlternateText = "Click to show calendar";
    //        ibCal.Style.Add("padding-left", "3px");
    //        ibCal.CausesValidation = false;


    //       AjaxControlToolkit.CalendarExtender  ibCalEx = new AjaxControlToolkit.CalendarExtender();
    //       ibCalEx.ID = "ibCalEx";
    //        ibCalEx.ClientIDMode = ClientIDMode.Static;
    //        ibCalEx.TargetControlID =txtDate.ID;
    //        ibCalEx.Format = "dd/MM/yyyy";
    //        ibCalEx.PopupButtonID = ibCal.ID;
    //        ibCalEx.FirstDayOfWeek = FirstDayOfWeek.Monday;


    //        celld.Controls.Add(txtDate);
    //        celldc.Controls.Add(ibCal);
    //        celldc.Controls.Add(ibCalEx);


    //        cell1.Controls.Add(txtClient);
    //        cell2.Controls.Add(txtJob);
    //        cell3.Controls.Add(txtContrator);

    //        pnlCustomSearch.Controls.Add(subTable);

    //        //hide controls
    //        if (pnlFullMap.FindControl("ddlTableMap") != null)
    //        {
    //            DropDownList ddlTableMap = (DropDownList)pnlFullMap.FindControl("ddlTableMap");
    //            ddlTableMap.Style.Add("display", "none");
    //        }
    //        if (pnlFullMap.FindControl("txtMapSearch") != null)
    //        {
    //            TextBox txtMapSearch = (TextBox)pnlFullMap.FindControl("txtMapSearch");
    //            txtMapSearch.Style.Add("display", "none");
    //        }

    //    }
    //    //DataTable dtColumnsDetail = RecordManager.ets_Table_Columns_Detail((int)theService.TableID);
    //    //for (int i = 0; i < dtColumnsDetail.Rows.Count; i++)
    //    //{
    //    //    if (dtColumnsDetail.Rows[i]["SystemName"].ToString().ToLower() == "v010")
    //    //    {
    //    //        ScriptManager.RegisterStartupScript(upDetailDynamic, upDetailDynamic.GetType(),
    //    //            "AutoCompleteJS" + theService.Temp_DynamicPartName + i.ToString(), "", true);
    //    //        break;
    //    //    }
    //    //}

    //    return null;
    //}

    
    public static List<object> windtech_billingaddress_hide_addedby(List<object> objList)
    {
        List<object> roList = new List<object>();
        string strDynamicpart = "";
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                Service thisService = (Service)obj;
                if (thisService != null)
                {
                    strDynamicpart = thisService.Temp_DynamicPartName;
                    break;
                }
            }
        }

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_detailedit_ascx")
            {

                System.Web.UI.UserControl theDetailControl = (System.Web.UI.UserControl)obj;

                if (theDetailControl != null)
                {

                    //Panel pnlDetailWhole = (Panel)theDetailControl.FindControl("pnlDetailWhole");
                    //strDynamicpart = pnlDetailWhole.ClientID.Replace("pnlDetailWhole", "");
                    if (theDetailControl.FindControl(strDynamicpart + "trXDateAdded") != null)
                    {
                        HtmlTableRow trXDateAdded = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXDateAdded"));
                        trXDateAdded.Style.Add("display", "none");
                        roList.Add(trXDateAdded);
                    }
                    if (theDetailControl.FindControl(strDynamicpart + "trXDateUpdated") != null)
                    {
                        HtmlTableRow trXDateUpdated = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXDateUpdated"));
                        trXDateUpdated.Style.Add("display", "none");
                        roList.Add(trXDateUpdated);
                    }
                    //trXV001

                    if (theDetailControl.FindControl(strDynamicpart + "trXV001") != null)
                    {
                        HtmlTableRow trXV001 = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXV001"));
                        trXV001.Style.Add("display", "none");
                        roList.Add(trXV001);
                    }
                    if (theDetailControl.FindControl(strDynamicpart + "trXDateAdded") != null)
                    {
                        HtmlTableRow trXDateAdded = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXDateAdded"));
                        trXDateAdded.Style.Add("display", "none");
                        roList.Add(trXDateAdded);
                    }


                    return roList;
                }
            }
        }

        return roList;
    }
  
    
    public static List<object> windtech_billingaddress_hide_things(List<object> objList)
    {
        List<object> roList = new List<object>();

        string strDynamicpart = "";
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                Service thisService = (Service)obj;
                if (thisService != null)
                {
                    strDynamicpart = thisService.Temp_DynamicPartName;
                    break;
                }
            }
        }
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_detailedit_ascx")
            {

                System.Web.UI.UserControl theDetailControl = (System.Web.UI.UserControl)obj;

                if (theDetailControl != null)
                {

                    //Panel pnlDetailWhole = (Panel)theDetailControl.FindControl("pnlDetailWhole");
                    //strDynamicpart = pnlDetailWhole.ClientID.Replace("pnlDetailWhole", "");
                    if (theDetailControl.FindControl(strDynamicpart + "trXDateAdded") != null)
                    {
                        HtmlTableRow trXDateAdded = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXDateAdded"));
                        trXDateAdded.Style.Add("display", "none");

                    }
                    if (theDetailControl.FindControl(strDynamicpart + "trXDateUpdated") != null)
                    {
                        HtmlTableRow trXDateUpdated = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXDateUpdated"));
                        trXDateUpdated.Style.Add("display", "none");

                    }
                    //trXV001

                    if (theDetailControl.FindControl(strDynamicpart + "trXV013") != null)
                    {
                        HtmlTableRow trXV013 = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXV013"));
                        trXV013.Style.Add("display", "none");

                    }
                    if (theDetailControl.FindControl(strDynamicpart + "trXV014") != null)
                    {
                        HtmlTableRow trXV014 = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXV014"));
                        trXV014.Style.Add("display", "none");

                    }


                    return roList;
                }
            }
        }


        return roList;
    }
    public static List<object> windtech_billpayment_baddebt_entry_v2(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {

            if (obj.GetType().Name == "Panel")
            {


                Panel pnlFullDetailPage = (Panel)obj;
                HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                if (hfRecordID.Value == "-1")
                {
                    return null;
                }



                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Init")
                {
                    //HtmlTable tblLeft = (HtmlTable)pnlFullDetailPage.FindControl("tblLeft");
                    //xx
                    HtmlTableRow theBillingRow = (HtmlTableRow)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "trXV010");
                    TextBox lnkV037 = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV010");
                    lnkV037.Style.Add("display", "none");

                    //HtmlTableRow theBillingRow = (HtmlTableRow)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "trXV037");


                    //LinkButton lnkV037 = (LinkButton)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "lnkV037");
                    //lnkV037.Style.Add("display", "none");

                    HyperLink hlCustomAdd = new HyperLink();
                    hlCustomAdd.ID = theService.Temp_DynamicPartName + "hlCustomAdd" + theService.ServiceID.ToString();
                    hlCustomAdd.ClientIDMode = ClientIDMode.Static;
                    hlCustomAdd.CssClass = "popup_datatable_class";
                    hlCustomAdd.Text = "View Billing Calculation";

                   
                    string strSPName = Common.GetValueFromSQL("SELECT V001 FROM [Record] WHERE RecordID =2706494");
                  //  if (strSPName == "")
                        strSPName = "Renzo_CreateViewBillingInformation_testUI";
                    string strXML =  @"<root>" +
                                        " <TopTitle>"+ HttpUtility.HtmlEncode("View Billing Calculation") +"</TopTitle>" +
                                        " <RecordID>" + HttpUtility.HtmlEncode(hfRecordID.Value) + "</RecordID>" +
                                        " <spname>" + HttpUtility.HtmlEncode(strSPName) + "</spname>" +
                                        " <methodname>" + HttpUtility.HtmlEncode("sp_common_datatable") + "</methodname>" +
                                    "</root>";
                    SearchCriteria aSearchCriteria = new SearchCriteria(null, strXML);
                    int scid = SystemData.SearchCriteria_Insert(aSearchCriteria);
                    hlCustomAdd.NavigateUrl = "~/Pages/Generic/DataTable.aspx?scid=" + scid.ToString();
                    string strFancy = @"
                             $(function () {
                                        $("".popup_datatable_class"").fancybox({
                                             iframe : {
                                                css : {
                                                    width : '500px',
                                                    height: '450px'
                                                }
                                            },       
                                            toolbar  : false,
	                                        smallBtn : true,  
                                            scrolling: 'auto',
                                            type: 'iframe',    
                                            titleShow: false
                                        });
                                    });
                                ";
                    strFancy = SystemData.ErrorGuardForServiceJS(strFancy);

                    theService.Temp_CustomJSCode = strFancy;
                    roList.Add(theService);

                    theBillingRow.Cells[1].Controls.Add(hlCustomAdd);
                    return roList;
                }
               

            }

        }



        return null;

    }
    public static List<object> windtech_billpayment_baddebt_entry(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {

            if (obj.GetType().Name == "Panel")
            {


                Panel pnlFullDetailPage = (Panel)obj;
                HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                if (hfRecordID.Value == "-1")
                {
                    return null;
                }



                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Init")
                {
                    //HtmlTable tblLeft = (HtmlTable)pnlFullDetailPage.FindControl("tblLeft");
                    //xx
                    HtmlTableRow theBillingRow = (HtmlTableRow)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "trXV010");
                    TextBox lnkV037 = (TextBox)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "txtV010");
                    lnkV037.Style.Add("display", "none");

                    //HtmlTableRow theBillingRow = (HtmlTableRow)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "trXV037");


                    //LinkButton lnkV037 = (LinkButton)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "lnkV037");
                    //lnkV037.Style.Add("display", "none");

                    
                    HtmlTable tblCutom = new HtmlTable();
                    HtmlTableRow htr1 = new HtmlTableRow();
                    HtmlTableRow htr2 = new HtmlTableRow();
                    HtmlTableCell cell1 = new HtmlTableCell();
                    HtmlTableCell cell2 = new HtmlTableCell();
                    LinkButton lnkCustomAdd = new LinkButton();
                    lnkCustomAdd.ID = theService.Temp_DynamicPartName + "lnkCustomAdd" + theService.ServiceID.ToString();
                    lnkCustomAdd.ClientIDMode = ClientIDMode.Static;
                    lnkCustomAdd.CausesValidation = false;
                  
                    lnkCustomAdd.CommandArgument = theService.ServiceID.ToString();
                    lnkCustomAdd.CssClass = "btn";
                    lnkCustomAdd.Text = "<strong>View Billing Calculation</strong>";
                    //lnkCustomAdd.Click += new EventHandler();
                    cell1.Controls.Add(lnkCustomAdd);

                    GridView gvCustom = new GridView();
                    gvCustom.ID = theService.Temp_DynamicPartName + "gvCustom" + theService.ServiceID.ToString();
                    gvCustom.AutoGenerateColumns = true;
                    gvCustom.ShowHeaderWhenEmpty = true;
                    gvCustom.AllowPaging = false;
                    gvCustom.CssClass = "gvCustom";
                    gvCustom.HeaderStyle.CssClass = "gridview_header";
                    gvCustom.RowStyle.CssClass = "gridview_row";
                    //gvCustom.AlternatingRowStyle.AddAttributesToRender()
                    cell2.Controls.Add(gvCustom);
                    htr1.Cells.Add(cell1);
                    htr2.Cells.Add(cell2);
                    tblCutom.Rows.Add(htr1);
                    tblCutom.Rows.Add(htr2);
                    theBillingRow.Cells[1].Controls.Add(tblCutom);
                    ControlEvent aControlEvent = new ControlEvent(lnkCustomAdd, lnkCustomAdd.ID, "Service_Control_Click");
                    List<ControlEvent> lstControlEvent = new List<ControlEvent>();
                    lstControlEvent.Add(aControlEvent);
                    theService.Temp_ControlEventList = lstControlEvent;
                    roList.Add(theService);
                    return roList;
                }
                else if (thePageLifeCycleService.ServerControlEvent == "Service_Control_Click")
                {
                    GridView gvCustom = (GridView)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "gvCustom" + theService.ServiceID.ToString());
                    string strReturn = "";
                    string strSPName = Common.GetValueFromSQL("SELECT V001 FROM [Record] WHERE RecordID =2706494");
                    if (strSPName == "")
                        strSPName = "Renzo_CreateViewBillingInformation_testUI";

                    gvCustom.DataSource = RecordManager.SP_Common_TEST("Renzo_CreateViewBillingInformation_testUI", int.Parse(hfRecordID.Value),
                        null, null, ref strReturn);
                    gvCustom.DataBind();
                }

            }
         
        }



        return null;

    }

    public static List<object> DEV_billpayment_baddebt_entry(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService=null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                 theService = (Service)obj;
                 break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            
            if (obj.GetType().Name == "Panel")
            {
               
               
                Panel pnlFullDetailPage = (Panel)obj;
                HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                if (hfRecordID.Value == "-1")
                {
                    return null;
                }



                if(thePageLifeCycleService.ServerControlEvent=="ASP_Page_Init")
                {
                    HtmlTable tblLeft = (HtmlTable)pnlFullDetailPage.FindControl("tblLeft");
                    HtmlTableRow htr1 = new HtmlTableRow();
                    HtmlTableCell cell11 = new HtmlTableCell();
                    HtmlTableCell cell12 = new HtmlTableCell();
                    LinkButton lnkCustomAdd = new LinkButton();
                    lnkCustomAdd.ID = theService.Temp_DynamicPartName + "lnkCustomAdd" + theService.ServiceID.ToString();
                    lnkCustomAdd.ClientIDMode = ClientIDMode.Static;
                    lnkCustomAdd.CausesValidation = true;
                    lnkCustomAdd.CommandArgument = theService.ServiceID.ToString();
                    lnkCustomAdd.CssClass = "btn";
                    lnkCustomAdd.Text = "<strong>View Billing Calculation</strong>";
                    //lnkCustomAdd.Click += new EventHandler();
                    cell11.Controls.Add(lnkCustomAdd);

                    GridView gvCustom = new GridView();
                    gvCustom.ID = theService.Temp_DynamicPartName + "gvCustom" + theService.ServiceID.ToString();
                    gvCustom.AutoGenerateColumns = true;
                    gvCustom.ShowHeaderWhenEmpty = true;
                    gvCustom.AllowPaging = true;
                    cell12.Controls.Add(gvCustom);
                    htr1.Cells.Add(cell11);
                    htr1.Cells.Add(cell12);
                    tblLeft.Rows.Add(htr1);
                    ControlEvent aControlEvent = new ControlEvent(lnkCustomAdd, lnkCustomAdd.ID, "Service_Control_Click");
                    List<ControlEvent> lstControlEvent = new List<ControlEvent>();
                    lstControlEvent.Add(aControlEvent);
                    theService.Temp_ControlEventList = lstControlEvent;
                    roList.Add(theService);
                    return roList;
                }
               else if (thePageLifeCycleService.ServerControlEvent == "Service_Control_Click")
                {
                    GridView gvCustom = (GridView)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "gvCustom" + theService.ServiceID.ToString());
                    string strReturn = "";
                    gvCustom.DataSource = RecordManager.SP_Common_TEST("Renzo_CreateViewBillingInformation_testUI", int.Parse(hfRecordID.Value),
                        null, null, ref strReturn);
                    gvCustom.DataBind();
                }
               
            }
            //else if (obj.GetType().Name == "pages_record_recorddetail_aspx")
            //{
            //    System.Web.UI.Page thePage = (System.Web.UI.Page)obj;
            //    string strRecordID = Cryptography.Decrypt(thePage.Request.QueryString["RecordID"].ToString());
            //     if (thePageLifeCycleService.ServerControlEvent == "Service_Control_Click")
            //    {
            //        GridView gvCustom = (GridView)thePage.FindControl(theService.Temp_DynamicPartName + "gvCustom" + theService.ServiceID.ToString());
            //        string strReturn="";
            //        gvCustom.DataSource = RecordManager.SP_Common_TEST("Renzo_CreateViewBillingInformation_testUI", int.Parse(strRecordID),
            //            null, null, ref strReturn);
            //        gvCustom.DataBind();
            //    }
               
            //}
        }



        return null;

    }
    public static List<object> windtech_billing_top(List<object> objList)
    {
        List<object> roList = new List<object>();
        string strDynamicpart = "";
        string strType = "";
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "String")
            {

                strDynamicpart = (string)obj;
                if (strDynamicpart != null)
                {
                    break;
                }
            }
        }
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Label")
            {

                Label lblstrType = (Label)obj;
                if (lblstrType != null)
                {
                    strType = lblstrType.Text.Trim();
                    break;
                }
            }
        }

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullListControl = (Panel)obj;
                if (pnlFullListControl != null)
                {
                    if (pnlFullListControl.FindControl("pnlListTop") != null)
                    {
                        Panel pnlListTop = ((Panel)pnlFullListControl.FindControl("pnlListTop"));

                        if (strType == "create")
                        {
                            HtmlTableRow htr1 = new HtmlTableRow();
                            HtmlTableRow htr2 = new HtmlTableRow();
                            HtmlTableCell cell11 = new HtmlTableCell();
                            HtmlTableCell cell12 = new HtmlTableCell();
                            HtmlTableCell cell13 = new HtmlTableCell();
                            HtmlTableCell cell14 = new HtmlTableCell();
                            HtmlTableCell cell21 = new HtmlTableCell();
                            HtmlTableCell cell22 = new HtmlTableCell();
                            cell22.ColSpan = 3;
                            Label lbl1 = new Label();
                            Label lbl2 = new Label();
                            Label lbl3 = new Label();
                            lbl1.Text = "Job";
                            lbl2.Text = "Value";
                            lbl3.Text = "Title";
                            cell11.Controls.Add(lbl1);
                            cell13.Controls.Add(lbl2);
                            cell21.Controls.Add(lbl3);

                            DropDownList ddl1 = new DropDownList();
                            ddl1.ID = "ctl00_HomeContentPlaceHolder_ddlV007_C";
                            TextBox txt1 = new TextBox();

                            txt1.ID = "ctl00_HomeContentPlaceHolder_txtV001_C";


                            cell14.Controls.Add(ddl1);
                            cell22.Controls.Add(txt1);

                            htr1.Cells.Add(cell11);
                            htr1.Cells.Add(cell12);
                            htr1.Cells.Add(cell13);
                            htr1.Cells.Add(cell14);

                            htr2.Cells.Add(cell21);
                            htr2.Cells.Add(cell22);


                            HtmlTable htmlT = new HtmlTable();
                            //TheDatabase.SetEnabled(htr1, false);
                            //TheDatabase.SetEnabled(htr2, false);

                            htmlT.Rows.Add(htr1);
                            htmlT.Rows.Add(htr2);
                            // //TheDatabase.ChangeControlName(htmlT, "ChildBilling");
                            //htmlT.EnableViewState = false;
                            pnlListTop.Controls.Add(htmlT);
                        }
                        else
                        {
                            if (pnlFullListControl.Parent.Parent.Parent.Parent.Parent.FindControl("divDynamic").FindControl("ctl00_HomeContentPlaceHolder_ddlV007") != null)
                            {
                                DropDownList ddlV007 = (DropDownList)pnlFullListControl.Parent.Parent.Parent.Parent.Parent.FindControl("divDynamic").FindControl("ctl00_HomeContentPlaceHolder_ddlV007");
                                if (ddlV007 != null && ddlV007.SelectedItem != null && pnlListTop.FindControl("ctl00_HomeContentPlaceHolder_ddlV007_C") != null)
                                {
                                    ListItem aListItem = new ListItem(ddlV007.SelectedItem.Text, ddlV007.SelectedItem.Value); ;
                                    DropDownList ddlV007_C = (DropDownList)pnlListTop.FindControl("ctl00_HomeContentPlaceHolder_ddlV007_C");
                                    ddlV007_C.Items.Add(aListItem);
                                    ddlV007_C.Enabled = false;
                                }
                                //htr1.ID = htr1.ID + "child";
                            }
                            if (pnlFullListControl.Parent.Parent.Parent.Parent.Parent.FindControl("divDynamic").FindControl("ctl00_HomeContentPlaceHolder_txtV001") != null)
                            {
                                TextBox txtV001 = (TextBox)pnlFullListControl.Parent.Parent.Parent.Parent.Parent.FindControl("divDynamic").FindControl("ctl00_HomeContentPlaceHolder_txtV001");
                                if (txtV001 != null && pnlListTop.FindControl("ctl00_HomeContentPlaceHolder_txtV001_C") != null)
                                {
                                    TextBox txtV001_c = (TextBox)pnlListTop.FindControl("ctl00_HomeContentPlaceHolder_txtV001_C");
                                    txtV001_c.Text = txtV001.Text;
                                    txtV001_c.Enabled = false;
                                }
                            }
                            ;

                        }


                        //roList.Add(pnlListTop);



                        return roList;
                    }

                }
            }
        }

        return roList;
    }

    public static List<object> windtech_searchbox_disbursement_date(List<object> objList)
    {
        List<object> roList = new List<object>();

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_recordlist_ascx")
            //if (obj.GetType().Name == "Panel")
            {
                System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;
                //Panel pnlFullListControl = (Panel)obj;
                if (theListControl != null)
                {
                    if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null)
                    {
                        if (theListControl.FindControl("txtLowerDate_V002") != null)
                        {
                            TextBox txtLowerDate = (TextBox)theListControl.FindControl("txtLowerDate_V002");
                            TextBox txtUpperDate = (TextBox)theListControl.FindControl("txtUpperDate_V002");
                            if (txtLowerDate != null && txtUpperDate != null)
                            {
                                if (System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_l"] != null)
                                {
                                    txtLowerDate.Text = System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_l"].ToString();
                                    txtUpperDate.Text = System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_u"].ToString();
                                    System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_l"] = null;
                                    System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_u"] = null;
                                    //roList.Add(txtLowerDate);
                                    //roList.Add(txtUpperDate);
                                    //string strMessage = "Updated Timesheet search date has been used in this search.";
                                    //roList.Add(strMessage);
                                }
                                System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_l"] = txtLowerDate.Text;
                                System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_u"] = txtUpperDate.Text;
                                break;
                            }
                        }

                    }

                    break;
                }
            }
        }


        return roList;
    }

    public static List<object> windtech_searchbox_timesheet_date(List<object> objList)
    {
        List<object> roList = new List<object>();

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_recordlist_ascx")
            //if (obj.GetType().Name == "Panel")
            {
                System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;
                //Panel pnlFullListControl = (Panel)obj;
                if (theListControl != null)
                {


                    if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null)
                    {
                        if (theListControl.FindControl("txtLowerDate_V004") != null)
                        {
                            TextBox txtLowerDate = (TextBox)theListControl.FindControl("txtLowerDate_V004");//4
                            TextBox txtUpperDate = (TextBox)theListControl.FindControl("txtUpperDate_V004");//
                            if (txtLowerDate != null && txtUpperDate != null)
                            {
                                if (System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_l"] != null)
                                {
                                    txtLowerDate.Text = System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_l"].ToString();
                                    txtUpperDate.Text = System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_u"].ToString();
                                    System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_l"] = null;
                                    System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_u"] = null;
                                    //roList.Add(txtLowerDate);
                                    //roList.Add(txtUpperDate);
                                    //string strMessage="Updated disbursement search date has been used in this search.";
                                    //roList.Add(strMessage);
                                }

                                System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_l"] = txtLowerDate.Text;
                                System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_u"] = txtUpperDate.Text;
                                //break;
                            }
                        }

                    }




                    break;
                }
            }
        }





        return roList;
    }
    
    public static List<object> tdb_columntype_number_id_71666(List<object> objList)
    {
        //seventh account
        List<object> roList = new List<object>();
        if (objList[0] != null)
        {
            if (objList[0].GetType().Name == "Column")
            {
                //reset values
                Column theColumn = (Column)objList[0];
                if (theColumn != null)
                {
                    SystemData.spResetIDs((int)theColumn.TableID, theColumn.SystemName, false);
                    Common.ExecuteText("UPDATE [Record] SET " + theColumn.SystemName + "='Org-' +RIGHT('0000' +"
                        + theColumn.SystemName + ",4)  FROM [Record] WHERE TableID=" + theColumn.TableID.ToString()
                        + " AND IsActive=1 AND " + theColumn.SystemName + " is not null and ISNUMERIC(" + theColumn.SystemName + ")=1");
                    roList.Add(true);
                }
            }
            else
            {
                string strValue = (string)objList[0];
                strValue = "Org-" + int.Parse(strValue).ToString("0000");
                roList.Add(strValue);
            }

        }

        return roList;
    }

    public static List<object> tdb_columntype_number_id_62667(List<object> objList)
    {
        //seventh account  - MR Local 71666
        List<object> roList = new List<object>();
        if (objList[0] != null)
        {
            if(objList[0].GetType().Name=="Column")
            {
                //reset values
                Column theColumn = (Column)objList[0];
                if(theColumn!=null)
                {
                   SystemData.spResetIDs((int)theColumn.TableID, theColumn.SystemName, false);
                   Common.ExecuteText("UPDATE [Record] SET " + theColumn.SystemName + "='Org-' +RIGHT('0000' +" 
                       + theColumn.SystemName + ",4)  FROM [Record] WHERE TableID="+theColumn.TableID.ToString()
                       + " AND IsActive=1 AND " + theColumn.SystemName + " is not null and ISNUMERIC(" + theColumn.SystemName + ")=1");
                   roList.Add(true);
                }
            }
            else
            {
                string strValue = (string)objList[0];
                strValue = "Org-" + int.Parse(strValue).ToString("0000");
                roList.Add(strValue);
            }
          
        }

        return roList;
    }
    

    //public static List<object> seventh_victimcm_include_listdetail(List<object> objList)
    //{
    //    //
    //    List<object> roList = new List<object>();
    //    Service theService = null;
    //    foreach (object obj in objList)
    //    {
    //        if (obj.GetType().Name == "Service")
    //        {
    //            theService = (Service)obj;
    //            break;
    //        }
    //    }
    //    PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

    //    foreach (object obj in objList)
    //    {
    //        if (obj.GetType().Name == "Panel")
    //        {
    //            Panel pnlFullDetailPage = (Panel)obj;
    //            HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
    //            HiddenField hfRecordAddEditView = (HiddenField)pnlFullDetailPage.FindControl("hfRecordAddEditView");

    //            if (hfRecordAddEditView.Value == "add")
    //            {
    //                return null;
    //            }

    //            HiddenField hfRecordDetailSCid = (HiddenField)pnlFullDetailPage.FindControl("hfRecordDetailSCid");
    //            Panel pnlDetail = (Panel)pnlFullDetailPage.FindControl("pnlDetail");
    //            //"ASP_Page_Init"  ASP_Page_Load
    //            if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
    //            {
    //                Record theRecord = RecordManager.ets_Record_Detail_Full(int.Parse(hfRecordID.Value));
    //                if (theRecord != null && !string.IsNullOrEmpty(theRecord.V012))
    //                {
    //                    Page thePage = pnlDetail.Page;
    //                    UserControl aD = (UserControl)thePage.LoadControl("~/Pages/UserControl/DetailEdit.ascx");
    //                    HtmlTableRow rowEmpty1 = (HtmlTableRow)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_trXV016");
    //                    Label lblE1 = (Label)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_lblV016");
    //                    TextBox txtE1 = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txtV016");
    //                    lblE1.Visible = false;
    //                    txtE1.Visible = false;
    //                    Record thePRecord = RecordManager.ets_Record_Detail_Full(int.Parse(theRecord.V012));
    //                    aD.GetType().GetProperty("RecordID").SetValue(aD, int.Parse(theRecord.V012));
    //                    aD.GetType().GetProperty("TableID").SetValue(aD, (int)thePRecord.TableID);
    //                    aD.GetType().GetProperty("ContentPage").SetValue(aD, "record");
    //                    aD.GetType().GetProperty("Mode").SetValue(aD, "view");
    //                    aD.GetType().GetProperty("ShowEditButton").SetValue(aD, true);
    //                    aD.GetType().GetProperty("HideHistory").SetValue(aD, true);
    //                    aD.GetType().GetProperty("ColumnNotIn").SetValue(aD, "57352,57353");
    //                    rowEmpty1.Cells[1].Controls.Add(aD);
    //                    rowEmpty1.Cells[1].Style.Add("border", "solid 1px black");
    //                    UserControl aL = (UserControl)thePage.LoadControl("~/Pages/UserControl/RecordList.ascx");
    //                    HtmlTableRow rowEmpty2 = (HtmlTableRow)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_trXV017");
    //                    Label lblE2 = (Label)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_lblV017");
    //                    TextBox txtE2 = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txtV017");
    //                    lblE2.Visible = false;
    //                    txtE2.Visible = false;
    //                    aL.GetType().GetProperty("TableID").SetValue(aL, 2972);
    //                    aL.GetType().GetProperty("PageType").SetValue(aL, "any");
    //                    aL.GetType().GetProperty("AnyTextSearch").SetValue(aL, " AND Record.V003='122' ");
    //                    //rowEmpty2.Cells[1].Style.Add("border", "solid 1px black");
    //                    rowEmpty2.Cells[1].Controls.Add(aL);
    //                }

    //            }

    //            break;
    //        }
    //    }





    //    return null;

    //}

    public static List<object> cs_test_method_join(List<object> objList)
    {
        List<object> roList = new List<object>();

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Record")
            {
                Record theRecord = (Record)obj;
                if (theRecord != null)
                {
                    theRecord.V010 = theRecord.V010 + @" 
                                           C#, version: " + System.Environment.Version.ToString();

                    string xmlRecord = TheDatabase.RecordToXML(theRecord, null);
                    DataTable dtD = TheDatabase.XMLtoDataTableDetail(xmlRecord, theRecord.TableID);
                    roList.Add(dtD);
                    roList.Add("From C# cs_test_method_join METHOD.".ToString());
                    return roList;
                }
            }
        }

        return roList;
    }
    public static List<object> cs_test_method_EMPTY(List<object> objList)
    {
        List<object> roList = new List<object>();

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Record")
            {
                Record theRecord = (Record)obj;
                if (theRecord != null)
                {
                    theRecord.V001 = "";
                    theRecord.V002 = "";
                    string xmlRecord = TheDatabase.RecordToXML(theRecord, null);
                    DataTable dtD = TheDatabase.XMLtoDataTableDetail(xmlRecord, theRecord.TableID);
                    roList.Add(dtD);
                    roList.Add("From C# cs_test_method_EMPTY METHOD.".ToString());
                    return roList;
                }
            }
        }

        return roList;
    }
    public static List<object> mr_dashboard_test(List<object> objList)
    {
        List<object> roList = new List<object>();




        return roList;
    }
    public static List<object> testing_new_site(List<object> objList)
    {
        List<object> roList = new List<object>();
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_recordlist_ascx")
            //if (obj.GetType().Name == "Panel")
            {
                
                System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;
                if (theListControl != null)
                {
                   
                    if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null)
                    {
                        if (theListControl.FindControl("txtSearch_V001") != null)
                        {
                            TextBox txtSearch_V001 = (TextBox)theListControl.FindControl("txtSearch_V001");
                            txtSearch_V001.Text = "";
                            txtSearch_V001.Enabled = false;
                        }
                    }

                }
            }
        }


        return roList;

    }
}
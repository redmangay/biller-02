﻿using DocGen.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
/// <summary>
/// Summary description for CustomMethod_windtech - Red
/// </summary>
public class CustomMethod_windtech
{
	public CustomMethod_windtech()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public static List<object> DotNetMethod(string sMethodName, List<object> objList)
    {
        List<object> roList = new List<object>();
        try
        {

            switch (sMethodName.ToLower())
            {
             
                case "windtech_searchbox_disbursement_date":
                    roList = windtech_searchbox_disbursement_date(objList);
                    return roList;
                case "windtech_searchbox_timesheet_date":
                    roList = windtech_searchbox_timesheet_date(objList);
                    return roList;
                case "windtech_billing_top":
                    roList = windtech_billing_top(objList);
                    return roList;

                case "windtech_billingaddress_hide_things":
                    roList = windtech_billingaddress_hide_things(objList);
                    return roList;
                case "windtech_billpayment_baddebt_entry":
                    roList = windtech_billpayment_baddebt_entry(objList);
                    return roList;
                case "windtech_disbursement_entry":
                    roList = windtech_disbursement_entry(objList);
                    return roList;

                case "windtech_divfooter_2711":
                    roList = windtech_divfooter_2711(objList);
                    return roList;

                case "windtech_interim_credit":
                    roList = windtech_interim_credit(objList);
                    return roList;

                case "windtech_total_custom_elements":
                    roList = windtech_total_custom_elements(objList);
                    return roList;

                case "windtech_bapa_custom_elements":
                    roList = windtech_bapa_custom_elements(objList);
                    return roList;

                case "windtech_timesheet_details_custom_elements":
                    roList = windtech_timesheet_details_custom_elements(objList);
                    return roList;

                case "windtech_addlink_contact":
                    roList = windtech_addlink_contact(objList);
                    return roList;

                case "windtech_delete_bapa":
                    roList = windtech_delete_bapa(objList);
                    return roList;

                case "windtech_process_unallocated_payments":
                    roList = windtech_process_unallocated_payments(objList);
                    return roList;

                case "windtech_view_billing_calculation":
                    roList = windtech_view_billing_calculation(objList);
                    return roList;

                case "timesheet_details_view_data_history":
                    roList = timesheet_details_view_data_history(objList);
                    return roList;

            }

            return roList;
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "DotNetMethod/SP" + sMethodName, ex.Message, ex.StackTrace, DateTime.Now, "App_Code");
            SystemData.ErrorLog_Insert(theErrorLog);
            if (roList != null)
                roList.Add(ex);
        }
        return null;
    }

    public static List<object> windtech_billingaddress_hide_addedby(List<object> objList)
    {
        List<object> roList = new List<object>();
        string strDynamicpart = "";
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                Service thisService = (Service)obj;
                if (thisService != null)
                {
                    strDynamicpart = thisService.Temp_DynamicPartName;
                    break;
                }
            }
        }

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_detailedit_ascx")
            {

                System.Web.UI.UserControl theDetailControl = (System.Web.UI.UserControl)obj;

                if (theDetailControl != null)
                {

                    if (theDetailControl.FindControl(strDynamicpart + "trXDateAdded") != null)
                    {
                        HtmlTableRow trXDateAdded = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXDateAdded"));
                        trXDateAdded.Style.Add("display", "none");
                        roList.Add(trXDateAdded);
                    }
                    if (theDetailControl.FindControl(strDynamicpart + "trXDateUpdated") != null)
                    {
                        HtmlTableRow trXDateUpdated = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXDateUpdated"));
                        trXDateUpdated.Style.Add("display", "none");
                        roList.Add(trXDateUpdated);
                    }

                    if (theDetailControl.FindControl(strDynamicpart + "trXV001") != null)
                    {
                        HtmlTableRow trXV001 = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXV001"));
                        trXV001.Style.Add("display", "none");
                        roList.Add(trXV001);
                    }
                    if (theDetailControl.FindControl(strDynamicpart + "trXDateAdded") != null)
                    {
                        HtmlTableRow trXDateAdded = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXDateAdded"));
                        trXDateAdded.Style.Add("display", "none");
                        roList.Add(trXDateAdded);
                    }


                    return roList;
                }
            }
        }

        return roList;
    }

    public static List<object> windtech_billingaddress_hide_things(List<object> objList)
    {
        List<object> roList = new List<object>();

        string strDynamicpart = "";
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                Service thisService = (Service)obj;
                if (thisService != null)
                {
                    strDynamicpart = thisService.Temp_DynamicPartName;
                    break;
                }
            }
        }
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_detailedit_ascx")
            {

                System.Web.UI.UserControl theDetailControl = (System.Web.UI.UserControl)obj;

                if (theDetailControl != null)
                {

                    if (theDetailControl.FindControl(strDynamicpart + "trXDateAdded") != null)
                    {
                        HtmlTableRow trXDateAdded = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXDateAdded"));
                        trXDateAdded.Style.Add("display", "none");

                    }
                    if (theDetailControl.FindControl(strDynamicpart + "trXDateUpdated") != null)
                    {
                        HtmlTableRow trXDateUpdated = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXDateUpdated"));
                        trXDateUpdated.Style.Add("display", "none");

                    }

                    if (theDetailControl.FindControl(strDynamicpart + "trXV013") != null)
                    {
                        HtmlTableRow trXV013 = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXV013"));
                        trXV013.Style.Add("display", "none");

                    }
                    if (theDetailControl.FindControl(strDynamicpart + "trXV014") != null)
                    {
                        HtmlTableRow trXV014 = ((HtmlTableRow)theDetailControl.FindControl(strDynamicpart + "trXV014"));
                        trXV014.Style.Add("display", "none");

                    }


                    return roList;
                }
            }
        }


        return roList;
    }
    
    public static List<object> windtech_billing_top(List<object> objList)
    {
        List<object> roList = new List<object>();
        string strDynamicpart = "";
        string strType = "";
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "String")
            {

                strDynamicpart = (string)obj;
                if (strDynamicpart != null)
                {
                    break;
                }
            }
        }
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Label")
            {

                Label lblstrType = (Label)obj;
                if (lblstrType != null)
                {
                    strType = lblstrType.Text.Trim();
                    break;
                }
            }
        }

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullListControl = (Panel)obj;
                if (pnlFullListControl != null)
                {
                    if (pnlFullListControl.FindControl("pnlListTop") != null)
                    {
                        Panel pnlListTop = ((Panel)pnlFullListControl.FindControl("pnlListTop"));

                        if (strType == "create")
                        {
                            HtmlTableRow htr1 = new HtmlTableRow();
                            HtmlTableRow htr2 = new HtmlTableRow();
                            HtmlTableCell cell11 = new HtmlTableCell();
                            HtmlTableCell cell12 = new HtmlTableCell();
                            HtmlTableCell cell13 = new HtmlTableCell();
                            HtmlTableCell cell14 = new HtmlTableCell();
                            HtmlTableCell cell21 = new HtmlTableCell();
                            HtmlTableCell cell22 = new HtmlTableCell();
                            cell22.ColSpan = 3;
                            Label lbl1 = new Label();
                            Label lbl2 = new Label();
                            Label lbl3 = new Label();
                            lbl1.Text = "Job";
                            lbl2.Text = "Value";
                            lbl3.Text = "Title";
                            cell11.Controls.Add(lbl1);
                            cell13.Controls.Add(lbl2);
                            cell21.Controls.Add(lbl3);

                            DropDownList ddl1 = new DropDownList();
                            ddl1.ID = "ctl00_HomeContentPlaceHolder_ddlV007_C";
                            TextBox txt1 = new TextBox();

                            txt1.ID = "ctl00_HomeContentPlaceHolder_txtV001_C";


                            cell14.Controls.Add(ddl1);
                            cell22.Controls.Add(txt1);

                            htr1.Cells.Add(cell11);
                            htr1.Cells.Add(cell12);
                            htr1.Cells.Add(cell13);
                            htr1.Cells.Add(cell14);

                            htr2.Cells.Add(cell21);
                            htr2.Cells.Add(cell22);

                            HtmlTable htmlT = new HtmlTable();

                            htmlT.Rows.Add(htr1);
                            htmlT.Rows.Add(htr2);
                            pnlListTop.Controls.Add(htmlT);
                        }
                        else
                        {
                            if (pnlFullListControl.Parent.Parent.Parent.Parent.Parent.FindControl("divDynamic").FindControl("ctl00_HomeContentPlaceHolder_ddlV007") != null)
                            {
                                DropDownList ddlV007 = (DropDownList)pnlFullListControl.Parent.Parent.Parent.Parent.Parent.FindControl("divDynamic").FindControl("ctl00_HomeContentPlaceHolder_ddlV007");
                                if (ddlV007 != null && ddlV007.SelectedItem != null && pnlListTop.FindControl("ctl00_HomeContentPlaceHolder_ddlV007_C") != null)
                                {
                                    ListItem aListItem = new ListItem(ddlV007.SelectedItem.Text, ddlV007.SelectedItem.Value); ;
                                    DropDownList ddlV007_C = (DropDownList)pnlListTop.FindControl("ctl00_HomeContentPlaceHolder_ddlV007_C");
                                    ddlV007_C.Items.Add(aListItem);
                                    ddlV007_C.Enabled = false;
                                }
                            }
                            if (pnlFullListControl.Parent.Parent.Parent.Parent.Parent.FindControl("divDynamic").FindControl("ctl00_HomeContentPlaceHolder_txtV001") != null)
                            {
                                TextBox txtV001 = (TextBox)pnlFullListControl.Parent.Parent.Parent.Parent.Parent.FindControl("divDynamic").FindControl("ctl00_HomeContentPlaceHolder_txtV001");
                                if (txtV001 != null && pnlListTop.FindControl("ctl00_HomeContentPlaceHolder_txtV001_C") != null)
                                {
                                    TextBox txtV001_c = (TextBox)pnlListTop.FindControl("ctl00_HomeContentPlaceHolder_txtV001_C");
                                    txtV001_c.Text = txtV001.Text;
                                    txtV001_c.Enabled = false;
                                }
                            }
                            ;

                        }

                        return roList;
                    }

                }
            }
        }

        return roList;
    }

    public static List<object> windtech_searchbox_disbursement_date(List<object> objList)
    {
        List<object> roList = new List<object>();

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_recordlist_ascx")
            {
                System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;

                if (theListControl != null)
                {

                    if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null)
                    {
                        if (theListControl.FindControl("txtLowerDate_V002") != null)
                        {
                            TextBox txtLowerDate = (TextBox)theListControl.FindControl("txtLowerDate_V002");
                            TextBox txtUpperDate = (TextBox)theListControl.FindControl("txtUpperDate_V002");
                            if (txtLowerDate != null && txtUpperDate != null)
                            {
                                if (System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_l"] != null)
                                {
                                    txtLowerDate.Text = System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_l"].ToString();
                                    txtUpperDate.Text = System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_u"].ToString();
                                    System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_l"] = null;
                                    System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_u"] = null;
                                }
                                System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_l"] = txtLowerDate.Text;
                                System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_u"] = txtUpperDate.Text;
                                break;
                            }
                        }

                    }

                    break;
                }
            }
        }

        return roList;
    }

    public static List<object> windtech_searchbox_timesheet_date(List<object> objList)
    {
        List<object> roList = new List<object>();

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_recordlist_ascx")
            {
                System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;
                if (theListControl != null)
                {
                    if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null)
                    {
                        if (theListControl.FindControl("txtLowerDate_V004") != null)
                        {
                            TextBox txtLowerDate = (TextBox)theListControl.FindControl("txtLowerDate_V004");
                            TextBox txtUpperDate = (TextBox)theListControl.FindControl("txtUpperDate_V004");
                            if (txtLowerDate != null && txtUpperDate != null)
                            {
                                if (System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_l"] != null)
                                {
                                    txtLowerDate.Text = System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_l"].ToString();
                                    txtUpperDate.Text = System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_u"].ToString();
                                    System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_l"] = null;
                                    System.Web.HttpContext.Current.Session["windtech_searchbox_disbursement_date_u"] = null;
                                }

                                System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_l"] = txtLowerDate.Text;
                                System.Web.HttpContext.Current.Session["windtech_searchbox_timesheet_date_u"] = txtUpperDate.Text;
                                //break;
                            }
                        }
                    }
                    break;
                }
            }
        }

        return roList;
    }

    public static List<object> windtech_billpayment_baddebt_entry(List<object> objList)
    {
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;
                HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                if (hfRecordID.Value == "-1")
                {
                    return null;
                }

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Init")
                {
                    HtmlTableRow theBillingRow = (HtmlTableRow)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "trXV037");
                    LinkButton lnkV037 = (LinkButton)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "lnkV037");
                    lnkV037.Style.Add("display", "none");

                    HtmlTable tblCutom = new HtmlTable();
                    HtmlTableRow htr1 = new HtmlTableRow();
                    HtmlTableRow htr2 = new HtmlTableRow();
                    HtmlTableCell cell1 = new HtmlTableCell();
                    HtmlTableCell cell2 = new HtmlTableCell();
                    LinkButton lnkCustomAdd = new LinkButton();
                    lnkCustomAdd.ID = theService.Temp_DynamicPartName + "lnkCustomAdd" + theService.ServiceID.ToString();
                    lnkCustomAdd.ClientIDMode = ClientIDMode.Static;
                    lnkCustomAdd.CausesValidation = false;

                    lnkCustomAdd.CommandArgument = theService.ServiceID.ToString();
                    lnkCustomAdd.CssClass = "btn";
                    lnkCustomAdd.Text = "<strong>View Billing Calculation</strong>";

                    cell1.Controls.Add(lnkCustomAdd);

                    GridView gvCustom = new GridView();
                    gvCustom.ID = theService.Temp_DynamicPartName + "gvCustom" + theService.ServiceID.ToString();
                    gvCustom.ClientIDMode = ClientIDMode.Static;
                    gvCustom.AutoGenerateColumns = true;
                    gvCustom.ShowHeaderWhenEmpty = true;
                    gvCustom.AllowPaging = false;
                    gvCustom.CssClass = "gridview";
                    gvCustom.HeaderStyle.CssClass = "gridview_header";
                    gvCustom.RowStyle.CssClass = "gridview_row";
                  
                    Panel pnlCustomGridCon = new Panel();
                    pnlCustomGridCon.ID = theService.Temp_DynamicPartName + "pnlCustomGridCon" + theService.ServiceID.ToString();
                    pnlCustomGridCon.ClientIDMode = ClientIDMode.Static;
                    pnlCustomGridCon.Style.Add("display", "none");
                    pnlCustomGridCon.Controls.Add(gvCustom);
                    cell2.Controls.Add(pnlCustomGridCon);
                    htr1.Cells.Add(cell1);
                    htr2.Cells.Add(cell2);
                    tblCutom.Rows.Add(htr1);
                    tblCutom.Rows.Add(htr2);
                    theBillingRow.Cells[1].Controls.Add(tblCutom);
                    ControlEvent aControlEvent = new ControlEvent(lnkCustomAdd, lnkCustomAdd.ID, "Service_Control_Click");
                    List<ControlEvent> lstControlEvent = new List<ControlEvent>();
                    lstControlEvent.Add(aControlEvent);
                    theService.Temp_ControlEventList = lstControlEvent;
                    roList.Add(theService);
                    return roList;
                }
                else if (thePageLifeCycleService.ServerControlEvent == "Service_Control_Click")
                {
                    GridView gvCustom = (GridView)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "gvCustom" + theService.ServiceID.ToString());

                    Panel pnlCustomGridCon = (Panel)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "pnlCustomGridCon" + theService.ServiceID.ToString());
                    pnlCustomGridCon.Style.Add("display", "block");
                    string strReturn = "";
                    string strSPName = Common.GetValueFromSQL("SELECT V001 FROM [Record] WHERE RecordID =2706494");
                    if (strSPName == "")
                        strSPName = "Renzo_CreateViewBillingInformation";

                    gvCustom.DataSource = RecordManager.SP_Common_TEST(strSPName, int.Parse(hfRecordID.Value),
                        null, null, ref strReturn);
                    gvCustom.DataBind();
                }
            }
        }
        return null;
    }

    public static List<object> windtech_disbursement_entry(List<object> objList)
    {
        //RED - windtech disbursement Item Predictive DD
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;

                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {

            if (obj.GetType().Name == "Panel")
            {
                try
                {

                    Panel pnlFullDetailPage = (Panel)obj;
                    UpdatePanel upDetailDynamic = null;



                    if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_PreRender")
                    {
                        HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");

                        if (hfRecordID.Value != "-1")
                        {
                            TextBox txtItemDD = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txtV003");
                            txtItemDD.Text = Common.GetValueFromSQL("SELECT V001 FROM[Record] R1 WHERE  R1.RECORDID = (SELECT R2.V003 FROM[Record] R2 WHERE R2.RECORDID = '" + hfRecordID.Value + "')");
                        }
                    }

                    if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Start_Init")
                    {
                        upDetailDynamic = (UpdatePanel)pnlFullDetailPage.FindControl("upDetailDynamic");

                        ScriptManager.RegisterStartupScript(upDetailDynamic, upDetailDynamic.GetType(), "AutoCompleteJS" + "ctl00_HomeContentPlaceHolder_4", "", true);


                        string strID = "ctl00_HomeContentPlaceHolder_txtV003";
                        string strhfID = "ctl00_HomeContentPlaceHolder_hfV003";
                        string strAutoDDJS = @" $(function () {$('#ctl00_HomeContentPlaceHolder_txtV001').blur(function (e) {
	                                        setTimeout(function () 
		                                        {  	            
	                                            var vRecordID=$('#ctl00_HomeContentPlaceHolder_hfV001').val();   
	                                                    if(vRecordID!='')   
		                                        {    
                                                $(""#" + strID.ToString() + @""").autocomplete({
                                    source: function (request, response) {
                                        $.ajax({
                                            url: ""../../CascadeDropdown.asmx/GetDisplayColumnsOfTwoTables"",
                                            data: ""{'RecordID':'""+ vRecordID +""' , 'search': '"" + request.term.replace(/'/g, ""\\'"") + ""' }"",
                                            dataType: ""json"",
                                            type: ""POST"",
                                            contentType: ""application/json; charset=utf-8"",
                                            dataFilter: function (data) { return data; },
                                            success: function (data) {
                                                response($.map(data.d, function (item) {
                                                    return {
                                                        value: item.Text,
                                                        id: item.ID
                                                    }
                                                }))
                                            },
                                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                                
                                            }
                                        });
                                    },
                                                minLength: 1,
                                                select: function (event, ui) {
                                                    if (ui.item.id == null) {
                                                        document.getElementById('" + strhfID.ToString() + @"').value = '';
                                                    }
                                                    else {
                                                        document.getElementById('" + strhfID.ToString() + @"').value = ui.item.id;
                                                    }
                                                }
                                            });
                                                    }
			

	                                        },300);

                                                });

                                        });

                                ";
                     
                        ScriptManager.RegisterStartupScript(pnlFullDetailPage, pnlFullDetailPage.GetType(), "AutoCompleteJS" + "ctl00_HomeContentPlaceHolder_4", strAutoDDJS, true);
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, " Disbursement", ex.Message, ex.StackTrace, DateTime.Now, "CustomMethod_windtech");
                    SystemData.ErrorLog_Insert(theErrorLog);
                }
            }
        }
        return null;
    }

    /* Total
      * - Interim Invoice 
      * - Interim Credit */
    public static List<object> windtech_total_custom_elements(List<object> objList)
    {
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            string chcec = obj.GetType().Name.ToString();

            if (obj.GetType().Name == "pages_usercontrol_recordlist_ascx")
            {
                System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Init")
                {
                    HtmlControl theInteCreditTabList = (HtmlControl)theListControl.FindControl("CustomDivFooterRow");
                    theInteCreditTabList.Attributes.Add("style", "float:right; padding-right:50px");

                    HtmlTable tblCustom = new HtmlTable();
                    HtmlTableRow htr1 = new HtmlTableRow();
                    htr1.ID = theService.Temp_DynamicPartName + "hrt1" + theService.TableChildID.ToString();
                    HtmlTableCell cell11 = new HtmlTableCell();
                    HtmlTableCell cell12 = new HtmlTableCell();
                    cell12.Attributes.Add("style", "width:20px;");
                    HtmlTableCell cell13 = new HtmlTableCell();
                    HtmlTableCell cell14 = new HtmlTableCell();

                    //First Row
                    Label lbl1One = new Label();
                    lbl1One.ID = theService.Temp_DynamicPartName + "lbl1One" + theService.TableChildID.ToString();
                    lbl1One.ClientIDMode = ClientIDMode.Static;
                    lbl1One.Text = "Total: ";
                    //lbl1One.Font.Bold = true;
                    lbl1One.Attributes.Add("style", "font-weight:bold; font-size:11px !important;");

                    cell11.Controls.Add(lbl1One);

                    Label lbl1Two = new Label();
                    lbl1Two.ID = theService.Temp_DynamicPartName + "lbl1Two" + theService.TableChildID.ToString();
                    lbl1Two.ClientIDMode = ClientIDMode.Static;
                    lbl1Two.Text = "0.00";
                    //lbl1Two.Font.Bold = true;
                    lbl1Two.Attributes.Add("style", "font-weight:bold; font-size:11px !important;");

                    cell14.Controls.Add(lbl1Two);

                    htr1.Cells.Add(cell11);
                    htr1.Cells.Add(cell12);
                    htr1.Cells.Add(cell13);
                    htr1.Cells.Add(cell14);
                    tblCustom.Rows.Add(htr1);
                    theInteCreditTabList.Controls.Add(tblCustom);
                    return roList;
                }
            }
        }
        return null;
    }

    /* Invoice (Payment) and Balance (Outstanding)
     * - bottom totals in Job Information > BAPA tab */
    public static List<object> windtech_bapa_custom_elements(List<object> objList)
    {
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {

            if (obj.GetType().Name == "pages_usercontrol_recordlist_ascx")
            {

                System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;
                Panel pnlFullListControl = (Panel)theListControl.FindControl("pnlFullListControl");

                HiddenField hfParentRecordIDList = (HiddenField)pnlFullListControl.FindControl("hfParentRecordIDList");

                DataTable dtTimesheetTotals = null;
                if (hfParentRecordIDList == null)
                {
                    return null;
                }
                else if (hfParentRecordIDList.Value == "-1")
                {
                    return null;
                }

                /* We rely on the parent recordids:
                   * Job info: Timesheet Details
                   * Edit BAPA Invoice: Billed Details
                   * Add BAPA Invoice: Billed Details
                   * Edit BAPA Credit Entry: Credit Details
                  */

                dtTimesheetTotals = Common.DataTableFromText(thePageLifeCycleService.SPName + " " + hfParentRecordIDList.Value);

                if (dtTimesheetTotals != null && dtTimesheetTotals.Rows.Count > 0)
                {

                    string strlbl1One = "";
                    decimal strlbl1Two = 0;
                    decimal strlbl1Three = 0;
                    string strlbl2One = "";
                    decimal strlbl2Two = 0;
                    decimal strlbl2Three = 0;
                    int counter = 1;

                    /* for sure there are only 2 rows - Red */
                    foreach (DataRow dtr in dtTimesheetTotals.Rows)
                    {
                        if (counter == 1)
                        {
                            strlbl1One = dtr["Label"].ToString();
                            strlbl1Two = decimal.Parse(dtr["Invoice"].ToString());
                            strlbl1Three = decimal.Parse(dtr["Payment"].ToString());
                        }
                        else
                        {
                            strlbl2One = dtr["Label"].ToString();
                            strlbl2Two = decimal.Parse(dtr["Invoice"].ToString());
                            strlbl2Three = decimal.Parse(dtr["Payment"].ToString());
                        }
                        counter += 1;
                    }

                    if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Init")
                    {
                        HtmlControl theInteCreditTabList = (HtmlControl)theListControl.FindControl("CustomDivFooterRow");
                        theInteCreditTabList.Attributes.Add("style", "float:right; padding-right:50px");

                        HtmlTable tblCustom = new HtmlTable();
                        HtmlTableRow htr0 = new HtmlTableRow();
                        htr0.ID = theService.Temp_DynamicPartName + "hrt0" + theService.TableChildID.ToString();
                        HtmlTableRow htr1 = new HtmlTableRow();
                        htr1.ID = theService.Temp_DynamicPartName + "hrt1" + theService.TableChildID.ToString();
                        HtmlTableRow htr2 = new HtmlTableRow();
                        htr2.ID = theService.Temp_DynamicPartName + "hrt2" + theService.TableChildID.ToString();
                        HtmlTableCell cell11 = new HtmlTableCell();
                        cell11.Attributes.Add("style", "text-align:right;");
                        HtmlTableCell cell12 = new HtmlTableCell();
                        cell12.Attributes.Add("style", "width:20px;");
                        HtmlTableCell cell13 = new HtmlTableCell();
                        cell13.Attributes.Add("style", "text-align:right;");
                        HtmlTableCell cell14 = new HtmlTableCell();
                        cell14.Attributes.Add("style", "text-align:right;");
                        HtmlTableCell cell21 = new HtmlTableCell();
                        cell21.Attributes.Add("style", "text-align:right;");
                        HtmlTableCell cell22 = new HtmlTableCell();
                        cell22.Attributes.Add("style", "width:20px;");
                        HtmlTableCell cell23 = new HtmlTableCell();
                        cell23.Attributes.Add("style", "text-align:right;");
                        HtmlTableCell cell24 = new HtmlTableCell();
                        cell24.Attributes.Add("style", "text-align:right;");
                        HtmlTableCell cellspacerOne = new HtmlTableCell();
                        cellspacerOne.Attributes.Add("style", "width:80px;");
                        HtmlTableCell cellspacerTwo = new HtmlTableCell();
                        cellspacerTwo.Attributes.Add("style", "width:80px;");

                        HtmlTableCell cellHeaerOne = new HtmlTableCell();
                        HtmlTableCell cellHeaderTwo = new HtmlTableCell();
                        cellHeaderTwo.Attributes.Add("style", "width:20px;");
                        HtmlTableCell cellHeaerThree = new HtmlTableCell();
                        cellHeaerThree.Attributes.Add("style", "border-bottom-style:groove; text-align:center; font-weight:bold; font-size:11px !important;");
                        cellHeaerThree.InnerText = "Invoice";
                        HtmlTableCell cellHeaderFour = new HtmlTableCell();
                        cellHeaderFour.Attributes.Add("style", "border-bottom-style:groove; text-align:center; font-weight:bold; font-size:11px !important;");
                        cellHeaderFour.InnerText = "Payment";
                        HtmlTableCell cellHeaderspacerOne = new HtmlTableCell();
                        cellHeaderspacerOne.Attributes.Add("style", "width:80px;");

                        //First Row
                        Label lbl1One = new Label();
                        lbl1One.ID = theService.Temp_DynamicPartName + "lbl1One" + theService.TableChildID.ToString();
                        lbl1One.ClientIDMode = ClientIDMode.Static;
                        lbl1One.Text = strlbl1One + ": ";
                        //lbl1One.Font.Bold = true;
                        lbl1One.Attributes.Add("style", "font-weight:bold; font-size:11px !important;");

                        cell11.Controls.Add(lbl1One);

                        Label lbl1Two = new Label();
                        lbl1Two.ID = theService.Temp_DynamicPartName + "lbl1Two" + theService.TableChildID.ToString();
                        lbl1Two.ClientIDMode = ClientIDMode.Static;
                        lbl1Two.Text = String.Format("{0:0,0.00}", strlbl1Two);
                        //lbl1Two.Font.Bold = true;
                        lbl1Two.Attributes.Add("style", "font-weight:bold; font-size:11px !important;");

                        cell13.Controls.Add(lbl1Two);

                        Label lbl1Three = new Label();
                        lbl1Three.ID = theService.Temp_DynamicPartName + "lbl1Three" + theService.TableChildID.ToString();
                        lbl1Three.ClientIDMode = ClientIDMode.Static;
                        lbl1Three.Text = String.Format("{0:0,0.00}", strlbl1Three);
                        //lbl1Three.Font.Bold = true;
                        lbl1Three.Attributes.Add("style", "font-weight:bold; font-size:11px !important;");

                        cell14.Controls.Add(lbl1Three);

                        //Second Row
                        Label lbl2One = new Label();
                        lbl2One.ID = theService.Temp_DynamicPartName + "lbl2One" + theService.TableChildID.ToString();
                        lbl2One.ClientIDMode = ClientIDMode.Static;
                        lbl2One.Text = strlbl2One + ": ";
                        //lbl2One.Font.Bold = true;
                        lbl2One.Attributes.Add("style", "font-weight:bold; font-size:11px !important;");

                        cell21.Controls.Add(lbl2One);

                        Label lbl2Two = new Label();
                        lbl2Two.ID = theService.Temp_DynamicPartName + "lbl2Two" + theService.TableChildID.ToString();
                        lbl2Two.ClientIDMode = ClientIDMode.Static;
                        lbl2Two.Text = String.Format("{0:0,0.00}", strlbl2Two);
                        //lbl2Two.Font.Bold = true;
                        lbl2Two.Attributes.Add("style", "font-weight:bold; font-size:11px !important;");

                        cell23.Controls.Add(lbl2Two);

                        Label lbl2Three = new Label();
                        lbl2Three.ID = theService.Temp_DynamicPartName + "lbl2Three" + theService.TableChildID.ToString();
                        lbl2Three.ClientIDMode = ClientIDMode.Static;
                        lbl2Three.Text = String.Format("{0:0,0.00}", strlbl2Three);
                        //lbl2Three.Font.Bold = true;
                        lbl2Three.Attributes.Add("style", "font-weight:bold; font-size:11px !important;");

                        cell24.Controls.Add(lbl2Three);

                        htr0.Cells.Add(cellHeaerOne);
                        htr0.Cells.Add(cellHeaderTwo);
                        htr0.Cells.Add(cellHeaerThree);
                        htr0.Cells.Add(cellHeaderspacerOne);
                        htr0.Cells.Add(cellHeaderFour);
                        htr1.Cells.Add(cell11);
                        htr1.Cells.Add(cell12);
                        htr1.Cells.Add(cell13);
                        htr1.Cells.Add(cellspacerOne);
                        htr1.Cells.Add(cell14);
                        htr2.Cells.Add(cell21);
                        htr2.Cells.Add(cell22);
                        htr2.Cells.Add(cell23);
                        htr2.Cells.Add(cellspacerTwo);
                        htr2.Cells.Add(cell24);
                        tblCustom.Rows.Add(htr0);
                        tblCustom.Rows.Add(htr1);
                        tblCustom.Rows.Add(htr2);
                        theInteCreditTabList.Controls.Add(tblCustom);
                        return roList;
                    }
                }
            }
        }

        return null;
    }

    /* GST and Total
    * - bottom totals in Job Information > Timesheet Details 
    * - borrm totals in BAPA > Billed Details (Add or Edit BAPA invoice) 
    * - bottom totals in Credit Details tab ie View BAPA Credit Entry */
    public static List<object> windtech_timesheet_details_custom_elements(List<object> objList)
    {
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_recordlist_ascx")
            {
                System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;
                Panel pnlFullListControl = (Panel)theListControl.FindControl("pnlFullListControl");

                HiddenField hfParentRecordIDList = (HiddenField)pnlFullListControl.FindControl("hfParentRecordIDList");

                DataTable dtTimesheetTotals = null;
                if (hfParentRecordIDList == null)
                {
                    return null;
                }
                else if (hfParentRecordIDList.Value == "-1")
                {
                    return null;
                }

                /* We rely on the parent recordids:
                   * Job info: Timesheet Details
                   * Edit BAPA Invoice: Billed Details
                   * Add BAPA Invoice: Billed Details
                   * Edit BAPA Credit Entry: Credit Details
                  */

                dtTimesheetTotals = Common.DataTableFromText(thePageLifeCycleService.SPName + " " + hfParentRecordIDList.Value);

                if (dtTimesheetTotals != null && dtTimesheetTotals.Rows.Count > 0)
                {

                    string strlbl1One = "";
                    decimal strlbl1Two = 0;
                    string strlbl2One = "";
                    decimal strlbl2Two = 0;
                    int counter = 1;

                    /* for sure there are only 2 rows - Red */
                    foreach (DataRow dtr in dtTimesheetTotals.Rows)
                    {
                        if (counter == 1)
                        {
                            strlbl1One = dtr["Label"].ToString();
                            strlbl1Two = decimal.Parse(dtr["Amount"].ToString());
                        }
                        else
                        {
                            strlbl2One = dtr["Label"].ToString();
                            strlbl2Two = decimal.Parse(dtr["Amount"].ToString());
                        }
                        counter += 1;
                    }

                    HtmlControl theInteCreditTabList = (HtmlControl)theListControl.FindControl("CustomDivFooterRow");
                    theInteCreditTabList.Attributes.Add("style", "float:right; padding-right:50px");

                    HtmlTable tblCustom = new HtmlTable();
                    HtmlTableRow htr1 = new HtmlTableRow();
                    htr1.ID = theService.Temp_DynamicPartName + "hrt1" + theService.TableChildID.ToString();
                    HtmlTableRow htr2 = new HtmlTableRow();
                    htr2.ID = theService.Temp_DynamicPartName + "hrt2" + theService.TableChildID.ToString();
                    HtmlTableCell cell11 = new HtmlTableCell();
                    cell11.Attributes.Add("style", "text-align:right;");
                    HtmlTableCell cell12 = new HtmlTableCell();
                    cell12.Attributes.Add("style", "width:20px;");
                    HtmlTableCell cell13 = new HtmlTableCell();
                    cell13.Attributes.Add("style", "text-align:right;");
                    HtmlTableCell cell14 = new HtmlTableCell();
                    cell14.Attributes.Add("style", "text-align:right;");
                    HtmlTableCell cell21 = new HtmlTableCell();
                    cell21.Attributes.Add("style", "text-align:right;");
                    HtmlTableCell cell22 = new HtmlTableCell();
                    cell22.Attributes.Add("style", "width:20px;");
                    HtmlTableCell cell23 = new HtmlTableCell();
                    cell23.Attributes.Add("style", "text-align:right;");
                    HtmlTableCell cell24 = new HtmlTableCell();
                    cell24.Attributes.Add("style", "text-align:right;");

                    //First Row
                    Label lbl1One = new Label();
                    lbl1One.ID = theService.Temp_DynamicPartName + "lbl1One" + theService.TableChildID.ToString();
                    lbl1One.ClientIDMode = ClientIDMode.Static;
                    lbl1One.Text = strlbl1One + ": ";
                    //lbl1One.Font.Bold = true;
                    lbl1One.Attributes.Add("style", "font-weight:bold; font-size:11px !important;");

                    cell11.Controls.Add(lbl1One);

                    Label lbl1Two = new Label();
                    lbl1Two.ID = theService.Temp_DynamicPartName + "lbl1Two" + theService.TableChildID.ToString();
                    lbl1Two.ClientIDMode = ClientIDMode.Static;
                    lbl1Two.Text = String.Format("{0:0,0.00}", strlbl1Two);
                    lbl1Two.Attributes.Add("style", "font-weight:bold; font-size:11px !important;");

                    cell14.Controls.Add(lbl1Two);

                    //Second Row
                    Label lbl2One = new Label();
                    lbl2One.ID = theService.Temp_DynamicPartName + "lbl2One" + theService.TableChildID.ToString();
                    lbl2One.ClientIDMode = ClientIDMode.Static;
                    lbl2One.Text = strlbl2One + ": ";
                    //lbl2One.Font.Bold = true;
                    lbl2One.Attributes.Add("style", "font-weight:bold; font-size:11px !important;");

                    cell21.Controls.Add(lbl2One);

                    Label lbl2Two = new Label();
                    lbl2Two.ID = theService.Temp_DynamicPartName + "lbl2Two" + theService.TableChildID.ToString();
                    lbl2Two.ClientIDMode = ClientIDMode.Static;
                    lbl2Two.Text = String.Format("{0:0,0.00}", strlbl2Two); ;
                    //lbl2Two.Font.Bold = true;
                    lbl2Two.Attributes.Add("style", "font-weight:bold; font-size:11px !important;");

                    cell24.Controls.Add(lbl2Two);

                    htr1.Cells.Add(cell11);
                    htr1.Cells.Add(cell12);
                    htr1.Cells.Add(cell13);
                    htr1.Cells.Add(cell14);
                    htr2.Cells.Add(cell21);
                    htr2.Cells.Add(cell22);
                    htr2.Cells.Add(cell23);
                    htr2.Cells.Add(cell24);
                    tblCustom.Rows.Add(htr1);
                    tblCustom.Rows.Add(htr2);
                    theInteCreditTabList.Controls.Add(tblCustom);
                    return roList;
                }
            }
        }

        return null;
    }

    /* Interim Invoice Bottom totals
    * Update status Allocated
    * when save new Invoice - Red */
    public static List<object> windtech_divfooter_2711(List<object> objList)
    {

        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                //  return null;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);
        //List<object> roList = new List<object>();

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_recordlist_ascx")
            {
                try
                {
                    //  Panel pnlFullDetailPage = (Panel)obj;
                    //  UpdatePanel upDetailDynamic = null;
                    System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;
                    Panel pnlFullListControl = (Panel)theListControl.FindControl("pnlFullListControl");
                    GridView gvTheGrid = (GridView)pnlFullListControl.FindControl("gvTheGrid");

                    foreach (GridViewRow row in gvTheGrid.Rows)
                    {                                               
                        CheckBox cbgvTheGrid = (CheckBox)row.FindControl("chkDelete");
                        string js = string.Format("javascript:gvTheGrid_OnRowChecked();");

                        cbgvTheGrid.Attributes["onclick"] = js;

                    }



                    string JS = @" function gvTheGrid_OnRowChecked()
                            {
     
                                var totalinvoiceamount = 0;
                                var gridView = document.getElementById('" + gvTheGrid.ClientID + @"');
                                var rows = gridView.rows;
                                for(var index=1;index<rows.length;index++) {
                                    var invoiceamount = rows[index].cells[4];        
                                    var checkBoxx = rows[index].getElementsByTagName('input')         
                                    for (var i = 0; i < checkBoxx.length; i++) {                
                                        if (checkBoxx != null) {
                                            if (checkBoxx[i].type == ""checkbox"") {
                                                if (checkBoxx[i].checked) {
                                                    totalinvoiceamount += parseFloat(invoiceamount.innerHTML.replace(/[^\d.]/g, ''));
                                                } 
                                            }
                                        }
                                    }
                                } 
                                document.getElementById('" + theService.Temp_DynamicPartName + "lbl1Two" + theService.TableChildID.ToString() + @"').innerHTML = totalinvoiceamount.toFixed(2);//alert(sum);
                                var InvoiceAmount = parseFloat($('#ctl00_HomeContentPlaceHolder_txtV005').val()) ? parseFloat($('#ctl00_HomeContentPlaceHolder_txtV005').val()) : 0;

                                $('#ctl00_HomeContentPlaceHolder_txtV059').val((InvoiceAmount + parseFloat(totalinvoiceamount)).toFixed(2));
                            }";
                    ScriptManager.RegisterStartupScript(pnlFullListControl, pnlFullListControl.GetType(), "TotalInvoiceJS", JS, true);

                    if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_PreRender")
                    {
                        foreach (GridViewRow row in gvTheGrid.Rows)
                        {
                            // Get UserID to use in [Record]
                            User _ObjUser = (User)System.Web.HttpContext.Current.Session["User"];
                            _ObjUser.UserID.ToString();
                            // Get the max RecrodID of the current user doing the transaction, with user so not to get other's Record
                            string iMaxRecordID = Common.GetValueFromSQL("SELECT MAX(RecordID) FROM [Record] WHERE TableID = 2711 AND [Enteredby]=" + _ObjUser.UserID.ToString());

                            CheckBox cbgvTheGridchecked = (CheckBox)row.FindControl("chkDelete");
                            if (cbgvTheGridchecked.Checked == true)
                            {

                                Label lblRecordID = (Label)row.FindControl("LblID");
                                int iRecordID = int.Parse(lblRecordID.Text);
                                Record theRecord = RecordManager.ets_Record_Detail_Full(iRecordID, null, false);
                                theRecord.V036 = "Yes"; // Field Unallocated?
                                theRecord.V061 = iMaxRecordID; // Field Interim Invoice, for parent and child linking
                                RecordManager.ets_Record_Update(theRecord, null);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, " Bills and Payment Activity", ex.Message, ex.StackTrace, DateTime.Now, "CustemMethod_windtech");
                    SystemData.ErrorLog_Insert(theErrorLog);
                }
            }
        }
        return roList;
    }

    public static List<object> windtech_interim_credit(List<object> objList)
    {
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_recordlist_ascx")
            {
                try
                {
                    System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;
                    Panel pnlFullListControl = (Panel)theListControl.FindControl("pnlFullListControl");
                    GridView gvTheGrid = (GridView)pnlFullListControl.FindControl("gvTheGrid");

                    foreach (GridViewRow row in gvTheGrid.Rows)
                    {
                        CheckBox cbgvTheGrid = (CheckBox)row.FindControl("chkDelete");
                        string js = string.Format("javascript:interimcreditGVTheGrid_OnRowChecked();");

                        cbgvTheGrid.Attributes["onclick"] = js;

                    }

                    string JS = @" 
                        function interimcreditGVTheGrid_OnRowChecked()
                            {
                                var interimcreditTotalCreditAmount = 0;
                                var gridView = document.getElementById('" + gvTheGrid.ClientID + @"');
                                var rows = gridView.rows;
                                for(var index=1;index<rows.length;index++) {
                                    var interimcreditAmount = rows[index].cells[4];        
                                    var checkBoxx = rows[index].getElementsByTagName('input')         
                                    for (var i = 0; i < checkBoxx.length; i++) {                
                                        if (checkBoxx != null) {
                                            if (checkBoxx[i].type == ""checkbox"") {
                                                if (checkBoxx[i].checked) {
                                                    interimcreditTotalCreditAmount += parseFloat(interimcreditAmount.innerHTML.replace(/[^\d.]/g, ''));
                                                } 
                                            }
                                        }
                                    }
                                } 
                                    document.getElementById('" + theService.Temp_DynamicPartName + "lbl1Two" + theService.TableChildID.ToString() + @"').innerHTML = interimcreditTotalCreditAmount.toFixed(2);//alert(sum);
                                    //alert(interimcreditTotalCreditAmount);
                                    var currentCreditAmount = parseFloat($('#ctl00_HomeContentPlaceHolder_txtV045').val()) ? parseFloat($('#ctl00_HomeContentPlaceHolder_txtV045').val()) : 0;

                                $('#ctl00_HomeContentPlaceHolder_txtV044').val((currentCreditAmount + parseFloat(interimcreditTotalCreditAmount)).toFixed(2));

                            }

                        ";
                    ScriptManager.RegisterStartupScript(pnlFullListControl, pnlFullListControl.GetType(), "TotalCreditAmountJS", JS, true);

                    if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_PreRender")
                    {
                        foreach (GridViewRow row in gvTheGrid.Rows)
                        {
                            // Get UserID to use in [Record]
                            User _ObjUser = (User)System.Web.HttpContext.Current.Session["User"];
                            _ObjUser.UserID.ToString();
                            // Get the max RecrodID of the current user doing the transaction, with user so not to get other's Record
                            string iMaxRecordID = Common.GetValueFromSQL("SELECT MAX(RecordID) FROM [Record] WHERE TableID = 2711 AND [Enteredby]=" + _ObjUser.UserID.ToString());

                            CheckBox cbgvTheGridchecked = (CheckBox)row.FindControl("chkDelete");
                            if (cbgvTheGridchecked.Checked == true)
                            {
                                Label lblRecordID = (Label)row.FindControl("LblID");
                                int iRecordID = int.Parse(lblRecordID.Text);
                                Record theRecord = RecordManager.ets_Record_Detail_Full(iRecordID, null, false);
                                theRecord.V036 = "Yes"; // Field Unallocated?
                                theRecord.V077 = iMaxRecordID; // Field Interim Credit, for parent and child linking
                                RecordManager.ets_Record_Update(theRecord, null);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "Interim Credit - Bills and Payment Activity", ex.Message, ex.StackTrace, DateTime.Now, "CustemMethod_windtech");
                    SystemData.ErrorLog_Insert(theErrorLog);

                }

            }
        }

        return roList;
    }

    //end windtech_AddLinkContact
    public static List<object> windtech_addlink_contact(List<object> objList)
    {
        List<object> roList = new List<object>();
        string strDynamicpart = "";
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                strDynamicpart = theService.Temp_DynamicPartName;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON); // be using this probably later    

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                try
                {

                    Panel pnlFullDetailPage = (Panel)obj;

                    //get parent recordid
                    HiddenField hfparentRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfParentRecordID");

                    //Add query string to add link
                    HyperLink hlAddlink = (HyperLink)pnlFullDetailPage.FindControl(strDynamicpart + "hlV008"); // get the link
                    hlAddlink.NavigateUrl = hlAddlink.NavigateUrl + "&parentRecordid=" + hfparentRecordID.Value;


                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, " Billing Address Addlink", ex.Message, ex.StackTrace, DateTime.Now, "CustemMethod_windtech");
                    SystemData.ErrorLog_Insert(theErrorLog);

                }

            }
        }

        return roList;
    } 

    //Reinstates Timesheet Detail and Interim Invoice when Invoice deleted
    public static List<object> windtech_delete_bapa(List<object> objList)
    {

        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                //  return null;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_recordlist_ascx")
            {
                try
                {
                   

                    System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;
                    Panel pnlFullListControl = (Panel)theListControl.FindControl("pnlFullListControl");
                    GridView gvTheGrid = (GridView)pnlFullListControl.FindControl("gvTheGrid");
                    string check = System.Web.HttpContext.Current.Request.Url.Host;

                    foreach (GridViewRow row in gvTheGrid.Rows)
                    {
                        CheckBox cbgvTheGrid = (CheckBox)row.FindControl("chkDelete");
                        string js = string.Format("javascript:gvTheGrid_OnRowChecked('"+ cbgvTheGrid.ClientID + "');");

                        cbgvTheGrid.Attributes["onclick"] = js;
                        

                    }

                    if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                    {
                        HiddenField hfReasonDelete = (HiddenField)theListControl.FindControl("hftxtDeleteReason");
                        if (hfReasonDelete != null)
                        {
                            if (!string.IsNullOrEmpty(hfReasonDelete.Value.ToString()))
                            {

                                foreach (GridViewRow row in gvTheGrid.Rows)
                                {
                                    CheckBox cbgvTheGridchecked = (CheckBox)row.FindControl("chkDelete");

                                    if (cbgvTheGridchecked.Checked == true)
                                    {

                                        Label lblRecordID = (Label)row.FindControl("LblID");
                                        int iRecordID = int.Parse(lblRecordID.Text);

                                        string strActivity = Common.GetValueFromSQL("SELECT V029 FROM [Record] WHERE [RecordID]=" + iRecordID);
                                        string strAllocatedStatus = Common.GetValueFromSQL("SELECT V036 FROM [Record] WHERE [RecordID]=" + iRecordID);
                                        string taxRate = Common.GetValueFromSQL("SELECT (V011 / 100.00) " +
                                                    "FROM [Record] WHERE RecordID = " +
                                                    "(SELECT [V002] FROM [Record] WHERE RecordID=" + iRecordID + ")");
                                        
                                        if (strActivity == "Invoice")
                                        {
                                             //Update Tax
                                            Common.ExecuteText("UPDATE [Timesheet Details] " +
                                                "SET V012 = CAST(CAST(REPLACE([Timesheet Details].V007, ',', '')  AS DEC(20, 4)) * " + taxRate + " AS VARCHAR(MAX) ) " +
                                                "FROM [Record] [Timesheet Details] WHERE [Timesheet Details].TableID = '2729' " +
                                                "AND [Timesheet Details].IsActive = 1 " +
                                                "AND ISNUMERIC(ISNULL( [Timesheet Details].V007, 0 ) ) = 1 " +
                                                "AND [Timesheet Details].V016 = CAST(" + iRecordID.ToString() + " AS VARCHAR(MAX)); ");

                                            Common.ExecuteText("UPDATE [Record] SET V016 = NULL WHERE [TableID] = 2729 AND V016 =" + iRecordID); //Timesheet Invoice Number
                                            Common.ExecuteText("UPDATE [Record] SET V036 = 'No', V061 = NULL WHERE [TableID] = 2711 AND V061 =" + iRecordID); //Timesheet Invoice Number
                                        }
                                       else if (strActivity == "Interim Invoice" && strAllocatedStatus == "Yes")
                                        {
                                            string strAmountToDeduct = Common.GetValueFromSQL("SELECT V005 FROM [Record] WHERE [RecordID]=" + iRecordID); //Interim Invoice Amount
                                            string strAssociatedInvoiceTotalAmount = Common.GetValueFromSQL("SELECT V059 FROM [Record] WHERE [RecordID]= (SELECT V061 FROM [Record] WHERE [RecordID]=" + iRecordID + ")"); //Associated Invoice Total Amount                                       
                                            decimal dDeductedAmount = decimal.Parse(strAssociatedInvoiceTotalAmount) - decimal.Parse(strAmountToDeduct); // Get the correct balance after delete
                                            Common.ExecuteText("UPDATE [Record] SET V059=" + dDeductedAmount.ToString() + " WHERE [RecordID]= (SELECT V061 FROM [Record] WHERE [RecordID]=" + iRecordID + ")"); // Force update balance
                                        }
                                        //reinstate interim credit
                                        else if (strActivity == "Credit Entry")
                                        {
                                            Common.ExecuteText("UPDATE [Record] SET V077 = NULL, " +
                                                               " V036 = 'No' " +
                                                               " WHERE IsActive = 1 " +
                                                               " AND [TableID] = 2711 " +
                                                               " AND V077 = CAST(" + iRecordID.ToString() + " AS VARCHAR(MAX)); ");
                                        }
                                    }
                                }
                                ScriptManager.RegisterStartupScript(pnlFullListControl, pnlFullListControl.GetType(), "ReloadPage", "location.reload();", true);
                            }
                        }
                    }
                   
                 string delJSFunction = @" 
                                        var billsAndPaymentActivity = '';
                                        var billsAndPaymentStatus = '';
                                        var associatedInvoiceNo;
                                        var associatedCreditNo;
                                        var canDeleteCreditEntry;
                                        var canDeleteInvoice;
                                        var selectedRow;

                                        function gvTheGrid_OnRowChecked(chkID) {
                                            var gridView = document.getElementById('ctl00_HomeContentPlaceHolder_ctl00_HomeContentPlaceHolder_ctList1_gvTheGrid');
                                            var rows = gridView.rows;

                                            for (var index = 2; index < rows.length; index++) {
                                                if (index < 10) { index = '0' + index }
                                                if ('ctl00_HomeContentPlaceHolder_ctl00_HomeContentPlaceHolder_ctList1_gvTheGrid_ctl' + index + '_chkDelete' != chkID) {
                                                    $('#ctl00_HomeContentPlaceHolder_ctl00_HomeContentPlaceHolder_ctList1_gvTheGrid_ctl' + index + '_chkDelete').prop('checked', false);
                                                }
                                                $('#ctl00_HomeContentPlaceHolder_ctl00_HomeContentPlaceHolder_ctList1_gvTheGrid_ctl02_chkAll').prop('checked', false);

                                            }

                                            if ($('#' + chkID + '').is(':checked')) {
                                                for (var index = 1; index < rows.length; index++) {
                                                    var recordId = rows[index].cells[2];
                                                    var bapaStatus = rows[index].cells[4];
                                                    var checkBoxx = rows[index].getElementsByTagName('input')
                                                    for (var i = 0; i < checkBoxx.length; i++) {
                                                        if (checkBoxx != null) {
                                                            if (checkBoxx[0].type == 'checkbox') {
                                                                if (checkBoxx[0].checked) {
                                                                    selectedRow = true;
                                                                    billsAndPaymentStatus = bapaStatus.innerHTML;

                                                                    var data;

                                                                    var url = '../DocGen/REST/Custom/Windtech/Actions/Request.ashx?RequestName=ReadSP&spName=Renzo_GetRecordToDelete&RecordID=' + recordId.innerHTML;
                                                                    $.getJSON(url, function (response) {
                                                                        data = response.data;

                                                                        billsAndPaymentActivity = data[0].V001;
                                                                        associatedInvoiceNo = data[0].V002;
                                                                        associatedCreditNo = data[0].V003;
                                                                        canDeleteCreditEntry = data[0].V004;
                                                                        canDeleteInvoice = data[0].V005;

                                                                    });
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                            else {
                                                selectedRow = false;
                                            }
                                        }

                                        $('#ctl00_HomeContentPlaceHolder_ctl00_HomeContentPlaceHolder_ctList1_gvTheGrid_ctl01_Pager_ImgDelete').click(function () {
                                            if (selectedRow) {
                                                if (billsAndPaymentActivity == 'Invoice') {
                                                    if (canDeleteInvoice == '0') {
                                                        alert('You cannot delete this entry because there is a Credit/Bad Debt/ \nDebt Recovery entry which is more recent than this entry. \nDelete that entry first.');
                                                        parent.$.fancybox.close(); return false;
                                                    }
                                                    else {
                                                        var delAction = confirm('Warning: \n\nDeleting this invoice will make the associated Timesheet Details re-appear \nand may affect debtors reconciliation. Are you sure you want to continue?');
                                                        if (delAction != true) {
                                                            parent.$.fancybox.close(); return false;
                                                        }
                                                    }

                                                }
                                                else if (billsAndPaymentActivity == 'Interim Invoice' && billsAndPaymentStatus == 'No') {
                                                    var delAction = confirm('Are you sure you want to delete this Interim Invoice entry?');
                                                    if (delAction != true) {

                                                        parent.$.fancybox.close(); return false;
                                                    }
                                                }
                                                else if (billsAndPaymentActivity == 'Interim Invoice' && billsAndPaymentStatus == 'Yes') {

                                                    alert('You cannot delete this Interim Invoice because it is associated with Invoice: ' + associatedInvoiceNo);
                                                    parent.$.fancybox.close(); return false;
                                                }

                                                else if (billsAndPaymentActivity == 'Credit Entry') {
                                                    if (canDeleteCreditEntry == '0') {
                                                        alert('You cannot delete this entry because there is a Credit/Bad Debt/ \nDebt Recovery entry which is more recent than this entry. \nDelete that entry first.');
                                                        parent.$.fancybox.close(); return false;
                                                    }
                                                    else {
                                                        var delAction = confirm('Are you sure want to delete this Credit Entry?');
                                                        if (delAction != true) {
                                                            parent.$.fancybox.close(); return false;
                                                        }
                                                    }
                                                }
                                                else if (billsAndPaymentActivity == 'Interim Credit' && billsAndPaymentStatus == 'Yes') {
                                                    alert('You cannot delete this Interim Credit because it is associated with credit: ' + associatedCreditNo);
                                                    parent.$.fancybox.close(); return false;
                                                }
                                            }
                                        });
         
                                    ";

                    ScriptManager.RegisterStartupScript(pnlFullListControl, pnlFullListControl.GetType(), "delJSFunction", delJSFunction, true);

                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "Job Info - Bills and Payment Activity Tab", ex.Message, ex.StackTrace, DateTime.Now, "CustemMethod_windtech");
                    SystemData.ErrorLog_Insert(theErrorLog);

                }

            }

        }

        return roList;
    }

    public static List<object> windtech_process_unallocated_payments(List<object> objList)

    {

        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                //  return null;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_usercontrol_recordlist_ascx")
            {
                try
                {


                    System.Web.UI.UserControl theListControl = (System.Web.UI.UserControl)obj;
                    Panel pnlFullListControl = (Panel)theListControl.FindControl("pnlFullListControl");
                    GridView gvTheGrid = (GridView)pnlFullListControl.FindControl("gvTheGrid");
                    string check = System.Web.HttpContext.Current.Request.Url.Host;

                    foreach (GridViewRow row in gvTheGrid.Rows)
                    {
                        CheckBox cbgvTheGrid = (CheckBox)row.FindControl("chkDelete");
                        string js = string.Format("javascript:mupGV_OnRowChecked('" + cbgvTheGrid.ClientID + "');");

                        cbgvTheGrid.Attributes["onclick"] = js;


                    }


                    string delJSFunction = @"  
                                function mupGV_OnRowChecked(chkID) {

                                    var chkSelectedMUP = document.getElementById(chkID);  
                                    if (chkSelectedMUP != null)
                                    {
                                        if (chkSelectedMUP.checked == true) {
                                            $('#ctl00_HomeContentPlaceHolder_lnkHeading').click();            
                                        }
                                    }

                                    var gridView = document.getElementById('ctl00_HomeContentPlaceHolder_ctl00_HomeContentPlaceHolder_ctList7_gvTheGrid');
                                    var rows = gridView.rows;

                                    //force to select only one record
                                    for (var index = 2; index < rows.length; index++) {
                                        if (index < 10) { index = '0' + index }
                                 
                                        if ('ctl00_HomeContentPlaceHolder_ctl00_HomeContentPlaceHolder_ctList7_gvTheGrid_ctl' + index + '_chkDelete' != chkID){
                                            $('#ctl00_HomeContentPlaceHolder_ctl00_HomeContentPlaceHolder_ctList7_gvTheGrid_ctl' + index + '_chkDelete').prop('checked', false);
                                        }
                                        $('#ctl00_HomeContentPlaceHolder_ctl00_HomeContentPlaceHolder_ctList7_gvTheGrid_ctl02_chkAll').prop('checked', false);
                                      
                                    }

                                    if ($('#'+ chkID +'').is(':checked')) {
                                        $('#ctl00_HomeContentPlaceHolder_trXV075').hide(); // button add mup
                                        $('#ctl00_HomeContentPlaceHolder_trXV074').show(); // button upate mup
                                        $('#ctl00_HomeContentPlaceHolder_trXV072').show(); // button allocate mup


                                        for(var index=1; index < rows.length;index++) {
                                            var mupDate = 0;
                                            mupDate = rows[index].cells[1];  
                                            var mupAmount = 0;
                                            mupAmount = rows[index].cells[3];
                             
                                            var mupDescription = rows[index].cells[2];
                                            var checkBoxx = rows[index].getElementsByTagName('input')      
                                            for (var i = 0; i < checkBoxx.length; i++) {      
                                                if (checkBoxx != null) {
                                                    if (checkBoxx[0].type == 'checkbox') {
                                                        if (checkBoxx[0].checked) {
                                                            $('#ctl00_HomeContentPlaceHolder_txtV004').val(mupDate.innerHTML);
                                                            $('#ctl00_HomeContentPlaceHolder_txtV007').val(parseFloat(mupAmount.innerHTML.replace(/[^\d.]/g, '')));
                                                            $('#ctl00_HomeContentPlaceHolder_txtV015').val(mupDescription.innerHTML.replace(/\n|<br>|/g,''));
                                                        }
                                                    }
                                                }
                                            }
                                        }        
                                    }
                                    else {
                                        $('#ctl00_HomeContentPlaceHolder_trXV075').show(); // button add mup
                                        $('#ctl00_HomeContentPlaceHolder_trXV074').hide(); // button update mup
                                        $('#ctl00_HomeContentPlaceHolder_trXV072').hide(); // button allocate mup


                                        var currentDate =  (new Date()).toLocaleDateString('en-GB');

                                        $('#ctl00_HomeContentPlaceHolder_txtV004').val(currentDate);
                                        $('#ctl00_HomeContentPlaceHolder_txtV007').val('');
                                        $('#ctl00_HomeContentPlaceHolder_txtV015').val('');
                                    }
                                 }                    
                      
                                                
                                    ";

                    ScriptManager.RegisterStartupScript(pnlFullListControl, pnlFullListControl.GetType(), "delJSFunction", delJSFunction, true);

                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "MUP - Bills and Payment Activity Tab", ex.Message, ex.StackTrace, DateTime.Now, "CustemMethod_windtech");
                    SystemData.ErrorLog_Insert(theErrorLog);

                }

            }

        }

        return roList;
    }

    public static List<object> windtech_view_billing_calculation(List<object> objList)
    {
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {

                Panel pnlFullDetailPage = (Panel)obj;
                HiddenField hfParentRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfParentRecordID");

                string strRecordID = "-1";
                if (hfParentRecordID != null)
                {
                    if (hfParentRecordID.Value != "")
                    {
                        strRecordID = hfParentRecordID.Value;
                    }
                }

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Init")
                {
                    //lets get the systemname to use in the hyperlink view billing calculations, and
                    //hide them
                    try
                    {
                        DataTable dtViewBillingCalculationColumns = Common.DataTableFromText("SELECT SystemName,ButtonInfo FROM [Column] WHERE TableID = " + theService.TableID + " AND DisplayTextDetail = 'View Billing Calculation'");

                        foreach (DataRow dr in dtViewBillingCalculationColumns.Rows)
                        {
                            ColumnButtonInfo theButtonInfo = JSONField.GetTypedObject<ColumnButtonInfo>(dr["ButtonInfo"].ToString());
                            theButtonInfo = RecordManager.UpdateColumnButtonInfo(theButtonInfo);
                            //lets get the sp to use to process and get the data
                            string strSPName = "Renzo_GetBilling";
                            if (!string.IsNullOrEmpty(theButtonInfo.SPToRun))
                            {
                                strSPName = theButtonInfo.SPToRun;
                            }

                            //change this to link instead of btn type of field
                            HtmlTableRow theBillingRow = (HtmlTableRow)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "trX" + dr["SystemName"]);
                            LinkButton lnkV037 = (LinkButton)pnlFullDetailPage.FindControl(theService.Temp_DynamicPartName + "lnk" + dr["SystemName"]);
                            lnkV037.Style.Add("display", "none");

                            //lets create a dynamic hyperlink
                            HyperLink hlCustomAdd = new HyperLink();
                            hlCustomAdd.ID = theService.Temp_DynamicPartName + "hlCustomAdd" + dr["SystemName"] + "_srvceId_" + theService.ServiceID.ToString();
                            hlCustomAdd.ClientIDMode = ClientIDMode.Static;
                            hlCustomAdd.CssClass = "popup_datatable_class";
                            hlCustomAdd.Text = "View Billing Calculation";

                            //lets set the parameters to be used in the common custome datatable
                            string strXML = @"<root>" +
                                                " <TopTitle>" + HttpUtility.HtmlEncode("View Billing Calculation") + "</TopTitle>" +
                                                " <NoOfColumns>" + HttpUtility.HtmlEncode("2") + "</NoOfColumns>" +
                                                " <alignCell0>" + HttpUtility.HtmlEncode("Left") + "</alignCell0>" +
                                                " <alignCell1>" + HttpUtility.HtmlEncode("Right") + "</alignCell1>" +
                                                " <widthCell0>" + HttpUtility.HtmlEncode("80") + "</widthCell0>" +
                                                " <widthCell1>" + HttpUtility.HtmlEncode("20") + "</widthCell1>" +
                                                " <RecordID>" + HttpUtility.HtmlEncode(strRecordID) + "</RecordID>" +
                                                " <spname>" + HttpUtility.HtmlEncode(strSPName) + "</spname>" +
                                                " <methodname>" + HttpUtility.HtmlEncode("sp_common_datatable") + "</methodname>" +
                                            "</root>";
                            SearchCriteria aSearchCriteria = new SearchCriteria(null, strXML);
                            int scid = SystemData.SearchCriteria_Insert(aSearchCriteria);
                            hlCustomAdd.NavigateUrl = "~/Pages/Generic/DataTable.aspx?scid=" + scid.ToString();

                            //lets nail the fancybox iframe
                            string strFancy = @"
                                $(function () {                              
                                    $('.popup_datatable_class').fancybox({
                                         iframe : {
                                            css : {
                                                width : '700px',
                                                height: '450px'
                                            }
                                        },       
                                        toolbar  : false,
	                                    smallBtn : true,  
                                        scrolling: 'auto',
                                        type: 'iframe',    
                                        titleShow: false,
                                        'onComplete' : function(){
                                            jQuery.fancybox.showActivity();
                                            jQuery('#fancybox-frame').load(function(){
                                                jQuery.fancybox.hideActivity();
                                            });
                                        },
                                        onStart: function () {
                                            var bapaActivity = $('#ctl00_HomeContentPlaceHolder_ddlV029').val();
                                            var badDebtAmount = $('#ctl00_HomeContentPlaceHolder_txtV007').val();
                                            if (badDebtAmount == '' && bapaActivity == 'Bad Debt Recovery Entry') {
                                                alert('Please enter bad debt recovery amount.');  
                                                return false;
                                            }
                                        }
                                    });
                                });
                                    
                                ";
                            strFancy = SystemData.ErrorGuardForServiceJS(strFancy);

                            theService.Temp_CustomJSCode = strFancy;
                            roList.Add(theService);

                            theBillingRow.Cells[1].Controls.Add(hlCustomAdd);
                        }
                        return roList;
                    }
                    catch (Exception ex)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "Bad Debt: View Billing Calc - Bills and Payment Activity Tab", ex.Message, ex.StackTrace, DateTime.Now, "CustemMethod_windtech");
                        SystemData.ErrorLog_Insert(theErrorLog);
                    }
                }
            }
        }
        return null;
    }


    public static List<object> timesheet_details_view_data_history(List<object> objList)
    {
        List<object> roList = new List<object>();

        Service thisService = null;
        string strDynamicpart = "";
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                thisService = (Service)obj;
                if (thisService != null)
                {
                    strDynamicpart = thisService.Temp_DynamicPartName;
                    break;
                }
            }
        }

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;
                if (pnlFullDetailPage != null)
                {
                    if (pnlFullDetailPage.FindControl("pnlDetail") != null)
                    {
                        Panel pnlListTop = ((Panel)pnlFullDetailPage.FindControl("pnlDetail"));
                        string strSystemName = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE DisplayName='TimesheetDataTableHistory'");
                        HtmlControl theTopTable = (HtmlControl)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_trX" + strSystemName);
                        
                        HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");

                        if (hfRecordID.Value != "-1")
                        {
                            PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(thisService.ServiceJSON);
                            DataTable dtTimesheetDetailsDataHistory = Common.DataTableFromText(thePageLifeCycleService.SPName + " " + hfRecordID.Value);
                            if (dtTimesheetDetailsDataHistory != null && dtTimesheetDetailsDataHistory.Rows.Count > 0)
                            {

                                decimal originalMultiplierX = 0;
                                decimal originalHour = 0;
                                decimal originalRate = 0;
                                decimal originalTotal = 0;
                                decimal modifiedMultiplierX = 0;
                                decimal modifiedHour = 0;
                                decimal modifiedRate = 0;
                                decimal modifiedTotal = 0;


                                int counter = 1;

                                /* for sure there are only 2 rows - Red */
                                foreach (DataRow dtr in dtTimesheetDetailsDataHistory.Rows)
                                {
                                    if (counter == 1)
                                    {
                                        originalMultiplierX = decimal.Parse(dtr["MultiplierX"].ToString());
                                        originalHour = decimal.Parse(dtr["Hour"].ToString());
                                        originalRate = decimal.Parse(dtr["Rate"].ToString());
                                        originalTotal = decimal.Parse(dtr["Total"].ToString());
                                    }
                                    else
                                    {
                                        modifiedMultiplierX = decimal.Parse(dtr["MultiplierX"].ToString());
                                        modifiedHour = decimal.Parse(dtr["Hour"].ToString());
                                        modifiedRate = decimal.Parse(dtr["Rate"].ToString());
                                        modifiedTotal = decimal.Parse(dtr["Total"].ToString());
                                    }
                                    counter += 1;
                                }


                                HtmlTable htmlTable = new HtmlTable();
                                htmlTable.BorderColor = "#505050";
                                htmlTable.CellPadding = 10;
                                htmlTable.Border = 1;

                                HtmlTableRow htr1 = new HtmlTableRow();
                                HtmlTableCell cellHeaderBlank = new HtmlTableCell();
                                HtmlTableCell cellMultiplierXHeader = new HtmlTableCell();
                                cellMultiplierXHeader.Attributes.Add("style", "text-align:center;");
                                HtmlTableCell cellHourHeader = new HtmlTableCell();
                                cellHourHeader.Attributes.Add("style", "text-align:center;");
                                HtmlTableCell cellRateHeader = new HtmlTableCell();
                                cellRateHeader.Attributes.Add("style", "text-align:center;");
                                HtmlTableCell cellTotalHeader = new HtmlTableCell();
                                cellTotalHeader.Attributes.Add("style", "text-align:center;");

                                htr1.Cells.Add(cellHeaderBlank);
                                htr1.Cells.Add(cellMultiplierXHeader);
                                htr1.Cells.Add(cellHourHeader);
                                htr1.Cells.Add(cellRateHeader);
                                htr1.Cells.Add(cellTotalHeader);

                                Label lbl1 = new Label();
                                lbl1.Attributes.Add("style", "color:black;font-style:italic"); 
                                Label lbl2 = new Label();
                                lbl2.Attributes.Add("style", "color:black;font-style:italic");
                                Label lbl3 = new Label();
                                lbl3.Attributes.Add("style", "color:black;font-style:italic");
                                Label lbl4 = new Label();
                                lbl4.Attributes.Add("style", "color:black;font-style:italic");


                                lbl1.Text = "Multiplier (X)";
                                lbl2.Text = "Hour";
                                lbl3.Text = "Rate";
                                lbl4.Text = "Total";

                                cellMultiplierXHeader.Controls.Add(lbl1);
                                cellHourHeader.Controls.Add(lbl2);
                                cellRateHeader.Controls.Add(lbl3);
                                cellTotalHeader.Controls.Add(lbl4);

                                htmlTable.Rows.Add(htr1);

                                /* Original Values */
                                HtmlTableRow htrOriginalValue = new HtmlTableRow();
                                HtmlTableCell cellItem = new HtmlTableCell();
                                cellItem.Attributes.Add("style", "text-align:right;");
                                HtmlTableCell cellOriginalValueMultiplierX = new HtmlTableCell();
                                cellOriginalValueMultiplierX.Attributes.Add("style", "text-align:right;");
                                HtmlTableCell cellOriginalValueHour = new HtmlTableCell();
                                cellOriginalValueHour.Attributes.Add("style", "text-align:right;");
                                HtmlTableCell cellOriginalValueRate = new HtmlTableCell();
                                cellOriginalValueRate.Attributes.Add("style", "text-align:right;");
                                HtmlTableCell cellOriginalValueTotal = new HtmlTableCell();
                                cellOriginalValueTotal.Attributes.Add("style", "text-align:right;");

                                htrOriginalValue.Cells.Add(cellItem);
                                htrOriginalValue.Cells.Add(cellOriginalValueMultiplierX);
                                htrOriginalValue.Cells.Add(cellOriginalValueHour);
                                htrOriginalValue.Cells.Add(cellOriginalValueRate);
                                htrOriginalValue.Cells.Add(cellOriginalValueTotal);

                                Label lblOriginalValue = new Label();
                                lblOriginalValue.Attributes["style"] = "color:black;";
                                Label lblOriginalValueMultiplierX = new Label();
                                lblOriginalValueMultiplierX.Attributes["style"] = "color:black;";
                                Label lblOriginalHour = new Label();
                                lblOriginalHour.Attributes["style"] = "color:black;";
                                Label lblOriginalValueRate = new Label();
                                lblOriginalValueRate.Attributes["style"] = "color:black;";
                                Label lblOriginalValueTotal = new Label();
                                lblOriginalValueTotal.Attributes["style"] = "color:black;";

                                lblOriginalValue.Text = "Original Value: ";
                                lblOriginalValueMultiplierX.Text = String.Format("{0:0,0.00}", originalMultiplierX);
                                lblOriginalHour.Text = String.Format("{0:0,0.00}", originalHour);
                                lblOriginalValueRate.Text = String.Format("{0:0,0.00}", originalRate);
                                lblOriginalValueTotal.Text = String.Format("{0:0,0.00}", originalTotal);

                                cellItem.Controls.Add(lblOriginalValue);
                                cellOriginalValueMultiplierX.Controls.Add(lblOriginalValueMultiplierX);
                                cellOriginalValueHour.Controls.Add(lblOriginalHour);
                                cellOriginalValueRate.Controls.Add(lblOriginalValueRate);
                                cellOriginalValueTotal.Controls.Add(lblOriginalValueTotal);


                                /* Modified Values */
                                HtmlTableRow htrModifiedValue = new HtmlTableRow();
                                HtmlTableCell cellItem2 = new HtmlTableCell();
                                cellItem2.Attributes.Add("style", "text-align:right;");
                                HtmlTableCell cellModifiedValueMultiplierX = new HtmlTableCell();
                                cellModifiedValueMultiplierX.Attributes.Add("style", "text-align:right;");
                                HtmlTableCell cellModifiedValueHour = new HtmlTableCell();
                                cellModifiedValueHour.Attributes.Add("style", "text-align:right;");
                                HtmlTableCell cellModifiedValueRate = new HtmlTableCell();
                                cellModifiedValueRate.Attributes.Add("style", "text-align:right;");
                                HtmlTableCell cellModifiedValueTotal = new HtmlTableCell();
                                cellModifiedValueTotal.Attributes.Add("style", "text-align:right;");

                                htrModifiedValue.Cells.Add(cellItem2);
                                htrModifiedValue.Cells.Add(cellModifiedValueMultiplierX);
                                htrModifiedValue.Cells.Add(cellModifiedValueHour);
                                htrModifiedValue.Cells.Add(cellModifiedValueRate);
                                htrModifiedValue.Cells.Add(cellModifiedValueTotal);

                                Label lblModifiedValue = new Label();
                                lblModifiedValue.Attributes["style"] = "color:black;";
                                Label lblModifiedValueMultiplierX = new Label();
                                lblModifiedValueMultiplierX.Attributes["style"] = "color:black;";
                                Label lblModifiedHour = new Label();
                                lblModifiedHour.Attributes["style"] = "color:black;";
                                Label lblModifiedValueRate = new Label();
                                lblModifiedValueRate.Attributes["style"] = "color:black;";
                                Label lblModifiedValueTotal = new Label();
                                lblModifiedValueTotal.Attributes["style"] = "color:black;";

                                lblModifiedValue.Text = "Adjusted Value: ";
                                lblModifiedValueMultiplierX.Text = String.Format("{0:0,0.00}", modifiedMultiplierX);
                                lblModifiedHour.Text = String.Format("{0:0,0.00}", modifiedHour);
                                lblModifiedValueRate.Text = String.Format("{0:0,0.00}", modifiedRate);
                                lblModifiedValueTotal.Text = String.Format("{0:0,0.00}", modifiedTotal);

                                cellItem2.Controls.Add(lblModifiedValue);
                                cellModifiedValueMultiplierX.Controls.Add(lblModifiedValueMultiplierX);
                                cellModifiedValueHour.Controls.Add(lblModifiedHour);
                                cellModifiedValueRate.Controls.Add(lblModifiedValueRate);
                                cellModifiedValueTotal.Controls.Add(lblModifiedValueTotal);

                                htmlTable.Rows.Add(htrOriginalValue);
                                htmlTable.Rows.Add(htrModifiedValue);


                                HtmlTableCell cellTimesheetHistoryValues = new HtmlTableCell();
                                cellTimesheetHistoryValues.Controls.Add(htmlTable);

                                HtmlTableCell cellNewBlank = new HtmlTableCell();


                                theTopTable.Controls.RemoveAt(0); /* remove the existing cell */
                                theTopTable.Controls.RemoveAt(0); /* remove the existing cell */
                                theTopTable.Controls.Add(cellNewBlank); 
                                theTopTable.Controls.Add(cellTimesheetHistoryValues);
                                
                            }
                        }

                    }

                    return roList;

                }

            }
        }

        return roList;
    }

}
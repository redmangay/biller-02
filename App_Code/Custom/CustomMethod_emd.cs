﻿using System;
using System.Collections.Generic;
//using System.Linq;
//using ChartDirector;
//using DocGen.DAL;
using System.Data;
using System.Web.UI.HtmlControls;
/// <summary>
/// Summary description for CustomMethod_emd
/// </summary>
public class CustomMethod_emd
{
	public CustomMethod_emd()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static List<object> DotNetMethod(string sMethodName, List<object> objList)
    {
        List<object> roList = new List<object>();
        try
        {

            switch (sMethodName.ToLower())
            {

                case "uhaqm_network_data_read_datetime":
                    roList = uhaqm_network_data_read_datetime(objList);
                    return roList;                

            }

            return roList;
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "DotNetMethod/SP" + sMethodName, ex.Message, ex.StackTrace, DateTime.Now, "App_Code");
            SystemData.ErrorLog_Insert(theErrorLog);
            if (roList != null)
                roList.Add(ex);
        }
        return null;
    }


    public static List<object> uhaqm_network_data_read_datetime(List<object> objList)
    {
        List<object> roList = new List<object>();

        DataTable dtImportFileTable = null;

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "DataTable")
            {
                dtImportFileTable = (DataTable)obj;
            }
        }


        if (dtImportFileTable != null && dtImportFileTable.Rows[0][0] != DBNull.Value
            && dtImportFileTable.Rows[0][0].ToString() != "")
        {
            string strThisFileDateTime = dtImportFileTable.Rows[0][0].ToString();
            if (strThisFileDateTime != "")
            {
                strThisFileDateTime = strThisFileDateTime.Substring(strThisFileDateTime.IndexOf(":") + 1).Trim();

                dtImportFileTable.Columns.Add("Date Time Sampled");
                foreach (DataRow dr in dtImportFileTable.Rows)
                {
                    dr["Date Time Sampled"] = strThisFileDateTime;//putting datatime value in all rows for this column
                }
                if (dtImportFileTable.Rows.Count > 2)
                {
                    dtImportFileTable.Rows[1]["Date Time Sampled"] = "Date Time Sampled";//puttng "Date Time Sampled" in line 23 as this line is the column header.
                }
                dtImportFileTable.AcceptChanges();
            }


            roList.Add(dtImportFileTable);
            return roList;

        }

        return null;
    }
    

}
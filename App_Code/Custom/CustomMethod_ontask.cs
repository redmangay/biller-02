﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;

/// <summary>
/// Summary description for CustomMethod_ontask
/// </summary>
public class CustomMethod_ontask
{
    public CustomMethod_ontask()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public static List<object> DotNetMethod(string sMethodName, List<object> objList)
    {
        List<object> roList = new List<object>();
        try
        {

            switch (sMethodName.ToLower())
            {

                case "ontask_show_invoice_item":
                    roList = ontask_show_invoice_item(objList);
                    return roList;

            }

            return roList;
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "DotNetMethod/SP" + sMethodName, ex.Message, ex.StackTrace, DateTime.Now, "App_Code");
            SystemData.ErrorLog_Insert(theErrorLog);
            if (roList != null)
                roList.Add(ex);
        }
        return null;
    }

    public static List<object> ontask_show_invoice_item(List<object> objList)
    {
        List<object> roList = new List<object>();

        string strDynamicpart = "";
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                Service thisService = (Service)obj;
                if (thisService != null)
                {
                    strDynamicpart = thisService.Temp_DynamicPartName;
                    break;
                }
            }
        }

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;
                if (pnlFullDetailPage != null)
                {
                    if (pnlFullDetailPage.FindControl("pnlDetail") != null)
                    {
                        Panel pnlListTop = ((Panel)pnlFullDetailPage.FindControl("pnlDetail"));
                        HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");

                        if (hfRecordID.Value != "-1")
                        {
                            Record theTaskRecord = RecordManager.ets_Record_Detail_Full(int.Parse(hfRecordID.Value), null, false);
                            DataTable dtInvoiceItems = Common.DataTableFromText("SELECT V002,V003,V004,V005 FROM RECORD  WHERE V001='" + theTaskRecord.RecordID + "'");
                            if(dtInvoiceItems != null && dtInvoiceItems.Rows.Count > 0)
                            {
                                HtmlTable htmlT = new HtmlTable();
                                htmlT.Style.Add("margin", "10px");
                                htmlT.BorderColor = "#505050";
                                htmlT.CellPadding = 10;
                                htmlT.Border = 1;

                                HtmlTableRow htr1 = new HtmlTableRow();
                                HtmlTableCell cellItemHeader = new HtmlTableCell();
                                HtmlTableCell cellNetHeader = new HtmlTableCell();
                                HtmlTableCell cellTaxHeader = new HtmlTableCell();
                                HtmlTableCell cellGrossHeader = new HtmlTableCell();

                                htr1.Cells.Add(cellItemHeader);
                                htr1.Cells.Add(cellNetHeader);
                                htr1.Cells.Add(cellTaxHeader);
                                htr1.Cells.Add(cellGrossHeader);

                                Label lbl1 = new Label();
                                lbl1.Attributes["style"] = "color:black;font-weight:bold;";
                                Label lbl2 = new Label();
                                lbl2.Attributes["style"] = "color:black;font-weight:bold;";
                                Label lbl3 = new Label();
                                lbl3.Attributes["style"] = "color:black;font-weight:bold;";
                                Label lbl4 = new Label();
                                lbl4.Attributes["style"] = "color:black;font-weight:bold;";
                                lbl1.Text = "Item";
                                lbl2.Text = "Net $";
                                lbl3.Text = "Tax $";
                                lbl4.Text = "Gross $";

                                cellItemHeader.Controls.Add(lbl1);
                                cellNetHeader.Controls.Add(lbl2);
                                cellTaxHeader.Controls.Add(lbl3);
                                cellGrossHeader.Controls.Add(lbl4);

                                htmlT.Rows.Add(htr1);

                                decimal totalNet = 0;
                                decimal totalTax = 0;
                                decimal totalGross = 0;

                                foreach (DataRow row in dtInvoiceItems.Rows)
                                {
                                    HtmlTableRow tableRow = new HtmlTableRow();
                                    HtmlTableCell cell1 = new HtmlTableCell();
                                    HtmlTableCell cell2 = new HtmlTableCell();
                                    HtmlTableCell cell3 = new HtmlTableCell();
                                    HtmlTableCell cell4 = new HtmlTableCell();

                                    tableRow.Cells.Add(cell1);
                                    tableRow.Cells.Add(cell2);
                                    tableRow.Cells.Add(cell3);
                                    tableRow.Cells.Add(cell4);

                                    Label lblRow1 = new Label();
                                    Label lblRow2 = new Label();
                                    Label lblRow3 = new Label();
                                    Label lblRow4 = new Label();
                                    lblRow1.Text = row["V005"].ToString();
                                    lblRow2.Text = "$" + decimal.Parse(row["V002"].ToString()).ToString("0.00");
                                    lblRow3.Text = "$" + decimal.Parse(row["V003"].ToString()).ToString("0.00");
                                    lblRow4.Text = "$" + decimal.Parse(row["V004"].ToString()).ToString("0.00");
                                    cell1.Controls.Add(lblRow1);
                                    cell2.Controls.Add(lblRow2);
                                    cell3.Controls.Add(lblRow3);
                                    cell4.Controls.Add(lblRow4);
                                    htmlT.Rows.Add(tableRow);
                                    totalNet += decimal.Parse(row["V002"].ToString());
                                    totalTax += decimal.Parse(row["V003"].ToString());
                                    totalGross += decimal.Parse(row["V004"].ToString());

                                }


                                HtmlTableRow htrTotal = new HtmlTableRow();
                                HtmlTableCell cellTotal = new HtmlTableCell();
                                HtmlTableCell cellTotalNet = new HtmlTableCell();
                                HtmlTableCell cellTotalTax = new HtmlTableCell();
                                HtmlTableCell cellTotalGross = new HtmlTableCell();

                                htrTotal.Cells.Add(cellTotal);
                                htrTotal.Cells.Add(cellTotalNet);
                                htrTotal.Cells.Add(cellTotalTax);
                                htrTotal.Cells.Add(cellTotalGross);

                                Label lblTotal = new Label();
                               
                                lblTotal.Attributes["style"] = "color:black;font-weight:bold;";
                                Label lblTotalNet = new Label();
                                lblTotalNet.Attributes["style"] = "color:black;font-weight:bold;";
                                Label lblTotalTax = new Label();
                                lblTotalTax.Attributes["style"] = "color:black;font-weight:bold;";
                                Label lblTotalGross = new Label();
                                lblTotalGross.Attributes["style"] = "color:black;font-weight:bold;";
                                lblTotal.Text = "Total";
                                lblTotalNet.Text = "$" + totalNet.ToString("0.00");
                                lblTotalTax.Text = "$" + totalTax.ToString("0.00");
                                lblTotalGross.Text = "$" + totalGross.ToString("0.00");

                                cellTotal.Controls.Add(lblTotal);
                                cellTotalNet.Controls.Add(lblTotalNet);
                                cellTotalTax.Controls.Add(lblTotalTax);
                                cellTotalGross.Controls.Add(lblTotalGross);

                                htmlT.Rows.Add(htrTotal);
                 
                                pnlListTop.Controls.Add(htmlT);
                            }
                        }

                    }

                    return roList;

                }

            }
        }

        return roList;
    }
}
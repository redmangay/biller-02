﻿using DocGen.DAL;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
//using System.Linq;
//using ChartDirector;
//using DocGen.DAL;
//using System.Web.UI.WebControls;
/// <summary>
/// Summary description for CustomMethod_adsafe
/// </summary>
public class CustomMethod_adsafe
{
    public CustomMethod_adsafe()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public static List<object> DotNetMethod(string sMethodName, List<object> objList)
    {
        List<object> roList = new List<object>();
        try
        {

            switch (sMethodName.ToLower())
            {

                case "case_note_last_milestone_description_and_date":
                    roList = case_note_last_milestone_description_and_date(objList);
                    return roList;

                case "people_code":
                    roList = people_code(objList);
                    return roList;

                case "location_code":
                    roList = location_code(objList);
                    return roList;

                case "event_code":
                    roList = event_code(objList);
                    return roList;

                case "investigation_code":
                    roList = investigation_code(objList);
                    return roList;

                case "victimcm_code":
                    roList = victimcm_code(objList);
                    return roList;

                case "poccm_code":
                    roList = poccm_code(objList);
                    return roList;

                case "query_code":
                    roList = query_code(objList);
                    return roList;

                case "rp_code":
                    roList = rp_code(objList);
                    return roList;

                case "milestone_process_point_code_dropdown_filter":
                    roList = milestone_process_point_code_dropdown_filter(objList);
                    return roList;

                case "return_to_edit_recorddetail":
                    roList = return_to_edit_recorddetail(objList);
                    return roList;

                case "return_to_event_recordlist":
                    roList = return_to_event_recordlist(objList);
                    return roList;

                case "send_email_button_message":
                    roList = send_email_button_message(objList);
                    return roList;

                case "test_message":
                    roList = test_message(objList);
                    return roList;


            }

            return roList;
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "DotNetMethod/SP" + sMethodName, ex.Message, ex.StackTrace, DateTime.Now, "App_Code");
            SystemData.ErrorLog_Insert(theErrorLog);
            if (roList != null)
                roList.Add(ex);
        }
        return null;
    }
    //Beggin  Menus (Query, Event, Investigation, Victim CM and POC CM): 
    //CN Code, CN Last MS Desc and Date, Last MS Desc and Date for Menus
    public static List<object> case_note_last_milestone_description_and_date(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                {
                    string strParentTableName = "";
                    HiddenField hfParentRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfParentRecordID");
                    HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");

                    if (hfRecordID != null && hfRecordID.Value == "-1")
                    {

                        if (hfParentRecordID != null)
                        {
                            strParentTableName = Common.GetValueFromSQL("SELECT [TableName] FROM [Table] WHERE [TableID] = (SELECT [TableID] FROM [Record] WHERE [RecordID]=" + hfParentRecordID.Value + ")");
                        }
                        string strSysMSCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='Captured MS Code'");
                        string strSysCNCode  = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='CN Code'");
                        string strSysMSDate = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='Captured MS Date'");
                        string strMSTableID = Common.GetValueFromSQL("SELECT [TableID] FROM [Table] WHERE [TableName]='Milestones' ");
                        string strSysQueryCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE DisplayName='Query Code' AND TableID=" + int.Parse(strMSTableID.ToString()));
                        string strSysEventCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE DisplayName='Allegation Code' AND TableID=" + int.Parse(strMSTableID.ToString()));
                        string strSysVictimCMCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE DisplayName='VCM Case Code' AND TableID=" + int.Parse(strMSTableID.ToString()));
                        string strSysInvestigationCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE DisplayName='Investigation Code' AND TableID=" + int.Parse(strMSTableID.ToString()));
                        string strSysPOCCMCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE DisplayName='POCCM Code' AND TableID=" + int.Parse(strMSTableID.ToString()));
                        string strSysPPCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE DisplayName='PP Code' AND TableID=" + int.Parse(strMSTableID.ToString()));
                        string strSysProcessPointDescription = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE DisplayName = 'Description' AND [TableID] =(SELECT [TableID] FROM [Table] WHERE [TableName] = 'Process Points')");
                        string strSysLastMSDate = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE DisplayName='MS Date' AND TableID=" + int.Parse(strMSTableID.ToString()));

                        // for CN Code

                        if (strSysCNCode != "")
                        {
                            TextBox txtCNCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysCNCode);
                            if (txtCNCode != null)
                            {
                                int iCodeNumber = 0;
                                string strCodeNumber = Common.GetValueFromSQL("SELECT TOP 1 V042 FROM [Record] WHERE [TableID] =3490 ORDER BY [RecordID] DESC");
                                iCodeNumber = int.Parse(strCodeNumber.ToString()) + 1;
                                txtCNCode.Text = "CN-" + iCodeNumber.ToString();
                            }
                        }

                        if (strParentTableName == "Query")
                        { // Query
                            
                            if (strSysMSCode != "")
                            {
                                TextBox txtMSCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysMSCode);
                                if (txtMSCode != null)
                                {
                                    txtMSCode.Text = Common.GetValueFromSQL("SELECT "+ strSysProcessPointDescription + " FROM [Record] WHERE [RecordID]=(SELECT TOP 1 " + strSysPPCode + "  FROM Record WHERE IsActive=1 and TableID=" + int.Parse(strMSTableID.ToString()) + " AND " + strSysQueryCode +  " = " + hfParentRecordID.Value + "ORDER BY RecordID DESC)");
                                }
                            }
                            if (strSysMSDate != "")
                            {
                                TextBox txtMSDate = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysMSDate);
                                if (txtMSDate != null)
                                {
                                    txtMSDate.Text = Common.GetValueFromSQL("SELECT TOP 1 " + strSysLastMSDate + " FROM [Record] WHERE IsActive=1 and TableID=" + int.Parse(strMSTableID.ToString()) + " AND " + strSysQueryCode + " = " + hfParentRecordID.Value + " ORDER BY RecordID DESC");
                                }
                            }

                           

                        } // End Query

                        if (strParentTableName == "Event")
                        { // Event

                            if (strSysMSCode != "")
                            {
                                TextBox txtMSCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysMSCode);
                                if (txtMSCode != null)
                                {
                                    txtMSCode.Text = Common.GetValueFromSQL("SELECT " + strSysProcessPointDescription + " FROM [Record] WHERE [RecordID]=(SELECT TOP 1 " + strSysPPCode +" FROM Record WHERE IsActive=1 and TableID=" + int.Parse(strMSTableID.ToString()) + " AND " + strSysEventCode + " = " + hfParentRecordID.Value + "ORDER BY RecordID DESC)");
                                }
                            }
                            if (strSysMSDate != "")
                            {
                                TextBox txtMSDate = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysMSDate);
                                if (txtMSDate != null)
                                {
                                    txtMSDate.Text = Common.GetValueFromSQL("SELECT TOP 1 "+ strSysLastMSDate + " FROM [Record] WHERE IsActive=1 and TableID=" + int.Parse(strMSTableID.ToString()) + " AND " + strSysEventCode + " = " + hfParentRecordID.Value + " ORDER BY RecordID DESC");
                                }
                            }

                        } // End Event

                        if (strParentTableName == "Investigation")
                        { // Investigation

                            if (strSysMSCode != "")
                            {
                                TextBox txtMSCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysMSCode);
                                if (txtMSCode != null)
                                {
                                    txtMSCode.Text = Common.GetValueFromSQL("SELECT " + strSysProcessPointDescription + " FROM [Record] WHERE [RecordID]=(SELECT TOP 1 " + strSysPPCode + " FROM Record WHERE IsActive=1 and TableID=" + int.Parse(strMSTableID.ToString()) + " AND " + strSysInvestigationCode + " = " + hfParentRecordID.Value + "ORDER BY RecordID DESC)");
                                }
                            }
                            if (strSysMSDate != "")
                            {
                                TextBox txtMSDate = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysMSDate);
                                if (txtMSDate != null)
                                {
                                    txtMSDate.Text = Common.GetValueFromSQL("SELECT TOP 1 " + strSysLastMSDate + " FROM [Record] WHERE IsActive=1 and TableID=" + int.Parse(strMSTableID.ToString()) + " AND " + strSysInvestigationCode + " = " + hfParentRecordID.Value + " ORDER BY RecordID DESC");
                                }
                            }

                        } // End Investigation

                        if (strParentTableName == "Victim CM")
                        { // Victim CM

                            if (strSysMSCode != "")
                            {
                                TextBox txtMSCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysMSCode);
                                if (txtMSCode != null)
                                {
                                    txtMSCode.Text = Common.GetValueFromSQL("SELECT " + strSysProcessPointDescription + " FROM [Record] WHERE [RecordID]=(SELECT TOP 1 " + strSysPPCode + " FROM Record WHERE IsActive=1 and TableID=" + int.Parse(strMSTableID.ToString()) + " AND " + strSysVictimCMCode + " = " + hfParentRecordID.Value + "ORDER BY RecordID DESC)");
                                }
                            }
                            if (strSysMSDate != "")
                            {
                                TextBox txtMSDate = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysMSDate);
                                if (txtMSDate != null)
                                {
                                    txtMSDate.Text = Common.GetValueFromSQL("SELECT TOP 1 " + strSysLastMSDate + " FROM [Record] WHERE IsActive=1 and TableID=" + int.Parse(strMSTableID.ToString()) + " AND " + strSysVictimCMCode + " = " + hfParentRecordID.Value + " ORDER BY RecordID DESC");
                                }
                            }

                        } // End Victim CM

                        if (strParentTableName == "POC Case Management")
                        { // POCCM

                            if (strSysMSCode != "")
                            {
                                TextBox txtMSCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysMSCode);
                                if (txtMSCode != null)
                                {
                                    txtMSCode.Text = Common.GetValueFromSQL("SELECT " + strSysProcessPointDescription + " FROM [Record] WHERE [RecordID]=(SELECT TOP 1 " + strSysPPCode + " FROM Record WHERE IsActive=1 and TableID=" + int.Parse(strMSTableID.ToString()) + " AND " + strSysPOCCMCode + " = " + hfParentRecordID.Value + "ORDER BY RecordID DESC)");
                                }
                            }
                            if (strSysMSDate != "")
                            {
                                TextBox txtMSDate = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysMSDate);
                                if (txtMSDate != null)
                                {
                                    txtMSDate.Text = Common.GetValueFromSQL("SELECT TOP 1 " + strSysLastMSDate + " FROM [Record] WHERE IsActive=1 and TableID=" + int.Parse(strMSTableID.ToString()) + " AND " + strSysPOCCMCode + " = " + hfParentRecordID.Value + " ORDER BY RecordID DESC");
                                }
                            }

                        } // End POCCM


                    }

                }

                break;
            }
        }
        
        return null;

    }
    //End 

    // People: Code, auto populate
    public static List<object> people_code(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                {
                    HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                    string strSysCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='Code'");
                    if (hfRecordID != null && hfRecordID.Value == "-1")
                    {

                        if (strSysCode != "")
                        {
                            TextBox txtCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysCode);
                            if (txtCode != null)
                            {
                                int iCodeNumber = 0;
                                string strCodeNumber = Common.GetValueFromSQL("SELECT TOP 1 [V033] FROM [Record] WHERE [TableID] =" + theService.TableID.ToString() + " ORDER BY [RecordID] DESC");
                                iCodeNumber = int.Parse(strCodeNumber.ToString()) + 1;
                                txtCode.Text = "P-" + iCodeNumber.ToString();
                            }
                        }
                    }
                                        
                }

                break;
            }
        }
                        
        return null;

    }
    // End

    // Location: Location Code, autopopulate
    public static List<object> location_code(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                {
                    HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                    string strSysLocationCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='Location Code'");
                    if (hfRecordID != null && hfRecordID.Value == "-1")
                    {

                        if (strSysLocationCode != "")
                        {
                            TextBox txtLocationCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysLocationCode);
                            if (txtLocationCode != null)
                            {
                                int iCodeNumber = 0;
                                string strLocationNumber = Common.GetValueFromSQL("SELECT TOP 1 [V034] FROM [Record] WHERE [TableID] =" + theService.TableID.ToString() + " ORDER BY [RecordID] DESC");
                                iCodeNumber = int.Parse(strLocationNumber.ToString()) + 1;
                                txtLocationCode.Text = "L-" + iCodeNumber.ToString();
                            }
                        }
                    }

                }

                break;
            }
        }

        return null;

    }
    // End

    // Event: Event Code
    public static List<object> event_code(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                {
                    HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                    string strSysEventCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='Event Code'");
                    if (hfRecordID != null && hfRecordID.Value == "-1")
                    {

                        if (strSysEventCode != "")
                        {
                            TextBox txtEventCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysEventCode);
                            if (txtEventCode != null)
                            {
                                int iCodeNumber = 0;
                                string strEventCode = Common.GetValueFromSQL("SELECT TOP 1 [V029] FROM [Record] WHERE [TableID] =" + theService.TableID.ToString() + " ORDER BY [RecordID] DESC");
                                iCodeNumber = int.Parse(strEventCode.ToString()) + 1;
                                txtEventCode.Text = "E-" + iCodeNumber.ToString();
                            }
                        }
                    }

                }

                break;
            }
        }

        return null;

    }
    // End

    // Investigation: Investigation Code, autopopulate
    public static List<object> investigation_code(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                {
                    HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                    string strSysInvestigationCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='Investigation Code'");
                    if (hfRecordID != null && hfRecordID.Value == "-1")
                    {

                        if (strSysInvestigationCode != "")
                        {
                            TextBox txtInvestigationCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysInvestigationCode);
                            if (txtInvestigationCode != null)
                            {
                                int iCodeNumber = 0;
                                string strInvestigationCode = Common.GetValueFromSQL("SELECT TOP 1 [V022] FROM [Record] WHERE [TableID] =" + theService.TableID.ToString() + " ORDER BY [RecordID] DESC");
                                iCodeNumber = int.Parse(strInvestigationCode.ToString()) + 1;
                                txtInvestigationCode.Text = "I-" + iCodeNumber.ToString();
                            }
                        }
                    }

                }

                break;
            }
        }

        return null;

    }
    // End

    // Victim CM: Case Code, autopopulate
    public static List<object> victimcm_code(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                {
                    HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                    string strSysVictimCMCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='Case Code'");
                    if (hfRecordID != null && hfRecordID.Value == "-1")
                    {

                        if (strSysVictimCMCode != "")
                        {
                            TextBox txtVictimCMCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysVictimCMCode);
                            if (txtVictimCMCode != null)
                            {
                                int iCodeNumber = 0;
                                string strVictimCMCode = Common.GetValueFromSQL("SELECT TOP 1 [V019] FROM [Record] WHERE [TableID] =" + theService.TableID.ToString() + " ORDER BY [RecordID] DESC");
                                iCodeNumber = int.Parse(strVictimCMCode.ToString()) + 1;
                                txtVictimCMCode.Text = "V-" + iCodeNumber.ToString();
                            }
                        }
                    }

                }

                break;
            }
        }

        return null;

    }
    // End

    // POC CM: Case Code, autopopulate
    public static List<object> poccm_code(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                {
                    HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                    string strSysPOCCMCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='Case Code'");
                    if (hfRecordID != null && hfRecordID.Value == "-1")
                    {

                        if (strSysPOCCMCode != "")
                        {
                            TextBox txtPOCCMCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysPOCCMCode);
                            if (txtPOCCMCode != null)
                            {
                                int iCodeNumber = 0;
                                string strPOCCMCode = Common.GetValueFromSQL("SELECT TOP 1 [V014] FROM [Record] WHERE [TableID] =" + theService.TableID.ToString() + " ORDER BY [RecordID] DESC");
                                iCodeNumber = int.Parse(strPOCCMCode.ToString()) + 1;
                                txtPOCCMCode.Text = "PCM-" + iCodeNumber.ToString();
                            }
                        }
                    }

                }

                break;
            }
        }

        return null;

    }
    // End

    // Query: Case Code, autopopulate
    public static List<object> query_code(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                {
                    HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                    string strSysQueryCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='Query Code'");
                    if (hfRecordID != null && hfRecordID.Value == "-1")
                    {

                        if (strSysQueryCode != "")
                        {
                            TextBox txtQueryCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysQueryCode);
                            if (txtQueryCode != null)
                            {
                                int iCodeNumber = 0;
                                string strQueryCode = Common.GetValueFromSQL("SELECT TOP 1 [V026] FROM [Record] WHERE [TableID] =" + theService.TableID.ToString() + " ORDER BY [RecordID] DESC");
                                iCodeNumber = int.Parse(strQueryCode.ToString()) + 1;
                                txtQueryCode.Text = "Q-" + iCodeNumber.ToString();
                            }
                        }
                    }

                }

                break;
            }
        }

        return null;

    }
    // End

    // RP Code: Case Code, autopopulate
    public static List<object> rp_code(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                {
                    HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                    string strSysRPCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='RP Code'");
                    if (hfRecordID != null && hfRecordID.Value == "-1")
                    {

                        if (strSysRPCode != "")
                        {
                            TextBox txtRPCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysRPCode);
                            if (txtRPCode != null)
                            {
                                int iCodeNumber = 0;
                                string strRPCode = Common.GetValueFromSQL("SELECT TOP 1 [V014] FROM [Record] WHERE [TableID] =" + theService.TableID.ToString() + " ORDER BY [RecordID] DESC");
                                iCodeNumber = int.Parse(strRPCode.ToString()) + 1;
                                txtRPCode.Text = "RP-" + iCodeNumber.ToString();
                            }
                        }
                    }

                }

                break;
            }
        }

        return null;

    }
    // End

    // Milestone PP Code: Dropdown Filter
    public static List<object> milestone_process_point_code_dropdown_filter(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                {
                    HiddenField hfParentRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfParentRecordID");
                    string strSysPPCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='PP Code'");
                    if (hfParentRecordID != null )
                    {
                        string strPorcessType = "";
                        string strParentTableName = "";
                        strParentTableName = Common.GetValueFromSQL("SELECT [TableName] FROM [Table] WHERE [TableID] = (SELECT [TableID] FROM [Record] WHERE [RecordID]=" + hfParentRecordID.Value + ")");
                        if (!string.IsNullOrEmpty(strParentTableName))
                        {
                            switch (strParentTableName.ToLower())
                            {
                                case "query":
                                   strPorcessType = "PT01";
                                    break;

                                case "event":
                                   strPorcessType = "PT02";
                                    break;

                                case "investigation":
                                    strPorcessType = "PT03";
                                    break;

                                case "victim cm":
                                    strPorcessType = "PT04";
                                    break;

                                case "poc case management":
                                    strPorcessType = "PT05";
                                    break;
                            }

                           
                        }
                        

                        if (strSysPPCode != "")
                        {
                            DropDownList ddlPPCode = (DropDownList)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_ddl" + strSysPPCode);

                            string ddlPPCodeSelectedValue = ddlPPCode.SelectedValue;
                            if (ddlPPCode != null)
                            {
                                ddlPPCode.Items.Clear();

                                DataTable dtPPCode  = Common.DataTableFromText("SELECT [RecordID], [V004] + ' ' + '(' + [V006] + ')' AS ProcessType FROM [Record] WHERE [TableID]=3554 AND [V006]='" + strPorcessType.ToString() + "'");

                                ListItem liAll = new ListItem("--Please Select--", "");
                                ddlPPCode.Items.Insert(0, liAll);

                                foreach (DataRow dr in dtPPCode.Rows)
                                {
                                    ListItem li = new ListItem(dr[1].ToString(), dr[0].ToString());
                                    ddlPPCode.Items.Add(li);
                                    
                                }

                               
                            }
                            if (!string.IsNullOrEmpty(ddlPPCodeSelectedValue))
                            {
                                ddlPPCode.SelectedValue = ddlPPCodeSelectedValue;

                            }
                        }
                    }

                }

                break;
            }
        }

        return null;

    }
    // End

    // Milestone PP Code: Dropdown Filter
    public static List<object> milestone_process_point_code_dropdown_filterx(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                {
                    HiddenField hfParentRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfParentRecordID");
                    string strSysPPCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='PP Code'");
                    if (hfParentRecordID != null)
                    {
                        string strPorcessType = "";
                        string strParentTableName = "";
                        strParentTableName = Common.GetValueFromSQL("SELECT [TableName] FROM [Table] WHERE [TableID] = (SELECT [TableID] FROM [Record] WHERE [RecordID]=" + hfParentRecordID.Value + ")");
                        if (!string.IsNullOrEmpty(strParentTableName))
                        {
                            switch (strParentTableName.ToLower())
                            {
                                case "query":
                                    strPorcessType = "PT01";
                                    break;

                                case "event":
                                    strPorcessType = "PT02";
                                    break;

                                case "investigation":
                                    strPorcessType = "PT03";
                                    break;

                                case "victim cm":
                                    strPorcessType = "PT04";
                                    break;

                                case "poc case management":
                                    strPorcessType = "PT05";
                                    break;
                            }


                        }


                        if (strSysPPCode != "")
                        {
                            DropDownList ddlPPCode = (DropDownList)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_ddl" + strSysPPCode);

                            ddlPPCode.Items.Clear();
                            if (ddlPPCode != null)
                            {

                                DataTable dtPPCode = Common.DataTableFromText("SELECT [RecordID], [V004] + ' ' + '(' + [V006] + ')' AS ProcessType FROM [Record] WHERE [TableID]=3554 AND [V006]='" + strPorcessType.ToString() + "'");

                                ListItem liAll = new ListItem("--Please Select--", "");
                                ddlPPCode.Items.Insert(0, liAll);

                                foreach (DataRow dr in dtPPCode.Rows)
                                {
                                    ListItem li = new ListItem(dr[1].ToString(), dr[0].ToString());
                                    ddlPPCode.Items.Add(li);

                                }


                            }
                        }
                    }

                }

                break;
            }
        }

        return null;

    }
    // End


    public static List<object> return_to_edit_recorddetail(List<object> objList)
    {
        List<object> roList = new List<object>();
        string strDynamicpart = "";
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                strDynamicpart = theService.Temp_DynamicPartName;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON); // be using this probably later    

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                try
                {
                    string CurrentURL = System.Web.HttpContext.Current.Request.RawUrl;
                    // Get UserID to use in [Record]
                    User _ObjUser = (User)System.Web.HttpContext.Current.Session["User"];
                    _ObjUser.UserID.ToString();
                    // Get the max RecrodID of the current user doing the transaction, with user so not to get other's Record
                    string iMaxRecordID = Common.GetValueFromSQL("SELECT MAX(RecordID) FROM [Record] WHERE [TableID]=3643 AND [Enteredby]=" + _ObjUser.UserID.ToString());


                    Panel pnlFullDetailPage = (Panel)obj;

                    if (CurrentURL.IndexOf("quickadd") > -1)
                    {

                        //Add query string to add link
                        HyperLink hlBackUrl = (HyperLink)pnlFullDetailPage.FindControl("hlBack"); // get the link
                        string strHLBackUrl = hlBackUrl.NavigateUrl;

                        if (strHLBackUrl.IndexOf("Recordid") > -1)
                        {
                            //do nothing
                        }
                        else
                        {
                            hlBackUrl.NavigateUrl = hlBackUrl.NavigateUrl + "&Recordid=" + Cryptography.Encrypt(iMaxRecordID);
                        }
                        hlBackUrl.NavigateUrl = hlBackUrl.NavigateUrl.Replace("mode=" + Cryptography.Encrypt("add"), "mode=" + Cryptography.Encrypt("edit"));
                    }

                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, " Quick add Location/Victim", ex.Message, ex.StackTrace, DateTime.Now, "CustemMethod_asafe");
                    SystemData.ErrorLog_Insert(theErrorLog);

                }

            }
        }

        return roList;
    } //end

    public static List<object> return_to_event_recordlist(List<object> objList)
    {
        List<object> roList = new List<object>();
        string strDynamicpart = "";
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                strDynamicpart = theService.Temp_DynamicPartName;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON); // be using this probably later    

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                try
                {

                    Panel pnlFullDetailPage = (Panel)obj;
                    HyperLink hlBackUrl = (HyperLink)pnlFullDetailPage.FindControl("hlBack"); // get the link

                    string strHLBackUrl = hlBackUrl.NavigateUrl;

                    if (strHLBackUrl.IndexOf("quickadd") > -1)
                    {
                        hlBackUrl.NavigateUrl = System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath + "/Pages/Record/RecordList.aspx?TableID=" + Cryptography.Encrypt("3643");
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "Add Event", ex.Message, ex.StackTrace, DateTime.Now, "CustemMethod_adsafe");
                    SystemData.ErrorLog_Insert(theErrorLog);

                }

            }
        }

        return roList;
    }

    public static List<object> send_email_button_message(List<object> objList)
    {
        List<object> roList = new List<object>();
        string strDynamicpart = "";
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                strDynamicpart = theService.Temp_DynamicPartName;
                break;
            }
        }

        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON); // be using this probably later    

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                try
                {
                    Panel pnlFullDetailPage = (Panel)obj;
                    HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");

                    if (hfRecordID != null && hfRecordID.Value != "-1")
                    {
                        string strMessage = "";
                        strMessage = Common.GetValueFromSQL("SELECT [MessageID] FROM [Message] WHERE RECORDID=" + hfRecordID.Value + " AND CONVERT(VARCHAR(50), DATETIME,103) = CONVERT(VARCHAR(50), GETDATE(),103) AND IsSent=1");
                        if (strMessage != "")
                        {
                            System.Web.HttpContext.Current.Session["tdbmsg"] = " Email sent successfully!";
                        }
                        else
                        {
                            System.Web.HttpContext.Current.Session["tdbmsg"] = " Email not sent successfully!";

                        }


                    }





                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "Button Click Event", ex.Message, ex.StackTrace, DateTime.Now, "CustemMethod_adsafe");
                    SystemData.ErrorLog_Insert(theErrorLog);

                }
            }
        }

        return null;
    }

    public static List<object> test_message(List<object> objList)
    {
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                try
                {
                    System.Web.HttpContext.Current.Session["tdbmsg"] = " This is a test message... - red";
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "Button Click Event", ex.Message, ex.StackTrace, DateTime.Now, "CustemMethod_adsafe");
                    SystemData.ErrorLog_Insert(theErrorLog);

                }
            }
        }

        return null;
    }
}
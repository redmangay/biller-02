﻿using DocGen.DAL;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
//using System.Linq;
//using ChartDirector;
//using DocGen.DAL;
//using System.Web.UI.WebControls;
/// <summary>
/// Summary description for CustomMethod_ind
/// </summary>
public class CustomMethod_ind
{
    public CustomMethod_ind()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public static List<object> DotNetMethod(string sMethodName, List<object> objList)
    {

        List<object> roList = new List<object>();
        try
        {
            switch (sMethodName.ToLower())
            {

                case "api_add_data_notification":
                    roList = api_add_data_notification(objList);
                    return roList;

            }

            return roList;
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "DotNetMethod" + sMethodName, ex.Message, ex.StackTrace, DateTime.Now, "App_Code");
            SystemData.ErrorLog_Insert(theErrorLog);
            if (roList != null)
                roList.Add(ex);
        }
        return null;
    }

    /* == Ind Tech == */
    [Serializable]
    public class indMessageContent : JSONField
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public string OtherParty { get; set; }
        public string MessageType { get; set; }
        public string DeliveryMethod { get; set; }

    }
    [Serializable]
    public class indData : JSONField
    {
        public string EventType { get; set; }
        public indMessageContent Event { get; set; }
        public string EventID { get; set; }
        public string Tags { get; set; }
        public string DateTimeStarted { get; set; }
        public string DateTimeEnded { get; set; }

    }   
    /* == end == */

    public static List<object> api_add_data_notification(List<object> objList)
    {
        GenericParam theGenericParam = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "GenericParam")
            {
                theGenericParam = (GenericParam)obj;
                break;
            }
        }

        try
        {
            string strSMSEMail = SystemData.SystemOption_ValueByKey_Account("WarningSMSEmail", null, null);
            Record theRecord = RecordManager.ets_Record_Detail_Full(int.Parse(theGenericParam.RecordID.ToString()), theGenericParam.TableID, false);

            indData indData = JSONField.GetTypedObject<indData>(theRecord.V002.ToString());
            
           
            //int newM = EmailManager.Message_Insert(m);
            //Message mSaved = EmailManager.Message_Detail(newM);

            string sSendEmailError = "";
            string strRecipient = "";
            if (indData.Event.DeliveryMethod == "S")
            {
                strRecipient = indData.Event.OtherParty + strSMSEMail;
            }
            if (indData.Event.DeliveryMethod == "E")
            {
                strRecipient = indData.Event.OtherParty;
            }

            Message m = new Message(null, null, null, int.Parse(theGenericParam.AccountID.ToString()), DateTime.Now, indData.Event.MessageType, indData.Event.DeliveryMethod,
                                             null, strRecipient, indData.Event.Subject, indData.Event.Body, null, "");

            DBGurus.SendEmail("Notifications",
                                indData.Event.DeliveryMethod == "E" ? true : false,
                                indData.Event.DeliveryMethod == "S" ? true : false, 
                                indData.Event.Subject, indData.Event.Body, "",
                                strRecipient, "", "", null, m, out sSendEmailError);



            theRecord.V001 = "Sent";
            int i = RecordManager.ets_Record_Update(theRecord, true);

            List<object> roList = new List<object>();
            roList.Add(i);

           
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "CustomMethod_ind/data notification", ex.Message, ex.StackTrace, DateTime.Now, "CustomMethod_ind/data notification");
            SystemData.ErrorLog_Insert(theErrorLog);
        }
        return null;

    }


}
﻿using DocGen.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for CustomMethod_jcu
/// </summary>
public class CustomMethod_jcu
{
    public CustomMethod_jcu()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static List<object> DotNetMethod(string sMethodName, List<object> objList)
    {
        List<object> roList = new List<object>();
        try
        {

            switch (sMethodName.ToLower())
            {

                case "checkrecord_if_complete":
                    roList = checkrecord_if_complete(objList);
                    return roList;
                case "seven_queryactivies_milestone_set":
                    roList = seven_queryactivies_milestone_set(objList);
                    return roList;
                case "seven_victimcmdetail_add_defaultset":
                    roList = seven_VictimCMDetail_Add_Defaultset(objList);
                    return roList;
                case "seven_victimcm_add_defaultset":
                    roList = seven_victimcm_add_defaultset(objList);
                    return roList;
                case "jcu_locked_unlocked":
                    roList = jcu_locked_unlocked(objList);
                    return roList;
                case "jcu_lock_unlock_buttontext":
                    roList = jcu_lock_unlock_buttontext(objList);
                    return roList;
                case "seventh_victimcm_include_listdetail":
                    roList = seventh_victimcm_include_listdetail(objList);
                    return roList;
                case "seventh_common_lastmscodeanddatedisplay":
                    roList = seventh_common_lastmscodeanddatedisplay(objList);
                    return roList;

            }

            return roList;
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "DotNetMethod/SP" + sMethodName, ex.Message, ex.StackTrace, DateTime.Now, "App_Code");
            SystemData.ErrorLog_Insert(theErrorLog);
            if (roList != null)
                roList.Add(ex);
        }
        return null;
    }

    public static List<object> seventh_common_lastmscodeanddatedisplay(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;

                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                {

                    string strSysMSCode = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='Last MS Code'");
                    string strSysMSDate = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + theService.TableID.ToString() + " AND DisplayName='Last MS Date'");

                    if (strSysMSCode != "")
                    {
                        TextBox txtMSCode = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysMSCode);
                        if (txtMSCode != null)
                        {
                            txtMSCode.Text = Common.GetValueFromSQL("SELECT TOP 1 V016 FROM [Record] WHERE IsActive=1 and TableID=3489 ORDER BY RecordID DESC");
                        }
                    }
                    if (strSysMSDate != "")
                    {
                        TextBox txtMSDate = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txt" + strSysMSDate);
                        if (txtMSDate != null)
                        {
                            txtMSDate.Text = Common.GetValueFromSQL("SELECT TOP 1 V005 FROM [Record] WHERE IsActive=1 and TableID=3489 ORDER BY RecordID DESC");
                        }
                    }

                }

                break;
            }
        }





        return null;

    }

    public static List<object> seventh_victimcm_include_listdetail(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Service theService = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
                break;
            }
        }
        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                Panel pnlFullDetailPage = (Panel)obj;
                HiddenField hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                HiddenField hfRecordAddEditView = (HiddenField)pnlFullDetailPage.FindControl("hfRecordAddEditView");

                if (hfRecordAddEditView.Value == "add")
                {
                    return null;
                }

                HiddenField hfRecordDetailSCid = (HiddenField)pnlFullDetailPage.FindControl("hfRecordDetailSCid");
                Panel pnlDetail = (Panel)pnlFullDetailPage.FindControl("pnlDetail");
                //"ASP_Page_Init"  ASP_Page_Load
                if (thePageLifeCycleService.ServerControlEvent == "ASP_Page_Load")
                {
                    Record theRecord = RecordManager.ets_Record_Detail_Full(int.Parse(hfRecordID.Value), null, false);
                    if (theRecord != null && !string.IsNullOrEmpty(theRecord.V002))
                    {
                        Page thePage = pnlDetail.Page;
                        UserControl aD = (UserControl)thePage.LoadControl("~/Pages/UserControl/DetailEdit.ascx");
                        HtmlTableRow rowEmpty1 = (HtmlTableRow)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_trXV007");
                        Label lblE1 = (Label)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_lblV007");
                        TextBox txtE1 = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txtV007");
                        lblE1.Visible = false;
                        txtE1.Visible = false;
                        Record thePRecord = RecordManager.ets_Record_Detail_Full(int.Parse(theRecord.V002), null, false);
                        aD.GetType().GetProperty("RecordID").SetValue(aD, int.Parse(theRecord.V002));
                        aD.GetType().GetProperty("TableID").SetValue(aD, (int)thePRecord.TableID);
                        aD.GetType().GetProperty("ContentPage").SetValue(aD, "record");
                        aD.GetType().GetProperty("Mode").SetValue(aD, "view");
                        aD.GetType().GetProperty("ShowEditButton").SetValue(aD, true);
                        aD.GetType().GetProperty("HideHistory").SetValue(aD, true);
                        aD.GetType().GetProperty("ColumnNotIn").SetValue(aD, "71679");
                        rowEmpty1.Cells[1].Controls.Add(aD);
                        rowEmpty1.Cells[1].Style.Add("border", "solid 1px black");
                        UserControl aL = (UserControl)thePage.LoadControl("~/Pages/UserControl/RecordList.ascx");
                        HtmlTableRow rowEmpty2 = (HtmlTableRow)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_trXV008");
                        Label lblE2 = (Label)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_lblV008");
                        TextBox txtE2 = (TextBox)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_txtV008");
                        lblE2.Visible = false;
                        txtE2.Visible = false;
                        aL.GetType().GetProperty("TableID").SetValue(aL, 3616);
                        aL.GetType().GetProperty("PageType").SetValue(aL, "any");
                        aL.GetType().GetProperty("AnyTextSearch").SetValue(aL, " AND Record.V006=" + theRecord.V002);
                        //rowEmpty2.Cells[1].Style.Add("border", "solid 1px black");
                        rowEmpty2.Cells[1].Controls.Add(aL);
                    }

                }

                break;
            }
        }





        return null;

    }

    public static List<object> jcu_lock_unlock_buttontext(List<object> objList)
    {
        Panel pnlFullDetailPage = null;
        LinkButton lockButton = null;
        HiddenField hfRecordID = null;

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Panel")
            {
                pnlFullDetailPage = (Panel)obj;
                if ((LinkButton)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_lnkV131") != null)
                {
                    lockButton = (LinkButton)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_lnkV131");
                }

                hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                break;
            }
        }

        string strReadOnly = Common.GetValueFromSQL("SELECT IsReadOnly FROM [Record] WHERE RecordID = " + hfRecordID.Value);

        bool bIsReadOnly;
        if (Boolean.TryParse(strReadOnly, out bIsReadOnly))
        {
            //Do nothing
        }

        if (bIsReadOnly == true)
        {
            string strLock = lockButton.Text;
            lockButton.Text = "<strong>Un" + strLock.ToLower() + "</strong>";
        }

        return null;
    }

    public static List<object> jcu_locked_unlocked(List<object> objList)
    {
        Service theService = null;
        Panel pnlFullDetailPage = null;
        HiddenField hfRecordID = null;
        HiddenField hfRecordDetailSCid = null;

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "Service")
            {
                theService = (Service)obj;
            }
            else if (obj.GetType().Name == "Panel")
            {
                pnlFullDetailPage = (Panel)obj;
                hfRecordID = (HiddenField)pnlFullDetailPage.FindControl("hfRecordID");
                hfRecordDetailSCid = (HiddenField)pnlFullDetailPage.FindControl("hfRecordDetailSCid");
            }
        }

        string strVisitTableID = theService.TableID.ToString();
        string strEditVisitURL = "RecordDetail.aspx?mode=" + Cryptography.Encrypt("edit") + "&SearchCriteriaID=" + Cryptography.Encrypt(hfRecordDetailSCid.Value) + "&TableID=" + Cryptography.Encrypt(strVisitTableID) + "&stackhault=yes";
        string strViewVisitURL = "RecordDetail.aspx?mode=" + Cryptography.Encrypt("view") + "&SearchCriteriaID=" + Cryptography.Encrypt(hfRecordDetailSCid.Value) + "&TableID=" + Cryptography.Encrypt(strVisitTableID) + "&stackhault=yes";

        //object serviceParams = objList[3];
        //LinkButton lnkButton = (LinkButton)serviceParams.GetType().GetProperty("ButtonObject").GetValue(serviceParams, null);
        
        string strReadOnly = Common.GetValueFromSQL("SELECT IsReadOnly FROM [Record] WHERE RecordID = " + hfRecordID.Value);

        bool bIsReadOnly;
        if (Boolean.TryParse(strReadOnly, out bIsReadOnly))
        {
            //Do nothing
        }
        

        if (bIsReadOnly == false)
        {
            LockUnlockAllRecords(hfRecordID.Value, 1);
            System.Web.HttpContext.Current.Response.Redirect(strViewVisitURL + "&RecordID=" + Cryptography.Encrypt(hfRecordID.Value), true);
        }
        else if (bIsReadOnly == true)
        {
            LockUnlockAllRecords(hfRecordID.Value, 0);
            System.Web.HttpContext.Current.Response.Redirect(strEditVisitURL + "&RecordID=" + Cryptography.Encrypt(hfRecordID.Value), true);
        }

        return null;
    }

    public static List<object> seven_victimcm_add_defaultset(List<object> objList)
    {
        //
        List<object> roList = new List<object>();

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_record_recorddetail_aspx")
            {
                System.Web.UI.Page thePage = (System.Web.UI.Page)obj;
                if (thePage.Request.QueryString["mode"] == null || thePage.IsPostBack)
                {
                    return null;
                }
                string strMode = Cryptography.Decrypt(thePage.Request.QueryString["mode"].ToString());
                if (strMode.ToLower() != "add")
                {
                    return null;
                }

                break;
            }
        }

        foreach (object obj in objList)
        {

            if (obj.GetType().Name == "Panel")
            {

                Panel pnlFullDetailPage = (Panel)obj;

                DropDownList ddlCaseManager = (DropDownList)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_ddlV025");

                if (ddlCaseManager != null && System.Web.HttpContext.Current.Session["User"] != null)
                {
                    User theUser = (User)System.Web.HttpContext.Current.Session["User"];
                    if (theUser != null)
                    {
                        string strPeopleRecordID = Common.GetValueFromSQL("SELECT RecordID FROM [Record] WHERE IsActive=1 AND TableID=3460 AND V026=" + theUser.UserID.ToString());
                        if (strPeopleRecordID != "")
                        {
                            if (ddlCaseManager.Items.FindByValue(strPeopleRecordID) != null)
                            {
                                ddlCaseManager.SelectedValue = strPeopleRecordID;
                            }
                        }

                    }

                }

                return null;


            }

        }



        return null;

    }

    public static List<object> seven_VictimCMDetail_Add_Defaultset(List<object> objList)
    {
        //
        List<object> roList = new List<object>();

        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_record_recorddetail_aspx")
            {
                System.Web.UI.Page thePage = (System.Web.UI.Page)obj;
                if (thePage.Request.QueryString["mode"] == null || thePage.IsPostBack)
                {
                    return null;
                }
                string strMode = Cryptography.Decrypt(thePage.Request.QueryString["mode"].ToString());
                if (strMode.ToLower() != "add")
                {
                    return null;
                }

                break;
            }
        }

        foreach (object obj in objList)
        {

            if (obj.GetType().Name == "Panel")
            {

                Panel pnlFullDetailPage = (Panel)obj;

                DropDownList ddlCaseManager = (DropDownList)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_ddlV004");

                if (ddlCaseManager != null && System.Web.HttpContext.Current.Session["User"] != null)
                {
                    User theUser = (User)System.Web.HttpContext.Current.Session["User"];
                    if (theUser != null)
                    {
                        string strPeopleRecordID = Common.GetValueFromSQL("SELECT RecordID FROM [Record] WHERE IsActive=1 AND TableID=3460 AND V026=" + theUser.UserID.ToString());
                        if (strPeopleRecordID != "")
                        {
                            if (ddlCaseManager.Items.FindByValue(strPeopleRecordID) != null)
                            {
                                ddlCaseManager.SelectedValue = strPeopleRecordID;
                            }
                        }

                    }

                }

                return null;


            }

        }



        return null;

    }

    public static List<object> seven_queryactivies_milestone_set(List<object> objList)
    {
        //
        List<object> roList = new List<object>();
        Record theQueryDetailRecord = null;
        foreach (object obj in objList)
        {
            if (obj.GetType().Name == "pages_record_recorddetail_aspx")
            {
                System.Web.UI.Page thePage = (System.Web.UI.Page)obj;
                if (thePage.Request.QueryString["parentRecordid"] == null)
                {
                    return null;
                }

                string strRecordID = Cryptography.Decrypt(thePage.Request.QueryString["parentRecordid"].ToString());
                theQueryDetailRecord = RecordManager.ets_Record_Detail_Full(int.Parse(strRecordID), null, false);

                break;
            }
        }

        foreach (object obj in objList)
        {

            if (obj.GetType().Name == "Panel")
            {


                Panel pnlFullDetailPage = (Panel)obj;



                DropDownList ddlCapturedMilstone = (DropDownList)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_ddlV007");

                if (!string.IsNullOrEmpty(theQueryDetailRecord.V006))
                {
                    if (ddlCapturedMilstone.Items.FindByValue(theQueryDetailRecord.V006) != null)
                    {
                        ddlCapturedMilstone.SelectedValue = theQueryDetailRecord.V006;
                    }
                }
                DropDownList ddlMSDate = (DropDownList)pnlFullDetailPage.FindControl("ctl00_HomeContentPlaceHolder_ddlV012");

                if (!string.IsNullOrEmpty(theQueryDetailRecord.V007))
                {
                    if (ddlMSDate.Items.FindByValue(theQueryDetailRecord.V007) != null)
                    {
                        ddlMSDate.SelectedValue = theQueryDetailRecord.V007;
                    }
                }



                return null;


            }

        }



        return null;

    }

    public static List<object> checkrecord_if_complete(List<object> objList)
    {
        object serviceParams = objList[3];
        string recordStatus = "Visit data is complete";

        DataTable ColumnDetails = (DataTable)serviceParams.GetType().GetProperty("ColumnDetails").GetValue(serviceParams, null);
        Record RecordToCheck = (Record)serviceParams.GetType().GetProperty("RecordToCheck").GetValue(serviceParams, null);
        string HiddenTableTab = serviceParams.GetType().GetProperty("HiddenTableTab").GetValue(serviceParams, null).ToString();
        string[] HiddenFields = (string[])serviceParams.GetType().GetProperty("HiddenFields").GetValue(serviceParams, null);
        int? UserID = (int?)serviceParams.GetType().GetProperty("UserID").GetValue(serviceParams, null);

        string strHiddenFieldID = string.Join(",", HiddenFields);

        for (int i = 0; i < ColumnDetails.Rows.Count; i++)
        {
            string strImportance = ColumnDetails.Rows[i]["Importance"].ToString();
            string strSystemName = ColumnDetails.Rows[i]["SystemName"].ToString();
            string strTableTabID = ColumnDetails.Rows[i]["TableTabID"].ToString();
            string columnValue = RecordManager.GetRecordValue(ref RecordToCheck, strSystemName);
            string strSystemFieldID = "#ctl00_HomeContentPlaceHolder_trX" + strSystemName;

            if ((strImportance == "r" && (columnValue == null || columnValue.ToString() == ""))
            && !Common.IsIn(strTableTabID, HiddenTableTab)
            && !Common.IsIn(strSystemFieldID, strHiddenFieldID))
            {
               recordStatus = "Visit has missing data";
            }
        }

        UpdateVisitStatus(RecordToCheck.RecordID, UserID, recordStatus);

        return null;
    }

    public static void LockUnlockAllRecords(string strRecordID, int lockIt)
    {
        Common.ExecuteText("UPDATE [Record] set IsReadOnly=" + lockIt + " where TableID = 3376 and RecordID = '" + strRecordID + "'");

        DataTable dtChildTables = Common.DataTableFromText("SELECT DISTINCT ParentTableID, ChildTableID FROM TableChild WHERE ParentTableID = 3376");

        if (dtChildTables.Rows.Count > 0)
        {
            foreach (DataRow drCT in dtChildTables.Rows)
            {
                DataTable dtLinkColumn = Common.DataTableFromText(@"SELECT ColumnID, SystemName, TableID FROM [Column] WHERE ColumnType='dropdown' 
                            AND (Dropdowntype='table' OR Dropdowntype='tabledd')
	                        AND TableID=" + drCT["ChildTableID"].ToString() + @" AND TableTableID=" + drCT["ParentTableID"].ToString());

                if (dtLinkColumn.Rows.Count > 0)
                {
                    foreach (DataRow drColumn in dtLinkColumn.Rows)
                    {
                        Common.ExecuteText("UPDATE [Record] set IsReadOnly=" + lockIt + " where TableID = " + drCT["ChildTableID"].ToString() + " and " + drColumn["SystemName"] + " = '" + strRecordID + "'");
                    }
                }
            }
        }
    }

    public static string UpdateVisitStatus(int? RecordID, int? UserID, string recordStatus)
    {
        string strValue = "";

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {

            using (SqlCommand command = new SqlCommand("JCU_Visit_Save", connection))
            {
                command.CommandType = CommandType.StoredProcedure;


                SqlParameter pRV = new SqlParameter("@Return", SqlDbType.VarChar);
                pRV.Size = 4000;
                pRV.Direction = ParameterDirection.Output;

                command.Parameters.Add(pRV);
                if (RecordID != null)
                    command.Parameters.Add(new SqlParameter("@RecordID", RecordID));



                if (UserID != null)
                    command.Parameters.Add(new SqlParameter("@UserID", UserID));

                if (recordStatus != null && recordStatus != string.Empty)
                    command.Parameters.Add(new SqlParameter("@IsMissingData", recordStatus));


                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    strValue = pRV.Value.ToString();
                }
                catch
                {

                }


                connection.Close();
                connection.Dispose();



                return strValue;
            }
        }
    }
}

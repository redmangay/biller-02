﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Configuration;

namespace Windtech
{
    public class ProcessRequest
    {
        public static string Read(int? recordId)
        {
            JSONList data = new JSONList("data");

            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand comm = new SqlCommand();
                    comm.Connection = conn;

                    comm.CommandText = "SELECT * " +
                                        "FROM [Record] " +
                                        "WHERE RecordID= @RecordId";
                    comm.Parameters.Clear();
                    comm.Parameters.AddWithValue("RecordId", recordId);

                    using (SqlDataReader reader = comm.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var jObjects = new JSONObject();
                            for (var i = 0; i < reader.FieldCount; i++)
                            {
                                jObjects.AddString(reader.GetName(i), reader[i]);
                            }
                            data.AddObject(jObjects);
                        }
                    }

                    return new JSONObject().AddList(data).ToString();
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "Windtech: Process Request/Read", ex.Message, ex.StackTrace, DateTime.Now, "Custom/Model/Windtech/ProcessRequest");
                    SystemData.ErrorLog_Insert(theErrorLog);
                }
                conn.Close();
                conn.Dispose();

                return new JSONObject().AddList(data).ToString();
            }
        }

        public static string ReadSP(int? recordId, string spName)
        {
            JSONList data = new JSONList("data");

            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                using (SqlCommand command = new SqlCommand(spName, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    if (recordId != null)
                        command.Parameters.Add(new SqlParameter("@RecordID", recordId));
                                        
                        conn.Open();
                        command.Connection = conn;

                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var jObjects = new JSONObject();
                                for (var i = 0; i < reader.FieldCount; i++)
                                {
                                    jObjects.AddString(reader.GetName(i), reader[i]);
                                }
                                data.AddObject(jObjects);
                            }
                        }
                        return new JSONObject().AddList(data).ToString();
                    }
                    catch (Exception ex)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "Windtech: Process Request/ReadSP", ex.Message, ex.StackTrace, DateTime.Now, "Custom/Model/Windtech/ProcessRequest");
                        SystemData.ErrorLog_Insert(theErrorLog);
                    }
                    conn.Close();
                    conn.Dispose();

                    return new JSONObject().AddList(data).ToString();
                }
            }
        }

        public static string ReadSPWithValues(int? recordId, string strV001, string strV002, string strV003, string strV004,
                string strV005, string strV006, string strV007, string strV008, string strV009, string strV010, string spName)
        {
            JSONList data = new JSONList("data");

            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                using (SqlCommand command = new SqlCommand(spName, conn))
                {

                    command.CommandType = CommandType.StoredProcedure;

                    if (recordId != null)
                        command.Parameters.Add(new SqlParameter("@RecordID", recordId));

                    if (strV001 != "")
                        command.Parameters.Add(new SqlParameter("@param001", strV001));

                    if (strV002 != "")
                        command.Parameters.Add(new SqlParameter("@param002", strV002));

                    if (strV003 != "")
                        command.Parameters.Add(new SqlParameter("@param003", strV003));

                    if (strV004 != "")
                        command.Parameters.Add(new SqlParameter("@param004", strV004));

                    if (strV005 != "")
                        command.Parameters.Add(new SqlParameter("@param005", strV005));

                    if (strV006 != "")
                        command.Parameters.Add(new SqlParameter("@param006", strV006));

                    if (strV007 != "")
                        command.Parameters.Add(new SqlParameter("@param007", strV007));

                    if (strV008 != "")
                        command.Parameters.Add(new SqlParameter("@param008", strV008));

                    if (strV009 != "")
                        command.Parameters.Add(new SqlParameter("@param009", strV009));

                    if (strV010 != "")
                        command.Parameters.Add(new SqlParameter("@param010", strV010));

                        conn.Open();
                        command.Connection = conn;

                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var jObjects = new JSONObject();
                                for (var i = 0; i < reader.FieldCount; i++)
                                {
                                    jObjects.AddString(reader.GetName(i), reader[i]);
                                }
                                data.AddObject(jObjects);
                            }
                        }

                        return new JSONObject().AddList(data).ToString();
                    }
                    catch (Exception ex)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "Windtech: Process Request/ReadSPWithValues", ex.Message, ex.StackTrace, DateTime.Now, "Custom/Model/Windtech/ProcessRequest");
                        SystemData.ErrorLog_Insert(theErrorLog);
                    }
                    conn.Close();
                    conn.Dispose();

                    return new JSONObject().AddList(data).ToString();
                }
            }
        }

        public static string ReadSPByValue(string strValue, string spName)
        {
            JSONList data = new JSONList("data");

            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                using (SqlCommand command = new SqlCommand(spName, conn))
                {

                    command.CommandType = CommandType.StoredProcedure;
                    if (!string.IsNullOrEmpty(strValue))
                        command.Parameters.Add(new SqlParameter("@sValue", strValue));

                    conn.Open();
                    command.Connection = conn;

                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var jObjects = new JSONObject();
                                for (var i = 0; i < reader.FieldCount; i++)
                                {
                                    jObjects.AddString(reader.GetName(i), reader[i]);
                                }
                                data.AddObject(jObjects);
                            }
                        }

                        return new JSONObject().AddList(data).ToString();
                    }
                    catch (Exception ex)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "Windtech: Process Request/ReadSPByValue", ex.Message, ex.StackTrace, DateTime.Now, "Custom/Model/Windtech/ProcessRequest");
                        SystemData.ErrorLog_Insert(theErrorLog);
                    }
                    conn.Close();
                    conn.Dispose();

                    return new JSONObject().AddList(data).ToString();
                }
            }
        }
        
        public static string Create(int? recordId)
        {
            JSONList data = new JSONList("data");
            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;

                //sql here

            }
            
                return new JSONObject().AddList(data).ToString();
        }

        public static string Update(int? recordId)
        {
            JSONList data = new JSONList("data");
            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;

                //sql here

            }

            return new JSONObject().AddList(data).ToString();
        }

        public static string UpdateSP(int? recordId, string spName)
        {

            JSONList data = new JSONList("data");

            using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
            {

                using (SqlCommand command = new SqlCommand(spName, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter pRV = new SqlParameter("@Return", SqlDbType.VarChar);
                    pRV.Size = 4000;
                    pRV.Direction = ParameterDirection.Output;

                    command.Parameters.Add(pRV);
                    if (recordId != null)
                        command.Parameters.Add(new SqlParameter("@RecordID", recordId));


                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                        data.AddObject(new JSONObject()
                           .AddString("Output", pRV.Value.ToString()));
                    }
                    catch (Exception ex)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "Windtech: Process Request/UpdateSP", ex.Message, ex.StackTrace, DateTime.Now, "Custom/Model/Windtech/ProcessRequest");
                        SystemData.ErrorLog_Insert(theErrorLog);
                    }

                    connection.Close();
                    connection.Dispose();

                    return new JSONObject().AddList(data).ToString();
                }
            }
        }

        public static string UpdateSPWithValues(int? recordId, string strV001, string strV002, string strV003, string strV004,
                string strV005, string strV006, string strV007, string strV008, string strV009, string strV010, string spName)
        {

            JSONList data = new JSONList("data");

            User _ObjUser = (User)System.Web.HttpContext.Current.Session["User"];
            _ObjUser.UserID.ToString();

            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                using (SqlCommand command = new SqlCommand(spName, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    if (recordId != null)
                        command.Parameters.Add(new SqlParameter("@RecordID", recordId));

                    if (strV001 != "")
                        command.Parameters.Add(new SqlParameter("@param001", strV001));

                    if (strV002 != "")
                        command.Parameters.Add(new SqlParameter("@param002", strV002));

                    if (strV003 != "")
                        command.Parameters.Add(new SqlParameter("@param003", strV003));

                    if (strV004 != "")
                        command.Parameters.Add(new SqlParameter("@param004", strV004));

                    if (strV005 != "")
                        command.Parameters.Add(new SqlParameter("@param005", strV005));

                    if (strV006 != "")
                        command.Parameters.Add(new SqlParameter("@param006", strV006));

                    if (strV007 != "")
                        command.Parameters.Add(new SqlParameter("@param007", strV007));

                    if (strV008 != "")
                        command.Parameters.Add(new SqlParameter("@param008", strV008));

                    if (strV009 != "")
                        command.Parameters.Add(new SqlParameter("@param009", strV009));

                    if (strV010 != "")
                        command.Parameters.Add(new SqlParameter("@param010", strV010));

                    if (!string.IsNullOrEmpty(_ObjUser.UserID.ToString()))
                        command.Parameters.Add(new SqlParameter("@userID", _ObjUser.UserID.ToString()));

                    conn.Open();
                    command.Connection = conn;

                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var jObjects = new JSONObject();
                                for (var i = 0; i < reader.FieldCount; i++)
                                {
                                    jObjects.AddString(reader.GetName(i), reader[i]);
                                }
                                data.AddObject(jObjects);
                            }
                        }
                        return new JSONObject().AddList(data).ToString();
                    }
                    catch (Exception ex)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "Windtech: Process Request/UpdateSPWithValues", ex.Message, ex.StackTrace, DateTime.Now, "Custom/Model/Windtech/ProcessRequest");
                        SystemData.ErrorLog_Insert(theErrorLog);
                    }

                    conn.Close();
                    conn.Dispose();

                    return new JSONObject().AddList(data).ToString();
                }
            }
        }

        public static string UpdateUnallocatedPayments(string recordId, string tableId, string activity, string isUnallocated, string jobNo, string date, string amount, string description)
        {
            JSONList data = new JSONList("data");
            int iResult = -1;

            try
            {
                int iRecordID = string.IsNullOrEmpty(recordId) ? 0 : int.Parse(recordId);
                int? iTableID = string.IsNullOrEmpty(tableId) ? 0 : int.Parse(tableId);

                User _ObjUser = (User)System.Web.HttpContext.Current.Session["User"];
                _ObjUser.UserID.ToString();

                Record theRecord = RecordManager.ets_Record_Detail_Full(iRecordID, iTableID, false);

                if (theRecord != null)
                {
                    if (jobNo != string.Empty)
                        theRecord.V002 = jobNo;

                    if (date != string.Empty)
                        theRecord.V004 = date;

                    if (amount != string.Empty)
                        theRecord.V007 = amount;

                    if (description != string.Empty)
                        theRecord.V015 = description;

                    if (activity != string.Empty)
                        theRecord.V029 = activity;

                    if (isUnallocated != string.Empty)
                    {
                        if (isUnallocated == "Null")
                        {
                            theRecord.V036 = null;
                        }
                        else
                        {
                            theRecord.V036 = isUnallocated;
                        }
                    }

                    theRecord.LastUpdatedUserID = int.Parse(_ObjUser.UserID.ToString());
                    iResult = RecordManager.ets_Record_Update(theRecord, true);
                }

                data.AddObject(new JSONObject()
                   .AddString("Output", iResult.ToString()));
            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Windtech: Process Request/UnAllocatedPayment", ex.Message, ex.StackTrace, DateTime.Now, "Custom/Model/Windtech/ProcessRequest");
                SystemData.ErrorLog_Insert(theErrorLog);
            }

            return new JSONObject().AddList(data).ToString();
        }

        public static string AddUnallocatedPayments(string tableId, string activity, string isUnallocated, string jobNo, string date, string amount, string description)
        {
            JSONList data = new JSONList("data");
            int iResult = -1;

            try
            {
                int? iTableID = string.IsNullOrEmpty(tableId) ? 0 : int.Parse(tableId);

                User _ObjUser = (User)System.Web.HttpContext.Current.Session["User"];
                _ObjUser.UserID.ToString();

                Record newRecord = new Record();

                newRecord.TableID = iTableID;
                newRecord.DateTimeRecorded = DateTime.Now;
                newRecord.IsActive = true;

                newRecord.V002 = null;

                if (jobNo != string.Empty)
                    newRecord.V002 = jobNo;

                if (date != string.Empty)
                    newRecord.V004 = date;

                if (amount != string.Empty)
                    newRecord.V007 = amount;

                if (description != string.Empty)
                    newRecord.V015 = description;

                if (activity != string.Empty)
                    newRecord.V029 = activity;

                if (isUnallocated != string.Empty)
                {
                    if (isUnallocated == "Null")
                    {
                        newRecord.V036 = null;
                    }
                    else
                    {
                        newRecord.V036 = isUnallocated;
                    }
                }

                newRecord.EnteredBy = int.Parse(_ObjUser.UserID.ToString());
                iResult = RecordManager.ets_Record_Insert(newRecord);

                data.AddObject(new JSONObject()
                   .AddString("Output", iResult.ToString()));
            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Bills and Payment Activity: AddUnallocatedPayments", ex.Message, ex.StackTrace, DateTime.Now, "Custom/Model/Windtech/ProcessRequest");
                SystemData.ErrorLog_Insert(theErrorLog);
            }

            return new JSONObject().AddList(data).ToString();
        }
        
        public static string Delete(int? recordId)
        {
            JSONList data = new JSONList("data");
            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;

                //sql here

            }

            return new JSONObject().AddList(data).ToString();
        }

        public static string Custom(int? recordId)
        {
            JSONList data = new JSONList("data");
            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                conn.Open();
                SqlCommand comm = new SqlCommand();
                comm.Connection = conn;

                //sql here

            }

            return new JSONObject().AddList(data).ToString();
        }
    }
}
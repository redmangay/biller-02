﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Configuration;

namespace CommonProcess
{
    public class ProcessRequest
    {

        public static string ReadTable(string strFrom, string strWhere, string strParamName001, string strParamValue001, string strParamName002, string strParamValue002)
        {
            JSONList data = new JSONList("data");

            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand comm = new SqlCommand();
                    comm.Connection = conn;

                    comm.CommandText = "SELECT * " +
                                        " FROM " + strFrom +
                                        " WHERE " + strWhere; //Role = @Role
                    comm.Parameters.Clear();
                    comm.Parameters.AddWithValue(strParamName001, strParamValue001); //@Role, "Role New"
                    comm.Parameters.AddWithValue(strParamName002, strParamValue002); //@Role, "Role New"

                    using (SqlDataReader reader = comm.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var jObjects = new JSONObject();
                            for (var i = 0; i < reader.FieldCount; i++)
                            {
                                jObjects.AddString(reader.GetName(i), reader[i]);
                            }
                            data.AddObject(jObjects);
                        }
                    }

                    return new JSONObject().AddList(data).ToString();
                    
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "Common: Process Request/ReadTable", ex.Message, ex.StackTrace, DateTime.Now, "Custom/Model/Common/ProcessRequest");
                    SystemData.ErrorLog_Insert(theErrorLog);
                }



                conn.Close();
                conn.Dispose();

                return new JSONObject().AddList(data).ToString();
            }
        }

    }
}
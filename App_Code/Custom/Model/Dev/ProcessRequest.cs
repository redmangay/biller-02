﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Configuration;
/// <summary>
/// Red: Process Request
/// </summary>
namespace Dev
{
    public class ProcessRequest
    {
        public static string Read(int? recordId)
        {
            JSONList data = new JSONList("data");

            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand comm = new SqlCommand();
                    comm.Connection = conn;

                    comm.CommandText = "SELECT * " +
                                        "FROM [Record] " +
                                        "WHERE RecordID= @RecordId";
                    comm.Parameters.Clear();
                    comm.Parameters.AddWithValue("RecordId", recordId);

                    using (SqlDataReader reader = comm.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var jObjects = new JSONObject();
                            for (var i = 0; i < reader.FieldCount; i++)
                            {
                                jObjects.AddString(reader.GetName(i), reader[i]);
                            }
                            data.AddObject(jObjects);
                        }
                    }

                    return new JSONObject().AddList(data).ToString();
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "Dev: Process Request/ReadSP", ex.Message, ex.StackTrace, DateTime.Now, "Custom/Model/Dev/ProcessRequest");
                    SystemData.ErrorLog_Insert(theErrorLog);
                }
                conn.Close();
                conn.Dispose();

                return new JSONObject().AddList(data).ToString();
            }
        }

        public static string ReadSP(int? recordId, string spName)
        {
            JSONList data = new JSONList("data");

            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                using (SqlCommand command = new SqlCommand(spName, conn))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    if (recordId != null)
                        command.Parameters.Add(new SqlParameter("@RecordID", recordId));
                                        
                        conn.Open();
                        command.Connection = conn;

                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var jObjects = new JSONObject();
                                for (var i = 0; i < reader.FieldCount; i++)
                                {
                                    jObjects.AddString(reader.GetName(i), reader[i]);
                                }
                                data.AddObject(jObjects);
                            }
                        }

                        return new JSONObject().AddList(data).ToString();
                    }
                    catch (Exception ex)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "Dev: Process Request/ReadSP", ex.Message, ex.StackTrace, DateTime.Now, "Custom/Model/Dev/ProcessRequest");
                        SystemData.ErrorLog_Insert(theErrorLog);
                    }
                    conn.Close();
                    conn.Dispose();

                    return new JSONObject().AddList(data).ToString();
                }
            }
        }

        public static string ReadSPWithValues(int? recordId, string strParam001, string strParam002, string strParam003, string strParam004,
                string strParam005, string strParam006, string strParam007, string strParam008, string strParam009, string strParam010, string spName)
        {
            JSONList data = new JSONList("data");

            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                using (SqlCommand command = new SqlCommand(spName, conn))
                {

                    command.CommandType = CommandType.StoredProcedure;

                    if (recordId != null)
                        command.Parameters.Add(new SqlParameter("@RecordID", recordId));

                    if (strParam001 != "")
                        command.Parameters.Add(new SqlParameter("@param001", strParam001));

                    if (strParam002 != "")
                        command.Parameters.Add(new SqlParameter("@param002", strParam002));

                    if (strParam003 != "")
                        command.Parameters.Add(new SqlParameter("@param003", strParam003));

                    if (strParam004 != "")
                        command.Parameters.Add(new SqlParameter("@param004", strParam004));

                    if (strParam005 != "")
                        command.Parameters.Add(new SqlParameter("@param005", strParam005));

                    if (strParam006 != "")
                        command.Parameters.Add(new SqlParameter("@param006", strParam006));

                    if (strParam007 != "")
                        command.Parameters.Add(new SqlParameter("@param007", strParam007));

                    if (strParam008 != "")
                        command.Parameters.Add(new SqlParameter("@param008", strParam008));

                    if (strParam009 != "")
                        command.Parameters.Add(new SqlParameter("@param009", strParam009));

                    if (strParam010 != "")
                        command.Parameters.Add(new SqlParameter("@param010", strParam010));

                        conn.Open();
                        command.Connection = conn;

                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var jObjects = new JSONObject();
                                for (var i = 0; i < reader.FieldCount; i++)
                                {
                                    jObjects.AddString(reader.GetName(i), reader[i]);
                                }
                                data.AddObject(jObjects);
                            }
                        }

                        return new JSONObject().AddList(data).ToString();
                    }
                    catch (Exception ex)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "Dev: Process Request/ReadSP", ex.Message, ex.StackTrace, DateTime.Now, "Custom/Model/Dev/ProcessRequest");
                        SystemData.ErrorLog_Insert(theErrorLog);
                    }
                    conn.Close();
                    conn.Dispose();

                    return new JSONObject().AddList(data).ToString();
                }
            }
        }

        public static string ReadSPByValue(string strValue, string spName)
        {
            JSONList data = new JSONList("data");

            using (SqlConnection conn = new SqlConnection(DBGurus.strGlobalConnectionString))
            {
                using (SqlCommand command = new SqlCommand(spName, conn))
                {

                    command.CommandType = CommandType.StoredProcedure;
                    if (!string.IsNullOrEmpty(strValue))
                        command.Parameters.Add(new SqlParameter("@sValue", strValue));

                    conn.Open();
                    command.Connection = conn;

                    try
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var jObjects = new JSONObject();
                                for (var i = 0; i < reader.FieldCount; i++)
                                {
                                    jObjects.AddString(reader.GetName(i), reader[i]);
                                }
                                data.AddObject(jObjects);
                            }
                        }

                        return new JSONObject().AddList(data).ToString();
                    }
                    catch (Exception ex)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "Dev: Process Request/ReadSPByValue", ex.Message, ex.StackTrace, DateTime.Now, "Custom/Model/Dev/ProcessRequest");
                        SystemData.ErrorLog_Insert(theErrorLog);
                    }
                    conn.Close();
                    conn.Dispose();

                    return new JSONObject().AddList(data).ToString();
                }
            }
        }
    }
}
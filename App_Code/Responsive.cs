﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
/// <summary>
/// Summary description for Responsive
/// </summary>
public class Responsive
{
	public Responsive()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static int Column_Fix_NullCoordinate(int nTableID)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Column_Fix_NullCoordinate", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nTableID", nTableID));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;

            }
        }
    }


    public static int? DeviceWidth()
    {
        if (HttpContext.Current != null)
        {
            if (HttpContext.Current.Session != null)
            {
                if (HttpContext.Current.Session["DeviceWidth"] != null)
                {
                    return int.Parse(HttpContext.Current.Session["DeviceWidth"].ToString());
                }
            }
        }

        return 1200;
    }

    public static int? DeviceHeight()
    {
        if (HttpContext.Current != null)
        {
            if (HttpContext.Current.Session != null)
            {
                if (HttpContext.Current.Session["DeviceHeight"] != null)
                {
                    return int.Parse(HttpContext.Current.Session["DeviceHeight"].ToString());
                }
            }
        }

        return 800;
    }



    public static string Device()
    {
        if (DeviceWidth() == null)
            return "md";
        int iW = (int)DeviceWidth();
        if(iW<=768)
        {
            return "xs";
        }
        else if(iW>768 && iW<=992)
        {
            return "sm";
        }
        else if (iW > 992 && iW <= 1200)
        {
            return "md";
        }
        else if (iW > 1200)
        {
            return "lg";
        }

        return "md";
    }
    public static int DeviceMaxXAxis()
    {
        string sDevice = Device();
        if(sDevice=="xs")
        {
            return 1;
        }
        else if (sDevice == "sm")
        {
            return 2;
        }
        else if (sDevice == "md")
        {
            return 4;
        }
        else if (sDevice == "lg")
        {
            return 8;
        }

        return 4;
    }

}
public enum DeviceType
{
    Phone_xs = 1,
    Tablet_sm = 2,
    Desktop_md = 3,
    Large_lg = 4

}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace LateDataManager
{
    public class LateData
    {

        public string ProcessLateData(string paramIn, out string paramOut)
        {
            paramOut = "";

            int TotalCount = LateDataMethod();

            //moved to EchotechBlastManager.cs
            //Import Dynamasters Blast data

            //if (System.Web.HttpContext.Current.Request.Url.Authority == "emd.thedatabase.net")
            //{
            //    try
            //    {
            //        EcotechBlastDataImport ecotechBlastDataImport = new EcotechBlastDataImport();
            //        ecotechBlastDataImport.ImportBlastEvents();
            //    }
            //    catch (Exception ex)
            //    {
            //        ErrorLog theErrorLog = new ErrorLog(null, " DBEmail -WinSchedules -EcotechBlastDataImport ", ex.Message, ex.StackTrace, DateTime.Now, System.Web.HttpContext.Current.Request.Path);
            //        SystemData.ErrorLog_Insert(theErrorLog);
            //    }
            //}

            return String.Format("Notification Count: {0} ", TotalCount);
        }

        protected int LateDataMethod()
        {
            try
            {

                int LateDataCount = 0;

                //KG 17/10/17 Ticket 3226
                //DataTable dtTables = Common.DataTableFromText(@"SELECT * FROM (SELECT      [Table].LateDataDays, Account.AccountID, [Table].TableName,[Table].TableID,
                //        (SELECT MAX(DateAdded)FROM Record WHERE Record.IsActive=1 AND Record.TableID=[Table].TableID) AS LastRecordDate,Account.SMTPEmail
                //        FROM Account INNER JOIN
                //          [Table] ON Account.AccountID = [Table].AccountID
                //          WHERE [Table].IsActive=1 AND Account.IsActive=1
                //          AND [Table].LateDataDays is not null) AS T
                //          WHERE LastRecordDate is not null
                //          AND cast (DATEADD(DAY,LateDataDays, LastRecordDate) as date) <CAST (Getdate() as date) ORDER BY AccountID");

                DataTable dtTables = Common.DataTableFromText(@"
                                SELECT * FROM 
                                    (SELECT [Table].LateDataDays,
                                            [Table].LateDataUnit,
                                            Account.AccountID,
                                            [Table].TableName,
                                            [Table].TableID,
                                            (SELECT MAX(DateAdded)
                                                FROM Record
                                                WHERE Record.IsActive = 1 AND Record.TableID =[Table].TableID) AS LastRecordDate, 
                                            Account.SMTPEmail
                                    FROM Account INNER JOIN[Table] ON Account.AccountID = [Table].AccountID
                                    WHERE[Table].IsActive = 1 AND Account.IsActive = 1
                                    AND[Table].LateDataDays is not null) AS T
                                WHERE LastRecordDate is not null
                                AND CAST (Getdate() as datetime) >
                                    CASE WHEN LateDataUnit = 'Hours'
                                    THEN CAST(DATEADD(HOUR, LateDataDays, LastRecordDate) as datetime)
                                    ELSE CAST(DATEADD(DAY, LateDataDays, LastRecordDate) as datetime) END
                                ORDER BY AccountID");

                //string strEmail = SystemData.SystemOption_ValueByKey("EmailFrom");
                //string strEmailServer = SystemData.SystemOption_ValueByKey("EmailServer");
                //string strEmailUserName = SystemData.SystemOption_ValueByKey("EmailUserName");
                //string strEmailPassword = SystemData.SystemOption_ValueByKey("EmailPassword");
                string strWarningSMSEMail = SystemData.SystemOption_ValueByKey_Account("WarningSMSEmail", null, null);
                //string strEnableSSL = SystemData.SystemOption_ValueByKey("EnableSSL");
                //string strSmtpPort = SystemData.SystemOption_ValueByKey("SmtpPort");


                string strAccountID = "-1";
                Content theContentEmail = null;
                Content theContentSMS = null;

                foreach (DataRow dr in dtTables.Rows)
                {


                    //check if it is in scheduledtask

                    int iTN = 0;

                    DataTable dtScheduledTask = ScheduleManager.dbg_ScheduledTask_Select(int.Parse(dr["AccountID"].ToString()), int.Parse(dr["TableID"].ToString()), "", "", "Late Data",
                        null, "", "", null, null, ref iTN, DateTime.Parse(dr["LastRecordDate"].ToString()));


                    ScheduledTask theScheduleTask = null;
                    if (dtScheduledTask.Rows.Count > 0)
                    {
                        //email was sent before

                        theScheduleTask = ScheduleManager.dbg_ScheduledTask_Detail(int.Parse(dtScheduledTask.Rows[0]["ScheduledTaskID"].ToString()));

                        //DateTime dtLastEmailSentDate = DateTime.Parse(DateTime.Parse(dr["LastRecordDate"].ToString()).ToShortDateString());
                        double dDayDifference = (DateTime.Today - (DateTime)theScheduleTask.LastEmailSentDate).TotalDays;
                        int iDayDiff = (int)dDayDifference;

                        if (iDayDiff <= 0)
                            iDayDiff = 1;

                        if (dtScheduledTask.Rows[0]["Frequency"].ToString().ToLower() == "t")
                        {
                            continue;//no need to send email for if
                        }

                        if (dtScheduledTask.Rows[0]["Frequency"].ToString().ToLower() == "w")
                        {
                            //week

                            if (iDayDiff % 7 == 0)
                            {

                            }
                            else
                            {
                                continue;//no need to send email for if
                            }

                        }

                        if (dtScheduledTask.Rows[0]["Frequency"].ToString().ToLower() == "m")
                        {
                            //week

                            if (iDayDiff % 30 == 0)
                            {

                            }
                            else
                            {
                                continue;//no need to send email for if
                            }

                        }


                    }

                    //lets send emails
                    if (strAccountID != dr["AccountID"].ToString())
                    {
                        strAccountID = dr["AccountID"].ToString();
                        theContentEmail = SystemData.Content_Details_ByKey("LateWarningEmail", int.Parse(dr["AccountID"].ToString()));
                        theContentSMS = SystemData.Content_Details_ByKey("LateWarningSMS", int.Parse(dr["AccountID"].ToString()));
                    }


                    Guid guidNew = Guid.NewGuid();
                    string strEmailUID = guidNew.ToString();


                    if (theContentEmail != null && theContentSMS != null)
                    {
                        string strBody = theContentEmail.ContentP;

                        string strBodySMS = theContentSMS.ContentP;

                        strBody = strBody.Replace("[DateTime]", dr["LastRecordDate"].ToString());
                        strBody = strBody.Replace("[Table]", dr["TableName"].ToString());
                        strBody = strBody.Replace("[LateDataDays]", dr["LateDataDays"].ToString() + " " + dr["LateDataUnit"].ToString().Replace("s", "(s)"));
                        strBody = strBody.Replace("[week]", System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath +
                            "/LateDataSnooze.aspx?AccountID=" + Cryptography.Encrypt(dr["AccountID"].ToString())
                            + "&TableID=" + Cryptography.Encrypt(dr["TableID"].ToString()) + "&Period=week&emailuid="
                            + Cryptography.Encrypt(strEmailUID) + "&datetime=" + Cryptography.Encrypt(dr["LastRecordDate"].ToString()));
                        strBody = strBody.Replace("[month]", System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath +
                            "/LateDataSnooze.aspx?AccountID=" + Cryptography.Encrypt(dr["AccountID"].ToString())
                            + "&TableID=" + Cryptography.Encrypt(dr["TableID"].ToString()) + "&Period=month&emailuid="
                            + Cryptography.Encrypt(strEmailUID) + "&datetime=" + Cryptography.Encrypt(dr["LastRecordDate"].ToString()));
                        strBody = strBody.Replace("[turnoff]", System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath +
                            "/LateDataSnooze.aspx?AccountID=" + Cryptography.Encrypt(dr["AccountID"].ToString())
                            + "&TableID=" + Cryptography.Encrypt(dr["TableID"].ToString()) + "&Period=turnoff&emailuid="
                            + Cryptography.Encrypt(strEmailUID) + "&datetime=" + Cryptography.Encrypt(dr["LastRecordDate"].ToString()));


                        strBodySMS = strBodySMS.Replace("[DateTime]", dr["LastRecordDate"].ToString());
                        strBodySMS = strBodySMS.Replace("[Table]", dr["TableName"].ToString());
                        strBodySMS = strBodySMS.Replace("[LateDataDays]", dr["LateDataDays"].ToString());

                        strBodySMS = strBodySMS.Replace("[week]", System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath +
                           "/LateDataSnooze.aspx?AccountID=" + Cryptography.Encrypt(dr["AccountID"].ToString())
                           + "&TableID=" + Cryptography.Encrypt(dr["TableID"].ToString()) + "&Period=week&emailuid="
                           + Cryptography.Encrypt(strEmailUID) + "&datetime=" + Cryptography.Encrypt(dr["LastRecordDate"].ToString()));
                        strBodySMS = strBodySMS.Replace("[month]", System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath +
                          "/LateDataSnooze.aspx?AccountID=" + Cryptography.Encrypt(dr["AccountID"].ToString())
                          + "&TableID=" + Cryptography.Encrypt(dr["TableID"].ToString()) + "&Period=month&emailuid="
                          + Cryptography.Encrypt(strEmailUID) + "&datetime=" + Cryptography.Encrypt(dr["LastRecordDate"].ToString()));
                        strBodySMS = strBodySMS.Replace("[turnoff]", System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority + System.Web.HttpContext.Current.Request.ApplicationPath +
                          "/LateDataSnooze.aspx?AccountID=" + Cryptography.Encrypt(dr["AccountID"].ToString())
                          + "&TableID=" + Cryptography.Encrypt(dr["TableID"].ToString()) + "&Period=turnoff&emailuid="
                          + Cryptography.Encrypt(strEmailUID) + "&datetime=" + Cryptography.Encrypt(dr["LastRecordDate"].ToString()));

                        //KG 17/10/17 Ticket 3226
                        Account theAccount = SecurityManager.Account_Details(int.Parse(dr["AccountID"].ToString()));
                        string strSubject = theContentEmail.Heading.Replace("[Table]", dr["TableName"].ToString()).Replace("[Account]", theAccount.AccountName);
                        DataTable dtUsersEmail = RecordManager.ets_TableUser_Select(null,
                         int.Parse(dr["TableID"].ToString()), null, true, null, null, null, null, null, null, null, null, null);

                        string strTempBody = strBody;

                        foreach (DataRow drU in dtUsersEmail.Rows)
                        {

                            string strTo = drU["Email"].ToString();
                            strTempBody = strBody;
                            strTempBody = strTempBody.Replace("[FullName]", drU["UserName"].ToString());
                            strTempBody = strTempBody.Replace("[FirstName]", drU["FirstName"].ToString());
                            try
                            {
                                if (theScheduleTask != null)
                                {
                                    theScheduleTask.LastEmailSentDate = DateTime.Now;
                                    ScheduleManager.dbg_ScheduledTask_Update(theScheduleTask);
                                }


                                string sSendEmailError = "";

                                //Message theMessage = new Message(null, null, null, null,
                                //    DateTime.Now, "E", "E",
                                //            null, strTo, strSubject, strTempBody, null, strEmailUID);

                                //KG 4/1/2018 - Save TableID and AccountID
                                Message theMessage = new Message(null, null, int.Parse(dr["TableID"].ToString()), int.Parse(dr["AccountID"].ToString()),
                                            DateTime.Now, "E", "E", null, strTo, strSubject, strTempBody, null, strEmailUID); 

                                DBGurus.SendEmail(theContentEmail.ContentKey, true, null, strSubject, strTempBody, "",
                                    strTo, "", "",
                                     null, theMessage, out sSendEmailError);

                                LateDataCount = LateDataCount + 1;

                            }
                            catch (Exception ex)
                            {

                                //strErrorMsg = "Server could not send warning Email & SMS";
                            }


                        }


                        strSubject = theContentSMS.Heading.Replace("[Table]", dr["TableName"].ToString()).Replace("[Account]", theAccount.AccountName);


                        DataTable dtUsersSMS = RecordManager.ets_TableUser_Select(null,
                      int.Parse(dr["TableID"].ToString()), null, null, true, null, null, null, null, null, null, null, null);

                        foreach (DataRow drs in dtUsersSMS.Rows)
                        {
                            //msg.To.Clear();
                            if (drs["PhoneNumber"] != DBNull.Value)
                            {
                                if (drs["PhoneNumber"].ToString() != "")
                                {
                                    //msg.To.Add(drs["PhoneNumber"].ToString() + strWarningSMSEMail);
                                    string strTo = drs["PhoneNumber"].ToString() + strWarningSMSEMail;

                                    strTempBody = strBodySMS;
                                    strTempBody = strTempBody.Replace("[FullName]", drs["UserName"].ToString());
                                    strTempBody = strTempBody.Replace("[FirstName]", drs["FirstName"].ToString());
                                    strTempBody = strTempBody.Replace("[Table]", dr["TableName"].ToString()).Replace("[Account]", theAccount.AccountName);
                                    strTempBody = strTempBody.Replace("[DateTime]", dr["LastRecordDate"].ToString());
                                    strTempBody = strTempBody.Replace("[LateDataDays]", dr["LateDataDays"].ToString() + " " + dr["LateDataUnit"].ToString().Replace("s", "(s)"));
                                    try
                                    {


                                        string sSendEmailError = "";

                                        Message theMessage = new Message(null, null, int.Parse(dr["TableID"].ToString()), int.Parse(dr["AccountID"].ToString()),
                   DateTime.Now, "W", "S",
                       null, strTo, strSubject, strTempBody, null, "");

                                        LateDataCount = LateDataCount + 1;

                                        DBGurus.SendEmail(theContentSMS.ContentKey, null, true, strSubject, strTempBody, "",
                                            strTo, "", "", null, theMessage, out sSendEmailError);


                                    }
                                    catch (Exception)
                                    {

                                        //strErrorMsg = "Server could not send warning Email & SMS";
                                    }
                                }
                            }
                        }

                    }





                }

                return LateDataCount;


            }
            catch (Exception ex)
            {
                //
                ErrorLog theErrorLog = new ErrorLog(null, "Late Data", ex.Message, ex.StackTrace, DateTime.Now, System.Web.HttpContext.Current.Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);

                return 0;
            }

        }
    }
}
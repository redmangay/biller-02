﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace XlsxExport
{
    public class XlsxGraphExport
    {
        public void ExportGraph(MemoryStream stream,
            int? tableId,
            string chartHeader,
            string chartSubHeader,
            string dataColumnSystemName,
            List<string> seriesIds,
            bool limits,
            string warningCaption,
            string warningStringValue,
            string warningColor,
            string exceedanceCaption,
            string exceedancStringeValue,
            string exceedanceColor,

            /* === Red: Charting Min variables === */
            string warningMinCaption,
            string warningMinColor,
            string warningStringMinValue,
            string exceedanceMinCaption,
            string exceedanceMinColor,
            string exceedanceStringMinValue,
            List<Tuple<int, string>> listSeriesColors,
            int seriesSelectedColumnID,
            /* === end Red === */

            string dateFrom,
            string timeFrom,
            string dateTo,
            string timeTo,
            int chartType,
            bool useLightText = true)
        {
            using (XlsxWriter xlsxWriter = new XlsxWriter(stream))
            {
                if (tableId.HasValue)
                {
                    int xAxisColumnId = 0;
                    //int seriesColumnId = 0;
                    int dataColumnId = 0;
                    Table table = RecordManager.ets_Table_Details(tableId.Value);
                    if (table != null)
                    {
                        xAxisColumnId = table.GraphXAxisColumnID ?? 0;
                        /* Red: Charting, we get the selected seriesColumnId, not the default?  */
                         //seriesColumnId = table.GraphSeriesColumnID ?? 0;
                    }
                    Column dataColumn = RecordManager.ets_Column_Details_By_Sys(tableId.Value, dataColumnSystemName);
                    if (dataColumn != null)
                    {
                        dataColumnId = dataColumn.ColumnID ?? 0;
                    }

                    double? warningValue = null;
                    double? exceedanceValue = null;
                    if (limits)
                    {
                        double doubleValue;
                        if (double.TryParse(warningStringValue, out doubleValue))
                            warningValue = doubleValue;
                        if (double.TryParse(exceedancStringeValue, out doubleValue))
                            exceedanceValue = doubleValue;
                    }

                    /* === Red: Charting, convert the values === */
                    double? warningMinValue = null;
                    double? exceedanceMinValue = null;
                    if (limits)
                    {
                        double doubleValue;
                        if (double.TryParse(warningStringMinValue, out doubleValue))
                            warningMinValue = doubleValue;
                        if (double.TryParse(exceedanceStringMinValue, out doubleValue))
                            exceedanceMinValue = doubleValue;
                    }
                    /* === end Red === */

                    WhereClause whereClausePeriod = null;
                    whereClausePeriod = new WhereClause(xAxisColumnId, "greaterthanequal",
                        dateFrom + (String.IsNullOrEmpty(timeFrom) ? "" : (" " + timeFrom)));
                    whereClausePeriod.And(xAxisColumnId, "lessthanequal",
                        dateTo + (String.IsNullOrEmpty(timeFrom) ? "" : (" " + timeTo)));

                    WhereClause whereClauseSeries = null;
                    Column seriesColumn = RecordManager.ets_Column_Details(seriesSelectedColumnID);
                    if (seriesColumn != null && seriesIds.Count > 0)
                    {
                        string filterString = "[Record].[" + seriesColumn.SystemName + "] IN (";
                        foreach (string id in seriesIds)
                        {
                            filterString += "'" + id + "', ";
                        }
                        filterString = filterString.Substring(0, filterString.Length - 2);
                        filterString += ")";

                        whereClauseSeries = new WhereClause(filterString);
                    }

                    WhereClause whereClause = null;
                    if (whereClauseSeries == null)
                        whereClause = whereClausePeriod;
                    else
                    {
                        whereClause = new WhereClause(whereClausePeriod);
                        whereClause.And(whereClauseSeries);
                    }

                    xlsxWriter.CreateChartWorksheet("Chart",
                        tableId.Value, dataColumnId, xAxisColumnId, seriesSelectedColumnID,
                        whereClause,
                        chartHeader, chartSubHeader,
                        warningCaption, warningValue, warningColor,
                        exceedanceCaption, exceedanceValue, exceedanceColor,
                        /* === Red: Charting, pass the values === */
                        warningMinCaption,
                        warningMinColor,
                        warningMinValue, 
                        exceedanceMinCaption,
                        exceedanceMinColor,
                        exceedanceMinValue,listSeriesColors,
                        /* === end Red === */
                        chartType, useLightText);

                    xlsxWriter.FinalizeWorkbook();
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeDoctor.Models
{
    public class WorkLog
    {
        public int length { get; set; }
        public int user_id { get; set; }
        public string user_name { get; set; }
        public int task_id { get; set; }
        public string task_name { get; set; }
        public int? project_id { get; set; }
        public string project_name { get; set; }
        public DateTime start_time { get; set; }
        public DateTime end_time { get; set; }
        public int edited { get; set; }
        public int work_mode { get; set; }
        public string last_modified { get; set; }
    }
}
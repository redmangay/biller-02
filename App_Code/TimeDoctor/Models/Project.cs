﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeDoctor.Models
{
    public class Project
    {
        public int project_id { get; set; }
        public string project_name { get; set; }
        public bool archived { get; set; }
    }
}
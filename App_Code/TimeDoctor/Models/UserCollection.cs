﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeDoctor.Models
{
    public class UserCollection
    {
        public int count { get; set; }
        public List<User> users { get; set; }
    }
}
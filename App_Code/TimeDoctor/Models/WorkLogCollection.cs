﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeDoctor.Models
{
    public class WorklogCollection
    {
        public WorkLogs worklogs { get; set; }
    }

    public class WorkLogs
    {
        public int count { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
        public List<WorkLog> items { get; set; }
    }
}
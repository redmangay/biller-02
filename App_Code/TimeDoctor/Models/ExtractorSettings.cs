﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeDoctor.Models
{
    public class ExtractorSettings
    {
        public string ApiUrl { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime? LastExtracting { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeDoctor.Models
{
    public class ProjectCollection
    {
        public int count { get; set; }
        public List<Project> projects { get; set; }
    }
}
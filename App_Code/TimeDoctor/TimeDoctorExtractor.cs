﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using TimeDoctor.DAL;
using TimeDoctor.Models;
using System.Data.SqlClient;
using System.Data;
namespace TimeDoctor
{
    public class TimeDoctorExtractor
    {
        string connectionString = DBGurus.strGlobalConnectionString;
        ExtractorSettings settings = null;

        public string Execute(string args, out string output)
        {
            DateTime lastExtracting = DateTime.Now.Date.AddMonths(-1); // If we have not done any extracting, the first run will get data for last 1 month
            output = "";
            StringBuilder sb = new StringBuilder();
            try
            {
                settings = JsonConvert.DeserializeObject<ExtractorSettings>(args);
            }
            catch
            {
                output = "Invalid settings for TimeDoctor Extractor";
            }
            if (String.IsNullOrEmpty(output))
            {
                if (settings.LastExtracting.HasValue)
                {
                    lastExtracting = settings.LastExtracting.Value;
                }
                try
                {


                    //Check token expired
                    try
                    {
                        Company Testcompany = GetCompany();
                    }
                    catch
                    {
                        //error, so lets try with new token
                        string url = String.Format("https://webapi.timedoctor.com/oauth/v2/token?client_id={0}&client_secret={1}&grant_type=refresh_token&refresh_token={2}", settings.ClientId, settings.ClientSecret, settings.RefreshToken);
                        string text = GetResponseText(url);
                        RefreshTokenMessage rt = JsonConvert.DeserializeObject<RefreshTokenMessage>(text);
                        settings.AccessToken = rt.access_token;
                        settings.RefreshToken = rt.refresh_token;
                        using (ExtractorSettingsDb db = new ExtractorSettingsDb(connectionString))
                        {
                            db.Save(settings);
                        }

                    }


                    // Extract data
                    Company company = GetCompany();
                    if (company != null)
                    {
                        // Get users
                        UserCollection userCollection = GetUsers(company.user.company_id);
                        if (userCollection.count > 0)
                        {
                            using (UserDb db = new UserDb(connectionString))
                            {
                                foreach (TimeDoctor.Models.User user in userCollection.users)
                                {
                                    db.Save(user);
                                }
                            }
                        }
                        sb.AppendLine("- Users: " + userCollection.count.ToString());

                        // Get Projects
                        ProjectCollection projectCollection = GetProjects(company.user.company_id, company.user.user_id);
                        if (projectCollection.count > 0)
                        {
                            using (ProjectDb db = new ProjectDb(connectionString))
                            {
                                foreach (Project project in projectCollection.projects)
                                {
                                    db.Save(project);
                                }
                            }
                        }
                        sb.AppendLine("- Projects: " + userCollection.count.ToString());

                        // Get worklogs (includes task, worklog and worker)                        
                        settings.LastExtracting = DateTime.Now;
                        int counter = 0;						
                        WorklogCollection worklogCollection = new WorklogCollection()
                        {
                            worklogs = new WorkLogs()
                            {
                                count = 1
                            }
                        };
                        while (counter == 0 || counter < worklogCollection.worklogs.count || worklogCollection.worklogs.items.Count > 0)
                        {
                            worklogCollection = GetWorkLogs(company.user.company_id, lastExtracting, counter);
                            if (worklogCollection.worklogs.items.Count > 0)
                            {
                                using (WorkLogDb db = new WorkLogDb(connectionString))
                                {
                                    foreach (WorkLog worklog in worklogCollection.worklogs.items)
                                    {
                                        db.Save(worklog);
                                        counter++;
                                    }
                                }
                            }                            
                        }
                        sb.AppendLine("- Worklogs: " + counter.ToString());


                        using (ExtractorSettingsDb db = new ExtractorSettingsDb(connectionString))
                        {
                            db.Save(settings);
                        }

                        //Run JC_TimeDoctor_ImportTimes - MR
                        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
                        {
                            using (SqlCommand command = new SqlCommand("JC_TimeDoctor_ImportTimes", connection))
                            {
                                command.CommandType = CommandType.StoredProcedure;
                                connection.Open();
                                try
                                {
                                    command.ExecuteNonQuery();
                                }
                                catch
                                {
                                    //
                                }
                                connection.Close();
                                connection.Dispose();
                            }
                        }

                    }
                    else
                    {
                        sb.AppendLine("- Company could not be found");
                    }

                }
                catch (Exception ex)
                {
					sb.AppendLine("ERROR:" + ex.Message);
                    output = ex.Message;
                }
            }
            return sb.ToString();
        }

        private Company GetCompany()
        {
            string url = String.Format("{0}/companies?access_token={1}&_format=json", settings.ApiUrl, settings.AccessToken);
            string text = GetResponseText(url);
            Company company = JsonConvert.DeserializeObject<Company>(text);
            return company;
        }

        private UserCollection GetUsers(int companyId)
        {
            string url = String.Format("{0}/companies/{1}/users?access_token={2}&_format=json", settings.ApiUrl, companyId, settings.AccessToken);
            string text = GetResponseText(url);
            UserCollection userCollection = JsonConvert.DeserializeObject<UserCollection>(text);
            return userCollection;
        }

        private ProjectCollection GetProjects(int companyId, int userId)
        {
            string url = String.Format("{0}/companies/{1}/users/{2}/projects?access_token={3}&_format=json&limit=1000", settings.ApiUrl, companyId, userId, settings.AccessToken);
            string text = GetResponseText(url);
			text = text.Replace("\"archived\":\"n\\/a\"", "\"archived\":false");
            ProjectCollection projects = JsonConvert.DeserializeObject<ProjectCollection>(text);
            return projects;
        }

        private WorklogCollection GetWorkLogs(int companyId, DateTime lastModified, int offset)
        {
            string url = String.Format("{0}/companies/{1}/worklogs?access_token={2}&_format=json&consolidated=0&offset={3}&limit=500&last_modified={4}", settings.ApiUrl, companyId, settings.AccessToken, offset, lastModified.ToString("yyyy-MM-dd"));
            string text = GetResponseText(url);
            WorklogCollection worklogCollection = JsonConvert.DeserializeObject<WorklogCollection>(text);
            return worklogCollection;
        }

        private string GetResponseText(string url)
        {
            string content = "";
            WebRequest request = WebRequest.Create(url);
            using (WebResponse response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        content = reader.ReadToEnd();
                    }
                }
            }
            return content;
        }
    }
}
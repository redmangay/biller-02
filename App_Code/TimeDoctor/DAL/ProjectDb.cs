﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TimeDoctor.Models;
using Dapper;

namespace TimeDoctor.DAL
{
    public class ProjectDb: IDisposable
    {        
        protected SqlConnection connection;

        public ProjectDb(string connectionString)
        {
            connection = new SqlConnection(connectionString);
        }

        public void Save(Project project)
        {
            connection.Execute("TimeDoctor_Project_Save", new
            {
                RefID = project.project_id,
                Name = project.project_name,
                Archived = project.archived
            }, commandType: System.Data.CommandType.StoredProcedure);
        }
                
        public void Dispose()
        {
            if (connection != null)
            {
                connection.Dispose();
            }
        }
    }
}
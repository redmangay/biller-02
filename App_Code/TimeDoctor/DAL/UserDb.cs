﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TimeDoctor.Models;
using Dapper;

namespace TimeDoctor.DAL
{
    public class UserDb: IDisposable
    {        
        protected SqlConnection connection;

        public UserDb(string connectionString)
        {
            connection = new SqlConnection(connectionString);
        }

        public void Save(TimeDoctor.Models.User user)
        {
            connection.Execute("TimeDoctor_User_Save", new
            {
                RefID = user.user_id,
                Username = user.full_name,
                FirstName = user.first_name,
                LastName = user.last_name,
                Email = user.email,
                Avatar = user.avatar                
            }, commandType: System.Data.CommandType.StoredProcedure);
        }
                
        public void Dispose()
        {
            if (connection != null)
            {
                connection.Dispose();
            }
        }
    }
}
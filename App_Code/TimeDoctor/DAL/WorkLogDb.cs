﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using TimeDoctor.Models;

public class WorkLogDb : IDisposable
{
    protected SqlConnection connection;

    public WorkLogDb(string connectionString)
    {
        connection = new SqlConnection(connectionString);
    }

    public void Save(WorkLog worklog)
    {
        connection.Execute("TimeDoctor_WorkLog_Save", new
        {
            ProjectRefID = worklog.project_id,
            ProjectName = worklog.project_name,
            TaskRefID = worklog.task_id,
            TaskName = worklog.task_name,
            UserRefID = worklog.user_id,
            UserName = worklog.user_name,
            LengthInSeconds = worklog.length,
            StartTime = worklog.start_time,
            EndTime = worklog.end_time,
            WorkMode = worklog.work_mode,
            Edited = worklog.edited == 1,
            LastModified = worklog.last_modified
        }, commandType: System.Data.CommandType.StoredProcedure);
    }

    public void Dispose()
    {
        if (connection != null)
        {
            connection.Dispose();
        }
    }
}
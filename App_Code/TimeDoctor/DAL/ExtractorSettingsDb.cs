﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using TimeDoctor.Models;
using Newtonsoft.Json;

/// <summary>
/// Summary description for ExtractorSettingsDb
/// </summary>
public class ExtractorSettingsDb: IDisposable
{
    protected SqlConnection connection;

    public ExtractorSettingsDb(string connectionString)
    {
        connection = new SqlConnection(connectionString);
    }

    public void Save(ExtractorSettings settings)
    {
        connection.Execute("TimeDoctor_ExtractorSettings_Save", new
        {
            Settings = JsonConvert.SerializeObject(settings)
        }, commandType: System.Data.CommandType.StoredProcedure);
    }

    public void Dispose()
    {
        if (connection != null)
        {
            connection.Dispose();
        }
    }
}
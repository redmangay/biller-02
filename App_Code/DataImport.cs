﻿using GenericParsing;
using System.Data;

namespace DataImport
{
    public class DataImport
    {
        public DataImport()
        {
        }

        public DataTable TestProcessRoutine(string strImportFolder, string strFileUniqueName, out string msg)
        {
            msg = "";
            DataTable dt = null;

            using (GenericParserAdapter parser = new GenericParserAdapter(strImportFolder + "\\" + strFileUniqueName,
                System.Text.Encoding.GetEncoding("iso-8859-1")))
            {
                dt = parser.GetDataTable();
            }
            dt.Columns[0].ColumnName = "Sample Site";
            dt.Columns[1].ColumnName = "Date/Time Sampled";
            dt.Columns[2].ColumnName = "Sample Value";
            dt.AcceptChanges();

            return dt;
        }
    }
}
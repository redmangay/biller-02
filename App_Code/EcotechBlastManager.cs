﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//added by KG for RegularTask 17-07-2017
namespace EcotechBlastManager
{
    public class EcotechBlastData
    {
        public string ProcessEchotechBlastData(string paramIn, out string paramOut)
        {
            paramOut = "";
            string returnStr = "";
            try
            {
                EcotechBlastDataImport ecotechBlastDataImport = new EcotechBlastDataImport();
                returnStr = ecotechBlastDataImport.ImportBlastEvents();
            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "RegularTask EcotechBlastManager - ProcessEchotechBlastData()"
                    , ex.Message, ex.StackTrace, DateTime.Now, System.Web.HttpContext.Current.Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }

            return returnStr;
        }
    }
}
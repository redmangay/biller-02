﻿using GenericParsing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Xml;


/// <summary>
/// Summary description for UploadWorld
/// </summary>
/// 

public partial class UploadWorld
{
    public int TableID;
    public UploadWorld(int iTableID, int? iImportTemplateID)
    {
        TableID = iTableID;
    }


    #region Condition

    public static int dbg_Condition_Insert(Condition p_Condition)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {

            using (SqlCommand command = new SqlCommand("dbg_Condition_Insert", connection))
            {

                command.CommandType = CommandType.StoredProcedure;

                SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int);
                pRV.Direction = ParameterDirection.Output;

                command.Parameters.Add(pRV);

                command.Parameters.Add(new SqlParameter("@nColumnID", p_Condition.ColumnID));
                command.Parameters.Add(new SqlParameter("@sConditionType", p_Condition.ConditionType));
                command.Parameters.Add(new SqlParameter("@nCheckColumnID", p_Condition.CheckColumnID));
                command.Parameters.Add(new SqlParameter("@sCheckFormula", p_Condition.CheckFormula));

                if (p_Condition.CheckValue != "")
                    command.Parameters.Add(new SqlParameter("@sCheckValue", p_Condition.CheckValue));

                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Dispose();
                    return Int32.Parse(pRV.Value.ToString());
                }
                catch
                {
                    connection.Close();
                    connection.Dispose();

                }
                return -1;
            }
        }



    }

    public static int dbg_Condition_Update(Condition p_Condition)
    {



        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("dbg_Condition_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;


                command.Parameters.Add(new SqlParameter("@nConditionID", p_Condition.ConditionID));


                command.Parameters.Add(new SqlParameter("@nColumnID", p_Condition.ColumnID));
                command.Parameters.Add(new SqlParameter("@sConditionType", p_Condition.ConditionType));
                command.Parameters.Add(new SqlParameter("@nCheckColumnID", p_Condition.CheckColumnID));
                command.Parameters.Add(new SqlParameter("@sCheckFormula", p_Condition.CheckFormula));
                if (p_Condition.CheckValue != "")
                    command.Parameters.Add(new SqlParameter("@sCheckValue", p_Condition.CheckValue));



                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;


            }

        }



    }

    public static int dbg_Condition_Delete(int nConditionID)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("dbg_Condition_Delete", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nConditionID ", nConditionID));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;

            }
        }
    }


    public static Condition dbg_Condition_Detail(int nConditionID)
    {


        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {

            using (SqlCommand command = new SqlCommand("dbg_Condition_Detail", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nConditionID", nConditionID));

                connection.Open();


                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Condition temp = new Condition(
                                (int)reader["ConditionID"], (int)reader["ColumnID"], (string)reader["ConditionType"],
                                (int)reader["CheckColumnID"],
                              (string)reader["CheckFormula"], (string)reader["CheckValue"]
                                );

                            connection.Close();
                            connection.Dispose();

                            return temp;
                        }

                    }
                }
                catch
                {

                }
                connection.Close();
                connection.Dispose();



                return null;

            }

        }



    }


    public static string Condition_GetFormula(int? ColumnID, int? CheckColumnID, string ConditionType, string CheckValue)
    {
        string strFormula = "";

        DataTable dtCondition = dbg_Condition_Select(ColumnID, CheckColumnID, ConditionType, CheckValue);
        if (dtCondition != null && dtCondition.Rows.Count > 0)
        {
            if (dtCondition.Rows[0]["CheckFormula"] != DBNull.Value)
            {
                strFormula = dtCondition.Rows[0]["CheckFormula"].ToString();
            }
        }
        return strFormula;
    }

    public static string Condition_GetFormulaHTMLTable(Column theColumn, string ConditionType, string CheckValue)
    {
        string strFormula = "";
        string strCheckColumnID = Common.GetValueFromSQL("SELECT TOP 1 CheckColumnID FROM [Condition] Con WHERE Con.ConditionType='" + ConditionType + "'AND Con.ColumnID=" + theColumn.ColumnID.ToString());
        Column theCheckColumn = null;
        if (strCheckColumnID != "")
        {
            theCheckColumn = RecordManager.ets_Column_Details(Int32.Parse(strCheckColumnID));
        }

        if (theCheckColumn == null)
            return strFormula;

        DataTable dtCondition = dbg_Condition_Select(theColumn.ColumnID, theCheckColumn.ColumnID, ConditionType, CheckValue);
        if (dtCondition != null && dtCondition.Rows.Count > 0)
        {
            strFormula = @"<table  style=""border-color: #600;border-width: 0 0 1px 1px;border-style: solid;border-collapse:collapse;"">
                               <tr >
                                   <td style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">
                                       <strong>When</strong>
                                   </td>
                                   <td style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">
                                       <strong>Is</strong>
                                   </td>
                                   <td style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">
                                       <strong></strong>
                                   </td>
                               </tr>";
            foreach (DataRow dr in dtCondition.Rows)
            {

                string strDisplayName = dr["DisplayName"].ToString();
                string strCheckValue = Common.GetDisplayTextFromColumnAndValue(theCheckColumn, dr["CheckValue"].ToString());

                string strCheckFormula = dr["CheckFormula"].ToString();
                strCheckFormula = Common.GetFromulaMsg("", theColumn.DisplayName, strCheckFormula);
                strCheckFormula = strCheckFormula.Replace("<br/>", " and ");
                try
                {
                    strDisplayName = WebUtility.HtmlEncode(strDisplayName);
                    strCheckValue = WebUtility.HtmlEncode(strCheckValue);
                    strCheckFormula = WebUtility.HtmlEncode(strCheckFormula);
                }
                catch
                {
                    //
                }


                strFormula = strFormula + @"<tr >
                                    <td  style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">" + dr["DisplayName"].ToString() + @"</td>
                                    <td  style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">" + strCheckValue + @"</td>
                                    <td  style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">" + strCheckFormula + @"</td>
                                </tr>";
            }

            strFormula = strFormula + "</table>";
        }
        return strFormula;
    }

    public static string Condition_GetFormula_Full(int? ColumnID, string ConditionType)
    {
        string strFormula = "";

        DataTable dtCondition = dbg_Condition_Select(ColumnID, null, ConditionType, "");
        if (dtCondition != null && dtCondition.Rows.Count > 0)
        {
            foreach (DataRow dr in dtCondition.Rows)
            {
                strFormula = strFormula + dr["CheckColumnID"].ToString() + dr["CheckFormula"].ToString() + (dr["CheckValue"] == DBNull.Value ? "" : dr["CheckValue"].ToString());
            }
            if (strFormula.Length > 0)
                strFormula = strFormula.ToUpper();
        }
        return strFormula;
    }

    public static DataTable dbg_Condition_Select(int? ColumnID, int? CheckColumnID, string ConditionType, string CheckValue)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("dbg_Condition_Select", connection))
            {
                command.CommandType = CommandType.StoredProcedure;



                command.Parameters.Add(new SqlParameter("@ColumnID", ColumnID));

                if (CheckColumnID != null)
                    command.Parameters.Add(new SqlParameter("@CheckColumnID", CheckColumnID));


                if (ConditionType != "")
                    command.Parameters.Add(new SqlParameter("@ConditionType", ConditionType));

                if (CheckValue != "")
                    command.Parameters.Add(new SqlParameter("@CheckValue", CheckValue));

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                //System.Data.DataSet ds = new System.Data.DataSet();
                connection.Open();
                try
                {
                    da.Fill(dt);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();

                return dt;

            }
        }
    }


    #endregion


    #region Advanced Conditions

    public static int ets_AdvancedCondition_Insert(AdvancedCondition advancedCondition)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_AdvancedCondition_Insert", connection))
            {

                command.CommandType = CommandType.StoredProcedure;
                SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                command.Parameters.Add(pRV);

                command.Parameters.Add(new SqlParameter("@nColumnID", advancedCondition.ColumnID));
                if (advancedCondition.ConditionColumnID.HasValue)
                    command.Parameters.Add(new SqlParameter("@nConditionColumnID", advancedCondition.ConditionColumnID.Value));
                command.Parameters.Add(new SqlParameter("@sConditionType", advancedCondition.ConditionType));
                if (!String.IsNullOrEmpty(advancedCondition.ConditionSubType))
                    command.Parameters.Add(new SqlParameter("@sConditionSubType", advancedCondition.ConditionSubType));
                if (!String.IsNullOrEmpty(advancedCondition.ConditionColumnValue))
                    command.Parameters.Add(new SqlParameter("@sConditionColumnValue", advancedCondition.ConditionColumnValue));
                if (!String.IsNullOrEmpty(advancedCondition.ConditionOperator))
                    command.Parameters.Add(new SqlParameter("@sConditionOperator", advancedCondition.ConditionOperator));
                command.Parameters.Add(new SqlParameter("@nDisplayOrder", advancedCondition.DisplayOrder));
                if (!String.IsNullOrEmpty(advancedCondition.JoinOperator))
                    command.Parameters.Add(new SqlParameter("@sJoinOperator", advancedCondition.JoinOperator));
                if (!String.IsNullOrEmpty(advancedCondition.Status))
                    command.Parameters.Add(new SqlParameter("@Status", advancedCondition.Status));

                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Dispose();
                    return Int32.Parse(pRV.Value.ToString());
                }
                catch (Exception)
                {
                    connection.Close();
                    connection.Dispose();
                }
                return -1;
            }
        }
    }

    public static int ets_AdvancedCondition_Update(AdvancedCondition advancedCondition)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_AdvancedCondition_Update", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@nAdvancedConditionID", advancedCondition.AdvancedConditionID));
                command.Parameters.Add(new SqlParameter("@sConditionType", advancedCondition.ConditionType));
                command.Parameters.Add(new SqlParameter("@sConditionSubType", advancedCondition.ConditionSubType));
                command.Parameters.Add(new SqlParameter("@nColumnID", advancedCondition.ColumnID));
                command.Parameters.Add(new SqlParameter("@nConditionColumnID", advancedCondition.ConditionColumnID));
                command.Parameters.Add(new SqlParameter("@sConditionColumnValue", advancedCondition.ConditionColumnValue));
                command.Parameters.Add(new SqlParameter("@sConditionOperator", advancedCondition.ConditionOperator));
                command.Parameters.Add(new SqlParameter("@nDisplayOrder", advancedCondition.DisplayOrder));
                command.Parameters.Add(new SqlParameter("@sJoinOperator", advancedCondition.JoinOperator));

                int i = 1;
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    i = -1;
                }

                connection.Close();
                connection.Dispose();

                return i;
            }
        }
    }

    public static AdvancedCondition ets_AdvancedCondition_Detail(int advancedConditionID)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_AdvancedCondition_Detail", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nAdvancedConditionID", advancedConditionID));
                connection.Open();

                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            AdvancedCondition advancedCondition = new AdvancedCondition(
                                (int)reader["AdvancedConditionID"],
                                reader["ConditionType"] == DBNull.Value ? (string)null : (string)reader["ConditionType"],
                                reader["ConditionSubType"] == DBNull.Value ? (string)null : (string)reader["ConditionSubType"],
                                (int)reader["ColumnID"],
                                (int)reader["ConditionColumnID"],
                                reader["ConditionColumnValue"] == DBNull.Value ? (string)null : (string)reader["ConditionColumnValue"],
                                reader["ConditionOperator"] == DBNull.Value ? (string)null : (string)reader["ConditionOperator"],
                                (int)reader["DisplayOrder"],
                                reader["JoinOperator"] == DBNull.Value ? (string)null : (string)reader["JoinOperator"]
                            );

                            connection.Close();
                            connection.Dispose();
                            return advancedCondition;
                        }
                    }
                }
                catch
                {
                }
                connection.Close();
                connection.Dispose();

                return null;
            }
        }
    }

    public static DataTable ets_AdvancedCondition_Select(int columnID, string conditionType, string conditionSubType)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_AdvancedCondition_Select", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@nColumnID", columnID));
                command.Parameters.Add(new SqlParameter("@sConditionType", conditionType));
                command.Parameters.Add(new SqlParameter("@sConditionSubType", conditionSubType));

                SqlDataAdapter da = new SqlDataAdapter()
                {
                    SelectCommand = command
                };
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();

                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
                else
                {
                    return null;
                }
            }
        }
    }

    public static string AdvancedCondition_GetFormulaHTMLTable(Column theColumn, string ConditionType, string ConditionSubType)
    {
        string strFormula = "";
        if (theColumn.ColumnID.HasValue)
        {
            DataTable dtCondition = UploadWorld.ets_AdvancedCondition_Select(theColumn.ColumnID.Value, ConditionType, ConditionSubType);
            if (dtCondition != null && dtCondition.Rows.Count > 0)
            { 
                strFormula = @"<table  style=""border-color: #600;border-width: 0 0 1px 1px;border-style: solid;border-collapse:collapse;"">
                            <tr >
                                <td style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">
                                    <strong>When</strong>
                                </td>
                                <td style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">
                                    <strong>Field</strong>
                                </td>
                                <td style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">
                                    <strong>Operator</strong>
                                </td>
                                <td style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">
                                    <strong>Value</strong>
                                </td>
                            </tr>";
                foreach (DataRow dr in dtCondition.Rows)
                {
                    string strJoinOperator = String.Empty;
                    if (dr.IsNull("JoinOperator") || String.IsNullOrEmpty(dr["JoinOperator"].ToString()))
                        strJoinOperator = ""; //"&nbsp;";
                    else
                        strJoinOperator = dr["JoinOperator"].ToString().ToUpper();

                    int columnId = 0;
                    string strDisplayName = String.Empty;
                    Column column = null;
                    if (!dr.IsNull("ConditionColumnID") &&
                        int.TryParse(dr["ConditionColumnID"].ToString(), out columnId))
                    {
                        column = RecordManager.ets_Column_Details(columnId);
                        if (column != null && !String.IsNullOrEmpty(column.DisplayName))
                        {
                            strDisplayName = column.DisplayName;
                        }
                        else
                            strDisplayName = ""; //"&nbsp;";
                    }
                    else
                        strDisplayName = ""; //"&nbsp;";

                    string strOperator = String.Empty;
                    if (dr.IsNull("ConditionOperator") || String.IsNullOrEmpty(dr["ConditionOperator"].ToString()))
                        strOperator = ""; //"&nbsp;";
                    else
                    {
                        switch (dr["ConditionOperator"].ToString().ToUpper())
                        {
                            case "EQUALS":
                                strOperator = "Equals";
                                break;
                            case "NOTEQUAL":
                                strOperator = "Not Equals";
                                break;
                            case "GREATERTHAN":
                                strOperator = "Greater Than";
                                break;
                            case "GREATERTHANEQUAL":
                                strOperator = "Greater or Equal to";
                                break;
                            case "LESSTHAN":
                                strOperator = "Less Than";
                                break;
                            case "LESSTHANEQUAL":
                                strOperator = "Less or Equal to";
                                break;
                            case "CONTAINS":
                                strOperator = "Contains";
                                break;
                            case "NOTCONTAINS":
                                strOperator = "Does Not Contain";
                                break;
                            case "EMPTY":
                                strOperator = "Is Empty";
                                break;
                            case "NOTEMPTY":
                                strOperator = "Is Not Empty";
                                break;
                            default:
                                strOperator = ""; //"&nbsp;";
                                break;
                        }
                    }

                    string strValue = String.Empty;
                    if (dr.IsNull("ConditionColumnValue") ||
                        String.IsNullOrEmpty(dr["ConditionColumnValue"].ToString()))
                        strValue = ""; //"&nbsp;";
                    else
                    {
                        if (column != null &&
                            column.ColumnType == "dropdown" && column.DropDownType == "tabledd" && column.TableTableID.HasValue)
                        {
                            string s = column.DisplayColumn;
                            DataTable dtParents = Common.spGetLinkedRecordIDnDisplayText(s, null, column.TableTableID.Value, 1,
                                "AND [Record].[RecordID]=" + dr["ConditionColumnValue"].ToString(), "", "");
                            if (dtParents != null && dtParents.Rows.Count > 0)
                                strValue = dtParents.Rows[0][1].ToString();
                        }
                        else
                            strValue = dr["ConditionColumnValue"].ToString();
                    }

                    try
                    {
                        strDisplayName = WebUtility.HtmlEncode(strDisplayName);
                        strJoinOperator = WebUtility.HtmlEncode(strJoinOperator);
                        strOperator = WebUtility.HtmlEncode(strOperator);
                        strValue = WebUtility.HtmlEncode(strValue);
                    }
                    catch
                    {
                        //
                    }

                    string spanStyle = String.Empty;
                    switch (strJoinOperator)
                    {
                        case "AND":
                            spanStyle = "font-weight: bold;";
                            break;
                        case "OR":
                            spanStyle = "font-style: italic;";
                            break;
                    }

                    strFormula = strFormula + @"<tr >
                                <td style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">" +
                                    @"<span style=""" + spanStyle + @""">" + strJoinOperator + @"</span></td>
                                <td style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">" + strDisplayName + @"</td>
                                <td style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">" + strOperator + @"</td>
                                <td style=""border-color: #600;border-width: 1px 1px 0 0; border-style: solid;margin: 0;"">" + strValue + @"</td>
                            </tr>";
                }
                strFormula = strFormula + "</table>";
            }
        }
        return strFormula;
    }

    #endregion
}


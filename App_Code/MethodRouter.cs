﻿using DocGen.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
//using ChartDirector;
//using DocGen.DAL;
using System.Web.UI.WebControls;
using TimeDoctor;
using Xero;
using System.IO;
/// <summary>
/// Summary description for MethodRouter
/// </summary>
public class MethodRouter
{
	public MethodRouter()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public static List<object> DotNetMethod(string sMethodName, List<object> objList)
    {
        List<object> roList = new List<object>();
        string temp = "";
        try
        {


            switch (sMethodName.ToLower())
            {
                case "processechotechblastdata":
                    roList = ProcessEchotechBlastData(objList);
                    return roList;
                case "importalslivedata":
                    roList = ImportALSLiveData(objList);
                    return roList;
                //case "bulganovecommeteodata":
                //    roList = ImportBulgaNovecomMeteoData(objList);
                //    return roList;
                case "smsemailcountreset":
                    roList = SMSEmailCountReset(objList);
                    return roList;
                case "processlatedata":
                    roList = ProcessLateData(objList);
                    return roList;
                case "sp_common_datatable":
                    roList = sp_common_datatable(objList);
                    return roList;
                case "test_custom_usercontrol_common":
                    roList = test_custom_usercontrol_common(objList);
                    return roList;
                case "runarchivedatabaserecordsync":
                    roList = runarchivedatabaserecordsync(objList);
                    return roList;
                case "timedoctor.timedoctorextractor.execute":
                    //roList = new List<object>();
                    //TimeDoctorExtractor timeDoctorExtractor = new TimeDoctorExtractor();
                    //roList.Add(timeDoctorExtractor.Execute(Convert.ToString(objList[1]), out temp));
                    roList = timedoctor_timedoctorextractor_execute(objList);
                    return roList;
                case "xero.xeroextractor.execute":
                    roList = new List<object>();
                    XeroExtractor xeroExtractor = new XeroExtractor();
                    roList.Add(xeroExtractor.Execute(Convert.ToString(objList[1]), out temp));
                    return roList;
                case "send_scheduled_reports":
                    roList = SendScheduledReports(objList);
                    return roList;
                case "sendqueuedemails":
                    roList = SendQueuedEmails(objList);
                    return roList;
                case "ontaskbillingupdate":
                    roList = OnTaskBillingUpdate(objList);
                    return roList;
                case "specialnotification":
                    roList = specialnotification(objList);
                    return roList;
                case "xmlexport":
                    roList = xmlexport(objList);
                    return roList;
                case "csvexport":
                    roList = csvexport(objList);
                    return roList;
                case "renzoupdatevalidationbackdate":
                    roList = renzoupdatevalidationbackdate(objList);
                    return roList;
                case "ontaskupdatequotestatus":
                    roList = ontaskupdatequotestatus(objList);
                    return roList;
            }


        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "MethodRouter.DotNetMethod/" + sMethodName, ex.Message, ex.StackTrace, DateTime.Now, "App_Code");
            int errorLogRecordId = SystemData.ErrorLog_Insert(theErrorLog);
            if (roList != null)
                roList.Add(String.Format("Error occurred. See error log (ErrorLogID {0})", errorLogRecordId));
        }
        return roList;
    }


    public static List<object> ontaskupdatequotestatus(List<object> objList)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("Ontask_Quote_UpdateStatus", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    //red
                }
                connection.Close();
                connection.Dispose();

                return null;

            }
        }
    }


    public static List<object> ProcessEchotechBlastData(List<object> objList)
    {
        EcotechBlastManager.EcotechBlastData oEE = new EcotechBlastManager.EcotechBlastData();
        string sOut = "";
        sOut = oEE.ProcessEchotechBlastData("", out sOut);
        List<object> roList = new List<object>();
        roList.Add(sOut);

        return roList;
    }

    public static List<object> SendQueuedEmails(List<object> objList)
    {
        int i=EmailManager.SendQueuedEmails();
        List<object> roList = new List<object>();
        roList.Add(i);

        return roList;
    }

    public static List<object> xmlexport(List<object> objList)
    {
       

        string strXMLExportFolder = SystemData.SystemOption_ValueByKey_Account("XMLExportFolder", null,null);


        string sListOfViews = Common.GetValueFromSQL("SELECT MethodParameters FROM RegularTask WHERE MethodName = 'XMLExport'");

        if(!string.IsNullOrEmpty(sListOfViews))
        {
            string[] values = sListOfViews.Split(',');
            foreach(string sView in values)
            {
                if (!string.IsNullOrEmpty(sView))
                {
                    string sViewName = sView.Trim();

                    string sXML = Common.GetValueFromSQL("SELECT CAST((SELECT * FROM "+ sViewName + " FOR XML PATH('" + sViewName + "'), ROOT('root')) as  VARCHAR(MAX)) AS XmlData");
                    if (!string.IsNullOrEmpty(sXML))
                    {
                        //TextWriter txWriter = new StreamWriter(strXMLExportFolder + "/"+ strXMLExportFolder + ".xml");
                        File.WriteAllText(strXMLExportFolder + "/" + sViewName + ".xml", sXML);
                    }
                }
            }
        }

        //TextWriter txWriter = new StreamWriter(Server.MapPath("Upload/") + Session["orderID"].ToString() + ".txt");

        List<object> roList = new List<object>();
        //roList.Add(i);

        return roList;
    }


    public static List<object> csvexport(List<object> objList)
    {

        string strCSVExportFolder = SystemData.SystemOption_ValueByKey_Account("CSVExportFolder", null, null);

        string sListOfViews = Common.GetValueFromSQL("SELECT MethodParameters FROM RegularTask WHERE MethodName = 'CSVExport'");

        if (!string.IsNullOrEmpty(sListOfViews))
        {
            string[] values = sListOfViews.Split(',');
            foreach (string sView in values)
            {
                StringWriter sw = new StringWriter();
                if (!string.IsNullOrEmpty(sView))
                {
                    string sViewName = sView.Trim();

                    DataTable dtTable = Common.DataTableFromText("SELECT * FROM " + sViewName);
                    int iColCount = dtTable.Columns.Count;

                    for (int i = 0; i < iColCount; i++)
                    {
                        sw.Write(dtTable.Columns[i].ToString());
                        if (i < iColCount)
                        {
                            sw.Write(",");
                        }

                    }

                    sw.Write(sw.NewLine);

                    // Now write all the rows.
                    foreach (DataRow dr in dtTable.Rows)
                    {
                        for (int i = 0; i < iColCount; i++)
                        {
                            if (!Convert.IsDBNull(dr[i]))
                            {
                                sw.Write("\"" + dr[i].ToString().Replace("\"", "'") + "\"");
                            }

                            if (i < iColCount)
                            {
                                sw.Write(",");
                            }
                        }

                        sw.Write(sw.NewLine);
                    }

                    sw.Close();

                    if (!string.IsNullOrEmpty(sw.ToString()))
                    {
                        File.WriteAllText(strCSVExportFolder + "/" + sViewName + ".csv", sw.ToString());
                    }
                }
            }
        }

        List<object> roList = new List<object>();
        return roList;
    }


    public static List<object> OnTaskBillingUpdate(List<object> objList)
    {
        int i = BillingAPI.OnTaskBillingUpdate();
        List<object> roList = new List<object>();
        roList.Add(i);

        return roList;
    }


    public static List<object> timedoctor_timedoctorextractor_execute(List<object> objList)
    {
        string temp;
        List<object> roList = new List<object>();
        try
        {
            TimeDoctorExtractor timeDoctorExtractor = new TimeDoctorExtractor();
            roList.Add(timeDoctorExtractor.Execute(Convert.ToString(objList[1]), out temp));
        }
        catch(Exception ex)
        {
            //
        }
        
        return roList;
    }


    public static List<object> ImportALSLiveData(List<object> objList)
    {
        EMDDataImport.ALSLiveImport oEA = new EMDDataImport.ALSLiveImport();
        string sOut = "";
        sOut = oEA.ImportALSLiveData("", out sOut);
        List<object> roList = new List<object>();
        roList.Add(sOut);

        return roList;
    }

    public static List<object> SendScheduledReports(List<object> objList)
    {
        ReportManager rm = new ReportManager();
        string sOut = "";
        sOut = rm.RunScheduledReports();
        List<object> roList = new List<object>();
        roList.Add(sOut);

        return roList;
    }


    //public static List<object> ImportBulgaNovecomMeteoData(List<object> objList)
    //{
    //    EMD.NovecomMeteoData oNmd = new EMD.NovecomMeteoData();
    //    string sOut = "";
    //    sOut = oNmd.ImportNovecomMeteoData("", out sOut);
    //    List<object> roList = new List<object>();
    //    roList.Add(sOut);

    //    return roList;
    //}

    public static List<object> SMSEmailCountReset(List<object> objList)
    {
        SMSEmailCountManager.SMSEmailCount oSS = new SMSEmailCountManager.SMSEmailCount();
        string sOut = "";
        sOut = oSS.SMSEmailCountReset("", out sOut);
        List<object> roList = new List<object>();
        roList.Add(sOut);

        return roList;
    }


    public static List<object> ProcessLateData(List<object> objList)
    {
        LateDataManager.LateData oLL = new LateDataManager.LateData();
        string sOut = "";
        sOut= oLL.ProcessLateData("", out sOut);
        List<object> roList = new List<object>();
        roList.Add(sOut);

        return roList;
    }




    public static List<object> test_custom_usercontrol_common(List<object> objList)
    {
        if (objList != null && objList.Count>0)
        {
            UserControl aUserControl = (UserControl)objList[0];
            if(aUserControl!=null)
            {
                aUserControl.GetType().GetProperty("TextProperty").SetValue(aUserControl, " Common Methods");
            }
        }
        return null;
    }
    public static List<object> sp_common_datatable(List<object> objList)
    {

         List<object> roList = new List<object>();
         GenericParam theGenericParam = null;
         foreach (object obj in objList)
         {
             if (obj.GetType().Name == "GenericParam")
             {
                theGenericParam = (GenericParam)obj;
                break;
             }
         }


        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand(theGenericParam.SPName, connection))
            {
                command.CommandType = CommandType.StoredProcedure;



                SqlParameter pRV = new SqlParameter("@Return", SqlDbType.VarChar);
                pRV.Size = 4000;
                pRV.Direction = ParameterDirection.Output;

                command.Parameters.Add(pRV);
                if (theGenericParam.RecordID != null)
                    command.Parameters.Add(new SqlParameter("@RecordID", theGenericParam.RecordID));



                if (theGenericParam.UserID != null)
                    command.Parameters.Add(new SqlParameter("@UserID", theGenericParam.UserID));

                if (theGenericParam.param001 != null)
                    command.Parameters.Add(new SqlParameter("@param001", theGenericParam.param001));

                if (theGenericParam.param002 != null)
                    command.Parameters.Add(new SqlParameter("@param002", theGenericParam.param002));

                if (theGenericParam.param003 != null)
                    command.Parameters.Add(new SqlParameter("@param003", theGenericParam.param003));

                if (theGenericParam.param004 != null)
                    command.Parameters.Add(new SqlParameter("@param004", theGenericParam.param004));

                if (theGenericParam.param005 != null)
                    command.Parameters.Add(new SqlParameter("@param005", theGenericParam.param005));


                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = command;
                DataTable dt = new DataTable();
                System.Data.DataSet ds = new System.Data.DataSet();
                connection.Open();
                try
                {
                    da.Fill(ds);
                }
                catch
                {
                    //
                }
                connection.Close();
                connection.Dispose();


                if (ds == null) return null;

               
                if (ds.Tables.Count > 0)
                {
                    roList.Add(ds.Tables[0]);
                    return roList;
                }
                {
                    return null;
                }


            }
        }
    }

    //Red Ticket 3358_2
    public static List<object> runarchivedatabaserecordsync(List<object> objList)
    {

        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("ets_Record_Sync_Run", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    //red
                }
                connection.Close();
                connection.Dispose();

                return null;

            }
        }
    }//end red


    public static List<object> specialnotification(List<object> objList)
    {
        try
        {
            DataTable dtDRUsers = Common.DataTableFromText(@"                  
                SELECT 
                    S.SpecialNotificationID,
                    S.NotificationHeader,
                    S.NotificationContent,
                    S.TableID,
                    S.Delivery,
                    R.RecipientID,
                    R.UserID,
                    R.ColumnID,
                    R.RecipientOption,
                    U.FirstName,
                    U.LastName,
                    U.Email,
                    U.PhoneNumber,
                    A.AccountID,
                    T.TableName 
                FROM SpecialNotification S
                    JOIN SpecialNotificationRecipient R ON R.SpecialNotificationID = S.SpecialNotificationID 
                    JOIN [Table] T ON T.TableID = S.TableID 
                        AND T.IsActive = 1
                    JOIN Account A ON A.AccountID= T.AccountID
                        AND A.IsActive = 1
                    LEFT JOIN [User] U ON U.UserID = R.UserID
                        AND U.IsActive = 1
                    LEFT JOIN [Column] C ON C.ColumnID = R.ColumnID
                ");


            string strWarningSMSEMail = SystemData.SystemOption_ValueByKey_Account("WarningSMSEmail", null, null);

            if (dtDRUsers != null)
            {
                foreach (DataRow dr in dtDRUsers.Rows)
                {
                    //lets send emails ReminderContent
                    DataTable _dtRecordColums = RecordManager.ets_Table_Columns_Summary(int.Parse(dr["TableID"].ToString()), null);

                    string strBody = dr["NotificationContent"].ToString();

                    DataTable dtRecords = RecordManager.dbg_Conditions_ReturnRecordID(int.Parse(dr["TableID"].ToString()), int.Parse(dr["SpecialNotificationID"].ToString()));

                    if (dtRecords.Rows.Count > 0)
                    {
                        foreach (DataRow drR in dtRecords.Rows)
                        {
                            Record thisRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drR["RecordID"].ToString()), null, false);

                            if (thisRecord != null)
                            {
                                string strEachRecordBody = strBody;
                                DataTable dtColumns = Common.DataTableFromText("SELECT SystemName,DisplayName FROM [Column] WHERE IsStandard=0 AND   TableID="
                                    + dr["TableID"].ToString() + "  ORDER BY DisplayName");
                                foreach (DataRow drC in dtColumns.Rows)
                                {
                                    strEachRecordBody = strEachRecordBody.Replace("[" + drC["DisplayName"].ToString() + "]", RecordManager.GetRecordValue(ref thisRecord, drC["SystemName"].ToString()));
                                }

                                DataTable dtPT = Common.DataTableFromText("SELECT distinct ParentTableID FROM TableChild WHERE ChildTableID=" + dr["TableID"].ToString()); //AND DetailPageType<>'not'

                                if (dtPT.Rows.Count > 0)
                                {
                                    foreach (DataRow drPT in dtPT.Rows)
                                    {
                                        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                                        {
                                            if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
                                       && (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
                                       || _dtRecordColums.Rows[i]["DropDownType"].ToString() == "tabledd")
                                        && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
                                       && _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
                                            {
                                                if (_dtRecordColums.Rows[i]["TableTableID"].ToString() == drPT["ParentTableID"].ToString())
                                                {
                                                    if (RecordManager.GetRecordValue(ref thisRecord, _dtRecordColums.Rows[i]["SystemName"].ToString()) != "")
                                                    {

                                                        Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["LinkedParentColumnID"].ToString()));
                                                        DataTable dtParentRecord = null;
                                                        if (theLinkedColumn.SystemName.ToLower() == "recordid")
                                                        {
                                                            dtParentRecord = Common.DataTableFromText("SELECT RecordID FROM Record WHERE RecordID=" + RecordManager.GetRecordValue(ref thisRecord, _dtRecordColums.Rows[i]["SystemName"].ToString()));
                                                        }
                                                        else
                                                        {
                                                            dtParentRecord = Common.DataTableFromText("SELECT RecordID FROM Record WHERE TableID=" + theLinkedColumn.TableID.ToString() + " AND " + theLinkedColumn.SystemName + "='" +
                                                                RecordManager.GetRecordValue(ref thisRecord, _dtRecordColums.Rows[i]["SystemName"].ToString()).ToString().Replace("'", "''") + "'");
                                                        }

                                                        if (dtParentRecord.Rows.Count > 0)
                                                        {
                                                            Record theParentRecord = RecordManager.ets_Record_Detail_Full(int.Parse(dtParentRecord.Rows[0]["RecordID"].ToString()), null, false);
                                                            DataTable dtColumnsPT = Common.DataTableFromText(@"SELECT distinct [Column].SystemName, TableName + ':' + DisplayName AS DP FROM [Column] INNER JOIN [Table]
                                                        ON [Column].TableID=[Table].TableID WHERE  [Column].IsStandard=0 AND  [Column].TableID=" + drPT["ParentTableID"].ToString());
                                                            foreach (DataRow drC in dtColumnsPT.Rows)
                                                            {
                                                                strEachRecordBody = strEachRecordBody.Replace("[" + drC["DP"].ToString() + "]", RecordManager.GetRecordValue(ref theParentRecord, drC["SystemName"].ToString()));

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                //lets send an email
                                string strSubject = dr["NotificationHeader"].ToString();
                                string strTo = "";

                                /* Specific User */


                                /* == SMS == */

                                if (dr["Delivery"].ToString() == "both")
                                {
                                     string phoneNumber = "";
                            //strSubject = dr["NotificationHeader"].ToString().Replace("[Table]", dr["TableName"].ToString());
                            if (dr["RecipientOption"].ToString() == "SU")
                            {
                                if (dr["PhoneNumber"] != DBNull.Value)
                                {
                                    if (dr["PhoneNumber"].ToString() != "")
                                    {
                                        phoneNumber = dr["PhoneNumber"].ToString();
                                    }
                                }
                            }
                            else if (dr["RecipientOption"].ToString() == "UWCTR")
                            {
                                Record theReminderRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drR["RecordID"].ToString()), null, false);
                                if (theReminderRecord != null)
                                {
                                    phoneNumber = Common.GetValueFromSQL("  SELECT PhoneNumber FROM [User] WHERE [UserID] = (SELECT ENTEREDBY FROM [Record] WHERE RecordID = " + drR["RecordID"].ToString() + ")");
                                }
                            }
                            else if (dr["RecipientOption"].ToString() == "UWLETR")
                            {
                                Record theReminderRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drR["RecordID"].ToString()), null, false);
                                if (theReminderRecord != null)
                                {
                                    phoneNumber = Common.GetValueFromSQL("  SELECT PhoneNumber FROM [User] WHERE [UserID] = (SELECT [LastUpdatedUserID] FROM [Record] WHERE RecordID = " + drR["RecordID"].ToString() + ")");
                                }
                            }

                            try
                            {
                                string sSendEmailError = "";

                                Message theMessage = new Message(null, int.Parse(drR["RecordID"].ToString()), int.Parse(dr["TableID"].ToString()), int.Parse(dr["AccountID"].ToString()),
                                         DateTime.Now, "E", "S",
                            null, dr["PhoneNumber"].ToString() + strWarningSMSEMail, strSubject, strEachRecordBody, null, "");

                                DBGurus.SendEmail("SpecialNotifications", null, true, strSubject, strEachRecordBody, "",
                                    phoneNumber + strWarningSMSEMail, "", "", null, theMessage, out sSendEmailError);
                            }
                            catch (Exception ex)
                            {
                                ErrorLog theErrorLog = new ErrorLog(null, "Special Notification: SMS Email", ex.Message, ex.StackTrace, DateTime.Now, "");
                                SystemData.ErrorLog_Insert(theErrorLog);
                            }

                            /* == End SMS ==*/
                            /* =============================================================== */

                                    /* == End SMS ==*/

                                    /* Email On Current Record */
                                    if (dr["RecipientOption"].ToString() == "EOCR")
                                    {
                                        Column theReminderColumn = RecordManager.ets_Column_Details(int.Parse(dr["ColumnID"].ToString()));

                                        if (theReminderColumn != null)
                                        {
                                            if (theReminderColumn.ColumnType == "text")
                                            {
                                                string strEmailRC = Common.GetValueFromSQL("  SELECT " + theReminderColumn.SystemName + " FROM Record WHERE RecordID=" + drR["RecordID"].ToString());
                                                strTo = strEmailRC;
                                            }

                                            if (theReminderColumn.ColumnType == "dropdown")
                                            {

                                                int iUserIDReminder = 0;
                                                if (int.TryParse(Common.GetValueFromSQL("  SELECT " + theReminderColumn.SystemName + " FROM Record WHERE RecordID=" + drR["RecordID"].ToString()), out iUserIDReminder))
                                                {
                                                    //
                                                }
                                                string strEmailRC = Common.GetValueFromSQL("  SELECT Email FROM [User] WHERE UserID=" + iUserIDReminder);

                                                if (string.IsNullOrEmpty(strEmailRC))
                                                {
                                                    if (iUserIDReminder > 0)
                                                    {
                                                        Record theRecordLevel2 = RecordManager.ets_Record_Detail_Full(iUserIDReminder, null, false);
                                                        if (theRecordLevel2 != null)
                                                        {
                                                            string strUserSys = Common.GetValueFromSQL("SELECT TOP 1 SystemName FROM [Column] WHERE TableTableID=-1 AND TableID=" + theRecordLevel2.TableID.ToString());
                                                            if (!string.IsNullOrEmpty(strUserSys))
                                                            {
                                                                string strUserID = RecordManager.GetRecordValue(ref theRecordLevel2, strUserSys);
                                                                if (!string.IsNullOrEmpty(strUserID))
                                                                {
                                                                    strEmailRC = Common.GetValueFromSQL(" SELECT EMAIL FROM [USER] WHERE [USERID] = " + strUserID);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                strTo = strEmailRC;
                                            }
                                        }
                                    }
                                    else if (dr["RecipientOption"].ToString() == "UWCTR")
                                    {
                                        Record theReminderRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drR["RecordID"].ToString()), null, false);
                                        if (theReminderRecord != null)
                                        {
                                            string strEmailRC = Common.GetValueFromSQL("  SELECT Email FROM [User] WHERE [UserID] = (SELECT ENTEREDBY FROM [Record] WHERE RecordID = " + drR["RecordID"].ToString() + ")");
                                            strTo = strEmailRC;
                                        }
                                    }
                                    else if (dr["RecipientOption"].ToString() == "UWLETR")
                                    {
                                        Record theReminderRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drR["RecordID"].ToString()), null, false);
                                        if (theReminderRecord != null)
                                        {
                                            string strEmailRC = Common.GetValueFromSQL("  SELECT Email FROM [User] WHERE [UserID] = (SELECT [LastUpdatedUserID] FROM [Record] WHERE RecordID = " + drR["RecordID"].ToString() + ")");
                                            strTo = strEmailRC;
                                        }
                                    }
                                    else if (dr["RecipientOption"].ToString() == "SU")
                                    {
                                        strTo = dr["Email"].ToString();
                                        strEachRecordBody = strEachRecordBody.Replace("[LastName]", dr["LastName"].ToString());
                                        strEachRecordBody = strEachRecordBody.Replace("[FirstName]", dr["FirstName"].ToString());
                                    }

                                    try
                                    {
                                        if (strTo != "")
                                        {
                                            string sSendEmailError = "";

                                            Message theMessage = new Message(null, null, int.Parse(dr["TableID"].ToString()), int.Parse(dr["AccountID"].ToString()),
                                DateTime.Now, "W", "E",
                               null, strTo, strSubject, strEachRecordBody, null, "");

                                            DBGurus.SendEmail("SpecialNotifications", true, null, strSubject, strEachRecordBody, "",
                                                strTo, "", "", null, theMessage, out sSendEmailError);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ErrorLog theErrorLog = new ErrorLog(null, "Send Special Notification Emails Error", ex.Message, ex.StackTrace, DateTime.Now, "MethodRouter/SpecialNotification");
                                        SystemData.ErrorLog_Insert(theErrorLog);
                                    }

                                } /* == End both == */
                                else if (dr["Delivery"].ToString() == "sms")
                                {
                                    string phoneNumber = "";
                                    //strSubject = dr["NotificationHeader"].ToString().Replace("[Table]", dr["TableName"].ToString());
                                    if (dr["RecipientOption"].ToString() == "SU")
                                    {
                                        if (dr["PhoneNumber"] != DBNull.Value)
                                        {
                                            if (dr["PhoneNumber"].ToString() != "")
                                            {
                                                phoneNumber = dr["PhoneNumber"].ToString();
                                            }
                                        }
                                    }
                                    else if (dr["RecipientOption"].ToString() == "UWCTR")
                                    {
                                        Record theReminderRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drR["RecordID"].ToString()), null, false);
                                        if (theReminderRecord != null)
                                        {
                                            phoneNumber = Common.GetValueFromSQL("  SELECT PhoneNumber FROM [User] WHERE [UserID] = (SELECT ENTEREDBY FROM [Record] WHERE RecordID = " + drR["RecordID"].ToString() + ")");
                                        }
                                    }
                                    else if (dr["RecipientOption"].ToString() == "UWLETR")
                                    {
                                        Record theReminderRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drR["RecordID"].ToString()), null, false);
                                        if (theReminderRecord != null)
                                        {
                                            phoneNumber = Common.GetValueFromSQL("  SELECT PhoneNumber FROM [User] WHERE [UserID] = (SELECT [LastUpdatedUserID] FROM [Record] WHERE RecordID = " + drR["RecordID"].ToString() + ")");
                                        }
                                    }

                                    try
                                    {
                                        string sSendEmailError = "";

                                        Message theMessage = new Message(null, int.Parse(drR["RecordID"].ToString()), int.Parse(dr["TableID"].ToString()), int.Parse(dr["AccountID"].ToString()),
                                                 DateTime.Now, "E", "S",
                                    null, dr["PhoneNumber"].ToString() + strWarningSMSEMail, strSubject, strEachRecordBody, null, "");

                                        DBGurus.SendEmail("SpecialNotifications", null, true, strSubject, strEachRecordBody, "",
                                            phoneNumber + strWarningSMSEMail, "", "", null, theMessage, out sSendEmailError);
                                    }
                                    catch (Exception ex)
                                    {
                                        ErrorLog theErrorLog = new ErrorLog(null, "Special Notification: SMS Email", ex.Message, ex.StackTrace, DateTime.Now, "");
                                        SystemData.ErrorLog_Insert(theErrorLog);
                                    }

                                    /* == End SMS ==*/
                                    /* =============================================================== */

                                }    /* == End SMS ==*/
                                else if (dr["Delivery"].ToString() == "email")
                                {
                                    /* Email On Current Record */
                                    if (dr["RecipientOption"].ToString() == "EOCR")
                                    {
                                        Column theReminderColumn = RecordManager.ets_Column_Details(int.Parse(dr["ColumnID"].ToString()));

                                        if (theReminderColumn != null)
                                        {
                                            if (theReminderColumn.ColumnType == "text")
                                            {
                                                string strEmailRC = Common.GetValueFromSQL("  SELECT " + theReminderColumn.SystemName + " FROM Record WHERE RecordID=" + drR["RecordID"].ToString());
                                                strTo = strEmailRC;
                                            }

                                            if (theReminderColumn.ColumnType == "dropdown")
                                            {

                                                int iUserIDReminder = 0;
                                                if (int.TryParse(Common.GetValueFromSQL("  SELECT " + theReminderColumn.SystemName + " FROM Record WHERE RecordID=" + drR["RecordID"].ToString()), out iUserIDReminder))
                                                {
                                                    //
                                                }
                                                string strEmailRC = Common.GetValueFromSQL("  SELECT Email FROM [User] WHERE UserID=" + iUserIDReminder);

                                                if (string.IsNullOrEmpty(strEmailRC))
                                                {
                                                    if (iUserIDReminder > 0)
                                                    {
                                                        Record theRecordLevel2 = RecordManager.ets_Record_Detail_Full(iUserIDReminder, null, false);
                                                        if (theRecordLevel2 != null)
                                                        {
                                                            string strUserSys = Common.GetValueFromSQL("SELECT TOP 1 SystemName FROM [Column] WHERE TableTableID=-1 AND TableID=" + theRecordLevel2.TableID.ToString());
                                                            if (!string.IsNullOrEmpty(strUserSys))
                                                            {
                                                                string strUserID = RecordManager.GetRecordValue(ref theRecordLevel2, strUserSys);
                                                                if (!string.IsNullOrEmpty(strUserID))
                                                                {
                                                                    strEmailRC = Common.GetValueFromSQL(" SELECT EMAIL FROM [USER] WHERE [USERID] = " + strUserID);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                strTo = strEmailRC;
                                            }
                                        }
                                    }

                                    else if (dr["RecipientOption"].ToString() == "UWCTR")
                                    {
                                        Record theReminderRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drR["RecordID"].ToString()), null, false);
                                        if (theReminderRecord != null)
                                        {
                                            string strEmailRC = Common.GetValueFromSQL("  SELECT Email FROM [User] WHERE [UserID] = (SELECT ENTEREDBY FROM [Record] WHERE RecordID = " + drR["RecordID"].ToString() + ")");
                                            strTo = strEmailRC;
                                        }
                                    }
                                    else if (dr["RecipientOption"].ToString() == "UWLETR")
                                    {
                                        Record theReminderRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drR["RecordID"].ToString()), null, false);
                                        if (theReminderRecord != null)
                                        {
                                            string strEmailRC = Common.GetValueFromSQL("  SELECT Email FROM [User] WHERE [UserID] = (SELECT [LastUpdatedUserID] FROM [Record] WHERE RecordID = " + drR["RecordID"].ToString() + ")");
                                            strTo = strEmailRC;
                                        }
                                    }
                                    else if (dr["RecipientOption"].ToString() == "SU")
                                    {
                                        strTo = dr["Email"].ToString();
                                        strEachRecordBody = strEachRecordBody.Replace("[LastName]", dr["LastName"].ToString());
                                        strEachRecordBody = strEachRecordBody.Replace("[FirstName]", dr["FirstName"].ToString());
                                    }

                                    try
                                    {
                                        if (strTo != "")
                                        {
                                            string sSendEmailError = "";

                                            Message theMessage = new Message(null, null, int.Parse(dr["TableID"].ToString()), int.Parse(dr["AccountID"].ToString()),
                                DateTime.Now, "W", "E",
                               null, strTo, strSubject, strEachRecordBody, null, "");

                                            DBGurus.SendEmail("SpecialNotifications", true, null, strSubject, strEachRecordBody, "",
                                                strTo, "", "", null, theMessage, out sSendEmailError);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ErrorLog theErrorLog = new ErrorLog(null, "Send Special Notification Emails Error", ex.Message, ex.StackTrace, DateTime.Now, "MethodRouter/SpecialNotification");
                                        SystemData.ErrorLog_Insert(theErrorLog);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Special Notifications Error", ex.Message, ex.StackTrace, DateTime.Now, "MethodRouter/SpecialNotification");
            SystemData.ErrorLog_Insert(theErrorLog);
        }


        return null;
    }



    /* == Red: windtech update validation field value == */
    public static List<object> renzoupdatevalidationbackdate(List<object> objList)
    {

        string sp = Common.GetValueFromSQL("SELECT MethodParameters FROM RegularTask WHERE MethodName = 'RenzoUpdateValidationBackDate'");

        if (!string.IsNullOrEmpty(sp))
        {
            Common.ExecuteText(sp);
        }

        List<object> roList = new List<object>();

        return roList;
    }


}
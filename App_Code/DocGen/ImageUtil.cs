﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Drawing2D;

namespace DocGen.Utility
{
    public class ImageUtil
    {
        public static string SaveAsPNG(string imagePath, byte[] content)
        {
            string Message = "";
            try
            {
                Image img = Image.FromStream(new MemoryStream(content));
                img.Save(imagePath, ImageFormat.Png);
            }
            catch(Exception ex)
            {
                Message = ex.Message;
            }
            return Message;
        }

        #region Helper methods
        public static void SaveThumbnail(System.Drawing.Bitmap bitmap, string thumbPath)
        {
            try
            {
                float ratio = 1;
                float minSize = Math.Min(75, 75);

                if (bitmap.Width > bitmap.Height)
                {
                    ratio = minSize / (float)bitmap.Width;
                }
                else
                {
                    ratio = minSize / (float)bitmap.Height;
                }

                SizeF newSize = new SizeF(bitmap.Width * ratio, bitmap.Height * ratio);
                Bitmap target = new Bitmap((int)newSize.Width, (int)newSize.Height);

                using (Graphics graphics = Graphics.FromImage(target))
                {
                    graphics.CompositingQuality = CompositingQuality.HighSpeed;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.DrawImage(bitmap, 0, 0, newSize.Width, newSize.Height);

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        target.Save(thumbPath);
                    }
                }
            }
            catch (Exception) { }
        }


        #endregion
    }
}

﻿using System;
using System.Collections.Generic;

namespace DocGen.DAL
{
    public class ImageSectionStyle : JSONField
    {
        public string Position { get; set; }
        public int Width { get; set; }
        public string OpenLink { get; set; }
        public string Imagefilename { get; set; }
    }

    public class CalendarSectionDetail : JSONField
    {
        public string CalendarTitle { get; set; }
        public int? TableID { get; set; }
        public int? DateFieldColumnID { get; set; }
        public string FieldDisplay { get; set; }
        public string FilterTextSearch { get; set; }
        public string FilterControlInfo { get; set; } //xml
        public string TextColourInfo { get; set; }//xml
        public bool? ShowAddRecordIcon { get; set; }
        public string CalendarDefaultView { get; set; }

        public int? Height { get; set; }
        public int? Width { get; set; }
        public int? EndDateFieldColumnID { get; set; }
        public string DayStart { get; set; } //TimeSpan, MS Design problem... so using string
        public string DayEnd { get; set; } //TimeSpan
        public int? NameFieldColumnID { get; set; }
        public int? FieldDisplayColumnID { get; set; }

    }

    public class MapSectionDetail : JSONField
    {
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? MapScale { get; set; }
        public int? ShowLocation { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
        public string MapTypeId { get; set; }
        public bool? AutoScale { get; set; }
        public int? SearchColumnID { get; set; }
        public int? PinControlColumnID { get; set; }
        public string PinControlValueInfo { get; set; }
        public bool? CustomService { get; set; }

        //public string FilterGridInformation { get; set; }
        //public List<FilterEachCondition> FullFilter { get; set; }
    }


    public class LocationSectionDetail : JSONField
    {
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? MapScale { get; set; }
        public int? ShowLocation { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
        public string MapTypeId { get; set; }
        public bool? AutoScale { get; set; }
        public int? SearchColumnID { get; set; }
        public int? PinControlColumnID { get; set; }
        public string PinControlValueInfo { get; set; }
        public bool? CustomService { get; set; }
        public bool? ShowStaff { get; set; }
        public bool? ShowOpenTask { get; set; }
        public bool? ShowCompleted { get; set; }

        //public string FilterGridInformation { get; set; }
        //public List<FilterEachCondition> FullFilter { get; set; }
    }


    //public class FilterEachCondition : JSONField
    //{
    //    public string ID { get; set; }
    //    public string AndOr { get; set; }
    //    public int? TableID { get; set; }
    //    public int? ColumnID { get; set; }
    //    public string CompareOperator { get; set; }
    //    public string Value { get; set; }
    //    public string EachTextSearch { get; set; }

    //}
    public class DialSectionDetail : JSONField
    {
        public int TableID{ get; set; }
        public int ColumnID { get; set; }
        public string Dial{ get; set; }
        public string  Label{ get; set; }
        public int? Scale { get; set; }
        public string Heading { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }

    }

    public class RecordTableSectionDetail : JSONField
    {
        public int TableID { get; set; }
        //public int SearchCriteriaID { get; set; } 
        public int ViewID { get; set; }
    }

    [Serializable]
    public class OptionImage : JSONField
    {
        public string OptionImageID { get; set; }
        public string Value { get; set; }
        public string  FileName { get; set; }
        public string UniqueFileName { get; set; }
    }

    [Serializable]
     public class OptionImageList : JSONField
     {
         public List<OptionImage> ImageList { get; set; }

     }



    [Serializable]
    public class EachPinInfo : JSONField
    {
        public string lat { get; set; }
        public string lon { get; set; }
        public string title { get; set; }
        public string pin { get; set; }
        public string rings { get; set; }
        public int? order { get; set; }
        public bool? onStart { get; set; }

        public string ssid { get; set; }
        public string url { get; set; }
        public string mappopup { get; set; }

    }

    [Serializable]
    public class EachLocationPinInfo : JSONField
    {
        public string lat { get; set; }
        public string lon { get; set; }
        public string title { get; set; }
        public string pin { get; set; }
        public string rings { get; set; }
        public int? order { get; set; }
        public bool? onStart { get; set; }

        public string ssid { get; set; }
        public string url { get; set; }
        public string label { get; set; }
        public string mappopup { get; set; }
        public int? userid { get; set; }
        public string pintype { get; set; }
        public int? recordid { get; set; }

    }


    [Serializable]
    public class ListPinInfo : JSONField
    {
        public List<EachPinInfo> PinList { get; set; }

    }


    [Serializable]
    public class UserControlInfo : JSONField
    {
        public string Ascx { get; set; }
        public string CommonMethodName { get; set; }
        public string CustomMethodName { get; set; }
        public int? RefColumnID { get; set; }
        public int? TableID { get; set; }
        public string ColumnNotIn { get; set; }
    }

    [Serializable]
    public class ColumnButtonInfo : JSONField
    {
        public string SPToRun { get; set; }
        public string ImageFullPath { get; set; }
        public string WarningMessage { get; set; }

        public string OnClick { get; set; }//need to remove
        public int? ChildTableID { get; set; }
        public string AdditionalParams { get; set; }
        public string Link { get; set; }
        public bool? IsItHyperLink { get; set; }
        public int? DocumentID { get; set; }
        public string TextOnEdit { get; set; }
        public bool? SaveRecord { get; set; }

        public bool? DisplayResultFromTheSP { get; set; }

        public string Action { get; set; }

        public string AfterAction { get; set; }

        public int? AddRecordTableID { get; set; }

        public string Service { get; set; }

    }
    [Serializable]
    public class ChartDashBoard : JSONField
    {
        public int RecentNumber { get; set; }
        public string RecentPeriod { get; set; }
    }

    [Serializable]
    public class PageLifeCycleService : JSONField
    {
       
        public string SPName { get; set; }
        public string DotNetMethod { get; set; }
        public string JavaScriptFunction { get; set; }      
        public string ServerControlEvent { get; set; }
       
    }

    [Serializable]
    public class OfflineTaskParameters : JSONField
    {
        public string ReturnSQL { get; set; }
        public string ReturnHeaderSQL { get; set; }
        public string UniqueFileName { get; set; }
        public string FileFriendlyName { get; set; }
        public int? TableID { get; set; }
        public string TableName { get; set; }
        public int TotalNumberOfRecords { get; set; }
    }

    [Serializable]
    public class MessageParameters : JSONField
    {
        public string ReturnSQL { get; set; }
        public string ReturnHeaderSQL { get; set; }
        public string UniqueFileName { get; set; }
        public string FileFriendlyName { get; set; }
        public int? TableID { get; set; }
        public string TableName { get; set; }
        public int TotalNumberOfRecords { get; set; }
        public int? UserID { get; set; }

        /* == Red 20012020: Ticket 5017 ==*/
        public int ExportTemplateID { get; set; }
    }

}
﻿using System;

namespace DocGen.DAL
{
    public class DocumentFilter : JSONField
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }


    }
}
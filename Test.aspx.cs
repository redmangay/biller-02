﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Trello;

public partial class TestPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["ScreenWidth"] != null)
        //    lblMessages.Text = Session["ScreenWidth"].ToString();        

        //Record_List_Param pRCParam = new Record_List_Param();
        //pRCParam.TableID = 100;
        //pRCParam.EnteredBy = 123;

        //ets_Record_List2(pRCParam);
    }

    //public static DataTable ets_Record_List2(Record_List_Param pRCParam)
    //{
    //    int? iTableID = pRCParam.TableID;
    //    // process

    //    return null;
    //}
    protected void btnInviteUser_Click(object sender, EventArgs e)
    {
        AccountInviteField theField = new AccountInviteField();
        //theField.FromAccountID = Session["AccountID"].ToString();
        User theUser = (User)Session["User"];
        theField.FromUserID = theUser.UserID.ToString();
        theField.AddAsTeamMember = "false";
        theField.InviteType = "N";
        theField.TaskID =txtTaskID.Text;
        theField.ToEmailAddress = txtEmail.Text;
        theField.ToFullName = txtName.Text;
        theField.ToMobile = "12345678";
        int iAccountInviteID= InvoiceManager.ForwardTask(int.Parse(Session["AccountID"].ToString()), theField);
        lblMessages.Text = iAccountInviteID.ToString();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Stripe;
public partial class ProcessPayment :Page
{

    Account _theAccount;
    int iOriginalAccountTypeID;
    //AccountType _theAccountType;
    User _theUser;
    BillingAPI.Billing _theBilling;
    public string stripePublishableKey = SystemData.SystemOption_ValueByKey_Account("StripePublishableKey", null, null);


    protected void PopulateAccountType()
    {
        ddlSAccountType.DataSource = Common.DataTableFromText("SELECT AccountTypeId, AccountTypeName FROM AccountTYpe");
        ddlSAccountType.DataBind();
    }
    public string GetTotalPaymentAmount()
    {
        return (_theBilling.Amount * 100).ToString();
    }
    public string GetCurrency()
    {
        return _theBilling.Currency;
    }
    public string GetUserEmail()
    {
        if (_theUser == null)
        {
            _theUser = (User)Session["User"];
        }
        return _theUser.Email;
    }


    public string DataDescription()
    {
        string sDataDescription = "Ontask Monthly Charge";
        if (_theBilling.IsYearlySubscription != null && (bool)_theBilling.IsYearlySubscription)
        {
            sDataDescription = "Ontask Yearly Charge";
        }

        return sDataDescription;
    }
    public string DataPanelLabel()
    {
        string sDataPanelLabel = "Pay Monthly {{amount}}";
        if (_theBilling.IsYearlySubscription != null && (bool)_theBilling.IsYearlySubscription)
        {
            sDataPanelLabel = "Pay Yearly {{amount}}";
        }

        return sDataPanelLabel;
    }
    public string DataLabel()
    {

        string sDataLabel = "Subscribe with Stripe Checkout";

        var secretKey = SystemData.SystemOption_ValueByKey_Account("StripeSecretKey", null, null);
        StripeConfiguration.SetApiKey(secretKey);


        var cus_options = new StripeCustomerCreateOptions
        {
            Email = _theUser.Email,//"jenny.rosen@example.com",
            Metadata = new Dictionary<string, string>()
                        {
                            { "AccountID", Session["AccountID"].ToString() }
                        }
        };
        var cus_service = new StripeCustomerService();


        var optionsCusList = new StripeCustomerListOptions();
        optionsCusList.Limit = 10000;
        var customers = cus_service.List(optionsCusList);
        //string sTest = "";
        bool bUpdateSubscription = false;
        StripeCustomer customer = new StripeCustomer();
        foreach (var customerEach in customers)
        {
            if (cus_options.Email.ToLower().Trim() == customerEach.Email.ToLower().Trim())
            {
                System.Collections.Generic.Dictionary<string, string> cusMetaData = customerEach.Metadata;
                if (cusMetaData != null && cusMetaData["AccountID"].ToString() == Session["AccountID"].ToString())
                {
                    customer = customerEach;
                    bUpdateSubscription = true;
                    break;
                }

            }
        }
        if (bUpdateSubscription)
        {
            sDataLabel = "Update Subscription with Stripe Checkout";
        }
        return sDataLabel;
    }


    protected void Page_Load(object sender, EventArgs e)
    {

        //pnlStripButton.Visible = false;
        if (Session["User"] == null)
            return;


        _theAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));
        iOriginalAccountTypeID = (int)_theAccount.AccountTypeID;
        //_theAccountType = SecurityManager.AccountType_Details((int)_theAccount.AccountTypeID);



        _theUser = (User)Session["User"];
        if (!IsPostBack)
        {
            PopulateAccountType();

            //if(Request.QueryString["accounttypeid"]!=null)
            //{
            //    string sATID = Request.QueryString["accounttypeid"].ToString();
            //    if (ddlSAccountType.Items.FindByValue(sATID) != null)
            //        ddlSAccountType.SelectedValue = sATID;
            //}


            if (Session["PendingBillingID"] == null)
            {
                string sBillingID = Common.GetValueFromSQL(@"SELECT BillingID FROM Billing WHERE AccountID = " + _theAccount.AccountID.ToString() +
                         @"  AND (PaymentSuccess=0 OR PaymentSuccess IS NULL) ORDER BY BillingID DESC");
                if(!string.IsNullOrEmpty(sBillingID))
                {
                    Session["PendingBillingID"] = sBillingID;
                }
            }


            if (Session["PendingBillingID"] == null)
            {
                //return;

                if (Request.QueryString["BillingNo"] != null)
                {
                    hfBillingID.Value = Request.QueryString["BillingNo"].ToString();
                    Session["SelectedAccountTypeID"] = null;
                }


                //BillingAPI.Billing newBilling = new BillingAPI.Billing();
                //newBilling.AccountID = (int)_theAccount.AccountID;

                //int? iAccountTypeID = null;
                //if (Session["SelectedAccountTypeID"] != null && Session["SelectedAccountTypeID"].ToString() != "")
                //{
                //    int iOut = 0;
                //    if (int.TryParse(Session["SelectedAccountTypeID"].ToString(),out iOut))
                //    {
                //        iAccountTypeID = iOut;
                //    }
                //}
                //newBilling.Amount = BillingAPI.TotalChargeAmount((int)_theAccount.AccountID, null, iAccountTypeID, null);
                //newBilling.IsYearlySubscription = BillingAPI.IsYearlySubscription(newBilling.AccountID);
                //newBilling.BillingDate = DateTime.Now;
                //newBilling.Currency = BillingAPI.GetAccountCurrency(newBilling.AccountID);
                //newBilling.NoOfUsers = BillingAPI.UsersPerAccount(newBilling.AccountID);
                //newBilling.PaymentMethod = BillingAPI.GetAccountPaymentMethod((int)_theAccount.AccountID);
                ////newBilling.
                //Int32 billingid = BillingAPI.CreateBilling(newBilling);
                //Session["PendingBillingID"] = billingid;
                //hfBillingID.Value = billingid.ToString();
            }
            else
            {
                hfBillingID.Value = Session["PendingBillingID"].ToString();                
            }
        }
        if (hfBillingID.Value != "")
            _theBilling = BillingAPI.GetBilling(int.Parse(hfBillingID.Value));

        if(!IsPostBack & _theBilling!=null)
        {
            lblTitle.Text = "Invoice Billing No: " + _theBilling.BillingID.ToString();
            txtAccountName.Text = _theAccount.AccountName;
            txtFirstName.Text = _theUser.FirstName;
            txtLastName.Text = _theUser.LastName;
            //txtAccountType.Text = _theAccountType.AccountTypeName;

            string sAccountTypeID = _theAccount.AccountTypeID.ToString();

            //int iVisibleBillingAmount = _theBilling.Amount;

            if(Session["SelectedAccountTypeID"]!=null && Session["SelectedAccountTypeID"].ToString()!="")
            {
                sAccountTypeID = Session["SelectedAccountTypeID"].ToString();
                _theBilling.Amount = BillingAPI.TotalChargeAmount((int)_theAccount.AccountID, null, int.Parse(sAccountTypeID), null);
                _theBilling.TargetAccountTypeID = int.Parse(sAccountTypeID);
                BillingAPI.UpdateBilling(_theBilling);
                
            }

            if (ddlSAccountType.Items.FindByValue(sAccountTypeID) != null)
                ddlSAccountType.SelectedValue = sAccountTypeID;


            lblAmountTobePaid.Text = "$" + _theBilling.Amount.ToString() + " " + _theBilling.Currency;

            //if(!string.IsNullOrEmpty(_theBilling.Remarks))
            //{
            //    lblAmountTobePaid.Text= lblAmountTobePaid.Text + " for " + _theBilling.Remarks;
            //}

            txtNoOfUsers.Text = _theBilling.NoOfUsers.ToString();
            if(_theBilling.IsYearlySubscription!=null)
            {
                if((bool)_theBilling.IsYearlySubscription)
                {
                    radioSubscriptionInterval.SelectedValue = "y";
                }
                else
                {
                    radioSubscriptionInterval.SelectedValue = "m";
                }
            }
            else
            {
                if (BillingAPI.IsYearlySubscription((int)_theAccount.AccountID))
                {
                    radioSubscriptionInterval.SelectedValue = "y";
                }
                else
                {
                    radioSubscriptionInterval.SelectedValue = "m";
                }
            }            

            lblAmountTobePaid.Text = "$" + _theBilling.Amount.ToString() + " " + _theBilling.Currency;// + " " + radioSubscriptionInterval.SelectedItem.Text;

            //if (Request.QueryString["BillingNo"] != null)
            //{
               
            //    pnlStripButton.Visible = false;
            //    ddlSAccountType.Enabled = false;
            //    radioSubscriptionInterval.Enabled = false;
            //}



        }


        if (IsPostBack)
        {
            if (Request.Form["stripeToken"] != null)
            {
                string stripeEmail = Request.Form["stripeEmail"].ToString();
                //lblMessage.Text += "<br />Email:" + stripeEmail;
                //lblMessage.Text += "<br />Stripe Token:" + Request.Form["stripeToken"].ToString();
                //pnlStripButton.Visible = false;

                //var secretKey = WebConfigurationManager.AppSettings["StripeSecretKey"];
                var secretKey = SystemData.SystemOption_ValueByKey_Account("StripeSecretKey", null, null);
                StripeConfiguration.SetApiKey(secretKey);


                var cus_options = new StripeCustomerCreateOptions
                {
                    Email = stripeEmail,//"jenny.rosen@example.com",
                    Metadata = new Dictionary<string, string>()
                        {
                            { "AccountID", Session["AccountID"].ToString() }
                        }
                };
                var cus_service = new StripeCustomerService();


                var optionsCusList = new StripeCustomerListOptions();
                optionsCusList.Limit = 10000;
                var customers = cus_service.List(optionsCusList);
                //string sTest = "";
                bool bUpdateSubscription = false;
                StripeCustomer customer = new StripeCustomer();
                foreach (var customerEach in customers)
                {
                    if (cus_options.Email.ToLower().Trim() == customerEach.Email.ToLower().Trim())
                    {
                        System.Collections.Generic.Dictionary<string, string> cusMetaData = customerEach.Metadata;
                        if (cusMetaData != null && cusMetaData["AccountID"].ToString() == Session["AccountID"].ToString())
                        {
                            customer = customerEach;
                            bUpdateSubscription = true;
                            break;
                        }

                    }
                }

                if (customer.Id == null)
                    customer = cus_service.Create(cus_options);


                if (bUpdateSubscription)
                {
                    BillingAPI.UpdateStripeSubscription(int.Parse(Session["AccountID"].ToString()),int.Parse(ddlSAccountType.SelectedValue));
                }
                else
                {
                    int iPlanQuantity = int.Parse(GetTotalPaymentAmount());
                    var items = new List<StripeSubscriptionItemOption> {
                    new StripeSubscriptionItemOption {
                            PlanId = BillingAPI.StripeAccountPlanID(int.Parse(Session["AccountID"].ToString())),
                            Quantity=iPlanQuantity
                        }
                    };

                    var optionsSubCreate = new StripeSubscriptionCreateOptions
                    {
                        Items = items,
                        Source = Request.Form["stripeToken"].ToString(),
                        Metadata = new Dictionary<string, string>()
                        {
                            { "AccountID", Session["AccountID"].ToString() }
                        }
                    };
                    var service = new StripeSubscriptionService();
                    StripeSubscription subscription = service.Create(customer.Id, optionsSubCreate);
                }
                //_theAccount.AccountTypeID = int.Parse( ddlSAccountType.SelectedValue);
                _theBilling.TargetAccountTypeID = int.Parse(ddlSAccountType.SelectedValue);
                BillingAPI.UpdateBilling(_theBilling);
                BillingAPI.AdjustUsersForAccountType((int)_theAccount.AccountTypeID, int.Parse(ddlSAccountType.SelectedValue), (int)_theAccount.AccountID);
                //SecurityManager.Account_Update(_theAccount);
            }
        }

    }

    //protected void btnStripe_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("~/Pages/Security/StripePayment.aspx", false);
    //}

    protected void UpdateTheCurrentBilling()
    {
        if (radioSubscriptionInterval.SelectedValue == "y")
        {
            Session["IsYearlySubscription"] = true;
        }
        else
        {
            Session["IsYearlySubscription"] = false;
        }
        Session["SelectedAccountTypeID"] = ddlSAccountType.SelectedValue;
        int? iAccountTypeID = int.Parse(ddlSAccountType.SelectedValue);
        bool bYearly = radioSubscriptionInterval.SelectedValue == "y" ? true : false;
        _theBilling.Amount = BillingAPI.TotalChargeAmount((int)_theAccount.AccountID, null, iAccountTypeID, bYearly);
        //_theBilling.BillingDate = DateTime.Now;
        _theBilling.IsYearlySubscription = bool.Parse(Session["IsYearlySubscription"].ToString());
        _theBilling.TargetAccountTypeID = iAccountTypeID;
        BillingAPI.UpdateBilling(_theBilling);
        lblAmountTobePaid.Text = "$" + _theBilling.Amount + " " + _theBilling.Currency;
    }

    protected void ddlSAccountType_TextChanged(object sender, EventArgs e)
    {
        try
        {
            
            UpdateTheCurrentBilling();
            //DisplayCostPerMonth()
            if (int.Parse(ddlSAccountType.SelectedValue) > (int)_theAccount.AccountTypeID)
            {

            }
            else
            {
                BillingAPI.AdjustUsersForAccountType((int)_theAccount.AccountTypeID, int.Parse(ddlSAccountType.SelectedValue), (int)_theAccount.AccountID);
                _theAccount.AccountTypeID = int.Parse(ddlSAccountType.SelectedValue);
                SecurityManager.Account_Update(_theAccount);
                if (int.Parse(ddlSAccountType.SelectedValue) == 1)
                {
                    BillingAPI.CancelStripeSubscription((int)_theAccount.AccountID, _theUser);//cancel subscription
                    Session["tdbmsgpb"] = "We have cancelled your subscription.";
                }
                else
                {
                    BillingAPI.UpdateStripeSubscription((int)_theAccount.AccountID,int.Parse(ddlSAccountType.SelectedValue));//update subscription
                    Session["tdbmsgpb"] = "Your subscription has been updated.";
                }
            }



        }
        catch (Exception ex)
        {
            Response.Redirect("~/Default.aspx");
        }
    }

    //protected void AdjustUsers()
    //{
    //    if (int.Parse(ddlSAccountType.SelectedValue) < iOriginalAccountTypeID)
    //    {
    //        //what to do?
    //        try
    //        {

    //            string sMaxUserAllowed = Common.GetValueFromSQL("SELECT MaxUsers FROM [AccountType] WHERE AccountTypeID=" + ddlSAccountType.SelectedValue);
    //            string sNoOfUsers = Common.GetValueFromSQL(@"SELECT COUNT(U.UserID) FROM [User] U JOIN [UserRole] UR 
    //                                        ON U.UserID=UR.UserID WHERE AccountID=" + _theAccount.AccountID.ToString() + @" AND UR.IsPrimaryAccount=1 AND U.IsActive=1");
    //            int iMaxUserAllowed = int.Parse(sMaxUserAllowed);
    //            int iNoOfUsers = int.Parse(sNoOfUsers);

    //            if (iNoOfUsers > iMaxUserAllowed)
    //            {
    //                int iInActiveUser = iNoOfUsers - iMaxUserAllowed;
    //                if (iInActiveUser > 0)
    //                {
    //                    Common.ExecuteText(@"UPDATE [User] SET IsActive=0 WHERE UserID IN 
    //                                (SELECT top " + iInActiveUser + @" U.UserID FROM [User] U JOIN [UserRole] UR ON U.UserID=UR.UserID
    //                                WHERE AccountID=" + _theAccount.AccountID.ToString() + @"  AND U.IsActive=1 AND UR.IsPrimaryAccount=1 AND IsAccountHolder=0 ORDER BY U.UserID DESC)");
    //                }

    //            }

    //        }
    //        catch (Exception ex)
    //        {

    //        }

    //    }
    //}

    protected void btnPaypal_Click(object sender, EventArgs e)
    {
        //Response.Redirect("~/Pages/Security/PaypalPaymentJS.aspx", false);

        Response.Redirect("~/Pages/Security/PaypalPaymentAPI.aspx", false);
    }

    protected void radioSubscriptionInterval_SelectedIndexChanged(object sender, EventArgs e)
    {
        UpdateTheCurrentBilling();
    }
}
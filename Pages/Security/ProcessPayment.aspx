﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true" CodeFile="ProcessPayment.aspx.cs" Inherits="ProcessPayment" %>

<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <asp:Panel ID="Panel2" runat="server" style="width:100%">
        <div runat="server" id="div1">
            <table border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td colspan="3" height="40">
                        <table>
                            <tr>
                                <td style="width: 400px;">
                                    <span class="TopTitle">
                                        <asp:Label runat="server" ID="lblTitle" Text="Invoice Billing No. #00000001"></asp:Label>
                                        <asp:HiddenField runat="server" ID="hfBillingID" />
                                    </span>
                                </td>
                                <td></td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 20px;">
                        <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                           <div>
                                                <table>
                                                    <tr>
                                                        <td>Account Name:</td>
                                                        <td>
                                                            <asp:Textbox ID="txtAccountName" Width="300px" runat="server" Enabled="false" Text="Ricky Pablo" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>First Name:</td>
                                                        <td>
                                                            <asp:Textbox ID="txtFirstName" Width="300px" runat="server" Enabled="false" Text="Ricky" />
                                                        </td>
                                                    </tr>                                                    
                                                    <tr>
                                                        <td>Last Name:</td>
                                                        <td>
                                                            <asp:Textbox ID="txtLastName" Width="300px" runat="server" Enabled="false" Text="Pablo" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>No. Of Users:</td>
                                                        <td>
                                                            <asp:Textbox ID="txtNoOfUsers" Width="300px" runat="server" Enabled="false" Text="3" />
                                                        </td>
                                                    </tr>                                                                                                                                                            
                                                    <tr>
                                                        <td>Account Type:</td>
                                                        <td>
                                                            <%--<asp:Textbox ID="txtAccountType" Width="300px" runat="server" Enabled="false" Text="Team" />--%>
                                                             <asp:DropDownList runat="server" ID="ddlSAccountType" CssClass="NormalTextBox" OnTextChanged="ddlSAccountType_TextChanged"
                                                            AutoPostBack="true" DataTextField="AccountTypeName" DataValueField="AccountTypeId" Width="279px"></asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td>Subscription Interval:</td>
                                                        <td>
                                                            <%--<asp:Textbox ID="txtSubscriptionInterval" Width="300px" runat="server" Enabled="false" Text="Monthly" />--%>
                                                           <asp:RadioButtonList runat="server" ID="radioSubscriptionInterval"
                                                               RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="radioSubscriptionInterval_SelectedIndexChanged">
                                                            <asp:ListItem Value="m" Text="Monthly" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Value="y" Text="Yearly(Save 10%)"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                   <%-- 
                                                   <tr>
                                                        <td>Cost Per Month:</td>
                                                        <td>
                                                            <asp:Label ID="lblCoastPerMonth" runat="server" ForeColor="Red" Font-Size="18px" Text="$10 AUD (Team Subscription) x 3 (No. Of Users)" />                                                           
                                                        </td>
                                                    </tr>
                                                   <tr>
                                                        <td>Amount to be paid Monthly:</td>
                                                        <td>
                                                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Font-Size="25px" Text="$30 AUD per Month" />                                                           
                                                        </td>
                                                    </tr>
                                                   <tr>
                                                        <td>Subscription Status:</td>
                                                        <td>
                                                            <asp:Label ID="Label2" runat="server" ForeColor="Red" Font-Size="15px" Text="UNSUBSCRIBED" />                                                           
                                                        </td>
                                                    </tr>
                                                    --%>
                                                   <tr>
                                                        <td>Amount to be paid</td>
                                                        <td>
                                                            <asp:Label ID="lblAmountTobePaid" runat="server"  Text="$10 AUD for 1 Additional User" />                                                           
                                                        </td>
                                                    </tr>
                                                   <tr>
                                                        <td>Payment Status</td>
                                                        <td>
                                                            <asp:Label ID="lblPaymentStatus" runat="server" Text="PENDING" />                                                           
                                                        </td>
                                                    </tr>
                                                   <%-- <tr>
                                                        <td>Credit Card Name:</td>
                                                        <td>
                                                            <asp:Textbox ID="Textbox1" Width="300px" runat="server" />
                                                        </td>
                                                    </tr>--%>
                                                    <%--<tr>
                                                        <td>Credit Card No.:</td>
                                                        <td>
                                                            <asp:Textbox ID="Textbox2" Width="300px" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Expiry Date</td>
                                                        <td>
                                                            <asp:Textbox ID="Textbox3" Width="50px" runat="server" placeholder="MM" />/
                                                            <asp:Textbox ID="Textbox4" Width="50px" runat="server" placeholder="YYYY" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>CSV:</td>
                                                        <td>
                                                            <asp:Textbox ID="Textbox5" Width="50px" runat="server" />
                                                        </td>
                                                    </tr>--%>

                                                    <tr>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <%--<asp:Button runat="server" ID="btnStripe" OnClick="btnStripe_Click" Text="Pay Now using Stripe" />--%>

                                                                        <div style="padding: 50px; min-height: 300px;">

                                                                            <asp:Panel runat="server" ID="pnlStripButton">
                                                                                <script
                                                                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                                                    data-label="<%=DataLabel() %>"
                                                                                    data-key="<%= stripePublishableKey %>"
                                                                                    data-amount="<%= GetTotalPaymentAmount() %>"
                                                                                    data-currency="<%= GetCurrency() %>"
                                                                                    data-name="DBGurus Australia"
                                                                                    data-description="<%=DataDescription() %>"
                                                                                    data-email="<%= GetUserEmail() %>"
                                                                                    data-locale="auto"
                                                                                    data-zip-code="false"
                                                                                    data-panel-label="<%=DataPanelLabel() %>">
                                                                                </script>
                                                                            </asp:Panel>

                                                                        </div>


                                                                    </td>
                                                                    <td>
                                                                        <%--<asp:Button runat="server" ID="btnPaypal" OnClick="btnPaypal_Click" Text="Pay Now using Paypal" />--%>
                                                                    </td>
                                                                </tr>
                                                            </table>                                                           
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            
                                                        </td>
                                                    </tr>
                                                </table>

                                            </div>                                      
                    </td>
                </tr>

                <tr>
                    <td colspan="3" height="13"></td>
                </tr>
            </table>
        </div>
    </asp:Panel>



</asp:Content>


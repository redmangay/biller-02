﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Security_PaypalPaymentJS : System.Web.UI.Page
{
    User _theUser;
    Account _theAccount;
    BillingAPI.Billing _theBilling;
    protected void Page_Load(object sender, EventArgs e)
    {
        _theUser = (User)Session["User"];
        _theAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));
        _theBilling = BillingAPI.GetBilling(int.Parse(Session["PendingBillingID"].ToString()));

        string sSubscriptionID = BillingAPI.LatestSubscriptionID((int)_theAccount.AccountID);
        string sUserRedirectURL = "";



    }

    public string BillingAmount()
    {
        return (_theBilling.Amount * 100).ToString();
    }
    public string PaypalPlanID()
    {
        return BillingAPI.PaypalAccountPlanID((int)_theAccount.AccountID);
    }

}
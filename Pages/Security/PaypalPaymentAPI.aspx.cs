﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DBGPayment.DBGPaypal;
using System.Net;
using System.IO;
using Newtonsoft.Json;
public partial class Pages_Security_PaypalPaymentAPI : System.Web.UI.Page
{

    User _theUser;
    Account _theAccount;
    BillingAPI.Billing _theBilling;
    protected void Page_Load(object sender, EventArgs e)
    {
        _theUser = (User)Session["User"];
        _theAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));
        _theBilling = BillingAPI.GetBilling(int.Parse(Session["PendingBillingID"].ToString()));

        string sSubscriptionID = BillingAPI.LatestSubscriptionID((int)_theAccount.AccountID);
        string sUserRedirectURL = "";

        try
        {
            if (!string.IsNullOrEmpty(sSubscriptionID))
            {


                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/billing/subscriptions/"+sSubscriptionID+"/revise");

                request.Method = "POST";
                //request.Accept = "application/json";
                request.ContentType = "application/json";
                request.Headers.Add("Prefer: return=representation");
                string authInfo = BillingAPI.PaypalAuthInfo();
                request.Headers["Authorization"] = "Basic " + authInfo;

                Subscription theSubscription = new Subscription();
                //theSubscription.plan_id = BillingAPI.PaypalAccountPlanID((int)_theAccount.AccountID);
                theSubscription.plan_id = GetMonthlyPlan(_theBilling.Amount, _theBilling.Currency);

                //theSubscription.quantity = (_theBilling.Amount * 100).ToString();
                theSubscription.quantity = "1";

                //ShippingAmount theShippingAmount = new ShippingAmount();
                //theShippingAmount.currency_code = BillingAPI.GetAccountCurrency((int)_theAccount.AccountID);
                //theShippingAmount.value = _theBilling.Amount.ToString();

                //theSubscription.shipping_amount = theShippingAmount;


                Subscriber theSubscriber = new Subscriber();
                Name theName = new Name();
                theName.given_name = _theUser.FirstName;
                theName.surname = _theUser.LastName;
                theSubscriber.name = theName;

                theSubscriber.email_address = _theUser.Email;
                //theSubscriber.email_address = "dbgurustest2-buyer@gmail.com";

                theSubscription.subscriber = theSubscriber;

                theSubscription.auto_renewal = true;


                ApplicationContext theApplicationContext = new ApplicationContext();
                theApplicationContext.brand_name = "DBGurus";
                theApplicationContext.locale = "en-US";
                theApplicationContext.shipping_preference = "SET_PROVIDED_ADDRESS";
                theApplicationContext.user_action = "SUBSCRIBE_NOW";
                DBGPayment.DBGPaypal.PaymentMethod thePaymentMethod = new DBGPayment.DBGPaypal.PaymentMethod();
                thePaymentMethod.payer_selected = "PAYPAL";
                thePaymentMethod.payee_preferred = "IMMEDIATE_PAYMENT_REQUIRED";

                theApplicationContext.payment_method = thePaymentMethod;

                if (Request.Url.Authority.IndexOf("localhost") > -1)
                {
                    //handle fucking localhost
                    theApplicationContext.return_url = "http://ontask.thedatabase.net/Pages/Security/Paypal/PaypalWebhook.aspx";
                    theApplicationContext.cancel_url = "http://ontask.thedatabase.net/Pages/Security/Paypal/PaypalWebhook.aspx";

                }
                else
                {
                    theApplicationContext.return_url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "Pages/Security/Paypal/PaypalWebhook.aspx";
                    theApplicationContext.cancel_url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "Pages/Security/Paypal/PaypalWebhook.aspx";

                }

                theSubscription.application_context = theApplicationContext;



                string sSubscriptionJson = theSubscription.GetJSONString();








                using (StreamWriter swt = new StreamWriter(request.GetRequestStream()))
                {
                    swt.Write(sSubscriptionJson);
                }


                request.BeginGetResponse((r) =>
                {
                    try
                    {
                        HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
                        System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                        string strResponseJSON = reader.ReadToEnd().Trim();

                        DBGPayment.DBGPaypal.Subscription returnSubscription = (DBGPayment.DBGPaypal.Subscription)JsonConvert.DeserializeObject(strResponseJSON, typeof(DBGPayment.DBGPaypal.Subscription));
                        if (returnSubscription != null)
                        {
                            sSubscriptionID = returnSubscription.id;
                            sUserRedirectURL = returnSubscription.links[0].href;
                            //keep this fucking ID
                            //string sPlanID = thePlan.id;
                        }
                    }
                    catch (Exception x)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "Paypal payment api_int BeginGetResponse", x.Message, x.StackTrace, DateTime.Now, Request.Path);
                        SystemData.ErrorLog_Insert(theErrorLog);

                        //.... 
                    } // log the exception - 401 Unauthorized
                }, null);

                //update this subscription
                //HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/billing/subscriptions/" + sSubscriptionID);

                //request.Method = "PATCH";
                //request.ContentType = "application/json";
                //string authInfo = BillingAPI.PaypalAuthInfo();
                //request.Headers["Authorization"] = "Basic " + authInfo;

                //string sSubscriptionPATCH = "[" +
                //                       "{" +
                //                         " \"op\": \"replace\"," +
                //                         " \"path\": \"/billing_info/outstanding_balance\"," +
                //                            " \"value\": { " +
                //                                " \"currency_code\": \"" + BillingAPI.GetAccountCurrency((int)_theAccount.AccountID) + "\"," +
                //                                "\"value\": \"" + (_theBilling.Amount).ToString() + "\" " +
                //                            " }" +
                //                             "}" +
                //                      "]";

                ////     Gets a System.IO.Stream object to use to write request data.
                //using (StreamWriter swt = new StreamWriter(request.GetRequestStream()))
                //{
                //    swt.Write(sSubscriptionPATCH);
                //}
                ////HttpWebResponse response2;
                //request.BeginGetResponse((r) =>
                //{
                //    try
                //    {
                //        HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
                //        System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                //        string strResponseJSON = reader.ReadToEnd().Trim();

                //        //Product theProduct = (Product)JsonConvert.DeserializeObject(strResponseJSON, typeof(Product));
                //        //if (theProduct != null)
                //        //{

                //        //    string sProductID = theProduct.id;

                //        //}

                //    }
                //    catch (Exception ex)
                //    {
                //        //.... 
                //    } // log the exception - 401 Unauthorized
                //}, null);

                //string sJSONWebResponse2 = "";// Response.Output.ToString();




            }
            else
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/billing/subscriptions");

                request.Method = "POST";
                request.Accept = "application/json";
                request.ContentType = "application/json";
                request.Headers.Add("Prefer: return=representation");
                string authInfo = BillingAPI.PaypalAuthInfo();
                request.Headers["Authorization"] = "Basic " + authInfo;

                Subscription theSubscription = new Subscription();
                //theSubscription.plan_id = BillingAPI.PaypalAccountPlanID((int)_theAccount.AccountID);
                theSubscription.plan_id = GetMonthlyPlan(_theBilling.Amount, _theBilling.Currency);

                //theSubscription.quantity = (_theBilling.Amount * 100).ToString();
                theSubscription.quantity = "1";

                //ShippingAmount theShippingAmount = new ShippingAmount();
                //theShippingAmount.currency_code = BillingAPI.GetAccountCurrency((int)_theAccount.AccountID);
                //theShippingAmount.value = _theBilling.Amount.ToString();

                //theSubscription.shipping_amount = theShippingAmount;


                Subscriber theSubscriber = new Subscriber();
                Name theName = new Name();
                theName.given_name = _theUser.FirstName;
                theName.surname = _theUser.LastName;
                theSubscriber.name = theName;

                theSubscriber.email_address = _theUser.Email;
                //theSubscriber.email_address = "dbgurustest2-buyer@gmail.com";

                theSubscription.subscriber = theSubscriber;

                theSubscription.auto_renewal = true;


                ApplicationContext theApplicationContext = new ApplicationContext();
                theApplicationContext.brand_name = "DBGurus";
                theApplicationContext.locale = "en-US";
                theApplicationContext.shipping_preference = "SET_PROVIDED_ADDRESS";
                theApplicationContext.user_action = "SUBSCRIBE_NOW";
                DBGPayment.DBGPaypal.PaymentMethod thePaymentMethod = new DBGPayment.DBGPaypal.PaymentMethod();
                thePaymentMethod.payer_selected = "PAYPAL";
                thePaymentMethod.payee_preferred = "IMMEDIATE_PAYMENT_REQUIRED";

                theApplicationContext.payment_method = thePaymentMethod;

                if(Request.Url.Authority.IndexOf("localhost")>-1)
                {
                    //handle fucking localhost
                    theApplicationContext.return_url = "http://ontask.thedatabase.net/Pages/Security/Paypal/PaypalWebhook.aspx";
                    theApplicationContext.cancel_url = "http://ontask.thedatabase.net/Pages/Security/Paypal/PaypalWebhook.aspx";

                }
                else
                {
                    theApplicationContext.return_url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "Pages/Security/Paypal/PaypalWebhook.aspx";
                    theApplicationContext.cancel_url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "Pages/Security/Paypal/PaypalWebhook.aspx";

                }

                theSubscription.application_context = theApplicationContext;



                string sSubscriptionJson = theSubscription.GetJSONString();








                using (StreamWriter swt = new StreamWriter(request.GetRequestStream()))
                {
                    swt.Write(sSubscriptionJson);
                }


                request.BeginGetResponse((r) =>
                {
                    try
                    {
                        HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
                        System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                        string strResponseJSON = reader.ReadToEnd().Trim();

                        DBGPayment.DBGPaypal.Subscription returnSubscription = (DBGPayment.DBGPaypal.Subscription)JsonConvert.DeserializeObject(strResponseJSON, typeof(DBGPayment.DBGPaypal.Subscription));
                        if (returnSubscription != null)
                        {
                            sSubscriptionID = returnSubscription.id;
                            sUserRedirectURL = returnSubscription.links[0].href;
                            //keep this fucking ID
                            //string sPlanID = thePlan.id;
                        }
                    }
                    catch (Exception x)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "Paypal payment api_int BeginGetResponse", x.Message, x.StackTrace, DateTime.Now, Request.Path);
                        SystemData.ErrorLog_Insert(theErrorLog);

                        //.... 
                    } // log the exception - 401 Unauthorized
                }, null);
            }
        }
        catch(Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Paypal payment api_load", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
        }
       
        
                
        

        string sJSONWebResponse = "";// Response.Output.ToString();

        _theBilling.PaymentMethod = "P";
        _theBilling.SubscriptionID = sSubscriptionID;
        BillingAPI.UpdateBilling(_theBilling);

        //Response.Redirect(SecurityManager.PaypalAPIBasicURL() + "/webapps/billing/subscriptions?ba_token=Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL");
        Response.Redirect(sUserRedirectURL, false);
    }


    protected string GetMonthlyPlan(int iAmount, string sCurrency)
    {
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/billing/plans");

        request.Method = "POST";
        request.Accept = "application/json";
        request.ContentType = "application/json";
        request.Headers.Add("Prefer: return=representation");
        //request.Headers.Add("Accept-Language:en_US");
        //string authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL:EMegY388yteRgur0cy3uAi2adyaiqmd6MQSbmyZtHyVy6n4bJ25WC3s04jiPICUj0l9OzJqZZiFcUcet"));
        string authInfo = BillingAPI.PaypalAuthInfo();
        request.Headers["Authorization"] = "Basic " + authInfo;

        DBGPayment.DBGPaypal.Plan newPlan = new DBGPayment.DBGPaypal.Plan();
        newPlan.product_id = BillingAPI.PaypalProductID();// "PROD -0TF91806F69784250";
        newPlan.name = "OnTask Monthly Plan";
        newPlan.description = "OnTask Monthly Plan Description";

        Frequency theFrequency = new Frequency();
        theFrequency.interval_count = 1;
        theFrequency.interval_unit = BillingAPI.PaypalMonthlyInterval();// "MONTH";
        //BillingCycle theBillingCycle = new BillingCycle();
        //theBillingCycle.frequency = theFrequency;
        //theBillingCycle.tenure_type = "TRIAL";
        //theBillingCycle.sequence = 1;
        //theBillingCycle.total_cycles = 1;

        Frequency theFrequency2 = new Frequency();
        theFrequency2.interval_count = 1;
        theFrequency2.interval_unit = BillingAPI.PaypalMonthlyInterval(); //"MONTH";
        BillingCycle theBillingCycle2 = new BillingCycle();
        theBillingCycle2.frequency = theFrequency2;
        theBillingCycle2.tenure_type = "REGULAR";
        theBillingCycle2.sequence = 1;
        theBillingCycle2.total_cycles = 99;

        FixedPrice theFixedPrice = new FixedPrice();
        theFixedPrice.value =iAmount.ToString();//0.01
        theFixedPrice.currency_code = sCurrency;
        PricingScheme thePricingScheme = new PricingScheme();
        thePricingScheme.fixed_price = theFixedPrice;

        theBillingCycle2.pricing_scheme = thePricingScheme;

        PaymentPreferences thePaymentPreferences = new PaymentPreferences();
        thePaymentPreferences.service_type = "PREPAID";
        thePaymentPreferences.auto_bill_outstanding = true;
        //SetupFee theSetupFee = new SetupFee();
        //theSetupFee.value = "0";
        //theSetupFee.currency_code = "AUD";
        //thePaymentPreferences.setup_fee = theSetupFee;
        thePaymentPreferences.setup_fee_failure_action = "CONTINUE";
        thePaymentPreferences.payment_failure_threshold = 3;

        List<BillingCycle> lstBillingCycle = new List<BillingCycle>();
        //lstBillingCycle.Add(theBillingCycle);
        lstBillingCycle.Add(theBillingCycle2);

        newPlan.billing_cycles = lstBillingCycle;

        newPlan.payment_preferences = thePaymentPreferences;
        newPlan.quantity_supported = true;

        //Taxes theTaxes = new Taxes();
        //theTaxes.percentage = "10";
        //theTaxes.inclusive = false;
        //newPlan.taxes = theTaxes;


        string sPlanJson = newPlan.GetJSONString();

        //     Gets a System.IO.Stream object to use to write request data.
        using (StreamWriter swt = new StreamWriter(request.GetRequestStream()))
        {
            swt.Write(sPlanJson);
        }
        string sPlanID = "";

        //HttpWebResponse response2;
        request.BeginGetResponse((r) =>
        {
            try
            {
                HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
                System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                string strResponseJSON = reader.ReadToEnd().Trim();

                DBGPayment.DBGPaypal.Plan thePlan = (DBGPayment.DBGPaypal.Plan)JsonConvert.DeserializeObject(strResponseJSON, typeof(DBGPayment.DBGPaypal.Plan));
                if (thePlan != null)
                {
                    sPlanID = thePlan.id;
                }
            }
            catch (Exception x)
            {
                //.... 
            } // log the exception - 401 Unauthorized
        }, null);

        return sPlanID;
    }

}



//HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/billing/subscriptions");

//request.Method = "POST";
//        request.Accept = "application/json";
//        request.ContentType = "application/json";
//        request.Headers.Add("Prefer: return=representation");
//        string authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL:EMegY388yteRgur0cy3uAi2adyaiqmd6MQSbmyZtHyVy6n4bJ25WC3s04jiPICUj0l9OzJqZZiFcUcet"));
//request.Headers["Authorization"] = "Basic " + authInfo;

//        Subscription theSubscription = new Subscription();
//theSubscription.plan_id = BillingAPI.PaypalAccountPlanID((int) _theAccount.AccountID);
//        //theSubscription.start_time = "2019-07-04T00:00:00Z";
//        //theSubscription.start_time = DateTime.UtcNow;
//        theSubscription.quantity = (_theBilling.Amount*100).ToString();

////ShippingAmount theShippingAmount = new ShippingAmount();
////theShippingAmount.currency_code = "USD";
////theShippingAmount.value = "10.00";

////theSubscription.shipping_amount = theShippingAmount;

//Subscriber theSubscriber = new Subscriber();
//Name theName = new Name();
//theName.given_name = _theUser.FirstName;
//        theName.surname = _theUser.LastName;
//        theSubscriber.name = theName;

//        theSubscriber.email_address = _theUser.Email;// "dbgurustest2-buyer@gmail.com";

//        //ShippingAddress theShippingAddress = new ShippingAddress();
//        //Name theShiName = new Name();
//        //theShiName.full_name = "John Doe";
//        //theShippingAddress.name = theShiName;
//        //Address theAddress = new Address();
//        //theAddress.address_line_1 = "2211 N First Street";
//        //theAddress.address_line_2 = "Building 17";
//        //theAddress.admin_area_2 = "San Jose";
//        //theAddress.admin_area_1 = "CA";
//        //theAddress.postal_code = "95131";
//        //theAddress.country_code = "US";
//        //theShippingAddress.address = theAddress;

//        //theSubscriber.shipping_address = theShippingAddress;

//        theSubscription.subscriber = theSubscriber;

//        theSubscription.auto_renewal = true;

//        //ApplicationContext theApplicationContext = new ApplicationContext();
//        //theApplicationContext.brand_name = "example";
//        //theApplicationContext.locale = "en-US";
//        //theApplicationContext.shipping_preference = "SET_PROVIDED_ADDRESS";
//        //theApplicationContext.user_action = "SUBSCRIBE_NOW";
//        //DBGPayment.DBGPaypal.PaymentMethod thePaymentMethod = new DBGPayment.DBGPaypal.PaymentMethod();
//        //thePaymentMethod.payer_selected = "PAYPAL";
//        //thePaymentMethod.payee_preferred = "IMMEDIATE_PAYMENT_REQUIRED";

//        //theApplicationContext.payment_method = thePaymentMethod;

//        //theApplicationContext.return_url = "https://svn.thedatabase.net/Pages/Security/Paypal/PaypalWebhook.aspx";
//        //theApplicationContext.cancel_url = "https://svn.thedatabase.net/Pages/Security/Paypal/PaypalWebhook.aspx";

//        //theSubscription.application_context = theApplicationContext;


//        string sSubscriptionJson = theSubscription.GetJSONString();

//        using (StreamWriter swt = new StreamWriter(request.GetRequestStream()))
//        {
//            swt.Write(sSubscriptionJson);
//        }


//        string sUserRedirectURL = "";


//request.BeginGetResponse((r) =>
//        {
//            try
//            {
//                HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
//System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
//string strResponseJSON = reader.ReadToEnd().Trim();

//DBGPayment.DBGPaypal.Subscription returnSubscription = (DBGPayment.DBGPaypal.Subscription)JsonConvert.DeserializeObject(strResponseJSON, typeof(DBGPayment.DBGPaypal.Subscription));
//                if (returnSubscription != null)
//                {
//                    string sID = returnSubscription.id;
//sUserRedirectURL = returnSubscription.links[0].href;
//                    //keep this fucking ID
//                    //string sPlanID = thePlan.id;
//                }
//            }
//            catch (Exception x)
//            {
//                //.... 
//            } // log the exception - 401 Unauthorized
//        }, null);

//        string sJSONWebResponse = "";// Response.Output.ToString();
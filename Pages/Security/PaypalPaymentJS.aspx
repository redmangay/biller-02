﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true" CodeFile="PaypalPaymentJS.aspx.cs" Inherits="Pages_Security_PaypalPaymentJS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

    <script type="text/javascript" src = "https://www.paypal.com/sdk/js?client-id=<%=BillingAPI.PaypalClientID() %>&vault=true" >
    </script>

    <div id="paypal-button-container"></div>

    <script type="text/javascript">
            //paypal.Buttons().render('#paypal-button-container');

        paypal.Buttons({

            createSubscription: function (data, actions) {

                return actions.subscription.create({

                    'plan_id': '<%=PaypalPlanID() %>',
                    'quantity':'<%=BillingAmount() %>'
                     
                });

            },


            onApprove: function (data, actions) {

                alert('You have successfully created subscription ' + data.subscriptionID);

            }


        }).render('#paypal-button-container');

    </script>

</asp:Content>


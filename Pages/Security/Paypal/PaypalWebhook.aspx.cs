using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Mail;
using DBGPayment.DBGPaypal;
using Newtonsoft.Json;
using DocGen.DAL;
public partial class PaypalWebhook : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
            StringBuilder sb = new StringBuilder();
            string strError = "";
            string sUserMessage = "Thank you.";
            if(Request.QueryString["subscription_id"]!=null)
            {
                string sSubscriptionID = Request.QueryString["subscription_id"].ToString();
                BillingAPI.Billing theBilling = BillingAPI.GetBilling(int.Parse(BillingAPI.BillingIDbySubscriptionID(sSubscriptionID)));
                string authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL:EMegY388yteRgur0cy3uAi2adyaiqmd6MQSbmyZtHyVy6n4bJ25WC3s04jiPICUj0l9OzJqZZiFcUcet"));

                //HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/billing/subscriptions/" + sSubscriptionID);

                //request.Method = "GET";
                //request.ContentType = "application/json";
                //request.Headers["Authorization"] = "Basic " + authInfo;


                //request.BeginGetResponse((r) =>
                //{
                //    try
                //    {
                //        HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
                //        System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                //        string strResponseJSON = reader.ReadToEnd().Trim();

                //        Subscription theSubscription=(Subscription)JsonConvert.DeserializeObject(strResponseJSON, typeof(Subscription));
                //        if(theSubscription!=null)
                //        {
                //            //string sStatus = theSubscription.status;
                //            if(theSubscription.status.ToLower() == "active")
                //            {


                //                if(theBilling!=null)
                //                {
                //                    theBilling.PaymentSuccess = true;
                //                    theBilling.PaymentDate = DateTime.Now;
                //                    BillingAPI.UpdateBilling(theBilling);
                //                    sUserMessage = sUserMessage + " " + "$" + theBilling.Amount + " " + theBilling.Currency + " payment succeeded.";
                //                    sb.AppendLine(sUserMessage);
                //                }
                //            }
                //        }                       

                //    }
                //    catch (Exception x)
                //    {
                //        //.... 
                //    } // log the exception - 401 Unauthorized
                //}, null);


                HttpWebRequest request2 = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/billing/subscriptions/"+sSubscriptionID+"/capture");

                request2.Method = "POST";
                //request2.Accept = "application/json";
                request2.ContentType = "application/json";
                //request2.Headers.Add("Prefer: return=representation");

                request2.Headers["Authorization"] = "Basic " + authInfo;

                string sCaptureJson = "{" +
                           "\"note\": \"Charging as the balance reached the limit\"," +
                           "\"capture_type\": \"OUTSTANDING_BALANCE\"," +
                           "\"amount\": {" +
                             "\"value\": \""+theBilling.Amount.ToString()+"\"," +
                             "\"currency_code\": \""+theBilling.Currency+"\"" +
                           "}" +
                                "}";


                using (StreamWriter swt = new StreamWriter(request2.GetRequestStream()))
                {
                    swt.Write(sCaptureJson);
                }

                request2.BeginGetResponse((r) =>
                {
                    try
                    {
                        HttpWebResponse response = request2.EndGetResponse(r) as HttpWebResponse; // Exception here
                        System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                        string strResponseJSON = reader.ReadToEnd().Trim();

                        //DBGPayment.DBGPaypal.Plan thePlan = (DBGPayment.DBGPaypal.Plan)JsonConvert.DeserializeObject(strResponseJSON, typeof(DBGPayment.DBGPaypal.Plan));
                        //if (thePlan != null)
                        //{
                        //    string sPlanID = thePlan.id;
                        //}
                    }
                    catch (Exception x)
                    {
                        //.... 
                    } // log the exception - 401 Unauthorized
                }, null);



            }

            //string sTest = "";

            foreach (String key in Request.QueryString.AllKeys)
            {
                sb.AppendLine("Key: " + key + " Value: " + Request.QueryString[key]);
            }
                     

            Common.SendSingleEmail("r_mohsin@yahoo.com", "Paypal IPN", sb.ToString(), ref strError);


            Response.Redirect("~/Default.aspx", false);


        }
        catch(Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Paypal Nofify URL", ex.Message, ex.StackTrace, DateTime.Now, "");
            SystemData.ErrorLog_Insert(theErrorLog);
        }


    }




    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    //Post back to either sandbox or live
    //    string strSandbox = "https://www.sandbox.paypal.com/cgi-bin/webscr";
    //    // string strLive = "https://www.paypal.com/cgi-bin/webscr";
    //    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strSandbox);

    //    //Set values for the request back
    //    req.Method = "POST";
    //    req.ContentType = "application/x-www-form-urlencoded";
    //    byte[] param = Request.BinaryRead(HttpContext.Current.Request.ContentLength);
    //    string strRequest = Encoding.ASCII.GetString(param);
    //    strRequest += "&cmd=_notify-validate";
    //    req.ContentLength = strRequest.Length;

    //    //for proxy
    //    //WebProxy proxy = new WebProxy(new Uri("http://url:port#"));
    //    //req.Proxy = proxy;

    //    //Send the request to PayPal and get the response
    //    StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII);
    //    streamOut.Write(strRequest);
    //    streamOut.Close();
    //    StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream());
    //    string strResponse = streamIn.ReadToEnd();
    //    streamIn.Close();

    //    Session["Reponse"] = strResponse;
    //    //if (strResponse == "VERIFIED")
    //    //{
    //    //    //UPDATE YOUR DATABASE

    //    //    TextWriter txWriter = new StreamWriter(Server.MapPath("Upload/") + Session["orderID"].ToString() + ".txt");
    //    //    sb.AppendLine(strResponse);
    //    //    txWriter.Close();

    //    //    //check the Invoice_status is Completed
    //    //    //check that txn_id has not been previously processed
    //    //    //check that receiver_email is your Primary PayPal email
    //    //    //check that Invoice_amount/Invoice_currency are correct
    //    //    //process Invoice
    //    //}
    //    //else if (strResponse == "INVALID")
    //    //{
    //    //    //UPDATE YOUR DATABASE

    //    //    TextWriter txWriter = new StreamWriter(Server.MapPath("Upload/") + Session["orderID"].ToString() + ".txt");
    //    //    sb.AppendLine(strResponse);
    //    //    //log for manual investigation
    //    //    txWriter.Close();
    //    //}
    //    //else
    //    //{  //UPDATE YOUR DATABASE

    //    //    TextWriter txWriter = new StreamWriter(Server.MapPath("Upload/") + Session["orderID"].ToString() + ".txt");
    //    //    sb.AppendLine("Invalid");
    //    //    //log response/ipn data for manual investigation
    //    //    txWriter.Close();
    //    //}
    //}
}

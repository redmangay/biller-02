using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class sendpayment_recurring : System.Web.UI.Page
{
    User _theUser;
    Account _theAccount;
    BillingAPI.Billing _theBilling;
    public string GetTotalPaymentAmount()
    {        
        return _theBilling.Amount.ToString();

    }
    public string GetInterval()
    {
        string sInterval = "M";
        if(_theBilling.IsYearlySubscription!=null && (bool)_theBilling.IsYearlySubscription)
        {
            sInterval = "Y";
        }
        return sInterval;
    }
    public string GetCurrency()
    {
        return _theBilling.Currency;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        _theUser = (User)Session["User"];
        _theAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));
        _theBilling = BillingAPI.GetBilling(int.Parse(Session["PendingBillingID"].ToString()));

    }
    public string GetReturnURL()
    {

        return Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "Pages/Security/Paypal/thanks.aspx";
    }
    public string Getcancel_returnURL()
    {

        return Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "Pages/Security/Paypal/cancel.aspx";
    }
    public string Getnotify_urlURL()
    {
        return "http://svn.thedatabase.net/Pages/Security/Paypal/PaypalWebhook.aspx";

        //return Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "Pages/Security/Paypal/PaypalWebhook.aspx";
    }

}

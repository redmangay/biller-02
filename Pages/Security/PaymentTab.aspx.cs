﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class Pages_Security_PaymentTab : System.Web.UI.Page
{

    public string stripePublishableKey = SystemData.SystemOption_ValueByKey_Account("StripePublishableKey", null, null);
    User _theUser;

    public string GetUserEmail()
    {
        if (_theUser == null)
        {
            _theUser = (User)Session["User"];
        }
        return _theUser.Email;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        _theUser = (User)Session["User"];
        if (!IsPostBack)
        {
            gvInvoice.PageSize = 50;
            gvInvoice.GridViewSortColumn = "InvoiceID";
            gvInvoice.GridViewSortDirection = SortDirection.Descending;
            BindInvoiceGrid(0, gvInvoice.PageSize);

            grdPaymentMethod.PageSize = 50;
            BindPaymentMethodGrid();

        }
    }

    protected void BindPaymentMethodGrid()
    {
        grdPaymentMethod.DataSource = InvoiceManager.PaymentMethod_Select(int.Parse(Session["AccountID"].ToString()));
        grdPaymentMethod.DataBind();
    }
    protected void BindInvoiceGrid(int iStartIndex, int iMaxRows)
    {


        lblMsg.Text = "";

        try
        {
            int iTN = 0;
            gvInvoice.DataSource = InvoiceManager.ets_Invoice_Select(int.Parse(Session["AccountID"].ToString()), "", null,
                                null, null,"", null, "", "","", "", "", null, null,
                gvInvoice.GridViewSortColumn, gvInvoice.GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC",
                iStartIndex, iMaxRows, ref iTN);

            gvInvoice.VirtualItemCount = iTN;
            gvInvoice.DataBind();
            if (gvInvoice.TopPagerRow != null)
                gvInvoice.TopPagerRow.Visible = true;

            GridViewRow gvr = gvInvoice.TopPagerRow;
            if (gvr != null)
            {
                Common_Pager _gvPager = (Common_Pager)gvr.FindControl("Pager");                
            }            
            

        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Sensor Type", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }

    protected void grdPaymentMethod_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //if (e.Row.RowType == DataControlRowType.Header)
        //{
        //    HyperLink hlAddDetail = e.Row.FindControl("hlAddDetail") as HyperLink;
        //    if (hlAddDetail != null)
        //    {
        //        hlAddDetail.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/ChildTableDetail.aspx?ParentTableID=" + _qsTableID;
        //    }

        //}
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
            e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");



            //HyperLink hlEditDetail = e.Row.FindControl("hlEditDetail") as HyperLink;

            //if (hlEditDetail != null)
            //{
            //    hlEditDetail.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/ChildTableDetail.aspx?ParentTableID=" + _qsTableID + "&TableChildID=" + DataBinder.Eval(e.Row.DataItem, "TableChildID").ToString();
            //}

            Label lblPaymentMethod = e.Row.FindControl("lblPaymentMethod") as Label;

            if (lblPaymentMethod != null)
            {
                switch (DataBinder.Eval(e.Row.DataItem, "PaymentMethod").ToString())
                {
                    case "v":
                        lblPaymentMethod.Text = "Visa";
                        break;
                    case "m":
                        lblPaymentMethod.Text = "Mastercard";
                        break;
                    case "p":
                        lblPaymentMethod.Text = "Paypal";
                        break;
                    //case "alone":
                    //    lblPaymentMethod.Text = "One Record Only";
                    //    break;
                }
            }           

        }
    }

    protected void grdPaymentMethod_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "deletetype")
        {
            InvoiceManager.PaymentMethod_Delete(int.Parse(e.CommandArgument.ToString()));

            BindPaymentMethodGrid();

        }

    }


    protected void gvInvoice_PreRender(object sender, EventArgs e)
    {
        GridView grid = (GridView)sender;
        if (grid != null)
        {
            GridViewRow pagerRow = (GridViewRow)grid.TopPagerRow;
            if (pagerRow != null)
            {
                pagerRow.Visible = true;
            }
        }
    }

    protected void gvInvoice_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

        }

    }


}
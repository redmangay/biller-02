﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true" CodeFile="PaymentTab.aspx.cs" Inherits="Pages_Security_PaymentTab" %>

<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

    <script type="text/javascript">
        $(document).ready(function () {
            $(".stripe-button-el span").remove();
            $("button.stripe-button-el").removeAttr('style').css({
                "display": "inline-block",
                "width": "100%",
                "padding": "15px",
                "background": "#ffffff",
                "color": "#0153A4",
                "font-size": "1.3em"
            }).html("Add Credit Card...");
        });
    </script>



    <asp:Panel ID="Panel2" runat="server">
        <div runat="server" id="div1">
            <table border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td colspan="3" height="40">
                        <table>
                            <tr>
                                <td style="width: 400px;">
                                    <span class="TopTitle">
                                        <asp:Label runat="server" ID="lblTitle" Text="Payment Information"></asp:Label>
                                    </span>
                                </td>
                                <td></td>

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left: 20px;">
                        <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <ajaxToolkit:TabContainer ID="tcPayment" runat="server" CssClass="DBGTab">
                            <ajaxToolkit:TabPanel ID="tpPayment" runat="server">
                                <HeaderTemplate>
                                    <strong>Payment</strong>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <dbg:dbgGridView ID="gvInvoice" runat="server" GridLines="Both" CssClass="gridview" EmptyDataText="No Payment."
                                        HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center"
                                        AllowPaging="True" AllowSorting="True" DataKeyNames="InvoiceID" HeaderStyle-ForeColor="Black"
                                        Width="100%" AutoGenerateColumns="false" PageSize="50"
                                        OnPreRender="gvInvoice_PreRender" OnRowDataBound="gvInvoice_RowDataBound">
                                        <PagerSettings Position="Top" />
                                        <Columns>

                                            <asp:TemplateField Visible="false">
                                                <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:Label ID="LblID" runat="server" Text='<%# Eval("InvoiceID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField>
                                                 <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                 <ItemTemplate>
                                                     <asp:HyperLink ID="viewHyperLink" runat="server" ToolTip="View" NavigateUrl='<%# GetViewURL() + Cryptography.Encrypt(Eval("InvoiceID").ToString())  %>'
                                                         ImageUrl="~/App_Themes/Default/Images/iconShow.png" />
                                                 </ItemTemplate>
                                             </asp:TemplateField>--%>

                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDate" runat="server" Text='<%# Eval("PaidDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Payment">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPaidAmount" runat="server" Text='<%# Eval("PaidAmount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNotes" runat="server" Text='<%# Eval("Notes") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Users">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotalUsers" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Account Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAccountType" runat="server" Text='<%# Eval("AccountType") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="gridview_header" />
                                        <RowStyle CssClass="gridview_row" />
                                        <PagerTemplate>
                                            <asp:GridViewPager runat="server" ID="Pager" HideAdd="false" HideDelete="true" />
                                        </PagerTemplate>
                                    </dbg:dbgGridView>
                                    <br />

                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="tpPaymentMethod" runat="server">
                                <HeaderTemplate>
                                    <strong>Payment Methods</strong>

                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="upPaymentMethod" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="padding-left: 20px; padding-top: 10px;">
                                                <asp:GridView ID="grdPaymentMethod" runat="server" AutoGenerateColumns="False" DataKeyNames="PaymentMethodID" EmptyDataText="No Payment Method"
                                                    HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" CssClass="gridview"
                                                    OnRowCommand="grdPaymentMethod_RowCommand" OnRowDataBound="grdPaymentMethod_RowDataBound" AlternatingRowStyle-BackColor="#DCF2F0">
                                                    <RowStyle CssClass="gridview_row" />
                                                    <Columns>
                                                        <asp:TemplateField Visible="false">
                                                            <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("PaymentMethodID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/App_Themes/Default/Images/icon_delete.gif"
                                                                    CommandName="deletetype" CommandArgument='<%# Eval("PaymentMethodID") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField HeaderText="">
                                                            <ItemTemplate>
                                                                <asp:HyperLink runat="server" CssClass="popuplinkct" NavigateUrl="~/Pages/Record/ChildTableDetail.aspx"
                                                                    ImageUrl="~/App_Themes/Default/Images/iconEdit.png" ToolTip="Edit" ID="hlEditDetail"></asp:HyperLink>
                                                            </ItemTemplate>
                                                            <HeaderTemplate>
                                                                <asp:HyperLink runat="server" CssClass="popuplinkct" NavigateUrl="~/Pages/Record/ChildTableDetail.aspx"
                                                                    ImageUrl="~/Pages/Pager/Images/add.png" ToolTip="Add Child Table" ID="hlAddDetail" Visible="false"></asp:HyperLink>
                                                            </HeaderTemplate>
                                                        </asp:TemplateField>--%>
                                                        <asp:TemplateField ItemStyle-CssClass="sortHandle2">
                                                            <ItemStyle HorizontalAlign="Center" Width="10px" />
                                                            <ItemTemplate>
                                                                <asp:Image ID="Image4" runat="server" ImageUrl="~/App_Themes/Default/Images/MoveIcon.png"
                                                                    ToolTip="Drag and drop to change order" />
                                                                <input type="hidden" id='hfPaymentMethodID' value='<%# Eval("PaymentMethodID") %>' class='PaymentMethodID' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField HeaderText="Table">
                                                            <ItemTemplate>
                                                                <div style="padding-right: 10px;">
                                                                    <a target="_blank" href='<%# GetTableViewURL() + Cryptography.Encrypt(Eval("ChildTableID").ToString())  %>'>
                                                                        <%# Eval("ChildTableName")%></a>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderText="Payment Method">
                                                            <ItemTemplate>
                                                                <div style="padding-left: 10px;">
                                                                    <asp:Label runat="server" ID="lblPaymentMethod"></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Details">
                                                            <ItemTemplate>
                                                                <div style="padding-left: 10px;">
                                                                    <asp:Label runat="server" ID="lblDetails" Text='<%# Eval("Details") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Status">
                                                            <ItemTemplate>
                                                                <div style="padding-left: 10px;">
                                                                    <asp:Label runat="server" ID="lblStatus" Text='<%# Eval("Status") %>'></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>

                                                    <HeaderStyle CssClass="gridview_header" />
                                                </asp:GridView>
                                            </div>
                                            <br />
                                            <br />
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Panel runat="server" ID="pnlStripButton">
                                                            <script
                                                                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                                data-key="<%= stripePublishableKey %>"
                                                                data-amount="10000"
                                                                data-currency="AUD"
                                                                data-name="DBGurus.com.au"
                                                                data-description="eContractor Monthly Charge"
                                                                data-email="<%= GetUserEmail() %>"
                                                                data-locale="auto"
                                                                data-zip-code="false"
                                                                data-panel-label="Pay Monthly {{amount}}">
                                                            </script>
                                                        </asp:Panel>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </table>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="grdPaymentMethod" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </td>
                </tr>

                <tr>
                    <td colspan="3" height="13"></td>
                </tr>
            </table>
        </div>
    </asp:Panel>



</asp:Content>


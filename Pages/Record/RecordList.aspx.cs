﻿using System;
using System.Web.UI;
using System.Data;
using System.Text.RegularExpressions;
public class DBGRecord
{
    public int ID { get; set; }
    public string FieldValue { get; set; }
    public string ColumnToUpdate { get; set; }
}

public partial class Record_Record_List : SecurePage
{

    Table _theTable;
   

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public static void SaveOneCell(DBGRecord onerecord)
    {
        
       try
        {
            Record theRecord = RecordManager.ets_Record_Detail_Full(onerecord.ID, null, false);

            if (theRecord != null)
            {
                Table theTable = RecordManager.ets_Table_Details((int)theRecord.TableID);
                string strColumnID = Common.GetValueFromSQL("SELECT ColumnID FROM [Column] WHERE TableID=" + theRecord.TableID.ToString() + " AND SystemName='" + onerecord.ColumnToUpdate + "'");
                Column theColumn = RecordManager.ets_Column_Details(int.Parse(strColumnID));
                if (theColumn != null)
                {
                    if (theColumn.ColumnType == "number")
                    {

                        onerecord.FieldValue = onerecord.FieldValue.Replace(",", "");
                        onerecord.FieldValue = onerecord.FieldValue.Replace("$", "");

                        bool bHasValidation = false;
                        bool bHasAdvancedValidation = false;
                        string strHasAdvancedCondition = Common.GetValueFromSQL(
                        "SELECT TOP 1 AdvancedConditionID FROM AdvancedCondition WHERE ConditionType = 'notification' AND ConditionSubType = 'V' AND ColumnID = " +
                        theColumn.ColumnID.ToString());
                        if (/*strHasCondition != "" || */strHasAdvancedCondition != "")
                        {
                            bHasValidation = true;
                            bHasAdvancedValidation = true;
                        }
                        if (!bHasValidation)
                        {
                            if (theColumn.ValidationOnEntry != "" || theColumn.ValidationOnExceedance != "" ||
                                theColumn.ValidationOnWarning != "")
                            {
                                bHasValidation = true;
                            }
                        }
                        bool bIsInvalid = false;
                        if (bHasValidation)
                        {
                            string strTemp = String.Empty;
                            if (bHasAdvancedValidation && theColumn.ColumnID.HasValue)
                            {

                                DataTable dt =
                                    UploadWorld.ets_AdvancedCondition_Select((int)theColumn.ColumnID.Value, "notification",
                                        "V");

                                RecordManager.MakeTheRecord(ref theRecord, theColumn.SystemName, onerecord.FieldValue);
                                if (!UploadManager.IsDataValidAdvanced((int)theColumn.ColumnID.Value, theRecord, dt,
                                    ref strTemp))
                                {
                                    bIsInvalid = true;
                                }

                            }
                            else
                            {

                                bool bEmpty = theColumn.ValidationOnEntry == "";

                                if (theColumn.ValidationOnEntry != "" || bEmpty == true)
                                {
                                    if (!UploadManager.IsDataValid(onerecord.FieldValue, theColumn.ValidationOnEntry, ref strTemp,
                                        theColumn.ColumnType))
                                    {
                                        bIsInvalid = true;
                                    }
                                }
                            }
                        }

                        if (bIsInvalid == false)
                        {
                            Common.ExecuteText("UPDATE Record SET " + theColumn.SystemName + "='" + onerecord.FieldValue.Replace("'", "''") + "'" +
                           ",DateUpdated=GETDATE()" +
                           " WHERE RecordID =" + onerecord.ID.ToString());
                            RecordManager.MakeTheRecord(ref theRecord, theColumn.SystemName, onerecord.FieldValue);
                          


                            if (theTable.UniqueColumnID != null && (theColumn.ColumnID == theTable.UniqueColumnID ||
                                (theTable.UniqueColumnID2 != null && theColumn.ColumnID == theTable.UniqueColumnID2)))
                            {
                                UploadManager.Record_Set_UniqueKey("R", theTable.TableID, null, null, theRecord.RecordID.ToString());
                            }



                            bHasValidation = false;
                            strHasAdvancedCondition = Common.GetValueFromSQL(
                                "SELECT TOP 1 AdvancedConditionID FROM AdvancedCondition WHERE ConditionType = 'notification' AND ColumnID = " +
                                theColumn.ColumnID.ToString());
                            if (!String.IsNullOrEmpty(strHasAdvancedCondition))
                            {
                                bHasValidation = true;
                            }
                            else if (theColumn.ValidationOnEntry != "" || theColumn.ValidationOnExceedance != "" || theColumn.ValidationOnWarning != "")
                            {
                                bHasValidation = true;
                            }
                            else
                            {
                                string strHasCondition = Common.GetValueFromSQL("SELECT TOP 1 ConditionID FROM Condition WHERE ColumnID=" + theColumn.ColumnID.ToString());
                                if (strHasCondition != "")
                                {
                                    bHasValidation = true;
                                }
                            }
                            if (bHasValidation)
                            {
                                string strValidRecordIDs = theRecord.RecordID.ToString();
                                string strInvalidRecordIDs = "";
                                string strSQL = @"SELECT " + theColumn.SystemName + @",RecordID,WarningResults,ValidationResults FROM Record
	                    WHERE  RecordID IN (" + theRecord.RecordID.ToString() + @")";

                                RecordManager.ets_AdjustValidFormulaChanges(theColumn, ref strInvalidRecordIDs, ref strValidRecordIDs, true, strSQL);
                            }
                        }
                        else
                        {
                            return;
                        }



                    }//number end
                    else
                    {
                        Common.ExecuteText("UPDATE Record SET " + theColumn.SystemName + "='" + onerecord.FieldValue.Replace("'", "''") + "'" +
                         ",DateUpdated=GETDATE()" +
                         " WHERE RecordID =" + onerecord.ID.ToString());
                        RecordManager.MakeTheRecord(ref theRecord, theColumn.SystemName, onerecord.FieldValue);
                    }


                    DataTable dtColumn = Common.DataTableFromText("SELECT  ColumnID,  SystemName, Calculation,RoundNumber,TextType,DateCalculationType FROM [Column] WHERE Calculation IS NOT NULL AND TableID="
                + theTable.TableID.ToString() + " AND CHARINDEX('" + theColumn.SystemName + "]',Calculation,0)>0");

                    if (dtColumn != null && dtColumn.Rows.Count > 0)
                    {
                        DataTable dtColumnsAll = RecordManager.ets_Table_Columns_All((int)theTable.TableID);
                        {
                            for (int x = 0; x < dtColumn.Rows.Count; x++)
                            {
                                string strCalColumnID = dtColumn.Rows[x][0].ToString();
                                string strCalSystemName = dtColumn.Rows[x][1].ToString();
                                string CalcFormula = dtColumn.Rows[x][2].ToString();
                                int RoundNumber = int.Parse(dtColumn.Rows[x][3].ToString());
                                string TextType = dtColumn.Rows[x][4].ToString();
                                string DateCalculationType = dtColumn.Rows[x][5].ToString();
                                Column theCalColumn = RecordManager.ets_Column_Details(int.Parse(strCalColumnID));
                                string strValue = "";
                                string strTempValue = "";
                                int? iParentRecordID = null;
                                if (TextType == "d")
                                {
                                    //datetime calculation
                                    string strCalculation = CalcFormula;// _dtColumnsDetail.Rows[i]["Calculation"].ToString();
                                    try
                                    {

                                        strTempValue = TheDatabaseS.GetDateCalculationResult(ref dtColumnsAll, strCalculation, null, theRecord, iParentRecordID,
                                        DateCalculationType == "" ? "" : DateCalculationType,
                                        null, theTable, false);

                                        strValue = strTempValue;
                                    }
                                    catch
                                    {
                                        //
                                    }

                                }
                                else if (TextType == "t")
                                {
                                    //text calculation
                                    try
                                    {
                                        string strFormula = Common.GetCalculationSystemNameOnly(CalcFormula, (int)theTable.TableID);

                                        //Column theColumn = RecordManager.ets_Column_Details(int.Parse(ColumnID));
                                        strTempValue = TheDatabaseS.GetTextCalculationResult(ref dtColumnsAll, strFormula, null, theRecord, iParentRecordID, null, theTable, theCalColumn);
                                        strValue = strTempValue;
                                        //Common.ExecuteText("UPDATE Record SET " + strCalSystemName + "=" + "'" + strValue + "'" + " WHERE RecordID=" + RecordID);
                                    }
                                    catch
                                    {
                                        //
                                    }

                                }
                                else
                                {
                                    //number calculation
                                    try
                                    {

                                        if (CalcFormula.Contains("{"))
                                        {
                                            Regex r = new Regex(@"\{(V\d{3})\}");
                                            MatchCollection matches = r.Matches(CalcFormula);
                                            for (int nMatch = 0; nMatch < matches.Count; nMatch++)
                                            {
                                                string v = String.Empty;
                                                v = RecordManager.GetRecordValue(ref theRecord, matches[nMatch].Value.Replace("{", "").Replace("}", ""));

                                                CalcFormula = CalcFormula.Replace(matches[nMatch].Value, "'" + v + "'");
                                            }
                                            // "dbo.": a simple protection against SQL Injection
                                            strValue = Common.GetValueFromSQL(String.Format("SELECT dbo.{0}", CalcFormula));
                                        }
                                        else
                                        {
                                            string strFormula = Common.GetCalculationSystemNameOnly(CalcFormula, (int)theTable.TableID);

                                            strTempValue = TheDatabaseS.GetCalculationResult(ref dtColumnsAll, strFormula, null, theRecord, iParentRecordID, null, theTable, theCalColumn);
                                            strValue = strTempValue;
                                        }
                                    }
                                    catch
                                    {
                                        //
                                    }

                                }
                                double dTemp = 0;
                                if (double.TryParse(strValue, out dTemp))
                                {
                                    strValue = string.Format("{0:#,##0.00}", dTemp);
                                }
                                RecordManager.MakeTheRecord(ref theRecord, strCalSystemName, strValue);
                                RecordManager.ets_Record_Update(theRecord, false);

                            }
                        }
                    }//calculation end


                    if (theTable.SPSaveRecord != "")
                    {
                        try
                        {
                            //User _theUser = (User)Session["User"];
                            RecordManager.Table_SPSaveRecord(theTable.SPSaveRecord,
                                          theRecord.RecordID, null, "", theColumn.ColumnID);
                        }
                        catch
                        {
                            //
                        }
                    }




                    RecordManager.Record_Audit(null, theRecord.RecordID.ToString(), true, DateTime.Now);

                }

            }
        }
        catch(Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "InlineEditing", ex.Message, ex.StackTrace, DateTime.Now, "RecordList/Detail");
            SystemData.ErrorLog_Insert(theErrorLog);
        }

        //Common.ExecuteText("UPDATE [Record] SET "+onerecord.ColumnToUpdate+"='" + onerecord.FieldValue.Replace("'","''") + "' WHERE RecordID=" + onerecord.ID);
       
    }




    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (Session["RunSpeedLog"] != null)
        {
            SpeedLog theSpeedLog = new SpeedLog();
            theSpeedLog.FunctionName = _theTable.TableName + " Page_LoadComplete ";
            theSpeedLog.FunctionLineNumber = 33;
            SecurityManager.AddSpeedLog(theSpeedLog);
        }

        
    }




    protected override void OnSaveStateComplete(EventArgs e)
    {
        if (Session["RunSpeedLog"] != null)
        {
            SpeedLog theSpeedLog = new SpeedLog();
            theSpeedLog.FunctionName = _theTable.TableName + " OnSaveStateComplete ";
            theSpeedLog.FunctionLineNumber = 270;
            SecurityManager.AddSpeedLog(theSpeedLog);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {


        if (Request.QueryString["TableID"] != null)
        {
            _theTable = RecordManager.ets_Table_Details(int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString())));
            //reset stack
            //if (_theTable != null)
            //{
            //    Session["stackURL"] = null;
            //}
        }
        if(!IsPostBack)
        {
            Session["stackURL"] = null;
            Session["stackTabIndex"] = null;
            Session["stackTableTabID"] = null;

            Session["CopyRecordID"] = null;
            Session["quickdone"] = null;
            Session["controlvalue"] = null;
           
            Session["edittab"] = null;
        }

        //if (this.MasterPageFile.ToLower().IndexOf("rrp") > -1)
        //{          
        //    if (_theTable.HeaderColor != "")
        //    {
        //        ltTextStyles.Text = "<style>.pagerstyle{ background: #" + _theTable.HeaderColor + ";}.pagergradient{ background: #" + _theTable.HeaderColor + ";}.TopTitle{color:#FFFFFF;}</style>";                
        //    }
        //}


    }

    protected void btnReloadMe_Click(object sender, EventArgs e)
    {

        string strRawURL = Request.RawUrl;
        
        Response.Redirect(strRawURL, false);
        return;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            if(_theTable!=null && _theTable.SummaryPageContent!="")
            {
                lblSummaryPageContent.Text = _theTable.SummaryPageContent;
            }
            //Session["stackURL"] = null;
            //Session["CopyRecordID"] = null;
            //Session["quickdone"] = null;
            //Session["controlvalue"] = null;
            //Session["tabindex"] = null;
            //Session["edittab"] = null;
           

            /*End Modification*/



        }



        string sEditableOnSummary = Common.GetValueFromSQL("SELECT TOP 1 TableID FROM [Column] WHERE TableID="+_theTable.TableID.ToString()+" AND EditableOnSummary IS NOT NULL AND EditableOnSummary=1");

        if(!string.IsNullOrEmpty(sEditableOnSummary))
        {
            string sInlineEditingScript = @"
                  //$(document).ready(function () {
                  //      $(window).keydown(function (event) {
                  //          if (event.keyCode == 13) {
                  //              event.preventDefault();
                  //              return false;
                  //          }
                  //      });
                  //  });

                $(document).ready(function () {
                        $('.inlineeditingclass').dblclick(function(e) {
                                e.stopPropagation();
                                var currentEle = $(this);
                                var value = $(this).html();
                                var row = $(e.target).closest('tr');
                                var DBGRecordID = $(this).attr('recordid');
                                var vxxx = $(this).attr('systemname');
                                UpdateVal(currentEle, value, DBGRecordID, vxxx);
                            });

                        });


        ";


            ScriptManager.RegisterStartupScript(this, this.GetType(), "Inlineediting", sInlineEditingScript, true);
        }      

    }
}
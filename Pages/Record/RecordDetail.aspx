﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="RecordDetail.aspx.cs" Inherits="Record_Record_Detail"
    EnableEventValidation="false" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Pages/UserControl/RecordList.ascx" TagName="ChildTable" TagPrefix="asp" %>
<%--<%@ Register Src="~/Pages/UserControl/DetailView.ascx" TagName="CTDetail" TagPrefix="asp" %>--%>
<%@ Register Src="~/Pages/UserControl/DetailEdit.ascx" TagName="CTDetail" TagPrefix="asp" %>
<%@ Register Src="~/Pages/UserControl/MessageList.ascx" TagName="MLList" TagPrefix="asp" %>

<%@ Register TagPrefix="editor" Assembly="WYSIWYGEditor" Namespace="InnovaStudio" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">

    <asp:Literal ID="ltTextStyles" runat="server"></asp:Literal>
    <style type="text/css">
        g polygon, g path {
            cursor: pointer;
        }

        g:hover polygon, g:hover path {
            opacity: 1;
            fill-opacity: 0.5 !important;
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <asp:ScriptManagerProxy runat="server" ID="ScriptManagerProxy1"></asp:ScriptManagerProxy>


    <script src="../Document/Uploadify/jquery.uploadify.v2.1.4.js" type="text/javascript"></script>
    <script src="../Document/Uploadify/swfobject.js" type="text/javascript"></script>
    <link href="../Document/Uploadify/uploadify.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="<%=ResolveUrl("~/JS/utm_converter/utmconv.js")%>"></script>
    <script type="text/javascript">
        var originalValidationFunction = Page_ClientValidate;
        if (originalValidationFunction && typeof (originalValidationFunction) == "function") {
            Page_ClientValidate = function (validationGroup) {
                originalValidationFunction(validationGroup);

                if (!Page_IsValid) {
                    // your code here
                    $(".ajax-indicator-full").fadeOut();
                }
            };
        }
    </script>
    <script type="text/javascript">
        function checkImage(fileupload)
        {
            var filetype = $(fileupload).prop('files')[0].type;
            if (filetype.split('/')[0] != 'image') {
                alert("Invalid image file. Kindly upload *.jpg, *.jpeg, *.png or *.gif file");
                $(fileupload).val('');
            }
        }
    </script>
    <script type="text/javascript">
        function GeoConverter(easting, northing, utmz, hemisphere)
        {
            latlon = new Array(2);
            var x, y, zone, southhemi;

            if (isNaN(parseFloat(easting))) {
                //alert("Please enter a valid easting in the x field.");
                return false;
            }

            x = parseFloat(easting.replace(/[^\d\.\-]/g, ""));

            if (isNaN(parseFloat(northing))) {
                //alert("Please enter a valid northing in the y field.");
                return false;
            }

            y = parseFloat(northing.replace(/[^\d\.\-]/g, ""));

            if (isNaN(parseInt(utmz))) {
                //alert("Please enter a valid UTM zone in the zone field.");
                return false;
            }

            zone = parseFloat(utmz);

            if ((zone < 1) || (60 < zone)) {
                //alert("The UTM zone you entered is out of range.  " +
                //       "Please enter a number in the range [1, 60].");
                return false;
            }

            if (hemisphere == true)
                southhemi = true;
            else
                southhemi = false;

            UTMXYToLatLon(x, y, zone, southhemi, latlon);

            //document.frmConverter.txtLongitude.value = RadToDeg(latlon[1]);
            //document.frmConverter.txtLatitude.value = RadToDeg(latlon[0]);

            return { longtitude: RadToDeg(latlon[1]), latitude: RadToDeg(latlon[0]) };
        }

        function UTMConverter(longtitude, latitude) {
            var xy = new Array(2);

            if (isNaN(parseFloat(longtitude))) {
                //alert("Please enter a valid longitude in the lon field.");
                return false;
            }

            lon = parseFloat(longtitude.replace(/[^\d\.\-]/g, ""));

            if ((lon < -180.0) || (180.0 <= lon)) {
                //alert("The longitude you entered is out of range.  " +
                //       "Please enter a number in the range [-180, 180).");
                return false;
            }

            if (isNaN(parseFloat(latitude))) {
                //alert("Please enter a valid latitude in the lat field.");
                return false;
            }

            lat = parseFloat(latitude.replace(/[^\d\.\-]/g, ""));

            if ((lat < -90.0) || (90.0 < lat)) {
                alert("The latitude you entered is out of range.  " +
                       "Please enter a number in the range [-90, 90].");
                return false;
            }

            // Compute the UTM zone.
            zone = Math.floor((lon + 180.0) / 6) + 1;

            zone = LatLonToUTMXY(DegToRad(lat), DegToRad(lon), zone, xy);

            /* Set the output controls.  */
            //document.frmConverter.txtX.value = xy[0];
            //document.frmConverter.txtY.value = xy[1];
            //document.frmConverter.txtZone.value = zone;
            var hemisphere;
            if (lat < 0)
                hemisphere = "South";
            else
                hemisphere = "North"

            return {easting: xy[0], northing: xy[1], zone: zone, hemisphere: hemisphere};
        }
    </script>

    <script type="text/javascript" src="<%=Request.Url.Scheme+@"://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCgc_Iim3AMzz7jYQ8bdqVKh1mnGxz7Y88" %>"></script>
    <script language="javascript" type="text/javascript" src="<%=ResolveUrl("~/Editor/scripts/innovaeditor.js")%>"></script>
    <%-- Red removed this 22may2017 --%>

    <%-- <script type="text/javascript" src="<%=Request.Url.Scheme+@"://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js" %>"></script>
    <link href="<%=Request.Url.Scheme+@"://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/themes/base/jquery-ui.css" %>" rel="stylesheet" type="text/css" />--%>

    <%-- end Red --%>

    <style type="text/css">
        .topview {
            top: 50px;
            border: 1px solid #4F8FDD;
            padding: 10px;
        }

        table.multiple_cbl tbody {
            display: block; /*height: 160px;   */
            overflow: auto;
        }

        .paddingClass {
            padding-left: 10px;
            padding-right: 10px;
        }

        .HeadingClass {
            color: rgb(0,0,0);
            font-size: larger;
            text-decoration: none;
        }

        .TableDetailClass {
            border: 1px solid #505050;
            padding: 10px;
            overflow: auto;
        }

        .CalculateWaitClass {
            background-image: url(/images/wait-xsmall-6px.gif);
            background-repeat: no-repeat;
            background-position: right center;
        }

        .printReportBtn {
            background-color: #008CBA;
            border: none;
            color: white;
            padding: 13px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            /*font-weight: bold;*/
            font-size: 13px;
            margin: 2px 1px;
            cursor: pointer;
            border-radius: 4px;

        }
            .fancybox-close-small {
            padding: 2px !important;
            top: -55px !important;
            right: -16px;
        }
         

    </style>
    <script type="text/javascript">
        /* == Red 04112019: View Report using button field == */
        $(function () {
            $(".popupShowReport").fancybox({
                iframe: {
                    css: {
                        width: '95%',
                        height: '95%',
                        border: '10px solid white',
                        'border-radius': '4px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                titleShow: false

        });
        });
        function reportPopupClose() {
            $("#reportPopup").hide();
        }
      
        function ViewReport(reportId) {
            $(".ajax-indicator-full").fadeIn();
            var parameters = new Array();
            parameters.push({ Name: "Param_RecordID", Value: $('#hfRecordID').val() });
            //parameters.push({ Name: "Param2", Value: true });
            
            showReportPopup(reportId, parameters);
        }

    

        function showReportPopup(reportId, parameters) {  //, siteURLPrefix) {

            var url = "../../Pages/Document/GenerateReport.aspx?rnd=" + new Date().getTime();

            // Convert any date parameters from dd/MM/yyyy format to yyyy-MM-dd format.
            for (var i = 0; i < parameters.length; i++) {
                var isADateParameter = parameters[i].Value != null && (parameters[i].Value.length === 10 || parameters[i].Value.length === 16) && parameters[i].Value.indexOf('/') > 0;

                if (isADateParameter) {
                    parameters[i].Value = convertDDMMYYYYtoYYYYMMDD(parameters[i].Value);
                }
            }

            // Generate the report.
            $.post(url, { ReportID: reportId, Parameters: parameters }, function (response) {

                if (response.match(/^Error/)) {
                    alert(response);
                    $("#reportPopupFrame").attr("src", "");
                }
                else {
                    $(".ajax-indicator-full").fadeOut();
                    $("#showReportPopup").attr("href", response + "?rnd=" + new Date().getTime());
                    document.getElementById('showReportPopup').click();
                }
            });
        }

        function downloadReport(filePath, fileName) {  //, siteURLPrefix) {

            $(".ajax-indicator-full").fadeOut();
             // https://stackoverflow.com/a/27563953
            var downloadLink = document.getElementById('downloadReport');
            //$("#downloadReport").attr("href", filePath + "?rnd=" + new Date().getTime());
            downloadLink.href = filePath + "?rnd=" + new Date().getTime();
            downloadLink.download = fileName;
            downloadLink.click();          
        }
        /* == End Red View Report == */


        var arrHiddenFieldID = [];

        function AddSlideDownMsg() {
            document.getElementById("ctl00_lblNotificationMessage").innerHTML = "Record has been saved successfully! <a id=\"aNotificationMessageClose\" onclick=\"document.getElementById('divNotificationMessage').style.display = 'none';return false;\" href=\"#\" >Close</a>";
        }

        function ValidatorUpdateDisplay(val) {

            try {
                val.style.visibility = val.isvalid ? "hidden" : "visible";
                if (val.isvalid) {
                    if (val.controltovalidate.indexOf('radioV') > 0) {
                        document.getElementById(val.controltovalidate).style.border = '0px none';
                    }
                    else { document.getElementById(val.controltovalidate).style.border = '1px solid #909090'; }
                }
                else {
                    document.getElementById(val.controltovalidate).style.border = '2px solid red';
                }
            }
            catch (err) {
                //
            }
        }
        //function setSelectedValue(selectObj, valueToSet) {
        //    //setTimeout(setSelectedValue, 1000);
        //    if (selectObj != null) {
        //        alert(selectObj.options.length);
        //        for (var i = 0; i < selectObj.options.length; i++) {
        //            if (selectObj.options[i].value == valueToSet) {
        //                selectObj.options[i].selected = true;
        //                return;
        //            }
        //        }
        //    }
        //}

        /* === Red 30042019: Content Reason for change and Leaving page notification ===  */
        var reasonForChangeContent = "";
        function contentChangeValidator(isChaged) {
            confirmBeforeLeaving = isChaged;
            if (isChaged && reasonForChangeContent == 'mandatory') {
                ValidatorEnable(document.getElementById('ctl00_HomeContentPlaceHolder_rfvReasonForChange'), true);
            }
            else {
                ValidatorEnable(document.getElementById('ctl00_HomeContentPlaceHolder_rfvReasonForChange'), false);
            }
        }
        /* === end Red === */

        function UpdateShownField(strTargetTRID) {
            var filtered = arrHiddenFieldID.filter(function (targetID) {
                return targetID != strTargetTRID;
            });
            arrHiddenFieldID = filtered.slice(0);
            $("#hfHiddenFieldID").val(arrHiddenFieldID.join(","));
        }

        function UpdateHiddenField(strTargetTRID) {
            arrHiddenFieldID.push(strTargetTRID);
            $("#hfHiddenFieldID").val(arrHiddenFieldID.join(","));
        }

        $(document).ready(function () {
            initElements();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initElements();
        });

        var ajaxCallArray = new Array();

        function initElements() {
            for (var x in ajaxCallArray) {
                if (ajaxCallArray.hasOwnProperty(x))
                    ajaxCallArray.pop().abort();
            }

            var calculations = $("#hfDependentColunmns").val();
            if (calculations) {
                var dict = JSON.parse(calculations);
                $.each(dict, function (key, value) {
                    console.log(key, value);
                    $("[data-column='" + key + "']").change(updateCalculatedValues);
                });
            }
        }

        function updateCalculatedValues() {
            var calculations = $("#hfDependentColunmns").val();
            var dict = JSON.parse(calculations);
            var data = {};
            data.queryString = encodeURIComponent(location.search.substring(1));
            data.columnValues = {};
            data.dependentColumns = dict[$(this).attr("data-column")];
            $.each($("[data-column]"),
                function () {
                    if ($(this).is("span")) {
                        if ($(this).children("input[type='radio']").length > 0)
                            data.columnValues[$(this).attr("data-column")] = $("input[type='radio']:checked", $(this)).val();
                        else if ($(this).children("input[type='checkbox']").length > 0)
                            data.columnValues[$(this).attr("data-column")] = $("input[type='checkbox']", $(this)).prop("checked");
                    }
                    else
                        data.columnValues[$(this).attr("data-column")] = $(this).val();
                });

            var $that = $(this);
            var timer = setTimeout(function () {
                $.each(dict[$that.attr("data-column")],
                    function () {
                        $("[data-column='" + this + "']").addClass("CalculateWaitClass");
                    });
            },
                250);
            ajaxCallArray.push($.ajax({
                type: "GET",
                url: "/Pages/Record/RecalcColumn.ashx",
                cache: false,
                contentType: "application/json",
                data: JSON.stringify(data)
            })
                .done(function (serverData, textStatus, jqXHR) {
                    for (var x in serverData) {
                        if (serverData.hasOwnProperty(x))
                            $("[data-column='" + x + "']").val(serverData[x]);
                    }
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                })
                .always(function () {
                    clearTimeout(timer);
                    $.each(dict[$that.attr("data-column")],
                        function () {
                            $("[data-column='" + this + "']").removeClass("CalculateWaitClass");
                        });
                }));
        }

        /* === Red 26042019: needed this to call the save function, 
         * problem with the image and file followting the next or prev button itself === */
        //function prevNextRecord() {
        //    if (Page_IsValid) {
        //        setTimeout(function () {
        //            //lets click the save button to save record before logging out
        //            //2 seconds to show the label Attempting to save the record...
        //            document.getElementById('ctl00_HomeContentPlaceHolder_lnkSaveClose').click();
        //        }, 500);
        //    }
        //    confirmBeforeLeaving = false;
        //}
        /* === end Red === */
        function SaveClick(DisplayInColumnID) {  //DisplayInColumnID
            //alert(AddControlName);
            //$('#ctl00_HomeContentPlaceHolder_lnkSaveClose').trigger('click');
            var hfDisplayInColumnIDOnAdd = document.getElementById('hfDisplayInColumnIDOnAdd');
            hfDisplayInColumnIDOnAdd.value = DisplayInColumnID;
            var btn = document.getElementById('ctl00_HomeContentPlaceHolder_lnkSaveClose');
            btn.click();
        }

    </script>

    <script type="text/javascript">


        //$(document).ready(function () {
        //    $(window).keydown(function (event) {
        //        if (event.keyCode == 13) {
        //            event.preventDefault();
        //            return false;
        //        }
        //    });
        //});

        //$(document).ready(function () {

        //    //$(".inlineeditingclass").click(function (e) {
        //    $('.inlineeditingclass').dblclick(function (e) {
        //        e.stopPropagation();
        //        var currentEle = $(this);
        //        var value = $(this).html();
        //        var row = $(e.target).closest('tr');
        //        //var DBGRecordID = row.find($("[id*=key]")).val();
        //        var DBGRecordID = $(this).attr('recordid');
        //        var vxxx = $(this).attr('systemname');
        //        UpdateVal(currentEle, value, DBGRecordID, vxxx);
        //    });

        //});

        function UpdateVal(CurrentEle, value, DBGRecordID, ColumnToUpdate) {
            if ($(".cellValue") !== undefined) {
                if ($(".cellValue").val() !== undefined) {
                    $(".cellValue").parent().html($("#OriginalValue").val().trim());
                    $(".cellValue").remove();
                }
                if (value.match("<") == null) {
                    $(CurrentEle).html('<div class="cellValue" id="cellWrapper"> ' +
                        ' <table  cellspacing="0" cellpadding="0" style="border:none"><tr><td  style="border:none"><input class="cellValue" type="text" id="txtValue" value="' + value + '" />' +
                        ' <input class="cellValue" type="hidden" value="' + DBGRecordID + '" id="keySelected" />' +
                        ' <input class="cellValue" type="hidden" value="' + ColumnToUpdate + '" id="ColumnToUpdate" />' +
                        ' <input class="cellValue" type="hidden" value="' + value + '" id="OriginalValue" /></td>' +
                        '<td  style="border:none"><input class="cellValue" type="button" style="background: url(GreenTick.png);width:18px;height:18px;border:none;padding-top:5px;" title="Save" id="btnOneSave" onClick="return SaveChanges()" /></td>' +
                        ' <td  style="border:none"><input class="cellValue" type="button" style="background: url(RedCross.png);width:18px;height:18px;border:none;padding-top:5px;" title="Cancel" id="btnOneCancel" onClick="return CancelChanges()" /></td></tr></table> ' +
                        //' <input class="cellValue" type="hidden" value="' + FieldVxxx + '" id="hfFieldVxxx" />' +  
                        //' <input class="cellValue" type="button" value="Save" id="btnOneSave" onClick="return SaveChanges()" />' +
                        //' <input class="cellValue" type="button" value="Cancel" id="btnOneCancel" onClick="return CancelChanges()" /> ' +
                        ' </div> ');
                }
                $(".cellValue").focus();
                //$(".cellValue").keyup(function (event) {
                //    if (event.keyCode == 13) {
                //        SaveChanges();
                //    }
                //});
                //$("#txtValue").blur(function (e) {
                //    CancelChanges();
                //});
            }
        }

        function CancelChanges(e) {
            if ($(".cellValue") !== undefined) {
                if ($(".cellValue").val() !== undefined) {
                    $(".cellValue").parent().html($("#OriginalValue").val().trim());
                    $(".cellValue").remove();
                }
            }
            //window.location.reload();
        }
        function SaveChanges(e) {
            var onerecord = {};
            onerecord.ColumnToUpdate = $("[id*=ColumnToUpdate]").val();
            onerecord.FieldValue = $("[id*=txtValue]").val();
            onerecord.ID = $("[id*=keySelected]").val();

            $.ajax({
                type: "POST",
                url: "RecordList.aspx/SaveOneCell",
                data: '{onerecord: ' + JSON.stringify(onerecord) + '}',
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //alert("saved!");
                    //location.href = location.href;
                    window.location.reload();
                }
            });
            if ($(".cellValue") !== undefined) {
                if ($(".cellValue").val() !== undefined) {
                    $(".cellValue").parent().html(onerecord.FieldValue);
                    $(".cellValue").remove();
                }
            }


        }       



</script>
    <div style="display:none">
        <a class="popupShowReport" id="showReportPopup">Open report</a>
        <a  id="downloadReport">Download report</a>
    </div>

    

    <div style="min-height: 300px;">
        <asp:UpdatePanel ID="upDetailDynamic" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlFullDetailPage">


                    <div runat="server" id="divHeaderColorDetail" style="">

                        <asp:Button runat="server" ID="btnSaveRecord" ClientIDMode="Static" Style="display: none;" OnClick="btnSaveRecord_Click" CausesValidation="true" />
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" style="width: 500px;">
                                    <span class="TopTitle">
                                        <asp:Label runat="server" ID="lblTitle"></asp:Label></span>
                                    <br />
                                    <br />
                                    <asp:Label runat="server" ID="lblHeaderName" Style="padding-left: 5px;" ClientIDMode="Static"> </asp:Label>
                                    <asp:HiddenField runat="server" ID="hfRecordID" Value="-1" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfPostback" Value="0" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfUserRoleName" Value="None" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfRecordAddEditView" Value="view" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfRecordTableID" Value="-1" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfRecordDetailSCid" Value="-1" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfParentRecordID" Value="" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfHiddenFieldID" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfDependentColunmns" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfLogmeOut" ClientIDMode="Static" Value="false" />
                                     <asp:HiddenField runat="server" ID="hfNexPrevPage" ClientIDMode="Static" Value=""/>
                                    <asp:HiddenField runat="server" ID="hfDisplayInColumnIDOnAdd" ClientIDMode="Static" Value=""/>
                                </td>
                                <td align="right">
                                    <table class="DetailPageControls">
                                        <tr>
                                            <td></td>
                                            <td>
                                                <div>


                                                    <div runat="server" id="trMainSave">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div runat="server" id="divLnkBack" visible="false">
                                                                        <asp:LinkButton runat="server" ID="lnkBack" OnClick="lnkSaveBack_Click" CausesValidation="true">
                                                                            <asp:Image runat="server" ID="lnkImgBack" ImageUrl="~/App_Themes/Default/images/Back.png"
                                                                                ToolTip="Save and Go Back" />
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                    <div runat="server" id="divHlBack" visible="false">
                                                                        <asp:HyperLink runat="server" ID="hlBack" Visible="true">
                                                                            <asp:Image runat="server" ID="imgBack" ImageUrl="~/App_Themes/Default/images/Back.png"
                                                                                ToolTip="Back" />
                                                                        </asp:HyperLink>
                                                                    </div>
                                                                    <%-- RP Added Ticket 4305 Navigation Issue in TDB --%>
                                                                    <div runat="server" id="divBackRoute">
                                                                        <asp:LinkButton runat="server" ID="hlBackRoute" Visible="true" OnClick="hlBackRoute_Click" CausesValidation="false">
                                                                            <asp:Image runat="server" ID="Image1" ImageUrl="~/App_Themes/Default/images/Back.png"
                                                                                ToolTip="Back" />
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                    <%-- End Modification --%>

                                                                      <div runat="server" id="divPrintReport">
                                                                        <asp:LinkButton runat="server" ID="lnkPrintReport" Visible="true" OnClick="lnkSavePrintReportPDF_Click" CausesValidation="true">
                                                                            <asp:Image runat="server" ID="Image4" ImageUrl="~/App_Themes/Default/images/export-to-pdf36.png"
                                                                                ToolTip="View Report PDF" />
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                  
                                                                </td>
                                                                <td>
                                                                      <div runat="server" id="divPrintExcel" style="padding-left:10px;">
                                                                        <asp:LinkButton runat="server" ID="lnkPrintReportExcel" Visible="true" OnClick="lnkSavePrintReportExcel_Click" CausesValidation="true">
                                                                            <asp:Image runat="server" ID="Image5" ImageUrl="~/App_Themes/Default/images/export-to-excel36.png"
                                                                                ToolTip="Download Report Excel" />
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                    <div runat="server" clientidmode="Static" id="divGraph" visible="false" style="padding-left: 15px">
                                                                        <asp:HyperLink ClientIDMode="Static" runat="server" ID="hlGraph">
                                                                            <asp:Image runat="server" ID="imgGraph" ImageUrl="~/App_Themes/Default/images/Graph.png"
                                                                                ToolTip="Show Graph" />
                                                                        </asp:HyperLink>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div id="divMainSaveEditAddetc">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <div runat="server" id="divEdit" visible="false">
                                                                                        <asp:HyperLink runat="server" ID="hlEditLink">
                                                                                            <asp:Image runat="server" ID="ImageEdit" ImageUrl="~/App_Themes/Default/images/Edit_big.png"
                                                                                                ToolTip="Edit" />
                                                                                        </asp:HyperLink>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <div runat="server" id="divSaveClose">
                                                                                        <%--  <asp:UpdatePanel ID="upkSaveClose" runat="server" UpdateMode="Always">
                                                                            <ContentTemplate>--%>
                                                                                        <table cellpadding="0" cellspacing="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:LinkButton runat="server" ID="lnkSaveClose" OnClientClick="confirmBeforeLeaving = false;" OnClick="lnkSaveClose_Click" CausesValidation="true">
                                                                                                        <asp:Image runat="server" ID="imgSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                                                                            ToolTip="Save" />
                                                                                                    </asp:LinkButton>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:LinkButton runat="server" ID="lnkSaveAndAdd" OnClientClick="confirmBeforeLeaving = false;" OnClick="lnkSaveAndAdd_Click" CausesValidation="true"
                                                                                                        Visible="false">
                                                                                                        <asp:Image runat="server" ID="Image11" ImageUrl="~/App_Themes/Default/images/SaveAndAdd2.png"
                                                                                                            ToolTip="Save and Add" />
                                                                                                    </asp:LinkButton>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <%--  </ContentTemplate>
                                                                           
                                                                        </asp:UpdatePanel>--%>
                                                                                    </div>
                                                                                    <%--<asp:Button Style="display: none;" runat="server" ID="lnkHiddenSave" ClientIDMode="Static"
                                                                                OnClick="tabDetail_ActiveTabChanged" CausesValidation="true"></asp:Button>--%>
                                                                                </td>
                                                                                <td>
                                                                                    <div runat="server" id="divSaveAndStay" visible="false">
                                                                                        <%--<asp:UpdatePanel ID="upSaveAndStay" runat="server" UpdateMode="Always">
                                                                            <ContentTemplate>--%>
                                                                                        <asp:LinkButton runat="server" ID="lnkSaveAndStay" OnClientClick="confirmBeforeLeaving = false;" OnClick="lnkSaveAndStay_Click"
                                                                                            CausesValidation="true">
                                                                                            <asp:Image runat="server" ID="Image6" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                                                                ToolTip="Save and Stay" />
                                                                                        </asp:LinkButton>
                                                                                        <%--  </ContentTemplate>
                                                                        </asp:UpdatePanel>--%>
                                                                                    </div>
                                                                                </td>
                                                                                <%-- <td>
                                                                                <div runat="server" id="divSaveAndAdd" visible="false">
                                                                        <asp:HyperLink runat="server" ID="hlSaveAndAdd">
                                                                            <asp:Image runat="server" ID="Image2" ImageUrl="~/App_Themes/Default/images/BigAdd.png"
                                                                                ToolTip="Add" />
                                                                        </asp:HyperLink>
                                                                    </div>
                                                                            </td>--%>
                                                                                <td>
                                                                                    <div runat="server" id="divWordExport" visible="false">
                                                                                        <asp:HyperLink runat="server" ID="lnkWordWxport" CssClass="popuplinkWE">
                                                                                            <asp:Image runat="server" ID="Image3" ImageUrl="~/App_Themes/Default/images/WordExport.png"
                                                                                                ToolTip="Document Generation" />
                                                                                        </asp:HyperLink>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <div runat="server" id="divPrint" visible="false">
                                                                                        <asp:HyperLink runat="server" ID="hlPrint" Target="_blank">
                                                                                            <asp:Image runat="server" ID="Image2" ImageUrl="~/App_Themes/Default/images/print-icon.png"
                                                                                                ToolTip="Print" />
                                                                                        </asp:HyperLink>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                            <td valign="middle" align="left">
                                                <table runat="server" id="tblNavigateRecords" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <asp:HyperLink runat="server" ID="hlNavigatePrev" Visible="false">
                                                                <asp:Image ID="Image7" runat="server" ImageUrl="~/App_Themes/Default/Images/bullet_arrow_left.png"
                                                                    ToolTip="Previous Record" />
                                                            </asp:HyperLink>
                                                            <asp:LinkButton runat="server" ID="lnkNavigatePrev" OnClientClick="confirmBeforeLeaving = false;"  OnClick="lnkNavigatePrev_Click" >
                                                                <asp:Image ID="Image9" runat="server" ImageUrl="~/App_Themes/Default/Images/bullet_arrow_left.png"
                                                                    ToolTip="Previous Record" />
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td>
                                                            <asp:HyperLink runat="server" ID="hlNavigateNext" Visible="false">
                                                                <asp:Image ID="Image8" runat="server" ImageUrl="~/App_Themes/Default/Images/bullet_arrow_right.png"
                                                                    ToolTip="Next Record" />
                                                            </asp:HyperLink>
                                                            <asp:LinkButton runat="server" ID="lnkNavigateNext" OnClientClick="confirmBeforeLeaving=false;" OnClick="lnkNavigateNext_Click">
                                                                <asp:Image ID="Image10" runat="server" ImageUrl="~/App_Themes/Default/Images/bullet_arrow_right.png"
                                                                    ToolTip="Next Record" />
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-left: 20px;" runat="server" id="tdValidationResult">
                                    <%-- <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Always">
                        <ContentTemplate>--%>
                                    <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                                    <%--/ContentTemplate>
                    </asp:UpdatePanel>--%>

                                    <div runat="server" id="divValidWarningGrid" visible="false" style="border: solid 1px #909090;">


                                        <table>
                                            <tr>
                                                <td>
                                                    <strong style="font-size: 14px;">Validation Results</strong><br />


                                                    <%--//Red Ticket 2099 - Removed this--%>
                                                    <%--<Red Begin>--%>
                                                    <%--<!-- JA - 9 DEC 2016 -->
                                            <asp:CheckBox ID="chkValidation" runat="server" Visible="True"  />
                                            <asp:Label ID="lblValidationWarningMsg" runat="server" Font-Size="Smaller" ForeColor="#3333FF"></asp:Label>--%>
                                                    <%--<Red End>--%>

                                                    <div style="padding-left: 10px;">

                                                        <asp:GridView ID="gvValidWarningGrid" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                                            HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" CssClass="gridview"
                                                            OnRowDataBound="gvValidWarningGrid_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField Visible="false">
                                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblColumnID" runat="server" Text='<%# Eval("ColumnID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField Visible="false">
                                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblValidationType" runat="server" Text='<%# Eval("ValidationType") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemStyle Width="20px" HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox runat="server" ID="chkIgnore" Checked='<%#  Eval("Ignore")=="yes"?true:false %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField>
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblFullMsg" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField Visible="false">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblValidWarningMsg" runat="server" Text='<%# Eval("ValidWarningMsg") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField Visible="false">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblValidWarningFormula" runat="server" Text='<%# Eval("ValidWarningFormula") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField Visible="false">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblValue" runat="server" Text='<%# Eval("Value") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>

                                                        </asp:GridView>

                                                        <span runat="server" id="spnIgnoreTick" style="font-size: 10px; padding-top: 5px;">To ignore tick the checkbox and press Save again</span>
                                                    </div>
                                                </td>
                                                <td valign="top">
                                                    <%--<asp:LinkButton runat="server" ID="lnkValidWarningRefresh" CssClass="btn" CausesValidation="false"
                                        OnClick="lnkValidWarningRefresh_Click"> <strong>Refresh</strong> </asp:LinkButton>--%>

                                                </td>
                                            </tr>

                                        </table>



                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <asp:Panel runat="server" ID="pnlDetailWhole">
                        <div runat="server" id="div1" onkeypress="abc();">

                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td></td>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="width: 10px;"></td>
                                                <td>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">

                                                        <tr>
                                                            <td valign="top"></td>
                                                            <td valign="top">
                                                                <%-- <asp:UpdatePanel ID="upDetailDynamic" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>--%>
                                                                <div id="search" style="padding-bottom: 10px">
                                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                                                                        ShowMessageBox="true" ShowSummary="false" HeaderText="Please correct the following errors:" />

                                                                </div>
                                                                <asp:Panel ID="Panel2" runat="server">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <div runat="server" id="divDynamic" class="DBGTab ajax__tab_container ajax__tab_default">

                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tr>
                                                                                            <td></td>
                                                                                            <td>
                                                                                                <asp:Panel runat="server" ID="pnlHeadingHor" ClientIDMode="Static" Style="display: inline-block;">
                                                                                                </asp:Panel>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="vertical-align: top;">
                                                                                                <asp:Panel runat="server" ID="pnlHeadingVer" ClientIDMode="Static" Style="vertical-align: top;">
                                                                                                </asp:Panel>

                                                                                            </td>
                                                                                            <td style="vertical-align: top;">
                                                                                                <asp:Panel runat="server" ID="pnlAllTables" CssClass="ajax__tab_body">
                                                                                                    <asp:Panel runat="server" ID="pnlDetail" ClientIDMode="Static" CssClass="ajax__tab_panel">
                                                                                                        <table runat="server" id="tblPanelDetail">
                                                                                                            <tr>
                                                                                                                <td valign="top">
                                                                                                                    <asp:Panel runat="server" ID="pnlEachTable">
                                                                                                                        <asp:Panel runat="server" ID="pnlTabHeading">
                                                                                                                        </asp:Panel>
                                                                                                                        <asp:Panel runat="server" ID="pnlDetailTab" CssClass="eachtabletab" ClientIDMode="Static">
                                                                                                                            <table runat="server" id="tblMain">
                                                                                                                                <tr>
                                                                                                                                    <td valign="top">
                                                                                                                                        <table id="tblLeft" runat="server" visible="true" cellpadding="3">
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                    <td valign="top">
                                                                                                                                        <table id="tblRight" runat="server" visible="true" cellpadding="3" style="margin-left: 10px;">
                                                                                                                                        </table>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                                <tr>
                                                                                                                                    <td colspan="2" align="right">
                                                                                                                                        <div runat="server" id="divSaveBottom" visible="false">
                                                                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                                                                <tr>
                                                                                                                                                    <td></td>
                                                                                                                                                    <td valign="middle">
                                                                                                                                                        <div runat="server" id="divSaveBottonSave" style="padding: 5px 15px 5px 15px;">
                                                                                                                                                            <asp:LinkButton runat="server" ID="lnkSaveRRP" OnClientClick="confirmBeforeLeaving = false;" OnClick="lnkSaveClose_Click" CausesValidation="true">
                                                                                                                                                                <table>
                                                                                                                                                                    <tr>
                                                                                                                                                                        <td>
                                                                                                                                                                            <asp:Image runat="server" ID="imgSaveRRP" ImageUrl="~/Pages/Pager/Images/rrp/save.png"
                                                                                                                                                                                ToolTip="Save" />
                                                                                                                                                                            <%--<img title="Save" src="../../Pages/Pager/Images/rrp/save.png" alt="Save" />--%>
                                                                                                                                                                        </td>
                                                                                                                                                                        <td valign="middle">
                                                                                                                                                                            <strong style="color: #ffffff;">SAVE</strong>
                                                                                                                                                                        </td>
                                                                                                                                                                    </tr>
                                                                                                                                                                </table>
                                                                                                                                                            </asp:LinkButton>
                                                                                                                                                        </div>
                                                                                                                                                    </td>
                                                                                                                                                </tr>
                                                                                                                                            </table>
                                                                                                                                        </div>
                                                                                                                                    </td>
                                                                                                                                </tr>
                                                                                                                            </table>

                                                                                                                            <%--<table>
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    XXXFILES501
                                                                                                                                    <asp:Image ID="ImageDetail" runat="server" />
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>--%>
                                                                                                                        </asp:Panel>
                                                                                                                    </asp:Panel>
                                                                                                                </td>

                                                                                                            </tr>
                                                                                                            <%-- RP Modified Ticket 4285 --%>
                                                                                                            <tr>
                                                                                                                <td style="padding-left: 15px; padding-right: 15px;">
                                                                                                                    <table runat="server" id="trReasonForChange" visible="false" style="width: 100%">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <strong runat="server" id="stgReasonForChange">Reason for change</strong>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:TextBox runat="server" ID="txtReasonForChange" Width="100%" CssClass="NormalTextBox"
                                                                                                                                    TextMode="MultiLine" Height="50px"></asp:TextBox>
                                                                                                                                <asp:RequiredFieldValidator runat="server" ID="rfvReasonForChange" ControlToValidate="txtReasonForChange"
                                                                                                                                    ErrorMessage="Reason for change is Required" Display="None" Enabled="false"></asp:RequiredFieldValidator>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <%-- End Modification --%>
                                                                                                        </table>
                                                                                                        <%--RP Removed Ticket 4285 --%>
                                                                                                        <%-- 
                                                                                                    <div>
                                                                                                        <table>
                                                                                                            <tr>
                                                                                                                <td style="width: 50px;"></td>
                                                                                                                <td colspan="2"></td>
                                                                                                                <td style="width: 30px;"></td>
                                                                                                                <td>
                                                                                                                    <table runat="server" id="trReasonForChange" visible="false">
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <strong runat="server" id="stgReasonForChange">Reason for change</strong>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td>
                                                                                                                                <asp:TextBox runat="server" ID="txtReasonForChange" Width="300px" CssClass="NormalTextBox"
                                                                                                                                    TextMode="MultiLine" Height="50px"></asp:TextBox>
                                                                                                                                <asp:RequiredFieldValidator runat="server" ID="rfvReasonForChange" ControlToValidate="txtReasonForChange"
                                                                                                                                    ErrorMessage="Reason for change is Required" Display="None" Enabled="false"></asp:RequiredFieldValidator>
                                                                                                                            </td>
                                                                                                                        </tr>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                        --%>
                                                                                                        <br />
                                                                                                    </asp:Panel>

                                                                                                </asp:Panel>




                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <%--<asp:Panel runat="server" ID="pnlHeading" ClientIDMode="Static">
                                                                                    <asp:Label runat="server" Font-Bold="true" ID="lblFristTabTableName" Text="Detail"></asp:Label>
                                                                                    <asp:LinkButton runat="server" ID="lnkHeading" ClientIDMode="Static" OnClientClick=""></asp:LinkButton>
                                                                                </asp:Panel>--%>
                                                                                </div>
                                                                                <%--//oliver <begin> Ticket 1476--%>
                                                                                <div id="divBottom" runat="server" style="margin: 10px;">
                                                                                    <div style="float: left; margin-left: -15px;" id="divHistory">
                                                                                        <asp:HyperLink runat="server" ID="lnkShowHistory" ClientIDMode="Static" Text="Show Change History" CssClass="popuplinkCH"
                                                                                            CausesValidation="false"></asp:HyperLink>

                                                                                    </div>
                                                                                    <div style="float: right; color: #C0C0C0;">
                                                                                        Record ID:
                                                                                <asp:Label ID="lblUIRecordID" runat="server" Text=""></asp:Label>
                                                                                    </div>
                                                                                    <div style="float: left;"></div>
                                                                                </div>
                                                                                <%--//oliver <end>--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                                <%--  </ContentTemplate>
                                                 
                                                    
                                                </asp:UpdatePanel>--%>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>

                    <br />
                    <%-- OnClick="btnReloadMe_Click"--%>
                       <asp:Label runat="server" ID="lblModalButtonResult" />
                <ajaxToolkit:ModalPopupExtender ID="mpeButtonResult" runat="server" TargetControlID="lblModalButtonResult"
                    PopupControlID="pnlButtonResult" BackgroundCssClass="modalBackground"  CancelControlID="lnkButtonResultCancel"  />



                    <asp:Panel ID="pnlButtonResult" runat="server"  Style="display: none">
                        <div style="border-width: 5px; background-color: #ffffff; border-color: #4F8FDD; border-style: outset; min-height:100px; min-width:200px;">
                            <table style="width:95%">
                                <tr>
                                    <td style="text-align:left;">
                                        
                                    </td>
                                    <td style="text-align:center; padding-left:10px;">
                                        <asp:LinkButton runat="server" ID="lnkButtonResultCancel" CssClass="btnx"
                                CausesValidation="false" > <strong>Close</strong></asp:LinkButton> <%--OnClick="lnkButtonResultCancel_Click"--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="lblButtonResult"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:GridView ID="gvButtonResult" runat="server" ShowHeaderWhenEmpty="true"
                                            AutoGenerateColumns="true" CssClass="gridview" AllowPaging="false"
                                            HeaderStyle-CssClass="gridview_header" RowStyle-CssClass="gridview_row">
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                            
                          
                            
                        </div>
                    </asp:Panel>




                    <asp:Button ID="btnReloadMe" runat="server" ClientIDMode="Static" OnClientClick="ReloadMe();return false;"
                        Style="display: none;" />
                    <asp:Literal ID="ltTextJS" runat="server"></asp:Literal>
                </asp:Panel>


             
              

            </ContentTemplate>
          <%--  <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gvButtonResult" />
            </Triggers>--%>
        </asp:UpdatePanel>
    </div>
  


</asp:Content>

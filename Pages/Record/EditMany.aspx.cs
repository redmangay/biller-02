﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Globalization;
using System.Data;
public partial class Pages_Record_EditMany :SecurePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            txtTextBulk.Attributes.Add("oninput", "hideErrorMessage();");
            txtNumberBulk.Attributes.Add("oninput", "hideErrorMessage();");
            txtDateBulk.Attributes.Add("oninput", "hideErrorMessage();");
            txtBulkTime.Attributes.Add("oninput", "hideErrorMessage();");
            ddlDropdownBulk.Attributes.Add("onchange", "hideErrorMessage();");
            chkCheckboxBulk.Attributes.Add("onchange", "hideErrorMessage();");

            string strParentObject = "";
            if (Request.QueryString["sc_id"] != null)
            {
                SearchCriteria theSC = SystemData.SearchCriteria_Detail(int.Parse(Request.QueryString["sc_id"].ToString()));
                if (theSC != null)
                {
                    System.Xml.XmlDocument xmlSC_Doc = new System.Xml.XmlDocument();

                    xmlSC_Doc.Load(new StringReader(theSC.SearchText));

                    strParentObject = strParentObject + " var p_lnkEditManyOK = window.parent.document.getElementById('" + xmlSC_Doc.FirstChild["lnkEditManyOK"].InnerText + @"');";
                    strParentObject = strParentObject + " var p_chkAll = window.parent.document.getElementById('" + xmlSC_Doc.FirstChild["chkAll"].InnerText + @"');";
                    ViewState["TableID"] = xmlSC_Doc.FirstChild["TableID"].InnerText;
                    ViewState["ViewID"] = xmlSC_Doc.FirstChild["ViewID"].InnerText;
                    ViewState["TableName"] = xmlSC_Doc.FirstChild["TableName"].InnerText;
                    //lblTitle.Text = ViewState["TableName"].ToString() + " - Update Multiple";
                    ViewState["_strRecordRightID"] = xmlSC_Doc.FirstChild["RecordRightID"].InnerText;
                    ViewState["_strDynamictabPart"] = xmlSC_Doc.FirstChild["DynamictabPart"].InnerText;
                    ViewState["ItemList"] = xmlSC_Doc.FirstChild["ItemList"].InnerText;

                    strParentObject = strParentObject + " var p_hfddlYAxisBulk = window.parent.document.getElementById('" + xmlSC_Doc.FirstChild["hfddlYAxisBulk"].InnerText + @"');";
                    strParentObject = strParentObject + " var p_hfBulkValue = window.parent.document.getElementById('" + xmlSC_Doc.FirstChild["hfBulkValue"].InnerText + @"');";
                    strParentObject = strParentObject + " var p_hfchkUpdateEveryItem = window.parent.document.getElementById('" + xmlSC_Doc.FirstChild["hfchkUpdateEveryItem"].InnerText + @"');";

                    PopulateYAxisBulk(int.Parse(ViewState["TableID"].ToString()), int.Parse(ViewState["ViewID"].ToString()));
                }
            }
            PopulateTerminology();

            ViewState["strParentObject"] = strParentObject;







            string strXX = @"
                       

                         $(document).ready(function () 
                            {
                                " + strParentObject + @"
                                    function EditManyCheckAll()
                                    {
                                       if(p_chkAll!=null )
                                        {
                                            if(p_chkAll.checked)
                                                {
                                                    $('#" + trUpdateEveryItem.ClientID + @"').fadeIn();
                                                     $('#" + hfChkAll.ClientID + @"').value='yes';
                                                }
                                            else
                                                {
                                                    $('#" + trUpdateEveryItem.ClientID + @"').fadeOut();
                                                }
                                        }

                                        

                                    }
                                   EditManyCheckAll();
                                
                                    
                           }); 
                ";


            ViewState["strXX"] = strXX;
        }


        ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "emJSCode" + ViewState["_strDynamictabPart"].ToString(), ViewState["strXX"].ToString(), true);

    }

    private void HideLabels()
    {
        lblInvalid.Visible = false;
        lblWarning.Visible = false;
        chkIgnoreWarnings.Visible = false;
    }

    protected void lnkEditManyOK_Click(object sender, EventArgs e)
    {
        HideLabels();

        hfEditManyValue.Value = "";
        string strValue = "";
        if (ddlYAxisBulk.SelectedValue == "")
        {
            Session["tdbmsgpb"] = "Please select a column.";

            return;
        }
        else
        {
            Column theColumn = RecordManager.ets_Column_Details(int.Parse(ddlYAxisBulk.SelectedValue));

            if (theColumn.ColumnType == "checkbox")
            {
                strValue = Common.GetCheckBoxValue(theColumn.DropdownValues, ref chkCheckboxBulk);
            }
            else if (theColumn.ColumnType == "number")
            {
                strValue = txtNumberBulk.Text;
            }
            else if (theColumn.ColumnType == "text")
            {
                strValue = txtTextBulk.Text;
            }
            else if (theColumn.ColumnType == "date")
            {
                strValue = txtDateBulk.Text;
            }
            else if (theColumn.ColumnType == "datetime")
            {
                try
                {
                    string strDateTime = "";
                    if (txtDateBulk.Text.Trim() == "")
                    {
                        //strDateTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
                    }
                    else
                    {
                        DateTime dtTemp;
                        if (DateTime.TryParseExact(txtDateBulk.Text.Trim(), Common.Dateformats, new CultureInfo("en-GB"), DateTimeStyles.None, out dtTemp))
                        {
                            txtDateBulk.Text = dtTemp.ToShortDateString();
                        }
                        string strTimePart = "";
                        if (txtBulkTime != null)
                        {
                            if (txtBulkTime.Text == "")
                            {
                                strTimePart = "00:00";
                            }
                            else
                            {
                                if (txtBulkTime.Text.ToLower().IndexOf(":am") > 0)
                                {
                                    strTimePart = txtBulkTime.Text.ToLower().Replace(":am", ":00");
                                }
                                else
                                {
                                    strTimePart = txtBulkTime.Text.ToLower().Replace(":pm", ":00");
                                }
                            }
                        }
                        else
                        {
                            strTimePart = "00:00";
                        }

                        strDateTime = txtDateBulk.Text + " " + strTimePart;
                        strDateTime = strDateTime.Replace("  ", " ");
                    }
                    strValue = strDateTime;
                }
                catch
                {
                    //
                    strValue = "";
                }
            }
            else if (theColumn.ColumnType == "dropdown")
            {
                strValue = ddlDropdownBulk.SelectedValue;
            }

            if (strValue == "")
            {
                Session["tdbmsgpb"] = "Please enter the new value.";
                //mpeEditMany.Show();
                return;
            }


            int invalidCount = 0;
            int exceedanceCount = 0;
            int warningCount = 0;
            List<int> itemIdList = new List<int>();
            if (ViewState["ItemList"] != null)
            {
                string s = ViewState["ItemList"].ToString();
                if (s.EndsWith(",-1"))
                    s = s.Remove(s.Length - 3, 3);
                string[] strIds = s.Split(',');
                foreach (string strId in strIds)
                {
                    int id = 0;
                    if (int.TryParse(strId, out id))
                        itemIdList.Add(id);
                }
            }

            if (theColumn.ColumnType == "number")
            {
                bool bHasValidation = false;
                bool bHasAdvancedValidation = false;

                //string strHasCondition = Common.GetValueFromSQL(
                //    "SELECT TOP 1 ConditionID FROM Condition WHERE ConditionType='V' AND ColumnID=" +
                //    theColumn.ColumnID.ToString());
                string strHasAdvancedCondition = Common.GetValueFromSQL(
                    "SELECT TOP 1 AdvancedConditionID FROM AdvancedCondition WHERE ConditionType = 'notification' AND ConditionSubType = 'V' AND ColumnID = " +
                    theColumn.ColumnID.ToString());
                if (/*strHasCondition != "" || */strHasAdvancedCondition != "")
                {
                    bHasValidation = true;
                    bHasAdvancedValidation = true;
                }
                if (!bHasValidation)
                {
                    if (theColumn.ValidationOnEntry != "" || theColumn.ValidationOnExceedance != "" ||
                        theColumn.ValidationOnWarning != "")
                    {
                        bHasValidation = true;
                    }
                }

                if (bHasValidation && itemIdList.Count > 0)
                {
                    string strTemp = String.Empty;

                    if (bHasAdvancedValidation && theColumn.ColumnID.HasValue)
                    {
                        string columnSystemName = theColumn.SystemName;
                        DataTable dt =
                            UploadWorld.ets_AdvancedCondition_Select((int) theColumn.ColumnID.Value, "notification",
                                "V");
                        foreach (int recordId in itemIdList)
                        {
                            Record vRecord = RecordManager.ets_Record_Detail_Full(recordId, null, false);
                            RecordManager.MakeTheRecord(ref vRecord, columnSystemName, strValue);
                            if (!UploadManager.IsDataValidAdvanced((int) theColumn.ColumnID.Value, vRecord, dt,
                                ref strTemp))
                            {
                                invalidCount++;
                            }
                        }
                    }
                    else
                    {
                        //string strCheckColumnId = Common.GetValueFromSQL(
                        //    "SELECT TOP 1 CheckColumnID FROM [Condition] WHERE ConditionType='V' AND ColumnID=" +
                        //    theColumn.ColumnID.ToString());
                        bool bEmpty = theColumn.ValidationOnEntry == "";
                        //&& strCheckColumnId == "";

                        if (theColumn.ValidationOnEntry != "" || bEmpty == true)
                        {
                            foreach (int recordId in itemIdList)
                            {
                                if (!UploadManager.IsDataValid(strValue, theColumn.ValidationOnEntry, ref strTemp,
                                    theColumn.ColumnType))
                                {
                                    invalidCount++;
                                }
                            }
                        }
                        //else
                        //{
                        //    if (strCheckColumnId != "")
                        //    {
                        //        Column theCheckColumn = RecordManager.ets_Column_Details(int.Parse(strCheckColumnId));
                        //        if (theCheckColumn != null)
                        //        {
                        //            foreach (int recordId in itemIdList)
                        //            {
                        //                string strEachFormula = "";

                        //                Record vRecord = RecordManager.ets_Record_Detail_Full(recordId, null, false);
                        //                string strCheckValue =
                        //                    RecordManager.GetRecordValue(ref vRecord, theCheckColumn.SystemName);
                        //                strEachFormula = UploadWorld.Condition_GetFormula((int) theColumn.ColumnID,
                        //                    theCheckColumn.ColumnID, "V", strCheckValue);

                        //                if (!UploadManager.IsDataValid(strValue, strEachFormula, ref strTemp,
                        //                    theColumn.ColumnType))
                        //                {
                        //                    invalidCount++;
                        //                }
                        //            }
                        //        }
                        //    }
                        //}
                    }
                }
            }

            if (invalidCount == 0)
            {


                if (theColumn.ColumnType == "number")
                {
                    string strTemp = String.Empty;

                    string strHasAdvancedCondition = Common.GetValueFromSQL(
                        "SELECT TOP 1 AdvancedConditionID FROM AdvancedCondition WHERE ConditionType = 'notification' AND ConditionSubType = 'E' AND ColumnID = " +
                        theColumn.ColumnID.ToString());
                    if (!String.IsNullOrEmpty(strHasAdvancedCondition))
                    {
                        string columnSystemName = theColumn.SystemName;
                        DataTable dt =
                            UploadWorld.ets_AdvancedCondition_Select((int)theColumn.ColumnID.Value, "notification",
                                "E");
                        foreach (int recordId in itemIdList)
                        {
                            Record vRecord = RecordManager.ets_Record_Detail_Full(recordId, null, false);
                            RecordManager.MakeTheRecord(ref vRecord, columnSystemName, strValue);
                            if (!UploadManager.IsDataValidAdvanced((int)theColumn.ColumnID.Value, vRecord, dt,
                                ref strTemp))
                            {
                                exceedanceCount++;
                            }
                        }
                    }
                    else
                    {
                        //string strCheckColumnId = Common.GetValueFromSQL(
                        //    "SELECT TOP 1 CheckColumnID FROM [Condition] WHERE ConditionType='W' AND ColumnID=" +
                        //    theColumn.ColumnID.ToString());
                        bool bEmpty = theColumn.ValidationOnEntry == "";
                        //&& strCheckColumnId == "";

                        if (theColumn.ValidationOnExceedance != "" || bEmpty == true)
                        {
                            foreach (int recordId in itemIdList)
                            {
                                if (!UploadManager.IsDataValid(strValue, theColumn.ValidationOnExceedance, ref strTemp,
                                    theColumn.ColumnType))
                                {
                                    exceedanceCount++;
                                }
                            }
                        }                        
                    }
                }


                if (exceedanceCount==0)
                {
                    if (theColumn.ColumnType == "number")
                    {
                        string strTemp = String.Empty;

                        string strHasAdvancedCondition = Common.GetValueFromSQL(
                            "SELECT TOP 1 AdvancedConditionID FROM AdvancedCondition WHERE ConditionType = 'notification' AND ConditionSubType = 'W' AND ColumnID = " +
                            theColumn.ColumnID.ToString());
                        if (!String.IsNullOrEmpty(strHasAdvancedCondition))
                        {
                            string columnSystemName = theColumn.SystemName;
                            DataTable dt =
                                UploadWorld.ets_AdvancedCondition_Select((int)theColumn.ColumnID.Value, "notification",
                                    "W");
                            foreach (int recordId in itemIdList)
                            {
                                Record vRecord = RecordManager.ets_Record_Detail_Full(recordId, null, false);
                                RecordManager.MakeTheRecord(ref vRecord, columnSystemName, strValue);
                                if (!UploadManager.IsDataValidAdvanced((int)theColumn.ColumnID.Value, vRecord, dt,
                                    ref strTemp))
                                {
                                    warningCount++;
                                }
                            }
                        }
                        else
                        {
                            //string strCheckColumnId = Common.GetValueFromSQL(
                            //    "SELECT TOP 1 CheckColumnID FROM [Condition] WHERE ConditionType='W' AND ColumnID=" +
                            //    theColumn.ColumnID.ToString());
                            bool bEmpty = theColumn.ValidationOnEntry == "";
                            //&& strCheckColumnId == "";

                            if (theColumn.ValidationOnWarning != "" || bEmpty == true)
                            {
                                foreach (int recordId in itemIdList)
                                {
                                    if (!UploadManager.IsDataValid(strValue, theColumn.ValidationOnWarning, ref strTemp,
                                        theColumn.ColumnType))
                                    {
                                        warningCount++;
                                    }
                                }
                            }
                            //else
                            //{
                            //    if (strCheckColumnId != "")
                            //    {
                            //        Column theCheckColumn = RecordManager.ets_Column_Details(int.Parse(strCheckColumnId));
                            //        if (theCheckColumn != null)
                            //        {
                            //            foreach (int recordId in itemIdList)
                            //            {
                            //                string strEachFormula = "";

                            //                Record vRecord = RecordManager.ets_Record_Detail_Full(recordId, null, false);
                            //                string strCheckValue =
                            //                    RecordManager.GetRecordValue(ref vRecord, theCheckColumn.SystemName);
                            //                strEachFormula = UploadWorld.Condition_GetFormula((int) theColumn.ColumnID,
                            //                    theCheckColumn.ColumnID, "W", strCheckValue);

                            //                if (!UploadManager.IsDataValid(strValue, strEachFormula, ref strTemp,
                            //                    theColumn.ColumnType))
                            //                {
                            //                    warningCount++;
                            //                }
                            //            }
                            //        }
                            //    }
                            //}
                        }
                    }
                }

                

                if ((warningCount == 0 && exceedanceCount==0)|| chkIgnoreWarnings.Checked)
                {
                    hfEditManyValue.Value = strValue;

                    string strJS = @"

                         $(document).ready(function () {

                                " + ViewState["strParentObject"].ToString() + @"

                                p_hfddlYAxisBulk.value=document.getElementById('" + ddlYAxisBulk.ClientID + @"').value;
                                    p_hfBulkValue.value=document.getElementById('" + hfEditManyValue.ClientID +
                                   @"').value;
                                p_hfchkUpdateEveryItem.value=document.getElementById('" + chkUpdateEveryItem.ClientID +
                                   @"').checked.toString();
                                $(p_lnkEditManyOK).trigger('click');parent.$.fancybox.close();
                           });
                    ";
                    if (IsPostBack)
                        ScriptManager.RegisterStartupScript(upMain, upMain.GetType(),
                            "doneEditMany" + ViewState["_strDynamictabPart"].ToString(), strJS, true);
                }
                else
                {
                    if (exceedanceCount>0)
                    {
                        lblWarning.Text = String.Format("The entered value causes exceedance  for {0} record(s).",
                                          exceedanceCount) +
                                      "<br />If you want to proceed please tick the checkbox below<br />and press \"Update\" button again.<br />";
                        lblWarning.Visible = true;
                        chkIgnoreWarnings.Visible = true;
                        lblWarning.ForeColor = System.Drawing.Color.Orange;
                        chkIgnoreWarnings.ForeColor = System.Drawing.Color.Orange;
                    }
                    else
                    {
                        lblWarning.Text = String.Format("The entered value causes warning for {0} record(s).",
                                          warningCount) +
                                      "<br />If you want to proceed please tick the checkbox below<br />and press \"Update\" button again.<br />";
                        lblWarning.Visible = true;
                        chkIgnoreWarnings.Visible = true;
                        lblWarning.ForeColor = System.Drawing.Color.Blue;
                        chkIgnoreWarnings.ForeColor = System.Drawing.Color.Blue;
                    }
                    
                }
            }
            else
            {
                lblInvalid.Text = String.Format("The entered value is invalid for {0} record(s).", invalidCount) +
                                  "<br />Update is not allowed.";
                lblInvalid.Visible = true;
            }
        }
    }
    protected void PopulateTerminology()
    {

        stgFieldToUpdate.InnerText = stgFieldToUpdate.InnerText.Replace("Field", SecurityManager.etsTerminology(Request.Path.Substring(Request.Path.LastIndexOf("/") + 1), "Field", "Field"));


    }
    protected void ddlYAxisBulk_SelectedIndexChanged(object sender, EventArgs e)
    {
        HideLabels();

        txtNumberBulk.Visible = false;
        txtNumberBulk.Text = "";
        txtTextBulk.Visible = false;
        txtTextBulk.Text = "";
        txtDateBulk.Visible = false;
        txtDateBulk.Text = "";
        ibBulkDate.Visible = false;
        txtBulkTime.Visible = false;
        txtBulkTime.Text = "";
        ddlDropdownBulk.Visible = false;
        chkCheckboxBulk.Visible = false;
        chkCheckboxBulk.Checked = false;

        if (ddlDropdownBulk.Items.Count > 0)
            ddlDropdownBulk.SelectedIndex = 0;

        if (ddlYAxisBulk.SelectedValue == "")
        {
            //
        }
        else
        {
            Column theColumn = RecordManager.ets_Column_Details(int.Parse(ddlYAxisBulk.SelectedValue));

            if (theColumn.ColumnType == "checkbox")
            {
                chkCheckboxBulk.Visible = true;
            }
            else if (theColumn.ColumnType == "number")
            {
                txtNumberBulk.Visible = true;

            }
            else if (theColumn.ColumnType == "text")
            {
                txtTextBulk.Visible = true;

            }
            else if (theColumn.ColumnType == "date")
            {
                txtDateBulk.Visible = true;
                ibBulkDate.Visible = true;

            }
            else if (theColumn.ColumnType == "datetime")
            {
                txtDateBulk.Visible = true;
                ibBulkDate.Visible = true;
                txtBulkTime.Visible = true;

            }
            else if (theColumn.ColumnType == "dropdown")
            {

                ddlDropdownBulk.Visible = true;

                if (theColumn.DropDownType == "values")
                {
                    Common.PutDDLValues(theColumn.DropdownValues, ref ddlDropdownBulk);
                }
                else if (theColumn.DropDownType == "value_text")
                {
                    Common.PutDDLValue_Text(theColumn.DropdownValues, ref ddlDropdownBulk);
                }
                else if ((theColumn.DropDownType == "table" || theColumn.DropDownType == "tabledd")
                    && theColumn.ParentColumnID == null)
                {
                    ddlDropdownBulk.Items.Clear();
                    RecordManager.PopulateTableDropDown((int)theColumn.ColumnID, ref ddlDropdownBulk, "", ""); //3611
                    // PutDDLValue_Text(theColumn.DropdownValues, ref ddlDropdownBulk);
                }
                else
                {
                    ddlDropdownBulk.Items.Clear();
                }

            }
        }

        //if(!IsPostBack)
        //{
        //   if(hfChkAll.Value=="yes")
        //   {
        //       trUpdateEveryItem.Visible = true;
        //   }
        //   else
        //   {
        //       trUpdateEveryItem.Visible = false;
        //   }
        //}

        //ScriptManager.RegisterStartupScript(this, this.GetType(), "emJSCode" + ViewState["_strDynamictabPart"].ToString(), "EditManyCheckAll();", true);

    }

    protected void PopulateYAxisBulk(int TableID, int ViewID)
    {

        DataTable dtSCs = RecordManager.ets_Table_Columns_Summary(TableID, ViewID);

        foreach (DataRow dr in dtSCs.Rows)
        {
            if (bool.Parse(dr["IsStandard"].ToString()) == false && (dr["IsReadOnly"]==DBNull.Value ||  dr["IsReadOnly"].ToString().ToLower() != "true"))
            {
                if (dr["ColumnType"].ToString() == "text" || dr["ColumnType"].ToString() == "checkbox"
                    || dr["ColumnType"].ToString() == "number"
                 || dr["ColumnType"].ToString() == "date" || dr["ColumnType"].ToString() == "datetime"
                    || (dr["ColumnType"].ToString() == "dropdown"))
                {
                    System.Web.UI.WebControls.ListItem aItem = new System.Web.UI.WebControls.ListItem(dr["Heading"].ToString(), dr["ColumnID"].ToString());

                    ddlYAxisBulk.Items.Insert(ddlYAxisBulk.Items.Count, aItem);
                }
            }

        }

        System.Web.UI.WebControls.ListItem fItem = new System.Web.UI.WebControls.ListItem("-- None --", "");

        ddlYAxisBulk.Items.Insert(0, fItem);

    }

}
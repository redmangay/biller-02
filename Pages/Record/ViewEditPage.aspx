﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true" CodeFile="ViewEditPage.aspx.cs" Inherits="Pages_Record_ViewEditPage" %>

<%@ Register Src="~/Pages/UserControl/ViewDetail.ascx" TagName="OneView" TagPrefix="dbg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Script/jquery-ui-1.9.2.custom.min.js")%>"></script> --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Script/jquery-ui-1.12.1.min.js")%>"></script>

    <script language="javascript" type="text/javascript">

        var fixHelper = function (e, ui) {
            ui.children().each(function () {
                $(this).width($(this).width());
            });

            return ui;
        };


        //function CloseAndRefresh() { jquery-ui-1.11.3.min.css
        //    window.parent.document.getElementById('btnReloadMe').click();
        //    parent.$.fancybox.close();

        //}

    </script>

    <div style="text-align: center;">
        <div style="display: inline-block;">
            <dbg:OneView runat="server" ID="vdOne" />
        </div>
    </div>


    <%--<script type="text/javascript" src="../../JS/jquery.mCustomScrollbar.js"></script>
    <script type="text/javascript">
        $(function () {
            setTimeout(function () {
                $("body").mCustomScrollbar({
                    theme: "dark"
                });
            }, 3000);
            
        });        
    </script>--%>
</asp:Content>


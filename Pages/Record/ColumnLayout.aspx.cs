﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Record_ColumnLayout : SecurePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if(Request.UrlReferrer!=null)
            {
                hlBack.NavigateUrl = Request.UrlReferrer.AbsoluteUri;
            }
        }
    }
}
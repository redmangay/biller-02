﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.IO;
public partial class Pages_Record_PinImageAdvanced : System.Web.UI.Page
{
    string _qsTableID = "";
    Table _theTable;
    protected void Page_Load(object sender, EventArgs e)
    {
        _qsTableID = Cryptography.Decrypt(Request.QueryString["TableID"].ToString());
        _theTable = RecordManager.ets_Table_Details(int.Parse(_qsTableID));
        if(!IsPostBack)
        {
            PopulateTLControllingField();
            PopulateTheRecord();
        }
    }

    protected void PopulateTheRecord()
    {
       if(!string.IsNullOrEmpty(_theTable.PinImageAdvanced))
       {
           try
           {
               PopulateMapPin(_theTable.PinImageAdvanced);
              //if(IsPostBack)
               //ddlControllingField_SelectedIndexChanged(null, null);
           }
           catch
           {
               //
           }
           
       }
    }
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        if(ddlControllingField.SelectedValue!="")
        {
            _theTable.PinImageAdvanced = GetMapPinXML();
            //if (_theTable.PinImageAdvanced!="")
            //{
            //    _theTable.PinImage = "";
            //}
            RecordManager.ets_Table_Update(_theTable);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Close the popup", "GetBack();", true);
        }        
    }
    protected void PopulateTLControllingField()
    {
        ddlControllingField.DataSource = Common.DataTableFromText("SELECT ColumnID,DisplayName FROM [Column] WHERE   TableID=" + _qsTableID+ " AND IsStandard=0 ");
        ddlControllingField.DataBind();
        ListItem liAll = new ListItem("--Please Select-", "");
        ddlControllingField.Items.Insert(0, liAll);

    }
    protected string GetMapPinXML()
    {
        string strMapPinValue = "<?xml version='1.0' encoding='utf-8' ?><items>";
        bool bFoundValue = false;
        for (int i = 0; i < grdMapPin.Rows.Count; i++)
        {
            GridViewRow row = grdMapPin.Rows[i];
            DropDownList ddlPinImages = row.FindControl("ddlPinImages") as DropDownList;
            Pages_UserControl_ControlByColumn cbcValue = row.FindControl("cbcValue") as Pages_UserControl_ControlByColumn;
            if (cbcValue.ddlYAxisV != "" && cbcValue.TextValue != "")
            {
                bFoundValue = true;
                strMapPinValue = strMapPinValue + "<item><columnid>"+ddlControllingField.SelectedValue+"</columnid><value>" + HttpUtility.HtmlEncode(cbcValue.TextValue)
                    + "</value><ImageFile>" + HttpUtility.HtmlEncode(ddlPinImages.SelectedValue) + "</ImageFile></item>";
            }

        }


        if (bFoundValue == false)
        {
            lblMsg.Text = "Please select some Value and Traffic image.";
            return "";
        }
        else
        {
            strMapPinValue = strMapPinValue + "</items>";
            return strMapPinValue;
        }
    }
    protected void PopulateMapPin(string strXML)
    {

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(strXML);

        XmlTextReader r = new XmlTextReader(new StringReader(xmlDoc.OuterXml));

        DataSet ds = new DataSet();
        ds.ReadXml(r);

        if (ds.Tables[0] != null)
        {
            DataTable dtMapPin = ds.Tables[0];
            dtMapPin.Columns.Add("ID");
            dtMapPin.AcceptChanges();


            foreach (DataRow dr in dtMapPin.Rows)
            {
                ddlControllingField.SelectedValue = dr["columnid"].ToString();
                break;
            }


            foreach (DataRow dr in dtMapPin.Rows)
            {
                dr["ID"] = Guid.NewGuid().ToString();
            }
            dtMapPin.AcceptChanges();
            grdMapPin.DataSource = dtMapPin;
            grdMapPin.DataBind();

        }
    }



    //protected void MapPinValueChanged(object sender, EventArgs e)
    //{
    //    Pages_UserControl_ControlByColumn cbcValue = sender as Pages_UserControl_ControlByColumn;

    //    if (cbcValue != null)
    //    {
    //        GridViewRow row = cbcValue.NamingContainer as GridViewRow;
    //        Label lblID = row.FindControl("lblID") as Label;
    //        ImageButton imgbtnMinus = row.FindControl("imgbtnMinus") as ImageButton;
    //        ImageButton imgbtnPlus = row.FindControl("imgbtnPlus") as ImageButton;

    //        if (cbcValue.ddlYAxisV != "" && cbcValue.TextValue != "")
    //        {
    //            imgbtnPlus.Visible = true;
    //        }
    //        else
    //        {
    //            imgbtnPlus.Visible = false;
    //        }
    //    }


    //}


    protected void grdMapPin_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            //SetMapPinRowData();

            DataTable dtMapPin = new DataTable();
            dtMapPin.Columns.Add("value");
            dtMapPin.Columns.Add("ImageFile");
            dtMapPin.Columns.Add("ID");

            for (int i = 0; i < grdMapPin.Rows.Count; i++)
            {
                GridViewRow row = grdMapPin.Rows[i];
                DropDownList ddlPinImages = row.FindControl("ddlPinImages") as DropDownList;
                Label lblID = row.FindControl("lblID") as Label;
                Pages_UserControl_ControlByColumn cbcValue = row.FindControl("cbcValue") as Pages_UserControl_ControlByColumn;
                if (cbcValue.ddlYAxisV != "" && cbcValue.TextValue != "")
                {
                    dtMapPin.Rows.Add(cbcValue.TextValue, ddlPinImages.SelectedValue, lblID.Text);
                }

            }
            dtMapPin.AcceptChanges();

            if (e.CommandName == "minus")
            {



                if (dtMapPin.Rows.Count == 1)
                {
                    return;
                }

                for (int i = dtMapPin.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = dtMapPin.Rows[i];
                    if (dr["id"].ToString() == e.CommandArgument.ToString())
                    {
                        dr.Delete();
                        break;
                    }

                }


                dtMapPin.AcceptChanges();

                grdMapPin.DataSource = dtMapPin;
                grdMapPin.DataBind();
                //PopulateMapPin();




            }
            if (e.CommandName == "plus")
            {


                DataRow newRow = dtMapPin.NewRow();
                newRow["ID"] = Guid.NewGuid().ToString();
                newRow["value"] = "";
                newRow["ImageFile"] = "";
                //newRow["columnid"] = ddlControllingField.SelectedValue;
                //dtMapPin.Rows.InsertAt(newRow, iPos + 1);
                dtMapPin.Rows.Add(newRow);
                dtMapPin.AcceptChanges();

                grdMapPin.DataSource = dtMapPin;
                grdMapPin.DataBind();

                //PopulateMapPin();



            }
        }
        catch
        {
            //
        }
    }




    protected void grdMapPin_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblID = e.Row.FindControl("lblID") as Label;
                ImageButton imgbtnMinus = e.Row.FindControl("imgbtnMinus") as ImageButton;
                ImageButton imgbtnPlus = e.Row.FindControl("imgbtnPlus") as ImageButton;
                DropDownList ddlPinImages = e.Row.FindControl("ddlPinImages") as DropDownList;
                //PopulateddlPinImages(ref ddlPinImages);
               

                Pages_UserControl_ControlByColumn cbcValue = e.Row.FindControl("cbcValue") as Pages_UserControl_ControlByColumn;


                cbcValue.TableID = int.Parse(_qsTableID);
                cbcValue.ddlYAxisV = ddlControllingField.SelectedValue;
                cbcValue.TextValue = DataBinder.Eval(e.Row.DataItem, "value").ToString();
                if (ddlPinImages.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "ImageFile").ToString()) != null)
                    ddlPinImages.SelectedValue = DataBinder.Eval(e.Row.DataItem, "ImageFile").ToString();
                if (cbcValue.TextValue == "")
                {
                    cbcValue.TextValue = "0";
                }
               

                lblID.Text = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
                imgbtnMinus.CommandArgument = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
                imgbtnPlus.CommandArgument = DataBinder.Eval(e.Row.DataItem, "ID").ToString();


            }
        }
        catch
        {
            //
        }
    }

    protected void ddlControllingField_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlControllingField.SelectedValue == "")
        {
            //trTLValueImage.Visible = false;
        }
        else
        {
            //trTLValueImage.Visible = true;

            DataTable dtMapPin = new DataTable();
            dtMapPin.Columns.Add("value");
            dtMapPin.Columns.Add("ImageFile");
            dtMapPin.Columns.Add("ID");
            DataRow newRow = dtMapPin.NewRow();
            newRow["ID"] = Guid.NewGuid().ToString();
            newRow["value"] = "";
            newRow["ImageFile"] = "";
            //dtMapPin.Rows.InsertAt(newRow, iPos + 1);
            dtMapPin.Rows.Add(newRow);
            dtMapPin.AcceptChanges();

            grdMapPin.DataSource = dtMapPin;
            grdMapPin.DataBind();
        }
    }
}
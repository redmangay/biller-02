﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master"
    AutoEventWireup="true" CodeFile="ChildTableDetail.aspx.cs" Inherits="Pages_Record_ChildTableDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

    <link href="<%=ResolveUrl("~/fancybox3/dist/jquery.fancybox.min.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/fancybox3/dist/jquery.fancybox.min.js")%>"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#chkConditions').click(function (e) {

                var chk = document.getElementById("chkConditions");
                if (chk.checked == false) { $("#trConditionsControls").fadeOut(); }
                if (chk.checked == true) { $("#trConditionsControls").fadeIn(); }
            });


            var chk = document.getElementById("chkConditions");
            if (chk.checked == false) { $("#trConditionsControls").fadeOut(); }
            if (chk.checked == true) { $("#trConditionsControls").fadeIn(); }

            $('#ddDDDisplayColumn').change(function (e) {

                var strColumnValue = $('#ddDDDisplayColumn').val();

                if (strColumnValue != '') {
                    $('#hlDDEdit').fadeOut();
                    document.getElementById('hfDisplayColumnsFormula').value = '[' + $('#ddDDDisplayColumn option:selected').text() + ']';
                }
                else {
                    $('#hlDDEdit').fadeIn();
                }

                document.getElementById('hlDDEdit').href = '../Help/TableColumn.aspx?ct=yes&formula=' + encodeURIComponent(document.getElementById('hfDisplayColumnsFormula').value) + '&Tableid=' + document.getElementById('hfParentTableID').value;


            });

        });

    </script>

    <div style="text-align: center;">
        <div style="display: inline-block;">

            <div style="min-height: 400px;">
                <table>
                    <tr>
                        <td colspan="2" style="height: 50px;"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label runat="server" ID="lblDetailTitle" Font-Size="16px" Font-Bold="true" Text="Child Table - Detail"> </asp:Label>
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <strong>Tab Table:</strong>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlChildTable" runat="server" AutoPostBack="true" DataTextField="TableName"
                                Width="255px" DataValueField="TableID" CssClass="NormalTextBox" OnSelectedIndexChanged="ddlChildTable_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvTable" runat="server" ControlToValidate="ddlChildTable"
                                ErrorMessage="Required" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr runat="server" id="trConnection" visible="false">
                        <td align="right">
                            <strong>How are they connected:</strong>
                        </td>
                        <td align="left">
                            <table>
                                <tr>
                                    <td align="left">
                                        <strong runat="server" id="stgParentFieldCap">Parent Field </strong>
                                    </td>
                                    <td align="left" style="padding-left: 20px;">
                                        <strong runat="server" id="stgChildFieldCap">Child Field</strong>

                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">

                                        <asp:DropDownList runat="server" ID="ddlParentColumn" CssClass="NormalTextBox"
                                            DataValueField="ColumnID" DataTextField="DisplayNamne" Enabled="false">
                                        </asp:DropDownList>

                                    </td>
                                    <td align="left" style="padding-left: 20px;">
                                        <asp:DropDownList runat="server" ID="ddlChildColumn" Enabled="false"
                                            CssClass="NormalTextBox">
                                        </asp:DropDownList>

                                    </td>
                                </tr>

                            </table>

                        </td>
                    </tr>

                    <tr runat="server" id="trDisplayColumn" visible="false">
                        <td align="right">
                            <strong runat="server" id="stgDisplayField">Display Field</strong>
                        </td>
                        <td align="left">
                            <asp:DropDownList runat="server" ID="ddDDDisplayColumn" ClientIDMode="Static" CssClass="NormalTextBox">
                            </asp:DropDownList>
                            <asp:HyperLink runat="server" ID="hlDDEdit" Text="Edit" ClientIDMode="Static" NavigateUrl="~/Pages/Help/TableColumn.aspx" Enabled="false"></asp:HyperLink>
                            <asp:HiddenField runat="server" ID="hfDisplayColumnsFormula" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="hfParentTableID" ClientIDMode="Static" />
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <strong>Tab Label:</strong>
                        </td>
                        <td align="left">


                            <asp:TextBox ID="txtDescription" runat="server" Width="250px"
                                CssClass="NormalTextBox"></asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <strong>Show On Detail Page:</strong>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlDetailPageType" runat="server" AutoPostBack="false"
                                Width="150px" CssClass="NormalTextBox">
                                <asp:ListItem Text="Not displayed" Value="not" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="As a list" Value="list"></asp:ListItem>
                                <asp:ListItem Text="One at a time" Value="one"></asp:ListItem>
                                <asp:ListItem Text="One Record Only" Value="alone"></asp:ListItem>
                            </asp:DropDownList>

                        </td>
                    </tr>


                    <tr>
                        <td></td>
                        <td align="left">
                            <asp:CheckBox runat="server" ID="chkShowAddButton" Text="Add Button" TextAlign="Right" Font-Bold="true" Checked="true" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="left">
                            <asp:CheckBox runat="server" ID="chkShowEditButton" Text="Edit Button" TextAlign="Right" Font-Bold="true" Checked="true" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="left">
                            <asp:CheckBox runat="server" ID="chkShowGraphIcon" Text="Show Graph Icon" TextAlign="Right" Font-Bold="true" Checked="false" />
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td align="left">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="3" align="left">
                                        <asp:CheckBox runat="server" ID="chkConditions" Text="Show When" TextAlign="Right"
                                            Font-Bold="true" ClientIDMode="Static" />
                                    </td>
                                </tr>
                                <tr id="trConditionsControls" style="display: none;">
                                    <td valign="top" style="padding-left: 20px;">
                                        <asp:DropDownList runat="server" ID="ddlHideColumn" CssClass="NormalTextBox"
                                            DataValueField="ColumnID" DataTextField="DisplayNamne" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlHideColumn_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td valign="top" style="padding-left: 3px;">
                                        <asp:DropDownList runat="server" ID="ddlOperator" CssClass="NormalTextBox">
                                            <asp:ListItem Value="equals" Text="Equals" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="contains" Text="Contains"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td valign="top" style="padding-left: 3px;">
                                        <asp:TextBox runat="server" ID="txtConditionValue"
                                            CssClass="NormalTextBox" Width="200px"></asp:TextBox>

                                        <asp:ListBox runat="server" ID="lstConditionValue" Visible="false"
                                            SelectionMode="Multiple" Style="min-width: 200px; min-height: 100px;"></asp:ListBox>

                                        <asp:DropDownList runat="server" ID="ddlConditionValue"
                                            Visible="false" CssClass="NormalTextBox">
                                        </asp:DropDownList>

                                        <asp:HiddenField runat="server" ID="hfConditionValueControl" Value="text" />

                                    </td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                    <tr runat="server" id="trFilterSQL">
                        <td align="right">
                            <strong>Filter SQL:</strong>
                        </td>
                        <td align="left">
                            <asp:TextBox runat="server" ID="txtFilterSQL" Width="400" TextMode="MultiLine" Height="50"></asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td align="right"></td>
                        <td align="left">
                            <%--<asp:CheckBox runat="server" ID="chkChildService" Text="" TextAlign="Right" Font-Bold="true"
                    ClientIDMode="Static" />--%>

                            <asp:HyperLink runat="server" NavigateUrl="~/Pages/Help/ColumnService.aspx"
                                ID="hlChildService" ClientIDMode="Static">Service...</asp:HyperLink>
                        </td>
                    </tr>

                    <tr style="height: 20px;">
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="left">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <div runat="server" id="div1" style="padding-left: 10px;">
                                            <asp:LinkButton runat="server" ID="lnkSave" CssClass="btn" CausesValidation="true"
                                                OnClick="lnkSave_Click"> <strong>Save</strong></asp:LinkButton>
                                        </div>
                                    </td>
                                    <td>
                                        <div runat="server" id="div2" style="padding-left: 10px;">
                                            <asp:LinkButton runat="server" ID="lnkBack" OnClientClick="javascript:  parent.$.fancybox.close();"
                                                CssClass="btn" CausesValidation="false"> <strong>Cancel</strong></asp:LinkButton>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>



    <script type="text/javascript">

        function CloseAndRefresh() {
            window.parent.document.getElementById('btnRefreshGrid').click();
            parent.$.fancybox.close();
            // alert('ok');
        }

    </script>

</asp:Content>


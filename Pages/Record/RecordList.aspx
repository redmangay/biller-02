<%@ Page Language="C#" MasterPageFile="~/Home/rResponsive.master" CodeFile="RecordList.aspx.cs"
    Inherits="Record_Record_List" EnableEventValidation="false" %>

<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>
<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Pages/UserControl/RecordList.ascx" TagName="RecordList" TagPrefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <asp:Literal ID="ltTextStyles" runat="server"></asp:Literal>
    <style type="text/css">
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
<script type="text/javascript">


    //$(document).ready(function () {
    //    $(window).keydown(function (event) {
    //        if (event.keyCode == 13) {
    //            event.preventDefault();
    //            return false;
    //        }
    //    });
    //});

    //$(document).ready(function () {

    //    //$(".inlineeditingclass").click(function (e) {
    //    $('.inlineeditingclass').dblclick(function (e) {
    //        e.stopPropagation();
    //        var currentEle = $(this);
    //        var value = $(this).html();
    //        var row = $(e.target).closest('tr');
    //        //var DBGRecordID = row.find($("[id*=key]")).val();
    //        var DBGRecordID = $(this).attr('recordid');
    //        var vxxx = $(this).attr('systemname');
    //        UpdateVal(currentEle, value, DBGRecordID, vxxx);
    //    });

    //});

    function UpdateVal(CurrentEle, value, DBGRecordID, ColumnToUpdate) {
        if ($(".cellValue") !== undefined) {
            if ($(".cellValue").val() !== undefined) {
                $(".cellValue").parent().html($("#OriginalValue").val().trim());
                $(".cellValue").remove();
            }
            if (value.match("<") == null) {
                $(CurrentEle).html('<div class="cellValue" id="cellWrapper"> ' +
                    ' <table  cellspacing="0" cellpadding="0" style="border:none"><tr><td  style="border:none"><input class="cellValue" type="text" id="txtValue" value="' + value + '" />' +
                    ' <input class="cellValue" type="hidden" value="' + DBGRecordID + '" id="keySelected" />' +
                    ' <input class="cellValue" type="hidden" value="' + ColumnToUpdate + '" id="ColumnToUpdate" />' +
                    ' <input class="cellValue" type="hidden" value="' + value + '" id="OriginalValue" /></td>' +
                    '<td  style="border:none"><input class="cellValue" type="button" style="background: url(GreenTick.png);width:18px;height:18px;border:none;padding-top:5px;" title="Save" id="btnOneSave" onClick="return SaveChanges()" /></td>' +
                    ' <td  style="border:none"><input class="cellValue" type="button" style="background: url(RedCross.png);width:18px;height:18px;border:none;padding-top:5px;" title="Cancel" id="btnOneCancel" onClick="return CancelChanges()" /></td></tr></table> ' +
                    //' <input class="cellValue" type="hidden" value="' + FieldVxxx + '" id="hfFieldVxxx" />' +  
                    //' <input class="cellValue" type="button" value="Save" id="btnOneSave" onClick="return SaveChanges()" />' +
                    //' <input class="cellValue" type="button" value="Cancel" id="btnOneCancel" onClick="return CancelChanges()" /> ' +
                    ' </div> ');
            }
            $(".cellValue").focus();
            //$(".cellValue").keyup(function (event) {
            //    if (event.keyCode == 13) {
            //        SaveChanges();
            //    }
            //});
            //$("#txtValue").blur(function (e) {
            //    CancelChanges();
            //});
        }
    }

    function CancelChanges(e) {
        if ($(".cellValue") !== undefined) {
            if ($(".cellValue").val() !== undefined) {
                $(".cellValue").parent().html($("#OriginalValue").val().trim());
                $(".cellValue").remove();
            }
        }
        //window.location.reload();
    }
    function SaveChanges(e) {
        var onerecord = {};
        onerecord.ColumnToUpdate = $("[id*=ColumnToUpdate]").val();
        onerecord.FieldValue = $("[id*=txtValue]").val();
        onerecord.ID = $("[id*=keySelected]").val();

        $.ajax({
            type: "POST",
            url: "RecordList.aspx/SaveOneCell",
            data: '{onerecord: ' + JSON.stringify(onerecord) + '}',
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            success: function (response) {
                //alert("saved!");
                //location.href = location.href;
                window.location.reload();
            }
        });
        if ($(".cellValue") !== undefined) {
            if ($(".cellValue").val() !== undefined) {
                $(".cellValue").parent().html(onerecord.FieldValue);
                $(".cellValue").remove();
            }
        }


    }





</script>
    
    <%-- OnClick="btnReloadMe_Click"--%>
    <asp:Button ID="btnReloadMe" runat="server" ClientIDMode="Static" OnClientClick="ReloadMe();return false;"
        Style="display: none;" />

    <table>
        <tr>
            <td>
                <asp:RecordList runat="server" ID="rlOne" PageType="p" ShowAddButton="true" ShowEditButton="true" />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label runat="server" ID="lblSummaryPageContent"></asp:Label>
            </td>
        </tr>
    </table>
    <script type="text/javascript" src="../../JS/ScrollableTable/table-scroll.min.js"></script>
</asp:Content>

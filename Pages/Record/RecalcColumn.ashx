﻿<%@ WebHandler Language="C#" Class="RecalcColumn" %>

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class RecalcColumn : IHttpHandler
{
    public void ProcessRequest (HttpContext context)
    {
        Dictionary<string, string> response = new Dictionary<string, string>();
        if (context.Request.Params.Count > 0)
        {
            JObject joData = JObject.Parse(context.Request.Params[0]);
            if (joData.Property("dependentColumns") != null)
            {
                List<string> dependentColumns = joData["dependentColumns"].ToObject<List<string>>();
                if (joData.Property("columnValues") != null)
                {
                    Dictionary<string, string> columnValues = joData["columnValues"].ToObject<Dictionary<string, string>>();

                    if (joData.Property("queryString") != null)
                    {
                        string queryString = joData.Property("queryString").Value.ToString();
                        string[] queryParameters = queryString.Split('&');
                        Dictionary<string, string> queryParametersDictionary = new Dictionary<string, string>();
                        foreach (string param in queryParameters)
                        {
                            int pos = param.IndexOf('=');
                            queryParametersDictionary.Add(param.Substring(0, pos).ToLower(), param.Substring(pos + 1));
                        }

                        string mode = "edit";
                        if (queryParametersDictionary.ContainsKey("mode"))
                            mode = Cryptography.Decrypt(queryParametersDictionary["mode"]).ToLower();
                        int tableId = 0;
                        if (queryParametersDictionary.ContainsKey("tableid"))
                            int.TryParse(Cryptography.Decrypt(queryParametersDictionary["tableid"]), out tableId);
                        int recordId = 0;
                        if (queryParametersDictionary.ContainsKey("recordid"))
                            int.TryParse(Cryptography.Decrypt(queryParametersDictionary["recordid"]), out recordId);
                        int? parentRecordId = null;
                        if (queryParametersDictionary.ContainsKey("parentrecordid"))
                        {
                            int parentRecordIdTest = 0;
                            if (int.TryParse(queryParametersDictionary["parentrecordid"], out parentRecordIdTest))
                            {
                                parentRecordId = parentRecordIdTest;
                            }
                            else
                            {
                                if (int.TryParse(Cryptography.Decrypt(queryParametersDictionary["parentrecordid"]), out parentRecordIdTest))
                                {
                                    parentRecordId = parentRecordIdTest;
                                }
                            }
                        }
                        bool useArchiveData = false;
                        if (queryParametersDictionary.ContainsKey("usearchivedata"))
                            if (String.Compare(queryParametersDictionary["usearchivedata"], "yes",
                                    StringComparison.InvariantCultureIgnoreCase) == 0)
                                useArchiveData = true;

                        Table table = RecordManager.ets_Table_Details(tableId);
                        DataTable allColumns = RecordManager.ets_Table_Columns_All(tableId);
                        Record editRecord = null;
                        if (String.Compare("add", mode, StringComparison.InvariantCultureIgnoreCase) == 0)
                            editRecord = new Record {TableID = tableId};
                        else if (String.Compare("edit", mode, StringComparison.InvariantCultureIgnoreCase) == 0)
                            editRecord = RecordManager.ets_Record_Detail_Full(recordId, tableId, useArchiveData);

                        if (table != null && allColumns != null && editRecord != null)
                        {
                            bool isIgnoreMidnight = false;
                            if (table.AccountID.HasValue)
                            {
                                string strIgnoreMidnight =
                                    SystemData.SystemOption_ValueByKey_Account("Time Calculation Ignore Midnight",
                                        (int)table.AccountID, tableId);
                                if (strIgnoreMidnight != "" && strIgnoreMidnight.ToString().ToLower() == "yes")
                                {
                                    isIgnoreMidnight = true;
                                }
                            }
                            
                            foreach (string columnName in columnValues.Keys)
                            {


                                if (columnName.IndexOf("table") > 0)
                                {
                                    try
                                    {
                                        string strLinkValue = columnValues[columnName];
                                        if(!string.IsNullOrEmpty(strLinkValue))
                                        {
                                            DataTable dtTemp = Common.DataTableFromText("SELECT TableTableID,DisplayColumn FROM [Column] WHERE TableID=" + tableId.ToString() + " AND SystemName='" + columnName.Substring(0, 4) + "'");

                                            if (dtTemp != null)
                                            {
                                                string strParentRecordID = Common.GetLinkedIDFromDisplayText(int.Parse(dtTemp.Rows[0][0].ToString()), dtTemp.Rows[0][1].ToString(), strLinkValue);
                                                RecordManager.MakeTheRecord(ref editRecord, columnName.Substring(0, 4), strParentRecordID);
                                            }
                                        }                                        
                                    }
                                    catch
                                    {
                                        //
                                    }

                                }
                                else if (columnName.IndexOf("time") > 0)
                                {
                                    try
                                    {
                                        string strDateTime = columnValues[columnName.Substring(0, 4)] + " " + columnValues[columnName];
                                        RecordManager.MakeTheRecord(ref editRecord, columnName.Substring(0, 4), strDateTime);
                                    }
                                    catch
                                    {
                                        //
                                    }
                                    
                                }
                                else
                                {
                                    //check if it is a calulated column
                                    DataTable dtTemp = Common.DataTableFromText("SELECT ColumnID FROM [Column] WHERE TableID="+table.TableID.ToString()
                                        +" AND SystemName='"+columnName+"' and Calculation is not null and ColumnType='calculation'");

                                    string sColumnValue = columnValues[columnName];
                                    if (dtTemp != null && dtTemp.Rows.Count>0)
                                    {
                                        sColumnValue = RecordManager.CalculateColumnValue(table, columnName, allColumns, editRecord,
                                        parentRecordId,
                                        isIgnoreMidnight);
                                    }
                                    RecordManager.MakeTheRecord(ref editRecord, columnName, sColumnValue);
                                }
                                
                            }

                           

                            foreach (string columnName in dependentColumns)
                            {
                                response.Add(columnName,
                                    RecordManager.CalculateColumnValue(table, columnName, allColumns, editRecord,
                                        parentRecordId,
                                        isIgnoreMidnight));
                            }
                        }
                    }
                }
            }
        }

        context.Response.ContentType = "application/json";
        context.Response.Write(JsonConvert.SerializeObject(response));
    }


    public bool IsReusable {
        get {
            return false;
        }
    }
}
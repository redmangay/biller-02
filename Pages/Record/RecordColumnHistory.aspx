﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="RecordColumnHistory.aspx.cs" EnableEventValidation="false"
    Inherits="Pages_Record_ColumnHistory" %>

<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>
<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Pages/UserControl/ControlByColumn.ascx" TagName="ControlByColumn" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

     <style type="text/css">
        .chkbox input[type="checkbox"] 
        { 
            margin-right: 5px; 
        }
    </style>

    <script type="text/javascript">
        $(function () {
            $('.popupViewHistoryAudit').fancybox({
                iframe: {
                    css: {
                        width: '60%',
                        height: '60%'
                    }
                },
                toolbar: false,
                smallBtn: false,
                scrolling: 'auto',
                type: 'iframe',
                titleShow: false
            });
        });


        $(document).ready(function () {
            $("#hlBack").on('click', function () {
                parent.$.fancybox.close();
            });
        });
    </script>
     <div>
         <div runat="server" id="divHlBack"  style="padding-right: 20px; float:right;">                                                                      

             <a id="hlBack" runat="server" clientidmode="Static" href="javascript:;" > 
                 <asp:Image runat="server" ID="Image1" ImageUrl="~/App_Themes/Default/images/Back.png"
                ToolTip="Back" />
             </a>
         </div>
    </div>
       
    <div style="padding-left: 20px; padding-right: 20px; padding-bottom:150px;  height:auto;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" onload="ShowHide();">
           
            <tr>
                <td colspan="3">
                      <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div runat="server" id="divHistoryRoot">
                               
                                <br />
                                <div runat="server" id="divHistory" >
                                    <strong>Change History</strong>
                                    <br />
                                    <dbg:dbgGridView ID="gvChangedLog" runat="server" GridLines="Both" CssClass="gridview"
                                        HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" AllowPaging="True"
                                        AllowSorting="false" DataKeyNames="DateAdded" HeaderStyle-ForeColor="Black" Width="100%"
                                        AutoGenerateColumns="false" PageSize="15" OnPreRender="gvChangedLog_PreRender"
                                        OnRowDataBound="gvChangedLog_RowDataBound">
                                        <PagerSettings Position="Top" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:HyperLink runat="server" ID="hlView" CssClass="popuplink">
                                                        <asp:Image runat="server" ID="imgView" ImageUrl="~/App_Themes/Default/Images/iconShow.png" />
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Updated Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="UpdateDate" runat="server" Text='<%# Eval("DateAdded") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="User">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUser" runat="server" Text='<%# Eval("User") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Changed Field List">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblColumnList" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="gridview_header" ForeColor="Black" HorizontalAlign="Center" />
                                        <RowStyle CssClass="gridview_row" HorizontalAlign="Center" />
                                        <PagerTemplate>
                                            <asp:GridViewPager runat="server" ID="CL_Pager" HideFilter="true" _doWordOnPage="true" DOCustomPDF="true" HideAdd="true"
                                                HideDelete="true" OnBindTheGridToExport="CL_Pager_BindTheGridToExport" OnApplyFilter="CL_Pager_OnApplyFilter"
                                                OnBindTheGridAgain="CL_Pager_BindTheGridAgain" OnCustomExportPDF="CL_Pager_OnExportForPDF" OnExportForWord="CL_Pager_OnExportForWord" OnExportForCSV="CL_Pager_OnExportForCSV" />
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            <div style="padding-left: 100px;">
                                                No changes have been made yet.
                                            </div>
                                        </EmptyDataTemplate>
                                    </dbg:dbgGridView>
                                </div>
                            </div>
                            </ContentTemplate>
                           <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="gvChangedLog" />
                        </Triggers>
                      </asp:UpdatePanel>
                        
                </td>
            </tr>
           
        </table>
    </div>
 
</asp:Content>

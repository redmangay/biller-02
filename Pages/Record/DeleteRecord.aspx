﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true" CodeFile="DeleteRecord.aspx.cs" Inherits="Pages_Record_DeleteRecord" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" Runat="Server">
    <asp:ScriptManagerProxy runat="server" ID="ScriptManagerProxy1"></asp:ScriptManagerProxy>
<asp:UpdatePanel ID="upMain" runat="server">
    <ContentTemplate>
    <asp:Panel runat="server" ID="pnlDeleteAll">
         <asp:Label runat="server" ID="lblTitle" CssClass="TopTitle"></asp:Label>
    <br />
                <div style="padding-top: 50px; padding: 20px;">
                               <table>
                        <tr runat="server" id="trDeleteRestoreMessage">
                            <td>
                                <asp:Label runat="server" ID="lblDeleteRestoreMessage" Font-Bold="true" Text="Are you sure you want to delete selected item(s)?"></asp:Label>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr runat="server" id="trDeleteReason" visible="false">
                            <td align="left">
                                <strong>Please enter the reason for deleting the selected records*</strong><br />
                                <br />

                                <asp:TextBox runat="server" ID="txtDeleteReason" ValidationGroup="DR" CssClass="NormalTextBox"
                                    TextMode="MultiLine" Height="70px" Width="395px"></asp:TextBox>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                                        <td align="left">
                                            <asp:Label ID="lblDeleteActionMessageNote" runat="server" ForeColor="Red" Width="400px"></asp:Label>
                                        <br />
                                        <br />
                                        </td>
                                    <tr>
                                        <td align="center">
                                            <asp:LinkButton ID="lnkDeleteAllOK" runat="server" CssClass="btn"> <strong>OK</strong></asp:LinkButton>
                                            <asp:LinkButton ID="lnkDeleteAllNo" runat="server" CausesValidation="false" CssClass="btn" OnClientClick="parent.$.fancybox.close();return false;"> 
                                                <strong>Cancel</strong></asp:LinkButton>
                                        </td>
                                        <caption>
                                            <br />
                                        </caption>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;</td>
                                <tr id="trUndo" runat="server" style="display: none;">
                                    <td>
                                        <asp:CheckBox ID="chkUndo" runat="server" AutoPostBack="false" Text="I understand that I will not be able to undo this action." TextAlign="Right" />
                                    </td>
                                </tr>
                                <tr id="trDeleteAllEvery" runat="server">
                                    <td>
                                        <asp:CheckBox ID="chkDelateAllEvery" runat="server" AutoPostBack="false" Text="I would like to delete EVERY item in this table" TextAlign="Right" />
                                    </td>
                                </tr>
                                <tr id="trDeleteParmanent" runat="server" style="display: none;">
                                    <td>
                                        <asp:CheckBox ID="chkDeleteParmanent" runat="server" AutoPostBack="false" Text="I wish to delete these records permanently." TextAlign="Right" />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 20px;">
                                        <asp:Label ID="lblDeleteMessageNote" runat="server" Text="Note: Deleted records are retained in the database and can be viewed or restored by your Admin user. Admin Users can also delete them permanently." Width="400px"></asp:Label>
                                    </td>
                                </tr>
                              
                            </tr>
                        </tr>
                     
                    </table>
                    <asp:HiddenField runat="server" ID="hfParmanentDelete" Value="no" />
                </div>
        
    </asp:Panel>
   
 </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

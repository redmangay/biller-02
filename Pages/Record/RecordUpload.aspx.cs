﻿using System;
//using System.Collections;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Data.SqlClient;
using DBG.Common;
public partial class Pages_Record_RecordUpload : SecurePage
{

    string _strFilesPhisicalPath = "";
    string _strFilesLocation = "";
    Table _theTable;
    ImportTemplate _theImportTemplate;
    Menu _qsMenu;
    User _ObjUser;
    string _qsTableID = "";
    int _iTotalDynamicColumns = 0;
    int _iValidRecordIDIndex = 0;
    string _strDateRecordedColumnName = "Date Recorded";
    string _strTimeSamledColumnName = "Time Recorded";
    string _strRecordRightID = Common.UserRoleType.None;
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            try
            {
                _ObjUser = (User)Session["User"];


                _strFilesPhisicalPath = Session["FilesPhisicalPath"].ToString();
                _strFilesLocation = Session["FilesLocation"].ToString();

                //JA 18 JAN 2017 FOR template dropdown
                try
                {
                    //Orig Code from query string  - God User
                    if (Request.QueryString["TableID"] != null)
                    {
                        _qsTableID = Cryptography.Decrypt(Request.QueryString["TableID"]);

                        //Red transferred here...
                        _theTable = RecordManager.ets_Table_Details(int.Parse(_qsTableID));

                        //JA 14 FEB 2017
                        //HttpContext.Current.Session["UserTableID"] = 0;
                    }

                }
                catch
                {
                    //Administrator User
                    //_qsTableID = Session["UserTableID"].ToString();
                }

                UserRole theUserRole = (UserRole)Session["UserRole"];
                //Condition 1
                if ((bool)theUserRole.IsAdvancedSecurity)
                {
                    //DataTable dtUserTable = SecurityManager.ets_UserTable_Select(null,
                    //    int.Parse(_qsTableID), _ObjUser.UserID, null);

                    DataTable dtUserTable = null;

                    //if (_ObjUser.RoleGroupID == null)
                    //{
                    dtUserTable = SecurityManager.dbg_RoleTable_Select(null,
                   int.Parse(_qsTableID), theUserRole.RoleID, null);

                    if (dtUserTable.Rows.Count > 0)
                    {
                        _strRecordRightID = dtUserTable.Rows[0]["RoleType"].ToString();
                    }
                }
                else
                {
                    if (Session["roletype"] != null)
                        _strRecordRightID = Session["roletype"].ToString();
                }
                //End Condition 1

                //Condition 2
                if (_strRecordRightID == Common.UserRoleType.None) //none role
                {
                    Response.Redirect(Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Default.aspx", false);
                    return;
                }


                //Condition 3
                if (!Common.HaveAccess(_strRecordRightID, "1,2,3,4,5,7,8,9"))
                { Response.Redirect("~/Default.aspx", false); }


                //Condition 4
                if (Common.HaveAccess(_strRecordRightID, Common.UserRoleType.ReadOnly))
                {
                    lnkNext.Enabled = false;
                    lblMsg.Text = "Read Only Account. To upload please ask your Account Administrator for appropriate rights.";
                }

                //Condition 5
                if (!IsPostBack)
                    PopulateImportTemplate(int.Parse(_qsTableID));


                //Condition 6
                //JA 18 JAN 2017 START  QUERYSTRING CODE IS HERE   
                //if (Request.QueryString["TableID"] != null)
                if (Request.QueryString["TableID"] != null || Request.QueryString["TableID"] == null) //Pass Administrator
                {
                    //JA 18 JAN 2017
                    var nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
                    nameValues.Set("TableID", _qsTableID);


                    //_qsMenu = RecordManager.ets_Menu_Details((int)_theTable.MenuID);


                    DataTable dtDateTimeColumnName = Common.DataTableFromText(@"SELECT      ImportHeaderName FROM  [Column] C JOIN ImportTemplateItem ITI 
                            ON C.ColumnID=ITI.ColumnID
                            WHERE SystemName='DateTimeRecorded' AND TableID=" + _theTable.TableID.ToString());

                    //condition 6.1
                    if (dtDateTimeColumnName.Rows.Count > 0 && dtDateTimeColumnName.Rows[0][0] != DBNull.Value)
                    {
                        string strDT = dtDateTimeColumnName.Rows[0][0].ToString();
                        if (strDT.IndexOf(",") > 0)
                        {
                            _strDateRecordedColumnName = strDT.Substring(0, strDT.IndexOf(","));
                            _strTimeSamledColumnName = strDT.Substring(strDT.IndexOf(",") + 1);
                        }
                        else
                        {
                            _strDateRecordedColumnName = strDT;
                            _strTimeSamledColumnName = "";
                        }
                    }

                }

                //JA 18 JAN 2017 END
                else
                {
                    Response.Redirect("~/empty.aspx", false);
                    return;
                }
                //END Condition 6

                //JA 18 JAN 2017
                if (!IsPostBack)
                {
                    BindGrids();

                    //CheckLocationColumn();
                    //JA 14 FEB 2017

                    hlBack.NavigateUrl = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;

                }

                if (_theTable.CustomUploadSheet != "")
                {

                    //hlCustomUploadSheet.NavigateUrl = "~//UserFiles//Template//" + _theTable.CustomUploadSheet;


                    if (_theTable.CustomUploadSheet != "")
                    {
                        try
                        {
                            //string strFilePath = Server.MapPath("~/UserFiles/Template/" + _theTable.CustomUploadSheet);

                            string strFilePath = _strFilesPhisicalPath + "\\UserFiles\\Template\\" + _theTable.CustomUploadSheet;

                            if (File.Exists(strFilePath))
                            {
                                lblImortType.Visible = false;
                                divColumnName.Visible = false;
                                divColumnPosition.Visible = false;
                                divCustomUploadFiles.Visible = true;
                                divUploadFiles.Visible = false;

                                hlCustomUploadSheet.Text = (_theTable.CustomUploadSheet.Substring(_theTable.CustomUploadSheet.IndexOf("_") + 1)).ToString();
                            }
                        }
                        catch
                        {
                            //
                        }
                    }
                }

                Title = "Upload Records - " + _theTable.TableName;
                lblTitle.Text = "Upload Records - " + _theTable.TableName;

                if (!IsPostBack)
                {
                    if (Session["CurrentBatch"] != null)
                    {
                        txtBatchDescription.Text = Session["CurrentBatch"].ToString();
                    }
                    //PopulateLocationDDL();
                }



                if (!IsPostBack && _theTable != null && _theTable.DefaultImportTemplateID != null)
                {
                    int? iIT_ID = _theTable.DefaultImportTemplateID; //Common.GetDefaultImportTemplate(_theTable.TableID);

                    if (iIT_ID != null && ddlTemplate.Items.FindByValue(iIT_ID.ToString()) != null)
                    {
                        ddlTemplate.SelectedValue = iIT_ID.ToString();
                        ddlTemplate_SelectedIndexChanged(null, null);
                    }
                }

            }
            catch (Exception ex)
            {

                ErrorLog theErrorLog = new ErrorLog(null, "Record Upload", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);

            }
        }
        catch
        { }

    }

    protected List<Tuple<string, string>> GetColumnInfo()
    {
        if (_theImportTemplate == null && ddlTemplate.SelectedValue != "")
        {
            _theImportTemplate = ImportManager.dbg_ImportTemplate_Detail(int.Parse(ddlTemplate.SelectedValue));
        }
        DataTable dtRecordTypleColumns = RecordManager.ets_Table_Columns_Import(int.Parse(_qsTableID), _theImportTemplate.ImportTemplateID);
        List<Tuple<string, string>> lstColumns = new List<Tuple<string, string>>();
        for (int j = 0; j < dtRecordTypleColumns.Rows.Count; j++)
        {
            string szDisplayName = dtRecordTypleColumns.Rows[j]["ImportHeaderName"].ToString().Trim();
            string szColumnType = dtRecordTypleColumns.Rows[j]["ColumnType"].ToString();

            if (szDisplayName != "" && lstColumns.Any(e => e.Item1.Contains(szDisplayName)) == false)
            {
                lstColumns.Add(Tuple.Create(szDisplayName, szColumnType));
            }
        }
        return lstColumns;
    }

    protected List<string> GetListColumnsByName()
    {
        if (_theImportTemplate == null && ddlTemplate.SelectedValue != "")
        {
            _theImportTemplate = ImportManager.dbg_ImportTemplate_Detail(int.Parse(ddlTemplate.SelectedValue));
        }
        DataTable dtRecordTypleColumns = RecordManager.ets_Table_Columns_Import(int.Parse(_qsTableID), _theImportTemplate.ImportTemplateID);
        List<string> lstColumns = new List<string>();
        for (int j = 0; j < dtRecordTypleColumns.Rows.Count; j++)
        {
            string strBeforeComma = dtRecordTypleColumns.Rows[j]["ImportHeaderName"].ToString();
            string strAfterComma = "";
            if (strBeforeComma.ToString().Trim().IndexOf(",") == -1)
            {
                //
            }
            else
            {
                strAfterComma = Common.AferComma(strBeforeComma);
                strBeforeComma = Common.BeforeComma(strBeforeComma);
            }

            if (strBeforeComma != "" && lstColumns.Contains(strBeforeComma) == false)
            {
                lstColumns.Add(strBeforeComma);
            }

            if (strAfterComma != "" && lstColumns.Contains(strAfterComma) == false)
            {
                lstColumns.Add(strAfterComma);
            }
        }
        return lstColumns;
    }
    protected void gvTheGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.Header)
        //{
        //    e.Row.Cells[_iTotalDynamicColumns].Visible = false;
        //    e.Row.Cells[_iValidRecordIDIndex + 1].Visible = false;

        //}

        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    e.Row.Cells[_iTotalDynamicColumns].Visible = false;
        //    e.Row.Cells[_iValidRecordIDIndex + 1].Visible = false;
        //}
    }
    protected DataTable GetPositionDataTable()
    {
        return Common.DataTableFromText(@"SELECT SystemName, DisplayName , PositionOnImport 
             FROM [Column] C JOIN ImportTemplateItem ITI ON C.ColumnID=ITI.ColumnID WHERE ITI.ImportTemplateID="
             + ddlTemplate.SelectedValue + @" AND PositionOnImport IS NOT NULL
                ORDER BY CASE 
				                WHEN Charindex(',', PositionOnImport)=0 THEN CAST(PositionOnImport AS INT)
				                WHEN Charindex(',', PositionOnImport)>1 THEN CAST(Substring(PositionOnImport, 1,Charindex(',', PositionOnImport)-1) AS INT)
		                  END");
    }

    protected void lnkDownloadXLS_Click(object sender, EventArgs e)
    {

        DataTable dtColumns = new DataTable();

        GetImportTemplate();


        if (_theImportTemplate.IsImportPositional == true)
        {
            //bool bFoundDateTime = false;
            dtColumns = GetPositionalColumns();
        }
        else
        {

            List<string> lstColumns = new List<string>();

            lstColumns = GetListColumnsByName();
            foreach (string strC in lstColumns)
            {
                dtColumns.Columns.Add(strC);
            }
            dtColumns.AcceptChanges();
            dtColumns.Rows.Add(dtColumns.NewRow());
            dtColumns.AcceptChanges();
        }




        ExportUtil.ExportToExcel(dtColumns, _theTable.TableName.Replace(' ', '-') + ".xls", null);

    }



    protected void hlCustomUploadSheet_Click(object sender, EventArgs e)
    {
        if (_theTable.CustomUploadSheet != "")
        {
            string strFilePath = _strFilesPhisicalPath + "\\UserFiles\\Template\\" + _theTable.CustomUploadSheet;
            if (File.Exists(strFilePath))
            {
                Response.ContentType = "application/octet-stream";

                //Response.AppendHeader("Content-Disposition", "attachment; filename=" +  theDocument.FileUniqename.Substring(37));
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + (_theTable.CustomUploadSheet.Substring(_theTable.CustomUploadSheet.IndexOf("_") + 1)).ToString());
                Response.WriteFile(strFilePath);
                Response.End();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Problem", "alert('This file is missing!');", true);

            }
        }
    }



    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }

    protected void lnkDownloadXLSX_Click(object sender, EventArgs e)
    {
        //XLSX

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition",
        "attachment;filename=\"" + _theTable.TableName + ".xlsx\"");
        //Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //Response.ContentType = "OCTET-STREAM";

        StringWriter sw = new StringWriter();
        //HtmlTextWriter hw = new HtmlTextWriter(sw);



        //BindGridAgainToExport();

        DataTable dt;
        if (_theImportTemplate.IsImportPositional == true)
        {
            dt = GetPositionDataTable();

            if (dt.Rows.Count > 0)
            {
                string strTempPoI = dt.Rows[dt.Rows.Count - 1]["PositionOnImport"].ToString();

                int iMaxColumn = int.Parse(Common.BeforeComma(strTempPoI));
                int m = 0;
                for (int i = 1; i <= iMaxColumn; i++)
                {

                    string strColumnName = "C" + i.ToString();
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (Common.BeforeComma(dr["PositionOnImport"].ToString()) == i.ToString())
                        {

                            strColumnName = dr["DisplayName"].ToString();

                        }
                    }
                    if (m == 0)
                    {
                        sw.Write(strColumnName);
                    }
                    else
                    {
                        sw.Write("\t" + strColumnName);
                    }
                    m = m + 1;


                }


                Response.Write(sw.ToString());
                //Response.BinaryWrite(EncodeString(sw.ToString(), "windows-1250"));
                Response.Flush();
                Response.End();


            }
        }
        else
        {
            int iTemp = 0;
            int? iImportTemplateID = null;

            if (ddlTemplate.SelectedValue != "")
                iImportTemplateID = int.Parse(ddlTemplate.SelectedValue);

            dt = UploadManager.ets_TempRecord_List(int.Parse(_qsTableID),
                 -1, false, null, null,
                 null, null,
              "", "", null, null, ref iTemp, ref _iTotalDynamicColumns, "", iImportTemplateID, "");
            DataSet ds = new DataSet();

            //OfficeManager.CreateWorkbook(dt.DataSet, Server.MapPath(""));


            int i = 0;

            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.ToLower() != "dbgsystemrecordid" && dc.ColumnName.ToLower() != "rownum")
                {
                    if (i == 0)
                    {

                        sw.Write(dc.ColumnName);
                    }
                    else
                    {

                        sw.Write("\t" + dc.ColumnName);
                    }
                    i = i + 1;
                }

            }

            Response.Write(sw.ToString());
            Response.Flush();
            Response.End();





        }



    }


    public static byte[] EncodeString(String sourceData, String charSet)
    {
        byte[] bSourceData = System.Text.Encoding.Unicode.GetBytes(sourceData);
        System.Text.Encoding outEncoding = System.Text.Encoding.GetEncoding(charSet);
        return System.Text.Encoding.Convert(System.Text.Encoding.Unicode, outEncoding, bSourceData);
    }

    protected void lnkDownloadTemplate_Click(object sender, EventArgs e)
    {
        //CSV
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition",
        "attachment;filename=\"" + _theTable.TableName.Replace(' ', '-') + ".csv\"");
        Response.Charset = "";
        Response.ContentType = "text/csv";

        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        DataTable dt;

        if (_theImportTemplate == null && ddlTemplate.SelectedValue != "")
        {
            _theImportTemplate = ImportManager.dbg_ImportTemplate_Detail(int.Parse(ddlTemplate.SelectedValue));
        }


        if (_theImportTemplate.IsImportPositional == true)
        {
            DataTable dtColumns = new DataTable();
            dtColumns = GetPositionalColumns();
            foreach (DataColumn drC in dtColumns.Columns)
            {
                sw.Write(drC.ColumnName);

                sw.Write(",");
            }
            sw.Write(sw.NewLine);
        }
        else
        {

            List<string> lstColumns = new List<string>();

            lstColumns = GetListColumnsByName();
            foreach (string strC in lstColumns)
            {
                sw.Write(strC);

                sw.Write(",");
            }
            sw.Write(sw.NewLine);
        }



        sw.Close();

        Response.Output.Write(sw.ToString());
        Response.Flush();
        Response.End();


    }

    protected DataTable GetPositionalColumns()
    {
        DataTable dtColumns = new DataTable();
        DataTable dt;
        dt = GetPositionDataTable();
        if (dt.Rows.Count > 0)
        {
            string strTempPoI = dt.Rows[dt.Rows.Count - 1]["PositionOnImport"].ToString();
            int iMaxPosition = 0;
            int iMaxBeforeComma = int.Parse(Common.BeforeComma(strTempPoI.Trim()));
            int iMaxAfterComma = 0;
            foreach (DataRow dr in dt.Rows)
            {
                int iTempMax = 0;
                if (dr["PositionOnImport"].ToString().IndexOf(",") > -1)
                {
                    iTempMax = int.Parse(Common.AferComma(dr["PositionOnImport"].ToString()).Trim());
                    if (iTempMax > iMaxAfterComma)
                    {
                        iMaxAfterComma = iTempMax;
                    }
                }
            }

            iMaxPosition = iMaxBeforeComma;
            if (iMaxAfterComma > iMaxBeforeComma)
                iMaxPosition = iMaxAfterComma;

            for (int i = 1; i <= iMaxPosition; i++)
            {
                dtColumns.Columns.Add("NoImport" + i.ToString());
            }


            foreach (DataRow dr in dt.Rows)
            {
                if (dr["PositionOnImport"].ToString().IndexOf(",") > -1)
                {
                    dtColumns.Columns[int.Parse(Common.AferComma(dr["PositionOnImport"].ToString())) - 1].ColumnName = dr["DisplayName"].ToString() + "_2ndPart";
                }
            }
            foreach (DataRow dr in dt.Rows)
            {
                try
                {
                    dtColumns.Columns[int.Parse(Common.BeforeComma(dr["PositionOnImport"].ToString())) - 1].ColumnName = dr["DisplayName"].ToString();
                }
                catch
                {
                    //
                }

            }

            dtColumns.AcceptChanges();
            dtColumns.Rows.Add(dtColumns.NewRow());
            dtColumns.AcceptChanges();
        }
        return dtColumns;
    }


    protected void GetImportTemplate()
    {
        int? iImportTemplateID = null;
        if (ddlTemplate.SelectedValue != "")
        {
            iImportTemplateID = int.Parse(ddlTemplate.SelectedValue);
            _theImportTemplate = ImportManager.dbg_ImportTemplate_Detail((int)iImportTemplateID);
        }
    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {


        lblMsg.Text = "";


        Guid guidNew = Guid.NewGuid();

        string strFileExtension = "";

        switch (fuRecordFile.FileName.Substring(fuRecordFile.FileName.LastIndexOf('.') + 1).ToLower())
        {

            case "txt":
                strFileExtension = ".txt";
                break;
            case "dbf":
                strFileExtension = ".dbf";
                break;
            case "csv":
                strFileExtension = ".csv";
                break;
            case "xls":
                strFileExtension = ".xls";
                break;
            case "xlsx":
                strFileExtension = ".xlsx";
                break;
            case "xml":
                strFileExtension = ".xml";
                break;
        }







        int? iImportTemplateID = null;
        ImportTemplate theImportTemplate = null;
        if (ddlTemplate.SelectedValue != "")
        {
            iImportTemplateID = int.Parse(ddlTemplate.SelectedValue);
            theImportTemplate = ImportManager.dbg_ImportTemplate_Detail((int)iImportTemplateID);
        }



        string strFileUniqueName;
        strFileUniqueName = guidNew.ToString() + strFileExtension;
        if (fuRecordFile.HasFile)
        {
            try
            {

                if (strFileExtension == "")
                {
                    lblMsg.Text = "Please select a .csv/.xls/.xlsx filem or .txt file or .dbf file";
                    return;
                }

                //fuRecordFile.SaveAs(Server.MapPath("../../UserFiles/AppFiles") + "\\" + strFileUniqueName);
                //fuRecordFile.SaveAs(_strFilesPhisicalPath + "\\UserFiles\\AppFiles" + "\\" + strFileUniqueName);
                //RP Modified Ticket 47142 - Added extra saving for Excel files. Causes error if we save the file only in the directory 
                String fName = _strFilesPhisicalPath + "\\UserFiles\\AppFiles" + "\\" + strFileUniqueName;
                fuRecordFile.SaveAs(fName);
                //if (strFileExtension == ".xls" || strFileExtension == ".xlsx")
                //{
                //    Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
                //    Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Open(fName);
                //    wb.Save();
                //    wb.Close(false);
                //    //System.Runtime.InteropServices.Marshal.ReleaseComObject(wb);
                //    app.Quit();
                //    //System.Runtime.InteropServices.Marshal.ReleaseComObject(app);

                //}
                //End Modification
            }
            catch (Exception ex)
            {
                lblMsg.Text = "ERROR: " + ex.Message.ToString();
                return;
            }
        }
        else
        {
            lblMsg.Text = "You have not specified a file.";
            return;
        }

        //check if it has multiple sheets
        string strSelectedSheet = "";

        if (strFileExtension == ".xls" || strFileExtension == ".xlsx")
        {

            List<string> lstSheets = OfficeManager.GetExcelSheetNames(_strFilesPhisicalPath + "\\UserFiles\\AppFiles", strFileUniqueName);

            if (lstSheets.Count > 1)
            {
                int iSCID = GetFileInforSC(guidNew, strFileExtension);
                if (Request.QueryString["SearchCriteriaID"] != null)
                {
                    Response.Redirect("~/Pages/Record/MultipleSheetsUpload.aspx?TableID=" + Request.QueryString["TableID"] + "&SearchCriteriaID=" + Request.QueryString["SearchCriteriaID"].ToString() + "&FileInfo=" + Cryptography.Encrypt(iSCID.ToString()), false);
                }
                else
                {
                    Response.Redirect("~/Pages/Record/MultipleSheetsUpload.aspx?TableID=" + Request.QueryString["TableID"] + "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&FileInfo=" + Cryptography.Encrypt(iSCID.ToString()), false);
                }
                return;
            }
            else
            {
                //Single

            }
        }

        /////

        string strMsg = "";
        int iBatchID = -1;

        try
        {

            if ((theImportTemplate != null) && !String.IsNullOrEmpty(theImportTemplate.SPProcessFile))
            {
                if (theImportTemplate.FileFormat.ToLower() == "other")
                {
                    //JA 18 JAN 2017
                    UploadManager.UploadCSV(_ObjUser.UserID, _theTable, txtBatchDescription.Text,
                            fuRecordFile.FileName, guidNew, _strFilesPhisicalPath + "\\UserFiles\\AppFiles",
                           out strMsg, out iBatchID, strFileExtension, strSelectedSheet,
                           int.Parse(Session["AccountID"].ToString()), null, iImportTemplateID, null, null);
                }
                else
                {
                    //JA 18 JAN 2017
                    UploadManager.UploadCSVRaw(_ObjUser.UserID, _theTable, txtBatchDescription.Text,
                        fuRecordFile.FileName, guidNew, _strFilesPhisicalPath + "\\UserFiles\\AppFiles",
                       out strMsg, out iBatchID, strFileExtension, strSelectedSheet,
                       int.Parse(Session["AccountID"].ToString()), null);

                    Byte[] parameters = new Byte[16];
                    Byte[] batchIDPart = BitConverter.GetBytes((long)iBatchID);
                    for (int j = 0; j < 8; j++)
                    {
                        parameters[j] = batchIDPart[j];
                        parameters[j + 8] = 0;
                    }

                    UploadManager.UploadCSV(_ObjUser.UserID, _theTable, txtBatchDescription.Text,
                            fuRecordFile.FileName, new Guid(parameters), null,
                           out strMsg, out iBatchID, "preProcessorSQL", null,
                           int.Parse(Session["AccountID"].ToString()), null, iImportTemplateID, null, null);
                }
            }
            else
            {
                UploadManager.UploadCSV(_ObjUser.UserID, _theTable, txtBatchDescription.Text,
                        fuRecordFile.FileName, guidNew, _strFilesPhisicalPath + "\\UserFiles\\AppFiles",
                       out strMsg, out iBatchID, strFileExtension, strSelectedSheet,
                       int.Parse(Session["AccountID"].ToString()), null, iImportTemplateID, null, null);
            }

            if (strMsg == "")
            {

            }
            else
            {
                lblMsg.Text = strMsg;
                //connection.Close();
                //connection.Dispose();
                return;

            }

            string strVirtualUsersTableID = SystemData.SystemOption_ValueByKey_Account("VirtualUsersTableID", int.Parse(Session["AccountID"].ToString()), _theTable.TableID);

            if (iBatchID != -1 && strVirtualUsersTableID != "" && strVirtualUsersTableID == _theTable.TableID.ToString())
            {
                SecurityManager.dbg_User_Validation(iBatchID);
            }

            string strExtra = "";

            if (Request.QueryString["SearchCriteriaID"] != null)
            {
                Response.Redirect("~/Pages/Record/UploadValidation.aspx?TableID=" + Request.QueryString["TableID"] + "&BatchID=" + Cryptography.Encrypt(iBatchID.ToString()) + "&SearchCriteriaID=" + Request.QueryString["SearchCriteriaID"].ToString() + strExtra, false);
            }
            else
            {
                Response.Redirect("~/Pages/Record/UploadValidation.aspx?TableID=" + Request.QueryString["TableID"] + "&BatchID=" + Cryptography.Encrypt(iBatchID.ToString()) + "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + strExtra, false);
            }
        }
        catch (Exception ex)
        {

            lblMsg.Text = strMsg;

            ErrorLog theErrorLog = new ErrorLog(null, "Record Upload", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            //throw;
        }


    }

    protected void ddlTemplate_PreRender(object sender, EventArgs e)
    {
        DataTable dtTemp = Common.DataTableFromText("SELECT ImportTemplateID,ImportTemplateName,HelpText FROM ImportTemplate WHERE TableID=" + _qsTableID + " ORDER BY ImportTemplateName");

        foreach (DataRow dr in dtTemp.Rows)
        {
            if (dr["HelpText"] != DBNull.Value && dr["HelpText"].ToString() != "")
            {
                foreach (ListItem liItem in ddlTemplate.Items)
                {
                    if (liItem.Value == dr["ImportTemplateID"].ToString())
                    {
                        liItem.Attributes.Add("title", dr["HelpText"].ToString());
                        break;
                    }
                }
            }
        }
    }

    protected void PopulateImportTemplate(int iTableID)
    {
        ddlTemplate.Items.Clear();

        DataTable dtTemp = Common.DataTableFromText("SELECT  ImportTemplateID,ImportTemplateName,HelpText  FROM ImportTemplate WHERE TableID="
            + iTableID.ToString() + " ORDER BY ImportTemplateName");

        if (dtTemp != null)
        {
            foreach (DataRow dr in dtTemp.Rows)
            {
                ListItem liTemp = new ListItem(dr["ImportTemplateName"].ToString(), dr["ImportTemplateID"].ToString());

                //JA 18 JAN 2017
                ddlTemplate.Items.Add(liTemp);
            }

            //Red added this to get the default template
            if (_theTable.DefaultImportTemplateID != null &&
                ddlTemplate.Items.FindByValue(_theTable.DefaultImportTemplateID.ToString()) != null)
            {
                ddlTemplate.SelectedValue = _theTable.DefaultImportTemplateID.ToString();
            }
        }
        else
        {
            if (!IsPostBack)
            {
                ImportManager.CreateDefaultImportTemplate(iTableID, "");
                PopulateImportTemplate(iTableID);
            }
        }

        ddlTemplate_SelectedIndexChanged(null, null);
    }

    protected void BindGrids()
    {
        int? iImportTemplateID = null;
        if (ddlTemplate.SelectedValue != "")
            iImportTemplateID = int.Parse(ddlTemplate.SelectedValue);

        if (!iImportTemplateID.HasValue)
            return;

        ImportTemplate theImprtTemplate = ImportManager.dbg_ImportTemplate_Detail((int)iImportTemplateID);

        if (theImprtTemplate == null)
            return;

        int iColumnCount = 0;
        int iTemp = 0;
        if (theImprtTemplate.IsImportPositional != null && (bool)theImprtTemplate.IsImportPositional == true)
        {

            BindPositionGrid();
        }
        else
        {
            BindTheColumnGrid();
        }
    }
    protected void BindTheColumnGrid()
    {
        divColumnPosition.Visible = false;
        divColumnName.Visible = true;
        lblImortType.Text = "Following columns will be imported from the file. Column names must match.";

        int iColumnCount = 0;
        int iTemp = 0;
        int iInvalid = 0;

        int? iImportTemplateID = null;

        if (ddlTemplate.SelectedValue != "")
            iImportTemplateID = int.Parse(ddlTemplate.SelectedValue);
        lblImportTemplateFile.Text = "";
        if (iImportTemplateID != null)
        {
            ImportTemplate theImprtTemplate = ImportManager.dbg_ImportTemplate_Detail((int)iImportTemplateID);
            if (theImprtTemplate != null && theImprtTemplate.TemplateUniqueFileName != "")
            {
                string strFilePath = Cryptography.Encrypt(_strFilesLocation + "/UserFiles/AppFiles/" + theImprtTemplate.TemplateUniqueFileName);
                string strFileName = theImprtTemplate.TemplateUniqueFileName.Substring(37);

                lblImportTemplateFile.Text = "<a target='_blank' href='" + Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Security/Filedownload.aspx?FilePath="
                       + strFilePath + "&FileName=" + Cryptography.Encrypt(strFileName) + "'>" +
                         strFileName + "</a>";
            }
        }

        gvTheGrid.Attributes.Add("bordercolor", "D1E9FF");

        DataTable dtNew = new DataTable();

        List<Tuple<string, string>> lstColumns = new List<Tuple<string, string>>();

        lstColumns = GetColumnInfo();
        foreach (Tuple<string, string> tplColumn in lstColumns)
        {
            dtNew.Columns.Add(tplColumn.Item1);
            if (tplColumn.Item2 == "image")
            {
                iInvalid++;
            }
        }
        dtNew.AcceptChanges();
        dtNew.Rows.Add(dtNew.NewRow());
        iColumnCount = dtNew.Columns.Count;
        dtNew.AcceptChanges();
        gvTheGrid.DataSource = dtNew;
        gvTheGrid.VirtualItemCount = 1;
        gvTheGrid.DataBind();

        if (iColumnCount == 0)
        {
            lnkNext.Visible = false;
            lblMsg.Text = "Data cannot be imported because no fields have been selected for import";
        }
        else if (iInvalid != 0)
        {
            lnkNext.Visible = true;
            lblMsg.Text = "Warning! Invalid field type in template.";
        }
        //JA 26 JAN 2017
        else
        {
            lnkNext.Visible = true;
            lblMsg.Text = string.Empty;
        }
    }
    protected void BindPositionGrid()
    {
        int iColumnCount = 0;
        int iTemp = 0;
        divColumnName.Visible = false;
        divColumnPosition.Visible = true;
        lblImortType.Text = "Following columns will be imported from the file. Column positions must match.";

        DataTable dt = GetPositionDataTable();

        iColumnCount = dt.Rows.Count;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string strTempPoI = dt.Rows[i]["PositionOnImport"].ToString();
        }

        gvPosition.DataSource = dt;//dtNew
        gvPosition.VirtualItemCount = dt.Rows.Count;
        gvPosition.DataBind();

        if (iColumnCount == 0)
        {
            lnkNext.Visible = false;
            lblMsg.Text = "Data cannot be imported because no fields have been selected for import";
        }
        else
        {
            lblMsg.Text = string.Empty;
        }
    }
    protected void ddlTemplate_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrids();
    }

    protected int GetFileInforSC(Guid guidNew, string strFileExtension)
    {

        //SearchCriteria 
        try
        {
            string xml = null;
            xml = @"<root>" +
                   " <txtBatchDescription>" + HttpUtility.HtmlEncode(txtBatchDescription.Text) + "</txtBatchDescription>" +
                   " <FileName>" + HttpUtility.HtmlEncode(fuRecordFile.FileName) + "</FileName>" +
                   " <guidNew>" + HttpUtility.HtmlEncode(guidNew.ToString()) + "</guidNew>" +
                    " <ddlTemplate>" + HttpUtility.HtmlEncode(ddlTemplate.SelectedValue) + "</ddlTemplate>" +
                  "</root>";

            SearchCriteria theSearchCriteria = new SearchCriteria(null, xml);
            return SystemData.SearchCriteria_Insert(theSearchCriteria);
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
        return -1;
        //End Searchcriteria

    }






}





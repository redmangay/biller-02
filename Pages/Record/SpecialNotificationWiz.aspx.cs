﻿using System;
using System.Web.UI;

public partial class Pages_Record_SpecialNotificationWiz : SecurePage
{

    Table _theTable;


    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (hfStep.Value == "conditions")
        {
            divSpecialNotification.Visible = false;
            divUsers.Visible = false;
            divCondtion.Visible = true;
            lblSteps.Text = "Step 3 of Step 3: Send condtions. When should we send this notification?";
        }
        else if (hfStep.Value == "users")
        {
            divSpecialNotification.Visible = false;
            divCondtion.Visible = false;
            divUsers.Visible = true;
            lblSteps.Text = "Step 2 of Step 3: Specify recipients";
        }
        else
        {
            divSpecialNotification.Visible = true;
            divUsers.Visible = false;
            divCondtion.Visible = false;
            lblSteps.Text = "Step 1 of Step 3: Define message. Note that for SMS only the Text will be sent";
        }
    }

   
}
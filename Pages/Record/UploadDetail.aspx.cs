﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class Pages_Record_UploadDetail : SecurePage
{

    string _strActionMode = "view";
    int? _iUploadID;
    string _qsMode = "";
    string _qsUploadID = "";


    protected void PopulateTerminology()
    {
        stgTableCap.InnerText = stgTableCap.InnerText.Replace("Table", SecurityManager.etsTerminology(Request.Path.Substring(Request.Path.LastIndexOf("/") + 1), "Table", "Table"));

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string type = Cryptography.Decrypt(Request.QueryString["type"]);
            ShowHideRows(type);

            PopulateTableDDL();
            // For Category 
            //Red Ticket 2011
            PopulateDocumentType();
            //PopulateLocationDDL();
            if (!Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
            { Response.Redirect("~/Default.aspx", false); }

            if (Request.QueryString["SearchCriteria"] != null)
            {

                hlBack.NavigateUrl = Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/Upload.aspx?SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString();
            }
            else
            {

                Response.Redirect(Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/Upload.aspx", false);//i think no need
            }


        }
        if (Request.QueryString["mode"] == null)
        {
            Server.Transfer("~/Default.aspx");
        }
        else
        {
            _qsMode = Cryptography.Decrypt(Request.QueryString["mode"]);

            if (_qsMode == "add" ||
                _qsMode == "view" ||
                _qsMode == "edit")
            {
                _strActionMode = _qsMode;


                if (Request.QueryString["UploadID"] != null)
                {

                    _qsUploadID = Cryptography.Decrypt(Request.QueryString["UploadID"]);

                    _iUploadID = int.Parse(_qsUploadID);
                }
            }
            else
            {
                Server.Transfer("~/Default.aspx");
            }
        }
       
        


        // checking permission


        switch (_strActionMode.ToLower())
        {
            case "add":
                lblTitle.Text = "Add Auto Upload";
                if (!IsPostBack)
                {
                    string s = Cryptography.Decrypt(Request.QueryString["type"]);
                    if (!String.IsNullOrEmpty(s))
                        ddlType.SelectedValue = s;
                }
                break;

            case "view":
                lblTitle.Text = "View Auto Upload";
                PopulateTheRecord();         
                EnableTheRecordControls(false);
                divSave.Visible = false;
                break;

            case "edit":
                lblTitle.Text = "Edit Auto Upload";
                if (!IsPostBack)
                {
                    PopulateTheRecord();
                }
                break;

            default:
                //?
                break;
        }

        Title = lblTitle.Text;

        if (!IsPostBack)
        {
            PopulateTerminology();
        }
    }

    protected void PopulateDocumentType()
    {
        ddlDocumentType.Items.Clear();
        ddlDocumentType.DataSource = Common.DataTableFromText("SELECT DocumentTypeID,DocumentTypeName FROM DocumentType WHERE DocumentTypeName <>'Custom Reports' AND AccountID=" + Session["AccountID"].ToString());
        ddlDocumentType.DataBind();
        System.Web.UI.WebControls.ListItem liSelect = new System.Web.UI.WebControls.ListItem("--Please Select--", "");
        ddlDocumentType.Items.Insert(0, liSelect);
    }

    protected void PopulateTableDDL()
    {
        int iTN = 0;
        ddlTable.DataSource = RecordManager.ets_Table_Select(null,
                null,
                null,
                int.Parse(Session["AccountID"].ToString()),
                null, null, true,
                "st.TableName", "ASC",
                null, null, ref  iTN, Session["STs"].ToString());

        ddlTable.DataBind();
        //if (iTN == 0)
        //{
        System.Web.UI.WebControls.ListItem liAll = new System.Web.UI.WebControls.ListItem("--Please Select--", "");
        ddlTable.Items.Insert(0, liAll);
        //}


    }

    protected void ddlTable_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateTemplateDDL(true);
    }


    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ShowHideRows(ddlType.SelectedValue);
    }


    protected void PopulateTemplateDDL(bool setDefaultTemplate)
    {
        ddlTemplate.Items.Clear();

        int iTableID = -1;
        if (int.TryParse(ddlTable.SelectedValue, out iTableID))
        {
            DataTable dtTemp = Common.DataTableFromText("SELECT  ImportTemplateID,ImportTemplateName,HelpText  FROM ImportTemplate WHERE TableID="
                + iTableID.ToString() + " ORDER BY ImportTemplateName");

            foreach (DataRow dr in dtTemp.Rows)
            {
                ListItem liTemp = new ListItem(dr["ImportTemplateName"].ToString(), dr["ImportTemplateID"].ToString());
                ddlTemplate.Items.Add(liTemp);
            }
        }

        ListItem liSelect = new ListItem("--Please select--", "");
        ddlTemplate.Items.Insert(0, liSelect);

        if (setDefaultTemplate)
        {
            int? iDT = ImportManager.GetDefaultImportTemplate((int)iTableID);
            if (iDT != null)
            {
                if (ddlTemplate.Items.FindByValue(iDT.ToString()) != null)
                {
                    ddlTemplate.SelectedValue = iDT.ToString();
                }
            }
        }
    }

    //protected void PopulateLocationDDL()
    //{
    //    int iTN = 0;
    //    if (ddlTable.SelectedValue != "")
    //    {
    //        ddlLocation.Items.Clear();
    //        ddlLocation.DataSource = SiteManager.ets_Location_Select(null, int.Parse(ddlTable.SelectedValue), null,
    //                string.Empty, string.Empty, true, null, null, null, null,
    //                int.Parse(Session["AccountID"].ToString()),
    //                "LocationName", "ASC",
    //                null, null, ref  iTN, "");

    //        ddlLocation.DataBind();
    //        System.Web.UI.WebControls.ListItem liSelect = new System.Web.UI.WebControls.ListItem("--All--", "");
    //        ddlLocation.Items.Insert(0, liSelect);
    //    }
    //    else
    //    {
    //        ddlLocation.Items.Clear();

    //        System.Web.UI.WebControls.ListItem liSelect = new System.Web.UI.WebControls.ListItem("--All--", "");
    //        ddlLocation.Items.Insert(0, liSelect);
    //    }

    //}

     
    protected void PopulateTheRecord()
    {
        try
        {
            if (_iUploadID.HasValue)
            {
                Upload theUpload = UploadManager.ets_Upload_Detail(_iUploadID.Value);
                txtUploadName.Text = theUpload.UploadName;
                ddlType.SelectedValue = theUpload.UploadType;
                ShowHideRows(theUpload.UploadType);
                txtFilename.Text = theUpload.Filename;
                ddlTable.SelectedValue = theUpload.TableID.ToString();
                PopulateTemplateDDL(false);
                ddlTemplate.SelectedValue = theUpload.TemplateID.ToString();
                chkUseMapping.Checked = theUpload.UseMapping;
                if (theUpload.TemplateID.HasValue)
                    ddlTemplate.SelectedValue = theUpload.TemplateID.Value.ToString();
                if (theUpload.DocumentTypeID != null)
                {
                    ddlDocumentType.SelectedValue = theUpload.DocumentTypeID.ToString();
                }
                chkSaveAttachment.Checked = theUpload.SaveAttachments;
                txtFilenameCriteria.Text = theUpload.Criteria ?? "";

                string type = theUpload.UploadType;
                switch (type)
                {
                    case "Email":
                        txtEmailFrom.Text = theUpload.EmailFrom;
                        break;
                    case "FTP":
                        txtLocation.Text = theUpload.EmailFrom;
                        break;
                    default:
                        txtEmailFrom.Text = theUpload.EmailFrom;
                        break;
                }
                if (_strActionMode == "edit")
                {
                    ViewState["theUpload"] = theUpload;
                }

                if (_strActionMode == "view")
                {
                    divEdit.Visible = true;
                    hlEditLink.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                                Request.ApplicationPath +
                                                "/Pages/Record/UploadDetail.aspx?mode=" +
                                                Cryptography.Encrypt("edit") +
                                                "&Type=" + Request.QueryString["Type"].ToString() +
                                                "&SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString() +
                                                "&UploadID=" + Cryptography.Encrypt(_iUploadID.Value.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Upload Detail", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }
    

    protected void EnableTheRecordControls(bool p_bEnable)
    {        
        txtUploadName.Enabled = p_bEnable;
        ddlType.Enabled = p_bEnable;
        txtEmailFrom.Enabled = p_bEnable;
        txtLocation.Enabled = p_bEnable;
        txtFilename.Enabled = p_bEnable;
        ddlTable.Enabled = p_bEnable;
        ddlTemplate.Enabled = p_bEnable;
        chkUseMapping.Enabled = p_bEnable;
        //ddlLocation.Enabled = p_bEnable;
        chkSaveAttachment.Enabled = p_bEnable;
        txtFilenameCriteria.Enabled = p_bEnable;
        ddlDocumentType.Enabled = p_bEnable;
    }

    protected bool IsUserInputOK()
    {
        //this is the final server side vaidation before database action
        return true;
    }


    //protected void cmdSave_Click(object sender, ImageClickEventArgs e)
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtFilenameCriteria.Text == "" && chkSaveAttachment.Checked)
            {
                lblMsgError.Visible = true;
                lblMsgError.Text = "Criteria - Required";
                txtFilenameCriteria.Focus();
            }

            else
            {
                if (IsUserInputOK())
                {
                    int intValue = 0;
                    switch (_strActionMode.ToLower())
                    {
                        case "add":

                            //Menu newMenu = new Menu(null, txtMenu.Text, int.Parse(ddlAccount.SelectedValue), chkShowOnMenu.Checked, "");
                            //SecurityManager.test_TestTable_Insert(newMenu);

                            int? templateID = null;
                            if (int.TryParse(ddlTemplate.SelectedValue, out intValue))
                            {
                                templateID = intValue;
                            }

                            string strDocumentTypeID = ddlDocumentType.SelectedValue;
                            string strUploadCriteria = txtFilenameCriteria.Text;

                            string emailFromOrLocation = String.Empty;
                            switch (ddlType.SelectedValue)
                            {
                                case "Email":
                                    emailFromOrLocation = txtEmailFrom.Text;
                                    break;
                                case "FTP":
                                    emailFromOrLocation = txtLocation.Text;
                                    break;
                            }
                            Upload newUpload = new Upload(null, int.Parse(ddlTable.SelectedValue),
                                txtUploadName.Text, emailFromOrLocation, txtFilename.Text, chkUseMapping.Checked, templateID,
                                chkSaveAttachment.Checked, strUploadCriteria == "" ? null : strUploadCriteria,
                                strDocumentTypeID == "" ? null : (int?)int.Parse(strDocumentTypeID),
                                ddlType.SelectedValue);
                            UploadManager.ets_Upload_Insert(newUpload);
                            break;

                        case "view":
                            break;

                        case "edit":
                            Upload editUpload = (Upload)ViewState["theUpload"];
                            editUpload.UploadName = txtUploadName.Text;
                            editUpload.UploadType = ddlType.SelectedValue;
                            editUpload.Filename = txtFilename.Text;
                            editUpload.TableID = int.Parse(ddlTable.SelectedValue);
                            editUpload.TemplateID = int.Parse(ddlTemplate.SelectedValue);
                            editUpload.UseMapping = chkUseMapping.Checked;
                            editUpload.SaveAttachments = chkSaveAttachment.Checked;
                            editUpload.DocumentTypeID = ddlDocumentType.SelectedValue == "" ? null : (int?)int.Parse(ddlDocumentType.SelectedValue);
                            editUpload.Criteria = txtFilenameCriteria.Text == "" ? null : txtFilenameCriteria.Text;
                            if (int.TryParse(ddlTemplate.SelectedValue, out intValue))
                                editUpload.TemplateID = intValue;
                            else
                                editUpload.TemplateID = null;
                            switch (ddlType.SelectedValue)
                            {
                                case "Email":
                                    editUpload.EmailFrom = txtEmailFrom.Text;
                                    //if (ddlLocation.SelectedValue == "")
                                    //{
                                    //    editUpload.LocationID = null;
                                    //}
                                    //else
                                    //{
                                    //    editUpload.LocationID = int.Parse(ddlLocation.SelectedValue);
                                    //}
                                    break;
                                case "FTP":
                                    editUpload.EmailFrom = txtLocation.Text;
                                    break;
                            }
                            UploadManager.ets_Upload_Update(editUpload);
                            break;

                        default:
                            //?
                            break;
                    }
                }
                else
                {
                    //user input is not ok
                }
                Response.Redirect(hlBack.NavigateUrl, false);
            }
        }
        catch (Exception ex)
        {

            if (ex.Message.IndexOf("UQ_EmailFrom_FileName") > -1)
            {
                switch (ddlType.SelectedValue)
                {
                    case "Email":
                        lblMsg.Text = "The combination of email & file name is already used, please use another email or file name.";
                        break;
                    case "FTP":
                        lblMsg.Text = "The combination of location & file name is already used, please use another location or file name.";
                        break;
                }
            }
            else
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Upload Detail", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
                lblMsg.Text = ex.Message;
            }
        }
    }

    private void ShowHideRows(string type)
    {
        trEmailFrom1.Visible = false;
        trEmailFrom2.Visible = false;
        trSendEmail.Visible = false;
        trFTP1.Visible = false;
        trFTP2.Visible = false;
        trFTPServer.Visible = false;

        if (String.IsNullOrEmpty(type))
            type = "Email";

        switch (type)
        {
            case "Email":
                //lblPopEmailFrom.Text = SystemData.SystemOption_ValueByKey_Account("PopEmailFrom", null, null);
                //KG 11/7/17 Update to reflect IMAP Settings
                lblPopEmailFrom.Text = SystemData.SystemOption_ValueByKey_Account("ImapUserName", null, null);
                trEmailFrom1.Visible = true;
                trEmailFrom2.Visible = true;
                trSendEmail.Visible = true;
                break;
            case "FTP":
                lblFTPServer.Text = SystemData.SystemOption_ValueByKey_Account("FTPServerIP", null, null);
                trFTP1.Visible = true;
                trFTP2.Visible = true;
                trFTPServer.Visible = true;
                break;
        }
    }
}

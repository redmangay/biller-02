﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using AjaxControlToolkit;
using System.Web.UI.HtmlControls;
using DocGen.DAL;
using System.Xml;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

public partial class Pages_Record_ColumnHistory : SecurePage
{

    Table _theTable;
    //int? _iTableTabID = null;
    Common_Pager _gvCL_Pager;
    Common_Pager _gvPager;
    int _iCLColumnCount = 0;
    int _iColumnID;
    //bool _bOK = false;
    int _iCLStartIndex = 0;
    int _iCLMaxRows = 0;
    int _iCLTN = 0;
    Column theColumn;

          
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["ColumnID"] != null)
        {
            string _qsColumnID = Cryptography.Decrypt(Request.QueryString["ColumnID"].ToString());
            theColumn = RecordManager.ets_Column_Details(int.Parse(_qsColumnID));
            _theTable = RecordManager.ets_Table_Details((int)theColumn.TableID);
        }
        if (Request.QueryString["TableID"] != null)
        {
            string _qTableID = Request.QueryString["TableID"].ToString();
            _theTable = RecordManager.ets_Table_Details(int.Parse(_qTableID));
        }

        if (!IsPostBack)
        {
            if (Session["GridPageSize"] != null && Session["GridPageSize"].ToString() != "")
            {
                gvChangedLog.PageSize = int.Parse(Session["GridPageSize"].ToString());
            }
            BindTheChangedLogGrid(0, gvChangedLog.PageSize);
        }

        GridViewRow gvrCL = gvChangedLog.TopPagerRow;
        if (gvrCL != null)
            _gvCL_Pager = (Common_Pager)gvrCL.FindControl("CL_Pager");

    }

    //Page_load eof
    protected void BindTheChangedLogGrid(int iStartIndex, int iMaxRows)
    {
        try
        {
            int iTN = 0;
            ViewState[gvChangedLog.ID + "PageIndex"] = (iStartIndex / gvChangedLog.PageSize) + 1;

            gvChangedLog.DataSource = RecordManager.Audit_Summary(
                    (int)_theTable.AccountID,
                    "column",
                    null,
                    null,
                   _theTable.TableID, 
                   null,
                   iStartIndex, 
                   iMaxRows,
                   null,
                   ref iTN);

            gvChangedLog.VirtualItemCount = iTN;

            _iCLColumnCount = 4;

            _iCLStartIndex = iStartIndex;
            _iCLMaxRows = iMaxRows;
            _iCLTN = iTN;


            gvChangedLog.DataBind();
            if (gvChangedLog.TopPagerRow != null)
                gvChangedLog.TopPagerRow.Visible = true;
            GridViewRow gvr = gvChangedLog.TopPagerRow;
            if (gvr != null)
            {
                _gvCL_Pager = (Common_Pager)gvr.FindControl("CL_Pager");

                if (ViewState[gvChangedLog.ID + "PageIndex"] != null)
                    _gvCL_Pager.PageIndex = int.Parse(ViewState[gvChangedLog.ID + "PageIndex"].ToString());

                _gvCL_Pager.PageSize = gvChangedLog.PageSize;
                _gvCL_Pager.TotalRows = iTN;
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Record Detail Change Log", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
        }
    }


    protected void CL_Pager_BindTheGridAgain(object sender, EventArgs e)
    {
        BindTheChangedLogGrid(_gvCL_Pager.StartIndex, _gvCL_Pager.PageSize);
    }

    protected void CL_Pager_BindTheGridToExport(object sender, EventArgs e)
    {
        _gvCL_Pager.ExportFileName = "Record Field Change Log ";
        BindTheChangedLogGrid(0, _gvCL_Pager.TotalRows);
    }
    protected void CL_Pager_OnApplyFilter(object sender, EventArgs e)
    {
        BindTheChangedLogGrid(0, gvChangedLog.PageSize);
    }

    protected void CL_Pager_OnExportForCSV(object sender, EventArgs e)
    {

        gvChangedLog.AllowPaging = false;
        BindTheChangedLogGrid(0, _gvCL_Pager.TotalRows);

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition",
        "attachment;filename=SensorChangedLog.csv");
        Response.Charset = "";
        Response.ContentType = "text/csv";

        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        DataTable dtTemp = (DataTable)gvChangedLog.DataSource;

        int iColCount = dtTemp.Columns.Count;
        for (int i = 0; i < iColCount - 1; i++)
        {
            if (string.IsNullOrEmpty(dtTemp.Columns[i].ColumnName))
            {
            }
            else
            {
                sw.Write(dtTemp.Columns[i].ColumnName);
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
        }

        sw.Write(sw.NewLine);

        // Now write all the rows.
        foreach (DataRow dr in dtTemp.Rows)
        {

            for (int i = 0; i < iColCount - 1; i++)
            {
                if (string.IsNullOrEmpty(dtTemp.Columns[i].ColumnName))
                {
                }
                else
                {


                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write("\"" + dr[i].ToString() + "\"");
                    }




                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
            }
            sw.Write(sw.NewLine);
        }
        sw.Close();


        Response.Output.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
       // return;
    }

    protected void CL_Pager_OnExportForWord(object sender, EventArgs e)
    {
        DBGServerControl.dbgGridView _gridView = gvChangedLog;      

        for (int i = 0; i < _gridView.Columns.Count; i++)
        {
            if (string.IsNullOrEmpty(_gridView.Columns[i].HeaderText))
                _gridView.Columns[i].Visible = false;
        }

        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string FileName = _theTable.TableName.ToString() + "_Column_Change_History" + DateTime.Now + ".doc";
        StringWriter strwritter = new StringWriter();
        HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/msword";
        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
        _gridView.GridLines = GridLines.Both;
        _gridView.HeaderStyle.Font.Bold = true;
        _gridView.RenderControl(htmltextwrtter);
        Response.Write(strwritter.ToString());

        /* == Red 09092019: Append cookie */
        HttpCookie cookie = new HttpCookie("ExportReport");
        cookie.Value = "Flag";
        cookie.Expires = DateTime.Now.AddDays(1);
        HttpContext.Current.Response.AppendCookie(cookie);
        /* == End Red == */

        Response.End();


    }

    protected void CL_Pager_OnExportForPDF(object sender, EventArgs e)
    {
        DBGServerControl.dbgGridView _gridView = gvChangedLog;

        for (int i = 0; i < _gridView.Columns.Count; i++)
        {
            if (string.IsNullOrEmpty(_gridView.Columns[i].HeaderText))
                _gridView.Columns[i].Visible = false;
        }

        _gridView.AllowPaging = false;

        string FileName = _theTable.TableName.ToString() + "_Column_Change_History" + DateTime.Now + ".pdf";
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition",
         "attachment;filename="+ FileName);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
     
        _gridView.RenderControl(hw);

        StringReader sr = new StringReader(sw.ToString());
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10f, 10f, 10f, 0f);
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        pdfDoc.Open();
        htmlparser.Parse(sr);
        pdfDoc.Close();

        Response.Write(pdfDoc);

        /* == Red 09092019: Append cookie */
        HttpCookie cookie = new HttpCookie("ExportReport");
        cookie.Value = "Flag";
        cookie.Expires = DateTime.Now.AddDays(1);
        HttpContext.Current.Response.AppendCookie(cookie);
        /* == End Red == */

        Response.End();
    }

    protected void gvChangedLog_PreRender(object sender, EventArgs e)
    {
        GridView grid = (GridView)sender;
        if (grid != null)
        {
            GridViewRow pagerRow = (GridViewRow)grid.TopPagerRow;
            if (pagerRow != null)
            {
                pagerRow.Visible = true;
            }
        }
    }

    protected void gvChangedLog_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //ImageButton lb = (ImageButton)e.Row.FindControl("btnView");                
                HyperLink hlView = (HyperLink)e.Row.FindControl("hlView");                
                DateTime dtUpdateDate = Convert.ToDateTime(DataBinder.Eval(e.Row.DataItem, "DateAdded"));
                //lb.Attributes.Add("onclick", "javascript:OpenAuditDetail('" + dtUpdateDate.ToString("yyyy-MM-dd HH:mm:ss.fff") + "','" + _iRecordID.ToString() + "');return false;");
                hlView.NavigateUrl = "RecordColumnAudit.aspx?UpdatedDate=" + Server.UrlEncode(dtUpdateDate.ToString("yyyy-MM-dd HH:mm:ss.fff")) + "&ColumnID=" + _iColumnID.ToString();


                string strColumnList = DataBinder.Eval(e.Row.DataItem, "ColumnList").ToString();
                string[] arrColumnList = strColumnList.Split(',');

                Label lblColumnList = (Label)e.Row.FindControl("lblColumnList");

                if (arrColumnList.Length > 3)
                {
                    //get first 3 names
                    int i = 0;
                    string strThreeeColumns = "";
                    foreach (string aColumn in arrColumnList)
                    {
                        if (i == 0)
                            strThreeeColumns = aColumn;
                        if (i == 1)
                            strThreeeColumns = strThreeeColumns + "," + aColumn;

                        if (i == 2)
                            strThreeeColumns = strThreeeColumns + " and " + aColumn;

                        i = i + 1;

                        if (i == 3)
                            break;
                    }

                    lblColumnList.Text = arrColumnList.Length + " fields including " + strThreeeColumns;

                }
                else
                {
                    lblColumnList.Text = strColumnList;
                }
            }

        }
        catch (Exception ex)
        {
           // lblMsg.Text = ex.Message;
        }
    }

}

<%@ Page Language="C#" MasterPageFile="~/Home/Popup.master" CodeFile="SpecialNotificationWiz.aspx.cs"
    Inherits="Pages_Record_SpecialNotificationWiz" ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>
<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Pages/UserControl/ConditionOptions.ascx" TagName="ConditionOption" TagPrefix="asp" %>
<%@ Register Src="~/Pages/UserControl/SpecialNotification.ascx" TagName="SpecialNotification" TagPrefix="asp" %>
<%@ Register Src="~/Pages/UserControl/SpecialNotificationUsers.ascx" TagName="SpecialNotificationUsers" TagPrefix="asp" %>



<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <asp:Literal ID="ltTextStyles" runat="server"></asp:Literal>
    <style type="text/css">
    </style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">   
     
<style type="text/css">
.button {
    background-color: #008CBA;
    border: none;
    color: white;
    padding: 10px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-weight: bold;
    font-size: 12px;
    margin: 2px 1px;
    cursor: pointer;
    border-radius: 4px;
}
  .footer {
   position:absolute;
   bottom:0;
   width:100%;
   height:60px;

}


     </style>

     <script type="text/javascript">



         $(document).ready(function () {
             ShowHideNext();

             function ShowHideNext() {
                 var step = $('#hfStep').val();
                 if (step == "conditions") {
                     $('#hlNext').val("Finish");
                 }
                 else {
                     $('#hlNext').val("Next Steps >");
                 }
             }


             //$('#hlNextConditions').click(function () {
             //    $('#hfStep').val("conditions");
             //    document.getElementById('ctl00_HomeContentPlaceHolder_ctl00_lnkSave').click();
               
             //});
             //$('#hlNextFinish').click(function () {
             //    $('#hfStep').val("finish");
             //    parent.$.fancybox.close();
             //});
             //$('#hlNextUser').click(function () {
             //    $('#hfStep').val("finish");
             //    document.getElementById('lnkReload').click();

             //});


             $('#hlNext').click(function () {
                 $(".ajax-indicator-full").fadeIn();
                 var stepx = $('#hfStep').val();
                 if (stepx == "") {
                     $('#hfStep').val("users");
                     document.getElementById('ctl00_HomeContentPlaceHolder_ctl00_lnkSave').click();
                 }
                 if (stepx == "users") {
                     $('#hfStep').val("conditions");
                     document.getElementById('lnkReload').click();
                 }

                 if (stepx == "conditions") {
                     parent.$.fancybox.close();
                 }

             });
         });
     </script>


    <table style="width:100%; text-align:center">
      <tr>
          <td>
               <%--next buttons--%>
                 <div id="divNext" runat="server" style="padding-top:30px; text-align:right; padding-right:100px;">
                      <asp:HiddenField ID="hfStep" runat="server" ClientIDMode="Static" />
                      <input type="button" id="hlNext" class="button" value="Finish"/>                     
                 </div>  
                  <%--Reload the page, display none --%>
                 <div runat="server" id="divSave" style="display:none">
                    <asp:LinkButton runat="server" ID="lnkReload" ClientIDMode="Static" CausesValidation="true">
                        <asp:Image runat="server" ID="Image1" ImageUrl="~/App_Themes/Default/images/Save.png"  ToolTip="Save" />
                    </asp:LinkButton>
                 </div>
                 <%--load the special notification page--%>
                 <div id="divSpecialNotification" clientidmode="Static"  runat="server">          
                    <asp:SpecialNotification runat="server"></asp:SpecialNotification>
                </div>  
                 <%--load the condition page--%>
                <div id="divCondtion" clientidmode="Static" visible="false" runat="server">
                    <asp:ConditionOption runat="server" />
                </div>
                <%--load the users page--%>
                <div id="divUsers" clientidmode="Static" visible="false" runat="server">
                <asp:SpecialNotificationUsers runat="server" />
            </div>
          </td>
      </tr>
        <tr>
            <td style="padding-top:30px;">
                 <%--next steps info--%>            
              <div class="row row-no-padding">
                            <div class="col-md-12">
                                <div id="footer">                       
                                    <div class="copyright-text" id="divSteps" runat="server">
                                        <asp:Label Font-Size="Medium" ID="lblSteps" runat="server" ForeColor="Blue"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
            </td>
        </tr>
    </table>
            
</asp:Content>

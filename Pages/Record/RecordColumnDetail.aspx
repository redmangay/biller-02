﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="RecordColumnDetail.aspx.cs" EnableEventValidation="false"
    Inherits="Pages_Record_ColumnDetail" %>

<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>
<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="editor" Assembly="WYSIWYGEditor" Namespace="InnovaStudio" %>

<%@ Register Src="~/Pages/UserControl/ControlByColumn.ascx" TagName="ControlByColumn" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <script src="../Document/Uploadify/jquery.uploadify.v2.1.4.js" type="text/javascript"></script>
    <script src="../Document/Uploadify/swfobject.js" type="text/javascript"></script>
    <link href="../Document/Uploadify/uploadify.css" rel="stylesheet" type="text/css" />

     <style type="text/css">
        .chkbox input[type="checkbox"] 
        { 
            margin-right: 5px; 
        }
    </style>

    <script type="text/javascript">


        $(document).ready(function () {

            $('#txtSQLRetrieval').on('change input focusout', function () {
                var scroll_height = $('#txtSQLRetrieval').get(0).scrollHeight;
                $('#txtSQLRetrieval').css('height', scroll_height + 'px');
            });

            $('#sqlOptions_1').click(function () {
                $('#trSQLSPRetrieval').show();
                $('#trSQLRetrieval').hide();
                var triggeredBy = $('#ctl00_HomeContentPlaceHolder_ddlTriggeredBy').val();
                if (triggeredBy == "") {
                    setSpanSQLRetrieval('spcurrent');
                }
                else {
                    setSpanSQLRetrieval('parameter');
                }
            });

            $('#sqlOptions_0').click(function () {
                $('#trSQLSPRetrieval').hide();
                $('#trSQLRetrieval').show();
                var triggeredBy = $('#ctl00_HomeContentPlaceHolder_ddlTriggeredBy').val();
                if (triggeredBy == "") {
                    setSpanSQLRetrieval('current');
                }
                else {
                    setSpanSQLRetrieval('recordid');
                }
            });

            $('#ctl00_HomeContentPlaceHolder_ddlTriggeredBy').change(function () {
                var triggeredBy = $('#ctl00_HomeContentPlaceHolder_ddlTriggeredBy').val();
                var sqlCommand = document.getElementById("sqlOptions_0").checked;
                if (triggeredBy == "" && sqlCommand) {
                    setSpanSQLRetrieval('current');
                }

                if (triggeredBy == "" && !sqlCommand) {
                    setSpanSQLRetrieval('spcurrent');
                }

                if (triggeredBy != "" && sqlCommand) {
                    setSpanSQLRetrieval('recordid');
                }
                if (triggeredBy != "" && !sqlCommand) {
                    setSpanSQLRetrieval('parameter');
                }

            });

            $('#txtSQLRetrieval').on('input', function () {
                validateSQL();
            });
        });

        function setSpanSQLRetrieval(flag) {
            if (flag == "parameter") {
                document.getElementById("spansqltriggerlabel").textContent = "On change as the Paramerter";
            }
            if (flag == "recordid") {
                document.getElementById("spansqltriggerlabel").textContent = "On change as the @RecordID";
            }
            if (flag == "current") {
                document.getElementById("spansqltriggerlabel").textContent = "Current RecordID as the @RecordID";
            }
            if (flag == "spcurrent") {
                document.getElementById("spansqltriggerlabel").textContent = "Current RecordID as the Paramerter";
            }
        }

        function validateSQL() {
            var sqlScript = $('#txtSQLRetrieval').val();
            if (sqlScript.toLowerCase().indexOf(';') > -1) {
                alert("Semi-colon is not allowed.");
                $('#txtSQLRetrieval').val(sqlScript.replace(';', ''));
            }
            else if (sqlScript.toLowerCase().indexOf('update') > -1) {
                alert("Update script is not allowed.");
                $('#txtSQLRetrieval').val(sqlScript.toLowerCase().replace('update', ''));
            }           
            else if (sqlScript.toLowerCase().indexOf('delete') > -1 ) {
                alert("Delete script is not allowed.");
                $('#txtSQLRetrieval').val(sqlScript.toLowerCase().replace('delete', ''));
            }           
        }

        /* == Red 15112019: Ticket 4583 == */
        function showRoles() {
            var divRoles = document.getElementById("ctl00_HomeContentPlaceHolder_divRoles");
            var chkAllowWhenRoles = document.getElementById("chkAllowWhenRoles");
            divRoles.style.display = chkAllowWhenRoles.checked ? "block" : "none";
        }

        /* == Red 15112019: Ticket 4583 == */
        function allowWhenRoles() {
            var chkValidationCanIgnore = document.getElementById("ctl00_HomeContentPlaceHolder_chkValidationCanIgnore");
            var divAllowWhenRoles = document.getElementById("divAllowWhenRoles");
            var divRoles = document.getElementById("ctl00_HomeContentPlaceHolder_divRoles");
            divAllowWhenRoles.style.display = chkValidationCanIgnore.checked ? "inherit" : "none";
            if (!chkValidationCanIgnore.checked) {
                var chkAllowWhenRoles = document.getElementById("chkAllowWhenRoles");
                chkAllowWhenRoles.checked = false;
                divRoles.style.display = "none";
            }
         
        }

      


        $(document).ready(function () {

            allowWhenRoles();
            showRoles();
            var hfTableID = document.getElementById("ctl00_HomeContentPlaceHolder_hfTableID");
            var hfColumnID = document.getElementById("ctl00_HomeContentPlaceHolder_hfColumnID");
            var strTypeV = $('#ctl00_HomeContentPlaceHolder_ddlType').val();
            //var strTypeV = $('#ctl00_HomeContentPlaceHolder_ddlType').val();

            var strTextType = $('#ddlTextType').val();
            var strCalType = $('#ctl00_HomeContentPlaceHolder_ddlCalculationType').val();
            var strCalFormatType = $('#ctl00_HomeContentPlaceHolder_ddlDateResultFormat').val();
            var strDDType = $('#ddlDDType').val();
            var txtColumnName = document.getElementById("ctl00_HomeContentPlaceHolder_txtColumnName");
            var chkSummaryPage = document.getElementById("ctl00_HomeContentPlaceHolder_chkSummaryPage");
            var chkDetailPage = document.getElementById("ctl00_HomeContentPlaceHolder_chkDetailPage");
            var chkGraph = document.getElementById("ctl00_HomeContentPlaceHolder_chkGraph");
            //var chkImport = document.getElementById("ctl00_HomeContentPlaceHolder_chkImport");
            //var chkExport = document.getElementById("ctl00_HomeContentPlaceHolder_chkExport");
            //var chkMobile = document.getElementById("ctl00_HomeContentPlaceHolder_chkMobile");

            var hfColumnSystemname = document.getElementById("hfColumnSystemname");
            var hfDateTimeColumn = document.getElementById("ctl00_HomeContentPlaceHolder_hfDateTimeColumn");
            var hfIsSystemColumn = document.getElementById("hfIsSystemColumn");

            var txtDisplayTextSummary = document.getElementById("ctl00_HomeContentPlaceHolder_txtDisplayTextSummary");
            var txtDisplayTextDetail = document.getElementById("ctl00_HomeContentPlaceHolder_txtDisplayTextDetail");
            var txtGraph = document.getElementById("ctl00_HomeContentPlaceHolder_txtGraph");

            //RP Added Ticket 5136
            var boolNoDisplayTextSummary = txtDisplayTextSummary.value.trim() == '' ? true : false;
            var boolDisplayTextDetail = txtDisplayTextDetail.value.trim() == '' ? true : false;
            var boolNoGraph = txtGraph.value.trim() == '' ? true : false;
            //End Modification

            //var txtNameOnImport = document.getElementById("ctl00_HomeContentPlaceHolder_txtNameOnImport");
            //var hfIsImportPositional = document.getElementById("ctl00_HomeContentPlaceHolder_hfIsImportPositional");
            //var txtNameOnExport = document.getElementById("ctl00_HomeContentPlaceHolder_txtNameOnExport");
            //var txtMobile = document.getElementById("ctl00_HomeContentPlaceHolder_txtMobile");
            //Warning

            var hlWarningAdvanced = document.getElementById("ctl00_HomeContentPlaceHolder_hlWarningAdvanced");
            var txtValidationOnWarning = document.getElementById("ctl00_HomeContentPlaceHolder_txtValidationOnWarning");
            var txtMinWaring = document.getElementById("ctl00_HomeContentPlaceHolder_txtMinWaring");
            var txtMaxWrning = document.getElementById("ctl00_HomeContentPlaceHolder_txtMaxWrning");
            var chkWarningFormula = document.getElementById("chkWarningFormula");
            var chkWarningConditions = document.getElementById("chkWarningConditions");
            //Exceedance
            var hfShowExceedance = document.getElementById("hfShowExceedance");

            var hlExceedanceAdvanced = document.getElementById("ctl00_HomeContentPlaceHolder_hlExceedanceAdvanced");
            var txtValidationOnExceedance = document.getElementById("ctl00_HomeContentPlaceHolder_txtValidationOnExceedance");
            var txtMinExceedance = document.getElementById("ctl00_HomeContentPlaceHolder_txtMinExceedance");
            var txtMaxExceedance = document.getElementById("ctl00_HomeContentPlaceHolder_txtMaxExceedance");
            var chkExceedanceFormula = document.getElementById("chkExceedanceFormula");
            var chkExceedanceConditions = document.getElementById("chkExceedanceConditions");
            //Validation
            var hlValidAdvanced = document.getElementById("ctl00_HomeContentPlaceHolder_hlValidAdvanced");
            var txtValidationEntry = document.getElementById("ctl00_HomeContentPlaceHolder_txtValidationEntry");
            var txtMinValid = document.getElementById("ctl00_HomeContentPlaceHolder_txtMinValid");
            var txtMaxValid = document.getElementById("ctl00_HomeContentPlaceHolder_txtMaxValid");
            var chkValidFormula = document.getElementById("chkValidFormula");
            var chkValidConditions = document.getElementById("chkValidConditions");
            var ddlImportance = document.getElementById("ctl00_HomeContentPlaceHolder_ddlImportance");

            var txtCalculation = document.getElementById("ctl00_HomeContentPlaceHolder_txtCalculation");



            //JA O3 JAN 2017
            function loadImageStatus() {
                //if ($('#ctl00_HomeContentPlaceHolder_chkUploadPhoto').attr('checked')) {
                //    $("#file_upload_image").hide();
                //    //$("#trImageHeightSummary").show();
                //    $("#lblImage").hide();
                //}
                //else {
                //    $("#file_upload_image").show();
                //    //$("#trImageHeightSummary").hide();
                //    $("#lblImage").show();
                //}


                if (strTypeV == 'image') {

                    if (document.getElementById('ctl00_HomeContentPlaceHolder_chkUploadPhoto').checked) {
                        $("#file_upload_image").hide();
                        $("#lblImage").hide();
                    }
                    else {
                        $("#file_upload_image").show();
                       $("#lblImage").show();
                    }

                }
                else
                {
                    $("#file_upload_image").hide();
                }

            }

            function ManageFormula() {
                var bShowValidationRoot = false;
                strTypeV = $('#ctl00_HomeContentPlaceHolder_ddlType').val();



                if (ColumnTypeIn(strTypeV, 'number')) {
                    var strNumberTypeV = $('#ctl00_HomeContentPlaceHolder_ddlNumberType').val();
                    if (ColumnTypeIn(strNumberTypeV, '1,4,5,6,7')) { bShowValidationRoot = true; }
                }

                if (ColumnTypeIn(strTypeV, 'calculation')) {
                    var strCalType = $('#ctl00_HomeContentPlaceHolder_ddlCalculationType').val();
                    if (ColumnTypeIn(strCalType, 'n,f,d')) { bShowValidationRoot = true; }
                }

                //KG 27-02-2017 ticket 2209
                if (ColumnTypeIn(strTypeV, 'date_time')) {
                    var strDateType = $('#ddlDateTimeType').val();
                    if (strDateType == 'date' || strDateType == 'datetime') { bShowValidationRoot = true; }
                }
                else
                {
                    $("#tdChkSecond").hide();
                    //ddlCalculationFormatTypeChange(false);
                }

                ddlCalculationFormatTypeChange(false);

                if (bShowValidationRoot) {
                    //$("#divchkValidationCanIgnore").show();
                    $("#divValidationRoot").show();
                    $("#divGraphOptions").show();
                    ShowWarningMinMax();

                    if (hfShowExceedance.value == 'yes') {
                        ShowExceedanceMinMax();
                    }
                    ShowValidMinMax();
                }
                else {
                    //$("#divchkValidationCanIgnore").hide();
                    $("#divValidationRoot").hide();
                    $("#divGraphOptions").hide();

                    var chkWarning = document.getElementById("ctl00_HomeContentPlaceHolder_chkWarning");
                    chkWarning.checked = false;
                    var chkExceedence = document.getElementById("ctl00_HomeContentPlaceHolder_chkExceedence");
                    chkExceedence.checked = false;
                    var chkMaximumValueat = document.getElementById("ctl00_HomeContentPlaceHolder_chkMaximumValueat");
                    chkMaximumValueat.checked = false;

                    txtMinValid.value = '';

                    txtMaxValid.value = '';

                    txtValidationEntry.value = '';

                    txtMinWaring.value = '';

                    txtMaxWrning.value = '';

                    txtValidationOnWarning.value = '';

                    txtMinExceedance.value = '';

                    txtMaxExceedance.value = '';

                    txtValidationOnExceedance.value = '';
                }
                chkWarningClick(false);
                chkExceedenceClick(false);
                chkMaximumValueatClick(false);
            }

            function ColumnTypeIn(sCT, sSCT) {
                var s_a = sSCT.split(',');
                for (var i = 0; i < s_a.length; i++) {
                    if (s_a[i] == sCT) {
                        return true;
                    }
                }
                return false;
            }


            //function clearMinMaxExceedanceValue() {
            //    document.getElementById("ctl00_HomeContentPlaceHolder_txtMinExceedance").value = "";
            //    document.getElementById("ctl00_HomeContentPlaceHolder_txtMaxExceedance").value = "";
            //}

            //function enableCompareValidatorForDataExceedance(bVal) {
            //    var cvtxtMinExceed = document.getElementById("ctl00_HomeContentPlaceHolder_cvtxtMinExceedance");
            //    var cvtxtMaxExceed = document.getElementById("ctl00_HomeContentPlaceHolder_cvtxtMaxExceedance");
            //    var cvExceedRanged = document.getElementById("ctl00_HomeContentPlaceHolder_cvExceedanceRange");
            //    if (bVal) {
            //        if (hfShowExceedance != null) {
            //            if (hfShowExceedance.value == 'no') {
            //                ValidatorEnable(cvtxtMinExceed, false)
            //                ValidatorEnable(cvtxtMaxExceed, false)
            //                ValidatorEnable(cvExceedRanged, false)
            //                clearMinMaxExceedanceValue();
            //            }
            //            else {
            //                ValidatorEnable(cvtxtMinExceed, true)
            //                ValidatorEnable(cvtxtMaxExceed, true)
            //                ValidatorEnable(cvExceedRanged, true)
            //            }
            //        }
            //    }
            //    else {
            //        ValidatorEnable(cvtxtMinExceed, false)
            //        ValidatorEnable(cvtxtMaxExceed, false)
            //        ValidatorEnable(cvExceedRanged, false)
            //    }
            //}

            function chkButtonWarningMessageClick() {
                var chkButtonWarningMessage = document.getElementById("chkButtonWarningMessage");
                var txtButtonWarningMessage = document.getElementById("txtButtonWarningMessage");
                if (chkButtonWarningMessage != null && chkButtonWarningMessage.checked == true) {
                    $("#txtButtonWarningMessage").show();
                }
                else {
                    $("#txtButtonWarningMessage").hide();
                    if (txtButtonWarningMessage != null) {
                        txtButtonWarningMessage.value = '';
                    }
                }
            }

             function ButtonOnClickProc() {
                 var sBtnOnClick = $('#ctl00_HomeContentPlaceHolder_ddlButtonAction').val();
                 var sBtnOnClickAfter = $('#ctl00_HomeContentPlaceHolder_ddlButtonAfterAction').val();


                 $("#trButton4_Link").show();
                 $("#trButton4_Param").show();

                 if ((sBtnOnClick == '' && sBtnOnClickAfter=='')) {
                    $("#trButton4_Child").hide();
                    $("#trButton4_Table").hide();
                    //$("#trButton4_Link").hide();
                    //$("#trButton4_Param").hide();
                    $("#trButton4_Service").hide();
                    $("#trButton4_Report").hide();
                    $("#trButton4_TextOnEdit").hide();
                }
                else {
                    $("#trButton4_Param").show();
                     if (sBtnOnClick == 'AddChild') {
                         $("#trButton4_Child").show();
                         $("#trButton4_Table").hide();
                         //$("#trButton4_Link").hide();
                         $("#trButton4_Service").hide();
                         $("#trButton4_Report").hide();
                         $("#trButton4_TextOnEdit").hide();
                     }
                     else if (sBtnOnClick == 'AddRecord') {
                         $("#trButton4_Child").hide();
                         $("#trButton4_Table").show();
                         //$("#trButton4_Link").hide();
                         $("#trButton4_Service").hide();
                         $("#trButton4_Report").hide();
                         $("#trButton4_TextOnEdit").hide();
                     }
                     else if (sBtnOnClick == 'ExecService') {
                         $("#trButton4_Child").hide();
                         $("#trButton4_Table").hide();
                         //$("#trButton4_Link").hide();
                         //$("#trButton4_Param").hide();
                         $("#trButton4_Service").show();
                         $("#trButton4_Report").hide();
                         $("#trButton4_TextOnEdit").hide();
                     }
                     else if (sBtnOnClick == 'Print_Report') {
                         $("#trButton4_Child").hide();
                         $("#trButton4_Table").hide();
                        // $("#trButton4_Link").hide();
                        // $("#trButton4_Param").hide();
                         $("#trButton4_Service").hide();
                         $("#trButton4_Report").show();
                         $("#trButton4_TextOnEdit").hide();
                     }
                     else if (sBtnOnClick == 'Edit_Current_Record') {
                         $("#trButton4_Child").hide();
                         $("#trButton4_Table").hide();
                        // $("#trButton4_Link").hide();
                        // $("#trButton4_Param").hide();
                         $("#trButton4_Service").hide();
                         $("#trButton4_Report").hide();
                         $("#trButton4_TextOnEdit").show();
                     }
                     else if (sBtnOnClick == 'RunProcedure' || sBtnOnClick == 'Export_Excel') {
                         $("#trButton4_Child").hide();
                         $("#trButton4_Table").hide();
                         //$("#trButton4_Link").show();
                         $("#trButton4_Service").hide();
                         $("#trButton4_Report").hide();
                         $("#trButton4_TextOnEdit").hide();
                         $("#trButton4_Param").hide();
                     }
                     else {
                         //$("#trButton4_Link").show();
                     }
                }

             }



            //function ButtonOnClickProc() {
            //    var sBtnOnClick = $('#ctl00_HomeContentPlaceHolder_ddlButtonOnClick').val();
            //    if (sBtnOnClick == '' ||
            //        sBtnOnClick == 'StayCurrent' ||
            //        sBtnOnClick == 'Goback' ||
            //        sBtnOnClick == 'Export_Excel') {
            //        $("#trButton4_Child").hide();
            //        $("#trButton4_Table").hide();
            //        $("#trButton4_Link").hide();
            //        $("#trButton4_Param").hide();
            //        $("#trButton4_Service").hide();
            //        $("#trButton4_Report").hide();
            //        $("#trButton4_TextOnEdit").hide();
            //    }
            //    else {
            //        $("#trButton4_Param").show();
            //        if (sBtnOnClick == 'AddChild' || sBtnOnClick == 'NoSave') {
            //            $("#trButton4_Child").show();
            //            $("#trButton4_Table").hide();
            //            $("#trButton4_Link").hide();
            //            $("#trButton4_Service").hide();
            //            $("#trButton4_Report").hide();
            //            $("#trButton4_TextOnEdit").hide();
            //        }
            //        else if (sBtnOnClick == 'AddRecord') {
            //            $("#trButton4_Child").hide();
            //            $("#trButton4_Table").show();
            //            $("#trButton4_Link").hide();
            //            $("#trButton4_Service").hide();
            //            $("#trButton4_Report").hide();
            //            $("#trButton4_TextOnEdit").hide();
            //        }
            //        else if (sBtnOnClick == 'ExecService') {
            //            $("#trButton4_Child").hide();
            //            $("#trButton4_Table").hide();
            //            $("#trButton4_Link").hide();
            //            $("#trButton4_Param").hide();
            //            $("#trButton4_Service").show();
            //            $("#trButton4_Report").hide();
            //            $("#trButton4_TextOnEdit").hide();
            //        }
            //        else if (sBtnOnClick == 'Print_Report') {
            //            $("#trButton4_Child").hide();
            //            $("#trButton4_Table").hide();
            //            $("#trButton4_Link").hide();
            //            $("#trButton4_Param").hide();
            //            $("#trButton4_Service").hide();
            //            $("#trButton4_Report").show();
            //            $("#trButton4_TextOnEdit").hide();
            //        }
            //        else if (sBtnOnClick == 'Edit_Current_Record') {
            //            $("#trButton4_Child").hide();
            //            $("#trButton4_Table").hide();
            //            $("#trButton4_Link").hide();
            //            $("#trButton4_Param").hide();
            //            $("#trButton4_Service").hide();
            //            $("#trButton4_Report").hide();
            //            $("#trButton4_TextOnEdit").show();
            //        }
            //        else {
            //            $("#trButton4_Child").hide();
            //            $("#trButton4_Table").hide();
            //            $("#trButton4_Link").show();
            //            $("#trButton4_Service").hide();
            //            $("#trButton4_Report").hide();
            //        }
            //    }

            //}



            function chkSPToRunClick() {
                var chk = document.getElementById("chkSPToRun");
                var ddlButtonSPToRun = document.getElementById("ddlButtonSPToRun");
                if (chk != null && chk.checked == true) {
                    $("#ddlButtonSPToRun").show();
                    $("#trButton1a").show();
                }
                else {
                    $("#ddlButtonSPToRun").hide();
                    $("#trButton1a").hide();
                    var chkButtonDisplayResult = document.getElementById("chkButtonDisplayResult");
                    chkButtonDisplayResult.chec = false;
                    if (ddlButtonSPToRun != null) {
                        ddlButtonSPToRun.value = '';
                    }
                }
            }
            function chkImageOnSummaryClick() {
                var chk = document.getElementById("chkImageOnSummary");
                var txt = document.getElementById("txtImageOnSummaryMaxHeight");

                if (chk != null) {
                    if (chk.checked == true) {
                        $("#tblImageOnSummaryMaxHeight").show();
                    }
                    else {

                        if (txt != null) {
                            txt.value = '';
                        }

                        $("#tblImageOnSummaryMaxHeight").hide();
                    }
                }
            }

            function chkCompareOperatorClick() {
                var chkCompareOperator = document.getElementById("chkCompareOperator");
                var divCompareOperator = document.getElementById("divCompareOperator");
                if (chkCompareOperator.checked == true) {

                    ValidatorEnable(document.getElementById('rfvCompareOperator'), true);
                    ValidatorEnable(document.getElementById('rfvCompareTable'), true);
                    ValidatorEnable(document.getElementById('rfvCompareColumnID'), true);
                    divCompareOperator.style.display = 'block';
                    document.getElementById('rfvCompareOperator').style.display = 'none';
                    document.getElementById('rfvCompareTable').style.display = 'none';
                    document.getElementById('rfvCompareColumnID').style.display = 'none';
                }
                else {
                    divCompareOperator.style.display = 'none';
                    ValidatorEnable(document.getElementById('rfvCompareOperator'), false);
                    ValidatorEnable(document.getElementById('rfvCompareTable'), false);
                    ValidatorEnable(document.getElementById('rfvCompareColumnID'), false);
                }
            }
            function ddlCalculationFormatTypeChange(bEvent) {
                if (ColumnTypeIn(strTypeV, 'calculation')) {
                    var strCalType = $('#ctl00_HomeContentPlaceHolder_ddlCalculationType').val();
                    if (ColumnTypeIn(strCalType, 'd')) {
                        var strCalFmtType = $('#ctl00_HomeContentPlaceHolder_ddlDateResultFormat').val();
                        if (strCalFmtType == 'time' || strCalFmtType == 'datetime') {
                            $("#tdChkSecond").show();
                        }
                        else
                        {
                            $("#tdChkSecond").hide();
                        }

                    }
                }
            }
            function ddlCalculationTypeChange(bEvent) {

                strCalType = $('#ctl00_HomeContentPlaceHolder_ddlCalculationType').val();
                //document.getElementById('ctl00_HomeContentPlaceHolder_hlCalculationEdit').href = document.getElementById('hfCalculationType').value;

                document.getElementById('ctl00_HomeContentPlaceHolder_hlCalculationEdit').href = document.getElementById('hfCalculationType').value + '&caltype=' + strCalType;

                if (strCalType == 'n' || strCalType == 'f') {
                    $("#trGraphOption").show();
                    if (bEvent = true)
                        chkGraph.checked = true;
                    $("#tblDateCal").hide();
                    $("#lblCheckForFlat").show();
                    $("#chkFlatLine").show();
                    $("#trFlatLine").show();
                    $("#trRound").show();

                    $("#trCheckUnlikelyValue").show();
                }
                else {
                    $("#trGraphOption").hide();
                    chkGraph.checked = false;
                }
                if (strCalType == 'n') {
                    $("#tblFinancialSymbol").hide();

                }
                if (strCalType == 'f') {
                    $("#tblFinancialSymbol").show();

                }
                if (strCalType == 'd' || strCalType == 't') {
                    $("#tblFinancialSymbol").hide();

                    $("#lblCheckForFlat").hide();
                    $("#chkFlatLine").hide();
                    $("#trFlatLine").hide();
                    $("#trRound").hide();

                    $("#trCheckUnlikelyValue").hide();

                    if (strCalType == 'd') {
                        $("#tblDateCal").show();
                        //document.getElementById('ctl00_HomeContentPlaceHolder_hlCalculationEdit').href = document.getElementById('hfCalculationType').value + '&date=yes';
                    }
                    else {
                        $("#tblDateCal").hide();
                    }

                }

                chkGraphClick();
                ManageFormula();
            }


            function ShowWarningMinMax() {
                var hfShowWarningMinMax = document.getElementById("hfShowWarningMinMax");
                if (hfShowWarningMinMax.value == 'yes') {

                    //var x = document.getElementById('ctl00_HomeContentPlaceHolder_txtValidationOnWarning');
                    txtValidationOnWarning.style.display = 'none';

                    var x = document.getElementById('divWarningMinMax');
                    x.style.display = 'block';
                    hlWarningAdvancedURL();
                }
                else {
                    //var x = document.getElementById('ctl00_HomeContentPlaceHolder_txtValidationOnWarning');
                    txtValidationOnWarning.style.display = '';
                    var x = document.getElementById('divWarningMinMax');
                    x.style.display = 'none';
                }

            }

            function ShowExceedanceMinMax() {
                var hfShowExceedanceMinMax = document.getElementById("hfShowExceedanceMinMax");
                if (hfShowExceedanceMinMax.value == 'yes') {

                    //var x = document.getElementById('ctl00_HomeContentPlaceHolder_txtValidationOnExceedance');
                    txtValidationOnExceedance.style.display = 'none';

                    var x = document.getElementById('divExceedanceMinMax');
                    x.style.display = 'block';

                    hlExceedanceAdvancedURL();

                }
                else {
                    //var x = document.getElementById('ctl00_HomeContentPlaceHolder_txtValidationOnExceedance');
                    txtValidationOnExceedance.style.display = '';
                    var x = document.getElementById('divExceedanceMinMax');
                    x.style.display = 'none';
                }

            }

            function ShowValidMinMax() {
                var hfShowValidMinMax = document.getElementById("hfShowValidMinMax");
                if (hfShowValidMinMax.value == 'yes') {

                    //var x = document.getElementById('ctl00_HomeContentPlaceHolder_txtValidationEntry');
                    txtValidationEntry.style.display = 'none';

                    var x = document.getElementById('divValidMinMax');
                    x.style.display = 'block';

                    //x = document.getElementById('ctl00_HomeContentPlaceHolder_hlValidAdvanced');
                    // x.style.display = 'block';
                    hlValidAdvancedURL();

                }
                else {

                    //var x = document.getElementById('ctl00_HomeContentPlaceHolder_txtValidationEntry');
                    txtValidationEntry.style.display = '';

                    var x = document.getElementById('divValidMinMax');
                    x.style.display = 'none';
                    //x = document.getElementById('ctl00_HomeContentPlaceHolder_hlValidAdvanced');
                    // x.style.display = 'none';
                }

            }

            //Start ResetColumnType
            function ResetColumnType(bEvent) {
                strTypeV = $('#ctl00_HomeContentPlaceHolder_ddlType').val();

                if (ColumnTypeIn(strTypeV, 'image,content,trafficlight,file,childtable')) {
                    $("#tdOption").hide();
                }
                else {
                    $("#tdOption").show();
                }

                if (ColumnTypeIn(strTypeV, 'image,calculation,content,trafficlight,file,childtable')) {
                    $("#trReadonly").hide();
                }
                else {
                    $("#trReadonly").show();
                }

                if (ColumnTypeIn(strTypeV, 'text,number,listbox,staticcontent')) {
                    $("#trTextDimension").show();
                    if (ColumnTypeIn(strTypeV, 'text,listbox,staticcontent')) {
                        $('#tdHeightLabel').show();
                        $('#tdHeight').show();
                    }
                    else {
                        $('#tdHeightLabel').hide();
                        $('#tdHeight').hide();
                    }
                }
                else {
                    $("#trTextDimension").hide();
                }


                if (ColumnTypeIn(strTypeV, 'text,number')) {
                    $("#trEditableOnSummary").show();                    
                }
                else {
                    $("#trEditableOnSummary").hide();
                }


                $("#trDetailPage1").show();
                $("#trDetailPage2").show();
                if (bEvent)
                    chkDetailPage.checked = true;






                //chkDetailPageClick();
                if (ColumnTypeIn(strTypeV, 'staticcontent,button,calculation,checkbox,trafficlight,childtable') == false) {
                    $("#trMandatory").show();
                }
                else {
                    $(ddlImportance).val('');
                    $("#trMandatory").hide();

                }

                if (ColumnTypeIn(strTypeV, 'number,calculation')) {
                    $("#trGraphOption").show();
                    if (bEvent)
                    {
                        chkGraph.checked = true;
                        chkGraphClick();
                    }
                        
                }
                else {
                    $("#trGraphOption").hide();
                    chkGraph.checked = false;
                    $("#trFlatLine").hide();
                }
                //chkGraphClick();
                if (ColumnTypeIn(strTypeV, 'staticcontent,button,childtable')) {
                    //$("#trImportOption").hide();
                    //$("#trExportOption").hide();
                    //$("#trMobileSiteSummary").hide();
                    $("#trSummaryPage1").hide();
                    chkSummaryPage.checked = false;
                    //chkImport.checked = false;
                    //chkExport.checked = false;
                    //chkMobile.checked = false;
                    $("#divchkCompareOperator").hide();
                    $("#divchkValidationCanIgnore").hide();
                    var chkValidationCanIgnore = document.getElementById("ctl00_HomeContentPlaceHolder_chkValidationCanIgnore");
                    chkValidationCanIgnore.checked = false;

                }
                else {

                    //if (ColumnTypeIn(strTypeV, 'calculation')) {
                    //    $("#trImportOption").hide(); chkImport.checked = false;
                    //}
                    //else {
                    //    $("#trImportOption").show();
                    //}

                    //$("#trExportOption").show();
                    //$("#trMobileSiteSummary").show();
                    $("#trSummaryPage1").show();

                    //JA 31 JAN 2017
                    //chkSummaryPage.checked = true;

                    $("#divchkValidationCanIgnore").show();
                    //chkSummaryPage.checked = true;
                    if (bEvent) {
                        //chkImport.checked = true;
                        //chkExport.checked = true;
                        //chkMobile.checked = false;
                    }


                    $("#divchkCompareOperator").show();

                    //chkCompareOperatorClick();
                }
                //chkSummaryPageClick();
                //chkImportClick();
                //chkExportClick();
                //chkMobileClick();
                if (ColumnTypeIn(strTypeV, 'calculation')) {
                    $("#trCalculationType").show();
                    $("#trCalculation").show();
                    $("#trCalculationNullAsZero").show();
                    ddlCalculationTypeChange(false);
                }
                else {
                    $("#trCalculationType").hide();
                    $("#trCalculation").hide();
                    $("#trCalculationNullAsZero").hide();
                }

                if (ColumnTypeIn(strTypeV, 'trafficlight')) {
                    $("#trTrafficLight").show();
                }
                else {
                    $("#trTrafficLight").hide();
                }

                if (ColumnTypeIn(strTypeV, 'staticcontent')) {
                    $("#trStaticContent").show();
                    $("#lblPX1").text("(px)");
                    $("#lblPX2").text("(px)");
                    $("#trSummaryPage1").show();
                }
                else {
                    $("#trStaticContent").hide();
                    $("#lblPX1").text("");
                    $("#lblPX2").text("");
                }
                if (ColumnTypeIn(strTypeV, 'childtable')) {
                    $("#trChildTable").show();                   
                }
                else {
                    $("#trChildTable").hide();
                }

                if (ColumnTypeIn(strTypeV, 'ascx')) {
                    $("#trUserControl").show();
                }
                else {
                    $("#trUserControl").hide();
                }

                if (ColumnTypeIn(strTypeV, 'button')) {
                    $("#trButton1").show();
                    $("#trButton2").show();
                    $("#trButton3").show();
                    $("#trButton4").show();
                    $("#trButton5").show();

                    chkButtonWarningMessageClick();
                    ButtonOnClickProc();
                    chkSPToRunClick();
                    chkImageOnSummaryClick();
                }
                else {
                    $("#trButton1").hide();
                    $("#trButton2").hide();
                    $("#trButton3").hide();
                    $("#trButton4").hide();
                    $("#trButton5").hide();
                }

                if (ColumnTypeIn(strTypeV, 'checkbox')) {
                    $("#trCheckbox1").show();
                    $("#trCheckbox2").show();
                    $("#trCheckbox3").show();
                   
                }
                else {
                    $("#trCheckbox1").hide();
                    $("#trCheckbox2").hide();
                    $("#trCheckbox3").hide();

                }


                if (ColumnTypeIn(strTypeV, 'location')) {
                    $("#trLocation").show();
                    chkShowMapClick(false);
                }
                else {
                    $("#trLocation").hide();
                }
                if (ColumnTypeIn(strTypeV, 'date_time')) {
                    //$("#trReminders").show();
                    $("#trDateTimeType").show();
                    ddlDateTimeTypeChange(false);
                    $('#ddlDefaultValue option[value="additional"]').show();
                }
                else {
                    $("#trDateTimeType").hide();
                    //$("#trReminders").hide();
                    $('#ddlDefaultValue option[value="additional"]').hide();
                }
                if (ColumnTypeIn(strTypeV, 'text')) {
                    $("#trTextType").show();

                    ddlTextTypeClick();
                }
                else {
                    $("#trTextType").hide();
                }



                if (ColumnTypeIn(strTypeV, 'dropdown,listbox') == false) {
                    $("#trDDTable").hide();
                    $("#trFilter").hide();
                    $("#trDDDisplayColumn").hide();
                    $("#trDDValues").hide();
                    $("#trDDType").hide();
                    $("#trListboxType").hide();
                }
                else {
                    if (ColumnTypeIn(strTypeV, 'listbox')) {
                        $("#trListboxType").show();

                        ddlListBoxTypeChange(false);
                    }
                    else {
                        $("#trListboxType").hide();
                    }
                    if (ColumnTypeIn(strTypeV, 'dropdown')) {
                        $("#trDDType").show();
                       
                        ddlDDTypeChange(false);
                        ddlDDTable_Change(false);
                    }
                    else {
                        $("#trDDType").hide();
                    }

                }



                //$("#trDDTableLookup").hide();
                // $("#trTextDimension").hide();



                //  $("#lblSPDefaultValue").hide();





                //$("#tblColumnColour").hide();
                //$("#trFlatLine").hide();

                if (ColumnTypeIn(strTypeV, 'image')) {
                    $("#trImageHeightSummary").show();
                    $("#trImageHeightDetail").show();
                    $("#chk_upload_image").show();
                }
                else {
                    //JA 03 JAN 2017
                    document.getElementById("ctl00_HomeContentPlaceHolder_chkUploadPhoto").checked = true;
                    $("#trImageHeightSummary").hide();
                    $("#trImageHeightDetail").hide();
                    $("#chk_upload_image").hide();
                }

                if (ColumnTypeIn(strTypeV, 'radiobutton')) {
                    $("#trRadioOptionType").show();
                    ddlOptionTypeChange(false);
                }
                else {
                    $("#trRadioOptionType").hide();
                    $('#trOptionImageGrid').hide();

                }



                if (ColumnTypeIn(strTypeV, 'number,calculation') == false) {
                    $("#trRecordCountTable").hide();
                    $("#trRecordCountClick").hide();
                    $("#trSlider").hide();
                    $("#trColumnToAvg").hide();
                    $("#trAvgNumValues").hide();
                    $("#trRound").hide();

                    $("#trIgnoreSymbols").hide();
                    $("#trCheckUnlikelyValue").hide();
                }

                if (ColumnTypeIn(strTypeV, 'calculation') == false) {
                    $("#tblDateCal").hide();
                }

                if (ColumnTypeIn(strTypeV, 'number,calculation')) {
                    chkRoundClick(false);
                    chkFlatLineClick(false)
                }
                if (ColumnTypeIn(strTypeV, 'staticcontent,button,file,image,calculation,trafficlight,childtable')) {
                    $("#trDefaultValue").hide();
                }
                else {
                    $("#trDefaultValue").show();

                }

                if (ColumnTypeIn(strTypeV, 'number')) {
                    $("#trNumber1").show();
                    ddlNumberTypeChange(false);
                }
                else {
                    $("#trNumber1").hide();
                }

                if (ColumnTypeIn(strTypeV, 'staticcontent,button,file,image,trafficlight,childtable')) {
                    $("#tblColumnColour").hide();
                }
                else {
                    $("#tblColumnColour").show();
                }
                var hlResetCalculations = document.getElementById("hlResetCalculations");
                if (ColumnTypeIn(strTypeV, 'calculation') && hfColumnID.value != -1) {
                    hlResetCalculations.style.display = 'block';
                }
                else {
                    hlResetCalculations.style.display = 'none';
                }

                if (bEvent) {
                    $("#ddlDefaultValue").val($("#ddlDefaultValue option:first").val());
                    var txtDefaultValue = document.getElementById("ctl00_HomeContentPlaceHolder_txtDefaultValue");
                    txtDefaultValue.value = '';
                    var chkCompareOperator = document.getElementById("chkCompareOperator");
                    chkCompareOperator.checked = false;

                    ShowHideDCEdit();
                    var chkRound = document.getElementById("ctl00_HomeContentPlaceHolder_chkRound");
                    chkRound.checked = false;
                    chkRoundClick(false);
                    var chkFlatLine = document.getElementById("chkFlatLine");
                    chkFlatLine.checked = false;
                    chkFlatLineClick(false);
                    var chkIgnoreSymbols = document.getElementById("chkIgnoreSymbols");
                    chkIgnoreSymbols.checked = false;
                    manageSymbolMode(); manageSymbolConstant();
                    var chkCheckUnlikelyValue = document.getElementById("ctl00_HomeContentPlaceHolder_chkCheckUnlikelyValue");
                    chkCheckUnlikelyValue.checked = false;
                    $(ddlImportance).val('');

                }



                ddlDefaultValueChange(false);
                chkCompareOperatorClick();
                ManageFormula();
                loadImageStatus();

                //JA O3 JAN 2017
                if (strTypeV != 'image') {
                    $("#trImageHeightSummary").hide();
                }

                chkSummaryPageClick();
                chkDetailPageClick();
                chkGraphClick();
                if (ColumnTypeIn(strTypeV, 'childtable')) {
                    $("#trChildTable2").show();
                    //alert(strTypeV);
                }
                else {
                    $("#trChildTable2").hide();
                }

                /* == Red 24012020: Ticket 5032 ==*/
                $("#trSQLRetrieval").hide();
                $("#trTriggeredBy").hide();
                $("#trSQLOptions").hide();
                $("#trSQLSPRetrieval").hide();
                
                if (ColumnTypeIn(strTypeV, 'sqlretrieval')) {
                    $("#trSQLRetrieval").show();
                    $("#trTriggeredBy").show();
                    $("#trSQLOptions").show();
                    $("#trSQLSPRetrieval").show();
                    $("#trMandatory").hide();
                    $("#trDefaultValue").hide();
                    $("#tdReadOnlyWhen").hide();
                    $("#trTextDimension").show();
                    $("#trSummaryPage1").hide();


                    var sqlCommand = document.getElementById("sqlOptions_0").checked;
                    if (sqlCommand) {
                        $('#trSQLSPRetrieval').hide();
                        $('#trSQLRetrieval').show();
                        var triggeredBy = $('#ctl00_HomeContentPlaceHolder_ddlTriggeredBy').val();
                        if (triggeredBy == "") {
                            setSpanSQLRetrieval('current');
                        }
                        else {
                            setSpanSQLRetrieval('recordid');
                        }
                    }
                    else {
                        $('#trSQLSPRetrieval').show();
                        $('#trSQLRetrieval').hide();
                        var triggeredBy = $('#ctl00_HomeContentPlaceHolder_ddlTriggeredBy').val();
                        if (triggeredBy == "") {
                            setSpanSQLRetrieval('spcurrent');
                        }
                        else {
                            setSpanSQLRetrieval('parameter');
                        }
                    }

                }
                /* == End Red == */
            }

            //END ResetColumnType



            function txtColumnNameChange() {

                if (chkSummaryPage.checked == true) {
                    txtDisplayTextSummary.value = txtDisplayTextSummary.value.trim() == '' || boolNoDisplayTextSummary ? txtColumnName.value : txtDisplayTextSummary.value;
                }
                if (chkDetailPage.checked == true) {
                    txtDisplayTextDetail.value = txtDisplayTextDetail.value.trim() == '' || boolDisplayTextDetail ? txtColumnName.value : txtDisplayTextDetail.value;
                }

                if (chkGraph.checked == true) {
                    txtGraph.value = txtGraph.value.trim() == '' || boolNoGraph ? txtColumnName.value : txtGraph.value;
                }

                //if (chkImport.checked == true) {
                //    if (hfIsImportPositional.value == '0') {
                //        txtNameOnImport.value = txtColumnName.value;
                //    }
                //}
                //if (chkExport.checked == true) {
                //    txtNameOnExport.value = txtColumnName.value;
                //}

                //if (chkMobile.checked == true) {
                //    txtMobile.value = txtColumnName.value;
                //}


            }

            function chkSummaryPageClick() {
                if (chkSummaryPage.checked == true) {
                    txtDisplayTextSummary.value = txtDisplayTextSummary.value.trim() == '' ? txtColumnName.value : txtDisplayTextSummary.value;
                    $("#ctl00_HomeContentPlaceHolder_txtDisplayTextSummary").show();
                    $("#ctl00_HomeContentPlaceHolder_lblSummaryPage").show();
                }
                else {
                    $("#ctl00_HomeContentPlaceHolder_txtDisplayTextSummary").hide();
                    $("#ctl00_HomeContentPlaceHolder_lblSummaryPage").hide();
                    txtDisplayTextSummary.value = '';
                }
            }

            function chkDetailPageClick() {
                if (chkDetailPage.checked == true) {
                    $("#trDetailPage2").show();
                    txtDisplayTextDetail.value = txtDisplayTextDetail.value.trim() == '' ? txtColumnName.value : txtDisplayTextDetail.value;

                    if (strTypeV == 'staticcontent') {
                        $("#ctl00_HomeContentPlaceHolder_lblDetailPage").hide();
                        $("#ctl00_HomeContentPlaceHolder_txtDisplayTextDetail").hide();
                    }
                    else {
                        $("#ctl00_HomeContentPlaceHolder_lblDetailPage").show();
                        $("#ctl00_HomeContentPlaceHolder_txtDisplayTextDetail").show();
                    }

                }
                else {
                    $("#ctl00_HomeContentPlaceHolder_lblDetailPage").hide();
                    $("#ctl00_HomeContentPlaceHolder_txtDisplayTextDetail").hide();

                    $("#trDetailPage2").hide();

                    txtDisplayTextDetail.value = '';
                }
            }


            function chkGraphClick() {
                if (chkGraph.checked == true) {
                    $("#ctl00_HomeContentPlaceHolder_lblGraph").show();
                    $("#ctl00_HomeContentPlaceHolder_txtGraph").show();

                    txtGraph.value = txtGraph.value.trim() == '' ? txtColumnName.value : txtGraph.value;
                    /* == Red per Jon == */
                    $("#divGraphOptions").show();
                    /* == end Red == */
                   
                }
                else {
                    $("#ctl00_HomeContentPlaceHolder_lblGraph").hide();
                    $("#ctl00_HomeContentPlaceHolder_txtGraph").hide();

                    txtGraph.value = '';

                    /* == Red per Jon == */
                    $("#divGraphOptions").hide();
                    /* == end Red == */
                }
            }

            //function chkImportClick() {

            //    if (chkImport.checked == true) {

            //        if (hfDateTimeColumn.value == 'yes') {
            //            $("#tblDateOptions").show();
            //            var opt = document.getElementById("ctl00_HomeContentPlaceHolder_optSingle");
            //            opt.checked = true;
            //            $("#trDateFormat").show();

            //        }
            //        $("#ctl00_HomeContentPlaceHolder_lblImport").show();
            //        $("#ctl00_HomeContentPlaceHolder_txtNameOnImport").show();

            //        $("#ctl00_HomeContentPlaceHolder_lblConstant").hide();
            //        $("#ctl00_HomeContentPlaceHolder_txtConstant").hide();

            //        var txtD = document.getElementById("ctl00_HomeContentPlaceHolder_txtConstant");

            //        if (hfIsImportPositional.value == '0') {
            //            txtNameOnImport.value = txtColumnName.value;
            //        }
            //        else {
            //            var hfPosMax = document.getElementById("ctl00_HomeContentPlaceHolder_hfMaxPosition");
            //            txtNameOnImport.value = hfPosMax.value;

            //        }

            //    }
            //    else {
            //        $("#ctl00_HomeContentPlaceHolder_lblImport").hide();
            //        $("#ctl00_HomeContentPlaceHolder_txtNameOnImport").hide();
            //        txtNameOnImport.value = '';
            //        if (hfDateTimeColumn.value == 'yes') {
            //            $("#tblDateOptions").hide();
            //            $("#trDateFormat").hide();
            //            $("#trTimeSection").hide();

            //            txtNameOnImport.value = '';
            //            txtNI = document.getElementById("ctl00_HomeContentPlaceHolder_txtNameOnImportTime");
            //            txtNI.value = '';

            //            var opt = document.getElementById("ctl00_HomeContentPlaceHolder_optSingle");
            //            opt.checked = true;
            //        }


            //    }
            //}

            //function chkExportClick() {
            //    if (chkExport.checked == true) {
            //        $("#ctl00_HomeContentPlaceHolder_lblExport").show();
            //        $("#ctl00_HomeContentPlaceHolder_txtNameOnExport").show();
            //        txtNameOnExport.value = txtColumnName.value;

            //    }
            //    else {
            //        $("#ctl00_HomeContentPlaceHolder_lblExport").hide();
            //        $("#ctl00_HomeContentPlaceHolder_txtNameOnExport").hide();
            //        txtNameOnExport.value = '';
            //    }
            //}

            //function chkMobileClick() {
                //if (chkMobile.checked == true) {
                //    $("#ctl00_HomeContentPlaceHolder_lblMobile").show();
                //    $("#ctl00_HomeContentPlaceHolder_txtMobile").show();
                //    txtMobile.value = txtColumnName.value;

                //}
                //else {
                //    $("#ctl00_HomeContentPlaceHolder_lblMobile").hide();
                //    $("#ctl00_HomeContentPlaceHolder_txtMobile").hide();
                //    txtMobile.value = '';
                //}
            //}



            function ShowHideDCEdit() {
                var hlDDEdit = document.getElementById('hlDDEdit');
                var ddDDDisplayColumn = document.getElementById('ddDDDisplayColumn');
                var strAD = ddDDDisplayColumn.value;
                // alert(strAD);
                //hlDDEdit.style.display = 'none';
                if (strAD == '') {
                    hlDDEdit.style.display = 'block';
                }
                else {
                    hlDDEdit.style.display = 'none';
                }
            }
            //setTimeout(function () { ShowHideDCEdit(); }, 2000);



            function ReadOnlyTextBox(event) {
                window.event.returnValue = false;
            }

            function hlWarningAdvancedURL() {
                hlWarningAdvanced.href = '../Help/FormulaTest.aspx?type=warning&formula='
                    + encodeURIComponent(txtValidationOnWarning.value)
                    + '&min=' + encodeURIComponent(txtMinWaring.value)
                    + '&max=' + encodeURIComponent(txtMaxWrning.value)
                    + "&Tableid=" + hfTableID.value + "&Columnid=" + hfColumnID.value;
            }

            function WarningChanged() {
                hlWarningAdvancedURL();
                if (chkWarningFormula != null) {
                    if (txtMinWaring.value == '' && txtMaxWrning.value == '' && txtValidationOnWarning.value == '') {
                        chkWarningFormula.checked = false;
                    }
                    else {
                        chkWarningFormula.checked = true;
                    }
                }


                if (chkWarningConditions != null)
                    chkWarningConditions.checked = false;
            }
            $('#ctl00_HomeContentPlaceHolder_txtMinWaring').change(function () {

                WarningChanged();
            });
            $('#ctl00_HomeContentPlaceHolder_txtMinWaring').keyup(function () {
                WarningKeyUp('Min');
            });

            $('#ctl00_HomeContentPlaceHolder_txtMaxWrning').change(function () {
                WarningChanged();
            });
            $('#ctl00_HomeContentPlaceHolder_txtMaxWrning').keyup(function () {
                WarningKeyUp('Max');
            });

            function WarningKeyUp(minOrMax) {
                if (document.getElementById("ctl00_HomeContentPlaceHolder_chkWarning").checked) {
                    if (minOrMax == 'Min') {
                        document.getElementById("ctl00_HomeContentPlaceHolder_txtWarningMin").value =
                            document.getElementById("ctl00_HomeContentPlaceHolder_txtMinWaring").value
                    }
                    else if (minOrMax == 'Max') {
                        document.getElementById("ctl00_HomeContentPlaceHolder_txtWarningMax").value =
                            document.getElementById("ctl00_HomeContentPlaceHolder_txtMaxWrning").value
                    }
                }
            }


            function hlExceedanceAdvancedURL() {
                hlExceedanceAdvanced.href = '../Help/FormulaTest.aspx?type=exceedance&formula='
                    + encodeURIComponent(txtValidationOnExceedance.value)
                    + '&min=' + encodeURIComponent(txtMinExceedance.value)
                    + '&max=' + encodeURIComponent(txtMaxExceedance.value)
                    + "&Tableid=" + hfTableID.value + "&Columnid=" + hfColumnID.value;
            }

            function ExceedanceChanged() {
                hlExceedanceAdvancedURL();
                if (chkExceedanceFormula != null) {
                    if (txtMinExceedance.value == '' && txtMaxExceedance.value == '' && txtValidationOnExceedance.value == '') {
                        chkExceedanceFormula.checked = false;
                    }
                    else {
                        chkExceedanceFormula.checked = true;
                    }
                }


                if (chkExceedanceConditions != null)
                    chkExceedanceConditions.checked = false;
            }

            $('#ctl00_HomeContentPlaceHolder_txtMinExceedance').change(function () {
                ExceedanceChanged();
            });
            $('#ctl00_HomeContentPlaceHolder_txtMinExceedance').keyup(function () {
                ExceedanceKeyUp('Min');
            });

            $('#ctl00_HomeContentPlaceHolder_txtMaxExceedance').change(function () {
                ExceedanceChanged();
            });
            $('#ctl00_HomeContentPlaceHolder_txtMaxExceedance').keyup(function () {
                ExceedanceKeyUp('Max');
            });

            function ExceedanceKeyUp(minOrMax) {
                if (document.getElementById("ctl00_HomeContentPlaceHolder_chkExceedence").checked) {
                    if (minOrMax == 'Min') {
                        document.getElementById("ctl00_HomeContentPlaceHolder_txtExceedenceMin").value =
                            document.getElementById("ctl00_HomeContentPlaceHolder_txtMinExceedance").value
                    }
                    else if (minOrMax == 'Max') {
                        document.getElementById("ctl00_HomeContentPlaceHolder_txtExceedenceMax").value =
                            document.getElementById("ctl00_HomeContentPlaceHolder_txtMaxExceedance").value
                    }
                }
            }

            function hlValidAdvancedURL() {
                hlValidAdvanced.href = '../Help/FormulaTest.aspx?type=valid&formula='
                    + encodeURIComponent(txtValidationEntry.value)
                    + '&min=' + encodeURIComponent(txtMinValid.value)
                    + '&max=' + encodeURIComponent(txtMaxValid.value)
                    + "&Tableid=" + hfTableID.value + "&Columnid=" + hfColumnID.value;
            }

            function ValidChanged() {
                hlValidAdvancedURL();
                if (chkValidFormula != null) {
                    if (txtMinValid.value == '' && txtMaxValid.value == '' && txtValidationEntry.value == '') {
                        chkValidFormula.checked = false;
                    }
                    else {
                        chkValidFormula.checked = true;
                    }
                }


                if (chkValidConditions != null)
                    chkValidConditions.checked = false;
            }

            function ShowMandatory(bShow) {
                if (bShow) {
                    $("#trMandatory").show();
                }
                else {
                    $(ddlImportance).val('');
                    $("#trMandatory").hide();

                }

            }

            $('#ctl00_HomeContentPlaceHolder_txtMinValid').change(function () {
                ValidChanged();
            });

            $('#ctl00_HomeContentPlaceHolder_txtMaxValid').change(function () {
                ValidChanged();

            });


            $('#ctl00_HomeContentPlaceHolder_txtValidationEntry').keypress(function (e) {

                $(hlValidAdvanced).focus();
                return false;

            });

            $('#ctl00_HomeContentPlaceHolder_txtValidationOnWarning').keypress(function (e) {

                $(hlWarningAdvanced).focus();
                return false;

            });

            $('#ctl00_HomeContentPlaceHolder_txtValidationOnExceedance').keypress(function (e) {

                $(hlExceedanceAdvanced).focus();
                return false;

            });

            $('#ctl00_HomeContentPlaceHolder_txtCalculation').keypress(function (e) {

                $(hlCalculationEdit).focus();
                return false;

            });


            $('#ctl00_HomeContentPlaceHolder_txtColumnName').change(function () {
                txtColumnNameChange();
            });
            $('#ctl00_HomeContentPlaceHolder_txtColumnName').keyup(function () {
                txtColumnNameChange();
            });


            $('#ctl00_HomeContentPlaceHolder_ddlCalculationType').change(function () {
                ddlCalculationTypeChange(true);
            });
            $('#ctl00_HomeContentPlaceHolder_ddlDateResultFormat').change(function () {
                ddlCalculationFormatTypeChange(true);
            });

            //RP Added Ticket 5136
            $("#ctl00_HomeContentPlaceHolder_txtDisplayTextSummary").keyup(function () {
                if (this.value.trim() == '')
                    boolNoDisplayTextSummary = true;
                else
                    boolNoDisplayTextSummary = false;
            });
            $("#ctl00_HomeContentPlaceHolder_txtDisplayTextDetail").keyup(function () {
                if (this.value.trim() == '')
                    boolDisplayTextDetail = true;
                else
                    boolDisplayTextDetail = false;
            });
            $("#ctl00_HomeContentPlaceHolder_txtGraph").keyup(function () {
                if (this.value.trim() == '')
                    boolNoGraph = true;
                else
                    boolNoGraph = false;
            });
            //End Modification

            $("#chkConditions").click(function () {
                var chkConditions = document.getElementById("chkConditions");
                if (chkConditions.checked == true) {
                    $("#hlConditions").trigger("click");
                }
            });

            $("#chkConditionsRO").click(function () {
                var chkConditionsRO = document.getElementById("chkConditionsRO");
                if (chkConditionsRO.checked == true) {
                    $("#hlConditionsRO").trigger("click");
                    var chkReadOnly = document.getElementById("chkReadOnly");
                    chkReadOnly.checked = false;
                }
            });

            $("#chkReadOnly").click(function () {
                var chkReadOnly = document.getElementById("chkReadOnly");
                if (chkReadOnly.checked == true) {                    
                    var chkConditionsRO = document.getElementById("chkConditionsRO");
                    chkConditionsRO.checked = false;
                }
            });



            //JA 03 JAN 2017
            $('#ctl00_HomeContentPlaceHolder_chkUploadPhoto').click(function () {
                loadImageStatus();
            });

            function OpenResetValidationConfirm() {
                $("#hlResetValidation").trigger("click");
            }

            $("#chkValidFormula").click(function () {
                if (chkValidFormula.checked === true) {
                    $("#ctl00_HomeContentPlaceHolder_hlValidAdvanced").trigger("click");
                    document.getElementById('hlValidConditions').innerHTML = 'Conditions'; //T
                    if (chkValidConditions != null)
                        chkValidConditions.checked = false;
                }
            });

            $("#chkValidConditions").click(function () {
                $("#hlValidConditions").trigger("click");
                if (chkValidConditions.checked === true) {
                    if (chkValidFormula != null)
                        chkValidFormula.checked = false;
                }
            });

            $("#chkWarningFormula").click(function () {
                if (chkWarningFormula.checked === true) {
                    $("#ctl00_HomeContentPlaceHolder_hlWarningAdvanced").trigger("click");
                    document.getElementById('hlWarningConditions').innerHTML = 'Conditions';
                    if (chkWarningConditions != null)
                        chkWarningConditions.checked = false;
                }

            });

            $("#chkWarningConditions").click(function () {
                $("#hlWarningConditions").trigger("click");
                if (chkWarningConditions.checked === true) {
                    if (chkWarningFormula != null)
                        chkValidFormula.checked = false;
                }
            });

            $("#chkExceedanceFormula").click(function () {
                if (chkExceedanceFormula.checked === true) {
                    $("#ctl00_HomeContentPlaceHolder_hlExceedanceAdvanced").trigger("click");
                    document.getElementById('hlExceedanceConditions').innerHTML = 'Conditions';
                    if (chkExceedanceConditions != null)
                        chkExceedanceConditions.checked = false;
                }

            });

            $("#chkExceedanceConditions").click(function () {
                $("#hlExceedanceConditions").trigger("click");
                if (chkExceedanceConditions.checked === true) {
                    if (chkExceedanceFormula != null)
                        chkExceedanceFormula.checked = false;
                }
            });

            $("#chkColumnColour").click(function () {
                var chkColumnColour = document.getElementById("chkColumnColour");
                if (chkColumnColour.checked === true) {
                    $("#hlColumnColour").trigger("click");
                }

            });

            //$("#chkControlValueChangeService").click(function () {
            //    var chkControlValueChangeService = document.getElementById("chkControlValueChangeService");
            //    if (chkControlValueChangeService.checked == true) {
            //        $("#hlControlValueChangeService").trigger("click");
            //    }

            //});
            //$("#chkListService").click(function () {
            //    var chkListService = document.getElementById("chkListService");
            //    if (chkListService.checked == true) {
            //        $("#hlListService").trigger("click");
            //    }

            //});


            $("#chkFiltered").click(function () {
                var chkFiltered = document.getElementById("chkFiltered");
                if (chkFiltered.checked == true) {
                    $("#hlFiltered").trigger("click");
                }

            });

            $("#chkImageOnSummary").click(function () {

                chkImageOnSummaryClick();
            });

            $("#chkButtonWarningMessage").click(function () {
                chkButtonWarningMessageClick();
            });

            //$("#chkButtonOpenLink").click(function () {
            //    ButtonOnClickProc();
            //});

            //$('#ctl00_HomeContentPlaceHolder_ddlButtonOnClick').change(function (e) {
            //    ButtonOnClickProc();
            //});

            $('#ctl00_HomeContentPlaceHolder_ddlButtonAction').change(function (e) {
                ButtonOnClickProc();
            });
            $('#ctl00_HomeContentPlaceHolder_ddlButtonAfterAction').change(function (e) {
                ButtonOnClickProc();
            });

            $("#chkSPToRun").click(function () {
                chkSPToRunClick();
            });



            $("#ctl00_HomeContentPlaceHolder_chkSummaryPage").click(function () {
                chkSummaryPageClick();
            });
            $("#ctl00_HomeContentPlaceHolder_chkDetailPage").click(function () {
                chkDetailPageClick();
            });
            $("#ctl00_HomeContentPlaceHolder_chkGraph").click(function () {
                chkGraphClick();
            });

            //$("#ctl00_HomeContentPlaceHolder_chkImport").click(function () {
            //    chkImportClick();
            //});
            //$("#ctl00_HomeContentPlaceHolder_chkExport").click(function () {
            //    chkExportClick();
            //});
            //$("#ctl00_HomeContentPlaceHolder_chkMobile").click(function () {
            //    chkMobileClick();
            //});

            function chkExceedenceClick(bEvent) {
                var chk = document.getElementById("ctl00_HomeContentPlaceHolder_chkExceedence");
                var txtMax = document.getElementById("ctl00_HomeContentPlaceHolder_txtExceedenceMax");
                var txtMin = document.getElementById("ctl00_HomeContentPlaceHolder_txtExceedenceMin");
                if (chk.checked == true) {
                    txtMax.disabled = false;
                    txtMin.disabled = false;
                    if (txtMaxExceedance.value != "" && txtMax.value == "") {
                        txtMax.value = txtMaxExceedance.value;
                    }
                    if (txtMinExceedance.value != "" && txtMin.value == "") {
                        txtMin.value = txtMinExceedance.value;
                    }
                }
                else {
                    txtMax.value = '';
                    txtMin.value = '';
                    txtMax.disabled = true;
                    txtMin.disabled = true;
                }
            }
            $("#ctl00_HomeContentPlaceHolder_chkExceedence").click(function () {
                chkExceedenceClick(true);
            });

            function chkWarningClick(bEvent) {
                var chk = document.getElementById("ctl00_HomeContentPlaceHolder_chkWarning");
                var txtMax = document.getElementById("ctl00_HomeContentPlaceHolder_txtWarningMax");
                var txtMin = document.getElementById("ctl00_HomeContentPlaceHolder_txtWarningMin");
                if (chk.checked == true) {
                    txtMax.disabled = false;
                    txtMin.disabled = false;
                    if (txtMaxWrning.value != "" && txtMax.value == "") {
                        txtMax.value = txtMaxWrning.value;
                    }
                    if (txtMinWaring.value != "" && txtMin.value == "") {
                        txtMin.value = txtMinWaring.value;
                    }
                }
                else {
                    txtMax.value = "";
                    txtMin.value = "";
                    txtMax.disabled = true;
                    txtMin.disabled = true;
                }
            }

            $("#ctl00_HomeContentPlaceHolder_chkWarning").click(function () {
                chkWarningClick(true);
            });

            $("#chkCompareOperator").click(function () {
                chkCompareOperatorClick();
            });

            function chkMaximumValueatClick(bEvent) {
                var chk = document.getElementById("ctl00_HomeContentPlaceHolder_chkMaximumValueat");
                var txt = document.getElementById("ctl00_HomeContentPlaceHolder_txtMaximumValueat");
                if (chk.checked == true) {
                    txt.disabled = false;
                }
                else {
                    txt.value = '';
                    txt.disabled = true;
                }
            }
            $("#ctl00_HomeContentPlaceHolder_chkMaximumValueat").click(function () {
                chkMaximumValueatClick(true);
            });

            function ddDDDisplayColumnChange(bEvent) {
                var ddDDDisplayColumn = document.getElementById('ddDDDisplayColumn');
                var hlDDEdit = document.getElementById('hlDDEdit');
                var strColumnValue = ddDDDisplayColumn.value;// $('#ddDDDisplayColumn').val();

                if (strColumnValue != '') {
                    hlDDEdit.style.display = 'none';
                    document.getElementById('hfDisplayColumnsFormula').value = '[' + $('#ddDDDisplayColumn option:selected').text() + ']';

                    ddlDefaultValueChange(true);//RM
                    //$("#ddDDDisplayColumnDefaultValue").show();//RM
                    //$("#ctl00_HomeContentPlaceHolder_txtDefaultValue").hide();//RM
                }
                else {
                    hlDDEdit.style.display = 'block';

                    ddlDefaultValueChange(true);//RM
                    //$("#ddDDDisplayColumnDefaultValue").hide();//RM
                    //$("#ctl00_HomeContentPlaceHolder_txtDefaultValue").show();//RM
                }

                document.getElementById('hlDDEdit').href = '../Help/TableColumn.aspx?formula=' + encodeURIComponent(document.getElementById('hfDisplayColumnsFormula').value) + '&Tableid=' + $('#ddlDDTable').val();

            }
            $('#ddDDDisplayColumn').change(function (e) {
                $("#ddlDefaultValue").val($("#ddlDefaultValue option:first").val()); //red 12082017
                ddDDDisplayColumnChange(true);
                //ddDDDisplayColumnChange(true); //red 22092017

            });

            function ddlOptionTypeChange(bEvent) {
                var strOptionType = $('#ddlOptionType').val();
                $('#trOptionImageGrid').hide();
                $('#trDDValues').show();
                if (strOptionType == 'values') {
                    $("#ctl00_HomeContentPlaceHolder_lblDropdownValuesCap").text('Values');
                    $("#ctl00_HomeContentPlaceHolder_lblDropdownValuesHelp").text('Enter the values – one on each line');
                }
                else if (strOptionType == 'value_text') {
                    $("#ctl00_HomeContentPlaceHolder_lblDropdownValuesCap").text('Value & Text');
                    $("#ctl00_HomeContentPlaceHolder_lblDropdownValuesHelp").text('Enter value, comma(,) and text – one on each line');
                }
                else {
                    $('#trOptionImageGrid').show();
                    $('#trDDValues').hide();
                }
            }

            $('#ddlOptionType').change(function (e) {
                ddlOptionTypeChange(true);
            });

            //red reset the value to none 31082017
            $('#hlDDEdit').click(function (e) {
                //alert('red');
                $("#ddlDefaultValue").val($("#ddlDefaultValue option:first").val()); //red 12082017
                ddDDDisplayColumnChange(true); //red 31082017
            });

            //red reset the value to none 31082017
            $('#hlFiltered').click(function (e) {
                //alert('red');
                $("#ddlDefaultValue").val($("#ddlDefaultValue option:first").val()); //red 12082017
                ddDDDisplayColumnChange(true); //red 31082017
            });

            //red reset the value to none 31082017
            $("#chkFiltered").click(function () {

                //alert('red');
                $("#ddlDefaultValue").val($("#ddlDefaultValue option:first").val()); //red 12082017
                ddDDDisplayColumnChange(true); //red 31082017

            });

            function ddlListBoxTypeChange(bEvent) {
                var strOptionType = $('#ddlListBoxType').val();
                $("#divQuickAddLink").hide();
                if (strOptionType == 'values') {
                    $("#trDDTable").hide();
                    $("#trDDTableLookup").hide();
                    $("#trDDDisplayColumn").hide();
                    $("#trDDValues").show();
                    $("#ctl00_HomeContentPlaceHolder_lblDropdownValuesCap").text('Values');
                    $("#ctl00_HomeContentPlaceHolder_lblDropdownValuesHelp").text('Enter the values – one on each line');

                }
                else if (strOptionType == 'value_text') {
                    $("#trDDTable").hide();
                    $("#trDDTableLookup").hide();
                    $("#trDDDisplayColumn").hide();
                    $("#trDDValues").show();
                    $("#ctl00_HomeContentPlaceHolder_lblDropdownValuesCap").text('Value & Text');
                    $("#ctl00_HomeContentPlaceHolder_lblDropdownValuesHelp").text('Enter value, comma(,) and text – one on each line');

                }
                else {
                    $("#trDDTable").show();
                    $("#trDDTableLookup").show();
                    $("#trDDDisplayColumn").show();
                    $("#trDDValues").hide();

                }
            }

            $('#ddlListBoxType').change(function (e) {
                ddlListBoxTypeChange(true);
            });



            function ddlDDTypeChange(bEvent) {
                strDDType = $('#ddlDDType').val();

                if (strDDType == 'values') {
                    $('#ctl00_HomeContentPlaceHolder_lnkCreateTable').show();
                }
                else {
                    $('#ctl00_HomeContentPlaceHolder_lnkCreateTable').hide();
                }

                if (strDDType == 'ct' || strDDType == 'lt') {
                    $('#trDDTable').show();
                    $("#trDDTableLookup").show();
                    $('#trDDDisplayColumn').show();
                    $('#trDDValues').hide();
                    // $("#trDefaultValue").hide();                  
                    var txtDefaultValue = document.getElementById("ctl00_HomeContentPlaceHolder_txtDefaultValue");
                    txtDefaultValue.value = '';
                    $("#trTextDimension").show();
                    $('#tdHeightLabel').hide();
                    $('#tdHeight').hide();

                    $("#tdPredictiveAndFilter").show();
                    //var chkFilterValues = document.getElementById("chkFilterValues");
                    if (strDDType == 'lt') {
                        $("#trFilter").show();
                        $("#tdPredictiveAndFilter").hide();
                        $("#lblDisplayColumn").text('2nd Dropdown');
                        $("#divQuickAddLink").hide();
                    }
                    else {
                        if (bEvent = false) {
                            ddDDDisplayColumnChange(false);
                        }

                        $("#trFilter").hide();
                        $("#tdPredictiveAndFilter").show();
                        $("#lblDisplayColumn").text('Display Field');
                        $("#divQuickAddLink").show();
                        //document.getElementById('hlFiltered').href = 'Filtered.aspx?hfFilterOperator=' + encodeURIComponent(document.getElementById('hfFilterOperator').value) + '&hfFilterParentColumnID=' + encodeURIComponent(document.getElementById('hfFilterParentColumnID').value) + "&hfFilterOtherColumnID=" + encodeURIComponent(document.getElementById("hfFilterOtherColumnID").value) + "&hfFilterValue=" + encodeURIComponent(document.getElementById("hfFilterValue").value) + "&ParentTableID=" + encodeURIComponent($('#ddlDDTable').val()) + "&Tableid=" + document.getElementById("ctl00_HomeContentPlaceHolder_hfTableID").value + "&Columnid=" + document.getElementById("ctl00_HomeContentPlaceHolder_hfColumnID").value;
                        document.getElementById('hlFiltered').href = 'Filtered.aspx?hfFilterOperator=' + encodeURIComponent(document.getElementById('hfFilterOperator').value) + '&hfFilterParentColumnID=' + encodeURIComponent(document.getElementById('hfFilterParentColumnID').value) + "&hfFilterOtherColumnID=" + encodeURIComponent(document.getElementById("hfFilterOtherColumnID").value) + "&hfFilterValue=" + encodeURIComponent(document.getElementById("hfFilterValue").value)
                            + '&hfFilterOperator2=' + encodeURIComponent(document.getElementById('hfFilterOperator2').value) + '&hfFilterParentColumnID2=' + encodeURIComponent(document.getElementById('hfFilterParentColumnID2').value) + "&hfFilterOtherColumnID2=" + encodeURIComponent(document.getElementById("hfFilterOtherColumnID2").value) + "&hfFilterValue2=" + encodeURIComponent(document.getElementById("hfFilterValue2").value)
                            + "&ParentTableID=" + encodeURIComponent($('#ddlDDTable').val()) + "&Tableid=" + document.getElementById("ctl00_HomeContentPlaceHolder_hfTableID").value + "&Columnid=" + document.getElementById("ctl00_HomeContentPlaceHolder_hfColumnID").value;

                    }
                }
                if (strDDType == 'values' || strDDType == 'value_text') {
                    $('#trDDValues').show();
                    $('#trDDTable').hide();
                    $("#trDDTableLookup").hide();
                    $('#trDDDisplayColumn').hide();
                    $("#trDefaultValue").show();

                    $("#trTextDimension").show();
                    $('#tdHeightLabel').hide();
                    $('#tdHeight').hide();

                    $('#trFilter').hide();
                    $("#tdPredictiveAndFilter").hide();

                    if (strDDType == 'values') {
                        $("#ctl00_HomeContentPlaceHolder_lblDropdownValuesCap").text('Values');
                        $("#ctl00_HomeContentPlaceHolder_lblDropdownValuesHelp").text('Enter the values – one on each line');
                    }
                    else {
                        $("#ctl00_HomeContentPlaceHolder_lblDropdownValuesCap").text('Value & Text');
                        $("#ctl00_HomeContentPlaceHolder_lblDropdownValuesHelp").text('Enter value, comma(,) and text – one on each line');
                    }
                }


                //                if (strDDType == 'linked') {
                //                    $('#trDDTable').show();
                //                    $('#trDDDisplayColumn').show();
                //                    $('#trDDValues').hide();
                //                    $("#trDefaultValue").hide();
                //                    var txtDefaultValue = document.getElementById("ctl00_HomeContentPlaceHolder_txtDefaultValue");
                //                    txtDefaultValue.value = '';
                //                    $("#trTextDimension").show();
                //                    $('#tdHeightLabel').hide();
                //                    $('#tdHeight').hide();
                //                    $('#trFilter').show();
                //                    $("#tdPredictiveAndFilter").show();
                //                }

            }
            $('#ddlDDType').change(function (e) {
                ddlDDTypeChange(true);


            });
            function ddlDDTable_Change(bEvent) {
                var strDDTableID = $('#ddlDDTable').val();
                var hlDDEdit = document.getElementById('hlDDEdit');
                if (strDDTableID == '-1') {
                    if (strTypeV == 'dropdown') {
                        $("#tdTableFilter").hide();
                        $("#divQuickAddLink").hide();
                    }

                    document.getElementById('hfDisplayColumnsFormula').value = 'email';
                    if (hlDDEdit != null) {
                        hlDDEdit.style.display = 'none';
                    }

                }
                else {
                    document.getElementById('hfDisplayColumnsFormula').value = ''; //red 22092017
                    if (strTypeV == 'dropdown') {
                        $("#tdTableFilter").show();
                        $("#divQuickAddLink").show();
                    }
                    document.getElementById('hfDisplayColumnsFormula').value = '';
                    document.getElementById('hlDDEdit').href = '../Help/TableColumn.aspx?formula=' + encodeURIComponent(document.getElementById('hfDisplayColumnsFormula').value) + '&Tableid=' + $('#ddlDDTable').val();


                    //document.getElementById('hlFiltered').href = 'Filtered.aspx?hfFilterOperator=' + encodeURIComponent(document.getElementById('hfFilterOperator').value) + '&hfFilterParentColumnID=' + encodeURIComponent(document.getElementById('hfFilterParentColumnID').value) + "&hfFilterOtherColumnID=" + encodeURIComponent(document.getElementById("hfFilterOtherColumnID").value) + "&hfFilterValue=" + encodeURIComponent(document.getElementById("hfFilterValue").value) + "&ParentTableID=" + encodeURIComponent($('#ddlDDTable').val()) + "&Tableid=" + document.getElementById("ctl00_HomeContentPlaceHolder_hfTableID").value + "&Columnid=" + document.getElementById("ctl00_HomeContentPlaceHolder_hfColumnID").value;
                    document.getElementById('hlFiltered').href = 'Filtered.aspx?hfFilterOperator=' + encodeURIComponent(document.getElementById('hfFilterOperator').value) + '&hfFilterParentColumnID=' + encodeURIComponent(document.getElementById('hfFilterParentColumnID').value) + "&hfFilterOtherColumnID=" + encodeURIComponent(document.getElementById("hfFilterOtherColumnID").value) + "&hfFilterValue=" + encodeURIComponent(document.getElementById("hfFilterValue").value)
                        + '&hfFilterOperator2=' + encodeURIComponent(document.getElementById('hfFilterOperator2').value) + '&hfFilterParentColumnID2=' + encodeURIComponent(document.getElementById('hfFilterParentColumnID2').value) + "&hfFilterOtherColumnID2=" + encodeURIComponent(document.getElementById("hfFilterOtherColumnID2").value) + "&hfFilterValue2=" + encodeURIComponent(document.getElementById("hfFilterValue2").value)
                        + "&ParentTableID=" + encodeURIComponent($('#ddlDDTable').val()) + "&Tableid=" + document.getElementById("ctl00_HomeContentPlaceHolder_hfTableID").value + "&Columnid=" + document.getElementById("ctl00_HomeContentPlaceHolder_hfColumnID").value;



                }
            }

            $('#ddlDDTable').change(function (e) {

                ddlDDTable_Change(true);
                ddlDefaultValueChange(true); //red 22092017
            });

            function ddlTextTypeClick() {
                strTextType = $('#ddlTextType').val();
                if (strTextType == "own") {
                    $("#txtOwnRegEx").show();
                    $("#hlRegEx").show();
                }
                else {
                    $("#txtOwnRegEx").hide();
                    $("#hlRegEx").hide();
                }

                //if (strTextType == "readonly") {

                //    $(ddlImportance).val('');
                //    $("#trMandatory").hide();
                //}
                //else {
                //    $("#trMandatory").show();

                //}
            }


            $('#ddlTextType').change(function (e) {

                ddlTextTypeClick();

            });

            $('#ctl00_HomeContentPlaceHolder_ddlType').change(function (e) {
                ResetColumnType(true);
            });
            function ddlDateTimeTypeChange(bEvent) {
                var strDateTimeTypeV = $('#ddlDateTimeType').val();
                //$("#trDefaultValue").show();
                /* == Red: we'll be using Special Notification == */
                //if (strDateTimeTypeV == 'datetime' || strDateTimeTypeV == 'date') {
                //    $("#trReminders").show();

                //}
                //else {
                //    $("#trReminders").hide();                    
                //}

                //Red 3661   
                if (strDateTimeTypeV == 'datetime' || strDateTimeTypeV == 'time') {
                    $("#tdChkSecond").show();
                    $('#ddlDefaultValue option[value="additional"]').text('Add X Hours');
                  
                }
                else {
                    $("#tdChkSecond").hide();
                    ddlCalculationFormatTypeChange(false);
                }

                if (strDateTimeTypeV == 'date') {
                    $('#ddlDefaultValue option[value="additional"]').text('Add X Days');
                   
                }

                //KG 27-02-2017 ticket 2209
                ManageFormula();
                $(".formula").hide();
                $(".conditions").hide();
            }



            $('#ddlDateTimeType').change(function (e) {     
                ddlDateTimeTypeChange(true);
            });

            function ddlDefaultValueChange(bEvent) {
                var strDefaultValue = $('#ddlDefaultValue').val();
               
                //var ddDDDisplayColumn = document.getElementById('ddDDDisplayColumn');
                if (strDefaultValue == null || strDefaultValue == '') {
                    strDefaultValue = $('#hf_ddlDefaultValue').val();
                    if (strDefaultValue == null || strDefaultValue == '') {
                        strDefaultValue = 'none';
                    }
                }
                
                //Red Ticket 2060 - I added the ddDDDisplayColumnDefaultValue for hiding and showing
                //<Begin 2060>
                if (strDefaultValue == 'none') {
                    $("#ctl00_HomeContentPlaceHolder_txtDefaultValue").hide();
                    $("#tdDefaultParent").hide();
                    $("#tdDefaultSyncData").hide();
                    $("#ddDDDisplayColumnDefaultValue").hide();
                }
                //Red Removed this...
                //if (strDefaultValue == 'value') {
                //    $("#ctl00_HomeContentPlaceHolder_txtDefaultValue").show();
                //    $("#tdDefaultParent").hide();
                //    $("#tdDefaultSyncData").hide();
                //}

                if (strDefaultValue == 'value') {

                    strDDType = $('#ddlDDType').val();

                    if (strDDType == 'ct') { //&& ddDDDisplayColumn.value != '') {
                        $("#ddDDDisplayColumnDefaultValue").show();
                        $("#ctl00_HomeContentPlaceHolder_txtDefaultValue").hide();
                        $("#tdDefaultParent").hide();
                        $("#tdDefaultSyncData").hide();


                    }
                    else {
                        $("#ctl00_HomeContentPlaceHolder_txtDefaultValue").show();
                        $("#tdDefaultParent").hide();
                        $("#tdDefaultSyncData").hide();
                        $("#ddDDDisplayColumnDefaultValue").hide();
                    }

                }
                if (strDefaultValue == 'parent') {
                    $("#ctl00_HomeContentPlaceHolder_txtDefaultValue").hide();
                    $("#tdDefaultParent").show();
                    $("#tdDefaultSyncData").show();
                    $("#ddDDDisplayColumnDefaultValue").hide();
                }

                if (strDefaultValue == 'login' || strDefaultValue == 'currentuser') {
                    $("#ctl00_HomeContentPlaceHolder_txtDefaultValue").hide();
                    $("#tdDefaultParent").hide();
                    $("#tdDefaultSyncData").hide();
                    $("#ddDDDisplayColumnDefaultValue").hide();
                }
                //<End 2060>
                if (strTypeV == 'date_time') {
                    $("#ctl00_HomeContentPlaceHolder_txtDefaultValue").hide();
                    $("#ctl00_HomeContentPlaceHolder_lblDefauleValue").text('Default');
                    $('#ddlDefaultValue option[value="value"]').text('Today/Now');
                }
                else {
                    $("#ctl00_HomeContentPlaceHolder_lblDefauleValue").text('Default Value');
                    $('#ddlDefaultValue option[value="value"]').text('Value');
                }

                if (strDefaultValue == 'additional') {
                    $("#ctl00_HomeContentPlaceHolder_txtDefaultValue").show();
                    $("#tdDefaultParent").hide();
                    $("#tdDefaultSyncData").hide();
                    $("#ddDDDisplayColumnDefaultValue").hide();
                }

            }

            $('#ddDDDisplayColumnDefaultValue').change(function (e) {

                $('#hf_ddDDDisplayColumnDefaultValue').val($('#ddDDDisplayColumnDefaultValue').val());
            });


            $(document).ready(function () {
                GetRecordColumnFormula();
            });

            $('#chkIgnoreSymbols').change(manageSymbolMode);

            function manageSymbolMode() {
                if ($('#chkIgnoreSymbols').prop('checked')) {
                    $('#panelSymbolMode').show();
                    manageSymbolConstant();
                } else
                    $('#panelSymbolMode').hide();
            }

            $('#ddlSymbolMode').change(manageSymbolConstant);

            function manageSymbolConstant() {
                if ($('#ddlSymbolMode').val() === "4") {
                    $('[id*="txtSymbolModeConstant"]').show();
                    $('[id*="cvSymbolModeConstant"]').show();
                } else {
                    $('[id*="txtSymbolModeConstant"]').hide();
                    $('[id*="cvSymbolModeConstant"]').hide();
                }
            }

            $('#ddlDefaultValue').change(function (e) {
                ddlDefaultValueChange(true);
                GetRecordColumnFormula();
                
            });

            function populateDefaultDropdown() {
            }

            function ddlNumberTypeChange(bEvent) {
                var strNumberTypeV = $('#ctl00_HomeContentPlaceHolder_ddlNumberType').val();
                var chkIgnoreSymbols = document.getElementById("chkIgnoreSymbols");
                var chkRound = document.getElementById("ctl00_HomeContentPlaceHolder_chkRound");
                var chkCheckUnlikelyValue = document.getElementById("ctl00_HomeContentPlaceHolder_chkCheckUnlikelyValue");
                var chkFlatLine = document.getElementById("chkFlatLine");

                //normal=1,4=avg, Financial=6,   7=slider, 8=id,constant=2,5=rc,

                if (ColumnTypeIn(strNumberTypeV, '8')) {
                    if (hfColumnID.value != -1) {
                        $("#hlResetIDs").show();
                    }
                    else {
                        $("#hlResetIDs").hide();
                    }
                }
                else {
                    $("#hlResetIDs").hide();
                }

                if (ColumnTypeIn(strNumberTypeV, '7')) {
                    $("#trSlider").show();
                    if (bEvent) {
                        //default
                    }
                }
                else {
                    $("#trSlider").hide();
                }

                if (ColumnTypeIn(strNumberTypeV, '5')) {
                    $("#trRecordCountTable").show();
                    $("#trRecordCountClick").show();
                }
                else {
                    $("#trRecordCountTable").hide();
                    $("#trRecordCountClick").hide();
                }

                if (ColumnTypeIn(strNumberTypeV, '2')) {
                    $("#ctl00_HomeContentPlaceHolder_lblConstant").show();
                    $("#ctl00_HomeContentPlaceHolder_txtConstant").show();
                }
                else {
                    $("#ctl00_HomeContentPlaceHolder_lblConstant").hide();
                    $("#ctl00_HomeContentPlaceHolder_txtConstant").hide();
                    var txtConstant = document.getElementById("ctl00_HomeContentPlaceHolder_txtConstant");
                    txtConstant.value = '';
                }

                if (ColumnTypeIn(strNumberTypeV, '1,6,4')) {
                    $("#trMandatory").show();
                }
                else {
                    $("#trMandatory").hide();
                    $(ddlImportance).val('');
                }
                if (ColumnTypeIn(strNumberTypeV, '1,6,4')) {
                    $("#trRound").show(); chkRoundClick(false);
                    $("#trIgnoreSymbols").show(); manageSymbolMode();
                    manageSymbolConstant();
                    $("#trCheckUnlikelyValue").show();
                    $("#trFlatLine").show(); chkFlatLineClick(false);
                }
                else {
                    $("#trRound").hide(); chkRound.checked = false; chkRoundClick(false);
                    $("#trIgnoreSymbols").hide(); chkIgnoreSymbols.checked = false;
                    manageSymbolMode();
                    $("#trCheckUnlikelyValue").hide(); chkCheckUnlikelyValue.checked = false;
                    $("#trFlatLine").hide(); chkFlatLine.checked = false; chkFlatLineClick(false);
                }
                if (ColumnTypeIn(strNumberTypeV, '1,6,7')) {
                    //$("#trImportOption").show();
                    $("#trDefaultValue").show();

                }
                else {
                    //$("#trImportOption").hide();
                    //chkImport.checked = false;
                    $("#trDefaultValue").hide();
                }


                if (ColumnTypeIn(strNumberTypeV, '1,4,5,6,7')) {
                    $("#divchkCompareOperator").show();
                }
                else {
                    var chkCompareOperator = document.getElementById("chkCompareOperator");
                    $("#divchkCompareOperator").show(); chkCompareOperator.checked = false;
                }
                if (ColumnTypeIn(strNumberTypeV, '4')) {
                    $("#trColumnToAvg").show();
                    $("#trAvgNumValues").show();
                }
                else {
                    $("#trColumnToAvg").hide();
                    $("#trAvgNumValues").hide();
                }

                if (ColumnTypeIn(strNumberTypeV, '6')) {
                    $("#trCurrencySymbol").show();
                    //$("#ctl00_HomeContentPlaceHolder_lblSymbol").show();
                    //$("#ctl00_HomeContentPlaceHolder_txtSymbol").show();
                }
                else {
                    $("#trCurrencySymbol").hide();
                    //$("#ctl00_HomeContentPlaceHolder_lblSymbol").hide();
                    //$("#ctl00_HomeContentPlaceHolder_txtSymbol").hide();
                }
                chkCompareOperatorClick();
                //chkImportClick();



                if (bEvent) { $("#ddlDefaultValue").val($("#ddlDefaultValue option:first").val()); }
                ddlDefaultValueChange(false);
                ManageFormula();

            }

            $('#ctl00_HomeContentPlaceHolder_ddlNumberType').change(function (e) {
                ddlNumberTypeChange(true);
            });
            function chkRoundClick(bEvent) {
                var chk = document.getElementById("ctl00_HomeContentPlaceHolder_chkRound");
                var txt = document.getElementById("ctl00_HomeContentPlaceHolder_txtRoundNumber");
                if (chk.checked == true) {
                    $("#ctl00_HomeContentPlaceHolder_txtRoundNumber").show();

                    if (txt.value == '') { txt.value = '2'; }


                }
                else {
                    $("#ctl00_HomeContentPlaceHolder_txtRoundNumber").hide();
                    txt.value = '';
                    //$("#ctl00_HomeContentPlaceHolder_lblRoundNumber").hide();

                }
            }

            $("#ctl00_HomeContentPlaceHolder_chkRound").click(function () {
                chkRoundClick(true);
            });

            //$("#ctl00_HomeContentPlaceHolder_optSingle").click(function () {
            //    var optSingle = document.getElementById("ctl00_HomeContentPlaceHolder_optSingle");
            //    if (optSingle.checked == true) {
            //        $("#trTimeSection").hide();
            //        if (hfIsImportPositional.value == "0") {
            //            $("#ctl00_HomeContentPlaceHolder_lblImport").text("Label");
            //            txtNameOn.value = "Date Time Recorded";
            //        }
            //        else {
            //            $("#ctl00_HomeContentPlaceHolder_lblImport").text("Position");

            //        }
            //    }

            //});

            //$("#ctl00_HomeContentPlaceHolder_optDouble").click(function () {
            //    var optDouble = document.getElementById("ctl00_HomeContentPlaceHolder_optDouble");
            //    if (optDouble.checked == true) {
            //        $("#trTimeSection").show();
            //        if (hfIsImportPositional.value == "0") {
            //            $("#ctl00_HomeContentPlaceHolder_lblImport").text("Date Label");

            //            txtNameOnImport.value = "Date Recorded";
            //            document.getElementById("ctl00_HomeContentPlaceHolder_txtNameOnImportTime").value = "Time Recorded";

            //        }
            //        else {
            //            $("#ctl00_HomeContentPlaceHolder_lblImport").text("Date Position");


            //        }
            //    }
            //});

            function chkFlatLineClick(bEvent) {
                var chk = document.getElementById("chkFlatLine");
                var txt = document.getElementById("txtFlatLine");
                if (chk.checked == true) {
                    $("#lblFlatlineNumber").show();
                    $("#txtFlatLine").show();
                    if (bEvent)
                        txt.value = '';
                }
                else {
                    $("#lblFlatlineNumber").hide();
                    $("#txtFlatLine").hide();
                    txt.value = '';
                }
            }

            $("#chkFlatLine").click(function () {
                chkFlatLineClick(true);
            });

            function chkShowMapClick(bEvent) {
                var chk = document.getElementById("chkShowMap");
                if (chk.checked == true) {
                    $("#tblMapDimension").show();
                    $("#trHoverText").show();

                }
                else {
                    $("#tblMapDimension").hide();
                    $("#trHoverText").hide();
                }
            }

            $("#chkShowMap").click(function () {
                chkShowMapClick(true);
            });



            if (hfShowExceedance != null) {
                if (hfShowExceedance.value == 'no') {
                    $("#trExceedance1").hide();
                    $("#trExceedance2").hide();
                }
            }

            var hfGOD = document.getElementById("hfGOD");
            if (hfGOD != null && hfColumnID.value != -1) {
                if (hfGOD.value == 'yes') {
                    $("#trResetValidation").show();
                }
            }

            var hfHideFormula = document.getElementById("hfHideFormula");
            if (hfHideFormula != null) {
                if (hfHideFormula.value == 'yes') {
                    $(".formula").hide();
                }
            }
            var hfHideConditions = document.getElementById("hfHideConditions");
            if (hfHideConditions != null) {
                if (hfHideConditions.value == 'yes') {
                    $(".conditions").hide();
                }
            }
            //if (hfShowWarningMinMax.value == 'no') {
            //    ShowWarningMinMax('no');
            //}
            function ShowHide() {
                ResetColumnType(false);
            }

            ShowHide();

            /* == Red hide this per Jon == */
            if (hfColumnSystemname.value == "recordid") {
                $("#divColumnType").hide();
                $("#divchkValidationCanIgnore").hide();
                $("#divchkCompareOperator").hide();
                $("#trRowSpan").hide();
            }


        });



        function GetRecordColumnFormula() {
            //alert('hf:' + $('#hf_ddlDDTable').val());
            //alert('Not-hf:' + $('#ddlDDTable').val());
            var ddlDDTableSelected = $('#hf_ddlDDTable').val();
            if ($('#hf_ddlDDTable').val() != $('#ddlDDTable').val() && $('#ddlDDTable').val() != ""
                && $('#ddlDDTable').val() != null) {
                ddlDDTableSelected = $('#ddlDDTable').val();
            }

            var ddlDDDefaultSelected = $('#hf_ddlDefaultValue').val();
            if ($('#hf_ddlDefaultValue').val() != $('#ddlDefaultValue').val() && $('#ddlDefaultValue').val() != ""
                && $('#ddlDefaultValue').val() != null) {
                ddlDDDefaultSelected = $('#ddlDefaultValue').val();
            }
            //alert('tableid: ' + ddlDDTableSelected);
            //red 31082017

            var FilterOperator = document.getElementById('hfFilterOperator').value;
            var FilterParentColumnID = document.getElementById('hfFilterParentColumnID').value;
            var FilterValue = document.getElementById('hfFilterValue').value;

            //KG Ticket 3985 9/7/18 2nd Filter 
            var FilterOperator2 = document.getElementById('hfFilterOperator2').value;
            var FilterParentColumnID2 = document.getElementById('hfFilterParentColumnID2').value;
            var FilterValue2 = document.getElementById('hfFilterValue2').value;

            var chkFiltered = document.getElementById("chkFiltered");
            if (chkFiltered.checked != true) {
                FilterOperator = '';
                FilterParentColumnID = '';
                FilterValue = '';

                FilterOperator2 = '';
                FilterParentColumnID2 = '';
                FilterValue2 = '';
            }
            //end red 31082017

            //red 12082017
            var ddDDDisplayColumn = document.getElementById('ddDDDisplayColumn');
            var strAD = ddDDDisplayColumn.value;
            var strColumnFormula = $('#hfDisplayColumnsFormula').val();
            //alert($('#hf_ddlDefaultValue').val());

            if (ddlDDDefaultSelected == 'value') {
                setTimeout(function () {

                    var strDefaultFormula = $('#hf_ddDDDisplayColumnDefaultValue').val();

                    var vExtraWhere = ' ';

                    $.ajax({
                        url: "../../CascadeDropdown.asmx/GetRecordColumnFormula",
                        data: "{'FormulaColumn':'" + strColumnFormula + "','strTableID': '" + ddlDDTableSelected + "'   , 'strOperator':'" + FilterOperator + "','strParentColumnID':'" + FilterParentColumnID + "','strFilterValue':'" + FilterValue + "'   , 'strOperator2':'" + FilterOperator2 + "','strParentColumnID2':'" + FilterParentColumnID2 + "','strFilterValue2':'" + FilterValue2 + "', 'extraWhere': '" + vExtraWhere + "'  }",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            $("#ddDDDisplayColumnDefaultValue").get(0).options.length = 0;
                            $("#ddDDDisplayColumnDefaultValue").get(0).options[0] = new Option("--Please Select--", "");

                            $.each(data.d, function (index, item) {
                                $("#ddDDDisplayColumnDefaultValue").get(0).options[$("#ddDDDisplayColumnDefaultValue").get(0).options.length] = new Option(item.Text, item.ID);
                                $("#ddDDDisplayColumnDefaultValue option[value='" + strDefaultFormula + "']").attr("selected", "selected");
                            });
                        },
                        error: function () {
                            $("#ddDDDisplayColumnDefaultValue").get(0).options.length = 0;
                        }
                    });


                }, 100);
            }


        }

        chkUploadPhoto_onChange($('#ctl00_HomeContentPlaceHolder_chkUploadPhoto'));

        function chkUploadPhoto_onChange(obj) {
            if (obj.checked) {
                $("#trMandatory").show();
            } else {
                $("#trMandatory").hide();
            }
        }


        function validateSymbolModeConstant(sender, args) {
            args.IsValid = false;
            var mode = $("#ddlSymbolMode").val();
            if (mode === "4") {
                var val = $("[id*='txtSymbolModeConstant']").val();
                if (val) {
                    args.IsValid = /^[+-]?(?:\d+\.?\d*|\d*\.?\d+)$/.test(val);
                } else {
                    args.IsValid = false;
                }
            } else
                args.IsValid = true;
        }

        //if (window.addEventListener)
        //    window.addEventListener("load", ShowHide, false);
        //else if (window.attachEvent)
        //    window.attachEvent("onload", ShowHide);
        //else if (document.getElementById)
        //    window.onload = ShowHide;

    </script>
    <div style="padding-left: 20px; padding-right: 20px; padding-bottom:150px;  height:auto;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" onload="ShowHide();">
            <tr>
                <td colspan="3">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" style="width: 50%;">
                                <span class="TopTitle">
                                    <asp:Label runat="server" ID="lblTitle"></asp:Label></span>
                            </td>
                            <td align="left">
                                <div style="width: 40px; height: 40px;">
                                    <%-- <asp:HyperLink runat="server" NavigateUrl="#" CssClass="validationlink" ID="hlWarningEdit"
                                                                                                                Style="display: none; top: 2px;">Edit</asp:HyperLink>--%>
                                </div>
                            </td>
                            <td style="padding-left: 100px;">
                                <div runat="server" id="divSaveGroup">
                                    <table>
                                        <tr>
                                            <td>
                                                <div>
                                                    <asp:HyperLink runat="server" ID="hlBack">
                                                        <asp:Image runat="server" ID="imgBack" ImageUrl="~/App_Themes/Default/images/Back.png"
                                                            ToolTip="Back" />
                                                    </asp:HyperLink>
                                                </div>
                                            </td>
                                            <td>
                                                <div runat="server" id="divEdit" visible="false">
                                                    <asp:HyperLink runat="server" ID="hlEditLink">
                                                        <asp:Image runat="server" ID="Image1" ImageUrl="~/App_Themes/Default/images/Edit_big.png"
                                                            ToolTip="Edit" />
                                                    </asp:HyperLink>
                                                </div>
                                            </td>
                                            <td>
                                                <div runat="server" id="divDelete">
                                                    <asp:LinkButton runat="server" ID="lnkDelete" OnClientClick="javascript:return confirm('Are you sure you want to delete this Field Permanently?')"
                                                        CausesValidation="false" OnClick="lnkDelete_Click">
                                                        <asp:Image runat="server" ID="Image3" ImageUrl="~/App_Themes/Default/images/delete_big.png"
                                                            ToolTip="Delete" />
                                                    </asp:LinkButton>
                                                </div>
                                            </td>
                                            <td>
                                                <div runat="server" id="divSave">
                                                    <asp:LinkButton runat="server" ID="lnkSave" OnClick="lnkSave_Click" CausesValidation="true" OnClientClick="return CheckEmailField();">
                                                        <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                                   ToolTip="Save" />
                                                    </asp:LinkButton>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <%-- <div style="float: left; margin-left: 40px;">--%>
                            </td>
                            <td align="right" style="padding-left: 50px;">
                                <asp:HyperLink runat="server" ID="hlHelpCommon" ClientIDMode="Static" NavigateUrl="~/Pages/Help/Help.aspx?contentkey=ColumnDetailHelp">
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/App_Themes/Default/images/help.png" />
                                </asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr style="height:auto">
                <td valign="top"></td>
                <td valign="top">
                    <div id="search" style="padding-bottom: 10px">
                    </div>
                    <div runat="server" id="divDetail">
                        <asp:HiddenField runat="server" ID="hfColumnID" Value="-1" />
                        <asp:HiddenField runat="server" ID="hfTableID" />
                        <%--<asp:RegularExpressionValidator ID="revtxtMinExceedance" ControlToValidate="txtMinExceedance"
                                                                                runat="server" ErrorMessage="Min must be a valid number." Display="Dynamic" ValidationExpression="(^-?\d{1,20}\.$)|(^-?\d{1,20}$)|(^-?\d{0,20}\.\d{1,10}$)">
                                                                            </asp:RegularExpressionValidator>--%><%--<asp:RegularExpressionValidator ID="revtxtMaxExceedance" ControlToValidate="txtMaxExceedance"
                                                                                runat="server" ErrorMessage="Max must be a valid number." Display="Dynamic" ValidationExpression="(^-?\d{1,20}\.$)|(^-?\d{1,20}$)|(^-?\d{0,20}\.\d{1,10}$)">
                                                                            </asp:RegularExpressionValidator>--%>
                        <asp:HiddenField runat="server" ID="hfShowWarningMinMax" Value="yes" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="hfShowExceedanceMinMax" Value="yes" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="hfShowValidMinMax" Value="yes" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="hfDateTimeColumn" Value="no" />
                        <asp:HiddenField runat="server" ID="hfColumnSystemname" Value="" ClientIDMode="Static" />

                        <asp:HiddenField runat="server" ID="hfShowExceedance" Value="no" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="hfHideFormula" Value="no" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="hfHideConditions" Value="no" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="hfConditions_T" Value="Conditions" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="hfRevalidationRequired" Value="no" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="hfGOD" Value="no" ClientIDMode="Static" />
                        <%--<asp:CompareValidator ID="cvtxtMinExceedance" runat="server" Operator="DataTypeCheck" Type="Double"
                                                                                                            ForeColor="Red"
                                                                                                            Display="Dynamic" SetFocusOnError="true"
                                                                                                            ControlToValidate="txtMinExceedance"
                                                                                                            ErrorMessage="Min must be a valid number.">
                                                                                                        </asp:CompareValidator>
                                                                                                        <asp:CompareValidator ID="cvtxtMaxExceedance" runat="server" Operator="DataTypeCheck" Type="Double"
                                                                                                            ForeColor="Red"
                                                                                                            Display="Dynamic" SetFocusOnError="true"
                                                                                                            ControlToValidate="txtMaxExceedance"
                                                                                                            ErrorMessage="Max must be a valid number.">
                                                                                                        </asp:CompareValidator>--%>


                        <asp:HiddenField runat="server" ID="hfIsSystemColumn" Value="" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="hfCalculationType" Value="" ClientIDMode="Static" />
                        <table cellpadding="3">
                            <tr>
                                <td align="right" runat="server" id="trTable">
                                    <strong runat="server" id="stgTableNameCaption">Table</strong>
                                </td>
                                <td colspan="6">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width: 300px;">
                                                <asp:TextBox runat="server" ID="txtTable" Enabled="false" CssClass="NormalTextBox"
                                                    Width="250px"></asp:TextBox>
                                            </td>
                                            <td style="padding-left: 50px;" runat="server" visible="false">
                                                <%--<asp:CheckBox runat="server" ID="chkSummarySearch" Text="Search Field" Font-Bold="true"
                                                    TextAlign="Right" />--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <strong runat="server" id="stgFieldNameCap">Field Name*</strong>
                                </td>
                                <td colspan="6">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="width: 300px;">
                                                <asp:TextBox ID="txtColumnName" runat="server" Width="250px" CssClass="NormalTextBox"></asp:TextBox>

                                            </td>
                                            <%--<td style="padding-left: 50px;">&nbsp;<strong>Visible To</strong>&nbsp;
                                                <asp:DropDownList runat="server" ID="ddlOnlyForAdmin" CssClass="NormalTextBox">
                                                 <asp:ListItem Value="0" Text="All Users" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Admin Only"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Own Data Only"></asp:ListItem>
                                            </asp:DropDownList>
                                            </td>--%>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblSystemName" Visible="false" Style="color: #C0C0C0"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="6"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td align="right"></td>
                                <td colspan="6">
                                    <asp:Label runat="server" ID="lblColumnMessage" Text="Note this is the field used as the date on the graphs"
                                        Visible="false"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rfvColumnName" runat="server" ControlToValidate="txtColumnName"
                                        ErrorMessage="Field Name - Required"></asp:RequiredFieldValidator>
                                </td>
                                <td></td>
                            </tr>

                            <tr>
                                <td colspan="8" style="height: 10px;"></td>
                            </tr>
                            <tr>
                                <td colspan="8">
                                    <table cellpadding="3">
                                        <tr>
                                            <td valign="top">
                                                <div style="border-style: solid; border-width: 1px; width: 500px; min-height: 140px; padding: 5px;">
                                                    <table>
                                                        <tr id="trSummaryPage1">
                                                            <td align="right">
                                                                <asp:CheckBox ID="chkSummaryPage" runat="server" ToolTip="Available for selection on views" Checked="true" />
                                                            </td>
                                                            <td align="left" colspan="2">

                                                                <strong>Views</strong>

                                                            </td>
                                                            <td align="right">
                                                                <asp:Label runat="server" ID="lblSummaryPage" Text="Heading" Font-Bold="true" ToolTip="Available for selection on views"></asp:Label>

                                                            </td>
                                                            <td>
                                                                <asp:TextBox ToolTip="Available for selection on views" runat="server" ID="txtDisplayTextSummary" CssClass="NormalTextBox" Width="250px"></asp:TextBox>
                                                            </td>
                                                            <td colspan="2"></td>
                                                        </tr>
                                                        <%--<asp:CompareValidator runat="server" ID="cvExceedanceRange" ControlToValidate="txtMinExceedance"
                                                                                ForeColor="Red"
                                                                                Display="Dynamic" SetFocusOnError="true" 
                                                                                ControlToCompare="txtMaxExceedance" Operator="LessThanEqual" Type="Double"
                                                                                ErrorMessage="Min must be less than Max.">
                                                                            </asp:CompareValidator>--%>                                                        <%--</div>--%>
                                                        <tr id="trDetailPage1">
                                                            <td align="right">
                                                                <asp:CheckBox ID="chkDetailPage" runat="server" />
                                                            </td>
                                                            <td>
                                                                <strong>Detail</strong>

                                                            </td>
                                                            <td></td>
                                                            <td align="right" style="width: 100px;">
                                                                <asp:Label runat="server" ID="lblDetailPage" Text="Label" Font-Bold="true"></asp:Label>
                                                            </td>

                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtDisplayTextDetail" Height="50px"
                                                                    CssClass="NormalTextBox" Width="250px" TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                            <td colspan="2"></td>
                                                        </tr>
                                                        <tr id="trDetailPage2">
                                                            <td align="right"></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td align="right" style="width: 100px;"></td>
                                                            <td colspan="3">
                                                                <%--<asp:CheckBox runat="server" ID="chkDisplayOnRight" Text="" TextAlign="Right" Font-Bold="true"
                                                                    ClientIDMode="Static" />--%>
                                                                <%--<asp:Label runat="server" ID="lblDisplayOnRight" Text="Display on the right" Font-Bold="true"
                                                                    ClientIDMode="Static"></asp:Label>--%>

                                                                <asp:CheckBox runat="server" ID="chkConditions" Text="" TextAlign="Right" Font-Bold="true"
                                                                    ClientIDMode="Static" />
                                                                <asp:HyperLink runat="server" NavigateUrl="~/Pages/Record/ShowHide.aspx" CssClass="showWhenLink"
                                                                    ID="hlConditions" ClientIDMode="Static">Show When...</asp:HyperLink>
                                                                <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfShowHref" />
                                                                <br />
                                                                <div runat="server" id="divTableTab">
                                                                    <strong>Page:</strong>
                                                                    <asp:DropDownList runat="server" ID="ddlTableTab" DataTextField="TabName" DataValueField="TableTabID"
                                                                        CssClass="NormalTextBox">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr id="trGraphOption">
                                                            <td>
                                                                <asp:CheckBox ID="chkGraph" runat="server" />
                                                            </td>
                                                            <td>
                                                                <strong>Graph</strong>
                                                            </td>
                                                            <td></td>
                                                            <td align="right" style="width: 100px;">
                                                                <asp:Label runat="server" ID="lblGraph" Text="Label" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtGraph" CssClass="NormalTextBox" Width="250px"></asp:TextBox>
                                                            </td>
                                                            <td colspan="2"></td>
                                                        </tr>
                                                        <%--<asp:HyperLink runat="server" NavigateUrl="#" CssClass="validationlink" ID="hlExceedanceEdit"
                                                                                                                Style="display: none; top: 2px;">Edit</asp:HyperLink>--%><%--<asp:ListItem Value="content" Text="Content Editor"></asp:ListItem>--%>
                                                        <tr>
                                                            <td colspan="7">
                                                                <!-- spacer to make it consistent -->
                                                            </td>
                                                        </tr>
                                                        <%--<tr id="trMobileSiteSummary">
                                                            <td align="right">
                                                                <asp:CheckBox ID="chkMobile" runat="server" />
                                                            </td>
                                                            <td>
                                                                <strong>Mobile</strong>
                                                            </td>
                                                            <td></td>
                                                            <td align="right" style="width: 100px;">
                                                                <asp:Label runat="server" ID="lblMobile" Text="Heading" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtMobile" CssClass="NormalTextBox" Width="250px"></asp:TextBox>
                                                            </td>
                                                            <td colspan="2"></td>
                                                        </tr>--%>

                                                        <tr>
                                                            <td colspan="7">
                                                                <table id="trDateFormat" style="display: none;" runat="server" clientidmode="Static">
                                                                    <tr>
                                                                        <td align="right" style="width: 94px;">
                                                                            <asp:Label runat="server" ID="lblDateFormat" Text="Date Format" Font-Bold="true"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="ddlDateFormat" CssClass="NormalTextBox">
                                                                                <asp:ListItem Text="DD/MM/YYYY" Value="DD/MM/YYYY" Selected="True"></asp:ListItem>
                                                                                <asp:ListItem Text="MM/DD/YYYY" Value="MM/DD/YYYY"></asp:ListItem>
                                                                                <asp:ListItem Text="YYYY-MM-DD" Value="YYYY-MM-DD"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>

                                                        </tr>

                                                    </table>
                                                </div>
                                                <br />
                                                <div id="divchkCompareOperator" style="display: none;">
                                                    <asp:CheckBox runat="server" ID="chkCompareOperator" ClientIDMode="Static" Text="Compare Values" Font-Bold="true" TextAlign="Right" />
                                                </div>


                                                <br />
                                                <div style="border-style: solid; border-width: 1px; width: 500px; padding: 5px; display: none; margin-top: 5px;"
                                                    id="divCompareOperator">
                                                    <table cellpadding="3">
                                                        <tr>
                                                            <td align="right">
                                                                <strong>Compare Operator</strong>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList runat="server" ID="ddlCompareOperator" CssClass="NormalTextBox" ClientIDMode="Static">
                                                                    <asp:ListItem Text="--Please Select--" Value="" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Text="Equal" Value="Equal"></asp:ListItem>
                                                                    <asp:ListItem Text="Not Equal" Value="NotEqual"></asp:ListItem>
                                                                    <asp:ListItem Text="Greater Than" Value="GreaterThan"></asp:ListItem>
                                                                    <asp:ListItem Text="Greater Than Equal" Value="GreaterThanEqual"></asp:ListItem>
                                                                    <asp:ListItem Text="Less Than" Value="LessThan"></asp:ListItem>
                                                                    <asp:ListItem Text="Less Than Equal" Value="LessThanEqual"></asp:ListItem>
                                                                    <asp:ListItem Text="Data Type Check" Value="DataTypeCheck"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:RequiredFieldValidator ID="rfvCompareOperator" runat="server" ControlToValidate="ddlCompareOperator" Display="Dynamic" SetFocusOnError="true"
                                                                    ErrorMessage="Required" ClientIDMode="Static"></asp:RequiredFieldValidator>

                                                            </td>


                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <strong>Compare Table</strong>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList runat="server" ID="ddlCompareTable" CssClass="NormalTextBox" ClientIDMode="Static">
                                                                </asp:DropDownList>
                                                                <ajaxToolkit:CascadingDropDown runat="server" ID="ddlCompareTableC" Category="Tableid"
                                                                    ClientIDMode="Static" TargetControlID="ddlCompareTable" ServicePath="~/CascadeDropdown.asmx"
                                                                    ServiceMethod="GetRelatedTablesCompare">
                                                                </ajaxToolkit:CascadingDropDown>
                                                                <%--<asp:ListItem Value="data_retriever" Text="Data Retriever"></asp:ListItem>--%>
                                                            </td>
                                                            <td>
                                                                <asp:RequiredFieldValidator ID="rfvCompareTable" runat="server" ControlToValidate="ddlCompareTable" Display="Dynamic" SetFocusOnError="true"
                                                                    ErrorMessage="Required" ClientIDMode="Static"></asp:RequiredFieldValidator>

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right">
                                                                <strong>Compare Column</strong>
                                                            </td>
                                                            <td align="left">
                                                                <%--<asp:LinkButton runat="server" ID="lnkAddTL5"  CausesValidation="false" OnClick="lnkAddTL5_Click">
                                                                                                    <asp:Image ID="Image12" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png" />
                                                                                                </asp:LinkButton>--%>

                                                                <asp:DropDownList runat="server" ID="ddlCompareColumnID" CssClass="NormalTextBox" ClientIDMode="Static">
                                                                </asp:DropDownList>

                                                                <ajaxToolkit:CascadingDropDown runat="server" ID="ddlCompareColumnIDC" Category="Column"
                                                                    ClientIDMode="Static" TargetControlID="ddlCompareColumnID" ServicePath="~/CascadeDropdown.asmx"
                                                                    ServiceMethod="GetCompareColumns" ParentControlID="ddlCompareTable">
                                                                </ajaxToolkit:CascadingDropDown>
                                                            </td>
                                                            <td>
                                                                <asp:RequiredFieldValidator ID="rfvCompareColumnID" runat="server" ControlToValidate="ddlCompareColumnID" Display="Dynamic" SetFocusOnError="true"
                                                                    ErrorMessage="Required" ClientIDMode="Static"></asp:RequiredFieldValidator>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <br />
                                                <div id="divchkValidationCanIgnore" style="display: none;">
                                                    <asp:CheckBox runat="server" onclick="allowWhenRoles();" ID="chkValidationCanIgnore"
                                                        Text="Allow user to ignore invalid data" Font-Bold="true" />

                                                </div>
                                                <div id="divAllowWhenRoles" style="padding: 0px 0px 0px 0px">
                                                              <asp:CheckBox ID="chkAllowWhenRoles" onclick="showRoles();" CssClass="chkbox" ClientIDMode="Static" runat="server" TextAlign="Right"
                                                                                Text=" When Role is/are:" Font-Bold="true"  />
                                                            <span style="font-size:10px;font-style:italic; color:gray;">(Unticked means all Roles can ignore.)</span>
                                                </div>
                                         
                                                                            
                                                                                   <div runat="server" id="divRoles" style="margin-top: 5px; margin-left: 3px; display:none;  padding-right: 5px">
                                                                            <asp:CheckBoxList CssClass="chkbox" runat="server" ID="ddlSelectedRoles" AutoPostBack="false" Style="display: block; overflow: auto; min-width: 300px; max-width: 300px; height: 140px; border: solid 1px #909090;">
                                                                             
                                                                            </asp:CheckBoxList>
                                                                            </div>
                                                <br />
                                                <div id="divValidationRoot" style="display: none;">
                                                    <div style="border-style: solid; border-width: 1px; width: 500px; min-height: 140px; padding: 5px;" id="divValidation">
                                                        <table cellpadding="3">
                                                            <tr>
                                                                <td colspan="3">
                                                                    <asp:Label runat="server" ID="lblValidationEntry" Text="Data invalid if outside the range" Font-Bold="true"></asp:Label>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" valign="top" style="width: 10px;"></td>
                                                                <td valign="top" style="padding-top: 5px;">
                                                                    <asp:TextBox runat="server" ID="txtValidationEntry" TextMode="MultiLine" CssClass="MultiLineTextBox"
                                                                        Width="250px" Height="60px"></asp:TextBox>

                                                                    <div id="divValidMinMax">
                                                                        <table cellpadding="3">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Label runat="server" ID="lblMinValid" Text="Value less than:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtMinValid" CssClass="NormalTextBox" Width="87px"></asp:TextBox>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <asp:Label runat="server" ID="lblMaxValid" Text="Value greater than:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtMaxValid" CssClass="NormalTextBox" Width="87px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>


                                                                    </div>

                                                                </td>
                                                                <td valign="top" style="padding-top: 7px;">

                                                                    <table cellpadding="3">
                                                                        <tr class="formula">
                                                                            <td>
                                                                                <asp:CheckBox runat="server" ID="chkValidFormula" ClientIDMode="Static" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:HyperLink runat="server" NavigateUrl="#" CssClass="validationlink" ID="hlValidAdvanced">Formula</asp:HyperLink>
                                                                                <%--<asp:TextBox runat="server" ID="txtButtonImage" CssClass="NormalTextBox" Width="400px" ToolTip="Image full URL."></asp:TextBox>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top" class="conditions">
                                                                            <td>
                                                                                <asp:CheckBox runat="server" ID="chkValidConditions" ClientIDMode="Static" />
                                                                            </td>
                                                                            <td>
                                                                                <div style="word-wrap: break-word;">
                                                                                    <asp:HyperLink runat="server" NavigateUrl="#" ClientIDMode="Static" ID="hlValidConditions">Conditions</asp:HyperLink>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <asp:Label runat="server" ID="lblWarningValidation" Text="Data Warning if outside the range"
                                                                        Font-Bold="true"></asp:Label>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" valign="top"></td>
                                                                <td valign="top" style="padding-top: 5px;">
                                                                    <asp:TextBox runat="server" ID="txtValidationOnWarning" TextMode="MultiLine" CssClass="MultiLineTextBox"
                                                                        Width="250px" Height="60px" Style="display: none;"></asp:TextBox>
                                                                    <div id="divWarningMinMax">

                                                                        <table cellpadding="3">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Label runat="server" ID="lblMinWarning" Text="Value less than:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtMinWaring" CssClass="NormalTextBox" Width="87px"></asp:TextBox>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <asp:Label runat="server" ID="lblMaxWarning" Text="Value greater than:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtMaxWrning" CssClass="NormalTextBox" Width="87px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>


                                                                    </div>


                                                                </td>
                                                                <td valign="top" style="padding-top: 7px;">
                                                                    <table cellpadding="3">
                                                                        <tr class="formula">
                                                                            <td>
                                                                                <asp:CheckBox runat="server" ID="chkWarningFormula" ClientIDMode="Static" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:HyperLink runat="server" NavigateUrl="#" CssClass="validationlink" ID="hlWarningAdvanced">Formula</asp:HyperLink>
                                                                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator12" ControlToValidate="txtMapHeight"
                                                                                   runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                               </asp:RegularExpressionValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top" class="conditions">
                                                                            <td>
                                                                                <asp:CheckBox ClientIDMode="Static" runat="server" ID="chkWarningConditions" />
                                                                            </td>
                                                                            <td>
                                                                                <div style="word-wrap: break-word;">
                                                                                    <asp:HyperLink ClientIDMode="Static" runat="server" NavigateUrl="#" CssClass="validationlink2" ID="hlWarningConditions">Conditions</asp:HyperLink>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td></td>
                                                            </tr>

                                                            <tr id="trExceedance1">
                                                                <td colspan="3">
                                                                    <asp:Label runat="server" ID="lblExceedanceValidation" Text="Data Exceedance if outside the range"
                                                                        Font-Bold="true"></asp:Label>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr id="trExceedance2">
                                                                <td align="right" valign="top"></td>
                                                                <td valign="top" style="padding-top: 5px;">
                                                                    <asp:TextBox runat="server" ID="txtValidationOnExceedance" TextMode="MultiLine" CssClass="MultiLineTextBox"
                                                                        Width="250px" Height="60px" Style="display: none;"></asp:TextBox>

                                                                    <div id="divExceedanceMinMax">
                                                                        <table cellpadding="3">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Label runat="server" ID="lblMinExceedance" Text="Value less than:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtMinExceedance" CssClass="NormalTextBox" Width="87px"></asp:TextBox>
                                                                                </td>
                                                                                <td align="right">
                                                                                    <asp:Label runat="server" ID="lblMaxExceedance" Text="Value greater than:"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtMaxExceedance" CssClass="NormalTextBox" Width="87px"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <%--<asp:ListItem Value="readonly" Text="Read Only (set elsewhere)"></asp:ListItem>--%>                                                                        <%--<asp:ListItem Value="content" Text="Content Editor"></asp:ListItem>--%>                                                                        <%--<asp:ListItem Value="data_retriever" Text="Data Retriever"></asp:ListItem>--%>                                                                        <%--<asp:LinkButton runat="server" ID="lnkAddTL5"  CausesValidation="false" OnClick="lnkAddTL5_Click">
                                                                                                    <asp:Image ID="Image12" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png" />
                                                                                                </asp:LinkButton>--%><%--<asp:ListItem Value="table" Text="Table Predictive"></asp:ListItem>--%><%--<asp:ListItem Value="tabledd" Text="Table Dropdown"></asp:ListItem>--%>
                                                                    </div>
                                                                </td>
                                                                <td valign="top" style="padding-top: 7px;">

                                                                    <table cellpadding="3">

                                                                        <tr class="formula">
                                                                            <td>
                                                                                <asp:CheckBox ClientIDMode="Static" runat="server" ID="chkExceedanceFormula" />
                                                                            </td>
                                                                            <td>
                                                                                <asp:HyperLink runat="server" NavigateUrl="#" CssClass="validationlink" ID="hlExceedanceAdvanced">Formula</asp:HyperLink>
                                                                                <%--<asp:ListItem Value="readonly" Text="Read Only (set elsewhere)"></asp:ListItem>--%>

                                                                            </td>
                                                                        </tr>
                                                                        <tr valign="top" class="conditions">
                                                                            <td>
                                                                                <asp:CheckBox ClientIDMode="Static" runat="server" ID="chkExceedanceConditions" />
                                                                            </td>
                                                                            <td>
                                                                                <div style="word-wrap: break-word;">
                                                                                    <asp:HyperLink ClientIDMode="Static" runat="server" NavigateUrl="#" CssClass="validationlink2" ID="hlExceedanceConditions">Conditions</asp:HyperLink>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr id="trResetValidation" style="display: none;">
                                                                <td></td>
                                                                <td colspan="3">
                                                                    <asp:HyperLink ID="hlResetValidation" ClientIDMode="Static" runat="server"
                                                                        CssClass="popupresetIDs">Revalidate Records</asp:HyperLink>
                                                                </td>
                                                            </tr>

                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                            <td valign="top" style="padding-left: 10px;">
                                                <div style="border-style: solid; border-width: 1px; min-width: 450px; min-height: 140px; padding: 5px;"
                                                    id="divColumnType">
                                                    <table cellpadding="3">
                                                        <tr id="trColumtType">
                                                            <td align="right">
                                                                <asp:Label runat="server" ID="lblFieldType" Text="Field Type" Font-Bold="true" Width="100px"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ddlType" CssClass="NormalTextBox">
                                                                    <asp:ListItem Value="button" Text="Button"></asp:ListItem>
                                                                    <asp:ListItem Value="calculation" Text="Calculation"></asp:ListItem>
                                                                    <asp:ListItem Value="checkbox" Text="Checkbox"></asp:ListItem>
                                                                    <asp:ListItem Value="childtable" Text="Child Table"></asp:ListItem>

                                                                    <asp:ListItem Value="staticcontent" Text="Content"></asp:ListItem>
                                                                    <%--<asp:ListItem Value="content" Text="Content Editor"></asp:ListItem>--%>
                                                                    <%--<asp:ListItem Value="data_retriever" Text="Data Retriever"></asp:ListItem>--%>
                                                                    <asp:ListItem Value="date_time" Text="Date / Time"></asp:ListItem>
                                                                    <asp:ListItem Value="dropdown" Text="Dropdown"></asp:ListItem>
                                                                    <asp:ListItem Value="file" Text="File"></asp:ListItem>
                                                                    <asp:ListItem Value="image" Text="Image"></asp:ListItem>
                                                                    <asp:ListItem Value="listbox" Text="List Box (multi-select)"></asp:ListItem>
                                                                    <asp:ListItem Value="location" Text="Location"></asp:ListItem>
                                                                    <asp:ListItem Value="number" Text="Number"></asp:ListItem>
                                                                    <asp:ListItem Value="radiobutton" Text="Radio Button"></asp:ListItem>
                                                                    <asp:ListItem Value="text" Text="Text" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Value="trafficlight" Text="Traffic Light"></asp:ListItem>
                                                                    <asp:ListItem Value="ascx" Text="User Control(Testing)"></asp:ListItem>
                                                                    <asp:ListItem Value="sqlretrieval" Text="SQL Retrieval"></asp:ListItem>
                                                                </asp:DropDownList>
                                                              <%--  <span>
                                                                    <asp:HyperLink 
                                                                        ID="fieldTypeInfo" 
                                                                        ClientIDMode="Static" 
                                                                        runat="server"                                                                         
                                                                        ImageUrl="~/Images/fieldinfo.png" Style="padding-left: 5px" 
                                                                        CssClass="popupFieldInfo">
                                                                    </asp:HyperLink>
                                                                </span>--%>
                                                            </td>
                                                            <td></td>
                                                            <td style="width: 10px;"></td>
                                                            <td align="right" colspan="2">
                                                                <asp:HyperLink ID="hlResetCalculations" ClientIDMode="Static" runat="server"
                                                                    CssClass="popupresetCals" Style="display: none;">Reset Calculation Values</asp:HyperLink>

                                                            </td>
                                                        </tr>
                                                        <tr id="trCalculationType" style="display: none;">
                                                            <td align="right" style="width: 120px;">
                                                                <strong>Calculation Type</strong>
                                                            </td>
                                                            <td colspan="5">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>

                                                                        <td align="left">
                                                                            <asp:DropDownList runat="server" ID="ddlCalculationType"
                                                                                CssClass="NormalTextBox">
                                                                                <asp:ListItem Value="n" Text="Number" Selected="True"></asp:ListItem>
                                                                                <asp:ListItem Value="f" Text="Financial"></asp:ListItem>
                                                                                <asp:ListItem Value="d" Text="Date/Time"></asp:ListItem>
                                                                                <asp:ListItem Value="t" Text="Text"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td>
                                                                            <table id="tblFinancialSymbol" style="display: none;">
                                                                                <tr>
                                                                                    <td align="right" style="padding-left: 10px;">
                                                                                        <strong>Symbol</strong>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtCalFinancialSymbol" Text="$" CssClass="NormalTextBox"
                                                                                            Width="50px"></asp:TextBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table id="tblDateCal" style="display: none;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <strong>Result Format</strong>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:DropDownList runat="server" ID="ddlDateResultFormat" CssClass="NormalTextBox">
                                                                                            <asp:ListItem Value="datetime" Text="Date and time" Selected="True"></asp:ListItem>
                                                                                            <asp:ListItem Value="date" Text="Date only"></asp:ListItem>
                                                                                            <asp:ListItem Value="time" Text="Time only"></asp:ListItem>
                                                                                            <asp:ListItem Value="minute" Text="Number of minutes"></asp:ListItem>
                                                                                            <asp:ListItem Value="hour" Text="Number of hours"></asp:ListItem>
                                                                                            <asp:ListItem Value="day" Text="Number of days"></asp:ListItem>

                                                                                            <asp:ListItem Value="ymd" Text="Year, Month & Day"></asp:ListItem>
                                                                                            <asp:ListItem Value="ym" Text="Year & Month"></asp:ListItem>

                                                                                        </asp:DropDownList>

                                                                                    </td>
                                                                                </tr>
                                                                            </table>

                                                                        </td>

                                                                    </tr>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="trCalculation" style="display: none;">
                                                            <td align="right" valign="top">
                                                                <asp:Label runat="server" ID="lblCalculation" Text="Calculation" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td colspan="5">
                                                                <table style="border-collapse: collapse; border-spacing: 0;">
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <asp:TextBox runat="server" ID="txtCalculation" TextMode="MultiLine" CssClass="MultiLineTextBox"
                                                                                Width="250px" Height="60px"></asp:TextBox>

                                                                        </td>
                                                                        <td valign="top">
                                                                            <asp:HyperLink runat="server" NavigateUrl="#" CssClass="calculationlink" ID="hlCalculationEdit">Edit</asp:HyperLink>
                                                                        </td>
                                                                    </tr>

                                                                </table>
                                                            </td>

                                                        </tr>
                                                        <tr id="trTrafficLight" style="display: none;">
                                                            <td colspan="5">
                                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                                                    <ContentTemplate>
                                                                        <table cellpadding="3">
                                                                            <tr>
                                                                                <td colspan="5" style="padding-left: 67px">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td align="right">
                                                                                                <strong>Controlling Field</strong>
                                                                                            </td>
                                                                                            <td align="left">
                                                                                                <asp:DropDownList runat="server" ID="ddlTLControllingField" CssClass="NormalTextBox"
                                                                                                    AutoPostBack="true" DataTextField="DisplayName" DataValueField="ColumnID" OnSelectedIndexChanged="ddlTLControllingField_SelectedIndexChanged">
                                                                                                </asp:DropDownList>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trTLValueImage" runat="server" visible="false">
                                                                                <td colspan="5">
                                                                                    <div style="padding-top: 10px; max-width: 600px;">
                                                                                        <asp:GridView ID="grdTrafficLight" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                                                                                            CssClass="gridview" OnRowCommand="grdTrafficLight_RowCommand" OnRowDataBound="grdTrafficLight_RowDataBound"
                                                                                            ShowHeaderWhenEmpty="true"
                                                                                            ShowFooter="true">
                                                                                            <Columns>
                                                                                                <asp:TemplateField Visible="false">
                                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label runat="server" ID="lblID" Text='<%# Eval("ID") %>'></asp:Label>

                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField>
                                                                                                    <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="imgbtnMinus" runat="server" ImageUrl="~/App_Themes/Default/Images/Minus.png"
                                                                                                            CommandName="minus" CommandArgument='<%# Eval("ID") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>

                                                                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                                                    <HeaderTemplate>
                                                                                                        <strong>Value</strong>
                                                                                                    </HeaderTemplate>

                                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ControlByColumn runat="server" ID="cbcValue" ShowColumnDDL="false" CustomCompareOperator="Traffic" />
                                                                                                        <%-- CustomCompareOperator << use this property when you want to modify the drop down option of ddlCompareOperator.
                                                                                                             This was added to comply to a fix where ddlCompareOperator for traffic needed to only show equals.
                                                                                                             By JV --%>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>


                                                                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                                                    <HeaderTemplate>
                                                                                                        <strong>Image</strong>
                                                                                                    </HeaderTemplate>

                                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                                    <ItemTemplate>

                                                                                                        <asp:DropDownList runat="server" ID="ddlTLImage" CssClass="NormalTextBox">
                                                                                                        </asp:DropDownList>

                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>


                                                                                                <asp:TemplateField>
                                                                                                    <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                                                                    <ItemTemplate>
                                                                                                        <asp:ImageButton ID="imgbtnPlus" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png"
                                                                                                            CommandName="plus" Visible="true" CommandArgument='<%# Eval("ID") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>

                                                                                            </Columns>
                                                                                            <HeaderStyle CssClass="gridview_header" Height="25px" />
                                                                                            <RowStyle CssClass="gridview_row_NoPadding" />
                                                                                        </asp:GridView>

                                                                                        <div>
                                                                                            <asp:Label ID="lblMsgTab" runat="server" ForeColor="Red"></asp:Label>
                                                                                        </div>
                                                                                    </div>




                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                        <tr id="trStaticContent" style="display: none;">
                                                            <td colspan="5">
                                                                <table cellpadding="3">
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:CheckBox runat="server" ID="chkAllowContenEdit" Visible="false" Text="Allow user to edit content"
                                                                                Font-Bold="true" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <editor:WYSIWYGEditor runat="server" scriptPath="../../Editor/scripts/" ID="edtContent"
                                                                                btnSave="false" EditorHeight="250" Height="250" EditorWidth="500" Width="500"
                                                                                AssetManager="../../assetmanager/assetmanager.aspx" AssetManagerWidth="550" AssetManagerHeight="400" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="trUserControl" style="display: none;">
                                                            <td colspan="5">
                                                                <table cellpadding="3">
                                                                    <tr>
                                                                        <td align="center">.ascx file name(*):
                                                                        </td>

                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtUserControl" Width="300" CssClass="NormalTextBox"
                                                                                ToolTip=".ascx file must be under Pages/UserControl/Custom/ folder"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">Common Method name:
                                                                        </td>

                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtUserControlMethod" Width="300" CssClass="NormalTextBox"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">Custom Method name:
                                                                        </td>

                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtUserControlCustomMethod" Width="300" CssClass="NormalTextBox"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">Ref ColumnID:
                                                                        </td>

                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtUserControlRefColumnID" Width="100" CssClass="NormalTextBox"></asp:TextBox>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td align="center">TableID:
                                                                        </td>

                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtUserControlTableID" Width="100" CssClass="NormalTextBox"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">Hidden ColumnIDs:
                                                                        </td>

                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtUserControlColumnNotIn" ToolTip="Comma separated ColumnIDs (e.g 1234,5432,3577)"
                                                                                Width="300" TextMode="MultiLine" CssClass="MultiLineTextBox"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="trButton3" style="display: none;">
                                                            <td align="right" valign="top">
                                                                <strong>Warning Message</strong>
                                                            </td>
                                                            <td valign="top" colspan="5">
                                                                <table style="border-collapse: collapse; border-spacing: 0;">
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <asp:CheckBox runat="server" ID="chkButtonWarningMessage" ClientIDMode="Static" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtButtonWarningMessage" CssClass="NormalTextBox" Width="400px" TextMode="MultiLine" ToolTip="" ClientIDMode="Static"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                         <tr id="trButton5" style="display: none;">
                                                            <td align="right" valign="top">
                                                                <strong>Save Record</strong>
                                                            </td>
                                                            <td valign="top" colspan="5">
                                                               <asp:CheckBox runat="server" ID="chkSaveRecord" ClientIDMode="Static" Checked="true" />
                                                            </td>
                                                        </tr>
                                                        <tr id="trButton1" style="display: none;">
                                                            <td align="right" valign="top">
                                                                <strong>Run Procedure</strong>
                                                            </td>
                                                            <td valign="top" colspan="5">
                                                                <table style="border-collapse: collapse; border-spacing: 0;">
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <asp:CheckBox runat="server" ID="chkSPToRun" ClientIDMode="Static" />
                                                                        </td>
                                                                        <td>
                                                                           <%-- <asp:TextBox runat="server" ID="txtSPToRun" CssClass="NormalTextBox" Width="200px" ClientIDMode="Static"
                                                                                ToolTip="@RecordID int,@UserID int, @Return varchar(max) output"></asp:TextBox>--%>
                                                                            <asp:DropDownList runat="server" ID="ddlButtonSPToRun"  
                                                                                CssClass="NormalTextBox"  ClientIDMode="Static" ></asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                     <tr id="trButton1a" style="display: none;">
                                                                        <td valign="top" colspan="2">
                                                                           
                                                                      
                                                                           <asp:CheckBox runat="server" ID="chkButtonDisplayResult" Text="Display Result" TextAlign="Right"
                                                                               ClientIDMode="Static" />
                                                                        </td>
                                                                    </tr>
                                                                </table>


                                                            </td>
                                                        </tr>
                                                        <tr id="trButton2" style="display: none;">
                                                            <td align="right" valign="top">
                                                                <strong>Image</strong>
                                                            </td>
                                                            <td valign="top" colspan="5">
                                                                <%--<asp:ListItem Value="table" Text="Table Predictive"></asp:ListItem>--%>
                                                                <table style="border-collapse: collapse; border-spacing: 0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblButtonValue" ClientIDMode="Static"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:FileUpload runat="server" ID="fuButtonValue" ClientIDMode="Static" />
                                                                            <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfButtonValue" />
                                                                        </td>
                                                                    </tr>

                                                                </table>

                                                            </td>

                                                        </tr>
                                                        
                                                        <tr id="trButton4" style="display: none;">
                                                            <td align="right" valign="top">
                                                                <%--<strong>On Click</strong>--%>
                                                            </td>
                                                            <td valign="top" colspan="5">
                                                                <table style="border-collapse: collapse; border-spacing: 0;">
                                                                    <%--<tr>
                                                                        <td colspan="2">
                                                                            <asp:DropDownList runat="server" ID="ddlButtonOnClick" CssClass="NormalTextBox">
                                                                                <asp:ListItem Text="--Please Select--" Value="" Selected="True"></asp:ListItem>
                                                                                <asp:ListItem Text="Save and stay on current page" Value="StayCurrent"></asp:ListItem>
                                                                                <asp:ListItem Text="Go back to previous page" Value="Goback"></asp:ListItem>
                                                                                <asp:ListItem Text="Save and add a child record" Value="AddChild"></asp:ListItem>
                                                                                <asp:ListItem Text="Add a child record (No save)" Value="NoSave"></asp:ListItem>
                                                                                <asp:ListItem Text="Add a record" Value="AddRecord"></asp:ListItem>
                                                                                <asp:ListItem Text="Open link in current tab" Value="CurrentTab"></asp:ListItem>
                                                                                <asp:ListItem Text="Open link in new tab" Value="NewTab"></asp:ListItem>    
                                                                                <asp:ListItem Text="Execute Service" Value="ExecService"></asp:ListItem>
                                                                                <asp:ListItem Text="Export to excel" Value="Export_Excel"></asp:ListItem>
                                                                                <asp:ListItem Text="Print Report" Value="Print_Report"></asp:ListItem>
                                                                                <asp:ListItem Text="Edit current record" Value="Edit_Current_Record"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>--%>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table>
                                                                                <tr>
                                                                                    <td style="text-align:right; width:170px;">
                                                                                         <strong>Action </strong>
                                                                                    </td>
                                                                                    <td style="text-align:left;">
                                                                                          <asp:DropDownList runat="server" ID="ddlButtonAction" CssClass="NormalTextBox">
                                                                                                <asp:ListItem Text="--Please Select--" Value="" Selected="True"></asp:ListItem>
                                                                                                <asp:ListItem Text="Add a Record" Value="AddRecord"></asp:ListItem>
                                                                                                 <asp:ListItem Text="Add a child record" Value="AddChild"></asp:ListItem>
                                                                                                <asp:ListItem Text="Run Procedure (as above)" Value="RunProcedure"></asp:ListItem>                                                                                                
                                                                                                <asp:ListItem Text="Execute Service" Value="ExecService"></asp:ListItem>
                                                                                                <asp:ListItem Text="Export to excel" Value="Export_Excel"></asp:ListItem>
                                                                                                <asp:ListItem Text="Print Report" Value="Print_Report"></asp:ListItem>
                                                                                                <asp:ListItem Text="Edit current record" Value="Edit_Current_Record"></asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                 
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="trButton4_TextOnEdit" style="display: none;">
                                                                        <td align="right">
                                                                            <strong>Text On Edit</strong>
                                                                        </td>
                                                                        <td style="padding-left: 2px;">
                                                                            <asp:TextBox runat="server" ID="txtTextOnEdit" CssClass="NormalTextBox" Width="300"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="trButton4_Child" style="display: none;">
                                                                        <td align="right">
                                                                            <strong>Child Table</strong>
                                                                        </td>
                                                                        <td style="padding-left: 2px;">
                                                                            <asp:DropDownList runat="server" ID="ddlButtonChild" DataTextField="TableName" DataValueField="TableID" CssClass="NormalTextBox"></asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="trButton4_Table" style="display: none;">
                                                                        <td align="right">
                                                                            <strong>Table</strong>
                                                                        </td>
                                                                        <td style="padding-left: 2px;">
                                                                            <asp:DropDownList runat="server" ID="ddlButtonTable" DataTextField="TableName" DataValueField="TableID" CssClass="NormalTextBox"></asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="trButton4_Service" style="display: none;">
                                                                        <td align="right">
                                                                            <strong>Service</strong>
                                                                        </td>
                                                                        <td style="padding-left: 2px;">
                                                                            <asp:TextBox runat="server" ID="txtService" CssClass="NormalTextBox" Width="300"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                   
                                                                    <tr id="trButton4_Report" style="display: none;">
                                                                        <td align="right">
                                                                            <strong>Report</strong></td>
                                                                        <td style="padding-left: 2px;">
                                                                            <asp:DropDownList runat="server" ID="ddlButtonCustomReport" DataTextField="DocumentText" DataValueField="DocumentID" CssClass="NormalTextBox"></asp:DropDownList>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table>
                                                                                <tr>
                                                                                    <td style="text-align: right;width:170px;">
                                                                                        <strong>After Action</strong>
                                                                                    </td>
                                                                                    <td style="text-align: left;">
                                                                                        <asp:DropDownList runat="server" ID="ddlButtonAfterAction" CssClass="NormalTextBox">
                                                                                            <asp:ListItem Text="--Please Select--" Value="" Selected="True"></asp:ListItem>
                                                                                            <asp:ListItem Text="Stay on current page" Value="StayCurrent"></asp:ListItem>
                                                                                            <asp:ListItem Text="Go back to previous page" Value="Goback"></asp:ListItem>
                                                                                            <asp:ListItem Text="Open link in current tab" Value="CurrentTab"></asp:ListItem>
                                                                                            <asp:ListItem Text="Open link in new tab" Value="NewTab"></asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>

                                                                            </table>
                                                                        </td>
                                                                    </tr>                                                                  
                                                                     <tr id="trButton4_Link" style="display: none;">
                                                                         <td colspan="2">
                                                                             <table>
                                                                                 <tr>
                                                                                     <td align="right"  style="width:170px;">
                                                                                         <strong>Link</strong>
                                                                                     </td>
                                                                                     <td style="padding-left: 2px;">
                                                                                         <asp:TextBox runat="server" ID="txtButtonLink" CssClass="NormalTextBox" Width="300"></asp:TextBox>
                                                                                     </td>
                                                                                 </tr>
                                                                             </table>
                                                                         </td>
                                                                       
                                                                    </tr>
                                                                    <tr id="trButton4_Param" style="display: none;">
                                                                        <td colspan="2">
                                                                            <table>
                                                                                <tr>
                                                                                    <td align="right" style="width:170px;">
                                                                                        <strong>Additional Parameters</strong>
                                                                                    </td>
                                                                                    <td style="padding-left: 2px;">
                                                                                        <asp:TextBox runat="server" ID="txtButtonParamas" CssClass="NormalTextBox" Width="300"></asp:TextBox>
                                                                                    </td>

                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td  style="width:170px;">

                                                                        </td>
                                                                        <td >
                                                                            <asp:CheckBox runat="server" ID="chkButtonHyperLink" Text="Always a hyperlink" TextAlign="Right" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="trCheckbox1" style="display: none;">
                                                            <td align="right" valign="top">
                                                                <strong>Ticked Value</strong>
                                                            </td>
                                                            <td valign="top">
                                                                <asp:TextBox runat="server" ID="txtTickedValue" CssClass="NormalTextBox" Text="Yes"></asp:TextBox>
                                                            </td>
                                                            <td></td>
                                                            <td style="width: 10px;"></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr id="trCheckbox2" style="display: none;">
                                                            <td align="right" valign="top">
                                                                <strong>Unticked Value</strong>
                                                            </td>
                                                            <td valign="top">
                                                                <asp:TextBox runat="server" ID="txtUntickedValue" CssClass="NormalTextBox" Text="No"></asp:TextBox>
                                                            </td>
                                                            <td></td>
                                                            <td style="width: 10px;"></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr id="trCheckbox3" style="display: none;">
                                                            <td align="right" valign="top"></td>
                                                            <td valign="top">
                                                                <asp:CheckBox ID="chkTickedByDefault" runat="server" Text="Ticked by Default" TextAlign="Right"
                                                                    Font-Bold="true" />
                                                            </td>
                                                            <td></td>
                                                            <td style="width: 10px;"></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr id="trLocation" style="display: none;">
                                                            <td align="right" valign="top"></td>
                                                            <td valign="top">
                                                                <asp:CheckBox ID="chkLocationAddress" CssClass="chkbox" runat="server" Text="Show Address"
                                                                    TextAlign="Right" Font-Bold="true" Checked="true" />
                                                                <br />
                                                                <asp:CheckBox ID="chkLatLong" CssClass="chkbox" runat="server" Text="Show Latitude/Longitude" TextAlign="Right"
                                                                    Font-Bold="true" Checked="true" />
                                                                <br />
                                                                <asp:CheckBox ID="chkShowMap" CssClass="chkbox" runat="server" Text="Show Map" TextAlign="Right"
                                                                    Font-Bold="true" Checked="true" ClientIDMode="Static" />
                                                                <br />
                                                                <table>

                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table id="tblMapDimension">
                                                                                <tr>
                                                                                    <td style="padding-left: 20px;">
                                                                                        <strong>Map Height</strong>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtMapHeight" Width="50px" Text="200"></asp:TextBox>
                                                                                        <asp:RangeValidator runat="server" ID="rvMapHeight" ControlToValidate="txtMapHeight"
                                                                                            Type="Integer" MinimumValue="100" MaximumValue="1000" Display="Dynamic" ErrorMessage="100 to 1000 please!"> </asp:RangeValidator>
                                                                                        <%--<asp:ListItem Value="tabledd" Text="Table Dropdown"></asp:ListItem>--%>
                                                                                    </td>
                                                                                    <td></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="padding-left: 20px;">
                                                                                        <strong>Map Width</strong>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox runat="server" ID="txtMapWidth" Width="50px" Text="400"></asp:TextBox>
                                                                                        <asp:RangeValidator runat="server" ID="rvtxtMapWidth" ControlToValidate="txtMapWidth"
                                                                                            Type="Integer" MinimumValue="200" MaximumValue="1000" Display="Dynamic" ErrorMessage="200 to 1000 please!"> </asp:RangeValidator>
                                                                                    </td>
                                                                                    <td></td>
                                                                                </tr>
                                                                            </table>

                                                                        </td>
                                                                    </tr>

                                                                    <tr id="trHoverText">
                                                                        <td style="padding-left: 20px;">
                                                                            <strong>Hover Text</strong>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="ddlMapPinHoverColumnID" CssClass="NormalTextBox"
                                                                                DataValueField="ColumnID" DataTextField="DisplayName">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td>
                                                                            <asp:HyperLink runat="server" ID="hlEditMappopup" ClientIDMode="Static"
                                                                                NavigateUrl="~/Pages/Content/MapPopup.aspx">Edit Popup</asp:HyperLink>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td></td>
                                                            <td style="width: 10px;"></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr id="trDateTimeType" style="display: none;">
                                                            <td align="right" valign="top">
                                                                <strong>Type</strong>
                                                            </td>
                                                            <td valign="top">
                                                                <asp:DropDownList runat="server" ID="ddlDateTimeType" ClientIDMode="Static" CssClass="NormalTextBox">
                                                                    <asp:ListItem Value="date" Text="Date Only"></asp:ListItem>
                                                                    <asp:ListItem Value="datetime" Text="Date & Time"></asp:ListItem>
                                                                    <asp:ListItem Value="time" Text="Time"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <%--Red 3661--%>
                                                            <td></td>

                                                            <td></td>
                                                            <td style="width: 10px;"></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr id="trTextType" style="display: none;">
                                                            <td align="right" valign="top">
                                                                <strong>Text Type</strong>
                                                            </td>
                                                            <td valign="top">
                                                                <asp:DropDownList runat="server" ID="ddlTextType" ClientIDMode="Static" CssClass="NormalTextBox">
                                                                    <asp:ListItem Value="" Text="Free text (any value) " Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Value="text" Text="Text only – no numbers"></asp:ListItem>
                                                                    <asp:ListItem Value="email" Text="Email address(es)"></asp:ListItem>
                                                                    <asp:ListItem Value="link" Text="Website/link"></asp:ListItem>
                                                                    <asp:ListItem Value="isbn" Text="ISBN"></asp:ListItem>
                                                                    <%--<asp:ListItem Value="readonly" Text="Read Only (set elsewhere)"></asp:ListItem>--%>
                                                                    <asp:ListItem Value="own" Text="Regular Expression"></asp:ListItem>
                                                                    <asp:ListItem Value="mobile" Text="Mobile Number"></asp:ListItem>                                                                    
                                                                </asp:DropDownList>
                                                                <br />
                                                                <asp:HyperLink Target="_blank" runat="server" ID="hlRegEx" ClientIDMode="Static"
                                                                    NavigateUrl="http://regexlib.com/" Text="http://regexlib.com/"></asp:HyperLink>
                                                            </td>
                                                            <td></td>
                                                            <td style="width: 10px;"></td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtOwnRegEx" ClientIDMode="Static" TextMode="MultiLine"
                                                                    CssClass="MultiLineTextBox" Width="150px" Height="50"></asp:TextBox>
                                                            </td>
                                                            <td></td>
                                                        </tr>
                                                        <tr id="trNumber1" style="display: none;">
                                                            <td align="right">
                                                                <asp:Label runat="server" ID="lblNumberType" Text="Number Type" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList runat="server" ID="ddlNumberType" CssClass="NormalTextBox">
                                                                    <asp:ListItem Value="4" Text="Average"></asp:ListItem>
                                                                    <%--<asp:ListItem Value="3" Text="Calculated"></asp:ListItem>--%>
                                                                    <asp:ListItem Value="2" Text="Constant"></asp:ListItem>
                                                                    <asp:ListItem Value="6" Text="Financial"></asp:ListItem>
                                                                    <asp:ListItem Value="8" Text="ID"></asp:ListItem>
                                                                    <asp:ListItem Value="1" Text="Normal" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Value="5" Text="Record Count"></asp:ListItem>
                                                                    <asp:ListItem Value="7" Text="Slider"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td></td>
                                                            <td style="width: 10px;"></td>
                                                            <td align="right">
                                                                <asp:HyperLink ID="hlResetIDs" ClientIDMode="Static" runat="server"
                                                                    CssClass="popupresetIDs" Style="display: none;">Reset IDs</asp:HyperLink>
                                                                <asp:Label runat="server" ID="lblConstant" Text="Value" Font-Bold="true" Style="display: none;"></asp:Label>
                                                            </td>
                                                            <td>

                                                                <asp:TextBox runat="server" ID="txtConstant" TextMode="SingleLine" CssClass="NormalTextBox"
                                                                    Width="150px" Style="display: none;"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="revtxtConstant" ControlToValidate="txtConstant"
                                                                    runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                </asp:RegularExpressionValidator>


                                                                <asp:Button runat="server" ID="btnResetIDsOK" ClientIDMode="Static" Style="display: none;" OnClick="btnResetIDsOK_Click" />
                                                                <asp:Button runat="server" ID="btnResetCalValues" ClientIDMode="Static" Style="display: none;" OnClick="btnResetCalValues_Click" />

                                                                <asp:Button runat="server" ID="btnValidateRecordsOK" ClientIDMode="Static" Style="display: none;" OnClick="btnValidateRecordsOK_Click" />
                                                                <asp:Button runat="server" ID="btnValidateRecordsNO" ClientIDMode="Static" Style="display: none;" OnClick="btnValidateRecordsNO_Click" />

                                                                <asp:Button runat="server" ID="btnConfirmInvalidOK" ClientIDMode="Static" Style="display: none;" OnClick="btnConfirmInvalidOK_Click" />
                                                                <asp:Button runat="server" ID="btnConfirmInvalidNO" ClientIDMode="Static" Style="display: none;" OnClick="btnConfirmInvalidNO_Click" />

                                                                <asp:Button runat="server" ID="btnRevalidateRecords" ClientIDMode="Static" Style="display: none;" OnClick="btnRevalidateRecords_Click" />

                                                            </td>
                                                        </tr>

                                                        <tr runat="server" clientidmode="Static" id="trDDType" style="display: none;">
                                                            <td align="right" valign="top">
                                                                <strong>Type</strong>
                                                            </td>
                                                            <td colspan="5">
                                                                <table>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <asp:DropDownList runat="server" ID="ddlDDType" ClientIDMode="Static" CssClass="NormalTextBox">
                                                                                <asp:ListItem Value="values" Text="Values" Selected="True"></asp:ListItem>
                                                                                <asp:ListItem Value="value_text" Text="Value & Text"></asp:ListItem>
                                                                                <%--<asp:ListItem Value="table" Text="Table Predictive"></asp:ListItem>--%>
                                                                                <%--<asp:ListItem Value="tabledd" Text="Table Dropdown"></asp:ListItem>--%>
                                                                                <asp:ListItem Value="ct" Text="Table"></asp:ListItem>
                                                                                <%-- removed by Jon Bosker on 22/03/2018: <asp:ListItem Value="lt" Text="Linked"></asp:ListItem> --%>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td id="tdPredictiveAndFilter">
                                                                            <table>
                                                                                <tr>
                                                                                    <td style="width: 100px;">
                                                                                        <%--<asp:CheckBox runat="server" ID="chkFilterValues" Text="Filter Values" ClientIDMode="Static"
                                                                                            TextAlign="Right" />--%>
                                                                                        <asp:CheckBox runat="server" ID="chkPredictive" Text="Predictive" ClientIDMode="Static"
                                                                                            TextAlign="Right" Font-Bold="true" />
                                                                                    </td>
                                                                                    <td style="width: 100px;" id="tdTableFilter">
                                                                                        <asp:CheckBox runat="server" ID="chkFiltered" Text="" TextAlign="Right" Font-Bold="true"
                                                                                            ClientIDMode="Static" />
                                                                                        <asp:HyperLink runat="server" NavigateUrl="~/Pages/Record/Filtered.aspx" CssClass="showfilteredlink"
                                                                                            ID="hlFiltered" ClientIDMode="Static">Filtered...</asp:HyperLink>
                                                                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfFiltered" />
                                                                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfFilterParentColumnID" />
                                                                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfFilterOtherColumnID" />
                                                                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfFilterValue" />
                                                                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfFilterOperator" Value="equals" />
                                                                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfParentTableID" />


                                                                                        <%--KG Ticket 3985 3/7/18 2nd Filter --%>
                                                                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfFiltered2" />
                                                                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfFilterParentColumnID2" />
                                                                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfFilterOtherColumnID2" />
                                                                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfFilterValue2" />
                                                                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfFilterOperator2" Value="equals" />
                                                                                    </td>
                                                                                </tr>
                                                                                <%-- RP Added Ticket 4310 --%>
                                                                                <tr>
                                                                                    <td colspan="2">
                                                                                         <asp:CheckBox runat="server" ID="chkShowAllValues" Text="Show all values on search" TextAlign="Right" Font-Bold="true"
                                                                                            ClientIDMode="Static" />
                                                                                    </td>
                                                                                </tr>
                                                                                <%-- End Modification --%>
                                                                            </table>
                                                                        </td>
                                                                        <td>
                                                                            <asp:LinkButton runat="server" ID="lnkCreateTable" Visible="false" CausesValidation="false"
                                                                                OnClick="lnkCreateTable_Click"> <strong>Create values from Table</strong> </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>

                                                        </tr>
                                                        <tr runat="server" clientidmode="Static" id="trRadioOptionType" style="display: none;">
                                                            <td align="right">
                                                                <strong>Type</strong>
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:DropDownList runat="server" ID="ddlOptionType" ClientIDMode="Static" CssClass="NormalTextBox">
                                                                    <asp:ListItem Value="values" Text="Values" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Value="value_text" Text="Value & Text"></asp:ListItem>
                                                                    <asp:ListItem Value="value_image" Text="Value & Image"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                &nbsp;
                                                                <asp:CheckBox runat="server" ID="chkDisplayVertical" Text="Vertical List" TextAlign="Right" />
                                                            </td>
                                                        </tr>
                                                        <tr runat="server" clientidmode="Static" id="trListboxType" style="display: none;">
                                                            <td align="right">
                                                                <strong>Type</strong>
                                                            </td>
                                                            <td colspan="4">
                                                                <asp:DropDownList runat="server" ID="ddlListBoxType" ClientIDMode="Static" CssClass="NormalTextBox">
                                                                    <asp:ListItem Value="values" Text="Values" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Value="value_text" Text="Value & Text"></asp:ListItem>
                                                                    <asp:ListItem Value="table" Text="Table"></asp:ListItem>
                                                                </asp:DropDownList>

                                                            </td>
                                                            <td>
                                                                <asp:CheckBox runat="server" ID="chkListCheckBox" Font-Bold="true" Checked="true" />
                                                                <asp:Label runat="server" ID="lblListCheckBox" Text="Use Checkboxes" Font-Bold="true"></asp:Label>
                                                            </td>
                                                        </tr>

                                                        <tr clientidmode="Static" id="trDDValues" style="display: none;">
                                                            <td align="right" style="vertical-align: top;">
                                                                <asp:Label runat="server" Font-Bold="true" Text="Values" ID="lblDropdownValuesCap"></asp:Label>
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:TextBox runat="server" ID="txtDropdownValues" CssClass="MultiLineTextBox" TextMode="MultiLine"
                                                                    Width="150px" Height="50px"></asp:TextBox>
                                                                <br />
                                                                <asp:Label runat="server" Text="Enter the values – one on each line" ID="lblDropdownValuesHelp"
                                                                    Font-Size="Small"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trOptionImageGrid" style="display: none;">
                                                            <td></td>
                                                            <td>
                                                                <div id="divOptionImageGrid" style="min-width: 400px;">
                                                                    <asp:UpdatePanel ID="upOptionImage" runat="server" UpdateMode="Always">
                                                                        <ContentTemplate>
                                                                            <dbg:dbgGridView ID="gvOptionImage" runat="server" GridLines="Both" CssClass="gridview"
                                                                                HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center"
                                                                                AllowPaging="True" AllowSorting="True" DataKeyNames="OptionImageID" HeaderStyle-ForeColor="Black"
                                                                                Width="100%" AutoGenerateColumns="false" PageSize="15" OnRowDataBound="gvOptionImage_RowDataBound">
                                                                                <PagerSettings Position="Top" />
                                                                                <Columns>
                                                                                    <asp:TemplateField Visible="false">
                                                                                        <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="LblID" runat="server" Text='<%# Eval("OptionImageID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField>
                                                                                        <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                                        <%--<HeaderTemplate>
                                                                                <input id="chkAll" onclick="DoMasterSelect(this, 'divOptionImageGrid')" runat="server" type="checkbox" />
                                                                            </HeaderTemplate>--%>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkDelete" runat="server" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField>
                                                                                        <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <asp:HyperLink ID="EditHyperLink" runat="server" ToolTip="Edit" NavigateUrl='<%# GetOptionImageEditURL() + Cryptography.Encrypt(Eval("OptionImageID").ToString()) %>'
                                                                                                ImageUrl="~/App_Themes/Default/Images/iconEdit.png" CssClass="optionimgelink" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField HeaderText="Value">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblValue" runat="server" Text='<%# Eval("Value") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Image">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="">
                                                                                        <ItemTemplate>
                                                                                            <asp:Image runat="server" ID="imgImage" AlternateText='<%# Eval("FileName") %>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                </Columns>
                                                                                <HeaderStyle CssClass="gridview_header" />
                                                                                <RowStyle CssClass="gridview_row" />
                                                                                <PagerTemplate>
                                                                                    <asp:GridViewPager runat="server" ID="Pager" HideDelete="false" HideAdd="false"
                                                                                        OnDeleteAction="Pager_DeleteAction" HideNavigation="true"
                                                                                        HideExport="true" HideFilter="true" HideRefresh="true" HideGo="true" HidePageSize="true" />
                                                                                </PagerTemplate>
                                                                                <EmptyDataTemplate>
                                                                                    <div style="padding-left: 10px;">
                                                                                        <asp:HyperLink runat="server" ID="hplNewData" Style="text-decoration: none; color: Black;" CssClass="optionimgelink">
                                                                                        <asp:Image runat="server" ID="imgAddNewRecord" ImageUrl="~/App_Themes/Default/images/BigAdd.png" />
                                                                                        <strong style="text-decoration: underline; color: Blue;">
                                                                                            Add new image with value.</strong>
                                                                                        </asp:HyperLink>
                                                                                    </div>
                                                                                </EmptyDataTemplate>
                                                                            </dbg:dbgGridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                                <br />

                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ClientIDMode="Static" runat="server" ID="chkImageOnSummary" Font-Bold="true" TextAlign="Right" Text="Image On Summary" />
                                                                        </td>
                                                                        <td style="padding-left: 20px;">
                                                                            <table id="tblImageOnSummaryMaxHeight">
                                                                                <tr>
                                                                                    <td align="right">

                                                                                        <strong>Max Height</strong>
                                                                                    </td>
                                                                                    <td align="left">
                                                                                        <asp:TextBox runat="server" ID="txtImageOnSummaryMaxHeight" CssClass="NormalTextBox" Width="50px" ClientIDMode="Static"></asp:TextBox>
                                                                                        <asp:RegularExpressionValidator ID="revtxtImageOnSummaryMaxHeight" ControlToValidate="txtImageOnSummaryMaxHeight"
                                                                                            runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                                        </asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>


                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trDDTableLookup" style="display: none;">
                                                            <td></td>
                                                            <td colspan="5">
                                                                <asp:LinkButton runat="server" ID="lnkCreateLookupTable" Visible="false" CausesValidation="false"
                                                                    OnClick="lnkCreateLookupTable_Click"> <strong>Create Lookup Table</strong> </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trDDTable" style="display: none;">
                                                            <td align="right">
                                                                <strong>Table</strong>
                                                            </td>
                                                            <td colspan="5">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="ddlDDTable" CssClass="NormalTextBox" ClientIDMode="Static">
                                                                            </asp:DropDownList>
                                                                            <ajaxToolkit:CascadingDropDown runat="server" ID="ddlDDTableC" Category="Tableid"
                                                                                ClientIDMode="Static" TargetControlID="ddlDDTable" ServicePath="~/CascadeDropdown.asmx"
                                                                                ServiceMethod="GetTables">
                                                                            </ajaxToolkit:CascadingDropDown>
                                                                            <asp:HiddenField runat="server" ID="hf_ddlDDTable" ClientIDMode="Static" />
                                                                        </td>
                                                                        <td align="right">
                                                                            <strong style="display: none;">Field</strong>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="ddDDLinkedParentColumn" ClientIDMode="Static"
                                                                                CssClass="NormalTextBox" Style="display: none;">
                                                                            </asp:DropDownList>
                                                                            <ajaxToolkit:CascadingDropDown runat="server" ID="ddDDLinkedParentColumnC" Category="Column"
                                                                                ClientIDMode="Static" TargetControlID="ddDDLinkedParentColumn" ServicePath="~/CascadeDropdown.asmx"
                                                                                ServiceMethod="GetColumnsLink" ParentControlID="ddlDDTable">
                                                                            </ajaxToolkit:CascadingDropDown>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trFilter" style="display: none;">
                                                            <td align="right">
                                                                <strong>1st Dropdown</strong>
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:DropDownList runat="server" ID="ddlFilter" ClientIDMode="Static" CssClass="NormalTextBox"
                                                                    DataTextField="DisplayName" DataValueField="ColumnID">
                                                                </asp:DropDownList>
                                                                <ajaxToolkit:CascadingDropDown runat="server" ID="ddlFilterC" Category="Column" ClientIDMode="Static"
                                                                    TargetControlID="ddlFilter" ServicePath="~/CascadeDropdown.asmx" ServiceMethod="GetFilteredColumns"
                                                                    ParentControlID="ddlDDTable">
                                                                </ajaxToolkit:CascadingDropDown>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trDDDisplayColumn" style="display: none;">
                                                            <td align="right">
                                                                <%--<HeaderTemplate>
                                                                                <input id="chkAll" onclick="DoMasterSelect(this, 'divOptionImageGrid')" runat="server" type="checkbox" />
                                                                            </HeaderTemplate>--%>
                                                                <asp:Label runat="server" ID="lblDisplayColumn" Text="Display Field" Font-Bold="true" ClientIDMode="Static"></asp:Label>
                                                            </td>
                                                            <td colspan="5">

                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="ddDDDisplayColumn" ClientIDMode="Static" CssClass="NormalTextBox">
                                                                            </asp:DropDownList>
                                                                            <ajaxToolkit:CascadingDropDown runat="server" ID="ddDDDisplayColumnC" Category="Column"
                                                                                ClientIDMode="Static" TargetControlID="ddDDDisplayColumn" ServicePath="~/CascadeDropdown.asmx"
                                                                                ServiceMethod="GetColumnsWithDefault" ParentControlID="ddlDDTable">
                                                                            </ajaxToolkit:CascadingDropDown>

                                                                            <asp:HiddenField runat="server" ID="hfDisplayColumnsFormula" ClientIDMode="Static" />

                                                                        </td>
                                                                        <td>
                                                                            <asp:HyperLink runat="server" ID="hlDDEdit" Text="Edit" ClientIDMode="Static" NavigateUrl="~/Pages/Help/TableColumn.aspx"></asp:HyperLink>
                                                                        </td>
                                                                        <td>
                                                                            <div id="divQuickAddLink">
                                                                                <asp:CheckBox runat="server" ID="chkQuickAddLink" TextAlign="Right" Text="Quick Add Link" Font-Bold="true" />
                                                                                <br />
                                                                                <%--<HeaderTemplate>
                                                                                <input id="chkAll" onclick="DoMasterSelect(this, 'divOptionImageGrid')" runat="server" type="checkbox" />
                                                                            </HeaderTemplate>--%>

                                                                                <asp:DropDownList runat="server" ID="ddlShowViewLink" CssClass="NormalTextBox">
                                                                                    <asp:ListItem Text="No View Link" Value=""></asp:ListItem>
                                                                                    <asp:ListItem Text="View Link on Detail" Value="Detail"></asp:ListItem>
                                                                                    <asp:ListItem Text="View Link on Summary" Value="Summary"></asp:ListItem>
                                                                                    <asp:ListItem Text="View Link on Both" Value="Both"></asp:ListItem>
                                                                                     <asp:ListItem Text="On invalid entry" Value="Invalid"></asp:ListItem>
                                                                                </asp:DropDownList>

                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                            </td>
                                                        </tr>

                                                        <!-- XXXFILES501 checkbox here -->
                                                        <tr id="chk_upload_image">
                                                            <td>&nbsp
                                                            </td>
                                                            <td align="left">
                                                                <asp:CheckBox runat="server" ID="chkUploadPhoto" Font-Bold="true" TextAlign="Right" Text="Allow user to upload image " onclick="chkUploadPhoto_onChange(this)" />
                                                            </td>
                                                        </tr>

                                                        

                                                        <tr id="file_upload_image" style="display: none">
                                                            <td align="right">
                                                                <asp:Label ID="lblBrowsePhoto" runat="server" Text="Upload Image" Font-Bold="True"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:FileUpload runat="server" ID="fileUpload_photo" ClientIDMode="Static" onchange="StartUpLoad" accept=".jpg,.png,.gif " EventName="Click" />

                                                            </td>
                                                        </tr>

                                                        <tr id="lblImage" style="display: none">
                                                            <td align="right">
                                                                <asp:Label ID="lblCurrentFileName_label" runat="server" Text="Last Uploaded" Font-Bold="True"></asp:Label>
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtCurrentFilename" runat="server" BorderStyle="None" ForeColor="#808080" ReadOnly="True"></asp:TextBox>
                                                            </td>
                                                        </tr>

                                                        <tr id="trSQLOptions">
                                                            <td align="right">
                                                                <strong>SQL Options</strong>
                                                            </td>
                                                            <td align="left">
                                                                <asp:RadioButtonList runat="server" ID="sqlOptions"
                                                                    RepeatDirection="Horizontal" ClientIDMode="Static">
                                                                    <asp:ListItem Text="Command" Value="command" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Text="Stored Procedure" Value="storedprocedure"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>

                                                            <td align="left"></td>
                                                            <td style="width: 10px;"></td>
                                                            <td align="right">
                                                                <%--<asp:CheckBox runat="server" ID="chkMandatory" Font-Bold="true" />--%>
                                                            </td>
                                                            <td>
                                                                <asp:Label runat="server" ID="Label2" ForeColor="Gray" Style="display: none;"></asp:Label>
                                                                <%--<asp:Label runat="server" ID="lblMandatory" Text="Mandatory" Font-Bold="true"></asp:Label>--%>
                                                            </td>
                                                        </tr>


                                                        <tr id="trSQLRetrieval">
                                                              <td align="right">
                                                                <asp:Label ID="lblsqlRetrieval" runat="server" Text="SQL" Font-Bold="True"></asp:Label>
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtSQLRetrieval" ClientIDMode="Static" Width="300px" TextMode="MultiLine" runat="server" BorderStyle="Solid"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="trSQLSPRetrieval">
                                                              <td align="right">
                                                                <asp:Label ID="lblsqlSPRetrieval" runat="server" Text="Stored Procedure" Font-Bold="True"></asp:Label>
                                                            </td>
                                                            <td align="left">
                                                                <asp:TextBox ID="txtSQLSPRetrieval" ClientIDMode="Static" Width="90%" runat="server" BorderStyle="Solid"></asp:TextBox>
                                                            </td>
                                                        </tr>

                                                        <tr id="trTriggeredBy">
                                                            <td align="right">
                                                                <strong>Triggered by</strong>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList runat="server" ID="ddlTriggeredBy" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                     
                                                                </asp:DropDownList> <span id="spansqltriggerlabel" style="font-size: 10px; font-style:italic;">On change as the @RecordID</span>
                                                            </td>

                                                            <td align="left"> </td>
                                                            <td style="width: 10px;"></td>
                                                           <td style="width: 10px;"></td>
                                                           <td style="width: 10px;"></td>
                                                        </tr>



                                                        <tr id="trMandatory">
                                                            <td align="right">
                                                                <strong>Importance</strong>
                                                            </td>
                                                            <td align="left">
                                                                <asp:DropDownList runat="server" ID="ddlImportance" ToolTip="Required means it is important but you can still save the data without it. Mandatory will prevent the data being saved unless entered." CssClass="NormalTextBox">
                                                                    <asp:ListItem Value="" Text="Optional" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Value="r" Text="Required"></asp:ListItem>
                                                                    <asp:ListItem Value="m" Text="Mandatory"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <%--<strong>Display Field</strong>--%><%--<asp:CheckBox runat="server" ID="chkShowViewLink" TextAlign="Right" Text="Show View Link" Font-Bold="true" />--%>
                                                            </td>

                                                            <td align="left"></td>
                                                            <td style="width: 10px;"></td>
                                                            <td align="right">
                                                                <%--<asp:CheckBox runat="server" ID="chkMandatory" Font-Bold="true" />--%>
                                                            </td>
                                                            <td>
                                                                <asp:Label runat="server" ID="lblSPDefaultValue" ForeColor="Gray" Style="display: none;"></asp:Label>
                                                                <%--<asp:Label runat="server" ID="lblMandatory" Text="Mandatory" Font-Bold="true"></asp:Label>--%>
                                                            </td>
                                                        </tr>

                                                        <tr clientidmode="Static" id="trDefaultValue" style="display: none;">
                                                            <td align="right">
                                                                <asp:Label runat="server" ID="lblDefauleValue" Text="Default Value" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td colspan="5">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:DropDownList runat="server" ID="ddlDefaultValue" ClientIDMode="Static" CssClass="NormalTextBox">
                                                                            </asp:DropDownList>
                                                                            <ajaxToolkit:CascadingDropDown runat="server" ID="ddlDefaultValueC" Category="DefaultValue"
                                                                                ClientIDMode="Static" TargetControlID="ddlDefaultValue" ServicePath="~/CascadeDropdown.asmx"
                                                                                ServiceMethod="GetDefaultValueOption" ParentControlID="ddlDDTable" >
                                                                            </ajaxToolkit:CascadingDropDown>
                                                                            <asp:HiddenField runat="server" ID="hf_ddlDefaultValue" ClientIDMode="Static" Value="none" />

                                                                        </td>

                                                                        <td align="left" style="padding-left: 2px;">
                                                                            <%--<asp:CheckBox runat="server" ID="chkReminders" CssClass="NormalTextBox" Font-Bold="true"
                                                                                ClientIDMode="Static" />--%><%--<asp:Label runat="server" ID="lblReminders" ClientIDMode="Static" Text="Reminders"
                                                                                Font-Bold="true"></asp:Label>--%>
                                                                            <asp:DropDownList runat="server" ID="ddDDDisplayColumnDefaultValue" ClientIDMode="Static" CssClass="NormalTextBox">
                                                                            </asp:DropDownList>
                                                                            <%--<ajaxToolkit:CascadingDropDown runat="server" ID="ddDDDisplayColumnDefaultValueC" Category="Column"
                                                                                ClientIDMode="Static" TargetControlID="ddDDDisplayColumnDefaultValue" ServicePath="~/CascadeDropdown.asmx"
                                                                                ServiceMethod="GetRecordColumn" ParentControlID="ddDDDisplayColumn">
                                                                            </ajaxToolkit:CascadingDropDown>--%>
                                                                            <%--//red 31082017--%>
                                                                            <asp:TextBox runat="server" ID="txtDefaultValue" CssClass="NormalTextBox" Width="165px"></asp:TextBox>
                                                                            <asp:HiddenField runat="server" ID="hf_ddDDDisplayColumnDefaultValue" ClientIDMode="Static" />
                                                                            <%-- Red Ticket 2060 --%>
                                                                        </td>

                                                                    </tr>

                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="trChildTable">
                                                            <td align="right"><strong>Child Table</strong></td>
                                                            <td align="left">
                                                                 <asp:DropDownList runat="server" ID="ddlChildTable" CssClass="NormalTextBox"
                                                                    ClientIDMode="Static">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr id="trChildTable2">
                                                            <td align="right"></td>
                                                            <td align="left">
                                                                 <asp:CheckBox runat="server" ID="chkQuickAddLink2" TextAlign="Right" Text="Show Add Icon" Font-Bold="true" />
                                                            </td>
                                                        </tr>
                                                        <tr id="tdDefaultParent" style="display: none;">
                                                            <td align="right"><strong>Table</strong></td>
                                                            <td align="left">
                                                                <asp:DropDownList runat="server" ID="ddlDefaultParentTable" CssClass="NormalTextBox"
                                                                    ClientIDMode="Static">
                                                                </asp:DropDownList>
                                                                <ajaxToolkit:CascadingDropDown runat="server" ID="ddlDefaultParentTableC" Category="Tableid"
                                                                    ClientIDMode="Static" TargetControlID="ddlDefaultParentTable" ServicePath="~/CascadeDropdown.asmx"
                                                                    ServiceMethod="GetRelatedTables">
                                                                </ajaxToolkit:CascadingDropDown>

                                                                <strong>Field</strong>
                                                                 <asp:DropDownList runat="server" ID="ddlDefaultParentColumn" ClientIDMode="Static"
                                                                    CssClass="NormalTextBox">
                                                                </asp:DropDownList>
                                                                <ajaxToolkit:CascadingDropDown runat="server" ID="ddlDefaultParentColumnC" Category="Column"
                                                                    ClientIDMode="Static" TargetControlID="ddlDefaultParentColumn" ServicePath="~/CascadeDropdown.asmx"
                                                                    ServiceMethod="GetDefaultParentColumns" ParentControlID="ddlDefaultParentTable">
                                                                </ajaxToolkit:CascadingDropDown>
                                                                <%-- Begin 2060 --%>
                                                            </td>
                                                           
                                                        </tr>
                                                        <tr>
                                                            <td align="right"></td>
                                                            <td id="tdDefaultSyncData" align="left">
                                                                <asp:CheckBox runat="server" ID="chkDefaultSyncData" TextAlign="Right" Font-Bold="true" />
                                                                <asp:Label runat="server" ID="lblDefaultSyncData" Font-Bold="true" TextAlign="Left" Text="Sync Data" />
                                                            </td>
                                                        </tr>

                                                        <tr id="trOptional">
                                                            <td align="right" id="tdOption">
                                                                <strong>Options</strong>
                                                            </td>
                                                            <td clientidmode="Static" align="left" id="trReadonly">
                                                                
                                                                <%--<asp:Label runat="server" ID="lblReadOnly" Font-Bold="true" TextAlign="Left" Text="Read Only" />--%>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox ClientIDMode="Static" CssClass="chkbox" runat="server" ID="chkReadOnly" Font-Bold="true"
                                                                                TextAlign="Right" Text="Read Only" />
                                                                        </td>
                                                                        <td style="padding-left:10px;" id="tdReadOnlyWhen">
                                                                             <asp:CheckBox runat="server" ID="chkConditionsRO" Text="" TextAlign="Right" Font-Bold="true"
                                                                                ClientIDMode="Static" />
                                                                            <asp:HyperLink runat="server" NavigateUrl="~/Pages/Record/ShowHide.aspx" CssClass="readOnlyWhenLink"
                                                                                ID="hlConditionsRO" ClientIDMode="Static">Read Only When...</asp:HyperLink>
                                                                            <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfShowHrefRO" />
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                            </td>
                                                        </tr>

                                                        <tr id="trEditableOnSummary">
                                                            <td>
                                                                
                                                            </td>
                                                            <td clientidmode="Static" align="left">                                                                
                                                               <asp:CheckBox ClientIDMode="Static" runat="server" ID="chkEditableOnSummary" Font-Bold="true"
                                                                                TextAlign="Right" Text="Edit On Summary Page" />
                                                            </td>
                                                        </tr>


                                                        <tr>
                                                            <td align="right"></td>
                                                            <td id="tdChkSecond" valign="top">
                                                                <asp:CheckBox ID="chkSecond" ClientIDMode="Static" TextAlign="Right" runat="server" />
                                                                <asp:Label runat="server" ID="lblSecond" Font-Bold="true" TextAlign="Left" Text="Do not show the seconds" />
                                                            </td>
                                                        </tr>
                                                        <tr id="tblColumnColour">
                                                            <td></td>
                                                            <td colspan="5">
                                                                <asp:CheckBox runat="server" ID="chkColumnColour" Text="" TextAlign="Right" Font-Bold="true"
                                                                    ClientIDMode="Static" />
                                                                <asp:HyperLink runat="server" NavigateUrl="~/Pages/Record/ColumnColourList.aspx" CssClass="colourlink"
                                                                    ID="hlColumnColour" ClientIDMode="Static">Set colour by value...</asp:HyperLink>
                                                            </td>
                                                        </tr>
                                                        <%--== Red: we'll be using Special Notification ==--%>
                                                        <tr clientidmode="Static" id="trReminders" style="display: none;">
                                                            <td align="right"></td>
                                                            <td colspan="5">
                                                                <%--<asp:Label runat="server" ID="lblShowTotal" Text="Show Field Total" Font-Bold="true"  Style="display: none;"></asp:Label>--%>
                                                                <asp:HyperLink runat="server" ID="hlReminders" ClientIDMode="Static" NavigateUrl="~/Pages/Schedule/DataReminder.aspx">Reminders</asp:HyperLink>

                                                            </td>
                                                        </tr>

                                                        <tr clientidmode="Static" id="trTextDimension">
                                                            <td align="right">
                                                                <strong>Width <asp:Label ID="lblPX1" ClientIDMode="Static" runat="server" /></strong>
                                                            </td>
                                                            <td colspan="5">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtTextWidth" Width="50px" Text="22"></asp:TextBox>&nbsp;
                                                                            <asp:RegularExpressionValidator ID="revtxtTextWidth" ControlToValidate="txtTextWidth"
                                                                                runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                            </asp:RegularExpressionValidator>
                                                                        </td>
                                                                        <td></td>
                                                                        <td id="tdHeightLabel">
                                                                            <strong>Height <asp:Label ID="lblPX2" ClientIDMode="Static" runat="server" /></strong>
                                                                        </td>
                                                                        <td id="tdHeight">
                                                                            <asp:TextBox runat="server" ID="txtTextHeight" Width="50px" Text="1"></asp:TextBox>&nbsp;
                                                                            <asp:RegularExpressionValidator ID="revtxtTextHeight" ControlToValidate="txtTextHeight"
                                                                                runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                            </asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trRecordCountTable" style="display: none;">
                                                            <td align="right">
                                                                <asp:Label runat="server" ID="Label1" Text="Table" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:DropDownList runat="server" ID="ddlRecordCountTable" CssClass="NormalTextBox"
                                                                    DataValueField="TableID" DataTextField="TableName">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trRecordCountClick" style="display: none;">
                                                            <td align="right">
                                                                <asp:Label runat="server" ID="Label3" Text="Click" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:DropDownList runat="server" ID="ddlRecordCountClick" CssClass="NormalTextBox">
                                                                    <asp:ListItem Value="no" Text="No action" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Value="open" Text="Open Child Records"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trSlider" style="display: none;">
                                                            <td align="right"></td>
                                                            <td colspan="5">
                                                                <table>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <strong>Min</strong>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtSliderMin" CssClass="NormalTextBox" Width="50px" Text="0"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="revtxtSliderMin" ControlToValidate="txtSliderMin"
                                                                                runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                            </asp:RegularExpressionValidator>
                                                                        </td>

                                                                        <td align="right">
                                                                            <strong>Max</strong>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtSliderMax" CssClass="NormalTextBox" Width="50px" Text="100"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="revtxtSliderMax" ControlToValidate="txtSliderMax"
                                                                                runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                            </asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trColumnToAvg" style="display: none;">
                                                            <td align="right">
                                                                <asp:Label runat="server" ID="lblColumnToAvg" Text="Field to Avg" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:DropDownList runat="server" ID="ddlAvgColumn" CssClass="NormalTextBox" DataValueField="ColumnID"
                                                                    DataTextField="DisplayName">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trAvgNumValues" style="display: none;">
                                                            <td align="right">
                                                                <asp:Label runat="server" ID="lblAvgNumValues" Text="Avg Num Values" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="txtAvgNumValues" CssClass="NormalTextBox" Width="50px"></asp:TextBox>
                                                            </td>
                                                            <td></td>
                                                            <td style="width: 10px;"></td>
                                                            <td align="right"></td>
                                                            <td>
                                                                <asp:RegularExpressionValidator ID="revtxtAvgNumValues" ControlToValidate="txtAvgNumValues"
                                                                    runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                </asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trCurrencySymbol" style="display: none;">
                                                            <td align="right">
                                                                <asp:Label runat="server" ID="lblSymbol" Text="Symbol" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:TextBox runat="server" ID="txtSymbol" Text="$" CssClass="NormalTextBox" Width="50px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trRound" style="display: none;">
                                                            <td>&nbsp;</td>
                                                            <td colspan="5">
                                                                <asp:CheckBox runat="server" Font-Bold="true" ID="chkRound" />
                                                                <asp:Label runat="server" ID="lblRound" Font-Bold="true" Text="Decimal Places"></asp:Label>
                                                                &nbsp;
                                                                <asp:TextBox runat="server" ID="txtRoundNumber" CssClass="NormalTextBox" Width="50px"></asp:TextBox>
                                                                <%--<asp:CheckBox runat="server" Font-Bold="true" ID="chkShowTotal" />--%>
                                                                <asp:RegularExpressionValidator ID="revtxtRoundNumber" ControlToValidate="txtRoundNumber"
                                                                    runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                </asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trIgnoreSymbols" style="display: none;">
                                                            <td>&nbsp;</td>
                                                            <td colspan="5">
                                                                <asp:CheckBox runat="server" Font-Bold="true" ID="chkIgnoreSymbols" ClientIDMode="Static" />
                                                                <asp:Label runat="server" ID="lblIgnoreSymbols" Text="Ignore Symbols" Font-Bold="true"></asp:Label>
                                                                <asp:Panel runat="server" ID="panelSymbolMode" ClientIDMode="Static">
                                                                    <div style="padding-left: 20px; padding-top: 5px;">
                                                                        <asp:Label runat="server">Treat values below the detection level as</asp:Label>
                                                                        <br />
                                                                        <asp:DropDownList runat="server" ID="ddlSymbolMode" ClientIDMode="Static">
                                                                            <asp:ListItem Value="0" Text="Zero" />
                                                                            <asp:ListItem Value="1" Text="Detection Limit" />
                                                                            <asp:ListItem Value="2" Text="Half the Detection Limit" />
                                                                            <asp:ListItem Value="3" Text="Detection Limit / square root of 2" />
                                                                            <asp:ListItem Value="4" Text="Constant" />
                                                                        </asp:DropDownList>
                                                                        &nbsp;
                                                                        <asp:TextBox Style="width: 5em;" runat="server" ID="txtSymbolModeConstant" />
                                                                        <br />
                                                                        <asp:CustomValidator
                                                                            ID="cvSymbolModeConstant" runat="server"
                                                                            ErrorMessage="Constant should be numeric" ClientValidationFunction="validateSymbolModeConstant"
                                                                            ControlToValidate="txtSymbolModeConstant" ValidateEmptyText="true">
                                                                        </asp:CustomValidator>
                                                                    </div>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trCalculationNullAsZero" style="display: none;">
                                                            <td>&nbsp;</td>
                                                            <td colspan="5">
                                                                <asp:CheckBox runat="server" ID="chkCalculationNullAsZero"
                                                                    ToolTip="The calculation will bring back empty value if this check box is not ticked and if any value used in calculation is not set" />
                                                                <asp:Label runat="server" ID="lblCalculationNullAsZero" Text="Treat NULL as zero"
                                                                    Font-Bold="true"
                                                                    ToolTip="The calculation will bring back empty value if this check box is not ticked and if any value used in calculation is not set">
                                                                </asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr clientidmode="Static" id="trCheckUnlikelyValue" style="display: none;">
                                                            <td>&nbsp;</td>
                                                            <td colspan="5">
                                                                <asp:CheckBox runat="server" Font-Bold="true" ID="chkCheckUnlikelyValue" ToolTip="Outside 3 standard deviations" />
                                                                <asp:Label runat="server" ID="lblCheckUnlikelyValue" Text="Warn of Unlikely Readings"
                                                                    Font-Bold="true" ToolTip="Outside 3 standard deviations"></asp:Label>
                                                            </td>
                                                        </tr>

                                                        <tr clientidmode="Static" id="trFlatLine" style="display: none;">
                                                            <td>&nbsp;</td>
                                                            <td colspan="5">
                                                                <asp:CheckBox runat="server" ID="chkFlatLine" ClientIDMode="Static" />
                                                                <asp:Label runat="server" ID="lblCheckForFlat" Text="Check for Flat Line" ClientIDMode="Static"
                                                                    Font-Bold="true"></asp:Label>
                                                                <div style="padding-left: 20px; padding-top: 5px;">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="padding-left: 3px;">
                                                                                <asp:Label runat="server" ID="lblFlatlineNumber" Text="Number of entries" ClientIDMode="Static"></asp:Label>
                                                                            </td>
                                                                            <td style="padding-left: 5px;">
                                                                                <asp:TextBox runat="server" ID="txtFlatLine" CssClass="NormalTextBox" Width="75px"
                                                                                    ClientIDMode="Static"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <asp:RegularExpressionValidator ID="revtxtFlatLine" ControlToValidate="txtFlatLine"
                                                                                    runat="server" ErrorMessage="Invalid Flat line number of entries!" Display="Dynamic"
                                                                                    ValidationExpression="^[0-9]+$">
                                                                                </asp:RegularExpressionValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="6"></td>
                                                        </tr>
                                                        <tr id="trImageHeightSummary" style="display: none;">
                                                            <td align="right">
                                                                <strong>Max Size on Summary</strong>
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:TextBox runat="server" ID="txtImageHeightSummary" TextMode="SingleLine" CssClass="NormalTextBox"
                                                                    Width="100px"></asp:TextBox>&nbsp;px
                                                                <asp:RegularExpressionValidator ID="revtxtImageHeightSummary" ControlToValidate="txtImageHeightSummary"
                                                                    runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                </asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                        <tr id="trImageHeightDetail" style="display: none;">
                                                            <td align="right">
                                                                <strong>Max Size on Detail</strong>
                                                            </td>
                                                            <td colspan="5">
                                                                <asp:TextBox runat="server" ID="txtImageHeightDetail" TextMode="SingleLine" CssClass="NormalTextBox"
                                                                    Width="100px"></asp:TextBox>&nbsp;px
                                                                <asp:RegularExpressionValidator ID="revtxtImageHeightDetail" ControlToValidate="txtImageHeightDetail"
                                                                    runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                </asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>



                                                        <%-- End 2060 --%><%--<asp:HiddenField runat="server" ID="hf_ddlDefaultParentTable" ClientIDMode="Static" />--%>
                                                    </table>
                                                </div>
                                                <br />
                                                <div id="divGraphOptions" style="border-style: solid; border-width: 1px; min-width: 450px; padding: 5px; display: none;">
                                                    <table>
                                                        <tr>
                                                            <td style="width: 100px;" valign="top" align="right">
                                                                <strong>Graph Options</strong>
                                                            </td>
                                                            <td valign="top" align="left">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox runat="server" ID="chkWarning" Text="Show Warning at" TextAlign="Right"
                                                                                Font-Bold="true" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtWarningMin" CssClass="NormalTextBox" Width="70px"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="revtxtWarningMin" ControlToValidate="txtWarningMin"
                                                                                runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                            </asp:RegularExpressionValidator>
                                                                            <asp:TextBox runat="server" ID="txtWarningMax" CssClass="NormalTextBox" Width="70px"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="revtxtWarningMax" ControlToValidate="txtWarningMax"
                                                                                runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                            </asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox runat="server" ID="chkExceedence" Text="Show Exceedance at" TextAlign="Right"
                                                                                Font-Bold="true" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtExceedenceMin" CssClass="NormalTextBox" Width="70px"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="revtxtExceedenceMin" ControlToValidate="txtExceedenceMin"
                                                                                runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                            </asp:RegularExpressionValidator>
                                                                            <asp:TextBox runat="server" ID="txtExceedenceMax" CssClass="NormalTextBox" Width="70px"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="revtxtExceedenceMax" ControlToValidate="txtExceedenceMax"
                                                                                runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                            </asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:CheckBox runat="server" ID="chkMaximumValueat" Text="Maximum Value at" TextAlign="Right"
                                                                                Font-Bold="true" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtMaximumValueat" CssClass="NormalTextBox" Width="70px"></asp:TextBox>
                                                                            <asp:RegularExpressionValidator ID="revtxtMaximumValueat" ControlToValidate="txtMaximumValueat"
                                                                                runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)">
                                                                            </asp:RegularExpressionValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100px;" valign="top" align="right">
                                                                <strong>Default Type</strong>
                                                            </td>
                                                            <td style="padding-left: 0.7em;">
                                                                <asp:DropDownList runat="server" ID="ddlDefaultGraphDefinition"
                                                                    DataTextField="DefinitionName" DataValueField="GraphDefinitionID"
                                                                    AppendDataBoundItems="true"
                                                                    CssClass="NormalTextBox" Width="262px">
                                                                    <asp:ListItem Text="--Please Select--" Value="-1"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div style="width: 450px; height: 140px;">
                                                    <table>
                                                        <tr id="trHelpText">
                                                            <td>
                                                                <table style="height:300px">
                                                                    <tr align="left">
                                                                        <td style="margin-top:10px; vertical-align:bottom">
                                                                            <strong>Help Text</strong>&nbsp; (appears when user clicks the bubble)
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 110px;">
                                                                          <%--  <asp:TextBox ID="txtNotes" AcceptsReturns="true" runat="server" CssClass="MultiLineTextBox" TextMode="MultiLine"
                                                                                Width="450px" Height="96px" Visible="false"></asp:TextBox>--%>
                                                                             <editor:WYSIWYGEditor runat="server" scriptPath="../../Editor/scripts/" ID="txtNotes" 
                                                                                btnSave="false" AssetManager="../../assetmanager/assetmanager.aspx" />
                                                                           <%-- <textarea id="txtNotes" runat="server" style="width:450px; height:96px"></textarea>--%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="trRowSpan" style="height:50px">
                                                            <td>
                                                                <table>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <strong>Row Span</strong>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtRowSpan" TextMode="Number" CssClass="NormalTextBox" Width="50px" MaxLength="3"></asp:TextBox>
                                                                        </td>
                                                                        <td align="right">
                                                                            <strong>Column Span</strong>
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox runat="server" ID="txtColumnSpan" TextMode="Number" CssClass="NormalTextBox" Width="50px" MaxLength="3"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="height: 10px;"></td>
                                        </tr>
                                        <tr>
                                            <td valign="top"></td>
                                            <td valign="top" style="padding-left: 10px;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3" height="13"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <div runat="server" id="divHistoryRoot" visible="false">
                        <a id="lnkShowHistoryLog" class="popupshowColumnLog" href="../Record/RecordColumnHistory.aspx" >Show Change History...</a>
                    </div>
                  
                </td>
            </tr>
            <tr>
                <td colspan="3" height="50">
                    <asp:Button ID="btnRefreshRminder" runat="server" ClientIDMode="Static" OnClick="btnRefreshRminder_Click"
                        Style="display: none;" />

                    <asp:Button ID="btnOptionImage" runat="server" ClientIDMode="Static" OnClick="btnOptionImage_Click"
                        Style="display: none;" />
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        //RP Added Ticket 4378 - Added additional function (checking of Email field)
        function CheckEmailField(e) {
            if (Page_IsValid)
            {
                var columnName = $("#<%=txtColumnName.ClientID %>").val();
                var fieldType = $("#<%=ddlType.ClientID %>").val();
                var textType = $("#<%=ddlTextType.ClientID %>").val();

                var regx = /email/i;

                if (columnName.match(regx) != null)
                {
                    if (fieldType != "text" || textType != "email")
                    {
                        //RP Modified Ticket 4378 - Additional Fix
                        if (confirm("If this is an email field we suggest you set the Text Type to Email address(es). Continue with save?")) {
                            $(".ajax-indicator-full").fadeIn();
                            return true;
                        }
                        else {
                            $("#ctl00_HomeContentPlaceHolder_lnkSave").off(); //Turning white wash off when canceled
                            return false;
                        }
                        //End Modification
                    }
                }

                return true;
            }
        }
        //End Modification
        function pageLoad() {
            changeDefaultValueOption();
        }
        function changeDefaultValueOption() {
            if ($get("ddlDefaultValue").options.length > 0) {
                var strDateTimeTypeV = $('#ddlDateTimeType').val();
                var strTypeV = $('#ctl00_HomeContentPlaceHolder_ddlType').val();
                if (strTypeV == 'date_time') {
                    if (strDateTimeTypeV == 'datetime' || strDateTimeTypeV == 'time') {
                        $('#ddlDefaultValue option[value="value"]').text('Today/Now');
                        $('#ddlDefaultValue option[value="additional"]').text('Add X Hours');
                    }
                    if (strDateTimeTypeV == 'date') {
                        $('#ddlDefaultValue option[value="value"]').text('Today/Now');
                        $('#ddlDefaultValue option[value="additional"]').text('Add X Days');
                    }

                    $('#ddlDefaultValue option[value="additional"]').show();
                }
                else {
                    //alert('Hello');
                    //console.log($('#ddlDefaultValue option[value="value"]'));
                    $('#ddlDefaultValue option[value="value"]').text('Value');
                    $('#ddlDefaultValue option[value="additional"]').hide();
                }
            }
            else {
                setTimeout("changeDefaultValueOption()", 500);
                //changeDefaultValueOption();
            }
        }
    </script>
</asp:Content>

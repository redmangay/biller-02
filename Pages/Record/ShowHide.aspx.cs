﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class Pages_Record_ShowHide : System.Web.UI.Page
{
   
    int _iTableID = -1;
    int _iColumnID = -1;
    int _iDocumentSectionID = -1;
    int _iSpecialNotificationID = -1;
    int _iTableTabID = -1;
    string _strContext = "field";
    string _strMode = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["TableID"]!=null)
            _iTableID =int.Parse( Request.QueryString["TableID"].ToString());

        if (Request.QueryString["ColumnID"] != null)
            _iColumnID = int.Parse( Request.QueryString["ColumnID"].ToString());

        if (Request.QueryString["DocumentSectionID"] != null)
            _iDocumentSectionID = int.Parse(Request.QueryString["DocumentSectionID"].ToString());

        if (Request.QueryString["TableTabID"] != null)
            _iTableTabID = int.Parse(Request.QueryString["TableTabID"].ToString());

        if (Request.QueryString["SpecialNotificationID"] != null)
            _iSpecialNotificationID = int.Parse(Request.QueryString["SpecialNotificationID"].ToString());

        if (Request.QueryString["mode"] != null)
            _strMode = Cryptography.Decrypt(Request.QueryString["mode"].ToString());

        if (Request.QueryString["Context"] != null)
            _strContext = Request.QueryString["Context"].ToString().ToLower();


        if (!IsPostBack)
        {
            if(_strContext=="readonly")
            {
                h3Title.InnerText = "Read Only When";
            }

            if (_strContext == "specialnotification")
            {
                h3Title.InnerText = "Send When";
            }
            PopulateConditions();
           
        }
    }


    protected void SWCHideColumnChanged(object sender, EventArgs e)
    {
        Pages_UserControl_ConditionsCondition swcConditions = sender as Pages_UserControl_ConditionsCondition;

        if (swcConditions != null)
        {
            GridViewRow row = swcConditions.NamingContainer as GridViewRow;
            Label lblID = row.FindControl("lblID") as Label;
            Label lblConditionsID = row.FindControl("lblConditionsID") as Label;
            ImageButton imgbtnMinus = row.FindControl("imgbtnMinus") as ImageButton;
            ImageButton imgbtnPlus = row.FindControl("imgbtnPlus") as ImageButton;

            if (swcConditions.ddlHideColumnV == "")
            {
                imgbtnPlus.Visible = false;
            }
            else
            {
                imgbtnPlus.Visible = true;
            }
        }


    }

    protected void grdConditions_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblID = e.Row.FindControl("lblID") as Label;
                Label lblConditionsID = e.Row.FindControl("lblConditionsID") as Label;
                ImageButton imgbtnMinus = e.Row.FindControl("imgbtnMinus") as ImageButton;
                ImageButton imgbtnPlus = e.Row.FindControl("imgbtnPlus") as ImageButton;

                //imgbtnPlus.CommandArgument = e.Row.RowIndex.ToString();
                //if(grdConditions.Rows.Count==1)
                //{
                //    imgbtnMinus.Visible = false;
                //}


                Pages_UserControl_ConditionsCondition swcConditions = e.Row.FindControl("swcConditions") as Pages_UserControl_ConditionsCondition;

                if (_strContext=="dashboard")
                {
                   // swcConditions.TableID = _iTableID;
                    swcConditions.ShowTable = true;
                    swcConditions.DocumentSectionID = _iDocumentSectionID;
                }
                else if (_strContext == "tabletab")
                {                  
                    swcConditions.TableID = _iTableID;
                    swcConditions.TableTabID = _iTableTabID;
                }
                else if (_strContext == "specialnotification")
                {
                    swcConditions.TableID = _iTableID;
                    swcConditions.SpecialNotificationID = _iSpecialNotificationID;
                }
                else
                {
                    swcConditions.TableID = _iTableID;
                    swcConditions.ColumnID = _iColumnID;
                }
                


                if (e.Row.RowIndex == 0)
                {
                    swcConditions.ShowJoinOperator = false;
                    swcConditions.ddlJoinOperatorV = "and";
                }
                else
                {
                    swcConditions.ShowJoinOperator = true;
                    swcConditions.ddlJoinOperatorV = DataBinder.Eval(e.Row.DataItem, "JoinOperator").ToString();
                }

                swcConditions.ddlOperatorV = DataBinder.Eval(e.Row.DataItem, "ConditionOperator").ToString();
                swcConditions.ddlHideColumnV = DataBinder.Eval(e.Row.DataItem, "ConditionColumnID").ToString();
                swcConditions.hfConditionValueV = DataBinder.Eval(e.Row.DataItem, "ConditionValue").ToString();


                lblConditionsID.Text = DataBinder.Eval(e.Row.DataItem, "ConditionsID").ToString();
                lblID.Text = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
                imgbtnMinus.CommandArgument = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
                imgbtnPlus.CommandArgument = DataBinder.Eval(e.Row.DataItem, "ID").ToString();


            }
        }
        catch
        {
            //
        }
    }


    protected void SetConditionsRowData()
    {

        if (ViewState["dtConditions"] != null)
        {
            DataTable dtConditions = (DataTable)ViewState["dtConditions"];
            dtConditions.Rows.Clear();

            int iDisplayOrder = 0;
            if (grdConditions.Rows.Count > 0)
            {
                foreach (GridViewRow gvRow in grdConditions.Rows)
                {

                    Label lblID = gvRow.FindControl("lblID") as Label;
                    Label lblConditionsID = gvRow.FindControl("lblConditionsID") as Label;
                    Pages_UserControl_ConditionsCondition swcConditions = gvRow.FindControl("swcConditions") as Pages_UserControl_ConditionsCondition;


                    dtConditions.Rows.Add(lblID.Text, lblConditionsID.Text,(_strContext== "field" || _strContext == "readonly") ? _iColumnID.ToString():"", swcConditions.ddlHideColumnV,
                        swcConditions.hfConditionValueV, swcConditions.ddlOperatorV, iDisplayOrder.ToString(), swcConditions.ddlJoinOperatorV,                      
                        _strContext == "dashboard" ? _iDocumentSectionID.ToString() : "",
                         _strContext == "tabletab" ? _iTableTabID.ToString() : "",
                        _strContext, _strContext == "specialnotification" ? _iSpecialNotificationID.ToString() : "");
                    iDisplayOrder = iDisplayOrder + 1;
                }

            }
            dtConditions.AcceptChanges();
            ViewState["dtConditions"] = dtConditions;


        }
    }

    public string ParentCheckboxName()
    {
        string sName = "chkConditions";
        if(_strContext=="readonly")
        {
            sName = "chkConditionsRO";
        }

        return sName;
    }

    protected void PopulateConditions()
    {
        DataTable dtConditions = new DataTable();
        dtConditions.Columns.Add("ID");
        dtConditions.Columns.Add("ConditionsID");
        dtConditions.Columns.Add("ColumnID");
        dtConditions.Columns.Add("ConditionColumnID");
        dtConditions.Columns.Add("ConditionValue");
        dtConditions.Columns.Add("ConditionOperator");
        dtConditions.Columns.Add("DisplayOrder");
        dtConditions.Columns.Add("JoinOperator");
        dtConditions.Columns.Add("DocumentSectionID");       
        dtConditions.Columns.Add("Context");
        dtConditions.Columns.Add("TableTabID");
        dtConditions.Columns.Add("SpecialNotificationID");
        dtConditions.AcceptChanges();

        if ((_iColumnID != -1 || _iDocumentSectionID!=-1 || _iTableTabID!=-1 || (_iSpecialNotificationID != -1 && _strMode=="old" )) && ViewState["dtConditions"] == null)
        {


            DataTable dtConditionsDB = RecordManager.dbg_Conditions_ForGrid((_strContext == "field" || _strContext == "readonly") ? (int?)_iColumnID : null,                                                                   
                                                                    _strContext == "dashboard" ? (int?)_iDocumentSectionID : null,
                                                                    _strContext =="tabletab"?(int?)_iTableTabID:null,
                                                                     _strContext == "specialnotification" ? (int?)_iSpecialNotificationID : null, 
                                                                     _strContext);

            if (dtConditions != null && dtConditionsDB != null)
            {

                //dtConditions.Columns.Add("ID");

                //dtConditions.AcceptChanges();

                foreach (DataRow dr in dtConditionsDB.Rows)
                {
                    DataRow newRow = dtConditions.NewRow();
                    newRow[0] = Guid.NewGuid().ToString();
                    newRow[1] = dr[1].ToString();
                    newRow[2] = dr[2].ToString();
                    newRow[3] = dr[3].ToString();
                    newRow[4] = dr[4].ToString();
                    newRow[5] = dr[5].ToString();
                    newRow[6] = dr[6].ToString();
                    newRow[7] = dr[7].ToString();
                    newRow[8] = dr[8].ToString();
                    newRow[8] = dr[9].ToString();
                    newRow[9] = dr[10].ToString();
                    newRow[10] = dr[11].ToString();
                    dtConditions.Rows.Add(newRow);
                }

                //foreach(DataRow dr in dtConditions.Rows)
                //{
                //    dr["ID"] = Guid.NewGuid().ToString();
                //}
                dtConditions.AcceptChanges();

                if(dtConditions.Rows.Count==0)
                {
                    dtConditions.Rows.Add(Guid.NewGuid().ToString(),"-1", (_strContext== "field" || _strContext == "readonly") ?"-1":"",
                        "", "", "equals", "1", "", _strContext == "dashboard" ? "-1" : "", _strContext, _strContext == "tabletab" ? "-1" : "", _strContext == "specialnotification" ? "-1" : "");
                    dtConditions.AcceptChanges();
                }

                ViewState["dtConditions"] = dtConditions;

                grdConditions.DataSource = dtConditions;
                grdConditions.DataBind();

            }
        }
        else
        {
            if (ViewState["dtConditions"] == null)
            {
                if(_strContext=="readonly")
                {
                    if (Session["dtConditionsRO"] == null)
                    {
                        dtConditions.Rows.Add(Guid.NewGuid().ToString(), "-1", (_strContext == "field" || _strContext == "readonly") ? "-1" : "", "", "",
                            "equals", "1", "", _strContext == "dashboard" ? "-1" : "", _strContext, _strContext == "tabletab" ? "-1" : "");

                        grdConditions.DataSource = dtConditions;
                        grdConditions.DataBind();

                        ViewState["dtConditions"] = dtConditions;
                    }
                    else
                    {
                        dtConditions = (DataTable)Session["dtConditionsRO"];
                        grdConditions.DataSource = dtConditions;
                        grdConditions.DataBind();

                        ViewState["dtConditions"] = dtConditions;

                    }
                }
                else
                {
                    if (Session["dtConditions"] == null)
                    {
                        dtConditions.Rows.Add(Guid.NewGuid().ToString(), "-1", (_strContext == "field" || _strContext == "readonly") ? "-1" : "", "", "",
                            "equals", "1", "", _strContext == "dashboard" ? "-1" : "", _strContext, _strContext == "tabletab" ? "-1" : "");

                        grdConditions.DataSource = dtConditions;
                        grdConditions.DataBind();

                        ViewState["dtConditions"] = dtConditions;
                    }
                    else
                    {
                        dtConditions = (DataTable)Session["dtConditions"];
                        grdConditions.DataSource = dtConditions;
                        grdConditions.DataBind();

                        ViewState["dtConditions"] = dtConditions;

                    }
                }

               
              
            }
            else
            {
                dtConditions = (DataTable)ViewState["dtConditions"];
                grdConditions.DataSource = dtConditions;
                grdConditions.DataBind();
            }


        }
    }


    protected void grdConditions_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            SetConditionsRowData();

            if (e.CommandName == "minus")
            {
                if (ViewState["dtConditions"] != null)
                {
                    DataTable dtConditions = (DataTable)ViewState["dtConditions"];

                    if (dtConditions.Rows.Count == 1)
                    {
                        return;
                    }

                    for (int i = dtConditions.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dtConditions.Rows[i];
                        if (dr["id"].ToString() == e.CommandArgument.ToString())
                        {
                            dr.Delete();
                            break;
                        }

                    }


                    dtConditions.AcceptChanges();

                    ViewState["dtConditions"] = dtConditions;

                    PopulateConditions();
                    //PopulatePreviousColorConditions();


                }
            }
            if (e.CommandName == "plus")
            {
                if (ViewState["dtConditions"] != null)
                {
                    DataTable dtConditions = (DataTable)ViewState["dtConditions"];


                    int iPos = 0;

                    for (int i = 0; i < dtConditions.Rows.Count; i++)
                    {
                        if (dtConditions.Rows[i][0].ToString() == e.CommandArgument.ToString())
                        {
                            iPos = i;
                            break;
                        }
                    }


                    DataRow newRow = dtConditions.NewRow();
                    newRow[0] = Guid.NewGuid().ToString();
                    newRow[1] = "-1";
                    newRow[2] = (_strContext == "field" || _strContext == "readonly") ? _iColumnID.ToString() : "";
                    newRow[3] = "";
                    newRow[4] = "";
                    newRow[5] = "equals";
                    newRow[6] = (dtConditions.Rows.Count + 1).ToString();
                    newRow[7] = "";
                    newRow[8] = _strContext == "dashboard" ? _iDocumentSectionID.ToString() : "";
                    newRow[9] = _strContext;
                    newRow[10] = _strContext == "tabletab" ? _iTableTabID.ToString() : "";
                    newRow[10] = _strContext == "specialnotification" ? _iSpecialNotificationID.ToString() : "";

                    dtConditions.Rows.InsertAt(newRow, iPos + 1);

                    dtConditions.AcceptChanges();

                    ViewState["dtConditions"] = dtConditions;

                    PopulateConditions();
                    //PopulatePreviousColorConditions();

                    //grdConditions.Rows.
                }

            }
        }
        catch
        {
            //
        }
    }

  
        

    protected void lnkSave_Click(object sender, EventArgs e)
    {

        SetConditionsRowData();

        if (ViewState["dtConditions"] != null && ((
            (_strContext == "field" || _strContext == "readonly") && _iColumnID != -1)
            || (_strContext == "specialnotification" && _iSpecialNotificationID != -1) 
            || (_strContext == "dashboard" && _iDocumentSectionID != -1) 
            || (_strContext == "tabletab" && _iTableTabID != -1)))
        {
            //Common.ExecuteText("UPDATE [Column] SET ConditionColumnID=" + ddlHideColumn.SelectedValue
            //    + ", ConditionValue='" + hfConditionValue.Value.Replace("'", "''") + "',ConditionOperator='"+ddlOperator.SelectedValue+"' WHERE ColumnID=" + _qsColumnID);

            DataTable dtOldConditions = RecordManager.dbg_Conditions_Select((_strContext == "field" || _strContext=="readonly") ? (int?)_iColumnID : null,
                                                                        _strContext == "dashboard" ? (int?)_iDocumentSectionID : null,
                                                                        _strContext == "tabletab" ? (int?)_iTableTabID : null,
                                                                        _strContext == "specialnotification" ? (int?)_iSpecialNotificationID : null,
                                                                        _strContext);

            DataTable dtConditions = (DataTable)ViewState["dtConditions"];

            int check = dtConditions.Rows.Count;
            int iOldRowsCount = dtOldConditions == null ? 0 : dtOldConditions.Rows.Count;
            string strActiveConditionsIDs = "-1";
            int iDO = 1;

            bool bInsert = false;

            foreach (DataRow drSW in dtConditions.Rows)
            {
                if (iDO == 1)
                {
                    if (drSW["ConditionColumnID"].ToString() == "") //|| drSW["ConditionValue"].ToString() == ""
                    {
                        continue;
                    }
                    Conditions theConditions1 = new Conditions();

                    if (iOldRowsCount > 0)
                        theConditions1.ConditionsID = int.Parse(dtOldConditions.Rows[0]["ConditionsID"].ToString());

                    theConditions1.ColumnID = (_strContext == "field" || _strContext=="readonly") ? (int?)_iColumnID : null;
                    theConditions1.DocumentSectionID = _strContext == "dashboard" ? (int?)_iDocumentSectionID : null;
                    theConditions1.TableTabID = _strContext == "tabletab" ? (int?)_iTableTabID : null;
                    theConditions1.Context = _strContext;

                    theConditions1.ConditionColumnID = int.Parse(drSW["ConditionColumnID"].ToString());
                    theConditions1.ConditionValue = drSW["ConditionValue"].ToString();
                    theConditions1.ConditionOperator = drSW["ConditionOperator"].ToString();
                    theConditions1.DisplayOrder = 1;
                    theConditions1.JoinOperator = "";
                    theConditions1.SpecialNotificationID = _strContext == "specialnotification" ? (int?)_iSpecialNotificationID : null;

                    if (iOldRowsCount > 0)
                    {
                        RecordManager.dbg_Conditions_Update(theConditions1);
                    }
                    else
                    {
                        theConditions1.ConditionsID = RecordManager.dbg_Conditions_Insert(theConditions1);
                        bInsert = true;
                    }


                    strActiveConditionsIDs = strActiveConditionsIDs + "," + theConditions1.ConditionsID.ToString();
                    iDO = iDO + 1;
                    continue;
                }
                //|| drSW["ConditionValue"].ToString() == ""
                if (drSW["ConditionColumnID"].ToString() == ""  || drSW["JoinOperator"].ToString() == "")
                {
                    continue;
                }




                if (iOldRowsCount - iDO < 0)
                {
                    bInsert = true;
                }

                if (bInsert == false)
                {
                    Conditions theConditionsJoin = new Conditions();
                    theConditionsJoin.ConditionsID = int.Parse(dtOldConditions.Rows[iDO - 1]["ConditionsID"].ToString());

                    theConditionsJoin.ColumnID = (_strContext == "field" || _strContext == "readonly") ? (int?)_iColumnID : null;
                    theConditionsJoin.DocumentSectionID = _strContext == "dashboard" ? (int?)_iDocumentSectionID : null;
                    theConditionsJoin.TableTabID = _strContext == "tabletab" ? (int?)_iTableTabID : null;
                    theConditionsJoin.Context = _strContext;

                    theConditionsJoin.ConditionColumnID = null;
                    theConditionsJoin.ConditionValue = "";
                    theConditionsJoin.ConditionOperator = "";
                    theConditionsJoin.DisplayOrder = iDO;
                    theConditionsJoin.JoinOperator = drSW["JoinOperator"].ToString();
                    theConditionsJoin.SpecialNotificationID = _strContext == "specialnotification" ? (int?)_iSpecialNotificationID : null;

                    RecordManager.dbg_Conditions_Update(theConditionsJoin);

                    strActiveConditionsIDs = strActiveConditionsIDs + "," + theConditionsJoin.ConditionsID.ToString();
                    iDO = iDO + 1;

                    if (iOldRowsCount - iDO < 0)
                    {
                        bInsert = true;
                    }


                    Conditions theConditions = new Conditions();
                    if (bInsert == false)
                        theConditions.ConditionsID = int.Parse(dtOldConditions.Rows[iDO - 1]["ConditionsID"].ToString());

                   
                    theConditions.ColumnID = (_strContext == "field" || _strContext == "readonly") ? (int?)_iColumnID : null;
                    theConditions.DocumentSectionID = _strContext == "dashboard" ? (int?)_iDocumentSectionID : null;
                    theConditions.TableTabID = _strContext == "tabletab" ? (int?)_iTableTabID : null;
                    theConditions.Context = _strContext;


                    theConditions.ConditionColumnID = int.Parse(drSW["ConditionColumnID"].ToString());
                    theConditions.ConditionValue = drSW["ConditionValue"].ToString();
                    theConditions.ConditionOperator = drSW["ConditionOperator"].ToString();
                    theConditions.DisplayOrder = iDO;
                    theConditions.JoinOperator = "";
                    theConditions.SpecialNotificationID = _strContext == "specialnotification" ? (int?)_iSpecialNotificationID : null;

                    if (bInsert == false)
                    {
                        RecordManager.dbg_Conditions_Update(theConditions);
                    }
                    else
                    {
                        theConditions.ConditionsID = RecordManager.dbg_Conditions_Insert(theConditions);
                    }


                    strActiveConditionsIDs = strActiveConditionsIDs + "," + theConditions.ConditionsID.ToString();
                    iDO = iDO + 1;

                    if (iOldRowsCount - iDO < 0)
                    {
                        bInsert = true;
                    }
                    continue;
                }
                else
                {
                    Conditions theConditionsJoin = new Conditions();
                    
                    theConditionsJoin.ColumnID = (_strContext == "field" || _strContext == "readonly") ? (int?)_iColumnID : null;
                    theConditionsJoin.DocumentSectionID = _strContext == "dashboard" ? (int?)_iDocumentSectionID : null;
                    theConditionsJoin.TableTabID = _strContext == "tabletab" ? (int?)_iTableTabID : null;
                    theConditionsJoin.Context = _strContext;

                    theConditionsJoin.ConditionColumnID = null;
                    theConditionsJoin.ConditionValue = "";
                    theConditionsJoin.ConditionOperator = "";
                    theConditionsJoin.DisplayOrder = iDO;
                    theConditionsJoin.JoinOperator = drSW["JoinOperator"].ToString();
                    theConditionsJoin.SpecialNotificationID = _strContext == "specialnotification" ? (int?)_iSpecialNotificationID : null;

                    theConditionsJoin.ConditionsID = RecordManager.dbg_Conditions_Insert(theConditionsJoin);

                    strActiveConditionsIDs = strActiveConditionsIDs + "," + theConditionsJoin.ConditionsID.ToString();
                    iDO = iDO + 1;

                    Conditions theConditions = new Conditions();
                   
                    theConditions.ColumnID = (_strContext == "field" || _strContext == "readonly") ? (int?)_iColumnID : null;
                    theConditions.DocumentSectionID = _strContext == "dashboard" ? (int?)_iDocumentSectionID : null;
                    theConditions.TableTabID = _strContext == "tabletab" ? (int?)_iTableTabID : null;
                    theConditions.Context = _strContext;

                    theConditions.ConditionColumnID = int.Parse(drSW["ConditionColumnID"].ToString());
                    theConditions.ConditionValue = drSW["ConditionValue"].ToString();
                    theConditions.ConditionOperator = drSW["ConditionOperator"].ToString();
                    theConditions.DisplayOrder = iDO;
                    theConditions.JoinOperator = "";
                    theConditions.SpecialNotificationID = _strContext == "specialnotification" ? (int?)_iSpecialNotificationID : null;

                    theConditions.ConditionsID = RecordManager.dbg_Conditions_Insert(theConditions);

                    strActiveConditionsIDs = strActiveConditionsIDs + "," + theConditions.ConditionsID.ToString();
                    iDO = iDO + 1;
                }
            }

            if(bInsert==false)
            {
                if (_strContext == "dashboard")
                {
                    Common.ExecuteText(@"DELETE Conditions WHERE DocumentSectionID=" + _iDocumentSectionID.ToString() 
                        + @" AND ConditionsID NOT IN (" + strActiveConditionsIDs + @")");

                }
                else if (_strContext == "tabletab")
                {
                    Common.ExecuteText(@"DELETE Conditions WHERE TableTabID=" + _iTableTabID.ToString()
                        + @" AND ConditionsID NOT IN (" + strActiveConditionsIDs + @")");

                }
                else if (_strContext == "specialnotification")
                {
                    Common.ExecuteText(@"DELETE Conditions WHERE SpecialNotificationID=" + _iSpecialNotificationID.ToString()
                        + @" AND ConditionsID NOT IN (" + strActiveConditionsIDs + @")");

                }
                else
                {
                    Common.ExecuteText(@"DELETE Conditions WHERE ColumnID=" + _iColumnID.ToString() 
                        + @" AND ConditionsID NOT IN (" + strActiveConditionsIDs + @") AND Context='"+_strContext+"'");

                }
            }
        }

        if ( ViewState["dtConditions"] != null &&
            (((_strContext == "field" || _strContext == "readonly") && _iColumnID == -1) || (_strContext == "dashboard" && _iDocumentSectionID == -1)
            || (_strContext == "specialnotification" && _iSpecialNotificationID == -1)
            || (_strContext == "tabletab" && _iTableTabID == -1)))
        {
            if(_strContext == "readonly")
            {
                Session["dtConditionsRO"] = (DataTable)ViewState["dtConditions"];
            }
            else
            {
                Session["dtConditions"] = (DataTable)ViewState["dtConditions"];
            }
            
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Save Action", "GetBackValueEdit();", true);
                

    }


  


}
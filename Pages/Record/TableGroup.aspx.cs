﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Linq;

public partial class Record_Menu : SecurePage
{

    Common_Pager _gvPager;
    int _iSearchCriteriaID = -1;
    int _iStartIndex = 0;
    int _iMaxRows = 10;
    string _strGridViewSortColumn = "Menu";
    string _strGridViewSortDirection = "ASC";

    string _strActionMode = "";
    int? _iMenuID;
    Menu _theMenu;

    //KG 16-2-2017 Ticket 2222 
    string _strFilesLocation = "";
    string _strFilesPhisicalPath = "";

    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }


    protected void PopulateDcouments()
    {
        try
        {
            /* == Red 04092019: Ticket 4818 == */
            //ddlReports.DataSource = Common.DataTableFromText("SELECT DocumentID,DocumentText FROM Document WHERE AccountID=" + Session["AccountID"].ToString() + " AND (ForDashBoard=0 or ForDashBoard IS NULL) ORDER BY DocumentText");
            //ddlReports.DataBind();
            //System.Web.UI.WebControls.ListItem liSelect = new System.Web.UI.WebControls.ListItem("-Please Select-", "");
            //ddlReports.Items.Insert(0, liSelect);
            /* == End Red == */

            string strReports = 
                        "SELECT 'docu' + '#' + CAST(DocumentID as varchar) as ReportID, DocumentText as ReportName " +
                           "FROM [Document] " +
                            "WHERE ReportType = 'ssrs' AND " +
                            "AccountID = " + Session["AccountID"].ToString() +
                        " UNION ALL " +
                        "SELECT 'repo' + '#' + CAST(ReportID as varchar), ReportName  " +
                            "FROM Report  " +
                            "WHERE AccountID = " + Session["AccountID"].ToString() +
                          " ORDER BY ReportName";

            DataTable dtCustomReport = Common.DataTableFromText(strReports);

            ddlReports.Items.Clear();
            ddlReports.DataSource = dtCustomReport;
            ddlReports.DataBind();
            ListItem liS = new ListItem("--Please Select--", "");
            ddlReports.Items.Insert(0, liS);

        }
        catch
        {
            //
        }

    }
    protected void PopulateDcoumentType()
    {
        try
        {

            ddlDocumentType.DataSource = Common.DataTableFromText("SELECT DocumentTypeID,DocumentTypeName FROM DocumentType WHERE AccountID=" + Session["AccountID"].ToString());
            ddlDocumentType.DataBind();
            System.Web.UI.WebControls.ListItem liSelect = new System.Web.UI.WebControls.ListItem("-Please Select-", "");
            ddlDocumentType.Items.Insert(0, liSelect);

        }
        catch
        {
            //
        }

    }

    protected void PopulateTableDDL()
    {
        int iTN = 0;
        ddlTable.DataSource = RecordManager.ets_Table_Select(null,
                null,
                null,
                int.Parse(Session["AccountID"].ToString()),
                null, null, true,
                "st.TableName", "ASC",
                null, null, ref iTN, Session["STs"].ToString());

        ddlTable.DataBind();
        System.Web.UI.WebControls.ListItem liSelect = new System.Web.UI.WebControls.ListItem("--Please Select--", "");
        ddlTable.Items.Insert(0, liSelect);
    }
    protected void ddlMenuType_SelectedIndexChanged(object sender, EventArgs e)
    {
        trOpenInNewWindow.Visible = false;

        stgShowUnder.InnerText = "Show Under*:";
        trMenu.Visible = false;
        trExternalPageLink.Visible = false;
        trTable.Visible = false;
        trReport.Visible = false;
        trDocumentType.Visible = false;
        trFileUpload.Visible = false; /*KG 14-2-2017 Ticket 2222*/
        trOverwriteFile.Visible = false; /*KG 2-8-2017 Ticket 2974*/

        rfvShowUnder.Enabled = false;

        if (ddlMenuType.SelectedValue != "d")
        {
            if (txtMenu.Text == Common.MenuDividerText)
            {
                txtMenu.Text = "";
            }

        }

        if (ddlMenuType.SelectedValue == "d")
        {
            rfvShowUnder.Enabled = true;

        }
        else if (ddlMenuType.SelectedValue == "l")
        {
            trExternalPageLink.Visible = true;
            txtExternalPageLink.ReadOnly = false;
            trMenu.Visible = true;
            trOpenInNewWindow.Visible = true;

        }
        else if (ddlMenuType.SelectedValue == "t")
        {
            trMenu.Visible = true;
            trTable.Visible = true;
        }
        else if (ddlMenuType.SelectedValue == "r")
        {

            trMenu.Visible = true;
            trReport.Visible = true;
        }
        else if (ddlMenuType.SelectedValue == "doc")
        {

            trMenu.Visible = true;
            trDocumentType.Visible = true;
        }
        else if (ddlMenuType.SelectedValue == "p")
        {
            //KG 14-2-2017 Ticket 2222
            trFileUpload.Visible = true;
            trMenu.Visible = true;
            trOpenInNewWindow.Visible = true;
            txtExternalPageLink.ReadOnly = true;

            //KG 2-8-2017 Ticket 2974
            if (_strActionMode.ToLower() == "edit")
            {
                trOverwriteFile.Visible = true;
                chkOverwriteFile.Checked = true;
            }

            //KG 19/6/2017 Ticket 2648 
            ScriptManager.RegisterStartupScript(this, this.GetType(), "RefreshUpload", "RefreshUpload();", true);
        }
        else
        {
            trMenu.Visible = true;
        }

        //ScriptManager.RegisterStartupScript(this, this.GetType(), "RefreshUpload", "RefreshUpload();", true);
    }

    protected void PopulateMenuDDL(ref DropDownList ddlShowUnder)
    {


        TheDatabaseS.PopulateMenuDDL(ref ddlShowUnder);

        ListItem liTop = new ListItem("-- Top Level --", "");
        ddlShowUnder.Items.Insert(0, liTop);
    }



    protected void PopulateTheRecord()
    {

        Menu theMenu = RecordManager.ets_Menu_Details((int)_iMenuID);

        if (theMenu.ParentMenuID != null)
        {
            if (ddlShowUnder.Items.FindByValue(theMenu.ParentMenuID.ToString()) != null)
                ddlShowUnder.SelectedValue = theMenu.ParentMenuID.ToString();

            hfParentMenuID.Value = theMenu.ParentMenuID.ToString();
        }

        txtMenu.Text = theMenu.MenuP;
        chkShowOnMenu.Checked = (bool)theMenu.ShowOnMenu;
        txtExternalPageLink.Text = theMenu.ExternalPageLink;

        if (theMenu.IsAdvancedSecurity != null)
            chkAdvancedSecurity.Checked = (bool)theMenu.IsAdvancedSecurity;
        else
            chkAdvancedSecurity.Checked = false;

        if (theMenu.OpenInNewWindow != null)
            chkOpenInNewWindow.Checked = (bool)theMenu.OpenInNewWindow;



        if (theMenu.MenuType == "r")
        {
           

        }


        if (theMenu.TableID != null)
        {
            ddlMenuType.SelectedValue = "t";
            ddlMenuType.Enabled = false;

            if (ddlTable.Items.FindByValue(theMenu.TableID.ToString()) != null)
                ddlTable.SelectedValue = theMenu.TableID.ToString();

        }
        else if (theMenu.MenuType == "r")
        {
            ddlMenuType.SelectedValue = "r";

            if (theMenu.ReportID != null)
            {
                if (ddlReports.Items.FindByValue("repo#" + theMenu.ReportID.ToString()) != null)
                    ddlReports.SelectedValue = "repo#" + theMenu.ReportID.ToString();
            }
            else if (theMenu.DocumentID != null)
            {
                if (ddlReports.Items.FindByValue("docu#" + theMenu.DocumentID.ToString()) != null)
                    ddlReports.SelectedValue = "docu#" + theMenu.DocumentID.ToString();
            }
        }
        else if (theMenu.DocumentTypeID != null)
        {
            ddlMenuType.SelectedValue = "doc";

            if (ddlDocumentType.Items.FindByValue(theMenu.DocumentTypeID.ToString()) != null)
                ddlDocumentType.SelectedValue = theMenu.DocumentTypeID.ToString();

        }
       
        else if (theMenu.MenuType == "doc")
        {
            ddlMenuType.SelectedValue = "doc";
            ddlDocumentType.SelectedValue = "";

        }
        else if (theMenu.MenuP == Common.MenuDividerText)
        {
            ddlMenuType.SelectedValue = "d";
        }
        else if (!string.IsNullOrEmpty(theMenu.ExternalPageLink))
        {
            //KG 14-2-2017 Ticket 2222
            if (theMenu.MenuType == "pdf")
            {
                ddlMenuType.SelectedValue = "p";
                lblFileName.Visible = true;
                lblFileName.Text = theMenu.ExternalPageLink.Replace(Session["FilesLocation"].ToString() + "\\UserFiles\\Documents\\", "");
            }
            else
                ddlMenuType.SelectedValue = "l";
        }
        else
        {
            ddlMenuType.SelectedValue = "m";
        }


        ddlMenuType_SelectedIndexChanged(null, null);

        if (_strActionMode == "edit")
        {
            ViewState["theMenu"] = theMenu;

            if (theMenu.IsActive == true)
            {
                divUnDelete.Visible = false;
            }
            else
            {
                divDelete.Visible = false;
            }

            if (theMenu.MenuP.ToLower() == "--none--")
            {
                EnableTheRecordControls(false);
                lnkDelete.Visible = false;
                lnkUnDelete.Visible = false;
                lnkSave.Visible = false;

            }
        }



    }


    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        try
        {
            RecordManager.ets_Menu_Delete((int)_iMenuID);
            Response.Redirect(hlBack.NavigateUrl, false);
        }
        catch (Exception ex)
        {
            if (ex is SqlException)
            {
                lblMsg.Text = "Delete failed! Please try again.";
            }
            else
            {

                lblMsg.Text = ex.Message;
            }
        }


    }

    protected void lnkUnDelete_Click(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        try
        {
            RecordManager.ets_Menu_UnDelete((int)_iMenuID);
            Response.Redirect(hlBack.NavigateUrl, false);
        }
        catch (Exception ex)
        {
            if (ex is SqlException)
            {
                if (ex.Message.IndexOf("UQ_Menu_Name_Parent_AccountID") > -1)
                {
                    lblMsg.Text = "A Menu '" + txtMenu.Text.Trim() + "' already exists.";
                }
                else
                {
                    lblMsg.Text = "Restore failed! Please try again.";
                }
            }
            else
            {

                lblMsg.Text = ex.Message;
            }
        }


    }


    protected void EnableTheRecordControls(bool p_bEnable)
    {
        txtMenu.Enabled = p_bEnable;
        chkShowOnMenu.Enabled = p_bEnable;
        ddlMenuType.Enabled = p_bEnable;
        //txtCustomPageLink.Enabled = p_bEnable;
        txtExternalPageLink.Enabled = p_bEnable;
        //chkIsActive.Enabled = p_bEnable;
        chkOpenInNewWindow.Enabled = p_bEnable;
        ddlShowUnder.Enabled = p_bEnable;
        //ddlAccount.Enabled = p_bEnable;       

    }

    /* == Red 24062019: Menu, Ticket 4689 == */   
    protected void PopulateRole()
    {
        ddlSelectedRoles.Items.Clear();

        string strExtraWhere = "";

        DataTable roleList = null;
        roleList = Common.DataTableFromText("SELECT RoleID, Role FROM [Role] WHERE (IsActive IS NULL OR IsActive=1) AND AccountID="
            + Session["AccountID"].ToString() + strExtraWhere + "  ORDER BY [Role]");

        foreach(DataRow dtRole in roleList.Rows)
        {
            ddlSelectedRoles.Items.Add(new ListItem(dtRole["Role"].ToString(), dtRole["RoleID"].ToString()));
        }

        DataTable dtMenuRole = SecurityManager.Menu_Role_Details(_iMenuID);
        if (dtMenuRole != null)
        {
            foreach (DataRow drMenuRole in dtMenuRole.Rows)
            {
                IEnumerable<ListItem> rolesIEnum = ddlSelectedRoles.Items.Cast<ListItem>().Where(item => item.Value.ToLower().Contains(drMenuRole["RoleID"].ToString()));
                if (rolesIEnum.Any())
                {
                    ListItem rolesItem = ddlSelectedRoles.Items.Cast<ListItem>().First(item => item.Value.ToLower().Contains(drMenuRole["RoleID"].ToString()));
                    rolesItem.Selected = true;
                }
            }
        }

        /* == Red 24062019: Menu, lets put the selected in the first row == */
        if (ddlSelectedRoles.SelectedItem != null)
        {
            CheckBoxList tempCBL = new CheckBoxList();

            foreach (ListItem li in ddlSelectedRoles.Items)
            {
                tempCBL.Items.Add(li);
            }

            ddlSelectedRoles.Items.Clear();

            foreach (ListItem li in tempCBL.Items)
            {
                if (li.Selected)
                {
                    if (ddlSelectedRoles.Items.FindByValue(li.Value) == null)
                        ddlSelectedRoles.Items.Add(new ListItem(li.Text, li.Value));
                }
            }
            foreach (ListItem li in ddlSelectedRoles.Items)
            {
                li.Selected = true;
            }
            foreach (ListItem li in tempCBL.Items)
            {
                if (li.Selected == false)
                {
                    if (ddlSelectedRoles.Items.FindByValue(li.Value) == null)
                        ddlSelectedRoles.Items.Add(new ListItem(li.Text, li.Value));
                }
            }

        }
        /* == End setting up == */


    }


    protected void saveSelectedRole(int? menuID)
    {
        /* == Red 24062019: Menu, Ticket 4689 == */
        int selectedCount = 0;
        foreach (ListItem li in ddlSelectedRoles.Items)
        {
            if (li.Selected)
            {
                selectedCount++;
            }
        }
        /* == End Red == */
        /* lets count the existing menuRole */
        string originalSelected = Common.GetValueFromSQL("SELECT COUNT(*) FROM MenuRole WHERE MenuID = " + menuID.ToString());

        /* lets delete the existing: if previously has selected then edited it none selected
         * or removed or added Roles
         * or  none at all */

        if (!chkAdvancedSecurity.Checked || selectedCount == 0 || (int.Parse(originalSelected) != selectedCount && int.Parse(originalSelected) > 0))
        {
            SecurityManager.Menu_Role_Delete(menuID);
        }

        if (chkAdvancedSecurity.Checked && selectedCount > 0)
        {
            foreach (ListItem roleId in ddlSelectedRoles.Items)
            {
                if (roleId.Selected)
                {
                    int checkddsd = roleId.Value.Length;
                    Menu_Role newMenuRole = new Menu_Role(int.Parse(roleId.Value.ToString()), menuID);
                    int iMenuRoleID = SecurityManager.Menu_Role_Insert(newMenuRole);
                }
            }

        }
    }
    /* == End Red == */

    protected bool IsUserInputOK()
    {
        //this is the final server side vaidation before database action
        //KG 7/8/17 Ticket 2974

        bool ret = true;
        if (ddlMenuType.SelectedValue == "p")
        {
            if (_strActionMode.ToLower() == "add" || (_strActionMode.ToLower() == "edit" && chkOverwriteFile.Checked == true))
            {
                if (fuReadOnly.HasFile == false && hfFileName.Value == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Problem", "alert('Please select a file.');", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "RefreshUpload", "RefreshUpload();", true);
                    ret = false;
                }
            }
        }

        return ret;
    }

    protected void lnkSave_Click(object sender, EventArgs e)
    {
        if (IsUserInputOK())
        {
            try
            {
                switch (_strActionMode.ToLower())
                {
                    case "add":

                        Menu newMenu = new Menu(null, txtMenu.Text,
                            int.Parse(Session["AccountID"].ToString()), chkShowOnMenu.Checked, true);


                        newMenu.ParentMenuID = ddlShowUnder.SelectedValue == "" ? null : (int?)int.Parse(ddlShowUnder.SelectedValue);
                        newMenu.OpenInNewWindow = chkOpenInNewWindow.Checked;
                        newMenu.MenuType = ddlMenuType.SelectedValue;
                        /* == Red 24062019: Menu, Ticket 4689 == */
                        newMenu.IsAdvancedSecurity = chkAdvancedSecurity.Checked;
                        /* == End Red == */

                        if (ddlMenuType.SelectedValue == "d")
                        {
                            newMenu.MenuP = Common.MenuDividerText;
                            newMenu.OpenInNewWindow = false;
                        }
                        else if (ddlMenuType.SelectedValue == "t")
                        {
                            newMenu.TableID = int.Parse(ddlTable.SelectedValue);

                        }
                        else if (ddlMenuType.SelectedValue == "doc")
                        {

                            if (ddlDocumentType.SelectedValue == "")
                            {
                                newMenu.MenuType = "doc";
                            }
                            else
                            {
                                newMenu.DocumentTypeID = int.Parse(ddlDocumentType.SelectedValue);
                            }

                        }
                        else if (ddlMenuType.SelectedValue == "r")
                        {
                            //newMenu.DocumentID = int.Parse(ddlReports.SelectedValue);
                            /* == Red 04092019: Check if it's a document or report ==*/
                            string[] values = ddlReports.SelectedValue.Split('#');
                            if (values[0].ToString() == "docu")
                            {
                                newMenu.DocumentID = int.Parse(values[1]);
                            }
                            else
                            {
                                newMenu.ReportID = int.Parse(values[1]);
                            }
                            /* == End Red == */

                        }
                        else if (ddlMenuType.SelectedValue == "l")
                        {
                            newMenu.ExternalPageLink = string.IsNullOrEmpty(txtExternalPageLink.Text) ? "" : txtExternalPageLink.Text;
                        }
                        //KG 14-2-2017 Ticket 2222 
                        else if (ddlMenuType.SelectedValue == "p")
                        {
                            newMenu.MenuType = "pdf";
                            newMenu.OpenInNewWindow = chkOpenInNewWindow.Checked;
                            newMenu.ExternalPageLink = SavePDF();
                        }
                        else
                        {
                            //menu
                        }


                        //if (newMenu.ParentMenuID == null && newMenu.TableID != null)
                        //{
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ProblemParentMenuID", "alert('This is a table menu, this menu can not be Top Level menu.');", true);
                        //    return;
                        //}


                        int iNewMenuID = RecordManager.ets_Menu_Insert(newMenu);

                        if (ddlMenuType.SelectedValue == "t")
                        {
                            Table theTable = RecordManager.ets_Table_Details(int.Parse(ddlTable.SelectedValue));
                            if (theTable != null) // && newMenu.ParentMenuID != null
                            {
                                //remove any other link this table with anyy menu
                                Common.ExecuteText("UPDATE Menu SET TableID=NULL WHERE MenuID<>" + iNewMenuID.ToString() + " AND TableID=" + newMenu.TableID.ToString());
                            }
                        }
                        if (ddlMenuType.SelectedValue == "r" && newMenu.DocumentID != null)
                        {
                            ////remove any other link this document with any menu
                            Common.ExecuteText("UPDATE Menu SET DocumentID=NULL WHERE MenuID<>" + iNewMenuID.ToString() + " AND DocumentID=" + newMenu.DocumentID.ToString());

                        }

                        /* == Red 24062019: Lets save the selected Role against the menu == */
                        saveSelectedRole(iNewMenuID);
                        /* == End Red == */                    

                        break;

                    case "view":


                        break;

                    case "edit":
                        Menu editMenu = (Menu)ViewState["theMenu"];
                        /* == Red 24062019: Menu, Ticket 4689 == */
                        editMenu.IsAdvancedSecurity = chkAdvancedSecurity.Checked;
                        /* ==  End Red ==*/

                        editMenu.MenuP = txtMenu.Text;
                        editMenu.ShowOnMenu = chkShowOnMenu.Checked;
                        //editMenu.AccountID = int.Parse(Session["AccountID"].ToString());

                        editMenu.ParentMenuID = ddlShowUnder.SelectedValue == "" ? null : (int?)int.Parse(ddlShowUnder.SelectedValue);
                      
                       


                        editMenu.DocumentTypeID = null;
                        editMenu.TableID = null;
                        editMenu.DocumentID = null;
                        editMenu.ExternalPageLink = "";
                        editMenu.OpenInNewWindow = false;
                        editMenu.MenuType = ""; // why we blank this? found in SVN version 268: Red 04092019
                        editMenu.MenuType = ddlMenuType.SelectedValue;
                        if (ddlMenuType.SelectedValue == "d")
                        {
                            editMenu.MenuP = Common.MenuDividerText;
                        }
                        else if (ddlMenuType.SelectedValue == "t")
                        {
                            editMenu.TableID = int.Parse(ddlTable.SelectedValue);
                        }
                        else if (ddlMenuType.SelectedValue == "doc")
                        {
                            if (ddlDocumentType.SelectedValue == "")
                            {
                                editMenu.MenuType = "doc";
                            }
                            else
                            {
                                editMenu.DocumentTypeID = int.Parse(ddlDocumentType.SelectedValue);
                            }

                        }
                        else if (ddlMenuType.SelectedValue == "r")
                        {
                            /* == Red 04092019: Check if it's a document or report ==*/
                            string[] values = ddlReports.SelectedValue.Split('#');
                            if (values[0].ToString() == "docu")
                            {
                                editMenu.DocumentID = int.Parse(values[1]);
                            }
                            else
                            {
                                editMenu.ReportID = int.Parse(values[1]); 
                            }
                            /* == End Red == */
                           

                        }
                        else if (ddlMenuType.SelectedValue == "d")
                        {
                            editMenu.MenuP = Common.MenuDividerText;
                        }
                        else if (ddlMenuType.SelectedValue == "l")
                        {
                            editMenu.ExternalPageLink = string.IsNullOrEmpty(txtExternalPageLink.Text) ? "" : txtExternalPageLink.Text;
                            editMenu.OpenInNewWindow = chkOpenInNewWindow.Checked;
                        }
                        //KG 14-2-2017 Ticket 2222 
                        else if (ddlMenuType.SelectedValue == "p")
                        {
                            editMenu.MenuType = "pdf";
                            editMenu.OpenInNewWindow = chkOpenInNewWindow.Checked;

                            editMenu.ExternalPageLink = SavePDF();
                        }
                        else
                        {
                            //
                        }

                        //if (editMenu.ParentMenuID == null && editMenu.TableID != null)
                        //{
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ProblemParentMenuID", "alert('This is a table menu, this menu can not be Top Level menu.');", true);
                        //    return;
                        //}

                       

                        int iIsUpdated = RecordManager.ets_Menu_Update(editMenu);

                        /* == Red 24062019: Menu, Ticket 4689 ==  */
                        saveSelectedRole(editMenu.MenuID);
                        /* == End Red == */

                        if (ddlMenuType.SelectedValue == "t")
                        {
                            Table theTable = RecordManager.ets_Table_Details(int.Parse(ddlTable.SelectedValue));
                            if (theTable != null) //&& editMenu.ParentMenuID != null
                            {
                                //remove any other link this table with anyy menu
                                Common.ExecuteText("UPDATE Menu SET TableID=NULL WHERE MenuID<>" + editMenu.MenuID.ToString() + " AND TableID=" + editMenu.TableID.ToString());
                            }
                        }

                        if (ddlMenuType.SelectedValue == "r") // what is this? Red
                        {
                            ////remove any other link this document with any menu
                            Common.ExecuteText("UPDATE Menu SET DocumentID=NULL WHERE MenuID<>" + editMenu.MenuID.ToString() + " AND DocumentID=" + editMenu.DocumentID.ToString());

                        }


                     

                        break;

                    default:
                        //?
                        break;
                }



                Response.Redirect(hlBack.NavigateUrl, false);
            }

            catch (Exception ex)
            {



                ErrorLog theErrorLog = new ErrorLog(null, "Table Group", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
                if (ex.Message.IndexOf("UQ_Menu_Name_Parent_AccountID") > -1)
                {
                    lblMsg.Text = "A Menu '" + txtMenu.Text + "' is already in same level, please try another Menu name.";
                    txtMenu.Focus();

                }
                else if (ex.Message.IndexOf("CHK_Menu_NotSelfParent") > -1)
                {
                    lblMsg.Text = "A Menu can not be under it's own , please try another Menu name.";
                    txtMenu.Focus();

                }
                else
                {
                    lblMsg.Text = ex.Message;
                }


            }

        }

    }


    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/TableGroup.aspx", false);
    }

    protected void btnOrderMT_Click(object sender, EventArgs e)
    {
        //
        if (hfOrderMT.Value != "")
        {

            try
            {
                string strNewMT = hfOrderMT.Value.Substring(0, hfOrderMT.Value.Length - 1);
                string[] newMT = strNewMT.Split(',');


                DataTable dtDO = Common.DataTableFromText("SELECT DisplayOrder,MenuID FROM [Menu] WHERE MenuID IN (" + strNewMT + ") ORDER BY DisplayOrder");
                if (newMT.Length == dtDO.Rows.Count)
                {
                    for (int i = 0; i < newMT.Length; i++)
                    {
                        if (dtDO.Rows[i][0].ToString() != "")
                        {
                            Common.ExecuteText("UPDATE [Menu] SET DisplayOrder =" + dtDO.Rows[i][0].ToString() + " WHERE MenuID=" + newMT[i]);
                        }

                    }
                }


            }
            catch (Exception ex)
            {

                //

            }

            Response.Redirect(Request.RawUrl, false);
            //Response.Redirect("~/Pages/Record/TableGroup.aspx?ParentMenuID=" + hfParentMenuID.Value, false);

            //lnkSearch_Click(null, null);
            //if (Request.QueryString["SearchCriteria"] != null)
            //{
            //    Response.Redirect("~/Pages/Record/TableGroup.aspx?SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString(), false);
            //}
            //else
            //{
            //    Response.Redirect("~/Pages/Record/TableGroup.aspx?ParentMenuID=" + hfParentMenuID.Value, false);
            //}
            //Home master = (Home)this.Master;


        }
    }



    protected void Page_Load(object sender, EventArgs e)
    {



        Title = "Menus";
        try
        {


            User ObjUser = (User)Session["User"];

            if (Request.QueryString["mode"] != null)
            {
                _strActionMode = Cryptography.Decrypt(Request.QueryString["mode"]);
            }

            if (Request.QueryString["MenuID"] != null)
            {
                _iMenuID = int.Parse(Cryptography.Decrypt(Request.QueryString["MenuID"].ToString()));
                _theMenu = RecordManager.ets_Menu_Details((int)_iMenuID);
            }

            //KG 16-2-2017 Ticket 2222 
            _strFilesLocation = Session["FilesLocation"].ToString();
            _strFilesPhisicalPath = Session["FilesPhisicalPath"].ToString();


            if (!IsPostBack)
            {
                PopulateTableDDL();
                PopulateDcouments();
                PopulateDcoumentType();
                if (!Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
                { Response.Redirect("~/Default.aspx", false); }

                if (Request.QueryString["ParentMenuID"] == null)
                {
                    hfParentMenuID.Value = "-1";
                }
                else
                {
                    hfParentMenuID.Value = Request.QueryString["ParentMenuID"].ToString();
                }
                if (Session["Menu_chkIsActive"] != null)
                {
                    chkIsActive.Checked = (bool)Session["Menu_chkIsActive"];
                }

                PopulateMenuDDL(ref ddlShowUnder);

                if (Request.QueryString["default"] != null)
                {
                    if (ddlShowUnder.Items.FindByValue(Request.QueryString["default"].ToString()) != null)
                        ddlShowUnder.SelectedValue = Request.QueryString["default"].ToString();

                    hfParentMenuID.Value = Request.QueryString["default"].ToString();
                }


                //if (Request.QueryString["SearchCriteria"] != null)
                //{
                //    PopulateSearchCriteria(int.Parse(Cryptography.Decrypt(Request.QueryString["SearchCriteria"].ToString())));
                //}



                MakeMenuPath();

                //if (Request.QueryString["SearchCriteria"] != null)
                //{
                //    gvTheGrid.PageSize = _iMaxRows;
                //    gvTheGrid.GridViewSortColumn = _strGridViewSortColumn;
                //    if (_strGridViewSortDirection.ToUpper() == "ASC")
                //    {
                //        gvTheGrid.GridViewSortDirection = SortDirection.Ascending;
                //    }
                //    else
                //    {
                //        gvTheGrid.GridViewSortDirection = SortDirection.Descending;
                //    }
                //    BindTheGrid(_iStartIndex, _iMaxRows);
                //}
                //else
                //{
                gvTheGrid.GridViewSortColumn = "Menu";
                gvTheGrid.GridViewSortDirection = SortDirection.Ascending;
                BindTheGrid(0, gvTheGrid.PageSize);
                //}                

                //KG 16-2-2017 Ticket 2222 
                hfRootURL.Value = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath;
                hfAccountID.Value = Session["AccountID"].ToString();
                ddlMenuType_SelectedIndexChanged(new object(), new EventArgs());
            }
            else
            {
            }


            GridViewRow gvr = gvTheGrid.TopPagerRow;
            if (gvr != null)
            {
                _gvPager = (Common_Pager)gvr.FindControl("Pager");
                _gvPager.AddURL = GetAddURL();
            }

            string strTitle = "Edit Menu";

            switch (_strActionMode.ToLower())
            {
                case "add":
                    divDelete.Visible = false;
                    divUnDelete.Visible = false;
                    strTitle = "Add Menu";
                    break;

                case "view":


                    strTitle = "View Menu";
                    break;

                case "edit":

                    strTitle = "Edit Menu";
                    if (!IsPostBack)
                        PopulateTheRecord();

                    break;


                default:
                    //?

                    break;
            }

            if (!IsPostBack)
            {


                if (hfParentMenuID.Value != "" && hfParentMenuID.Value != "-1")
                {
                    hlBack.NavigateUrl = GetEditURL() + Cryptography.Encrypt(hfParentMenuID.Value);
                }
                else
                {

                    hlBack.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/TableGroup.aspx";

                    if (_strActionMode == "" && (hfParentMenuID.Value == "" || hfParentMenuID.Value == "-1"))
                    {
                        trEditPart.Visible = false;
                    }

                }
            }



            Title = strTitle;
            lblTitle.Text = strTitle;


            /* == Red 24062019: Menu == */
            if (!IsPostBack)
            {
                PopulateRole();
            }
         
            /* == End Red == */

        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Menu Load", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }




    protected void PopulateSearchCriteria(int iSearchCriteriaID)
    {
        try
        {
            SearchCriteria theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);


            if (theSearchCriteria != null)
            {

                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

                xmlDoc.Load(new StringReader(theSearchCriteria.SearchText));

                //txtMenuSearch.Text = xmlDoc.FirstChild[txtMenuSearch.ID].InnerText;
                chkIsActive.Checked = bool.Parse(xmlDoc.FirstChild[chkIsActive.ID].InnerText);
                hfParentMenuID.Value = xmlDoc.FirstChild[hfParentMenuID.ID].InnerText;
                _iStartIndex = int.Parse(xmlDoc.FirstChild["iStartIndex"].InnerText);
                _iMaxRows = int.Parse(xmlDoc.FirstChild["iMaxRows"].InnerText);
                _strGridViewSortColumn = xmlDoc.FirstChild["GridViewSortColumn"].InnerText;
                _strGridViewSortDirection = xmlDoc.FirstChild["GridViewSortDirection"].InnerText;
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }


    }




    public string GetEditURL()
    {

        return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/TableGroup.aspx?mode=" + Cryptography.Encrypt("edit") + "&MenuID=";

    }

    public string GetAddURL()
    {
        if (hfParentMenuID.Value == "")
            hfParentMenuID.Value = "-1";

        string strparentForAdd = hfParentMenuID.Value;

        if (_iMenuID != null)
            strparentForAdd = _iMenuID.ToString();
        return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/TableGroup.aspx?mode=" + Cryptography.Encrypt("add") + "&default=" + strparentForAdd;

    }

    public string GetRoot()
    {

        return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/TableGroup.aspx";

    }
    //public string GetViewURL()
    //{

    //    return Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/TableGroup.aspx?SearchCriteria=" + Cryptography.Encrypt(_iSearchCriteriaID.ToString()) + "&ParentMenuID=";

    //}

    //public string GetAccountViewURL()
    //{

    //    return Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Security/AccountDetail.aspx?mode=" + Cryptography.Encrypt("view") + "&accountid=";

    //}


    protected void chkIsActive_CheckedChanged(object sender, EventArgs e)
    {

        Session["Menu_chkIsActive"] = chkIsActive.Checked;

        lnkSearch_Click(null, null);
    }

    protected void BindTheGrid(int iStartIndex, int iMaxRows)
    {

        //lblMsg.Text = "";

        //SearchCriteria 
        //try
        //{
        //    string xml = null;
        //    xml = @"<root>" +
        //           " <" + txtMenuSearch.ID + ">" + HttpUtility.HtmlEncode(txtMenuSearch.Text) + "</" + txtMenuSearch.ID + ">" +
        //           " <" + chkIsActive.ID + ">" + HttpUtility.HtmlEncode(chkIsActive.Checked.ToString()) + "</" + chkIsActive.ID + ">" +
        //           " <GridViewSortColumn>" + HttpUtility.HtmlEncode(gvTheGrid.GridViewSortColumn) + "</GridViewSortColumn>" +
        //           " <GridViewSortDirection>" + HttpUtility.HtmlEncode(gvTheGrid.GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC") + "</GridViewSortDirection>" +
        //           " <iStartIndex>" + HttpUtility.HtmlEncode(iStartIndex.ToString()) + "</iStartIndex>" +
        //           " <iMaxRows>" + HttpUtility.HtmlEncode(iMaxRows.ToString()) + "</iMaxRows>" +
        //            " <" + hfParentMenuID.ID + ">" + HttpUtility.HtmlEncode(hfParentMenuID.Value) + "</" + hfParentMenuID.ID + ">" +
        //          "</root>";

        //    SearchCriteria theSearchCriteria = new SearchCriteria(null, xml);
        //    _iSearchCriteriaID = SystemData.SearchCriteria_Insert(theSearchCriteria);
        //}
        //catch (Exception ex)
        //{
        //    lblMsg.Text = ex.Message;
        //}

        //End Searchcriteria




        try
        {
            int iTN = 0;
            if (hfParentMenuID.Value == "")
                hfParentMenuID.Value = "-1";

            int? iParentMenuID = int.Parse(hfParentMenuID.Value);

            if (_iMenuID != null)
                iParentMenuID = _iMenuID;
            ViewState[gvTheGrid.ID + "PageIndex"] = (iStartIndex / gvTheGrid.PageSize) + 1;
            gvTheGrid.DataSource = RecordManager.ets_Menu_Select(null,
                "",
                null, int.Parse(Session["AccountID"].ToString()), !chkIsActive.Checked,
                "M.DisplayOrder", "ASC",
                iStartIndex, iMaxRows, ref iTN, iParentMenuID, true);

            gvTheGrid.VirtualItemCount = iTN;
            gvTheGrid.DataBind();
            if (gvTheGrid.TopPagerRow != null)
                gvTheGrid.TopPagerRow.Visible = true;


            GridViewRow gvr = gvTheGrid.TopPagerRow;
            if (gvr != null)
            {
                _gvPager = (Common_Pager)gvr.FindControl("Pager");
                _gvPager.AddURL = GetAddURL();
                if (ViewState[gvTheGrid.ID + "PageIndex"] != null)
                    _gvPager.PageIndex = int.Parse(ViewState[gvTheGrid.ID + "PageIndex"].ToString());

                _gvPager.PageSize = gvTheGrid.PageSize;
                _gvPager.TotalRows = iTN;
                //_gvPager.PageIndexTextSet = (int)(iStartIndex / iMaxRows + 1);
            }

            if (iTN == 0)
            {
                if (IsFiltered())
                {
                    //divNoFilter.Visible = true;
                    divEmptyData.Visible = false;
                }
                else
                {
                    divEmptyData.Visible = true;
                    //divNoFilter.Visible = false;
                }
                hplNewData.NavigateUrl = GetAddURL();
                //hplNewDataFilter.NavigateUrl = GetAddURL();
                if (_strActionMode == "add")
                {
                    //divNoFilter.Visible = false;
                    divEmptyData.Visible = false;
                    chkIsActive.Visible = false;
                }
            }
            else
            {
                divEmptyData.Visible = false;
                //divNoFilter.Visible = false;
            }

            //if(_theMenu!=null && (_theMenu.TableID!=null || _theMenu.DocumentID!=null 
            //    || _theMenu.CustomPageLink!="" || _theMenu.ExternalPageLink!="" ))
            //{
            //    divEmptyData.Visible = false;
            //    divNoFilter.Visible = false;
            //    chkIsActive.Visible = false;
            //}

            lblMsg.Text = "";
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Group", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }

    protected void MakeMenuPath()
    {
        string strMenu = "";

        GetMenuPath(_iMenuID == null ? int.Parse(hfParentMenuID.Value) : (int)_iMenuID, ref strMenu);

        //strMenu = "<a href='javascript:SetMenu(-1)'>Top Level</a>/" + strMenu;

        strMenu = "<a href='" + GetRoot() + "'>Top Level</a>/" + strMenu;

        lblCurrentMenu.Text = strMenu;
    }
    protected void GetMenuPath(int iMenuID, ref string strMenu)
    {
        //Folder theFolder = DocumentManager.ets_Folder_Detail(iFolderID, null, null);
        Menu theMenu = RecordManager.ets_Menu_Details(iMenuID);
        if (theMenu != null)
        {
            //strMenu = "<a href='javascript:SetMenu(" + iMenuID.ToString() + ")'>" + theMenu.MenuP + "</a>/" + strMenu;

            strMenu = "<a href='" + GetEditURL() + Cryptography.Encrypt(iMenuID.ToString()) + "'>" + theMenu.MenuP + "</a>/" + strMenu;

            if (theMenu.ParentMenuID != null)
            {
                GetMenuPath((int)theMenu.ParentMenuID, ref strMenu);
            }
        }

    }

    protected void gvTheGrid_Sorting(object sender, GridViewSortEventArgs e)
    {

        BindTheGrid(0, gvTheGrid.PageSize);
    }


    protected void gvTheGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName == "delete")
        //{
        //    try
        //    {
        //        RecordManager.ets_Column_Delete(Convert.ToInt32(e.CommandArgument));

        //        BindTheGrid(_gvPager.StartIndex, gvTheGrid.PageSize);

        //        _gvPager._gridView.PageIndex = _gvPager.PageIndex - 1;
        //        if (_gvPager._gridView.Rows.Count == 0 && _gvPager._gridView.PageIndex > 0)
        //        {
        //            BindTheGrid(_gvPager.StartIndex - gvTheGrid.PageSize, gvTheGrid.PageSize);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog theErrorLog = new ErrorLog(null, "Table Group", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
        //        SystemData.ErrorLog_Insert(theErrorLog);
        //        lblMsg.Text = ex.Message;

        //        //ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "msg_delete", "alert('Delete failed!');", true);
        //    }
        //}
        //if (e.CommandName == "uporder")
        //{
        //    try
        //    {
        //        RecordManager.ets_Menu_OrderChange(Convert.ToInt32(e.CommandArgument), true);

        //        BindTheGrid(_gvPager.StartIndex, gvTheGrid.PageSize);

        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog theErrorLog = new ErrorLog(null, "Table Group", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
        //        SystemData.ErrorLog_Insert(theErrorLog);
        //        lblMsg.Text = ex.Message;

        //        //ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "msg_delete", "alert('Order failed!');", true);
        //    }
        //}
        //if (e.CommandName == "downorder")
        //{
        //    try
        //    {
        //        RecordManager.ets_Menu_OrderChange(Convert.ToInt32(e.CommandArgument), false);

        //        BindTheGrid(_gvPager.StartIndex, gvTheGrid.PageSize);


        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog theErrorLog = new ErrorLog(null, "Table Group", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
        //        SystemData.ErrorLog_Insert(theErrorLog);
        //        lblMsg.Text = ex.Message;

        //        //ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "msg_delete", "alert('Order failed!');", true);
        //    }
        //}

        //Response.Redirect(Request.RawUrl, false);
    }

    protected void gvTheGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
            e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");

            Label lblType = (Label)e.Row.FindControl("lblType");

            Label lblMenuP = (Label)e.Row.FindControl("lblMenuP");
            //HyperLink hlView = (HyperLink)e.Row.FindControl("hlView");

            if (DataBinder.Eval(e.Row.DataItem, "TableID") == DBNull.Value && DataBinder.Eval(e.Row.DataItem, "Menu").ToString() != Common.MenuDividerText)
            {
                lblType.Text = "Menu";


            }
            else
            {
                lblType.Text = "Table";

            }
            if (DataBinder.Eval(e.Row.DataItem, "DocumentID") != DBNull.Value)
            {
                lblType.Text = "Report";

            }
            /* == Red 04092019: Ticket 4818 == */
            if (DataBinder.Eval(e.Row.DataItem, "ReportID") != DBNull.Value)
            {
                lblType.Text = "Report";
            }
            /* == End Red == */

            if (DataBinder.Eval(e.Row.DataItem, "Menu").ToString() == Common.MenuDividerText)
            {
                lblType.Text = "Divider";
                lblMenuP.Text = "-- Divider --";

            }

            //if (DataBinder.Eval(e.Row.DataItem, "CustomPageLink")!=null && !string.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "CustomPageLink").ToString()))
            //{
            //    lblType.Text = "Custom";

            //}

            if (DataBinder.Eval(e.Row.DataItem, "ExternalPageLink") != DBNull.Value && !string.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "ExternalPageLink").ToString()))
            {
                if (DataBinder.Eval(e.Row.DataItem, "MenuType").ToString().ToLower() == "pdf")
                    lblType.Text = "PDF";
                else
                    lblType.Text = "Link";

            }

            if (DataBinder.Eval(e.Row.DataItem, "DocumentTypeID") != DBNull.Value
                || (DataBinder.Eval(e.Row.DataItem, "MenuType") != DBNull.Value && DataBinder.Eval(e.Row.DataItem, "MenuType").ToString() == "doc"))
            {
                lblType.Text = "Documents";

            }

        }



    }


    protected void gvTheGrid_PreRender(object sender, EventArgs e)
    {
        GridView grid = (GridView)sender;
        if (grid != null)
        {
            GridViewRow pagerRow = (GridViewRow)grid.TopPagerRow;
            if (pagerRow != null)
            {
                pagerRow.Visible = true;
            }
        }
    }


    //protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    protected void lnkSearch_Click(object sender, EventArgs e)
    {

        BindTheGrid(0, gvTheGrid.PageSize);
        MakeMenuPath();
    }
    protected void Pager_BindTheGridToExport(object sender, EventArgs e)
    {
        //string strTable = "Table";
        //strTable = SecurityManager.etsTerminology("", "Table", "Table");

        _gvPager.ExportFileName = SecurityManager.etsTerminology(Request.Path.Substring(Request.Path.LastIndexOf("/") + 1), "Table", "Table") + " Groups";
        BindTheGrid(0, _gvPager.TotalRows);
    }

    protected void Pager_BindTheGridAgain(object sender, EventArgs e)
    {
        BindTheGrid(_gvPager.StartIndex, _gvPager.PageSize);
    }

    protected void Pager_OnApplyFilter(object sender, EventArgs e)
    {

        txtMenuSearch.Text = "";
        chkIsActive.Checked = false;
        gvTheGrid.GridViewSortColumn = "Menu";
        gvTheGrid.GridViewSortDirection = SortDirection.Ascending;
        lnkSearch_Click(null, null);
        //BindTheGrid(0, gvTheGrid.PageSize);
    }


    protected void Pager_DeleteAction(object sender, EventArgs e)
    {
        string sCheck = "";
        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
        {
            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
            if (ischeck)
            {
                sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
            }
        }
        if (string.IsNullOrEmpty(sCheck))
        {
            ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select a record.');", true);
        }
        else
        {
            DeleteItem(sCheck);
            Response.Redirect(Request.RawUrl, false);
            //BindTheGrid(_gvPager.StartIndex, gvTheGrid.PageSize);
            //_gvPager._gridView.PageIndex = _gvPager.PageIndex - 1;
            //if (_gvPager._gridView.Rows.Count == 0 && _gvPager._gridView.PageIndex > 0)
            //{
            //    BindTheGrid(_gvPager.StartIndex - gvTheGrid.PageSize, gvTheGrid.PageSize);
            //}
        }

    }



    private void DeleteItem(string keys)
    {
        string strID = "";
        lblMsg.Text = "";
        try
        {
            if (!string.IsNullOrEmpty(keys))
            {

                foreach (string sTemp in keys.Split(','))
                {
                    if (!string.IsNullOrEmpty(sTemp))
                    {
                        strID = sTemp;
                        RecordManager.ets_Menu_Delete(int.Parse(sTemp));

                    }
                }


            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Record Group Delete", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            Menu oMenu = RecordManager.ets_Menu_Details(int.Parse(strID));

            if (ex.Message.IndexOf("FK_Table_Menu") == -1)
            {
                lblMsg.Text = ex.Message;
            }
            else
            {
                string strTable = "Table";
                strTable = SecurityManager.etsTerminology(Request.Path.Substring(Request.Path.LastIndexOf("/") + 1), "Table", "Table");
                lblMsg.Text = "Delete failed! " + strTable + " Group -" + oMenu.MenuP + "- has " + strTable + "s, please remove those " + strTable + "s then try again to delete.";

            }
        }
    }



    protected void Pager_OnExportForCSV(object sender, EventArgs e)
    {

        gvTheGrid.AllowPaging = false;
        BindTheGrid(0, _gvPager.TotalRows);



        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition",
        "attachment;filename=Menus.csv");
        Response.Charset = "";
        Response.ContentType = "text/csv";

        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);


        int iColCount = gvTheGrid.Columns.Count;
        for (int i = 0; i < iColCount; i++)
        {
            if (string.IsNullOrEmpty(gvTheGrid.Columns[i].HeaderText))
            {
            }
            else
            {
                sw.Write(gvTheGrid.Columns[i].HeaderText);
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
        }

        sw.Write(sw.NewLine);

        // Now write all the rows.
        foreach (GridViewRow dr in gvTheGrid.Rows)
        {

            for (int i = 0; i < iColCount; i++)
            {
                if (string.IsNullOrEmpty(gvTheGrid.Columns[i].HeaderText))
                {
                }
                else
                {
                    switch (i)
                    {
                        case 3:
                            HyperLink hlView = (HyperLink)dr.FindControl("hlView");
                            sw.Write("\"" + hlView.Text + "\"");
                            break;


                        default:
                            if (!Convert.IsDBNull(dr.Cells[i]))
                            {
                                sw.Write("\"" + dr.Cells[i].Text + "\"");
                            }

                            break;
                    }

                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
            }
            sw.Write(sw.NewLine);
        }
        sw.Close();


        Response.Output.Write(sw.ToString());
        Response.Flush();
        Response.End();
    }

    protected bool IsFiltered()
    {
        if (txtMenuSearch.Text != ""
            || chkIsActive.Checked != false)
        {
            return true;
        }

        return false;
    }

    //KG 16-2-2017 Ticket 2222 
    protected string SavePDF()
    {
        string strFileName = "";
        string strUniqueName = "";
        string strFolder = "\\UserFiles\\Documents";
        string strPath = "";

        if (fuReadOnly.HasFile)
        {
            strFileName = fuReadOnly.FileName;

            //KG 2-8-2017 Ticket 2974
            //strUniqueName = Session["AccountID"].ToString() + "_" + Guid.NewGuid().ToString() + "_" + strFileName;
            if (chkOverwriteFile.Checked == true && _strActionMode == "edit" && lblFileName.Text != "")
                strUniqueName = lblFileName.Text;
            else
                strUniqueName = Session["AccountID"].ToString() + "_" + Guid.NewGuid().ToString() + "_" + strFileName;

            strPath = _strFilesPhisicalPath + strFolder + "\\" + strUniqueName;

            fuReadOnly.SaveAs(strPath);
        }
        else
        {
            if (chkOverwriteFile.Checked == false && _strActionMode == "edit")
                strUniqueName = lblFileName.Text;
            else
            {
                if (hfFileName.Value != null)
                    strUniqueName = hfFileName.Value.Substring(0, hfFileName.Value.IndexOf(','));
                else
                    strUniqueName = lblFileName.Text;
            }
        }

        strPath = _strFilesLocation + strFolder + "\\" + strUniqueName;
        txtExternalPageLink.Text = strPath;

        return strPath;
    }

}

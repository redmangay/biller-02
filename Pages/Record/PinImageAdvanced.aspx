﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true" CodeFile="PinImageAdvanced.aspx.cs" Inherits="Pages_Record_PinImageAdvanced" %>

<%@ Register Src="~/Pages/UserControl/ControlByColumn.ascx" TagName="ControlByColumn" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <script language="javascript" type="text/javascript">

        function GetBack() {
            parent.$.fancybox.close();
        }
    </script>

    <div style="text-align: center;">
        <div style="display: inline-block;">
            <div style="padding: 20px;">

                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td colspan="3">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" style="width: 200px;">
                                        <span class="TopTitle">
                                            <asp:Label runat="server" ID="lblTitle" Text="Advanced Map Pin"></asp:Label></span>
                                    </td>
                                    <td align="left">

                                        <table>
                                            <tr>

                                                <td>
                                                    <asp:LinkButton runat="server" ID="lnkBack" OnClientClick="javascript:parent.$.fancybox.close(); return false;"
                                                        CssClass="btn" CausesValidation="false"> <strong>Cancel</strong></asp:LinkButton>
                                                    <%--<asp:Button runat="server" ID="btnPopulateAdd" ClientIDMode="Static"  style="display:none;"/>--%>
                                                </td>
                                                <td>
                                                    <asp:LinkButton runat="server" ID="lnkSave" CssClass="btn" OnClick="lnkSave_Click"
                                                        CausesValidation="true"> <strong>Save</strong></asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="13"></td>
                    </tr>
                    <tr>
                        <td valign="top"></td>
                        <td valign="top">
                            <asp:UpdatePanel ID="upPages" runat="server">
                                <ContentTemplate>
                                    <div id="search" style="padding-bottom: 10px">
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                                            ShowMessageBox="false" ShowSummary="false" HeaderText="Please correct the following errors:" />
                                    </div>
                                    <asp:Panel ID="Panel2" runat="server">

                                        <div runat="server" id="divDetail">
                                            <table cellpadding="3">
                                                <tr>
                                                    <td colspan="5" style="padding-left: 67px">
                                                        <table>
                                                            <tr>
                                                                <td align="right">
                                                                    <strong>Controlling Field</strong>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:DropDownList runat="server" ID="ddlControllingField" CssClass="NormalTextBox"
                                                                        AutoPostBack="true" DataTextField="DisplayName" DataValueField="ColumnID" OnSelectedIndexChanged="ddlControllingField_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>

                                                        <div style="padding-top: 10px; max-width: 600px;">
                                                            <asp:GridView ID="grdMapPin" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                                                                CssClass="gridview" OnRowCommand="grdMapPin_RowCommand" OnRowDataBound="grdMapPin_RowDataBound"
                                                                ShowHeaderWhenEmpty="true"
                                                                ShowFooter="true">
                                                                <Columns>
                                                                    <asp:TemplateField Visible="false">
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:Label runat="server" ID="lblID" Text='<%# Eval("ID") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="imgbtnMinus" runat="server" ImageUrl="~/App_Themes/Default/Images/Minus.png"
                                                                                CommandName="minus" CommandArgument='<%# Eval("ID") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                        <HeaderTemplate>
                                                                            <strong>Value</strong>
                                                                        </HeaderTemplate>

                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <ItemTemplate>
                                                                            <asp:ControlByColumn runat="server" ID="cbcValue" ShowColumnDDL="false" CustomCompareOperator="Traffic" />
                                                                            <%-- CustomCompareOperator << use this property when you want to modify the drop down option of ddlCompareOperator.
                                                                                                             This was added to comply to a fix where ddlCompareOperator for traffic needed to only show equals.
                                                                                                             By JV --%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                        <HeaderTemplate>
                                                                            <strong>Image</strong>
                                                                        </HeaderTemplate>

                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                        <ItemTemplate>

                                                                            <asp:DropDownList ID="ddlPinImages" runat="server" ClientIDMode="Static" CssClass="NormalTextBox">
                                                                                <asp:ListItem Selected="True" Text="--Please Select--" Value="Pages/Record/PINImages/DefaultPin.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Pin Blue" Value="Pages/Record/PINImages/PinBlue.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Pin Black" Value="Pages/Record/PINImages/PinBlack.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Pin Gray" Value="Pages/Record/PINImages/PinGray.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Pin Green" Value="Pages/Record/PINImages/PinGreen.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Pin Light Blue" Value="Pages/Record/PINImages/PinLBlue.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Pin Orange" Value="Pages/Record/PINImages/PinOrange.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Pin Purple" Value="Pages/Record/PINImages/PinPurple.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Pin Red" Value="Pages/Record/PINImages/PinRed.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Pin Yellow" Value="Pages/Record/PINImages/PinYellow.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Round Blue" Value="Pages/Record/PINImages/RoundBlue.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Round Gray" Value="Pages/Record/PINImages/RoundGray.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Round Green" Value="Pages/Record/PINImages/RoundGreen.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Round Light Blue" Value="Pages/Record/PINImages/RoundLightBlue.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Round Orange" Value="Pages/Record/PINImages/RoundOrange.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Round Purple" Value="Pages/Record/PINImages/RoundPurple.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Round Red" Value="Pages/Record/PINImages/RoundRed.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Round White" Value="Pages/Record/PINImages/RoundWhite.png"></asp:ListItem>
                                                                                <asp:ListItem Text="Round Yellow" Value="Pages/Record/PINImages/RoundYellow.png"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField>
                                                                        <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="imgbtnPlus" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png"
                                                                                CommandName="plus" Visible="true" CommandArgument='<%# Eval("ID") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                                <HeaderStyle CssClass="gridview_header" Height="25px" />
                                                                <RowStyle CssClass="gridview_row_NoPadding" />
                                                            </asp:GridView>

                                                            <div>
                                                                <asp:Label ID="lblMsgTab" runat="server" ForeColor="Red"></asp:Label>
                                                            </div>
                                                        </div>


                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <br />
                                        <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>

                                        <br />

                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3" height="13"></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>




</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Record_RecordDisplay : SecurePage
{
    private Pages_UserControl_DetailEdit oneCTDetail;

    protected void Page_Init(object sender, EventArgs e)
    {
        oneCTDetail = (Pages_UserControl_DetailEdit)LoadControl("~/Pages/UserControl/DetailEdit.ascx");
        //oneCTDetail.TableChildID = int.Parse(dr["TableChildID"].ToString());
        oneCTDetail.ContentPage = "record";
        oneCTDetail.TableID = int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
        oneCTDetail.ID = "ctRecordDetail";
        oneCTDetail.ShowAddButton = false;
        oneCTDetail.ShowEditButton = false;
        oneCTDetail.Mode = "view";
        oneCTDetail.OnlyOneRecord = true;
        oneCTDetail.RecordID =int.Parse( Cryptography.Decrypt(Request.QueryString["RecordID"].ToString()));
        pnlRecord.Controls.Add(oneCTDetail);
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
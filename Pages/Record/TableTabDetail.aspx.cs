﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class Pages_Record_TableTabDetail : SecurePage
{

    string _strActionMode = "view";
    int? _iTableTabID;
    string _qsMode = "";
    string _qsTableTabID = "";
    string _qsTableID = "";
   
    protected void Page_Load(object sender, EventArgs e)
    {
        //int iTemp = 0;

       

        //int iTableTabID = int.Parse(Cryptography.Decrypt(Request.QueryString["TableTabID"].ToString()));

        _qsTableID = Cryptography.Decrypt(Request.QueryString["TableID"].ToString());

        if (Request.QueryString["TableTabID"] != null)
        {

            _qsTableTabID = Cryptography.Decrypt(Request.QueryString["TableTabID"]);

            _iTableTabID = int.Parse(_qsTableTabID);
            hfTableTabID.Value = _qsTableTabID;
        }
        if (!IsPostBack)
        {
            Session["dtConditions"] = null;
            Session["dtColumnColour"] = null;
            string strConditionsID = "";
            string strColumnColourID = "";
            if(_qsTableTabID=="")
            {
                hlConditions.NavigateUrl = "~/Pages/Record/ShowHide.aspx?TableID="+_qsTableID+"&Context=tabletab";
            }
            else
            {
                hlConditions.NavigateUrl = "~/Pages/Help/ConditionOptions.aspx?TableID=" + _qsTableID + "&Context=tabletab&tabletabid=" + _qsTableTabID;
                strConditionsID = Common.GetValueFromSQL("SELECT TOP 1 ConditionsID FROM Conditions WHERE tabletabid=" + _qsTableTabID);

                strColumnColourID = Common.GetValueFromSQL("SELECT TOP 1 ColumnColourID FROM ColumnColour WHERE Context='tabletabid' AND ID=" + _qsTableTabID);
            }

            hlColumnColour.NavigateUrl = "~/Pages/Record/ColumnColourList.aspx?Context=tabletabid&ID=" + (_qsTableTabID == "" ? "-1" : _qsTableTabID) + "&TableID=" + _qsTableID;


            if (strConditionsID != "")
                chkConditions.Checked = true;

            if (strColumnColourID != "")
                chkColumnColour.Checked = true;
        }
           
        if (Request.QueryString["mode"] == null)
        {
            Server.Transfer("~/Default.aspx");
        }
        else
        {
            _qsMode = Cryptography.Decrypt(Request.QueryString["mode"]);

            if (_qsMode == "add" ||
                _qsMode == "view" ||
                _qsMode == "edit")
            {
                _strActionMode = _qsMode;                            
            }
            else
            {
                Server.Transfer("~/Default.aspx");
            }
        }     


        // checking permission

        string strTitle = "Add Page";
        switch (_strActionMode.ToLower())
        {
            case "add":


                break;

            case "view":


                strTitle = "View Page";
                
                PopulateTheRecord();         

                EnableTheRecordControls(false);
                divSave.Visible = false;

                break;

            case "edit":
                strTitle = "Edit Page";
                if (!IsPostBack)
                {
                    PopulateTheRecord();
                }
                break;


            default:
                //?

                break;
        }

        Title =strTitle;
        lblTitle.Text = strTitle;



        string showWhenFancy = @" 
             $(function () {
                $("".showWhenLinkTableTab"").fancybox({ 
                beforeLoad: function(){ 
                        if($('#"+ hfTableTabID.ClientID + @"').val() == '')
                          {
                                alert('Please save the page first.');  $.fancybox.close();
                            } 
                } ,
                afterClose: function(){ 
                    $.ajax({
                    url: '../../GetLocations.aspx?Context=tabletab&ID=" + _qsTableTabID + @"',
                        dataType: 'json',
                        success: function(res) {
                        var chkConditions = document.getElementById('chkConditions');
                        if (res > 0)
                        { 
                            chkConditions.checked= true;
                            }
                            else
                            {
                                chkConditions.checked= false;
                                }
                            }
                        });
                    },
                    iframe:
                    {
                        css:
                        {
                            width: 1400,
                            height: 700
                        }
                    },       
                toolbar: false,
                smallBtn: true, 
                type: 'iframe',
                titleShow: false,

            });
            });

            ";


        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "showWhenFancyTableTab", showWhenFancy, true);

    }

   

    protected void PopulateTheRecord()
    {
        try
        {
            //int iTemp = 0;
            //List<TableTab> listTableTab = SystemData.TableTab_Select(_iTableTabID, "", "", "", null, null, "TableTabID", "ASC", null, null, ref iTemp);

            TableTab theTableTab = RecordManager.ets_TableTab_Detail((int)_iTableTabID);

           
            
            txtTabName.Text = theTableTab.TabName;

            if (_strActionMode == "edit")
            {
                ViewState["theTableTab"] = theTableTab;
            }
          
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Tab Detail", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }

    }
    

    protected void EnableTheRecordControls(bool p_bEnable)
    {        
       
        txtTabName.Enabled = p_bEnable;
       

    }

    protected bool IsUserInputOK()
    {
        //this is the final server side vaidation before database action


        return true;
    }



    //protected void cmdSave_Click(object sender, ImageClickEventArgs e)
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (IsUserInputOK())
            {

                switch (_strActionMode.ToLower())
                {
                    case "add":

                        //Menu newMenu = new Menu(null, txtMenu.Text, int.Parse(ddlAccount.SelectedValue), chkShowOnMenu.Checked, "");
                        //SecurityManager.test_TestTable_Insert(newMenu);

                        string strDisplayOrder = Common.GetValueFromSQL(@"SELECT MAX(DisplayOrder) FROM 
                        TableTab WHERE TableID=" + _qsTableID);

                        int iDisplayOrder = 0;
                        if (strDisplayOrder != "")
                            iDisplayOrder = int.Parse(strDisplayOrder) + 1;

                        TableTab newTableTab = new TableTab(null, int.Parse(_qsTableID), txtTabName.Text,
                            iDisplayOrder);


                        int iTableTabID=RecordManager.dbg_TableTab_Insert(newTableTab);

                        if (chkConditions.Checked && Session["dtConditions"] != null)
                        {
                            //insert new show when
                            DataTable dtConditions = (DataTable)Session["dtConditions"];
                            int iDO = 1;
                            foreach (DataRow drSW in dtConditions.Rows)
                            {
                                if (iDO == 1)
                                {
                                    if (drSW["ConditionColumnID"].ToString() == "" || drSW["ConditionValue"].ToString() == "")
                                    {
                                        continue;
                                    }
                                    Conditions theConditions1 = new Conditions();
                                    theConditions1.TableTabID = iTableTabID;
                                    theConditions1.Context = "tabletab";
                                    theConditions1.ConditionColumnID = int.Parse(drSW["ConditionColumnID"].ToString());
                                    theConditions1.ConditionValue = drSW["ConditionValue"].ToString();
                                    theConditions1.ConditionOperator = drSW["ConditionOperator"].ToString();
                                    theConditions1.DisplayOrder = 1;
                                    theConditions1.JoinOperator = "";
                                    theConditions1.ConditionsID = RecordManager.dbg_Conditions_Insert(theConditions1);

                                    iDO = iDO + 1;
                                    continue;
                                }
                                else
                                {
                                    if (drSW["ConditionColumnID"].ToString() == "" || drSW["ConditionValue"].ToString() == "" || drSW["JoinOperator"].ToString() == "")
                                    {
                                        continue;
                                    }


                                    Conditions theConditionsJoin = new Conditions();
                                    theConditionsJoin.TableTabID = iTableTabID;
                                    theConditionsJoin.Context = "tabletab";
                                    theConditionsJoin.ConditionColumnID = null;
                                    theConditionsJoin.ConditionValue = "";
                                    theConditionsJoin.ConditionOperator = "";
                                    theConditionsJoin.DisplayOrder = iDO;
                                    theConditionsJoin.JoinOperator = drSW["JoinOperator"].ToString();

                                    theConditionsJoin.ConditionsID = RecordManager.dbg_Conditions_Insert(theConditionsJoin);
                                    iDO = iDO + 1;

                                    Conditions theConditions = new Conditions();
                                    theConditions.TableTabID = iTableTabID;
                                    theConditions.Context = "tabletab";
                                    theConditions.ConditionColumnID = int.Parse(drSW["ConditionColumnID"].ToString());
                                    theConditions.ConditionValue = drSW["ConditionValue"].ToString();
                                    theConditions.ConditionOperator = drSW["ConditionOperator"].ToString();
                                    theConditions.DisplayOrder = iDO;
                                    theConditions.JoinOperator = "";

                                    theConditions.ConditionsID = RecordManager.dbg_Conditions_Insert(theConditions);
                                    iDO = iDO + 1;

                                }
                            }

                        }

                        if (chkColumnColour.Checked && Session["dtColumnColour"] != null)
                        {

                            DataTable dtColumnColour = (DataTable)Session["dtColumnColour"];
                            foreach (DataRow drCC in dtColumnColour.Rows)
                            {

                                if (drCC["ControllingColumnID"].ToString() == "" || drCC["Operator"].ToString() == "" || drCC["Value"].ToString() == "" || drCC["ColourText"].ToString() == "" || drCC["ColourCell"].ToString() == "")
                                {
                                    continue;
                                }

                                ColumnColour theColumnColour = new ColumnColour();
                                theColumnColour.Context = "tabletabid";
                                theColumnColour.ID = iTableTabID;
                                theColumnColour.ControllingColumnID = int.Parse(drCC["ControllingColumnID"].ToString());
                                theColumnColour.Value = drCC["Value"].ToString();
                                theColumnColour.Operator = drCC["Operator"].ToString();
                                theColumnColour.ColourText = drCC["ColourText"].ToString(); //red 3802
                                theColumnColour.ColourCell = drCC["ColourCell"].ToString(); //red 3802

                                Cosmetic.dbg_ColumnColour_Insert(theColumnColour);


                            }

                        }

                        break;

                    case "view":


                        break;

                    case "edit":
                        TableTab editTableTab = (TableTab)ViewState["theTableTab"];

                        editTableTab.TabName = txtTabName.Text;

                        RecordManager.dbg_TableTab_Update(editTableTab);


                        break;

                    default:
                        //?
                        break;
                }


               

            }
            else
            {
                //user input is not ok

            }
            //Response.Redirect(hlBack.NavigateUrl, false);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "RefreshGrid", "CloseAndRefresh();", true);
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Form set form Detail", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }

     

    }
   
}

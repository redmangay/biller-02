﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true"
    CodeFile="TableDetail.aspx.cs" Inherits="Pages_Record_TableDetail" EnableEventValidation="false" %>

<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>
<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%--<%@ Register Src="~/Pages/UserControl/ControlByColumnValue.ascx" TagName="cbcValue" TagPrefix="dbg" %>--%>
<%--<%@ Register Src="~/Pages/UserControl/ControlByColumn.ascx" TagName="cbcValue" TagPrefix="dbg" %>--%>

<%@ Register Src="~/Pages/UserControl/ControlByColumn.ascx" TagName="ControlByColumn" TagPrefix="dbg" %>

<%@ Register Src="~/Pages/UserControl/ViewDetail.ascx" TagName="OneView" TagPrefix="dbg" %>
<%@ Register Src="~/Pages/UserControl/ExportTemplate.ascx" TagName="ExpTem" TagPrefix="dbg" %>
<%@ Register Src="~/Pages/UserControl/ImportTemplate.ascx" TagName="ImpTem" TagPrefix="dbg" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

    

    <!-- JA 09 FEB 2017 -->
    <style type="text/css">
        .scroller-container {
            max-width: 100%;
        }

        .ajax__tab_body {
            padding-right: 15px;
        }

        .gridview_header_freeze {
            height: 46px;
            background-color: #EDF3F7;
            color: #000000;
            position: relative; /*top: expression(this.parentNode.parentNode.parentNode.scrollTop-1);*/
            z-index: 10;
        }

        .sortHandle {
            cursor: move;
        }

        .sortHandle2 {
            cursor: move;
        }


        .sortHandle3 {
            cursor: move;
        }



        .cssplaceholder {
            border-top: 2px solid #00FFFF;
            border-bottom: 2px solid #00FFFF;
        }

        .ddlist {
            width: 400px;
        }

        .space {
            width: 365px;
        }
    </style>
    <script type="text/javascript">
        //$('.mCustomScrollbar').style.width = 900;
        //red 3090_2
        function toggleAndOr(t, hf) {
            // alert(hf);

            if (t.text == "and") {
                t.text = "or";
            } else {
                t.text = "and";
            }
            document.getElementById(hf).value = t.text;

        }
        //red end

        function SelectAllCheckboxes(spanChk) {
            checkAll(spanChk);
            var GridView = spanChk.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && spanChk != inputList[i]) {
                    if (spanChk.checked) {
                        inputList[i].checked = true;
                    }
                    else {
                        inputList[i].checked = false;
                    }
                }

            }
        }

        function MouseEvents(objRef, evt) {
            if (evt.type == "mouseover") {
                objRef.style.backgroundColor = "#76BAF2";
                objRef.style.cursor = 'pointer';
            }
            else {

                if (evt.type == "mouseout") {
                    if (objRef.rowIndex % 2 == 0) {
                        //Alternating Row Color
                        objRef.style.backgroundColor = "white";
                    }
                    else {
                        objRef.style.backgroundColor = "#DCF2F0";
                    }
                }
            }
        }

        //        function ShowHistory() {
        //            $("#divHistory").show();
        //            $("#lnkShowHistory").hide();
        //            $("#lnkHideHistory").show();

        //        }

        //        function HideHistory() {
        //            $("#divHistory").hide();
        //            $("#lnkShowHistory").show();
        //            $("#lnkHideHistory").hide();

        //        }




        /* == Red: Fancy box == */
        $(document).ready(function () {
            $("#lnkAddUser").on('click', function () {
                $(".ajax-indicator-full").hide();
            });

            $("#ddlUserChoices").on('change', function () {
                document.getElementById('trErrorNoRecipient').style.display = 'none';
            });

            $("#ctl00_HomeContentPlaceHolder_TabContainer2_TabTable_ddlUser").on('change', function () {
                document.getElementById('trErrorNoRecipient').style.display = 'none';
            });

            $("#ddlNotificationEmailFromRecord").on('change', function () {
                document.getElementById('trErrorNoRecipient').style.display = 'none';
            });

            $("#ddlNotificationPhoneFromRecord").on('change', function () {
                document.getElementById('trErrorNoRecipient').style.display = 'none';
            });
            
        });

        function pageLoad(sender, args) {
            $("#lnkAddUser").on('click', function () {

                /* Specific user */
                var ddlUserChoice = $("#ddlUserChoices").val()
                var recipientOption = $("#hfRecipientOption").val(ddlUserChoice);

                if (ddlUserChoice == "") {
                    document.getElementById('trErrorNoRecipient').textContent = 'Please select a Recipient.';
                    document.getElementById('trErrorNoRecipient').style.display = 'inherit';
                    return false;
                }

                if (ddlUserChoice == "SU") {
                    if ($("#ctl00_HomeContentPlaceHolder_TabContainer2_TabTable_ddlUser").val() == "-1") {
                        document.getElementById('trErrorNoRecipient').textContent = 'Please select a Specific User.';
                        document.getElementById('trErrorNoRecipient').style.display = 'inherit';
                        return false;
                    }
                }

                if (ddlUserChoice == "EOCR") {
                    if ($("#ddlNotificationEmailFromRecord").val() == "") {
                        document.getElementById('trErrorNoRecipient').textContent = 'Please select Email on current record.';
                        document.getElementById('trErrorNoRecipient').style.display = 'inherit';
                        return false;
                    }
                }

                if (ddlUserChoice == "POCR") {
                    if ($("#ddlNotificationPhoneFromRecord").val() == "") {
                        document.getElementById('trErrorNoRecipient').textContent = 'Please select Phone on current record.';
                        document.getElementById('trErrorNoRecipient').style.display = 'inherit';
                        return false;
                    }
                }

                if (ddlUserChoice == "SU")
                {
                    $("#hfSelectedUser").val($("#ctl00_HomeContentPlaceHolder_TabContainer2_TabTable_ddlUser").val());
                }
                else if (ddlUserChoice == "EOCR") {
                    $("#hfSelectedUser").val($("#ddlNotificationEmailFromRecord").val());
                }
                else if (ddlUserChoice == "POCR") {
                    $("#hfSelectedUser").val($("#ddlNotificationPhoneFromRecord").val());
                }
              
                //$.fancybox.close();
                document.getElementById("ctl00_HomeContentPlaceHolder_TabContainer2_TabTable_lnkSmallSave").click();
            });
        }
        //$(document).ready(function () {
        //    //$("#lnkAddRecipeint").on('click', function () {

        //    //    $.fancybox.open({
        //    //        src: '#hidden-content',
        //    //        type: 'inline',
        //    //        toolbar: false,
        //    //        smallBtn: true,

        //    //    });
        //    //});
        //$("#lnkAddUser").on('click', function () {
        //    $("#hfSelectedUser").val($("#ctl00_HomeContentPlaceHolder_TabContainer2_TabTable_ddlUser").val());
        //    $.fancybox.close();
        //    if (Page_IsValid) {
        //        $(".ajax-indicator-full").hide();
        //        document.getElementById("ctl00_HomeContentPlaceHolder_TabContainer2_TabTable_lnkSmallSave").click();
        //    }
        //});

        //});

        //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(applicationInitHandler);
        //function applicationInitHandler() {
        //    $("#lnkAddRecipeint").on('click', function () {

        //        $.fancybox.open({
        //            src: '#hidden-content',
        //            type: 'inline',
        //            toolbar: false,
        //            smallBtn: true,

        //        });
        //    });

        //    $("#lnkAddUser").on('click', function () {
        //        $("#hfSelectedUser").val($("#ctl00_HomeContentPlaceHolder_TabContainer2_TabTable_ddlUser").val());
        //        $.fancybox.close();
        //        if (Page_IsValid) {
        //            //$(".ajax-indicator-full").hide();
        //            //$("#lnkSmallSave").click();
        //            document.getElementById("ctl00_HomeContentPlaceHolder_TabContainer2_TabTable_lnkSmallSave").click();
        //        }


        //    });
        //}
       
    </script>

    <script type="text/javascript">
        function toggleAndOr(t, hf) {
            // alert(hf);

            if (t.text == "and") {
                t.text = "or";
            } else {
                t.text = "and";
            }
            document.getElementById(hf).value = t.text;
            //alert(document.getElementById(hf).value);

            //            if (t.innerHTML == "and") {
            //                t.innerHTML = "or";
            //            } else {
            //                t.innerHTML = "and";
            //            }

        }
    </script>


    <script language="javascript" type="text/javascript">

        function Submit() {

            document.forms["aspnetForm"].submit()
            //        alert('ok');

        }

        function ClientActiveTabChanged() {
            if ($find("ctl00_HomeContentPlaceHolder_TabContainer2").get_activeTabIndex() == 1) {

                ShowHide();
            }

            if ($find("ctl00_HomeContentPlaceHolder_TabContainer2").get_activeTabIndex() == 3) {

                ShowHide();
            }

        };

        var fixHelper = function (e, ui) {
            ui.children().each(function () {
                $(this).width($(this).width());
            });

            return ui;
        };

        function ShowHideCategory(sCatergory, lnk) {

            $('#hfCategory').val(sCatergory);
            ShowHide();
            if ($(".TablLinkClass") != null && lnk != null) {
                $(".TablLinkClass").css('font-weight', 'normal');
            }
            if (lnk != null) {
                lnk.style.fontWeight = 'bold';
            }
        }

        function ShowHideTemplateCategory(sTemplateCategory, lnk) {

            $('#hfTemplateCategory').val(sTemplateCategory);
            ShowHide();
            if ($(".TemplateTablLinkClass") != null && lnk != null) {
                $(".TemplateTablLinkClass").css('font-weight', 'normal');
            }
            if (lnk != null) {
                lnk.style.fontWeight = 'bold';
            }
        }

        function OpenTableRenameConfirm() {
            $('#hlTableRename').trigger('click');
            //alert($('#hlTableRename').toString());
        }



        function ShowHide() {

            //var sCatergory = $('#ddlCategory').val();

            //red Ticket 3358
            if ($("#ddlArchiveDataMonth").val() == "-1") {                
                $("#trddlArchiveColumnID").hide();
                $("#trddlArchiveDatabase").hide();
                $("#trlblLastArchiveRun").hide();
            }
            else {
                $("#trddlArchiveColumnID").show();
                $("#trddlArchiveDatabase").show();
                $("#trlblLastArchiveRun").show();
            }

            var sTemplateCategory = $('#hfTemplateCategory').val();

            if (sTemplateCategory == 'import') {
                $("#divTemplateImport").show();
                $("#divTemplateExport").hide();
                $("#divTemplateWordMerge").hide();
            }

            if (sTemplateCategory == 'export') {
                $("#divTemplateImport").hide();
                $("#divTemplateExport").show();
                $("#divTemplateWordMerge").hide();
            }

            if (sTemplateCategory == 'word') {
                $("#divTemplateImport").hide();
                $("#divTemplateExport").hide();
                $("#divTemplateWordMerge").show();
            }

            var sCatergory = $('#hfCategory').val();
            if (sCatergory == 'display') {
                $("#divDisplay").show();
                $("#divImportData").hide();
                $("#divAddRecords").hide();
                $("#divColours").hide();
                $("#divGraphs").hide();
                $("#divMaps").hide();
                $("#divEmails").hide();
                $("#divArchiveData").hide();
            }
            if (sCatergory == 'importdata') {
                $("#divDisplay").hide();
                $("#divImportData").show();
                $("#divAddRecords").hide();
                $("#divColours").hide();
                $("#divGraphs").hide();
                $("#divMaps").hide();
                $("#divEmails").hide();
                $("#divArchiveData").hide();
            }
            if (sCatergory == 'addrecords') {
                $("#divDisplay").hide();
                $("#divImportData").hide();
                $("#divAddRecords").show();
                $("#divColours").hide();
                $("#divGraphs").hide();
                $("#divMaps").hide();
                $("#divEmails").hide();
                $("#divArchiveData").hide();
            }
            if (sCatergory == 'colours') {
                $("#divDisplay").hide();
                $("#divImportData").hide();
                $("#divAddRecords").hide();
                $("#divColours").show();
                $("#divGraphs").hide();
                $("#divMaps").hide();
                $("#divEmails").hide();
                $("#divArchiveData").hide();
            }
            if (sCatergory == 'graphs') {
                $("#divDisplay").hide();
                $("#divImportData").hide();
                $("#divAddRecords").hide();
                $("#divColours").hide();
                $("#divGraphs").show();
                $("#divMaps").hide();
                $("#divEmails").hide();
                $("#divArchiveData").hide();
            }
            if (sCatergory == 'maps') {
                $("#divDisplay").hide();
                $("#divImportData").hide();
                $("#divAddRecords").hide();
                $("#divColours").hide();
                $("#divGraphs").hide();
                $("#divMaps").show();
                $("#divEmails").hide();
                $("#divArchiveData").hide();
            }
            if (sCatergory == 'emails') {
                $("#divDisplay").hide();
                $("#divImportData").hide();
                $("#divAddRecords").hide();
                $("#divColours").hide();
                $("#divGraphs").hide();
                $("#divMaps").hide();
                $("#divEmails").show();
                $("#divArchiveData").hide();
            }
            if (sCatergory == 'archive') {
                $("#divDisplay").hide();
                $("#divImportData").hide();
                $("#divAddRecords").hide();
                $("#divColours").hide();
                $("#divGraphs").hide();
                $("#divMaps").hide();
                $("#divEmails").hide();
                $("#divArchiveData").show();
            }

            var chkAddUserRecord = document.getElementById("chkAddUserRecord");
            var divAutomaticallyAddUserRecord = document.getElementById("divAutomaticallyAddUserRecord");
            if (chkAddUserRecord != null) {
                if (chkAddUserRecord.checked == true) {
                    $("#divAutomaticallyAddUserRecord").show();
                }
                else {
                    $("#divAutomaticallyAddUserRecord").hide();
                }
            }



            var chkUniqueRecordedate = document.getElementById("chkUniqueRecordedate");
            if (chkUniqueRecordedate != null) {
                if (chkUniqueRecordedate.checked == true) {
                    $("#divUniqueRecord").show();
                    if ($("[id$='ddlUniqueColumnID'] option:selected").index() == 0)
                        $("#trDuplicateRecordAction").hide();
                    else
                        $("#trDuplicateRecordAction").show();
                }
                else {
                    $("#divUniqueRecord").hide();
                    $("#trDuplicateRecordAction").hide();
                }
            }


            /*
            var chkDataUpdateUniqueColumnID = document.getElementById("chkDataUpdateUniqueColumnID");
            var divDataUpdateUniqueColumnID = document.getElementById("divDataUpdateUniqueColumnID");
            if (chkDataUpdateUniqueColumnID != null) {
                if (chkDataUpdateUniqueColumnID.checked == true) {
                    $("#divDataUpdateUniqueColumnID").show();
                }
                else {
                    $("#divDataUpdateUniqueColumnID").hide();
                }
            }
            */


            var strColumnValue = $('#ddlHeaderText').val();
            if (strColumnValue != '') {
                $('#hlDDEdit').hide();
            }
            else {
                $('#hlDDEdit').show();
            }

            //red 04092017
            var chkDeleteRecordsActionAdvanced = document.getElementById("chkDeleteRecordsActionAdvanced");
            if (chkDeleteRecordsActionAdvanced != null) {
                if (chkDeleteRecordsActionAdvanced.checked == true) {
                    $("#trColumnPreventDeletion").show();
                    $("#trchkCustomNotification").show();

                }
                else {
                    $("#chkCustomNotification").attr('checked', false)
                    $("#trColumnPreventDeletion").hide();
                    $("#trchkCustomNotification").hide();
                }
            }
            //red 04092017
            //red 04092017
            if ($("#ddlDeleteAction").val() == "Prevent deletion") {
                $("#trchkDeleteRecordsActionAdvanced").show();
            }

            else {
                $("#chkDeleteRecordsActionAdvanced").attr('checked', false)
                $("#chkCustomNotification").attr('checked', false)
                $("#trCustomizedNotification").hide();
                $("#trColumnPreventDeletion").hide();
                $("#trchkCustomNotification").hide();
                $("#trchkDeleteRecordsActionAdvanced").hide();


            }
            //red 04092017

            //red 04092017
            var chkCustomNotification = document.getElementById("chkCustomNotification");
            if (chkCustomNotification != null) {
                if (chkCustomNotification.checked == true) {
                    $("#trCustomizedNotification").show();
                }
                else {
                    $("#trCustomizedNotification").hide();
                }
            }
            //red 04092017

            //red 07092017



            //red 3090_2
            //if ($("#ddlFilterOperator").val() == "empty" || $("#ddlFilterOperator").val() == "notempty") {
            //    $("#txtColumnPrevent").hide();
            //    $("#chkIsDate").hide();
            //    $("#txtDatePrevent").hide();
            //    $("#ibDatePrevent").hide();
            //    $('label[for="chkIsDate"]').hide();
            //}
            //else {
            //    $("#chkIsDate").show();
            //    $('label[for="chkIsDate"]').show();
            //    var chkIsDate = document.getElementById("chkIsDate");
            //    if (chkIsDate != null) {
            //        if (chkIsDate.checked == true) {
            //            $("#txtDatePrevent").show();
            //            $("#ibDatePrevent").show();
            //            $("#txtColumnPrevent").hide();

            //        }
            //        else {
            //            $("#txtDatePrevent").hide();
            //            $("#ibDatePrevent").hide();
            //            $("#txtColumnPrevent").show();
            //        }
            //    }

            //}

            //red 3090_2
            //if ($("#ddlColumnPreventDeletion").val() == "") {
            //    $("#txtColumnPrevent").hide();
            //    $("#chkIsDate").hide();
            //    $("#txtDatePrevent").hide();
            //    $("#ibDatePrevent").hide();
            //    $('label[for="chkIsDate"]').hide();
            //    $("#ddlFilterOperator").hide();


            //} else {
            //    $("#ddlFilterOperator").show();
            //}

            //red end 07092017

        };

        if (window.addEventListener)
            window.addEventListener("load", ShowHide(), false);
        else if (window.attachEvent)
            window.attachEvent("onload", ShowHide());
        else if (document.getElementById)
            window.onload = ShowHide();

        //        $(document).ready(function () {
        //            gridviewScroll();
        //        });

        //        function gridviewScroll() {
        //            $('#ctl00_HomeContentPlaceHolder_TabContainer2_tabField_gvTheGrid').gridviewScroll({
        //                width: 660,
        //                height: 200,
        //                freezesize: 2
        //            });
        //        }





        $(document).ready(function () {


            if (document.getElementById('hlDDEdit') != null) {
                document.getElementById('hlDDEdit').href = '../Help/TableColumn.aspx?headername=yes&formula=' + encodeURIComponent(document.getElementById('hfDisplayColumnsFormula').value) + '&Tableid=' + document.getElementById('ctl00_HomeContentPlaceHolder_hfTableID').value;

            }


            $('#ddlHeaderText').change(function (e) {

                var strColumnValue = $('#ddlHeaderText').val();
                if (document.getElementById('hlDDEdit') != null) {
                    if (strColumnValue != '') {
                        $('#hlDDEdit').hide();
                        document.getElementById('hfDisplayColumnsFormula').value = '[' + $('#ddlHeaderText option:selected').text() + ']';
                    }
                    else {
                        $('#hlDDEdit').show();
                    }

                    document.getElementById('hlDDEdit').href = '../Help/TableColumn.aspx?headername=yes&formula=' + encodeURIComponent(document.getElementById('hfDisplayColumnsFormula').value) + '&Tableid=' + document.getElementById('ctl00_HomeContentPlaceHolder_hfTableID').value;

                }

            });


            $('#ddlHeaderText').change();



            //$('#ddlPinImages').change(function (e) {

            //    var strPinImages = $('#ddlPinImages').val();
            //    if (document.getElementById('hlAdvancedPinImage') != null) {
            //        if (strPinImages != '') {
            //            $('#hlAdvancedPinImage').hide();
            //            $('#imgPIN').show();
            //        }
            //        else {
            //            $('#hlAdvancedPinImage').show();
            //            $('#imgPIN').hide();
            //        }
            //    }

            //});

            //$('#ddlPinImages').change();

            $(function () {
                $("#ctl00_HomeContentPlaceHolder_TabContainer2_tabField_upGrid").sortable({
                    items: '.gridview_row',
                    cursor: 'crosshair',
                    helper: fixHelper,
                    cursorAt: { left: 10, top: 10 },
                    connectWith: '#ctl00_HomeContentPlaceHolder_TabContainer2_tabField_upGrid',
                    handle: '.sortHandle',
                    axis: 'y',
                    distance: 15,
                    dropOnEmpty: true,
                    receive: function (e, ui) {
                        $(this).find("tbody").append(ui.item);

                    },
                    start: function (e, ui) {
                        ui.placeholder.css("border-top", "2px solid #00FFFF");
                        ui.placeholder.css("border-bottom", "2px solid #00FFFF");

                    },
                    update: function (event, ui) {
                        var SC = '';
                        $(".ColumnID").each(function () {
                            SC = SC + this.value.toString() + ',';
                        });
                        //                        alert( SC);
                        document.getElementById("hfOrderSC").value = SC;
                        $("#btnOrderSC").trigger("click");


                        //                        $.ajax({
                        //                            url: 'OrderSC.aspx?newSC=' + SC,
                        //                            dataType: 'json'

                        //                        });

                    }
                });
            });

            $(function () {
                $("#ctl00_HomeContentPlaceHolder_TabContainer2_tabChildTTables_upTableChild").sortable({
                    items: '.gridview_row',
                    cursor: 'crosshair',
                    helper: fixHelper,
                    cursorAt: { left: 10, top: 10 },
                    connectWith: '#ctl00_HomeContentPlaceHolder_TabContainer2_tabChildTTables_upTableChild',
                    handle: '.sortHandle2',
                    axis: 'y',
                    distance: 15,
                    dropOnEmpty: true,
                    receive: function (e, ui) {
                        $(this).find("tbody").append(ui.item);

                    },
                    start: function (e, ui) {
                        ui.placeholder.css("border-top", "2px solid #00FFFF");
                        ui.placeholder.css("border-bottom", "2px solid #00FFFF");

                    },
                    update: function (event, ui) {
                        var TC = '';
                        $(".TableChildID").each(function () {
                            TC = TC + this.value.toString() + ',';
                        });

                        document.getElementById("hfOrderTC").value = TC;
                        $("#btnOrderTC").trigger("click");

                    }
                });
            });


            $(function () {
                $("#ctl00_HomeContentPlaceHolder_TabContainer2_tabForms_upForms").sortable({
                    items: '.gridview_row',
                    cursor: 'crosshair',
                    helper: fixHelper,
                    cursorAt: { left: 10, top: 10 },
                    connectWith: '#ctl00_HomeContentPlaceHolder_TabContainer2_tabForms_upForms',
                    handle: '.sortHandle3',
                    axis: 'y',
                    distance: 15,
                    dropOnEmpty: true,
                    receive: function (e, ui) {
                        $(this).find("tbody").append(ui.item);

                    },
                    start: function (e, ui) {
                        ui.placeholder.css("border-top", "2px solid #00FFFF");
                        ui.placeholder.css("border-bottom", "2px solid #00FFFF");

                    },
                    update: function (event, ui) {
                        var TC = '';
                        $(".FormSetID").each(function () {
                            TC = TC + this.value.toString() + ',';
                        });

                        document.getElementById("hfOrderFS").value = TC;
                        $("#btnOrderFS").trigger("click");

                    }
                });
            });

            //ValidatorEnable(document.getElementById('rfvNewMenuName'), false);

            //$('#ctl00_HomeContentPlaceHolder_TabContainer2_tabProperties_ddlMenu').change(function (e) {
            //    if (document.getElementById("ctl00_HomeContentPlaceHolder_TabContainer2_tabProperties_ddlMenu").value == 'new') {
            //        $("#trNewMenuName").show();
            //        var txtNewMenuName = document.getElementById("ctl00_HomeContentPlaceHolder_TabContainer2_tabProperties_txtNewMenuName");
            //        txtNewMenuName.value = '';
            //        $('#ctl00_HomeContentPlaceHolder_lblMsg').text('');
            //        ValidatorEnable(document.getElementById('rfvNewMenuName'), true);
            //    }
            //    else {
            //        $("#trNewMenuName").hide();
            //        var txtNewMenuName = document.getElementById("ctl00_HomeContentPlaceHolder_TabContainer2_tabProperties_txtNewMenuName");
            //        txtNewMenuName.value = '';
            //        $('#ctl00_HomeContentPlaceHolder_lblMsg').text('');
            //        ValidatorEnable(document.getElementById('rfvNewMenuName'), false);
            //    }
            //});


            $("#chkAddUserRecord").click(function () {
                ShowHide();
            });


            $("#chkUniqueRecordedate").click(function () {
                ShowHide();
            });

            //red 04092017
            $("#chkDeleteRecordsActionAdvanced").click(function () {
                ShowHide();
            });

            $("#chkCustomNotification").click(function () {
                ShowHide();
            });


            $("#ddlDeleteAction").click(function () {
                ShowHide();
            });

            //red Ticket 3358
            $("#ddlArchiveDataMonth").click(function () {
                ShowHide();
            });

            $("#ddlFilterOperator").click(function () {
                ShowHide();
            }); // red 07092017

            $("#ddlColumnPreventDeletion").click(function () {
                ShowHide();
            }); // red 07092017


            //red 3090_2
            //$("#chkIsDate").click(function () {
            //    ShowHide();
            //}); // red 07092017
            //red 04092017

            //$("#chkDataUpdateUniqueColumnID").click(function () {
            //    ShowHide();
            //});



            $("#chkSummaryPageContent").click(function () {
                var chkSummaryPageContent = document.getElementById("chkSummaryPageContent");
                if (chkSummaryPageContent.checked == true) {
                    $("#hlSummaryPageContent").trigger("click");
                }
            });

            //            $("#ddlCategory").change(function () {
            //                ShowHide();
            //            });

            $("[id$='ddlUniqueColumnID']").change(function () {
                if ($("[id$='ddlUniqueColumnID'] option:selected").index() == 0) {
                    $("#trDuplicateRecordAction").hide();
                    $("#ddlDuplicateRecordAction").attr('selectedIndex', 0);
                }
                else
                    $("#trDuplicateRecordAction").show();
            });

        });



    </script>
    <%--<asp:UpdateProgress class="ajax-indicator-full" ID="UpdateProgress3" runat="server">
        <ProgressTemplate>
            <table style="width:100%;  height:100%; text-align: center;" >
                <tr valign="middle">
                    <td>
                        <p style="font-weight:bold;"> Please wait...</p>
                        <asp:Image ID="Image5" runat="server" AlternateText="Processing..." ImageUrl="~/Images/ajax.gif" />
                    </td>
                </tr>
            </table>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <table border="0" cellpadding="0" cellspacing="0" align="center" onload="ShowHide();" style="max-width: 100%;">
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" style="width: 50%;">
                            <span class="TopTitle">
                                <asp:Label runat="server" ID="lblTitle"></asp:Label></span> <a name="topline" id="topline"></a>
                        </td>
                        <td align="left">
                            <div>
                                <%-- <asp:UpdateProgress class="ajax-indicator-Grey" ID="UpdateProgress3" runat="server">
                                    <ProgressTemplate>
                                        <table style="width:600px; height:600px; text-align: center">
                                            <tr>
                                                <td>
                                                    
                                                    <asp:Image runat="server" AlternateText="Processing..." ImageUrl="~/Images/ajax.gif" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>--%>
                            </div>
                        </td>
                        <td align="right">
                            <table>
                                <tr style="width: 100%">
                                    <td align="right"></td>
                                    <td align="right">
                                        <div>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div runat="server" id="divFinished" visible="false">
                                                            <asp:HyperLink runat="server" ID="hlFinished" Text="Back" CssClass="btn"><strong>Finished</strong></asp:HyperLink>
                                                        </div>
                                                        <div>
                                                            <table runat="server" id="tblButtons">
                                                                <tr>
                                                                    <td>
                                                                        <%--<div>
                                                                            <asp:HyperLink runat="server" ID="hlProperties" ClientIDMode="Static" Text="Properties"
                                                                                CssClass="btn"><strong>Properties</strong></asp:HyperLink>
                                                                        </div>--%>
                                                                    </td>
                                                                    <td>
                                                                         <div runat="server" id="divhlBack">
                                                                            <asp:HyperLink runat="server" ID="hlBack" Text="Back" CssClass="btn"><strong>Back</strong></asp:HyperLink>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div runat="server" id="divPermanentDelete">

                                                                            <asp:LinkButton runat="server" ID="lnkPermanetDelete" OnClientClick="javascript:return confirm('Are you sure you want to permanently delete this Table?')"
                                                                                CssClass="btn" OnClick="lnkPermanentDelete_Click"
                                                                                CausesValidation="false"> <strong>Permanent Delete</strong> </asp:LinkButton>

                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div runat="server" id="divEdit" visible="false">
                                                                            <asp:HyperLink runat="server" ID="hlEdit" CssClass="btn"> <strong>Edit</strong> </asp:HyperLink>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div runat="server" id="divDelete">
                                                                            <asp:LinkButton runat="server" ID="lnkDelete" CssClass="btn" CausesValidation="false"
                                                                                OnClick="lnkDelete_Click"> <strong>Delete</strong> </asp:LinkButton>
                                                                        </div>
                                                                        <div runat="server" id="divUnDelete">
                                                                            <asp:LinkButton runat="server" ID="lnkUnDelete" OnClientClick="javascript:return confirm('Are you sure you want to restore this Table?')"
                                                                                CssClass="btn" CausesValidation="false" OnClick="lnkUnDelete_Click"> <strong>Restore</strong>  </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div runat="server" id="divRecords">
                                                                            <asp:HyperLink runat="server" ID="hlRecords" CssClass="btn"> <strong>Records</strong></asp:HyperLink>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td>
                                        <asp:HyperLink runat="server" ID="hlHelpCommon" ClientIDMode="Static" NavigateUrl="~/Pages/Help/Help.aspx?contentkey=TableDetailHelp">
                                            <asp:Image ID="Image2" runat="server" ImageUrl="~/App_Themes/Default/images/help.png" />
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="13"></td>
        </tr>
        <tr>
            <td valign="top">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="Panel2" runat="server">
                            <div runat="server" id="div1">
                                <div runat="server" id="divDetail">
                                    <%--<asp:HiddenField runat="server" ID="hfMenuID" />--%>
                                    <asp:HiddenField runat="server" ID="hfTableID" />
                                    <asp:HiddenField runat="server" ID="hfOrderSC" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfOrderTC" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfOrderFS" ClientIDMode="Static" />
                                    <asp:HiddenField ID="tochkUpdateMenuName" runat="server" Value="" />
                                    <asp:HiddenField ID="tochkUpdateViewName" runat="server" Value="" />
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <ajaxToolkit:TabContainer ID="TabContainer2" runat="server" OnClientActiveTabChanged="ClientActiveTabChanged"
                    Width="1000px" CssClass="DBGTab" ActiveTabIndex="0">
                    <ajaxToolkit:TabPanel ID="tabField" runat="server">
                        <HeaderTemplate>
                            <strong runat="server" id="stgFieldsCap">Fields</strong>

                        </HeaderTemplate>




















                        <ContentTemplate>
                            <div style="padding-left: 20px; padding-top: 10px;">
                                <asp:UpdatePanel ID="upGrid" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:CheckBox runat="server" ID="chkShowSystemFields" TextAlign="Right" Font-Bold="true"
                                            Text="Show System Fields" AutoPostBack="true" OnCheckedChanged="chkShowSystemFields_OnCheckedChanged" />
                                        &nbsp;
                                        <asp:DropDownList runat="server" ID="ddlTableTabFilter" AutoPostBack="true" CssClass="NormalTextBox"
                                            OnSelectedIndexChanged="ddlTableTabs_SelectedIndexChanged" DataTextField="TabName" DataValueField="TableTabID">
                                        </asp:DropDownList>
                                        <p runat="server" id="pInstruction" visible="false">
                                            Click on the Add icon to add fields:
                                        </p>
                                        <asp:Label runat="server" ID="lblSaveFieldsLabel" Width="200px"></asp:Label>
                                        <asp:LinkButton runat="server" ID="lnkSaveFields" OnClick="lnkSaveFields_Click">
                                            <asp:Image runat="server" ID="Image3" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                ToolTip="Save" />
                                        </asp:LinkButton>
                                        &nbsp; &nbsp; &nbsp;
                                        <asp:HyperLink runat="server" ID="hlTableTabs" Style="text-decoration: none; color: Black; padding-bottom: 10px;"
                                            CssClass="popuplinktt"> <strong style="text-decoration: underline; color: Blue;">
                                               Configure Pages</strong>
                                        </asp:HyperLink>

                                        &nbsp; &nbsp; &nbsp;
                                        <asp:HyperLink runat="server" ID="hlColumnLayout" Style="text-decoration: none; "
                                          NavigateUrl="~/Pages/Record/ColumnLayout.aspx" > 
                                               Column Display Layout
                                        </asp:HyperLink>


                                        <dbg:dbgGridView ID="gvTheGrid" runat="server" GridLines="Both" CssClass="gridviewnormal" BorderWidth="0" 
                                            HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" AllowPaging="True"
                                            DataKeyNames="ColumnID" HeaderStyle-ForeColor="Black" Width="100%" AutoGenerateColumns="false"
                                            PageSize="550" OnPreRender="gvTheGrid_PreRender" OnRowCommand="gvTheGrid_RowCommand"
                                            OnRowDataBound="gvTheGrid_RowDataBound" OnRowDeleting="gvTheGrid_RowDeleting"
                                            AlternatingRowStyle-BackColor="#DCF2F0">
                                            <PagerSettings Position="Top" />
                                            <HeaderStyle CssClass="gridview_header" />
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblID" runat="server" Text='<%# Eval("ColumnID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/App_Themes/Default/Images/delete.png"
                                                            CommandName="delete" CommandArgument='<%# Eval("ColumnID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="EditHyperLink" runat="server" ToolTip="Edit" NavigateUrl='<%# GetEditURL() +  Cryptography.Encrypt(Eval("ColumnID").ToString()) %>'
                                                            ImageUrl="~/App_Themes/Default/Images/iconEdit.png" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="sortHandle">
                                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/App_Themes/Default/Images/MoveIcon.png"
                                                            ToolTip="Drag and drop to change order" />
                                                        <input type="hidden" id='hfColumnID' value='<%# Eval("ColumnID") %>' class='ColumnID' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Field Name">
                                                    <ItemTemplate>
                                                        <%--<asp:HyperLink ID="hlView" runat="server" NavigateUrl='<%# GetViewURL() + Cryptography.Encrypt(Eval("ColumnID").ToString())  %>'
                                                            Text='<%# Eval("DisplayName")%>' />--%>
                                                        <asp:Label ID="lblDisplayName" runat="server" Text='<%# Eval("DisplayName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="true" HeaderText="Type" ItemStyle-Width="100px">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIsNumeric" runat="server" Text='<%# Eval("ColumnType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="true" HeaderText="Views" ItemStyle-Width="100px">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkDisplayTextSummary" runat="server" Checked='<%#  !String.IsNullOrEmpty(Eval("DisplayTextSummary").ToString()) %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="true" HeaderText="Detail" ItemStyle-Width="80px">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkDisplayTextDetail" runat="server" Checked='<%#  !String.IsNullOrEmpty(Eval("DisplayTextDetail").ToString()) %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false" HeaderText="Page">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:DropDownList runat="server" ID="ddlTableTab" DataTextField="TabName" DataValueField="TableTabID"
                                                            CssClass="NormalTextBox">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="true" HeaderText=" Importance" ItemStyle-Width="80px">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%--<asp:CheckBox ID="chkIsMandatory" runat="server" Checked='<%#  Eval("IsMandatory") %>' />--%>
                                                        <asp:DropDownList runat="server" ID="ddlImportance" ToolTip="Required means it is important but you can still save the data without it. Mandatory will prevent the data being saved unless entered." CssClass="NormalTextBox">
                                                            <asp:ListItem Value="" Text="Optional" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Value="r" Text="Required"></asp:ListItem>
                                                            <asp:ListItem Value="m" Text="Mandatory"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="true" HeaderText=" Display on the Right" ItemStyle-Width="100px">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkDisplayRight" runat="server" Checked='<%#  Eval("DisplayRight") %>' />
                                                        <%--<asp:Label ID="lblDisplayRight" runat="server" Text=""></asp:Label>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- <asp:TemplateField Visible="true" HeaderText="Import" ItemStyle-Width="70px">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkNameOnImport" runat="server" Checked='<%#  !String.IsNullOrEmpty(Eval("Name_OnImport").ToString()) %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="true" HeaderText="Import Position" ItemStyle-Width="100px">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPositionOnImport" runat="server" Text='<%# Eval("PositionOnImport") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField  HeaderText="Export" ItemStyle-Width="70px" >
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkNameOnExport" runat="server" Checked='<%#  !String.IsNullOrEmpty(Eval("NameOnExport").ToString()) %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Copy Field" ItemStyle-Width="70px" Visible="false"><%-- 14th columns--%>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkAllowCopy" runat="server" Checked='<%#  Eval("AllowCopy")==DBNull.Value?false: Eval("AllowCopy") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="gridview_row" />
                                            <PagerTemplate>
                                                <asp:GridViewPager runat="server" ID="Pager" HideDelete="true" HideFilter="true" HideGo="true" HideNavigation="true" HidePageSize="true"
                                                    OnExportForCSV="Pager_OnExportForCSV" OnApplyFilter="Pager_OnApplyFilter" OnBindTheGridToExport="Pager_BindTheGridToExport"
                                                    OnBindTheGridAgain="Pager_BindTheGridAgain" />
                                            </PagerTemplate>
                                        </dbg:dbgGridView>
                                        <br />
                                        <div runat="server" id="divEmptyFields" visible="false" style="padding-left: 20px;">
                                            <asp:HyperLink runat="server" ID="hlAddNewField" Style="text-decoration: none; color: Black;">
                                            <asp:Image runat="server" ID="Image7" ImageUrl="~/App_Themes/Default/images/BigAdd.png" />
                                            No custom fields have been added yet. <strong style="text-decoration: underline; color: Blue;">
                                                Add new field now.</strong>
                                            </asp:HyperLink>
                                        </div>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="gvTheGrid"></asp:AsyncPostBackTrigger>
                                        <asp:AsyncPostBackTrigger ControlID="chkShowSystemFields"></asp:AsyncPostBackTrigger>
                                    </Triggers>
                                </asp:UpdatePanel>




















                            </div>

                        </ContentTemplate>




















                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="tabProperties" runat="server">
                        <HeaderTemplate>
                            <strong>Properties</strong>

                        </HeaderTemplate>




















                        <ContentTemplate>
                            <div style="padding-left: 20px; padding-top: 10px;">
                                <table>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:HiddenField ID="hfCategory" runat="server" ClientIDMode="Static" Value="display"></asp:HiddenField>
                                                        <asp:LinkButton ID="lnkPropertyDisplay" runat="server" Style="font-weight: bold;" Text="Display"></asp:LinkButton>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkPropertyImportData" runat="server" Text="Import/Data"></asp:LinkButton>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkPropertyAddRecords" runat="server" Text="Add Records"></asp:LinkButton>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkColours" runat="server" Text="Colours"></asp:LinkButton>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkGraphs" runat="server" Text="Graphs"></asp:LinkButton>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkMaps" runat="server" Text="Maps"></asp:LinkButton>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkEmails" runat="server" Text="Emails"></asp:LinkButton>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton ID="lnkArchiveData" runat="server" Text="Archive"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                                <tr style="height: 20px;">
                                                    <td colspan="2">
                                                        <div id="divDisplay">
                                                            <table cellpadding="3">
                                                                <tr>
                                                                    <td align="right" style="width: 230px;"><strong>Name*:</strong> </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtTable" runat="server" CssClass="NormalTextBox" Width="256px"></asp:TextBox>
                                                                        <asp:HyperLink ID="hlTableRename" runat="server" ClientIDMode="Static" CssClass="popuprenametable" Style="display: none;"></asp:HyperLink>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trMenu" runat="server" visible="False">
                                                                    <td runat="server" align="right"><strong>Menu:</strong> </td>
                                                                    <td runat="server"></td>
                                                                </tr>
                                                                <tr style="height: 15px;">
                                                                    <td colspan="2"></td>
                                                                </tr>
                                                                <tr runat="server" visible="False">
                                                                    <td runat="server" align="right"><strong>Search or Filter:</strong> </td>
                                                                    <td runat="server">
                                                                        <asp:DropDownList ID="ddlFilterType" runat="server" CssClass="NormalTextBox">
                                                                            <asp:ListItem Text="Search Fields" Value="box"></asp:ListItem>
                                                                            <asp:ListItem Selected="True" Text="Filter Dropdowns" Value="ddl"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"></td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkShowAdvancedOptions" runat="server" Checked="True" Font-Bold="True" Text="Show Advanced Search Options"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFilter" runat="server" visible="False">
                                                                    <td runat="server" align="right"><strong>Summary Page Filter</strong> </td>
                                                                    <td runat="server" align="left">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <dbg:ControlByColumn ID="cbcvSumFilter" runat="server" OnddlYAxis_Changed="cbcvSumFilter_OnddlYAxis_Changed"></dbg:ControlByColumn>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:CheckBox ID="chkHideFilter" runat="server" Font-Bold="True" Text="Hide Summary Page Filter"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trSummaryPageSort" runat="server" visible="False">
                                                                    <td runat="server" align="right"><strong>Summary Page Sort by</strong> </td>
                                                                    <td runat="server">
                                                                        <asp:DropDownList ID="ddlSortColumn" runat="server" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trShowChangeHistory" runat="server">
                                                                    <td runat="server" align="right"><strong>Show Change History</strong> </td>
                                                                    <td runat="server">
                                                                        <asp:DropDownList ID="ddlShowChangeHistory" runat="server" CssClass="NormalTextBox">

                                                                            <asp:ListItem Selected="True" Text="Always Visible" Value="Always Visible"></asp:ListItem>
                                                                            <asp:ListItem Text="Not visible" Value="Not visible"></asp:ListItem>
                                                                            <asp:ListItem Text="Visible to Admin" Value="Visible to Admin"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trReasonChange" runat="server" visible="False">
                                                                    <td runat="server" align="right"><strong>Reason for change</strong> </td>
                                                                    <td runat="server">
                                                                        <asp:DropDownList ID="ddlReasonChange" runat="server" CssClass="NormalTextBox">
                                                                            <asp:ListItem Text="Mandatory" Value="mandatory"></asp:ListItem>
                                                                            <asp:ListItem Text="Optional" Value="optional"></asp:ListItem>
                                                                            <asp:ListItem Selected="True" Text="None" Value="none"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trChangeHistory" runat="server" visible="False">
                                                                    <td runat="server" align="right"><strong>Change History</strong> </td>
                                                                    <td runat="server">
                                                                        <asp:DropDownList ID="ddlChangeHistory" runat="server" CssClass="NormalTextBox">
                                                                            <asp:ListItem Selected="True" Text="Always Visible" Value="always"></asp:ListItem>
                                                                            <asp:ListItem Text="Visible to Admin Users" Value="admin"></asp:ListItem>
                                                                            <asp:ListItem Text="Not Visible" Value="none"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><strong id="stgDisplayHeader" runat="server">Display Field In Header</strong> </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlHeaderText" runat="server" ClientIDMode="Static" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                        </asp:DropDownList>
                                                                        <asp:HiddenField ID="hfDisplayColumnsFormula" runat="server" ClientIDMode="Static"></asp:HiddenField>
                                                                        &nbsp;
                                                                        <asp:HyperLink ID="hlDDEdit" runat="server" ClientIDMode="Static" NavigateUrl="~/Pages/Help/TableColumn.aspx" Text="Edit"></asp:HyperLink>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"></td>
                                                                    <td>
                                                                        <table style="border-collapse: collapse; border-spacing: 0;">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkSummaryPageContent" runat="server" ClientIDMode="Static"></asp:CheckBox>
                                                                                </td>
                                                                                <td style="padding-left: 2px;">
                                                                                    <asp:HyperLink ID="hlSummaryPageContent" runat="server" ClientIDMode="Static" Text="Display Content Below Summary Page"></asp:HyperLink>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"></td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkBoxAroundField" runat="server" Text="Box Around Fields"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"></td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkShowTabVertically" runat="server" Text="Tabs as a Vertical list"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 15px;">
                                                                    <td colspan="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkNavigationArrows" runat="server" Text="Navigation Arrows"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkSaveAndAdd" runat="server" Text="Save and Add another button"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>
                                                                        <asp:HyperLink ID="hlService" runat="server" Target="_parent">Service...</asp:HyperLink>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="divImportData">
                                                            <table  cellpadding="3">
                                                                <tr id="trMaxTime" runat="server">
                                                                    <td runat="server" align="right"><strong>Maximum Time Between Records:</strong> </td>
                                                                    <td runat="server" align="left">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtMaxTimeBetweenRecords" runat="server" CssClass="NormalTextBox" Width="75px"></asp:TextBox>
                                                                                    &nbsp;&nbsp; </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlMaxTimeBetweenRecordsUnit" runat="server" CssClass="NormalTextBox">
                                                                                        <asp:ListItem Selected="True" Text="Minutes" Value="Minutes"></asp:ListItem>
                                                                                        <asp:ListItem Text="Hours" Value="Hours"></asp:ListItem>
                                                                                        <asp:ListItem Text="Days" Value="Days"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trLateDate" runat="server">
                                                                    <td runat="server" align="right"><strong>Late Data - Notify after:</strong> </td>
                                                                    <td runat="server">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtLateDataDays" runat="server" CssClass="NormalTextBox" Width="75px"></asp:TextBox>
                                                                                    &nbsp;&nbsp;
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlLateDataDays" runat="server" CssClass="NormalTextBox">
                                                                                        <asp:ListItem Selected="True" Text="Days" Value="Days"></asp:ListItem>
                                                                                        <asp:ListItem Text="Hours" Value="Hours"></asp:ListItem>                                
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>


                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" valign="top"><strong>Custom Upload Sheet:</strong> </td>
                                                                    <td>
                                                                        <asp:FileUpload ID="fuRecordFile" runat="server" Font-Size="11px" size="70" Style="width: 500px;"></asp:FileUpload>
                                                                        <br />
                                                                        Please select a CSV/XLS/XLSX file to upload.
                                                                        <br />
                                                                        <asp:LinkButton ID="hlCustomUploadSheet" runat="server" OnClick="hlCustomUploadSheet_Click" Text="View" Visible="False"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 15px;">
                                                                    <td colspan="2"></td>
                                                                </tr>
                                                                <tr id="trUniqueRecordedate" runat="server">
                                                                    <td runat="server" align="right"><strong></strong></td>
                                                                    <td runat="server">
                                                                        <asp:CheckBox ID="chkUniqueRecordedate" runat="server" ClientIDMode="Static" Text="Unique Records Only"></asp:CheckBox>
                                                                        <br />
                                                                        <div id="divUniqueRecord" clientidmode="Static" style="padding-left: 50px;">
                                                                            <table>
                                                                                <tr>
                                                                                    <td align="right" style="width: 150px;"><strong>1st Column</strong> </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlUniqueColumnID" runat="server" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right" style="width: 150px;"><strong>2nd Column</strong> </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlUniqueColumnID2" runat="server" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trDuplicateRecordAction">
                                                                    <td align="right"><strong>Duplicate Record Action </strong></td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlDuplicateRecordAction" runat="server" CssClass="NormalTextBox">
                                                                            <asp:ListItem Selected="True" Text="--Please Select--"></asp:ListItem>
                                                                            <asp:ListItem Text="Reject Duplicates" Value="Reject Duplicates"></asp:ListItem>
                                                                            <asp:ListItem Text="Update Blank Values" Value="Update Blank Values"></asp:ListItem>
                                                                            <asp:ListItem Text="Update All Values" Value="Update All Values"></asp:ListItem>
                                                                            <asp:ListItem Text="Update Where Provided" Value="Update Where Provided"></asp:ListItem>
                                                                            <asp:ListItem Text="Admin Review" Value="Admin Review"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"></td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkCopyToChildTables" runat="server" Text="Copy data to child tables"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><strong>Delete Records Action:</strong></td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlDeleteAction" runat="server" ClientIDMode="Static" CssClass="NormalTextBox">
                                                                            <asp:ListItem Text="--Please Select--" Value=""></asp:ListItem>
                                                                            <asp:ListItem Text="Delete children records" Value="Delete children records"></asp:ListItem>
                                                                            <asp:ListItem Text="Prevent deletion" Value="Prevent deletion"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                               
                                                                <tr id="trchkDeleteRecordsActionAdvanced">
                                                                    <td align="right">&nbsp;</td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkDeleteRecordsActionAdvanced" runat="server" ClientIDMode="Static" Text="Advanced options"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trColumnPreventDeletion">
                                                                    <td align="right"><strong>Prevent deletion when:</strong></td>
                                                                    <td>
                                                                        <table>
                                    <tr>
                                    <td style="width: 10px;"></td>
                                    <td style="width: 10px;"></td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <dbg:ControlByColumn ID="cbcPreventDelete" runat="server" OnddlYAxis_Changed="cbcPreventDelete_OnddlYAxis_Changed" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkAddSearch1" runat="server"><asp:Image ID="Image10" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png" />
</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr runat="server" id="trSearch1" style="display: none;">
                                    <td runat="server">
                                        <asp:LinkButton runat="server" ID="lnkMinusSearch1"><asp:Image ID="Image11" runat="server" ImageUrl="~/App_Themes/Default/Images/Minus.png" />
</asp:LinkButton>
                                    </td>
                                    <td align="right" runat="server">
                                        <asp:HiddenField runat="server" ID="hfAndOr1" />
                                        <asp:LinkButton runat="server" ID="lnkAndOr1" Text="and"></asp:LinkButton>
                                    </td>
                                    <td runat="server">
                                        <table>
                                            <tr>
                                                <td>
                                                    <dbg:ControlByColumn ID="cbcPreventDelete1" runat="server" OnddlYAxis_Changed="cbcPreventDelete1_OnddlYAxis_Changed" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkAddSearch2" runat="server"><asp:Image ID="Image12" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png" />
</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr runat="server" id="trSearch2" style="display: none;">
                                    <td runat="server">
                                        <asp:LinkButton runat="server" ID="lnkMinusSearch2"><asp:Image ID="Image13" runat="server" ImageUrl="~/App_Themes/Default/Images/Minus.png" />
</asp:LinkButton>
                                    </td>
                                    <td align="right" runat="server">
                                        <asp:HiddenField runat="server" ID="hfAndOr2" />
                                        <asp:LinkButton runat="server" ID="lnkAndOr2">and</asp:LinkButton>
                                    </td>
                                    <td runat="server">
                                        <table>
                                            <tr>
                                                <td>
                                                    <dbg:ControlByColumn ID="cbcPreventDelete2" runat="server" OnddlYAxis_Changed="cbcPreventDelete_OnddlYAxis_Changed" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkAddSearch3" runat="server" Style="display: none;"><asp:Image ID="Image14" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png" />
</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr runat="server" id="trSearch3" style="display: none;">
                                    <td runat="server">
                                        <asp:LinkButton runat="server" ID="lnkMinusSearch3"><asp:Image ID="Image15" runat="server" ImageUrl="~/App_Themes/Default/Images/Minus.png" />
</asp:LinkButton>
                                    </td>
                                    <td align="right" runat="server">
                                        <asp:HiddenField runat="server" ID="hfAndOr3" />
                                        <asp:LinkButton runat="server" ID="lnkAndOr3">and</asp:LinkButton>
                                    </td>
                                    <td runat="server">
                                        <table>
                                            <tr>
                                                <td>
                                                    <dbg:ControlByColumn runat="server" ID="cbcPreventDelete3"  />
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table >
                                                                        <asp:HiddenField runat="server" ID="hfTextSearch" />
                                                                    </td>
                                                                </tr>
                                                                <tr   id="trchkCustomNotification">
                                                                    <td align="right" runat="server">&nbsp;</td>
                                                                    <td runat="server">
                                                                        <asp:CheckBox ID="chkCustomNotification" runat="server" ClientIDMode="Static" Text="Custom Notification"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr  id="trCustomizedNotification">
                                                                    <td align="right" runat="server"><strong>Prevent Delete Message:</strong></td>
                                                                    <td runat="server">
                                                                        <asp:TextBox ID="txtCustomizedNotification" runat="server" Height="50px" TextMode="MultiLine" Width="350px"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td align="right"></td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkCopyDataWithTemplate" runat="server" Text="Copy data too when copy the template/table."></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="divAddRecords" style="padding-left: 100px;">
                                                            <table  cellpadding="3">
                                                                <tr id="trAnonymousUser" runat="server" visible="False">
                                                                    <td runat="server" align="right"><strong></strong></td>
                                                                    <td runat="server">
                                                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <div>
                                                                                    <span style="padding-left: 5px;">Parent Table</span>
                                                                                    <asp:DropDownList ID="ddlParentTable" runat="server" AutoPostBack="true" CssClass="NormalTextBox" DataTextField="TableName" DataValueField="ParentTableID" OnSelectedIndexChanged="ddlParentTable_SelectedIndexChanged">
                                                                                    </asp:DropDownList>
                                                                                    <br />
                                                                                    <asp:CheckBox ID="chkAnonymous" runat="server" AutoPostBack="true" OnCheckedChanged="chkAnonymous_CheckedChanged" Text="Allow users to Add records without login" TextAlign="Right" />
                                                                                    <br />
                                                                                    <div id="divValidateUser" runat="server" visible="false">
                                                                                        Validate user be asking them to enter:
                                                                                        <br />
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <asp:DropDownList ID="ddlValidateColumnID1" runat="server" AutoPostBack="false" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                                <td style="padding-left: 5px;">and </td>
                                                                                                <td>
                                                                                                    <asp:DropDownList ID="ddlValidateColumnID2" runat="server" AutoPostBack="false" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                                                    </asp:DropDownList>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <br />
                                                                                    </div>
                                                                                    <br />
                                                                                    Link:<asp:HyperLink ID="hlPublicFormURL" runat="server" NavigateUrl="#" Target="_blank"></asp:HyperLink>
                                                                                    <%--<asp:Label runat="server" ID="lblPublicFormURL"></asp:Label>--%>
                                                                                </div>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 20px;">
                                                                    <td colspan="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" valign="top"></td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkAddUserRecord" runat="server" ClientIDMode="Static" Text="Automatically Create a User Record"></asp:CheckBox>
                                                                        <br />
                                                                        <div id="divAutomaticallyAddUserRecord" clientidmode="Static" style="padding-left: 50px;">
                                                                            <table>
                                                                                <tr>
                                                                                    <td align="right"><strong>Username</strong> </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlAddUserUserColumnID" runat="server" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right"><strong>Password</strong> </td>
                                                                                    <td>
                                                                                        <asp:DropDownList ID="ddlAddUserPasswordColumnID" runat="server" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right"></td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkAddUserNotification" runat="server" Font-Bold="True" Text="Notify User"></asp:CheckBox>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkShowEditAfterAdd" runat="server" ClientIDMode="Static" Text="Show Edit After Add"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkAllowCopyRecords" runat="server" ClientIDMode="Static" Text="Allow users to copy records"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td align="left">
                                                                        <asp:CheckBox ID="chkShowChildTabsOnAdd" runat="server" ClientIDMode="Static" Text="Show Child Tabs On Add"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="divColours">
                                                            <table  cellpadding="3">
                                                                <tr>
                                                                    <td align="right"><strong>Header Colour:</strong> </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtHeaderColor" runat="server" CssClass="NormalTextBox" MaxLength="6"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><strong>Tab Colour:</strong> </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtTabColour" runat="server" CssClass="NormalTextBox" MaxLength="6"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><strong>Tab Text Colour:</strong> </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtTabTextColour" runat="server" CssClass="NormalTextBox" MaxLength="6"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><strong>Filter Top Colour:</strong> </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFilterTopColour" runat="server" CssClass="NormalTextBox" MaxLength="6"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><strong>Filter Bottom Colour:</strong> </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFilterBottomColour" runat="server" CssClass="NormalTextBox" MaxLength="6"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="divGraphs">
                                                            <table  cellpadding="3">
                                                                <tr>
                                                                    <td align="right"><strong>Default Graph:</strong> </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlDefaultTableGraphOption" runat="server" CssClass="NormalTextBox" >
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                 <tr>
                                                                    <td align="right"><strong>Default Field:</strong> </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlGraphDefaultYAxisColumnID" runat="server" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><strong>X-Axis:</strong> </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlGraphXAxisColumnID" runat="server" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><strong>Series:</strong> </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlGraphSeriesColumnID" runat="server" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                               
                                                                <tr>
                                                                    <td align="right"><strong>On Start:</strong> </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlGraphOnStart" runat="server" CssClass="NormalTextBox">
                                                                            <asp:ListItem Text="Show Averages" Value="Averages"></asp:ListItem>
                                                                            <asp:ListItem Text="Show Empty Graph" Value="EmptyGraph"></asp:ListItem>
                                                                            <asp:ListItem Text="Show All Series" Value="AllSeries"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><strong>Default Graph Period:</strong> </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlDefaultGraphPeriod" runat="server" CssClass="NormalTextBox">
                                                                            <asp:ListItem Value="-1">-- Please Select --</asp:ListItem>
                                                                            <asp:ListItem Value="0">All</asp:ListItem>
                                                                            <asp:ListItem Value="1">1 year</asp:ListItem>
                                                                            <asp:ListItem Value="2">6 months</asp:ListItem>
                                                                            <asp:ListItem Value="3">3 months</asp:ListItem>
                                                                            <asp:ListItem Value="4">1 month</asp:ListItem>
                                                                            <asp:ListItem Value="5">1 week</asp:ListItem>
                                                                            <asp:ListItem Value="6">1 day</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><strong>Missing Values:</strong></td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlMissingValues" runat="server" CssClass="NormalTextBox">
                                                                            <asp:ListItem Value="ignore">Ignore and plot next</asp:ListItem>
                                                                            <asp:ListItem Value="noplot">Do not plot (show gaps)</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><strong>Values With Characters:</strong></td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlSpecialCharValue" runat="server" CssClass="NormalTextBox">
                                                                            <asp:ListItem Value="plot">Plot as numbers</asp:ListItem>
                                                                            <asp:ListItem Value="ignore">Ignore and plot next</asp:ListItem>
                                                                            <%--<asp:ListItem Value="noplot">Do not plot (show gaps)</asp:ListItem>--%>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="divMaps">
                                                            <table  cellpadding="3">
                                                                <tr id="Tr1" runat="server">
                                                                    <td runat="server" align="right"><strong>Pin</strong> </td>
                                                                    <td runat="server" align="left">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlPinImages" runat="server" ClientIDMode="Static" CssClass="NormalTextBox">
                                                                                        <%--<asp:ListItem Selected="True" Text="--Advanced--" Value=""></asp:ListItem>--%>
                                                                                        <asp:ListItem Selected="True" Text="--Please Select--" Value="Pages/Record/PINImages/DefaultPin.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Pin Blue" Value="Pages/Record/PINImages/PinBlue.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Pin Black" Value="Pages/Record/PINImages/PinBlack.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Pin Gray" Value="Pages/Record/PINImages/PinGray.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Pin Green" Value="Pages/Record/PINImages/PinGreen.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Pin Light Blue" Value="Pages/Record/PINImages/PinLBlue.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Pin Orange" Value="Pages/Record/PINImages/PinOrange.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Pin Purple" Value="Pages/Record/PINImages/PinPurple.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Pin Red" Value="Pages/Record/PINImages/PinRed.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Pin Yellow" Value="Pages/Record/PINImages/PinYellow.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Round Blue" Value="Pages/Record/PINImages/RoundBlue.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Round Gray" Value="Pages/Record/PINImages/RoundGray.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Round Green" Value="Pages/Record/PINImages/RoundGreen.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Round Light Blue" Value="Pages/Record/PINImages/RoundLightBlue.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Round Orange" Value="Pages/Record/PINImages/RoundOrange.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Round Purple" Value="Pages/Record/PINImages/RoundPurple.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Round Red" Value="Pages/Record/PINImages/RoundRed.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Round White" Value="Pages/Record/PINImages/RoundWhite.png"></asp:ListItem>
                                                                                        <asp:ListItem Text="Round Yellow" Value="Pages/Record/PINImages/RoundYellow.png"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Panel ID="pnlPINImages" runat="server" Width="50px">
                                                                                        &#160;
                                                                                        <asp:Image ID="imgPIN" runat="server" ClientIDMode="Static" ImageUrl="~/Pages/Record/PINImages/DefaultPin.png"></asp:Image>
                                                                                    </asp:Panel>
                                                                                </td>
                                                                                <td align="left">
                                                                                    <asp:HyperLink runat="server" ID="hlAdvancedPinImage" Style="text-decoration: none;  padding-bottom: 10px;"
                                                                                        CssClass="popuplinkpin" ClientIDMode="Static"> 
                                                                                            Advanced...
                                                                                    </asp:HyperLink>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><strong>Display Order</strong> </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPinDisplayOrder" runat="server" CssClass="NormalTextBox" Width="50px"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtPinDisplayOrder" Display="Dynamic" ErrorMessage="Numeric!" ValidationExpression="(^-?\d{1,18}\.$)|(^-?\d{1,18}$)|(^-?\d{0,18}\.\d{1,4}$)"></asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right"><strong>Distance Rings (km)</strong> </td>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtDistanceRings" runat="server" CssClass="NormalTextBox" ToolTip="You can display multiple rings by putting commas in between e.g. 5,10,20" Width="115px"></asp:TextBox>
                                                                                    <asp:RegularExpressionValidator ID="validDistanceRings" runat="server" ControlToValidate="txtDistanceRings" Display="Dynamic" ErrorMessage="Wrong Format!" ValidationExpression="^\d+(,\d+)*$"></asp:RegularExpressionValidator>
                                                                                </td>
                                                                                <td style="padding-left: 20px">
                                                                                    <asp:CheckBox ID="chkShowOnStart" runat="server" Text="Show on start up"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trFlashAlerts" runat="server">
                                                                    <td runat="server"></td>
                                                                    <td runat="server">
                                                                        <asp:CheckBox ID="chkFlashAlerts" runat="server" Text="Flash on recent alerts"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="divEmails" style="padding-left: 200px;">
                                                            <br />
                                                            <br />
                                                            <asp:CheckBox ID="chkShowSentEmails" runat="server" Font-Bold="True" Text="Show Sent Emails"></asp:CheckBox>
                                                            <br />
                                                            <br />
                                                            <asp:CheckBox ID="chkShowReceivedEmails" runat="server" Font-Bold="True" Text="Show Received Emails "></asp:CheckBox>
                                                        </div>
                                                         <div id="divArchiveData"  >
                                                            <br />
                                                            <br />
                                                            <table  cellpadding="3">
                                                                <tr>
                                                                    <td><strong>Archive Data After:</strong> &nbsp;</td>
                                                                    <td>
                                                                         <asp:DropDownList ID="ddlArchiveDataMonth" ClientIDMode="Static" CssClass="NormalTextBox" runat="server">
                                                                <asp:ListItem Selected="True" Text="--Please Select--" Value="-1"></asp:ListItem>
                                                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                              <%--// Red Ticket 3358_2--%>
                                                                <%--<asp:ListItem Text="7" Value="7"></asp:ListItem>--%>
                                                                <%--<asp:ListItem Text="8" Value="8"></asp:ListItem>--%>
                                                                <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                                <%--<asp:ListItem Text="10" Value="10"></asp:ListItem>--%>
                                                                <%--<asp:ListItem Text="11" Value="11"></asp:ListItem>--%>
                                                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                                              <%--<asp:ListItem Text="13" Value="13"></asp:ListItem>--%>
                                                                              <%--<asp:ListItem Text="13" Value="13"></asp:ListItem>--%>
                                                                              <%--<asp:ListItem Text="15" Value="15"></asp:ListItem>--%>
                                                                              <%--<asp:ListItem Text="16" Value="16"></asp:ListItem>--%>
                                                                              <%--<asp:ListItem Text="17" Value="17"></asp:ListItem>--%>
                                                                              <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                                              <%--<asp:ListItem Text="19" Value="19"></asp:ListItem>--%>
                                                                              <%--<asp:ListItem Text="20" Value="20"></asp:ListItem>--%>
                                                                              <%--<asp:ListItem Text="21" Value="21"></asp:ListItem>--%>
                                                                              <%--<asp:ListItem Text="22" Value="22"></asp:ListItem>--%>
                                                                              <%--<asp:ListItem Text="23" Value="23"></asp:ListItem>--%>
                                                                              <asp:ListItem Text="24" Value="24"></asp:ListItem>
                                                                             <%-- <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                                                              <asp:ListItem Text="26" Value="26"></asp:ListItem>
                                                                              <asp:ListItem Text="27" Value="27"></asp:ListItem>
                                                                              <asp:ListItem Text="28" Value="28"></asp:ListItem>
                                                                              <asp:ListItem Text="29" Value="29"></asp:ListItem>
                                                                             <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                                                             <asp:ListItem Text="31" Value="31"></asp:ListItem>
                                                                             <asp:ListItem Text="32" Value="32"></asp:ListItem>
                                                                             <asp:ListItem Text="33" Value="33"></asp:ListItem>
                                                                             <asp:ListItem Text="34" Value="34"></asp:ListItem>
                                                                             <asp:ListItem Text="35" Value="35"></asp:ListItem>--%>
                                                                             <asp:ListItem Text="36" Value="36"></asp:ListItem>
                                                                             <asp:ListItem Text="48" Value="48"></asp:ListItem>
                                                                             <asp:ListItem Text="60" Value="60"></asp:ListItem>
                                                          

                                                            </asp:DropDownList> &nbsp;

                                                                        <strong>month(s)</strong>
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                                 <tr id="trddlArchiveColumnID">
                                                                  
                                                                    <td>
                                                                        <strong>Archive Date*:</strong>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlArchiveDateColumn" ClientIDMode="Static" DataTextField="DisplayName" DataValueField="ColumnID" runat="server"></asp:DropDownList>
                                                                    </td>
                                                                        
                                                                </tr>
                                                                <tr id="trddlArchiveDatabase">
                                                                  
                                                                    <td>
                                                                        <strong>Archive Database*:</strong>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlArchiveDatabase" ClientIDMode="Static" runat="server"></asp:DropDownList>
                                                                    </td>
                                                                        
                                                                </tr>
                                                                <tr id="trlblLastArchiveRun">
                                                                    <td>
                                                                        <strong>Last Archive Run:</strong>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblLastArchiveRun" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>

                                                            </table>
                                                           
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"></td>
                                                </tr>
                                                <tr>
                                                    <td align="right"></td>
                                                    <td align="left">
                                                        <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtLateDataDays" Display="None" ErrorMessage="Invalid Late data!" MaximumValue="1000000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
                                                        <asp:RequiredFieldValidator ID="rfvTable" runat="server" ControlToValidate="txtTable" CssClass="NormalTextBox" Display="None" ErrorMessage="Name - Required"></asp:RequiredFieldValidator>
                                                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtMaxTimeBetweenRecords" Display="None" ErrorMessage="Invalid Maximum Time Between Records!" MaximumValue="1000000" MinimumValue="0" Type="Double"></asp:RangeValidator>
                                                        <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Please correct following errors:" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top" style="vertical-align: top">
                                            <div runat="server" id="divSave">
                                                <asp:LinkButton runat="server" ID="lnkSave" OnClick="lnkSave_Click"><asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                        ToolTip="Save" />
</asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </div>

                        </ContentTemplate>




















                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="tabChildTTables" runat="server">
                        <HeaderTemplate>
                            <strong>Child Tables</strong>

                        </HeaderTemplate>




















                        <ContentTemplate>
                            <asp:UpdatePanel ID="upTableChild" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="padding-left: 20px; padding-top: 10px;">
                                        <asp:GridView ID="grdTable" runat="server" AutoGenerateColumns="False" DataKeyNames="TableChildID" EmptyDataText="No Child Tables"
                                            HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" CssClass="gridviewnormal"
                                            OnRowCommand="grdTable_RowCommand" OnRowDataBound="grdTable_RowDataBound" AlternatingRowStyle-BackColor="#DCF2F0">
                                            <RowStyle CssClass="gridview_row" />
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("TableChildID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/App_Themes/Default/Images/icon_delete.gif"
                                                            CommandName="deletetype" CommandArgument='<%# Eval("TableChildID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" CssClass="popuplinkct" NavigateUrl="~/Pages/Record/ChildTableDetail.aspx"
                                                            ImageUrl="~/App_Themes/Default/Images/iconEdit.png" ToolTip="Edit" ID="hlEditDetail"></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:HyperLink runat="server" CssClass="popuplinkct" NavigateUrl="~/Pages/Record/ChildTableDetail.aspx"
                                                            ImageUrl="~/Pages/Pager/Images/add.png" ToolTip="Add Child Table" ID="hlAddDetail" Visible="false"></asp:HyperLink>
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="sortHandle2">
                                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image4" runat="server" ImageUrl="~/App_Themes/Default/Images/MoveIcon.png"
                                                            ToolTip="Drag and drop to change order" />
                                                        <input type="hidden" id='hfTableChildID' value='<%# Eval("TableChildID") %>' class='TableChildID' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Table">
                                                    <ItemTemplate>
                                                        <div style="padding-right: 10px;">
                                                            <a target="_blank" href='<%# GetTableViewURL() + Cryptography.Encrypt(Eval("ChildTableID").ToString())  %>'>
                                                                <%# Eval("ChildTableName")%></a>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <div style="padding-left: 10px;">
                                                            <asp:Label runat="server" ID="lblDescription" Text='<%# Eval("Description")%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Show On Detail Page">
                                                    <ItemTemplate>
                                                        <div style="padding-left: 10px;">
                                                            <asp:Label runat="server" ID="lblDetailPageType" Text=""></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:CheckBoxField HeaderText="Add Button" DataField="ShowAddButton" />--%>
                                                <%--<asp:CheckBoxField HeaderText="Edit Button" DataField="ShowEditButton" />--%>
                                                <%--<asp:TemplateField Visible="false" HeaderText="Add Button" ItemStyle-Width="100px">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkShowAddButton" runat="server" AutoPostBack="true" OnCheckedChanged="UpdateTableChild"
                                                            Checked='<%#  Eval("ShowAddButton")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false" HeaderText="Edit Button" ItemStyle-Width="100px">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkShowEditButton" runat="server" AutoPostBack="true" OnCheckedChanged="UpdateTableChild"
                                                            Checked='<%#  Eval("ShowEditButton") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField Visible="true" HeaderText="Show When" ItemStyle-Width="100px">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkConditions" runat="server" AutoPostBack="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                No Child Tables
                                                <div style="color: red; font-size: 14px; margin: 20px 100px">
                                                    To add a Child Table:<br />
                                                    1. Create a new table or edit an existing table<br />
                                                    2. Add a dropdown type Field<br />
                                                    3. Point it to this table
                                                </div>
                                            </EmptyDataTemplate>
                                            <HeaderStyle CssClass="gridview_header" />
                                        </asp:GridView>
                                    </div>
                                    <br />
                                    <div runat="server" id="divEmptyData" visible="false" style="padding-left: 20px;">
                                        <asp:HyperLink runat="server" ID="hplAddChildTable" Style="text-decoration: none; color: Black;"
                                            CssClass="popuplink">
                                            <asp:Image runat="server" ID="imgAddNewRecord" ImageUrl="~/App_Themes/Default/images/BigAdd.png" />
                                            No records have been added yet. <strong style="text-decoration: underline; color: Blue;">
                                                Add new record now.</strong>
                                        </asp:HyperLink>
                                    </div>
                                    <br />
                                    <asp:HyperLink runat="server" ID="hplAddTab" CssClass="popuplink">
                                           Add a Tab
                                    </asp:HyperLink>
                                    <br />
                                    <asp:HiddenField runat="server" ID="hfTableChildDeleteID" Value="" />
                                    <asp:Label runat="server" ID="lblModalDeleteTableChild" />
                                    <ajaxToolkit:ModalPopupExtender ID="mpeModalDeleteTableChild" ClientIDMode="Static"
                                        runat="server" BehaviorID="popup" TargetControlID="lblModalDeleteTableChild"
                                        PopupControlID="pnlModalDeleteTableChild" BackgroundCssClass="modalBackground"
                                        OkControlID="lnkDeleteTableChildCancel" />
                                    <asp:Panel ID="pnlModalDeleteTableChild" runat="server" Style="display: none">
                                        <div style="border-width: 5px; background-color: #ffffff; border-color: #4F8FDD; height: 150px; border-style: outset;">
                                            <div style="padding-top: 50px; padding: 20px;">
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label Width="400px" runat="server" ID="lblTableChildDelateMessage" Text="Are you sure you wish to delete this relationship?  
                                                                The link field will be converted to a text field and the 
                                                                values retained so you can relink it later if you want."></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 150px;"></td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:LinkButton runat="server" ID="lnkDeleteTableChildOK" CssClass="btn" CausesValidation="false"
                                                                            OnClick="lnkDeleteTableChildOK_Click"> <strong>Ok</strong></asp:LinkButton>
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton runat="server" ID="lnkDeleteTableChildCancel" CssClass="btn" CausesValidation="false"> <strong>Cancel</strong></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <br />
                                    <br />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="grdTable" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </ContentTemplate>




















                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="tabTemplates" runat="server">
                        <HeaderTemplate>
                            <strong>Templates</strong>

                        </HeaderTemplate>




















                        <ContentTemplate>

                            <div style="padding-left: 20px; padding-top: 10px;">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <asp:HiddenField runat="server" ID="hfTemplateCategory" Value="import" ClientIDMode="Static" />
                                            <asp:LinkButton runat="server" ID="lnkTemplateImport" Text="Import" Font-Bold="true"></asp:LinkButton>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton runat="server" ID="lnkTemplateExport" Text="Export"></asp:LinkButton>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:LinkButton runat="server" ID="lnkTemplateWordMerge" Text="Word Merge"></asp:LinkButton>

                                        </td>
                                    </tr>
                                    <tr style="height: 20px;">
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <!-- Import Template-->
                                            <div id="divTemplateImport">
                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>

                                                        <!-- JA 09 FEB 2017 -->
                                                        <table>
                                                            <tr>

                                                                <td align="right" style="vertical-align: bottom;">
                                                                    <strong>Default Template</strong>
                                                                </td>
                                                                <td style="vertical-align: bottom;">
                                                                    <asp:DropDownList runat="server" ID="ddlTemplate" CssClass="NormalTextBox ddlist">
                                                                    </asp:DropDownList>
                                                                    <br />
                                                                </td>
                                                                <td align="right" class="space">&nbsp;</td>


                                                                <td style="vertical-align: top;">
                                                                    <asp:LinkButton runat="server" ID="lnkImportTemplateSave" OnClick="lnkImportTemplateSave_Click">
                                            <asp:Image runat="server" ID="Image5" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                ToolTip="Save Default Template" />
                                                                    </asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!-- END -->

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>

                                                <br />
                                                <dbg:ImpTem runat="server" ID="itOne" OnPreRender="ImpTem_OnPreRender" />
                                            </div>

                                            <!-- Export Template-->
                                            <!-- JA 09 FEB 2017 -->
                                            <div id="divTemplateExport">

                                                <table class="table">
                                                    <tr>

                                                        <td align="right" style="vertical-align: bottom;">
                                                            <strong>Default Template</strong>
                                                        </td>
                                                        <td style="vertical-align: bottom;">
                                                            <asp:DropDownList runat="server" ID="ddlTemplate_export" CssClass="NormalTextBox ddlist">
                                                            </asp:DropDownList>
                                                            <br />
                                                        </td>
                                                        <td align="right" class="space">&nbsp;</td>
                                                        <td style="vertical-align: top;">
                                                            <asp:LinkButton runat="server" ID="LinkButton_export" OnClick="lnkExportTemplateSave_Click">
                                            <asp:Image runat="server" ID="Image_export" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                ToolTip="Save Default Template" />
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>


                                                <!-- Export list calling-->
                                                <dbg:ExpTem runat="server" ID="etOne" />

                                            </div>
                                            <!-- END EXPORT-->
                                            <!-- END -->


                                            <div id="divTemplateWordMerge">
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div style="padding-left: 20px; padding-top: 10px;">
                                                            <asp:GridView ID="gvTemplates" runat="server" AutoGenerateColumns="False" DataKeyNames="DocTemplateID"
                                                                HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" CssClass="gridview"
                                                                OnRowCommand="gvTemplates_RowCommand" OnRowDataBound="gvTemplates_RowDataBound">
                                                                <Columns>
                                                                    <asp:TemplateField Visible="false">
                                                                        <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("DocTemplateID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/App_Themes/Default/Images/delete_s.png"
                                                                                CommandName="deletetype" CommandArgument='<%# Eval("DocTemplateID") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="">
                                                                        <ItemTemplate>

                                                                            <asp:HyperLink runat="server" CssClass="popuplink" NavigateUrl="~/Pages/Record/DocTemplateDetail.aspx"
                                                                                ImageUrl="~/App_Themes/Default/Images/iconEdit.png" ToolTip="Edit" ID="HyperLink1"></asp:HyperLink>

                                                                        </ItemTemplate>
                                                                        <HeaderTemplate>
                                                                            <asp:HyperLink runat="server" CssClass="popuplink" NavigateUrl="~/Pages/Record/DocTemplateDetail.aspx"
                                                                                ImageUrl="~/Pages/Pager/Images/add.png" ToolTip="Add Child Table" ID="HyperLink2"></asp:HyperLink>
                                                                        </HeaderTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Word Document">
                                                                        <ItemTemplate>
                                                                            <div style="padding-right: 10px;">
                                                                                <asp:Label runat="server" ID="lblFileName" Text='<%# Eval("FileName")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%--//oliver <begin> Jon asked me to hide this based on email 8/18/2016--%>
                                                                    <asp:TemplateField HeaderText="Data Retriever" Visible="false">
                                                                        <ItemTemplate>
                                                                            <div style="padding-left: 10px;">
                                                                                <asp:Label runat="server" ID="lblSPName" Text='<%# Eval("SPName")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <%--//oliver <end>--%>
                                                                </Columns>
                                                                <HeaderStyle CssClass="gridview_header" />
                                                            </asp:GridView>
                                                        </div>
                                                        <br />
                                                        <div runat="server" id="divEmptyDataTemplates" visible="false" style="padding-left: 20px;">
                                                            <asp:HyperLink runat="server" ID="hlAddTemplates" Style="text-decoration: none; color: Black;"
                                                                CssClass="popuplink">
                                            <asp:Image runat="server" ID="Image6" ImageUrl="~/App_Themes/Default/images/BigAdd.png" />
                                            No records have been added yet. <strong style="text-decoration: underline; color: Blue;">
                                                Add new record now.</strong>
                                                            </asp:HyperLink>
                                                        </div>
                                                        <br />
                                                        <br />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="gvTemplates" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>

                                        </td>
                                    </tr>
                                </table>
                            </div>




                        </ContentTemplate>




















                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabTable" runat="server">
                        <HeaderTemplate>
                            <strong>Notifications</strong>

                        </HeaderTemplate>




















<ContentTemplate>
                                   
                            <asp:UpdatePanel ID="upWarningRecipients" runat="server" UpdateMode="Conditional"><ContentTemplate>
                                     <div id="divAddUser" style=" display:none; min-height:300px;  overflow: hidden">
	                                    <div><h3> <strong>Add Recipient</strong></h3> </div>
                                            <table cellpadding="0" cellspacing="0" style="padding-top:10px">                                             
                                                <tr>                                                    
                                                    <td>
                                                        <table>
                                                            <tr >
                                                                <td>
                                                                    <asp:DropDownList ID="ddlUserChoices" runat="server"
                                                                        CssClass="NormalTextBox" ClientIDMode="Static">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td runat="server" id="tdReminderColumn" clientidmode="Static" >
                                                                    <asp:DropDownList ID="ddlUser" runat="server" DataTextField="UserName" DataValueField="UserID"
                                                                        CssClass="NormalTextBox">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                 <td runat="server" id="tdReminderColumnOne" clientidmode="Static" >
                                                                    <asp:DropDownList ID="ddlNotificationEmailFromRecord" runat="server" ClientIDMode="Static" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                 <td runat="server" id="tdReminderColumnTwo" clientidmode="Static" >
                                                                    <asp:DropDownList ID="ddlNotificationPhoneFromRecord" runat="server" ClientIDMode="Static" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </td>                                                    
                                                    <td style="padding-left:50px">
                                                            <a id="lnkAddUser" data-fancybox-close href="javascript:;" class="btn">Add</a>
                                                    </td>
                                                </tr>
                                                <tr id="trErrorNoRecipient" style="display:none; color:red; font-size:12px; font-style:italic">
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>
                                    </div>
                                     <asp:HiddenField runat="server" ID="hfSelectedUser" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfRecipientOption" ClientIDMode="Static" />
                                    <div style="padding-left: 50px; padding-top: 10px;">
                                        <table>
                                            <tr>
                                                <td>
                                               <strong>Standard Notifications</strong>
                                            </td>
                                                <td style="text-align:right">   
                                                    <div>                                                 
                                                     <a id="lnkAddRecipeint" data-fancybox-title="Add Recipient" data-fancybox data-options='{"src": "#divAddUser", "touch": false, "smallBtn" : true, "autoSize": false}'  href="javascript:;">
                                                     <img src="../Pager/Images/add.png" alt="Add Recipient" />
                                                           </a>
                                                        </div> 
                                             </td>
                                                <td>&nbsp;</td>
                                               
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:GridView ID="grdTableUser" runat="server" AutoGenerateColumns="False" DataKeyNames="TableUserID"
                                            CssClass="gridviewborder" OnRowCommand="grdTableUser_RowCommand" OnRowDataBound="grdTableUser_RowDataBound"
                                            BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" ShowHeaderWhenEmpty="true"
                                            ShowFooter="true">
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblID" Text='<%# Eval("TableUserID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/App_Themes/Default/Images/delete.png"
                                                            CommandName="deletetype" CommandArgument='<%# Eval("TableUserID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:BoundField HeaderText="User" DataField="UserName" />--%>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td style="padding: 5px 5px 5px 5px; text-align:center">
                                                                        <asp:Label runat="server" ID="lblUserNameHeader" Text="User"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                      
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td style="padding: 5px 5px 5px 5px; text-align: center">
                                                                      <asp:Label runat="server" ID="lblUserName" Text='<%# Eval("UserName") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                      
                                                        <%--<asp:Label runat="server" ID="lblUserName"></asp:Label>--%>
                                                    </ItemTemplate>
                                                    <%--<FooterTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="center">
                                                                    <p>Templates</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                   <p></p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td> <p></p></td>
                                                            </tr>
                                                             <tr>
                                                                <td> <p>Min Period Between</p></td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>--%>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td align="center">
                                                                    <strong>Add Data Notifications</strong>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <FooterTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:HyperLink ID="hlAddDataEmail" runat="server" Target="_blank">View Email</asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:HyperLink ID="hlAddDataSMS" runat="server" Target="_blank">View SMS</asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p></p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p></p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:DropDownList runat="server" ID="ddlAddDataOption" AutoPostBack="true" CssClass="NormalTextBox"
                                                            OnSelectedIndexChanged="UpdateSupplyLed">
                                                            <asp:ListItem Text="None" Value="none" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="Email" Value="email"></asp:ListItem>
                                                            <asp:ListItem Text="SMS" Value="sms"></asp:ListItem>
                                                            <asp:ListItem Text="Both" Value="both"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td align="center">
                                                                    <strong>Upload Notifications</strong>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <FooterTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:HyperLink ID="hlUploadEmail" runat="server" Target="_blank">View Email</asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:HyperLink ID="hlUploadSMS" runat="server" Target="_blank">View SMS</asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p></p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p></p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:DropDownList runat="server" ID="ddlUploadOption" AutoPostBack="true" CssClass="NormalTextBox"
                                                            OnSelectedIndexChanged="UpdateSupplyLed">
                                                            <asp:ListItem Text="None" Value="none"></asp:ListItem>
                                                            <asp:ListItem Text="Email" Value="email" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="SMS" Value="sms"></asp:ListItem>
                                                            <asp:ListItem Text="Both" Value="both"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td align="center">
                                                                    <strong>Warnings</strong>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <FooterTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:HyperLink ID="hlUploadWarningEmail" runat="server" Target="_blank">View Email</asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:HyperLink ID="hlUploadWarningSMS" runat="server" Target="_blank">View SMS</asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p></p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:DropDownList runat="server" ID="ddlWarningMinPeriod" AutoPostBack="true"
                                                                        CssClass="NormalTextBox" OnSelectedIndexChanged="UpdateSupplyLed">
                                                                        <asp:ListItem Text="No Min (default)" Value="" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Text="1 hour" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="4 hours" Value="4"></asp:ListItem>
                                                                        <asp:ListItem Text="1 day" Value="24"></asp:ListItem>
                                                                        <asp:ListItem Text="1 week" Value="168"></asp:ListItem>

                                                                    </asp:DropDownList>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:DropDownList runat="server" ID="ddlUploadWarningOption" AutoPostBack="true"
                                                            CssClass="NormalTextBox" OnSelectedIndexChanged="UpdateSupplyLed">
                                                            <asp:ListItem Text="None" Value="none"></asp:ListItem>
                                                            <asp:ListItem Text="Email" Value="email" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="SMS" Value="sms"></asp:ListItem>
                                                            <asp:ListItem Text="Both" Value="both"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td align="center">
                                                                    <strong>Exceedances</strong>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <FooterTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:HyperLink ID="hlUploadExceedanceEmail" runat="server" Target="_blank">View Email</asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:HyperLink ID="hlUploadExceedanceSMS" runat="server" Target="_blank">View SMS</asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p></p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:DropDownList runat="server" ID="ddlExceedanceMinPeriod" AutoPostBack="true"
                                                                        CssClass="NormalTextBox" OnSelectedIndexChanged="UpdateSupplyLed">
                                                                        <asp:ListItem Text="No Min (default)" Value="" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Text="1 hour" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="4 hours" Value="4"></asp:ListItem>
                                                                        <asp:ListItem Text="1 day" Value="24"></asp:ListItem>
                                                                        <asp:ListItem Text="1 week" Value="168"></asp:ListItem>

                                                                    </asp:DropDownList>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:DropDownList runat="server" ID="ddlUploadExceedanceOption" AutoPostBack="true"
                                                            CssClass="NormalTextBox" OnSelectedIndexChanged="UpdateSupplyLed">
                                                            <asp:ListItem Text="None" Value="none"></asp:ListItem>
                                                            <asp:ListItem Text="Email" Value="email" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="SMS" Value="sms"></asp:ListItem>
                                                            <asp:ListItem Text="Both" Value="both"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td align="center">
                                                                    <strong>Late data/Flat data</strong>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <FooterTemplate>
                                                        <table width="100%">
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:HyperLink ID="hlLateWarningEmail" runat="server" Target="_blank">View Email</asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:HyperLink ID="hlLateWarningSMS" runat="server" Target="_blank">View SMS</asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p></p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">


                                                                    <%-- commented by KG 18/7/2017 Ticket 2571                                                                
                                                                    <asp:DropDownList runat="server" ID="ddlFlatDataMinPeriod" AutoPostBack="true"
                                                                        CssClass="NormalTextBox" OnSelectedIndexChanged="UpdateSupplyLed">
                                                                        <asp:ListItem Text="No Min (default)" Value="" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Text="1 hour" Value="1" ></asp:ListItem>
                                                                        <asp:ListItem Text="4 hours" Value="4"></asp:ListItem>
                                                                        <asp:ListItem Text="1 day" Value="24"></asp:ListItem>
                                                                        <asp:ListItem Text="1 week" Value="168"></asp:ListItem>
                                                                    </asp:DropDownList>--%>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:DropDownList runat="server" ID="ddlLateWarningOption" AutoPostBack="true" CssClass="NormalTextBox"
                                                            OnSelectedIndexChanged="UpdateSupplyLed">
                                                            <asp:ListItem Text="None" Value="none"></asp:ListItem>
                                                            <asp:ListItem Text="Email" Value="email" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="SMS" Value="sms"></asp:ListItem>
                                                            <asp:ListItem Text="Both" Value="both"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridviewborder_header" />
                                            <RowStyle CssClass="gridviewborder_row" />
                                        </asp:GridView>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <br />
                                                    <br />
                                                </td>
                                               
                                            </tr>
                                            </table>
                                       
                                        <%--Red 11072019: Ticket 4589--%> 
                                        <div runat="server" id="divUserAdd" style="display:none" >
                                              <asp:LinkButton runat="server" ID="lnkSmallSave" CssClass="btn" CausesValidation="False"
                                                                OnClick="lnkSmallSave_Click"> <strong>Add</strong>   </asp:LinkButton>
                                        </div>  
                                                                               
                                        <%-- End Red --%> 
                                        <div>
                                            <asp:Label ID="lblMsgTab" runat="server" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>


                                  

                                
</ContentTemplate>
<Triggers>
<asp:AsyncPostBackTrigger ControlID="grdTableUser" />
</Triggers>
</asp:UpdatePanel>

   <%-- </ContentTemplate>
                        <ContentTemplate>--%>
    

    <asp:UpdatePanel ID="updatePanelSpecialNotification" runat="server" UpdateMode="Conditional">
        <ContentTemplate>  
              <div style="padding-left: 50px; padding-top: 10px;">
                <%--Red--%>
                 <table>
                                             <tr>
                                                  <td>
                                               <strong>Special Notifications</strong>
                                            </td>
                                                <td style="text-align:right">
                                                       <div>          
                                                           <asp:HyperLink  ID="hlAddSpeciaNotification" ImageUrl="../Pager/Images/add.png" runat="server">                                    
                                                    <%-- <a id="aAddSpecialNotification" runat="server"   href="javascript:;">
                                                     <img src="../Pager/Images/add.png" alt="Add Recipient" />
                                                           </a>--%>
                                                               </asp:HyperLink>   
                                                        </div> 
                                                </td>
                                               
                                               
                                            </tr>
                                             
                                            <tr>
                                                <td colspan="2">
                                                    <asp:GridView ID="grdSpecialNotification" runat="server" AutoGenerateColumns="False" DataKeyNames="SpecialNotificationID"
                                            CssClass="gridviewborder" OnRowDataBound="grdSpecialNotification_RowDataBound" OnRowCommand="grdSpecialNotification_RowCommand"
                                            BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" ShowHeaderWhenEmpty="true"
                                            ShowFooter="false">
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblID" Text='<%# Eval("SpecialNotificationID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                      
                                                        <asp:ImageButton ID="imgbtnSpecialNotificationDelete" runat="server" ImageUrl="~/App_Themes/Default/Images/delete.png"
                                                            CommandName="deletetype"   CommandArgument='<%# Eval("SpecialNotificationID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:BoundField HeaderText="User" DataField="UserName" />--%>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                         <table>
                                                            <tr>
                                                                <td align="center" style="padding:">
                                                                    <strong>Special Notification</strong>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <%--<asp:Label runat="server" ID="lblUserNameHeader" Text="Special Notification"></asp:Label>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                       <%-- <asp:Label runat="server" ID="lblUserName" Text='<%# Eval("UserName") %>'></asp:Label>--%>
                                                       <%--  <asp:Label runat="server" ID="lblUserName" Text="Sample Email"></asp:Label>--%>
                                                        <table>
                                                            <tr>
                                                                 <td align="left" style="min-width:300px; padding:2px 2px 2px 2px">
                                                                      <asp:HyperLink ID="hlViewAddEmail" runat="server" Text='<%# Eval("NotificationHeader") %>'>
                                                             </asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        
                                                    </ItemTemplate>
                                                   
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td align="center" style="padding:2px 2px 2px 2px">
                                                                    <strong>Conditions</strong>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                   
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td align="left" style="min-width:100px; padding:2px 2px 2px 2px">
                                                                     <asp:HyperLink ID="hlSEndWhen" runat="server" Text="Send When...">
                                                             </asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                           
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td align="center">
                                                                    <strong>Recipients</strong>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>                                                   
                                                    <ItemTemplate>
                                                       <table>
                                                            <tr>
                                                                <td align="left" style="min-width:100px; padding:2px 2px 2px 2px">
                                                                     <asp:HyperLink ID="hlUsers" style="cursor: pointer;"   runat="server"  Text="12 Users...">
                                                             </asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                           
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                                                           



                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td align="center">
                                                                    <strong>Delivery</strong>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                   
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                               <td align="center" style="min-width:70px; padding:2px 2px 2px 2px">
                                                                     <asp:DropDownList runat="server" ID="ddlDeliveryOption" AutoPostBack="true" CssClass="NormalTextBox"
                                                            OnSelectedIndexChanged="UpdateNotificationDelivery">
                                                            <asp:ListItem Text="None" Value="none"></asp:ListItem>
                                                            <asp:ListItem Text="Email" Value="email" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="SMS" Value="sms"></asp:ListItem>
                                                            <asp:ListItem Text="Both" Value="both"></asp:ListItem>
                                                        </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                       
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%--New plan: Removed this per Jon--%>
                                                <%-- <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td align="center">
                                                                    <strong>Frequency</strong>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>                                                   
                                                    <ItemTemplate>
                                                       <table>
                                                            <tr>
                                                                <td align="left" style="min-width:100px; padding:2px 2px 2px 2px">
                                                                     <asp:HyperLink ID="hlFrequency" style="cursor: pointer;"   runat="server"  Text="Frequency...">
                                                             </asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                           
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                                                   

                                            </Columns>
                                            <HeaderStyle CssClass="gridviewborder_header" />
                                            <RowStyle CssClass="gridviewborder_row" />
                                        </asp:GridView>
                                                    <br />
                                                    <br />

                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>

                                         <%--Red 11072019: Ticket 4589--%> 
                                        <div runat="server" id="divSpecialNotificationAdd" style="display:none" >
                                            
                                            <asp:LinkButton runat="server" ID="lnkSaveSpecialNotification" ClientIDMode="Static" CssClass="btn" CausesValidation="False"
                                                    OnClick="lnkSaveSpecialNotification_Click"> <strong>Add</strong>   </asp:LinkButton>
                                        
                                        </div>  
                                                   
                                       
                                        <%-- End Red --%> 
                                        <div>
                                            <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
                                        </div>
                 
                <%--End Red--%>
            </div>
        </ContentTemplate>
            <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grdSpecialNotification"/>
            <asp:AsyncPostBackTrigger ControlID="lnkSaveSpecialNotification" EventName="Click"/>
            </Triggers>
       
    </asp:UpdatePanel>










                        
</ContentTemplate>




















                    




















                    </ajaxToolkit:TabPanel>


                    <%--  <ajaxToolkit:TabPanel ID="tabAttachMents" runat="server">
                        <HeaderTemplate>
                            <strong>Attachments</strong>
                        </HeaderTemplate>
                        <ContentTemplate>

                             <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                            <div style="padding:10px;">
                            <table>
                                <tr>
                                    <td valign="top">
                                        <table>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:CheckBox runat="server" ID="chkAttachOutgoingEmails" Text="Attach Outgoing Emails"
                                                        TextAlign="Right" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    Save to Table
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlOutSaveToTable" CssClass="NormalTextBox"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlOutSaveToTable_SelectedIndexChanged">
                                                            <asp:ListItem Text="--Select Table--" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    Save Recipient
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlOutSaveRecipient" CssClass="NormalTextBox">
                                                        <asp:ListItem Text="--None--" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    Save Subject to
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlOutSaveSubjectto" CssClass="NormalTextBox">
                                                         <asp:ListItem Text="--None--" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    Save Body to
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlOutSaveBodyTo" CssClass="NormalTextBox">
                                                         <asp:ListItem Text="--None--" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                    </td>
                                    <td valign="top">
                                        <table>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:CheckBox runat="server" ID="chkAttachIncomingEmails" Text="Attach Incoming Emails"
                                                        TextAlign="Right" />
                                                </td>
                                            </tr>
                                           
                                            <tr>
                                                <td align="right">
                                                    Save to Table
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlInSaveToTable" CssClass="NormalTextBox"
                                                     AutoPostBack="true" OnSelectedIndexChanged="ddlInSaveToTable_SelectedIndexChanged">
                                                         <asp:ListItem Text="--Select Table--" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>


                                             <tr>
                                                <td align="right">
                                                    Save Sender
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlInSaveToSender" CssClass="NormalTextBox">
                                                        <asp:ListItem Text="--None--" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="right">
                                                    Save Subject to
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlInSaveSubJectTo" CssClass="NormalTextBox">
                                                         <asp:ListItem Text="--None--" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    Save Body to
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlInSaveBodyTo" CssClass="NormalTextBox">
                                                         <asp:ListItem Text="--None--" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    Save Attachment to
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlInSaveAttachmentTo" CssClass="NormalTextBox">
                                                         <asp:ListItem Text="--None--" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>

                                             <tr>
                                                <td align="right">
                                                    Identifier
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlInIdentifier" CssClass="NormalTextBox">
                                                         <asp:ListItem Text="--None--" Value=""></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    Note: Identifier must be between two hash <br />
                                                     characters like this #12345#
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                    <td valign="top">

                                     <asp:LinkButton runat="server" ID="lnkAttachementSave" OnClick="lnkAttachementSave_Click" CausesValidation="true">
                                                    <asp:Image runat="server" ID="Image4" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                        ToolTip="Save" />
                                                </asp:LinkButton>

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <div style="border: 1px solid; padding:10px 10px 10px 10px;">
                                            <table>
                                                <tr>
                                                    <td align="right">
                                                        Email
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtAttacmentEmail" Width="200px" CssClass="NormalTextBox"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td align="right">
                                                        Port
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtAttacmentPort" CssClass="NormalTextBox"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        User Name
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtAttacmentUserName" Width="200px" CssClass="NormalTextBox"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td align="right">
                                                        Server
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtAttacmentServer" CssClass="NormalTextBox"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        Password
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtAttacmentPassword" Width="200px"  
                                                           CssClass="NormalTextBox"  MaxLength="30"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td align="right">
                                                        SSL
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList runat="server" ID="optAttachmentSSL" RepeatDirection="Horizontal">
                                                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="No" Value="0" Selected="True"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            </div>
                                 </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>--%>


                    <ajaxToolkit:TabPanel ID="tabForms" runat="server" Visible="false">
                        <HeaderTemplate>
                            <strong>Forms</strong>

                        </HeaderTemplate>




















                        <ContentTemplate>

                            <asp:UpdatePanel ID="upForms" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="padding-left: 20px; padding-top: 10px;">
                                        <asp:GridView ID="grdFormSet" runat="server" AutoGenerateColumns="False" DataKeyNames="FormSetID"
                                            HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" CssClass="gridview"
                                            OnRowCommand="grdFormSet_RowCommand" OnRowDataBound="grdFormSet_RowDataBound"
                                            AlternatingRowStyle-BackColor="#DCF2F0">
                                            <RowStyle CssClass="gridview_row" />
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label5" runat="server" Text='<%# Eval("FormSetID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/App_Themes/Default/Images/icon_delete.gif"
                                                            CommandName="deletetype" CommandArgument='<%# Eval("FormSetID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" CssClass="popuplink" NavigateUrl="~/Pages/Record/ChildTableDetail.aspx"
                                                            ImageUrl="~/App_Themes/Default/Images/iconEdit.png" ToolTip="Edit" ID="HyperLink3"></asp:HyperLink>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>
                                                        <asp:HyperLink runat="server" CssClass="popuplink" NavigateUrl="~/Pages/Record/ChildTableDetail.aspx"
                                                            ImageUrl="~/Pages/Pager/Images/add.png" ToolTip="Add Child Table" ID="HyperLink4"></asp:HyperLink>
                                                    </HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-CssClass="sortHandle3">
                                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image8" runat="server" ImageUrl="~/App_Themes/Default/Images/MoveIcon.png"
                                                            ToolTip="Drag and drop to change order" />
                                                        <input type="hidden" id='hfFormSetID' value='<%# Eval("FormSetID") %>' class='FormSetID' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Form">
                                                    <ItemTemplate>
                                                        <div style="padding-left: 10px;">
                                                            <asp:Label runat="server" ID="lblForm" Text='<%# Eval("FormSetName")%>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                            <HeaderStyle CssClass="gridview_header" />
                                        </asp:GridView>
                                    </div>
                                    <br />
                                    <div runat="server" id="divEmptyDataFormSet" visible="false" style="padding-left: 20px;">
                                        <asp:HyperLink runat="server" ID="hlAddFormSet" Style="text-decoration: none; color: Black;"
                                            CssClass="popuplink">
                                            <asp:Image runat="server" ID="Image9" ImageUrl="~/App_Themes/Default/images/BigAdd.png" />
                                            No forms have been added yet. <strong style="text-decoration: underline; color: Blue;">
                                                Add new form now.</strong>
                                        </asp:HyperLink>
                                    </div>
                                    <br />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="grdFormSet" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </ContentTemplate>




















                    </ajaxToolkit:TabPanel>


                    <%--<ajaxToolkit:TabPanel ID="tabView" runat="server">
                        <HeaderTemplate>
                            <strong>Views</strong>
                        </HeaderTemplate>
                        <ContentTemplate>

                                     <dbg:OneView runat="server" ID="vdOne" />


                          </ContentTemplate>
                    </ajaxToolkit:TabPanel>--%>
                </ajaxToolkit:TabContainer>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td height="20px"></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <table>
                            <tr>
                                <td align="left">
                                    <asp:LinkButton runat="server" ID="lnkShowHistory" ClientIDMode="Static" Text="Show Change History"
                                        OnClick="lnkShowHistory_Click" CausesValidation="false"></asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="lnkHideHistory" ClientIDMode="Static" Text="Hide Change History"
                                        CausesValidation="false" OnClick="lnkHideHistory_Click" Visible="false"></asp:LinkButton>
                                </td>
                                <td align="right">
                                    <asp:Label runat="server" ID="lblTableID" Style="color: #C0C0C0"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <div id="divHistory" runat="server" visible="false" style="width: 1000px">
                            <div runat="server" id="div3">
                                <strong>Change History</strong>
                                <br />
                                <dbg:dbgGridView ID="gvChangedLog" runat="server" GridLines="Both" CssClass="gridview"
                                    HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" AllowPaging="True"
                                    AllowSorting="false" DataKeyNames="DateAdded" HeaderStyle-ForeColor="Black" Width="100%"
                                    AutoGenerateColumns="false" PageSize="15" OnPreRender="gvChangedLog_PreRender"
                                    OnRowDataBound="gvChangedLog_RowDataBound">
                                    <PagerSettings Position="Top" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemStyle Width="10px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:HyperLink runat="server" ID="hlView" CssClass="popuplink">
                                                    <asp:Image runat="server" ID="imgView" ImageUrl="~/App_Themes/Default/Images/iconShow.png" />
                                                </asp:HyperLink>
                                                <%--<asp:ImageButton ID="btnView" runat="server" ToolTip="View" 
                                                                        ImageUrl="~/App_Themes/Default/Images/iconShow.png"  />--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="true" HeaderText="Updated Date" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="UpdateDate" runat="server" Text='<%# Eval("DateAdded") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="true" HeaderText="Table Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTableName" runat="server" Text='<%# Eval("TableName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="true" HeaderText="User">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUser" runat="server" Text='<%# Eval("User") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField Visible="true" HeaderText="Changed Field List">
                                            <ItemTemplate>
                                                <asp:Label ID="lblColumnList" runat="server" Text='<%# Eval("ColumnList") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="gridview_header" />
                                    <RowStyle CssClass="gridview_row" />
                                    <PagerTemplate>
                                        <asp:GridViewPager runat="server" ID="CL_Pager" HideFilter="true" HideAdd="true"
                                            HideDelete="true" OnBindTheGridToExport="CL_Pager_BindTheGridToExport" OnApplyFilter="CL_Pager_OnApplyFilter"
                                            OnBindTheGridAgain="CL_Pager_BindTheGridAgain" OnExportForCSV="CL_Pager_OnExportForCSV" />
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        <div style="padding-left: 100px;">
                                            No changes have been made yet.
                                        </div>
                                    </EmptyDataTemplate>
                                </dbg:dbgGridView>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="gvChangedLog" />
                        <asp:AsyncPostBackTrigger ControlID="lnkShowHistory" />
                        <asp:AsyncPostBackTrigger ControlID="lnkHideHistory" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <asp:Button runat="server" ID="btnRefreshGrid" ClientIDMode="Static" Style="display: none;"
        OnClick="btnRefreshGrid_Click" />
    <asp:Button runat="server" ID="btnRefreshColumns" ClientIDMode="Static" Style="display: none;"
        OnClick="btnRefreshColumns_Click" />

    <asp:Button runat="server" ID="btnRefreshForms" ClientIDMode="Static" Style="display: none;"
        OnClick="btnRefreshForms_Click" />
    <asp:Button runat="server" ID="btnRefreshTemplates" ClientIDMode="Static" Style="display: none;"
        OnClick="btnRefreshTemplates_Click" />
    <asp:Button runat="server" ID="btnOrderSC" ClientIDMode="Static" Style="display: none;"
        OnClick="btnOrderSC_Click" />
    <asp:Button runat="server" ID="btnOrderTC" ClientIDMode="Static" Style="display: none;"
        OnClick="btnOrderTC_Click" />

    <asp:Button runat="server" ID="btnOrderFS" ClientIDMode="Static" Style="display: none;"
        OnClick="btnOrderFS_Click" />


    <asp:Button runat="server" ID="btnTableRenameOK" ClientIDMode="Static" Style="display: none;" OnClick="btnTableRenameOK_Click" />
    <asp:Button runat="server" ID="btnTableRenameNo" ClientIDMode="Static" Style="display: none;" OnClick="btnTableRenameNo_Click" />
    <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfWebroot" />
    <br />
    <asp:Label runat="server" ID="lblDeleteAll" />
    <ajaxToolkit:ModalPopupExtender ID="mpeDeleteAll" runat="server" TargetControlID="lblDeleteAll"
        PopupControlID="pnlDeleteAll" BackgroundCssClass="modalBackground" />
    <asp:Panel ID="pnlDeleteAll" runat="server" Style="display: none">
        <div style="border-width: 5px; background-color: #ffffff; border-color: #4F8FDD; height: 220px; border-style: outset;">
            <div style="padding-top: 50px; padding: 20px;">
                <table>
                    <tr>
                        <td colspan="2">
                            <asp:Label runat="server" ID="lblDeleteRestoreMessage" Font-Bold="true" Text="Are you sure you want to delete this table?"></asp:Label>
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table>
                                <tr>
                                    <td style="width: 100px;"></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px;">&nbsp;</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="width: 100px;">&nbsp;</td>
                                    <td>
                                        <asp:LinkButton ID="lnkDeleteAllOK" runat="server" CausesValidation="false" CssClass="btn" OnClick="lnkDeleteAllOK_Click"> <strong>OK</strong></asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkDeleteAllNo" runat="server" CausesValidation="false" CssClass="btn"> <strong>Cancel</strong></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr runat="server" id="trDeleteParmanent">
                        <td colspan="2">
                            <asp:CheckBox runat="server" Visible="false" ID="chkDeleteParmanent" TextAlign="Right" Text="I wish to delete this table permanently." />
                        </td>
                    </tr>
                    <tr runat="server" id="trUndo">
                        <td colspan="2">
                            <asp:CheckBox runat="server" Visible="false" ID="chkUndo" TextAlign="Right" Text="I will not be able to undo this action." />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="color: #FF0000"><%--<asp:UpdateProgress class="ajax-indicator" ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <table style="width: 50px; text-align: center">
                                        <tr>
                                            <td>
                                                <img alt="Processing..." src="../../Images/ajax.gif" />
                                            </td>
                                        </tr>
                                    </table>
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>Note: You can restore this table later if you view </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="color: #FF0000">deleted tables.</td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <br />

</asp:Content>

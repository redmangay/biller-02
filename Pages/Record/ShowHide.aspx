﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true"
    CodeFile="ShowHide.aspx.cs" Inherits="Pages_Record_ShowHide" %>
<%@ Register Src="~/Pages/UserControl/ConditionsCondition.ascx" TagName="SWCon" TagPrefix="dbg" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

<script language="javascript" type="text/javascript">

    function GetBackValueEdit() {
        var p_Checkbox = window.parent.document.getElementById('<%=ParentCheckboxName()%>');
        //alert(p_Checkbox);
        if (p_Checkbox != null){
            p_Checkbox.checked = true;
        }

        //window.parent.document.getElementById('ctl00_HomeContentPlaceHolder_hlCalculationEdit').href = '../Help/CalculationTest.aspx?type=calculation&formula=' + encodeURIComponent(document.getElementById('txtFormula').value) + "&Tableid=" + document.getElementById("ctl00_HomeContentPlaceHolder_hfTableID").value + "&Columnid=" + document.getElementById("ctl00_HomeContentPlaceHolder_hfColumnID").value;
        parent.$.fancybox.close();

    }

    function GetBackValueAdd() {
        //window.parent.document.getElementById('<%=ParentCheckboxName()%>').checked = true;
        var p_Checkbox = window.parent.document.getElementById('<%=ParentCheckboxName()%>');
        if (p_Checkbox != null) {
            p_Checkbox.checked = true;
        }       
       
        parent.$.fancybox.close();
            
    }

    //function PutAddValue() {
       

    //    if (document.getElementById('ddlHideColumn').value != '') {
    //        $("#btnPopulateAdd").trigger("click");
    //    }
    //}

    </script>
    <div style="text-align: center;">
        <div style="padding-top: 10px; min-height: 400px; display: inline-block;">
            <asp:UpdatePanel ID="upConditions" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td align="left" colspan="4">
                                <table>
                                    <td align="left">
                                        <h3 runat="server" id="h3Title">Show When</h3>
                                    </td>
                                    <td align="left" style="padding-left: 400px;">
                                        <table>
                                            <tr>

                                                <td>
                                                    <asp:LinkButton runat="server" ID="lnkBack" OnClientClick="javascript:parent.$.fancybox.close(); return false;"
                                                        CssClass="btn" CausesValidation="false" Visible="false"> <strong>Cancel</strong></asp:LinkButton>
                                                    <%--<asp:Button runat="server" ID="btnPopulateAdd" ClientIDMode="Static"  style="display:none;"/>--%>
                                                </td>
                                                <td>
                                                    <asp:LinkButton runat="server" ID="lnkSave" OnClick="lnkSave_Click"
                                                        CausesValidation="true"> 
                                                        <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                                   ToolTip="Save" />
                                                    </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="left"></td>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">

                                <div style="padding-left: 50px; padding-top: 10px;">
                                    <asp:GridView ID="grdConditions" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                                        CssClass="gridview" OnRowCommand="grdConditions_RowCommand" OnRowDataBound="grdConditions_RowDataBound"
                                        ShowHeaderWhenEmpty="true"
                                        ShowFooter="true">
                                        <Columns>
                                            <asp:TemplateField Visible="false">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblID" Text='<%# Eval("ID") %>'></asp:Label>
                                                    <asp:Label runat="server" ID="lblConditionsID" Text='<%# Eval("ConditionsID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnMinus" runat="server" ImageUrl="~/App_Themes/Default/Images/Minus.png" CausesValidation="false"
                                                        CommandName="minus" CommandArgument='<%# Eval("ID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderTemplate>
                                                    <strong></strong>
                                                </HeaderTemplate>

                                                <ItemStyle HorizontalAlign="Left" />
                                                <ItemTemplate>

                                                    <dbg:SWCon runat="server" ID="swcConditions" OnddlHideColumn_Changed="SWCHideColumnChanged" />

                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField>
                                                <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgbtnPlus" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png"
                                                        CommandName="plus" Visible="false" CommandArgument='<%# Eval("ID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                        <HeaderStyle CssClass="gridview_header" Height="25px" />
                                        <RowStyle CssClass="gridview_row_NoPadding" />
                                    </asp:GridView>

                                    <div>
                                        <asp:Label ID="lblMsgTab" runat="server" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>

                            </td>
                        </tr>

                    </table>

                </ContentTemplate>
                <Triggers>
                    <%--<asp:AsyncPostBackTrigger ControlID="grdConditions" />--%>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

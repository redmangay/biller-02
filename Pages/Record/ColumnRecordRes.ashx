﻿<%@ WebHandler Language="C#" Class="ColumnRecordRes"  %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using DocGen.DAL;
using System.Text;
using System.Web.SessionState;
public class ColumnRecordRes : IHttpHandler, IRequiresSessionState
{

    public void ProcessRequest (HttpContext context) {
        bool bSuccess = false;

        if (context.Request.QueryString["sql"]!=null)
        {
            context.Response.Write(Common.GetValueFromSQL(context.Request.QueryString["sql"].ToString()));
            return;
        }
        if (context.Request.QueryString["DocumentSectionID"] != null)
        {
            CalendarSectionDetail calDetail = null;
            using (DocGen.DAL.DocGenDataContext ctx = new DocGen.DAL.DocGenDataContext())
            {
                DocGen.DAL.DocumentSection section = ctx.DocumentSections.SingleOrDefault<DocGen.DAL.DocumentSection>(s => s.DocumentSectionID == int.Parse(context.Request.QueryString["DocumentSectionID"].ToString()));
                if (section != null)
                {
                    calDetail = JSONField.GetTypedObject<CalendarSectionDetail>(section.Details);
                }
            }
            string hostcellid = context.Request.QueryString["hostcellid"].ToString();
            string recordid = context.Request.QueryString["recordid"].ToString();
            string namefieldvalue = context.Request.QueryString["namefieldvalue"].ToString();
            //string endtime = context.Request.QueryString["endtime"].ToString();
            //string starttime = context.Request.QueryString["starttime"].ToString();
            string divContentid = context.Request.QueryString["divContentid"].ToString();
            string hostnamefieldvalue = context.Request.QueryString["hostnamefieldvalue"].ToString();
            string hoststarttime = context.Request.QueryString["hoststarttime"].ToString();

            Record theRecord = RecordManager.ets_Record_Detail_Full(int.Parse(recordid),null,false);

            Column theStartColumn = null;
            if (calDetail.DateFieldColumnID != null)
            {
                theStartColumn = RecordManager.ets_Column_Details((int)calDetail.DateFieldColumnID);
            }
            Column theEndColumn = null;
            if (calDetail.EndDateFieldColumnID != null)
            {
                theEndColumn = RecordManager.ets_Column_Details((int)calDetail.EndDateFieldColumnID);
            }
            string starttime = RecordManager.GetRecordValue(ref theRecord, theStartColumn.SystemName);
            string endtime = RecordManager.GetRecordValue(ref theRecord, theEndColumn.SystemName);
            DateTime dtStartTime = DateTime.Parse(starttime);
            DateTime dtEndTime = DateTime.Parse(endtime);
            TimeSpan tsEventDuration = dtEndTime - dtStartTime;


            DateTime dtHostStartTime=DateTime.Parse(hoststarttime);
            DateTime dtHostEndTime=dtHostStartTime+tsEventDuration;

            RecordManager.MakeTheRecord(ref theRecord, theStartColumn.SystemName, dtHostStartTime.ToString("dd/MM/yyyy HH:mm:ss"));

            RecordManager.MakeTheRecord(ref theRecord, theEndColumn.SystemName, dtHostEndTime.ToString("dd/MM/yyyy HH:mm:ss"));

            Column theNameField = null;
            if (calDetail.NameFieldColumnID != null)
            {
                theNameField = RecordManager.ets_Column_Details((int)calDetail.NameFieldColumnID);
            }
            if(namefieldvalue!=hostnamefieldvalue)
            {
                RecordManager.MakeTheRecord(ref theRecord, theNameField.SystemName, hostnamefieldvalue);
            }

            RecordManager.ets_Record_Update(theRecord, true);
            context.Response.Write("True");
            return;
        }
        if (context.Request.QueryString["Coordinate"] != null &&
            context.Request.QueryString["ColumnID"] != null)
        {
            string strCoordinateControl = context.Request.QueryString["Coordinate"].ToString();
            string strColumnIDControl = context.Request.QueryString["ColumnID"].ToString();
            string strColumnID = strColumnIDControl.Replace("span_", "");
            strCoordinateControl=strCoordinateControl.Replace("td_","");
            string strX = strCoordinateControl.Substring(0, strCoordinateControl.IndexOf("_"));
            string strY = strCoordinateControl.Replace(strX + "_", "");
            Common.ExecuteText("UPDATE [Column] SET Xcoordinate=" + strX + ",Ycoordinate=" + strY + " WHERE ColumnID=" + strColumnID);
            context.Response.Write("True");
            return;
        }


        if (context.Request.QueryString["DeviceWidth"] != null)
        {
            if(context.Session["DeviceWidth"] == null && int.Parse(context.Request.QueryString["DeviceWidth"].ToString()) < 768)
            {
                context.Session["DeviceWidth"] = context.Request.QueryString["DeviceWidth"].ToString();
                context.Response.Write("mobile");
            }else
            {
                context.Session["DeviceWidth"] = context.Request.QueryString["DeviceWidth"].ToString();
            }


            if (context.Request.QueryString["DeviceHeight"] != null)
            {
                if(context.Session["DeviceHeight"] == null )
                {
                    context.Session["DeviceHeight"] = context.Request.QueryString["DeviceHeight"].ToString();                    
                }
                else
                {
                    context.Session["DeviceHeight"] = context.Request.QueryString["DeviceHeight"].ToString();
                }
            }




            return;
        }


        context.Response.Write(bSuccess.ToString());
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

    //protected bool DeleteSection(string sectionId)
    //{

    //}


}
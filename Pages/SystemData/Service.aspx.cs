﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using DocGen.DAL;
public partial class Pages_SystemData_Service : SecurePage
{
    Common_Pager _gvPager;

    int _iSearchCriteriaID = -1;
    int _iStartIndex = 0;
    int _iMaxRows = 10;
    string _strGridViewSortColumn = "ServiceID";
    string _strGridViewSortDirection = "DESC";
    string _strExtraQS = "";
    bool _bEmptyRecord = false;
    protected void PopulateTerminology()
    {
        //stgTableCap.InnerText = stgTableCap.InnerText.Replace("Table", SecurityManager.etsTerminology(Request.Path.Substring(Request.Path.LastIndexOf("/") + 1), "Table", "Table"));

    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }

    //protected void PopulateTableDDL()
    //{
    //    int iTN = 0;
    //    ddlTable.DataSource = RecordManager.ets_Table_Select(null,
    //            null,
    //            null,
    //            int.Parse(Session["AccountID"].ToString()),
    //            null, null, true,
    //            "st.TableName", "ASC",
    //            null, null, ref  iTN, Session["STs"].ToString());

    //    ddlTable.DataBind();
    //    //if (iTN == 0)
    //    //{
    //    System.Web.UI.WebControls.ListItem liAll = new System.Web.UI.WebControls.ListItem("All", "");
    //    ddlTable.Items.Insert(0, liAll);
    //    System.Web.UI.WebControls.ListItem liO = new System.Web.UI.WebControls.ListItem("--Other Pages--", "");
    //    ddlTable.Items.Insert(1, liO);
    //    //}


    //}


    //protected void ddlTable_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    lnkSearch_Click(null, null);
    //}




    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "Services";

        try
        {


            User ObjUser = (User)Session["User"];

            if (!IsPostBack)
            {
                //PopulateTableDDL();


                if (Common.HaveAccess(Session["roletype"].ToString(), "1") || ObjUser.Email.IndexOf(Common.DBGEmailPart) > -1)
                {
                    //pass
                }
                else
                { Response.Redirect("~/Default.aspx", false); }

                if (Request.QueryString["SearchCriteria"] != null)
                {
                    PopulateSearchCriteria(int.Parse(Cryptography.Decrypt(Request.QueryString["SearchCriteria"].ToString())));
                }
                else
                {
                    if(!IsPostBack && Request.UrlReferrer!=null)
                    {
                        hlBack.NavigateUrl = Request.UrlReferrer.AbsoluteUri;
                    }
                }
              
                if (Request.QueryString["TableID"] != null)
                {
                    _strExtraQS = _strExtraQS + "&TableID=" + Request.QueryString["TableID"].ToString();
                    ViewState["PageSource"] = "TableID";
                    Table theTable = RecordManager.ets_Table_Details((int)qsTableID);
                    if(theTable!=null)
                    {
                        lblToptitle.Text = theTable.TableName + " Services";
                    }
                }
                else if (Request.QueryString["TableChildID"] != null)
                {
                    _strExtraQS = _strExtraQS + "&TableChildID=" + Request.QueryString["TableChildID"].ToString();
                    ViewState["PageSource"] = "TableChildID";
                    TableChild theTableChild = RecordManager.ets_TableChild_Detail((int)qsTableChildID);
                    Table theTable = RecordManager.ets_Table_Details((int)theTableChild.ChildTableID);
                    if (theTable != null)
                    {
                        lblToptitle.Text = theTable.TableName + " Tab Services";
                    }

                }
                else if (Request.QueryString["AccountID"] != null)
                {
                    _strExtraQS = _strExtraQS + "&AccountID=" + Request.QueryString["AccountID"].ToString();
                    ViewState["PageSource"] = "AccountID";
                    lblToptitle.Text = "Other Pages";
                }
                ViewState["_strExtraQS"] = _strExtraQS;
                if (Session["GridPageSize"] != null && Session["GridPageSize"].ToString() != "")
                { gvTheGrid.PageSize = 50; }

                if (Request.QueryString["SearchCriteria"] != null)
                {
                    gvTheGrid.PageSize = _iMaxRows;
                    gvTheGrid.GridViewSortColumn = _strGridViewSortColumn;
                    if (_strGridViewSortDirection.ToUpper() == "ASC")
                    {
                        gvTheGrid.GridViewSortDirection = SortDirection.Ascending;
                    }
                    else
                    {
                        gvTheGrid.GridViewSortDirection = SortDirection.Descending;
                    }
                    BindTheGrid(_iStartIndex, _iMaxRows);
                }
                else
                {
                    gvTheGrid.GridViewSortColumn = "ServiceID";
                    gvTheGrid.GridViewSortDirection = SortDirection.Descending;
                    BindTheGrid(0, gvTheGrid.PageSize);
                }
            }
            else
            {
            }

            GridViewRow gvr = gvTheGrid.TopPagerRow;
            if (gvr != null)
                _gvPager = (Common_Pager)gvr.FindControl("Pager");

            if (!IsPostBack)
            {
                PopulateTerminology();
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Service", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }




    protected void PopulateSearchCriteria(int iSearchCriteriaID)
    {
        try
        {
            SearchCriteria theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);


            if (theSearchCriteria != null)
            {

                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

                xmlDoc.Load(new StringReader(theSearchCriteria.SearchText));

                if(xmlDoc.FirstChild[hlBack.ID]!=null)
                    hlBack.NavigateUrl = xmlDoc.FirstChild[hlBack.ID].InnerText;

                //ddlTable.Text = xmlDoc.FirstChild[ddlTable.ID].InnerText;
                txtSearch.Text = xmlDoc.FirstChild[txtSearch.ID].InnerText;

                _iStartIndex = int.Parse(xmlDoc.FirstChild["iStartIndex"].InnerText);
                _iMaxRows = int.Parse(xmlDoc.FirstChild["iMaxRows"].InnerText);
                _strGridViewSortColumn = xmlDoc.FirstChild["GridViewSortColumn"].InnerText;
                _strGridViewSortDirection = xmlDoc.FirstChild["GridViewSortDirection"].InnerText;
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }


    }

    public int? qsAccountID
    {
        get
        {
            return Request.QueryString["AccountID"] == null ? null : (int?)int.Parse(Cryptography.Decrypt(Request.QueryString["AccountID"].ToString()));
        }
    }
    public int? qsTableID
    {
        get
        {
            return Request.QueryString["TableID"] == null ? null : (int?)int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
        }
    }

    public int? qsTableChildID
    {
        get
        {
            return Request.QueryString["TableChildID"] == null ? null : (int?)int.Parse(Cryptography.Decrypt(Request.QueryString["TableChildID"].ToString()));
        }
    }



    protected void BindTheGrid(int iStartIndex, int iMaxRows)
    {




        lblMsg.Text = "";

        //SearchCriteria 
        try
        {
            string xml = null;
            xml = @"<root>" +
                //" <" + ddlTable.ID + ">" + HttpUtility.HtmlEncode(ddlTable.Text) + "</" + ddlTable.ID + ">" +
                " <" + hlBack.ID + ">" + HttpUtility.HtmlEncode(hlBack.NavigateUrl) + "</" + hlBack.ID + ">" +
                    " <" + txtSearch.ID + ">" + HttpUtility.HtmlEncode(txtSearch.Text) + "</" + txtSearch.ID + ">" +
                   " <GridViewSortColumn>" + HttpUtility.HtmlEncode(gvTheGrid.GridViewSortColumn) + "</GridViewSortColumn>" +
                   " <GridViewSortDirection>" + HttpUtility.HtmlEncode(gvTheGrid.GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC") + "</GridViewSortDirection>" +
                   " <iStartIndex>" + HttpUtility.HtmlEncode(iStartIndex.ToString()) + "</iStartIndex>" +
                   " <iMaxRows>" + HttpUtility.HtmlEncode(iMaxRows.ToString()) + "</iMaxRows>" +
                  "</root>";

            SearchCriteria theSearchCriteria = new SearchCriteria(null, xml);
            _iSearchCriteriaID = SystemData.SearchCriteria_Insert(theSearchCriteria);
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }

        //End Searchcriteria





        try
        {
            int iTN = 0;

            //gvTheGrid.Columns[6].HeaderText = gvTheGrid.Columns[6].HeaderText.Replace("Table", SecurityManager.etsTerminology(Request.Path.Substring(Request.Path.LastIndexOf("/") + 1), "Table", "Table"));
            ViewState[gvTheGrid.ID + "PageIndex"] = (iStartIndex / gvTheGrid.PageSize) + 1;
            Service sService = new Service(null, "",
                qsTableID,
                 qsTableChildID,
                  qsAccountID,
                  null, txtSearch.Text,null
                );
            DataTable dtTemp = SystemData.Service_Select(sService, gvTheGrid.GridViewSortColumn, gvTheGrid.GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC",
                   iStartIndex, iMaxRows, ref iTN);

            if (dtTemp != null && dtTemp.Rows.Count == 0)
            {
                dtTemp.Rows.Add(dtTemp.NewRow());
                _bEmptyRecord = true;
            }
            gvTheGrid.DataSource = dtTemp;
            gvTheGrid.VirtualItemCount = iTN;
            gvTheGrid.DataBind();
            if (gvTheGrid.TopPagerRow != null)
                gvTheGrid.TopPagerRow.Visible = true;

            GridViewRow gvr = gvTheGrid.TopPagerRow;
            if (gvr != null)
            {
                _gvPager = (Common_Pager)gvr.FindControl("Pager");
                _gvPager.AddURL = GetAddURL();
                //_gvPager.PageIndexTextSet = (int)(iStartIndex / iMaxRows + 1);
                if (ViewState[gvTheGrid.ID + "PageIndex"] != null)
                    _gvPager.PageIndex = int.Parse(ViewState[gvTheGrid.ID + "PageIndex"].ToString());
                _gvPager.PageSize = gvTheGrid.PageSize;
                _gvPager.TotalRows = iTN;
            }






        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Service", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }


    protected void gvTheGrid_Sorting(object sender, GridViewSortEventArgs e)
    {

        BindTheGrid(0, gvTheGrid.PageSize);
    }

    //protected void btnSearch_Click(object sender, ImageClickEventArgs e)

    protected void lnkSearch_Click(object sender, EventArgs e)
    {

        BindTheGrid(0, gvTheGrid.PageSize);

    }

    protected void gvTheGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
            e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");
            
            //Label lblTable = (Label)e.Row.FindControl("lblTable");
            //lblTable.Text = Common.GetValueFromSQL("SELECT TableName FROM [Table] WHERE TableID=" + (DataBinder.Eval(e.Row.DataItem, "TableID").ToString()));

            if(_bEmptyRecord)
            {
                e.Row.Visible = false;
                return;
            }
            DataRowView rowView = (DataRowView)e.Row.DataItem;
            Service theService =SystemData.Service_Detail(int.Parse( rowView["ServiceID"].ToString()));



            if(theService!=null)
            {
                Label lblServiceType = (Label)e.Row.FindControl("lblServiceType");
                if(lblServiceType!=null && string.IsNullOrEmpty( theService.ServiceType)==false)
                {
                    lblServiceType.Text = PageByServiceType(theService.ServiceType);
                }

                if (!string.IsNullOrEmpty(theService.ServiceJSON))
                {
                    PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);
                    if (thePageLifeCycleService != null)
                    {

                        Label lblEvent = (Label)e.Row.FindControl("lblEvent");
                        Label lblDotNet = (Label)e.Row.FindControl("lblDotNet");
                        Label lblSP = (Label)e.Row.FindControl("lblSP");
                        Label lblNotes = (Label)e.Row.FindControl("lblNotes");

                        if (!string.IsNullOrEmpty(thePageLifeCycleService.ServerControlEvent))
                        {
                            if (thePageLifeCycleService.ServerControlEvent.IndexOf("_Start")>0)
                            {
                                //string strSE = thePageLifeCycleService.ServerControlEvent.Replace("ASP_", "Start of ");
                                //lblEvent.Text = strSE.Replace("_Start", "");
                                lblEvent.Text = thePageLifeCycleService.ServerControlEvent;
                            }
                            else
                            {
                                lblEvent.Text = thePageLifeCycleService.ServerControlEvent.Replace("ASP_", "End of ");

                            }
                              
                        }
                        if (!string.IsNullOrEmpty(thePageLifeCycleService.SPName))
                            lblSP.Text = thePageLifeCycleService.SPName;
                        //RP Modified Ticket 2004
                        if (!string.IsNullOrEmpty(theService.ProcessNotes))
                            lblNotes.Text = theService.ProcessNotes;
                        //End Modification
                        if (!string.IsNullOrEmpty(thePageLifeCycleService.DotNetMethod))
                            lblDotNet.Text = thePageLifeCycleService.DotNetMethod;



                    }
                }
            }
        }
    }

    protected string PageByServiceType(string strServiceType)
    {
        switch (strServiceType)
        {
            case "RecordList_P":
                return "Record List";
            case "RecordDetail_P":
                return "Record Detail";
            case "RecordList_C":
                return "Child Record List";
            case "RecordDetail_C":
                return "Child Record Detail";
            case "Default_aspx":
                return "Dashboard";
            case "EachMap_aspx":
                return "Dashboard Map";
            
            default:
                return strServiceType;
            //break; EachMap_aspx
        }
    }
    protected void gvTheGrid_PreRender(object sender, EventArgs e)
    {
        GridView grid = (GridView)sender;
        if (grid != null)
        {
            GridViewRow pagerRow = (GridViewRow)grid.TopPagerRow;
            if (pagerRow != null)
            {
                pagerRow.Visible = true;
            }
        }
    }

    public string GetEditURL()
    {

        return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/SystemData/ServiceDetail.aspx?mode=" + Cryptography.Encrypt("edit") + (ViewState["_strExtraQS"] == null ? "" : ViewState["_strExtraQS"].ToString()) + "&SearchCriteria=" + Cryptography.Encrypt(_iSearchCriteriaID.ToString()) + "&ServiceID=";

    }


    public string GetAddURL()
    {

        return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/SystemData/ServiceDetail.aspx?mode=" + Cryptography.Encrypt("add") + (ViewState["_strExtraQS"] == null ? "" : ViewState["_strExtraQS"].ToString()) + "&SearchCriteria=" + Cryptography.Encrypt(_iSearchCriteriaID.ToString());

    }


   
    protected void Pager_BindTheGridToExport(object sender, EventArgs e)
    {
        _gvPager.ExportFileName = "Services";
        BindTheGrid(0, _gvPager.TotalRows);
    }

    protected void Pager_BindTheGridAgain(object sender, EventArgs e)
    {
        BindTheGrid(_gvPager.StartIndex, _gvPager.PageSize);
    }

    protected void Pager_OnApplyFilter(object sender, EventArgs e)
    {
        //txtOptionKeySearch.Text = "";
        //ddlTable.Text = "";
        gvTheGrid.GridViewSortColumn = "ServiceID";
        gvTheGrid.GridViewSortDirection = SortDirection.Descending;
        lnkSearch_Click(null, null);
        //BindTheGrid(0, gvTheGrid.PageSize);
    }


    protected void Pager_DeleteAction(object sender, EventArgs e)
    {
        string sCheck = "";
        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
        {
            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
            if (ischeck)
            {
                sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
            }
        }
        if (string.IsNullOrEmpty(sCheck))
        {
            ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select a record.');", true);
        }
        else
        {
            DeleteItem(sCheck);
            BindTheGrid(_gvPager.StartIndex, gvTheGrid.PageSize);
        }

    }



    private void DeleteItem(string keys)
    {
        try
        {
            if (!string.IsNullOrEmpty(keys))
            {
                foreach (string sTemp in keys.Split(','))
                {
                    if (!string.IsNullOrEmpty(sTemp))
                    {
                        SystemData.Service_Delete(int.Parse(sTemp));
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Service", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }



}

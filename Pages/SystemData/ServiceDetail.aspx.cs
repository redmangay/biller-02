﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using DocGen.DAL;
public partial class Pages_SystemData_ServiceDetail : SecurePage
{

    string _strActionMode = "view";
    int? _iServiceID;
    string _qsMode = "";
    string _qsServiceID = "";
    string _strExtraQS = "";
    protected void PopulateTheRecord()
    {
        try
        {

            Service theService = SystemData.Service_Detail((int)_iServiceID);

            if (!string.IsNullOrEmpty(theService.ServiceType) && ddlServiceType.Items.FindByValue(theService.ServiceType) != null)
                ddlServiceType.SelectedValue = theService.ServiceType;

            if(!string.IsNullOrEmpty( theService.ServiceJSON))
            {
                PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);
                if (thePageLifeCycleService != null)
                {

                   

                    if (!string.IsNullOrEmpty(thePageLifeCycleService.ServerControlEvent))
                    {
                        if (ddlServerControlEvent.Items.FindByValue(thePageLifeCycleService.ServerControlEvent) != null)
                            ddlServerControlEvent.SelectedValue = thePageLifeCycleService.ServerControlEvent;
                    }
                    if (!string.IsNullOrEmpty(thePageLifeCycleService.SPName))
                        txtSPName.Text = thePageLifeCycleService.SPName;

                    if (!string.IsNullOrEmpty(thePageLifeCycleService.DotNetMethod))
                        txtDotNetMethod.Text = thePageLifeCycleService.DotNetMethod;

                   
                    if (!string.IsNullOrEmpty(thePageLifeCycleService.JavaScriptFunction))
                        txtJavaScriptFunction.Text = thePageLifeCycleService.JavaScriptFunction;

                    //red service/custome 12112017
                    if (!IsPostBack)
                    {
                        PopulateControl(thePageLifeCycleService.ServerControlEvent.ToString());
                        if (!string.IsNullOrEmpty(theService.ColumnSystemName))
                        {
                            ddlControlField.SelectedValue = theService.ColumnSystemName;
                        }

                    }


                }
            }

            if (!string.IsNullOrEmpty(theService.ProcessNotes))
                txtNotes.Text = theService.ProcessNotes;
            if (!string.IsNullOrEmpty(theService.ServiceDefinition))
                txtServiceDefinition.Text = theService.ServiceDefinition;
            if (theService.SameEventOrder!=null)
                txtSameEventOrder.Text = theService.SameEventOrder.ToString();
          
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Service Detail", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }

    }
    
    

    protected void Page_Load(object sender, EventArgs e)
    {
        //int iTemp = 0;
        trControl.Visible = false; //red service/custom
        User ObjUser = (User)Session["User"];
        if (!IsPostBack)
        {
           
            if (Common.HaveAccess(Session["roletype"].ToString(), "1") || ObjUser.Email.IndexOf(Common.DBGEmailPart) > -1)
            {
                //pass
            }
            else
            { Response.Redirect("~/Default.aspx", false); }


            _strExtraQS = "?SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString();

            if (Request.QueryString["TableID"] != null)
            {
                _strExtraQS = _strExtraQS + "&TableID=" + Request.QueryString["TableID"].ToString();
                ViewState["PageSource"] = "TableID";
                ViewState["ActiveTableID"] = Cryptography.Decrypt(Request.QueryString["TableID"].ToString());
            }
            else if (Request.QueryString["TableChildID"] != null)
            {
                _strExtraQS = _strExtraQS + "&TableChildID=" + Request.QueryString["TableChildID"].ToString();
                ViewState["PageSource"] = "TableChildID";
                TableChild theTableChild = RecordManager.ets_TableChild_Detail((int)qsTableChildID);
                ViewState["theTableChild"] = theTableChild;
                ViewState["ActiveTableID"] = theTableChild.ChildTableID;
            }
            else if (Request.QueryString["AccountID"] != null)
            {
                _strExtraQS = _strExtraQS + "&AccountID=" + Request.QueryString["AccountID"].ToString();
                ViewState["PageSource"] = "AccountID";
            }
            
            hlBack.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/SystemData/Service.aspx" +_strExtraQS;
            PopulatePageType();
        }



        if (Request.QueryString["mode"] == null)
        {
            Server.Transfer("~/Default.aspx");
        }
        else
        {
            _qsMode = Cryptography.Decrypt(Request.QueryString["mode"]);

            if (_qsMode == "add" ||
                _qsMode == "view" ||
                _qsMode == "edit")
            {
                _strActionMode = _qsMode;


                if (Request.QueryString["ServiceID"] != null)
                {

                    _qsServiceID = Cryptography.Decrypt(Request.QueryString["ServiceID"]);

                    _iServiceID = int.Parse(_qsServiceID);
                }

            }
            else
            {
                Server.Transfer("~/Default.aspx");
            }


        }
       
        


        // checking permission


        switch (_strActionMode.ToLower())
        {
            case "add":
                //

                break;

            case "view":

               
                                
                PopulateTheRecord();     

              
                divSave.Visible = false;

                break;

            case "edit":

              
                if (!IsPostBack)
                {
                    PopulateTheRecord();
                }
                break;


            default:
                //?

                break;
        }

        if(!IsPostBack)
        {
              switch (_strActionMode.ToLower())
              {
                   case "add":
                        lblTitle.Text = "Add Service";

                        break;
                     case "view":

                        lblTitle.Text = "View Service";
                      break;
                      case "edit":

                        lblTitle.Text = "Edit Service";
                      break;
              }
            string strExtraTitle="";
            if(ViewState["ActiveTableID"]!=null)
            {
                Table tTable = RecordManager.ets_Table_Details(int.Parse(ViewState["ActiveTableID"].ToString()));
                if(tTable!=null)
                {
                    strExtraTitle = " - " + tTable.TableName;
                    if(  ViewState["theTableChild"]!=null)
                    {
                        TableChild theTableChild=(TableChild)ViewState["theTableChild"];
                        if(theTableChild!=null)
                        {
                            Table pTable = RecordManager.ets_Table_Details((int)theTableChild.ParentTableID);
                            if (pTable != null)
                            {
                                strExtraTitle = strExtraTitle + " Tab when Child of " + pTable.TableName;
                            }
                        }
                       
                    }
                }
            }
            else
            {
                Account tAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));
                if(tAccount!=null)
                {
                    strExtraTitle = " - Account Level- " + tAccount.AccountName;
                }
            }

            lblTitle.Text = lblTitle.Text + strExtraTitle;
        }
        Title = lblTitle.Text;


    }

    protected void ddlServiceType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //PopulatePageType();
        PopulateControlDDL();
    }
    protected void PopulatePageType()
    {
        if (ViewState["PageSource"]!=null && ViewState["PageSource"].ToString() == "TableID")
        {
            ListItem liT1 = new ListItem("Record List", "RecordList_P");
            ListItem liT2 = new ListItem("Record Detail", "RecordDetail_P",true);
            ddlServiceType.Items.Add(liT1);
            ddlServiceType.Items.Add(liT2);

        }
        else if (ViewState["PageSource"].ToString() == "TableChildID")
        {
            TableChild theTableChild = (TableChild)ViewState["theTableChild"];
            ListItem liTC1 = new ListItem("Child Record List", "RecordList_C");
            ListItem liTC2 = new ListItem("Child Record Detail", "RecordDetail_C");
            if (theTableChild.DetailPageType=="list")
            {
                liTC1.Selected = true;
            }
            else
            {
                liTC2.Selected = true;
            }

            ddlServiceType.Items.Add(liTC1);
            ddlServiceType.Items.Add(liTC2);
        }
        else if (ViewState["PageSource"].ToString() == "AccountID")
        {
            ListItem liAccountDetail = new ListItem("Account Detail -- coming soon--", "AccountDetail_aspx");
            ListItem liUser_List_aspx = new ListItem("User List -- coming soon--", "User_List_aspx");
            ListItem liUser_Detail_aspx = new ListItem("User Detail -- coming soon--", "User_Detail_aspx");
            ListItem liDefault_aspx = new ListItem("Dashboard -- coming soon--", "Default_aspx");
            ListItem liEachMap_aspx = new ListItem("Dashboard - Map Section", "EachMap_aspx");
            ListItem liLogin_aspx = new ListItem("Login Page", "Login_aspx");

            ddlServiceType.Items.Add(liAccountDetail);
            ddlServiceType.Items.Add(liUser_List_aspx);
            ddlServiceType.Items.Add(liUser_Detail_aspx);
            ddlServiceType.Items.Add(liDefault_aspx);
            ddlServiceType.Items.Add(liLogin_aspx);
            ddlServiceType.Items.Add(liEachMap_aspx);

        }
      
    }


    //protected void ddlTable_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    PopulateTemplateDDL(true);
    //}

     
    //protected void PopulateTheRecord()
    //{
    //    try
    //    {

    //        Service theService = ServiceManager.ets_Service_Detail((int)_iServiceID);

    //        ddlTable.SelectedValue = theService.TableID.ToString();
    //        chkUseMapping.Checked = theService.UseMapping;

    //        //PopulateLocationDDL();

    //        //if (theService.LocationID != null)
    //        //    ddlLocation.SelectedValue = theService.LocationID.ToString();

    //        txtEmailFrom.Text = theService.EmailFrom;
    //        txtServiceName.Text = theService.ServiceName;
    //        txtFilename.Text = theService.Filename;

    //        PopulateTemplateDDL(false);
    //        if (theService.TemplateID.HasValue)
    //            ddlTemplate.SelectedValue = theService.TemplateID.Value.ToString();

    //        if (_strActionMode == "edit")
    //        {
    //            ViewState["theService"] = theService;
    //        }
    //        else if (_strActionMode == "view")
    //        {
    //            divEdit.Visible = true;
    //            hlEditLink.NavigateUrl = Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/SystemData/ServiceDetail.aspx?mode=" + Cryptography.Encrypt("edit") + "&SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString() + "&ServiceID=" + Cryptography.Encrypt(theService.ServiceID.ToString());
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorLog theErrorLog = new ErrorLog(null, "Service Detail", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //        SystemData.ErrorLog_Insert(theErrorLog);
    //        lblMsg.Text = ex.Message;
    //    }

    //}
    

    //protected void EnableTheRecordControls(bool p_bEnable)
    //{        
    //    txtServiceName.Enabled = p_bEnable;
    //    txtEmailFrom.Enabled = p_bEnable;
    //    txtFilename.Enabled = p_bEnable;
    //    ddlTable.Enabled = p_bEnable;
    //    chkUseMapping.Enabled = p_bEnable;
    //    //ddlLocation.Enabled = p_bEnable;
            

    //}

    protected bool IsUserInputOK()
    {
        //this is the final server side vaidation before database action


        return true;
    }

    protected void PopulateControlDDL()
    {
        ddlConrolIDnText.Items.Clear();
        ddlConrolIDnText.Items.Add(new ListItem("", ""));


        if(ViewState["ActiveTableID"] ==null)
        {
            return;
        }

        string _strDynamictabPart = "";
        int iTableID = int.Parse(ViewState["ActiveTableID"].ToString());
        
        Table theTable = null;
        
         theTable = RecordManager.ets_Table_Details(iTableID);

        if (theTable!=null && ddlServiceType.SelectedValue == "RecordDetail_P")
        {
            if (lblTitle.Text.IndexOf(theTable.TableName) == -1)
                lblTitle.Text = lblTitle.Text + " - " + theTable.TableName;

            _strDynamictabPart = "ctl00_HomeContentPlaceHolder_";
            DataTable _dtColumnsDetail = RecordManager.ets_Table_Columns_Detail(iTableID);

            ddlConrolIDnText.Items.Add(new ListItem("Save button", "btnSaveRecord"));
            ddlConrolIDnText.Items.Add(new ListItem("User Role Name", "hfUserRoleName"));
            ddlConrolIDnText.Items.Add(new ListItem("Add/Edit/View", "hfRecordAddEditView"));
            ddlConrolIDnText.Items.Add(new ListItem("TableID", "hfRecordTableID"));
            ddlConrolIDnText.Items.Add(new ListItem("---------------------------------------", ""));

            for (int i = 0; i < _dtColumnsDetail.Rows.Count; i++)
            {

                string strPrefix = "";
                string strPrefix2 = "";
                string strPrefix3 = "";
                string strHTMLControl = "";
                string strHTMLControl2 = "";
                string strHTMLControl3 = "";
                if (Common.IsIn(_dtColumnsDetail.Rows[i]["ColumnType"].ToString(), "text,number,date,time,calculation")
                           || (_dtColumnsDetail.Rows[i]["ColumnType"].ToString() == "dropdown" &&
                               _dtColumnsDetail.Rows[i]["DropDownType"].ToString() == "table"))
                {
                    strPrefix = "txt";
                    strHTMLControl = "Textbox";
                }
                else if (_dtColumnsDetail.Rows[i]["ColumnType"].ToString() == "dropdown" &&
                               _dtColumnsDetail.Rows[i]["DropDownType"].ToString() != "table")
                {
                    strPrefix = "ddl";
                    strHTMLControl = "Dropdown";
                }
                else if (_dtColumnsDetail.Rows[i]["ColumnType"].ToString() == "datetime")
                {
                    strPrefix = "txt";
                    strHTMLControl = "Textbox";
                    ddlConrolIDnText.Items.Add(new ListItem(_dtColumnsDetail.Rows[i]["DisplayTextDetail"].ToString() + "(Textbox)",
                        _strDynamictabPart + "txtTime" + _dtColumnsDetail.Rows[i]["SystemName"].ToString()));
                }
                else if (_dtColumnsDetail.Rows[i]["SystemName"].ToString().ToLower() == "enteredby")
                {

                    ddlConrolIDnText.Items.Add(new ListItem(_dtColumnsDetail.Rows[i]["DisplayTextDetail"].ToString() + "(Dropdown)",
                        _strDynamictabPart + "ddlEnteredBy"));

                }
                else if (_dtColumnsDetail.Rows[i]["ColumnType"].ToString() == "button")
                {
                    strPrefix = "lnk";
                    strHTMLControl = "LinkButton";
                }
                else if (_dtColumnsDetail.Rows[i]["ColumnType"].ToString() == "file"
                       || _dtColumnsDetail.Rows[i]["ColumnType"].ToString() == "image")
                {
                    strPrefix = "fu";
                    strHTMLControl = "FileService";
                    strPrefix2 = "hf";
                    strHTMLControl2 = "HiddenField";
                    strPrefix3 = "pnl";
                    strHTMLControl3 = "Div";
                }
                else if (_dtColumnsDetail.Rows[i]["ColumnType"].ToString() == "radiobutton")
                {
                    strPrefix = "radio";
                    strHTMLControl = "Radiobutton List";
                }
                else if (_dtColumnsDetail.Rows[i]["ColumnType"].ToString() == "location")
                {
                    strPrefix = "hf";
                    strHTMLControl = "HiddenField";
                    strPrefix2 = "hf2";
                    strHTMLControl2 = "HiddenField";
                    strPrefix3 = "hf3";
                    strHTMLControl3 = "HiddenField";
                }
                else if (_dtColumnsDetail.Rows[i]["ColumnType"].ToString() == "checkbox")
                {
                    strPrefix = "chk";
                    strHTMLControl = "Checkbox";
                }
                else if (_dtColumnsDetail.Rows[i]["ColumnType"].ToString() == "listbox"
                        && _dtColumnsDetail.Rows[i]["DateCalculationType"].ToString() == "")
                {
                    strPrefix = "lst";
                    strHTMLControl = "Listbox";
                }
                else if (_dtColumnsDetail.Rows[i]["ColumnType"].ToString() == "listbox"
                      && _dtColumnsDetail.Rows[i]["DateCalculationType"].ToString() == "checkbox")
                {
                    strPrefix = "cbl";
                    strHTMLControl = "Listbox with Checkbox";
                }

                if (strPrefix != "")
                {
                    ddlConrolIDnText.Items.Add(new ListItem(_dtColumnsDetail.Rows[i]["DisplayTextDetail"].ToString() + "(" + strHTMLControl + ")",
                       _strDynamictabPart + strPrefix + _dtColumnsDetail.Rows[i]["SystemName"].ToString()));
                }

                if (strPrefix2 != "")
                {
                    ddlConrolIDnText.Items.Add(new ListItem(_dtColumnsDetail.Rows[i]["DisplayTextDetail"].ToString() + "(" + strHTMLControl2 + ")",
                       _strDynamictabPart + strPrefix2 + _dtColumnsDetail.Rows[i]["SystemName"].ToString()));
                }

                if (strPrefix3 != "")
                {
                    ddlConrolIDnText.Items.Add(new ListItem(_dtColumnsDetail.Rows[i]["DisplayTextDetail"].ToString() + "(" + strHTMLControl3 + ")",
                       _strDynamictabPart + strPrefix3 + _dtColumnsDetail.Rows[i]["SystemName"].ToString()));
                }

                ddlConrolIDnText.Items.Add(new ListItem(_dtColumnsDetail.Rows[i]["DisplayTextDetail"].ToString() + "(row)",
                      _strDynamictabPart + "trX" + _dtColumnsDetail.Rows[i]["SystemName"].ToString()));
            }


            DataTable dtCT = Common.DataTableFromText("SELECT * FROM TableChild WHERE ParentTableID=" + iTableID.ToString() + " AND DetailPageType<>'not' ORDER BY DisplayOrder");

            if (dtCT.Rows.Count > 0)
            {

                ddlConrolIDnText.Items.Add(new ListItem("------------------------------", ""));
                ddlConrolIDnText.Items.Add(new ListItem(theTable.TableName + " Header (div)", "div" + _strDynamictabPart + "lnkHeading"));
                ddlConrolIDnText.Items.Add(new ListItem(theTable.TableName + " Header (Linkbutton)", _strDynamictabPart + "lnkHeading"));
                ddlConrolIDnText.Items.Add(new ListItem(theTable.TableName + " Record detail (div)", "pnlDetail"));

                foreach (DataRow dr in dtCT.Rows)
                {
                    Table ctTable = RecordManager.ets_Table_Details(int.Parse(dr["ChildTableID"].ToString()));
                    string strCaption = dr["Description"].ToString();

                    if (strCaption == "")
                    {
                        strCaption = ctTable.TableName;
                    }
                    string strDetail = "detail";
                    if (dr["DetailPageType"].ToString() == "list")
                    {
                        strDetail = "list";
                    }
                    ddlConrolIDnText.Items.Add(new ListItem("----------", ""));
                    ddlConrolIDnText.Items.Add(new ListItem(strCaption + " Child Header (div)", "div" + _strDynamictabPart + "lnkHeading" + dr["ChildTableID"].ToString()));
                    ddlConrolIDnText.Items.Add(new ListItem(strCaption + " Child Header (Linkbutton)", _strDynamictabPart + "lnkHeading" + dr["ChildTableID"].ToString()));
                    ddlConrolIDnText.Items.Add(new ListItem(strCaption + " Child " + strDetail + " (div)", _strDynamictabPart + "pnlDetail" + dr["ChildTableID"].ToString()));

                }
            }

            if ((theTable.ShowSentEmails != null && (bool)theTable.ShowSentEmails)
               || (theTable.ShowReceivedEmails != null && (bool)theTable.ShowReceivedEmails))
            {
                ddlConrolIDnText.Items.Add(new ListItem("----------", ""));

                ddlConrolIDnText.Items.Add(new ListItem("Message Header (div)", "div" + _strDynamictabPart + "lnkHeadingmlList"));
                ddlConrolIDnText.Items.Add(new ListItem("Message Header (Linkbutton)", _strDynamictabPart + "lnkHeadingmlList"));
                ddlConrolIDnText.Items.Add(new ListItem("Message list (div)", _strDynamictabPart + "pnlDetailmlList"));

            }
            DataTable dtDBTableTab = Common.DataTableFromText("SELECT * FROM TableTab WHERE TableID=" +
                     theTable.TableID.ToString() + " ORDER BY DisplayOrder");
            if (dtDBTableTab != null)
            {
                if (dtDBTableTab.Rows.Count > 1)
                {
                    ddlConrolIDnText.Items.Add(new ListItem("-------" + theTable.TableName + " Pages------", ""));

                    for (int t = 0; t < dtDBTableTab.Rows.Count; t++)
                    {
                        if (t == 0)
                        {
                            ddlConrolIDnText.Items.Add(new ListItem("       " + dtDBTableTab.Rows[t]["TabName"].ToString() + " -- Header (Linkbutton)", _strDynamictabPart + "lnkDetialTab"));
                            ddlConrolIDnText.Items.Add(new ListItem("       " + dtDBTableTab.Rows[t]["TabName"].ToString() + " -- Detail (Div)", "pnlDetailTab"));
                        }
                        else
                        {
                            ddlConrolIDnText.Items.Add(new ListItem("       " + dtDBTableTab.Rows[t]["TabName"].ToString() + " -- Header (Linkbutton)", _strDynamictabPart + "lnkDetialTabD" + dtDBTableTab.Rows[t]["TableTabID"].ToString()));
                            ddlConrolIDnText.Items.Add(new ListItem("       " + dtDBTableTab.Rows[t]["TabName"].ToString() + " -- Detail (Div)", _strDynamictabPart + "pnlDetailTabD" + dtDBTableTab.Rows[t]["TableTabID"].ToString()));
                        }
                    }
                }
            }
        }
        else if (theTable != null && ddlServiceType.SelectedValue == "RecordList_P")
        {
            ddlConrolIDnText.Items.Add(new ListItem("Go", "ctl00_HomeContentPlaceHolder_rlOne_lnkSearch"));
            ddlConrolIDnText.Items.Add(new ListItem("--Coming soon--", "ctl00_HomeContentPlaceHolder_rlOne_lnkSearch"));
            //DivRoot 
        }
        else if (theTable != null && ddlServiceType.SelectedValue == "RecordList_C")
        {
            ddlConrolIDnText.Items.Add(new ListItem("--Coming soon--", ""));
            //ddlConrolIDnText.Items.Add(new ListItem("Go", "ctl00_HomeContentPlaceHolder_rlOne_lnkSearch"));
            //ddlConrolIDnText.Items.Add(new ListItem("--Coming soon--", "ctl00_HomeContentPlaceHolder_rlOne_lnkSearch"));
            //DivRoot 
        }
        else if (theTable != null && ddlServiceType.SelectedValue == "RecordDetail_C")
        {
            ddlConrolIDnText.Items.Add(new ListItem("--Coming soon--", ""));
        }
        else
        {
            ddlConrolIDnText.Items.Add(new ListItem("--Coming soon--", ""));
            //need to
        }

    }

    public int? qsAccountID
    {
       get
        {
          return  Request.QueryString["AccountID"] == null ? null : (int?)int.Parse(Cryptography.Decrypt(Request.QueryString["AccountID"].ToString()));
        }
    }
    public int? qsTableID
    {
        get
        {
            return Request.QueryString["TableID"] == null ? null : (int?)int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
        }
    }

    public int? qsTableChildID
    {
        get
        {
            return Request.QueryString["TableChildID"] == null ? null : (int?)int.Parse(Cryptography.Decrypt(Request.QueryString["TableChildID"].ToString()));
        }
    }



    //protected void cmdSave_Click(object sender, ImageClickEventArgs e)
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (IsUserInputOK())
            {
                //int intValue = 0;

                bool IsServerCode = true;
                PageLifeCycleService thePageLifeCycleService = new PageLifeCycleService();

                if (ddlServerControlEvent.SelectedValue != "")
                    thePageLifeCycleService.ServerControlEvent = ddlServerControlEvent.SelectedValue;

                if (txtSPName.Text.Trim() != "")
                    thePageLifeCycleService.SPName = txtSPName.Text.Trim();

                if (txtDotNetMethod.Text.Trim() != "")
                    thePageLifeCycleService.DotNetMethod = txtDotNetMethod.Text.Trim();
                
                if (txtJavaScriptFunction.Text.Trim() != "")
                    thePageLifeCycleService.JavaScriptFunction = txtJavaScriptFunction.Text.Trim();
                              


                if (txtSPName.Text.Trim() == "" && txtDotNetMethod.Text.Trim() == ""
                    && txtJavaScriptFunction.Text.Trim() == "" && ddlServerControlEvent.SelectedValue == ""
                    )
                {
                    IsServerCode = false;
                }
                //int iSameEventOrder = 0;
                //if(int.TryParse(txtSameEventOrder.Text,iSameEventOrder))
                //{
                   
                //}

                switch (_strActionMode.ToLower())
                {
                    case "add":
                                              
                        Service newService = new Service(null, ddlServiceType.SelectedValue,
                           qsTableID,
                 qsTableChildID,
                  qsAccountID,
                         IsServerCode, thePageLifeCycleService.GetJSONString(),ddlControlField.SelectedValue);
                        newService.ProcessNotes = txtNotes.Text;
                        newService.ServiceDefinition = txtServiceDefinition.Text;
                        newService.SameEventOrder = txtSameEventOrder.Text == "" ? null :(int?) int.Parse(txtSameEventOrder.Text);
                        //newService.ColumnSystemName = ddlControlField.SelectedValue == "" ? null : ddlControlField.SelectedValue;
                        SystemData.Service_Insert(newService);
                       

                        break;

                    case "view":


                        break;

                    case "edit":
                        Service editService = SystemData.Service_Detail((int)_iServiceID);

                        editService.ServiceType = ddlServiceType.SelectedValue;
                        editService.ServiceJSON = thePageLifeCycleService.GetJSONString();
                        editService.IsServerCode = IsServerCode;

                        editService.TableID =qsTableID;
                        editService.TableChildID =qsTableChildID;
                        editService.AccountID =  qsAccountID;

                        editService.ProcessNotes = txtNotes.Text;

                        editService.ServiceDefinition = txtServiceDefinition.Text;
                        editService.SameEventOrder = txtSameEventOrder.Text == "" ? null : (int?)int.Parse(txtSameEventOrder.Text);
                        editService.ColumnSystemName = ddlControlField.SelectedValue; //red service/custome 12112017

                        SystemData.Service_Update(editService);
                        


                        break;

                    default:
                        //?
                        break;
                }
            }
            else
            {
                //user input is not ok

            }
            Response.Redirect(hlBack.NavigateUrl, false);

        }
        catch (Exception ex)
        {

           
                ErrorLog theErrorLog = new ErrorLog(null, "Service Detail", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
                lblMsg.Text = ex.Message;
            
            
        }

     

    }

    //red service/custome 12112017
    protected void ddlServerControlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        trControl.Visible = false;

        // if (ddlServerControlEvent.SelectedValue == "ASP_Control_Event_Button")
        // {

        PopulateControl(ddlServerControlEvent.SelectedValue.ToString());
        // }        
    }

    //red service/custome 12112017
    protected void PopulateControl(string strEventType)
    {
      
        if (strEventType == "ASP_Control_Event_Button")
        {
            trControl.Visible = true;
            string sSQL = "";
            ddlControlField.Items.Clear();
            sSQL = "SELECT c.SystemName, c.DisplayName ";
            sSQL += "FROM [Column] c ";
            sSQL += "WHERE c.ColumnType = 'button' AND c.TableID = " + int.Parse(ViewState["ActiveTableID"].ToString());
            ddlControlField.DataSource = Common.DataTableFromText(sSQL);
            ddlControlField.DataBind();

            ListItem liSelect = new ListItem("--Please Select--", "");
            ddlControlField.Items.Insert(0, liSelect);

        }
    }

}

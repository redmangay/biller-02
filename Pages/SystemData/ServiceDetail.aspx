﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true"
    CodeFile="ServiceDetail.aspx.cs" Inherits="Pages_SystemData_ServiceDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <script language="javascript" type="text/javascript">
      

        function abc() {
            var b = document.getElementById('<%= lnkSave.ClientID %>');
            if (b && typeof (b.click) == 'undefined') {
                b.click = function () {
                    var result = true;
                    if (b.onclick) result = b.onclick();
                    if (typeof (result) == 'undefined' || result) {
                        eval(b.getAttribute('href'));
                    }
                }
            }

        }
    </script>
    <table border="0" cellpadding="0" cellspacing="0"  align="center">
        <tr>
            <td colspan="3">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" style=" padding-left:20px; width: 50%;">
                            <span class="TopTitle">
                                <asp:Label runat="server" ID="lblTitle"></asp:Label></span>
                        </td>
                        <td align="left">
                            <table>
                                <tr>
                                    <td>
                                        <div>
                                            <asp:HyperLink runat="server" ID="hlBack">
                                                <asp:Image runat="server" ID="imgBack" ImageUrl="~/App_Themes/Default/images/Back.png" ToolTip="Back" />
                                            </asp:HyperLink>
                                        </div>
                                    </td>

                                    <td>
                                        <div runat="server" id="divEdit" visible="false">
                                            <asp:HyperLink runat="server" ID="hlEditLink">
                                                <asp:Image runat="server" ID="Image2" ImageUrl="~/App_Themes/Default/images/Edit_big.png" ToolTip="Edit" />
                                            </asp:HyperLink>
                                        </div>
                                    </td>

                                    <td>
                                        <div runat="server" id="divSave">
                                            <asp:LinkButton runat="server" ID="lnkSave" OnClick="lnkSave_Click"
                                                CausesValidation="true">
                                                <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png" ToolTip="Save" />
                                            </asp:LinkButton>
                                        </div>
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="13">
            </td>
        </tr>
        <tr>
            <td valign="top">
            </td>
            <td valign="top">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div id="search" style="padding-bottom: 10px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                                ShowMessageBox="false" ShowSummary="false" HeaderText="Please correct the following errors:" />
                        </div>
                        <asp:Panel ID="Panel2" runat="server" DefaultButton="lnkSave">
                            <div runat="server" id="divDetail" onkeypress="abc();">
                                <table cellpadding="3">
                                    <tr>
                                         <td align="right" style="width:150px;">
                                            <strong>Page</strong>
                                        </td>
                                        <td>
                                            <asp:DropDownList runat="server" ID="ddlServiceType" OnSelectedIndexChanged="ddlServiceType_SelectedIndexChanged"
                                                CssClass="NormalTextBox" AutoPostBack="true">
                                               
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator runat="server" ID="rfvST" ControlToValidate="ddlServiceType" ErrorMessage="*"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <strong>Server Event</strong>
                                        </td>
                                        <td>
                                             <asp:DropDownList runat="server" ID="ddlServerControlEvent"
                                                CssClass="NormalTextBox" AutoPostBack="true" OnSelectedIndexChanged="ddlServerControlEvent_SelectedIndexChanged">
                                                
                                                  <asp:ListItem Value="ASP_OnPreInit" Text="OnPreInit"></asp:ListItem>
                                                 <asp:ListItem Value="ASP_Page_Start_Init" Text="Page_Init(Start)"></asp:ListItem>
                                                 <asp:ListItem Value="ASP_Page_Init" Text="Page_Init"></asp:ListItem>
                                                 <asp:ListItem Value="ASP_Page_Load" Text="Page_Load" Selected="True"></asp:ListItem>
                                                 <asp:ListItem Value="ASP_Control_Event_Button" Text="Control_Event_Button"></asp:ListItem> <%--//red service/custome 12112017--%>
                                                 <asp:ListItem Value="ASP_Page_LoadComplete" Text="Page_LoadComplete"></asp:ListItem>
                                                 <asp:ListItem Value="ASP_Page_PreRender" Text="Page_PreRender"></asp:ListItem>
                                                 <asp:ListItem Value="ASP_OnSaveStateComplete" Text="OnSaveStateComplete"></asp:ListItem>

                                                 <asp:ListItem Value="OnPerformSave" Text="OnPerformSave"></asp:ListItem>
                                                 <asp:ListItem Value="ButtonClick" Text="ButtonClick"></asp:ListItem>
                                               
                                                <asp:ListItem Value="Before_Search" Text="TDB_Before_Search"></asp:ListItem>
                                                 <asp:ListItem Value="TDB_After_Login" Text="TDB_After_Login"></asp:ListItem>
                                                <asp:ListItem Value="RecordList_GridView_RowDataBound" Text="End of RecordList_GridView_RowDataBound"></asp:ListItem>
                                                <asp:ListItem Value="RecordList_GridView_DataBound" Text="end of RecordList_GridView_DataBound"></asp:ListItem>
                                                <asp:ListItem Value="ASP_Page_Load" Text="--more can be added on deman..."></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                     <tr id="trControl" runat="server" clientidmode="Static">
                                        <td align="right"><strong>Control</strong></td>
                                        <td>                                          
                                              <asp:DropDownList runat="server" CssClass="NormalTextBox" ID="ddlControlField" DataValueField="SystemName"
                                            DataTextField="DisplayName">
                                        </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <strong>SP Name</strong>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtSPName" CssClass="NormalTextBox" Width="400"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <strong>.Net Method</strong>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtDotNetMethod" CssClass="NormalTextBox" Width="400"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <strong>Notes</strong>
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtNotes" CssClass="NormalTextBox" Width="400" Height="50" TextMode="MultiLine" ></asp:TextBox>
                                            <asp:RequiredFieldValidator runat="server" ID="requiredtxtNotes" ControlToValidate="txtNotes" ErrorMessage="This field is required" SetFocusOnError="true" EnableClientScript="true" ></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top">
                                            <p></p>
                                            <strong>Javascript Code</strong>
                                        </td>
                                        <td>

                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList runat="server" ID="ddlConrolIDnText" Width="400" ClientIDMode="Static"
                                                            ToolTip="Select a field and then click Add control ID." AutoPostBack="false">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left">
                                                        <asp:LinkButton runat="server" ID="lnlAddDataBaseFieldText" OnClientClick=" insertAtCaret(); return false;"
                                                            CausesValidation="true" Visible="true"> <strong>Add Control ID</strong></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:TextBox runat="server" ID="txtJavaScriptFunction" TextMode="MultiLine" ClientIDMode="Static"
                                                CssClass="NormalTextBox" Width="1400" Height="1600"></asp:TextBox>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td align="right">
                                           
                                        </td>
                                        <td>
                                             <p> When many services are in a single event, i want this service run order as below</p>
                                            <asp:TextBox runat="server" ID="txtSameEventOrder" CssClass="NormalTextBox" Width="50"></asp:TextBox>
                                            
                                        </td>
                                    </tr>
                                      <tr>
                                        <td align="right">
                                          
                                        </td>
                                        <td>
                                            <p>Service difinition in XML</p>
                                            <asp:TextBox runat="server" ID="txtServiceDefinition" CssClass="NormalTextBox" Width="400" Height="500"></asp:TextBox>
                                        </td>
                                    </tr>
                                     
                                </table>
                            </div>
                            <br />
                            <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                            <br />
                          
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="13">
            </td>
        </tr>
    </table>
</asp:Content>

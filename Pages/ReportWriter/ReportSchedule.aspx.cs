﻿using System;
using System.Drawing;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_ReportWriter_ReportSchedule : SecurePage
{

    private string _strActionMode = "view";
    private int? _iReportId;
    private string _qsReportId = "";
    private User _objUser;


    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Request.QueryString["popup"] != null)
        {
            this.Page.MasterPageFile = "~/Home/PopUp.master";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _objUser = (User) Session["User"];
        if (!IsPostBack)
        {
            //if (!Common.HaveAccess(Session["roletype"].ToString(), "1,2,"))
            //{ Response.Redirect("~/Default.aspx", false); }

            if (Request.QueryString["popup"] != null)
            {
                divBack.Visible = false;
            }
        }

        if (Request.QueryString["mode"] == null)
        {
            Server.Transfer("~/Default.aspx");
        }
        else
        {
            string qsMode = Cryptography.Decrypt(Request.QueryString["mode"]);

            if (qsMode == "add" ||
                qsMode == "view" ||
                qsMode == "edit")
            {
                _strActionMode = qsMode.ToLower();

                if (Request.QueryString["ReportID"] != null)
                {
                    _qsReportId = Cryptography.Decrypt(Request.QueryString["ReportID"]);
                    _iReportId = int.Parse(_qsReportId);
                }
                else
                {
                    Server.Transfer("~/Default.aspx");
                }
            }
            else
            {
                Server.Transfer("~/Default.aspx");
            }

            switch (_strActionMode)
            {
                case "add":
                    if (_iReportId.HasValue && !IsPostBack)
                        PopulateTheRecord();
                    break;
                case "view":
                    PopulateTheRecord();
                    EnableTheRecordControls(false);
                    divSave.Visible = false;
                    divEdit.Visible = true;
                    break;
                case "edit":
                    if (!IsPostBack)
                    {
                        PopulateTheRecord();
                    }
                    break;
                default:
                    Server.Transfer("~/Default.aspx");
                    break;
            }
        }

        if (!IsPostBack)
        {
            int itemsCount = 0;

            if (_iReportId.HasValue)
                itemsCount = PopulateReportRecipient(_iReportId.Value);
        }
    }

    protected bool IsUserInputOK(out string message)
    {
        //this is the final server side vaidation before database action
        message = String.Empty;
        bool rv = true;

        DateTime dt;
        if (!DateTime.TryParse(txtDate.Text, out dt))
        {
            message = "Invalid Date.";
            rv = false;
        }
        TimeSpan ts;
        if (!String.IsNullOrEmpty(txtTime.Text) && !TimeSpan.TryParse(txtTime.Text, out ts))
        {
            if (!String.IsNullOrEmpty(message))
                message += " ";
            message += "Invalid Time.";
            rv = false;
        }

        return rv;
    }

    protected void PopulateTheRecord()
    {
        if (_iReportId.HasValue)
        {
            try
            {
                Report report = ReportManager.ets_Report_Detail(_iReportId.Value);
                lblReportName.Text = report.ReportName;

                ReportSchedule reportSchedule = ReportManager.ets_ReportSchedule_Detail(_iReportId.Value);
                if (reportSchedule == null)
                {
                    ddlRepeatEvery.SelectedValue = ReportSchedule.ReportScheduleFrequency.OneOff.ToString();
                    txtDate.Text = DateTime.Today.AddDays(1).ToString("d");
                    txtTime.Text = "00:00";
                    switch (DateTime.Today.AddDays(1).DayOfWeek)
                    {
                        case DayOfWeek.Monday:
                            cbMonday.Checked = true;
                            break;
                        case DayOfWeek.Tuesday:
                            cbTuesday.Checked = true;
                            break;
                        case DayOfWeek.Wednesday:
                            cbWednesday.Checked = true;
                            break;
                        case DayOfWeek.Thursday:
                            cbThursday.Checked = true;
                            break;
                        case DayOfWeek.Friday:
                            cbFriday.Checked = true;
                            break;
                        case DayOfWeek.Saturday:
                            cbSaturday.Checked = true;
                            break;
                        case DayOfWeek.Sunday:
                            cbSunday.Checked = true;
                            break;
                    }
                }
                else
                {
                    switch (reportSchedule.Frequency)
                    {
                        case ReportSchedule.ReportScheduleFrequency.Undefined:
                            ddlFrequency.SelectedValue = ReportSchedule.ReportScheduleFrequency.Undefined.ToString();
                            break;
                        case ReportSchedule.ReportScheduleFrequency.OneOff:
                            ddlFrequency.SelectedValue = ReportSchedule.ReportScheduleFrequency.OneOff.ToString();
                            if (reportSchedule.RunDateTime.HasValue)
                            {
                                txtDate.Text = reportSchedule.RunDateTime.Value.ToString("d");
                                txtTime.Text = reportSchedule.RunDateTime.Value.ToString("HH:mm");
                            }
                            else
                            {
                                txtDate.Text = DateTime.Today.AddDays(1).ToString("d");
                                txtTime.Text = "00:00";
                            }
                            break;
                        case ReportSchedule.ReportScheduleFrequency.Weekly:
                            ddlFrequency.SelectedValue = ReportSchedule.ReportScheduleFrequency.Weekly.ToString();
                            if (reportSchedule.RunDateTime.HasValue)
                            {
                                txtDate.Text = reportSchedule.RunDateTime.Value.ToString("d");
                                txtTime.Text = reportSchedule.RunDateTime.Value.ToString("HH:mm");
                            }
                            else
                            {
                                txtDate.Text = DateTime.Today.AddDays(1).ToString("d");
                                txtTime.Text = "00:00";
                            }
                            ddlRepeatEvery.SelectedValue = reportSchedule.RepeatEvery.ToString();
                            cbMonday.Checked = reportSchedule.WeeklyMonday;
                            cbTuesday.Checked = reportSchedule.WeeklyTuesday;
                            cbWednesday.Checked = reportSchedule.WeeklyWednesday;
                            cbThursday.Checked = reportSchedule.WeeklyThurdsay;
                            cbFriday.Checked = reportSchedule.WeeklyFriday;
                            cbSaturday.Checked = reportSchedule.WeeklySaturday;
                            cbSunday.Checked = reportSchedule.WeeklySunday;
                            break;
                        case ReportSchedule.ReportScheduleFrequency.Monthly:
                            ddlFrequency.SelectedValue = ReportSchedule.ReportScheduleFrequency.Monthly.ToString();
                            if (reportSchedule.RunDateTime.HasValue)
                            {
                                txtDate.Text = reportSchedule.RunDateTime.Value.ToString("d");
                                txtTime.Text = reportSchedule.RunDateTime.Value.ToString("HH:mm");
                            }
                            else
                            {
                                txtDate.Text = DateTime.Today.AddDays(1).ToString("d");
                                txtTime.Text = "00:00";
                            }
                            ddlRepeatEvery.SelectedValue = reportSchedule.RepeatEvery.ToString();
                            rblRepeatOnMonth.SelectedValue = reportSchedule.MonthlyMode.ToString();
                            break;
                    }
                }

                string s = String.Empty;
                if (report.ReportDates == Report.ReportInterval.IntervalDateRange &&
                    (!report.ReportStartDate.HasValue || !report.ReportEndDate.HasValue))
                {
                    ddlFrequency.SelectedValue = ReportSchedule.ReportScheduleFrequency.Undefined.ToString();
                    ddlFrequency.Enabled = false;
                    s = "Report cannot be scheduled when its dates are set to \"Enter when running report\"";
                }

                SetRows();
                SetSummary(s);
                SetNextRunDateList();

                if (_strActionMode == "view")
                {
                    hlEditLink.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                             Request.ApplicationPath +
                                             "Pages/ReportWriter/ReportSchedule.aspx" +
                                             "?mode=" + Cryptography.Encrypt("edit") +
                                             "&ReportID=" + Request.QueryString["ReportID"].ToString() +
                                             "&SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Report Detail", ex.Message, ex.StackTrace, DateTime.Now,
                    Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
                lblMsg.Text = ex.Message;
            }
        }
    }

    protected void EnableTheRecordControls(bool enable)
    {
        ddlFrequency.Enabled = enable;
        txtDate.Enabled = enable;
        txtTime.Enabled = enable;
        ddlRepeatEvery.Enabled = enable;
        cbMonday.Enabled = enable;
        cbTuesday.Enabled = enable;
        cbWednesday.Enabled = enable;
        cbThursday.Enabled = enable;
        cbFriday.Enabled = enable;
        cbSaturday.Enabled = enable;
        cbSunday.Enabled = enable;
        rblRepeatOnMonth.Enabled = enable;
    }

    protected void lnkSave_Click(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        string strEditURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                            "Pages/ReportWriter/ReportTable.aspx" +
                            "?ReportID=" + Request.QueryString["ReportID"] +
                            "&mode=" + Request.QueryString["mode"] +
                            "&SearchCriteria=" + Request.QueryString["SearchCriteria"];

        try
        {
            string message = String.Empty;
            if (IsUserInputOK(out message))
            {
                if (_strActionMode != "view" && _iReportId.HasValue)
                {
                    DateTime dt = new DateTime();
                    ReportSchedule.ReportScheduleFrequency frequency = ReportSchedule.ReportScheduleFrequency.Undefined;
                    Enum.TryParse(ddlFrequency.SelectedValue, out frequency);
                    if (frequency != ReportSchedule.ReportScheduleFrequency.Undefined)
                    {
                        dt = DateTime.Parse(txtDate.Text);
                        if (!String.IsNullOrEmpty(txtTime.Text))
                        {
                            TimeSpan ts = TimeSpan.Parse(txtTime.Text);
                            dt = dt + ts;
                        }
                    }
                    switch (frequency)
                    {
                        case ReportSchedule.ReportScheduleFrequency.Undefined:
                            ReportManager.ets_ReportSchedule_SetNotScheduled(_iReportId.Value);
                            break;
                        case ReportSchedule.ReportScheduleFrequency.OneOff:
                            ReportManager.ets_ReportSchedule_SetOneOffSchedule(_iReportId.Value, dt);
                            break;
                        case ReportSchedule.ReportScheduleFrequency.Weekly:
                            if (!cbMonday.Checked && !cbThursday.Checked && !cbWednesday.Checked &&
                                !cbThursday.Checked && !cbFriday.Checked && !cbSaturday.Checked && !cbSunday.Checked)
                            {
                                switch (dt.DayOfWeek)
                                {
                                    case DayOfWeek.Monday:
                                        cbMonday.Checked = true;
                                        break;
                                    case DayOfWeek.Tuesday:
                                        cbTuesday.Checked = true;
                                        break;
                                    case DayOfWeek.Wednesday:
                                        cbWednesday.Checked = true;
                                        break;
                                    case DayOfWeek.Thursday:
                                        cbThursday.Checked = true;
                                        break;
                                    case DayOfWeek.Friday:
                                        cbFriday.Checked = true;
                                        break;
                                    case DayOfWeek.Saturday:
                                        cbSaturday.Checked = true;
                                        break;
                                    case DayOfWeek.Sunday:
                                        cbSunday.Checked = true;
                                        break;
                                }
                            }
                            ReportManager.ets_ReportSchedule_SetWeeklySchedule(_iReportId.Value, dt,
                                int.Parse(ddlRepeatEvery.SelectedValue),
                                cbMonday.Checked, cbTuesday.Checked, cbWednesday.Checked, cbThursday.Checked,
                                cbFriday.Checked, cbSaturday.Checked, cbSunday.Checked);
                            break;
                        case ReportSchedule.ReportScheduleFrequency.Monthly:
                            ReportSchedule.ReportScheduleMonthlyMode mode =
                                ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth;
                            Enum.TryParse(rblRepeatOnMonth.SelectedValue, out mode);
                            switch (mode)
                            {
                                case ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth:
                                    ReportManager.ets_ReportSchedule_SetMonthlySchedule(_iReportId.Value, dt,
                                        int.Parse(ddlRepeatEvery.SelectedValue), dt.Day);
                                    break;
                                case ReportSchedule.ReportScheduleMonthlyMode.DayOfWeek:
                                    ReportManager.ets_ReportSchedule_SetMonthlySchedule(_iReportId.Value, dt,
                                        int.Parse(ddlRepeatEvery.SelectedValue), (dt.Day - 1) / 7 + 1, dt.DayOfWeek);
                                    break;
                            }
                            break;
                    }
                }

                Response.Redirect(strEditURL, false);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "error",
                    String.Format("alert('{0}');", message), true);
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Report Detail", ex.Message, ex.StackTrace, DateTime.Now,
                Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }

    public int AccountID
    {
        get
        {
            int retVal = 0;
            if (Session["AccountID"] != null)
                retVal = Convert.ToInt32(Session["AccountID"]);
            return retVal;
        }
    }

    protected void lbBack_OnClick(object sender, EventArgs e)
    {
        if (Request.QueryString["popup"] != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "OnClose", "Close();", true);
        }
        else
        {
            string navigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                 "Pages/ReportWriter/ReportTable.aspx" +
                                 "?ReportID=" + Request.QueryString["ReportID"] +
                                 "&mode=" + Request.QueryString["mode"] +
                                 "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
            Response.Redirect(navigateUrl);
        }
    }

    protected void ddlFrequency_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        SetRows();
        SetSummary(null);
        SetNextRunDateList();
    }


    protected void OnScheduleChanged(object sender, EventArgs e)
    {
        SetSummary(null);
        SetNextRunDateList();
    }

    private void SetRows()
    {
        ReportSchedule.ReportScheduleFrequency frequency = ReportSchedule.ReportScheduleFrequency.Undefined;
        Enum.TryParse(ddlFrequency.SelectedValue, out frequency);
        switch (frequency)
        {
            case ReportSchedule.ReportScheduleFrequency.Undefined:
                trDate.Visible = false;
                trRepeatEvery.Visible = false;
                trRepeatOnWeeek.Visible = false;
                trRepeatOnMonth.Visible = false;
                hlScheduledDates.Visible = false;
                break;
            case ReportSchedule.ReportScheduleFrequency.OneOff:
                trDate.Visible = true;
                lblDate.Text = "Date";
                trRepeatEvery.Visible = false;
                trRepeatOnWeeek.Visible = false;
                trRepeatOnMonth.Visible = false;
                hlScheduledDates.Visible = false;
                break;
            case ReportSchedule.ReportScheduleFrequency.Weekly:
                trDate.Visible = true;
                lblDate.Text = "Next run on";
                trRepeatEvery.Visible = true;
                lblRepeatInterval.Text = "weeks";
                trRepeatOnWeeek.Visible = true;
                trRepeatOnMonth.Visible = false;
                hlScheduledDates.Visible = true;
                break;
            case ReportSchedule.ReportScheduleFrequency.Monthly:
                trDate.Visible = true;
                lblDate.Text = "Next run on";
                trRepeatEvery.Visible = true;
                lblRepeatInterval.Text = "months";
                trRepeatOnWeeek.Visible = false;
                trRepeatOnMonth.Visible = true;
                hlScheduledDates.Visible = true;
                break;
        }
    }

    private void SetSummary(string summary)
    {
        string s;
        if (!String.IsNullOrEmpty(summary))
        {
            s = summary;
            lblSummary.ForeColor = Color.Red;
        }
        else
        {
            ReportSchedule.ReportScheduleFrequency frequency = ReportSchedule.ReportScheduleFrequency.Undefined;
            Enum.TryParse(ddlFrequency.SelectedValue, out frequency);
            s = "Not scheduled to run";
            switch (frequency)
            {
                case ReportSchedule.ReportScheduleFrequency.Undefined:
                    break;
                case ReportSchedule.ReportScheduleFrequency.OneOff:
                    s = String.Concat("One off on ", String.IsNullOrEmpty(txtDate.Text) ? "(undefined)" : txtDate.Text,
                        String.IsNullOrEmpty(txtTime.Text) ? "" : (" " + txtTime.Text));
                    break;
                case ReportSchedule.ReportScheduleFrequency.Weekly:
                    string s1 = ddlRepeatEvery.SelectedValue == "1"
                        ? "Weekly"
                        : String.Concat("Every ", ddlRepeatEvery.SelectedValue, " weeks");
                    string s2 = "";
                    int i = 0;
                    if (cbMonday.Checked)
                    {
                        s2 = String.Concat(s2, "Monday, ");
                        i++;
                    }
                    if (cbTuesday.Checked)
                    {
                        s2 = String.Concat(s2, "Tuesday, ");
                        i++;
                    }
                    if (cbWednesday.Checked)
                    {
                        s2 = String.Concat(s2, "Wednesday, ");
                        i++;
                    }
                    if (cbThursday.Checked)
                    {
                        s2 = String.Concat(s2, "Thursday, ");
                        i++;
                    }
                    if (cbFriday.Checked)
                    {
                        s2 = String.Concat(s2, "Friday, ");
                        i++;
                    }
                    if (cbSaturday.Checked)
                    {
                        s2 = String.Concat(s2, "Saturday, ");
                        i++;
                    }
                    if (cbSunday.Checked)
                    {
                        s2 = String.Concat(s2, "Sunday, ");
                        i++;
                    }
                    if (i == 0)
                    {
                        switch (DateTime.Today.DayOfWeek)
                        {
                            case DayOfWeek.Monday:
                                s2 = "Monday";
                                break;
                            case DayOfWeek.Tuesday:
                                s2 = "Tuesday";
                                break;
                            case DayOfWeek.Wednesday:
                                s2 = "Wednesday";
                                break;
                            case DayOfWeek.Thursday:
                                s2 = "Thursday";
                                break;
                            case DayOfWeek.Friday:
                                s2 = "Friday";
                                break;
                            case DayOfWeek.Saturday:
                                s2 = "Saturday";
                                break;
                            case DayOfWeek.Sunday:
                                s2 = "Sunday";
                                break;
                        }
                    }
                    else
                    {
                        if (i == 7)
                            s2 = "all days";
                        else
                        {
                            s2 = s2.Substring(0, s2.Length - 2);
                        }
                    }
                    s = String.Concat(s1, " on ", s2);
                    break;
                case ReportSchedule.ReportScheduleFrequency.Monthly:
                    string s3 = ddlRepeatEvery.SelectedValue == "1"
                        ? "Monthly"
                        : String.Concat("Every ", ddlRepeatEvery.SelectedValue, " months");
                    string s4 = "";
                    DateTime dt;
                    if (!String.IsNullOrEmpty(txtDate.Text) && DateTime.TryParse(txtDate.Text, out dt))
                    {
                        ReportSchedule.ReportScheduleMonthlyMode mode =
                            ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth;
                        Enum.TryParse(rblRepeatOnMonth.SelectedValue, out mode);
                        switch (mode)
                        {
                            case ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth:
                                if (dt.Day == 31)
                                    s4 = "the last day of month";
                                else
                                    s4 = String.Format("day {0}", dt.Day);
                                break;
                            case ReportSchedule.ReportScheduleMonthlyMode.DayOfWeek:
                                switch ((dt.Day - 1) / 7)
                                {
                                    case 0:
                                        s4 = "the first ";
                                        break;
                                    case 1:
                                        s4 = "the second ";
                                        break;
                                    case 2:
                                        s4 = "the third ";
                                        break;
                                    case 3:
                                        s4 = "the fourth ";
                                        break;
                                    case 4:
                                        s4 = "the last ";
                                        break;
                                }
                                switch (dt.DayOfWeek)
                                {
                                    case DayOfWeek.Monday:
                                        s4 += "Monday";
                                        break;
                                    case DayOfWeek.Tuesday:
                                        s4 += "Tuesday";
                                        break;
                                    case DayOfWeek.Wednesday:
                                        s4 += "Wednesday";
                                        break;
                                    case DayOfWeek.Thursday:
                                        s4 += "Thursday";
                                        break;
                                    case DayOfWeek.Friday:
                                        s4 += "Friday";
                                        break;
                                    case DayOfWeek.Saturday:
                                        s4 += "Saturday";
                                        break;
                                    case DayOfWeek.Sunday:
                                        s4 += "Sunday";
                                        break;
                                }
                                break;
                        }
                        s = String.Concat(s3, " on ", s4);
                    }
                    else
                    {
                        s = s3;
                    }
                    break;
            }
            lblSummary.ForeColor = Color.Black;
        }
        lblSummary.Text = s;
    }

    private void SetNextRunDateList()
    {
        System.Collections.Generic.List<DateTime> nextRuns = null;

        if (!String.IsNullOrEmpty(txtDate.Text))
        {
            ReportSchedule.ReportScheduleFrequency frequency = ReportSchedule.ReportScheduleFrequency.Undefined;
            Enum.TryParse(ddlFrequency.SelectedValue, out frequency);
            DateTime dt = new DateTime();
            if (frequency != ReportSchedule.ReportScheduleFrequency.Undefined)
            {
                dt = DateTime.Parse(txtDate.Text);
                if (!String.IsNullOrEmpty(txtTime.Text))
                {
                    TimeSpan ts = TimeSpan.Parse(txtTime.Text);
                    dt = dt + ts;
                }
            }

            switch (frequency)
            {
                case ReportSchedule.ReportScheduleFrequency.Weekly:
                    bool onMonday = cbMonday.Checked;
                    bool onTuesday = cbTuesday.Checked;
                    bool onWednesday = cbWednesday.Checked;
                    bool onThursday = cbThursday.Checked;
                    bool onFriday = cbFriday.Checked;
                    bool onSaturday = cbSaturday.Checked;
                    bool onSunday = cbSunday.Checked;
                    if (!onMonday && !onTuesday && !onWednesday && !onThursday && !onFriday && !onSaturday && !onSunday)
                    {
                        switch (DateTime.Today.DayOfWeek)
                        {
                            case DayOfWeek.Monday:
                                onMonday = true;
                                break;
                            case DayOfWeek.Tuesday:
                                onTuesday = true;
                                break;
                            case DayOfWeek.Wednesday:
                                onWednesday = true;
                                break;
                            case DayOfWeek.Thursday:
                                onThursday = true;
                                break;
                            case DayOfWeek.Friday:
                                onFriday = true;
                                break;
                            case DayOfWeek.Saturday:
                                onSaturday = true;
                                break;
                            case DayOfWeek.Sunday:
                                onSunday = true;
                                break;
                        }
                    }

                    nextRuns = ReportManager.ets_ReportSchedule_GetScheduledDates(DateTime.Now, dt,
                        int.Parse(ddlRepeatEvery.SelectedValue), onMonday,
                        onTuesday, onWednesday, onThursday, onFriday, onSaturday, onSunday, 10);

                    break;
                case ReportSchedule.ReportScheduleFrequency.Monthly:
                    ReportSchedule.ReportScheduleMonthlyMode mode = ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth;
                    Enum.TryParse(rblRepeatOnMonth.SelectedValue, out mode);
                    switch (mode)
                    {
                        case ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth:
                            nextRuns = ReportManager.ets_ReportSchedule_GetScheduledDates(DateTime.Now, dt,
                                int.Parse(ddlRepeatEvery.SelectedValue), dt.Day, 10);
                            break;
                        case ReportSchedule.ReportScheduleMonthlyMode.DayOfWeek:
                            nextRuns = ReportManager.ets_ReportSchedule_GetScheduledDates(DateTime.Now, dt,
                                int.Parse(ddlRepeatEvery.SelectedValue), (dt.Day - 1) / 7 + 1, dt.DayOfWeek, 10);
                            break;
                    }
                    break;
            }
        }

        if (nextRuns != null)
        {
            System.Collections.Generic.List<string> nextRunsString = new System.Collections.Generic.List<string>();
            CultureInfo culture = CultureInfo.CreateSpecificCulture("en-AU");
            foreach (DateTime nextRun in nextRuns)
            {
                nextRunsString.Add(nextRun.ToString("f", culture));
            }
            repeater10Runs.DataSource = nextRunsString;
            repeater10Runs.DataBind();
        }
    }

    protected void grdRepotRecipient_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    protected void grdReportRecipient_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }


    protected void btnRefreshReportRecipient_Click(object sender, EventArgs e)
    {
        if (_iReportId.HasValue)
            PopulateReportRecipient(_iReportId.Value);
    }

    protected void ReportRecipientPager_BindTheGridAgain(object sender, EventArgs e)
    {
    }

    protected void ReportRecipientPager_DeleteAction(object sender, EventArgs e)
    {
        string sCheck = "";

        for (int i = 0; i < grdReportRecipient.Rows.Count; i++)
        {
            bool ischeck = ((CheckBox)grdReportRecipient.Rows[i].FindControl("chkDelete")).Checked;
            if (ischeck)
            {
                sCheck = sCheck + ((Label)grdReportRecipient.Rows[i].FindControl("LblID")).Text + ",";
            }
        }
        if (string.IsNullOrEmpty(sCheck))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message_alert", "alert('Please select a record.');", true);
            return;
        }

        sCheck = sCheck + "-1";
        Common.ExecuteText("DELETE ReportRecipient WHERE ReportRecipientID IN (" + sCheck + ") ");
        if (_iReportId.HasValue)
            PopulateReportRecipient(_iReportId.Value);
    }

    public string GetAddReportRecipientURL(int reportId)
    {
        return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
               "/Pages/ReportWriter/AddReportRecipient.aspx?mode=" + Cryptography.Encrypt("add") + "&ReportID=" +
               Cryptography.Encrypt(reportId.ToString());
    }

    protected int PopulateReportRecipient(int reportId)
    {
        int iTN = 0;

        hlAddReportRecipient.NavigateUrl = GetAddReportRecipientURL(reportId);
        hlAddReportRecipient2.NavigateUrl = hlAddReportRecipient.NavigateUrl;
        System.Data.DataTable dtReportRecipients = Common.DataTableFromText("SELECT * FROM ReportRecipient WHERE ReportID =" + reportId.ToString());

        grdReportRecipient.DataSource = dtReportRecipients;
        iTN = dtReportRecipients.Rows.Count;
        grdReportRecipient.VirtualItemCount = iTN;
        grdReportRecipient.DataBind();

        if (_strActionMode == "view")
        {
            grdReportRecipient.Columns[2].Visible = false;
        }

        if (grdReportRecipient.TopPagerRow != null)
            grdReportRecipient.TopPagerRow.Visible = true;

        GridViewRow gvr = grdReportRecipient.TopPagerRow;

        if (gvr != null)
        {
            Common_Pager reportRecipientPager = (Common_Pager)gvr.FindControl("ReportRecipientPager");
            reportRecipientPager.AddURL = GetAddReportRecipientURL(reportId);
            reportRecipientPager.HyperAdd_CSS = "popuplinkRR";
            reportRecipientPager.AddToolTip = "Add";
            reportRecipientPager.TotalRows = iTN;

            if (_strActionMode == "view")
            {
                reportRecipientPager.HideAdd = true;
                reportRecipientPager.HideDelete = true;
            }
        }

        if (iTN == 0)
        {
            if (_strActionMode != "view")
                divEmptyAddReportRecipient.Visible = true;
        }
        else
        {
            divEmptyAddReportRecipient.Visible = false;
        }

        return iTN;
    }
}

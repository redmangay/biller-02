﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Globalization;
using System.Data;
using System.IO;
public partial class Pages_ReportWriter_ReportFull : System.Web.UI.Page
{

    //detail
    private string _strActionMode = "view";
    private int? _iReportId;
    private string _qsReportId = "";
    private User _objUser;
    private Report _theReport;

    private bool bRunReport = false;

    protected void Page_PreRender(object sender, EventArgs e)
    {
        //if(ddlPeriodDetail.SelectedItem!=null && ddlPeriodDetail.SelectedValue== "IntervalCustom")
        //{
        //    ddlPeriodEnd.Enabled = false;
        //}
        //else
        //{
        //    ddlPeriodEnd.Enabled = true;
        //}

    }
    protected void Page_Load(object sender, EventArgs e)
    {

        //detail

        _objUser = (User)Session["User"];
        

        if (!IsPostBack)
        {
            cvDate.ValueToCompare = DateTime.Now.ToString("dd/MM/yyyy");          

            PopulatePeriodDDLDetail();
            PopulatePeriodEndDDLDetail();

        }

        if (Request.QueryString["mode"] == null)
        {
            Response.Redirect("~/Default.aspx", false);
            return;
        }
        else
        {
            string qsMode = Cryptography.Decrypt(Request.QueryString["mode"]);
            if (qsMode == "add" || qsMode == "view" || qsMode == "edit")
            {
                _strActionMode = qsMode.ToLower();
                if (Request.QueryString["ReportID"] != null)
                {
                    _qsReportId = Cryptography.Decrypt(Request.QueryString["ReportID"]);
                    _iReportId = int.Parse(_qsReportId);
                    hfReportId.Value = _iReportId.ToString();
                    _theReport = ReportManager.ets_Report_Detail((int)_iReportId);
                    if (!IsPostBack)
                    {
                        SetPageButtonsItem();
                        if (Request.QueryString["popupitem"] != null)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopItem", "  setTimeout(function () { OpenReportItem(); }, 1000); ", true);
                        }
                    }

                }
            }
            else
            {
                Response.Redirect("~/Default.aspx", false);
                return;
            }

            string strTitle = "Report Detail";
            switch (_strActionMode)
            {
                case "add":
                    strTitle = "Create Report";
                    tblSendReportTo.Visible = false;
                    tblReportItems.Visible = false;
                    lnkSaveDetail.Visible = false;
                    lnkNextStep.Visible = true;
                    this.Form.DefaultButton = lnkNextStep.UniqueID;
                    if (!IsPostBack)
                    {
                        optReportDates_SelectedIndexChanged(null, null);
                        ddlFrequencySchedule_OnSelectedIndexChanged(null, null);
                    }

                    break;
                case "view":
                    strTitle = "View Report";
                    PopulateTheRecordDetail();
                    EnableTheRecordControlsDetail(false);
                    PopulateTheRecordSchedule();
                    EnableTheRecordControlsSchedule(false);

                    if (!IsPostBack)
                        PopulateTheRecordItem();
                    EnableTheRecordControlsItem(false);

                    //divSave.Visible = false;
                    //divEdit.Visible = true;
                    break;
                case "edit":
                    strTitle = "Edit Report";
                    if (!IsPostBack)
                    {
                        PopulateTheRecordDetail();
                        PopulateTheRecordSchedule();
                        PopulateTheRecordItem();
                    }
                    break;
                default:
                    Response.Redirect("~/Default.aspx", false);
                    return;
                    break;
            }

            Title = strTitle;
            lblTitle.Text = strTitle;
        }

        //detail end


        //schedule


        if (!IsPostBack)
        {
            int itemsCount = 0;

            if (_iReportId.HasValue)
            {
                itemsCount = PopulateReportRecipient(_iReportId.Value);
            }
            else
            {
                itemsCount = PopulateReportRecipient(-1);
            }

        }
        //schedule end

        //item               

        if (!IsPostBack)
        {
            int itemsCount = 0;

            if (_iReportId.HasValue)
            if (_iReportId.HasValue)
            if (_iReportId.HasValue)
                itemsCount = PopulateReportItem(_iReportId.Value);

            PopulateTerminologyItem();

            if (_strActionMode == "edit" && itemsCount == 0 && _iReportId.HasValue)
                txtClickOnLoad.Text = hlAddReportItem.ClientID;
        }

        //item end

    }



    //Detail
    protected void PopulatePeriodDDLDetail()
    {
        ddlPeriodDetail.Items.Add(new ListItem() { Text = "30 Days before Run Date", Value = Report.ReportInterval.Interval30Days.ToString() });
        ddlPeriodDetail.Items.Add(new ListItem() { Text = "60 Days before Run Date", Value = Report.ReportInterval.Interval60Days.ToString() });
        ddlPeriodDetail.Items.Add(new ListItem() { Text = "90 Days before Run Date", Value = Report.ReportInterval.Interval90Days.ToString() });
        ddlPeriodDetail.Items.Add(new ListItem() { Text = "1 Year before Run Date", Value = Report.ReportInterval.Interval1Year.ToString() });
        ddlPeriodDetail.Items.Add(new ListItem() { Text = "YTD", Value = Report.ReportInterval.IntervalYTD.ToString() });
        ddlPeriodDetail.Items.Add(new ListItem() { Text = "Days before Run Date:", Value = Report.ReportInterval.IntervalCustom.ToString() });
        ddlPeriodDetail.Items.Add(new ListItem() { Text = "Days after Run Date:", Value = Report.ReportInterval.IntervalCustom2.ToString() });

    }

    protected void PopulatePeriodEndDDLDetail()
    {
        ddlPeriodEnd.Items.Add(new ListItem() { Text = "Run Date", Value = Report.ReportIntervalEnd.Today.ToString() });
        ddlPeriodEnd.Items.Add(new ListItem() { Text = "End of previous week", Value = Report.ReportIntervalEnd.EndOfLastWeek.ToString() });
        ddlPeriodEnd.Items.Add(new ListItem() { Text = "End of previous month", Value = Report.ReportIntervalEnd.EndOfLastMonth.ToString() });
        ddlPeriodEnd.Items.Add(new ListItem() { Text = "End of previous year", Value = Report.ReportIntervalEnd.EndOfLastYear.ToString() });
        ddlPeriodEnd.Items.Add(new ListItem() { Text = "Last data point on first sheet", Value = Report.ReportIntervalEnd.LastDataPoint.ToString() });
        ddlPeriodEnd.Items.Add(new ListItem() { Text = "Days before Run Date:", Value = Report.ReportIntervalEnd.EndOfCustom.ToString() });
        ddlPeriodEnd.Items.Add(new ListItem() { Text = "Days after Run Date:", Value = Report.ReportIntervalEnd.EndOfCustom2.ToString() });
    }

    protected bool IsUserInputOKDetail(out string message)
    {
        //this is the final server side vaidation before database action
        message = String.Empty;
        return true;
    }

    protected void PopulateTheRecordDetail()
    {
        if (_iReportId.HasValue)
        {
            try
            {
                Report report = ReportManager.ets_Report_Detail(_iReportId.Value);
                ReportSchedule reportSchedule = ReportManager.ets_ReportSchedule_Detail(_iReportId.Value);
                txtReportName.Text = report.ReportName;
                //txtReportDescription.Text = report.ReportDescription;
                if(reportSchedule!=null)
                {
                    if (reportSchedule.Frequency == ReportSchedule.ReportScheduleFrequency.Undefined)
                    {
                        optReportDates.SelectedValue = "dateOnRun";
                    }
                    else
                    {
                        optReportDates.SelectedValue = "schedule";
                    }
                }
                

                //optReportDates.SelectedValue = report.ReportDates == Report.ReportInterval.IntervalDateRange
                //    ? (report.ReportStartDate.HasValue && report.ReportEndDate.HasValue ? "dateRange" : "dateOnRun")
                //    : "period";
                optReportDates_SelectedIndexChanged(null, null);


                if (optReportDates.SelectedValue == "schedule")
                {
                    if (report.ReportDates!=null)
                        ddlPeriodDetail.SelectedValue = report.ReportDates.ToString();
                    if(report.ReportDatesBefore!=null)
                        ddlPeriodEnd.SelectedValue = report.ReportDatesBefore.ToString();

                    if (report.ReportDates == Report.ReportInterval.IntervalCustom &&
                                           report.DaysFrom.HasValue)
                    {
                        ddlPeriodDetail.SelectedValue = Report.ReportInterval.IntervalCustom.ToString();
                        ddlPeriodDetail_SelectedIndexChanged(null, null);
                        txtDaysFrom.Text = report.DaysFrom.ToString();
                    }
                    else if (report.ReportDates == Report.ReportInterval.IntervalCustom2 &&
                                           report.DaysFrom.HasValue)
                    {
                        ddlPeriodDetail.SelectedValue = Report.ReportInterval.IntervalCustom2.ToString();
                        ddlPeriodDetail_SelectedIndexChanged(null, null);
                        txtDaysFrom.Text = report.DaysFrom.ToString();
                    }

                    if (report.ReportDatesBefore == Report.ReportIntervalEnd.EndOfCustom &&
                                           report.DaysTo.HasValue)
                    {
                        ddlPeriodEnd.SelectedValue = Report.ReportIntervalEnd.EndOfCustom.ToString();
                        ddlPeriodDetail_SelectedIndexChanged(null, null);
                        txtDaysTo.Text = report.DaysTo.ToString();
                    }
                    else if (report.ReportDatesBefore == Report.ReportIntervalEnd.EndOfCustom2 &&
                                          report.DaysTo.HasValue)
                    {
                        ddlPeriodEnd.SelectedValue = Report.ReportIntervalEnd.EndOfCustom2.ToString();
                        ddlPeriodDetail_SelectedIndexChanged(null, null);
                        txtDaysTo.Text = report.DaysTo.ToString();
                    }
                }
                else
                {
                    if (report.ReportStartDate!=null)
                    {
                        txtStartDate.Text = report.ReportStartDate.Value.ToShortDateString();
                        txtStartTime.Text = report.ReportStartDate.Value.ToString("HH:mm");
                    }
                    if (report.ReportEndDate!=null)
                    {
                        txtEndDate.Text = report.ReportEndDate.Value.ToShortDateString();
                        txtEndTime.Text = report.ReportEndDate.Value.ToString("HH:mm");
                    }                 
                }

                //if (report.ReportDates != Report.ReportInterval.IntervalUndefined &&
                //    report.ReportDates != Report.ReportInterval.IntervalDateRange)
                //{
                //    //RP Modified Ticket 4513
                //    if (report.ReportDates == Report.ReportInterval.IntervalCustom &&
                //                            report.Days.HasValue)
                //    {
                //        ddlPeriodDetail.SelectedValue = Report.ReportInterval.IntervalCustom.ToString();
                //        ddlPeriodDetail_SelectedIndexChanged(null, null);
                //        txtDays.Text = report.Days.ToString();
                //    }
                //    else
                //    {
                //        ddlPeriodDetail.SelectedValue = report.ReportDates.ToString();
                //        ddlPeriodEnd.SelectedValue = report.ReportDatesBefore.ToString();
                //    }
                //    //End Modification
                //}
                //else if (report.ReportDates == Report.ReportInterval.IntervalDateRange &&
                //         report.ReportStartDate.HasValue && report.ReportEndDate.HasValue)
                //{
                //    txtStartDate.Text = report.ReportStartDate.Value.ToShortDateString();
                //    txtEndDate.Text = report.ReportEndDate.Value.ToShortDateString();
                //}

                if (report.ReportDates != Report.ReportInterval.IntervalUndefined)
                {
                    //ReportSchedule reportSchedule = ReportManager.ets_ReportSchedule_Detail(_iReportId.Value);
                    if (reportSchedule != null)
                    {
                        if (reportSchedule.Frequency != ReportSchedule.ReportScheduleFrequency.Undefined)
                        {
                            lblWarning.Text = "Report schedule will be changed to \"Not scheduled\"";
                            lblWarning.ForeColor = Color.Red;
                        }
                    }
                    else
                    {
                        lblWarning.Text = "Report schedule will be set to \"Not scheduled\"";
                        lblWarning.ForeColor = Color.Blue;
                    }
                }

                //divSave.Visible = true;

                if (_strActionMode == "edit" || (_strActionMode == "add" && _iReportId.HasValue))
                {
                    ViewState["theReport"] = report;
                }
                else if (_strActionMode == "view")
                {
                    //hlEditLink.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                    //                         "Pages/ReportWriter/ReportDetail.aspx" +
                    //                         "?mode=" + Cryptography.Encrypt("edit") +
                    //                         "&ReportID=" + Request.QueryString["ReportID"].ToString() +
                    //                         "&SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Report Detail", ex.Message, ex.StackTrace, DateTime.Now,
                    Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
                lblMsg.Text = ex.Message;
            }
        }
    }

    protected void EnableTheRecordControlsDetail(bool enable)
    {
        txtReportName.Enabled = enable;
        //txtReportDescription.Enabled = enable;
        optReportDates.Enabled = enable;
        ddlPeriodDetail.Enabled = enable;
        ddlPeriodEnd.Enabled = enable;
        txtStartDate.Enabled = enable;
        txtEndDate.Enabled = enable;
    }

    protected void lnkSaveDetail_Click(object sender, EventArgs e)
    {
        lblMsg.Text = "";

        try
        {
            string message = String.Empty;
            if (IsUserInputOKDetail(out message))
            {
                Report.ReportInterval reportInterval = Report.ReportInterval.IntervalUndefined;
                Report.ReportIntervalEnd reportIntervalEnd = Report.ReportIntervalEnd.Undefined;
                DateTime? dtDateFrom = null;
                DateTime? dtDateTo = null;

                switch (optReportDates.SelectedValue)
                {
                    case "dateOnRun":
                        reportInterval = Report.ReportInterval.IntervalDateRange;
                        if (!String.IsNullOrEmpty(txtStartDate.Text) && !String.IsNullOrEmpty(txtEndDate.Text))
                        {
                            DateTime dtTemp;
                            if (DateTime.TryParseExact(txtStartDate.Text.Trim(), Common.Dateformats,
                                new CultureInfo("en-GB"), DateTimeStyles.None, out dtTemp))
                            {
                                if (!String.IsNullOrEmpty(txtStartTime.Text))
                                {
                                    TimeSpan ts = TimeSpan.Parse(txtStartTime.Text);
                                    dtTemp = dtTemp + ts;
                                }
                                dtDateFrom = dtTemp;
                            }
                            if (DateTime.TryParseExact(txtEndDate.Text.Trim(), Common.Dateformats,
                                new CultureInfo("en-GB"), DateTimeStyles.None, out dtTemp))
                            {
                                if (!String.IsNullOrEmpty(txtEndTime.Text))
                                {
                                    TimeSpan ts = TimeSpan.Parse(txtEndTime.Text);
                                    dtTemp = dtTemp + ts;
                                }

                                dtDateTo = dtTemp;
                            }
                        }
                        break;
                    //case "dateOnRun":
                    //    reportInterval = Report.ReportInterval.IntervalDateRange;
                    //    break;
                    case "schedule":
                        reportInterval = (Report.ReportInterval)Enum.Parse(typeof(Report.ReportInterval), ddlPeriodDetail.SelectedValue);
                        reportIntervalEnd = (Report.ReportIntervalEnd)Enum.Parse(typeof(Report.ReportIntervalEnd), ddlPeriodEnd.SelectedValue);
                        break;
                }

                switch (_strActionMode)
                {
                    case "add":
                        int reportId = 0;
                        //if (_iReportId.HasValue)
                        //{
                        //    Report editReport2 = (Report)ViewState["theReport"];
                        //    editReport2.ReportName = txtReportName.Text;
                        //    //editReport2.ReportDescription = txtReportDescription.Text;
                        //    editReport2.ReportDates = reportInterval;
                        //    editReport2.ReportDatesBefore = reportIntervalEnd;
                        //    editReport2.ReportStartDate = dtDateFrom;
                        //    editReport2.ReportEndDate = dtDateTo;
                        //    editReport2.UserID = _objUser.UserID;
                        //    //RP Added Ticket 4513
                        //    if (reportInterval == Report.ReportInterval.IntervalCustom)
                        //    {
                        //        int days = 0;
                        //        Int32.TryParse(txtDays.Text, out days);
                        //        editReport2.Days = days;
                        //    }
                        //    //End Modification
                        //    ReportManager.ets_Report_Update(editReport2);
                        //    reportId = _iReportId.Value;
                        //}
                        //else
                        //{
                            int daysFrom = 0;
                            if (reportInterval == Report.ReportInterval.IntervalCustom 
                            || reportInterval == Report.ReportInterval.IntervalCustom2)
                            {
                                Int32.TryParse(txtDaysFrom.Text, out daysFrom);
                            }

                        int daysTo = 0;
                        if (reportIntervalEnd == Report.ReportIntervalEnd.EndOfCustom
                            || reportIntervalEnd == Report.ReportIntervalEnd.EndOfCustom2)
                        {
                            Int32.TryParse(txtDaysTo.Text, out daysTo);
                        }

                        Report newReport = new Report(null, AccountID,
                                txtReportName.Text, "",
                                reportInterval, reportIntervalEnd, dtDateFrom, dtDateTo,
                                null, null, _objUser.UserID, daysFrom, daysTo);

                            reportId = ReportManager.ets_Report_Insert(newReport);
                            _iReportId = reportId;
                        //}

                        if (optReportDates.SelectedValue == "dateOnRun")
                            ReportManager.ets_ReportSchedule_SetNotScheduled(reportId);

                        //Response.Redirect(Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                        //                  "Pages/ReportWriter/ReportFull.aspx" +
                        //                  "?ReportID=" + Cryptography.Encrypt(reportId.ToString()) +
                        //                  "&mode=" + Cryptography.Encrypt("edit") + //"add"
                        //                  "&SearchCriteria=" + Request.QueryString["SearchCriteria"]);

                        break;
                    case "view":
                        break;
                    case "edit":
                        Report editReport = (Report)ViewState["theReport"];
                        editReport.ReportName = txtReportName.Text;
                        //editReport.ReportDescription = txtReportDescription.Text;
                        editReport.ReportDates = reportInterval;
                        editReport.ReportDatesBefore = reportIntervalEnd;
                        editReport.ReportStartDate = dtDateFrom;
                        editReport.ReportEndDate = dtDateTo;
                        editReport.UserID = _objUser.UserID;
                        //RP Added Ticket 4513
                        if (reportInterval == Report.ReportInterval.IntervalCustom
                            || reportInterval == Report.ReportInterval.IntervalCustom2)
                        {
                            int days2From = 0;
                            Int32.TryParse(txtDaysFrom.Text, out days2From);
                            editReport.DaysFrom = days2From;
                        }
                        //End Modification

                        if (reportIntervalEnd == Report.ReportIntervalEnd.EndOfCustom
                           || reportIntervalEnd == Report.ReportIntervalEnd.EndOfCustom2)
                        {
                            int days2To = 0;
                            Int32.TryParse(txtDaysTo.Text, out days2To);
                            editReport.DaysTo = days2To;
                        }

                        ReportManager.ets_Report_Update(editReport);
                        if (optReportDates.SelectedValue == "dateOnRun" && editReport.ReportID.HasValue)
                            ReportManager.ets_ReportSchedule_SetNotScheduled(editReport.ReportID.Value);

                        //if (Request.QueryString["popup"] != null)
                        //{
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Refresh", "SavedAndRefresh();", true);
                        //}
                        //else
                        //{
                        //    string navigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                        //                         "Pages/ReportWriter/Report.aspx" +
                        //                         "?SearchCriteria=" + Request.QueryString["SearchCriteria"];
                        //    //Response.Redirect(navigateUrl);
                        //}
                        break;

                    default:
                        //?
                        break;
                }

                lnkSaveSchedule_Click(null, null);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "error", String.Format("alert('{0}');", message), true);
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Report Detail", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }

    public int AccountID
    {
        get
        {
            int retVal = 0;
            if (Session["AccountID"] != null)
                retVal = Convert.ToInt32(Session["AccountID"]);
            return retVal;
        }
    }

    protected void optReportDates_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (optReportDates.SelectedValue == "dateOnRun")
        {
            panelReportDates.Visible = false;
            trRunDate.Visible = false;
            rfvRunDate.Enabled = false;
            trReportPeriod.Visible = false;
            trReportPeriodEnd.Visible = false;
            trDateStart.Visible = true;
            //rfvDateFrom.Enabled = false;
            trDateEnd.Visible = true; 
            //rfvDateTo.Enabled = false;
            tblSchedule.Visible = false;
            tblSendReportTo.Visible = false;

            ddlFrequencySchedule.SelectedValue = ReportSchedule.ReportScheduleFrequency.Undefined.ToString();
            ddlFrequencySchedule_OnSelectedIndexChanged(null, null);
            //strRunReport.InnerText = "Run Report";
            lnkRunReport.Visible = false;
            lnkRunReportDate.Visible = true;
            imgRunReport.Visible = true;
        }
        else
        {
            lnkRunReport.Visible = true;
            lnkRunReportDate.Visible = false;
            panelReportDates.Visible = true;
            trRunDate.Visible = true;
            rfvRunDate.Enabled = true;
            trReportPeriod.Visible = true;
            trReportPeriodEnd.Visible = true;
            trDateStart.Visible = false;
            //rfvDateFrom.Enabled = false;
            trDateEnd.Visible = false;
            //rfvDateTo.Enabled = false;
            tblSchedule.Visible = true;
            if(_strActionMode=="add")
            {
                tblSendReportTo.Visible = false;
            }
            else
            {
                tblSendReportTo.Visible = true;
            }
            
            if(sender!=null)
            {
                if(ddlFrequencySchedule.SelectedValue == ReportSchedule.ReportScheduleFrequency.Undefined.ToString())
                {
                    //user clicked schedule option so let's make daily/weekly as default.
                    ddlFrequencySchedule.SelectedValue = ReportSchedule.ReportScheduleFrequency.Weekly.ToString();
                    ddlFrequencySchedule_OnSelectedIndexChanged(null, null);
                }
            }
            strRunReport.InnerText = "Save";
            imgRunReport.Visible = false;
        }

        //trReportPeriod.Visible = optReportDates.SelectedValue == "period";
        //trReportPeriodEnd.Visible = optReportDates.SelectedValue == "period";
        //RP Added Ticket 4513
        //trDays.Visible = optReportDates.SelectedValue == "period";
        ddlPeriodDetail_SelectedIndexChanged(null, null);
        //End Modification
        //trDateStart.Visible = optReportDates.SelectedValue == "dateRange";
        //trDateEnd.Visible = optReportDates.SelectedValue == "dateRange";
        //rfvDateFrom.Enabled = optReportDates.SelectedValue == "dateRange";
        //rfvDateTo.Enabled = optReportDates.SelectedValue == "dateRange";
        //trWarning.Visible = optReportDates.SelectedValue == "dateOnRun";
    }

    protected void lbBackDetail_OnClick(object sender, EventArgs e)
    {
        if (Request.QueryString["popup"] != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "OnClose", "Close();", true);
        }
        else
        {
            string navigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                 "Pages/ReportWriter/Report.aspx" +
                                 "?SearchCriteria=" + Request.QueryString["SearchCriteria"];
            Response.Redirect(navigateUrl);
        }
    }
    //RP Added Ticket 4513
    protected void ddlPeriodDetail_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //trDays.Visible = (Report.ReportInterval)Enum.Parse(typeof(Report.ReportInterval), ddlPeriodDetail.SelectedValue) == Report.ReportInterval.IntervalCustom && optReportDates.SelectedValue == "schedule";
            tdDaysFrom.Visible = (Report.ReportInterval)Enum.Parse(typeof(Report.ReportInterval), ddlPeriodDetail.SelectedValue) == Report.ReportInterval.IntervalCustom
                || (Report.ReportInterval)Enum.Parse(typeof(Report.ReportInterval), ddlPeriodDetail.SelectedValue) == Report.ReportInterval.IntervalCustom2
                && optReportDates.SelectedValue == "schedule";

            tdDaysTo.Visible = (Report.ReportIntervalEnd)Enum.Parse(typeof(Report.ReportIntervalEnd), ddlPeriodEnd.SelectedValue) == Report.ReportIntervalEnd.EndOfCustom
                || (Report.ReportIntervalEnd)Enum.Parse(typeof(Report.ReportIntervalEnd), ddlPeriodEnd.SelectedValue) == Report.ReportIntervalEnd.EndOfCustom2
                && optReportDates.SelectedValue == "schedule";

        }
        catch
        {
            //
        }

    }


    //Detail end




    //schedule

    protected bool IsUserInputOKSchedule(out string message)
    {
        //this is the final server side vaidation before database action
        message = String.Empty;
        bool rv = true;

        if(optReportDates.SelectedValue== "schedule")
        {
            DateTime dt;
            if (!DateTime.TryParse(txtDate.Text, out dt))
            {
                message = "Invalid Date.";
                rv = false;
            }
            TimeSpan ts;
            if (!String.IsNullOrEmpty(txtTime.Text) && !TimeSpan.TryParse(txtTime.Text, out ts))
            {
                if (!String.IsNullOrEmpty(message))
                    message += " ";
                message += "Invalid Time.";
                rv = false;
            }
        }

       

        return rv;
    }

    protected void PopulateTheRecordSchedule()
    {
        if (_iReportId.HasValue)
        {
            try
            {
                Report report = ReportManager.ets_Report_Detail(_iReportId.Value);
                //lblReportName.Text = report.ReportName;

                ReportSchedule reportSchedule = ReportManager.ets_ReportSchedule_Detail(_iReportId.Value);
                if (reportSchedule == null)
                {
                    ddlRepeatEvery.SelectedValue = ReportSchedule.ReportScheduleFrequency.OneOff.ToString();
                    txtDate.Text = DateTime.Today.AddDays(1).ToString("d");
                    txtTime.Text = "00:00";
                    switch (DateTime.Today.AddDays(1).DayOfWeek)
                    {
                        case DayOfWeek.Monday:
                            cbMonday.Checked = true;
                            break;
                        case DayOfWeek.Tuesday:
                            cbTuesday.Checked = true;
                            break;
                        case DayOfWeek.Wednesday:
                            cbWednesday.Checked = true;
                            break;
                        case DayOfWeek.Thursday:
                            cbThursday.Checked = true;
                            break;
                        case DayOfWeek.Friday:
                            cbFriday.Checked = true;
                            break;
                        case DayOfWeek.Saturday:
                            cbSaturday.Checked = true;
                            break;
                        case DayOfWeek.Sunday:
                            cbSunday.Checked = true;
                            break;
                    }
                }
                else
                {
                    switch (reportSchedule.Frequency)
                    {
                        case ReportSchedule.ReportScheduleFrequency.Undefined:
                            ddlFrequencySchedule.SelectedValue = ReportSchedule.ReportScheduleFrequency.Undefined.ToString();
                            break;
                        case ReportSchedule.ReportScheduleFrequency.OneOff:
                            ddlFrequencySchedule.SelectedValue = ReportSchedule.ReportScheduleFrequency.OneOff.ToString();
                            if (reportSchedule.RunDateTime.HasValue)
                            {
                                txtDate.Text = reportSchedule.RunDateTime.Value.ToString("d");
                                txtTime.Text = reportSchedule.RunDateTime.Value.ToString("HH:mm");
                            }
                            else
                            {
                                txtDate.Text = DateTime.Today.AddDays(1).ToString("d");
                                txtTime.Text = "00:00";
                            }
                            break;
                        case ReportSchedule.ReportScheduleFrequency.Weekly:
                            ddlFrequencySchedule.SelectedValue = ReportSchedule.ReportScheduleFrequency.Weekly.ToString();
                            if (reportSchedule.RunDateTime.HasValue)
                            {
                                txtDate.Text = reportSchedule.RunDateTime.Value.ToString("d");
                                txtTime.Text = reportSchedule.RunDateTime.Value.ToString("HH:mm");
                            }
                            else
                            {
                                txtDate.Text = DateTime.Today.AddDays(1).ToString("d");
                                txtTime.Text = "00:00";
                            }
                            ddlRepeatEvery.SelectedValue = reportSchedule.RepeatEvery.ToString();
                            cbMonday.Checked = reportSchedule.WeeklyMonday;
                            cbTuesday.Checked = reportSchedule.WeeklyTuesday;
                            cbWednesday.Checked = reportSchedule.WeeklyWednesday;
                            cbThursday.Checked = reportSchedule.WeeklyThurdsay;
                            cbFriday.Checked = reportSchedule.WeeklyFriday;
                            cbSaturday.Checked = reportSchedule.WeeklySaturday;
                            cbSunday.Checked = reportSchedule.WeeklySunday;
                            break;
                        case ReportSchedule.ReportScheduleFrequency.Monthly:
                            ddlFrequencySchedule.SelectedValue = ReportSchedule.ReportScheduleFrequency.Monthly.ToString();
                            if (reportSchedule.RunDateTime.HasValue)
                            {
                                txtDate.Text = reportSchedule.RunDateTime.Value.ToString("d");
                                txtTime.Text = reportSchedule.RunDateTime.Value.ToString("HH:mm");
                            }
                            else
                            {
                                txtDate.Text = DateTime.Today.AddDays(1).ToString("d");
                                txtTime.Text = "00:00";
                            }
                            ddlRepeatEvery.SelectedValue = reportSchedule.RepeatEvery.ToString();
                            rblRepeatOnMonth.SelectedValue = reportSchedule.MonthlyMode.ToString();
                            break;
                    }
                }

                string s = String.Empty;
                if (report.ReportDates == Report.ReportInterval.IntervalDateRange &&
                    (!report.ReportStartDate.HasValue || !report.ReportEndDate.HasValue))
                {
                    ddlFrequencySchedule.SelectedValue = ReportSchedule.ReportScheduleFrequency.Undefined.ToString();
                    //ddlFrequencySchedule.Enabled = false;
                    s = "Report cannot be scheduled when its dates are set to \"Enter when running report\"";
                }

                SetRowsSchedule();
                SetSummarySchedule(s);
                SetNextRunDateListSchedule();

                //if (_strActionMode == "view")
                //{
                //    hlEditLink.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                //                             Request.ApplicationPath +
                //                             "Pages/ReportWriter/ReportSchedule.aspx" +
                //                             "?mode=" + Cryptography.Encrypt("edit") +
                //                             "&ReportID=" + Request.QueryString["ReportID"].ToString() +
                //                             "&SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString();
                //}
            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Report Detail", ex.Message, ex.StackTrace, DateTime.Now,
                    Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
                lblMsg.Text = ex.Message;
            }
        }
    }

    protected void EnableTheRecordControlsSchedule(bool enable)
    {
        ddlFrequencySchedule.Enabled = enable;
        txtDate.Enabled = enable;
        txtTime.Enabled = enable;
        ddlRepeatEvery.Enabled = enable;
        cbMonday.Enabled = enable;
        cbTuesday.Enabled = enable;
        cbWednesday.Enabled = enable;
        cbThursday.Enabled = enable;
        cbFriday.Enabled = enable;
        cbSaturday.Enabled = enable;
        cbSunday.Enabled = enable;
        rblRepeatOnMonth.Enabled = enable;
    }
    protected void RedirectReportDetailPage()
    {
        string navigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                           "Pages/ReportWriter/ReportFull.aspx" +
                           "?ReportID=" + Cryptography.Encrypt(_iReportId.ToString()) + //Request.QueryString["ReportID"] +
                           "&mode=" + Cryptography.Encrypt("edit") +
                           "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
        if (_strActionMode == "add")
        {
           //
        }
        else
        {
            navigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                 "Pages/ReportWriter/Report.aspx" +
                                 "?SearchCriteria=" + Request.QueryString["SearchCriteria"];
           
        }
        Response.Redirect(navigateUrl,false);
    }
    protected void lnkSaveSchedule_Click(object sender, EventArgs e)
    {
        lblMsg.Text = "";
       

        try
        {
            string message = String.Empty;
            if (IsUserInputOKSchedule(out message))
            {
                if (_strActionMode != "view" && _iReportId.HasValue)
                {
                    DateTime dt = new DateTime();
                    ReportSchedule.ReportScheduleFrequency frequency = ReportSchedule.ReportScheduleFrequency.Undefined;
                    Enum.TryParse(ddlFrequencySchedule.SelectedValue, out frequency);
                    if (frequency != ReportSchedule.ReportScheduleFrequency.Undefined)
                    {
                        dt = DateTime.Parse(txtDate.Text);
                        if (!String.IsNullOrEmpty(txtTime.Text))
                        {
                            TimeSpan ts = TimeSpan.Parse(txtTime.Text);
                            dt = dt + ts;
                        }
                    }
                    switch (frequency)
                    {
                        case ReportSchedule.ReportScheduleFrequency.Undefined:
                            ReportManager.ets_ReportSchedule_SetNotScheduled(_iReportId.Value);
                            break;
                        case ReportSchedule.ReportScheduleFrequency.OneOff:
                            ReportManager.ets_ReportSchedule_SetOneOffSchedule(_iReportId.Value, dt);
                            break;
                        case ReportSchedule.ReportScheduleFrequency.Weekly:
                            if (!cbMonday.Checked && !cbThursday.Checked && !cbWednesday.Checked &&
                                !cbThursday.Checked && !cbFriday.Checked && !cbSaturday.Checked && !cbSunday.Checked)
                            {
                                switch (dt.DayOfWeek)
                                {
                                    case DayOfWeek.Monday:
                                        cbMonday.Checked = true;
                                        break;
                                    case DayOfWeek.Tuesday:
                                        cbTuesday.Checked = true;
                                        break;
                                    case DayOfWeek.Wednesday:
                                        cbWednesday.Checked = true;
                                        break;
                                    case DayOfWeek.Thursday:
                                        cbThursday.Checked = true;
                                        break;
                                    case DayOfWeek.Friday:
                                        cbFriday.Checked = true;
                                        break;
                                    case DayOfWeek.Saturday:
                                        cbSaturday.Checked = true;
                                        break;
                                    case DayOfWeek.Sunday:
                                        cbSunday.Checked = true;
                                        break;
                                }
                            }
                            ReportManager.ets_ReportSchedule_SetWeeklySchedule(_iReportId.Value, dt,
                                int.Parse(ddlRepeatEvery.SelectedValue),
                                cbMonday.Checked, cbTuesday.Checked, cbWednesday.Checked, cbThursday.Checked,
                                cbFriday.Checked, cbSaturday.Checked, cbSunday.Checked);
                            break;
                        case ReportSchedule.ReportScheduleFrequency.Monthly:
                            ReportSchedule.ReportScheduleMonthlyMode mode =
                                ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth;
                            Enum.TryParse(rblRepeatOnMonth.SelectedValue, out mode);
                            switch (mode)
                            {
                                case ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth:
                                    ReportManager.ets_ReportSchedule_SetMonthlySchedule(_iReportId.Value, dt,
                                        int.Parse(ddlRepeatEvery.SelectedValue), dt.Day);
                                    break;
                                case ReportSchedule.ReportScheduleMonthlyMode.DayOfWeek:
                                    ReportManager.ets_ReportSchedule_SetMonthlySchedule(_iReportId.Value, dt,
                                        int.Parse(ddlRepeatEvery.SelectedValue), (dt.Day - 1) / 7 + 1, dt.DayOfWeek);
                                    break;
                            }
                            break;
                    }
                }

                if(bRunReport==false)
                    RedirectReportDetailPage();



            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "error",
                    String.Format("alert('{0}');", message), true);
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Report Detail", ex.Message, ex.StackTrace, DateTime.Now,
                Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }

    //public int AccountID
    //{
    //    get
    //    {
    //        int retVal = 0;
    //        if (Session["AccountID"] != null)
    //            retVal = Convert.ToInt32(Session["AccountID"]);
    //        return retVal;
    //    }
    //}

    protected void lbBackSchedule_OnClick(object sender, EventArgs e)
    {
        if (Request.QueryString["popup"] != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "OnClose", "Close();", true);
        }
        else
        {
            string navigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                 "Pages/ReportWriter/ReportFull.aspx" +
                                 "?ReportID=" + Cryptography.Encrypt(_iReportId.ToString()) +
                                 "&mode=" + Cryptography.Encrypt("edit") +
                                 "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
            Response.Redirect(navigateUrl);
        }
    }

    protected void ddlFrequencySchedule_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        SetRowsSchedule();
        SetSummarySchedule(null);
        SetNextRunDateListSchedule();
    }


    protected void OnScheduleChangedSchedule(object sender, EventArgs e)
    {
        SetSummarySchedule(null);
        SetNextRunDateListSchedule();
    }

    private void SetRowsSchedule()
    {
        ReportSchedule.ReportScheduleFrequency frequency = ReportSchedule.ReportScheduleFrequency.Undefined;
        Enum.TryParse(ddlFrequencySchedule.SelectedValue, out frequency);
        switch (frequency)
        {
            case ReportSchedule.ReportScheduleFrequency.Undefined:
                //trDate.Visible = false;
                trRepeatEvery.Visible = false;
                trRepeatOnWeeek.Visible = false;
                trRepeatOnMonth.Visible = false;
                hlScheduledDates.Visible = false;
                strRepeatEvery.InnerText = "Every";
                break;
            case ReportSchedule.ReportScheduleFrequency.OneOff:
                //trDate.Visible = true;
                //lblDate.Text = "Date";
                trRepeatEvery.Visible = false;
                trRepeatOnWeeek.Visible = false;
                trRepeatOnMonth.Visible = false;
                hlScheduledDates.Visible = false;
                strRepeatEvery.InnerText = "Every";
                break;
            case ReportSchedule.ReportScheduleFrequency.Weekly:
                //trDate.Visible = true;
                //lblDate.Text = "Next run on";
                trRepeatEvery.Visible = true;
                lblRepeatInterval.Text = "weeks";
                trRepeatOnWeeek.Visible = true;
                trRepeatOnMonth.Visible = false;

                if (_iReportId.HasValue)
                    hlScheduledDates.Visible = true;
                else
                    hlScheduledDates.Visible = false;

                strRepeatEvery.InnerText = "Every";
                break;
            case ReportSchedule.ReportScheduleFrequency.Monthly:
                //trDate.Visible = true;
                //lblDate.Text = "Next run on";
                trRepeatEvery.Visible = true;
                lblRepeatInterval.Text = "months";
                trRepeatOnWeeek.Visible = false;
                trRepeatOnMonth.Visible = true;
                if (_iReportId.HasValue)
                    hlScheduledDates.Visible = true;
                else
                    hlScheduledDates.Visible = false;

                strRepeatEvery.InnerText = "Repeat Every";
                break;
        }
    }

    private void SetSummarySchedule(string summary)
    {
        string s;
        if (!String.IsNullOrEmpty(summary))
        {
            s = summary;
            lblSummary.ForeColor = Color.Red;
        }
        else
        {
            ReportSchedule.ReportScheduleFrequency frequency = ReportSchedule.ReportScheduleFrequency.Undefined;
            Enum.TryParse(ddlFrequencySchedule.SelectedValue, out frequency);
            s = "Not scheduled to run";
            switch (frequency)
            {
                case ReportSchedule.ReportScheduleFrequency.Undefined:
                    break;
                case ReportSchedule.ReportScheduleFrequency.OneOff:
                    s = String.Concat("No Repeating ", String.IsNullOrEmpty(txtDate.Text) ? "(undefined)" : txtDate.Text,
                        String.IsNullOrEmpty(txtTime.Text) ? "" : (" " + txtTime.Text));
                    break;
                case ReportSchedule.ReportScheduleFrequency.Weekly:
                    string s1 = ddlRepeatEvery.SelectedValue == "1"
                        ? "Weekly"
                        : String.Concat("Every ", ddlRepeatEvery.SelectedValue, " weeks");
                    string s2 = "";
                    int i = 0;
                    if (cbMonday.Checked)
                    {
                        s2 = String.Concat(s2, "Monday, ");
                        i++;
                    }
                    if (cbTuesday.Checked)
                    {
                        s2 = String.Concat(s2, "Tuesday, ");
                        i++;
                    }
                    if (cbWednesday.Checked)
                    {
                        s2 = String.Concat(s2, "Wednesday, ");
                        i++;
                    }
                    if (cbThursday.Checked)
                    {
                        s2 = String.Concat(s2, "Thursday, ");
                        i++;
                    }
                    if (cbFriday.Checked)
                    {
                        s2 = String.Concat(s2, "Friday, ");
                        i++;
                    }
                    if (cbSaturday.Checked)
                    {
                        s2 = String.Concat(s2, "Saturday, ");
                        i++;
                    }
                    if (cbSunday.Checked)
                    {
                        s2 = String.Concat(s2, "Sunday, ");
                        i++;
                    }
                    if (i == 0)
                    {
                        switch (DateTime.Today.DayOfWeek)
                        {
                            case DayOfWeek.Monday:
                                s2 = "Monday";
                                break;
                            case DayOfWeek.Tuesday:
                                s2 = "Tuesday";
                                break;
                            case DayOfWeek.Wednesday:
                                s2 = "Wednesday";
                                break;
                            case DayOfWeek.Thursday:
                                s2 = "Thursday";
                                break;
                            case DayOfWeek.Friday:
                                s2 = "Friday";
                                break;
                            case DayOfWeek.Saturday:
                                s2 = "Saturday";
                                break;
                            case DayOfWeek.Sunday:
                                s2 = "Sunday";
                                break;
                        }
                    }
                    else
                    {
                        if (i == 7)
                            s2 = "all days";
                        else
                        {
                            s2 = s2.Substring(0, s2.Length - 2);
                        }
                    }
                    s = String.Concat(s1, " on ", s2);
                    break;
                case ReportSchedule.ReportScheduleFrequency.Monthly:
                    string s3 = ddlRepeatEvery.SelectedValue == "1"
                        ? "Monthly"
                        : String.Concat("Every ", ddlRepeatEvery.SelectedValue, " months");
                    string s4 = "";
                    DateTime dt;
                    if (!String.IsNullOrEmpty(txtDate.Text) && DateTime.TryParse(txtDate.Text, out dt))
                    {
                        ReportSchedule.ReportScheduleMonthlyMode mode =
                            ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth;
                        Enum.TryParse(rblRepeatOnMonth.SelectedValue, out mode);
                        switch (mode)
                        {
                            case ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth:
                                if (dt.Day == 31)
                                    s4 = "the last day of month";
                                else
                                    s4 = String.Format("day {0}", dt.Day);
                                break;
                            case ReportSchedule.ReportScheduleMonthlyMode.DayOfWeek:
                                switch ((dt.Day - 1) / 7)
                                {
                                    case 0:
                                        s4 = "the first ";
                                        break;
                                    case 1:
                                        s4 = "the second ";
                                        break;
                                    case 2:
                                        s4 = "the third ";
                                        break;
                                    case 3:
                                        s4 = "the fourth ";
                                        break;
                                    case 4:
                                        s4 = "the last ";
                                        break;
                                }
                                switch (dt.DayOfWeek)
                                {
                                    case DayOfWeek.Monday:
                                        s4 += "Monday";
                                        break;
                                    case DayOfWeek.Tuesday:
                                        s4 += "Tuesday";
                                        break;
                                    case DayOfWeek.Wednesday:
                                        s4 += "Wednesday";
                                        break;
                                    case DayOfWeek.Thursday:
                                        s4 += "Thursday";
                                        break;
                                    case DayOfWeek.Friday:
                                        s4 += "Friday";
                                        break;
                                    case DayOfWeek.Saturday:
                                        s4 += "Saturday";
                                        break;
                                    case DayOfWeek.Sunday:
                                        s4 += "Sunday";
                                        break;
                                }
                                break;
                        }
                        s = String.Concat(s3, " on ", s4);
                    }
                    else
                    {
                        s = s3;
                    }
                    break;
            }
            lblSummary.ForeColor = Color.Black;
        }
        lblSummary.Text = s;
    }

    private void SetNextRunDateListSchedule()
    {
        System.Collections.Generic.List<DateTime> nextRuns = null;

        if (!String.IsNullOrEmpty(txtDate.Text))
        {
            ReportSchedule.ReportScheduleFrequency frequency = ReportSchedule.ReportScheduleFrequency.Undefined;
            Enum.TryParse(ddlFrequencySchedule.SelectedValue, out frequency);
            DateTime dt = new DateTime();
            if (frequency != ReportSchedule.ReportScheduleFrequency.Undefined)
            {
                dt = DateTime.Parse(txtDate.Text);
                if (!String.IsNullOrEmpty(txtTime.Text))
                {
                    TimeSpan ts = TimeSpan.Parse(txtTime.Text);
                    dt = dt + ts;
                }
            }

            switch (frequency)
            {
                case ReportSchedule.ReportScheduleFrequency.Weekly:
                    bool onMonday = cbMonday.Checked;
                    bool onTuesday = cbTuesday.Checked;
                    bool onWednesday = cbWednesday.Checked;
                    bool onThursday = cbThursday.Checked;
                    bool onFriday = cbFriday.Checked;
                    bool onSaturday = cbSaturday.Checked;
                    bool onSunday = cbSunday.Checked;
                    if (!onMonday && !onTuesday && !onWednesday && !onThursday && !onFriday && !onSaturday && !onSunday)
                    {
                        switch (DateTime.Today.DayOfWeek)
                        {
                            case DayOfWeek.Monday:
                                onMonday = true;
                                break;
                            case DayOfWeek.Tuesday:
                                onTuesday = true;
                                break;
                            case DayOfWeek.Wednesday:
                                onWednesday = true;
                                break;
                            case DayOfWeek.Thursday:
                                onThursday = true;
                                break;
                            case DayOfWeek.Friday:
                                onFriday = true;
                                break;
                            case DayOfWeek.Saturday:
                                onSaturday = true;
                                break;
                            case DayOfWeek.Sunday:
                                onSunday = true;
                                break;
                        }
                    }

                    nextRuns = ReportManager.ets_ReportSchedule_GetScheduledDates(DateTime.Now, dt,
                        int.Parse(ddlRepeatEvery.SelectedValue), onMonday,
                        onTuesday, onWednesday, onThursday, onFriday, onSaturday, onSunday, 10);

                    break;
                case ReportSchedule.ReportScheduleFrequency.Monthly:
                    ReportSchedule.ReportScheduleMonthlyMode mode = ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth;
                    Enum.TryParse(rblRepeatOnMonth.SelectedValue, out mode);
                    switch (mode)
                    {
                        case ReportSchedule.ReportScheduleMonthlyMode.DayOfMonth:
                            nextRuns = ReportManager.ets_ReportSchedule_GetScheduledDates(DateTime.Now, dt,
                                int.Parse(ddlRepeatEvery.SelectedValue), dt.Day, 10);
                            break;
                        case ReportSchedule.ReportScheduleMonthlyMode.DayOfWeek:
                            nextRuns = ReportManager.ets_ReportSchedule_GetScheduledDates(DateTime.Now, dt,
                                int.Parse(ddlRepeatEvery.SelectedValue), (dt.Day - 1) / 7 + 1, dt.DayOfWeek, 10);
                            break;
                    }
                    break;
            }
        }

        if (nextRuns != null)
        {
            System.Collections.Generic.List<string> nextRunsString = new System.Collections.Generic.List<string>();
            CultureInfo culture = CultureInfo.CreateSpecificCulture("en-AU");
            foreach (DateTime nextRun in nextRuns)
            {
                nextRunsString.Add(nextRun.ToString("f", culture));
            }
            repeater10Runs.DataSource = nextRunsString;
            repeater10Runs.DataBind();
        }
    }

    protected void grdRepotRecipient_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "deletetype")
        {
            try
            {                

                RecordManager.ets_TableUser_Delete(Convert.ToInt32(e.CommandArgument));
                Common.ExecuteText("DELETE ReportRecipient WHERE ReportRecipientID =" + e.CommandArgument);
                if (_iReportId.HasValue)
                    PopulateReportRecipient(_iReportId.Value);

            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Report Recipient delete", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
                //ScriptManager.RegisterClientScriptBlock(grdTable, typeof(Page), "msg_delete", "alert('Delete failed!');", true);
            }
        }
    }

    protected void grdReportRecipient_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton ib = (ImageButton)e.Row.FindControl("imgbtnDelete");
                ib.Attributes.Add("onclick", "javascript:return " +
                "confirm('Are you sure you want to remove "+ DataBinder.Eval(e.Row.DataItem, "Email") + " recipient?');");
            }
        }
        catch
        {

        }
    }


    protected void btnRefreshReportRecipient_Click(object sender, EventArgs e)
    {
        if (_iReportId.HasValue)
            PopulateReportRecipient(_iReportId.Value);
    }

    protected void ReportRecipientPager_BindTheGridAgain(object sender, EventArgs e)
    {
    }

    //protected void ReportRecipientPager_DeleteAction(object sender, EventArgs e)
    //{
    //    string sCheck = "";

    //    for (int i = 0; i < grdReportRecipient.Rows.Count; i++)
    //    {
    //        bool ischeck = ((CheckBox)grdReportRecipient.Rows[i].FindControl("chkDelete")).Checked;
    //        if (ischeck)
    //        {
    //            sCheck = sCheck + ((Label)grdReportRecipient.Rows[i].FindControl("LblID")).Text + ",";
    //        }
    //    }
    //    if (string.IsNullOrEmpty(sCheck))
    //    {
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "message_alert", "alert('Please select a record.');", true);
    //        return;
    //    }

    //    sCheck = sCheck + "-1";
    //    Common.ExecuteText("DELETE ReportRecipient WHERE ReportRecipientID IN (" + sCheck + ") ");
    //    if (_iReportId.HasValue)
    //        PopulateReportRecipient(_iReportId.Value);
    //}

    public string GetAddReportRecipientURL(int reportId)
    {
        return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
               "/Pages/ReportWriter/AddReportRecipient.aspx?mode=" + Cryptography.Encrypt("add") + "&ReportID=" +
               Cryptography.Encrypt(reportId.ToString());
    }

    protected int PopulateReportRecipient(int reportId)
    {
        int iTN = 0;

        hlAddReportRecipient.NavigateUrl = GetAddReportRecipientURL(reportId);
        hlAddReportRecipient2.NavigateUrl = hlAddReportRecipient.NavigateUrl;
        System.Data.DataTable dtReportRecipients = Common.DataTableFromText("SELECT * FROM ReportRecipient WHERE ReportID =" + reportId.ToString());

        grdReportRecipient.DataSource = dtReportRecipients;
        iTN = dtReportRecipients.Rows.Count;
        grdReportRecipient.VirtualItemCount = iTN;
        grdReportRecipient.DataBind();

        if (_strActionMode == "view")
        {
            grdReportRecipient.Columns[2].Visible = false;
        }

        if (grdReportRecipient.TopPagerRow != null)
            grdReportRecipient.TopPagerRow.Visible = true;

        GridViewRow gvr = grdReportRecipient.TopPagerRow;

        if (gvr != null)
        {
            Common_Pager reportRecipientPager = (Common_Pager)gvr.FindControl("ReportRecipientPager");
            reportRecipientPager.AddURL = GetAddReportRecipientURL(reportId);
            reportRecipientPager.HyperAdd_CSS = "popuplinkRR";
            reportRecipientPager.AddToolTip = "Add";
            reportRecipientPager.TotalRows = iTN;

            if (_strActionMode == "view")
            {
                reportRecipientPager.HideAdd = true;
                reportRecipientPager.HideDelete = true;
            }
        }

        if (iTN == 0)
        {
            if (_strActionMode != "view")
                divEmptyAddReportRecipient.Visible = true;
        }
        else
        {
            divEmptyAddReportRecipient.Visible = false;
        }

        return iTN;
    }


    //schedule end


    //item
    protected void PopulateTerminologyItem()
    {
    }

    protected void PopulateSearchCriteriaItem(int iSearchCriteriaID)
    {
        try
        {
            SearchCriteria theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);
            if (theSearchCriteria != null)
            {
                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.Load(new StringReader(theSearchCriteria.SearchText));
            }
            else
            {
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
    }

    protected void ReportItemPager_DeleteAction(object sender, EventArgs e)
    {
        string sCheck = "";

        for (int i = 0; i < grdReportItem.Rows.Count; i++)
        {
            bool ischeck = ((CheckBox)grdReportItem.Rows[i].FindControl("chkDelete")).Checked;
            if (ischeck)
            {
                sCheck = sCheck + ((Label)grdReportItem.Rows[i].FindControl("LblID")).Text + ",";
            }
        }
        if (string.IsNullOrEmpty(sCheck))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message_alert", "alert('Please select a record.');", true);
            return;
        }

        sCheck = sCheck + "-1";
        Common.ExecuteText("DELETE ReportItem WHERE ReportItemID IN (" + sCheck + ") ");
        if (_iReportId.HasValue)
            PopulateReportItem(_iReportId.Value);
    }

    protected void btnReportItemIDForItemPosition_Click(object sender, EventArgs e)
    {
        if (hfReportItemIDForItemPosition.Value != "")
        {
            try
            {
                string strNewVIT = hfReportItemIDForItemPosition.Value.Substring(0, hfReportItemIDForItemPosition.Value.Length - 1);
                string[] newVT = strNewVIT.Split(',');
                DataTable dtDO = Common.DataTableFromText("SELECT ItemPosition, ReportItemID FROM [ReportItem] WHERE ReportItemID IN (" + strNewVIT + ") ORDER BY ItemPosition");
                if (newVT.Length == dtDO.Rows.Count)
                {
                    for (int i = 0; i < newVT.Length; i++)
                    {
                        Common.ExecuteText("UPDATE [ReportItem] SET ItemPosition = " + i.ToString() + " WHERE ReportItemID = " + newVT[i]);
                    }
                }
            }
            catch (Exception)
            {
                //
            }

            if (_iReportId.HasValue)
                PopulateReportItem(_iReportId.Value);
        }
    }


    public string GetAddReportItemURL(int reportId)
    {
        return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
               "/Pages/ReportWriter/AddReportItem.aspx?mode=" + Cryptography.Encrypt("add") + "&ReportID=" +
               Cryptography.Encrypt(reportId.ToString());
    }

    public string GetEditURLItem(string objectID)
    {
        string strURL = "";
        if (Request.QueryString["SearchCriteria"] != null)
        {
            strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                     "Pages/ReportWriter/ReportTableDetail.aspx" +
                     "?mode=" + Request.QueryString["mode"] +
                     "&SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString() +
                     "&ReportID=" + Cryptography.Encrypt(_iReportId.ToString()) +
                     "&ReportItemID=" + Cryptography.Encrypt(objectID);
        }
        else
        {
            strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                     "Pages/ReportWriter/ReportTableDetail.aspx" +
                     "?mode=" + Request.QueryString["mode"] +
                     "&ReportID=" + Cryptography.Encrypt(_iReportId.ToString()) +
                     "&ReportItemID=" + Cryptography.Encrypt(objectID);
        }
        return strURL;
    }

    protected int PopulateReportItem(int reportId)
    {
        int iTN = 0;

        hlAddReportItem.NavigateUrl = GetAddReportItemURL(reportId);
        hlAddReportItem2.NavigateUrl = hlAddReportItem.NavigateUrl;
        DataTable dtReportItems = Common.DataTableFromText("SELECT * FROM ReportItem WHERE ReportID =" + reportId.ToString() + " ORDER BY ItemPosition");
       
        grdReportItem.DataSource = dtReportItems;
        iTN = dtReportItems.Rows.Count;
        grdReportItem.VirtualItemCount = iTN;
        grdReportItem.DataBind();

        if (_strActionMode == "view")
        {
            grdReportItem.Columns[2].Visible = false;
            grdReportItem.Columns[3].Visible = false;
        }

        if (grdReportItem.TopPagerRow != null)
            grdReportItem.TopPagerRow.Visible = true;

        GridViewRow gvr = grdReportItem.TopPagerRow;

        if (gvr != null)
        {
            Common_Pager reportItemPager = (Common_Pager)gvr.FindControl("ReportItemPager");
            reportItemPager.AddURL = GetAddReportItemURL(reportId);
            reportItemPager.HyperAdd_CSS = "popuplinkRI";
            reportItemPager.AddToolTip = "Add";
            reportItemPager.TotalRows = iTN;

            if (_strActionMode == "view")
            {
                reportItemPager.HideAdd = true;
                reportItemPager.HideDelete = true;
            }
        }

        if (iTN == 0)
        {
            if (_strActionMode != "view")
            {
                divEmptyAddReportItem.Visible = true;
                lnkRunReport.Visible = false;
            }
                
        }
        else
        {
            divEmptyAddReportItem.Visible = false;
            if(optReportDates.SelectedValue != "dateOnRun")
            {
                lnkRunReport.Visible = true;
                lnkRunReportDate.Visible = false;
            }
               
            //strRunReport.Visible = true;
            //imgRunReport.Visible = true;
        }

        return iTN;
    }

    protected void grdReportItem_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    protected void grdReportItem_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            bool objectNotFound = false;
            ReportItem.ReportItemType itemType = (ReportItem.ReportItemType)DataBinder.Eval(e.Row.DataItem, "ItemType");
            int objectId = (int)DataBinder.Eval(e.Row.DataItem, "ObjectID");
            string objectName = String.Empty;
            string objectTypeText = String.Empty;
            switch (itemType)
            {
                case ReportItem.ReportItemType.ItemTypeTable:
                    Table theTable = RecordManager.ets_Table_Details(objectId);
                    if (theTable == null)
                        objectNotFound = true;
                    else
                    {
                        objectName = theTable.TableName;
                        objectTypeText = "Table";
                    }
                    break;
                case ReportItem.ReportItemType.ItemTypeGraph:
                    GraphOption theGraph = GraphManager.ets_GraphOption_Detail(objectId);
                    if (theGraph == null)
                        objectNotFound = true;
                    else
                    {
                        objectName = theGraph.Heading;
                        objectTypeText = "Chart";
                    }
                    break;
                case ReportItem.ReportItemType.ItemTypeView:
                    ReportView theView = ReportManager.ets_ReportView_Detail(objectId);
                    if (theView == null)
                        objectNotFound = true;
                    else
                    {
                        objectName = theView.ViewName;
                        objectTypeText = "View";
                    }
                    break;
                default:
                    break;
            }

            if (!objectNotFound)
            {
                Label lblObjectName = e.Row.FindControl("lblObjectName") as Label;
                if (lblObjectName != null)
                {
                    lblObjectName.Text = objectName;
                }
                Label lblObjectType = e.Row.FindControl("lblObjectType") as Label;
                if (lblObjectType != null)
                {
                    lblObjectType.Text = objectTypeText;
                }
            }

            Label lblHeading = e.Row.FindControl("lblHeading") as Label;
            if (lblHeading != null)
            {
                lblHeading.Text = DataBinder.Eval(e.Row.DataItem, "ItemTitle").ToString();
                if (_strActionMode == "view")
                    lblHeading.Enabled = false;
            }
        }
    }

    protected void btnRefreshReportItem_Click(object sender, EventArgs e)
    {
        if (_iReportId.HasValue)
            PopulateReportItem(_iReportId.Value);
    }

    protected void ReportItemPager_BindTheGridAgain(object sender, EventArgs e)
    {
        if (_iReportId.HasValue)
            PopulateReportItem(_iReportId.Value);
    }

    protected void PopulateTheRecordItem()
    {
        try
        {
            //lblReportName.Text = _theReport.ReportName;

            if ((_strActionMode == "edit") || (_strActionMode == "add"))
            {
                ViewState["theReport"] = _theReport;
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Report Table", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }

    }

    protected void EnableTheRecordControlsItem(bool enabled)
    {
    }

    private void SetPageButtonsItem()
    {
        //switch (_strActionMode)
        //{
        //    case "add":
        //        hlBack.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
        //                             Request.ApplicationPath +
        //                             "Pages/ReportWriter/Report.aspx" +
        //                             "?SearchCriteria=" + Request.QueryString["SearchCriteria"];
        //        divEdit.Visible = false;
        //        divDetails.Visible = true;
        //        hlDetails.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
        //                                Request.ApplicationPath +
        //                                "Pages/ReportWriter/ReportDetail.aspx" +
        //                                "?ReportID=" + Request.QueryString["ReportID"] +
        //                                "&mode=" + Cryptography.Encrypt("edit") +
        //                                "&popup=1" +
        //                                "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
        //        hlSchedule.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
        //                                 Request.ApplicationPath +
        //                                 "Pages/ReportWriter/ReportSchedule.aspx" +
        //                                 "?ReportID=" + Request.QueryString["ReportID"] +
        //                                 "&mode=" + Cryptography.Encrypt("edit") +
        //                                 "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
        //        break;
        //    case "view":
        //        hlBack.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
        //                             Request.ApplicationPath +
        //                             "Pages/ReportWriter/Report.aspx" +
        //                             "?SearchCriteria=" + Request.QueryString["SearchCriteria"];
        //        divEdit.Visible = true;
        //        hlEditLink.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
        //                                 "Pages/ReportWriter/ReportFull.aspx" +
        //                                 "?mode=" + Cryptography.Encrypt("edit") +
        //                                 "&ReportID=" + Request.QueryString["ReportID"].ToString() +
        //                                 "&SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString();
        //        divDetails.Visible = true;
        //        hlDetails.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
        //                                Request.ApplicationPath +
        //                                "Pages/ReportWriter/ReportDetail.aspx" +
        //                                "?ReportID=" + Request.QueryString["ReportID"] +
        //                                "&mode=" + Cryptography.Encrypt("view") +
        //                                "&popup=1" +
        //                                "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
        //        hlSchedule.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
        //                                 Request.ApplicationPath +
        //                                 "Pages/ReportWriter/ReportSchedule.aspx" +
        //                                 "?ReportID=" + Request.QueryString["ReportID"] +
        //                                 "&mode=" + Cryptography.Encrypt("view") +
        //                                 "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
        //        break;
        //    case "edit":
        //        hlBack.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
        //                             Request.ApplicationPath +
        //                             "Pages/ReportWriter/Report.aspx" +
        //                             "?SearchCriteria=" + Request.QueryString["SearchCriteria"];
        //        divEdit.Visible = false;
        //        divDetails.Visible = true;
        //        hlDetails.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
        //                                Request.ApplicationPath +
        //                                "Pages/ReportWriter/ReportDetail.aspx" +
        //                                "?ReportID=" + Request.QueryString["ReportID"] +
        //                                "&mode=" + Cryptography.Encrypt("edit") +
        //                                "&popup=1" +
        //                                "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
        //        hlSchedule.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
        //                                 Request.ApplicationPath +
        //                                 "Pages/ReportWriter/ReportSchedule.aspx" +
        //                                 "?ReportID=" + Request.QueryString["ReportID"] +
        //                                 "&mode=" + Cryptography.Encrypt("edit") +
        //                                 "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
        //        break;
        //}

        //if (Request.QueryString["fixedbackurl"] != null)
        //    hlBack.NavigateUrl = Cryptography.Decrypt(Request.QueryString["fixedbackurl"].ToString());

        if (_iReportId.HasValue)
        {
            //hlReportDate.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
            //                           "Pages/ReportWriter/ReportDates.aspx" +
            //                           "?ReportID=" + Cryptography.Encrypt(_iReportId.Value.ToString());
            //hlReport.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
            //                       "Pages/ReportWriter/ReportHandler.ashx" +
            //                       "?ReportID=" + Cryptography.Encrypt(_iReportId.Value.ToString());

            SetAskReportDate();
        }
    }

    private void SetAskReportDate() //??
    {
        if (_iReportId.HasValue)
        {
            //Report report = ReportManager.ets_Report_Detail(_iReportId.Value);
            if (_theReport != null)
            {
                if ((_theReport.ReportDates == Report.ReportInterval.IntervalDateRange) &&
                    (!_theReport.ReportStartDate.HasValue || !_theReport.ReportEndDate.HasValue))
                {
                    hfAskReportDate.Value = "1";
                }
                else
                {
                    hfAskReportDate.Value = "0";
                }
            }
        }
    }

    protected void lbDuplicateReport_OnClick(object sender, EventArgs e)
    {
        int sourceReportId = -1;
        if (_iReportId.HasValue && int.TryParse(txtSourceReportId.Text, out sourceReportId) && sourceReportId != -1)
        {
            if (sourceReportId == _iReportId.Value)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "message_alert",
                    "alert('Cannot copy sheets from itself.');", true);
            else
            {
                ReportManager.ets_Report_Clone(sourceReportId, _iReportId.Value);
                PopulateReportItem(_iReportId.Value);
            }
        }
    }

    protected void lbUpdateDetails_OnClick(object sender, EventArgs e)
    {
        PopulateTheRecordItem();
        SetAskReportDate();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "set_report_button_visibility",
            "setReportButtonVisibility();", true);
    }

    //item end


    protected void lnkRunReport_Click(object sender, EventArgs e)
    {
        //save the report
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "lnkRunReport_Click_ajax-indicator-full",
        //  "$('.ajax-indicator-full').hide();", true);
        if (optReportDates.SelectedValue== "dateOnRun")
        {          
            bRunReport = true;
            lnkSaveDetail_Click(null, null);
            Response.Redirect("ReportHandler.ashx?ReportID=" + Cryptography.Encrypt(_iReportId.ToString()), false);
        }
        else
        {
            lnkSaveDetail_Click(null, null);
        }

       
        //lets run the report
    }

    protected void lnkNextStep_Click(object sender, EventArgs e)
    {
        lnkSaveDetail_Click(null, null);
    }

    protected void txtDate_TextChanged(object sender, EventArgs e)
    {
        ddlFrequencySchedule_OnSelectedIndexChanged(null, null);
    }
}
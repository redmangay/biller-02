﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class Pages_ReportWriter_ReportTableFilter : System.Web.UI.Page
{
    private int _iReportItemID = -1;
    private int _iTableID = -1;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ReportItemID"] != null)
            _iReportItemID = int.Parse(Cryptography.Decrypt(Request.QueryString["ReportItemID"].ToString()));
        ReportItem reportItem = ReportManager.ets_ReportItem_Detail(_iReportItemID);
        if (reportItem != null)
            _iTableID = reportItem.ObjectId;

        if (Request.QueryString["TableID"]!=null)
            _iTableID =int.Parse( Request.QueryString["TableID"].ToString());

        if (!IsPostBack)
        {
            PopulateReportItemFilter();
        }
    }


    protected void SWCHideColumnChanged(object sender, EventArgs e)
    {
        Pages_UserControl_ConditionsCondition swcReportItemFilter = sender as Pages_UserControl_ConditionsCondition;

        if (swcReportItemFilter != null)
        {
            GridViewRow row = swcReportItemFilter.NamingContainer as GridViewRow;
            ImageButton imgbtnPlus = row.FindControl("imgbtnPlus") as ImageButton;

            if (swcReportItemFilter.ddlHideColumnV == "")
            {
                imgbtnPlus.Visible = false;
            }
            else
            {
                imgbtnPlus.Visible = true;
            }
        }
    }

    protected void grdReportItemFilter_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblID = e.Row.FindControl("lblID") as Label;
                Label lblReportItemFilterID = e.Row.FindControl("lblReportItemFilterID") as Label;
                ImageButton imgbtnMinus = e.Row.FindControl("imgbtnMinus") as ImageButton;
                ImageButton imgbtnPlus = e.Row.FindControl("imgbtnPlus") as ImageButton;

                Pages_UserControl_ConditionsCondition swcReportItemFilter = e.Row.FindControl("swcReportItemFilter") as Pages_UserControl_ConditionsCondition;

                swcReportItemFilter.TableID = _iTableID;
                swcReportItemFilter.ColumnID = -1;

                if (e.Row.RowIndex == 0)
                {
                    swcReportItemFilter.ShowJoinOperator = false;
                    swcReportItemFilter.ddlJoinOperatorV = "and";
                }
                else
                {
                    swcReportItemFilter.ShowJoinOperator = true;
                    swcReportItemFilter.ddlJoinOperatorV = DataBinder.Eval(e.Row.DataItem, "JoinOperator").ToString();
                }

                swcReportItemFilter.ddlOperatorV = DataBinder.Eval(e.Row.DataItem, "FilterOperator").ToString();
                swcReportItemFilter.ddlHideColumnV = DataBinder.Eval(e.Row.DataItem, "FilterColumnID").ToString();
                swcReportItemFilter.hfConditionValueV = DataBinder.Eval(e.Row.DataItem, "FilterColumnValue").ToString();

                lblReportItemFilterID.Text = DataBinder.Eval(e.Row.DataItem, "ReportItemFilterID").ToString();
                lblID.Text = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
                imgbtnMinus.CommandArgument = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
                imgbtnPlus.CommandArgument = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
            }
        }
        catch
        {
            //
        }
    }

    protected void SetReportItemFilterRowData()
    {
        if (ViewState["dtReportItemFilter"] != null)
        {
            DataTable dtReportItemFilter = (DataTable)ViewState["dtReportItemFilter"];
            dtReportItemFilter.Rows.Clear();

            int iDisplayOrder = 0;
            if (grdReportItemFilter.Rows.Count > 0)
            {
                foreach (GridViewRow gvRow in grdReportItemFilter.Rows)
                {
                    Label lblID = gvRow.FindControl("lblID") as Label;
                    Label lblReportItemFilterID = gvRow.FindControl("lblReportItemFilterID") as Label;
                    Pages_UserControl_ConditionsCondition swcReportItemFilter = gvRow.FindControl("swcReportItemFilter") as Pages_UserControl_ConditionsCondition;

                    dtReportItemFilter.Rows.Add(lblID.Text, lblReportItemFilterID.Text, swcReportItemFilter.ddlHideColumnV,
                        swcReportItemFilter.hfConditionValueV, swcReportItemFilter.ddlOperatorV, iDisplayOrder.ToString(), swcReportItemFilter.ddlJoinOperatorV);
                    iDisplayOrder = iDisplayOrder + 1;
                }
            }
            dtReportItemFilter.AcceptChanges();
            ViewState["dtReportItemFilter"] = dtReportItemFilter;
        }
    }

    protected void PopulateReportItemFilter()
    {
        DataTable dtReportItemFilter = new DataTable();
        dtReportItemFilter.Columns.Add("ID"); // 0
        dtReportItemFilter.Columns.Add("ReportItemFilterID"); // 1
        dtReportItemFilter.Columns.Add("FilterColumnID"); // 2
        dtReportItemFilter.Columns.Add("FilterColumnValue"); // 3
        dtReportItemFilter.Columns.Add("FilterOperator"); // 4
        dtReportItemFilter.Columns.Add("DisplayOrder"); // 5
        dtReportItemFilter.Columns.Add("JoinOperator"); // 6
        dtReportItemFilter.AcceptChanges();

        if (ViewState["dtReportItemFilter"] == null)
        {
            DataTable dtReportItemFilterDB = ReportManager.ets_ReportItemFilter_Select(_iReportItemID);

            if (dtReportItemFilter != null && dtReportItemFilterDB != null)
            {
                foreach (DataRow dr in dtReportItemFilterDB.Rows)
                {
                    DataRow newRow = dtReportItemFilter.NewRow();
                    newRow["ID"] = Guid.NewGuid().ToString();
                    newRow["ReportItemFilterID"] = dr["ReportItemFilterID"].ToString();
                    newRow["FilterColumnID"] = dr["FilterColumnID"].ToString();
                    newRow["FilterColumnValue"] = dr["FilterColumnValue"].ToString();
                    newRow["FilterOperator"] = dr["FilterOperator"].ToString();
                    newRow["DisplayOrder"] = dr["DisplayOrder"].ToString();
                    newRow["JoinOperator"] = dr["JoinOperator"].ToString();
                    dtReportItemFilter.Rows.Add(newRow);
                }
                dtReportItemFilter.AcceptChanges();

                if(dtReportItemFilter.Rows.Count == 0)
                {
                    dtReportItemFilter.Rows.Add(Guid.NewGuid().ToString(), "-1", "", "", "equals", "1", "");
                    dtReportItemFilter.AcceptChanges();
                }

                grdReportItemFilter.DataSource = dtReportItemFilter;
                grdReportItemFilter.DataBind();

                ViewState["dtReportItemFilter"] = dtReportItemFilter;
            }
        }
        else
        {
            if (ViewState["dtReportItemFilter"] == null)
            {
                if (Session["dtReportItemFilter"] == null)
                {
                   dtReportItemFilter.Rows.Add(Guid.NewGuid().ToString(), "-1", "", "", "equals", "1", "");
                   grdReportItemFilter.DataSource = dtReportItemFilter;
                   grdReportItemFilter.DataBind();

                   ViewState["dtReportItemFilter"] = dtReportItemFilter;
               }
               else
               {
                   dtReportItemFilter = (DataTable)Session["dtReportItemFilter"];
                   grdReportItemFilter.DataSource = dtReportItemFilter;
                   grdReportItemFilter.DataBind();

                   ViewState["dtReportItemFilter"] = dtReportItemFilter;
               }
            }
            else
            {
                dtReportItemFilter = (DataTable)ViewState["dtReportItemFilter"];
                grdReportItemFilter.DataSource = dtReportItemFilter;
                grdReportItemFilter.DataBind();
            }
        }
    }

    protected void grdReportItemFilter_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            SetReportItemFilterRowData();

            if (e.CommandName == "minus")
            {
                if (ViewState["dtReportItemFilter"] != null)
                {
                    DataTable dtReportItemFilter = (DataTable)ViewState["dtReportItemFilter"];

                    if (dtReportItemFilter.Rows.Count == 1)
                    {
                        return;
                    }

                    for (int i = dtReportItemFilter.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dtReportItemFilter.Rows[i];
                        if (dr["id"].ToString() == e.CommandArgument.ToString())
                        {
                            dr.Delete();
                            break;
                        }
                    }
                    dtReportItemFilter.AcceptChanges();
                    ViewState["dtReportItemFilter"] = dtReportItemFilter;
                    PopulateReportItemFilter();
                }
            }
            else if (e.CommandName == "plus")
            {
                if (ViewState["dtReportItemFilter"] != null)
                {
                    DataTable dtReportItemFilter = (DataTable)ViewState["dtReportItemFilter"];

                    int iPos = 0;

                    for (int i = 0; i < dtReportItemFilter.Rows.Count; i++)
                    {
                        if (dtReportItemFilter.Rows[i][0].ToString() == e.CommandArgument.ToString())
                        {
                            iPos = i;
                            break;
                        }
                    }

                    DataRow newRow = dtReportItemFilter.NewRow();
                    newRow["ID"] = Guid.NewGuid().ToString();
                    newRow["ReportItemFilterID"] = "-1";
                    newRow["FilterColumnID"] = "";
                    newRow["FilterColumnValue"] = "";
                    newRow["FilterOperator"] = "equals";
                    newRow["DisplayOrder"] = (dtReportItemFilter.Rows.Count + 1).ToString();
                    newRow["JoinOperator"] = "and";

                    dtReportItemFilter.Rows.InsertAt(newRow, iPos + 1);
                    dtReportItemFilter.AcceptChanges();
                    ViewState["dtReportItemFilter"] = dtReportItemFilter;
                    PopulateReportItemFilter();
                }
            }
        }
        catch
        {
            //
        }
    }

    protected void lnkSave_Click(object sender, EventArgs e)
    {

        SetReportItemFilterRowData();

        if (ViewState["dtReportItemFilter"] != null)
        {
            DataTable dtOldReportItemFilter = ReportManager.ets_ReportItemFilter_Select(_iReportItemID);

            DataTable dtReportItemFilter = (DataTable)ViewState["dtReportItemFilter"];

            int iOldRowsCount = dtOldReportItemFilter.Rows.Count;
            string strActiveReportItemFilterIDs = "-1";
            int iDO = 1;

            foreach (DataRow drSW in dtReportItemFilter.Rows)
            {
                if (!String.IsNullOrEmpty(drSW["FilterColumnID"].ToString()) &&
                    ((iDO == 1) || !String.IsNullOrEmpty(drSW["JoinOperator"].ToString())))
                {
                    bool bInsert = iOldRowsCount < iDO;

                    ReportItemFilter theReportItemFilter = new ReportItemFilter()
                    {
                        ReportItemFilterID = bInsert
                            ? -1
                            : int.Parse(dtOldReportItemFilter.Rows[iDO - 1]["ReportItemFilterID"].ToString()),
                        ReportItemID = _iReportItemID,
                        FilterColumnID = int.Parse(drSW["FilterColumnID"].ToString()),
                        FilterColumnValue = drSW["FilterColumnValue"].ToString(),
                        FilterOperator = drSW["FilterOperator"].ToString(),
                        DisplayOrder = iDO,
                        JoinOperator = (iDO == 1) ? "" : drSW["JoinOperator"].ToString()
                    };
                    if (bInsert)
                    {
                        theReportItemFilter.ReportItemFilterID =
                            ReportManager.ets_ReportItemFilter_Insert(theReportItemFilter);
                    }
                    else
                    {
                        ReportManager.ets_ReportItemFilter_Update(theReportItemFilter);
                    }

                    strActiveReportItemFilterIDs = strActiveReportItemFilterIDs + "," +
                                                   theReportItemFilter.ReportItemFilterID.ToString();
                    iDO = iDO + 1;
                }
            }
            Common.ExecuteText(@"DELETE ReportItemFilter WHERE ReportItemID = " + _iReportItemID.ToString() 
                + @" AND ReportItemFilterID NOT IN (" + strActiveReportItemFilterIDs + @")");
        }

        if (ViewState["dtReportItemFilter"] != null)
        {
            Session["dtReportItemFilter"] = (DataTable)ViewState["dtReportItemFilter"];
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Save Action", "GetBackValueEdit();", true);
    }
}
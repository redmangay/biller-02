﻿<%@ Page Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true"
    CodeFile="ReportDates.aspx.cs" Inherits="Pages_ReportWriter_ReportDates" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        table
        {
            margin: 0 auto;
        }
        td
        {
            padding: 5px;
        }

        .button {
            background-color: #008CBA;
            border: none;
            color: white;
            padding: 10px 25px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-weight: bold;
            font-size: 12px;
            margin: 2px 1px;
            cursor: pointer;
            border-radius: 4px;
 
}
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <script language="javascript" type="text/javascript">

        function jsReportDatesShowHideProgress() {
            document.getElementById('hlRunDate').value = 'Running';
            document.getElementById("hlRunDate").disabled = true;
            document.getElementById("hlRunDate").style.backgroundColor = "lightblue";
            setTimeout(function () { document.getElementById('imgloadinggif').style.display = 'block'; }, 200);
            deleteCookie();

            var timeInterval = 200; // milliseconds (checks the cookie for every half second )

            var loop = setInterval(function () {
                if (IsCookieValid())
                {
                    document.getElementById("hlRunDate").disabled = false;
                    document.getElementById("hlRunDate").style.backgroundColor = "#008CBA";
                    document.getElementById('hlRunDate').value = 'Run';
                    document.getElementById('imgloadinggif').style.display = 'none';
                    showErrorMessage('Generated Successfully!');
                    clearInterval(loop)
                }

            }, timeInterval);
        }

        // cookies
        function deleteCookie() {
           
            var cook = getCookie('ExportReport');
            if (cook != "") {
                document.cookie = "ExportReport=; Path=/; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            }
        }

        function IsCookieValid() {
            var cook = getCookie('ExportReport');
            return cook != '';
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        $(document).ready(function () {
            initElements();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initElements();
        });

        function initElements() {
            $("#Content").css("min-height", 100);
        }

        function CloseAndRefresh() {
            parent.$.fancybox.close();
            return false;
        }


        $(document).ready(function () {
            $('#hlRunDate').click(function () {
                rundateGetDates();
            });
        });

        //RP Added Ticket 5121
        window.parent.$('.ajax-indicator-full').hide();
        //End Modification
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

             <div id="reportDatesSection">
                   <div id="reportDateHeader" style="text-align:center; margin-top:30px;">
                    <h2>Run Report</h2>
                  </div>

                  <div id="reportDateDates" style="text-align:center; margin-top:30px;">
                               <table  border="0" cellpadding="0" cellspacing="0" style="width:100%;">  
             
                <tr>
                    <td style="padding-right:5px; text-align: right;">Start Date: </td>
                     <td style="text-align: left; width: 130px">
                        <asp:TextBox ID="txtStartDate" ClientIDMode="Static" runat="server" CssClass="NormalTextBox" 
                                     Width="100px"  BorderStyle="Solid"
                                     BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                            <asp:ImageButton runat="server" ID="ibStartDate"  ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false"/>  
                          <ajaxToolkit:CalendarExtender ID="ce_txtDateFrom" runat="server" TargetControlID="txtStartDate"
                                                      Format="dd/MM/yyyy" PopupButtonID="ibStartDate" FirstDayOfWeek="Monday">
                        </ajaxToolkit:CalendarExtender>
                                                           
                        <ajaxToolkit:TextBoxWatermarkExtender ID="tbwStartDate" TargetControlID="txtStartDate" WatermarkText="dd/mm/yyyy"
                                                              runat="server" WatermarkCssClass="MaskText"></ajaxToolkit:TextBoxWatermarkExtender>
                    </td>
                    <td align="left">
                          Time: 
                        <asp:TextBox ID="txtStartTime" runat="server" CssClass="NormalTextBox"
                            Width="60px" BorderStyle="Solid" placeholder="hh:mm"
                            BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                  
                        <ajaxtoolkit:maskededitextender id="meeStartTime" runat="server" targetcontrolid="txtStartTime"
                             mask="99:99" masktype="Time">
                        </ajaxtoolkit:maskededitextender>
                    
                    </td>
                </tr>
                <tr>
                    <td style="padding-right:5px; text-align: right;">End Date:</td>
                   <td style="text-align: left; width: 130px">
                        <asp:TextBox ID="txtEndDate" ClientIDMode="Static" runat="server" CssClass="NormalTextBox" 
                                     Width="100px"  BorderStyle="Solid"
                                     BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                          <asp:ImageButton runat="server" ID="ibEndDate"  ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false"/>  
                        
                          <ajaxToolkit:CalendarExtender ID="ce_txtDateTo" runat="server" TargetControlID="txtEndDate"
                                                      Format="dd/MM/yyyy" PopupButtonID="ibEndDate" FirstDayOfWeek="Monday">
                        </ajaxToolkit:CalendarExtender>

                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" TargetControlID="txtEndDate" WatermarkText="dd/mm/yyyy"
                                                              runat="server" WatermarkCssClass="MaskText"></ajaxToolkit:TextBoxWatermarkExtender>
                    </td>
                    <td align="left">
                    Time: 
                    <asp:TextBox ID="txtEndTime" runat="server" CssClass="NormalTextBox"
                        Width="60px" BorderStyle="Solid" placeholder="hh:mm"
                        BorderColor="#909090" BorderWidth="1"></asp:TextBox>
               
                    <ajaxtoolkit:maskededitextender id="meeEndTime" runat="server" targetcontrolid="txtEndTime"
                         mask="99:99" masktype="Time">
                    </ajaxtoolkit:maskededitextender>
                   
                </td>
                           
                </tr>
                  <tr style="text-align: center">
                    <td>&nbsp;</td>
                      <td>&nbsp;</td>
                       <td>&nbsp;</td>
                </tr>
                  <tr style="text-align: center">
                    <td colspan="3">
                         <div style="text-align: center; display: flex; justify-content: center;">

                           <asp:Label ID="lblErrorMessage" ClientIDMode="Static" Font-Size="Small" ForeColor="Red" runat="server"> 
                                     </asp:Label>  
                                  <img style="display: none" id="imgloadinggif" src="../../Images/loading.gif" alt="loading.." />
                        </div>                   
                    </td>
                </tr>
                 <tr style="text-align: center">
                      <td colspan="3">

                           <div style="text-align: center;display:none">
                                <asp:LinkButton runat="server" ID="lnkRunReport" ClientIDMode="Static" 
                                CssClass="btn btn-primary" OnClick="lnkRunReport_OnClick" ForeColor="White" Width="150px"><strong>Run</strong> </asp:LinkButton>
                           </div>
                          
                          </td>
                 </tr>
            </table>
                   </div>

                    <div style="text-align:center; margin-top:50px;">
                        <input type="button" id="hlCancel" onclick="runReportClose(); " class="button" value="Close"/>                  
                        &nbsp;
                        <input type="button" id="hlRunDate" onclick="rundateGetDates();" class="button" value="Run"/> 
                    </div>

             </div>





        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

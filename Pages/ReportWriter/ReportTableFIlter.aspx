﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true"
    CodeFile="ReportTableFIlter.aspx.cs" Inherits="Pages_ReportWriter_ReportTableFilter" %>

<%@ Register Src="~/Pages/UserControl/ConditionsCondition.ascx" TagName="SWCon" TagPrefix="dbg" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            initElements();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initElements();
        });

        function initElements() {
            $("[id*='ddlHideColumn']").css("max-width", "15em");
        }

        function GetBackValueEdit() {
            window.parent.document.getElementById('cbApplyFilter').checked = true;
            parent.$.fancybox.close();
        }

        function GetBackValueAdd() {
            parent.$.fancybox.close();
        }
    </script>
    <div style="padding-top: 10px;">
        <asp:UpdatePanel ID="upFilter" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <table style="width: 100%;">
                    <td>
                        <span style="font-size: 16px; font-weight: bold;">Filter</span>
                    </td>
                    <td style="width: 50%;">
                        <div runat="server" id="divSave" style="float: right; width: 36px;">
                            <asp:LinkButton runat="server" ID="lnkSaveNew" CausesValidation="true" OnClick="lnkSave_Click">
                                <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                    ToolTip="Save" />
                            </asp:LinkButton>
                        </div>
                    </td>
                </table>
                <br />
                <table style="width:100%">
                    <tr>
                        <td>
                            <div style="padding-left: 0px; padding-top: 10px;">
                                <asp:GridView ID="grdReportItemFilter" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" 
                                    CssClass="gridview" OnRowCommand="grdReportItemFilter_RowCommand" OnRowDataBound="grdReportItemFilter_RowDataBound"
                                    ShowHeaderWhenEmpty="true"
                                    ShowFooter="true">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblID" Text='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label runat="server" ID="lblReportItemFilterID" Text='<%# Eval("ReportItemFilterID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemStyle Width="30px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnMinus" runat="server" ImageUrl="~/App_Themes/Default/Images/Minus.png" CausesValidation="false"
                                                    CommandName="minus" CommandArgument='<%# Eval("ID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                <strong></strong>
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <dbg:SWCon runat="server" ID="swcReportItemFilter" OnddlHideColumn_Changed="SWCHideColumnChanged" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemStyle Width="30px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnPlus" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png"
                                                    CommandName="plus" Visible="false" CommandArgument='<%# Eval("ID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="gridview_header" Height="25px" />
                                    <RowStyle CssClass="gridview_row_NoPadding" />
                                </asp:GridView>
                                <div>
                                    <asp:Label ID="lblMsgTab" runat="server" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </td>


                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>

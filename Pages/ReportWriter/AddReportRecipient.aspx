﻿<%@ Page Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true"
    CodeFile="AddReportRecipient.aspx.cs" Inherits="Pages_ReportWriter_AddReportRecipient" EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        table
        {
            margin: 0 auto;
        }
        td
        {
            padding: 5px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <link href="<%=ResolveUrl("~/Styles/jquery-ui-dbgcustom.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Script/jquery-ui-1.12.1.min.js")%>"></script>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            initElements();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initElements();
        });

        function initElements() {
            $("#Content").css("min-height", 100);
            $("#txtEmail").css("width", "28em");

            $(function () {
                var availableTags = $("#lblEmailList").text().split(";");
                $("#txtEmail").autocomplete({
                    source: availableTags,
                    open: function () {
                        $(this).autocomplete('widget')
                            .css('overflow', 'scroll')
                            .css('overflow-x', 'hidden')
                            .css('max-height', '110px')
                            .css('font-size', '12px');
                        return false;
                    }
                });
            });
        }

        function CloseAndRefresh() {
            window.parent.document.getElementById('btnRefreshReportRecipient').click();
            parent.$.fancybox.close();
            return false;
        }
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table id="MainTable" style="margin-top: 10px; width: 450px;">
                <tr>
                    <td colspan="2" style="white-space: nowrap">
                        <asp:Label runat="server" ID="lblDetailTitle" Font-Size="16px"
                                   Font-Bold="true" Text="Add Recipient">
                        </asp:Label>
                    </td>
                    <td style="text-align: right;">
                        <div runat="server" id="divSave" style="float: right;">
                            <asp:LinkButton runat="server" ID="lnkAdd" CausesValidation="true" OnClick="lnkAdd_OnClick">
                                <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                           ToolTip="Save" />
                            </asp:LinkButton>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: left;">
                        <table>
                            <tr>
                                <td style="vertical-align: top">
                                    <asp:Label Font-Bold="True" runat="server" Text="Email"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtEmail" ClientIDMode="Static" />
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                                                CssClass="failureNotification" Display="Dynamic" ErrorMessage="Email is required."
                                                                ToolTip="Email is required.">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator id="RegularExpressionValidator1" 
                                                                    runat="server" ControlToValidate="txtEmail" 
                                                                    CssClass="failureNotification" ErrorMessage="Invalid Email" 
                                                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*
                                    </asp:RegularExpressionValidator>
                                    <br /><br />
                                    Start typing the email address above and if an existing user they will come up as a suggestion
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">
                        <asp:Label ID="errorMessage" runat="server" ForeColor="red"></asp:Label>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                                               ShowMessageBox="true" ShowSummary="false" HeaderText="Please correct the following errors:"/>
                        <div style="display: none; visibility: hidden">
                            <asp:Label runat="server" ID="lblEmailList" ClientIDMode="Static"></asp:Label>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

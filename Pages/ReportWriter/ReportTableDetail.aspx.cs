﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;

public partial class Pages_ReportWriter_ReportTableDetail : SecurePage
{
    string _strActionMode = "view";
    private int? _iReportItemID;
    private ReportItem _theReportItem;
    private string _qsReportItemID = "";


    protected void PopulateTerminology()
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (!Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
            //    Response.Redirect("~/Default.aspx", false);

            hlBack.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                 "Pages/ReportWriter/ReportFull.aspx" +
                                 "?SearchCriteria=" + Request.QueryString["SearchCriteria"] +
                                 "&mode=" + Request.QueryString["mode"] + 
                                 "&ReportID=" + Request.QueryString["ReportID"];

            if(Request.QueryString["fixedbackurl"] != null)
                hlBack.NavigateUrl = Cryptography.Decrypt(Request.QueryString["fixedbackurl"].ToString());

            hlFilter.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                   "Pages/ReportWriter/ReportTableFilter.aspx" +
                                   "?ReportItemID=" + Request.QueryString["ReportItemID"];
            hlSortOrder.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                   "Pages/ReportWriter/ReportTableSortOrder.aspx" +
                                   "?ReportItemID=" + Request.QueryString["ReportItemID"];

            PopulateListBoxes(Session["AccountID"].ToString());
        }

        if (Request.QueryString["mode"] == null)
        {
            Server.Transfer("~/Default.aspx");
        }
        else
        {
            string qsMode = Cryptography.Decrypt(Request.QueryString["mode"]);

            if (qsMode == "add" ||
                qsMode == "view" ||
                qsMode == "edit")
            {
                _strActionMode = qsMode.ToLower();

                if (Request.QueryString["ReportID"] != null)
                {
                    _qsReportItemID = Cryptography.Decrypt(Request.QueryString["ReportItemID"]);
                    _iReportItemID = int.Parse(_qsReportItemID);
                    _theReportItem = ReportManager.ets_ReportItem_Detail((int)_iReportItemID);
                    if (!IsPostBack)
                    {
                        PopulateColumnList();

                        if (Request.QueryString["popupitem"] != null)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopItem", "  setTimeout(function () { OpenReportItem(); }, 1000); ", true);
                        }
                    }
                }
            }
            else
            {
                Server.Transfer("~/Default.aspx");
            }
        }

        switch (_strActionMode.ToLower())
        {
            case "add":
                switch (_theReportItem.ItemType)
                {
                    case ReportItem.ReportItemType.ItemTypeTable:
                        lblTitle.Text = "Edit Data Sheet";
                        break;
                    case ReportItem.ReportItemType.ItemTypeGraph:
                        lblTitle.Text = "Edit Graph Sheet";
                        break;
                    case ReportItem.ReportItemType.ItemTypeView:
                        lblTitle.Text = "Edit View Sheet";
                        break;
                }
                if (!IsPostBack)
                    PopulateTheRecord();
                break;
            case "view":
                switch (_theReportItem.ItemType)
                {
                    case ReportItem.ReportItemType.ItemTypeTable:
                        lblTitle.Text = "View Data Sheet";
                        break;
                    case ReportItem.ReportItemType.ItemTypeGraph:
                        lblTitle.Text = "View Graph Sheet";
                        break;
                    case ReportItem.ReportItemType.ItemTypeView:
                        lblTitle.Text = "View View Sheet";
                        break;
                }
                if (!IsPostBack)
                    PopulateTheRecord();         
                EnableTheRecordControls(false);
                divSave.Visible = false;
                break;
            case "edit":
                switch (_theReportItem.ItemType)
                {
                    case ReportItem.ReportItemType.ItemTypeTable:
                        lblTitle.Text = "Edit Data Sheet";
                        break;
                    case ReportItem.ReportItemType.ItemTypeGraph:
                        lblTitle.Text = "Edit Graph Sheet";
                        break;
                    case ReportItem.ReportItemType.ItemTypeView:
                        lblTitle.Text = "Edit View Sheet";
                        break;
                }
                if (!IsPostBack)
                    PopulateTheRecord();
                break;
            default:
                //?
                break;
        }

        switch (_theReportItem.ItemType)
        {
            case ReportItem.ReportItemType.ItemTypeTable:
                break;
            case ReportItem.ReportItemType.ItemTypeGraph:
                trSeparator1.Visible = false;
                tr2.Visible = false;
                trSeparator2.Visible = false;
                tr3.Visible = false;
                tr4.Visible = false;
                tr5.Visible = false;
                tr6.Visible = false;
                trInnerTable.Visible = false;

                tr2chart.Visible = true;
                break;
            case ReportItem.ReportItemType.ItemTypeView:
                trSeparator1.Visible = false;
                tr2.Visible = false;
                trSeparator2.Visible = false;
                tr3.Visible = false;
                tr4.Visible = false;
                tr5.Visible = false;
                tr6.Visible = false;
                trInnerTable.Visible = false;

                tr2view.Visible = true;
                break;
        }

        Title = lblTitle.Text;

        if (!IsPostBack)
        {
            if (_iReportItemID.HasValue)
                PopulateReportTableColumn(_iReportItemID.Value);

            PopulateTerminology();
        }
    }

    protected void PopulateListBoxes(string strAccountId)
    {
        ddlView.Items.Clear();
        DataTable dtActiveViewList =
            Common.DataTableFromText("SELECT DataViewID, ViewName FROM [DataView] WHERE IsActive=1 AND AccountID=" +
                                     strAccountId + " ORDER BY ViewName");
        foreach (DataRow row in dtActiveViewList.Rows)
        {
            ddlView.Items.Add(new ListItem()
            {
                Text = row["ViewName"].ToString(),
                Value = row["DataViewID"].ToString()
            });
        }

        ddlChart.Items.Clear();
        DataTable dtActiveGraphList =
            Common.DataTableFromText(
                "SELECT GraphOptionID, Heading FROM [GraphOption] WHERE IsActive=1 AND AccountID=" + strAccountId +
                " ORDER BY Heading");
        foreach (DataRow row in dtActiveGraphList.Rows)
        {
            ddlChart.Items.Add(new ListItem()
            {
                Text = row["Heading"].ToString(),
                Value = row["GraphOptionID"].ToString()
            });
        }
    }

    protected void PopulateSearchCriteria(int iSearchCriteriaID)
    {
        try
        {
            SearchCriteria theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);
            if (theSearchCriteria != null)
            {
                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.Load(new StringReader(theSearchCriteria.SearchText));
            }
            else
            {
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
    }

    protected void ReportTableColumnPager_DeleteAction(object sender, EventArgs e)
    {
        string sCheck = "";
     
        for (int i = 0; i < grdReportTableColumn.Rows.Count; i++)
        {
            bool ischeck = ((CheckBox)grdReportTableColumn.Rows[i].FindControl("chkDelete")).Checked;
            if (ischeck)
            {
                sCheck = sCheck + ((Label)grdReportTableColumn.Rows[i].FindControl("LblID")).Text + ",";
            }
        }
        if (string.IsNullOrEmpty(sCheck))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message_alert", "alert('Please select a record.');", true);
            return;
        }

        sCheck = sCheck + "-1";
        Common.ExecuteText("DELETE ReportTableItem WHERE ReportTableItemID IN (" + sCheck + ") ");
        if (_iReportItemID.HasValue)
            PopulateReportTableColumn(_iReportItemID.Value);
    }

    protected void btnReportTableColumnIDForColumnPosition_Click(object sender, EventArgs e)
    {
        if (hfReportTableColumnIDForColumnPosition.Value != "")
        {
            try
            {
                string strNewVIT = hfReportTableColumnIDForColumnPosition.Value.Substring(0, hfReportTableColumnIDForColumnPosition.Value.Length - 1);
                string[] newVT = strNewVIT.Split(',');
                DataTable dtDO = Common.DataTableFromText("SELECT ColumnPosition, ReportTableItemID FROM [ReportTableItem] WHERE ReportTableItemID IN (" + strNewVIT + ") ORDER BY ColumnPosition");
                if (newVT.Length == dtDO.Rows.Count)
                {
                    for (int i = 0; i < newVT.Length; i++)
                    {
                        Common.ExecuteText("UPDATE [ReportTableItem] SET ColumnPosition = " + i.ToString() + " WHERE ReportTableItemID = " + newVT[i]);
                    }
                }
            }
            catch (Exception)
            {
                //
            }

            if (_iReportItemID.HasValue)
                PopulateReportTableColumn(_iReportItemID.Value);
        }
    }

    public string GetAddReportTableColumnURL(int reportItemId)
    {
        return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
               "/Pages/ReportWriter/ReportTableItemDetail.aspx?mode=" + Cryptography.Encrypt("add") + "&ReportItemID=" +
               Cryptography.Encrypt(reportItemId.ToString());
    }

    protected void PopulateReportTableColumn(int reportItemId)
    {
        int iTN = 0;

        hlAddReportTableColumn.NavigateUrl = GetAddReportTableColumnURL(reportItemId);
        hlAddReportTableColumn2.NavigateUrl = hlAddReportTableColumn.NavigateUrl;
        DataTable dtReportTableColumns = Common.DataTableFromText("SELECT * FROM ReportTableItem WHERE ReportItemID =" + reportItemId.ToString() + " ORDER BY ColumnPosition");

        grdReportTableColumn.DataSource = dtReportTableColumns;
        iTN = dtReportTableColumns.Rows.Count;
        grdReportTableColumn.VirtualItemCount = iTN;
        grdReportTableColumn.DataBind();

        if (_strActionMode == "view")
        {
            grdReportTableColumn.Columns[2].Visible = false;
        }

        if (grdReportTableColumn.TopPagerRow != null)
            grdReportTableColumn.TopPagerRow.Visible = true;

        GridViewRow gvr = grdReportTableColumn.TopPagerRow;

        if (gvr != null)
        {
            Common_Pager reportTableColumnPager = (Common_Pager)gvr.FindControl("ReportTableColumnPager");
            reportTableColumnPager.AddURL = GetAddReportTableColumnURL(reportItemId);
            reportTableColumnPager.HyperAdd_CSS = "popuplinkRIC";
            reportTableColumnPager.AddToolTip = "Add/Remove";
            reportTableColumnPager.TotalRows = iTN;

            if (_strActionMode == "view")
            {
                reportTableColumnPager.HideAdd = true;
                reportTableColumnPager.HideDelete = true;
            }
        }

        if (iTN == 0)
        {
            if (_strActionMode != "view")
                divEmptyAddReportTableColumn.Visible = true;
        }
        else
        {
            divEmptyAddReportTableColumn.Visible = false;
        }
    }

    protected void grdReportTableColumn_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    protected void grdReportTableColumn_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            bool objectNotFound = false;
            int columnId = (int)DataBinder.Eval(e.Row.DataItem, "ColumnID");
            string objectName = String.Empty;
            Column theColumn = RecordManager.ets_Column_Details(columnId);
            if (theColumn == null)
                objectNotFound = true;
            else
                objectName = theColumn.DisplayName;

            if (!objectNotFound)
            {
                Label lblObjectName = e.Row.FindControl("lblObjectName") as Label;
                if (lblObjectName != null)
                {
                    lblObjectName.Text = objectName;
                }
            }

            TextBox txtHeading = e.Row.FindControl("txtHeading") as TextBox;
            if (txtHeading != null)
            {
                txtHeading.Text = DataBinder.Eval(e.Row.DataItem, "ColumnTitle").ToString();
                if (_strActionMode == "view")
                    txtHeading.Enabled = false;
            }
        }
    }

    protected void btnRefreshReportTableColumn_Click(object sender, EventArgs e)
    {
        if (_iReportItemID.HasValue)
            PopulateReportTableColumn(_iReportItemID.Value);
    }

    protected void ReportTableColumnPager_BindTheGridAgain(object sender, EventArgs e)
    {
        if (_iReportItemID.HasValue)
            PopulateReportTableColumn(_iReportItemID.Value);
    }
     
    protected void PopulateTheRecord()
    {
        try
        {
            txtItemTitle.Text = _theReportItem.ItemTitle;
            ddlDateColumn.SelectedValue = _theReportItem.PeriodColumnID.ToString();
            cbApplyFilter.Checked = _theReportItem.ApplyFilter.HasValue && _theReportItem.ApplyFilter.Value;
            cbApplySort.Checked = _theReportItem.ApplySort.HasValue && _theReportItem.ApplySort.Value;
            cbUseColors.Checked = _theReportItem.UseColors.HasValue && _theReportItem.UseColors.Value;
            cbWarnings.Checked = _theReportItem.HighlightWarnings.HasValue && _theReportItem.HighlightWarnings.Value;

            switch (_theReportItem.ItemType)
            {
                case ReportItem.ReportItemType.ItemTypeView:
                    ddlView.SelectedValue = _theReportItem.ObjectId.ToString();
                    break;
                case ReportItem.ReportItemType.ItemTypeGraph:
                    string itemId = _theReportItem.ObjectId.ToString();
                    if (ddlChart.Items.FindByValue(itemId) == null)
                    {
                        GraphOption graph = GraphManager.ets_GraphOption_Detail(_theReportItem.ObjectId);
                        ddlChart.Items.Add(new ListItem()
                        {
                            Text = graph == null ? "?" : graph.Heading,
                            Value = itemId
                        });
                    }
                    ddlChart.SelectedValue = itemId;
                    break;
            }

            if ((_strActionMode == "edit") || (_strActionMode == "add"))
            {
                ViewState["theReportItem"] = _theReportItem;
            }
            else if (_strActionMode == "view")
            {
                divEdit.Visible = true;
                hlEditLink.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                         "Pages/ReportWriter/ReportTableDetail.aspx" +
                                         "?mode=" + Cryptography.Encrypt("edit") +
                                         "&SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString() +
                                         "&ReportID=" + Request.QueryString["ReportID"].ToString() +
                                         "&ReportItemID=" + Request.QueryString["ReportItemID"].ToString();
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Report Table", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }

    protected void EnableTheRecordControls(bool enabled)
    {        
        txtItemTitle.Enabled = enabled;
        ddlChart.Enabled = enabled;
        ddlView.Enabled = enabled;
        ddlDateColumn.Enabled = enabled;
        cbApplyFilter.Enabled = enabled;
        cbApplySort.Enabled = enabled;
        cbUseColors.Enabled = enabled;
        cbWarnings.Enabled = enabled;
    }

    protected bool IsUserInputOK(out string message)
    {
        //this is the final server side vaidation before database action
        message = String.Empty;

        string qsReportId = Cryptography.Decrypt(Request.QueryString["ReportID"]);
        int reportId = int.Parse(qsReportId);
        DataTable dtReportItems = Common.DataTableFromText("SELECT * FROM ReportItem WHERE ReportID =" + reportId.ToString() + " ORDER BY ItemPosition");
        string s = txtItemTitle.Text;
        foreach (DataRow row in dtReportItems.Rows)
        {
            if ((int) row["ReportItemID"] != _iReportItemID && String.Compare(s, row["ItemTitle"].ToString(),
                    StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                message = "Title shoud be unique";
                return false;
            }
        }

        List<string> headings = new List<string>();
        for (int i = 0; i < grdReportTableColumn.Rows.Count; i++)
        {
            string s2 = ((TextBox)grdReportTableColumn.Rows[i].FindControl("txtHeading")).Text;
            if (headings.Contains(s2))
            {
                message = "All column titles shoud be unique";
                return false;
            }
            else
            {
                headings.Add(s2);
            }
        }

        return true;
    }

    protected void lnkSave_Click(object sender, EventArgs e)
    {
        string strBackURL = hlBack.NavigateUrl;
        if (SaveChanges())
            Response.Redirect(strBackURL, false);
    }

    private void PopulateColumnList()
    {
        DataTable dtColumns = Common.DataTableFromText(@"SELECT ColumnID, DisplayName, SystemName FROM [Column] WHERE 
            SystemName NOT IN ('IsActive','TableID') AND DisplayName IS NOT NULL AND LEN(DisplayName) > 0  
            AND (ColumnType LIKE 'datetime' OR ColumnType LIKE 'date') AND TableID = " + _theReportItem.ObjectId.ToString() + @" ORDER BY DisplayName ASC");

        ddlDateColumn.Items.Clear();

        ddlDateColumn.Items.Add(new ListItem()
        {
            Text = "-- Not Applicable --",
            Value = "",
            Selected = true
        });
        foreach (DataRow dr in dtColumns.Rows)
        {
            ListItem liTemp = new ListItem(dr["DisplayName"].ToString(), dr["ColumnID"].ToString());
            ddlDateColumn.Items.Add(liTemp);
        }
    }

    protected void ReportTableColumnPager_OnSaveChangesAction(object sender, EventArgs e)
    {
        SaveChanges();
    }

    private bool SaveChanges()
    {
        try
        {
            string message = string.Empty;
            if (IsUserInputOK(out message))
            {
                if ((_strActionMode == "add") || (_strActionMode == "edit"))
                {
                    ReportItem editReportItem = (ReportItem)ViewState["theReportItem"];
                    editReportItem.ItemTitle = txtItemTitle.Text;
                    editReportItem.ApplyFilter = cbApplyFilter.Checked;
                    editReportItem.ApplySort = cbApplySort.Checked;
                    editReportItem.UseColors = cbUseColors.Checked;
                    editReportItem.HighlightWarnings = cbWarnings.Checked;
                    int periodColumnId = 0;
                    if (int.TryParse(ddlDateColumn.SelectedValue, out periodColumnId))
                        editReportItem.PeriodColumnID = periodColumnId;
                    else
                        editReportItem.PeriodColumnID = null;

                    switch (_theReportItem.ItemType)
                    {
                        case ReportItem.ReportItemType.ItemTypeView:
                            int viewObjectId = 0;
                            if (int.TryParse(ddlView.SelectedValue, out viewObjectId))
                                editReportItem.ObjectId = viewObjectId;
                            break;
                        case ReportItem.ReportItemType.ItemTypeGraph:
                            int chartObjectId = 0;
                            if (int.TryParse(ddlChart.SelectedValue, out chartObjectId))
                                editReportItem.ObjectId = chartObjectId;
                            break;
                    }

                    ReportManager.ets_ReportItem_Update(editReportItem);

                    //now lets update Items

                    for (int i = 0; i < grdReportTableColumn.Rows.Count; i++)
                    {
                        string strReportTableColumnId = ((Label)grdReportTableColumn.Rows[i].FindControl("LblID")).Text;

                        ReportTableItem theReportTableItem = ReportManager.ets_ReportTableItem_Detail(int.Parse(strReportTableColumnId));

                        if (theReportTableItem != null)
                        {
                            theReportTableItem.Position = i;
                            theReportTableItem.ItemTitle = ((TextBox)grdReportTableColumn.Rows[i].FindControl("txtHeading")).Text;
                            if (String.IsNullOrEmpty(theReportTableItem.ItemTitle.Trim()))
                            {
                                Column theColumn = RecordManager.ets_Column_Details((int)theReportTableItem.ColumnId);
                                theReportTableItem.ItemTitle = theColumn.DisplayName;
                            }
                            ReportManager.ets_ReportTableItem_Update(theReportTableItem);
                        }
                    }
                }
                return true;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "message_alert",
                    "alert('" + message + "');", true);
                return false;
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Report Table", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
            return false;
        }
    }
}

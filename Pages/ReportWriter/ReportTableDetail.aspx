﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true"
    CodeFile="ReportTableDetail.aspx.cs" Inherits="Pages_ReportWriter_ReportTableDetail" %>

<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>
<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <style type="text/css">
        .sortHandleVT
        {
            cursor: move;
        }
        
        .cssplaceholder
        {
            border-top: 2px solid #00FFFF;
            border-bottom: 2px solid #00FFFF;
        }

        .gridview_header th {
            text-align: left;
            padding-left: 50px;
        }
    </style>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            initElements();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initElements();
        });

        function initElements() {
            $('#divReportTableColumnSingleInstance').sortable({
                items: '.gridview_row',
                cursor: 'crosshair',
                helper: fixHelper,
                cursorAt: { left: 10, top: 10 },
                connectWith: '#divReportTableColumnSingleInstance',
                handle: '.sortHandleVT',
                axis: 'y',
                distance: 15,
                dropOnEmpty: true,
                receive: function(e, ui) {
                    $(this).find('tbody').append(ui.item);
                },
                start: function(e, ui) {
                    ui.placeholder.css('border-top', '2px solid #00FFFF');
                    ui.placeholder.css('border-bottom', '2px solid #00FFFF');
                },
                update: function(event, ui) {
                    var TC = '';
                    $('.ReportTableItemID').each(function() {
                        TC = TC + this.value.toString() + ',';
                    });
                    //alert(TC);
                    document.getElementById('hfReportTableColumnIDForColumnPosition').value = TC;
                    $('#btnReportTableColumnIDForColumnPosition').trigger('click');
                }
            });

            $('.popuplinkRIC').fancybox({
                iframe: {
                    css: {
                        width: '650px',
                        height: '390px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                'transitionIn': 'elastic',
                titleShow: false,
                afterShow: function () {
                    $(".ajax-indicator-full").hide();
                }
            });

            $('.popuplinkRIF').fancybox({
                iframe: {
                    css: {
                        width: '800px',
                        height: '400px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                'transitionIn': 'elastic',
                titleShow: false
            });

            $('.popuplinkRIS').fancybox({
                iframe: {
                    css: {
                        width: '680px',
                        height: '390px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                'transitionIn': 'elastic',
                titleShow: false
            });

            $("#cbApplyFilter").click(function () {
                if (this.checked) {
                    $("#hlFilter").trigger("click");
                }
            });

            $("#cbApplySort").click(function () {
                if (this.checked) {
                    $("#hlSortOrder").trigger("click");
                }
            });
        }

        function abc() {
            var b = document.getElementById('<%= lnkSave.ClientID %>');
            if (b && typeof (b.click) == 'undefined') {
                b.click = function () {
                    var result = true;
                    if (b.onclick) result = b.onclick();
                    if (typeof (result) == 'undefined' || result) {
                        eval(b.getAttribute('href'));
                    }
                }
            }
        }

        var fixHelper = function (e, ui) {
            ui.children().each(function () {
                $(this).width($(this).width());
            });

            return ui;
        };

        function checkAll(objRef) {
            var gridView = objRef.parentNode.parentNode.parentNode;
            var inputList = gridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type === "checkbox" && objRef !== inputList[i]) {
                    if (objRef.checked) {
                        //If the header checkbox is checked
                        //check all checkboxes                       
                        inputList[i].checked = true;
                    }
                    else {
                        //If the header checkbox is checked
                        //uncheck all checkboxes                        
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function SelectAllCheckboxes(spanChk) {
            checkAll(spanChk);
            // Added as ASPX uses SPAN for checkbox
            var oItem = spanChk.children;
            var theBox = (spanChk.type === "checkbox") ? spanChk : spanChk.children.item[0];
            var xState = theBox.checked;
            var elm = theBox.form.elements;
            for (var i = 0; i < elm.length; i++) {
                if (elm[i].type === "checkbox" && elm[i].id !== theBox.id) {
                    if (elm[i].checked !== xState)
                        elm[i].click();
                }
            }
        }
    </script>
    
 
    <table border="0" cellpadding="0" cellspacing="0"  align="center">
        <tr>
            <td colspan="3">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" style="width: 50%;">
                            <span class="TopTitle">
                                <asp:Label runat="server" ID="lblTitle"></asp:Label></span>
                        </td>
                        <td align="right">
                            <div style="width: 100%">
                                <%-- RP Modified Ticket 4267 Change Buttons width to 55px--%>
                                
                                <div style="float: right; width: 55px;" runat="server" id="divSave">
                                    <asp:LinkButton runat="server" ID="lnkSave" OnClick="lnkSave_Click" CausesValidation="true">
                                        <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                   ToolTip="Save" />
                                    </asp:LinkButton>
                                </div>
                                <div style="float: right; width: 55px;">
                                    <asp:HyperLink runat="server" ID="hlBack">
                                        <asp:Image runat="server" ID="imgBack" ImageUrl="~/App_Themes/Default/images/Back.png"
                                                   ToolTip="Back" />
                                    </asp:HyperLink>
                                </div>
                                <div style="float: right; width: 55px;" runat="server" id="divEdit" visible="false">
                                    <asp:HyperLink runat="server" ID="hlEditLink">
                                        <asp:Image runat="server" ID="Image2" ImageUrl="~/App_Themes/Default/images/Edit_big.png"
                                                   ToolTip="Edit" />
                                    </asp:HyperLink>
                                </div>
                                <%--End Modification --%>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="13">
            </td>
        </tr>
        <tr>
            <td valign="top">
                &nbsp;
            </td>
            <td valign="top">
                <div id="search" style="padding-bottom: 10px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                        ShowMessageBox="true" ShowSummary="false" HeaderText="Please correct the following errors:" />
                </div>
                <asp:Panel ID="Panel2" runat="server" DefaultButton="lnkSave">
                    <div runat="server" id="divDetail" onkeypress="abc();">
                        <table cellpadding="3" style="min-width: 600px;">
                            <tr>
                                <td align="right">
                                    <strong>Title*</strong>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtItemTitle" runat="server" Width="250px" CssClass="NormalTextBox"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvItemTitle" runat="server" ControlToValidate="txtItemTitle"
                                        ErrorMessage="Table Title - Required">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr runat="server" id="trSeparator1" style="height: 10px;">
                                <td colspan="2">
                                </td>
                            </tr>
                            <tr runat="server" id="tr2view" Visible="False">
                                <td align="right" style="width: 100px;">
                                    <strong>View*</strong>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlView" runat="server" Width="250px" CssClass="NormalTextBox"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr runat="server" id="tr2chart" Visible="False">
                                <td align="right" style="width: 100px;">
                                    <strong>Chart*</strong>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlChart" runat="server" Width="250px" CssClass="NormalTextBox"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr runat="server" id="tr2">
                                <td align="right" style="width: 250px;">
                                    <strong>Report Date Column</strong>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDateColumn" runat="server" Width="250px" CssClass="NormalTextBox"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr runat="server" id="trSeparator2" style="height: 10px;">
                                <td colspan="2">
                                </td>
                            </tr>
                            <tr runat="server" id="tr3">
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbApplyFilter" runat="server" ClientIDMode="Static"/>
                                    <asp:HyperLink ID="hlFilter" runat="server" ClientIDMode="Static" CssClass="popuplinkRIF">
                                        <span style="text-decoration: underline; color: Blue;">
                                            Filter Data...
                                        </span>
                                    </asp:HyperLink>
                                </td>
                            </tr>
                            <tr runat="server" id="tr4">
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbApplySort" runat="server" ClientIDMode="Static"/>
                                    <asp:HyperLink ID="hlSortOrder" runat="server" ClientIDMode="Static" CssClass="popuplinkRIS">
                                        <span style="text-decoration: underline; color: Blue;">
                                            Sort Data...
                                        </span>
                                    </asp:HyperLink>
                                </td>
                            </tr>
                            <tr runat="server" id="tr5">
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbUseColors" runat="server"/>
                                    <asp:Label runat="server" Text="Use cell colours"/>
                                </td>
                            </tr>
                            <tr runat="server" id="tr6">
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbWarnings" runat="server"/>
                                    <asp:Label runat="server" Text="Highlight warnings and exceedances"/>
                                </td>
                            </tr>
                            <tr runat="server" id="trInnerTable">
                                <td colspan="2">
                                    <div>
                                        <%-- <asp:UpdateProgress class="ajax-indicator" ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                            <ProgressTemplate>
                                                <table style="width: 100%; text-align: center">
                                                    <tr>
                                                        <td>
                                                            <asp:Image ID="Image1" runat="server" AlternateText="Processing..." ImageUrl="~/Images/ajax.gif" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>--%>

                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="always">
                                        <ContentTemplate>
                                        <br />
                                        <div style="padding-left: 20px; padding-top: 10px;" id="divReportTableColumnSingleInstance">
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <asp:HiddenField runat="server" ID="hfReportTableColumnIDForColumnPosition" ClientIDMode="Static" />
                                                        <asp:Button runat="server" ID="btnRefreshReportTableColumn" ClientIDMode="Static"
                                                                    Style="display: none;" OnClick="btnRefreshReportTableColumn_Click" />
                                                        <asp:Button runat="server" ID="btnReportTableColumnIDForColumnPosition" ClientIDMode="Static"
                                                                    Style="display: none;" OnClick="btnReportTableColumnIDForColumnPosition_Click" />
                                                        <%-- RP Modified Ticket 4267 Removed Width Property --%>
                                                        <dbg:dbgGridView ID="grdReportTableColumn" AllowPaging="True" runat="server" AutoGenerateColumns="false"
                                                            DataKeyNames="ReportTableItemID" PageSize="500" CssClass="gridview" OnRowCommand="grdReportTableColumn_RowCommand"
                                                            OnRowDataBound="grdReportTableColumn_RowDataBound" AlternatingRowStyle-BackColor="#DCF2F0">
                                                            <PagerSettings Position="Top" />
                                                            <RowStyle CssClass="gridview_row" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                    <HeaderTemplate>
                                                                        <input id="chkAll" onclick="javascript: checkAll(this);" runat="server"
                                                                            type="checkbox" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkDelete" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField Visible="false">
                                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="LblID" runat="server" Text='<%# Eval("ReportTableItemID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-CssClass="sortHandleVT">
                                                                    <ItemStyle HorizontalAlign="Center" Width="10px" />
                                                                    <ItemTemplate>
                                                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/App_Themes/Default/Images/MoveIcon.png"
                                                                                   ToolTip="Drag and drop to change order" />
                                                                        <input type="hidden" id='hfReportTableColumnID' value='<%# Eval("ReportTableItemID") %>'
                                                                               class='ReportTableItemID' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Column Name">
                                                                    <ItemTemplate>
                                                                        <div style="padding-left: 10px;">
                                                                            <asp:Label runat="server" ID="lblObjectName"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Column Title" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <div style="padding-left: 10px;">
                                                                            <asp:TextBox runat="server" ID="txtHeading" CssClass="NormalTextBox" Width="200px"></asp:TextBox>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Position" Visible="false">
                                                                    <ItemTemplate>
                                                                        <div style="padding-left: 10px;">
                                                                            <asp:TextBox runat="server" ID="txtColumnPosition" CssClass="NormalTextBox" Width="50px"></asp:TextBox>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="gridview_header" />
                                                            <PagerTemplate>
                                                                <asp:GridViewPager runat="server" ID="ReportTableColumnPager" OnDeleteAction="ReportTableColumnPager_DeleteAction"
                                                                                   OnSaveChangesAction="ReportTableColumnPager_OnSaveChangesAction"
                                                                                   HideRefresh="true" HideExport="true" HideGo="true" HideNavigation="true" HideFilter="true"
                                                                                   HidePageSizeButton="true" HidePageSize="True" ShowSaveChanges="True"
                                                                                   OnBindTheGridAgain="ReportTableColumnPager_BindTheGridAgain" />
                                                            </PagerTemplate>
                                                        </dbg:dbgGridView>
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>
                                        <br />
                                        <div runat="server" id="divEmptyAddReportTableColumn" visible="false" style="padding-left: 20px;">
                                            <asp:HyperLink runat="server" ID="hlAddReportTableColumn2" Style="text-decoration: none; color: Black;"
                                                           ToolTip="New Report" CssClass="popuplinkRIC">
                                                <asp:Image runat="server" ID="Image1" ImageUrl="~/App_Themes/Default/images/add32.png" />
                                            </asp:HyperLink>                             
                                            &nbsp;No items have been added yet.&nbsp;
                                            <asp:HyperLink runat="server" ID="hlAddReportTableColumn" Style="text-decoration: none; color: Black;"
                                                           CssClass="popuplinkRIC">
                                                <strong style="text-decoration: underline; color: Blue;">
                                                    Add new item now.
                                                </strong>
                                            </asp:HyperLink>
                                        </div>
                                        <br />
                                            </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="grdReportTableColumn" />
                                    </Triggers>
                                </asp:UpdatePanel>

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                    <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="13">
            </td>
        </tr>
    </table>
     

    <script type="text/javascript">
        function OpenReportItem() {
            $("[id*='hlAddReportTableColumn']").trigger('click');
        }
    </script>
</asp:Content>

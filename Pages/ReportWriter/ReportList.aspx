﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true"
CodeFile="ReportList.aspx.cs" Inherits="Pages_ReportWriter_ReportList" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            initElements();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initElements();
        });

        function initElements() {
            $("#Content").css("min-height", 100);
        }

        function OnClose() {
            parent.$.fancybox.close();
        }

        function CloseAndRefersh(id) {
            var button = $("[id*='lbDuplicateReport']", parent.document); // jQuery click() method will not cause postback on parent page!
            var input = $("[id*='txtSourceReportId']", parent.document);
            if (button && input) {
                parent.document.getElementById(input.attr("id")).value = id.toString(); 
                parent.document.getElementById(button.attr("id")).click();
            }
            OnClose();
        }
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table id="MainTable" style="margin-top: 10px; width: 350px;">
                <tr>
                    <td colspan="2" style="white-space: nowrap">
                        <asp:Label runat="server" ID="lblTitle" Font-Size="16px"
                                   Font-Bold="true" Text="Duplicate Report">
                        </asp:Label>
                    </td>
                    <td colspan="2">
                        <div runat="server" id="divCopy" style="float: right; width: 36px;">
                            <asp:LinkButton runat="server" ID="lnkCopy" CausesValidation="true" OnClick="lnkCopy_OnClick">
                                <asp:Image runat="server" ID="ImageCopy" ImageUrl="~/App_Themes/Default/images/save.png"
                                           ToolTip="Duplicate" />
                            </asp:LinkButton>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 1em;">&nbsp;</td>
                    <td colspan="2">
                        <asp:DropDownList ID="ddlReports" runat="server"
                                          DataValueField="ReportID" DataTextField="ReportName" Width="320px" >
                        </asp:DropDownList>
                    </td>
                    <td style="width: 1em;">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td colspan="2"><asp:Label ID="errorMessage" runat="server" ForeColor="red"></asp:Label></td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>        
    </asp:UpdatePanel>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_ReportWriter_ReportList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int iTN = 0;
            if (Session["AccountID"] != null)
            {
                DataTable dtReport = ReportManager.ets_Report_Select(null, Convert.ToInt32(Session["AccountID"]),
                    null, null, "", Report.ReportInterval.IntervalUndefined, null, null,
                    null, null, null,
                    "", "", null, null, out iTN);

                ddlReports.DataSource = dtReport;
                ddlReports.DataBind();
                ListItem liAny = new ListItem("--None--", "-1");
                ddlReports.Items.Insert(0, liAny);
            }
        }
    }

    protected void lnkCopy_OnClick(object sender, EventArgs e)
    {
        if (ddlReports.SelectedValue != "-1")
        {
            string script = String.Format("CloseAndRefersh({0});",
                ddlReports.SelectedValue);
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "close_script", script, true);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Pages_ReportWriter_ReportTableItemDetail : SecurePage
{
    private int? _iReportItemID = null;
    private ReportItem _theReportItem = null;


    protected void PopulateTerminology()
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _iReportItemID = int.Parse( Cryptography.Decrypt(Request.QueryString["ReportItemID"].ToString()));
        _theReportItem = ReportManager.ets_ReportItem_Detail((int)_iReportItemID);

        if (!IsPostBack)
        {
            PopulateListBoxes();
            PopulateTerminology();
        }
    }

    protected void lnkRemove_Click(object sender, EventArgs e)
    {
        if (lstUsed.SelectedItem != null)
        {
            for (int i = lstUsed.Items.Count - 1; i >= 0; --i)
            {
                if (lstUsed.Items[i].Selected)
                {
                    lstNotUsed.Items.Add(new ListItem(lstUsed.Items[i].Text, lstUsed.Items[i].Value));
                    lstUsed.Items.RemoveAt(i);
                }
            }
        }
        else
        {
            lstNotUsed.Focus();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "RemoveMessage", "alert('Please select a column from right side list.');", true);
        }
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        if (lstNotUsed.SelectedItem != null)
        {
            for (int i = lstNotUsed.Items.Count - 1; i >= 0; --i)
            {
                if (lstNotUsed.Items[i].Selected)
                {
                    lstUsed.Items.Add(new ListItem(lstNotUsed.Items[i].Text, lstNotUsed.Items[i].Value));
                    lstNotUsed.Items.RemoveAt(i);
                }
            }
        }
        else
        {
            lstUsed.Focus();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "RemoveMessage", "alert('Please select a column from left side list.');", true);
        }
    }

    protected void lnkSaveNew_Click(object sender, EventArgs e)
    {
        //DELETE Removed item.
        foreach (ListItem li in lstNotUsed.Items)
        {
            Common.ExecuteText(String.Format("DELETE FROM ReportTableItem WHERE ReportItemID = {0} AND ColumnID = {1}",
                _iReportItemID, li.Value));
        }

        //add new items
        if (_iReportItemID.HasValue)
        {
            foreach (ListItem li in lstUsed.Items)
            {
                DataTable dtTemp = Common.DataTableFromText(
                    String.Format("SELECT ReportTableItemID FROM ReportTableItem WHERE ReportItemID = {0} AND ColumnID = {1}",
                    _iReportItemID, li.Value));
                if (dtTemp.Rows.Count == 0)
                {
                    ReportTableItem newReportTableItem =
                        new ReportTableItem(null, _iReportItemID.Value, int.Parse(li.Value), li.Text, null);
                    ReportManager.ets_ReportTableItem_Insert(newReportTableItem);
                }
            }
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "RefreshGrid", "CloseAndRefresh();", true);
    }

    protected void PopulateListBoxes()
    {
        DataTable dtNotUsed = Common.DataTableFromText(@"SELECT ColumnID, DisplayName, SystemName FROM [Column] WHERE 
            SystemName NOT IN ('IsActive','TableID') AND DisplayName IS NOT NULL AND LEN(DisplayName) > 0  
            AND ColumnType NOT IN ('staticcontent') AND TableID = " + _theReportItem.ObjectId.ToString() +
            @" AND ColumnID NOT IN (SELECT ColumnID FROM ReportTableItem WHERE ReportItemID = " + _theReportItem.ReportItemID.ToString() + @") ORDER BY DisplayName ASC");

        lstNotUsed.Items.Clear();

        foreach (DataRow dr in dtNotUsed.Rows)
        {
            ListItem liTemp = new ListItem(dr["DisplayName"].ToString(), dr["ColumnID"].ToString());
            lstNotUsed.Items.Add(liTemp);
        }

        DataTable dtUsed = Common.DataTableFromText(@"SELECT ColumnID, DisplayName, SystemName FROM [Column] WHERE 
            SystemName NOT IN ('IsActive','TableID','EnteredBy') AND DisplayName IS NOT NULL AND LEN(DisplayName) > 0  
            AND ColumnType NOT IN ('staticcontent') AND TableID = " + _theReportItem.ObjectId.ToString() +
            @" AND ColumnID IN(SELECT ColumnID FROM ReportTableItem WHERE ReportItemID = " + _theReportItem.ReportItemID.ToString() + @") ORDER BY DisplayName ASC");

        lstUsed.Items.Clear();

        foreach (DataRow dr in dtUsed.Rows)
        {
            ListItem liTemp = new ListItem(dr["DisplayName"].ToString(), dr["ColumnID"].ToString());
            lstUsed.Items.Add(liTemp);
        }
    }
}
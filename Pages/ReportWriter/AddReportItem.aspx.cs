﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_ReportWriter_AddReportItem : System.Web.UI.Page
{
    int? _iReportId = null;
    Report _report = null;


    protected void Page_Load(object sender, EventArgs e)
    {
        _iReportId = int.Parse(Cryptography.Decrypt(Request.QueryString["ReportID"].ToString()));
        _report = ReportManager.ets_Report_Detail((int)_iReportId);

        if (!IsPostBack)
        {
            PopulateAccountList();
            PopulateListBoxes(Session["AccountID"].ToString());
            PopulateViewListBox();
            PopulateTerminology();
        }
    }

    protected void PopulateTerminology()
    {
    }

    protected void PopulateAccountList()
    {
        User user = (User)Session["User"];

        listAccountID.Items.Clear();
        DataTable dtAccountList = Common.DataTableFromText(
            String.Format("SELECT DISTINCT UserRole.AccountID, AccountName FROM "
                          + "UserRole INNER JOIN Account ON UserRole.AccountID = Account.AccountID "
                          + "WHERE UserID = {0} ORDER BY AccountName", user.UserID));
        if (dtAccountList.Rows.Count > 1)
        {
            listAccountID.Visible = true;
            foreach (DataRow row in dtAccountList.Rows)
            {
                listAccountID.Items.Add(new ListItem()
                {
                    Text = row["AccountName"].ToString(),
                    Value = row["AccountID"].ToString(),
                    Selected = row["AccountID"].ToString() == Session["AccountID"].ToString()
                });
            }
        }
        else
        {
            listAccountID.Visible = false;
        }
    }

    protected void PopulateListBoxes(string strAccountId)
    {
        listTableID.Items.Clear();
        DataTable dtActiveTableList =
            Common.DataTableFromText("SELECT TableID, TableName FROM [Table] WHERE IsActive=1 AND AccountID=" +
                                     strAccountId + " ORDER BY TableName");
        listTableID.Items.Add(new ListItem()
        {
            Text = "-- Please Select --",
            Value = "0"
        });
        foreach (DataRow row in dtActiveTableList.Rows)
        {
            listTableID.Items.Add(new ListItem()
            {
                Text = row["TableName"].ToString(),
                Value = row["TableID"].ToString()
            });
        }

        listGraphID.Items.Clear();
        DataTable dtActiveGraphList =
            Common.DataTableFromText(
                "SELECT GraphOptionID, GraphName FROM [GraphOption] WHERE IsActive=1 AND AccountID=" + strAccountId +
                " ORDER BY GraphName");
        listGraphID.Items.Add(new ListItem()
        {
            Text = "-- Please Select --",
            Value = "0"
        });
        foreach (DataRow row in dtActiveGraphList.Rows)
        {
            listGraphID.Items.Add(new ListItem()
            {
                Text = row["GraphName"].ToString(),   
                Value = row["GraphOptionID"].ToString()
            });
        }
    }

    protected void PopulateViewListBox()
    {
        listViewID.Items.Clear();
        DataTable dtActiveViewList =
            Common.DataTableFromText("SELECT DataViewID, ViewName FROM [DataView] WHERE IsActive=1 AND AccountID=" +
                                     Session["AccountID"].ToString() + " ORDER BY ViewName");
        listViewID.Items.Add(new ListItem()
        {
            Text = "-- Please Select --",
            Value = "0"
        });
        foreach (DataRow row in dtActiveViewList.Rows)
        {
            listViewID.Items.Add(new ListItem()
            {
                Text = row["ViewName"].ToString(),
                Value = row["DataViewID"].ToString()
            });
        }
    }

    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        if (dataTable.Checked && listTableID.SelectedValue == "0")
            errorMessage.Text = "Please select data table";
        else if (graph.Checked && listGraphID.SelectedValue == "0")
            errorMessage.Text = "Please select chart";
        //else if (view.Checked && listViewID.SelectedValue == "0")
        //    errorMessage.Text = "Please select data view";
        else
        {
            errorMessage.Text = String.Empty;
            int objectId = 0;
            string itemTitle = String.Empty;
            ReportItem.ReportItemType itemType = ReportItem.ReportItemType.ItemTypeUndefined;
            if (dataTable.Checked)
            {
                objectId = int.Parse(listTableID.SelectedValue);
                Table theTable = RecordManager.ets_Table_Details(objectId);
                if (theTable != null)
                    /* === Red 23042019: Returned original, 
                     * save the TableName in Report Item, 
                     * but hide/blank in the grid Report Table page Item Title 
                     * we need the ItemTitle for the sheetName === */
                    itemTitle = theTable.TableName;  // "" ; // theTable.TableName; //RP MODIFIED Ticket 4429
                itemType = ReportItem.ReportItemType.ItemTypeTable;
            }
            else if (graph.Checked)
            {
                objectId = int.Parse(listGraphID.SelectedValue);
                GraphOption theGraph = GraphManager.ets_GraphOption_Detail(objectId);
                if (theGraph != null)
                    itemTitle = listGraphID.SelectedItem.Text; //theGraph.Heading; //RP Modified Ticket 4429
                itemType = ReportItem.ReportItemType.ItemTypeGraph;
            }
            //else if (view.Checked)
            //{
            //    objectId = int.Parse(listViewID.SelectedValue);
            //    ReportView theView = ReportManager.ets_ReportView_Detail(objectId);
            //    if (theView != null)
            //        itemTitle = theView.ViewName;
            //    itemType = ReportItem.ReportItemType.ItemTypeView;
            //}
            if (_iReportId.HasValue)
            {
                try
                {
                    User user = (User)Session["User"];
                    ReportItem newReportItem = new ReportItem(null, _iReportId.Value,
                        objectId,
                        itemType,
                        itemTitle, false, false, false, false, null, null,
                        null, null, user.UserID);

                    int reportItemId = ReportManager.ets_ReportItem_Insert(newReportItem);
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "Add Report Item", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                    SystemData.ErrorLog_Insert(theErrorLog);
                }
            }
            else
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Add Report Item", "Report ID is null", "", DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "RefreshGrid", "CloseAndRefresh();", true);
        }
    }

    protected void listAccountID_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        string s = "table";
        if (graph.Checked)
            s = "graph";
        //else if (view.Checked)
        //    s = "view";
        PopulateListBoxes(listAccountID.SelectedValue);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "SetVisibility", String.Format("setVisibility(\"{0}\");", s), true);
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true"
    CodeFile="AddReportItem.aspx.cs" Inherits="Pages_ReportWriter_AddReportItem" EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        table
        {
            margin: 0 auto;
        }
        td
        {
            padding: 5px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            initElements();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initElements();
        });

        function initElements() {
            $("#Content").css("min-height", 100);
            $("select").css("max-width", "25em");

            $("#listTableID").change(function() {
                ClearError();
            });
            $("#listGraphID").change(function () {
                ClearError();
            });
            $("#listViewID").change(function () {
                ClearError();
            });
            $("#dataTable").click(function () {
                setVisibility("table");
            });
            $("#graph").click(function () {
                setVisibility("graph");
            });
            //$("#view").click(function () {
            //    setVisibility("view");
            //});
        }

        function setVisibility(mode) {
            switch (mode) {
            case "table":
                $("#listTableID").css("visibility", "visible").css("display", "block");
                $("#listGraphID").css("visibility", "hidden").css("display", "none");
                $("#listViewID").css("visibility", "hidden").css("display", "none");
                $("#listAccountID").css("visibility", "visible").css("display", "block");
                break;
            case "graph":
                $("#listTableID").css("visibility", "hidden").css("display", "none");
                $("#listGraphID").css("visibility", "visible").css("display", "block");
                $("#listViewID").css("visibility", "hidden").css("display", "none");
                $("#listAccountID").css("visibility", "visible").css("display", "block");
                break;
            case "view":
                $("#listTableID").css("visibility", "hidden").css("display", "none");
                $("#listGraphID").css("visibility", "hidden").css("display", "none");
                $("#listViewID").css("visibility", "visible").css("display", "block");
                $("#listAccountID").css("visibility", "hidden").css("display", "none");
                break;
            }
        }

        function ClearError() {
            $("[id*='errorMessage']").text("");
        }

        function CloseAndRefresh() {
            window.parent.document.getElementById('btnRefreshReportItem').click();
            parent.$.fancybox.close();
            return false;
        }
    </script>
    
    <table border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td colspan="2" style="white-space: nowrap">
                <asp:Label runat="server" ID="lblDetailTitle" Font-Size="16px"
                           Font-Bold="true" Text="Add Sheet">
                </asp:Label>
            </td>
            <td style="text-align: right;">
                <div runat="server" id="divSave" style="float: right;">
                    <asp:LinkButton runat="server" ID="lnkAdd" CausesValidation="true" OnClick="lnkAdd_OnClick">
                        <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                   ToolTip="Save" />
                    </asp:LinkButton>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table id="MainTable" style="margin-top: 10px; width: 350px;">
                            <tr>
                                <td colspan="3">
                                    <asp:RadioButton GroupName="itemType" ID="dataTable" Checked="true" runat="server" ClientIDMode="Static" />
                                    <label for="dataTable">Data</label>
                                    <asp:RadioButton GroupName="itemType" ID="graph" Checked="false" runat="server" ClientIDMode="Static" />
                                    <label for="graph">Graph</label>
                                    <%--<asp:RadioButton GroupName="itemType" ID="view" Checked="false" runat="server" ClientIDMode="Static" />
                                    <label for="graph">View</label>--%>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="listAccountID" ClientIDMode="Static" AutoPostBack="True" OnSelectedIndexChanged="listAccountID_OnSelectedIndexChanged"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="listTableID" ClientIDMode="Static" style="visibility: visible; display: block;"/>
                                    <asp:DropDownList runat="server" ID="listGraphID" ClientIDMode="Static" style="visibility: hidden; display: none"/>
                                    <asp:DropDownList runat="server" ID="listViewID" ClientIDMode="Static" style="visibility: hidden; display: none"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2"><asp:Label ID="errorMessage" runat="server" ForeColor="red"></asp:Label></td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>        
    </table>
</asp:Content>

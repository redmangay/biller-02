﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class Pages_ReportWriter_ReportTableSortOrder : System.Web.UI.Page
{
    private int _iReportItemID = -1;
    private int _iTableID = -1;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ReportItemID"] != null)
            _iReportItemID = int.Parse(Cryptography.Decrypt(Request.QueryString["ReportItemID"].ToString()));
        ReportItem reportItem = ReportManager.ets_ReportItem_Detail(_iReportItemID);
        if (reportItem != null)
            _iTableID = reportItem.ObjectId;

        if (Request.QueryString["TableID"]!=null)
            _iTableID =int.Parse( Request.QueryString["TableID"].ToString());

        if (!IsPostBack)
        {
            PopulateReportItemSortOrder();
        }
    }


    protected void obSortOrderChanged(object sender, EventArgs e)
    {
        Pages_UserControl_OrderBy obReportItemSortOrder = sender as Pages_UserControl_OrderBy;

        if (obReportItemSortOrder != null)
        {
            GridViewRow row = obReportItemSortOrder.NamingContainer as GridViewRow;
            ImageButton imgbtnPlus = row.FindControl("imgbtnPlus") as ImageButton;
            imgbtnPlus.Visible = !String.IsNullOrEmpty(obReportItemSortOrder.ddlColumnV);
        }
    }

    protected void grdReportItemSortOrder_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblID = e.Row.FindControl("lblID") as Label;
                Label lblReportItemSortOrderID = e.Row.FindControl("lblReportItemSortOrderID") as Label;
                ImageButton imgbtnMinus = e.Row.FindControl("imgbtnMinus") as ImageButton;
                ImageButton imgbtnPlus = e.Row.FindControl("imgbtnPlus") as ImageButton;

                Pages_UserControl_OrderBy obReportItemSortOrder = e.Row.FindControl("obReportItemSortOrder") as Pages_UserControl_OrderBy;

                obReportItemSortOrder.TableID = _iTableID;
                obReportItemSortOrder.ColumnID = -1;

                obReportItemSortOrder.ShowThenByLabel = e.Row.RowIndex != 0;
                obReportItemSortOrder.ddlColumnV = DataBinder.Eval(e.Row.DataItem, "SortColumnID").ToString();
                obReportItemSortOrder.ddlDirectionV = DataBinder.Eval(e.Row.DataItem, "Direction").ToString();

                lblReportItemSortOrderID.Text = DataBinder.Eval(e.Row.DataItem, "ReportItemSortOrderID").ToString();
                lblID.Text = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
                imgbtnMinus.CommandArgument = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
                imgbtnPlus.CommandArgument = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
            }
        }
        catch
        {
            //
        }
    }

    protected void SetReportItemSortOrderRowData()
    {
        if (ViewState["dtReportItemSortOrder"] != null)
        {
            DataTable dtReportItemSortOrder = (DataTable)ViewState["dtReportItemSortOrder"];
            dtReportItemSortOrder.Rows.Clear();

            int iDisplayOrder = 0;
            if (grdReportItemSortOrder.Rows.Count > 0)
            {
                foreach (GridViewRow gvRow in grdReportItemSortOrder.Rows)
                {
                    Label lblID = gvRow.FindControl("lblID") as Label;
                    Label lblReportItemSortOrderID = gvRow.FindControl("lblReportItemSortOrderID") as Label;
                    Pages_UserControl_OrderBy obReportItemSortOrder = gvRow.FindControl("obReportItemSortOrder") as Pages_UserControl_OrderBy;

                    dtReportItemSortOrder.Rows.Add(lblID.Text, lblReportItemSortOrderID.Text, obReportItemSortOrder.ddlColumnV,
                        obReportItemSortOrder.ddlDirectionV, iDisplayOrder.ToString());
                    iDisplayOrder = iDisplayOrder + 1;
                }
            }
            dtReportItemSortOrder.AcceptChanges();
            ViewState["dtReportItemSortOrder"] = dtReportItemSortOrder;
        }
    }

    protected void PopulateReportItemSortOrder()
    {
        DataTable dtReportItemSortOrder = new DataTable();
        dtReportItemSortOrder.Columns.Add("ID"); // 0
        dtReportItemSortOrder.Columns.Add("ReportItemSortOrderID"); // 1
        dtReportItemSortOrder.Columns.Add("SortColumnID"); // 2
        dtReportItemSortOrder.Columns.Add("Direction"); // 3
        dtReportItemSortOrder.Columns.Add("DisplayOrder"); // 4
        dtReportItemSortOrder.AcceptChanges();

        if (ViewState["dtReportItemSortOrder"] == null)
        {
            DataTable dtReportItemSortOrderDB = ReportManager.ets_ReportItemSortOrder_Select(_iReportItemID);

            if (dtReportItemSortOrder != null && dtReportItemSortOrderDB != null)
            {
                foreach (DataRow dr in dtReportItemSortOrderDB.Rows)
                {
                    DataRow newRow = dtReportItemSortOrder.NewRow();
                    newRow["ID"] = Guid.NewGuid().ToString();
                    newRow["ReportItemSortOrderID"] = dr["ReportItemSortOrderID"].ToString();
                    newRow["SortColumnID"] = dr["SortColumnID"].ToString();
                    newRow["Direction"] = dr["IsDescending"] != DBNull.Value && (bool)dr["IsDescending"] ? "desc" : "asc";
                    newRow["DisplayOrder"] = dr["DisplayOrder"].ToString();
                    dtReportItemSortOrder.Rows.Add(newRow);
                }
                dtReportItemSortOrder.AcceptChanges();

                if(dtReportItemSortOrder.Rows.Count == 0)
                {
                    dtReportItemSortOrder.Rows.Add(Guid.NewGuid().ToString(), "-1", "", "asc", "1");
                    dtReportItemSortOrder.AcceptChanges();
                }

                grdReportItemSortOrder.DataSource = dtReportItemSortOrder;
                grdReportItemSortOrder.DataBind();

                ViewState["dtReportItemSortOrder"] = dtReportItemSortOrder;
            }
        }
        else
        {
            if (ViewState["dtReportItemSortOrder"] == null)
            {
                if (Session["dtReportItemSortOrder"] == null)
                {
                   dtReportItemSortOrder.Rows.Add(Guid.NewGuid().ToString(), "-1", "", "asc", "1");
                   grdReportItemSortOrder.DataSource = dtReportItemSortOrder;
                   grdReportItemSortOrder.DataBind();

                   ViewState["dtReportItemSortOrder"] = dtReportItemSortOrder;
               }
               else
               {
                   dtReportItemSortOrder = (DataTable)Session["dtReportItemSortOrder"];
                   grdReportItemSortOrder.DataSource = dtReportItemSortOrder;
                   grdReportItemSortOrder.DataBind();

                   ViewState["dtReportItemSortOrder"] = dtReportItemSortOrder;
               }
            }
            else
            {
                dtReportItemSortOrder = (DataTable)ViewState["dtReportItemSortOrder"];
                grdReportItemSortOrder.DataSource = dtReportItemSortOrder;
                grdReportItemSortOrder.DataBind();
            }
        }
    }

    protected void grdReportItemSortOrder_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            SetReportItemSortOrderRowData();

            if (e.CommandName == "minus")
            {
                if (ViewState["dtReportItemSortOrder"] != null)
                {
                    DataTable dtReportItemSortOrder = (DataTable)ViewState["dtReportItemSortOrder"];

                    if (dtReportItemSortOrder.Rows.Count == 1)
                    {
                        return;
                    }

                    for (int i = dtReportItemSortOrder.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dtReportItemSortOrder.Rows[i];
                        if (dr["id"].ToString() == e.CommandArgument.ToString())
                        {
                            dr.Delete();
                            break;
                        }
                    }
                    dtReportItemSortOrder.AcceptChanges();
                    ViewState["dtReportItemSortOrder"] = dtReportItemSortOrder;
                    PopulateReportItemSortOrder();
                }
            }
            else if (e.CommandName == "plus")
            {
                if (ViewState["dtReportItemSortOrder"] != null)
                {
                    DataTable dtReportItemSortOrder = (DataTable)ViewState["dtReportItemSortOrder"];

                    int iPos = 0;

                    for (int i = 0; i < dtReportItemSortOrder.Rows.Count; i++)
                    {
                        if (dtReportItemSortOrder.Rows[i][0].ToString() == e.CommandArgument.ToString())
                        {
                            iPos = i;
                            break;
                        }
                    }

                    DataRow newRow = dtReportItemSortOrder.NewRow();
                    newRow["ID"] = Guid.NewGuid().ToString();
                    newRow["ReportItemSortOrderID"] = "-1";
                    newRow["SortColumnID"] = "";
                    newRow["Direction"] = "asc";
                    newRow["DisplayOrder"] = (dtReportItemSortOrder.Rows.Count + 1).ToString();

                    dtReportItemSortOrder.Rows.InsertAt(newRow, iPos + 1);
                    dtReportItemSortOrder.AcceptChanges();
                    ViewState["dtReportItemSortOrder"] = dtReportItemSortOrder;
                    PopulateReportItemSortOrder();
                }
            }
        }
        catch
        {
            //
        }
    }

    protected void lnkSave_Click(object sender, EventArgs e)
    {

        SetReportItemSortOrderRowData();

        if (ViewState["dtReportItemSortOrder"] != null)
        {
            DataTable dtOldReportItemSortOrder = ReportManager.ets_ReportItemSortOrder_Select(_iReportItemID);

            DataTable dtReportItemSortOrder = (DataTable)ViewState["dtReportItemSortOrder"];

            int iOldRowsCount = dtOldReportItemSortOrder.Rows.Count;
            string strActiveReportItemSortOrderIDs = "-1";
            int iDO = 1;

            foreach (DataRow drSW in dtReportItemSortOrder.Rows)
            {
                if (!String.IsNullOrEmpty(drSW["SortColumnID"].ToString()))
                {
                    bool bInsert = iOldRowsCount < iDO;

                    ReportItemSortOrder theReportItemSortOrder = new ReportItemSortOrder()
                    {
                        ReportItemSortOrderID = bInsert
                            ? -1
                            : int.Parse(dtOldReportItemSortOrder.Rows[iDO - 1]["ReportItemSortOrderID"].ToString()),
                        ReportItemID = _iReportItemID,
                        SortColumnID = int.Parse(drSW["SortColumnID"].ToString()),
                        IsDescending = drSW["Direction"].ToString() == "desc",
                        DisplayOrder = iDO
                    };
                    if (bInsert)
                    {
                        theReportItemSortOrder.ReportItemSortOrderID =
                            ReportManager.ets_ReportItemSortOrder_Insert(theReportItemSortOrder);
                    }
                    else
                    {
                        ReportManager.ets_ReportItemSortOrder_Update(theReportItemSortOrder);
                    }

                    strActiveReportItemSortOrderIDs = strActiveReportItemSortOrderIDs + "," +
                                                   theReportItemSortOrder.ReportItemSortOrderID.ToString();
                    iDO = iDO + 1;
                }
            }
            Common.ExecuteText(@"DELETE ReportItemSortOrder WHERE ReportItemID = " + _iReportItemID.ToString() 
                + @" AND ReportItemSortOrderID NOT IN (" + strActiveReportItemSortOrderIDs + @")");
        }

        if (ViewState["dtReportItemSortOrder"] != null)
        {
            Session["dtReportItemSortOrder"] = (DataTable)ViewState["dtReportItemSortOrder"];
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Save Action", "GetBackValueEdit();", true);
    }
}
﻿using System;
using System.Drawing;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_ReportWriter_ReportDetail : SecurePage
{

    private string _strActionMode = "view";
    private int? _iReportId;
    private string _qsReportId = "";
    private User _objUser;


    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Request.QueryString["popup"] != null)
        {
            this.Page.MasterPageFile = "~/Home/PopUp.master";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        //ReportManager rm = new ReportManager();
        //rm.RunScheduledReports();

        _objUser = (User)Session["User"];
        if (!IsPostBack)
        {
            //if (!Common.HaveAccess(Session["roletype"].ToString(), "1,2,"))
            //{ Response.Redirect("~/Default.aspx", false); }

            PopulatePeriodDDL();
            PopulatePeriodEndDDL();

            if (Request.QueryString["popup"] != null)
            {
                divBack.Visible = false;
                lblSubtitle.Visible = false;
            }
        }

        if (Request.QueryString["mode"] == null)
        {
            Server.Transfer("~/Default.aspx");
        }
        else
        {
            string qsMode = Cryptography.Decrypt(Request.QueryString["mode"]);

            if (qsMode == "add" ||
                qsMode == "view" ||
                qsMode == "edit")
            {
                _strActionMode = qsMode.ToLower();

                if (Request.QueryString["ReportID"] != null)
                {
                    _qsReportId = Cryptography.Decrypt(Request.QueryString["ReportID"]);
                    _iReportId = int.Parse(_qsReportId);
                }
            }
            else
            {
                Server.Transfer("~/Default.aspx");
            }

            string strTitle = "Report Detail";
            switch (_strActionMode)
            {
                case "add":
                    strTitle = "Create Report";
                    if (_iReportId.HasValue && !IsPostBack)
                        PopulateTheRecord();
                    break;
                case "view":
                    strTitle = "View Report";
                    PopulateTheRecord();
                    EnableTheRecordControls(false);
                    divSave.Visible = false;
                    divEdit.Visible = true;
                    break;
                case "edit":
                    strTitle = "Report Properties";
                    if (!IsPostBack)
                    {
                        PopulateTheRecord();
                    }
                    break;
                default:
                    Server.Transfer("~/Default.aspx");
                    break;
            }

            Title = strTitle;
            lblTitle.Text = strTitle;
        }
    }

    protected void PopulatePeriodDDL()
    {
        ddlPeriod.Items.Add(new ListItem() { Text = "30 Days", Value = Report.ReportInterval.Interval30Days.ToString() });
        ddlPeriod.Items.Add(new ListItem() { Text = "60 Days", Value = Report.ReportInterval.Interval60Days.ToString() });
        ddlPeriod.Items.Add(new ListItem() { Text = "90 Days", Value = Report.ReportInterval.Interval90Days.ToString() });
        ddlPeriod.Items.Add(new ListItem() { Text = "1 Year", Value = Report.ReportInterval.Interval1Year.ToString() });
        ddlPeriod.Items.Add(new ListItem() { Text = "YTD", Value = Report.ReportInterval.IntervalYTD.ToString() });
        ddlPeriod.Items.Add(new ListItem() { Text = "Custom", Value = Report.ReportInterval.IntervalCustom.ToString() });
    }

    protected void PopulatePeriodEndDDL()
    {
        ddlPeriodEnd.Items.Add(new ListItem() { Text = "Today", Value = Report.ReportIntervalEnd.Today.ToString() });
        ddlPeriodEnd.Items.Add(new ListItem() { Text = "End of last week", Value = Report.ReportIntervalEnd.EndOfLastWeek.ToString() });
        ddlPeriodEnd.Items.Add(new ListItem() { Text = "End of last month", Value = Report.ReportIntervalEnd.EndOfLastMonth.ToString() });
        ddlPeriodEnd.Items.Add(new ListItem() { Text = "End of last year", Value = Report.ReportIntervalEnd.EndOfLastYear.ToString() });
        ddlPeriodEnd.Items.Add(new ListItem() { Text = "Last data point on first sheet", Value = Report.ReportIntervalEnd.LastDataPoint.ToString() });
    }

    protected bool IsUserInputOK(out string message)
    {
        //this is the final server side vaidation before database action
        message = String.Empty;
        return true;
    }

    protected void PopulateTheRecord()
    {
        if (_iReportId.HasValue)
        {
            try
            {
                Report report = ReportManager.ets_Report_Detail(_iReportId.Value);
                txtReportName.Text = report.ReportName;
                txtReportDescription.Text = report.ReportDescription;

                optReportDates.SelectedValue = report.ReportDates == Report.ReportInterval.IntervalDateRange
                    ? (report.ReportStartDate.HasValue && report.ReportEndDate.HasValue ? "dateRange" : "dateOnRun")
                    : "period";
                optReportDates_SelectedIndexChanged(null, null);

                if (report.ReportDatesBefore != Report.ReportIntervalEnd.EndOfCustom ||
                   report.ReportDatesBefore != Report.ReportIntervalEnd.EndOfCustom2)
                {
                    //RP Modified Ticket 4513
                    if (report.ReportDatesBefore == Report.ReportIntervalEnd.EndOfCustom &&
                                            report.DaysTo.HasValue)
                    {
                        ddlPeriodEnd.SelectedValue = Report.ReportIntervalEnd.EndOfCustom.ToString();
                        ddlPeriod_SelectedIndexChanged(null, null);
                        txtDays.Text = report.DaysFrom.ToString();
                    }
                    else
                    {
                        ddlPeriod.SelectedValue = report.ReportDates.ToString();
                        ddlPeriodEnd.SelectedValue = report.ReportDatesBefore.ToString();
                    }
                    //End Modification
                }
                if (report.ReportDates != Report.ReportInterval.IntervalUndefined &&
                    report.ReportDates != Report.ReportInterval.IntervalDateRange)
                {
                    //RP Modified Ticket 4513
                    if (report.ReportDates == Report.ReportInterval.IntervalCustom &&
                                            report.DaysFrom.HasValue)
                    {
                        ddlPeriod.SelectedValue = Report.ReportInterval.IntervalCustom.ToString();
                        ddlPeriod_SelectedIndexChanged(null, null);
                        txtDays.Text = report.DaysFrom.ToString();
                    }
                    else
                    {
                        ddlPeriod.SelectedValue = report.ReportDates.ToString();
                        ddlPeriodEnd.SelectedValue = report.ReportDatesBefore.ToString();
                    }
                    //End Modification
                }                
                else if (report.ReportDates == Report.ReportInterval.IntervalDateRange &&
                         report.ReportStartDate.HasValue && report.ReportEndDate.HasValue)
                {
                    txtStartDate.Text = report.ReportStartDate.Value.ToShortDateString();
                    txtEndDate.Text = report.ReportEndDate.Value.ToShortDateString();
                }

                if (report.ReportDates != Report.ReportInterval.IntervalUndefined)
                {
                    ReportSchedule reportSchedule = ReportManager.ets_ReportSchedule_Detail(_iReportId.Value);
                    if (reportSchedule != null)
                    {
                        if (reportSchedule.Frequency != ReportSchedule.ReportScheduleFrequency.Undefined)
                        {
                            lblWarning.Text = "Report schedule will be changed to \"Not scheduled\"";
                            lblWarning.ForeColor = Color.Red;
                        }
                    }
                    else
                    {
                        lblWarning.Text = "Report schedule will be set to \"Not scheduled\"";
                        lblWarning.ForeColor = Color.Blue;
                    }
                }

                divSave.Visible = true;

                if (_strActionMode == "edit" || (_strActionMode == "add" && _iReportId.HasValue))
                {
                    ViewState["theReport"] = report;
                }
                else if (_strActionMode == "view")
                {
                    hlEditLink.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                             "Pages/ReportWriter/ReportDetail.aspx" +
                                             "?mode=" + Cryptography.Encrypt("edit") +
                                             "&ReportID=" + Request.QueryString["ReportID"].ToString() +
                                             "&SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString();
                }
            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Report Detail", ex.Message, ex.StackTrace, DateTime.Now,
                    Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
                lblMsg.Text = ex.Message;
            }
        }
    }

    protected void EnableTheRecordControls(bool enable)
    {
        txtReportName.Enabled = enable;
        txtReportDescription.Enabled = enable;
        optReportDates.Enabled = enable;
        ddlPeriod.Enabled = enable;
        ddlPeriodEnd.Enabled = enable;
        txtStartDate.Enabled = enable;
        txtEndDate.Enabled = enable;
    }

    protected void lnkSave_Click(object sender, EventArgs e)
    {
        lblMsg.Text = "";

        try
        {
            string message = String.Empty;
            if (IsUserInputOK(out message))
            {
                Report.ReportInterval reportInterval = Report.ReportInterval.IntervalUndefined;
                Report.ReportIntervalEnd reportIntervalEnd = Report.ReportIntervalEnd.Undefined;
                DateTime? dtDateFrom = null;
                DateTime? dtDateTo = null;

                switch (optReportDates.SelectedValue)
                {
                    case "dateRange":
                        reportInterval = Report.ReportInterval.IntervalDateRange;
                        if (!String.IsNullOrEmpty(txtStartDate.Text) && !String.IsNullOrEmpty(txtEndDate.Text))
                        {
                            DateTime dtTemp;
                            if (DateTime.TryParseExact(txtStartDate.Text.Trim(), Common.Dateformats,
                                new CultureInfo("en-GB"), DateTimeStyles.None, out dtTemp))
                            {
                                dtDateFrom = dtTemp;
                            }
                            if (DateTime.TryParseExact(txtEndDate.Text.Trim(), Common.Dateformats,
                                new CultureInfo("en-GB"), DateTimeStyles.None, out dtTemp))
                            {
                                dtDateTo = dtTemp;
                            }
                        }
                        break;
                    case "dateOnRun":
                        reportInterval = Report.ReportInterval.IntervalDateRange;
                        break;
                    case "period":
                        reportInterval = (Report.ReportInterval) Enum.Parse(typeof(Report.ReportInterval), ddlPeriod.SelectedValue);
                        reportIntervalEnd = (Report.ReportIntervalEnd) Enum.Parse(typeof(Report.ReportIntervalEnd), ddlPeriodEnd.SelectedValue);
                        break;
                }

                switch (_strActionMode)
                {
                    case "add":
                        int reportId = 0;
                        if (_iReportId.HasValue)
                        {
                            Report editReport2 = (Report)ViewState["theReport"];
                            editReport2.ReportName = txtReportName.Text;
                            editReport2.ReportDescription = txtReportDescription.Text;
                            editReport2.ReportDates = reportInterval;
                            editReport2.ReportDatesBefore = reportIntervalEnd;
                            editReport2.ReportStartDate = dtDateFrom;
                            editReport2.ReportEndDate = dtDateTo;
                            editReport2.UserID = _objUser.UserID;
                            //RP Added Ticket 4513
                            if (reportInterval == Report.ReportInterval.IntervalCustom)
                            {
                                int days = 0;
                                Int32.TryParse(txtDays.Text, out days);
                                editReport2.DaysFrom = days;
                            }
                            //End Modification
                            ReportManager.ets_Report_Update(editReport2);
                            reportId = _iReportId.Value;
                        }
                        else
                        {
                            int days = 0;
                            if (reportInterval == Report.ReportInterval.IntervalCustom)
                            {
                                Int32.TryParse(txtDays.Text, out days);
                            }

                            Report newReport = new Report(null, AccountID,
                                txtReportName.Text, txtReportDescription.Text,
                                reportInterval, reportIntervalEnd, dtDateFrom, dtDateTo,
                                null, null, _objUser.UserID, days);

                            reportId = ReportManager.ets_Report_Insert(newReport);
                        }

                        if (optReportDates.SelectedValue == "dateOnRun")
                            ReportManager.ets_ReportSchedule_SetNotScheduled(reportId);

                        Response.Redirect(Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                          "Pages/ReportWriter/ReportTable.aspx" +
                                          "?ReportID=" + Cryptography.Encrypt(reportId.ToString()) +
                                          "&mode=" + Cryptography.Encrypt("add") +
                                          "&SearchCriteria=" + Request.QueryString["SearchCriteria"]);
                        break;
                    case "view":
                        break;
                    case "edit":
                        Report editReport = (Report)ViewState["theReport"];
                        editReport.ReportName = txtReportName.Text;
                        editReport.ReportDescription = txtReportDescription.Text;
                        editReport.ReportDates = reportInterval;
                        editReport.ReportDatesBefore = reportIntervalEnd;
                        editReport.ReportStartDate = dtDateFrom;
                        editReport.ReportEndDate = dtDateTo;
                        editReport.UserID = _objUser.UserID;
                        //RP Added Ticket 4513
                        if (reportInterval == Report.ReportInterval.IntervalCustom)
                        {
                            int days = 0;
                            Int32.TryParse(txtDays.Text, out days);
                            editReport.DaysFrom = days;
                        }
                        //End Modification
                        ReportManager.ets_Report_Update(editReport);
                        if (optReportDates.SelectedValue == "dateOnRun" && editReport.ReportID.HasValue)
                            ReportManager.ets_ReportSchedule_SetNotScheduled(editReport.ReportID.Value);

                        if (Request.QueryString["popup"] != null)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Refresh", "SavedAndRefresh();", true);
                        }
                        else
                        {
                            string navigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                                 "Pages/ReportWriter/Report.aspx" +
                                                 "?SearchCriteria=" + Request.QueryString["SearchCriteria"];
                            Response.Redirect(navigateUrl);
                        }
                        break;

                    default:
                        //?
                        break;
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "error", String.Format("alert('{0}');", message), true);
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Report Detail", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }

    public int AccountID
    {
        get
        {
            int retVal = 0;
            if (Session["AccountID"] != null)
                retVal = Convert.ToInt32(Session["AccountID"]);
            return retVal;
        }
    }

    protected void optReportDates_SelectedIndexChanged(object sender, EventArgs e)
    {
        trReportPeriod.Visible = optReportDates.SelectedValue == "period";
        trReportPeriodEnd.Visible = optReportDates.SelectedValue == "period";
        //RP Added Ticket 4513
        trDays.Visible = optReportDates.SelectedValue == "period";
        ddlPeriod_SelectedIndexChanged(null, null);
        //End Modification
        trDateStart.Visible = optReportDates.SelectedValue == "dateRange";
        trDateEnd.Visible = optReportDates.SelectedValue == "dateRange";
        rfvDateFrom.Enabled = optReportDates.SelectedValue == "dateRange";
        rfvDateTo.Enabled = optReportDates.SelectedValue == "dateRange";
        trWarning.Visible = optReportDates.SelectedValue == "dateOnRun";
    }

    protected void lbBack_OnClick(object sender, EventArgs e)
    {
        if (Request.QueryString["popup"] != null)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "OnClose", "Close();", true);
        }
        else
        {
            string navigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                 "Pages/ReportWriter/Report.aspx" +
                                 "?SearchCriteria=" + Request.QueryString["SearchCriteria"];
            Response.Redirect(navigateUrl);
        }
    }
    //RP Added Ticket 4513
    protected void ddlPeriod_SelectedIndexChanged(object sender, EventArgs e)
    {
        trDays.Visible = (Report.ReportInterval)Enum.Parse(typeof(Report.ReportInterval), ddlPeriod.SelectedValue) == Report.ReportInterval.IntervalCustom && optReportDates.SelectedValue == "period";
    }
}

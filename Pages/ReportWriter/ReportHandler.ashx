﻿<%@ WebHandler Language="C#" Class="ReportHandler" %>

using System.Collections.Generic;
using System;
using System.Data;
using System.IO;
using System.Web;
using XlsxExport;

public class ReportHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest (HttpContext context)
    {
        if (context.Request.QueryString["ReportID"] != null)
        {
            int reportId = 0;
            if (int.TryParse(Cryptography.Decrypt(context.Request.QueryString["ReportID"]), out reportId))
            {
                Report report = ReportManager.ets_Report_Detail(reportId);
                if ((report != null) && !String.IsNullOrEmpty(report.ReportName))
                {
                    DateTime? reportStartDate = null;
                    DateTime? reportEndDate = null;

                    switch (report.ReportDatesBefore)
                    {
                        case Report.ReportIntervalEnd.Today:
                            reportEndDate = DateTime.Today.AddDays(1);
                            break;
                        case Report.ReportIntervalEnd.EndOfLastWeek:
                            reportEndDate =
                                DateTime.Today.AddDays(DateTime.Today.DayOfWeek == DayOfWeek.Sunday
                                    ? -6
                                    : 1 - (int) DateTime.Today.DayOfWeek);
                            break;
                        case Report.ReportIntervalEnd.EndOfLastMonth:
                            reportEndDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                            break;
                        case Report.ReportIntervalEnd.EndOfLastYear:
                            reportEndDate = new DateTime(DateTime.Today.Year, 1, 1);
                            break;
                        case Report.ReportIntervalEnd.LastDataPoint:
                            reportEndDate = GetLastDataPoint(reportId);
                            break;
                         case Report.ReportIntervalEnd.EndOfCustom:
                            reportEndDate = DateTime.Today.AddDays(Int32.Parse(report.DaysTo.ToString()) * -1); 
                            break;
                        case Report.ReportIntervalEnd.EndOfCustom2:
                            reportEndDate = DateTime.Today.AddDays(Int32.Parse(report.DaysTo.ToString()));
                        break;
                    }

                    switch (report.ReportDates)
                    {
                        case Report.ReportInterval.Interval30Days:
                            if (reportEndDate.HasValue)
                                reportStartDate = reportEndDate.Value.AddDays(-30);
                            break;
                        case Report.ReportInterval.Interval60Days:
                            if (reportEndDate.HasValue)
                                reportStartDate = reportEndDate.Value.AddDays(-60);
                            break;
                        case Report.ReportInterval.Interval90Days:
                            if (reportEndDate.HasValue)
                                reportStartDate = reportEndDate.Value.AddDays(-90);
                            break;
                        case Report.ReportInterval.Interval1Year:
                            if (reportEndDate.HasValue)
                                reportStartDate = reportEndDate.Value.AddYears(-1);
                            break;
                        case Report.ReportInterval.IntervalCustom:
                            //if (reportEndDate.HasValue && report.DaysFrom.HasValue)
                            reportStartDate = DateTime.Today.AddDays(Int32.Parse(report.DaysFrom.ToString()) * -1);
                            //reportStartDate = reportEndDate.Value.AddDays((Int32.Parse(report.Days.ToString()) * -1)-1);
                            break;
                        case Report.ReportInterval.IntervalCustom2:
                            reportStartDate = DateTime.Today.AddDays(Int32.Parse(report.DaysFrom.ToString()));
                        //reportStartDate = reportEndDate.Value.AddDays((Int32.Parse(report.Days.ToString()) * -1)-1);
                            break;
                        case Report.ReportInterval.IntervalYTD:
                            if (reportEndDate.HasValue)
                                reportStartDate = new DateTime(reportEndDate.Value.Year, 1, 1, 0, 0, 0);
                            break;
                        case Report.ReportInterval.IntervalDateRange:
                            if (report.ReportStartDate.HasValue && report.ReportEndDate.HasValue)
                            {
                                reportStartDate = report.ReportStartDate.Value;
                                reportEndDate = report.ReportEndDate.Value.AddDays(1);
                            }
                            else
                            {
                                DateTime dtValue;
                                //System.Globalization.CultureInfo enAU = new System.Globalization.CultureInfo("en-AU");
                                //if (context.Request.QueryString["StartDate"] != null)
                                //    if (DateTime.TryParseExact(Cryptography.Decrypt(context.Request.QueryString["StartDate"]), "d", enAU,
                                //        System.Globalization.DateTimeStyles.None, out dtValue))
                                if(context.Request.QueryString["StartDate"]!=null)
                                {
                                    if (DateTime.TryParse(Cryptography.Decrypt(context.Request.QueryString["StartDate"].ToString()), out dtValue))
                                        reportStartDate = dtValue;
                                }



                                //if (context.Request.QueryString["EndDate"] != null)
                                //    if (DateTime.TryParseExact(Cryptography.Decrypt(context.Request.QueryString["EndDate"]), "d", enAU,
                                //        System.Globalization.DateTimeStyles.None, out dtValue))


                                if (context.Request.QueryString["EndDate"] != null)
                                    if (DateTime.TryParse(Cryptography.Decrypt(context.Request.QueryString["EndDate"]), out dtValue))
                                        reportEndDate = dtValue.AddDays(1);
                            }
                            break;
                    }

                    XlsxReportWriter xlw = new XlsxReportWriter();
                    using (MemoryStream stream = new MemoryStream())
                    {
                        xlw.CreateWorkbook(stream, reportId, reportStartDate, reportEndDate, int.Parse(System.Web.HttpContext.Current.Session["AccountID"].ToString()));
                        context.Response.ContentType =
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        context.Response.AppendHeader("Content-Disposition", String.Format("attachment;filename=\"{0}.xlsx\"", report.ReportName));
                        stream.Position = 0;
                        stream.CopyTo(context.Response.OutputStream);

                        /* == Red 09092019: Append cookie */ 
                        HttpCookie cookie = new HttpCookie("ExportReport");
                        cookie.Value = "Flag";
                        cookie.Expires = DateTime.Now.AddDays(1);
                        HttpContext.Current.Response.AppendCookie(cookie);
                        /* == End Red == */

                        context.Response.Flush();
                        context.Response.End();
                    }
                }
            }
        }
    }

    private DateTime? GetLastDataPoint(int reportId)
    {
        DateTime? dateReturn = null;

        DataTable dtReportItems = Common.DataTableFromText("SELECT ReportItemID, ItemType FROM ReportItem WHERE ReportID =" +
                                                           reportId.ToString() + " ORDER BY ItemPosition");

        foreach (DataRow row in dtReportItems.Rows)
        {
            int nItemType = (int) row["ItemType"];
            if (Enum.IsDefined(typeof(ReportItem.ReportItemType), nItemType))
            {
                ReportItem.ReportItemType itemType = (ReportItem.ReportItemType) nItemType;
                if (itemType == ReportItem.ReportItemType.ItemTypeTable)
                {
                    int reportItemId = (int) row["ReportItemID"];
                    ReportItem reportItem = ReportManager.ets_ReportItem_Detail(reportItemId);

                    if (reportItem.PeriodColumnID.HasValue)
                    {
                        WhereClause whereClauseSettings = null;
                        if (reportItem.ApplyFilter.HasValue && reportItem.ApplyFilter.Value)
                        {
                            DataTable filter = ReportManager.ets_ReportItemFilter_Select(reportItemId);
                            foreach (DataRow filterRow in filter.Rows)
                            {
                                string filterOperator = string.Empty;
                                if (filterRow.IsNull("FilterOperator") ||
                                    String.IsNullOrEmpty(filterRow["FilterOperator"].ToString()))
                                    break;
                                else
                                {
                                    filterOperator = filterRow["FilterOperator"].ToString();
                                }

                                if (whereClauseSettings == null)
                                    whereClauseSettings = new WhereClause((int) filterRow["FilterColumnID"],
                                        filterOperator, filterRow["FilterColumnValue"].ToString());
                                else
                                {
                                    string joinOperator = string.Empty;
                                    if (filterRow.IsNull("JoinOperator") ||
                                        String.IsNullOrEmpty(filterRow["JoinOperator"].ToString()))
                                        break;
                                    else
                                    {
                                        joinOperator = filterRow["JoinOperator"].ToString();
                                    }
                                    if (joinOperator == "or")
                                        whereClauseSettings.Or((int) filterRow["FilterColumnID"], filterOperator,
                                            filterRow["FilterColumnValue"].ToString());
                                    else
                                        whereClauseSettings.And((int) filterRow["FilterColumnID"], filterOperator,
                                            filterRow["FilterColumnValue"].ToString());
                                }
                            }
                        }

                        string whereClauseString = String.Empty;
                        if (whereClauseSettings != null)
                            whereClauseString = whereClauseSettings.GetWhereClauseString();

                        Column periodColumn = RecordManager.ets_Column_Details(reportItem.PeriodColumnID.Value);
                        if (periodColumn != null)
                        {
                            string query = String.Format(
                                "SET DATEFORMAT dmy; SELECT MAX(CONVERT(datetime, [dbo].[fnRemoveNonDate]([Record].[{0}]), 103)) FROM [Record] WHERE [TableID] = {1}",
                                periodColumn.SystemName, reportItem.ObjectId);
                            if (!String.IsNullOrEmpty(whereClauseString))
                                query += " AND " + whereClauseString;
                            DataTable dtLastDataPoint = Common.DataTableFromText(query);
                            if (dtLastDataPoint != null && dtLastDataPoint.Rows.Count > 0)
                                dateReturn = (DateTime)dtLastDataPoint.Rows[0][0];
                        }
                    }
                    return dateReturn;
                }
            }
        }
        return null;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}
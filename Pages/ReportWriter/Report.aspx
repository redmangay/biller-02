﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true"
    CodeFile="Report.aspx.cs" Inherits="Pages_ReportWriter_Report" EnableEventValidation="false" %>

<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>
<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <script language="javascript" type="text/javascript">
        $(document).ready(function() {
            initElements();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initElements();
        });

        function initElements() {
            $('.popuplinkRD').fancybox({
                iframe: {
                    css: {
                        width: '400px',
                        height: '200px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                titleShow: false
            });

            $("select").css("max-width", "25em");
        }

        function abc() {
            var b = document.getElementById('<%= lnkSearch.ClientID %>');
            if (b && typeof (b.click) == 'undefined') {
                b.click = function () {
                    var result = true;
                    if (b.onclick) result = b.onclick();
                    if (typeof (result) === 'undefined' || result) {
                        eval(b.getAttribute('href'));
                    }
                }
            }
        }

        function MouseEvents(objRef, evt) {
            var checkbox = objRef.getElementsByTagName("input")[0];
            if (evt.type === "mouseover") {
                objRef.style.backgroundColor = "#76BAF2";
                objRef.style.cursor = 'pointer';
            }
            else {
                if (checkbox != null && checkbox.checked) {
                    objRef.style.backgroundColor = "#96FFFF";
                }
                else if (evt.type === "mouseout") {
                    if (objRef.rowIndex % 2 === 0) {
                        //Alternating Row Color
                        objRef.style.backgroundColor = "white";
                    }
                    else {
                        objRef.style.backgroundColor = "#DCF2F0";
                    }
                }
            }
        }


        $(document).ready(function () {

            $(function () {
                $('.popupRunReportDate').fancybox({
                    iframe: {
                        css: {
                            width: '480px',
                            height: '350px'
                        }
                    },
                    toolbar: false,
                    smallBtn: true,
                    scrolling: 'auto',
                    type: 'iframe',
                    titleShow: false//,
                    //afterClose: function () {
                    //    $(".ajax-indicator-full").fadeIn();
                    //    __doPostBack('ctl00$lkChangeAccount', '');
                    //}

                });

            });
        });
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="0" cellspacing="0" width="928" align="center">
                <tr>
                    <td colspan="3" height="40">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" style="width: 50%;">
                                    <span class="TopTitle">Reports</span>
                                </td>
                                <td align="left">&nbsp;</td>
                                <td style="width: 200px;">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" height="13">
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                    </td>
                    <td valign="top">
                        <asp:Panel ID="Panel2" runat="server" DefaultButton="lnkSearch">
                            <div id="search" class="searchcorner" onkeypress="abc();">
                                <br />
                                <table style="border-collapse: collapse; margin-left: 1em;" cellpadding="5">
                                    <tr>
                                        <td>
                                            <strong>Table</strong>
                                            <br />
                                            <asp:DropDownList ID="ddlTable" runat="server" AutoPostBack="False" DataTextField="TableName"
                                                              DataValueField="TableID" CssClass="NormalTextBox">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <strong>Graph</strong>
                                            <br />
                                            <asp:DropDownList ID="ddlGraph" runat="server" AutoPostBack="false" DataTextField="Heading"
                                                              DataValueField="GraphOptionID" CssClass="NormalTextBox">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <strong>Period</strong>
                                            <br />
                                            <asp:DropDownList ID="ddlPeriod" runat="server" AutoPostBack="False" CssClass="NormalTextBox">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <strong>Search</strong>
                                            <br />
                                            <asp:TextBox runat="server" ID="txtReportText" CssClass="NormalTextBox"></asp:TextBox>
                                        </td>
                                        <td style="text-align: left; vertical-align: bottom;">
                                            <div>
                                                <asp:LinkButton runat="server" ID="lnkSearch" CssClass="btn" ValidationGroup="MKE" CausesValidation="False" OnClick="lnkSearch_OnClick"
                                                ><strong>Go</strong></asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </div>
                        </asp:Panel>
                        <dbg:dbgGridView ID="gvTheGrid" runat="server" GridLines="Both" CssClass="gridview"
                                         HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" AllowPaging="True"
                                         AllowSorting="True" DataKeyNames="ReportID" HeaderStyle-ForeColor="Black" Width="100%"
                                         AutoGenerateColumns="false" PageSize="1000" OnSorting="gvTheGrid_Sorting" OnRowDataBound="gvTheGrid_RowDataBound"
                                         OnPreRender="gvTheGrid_PreRender" AlternatingRowStyle-BackColor="#DCF2F0">
                            <PagerSettings Position="Top" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkDelete" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false">
                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="LblID" runat="server" Text='<%# Eval("ReportID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="EditHyperLink" runat="server" ToolTip="Edit" NavigateUrl='<%# GetEditURL(Eval("ReportID").ToString()) %>'
                                            ImageUrl="~/App_Themes/Default/Images/iconEdit.png" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField SortExpression="ReportName" HeaderText="Name">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lbOpen" runat="server" ToolTip="Run Report" 
                                                       Text='<%# Eval("ReportName") %>'/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField SortExpression="ReportDescription" HeaderText="Description" Visible="False">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblReportDescription" runat="server" Text='<%# Eval("ReportDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField SortExpression="ReportDates" HeaderText="Period">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblReportPeriod" runat="server" Text='<%# GetPeriod(Eval("ReportDates").ToString(), Eval("DaysFrom").ToString()) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Run&nbsp;Report">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="RunHyperLink" runat="server" ToolTip="Run Report"
                                                       ImageUrl="~/App_Themes/Default/Images/RunReport16.png" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="gridview_header" />
                            <RowStyle CssClass="gridview_row" />
                            <PagerTemplate>
                                <asp:GridViewPager runat="server" ID="Pager"
                                                   HideAdd="false" OnDeleteAction="Pager_DeleteAction" OnApplyFilter="Pager_OnApplyFilter"
                                                   DelConfirmation="Are you sure you want to remove selected report(s)?"
                                                   HidePageSize="True" HidePagerGoButton="True" HideGo="True" HideNavigation="True"
                                                   HideRefresh="True" HideFilter="True" HideExport="True"
                                                   OnBindTheGridToExport="Pager_BindTheGridToExport" OnBindTheGridAgain="Pager_BindTheGridAgain" />
                            </PagerTemplate>
                        </dbg:dbgGridView>
                        <br />
                        <div runat="server" id="divEmptyData" visible="false" style="padding-left: 100px;">                            
                            <asp:HyperLink runat="server" ID="hpNew2" Style="text-decoration: none; color: Black;" ToolTip="New Report" />                               
                            &nbsp;No records have been added yet.&nbsp;
                            <asp:HyperLink runat="server" ID="hpNew" Style="text-decoration: none; color: Black;">
                                <strong style="text-decoration: underline; color: Blue;">
                                    Create report now
                                </strong>
                            </asp:HyperLink>
                        </div>
                        
                        <div runat="server" id="divNoFilter" visible="false" style="padding-left: 100px;">
                            No records match that filter.
                            <br />
                            <asp:LinkButton runat="server" ID="lnkNoFilter" Style="text-decoration: none; color: Black;"
                                            OnClick="Pager_OnApplyFilter">
                                <strong style="text-decoration: underline; color: Blue;">
                                    Clear filter
                                </strong>
                                <asp:Image runat="server" ID="Image2" ImageUrl="~/App_Themes/Default/images/BigFilter.png"  />
                            </asp:LinkButton>
                            &nbsp;or&nbsp;
                            <asp:HyperLink runat="server" ID="hplNewDataFilter" Style="text-decoration: none; color: Black;">
                                <strong style="text-decoration: underline; color: Blue;">
                                    create report now
                                </strong>
                            </asp:HyperLink>
                            <asp:HyperLink runat="server" ID="hplNewDataFilter2" Style="text-decoration: none; color: Black;"
                                ToolTip="New Report" />
                        </div>
                        <br />
                        <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                        <a id="runReportDate_" class="popupRunReportDate" style="visibility: hidden; display: none;">Open Run Report Dates</a>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="gvTheGrid" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

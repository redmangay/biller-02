﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="ReportDetail.aspx.cs" Inherits="Pages_ReportWriter_ReportDetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <asp:Literal ID="ltTextStyles" runat="server"></asp:Literal>
    <asp:Literal ID="ltCommonStyles" runat="server"></asp:Literal>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <script type="text/javascript">
        $(document).ready(function() {
            InitElements();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            InitElements();
        });

        function InitElements() {
        }

        function validate() {
            var isValid = Page_ClientValidate("MKE"); //parameter is the validation group
            if (!isValid)
                Page_BlockSubmit = false;
            return isValid;
        }

        function Close() {
            parent.$.fancybox.close();
        }

        function SavedAndRefresh() {
            var x = $("[id*='lbUpdateDetails']", parent.document); // jQuery click() method will not cause postback on parent page!
            if (x)
                parent.document.getElementById(x.attr("id")).click();
            parent.$.fancybox.close();
        }
    </script>

    <table border="0" cellpadding="0" cellspacing="0" >
        <tr>
            <td height="40" style="width: 50%; text-align: left;">
                <span class="TopTitle">
                    <asp:Label runat="server" ID="lblTitle"></asp:Label>
                </span>
            </td>
            <td>
                <div style="width: 100%;">
                    <%-- RP Modified Ticket 4627 Issue 2 --%>
                    <%-- 
                     OLD   
                    <div style="float: right; width: 36px;" runat="server" id="divSave">
                    --%>
                    <div style="float: right; width: 55px;" runat="server" id="divSave">
                        <asp:LinkButton runat="server" ID="lnkSave" OnClick="lnkSave_Click" CausesValidation="true"
                                        OnClientClick="return validate();" >
                            <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                       ToolTip="Save" />
                        </asp:LinkButton>
                    </div>
                    <%-- RP Modified Ticket 4627 Issue 2 --%>
                    <%-- 
                     OLD   
                    <div style="float: right; width: 36px;" runat="server" id="divEdit" visible="false">
                    --%>
                    <div style="float: right; width: 55px;" runat="server" id="divEdit" visible="false">
                        <asp:HyperLink runat="server" ID="hlEditLink">
                            <asp:Image runat="server" ID="Image2"  ImageUrl="~/App_Themes/Default/images/Edit_big.png"
                                        ToolTip="Edit" />
                        </asp:HyperLink>
                    </div>
                    <%-- RP Modified Ticket 4627 Issue 2 --%>
                    <%-- 
                     OLD   
                    <div style="float: right; width: 36px;" runat="server" id="divBack">
                     --%>
                    <div style="float: right; width: 55px;" runat="server" id="divBack">
                    <%-- END MODIFICATION --%>
                        <asp:LinkButton runat="server" ID="lbBack" OnClick="lbBack_OnClick" CausesValidation="False">
                            <asp:Image runat="server" ID="imgBack" ImageUrl="~/App_Themes/Default/images/Back.png"
                                       ToolTip="Back" />
                        </asp:LinkButton>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" height="13">
                <asp:Label runat="server" ID="lblSubtitle">To create a new report we need to gather a few details first</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" height="13">
            </td>
        </tr>
        <tr>
            <td valign="top" align="left" colspan="2">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div id="search" style="padding-bottom: 10px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" ValidationGroup="MKE"
                                ShowMessageBox="true" ShowSummary="false" HeaderText="Please correct the following errors:" />
                        </div>
                        <asp:Panel ID="Panel2" runat="server" DefaultButton="lnkSave">
                            <div runat="server" id="divDetail">
                                <table>
                                    <tr>
                                        <td valign="top" style="padding-top:7px;" align="left" >
                                            <table cellpadding="3">
                                                <tr runat="server" id="trReportName">
                                                    <td style="text-align: right; width: 10em;">
                                                        <strong>Report Name</strong>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtReportName" CssClass="NormalTextBox" ClientIDMode="Static"
                                                            Width="450px"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="rfvReportName" runat="server" ControlToValidate="txtReportName"
                                                            CssClass="failureNotification" ErrorMessage="Report name is required." ToolTip="Report name is required."
                                                            ValidationGroup="MKE">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="tr1">
                                                    <td style="text-align: right; width: 10em; vertical-align: top;">
                                                        <strong runat="server" id="stgDescription">Description</strong>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" ID="txtReportDescription" TextMode="MultiLine" CssClass="MultiLineTextBox"
                                                            ClientIDMode="Static" Width="450px" Height="60px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList runat="server" ID="optReportDates"
                                                            RepeatDirection="Horizontal" AutoPostBack="true"
                                                            OnSelectedIndexChanged="optReportDates_SelectedIndexChanged">
                                                            <asp:ListItem Text="Period" Value="period" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="Date Range" Value="dateRange"></asp:ListItem>
                                                            <asp:ListItem Text="Enter when running report" Value="dateOnRun"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trReportPeriod" Visible="True">
                                                    <td style="text-align: right; width: 10em;">
                                                        <strong>Report Period</strong>
                                                    </td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddlPeriod" runat="server" AutoPostBack="true" Width="155px" CssClass="NormalTextBox" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trDays" Visible="False">
                                                    <td style="text-align: right; width: 10em;">
                                                        <strong>Days</strong>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtDays" runat="server" />
                                                        <asp:RegularExpressionValidator id="revDays"
                                                                           ControlToValidate="txtDays"
                                                                           ValidationExpression="\d+"
                                                                           Display="Dynamic"
                                                                           EnableClientScript="true"
                                                                           ErrorMessage="Please enter numbers only"
                                                                           runat="server"/>
                                                        <asp:RequiredFieldValidator ID="rfvDays"
                                                                           ControlToValidate="txtDays"
                                                                           Display="Dynamic"
                                                                           ErrorMessage="Days is required"
                                                                           runat="server"/>                                                                            
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trReportPeriodEnd" Visible="True">
                                                    <td style="text-align: right; width: 10em;">
                                                        <strong>Before</strong>
                                                    </td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddlPeriodEnd" runat="server" AutoPostBack="false" Width="155px" CssClass="NormalTextBox">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trDateStart" Visible="False">
                                                    <td align="right">
                                                        <strong>From</strong>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtStartDate" runat="server" CssClass="NormalTextBox" 
                                                                     Width="100px"  BorderStyle="Solid"
                                                                     BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                                                        <asp:ImageButton runat="server" ID="ibStartDate" ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false"/>  

                                                        <ajaxToolkit:CalendarExtender ID="ce_txtDateFrom" runat="server" TargetControlID="txtStartDate"
                                                                                      Format="dd/MM/yyyy" PopupButtonID="ibStartDate" FirstDayOfWeek="Monday">
                                                        </ajaxToolkit:CalendarExtender>
                                                           
                                                        <asp:RangeValidator ID="rngDateFrom" runat="server" ControlToValidate="txtStartDate"
                                                                            ValidationGroup="MKE" ErrorMessage="Invalid Start date" Font-Bold="true" Display="Dynamic" Type="Date"
                                                                            MinimumValue="1/1/1753" MaximumValue="1/1/3000">*</asp:RangeValidator> 
                                                        <ajaxToolkit:TextBoxWatermarkExtender ID="tbwStartDate" TargetControlID="txtStartDate" WatermarkText="dd/mm/yyyy"
                                                                                              runat="server" WatermarkCssClass="MaskText"></ajaxToolkit:TextBoxWatermarkExtender>

                                                        <asp:RequiredFieldValidator ID="rfvDateFrom" runat="server" Enabled="False" ControlToValidate="txtStartDate"
                                                                                    CssClass="failureNotification" ErrorMessage="From Date is required." ToolTip="From Date is required."
                                                                                    ValidationGroup="MKE">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trDateEnd" Visible="False">
                                                    <td align="right">
                                                        <strong>To</strong>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtEndDate" runat="server" CssClass="NormalTextBox" 
                                                                     Width="100px"  BorderStyle="Solid"
                                                                     BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                                                        <asp:ImageButton runat="server" ID="ibEndDate" ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false"/>  
                                                        <ajaxToolkit:CalendarExtender ID="ce_txtDateTo" runat="server" TargetControlID="txtEndDate"
                                                                                      Format="dd/MM/yyyy" PopupButtonID="ibEndDate" FirstDayOfWeek="Monday">
                                                        </ajaxToolkit:CalendarExtender>
                                                           
                                                        <asp:RangeValidator ID="rngDateTo" runat="server" ControlToValidate="txtEndDate"
                                                                            ValidationGroup="MKE" ErrorMessage="Invalid End date" Font-Bold="true" Display="Dynamic" Type="Date"
                                                                            MinimumValue="1/1/1753" MaximumValue="1/1/3000">*</asp:RangeValidator>
                                                        <ajaxToolkit:TextBoxWatermarkExtender ID="tbwEndDate" TargetControlID="txtEndDate" WatermarkText="dd/mm/yyyy"
                                                                                              runat="server" WatermarkCssClass="MaskText"></ajaxToolkit:TextBoxWatermarkExtender>
                                                           
                                                        <asp:RequiredFieldValidator ID="rfvDateTo" runat="server" Enabled="False" ControlToValidate="txtEndDate"
                                                                                    CssClass="failureNotification" ErrorMessage="To Date is required." ToolTip="To Date is required."
                                                                                    ValidationGroup="MKE">*</asp:RequiredFieldValidator>

                                                        <asp:CompareValidator ID="EndDateCompare" runat="server" Type="Date" ControlToValidate="txtEndDate"
                                                                              ControlToCompare="txtStartDate" Operator="GreaterThanEqual"  ToolTip="End date must be after the start date"
                                                                              ErrorMessage="End date must be after the start date" CssClass="failureNotification"
                                                                              ValidationGroup="MKE">*</asp:CompareValidator>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trWarning" Visible="False">
                                                    <td style="text-align: right; width: 10em;">
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblWarning" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 5px;">
                                        </td>
                                    </tr>
                                </table>
                               
                            </div>
                            <br />
                            <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                            <br />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>

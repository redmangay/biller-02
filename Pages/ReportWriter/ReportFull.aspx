﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true" CodeFile="ReportFull.aspx.cs" Inherits="Pages_ReportWriter_ReportFull" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>
<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <style type="text/css">
        .sortHandleVT {
            cursor: move;
        }

        .cssplaceholder {
            border-top: 2px solid #00FFFF;
            border-bottom: 2px solid #00FFFF;
        }

        .Schedulelegend {
            display: block;
            width: 70px;
            padding: 4px;
            margin-bottom: 10px;
            font-size: 12px;
            font-weight: bold;
            line-height: inherit;
            color: #333333;
            border: 0;
            border-bottom: 0px solid #e5e5e5;
        }

        .runnowlegend {
            display: block;
            width: 103px;
            padding: 4px;
            margin-bottom: 10px;
            font-size: 12px;
            font-weight: bold;
            line-height: inherit;
            color: #333333;
            border: 0;
            border-bottom: 0px solid #e5e5e5;
        }

        fieldset {
            display: block;
            margin-left: 2px;
            margin-right: 2px;
            padding-top: 0.35em;
            padding-bottom: 0.625em;
            padding-left: 0.75em;
            padding-right: 0.75em;
            border: groove;
            border-width: 2px;
            border-color: #DADADA;
        }
    </style>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {

            $(function () {
                $('.popupRunReportDate').fancybox({
                    iframe: {
                        css: {
                            width: '480px',
                            height: '350px'
                        }
                    },
                    toolbar: false,
                    smallBtn: true,
                    scrolling: 'auto',
                    type: 'iframe',
                    titleShow: false,
                    beforeClose: function () {
                        $(".ajax-indicator-full").hide();
                    },
                    beforeShow: function () {
                        $(".ajax-indicator-full").hide();
                        //alert('Hello World');
                    }

                });

            });
        });

        function runReportDate() {
            var openReportDate = document.getElementById('hlOpenReportHidden');
            var reportId = $('#hfReportId').val();
            openReportDate.setAttribute('href', '/Pages/ReportWriter/ReportDates.aspx?ReportID=' + reportId);
            openReportDate.click();
            $('.ajax-indicator-full').hide();

        }

        $(document).ready(function () {
            initElements();
            clickOnLoad();

            $("#ctl00_HomeContentPlaceHolder_lnkRunReport")
                .click(
                    function () {
                       
                        $(".ajax-indicator-full").fadeOut();
                       
                    }
                );
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initElements();
        });

        function initElements() {
            $('#divReportItemSingleInstance').sortable({
                items: '.gridview_row',
                cursor: 'crosshair',
                helper: fixHelper,
                cursorAt: { left: 10, top: 10 },
                connectWith: '#divReportItemSingleInstance',
                handle: '.sortHandleVT',
                axis: 'y',
                distance: 15,
                dropOnEmpty: true,
                receive: function (e, ui) {
                    $(this).find('tbody').append(ui.item);
                },
                start: function (e, ui) {
                    ui.placeholder.css('border-top', '2px solid #00FFFF');
                    ui.placeholder.css('border-bottom', '2px solid #00FFFF');
                },
                update: function (event, ui) {
                    var TC = '';
                    $('.ReportItemID').each(function () {
                        TC = TC + this.value.toString() + ',';
                    });
                    //alert(TC);
                    document.getElementById('hfReportItemIDForItemPosition').value = TC;
                    $('#btnReportItemIDForItemPosition').trigger('click');
                }
            });

            $('.popuplinkRI').fancybox({
                iframe: {
                    css: {
                        width: '420px',
                        height: '220px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                titleShow: false,
                afterShow: function () {
                    $(".ajax-indicator-full").hide();
                },
                afterClose: function () {
                    $(".ajax-indicator-full").hide();
                }
            });

            $('.popuplinkDetails').fancybox({
                iframe: {
                    css: {
                        width: '680px',
                        height: '320px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                titleShow: false,
                afterClose: function () {
                    $(".ajax-indicator-full").hide();
                }
            });

            $('.popuplinkCopy').fancybox({
                iframe: {
                    css: {
                        width: '420px',
                        height: '140px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                titleShow: false,
                afterClose: function () {
                    $(".ajax-indicator-full").hide();
                }
            });

            $('.popuplinkRD').fancybox({
                iframe: {
                    css: {
                        width: '400px',
                        height: '200px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                titleShow: false,
                afterClose: function () {
                    $(".ajax-indicator-full").hide();
                }
            });

            $('.popuplink10').fancybox({
                toolbar: false,
                smallBtn: true,
                'titleShow': false,
                afterClose: function () {
                    $(".ajax-indicator-full").hide();
                }
            });

            $('.popuplinkRR').fancybox({
                iframe: {
                    css: {
                        width: '520px',
                        height: '250px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                width: 520,
                height: 250,
                titleShow: false,
                afterClose: function () {
                    $(".ajax-indicator-full").hide();
                }
            });




            //setReportButtonVisibility();
        }

        function setReportButtonVisibility() {
            if ($("#hfAskReportDate").val() === "1") {
                $("#divReportDate").css("visibility", "visible").css("display", "block");
                $("#divReport").css("visibility", "hidden").css("display", "none");
            } else {
                $("#divReport").css("visibility", "visible").css("display", "block");
                $("#divReportDate").css("visibility", "hidden").css("display", "none");
            }
        }

        function clickOnLoad() {
            var x = $("#txtClickOnLoad").val();
            if (x !== "")
                $("#" + x).trigger("click");
        }

        var fixHelper = function (e, ui) {
            ui.children().each(function () {
                $(this).width($(this).width());
            });

            return ui;
        };

        function checkAll(objRef) {
            var gridView = objRef.parentNode.parentNode.parentNode;
            var inputList = gridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type === "checkbox" && objRef !== inputList[i]) {
                    if (objRef.checked) {
                        //If the header checkbox is checked
                        //check all checkboxes                       
                        inputList[i].checked = true;
                    }
                    else {
                        //If the header checkbox is checked
                        //uncheck all checkboxes                        
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function SelectAllCheckboxes(spanChk) {
            checkAll(spanChk);
            // Added as ASPX uses SPAN for checkbox
            var oItem = spanChk.children;
            var theBox = (spanChk.type === "checkbox") ? spanChk : spanChk.children.item[0];
            var xState = theBox.checked;
            var elm = theBox.form.elements;
            for (var i = 0; i < elm.length; i++) {
                if (elm[i].type === "checkbox" && elm[i].id !== theBox.id) {
                    if (elm[i].checked !== xState)
                        elm[i].click();
                }
            }
        }
        function isNumeric(value) {
            return /^\d+$/.test(value);
        }

        function validateTime(source, arg) {
            if (arg.Value.indexOf(":") === -1) {
                arg.IsValid = false;
            } else {
                var parts = arg.Value.split(":");
                if (parts.length !== 2) {
                    arg.IsValid = false;
                } else {
                    if (parts[0].length === 0 || !isNumeric(parts[0]) ||
                        parts[1].length === 0 || !isNumeric(parts[1])) {
                        arg.IsValid = false;
                    } else {
                        var h = 0;
                        var m = 0;
                        h = parseInt(parts[0]);
                        m = parseInt(parts[1]);
                        if (h < 0 || h > 23 || m < 0 || m > 59) {
                            arg.IsValid = false;
                        } else {
                            arg.IsValid = true;
                        }
                    }
                }
            }
        }

        function validate() {
            var isValid = Page_ClientValidate("MKE"); //parameter is the validation group
            if (!isValid)
                Page_BlockSubmit = false;
            return isValid;
        }

        function Close() {
            parent.$.fancybox.close();
        }

        function SavedAndRefresh() {
            var x = $("[id*='lbUpdateDetails']", parent.document); // jQuery click() method will not cause postback on parent page!
            if (x)
                parent.document.getElementById(x.attr("id")).click();
            parent.$.fancybox.close();
        }


    </script>

    <div style="padding-left: 20px; padding-top: 10px;">



        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td colspan="3">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td height="40" style="width: 50%; text-align: left;">
                                <span class="TopTitle">
                                    <asp:Label runat="server" ID="lblTitle"></asp:Label>
                                </span>
                            </td>
                            <td></td>
                        </tr>
                        <%--<tr>
                        <td colspan="2" height="13">
                            <asp:Label runat="server" ID="lblSubtitle">To create a new report we need to gather a few details first</asp:Label>
                        </td>
                    </tr>--%>
                    </table>
                </td>
                <td>
                    <div style="width: 100%;">
                        <%-- RP Modified Ticket 4627 Issue 2 --%>
                        <%-- 
                     OLD   
                    <div style="float: right; width: 36px;" runat="server" id="divSave">
                        --%>
                        <div style="float: right; width: 55px;" runat="server" id="div1">
                            <asp:LinkButton runat="server" ID="lnkSaveDetail" OnClick="lnkSaveDetail_Click" CausesValidation="true"
                                OnClientClick="return validate();">
                                <asp:Image runat="server" ID="Image3" ImageUrl="~/App_Themes/Default/images/Save.png"
                                    ToolTip="Save" />
                            </asp:LinkButton>
                        </div>
                        <%-- RP Modified Ticket 4627 Issue 2 --%>
                        <%-- 
                     OLD   
                    <div style="float: right; width: 36px;" runat="server" id="divEdit" visible="false">
                        --%>
                        <div style="float: right; width: 55px;" runat="server" id="div2" visible="false">
                            <asp:HyperLink runat="server" ID="HyperLink1">
                                <asp:Image runat="server" ID="Image4" ImageUrl="~/App_Themes/Default/images/Edit_big.png"
                                    ToolTip="Edit" />
                            </asp:HyperLink>
                        </div>
                        <%-- RP Modified Ticket 4627 Issue 2 --%>
                        <%-- 
                     OLD   
                    <div style="float: right; width: 36px;" runat="server" id="divBack">
                        --%>
                        <div style="float: right; width: 55px;" runat="server" id="div3">
                            <%-- END MODIFICATION --%>
                            <asp:LinkButton runat="server" ID="lbBackDetail" OnClick="lbBackDetail_OnClick" CausesValidation="False">
                                <asp:Image runat="server" ID="Image5" ImageUrl="~/App_Themes/Default/images/Back.png"
                                    ToolTip="Back" />
                            </asp:LinkButton>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <br />
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="4">
                    <div id="search" style="padding-bottom: 10px">
                        <asp:ValidationSummary ID="ValidationSummary" runat="server" EnableClientScript="true" ValidationGroup="MKE"
                            ShowMessageBox="true" ShowSummary="false" HeaderText="Please correct the following errors:" />
                    </div>
                    <table cellpadding="3">
                        <tr runat="server" id="trReportName">
                            <td style="text-align: right; width: 10em;">
                                <strong>Report Name</strong>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtReportName" CssClass="NormalTextBox" ClientIDMode="Static"
                                    Width="450px"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="rfvReportName" runat="server" ControlToValidate="txtReportName"
                                    CssClass="failureNotification" ErrorMessage="Report name is required." ToolTip="Report name is required."
                                    ValidationGroup="MKE">*</asp:RequiredFieldValidator>
                            </td>
                            <td></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td>
                                <asp:RadioButtonList runat="server" ID="optReportDates"
                                    RepeatDirection="Horizontal" AutoPostBack="true"
                                    OnSelectedIndexChanged="optReportDates_SelectedIndexChanged">
                                    <asp:ListItem Text="Adhoc" Value="dateOnRun" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Scheduled" Value="schedule"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top;padding-left:30px;width:400px;">
                    <table border="0" cellpadding="0" cellspacing="0">

                        <tr>
                            <td valign="top" align="left" colspan="2">
                                <%--  <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>--%>

                                <asp:Panel ID="panelReportDates" runat="server">
                                    <div runat="server" id="div4" >
                                        <table>
                                            <tr>
                                                <td valign="top" style="padding-top: 7px;" align="left">
                                                    <fieldset>
                                                        <legend class="runnowlegend">Report Dates</legend>
                                                        <table cellpadding="3">


                                                            <tr runat="server" id="trReportPeriod" visible="True">
                                                                <td style="text-align: right; width: 10em;">
                                                                    <strong>From</strong>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:DropDownList ID="ddlPeriodDetail" runat="server" AutoPostBack="true" Width="230px"
                                                                        CssClass="NormalTextBox" OnSelectedIndexChanged="ddlPeriodDetail_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                 <td runat="server" id="tdDaysFrom" visible="false" align="left">
                                                                    <asp:TextBox ID="txtDaysFrom" Width="40px" placeholder="Days" runat="server" />
                                                                    <asp:RegularExpressionValidator ID="revDays"
                                                                        ControlToValidate="txtDaysFrom"
                                                                        ValidationExpression="\d+"
                                                                        Display="Dynamic"
                                                                        EnableClientScript="true"
                                                                        ErrorMessage="Please enter numbers only"
                                                                        runat="server" />
                                                                    <asp:RequiredFieldValidator ID="rfvDays"
                                                                        ControlToValidate="txtDaysFrom"
                                                                        Display="Dynamic"
                                                                        ErrorMessage="Days is required"
                                                                        runat="server" />
                                                                </td>
                                                            </tr>
                                                            <%--<tr runat="server" id="trDays" visible="False">
                                                                <td style="text-align: right; width: 10em;">
                                                                    <strong>Days</strong>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="txtDays" runat="server" />
                                                                    <asp:RegularExpressionValidator ID="revDays"
                                                                        ControlToValidate="txtDays"
                                                                        ValidationExpression="\d+"
                                                                        Display="Dynamic"
                                                                        EnableClientScript="true"
                                                                        ErrorMessage="Please enter numbers only"
                                                                        runat="server" />
                                                                    <asp:RequiredFieldValidator ID="rfvDays"
                                                                        ControlToValidate="txtDays"
                                                                        Display="Dynamic"
                                                                        ErrorMessage="Days is required"
                                                                        runat="server" />
                                                                </td>
                                                                <td></td>
                                                            </tr>--%>
                                                            <tr runat="server" id="trReportPeriodEnd" visible="True">
                                                                <td style="text-align: right; width: 10em;">
                                                                    <strong>To</strong>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:DropDownList ID="ddlPeriodEnd" runat="server" AutoPostBack="true" Width="230px" OnSelectedIndexChanged="ddlPeriodDetail_SelectedIndexChanged"
                                                                        CssClass="NormalTextBox">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td runat="server" id="tdDaysTo" visible="false" align="left">
                                                                    <asp:TextBox ID="txtDaysTo" Width="40px" placeholder="Days" runat="server" />
                                                                    <asp:RegularExpressionValidator ID="revDaysTo"
                                                                        ControlToValidate="txtDaysTo"
                                                                        ValidationExpression="\d+"
                                                                        Display="Dynamic"
                                                                        EnableClientScript="true"
                                                                        ErrorMessage="Please enter numbers only"
                                                                        runat="server" />
                                                                    <asp:RequiredFieldValidator ID="rfvDaysTo"
                                                                        ControlToValidate="txtDaysTo"
                                                                        Display="Dynamic"
                                                                        ErrorMessage="Days is required"
                                                                        runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="trDateStart">
                                                                <td align="right">
                                                                    <strong>From*</strong>
                                                                </td>
                                                                <td align="left">
                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <%--== Red 06092019: Ticket 4818, disabled validator ==--%>
                                                                                <asp:TextBox ID="txtStartDate" runat="server" CssClass="NormalTextBox"
                                                                                    Width="100px" BorderStyle="Solid" placeholder="dd/mm/yyyy"
                                                                                    BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                                                                                <asp:ImageButton runat="server" ID="ibStartDate" ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false" />

                                                                                <ajaxtoolkit:calendarextender id="ce_txtDateFrom" runat="server" targetcontrolid="txtStartDate"
                                                                                    format="dd/MM/yyyy" popupbuttonid="ibStartDate" firstdayofweek="Monday">
                                                                                </ajaxtoolkit:calendarextender>

                                                                               <%-- <asp:RangeValidator ID="rngDateFrom" runat="server" ControlToValidate="txtStartDate"
                                                                                    ValidationGroup="MKE" ErrorMessage="Invalid Start date" Font-Bold="true" Display="Dynamic" Type="Date"
                                                                                    MinimumValue="1/1/1753" MaximumValue="1/1/3000">*</asp:RangeValidator>--%>

                                                                                <%--<ajaxToolkit:TextBoxWatermarkExtender ID="tbwStartDate" TargetControlID="txtStartDate" WatermarkText="dd/mm/yyyy"
                                                                                    runat="server" WatermarkCssClass="MaskText">
                                                                                </ajaxToolkit:TextBoxWatermarkExtender>--%>

<%--                                                                                <asp:RequiredFieldValidator ID="rfvDateFrom" runat="server" Enabled="False" ControlToValidate="txtStartDate"
                                                                                    CssClass="failureNotification" ErrorMessage="Start Date is required." ToolTip="Start Date is required."
                                                                                    ValidationGroup="MKE">*</asp:RequiredFieldValidator>--%>
                                                                            </td>
                                                                            <td>
                                                                                   <%--== Red 06092019: Ticket 4818, disabled validator ==--%>
                                                                                <asp:Label Font-Bold="True" runat="server" ID="Label1">Time</asp:Label>
                                                                                <asp:TextBox ID="txtStartTime" runat="server" CssClass="NormalTextBox"
                                                                                    Width="60px" BorderStyle="Solid" placeholder="hh:mm"
                                                                                    BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                                                                              <%--  <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtStartTime"
                                                                                    ValidationGroup="MKE" ErrorMessage="Invalid time" Font-Bold="true" Display="Dynamic"
                                                                                    ClientValidationFunction="validateTime">*</asp:CustomValidator>--%>
                                                                                <ajaxtoolkit:maskededitextender id="meeStartTime" runat="server" targetcontrolid="txtStartTime"
                                                                                    autocompletevalue="00:00" mask="99:99" masktype="Time">
                                                                                </ajaxtoolkit:maskededitextender>
                                                                                <%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" TargetControlID="txtStartTime" WatermarkText="hh:mm"
                                                                                    runat="server" WatermarkCssClass="MaskText">
                                                                                </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr runat="server" id="trDateEnd">
                                                                <td align="right">
                                                                    <strong>To*</strong>
                                                                </td>
                                                                <td align="left">
                                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                   <%--== Red 06092019: Ticket 4818, disabled validator ==--%>
                                                                                <asp:TextBox ID="txtEndDate" runat="server" CssClass="NormalTextBox"
                                                                                    Width="100px" BorderStyle="Solid" placeholder="dd/mm/yyyy"
                                                                                    BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                                                                                <asp:ImageButton runat="server" ID="ibEndDate" ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false" />
                                                                                <ajaxtoolkit:calendarextender id="ce_txtDateTo" runat="server" targetcontrolid="txtEndDate"
                                                                                    format="dd/MM/yyyy" popupbuttonid="ibEndDate" firstdayofweek="Monday">
                                                                                </ajaxtoolkit:calendarextender>

                                                                              <%--  <asp:RangeValidator ID="rngDateTo" runat="server" ControlToValidate="txtEndDate"
                                                                                    ValidationGroup="MKE" ErrorMessage="Invalid End date" Font-Bold="true" Display="Dynamic" Type="Date"
                                                                                    MinimumValue="1/1/1753" MaximumValue="1/1/3000">*</asp:RangeValidator>--%>
                                                                                <%--<ajaxToolkit:TextBoxWatermarkExtender ID="tbwEndDate" TargetControlID="txtEndDate" WatermarkText="dd/mm/yyyy"
                                                                                    runat="server" WatermarkCssClass="MaskText">
                                                                                </ajaxToolkit:TextBoxWatermarkExtender>--%>

                                                                             <%--   <asp:RequiredFieldValidator ID="rfvDateTo" runat="server" Enabled="False" ControlToValidate="txtEndDate"
                                                                                    CssClass="failureNotification" ErrorMessage="To Date is required." ToolTip="To Date is required."
                                                                                    ValidationGroup="MKE">*</asp:RequiredFieldValidator>--%>


                                                                            </td>
                                                                            <td>
                                                                                   <%--== Red 06092019: Ticket 4818, disabled validator ==--%>
                                                                                <asp:Label Font-Bold="True" runat="server" ID="Label3">Time</asp:Label>
                                                                                <asp:TextBox ID="txtEndTime" runat="server" CssClass="NormalTextBox"
                                                                                    Width="60px" BorderStyle="Solid" placeholder="hh:mm"
                                                                                    BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                                                                             <%--   <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtEndTime"
                                                                                    ValidationGroup="MKE" ErrorMessage="Invalid time" Font-Bold="true" Display="Dynamic"
                                                                                    ClientValidationFunction="validateTime">*</asp:CustomValidator>--%>
                                                                                <ajaxtoolkit:maskededitextender id="meeEndTime" runat="server" targetcontrolid="txtEndTime"
                                                                                    autocompletevalue="00:00" mask="99:99" masktype="Time">
                                                                                </ajaxtoolkit:maskededitextender>
                                                                                <%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" TargetControlID="txtEndTime" WatermarkText="hh:mm"
                                                                                    runat="server" WatermarkCssClass="MaskText">
                                                                                </ajaxToolkit:TextBoxWatermarkExtender>--%>

                                                                               <%-- <asp:CompareValidator ID="EndDateCompare" runat="server" Type="Date" ControlToValidate="txtEndDate"
                                                                                    ControlToCompare="txtStartDate" Operator="GreaterThanEqual" ToolTip="End date must be after the start date"
                                                                                    ErrorMessage="End date must be after the start date" CssClass="failureNotification"
                                                                                    ValidationGroup="MKE">*</asp:CompareValidator>--%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr runat="server" id="trWarning" visible="False">
                                                                <td style="text-align: right; width: 10em;">&nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:Label runat="server" ID="lblWarning" />
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>

                                                </td>
                                                <td style="width: 5px;"></td>
                                            </tr>
                                        </table>

                                    </div>
                                    <br />
                                    <asp:Label runat="server" ID="Label2" ForeColor="Red"></asp:Label>
                                    <br />
                                </asp:Panel>
                                <%--    </ContentTemplate>
                            </asp:UpdatePanel>--%>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top;padding-left:20px;width:450px;" >
                    <table runat="server" id="tblSchedule" border="0" cellpadding="0" cellspacing="0">

                        <tr>
                            <td valign="top" align="left" colspan="2">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:Panel ID="Panel2" runat="server">
                                            <div runat="server" id="divDetail" >
                                                <table>
                                                    <tr>
                                                        <td valign="top" style="padding-top: 7px;" align="left">

                                                            <fieldset>
                                                                <legend class="Schedulelegend">Schedule</legend>
                                                                <table cellpadding="3">

                                                                    <tr runat="server" id="trRunDate" visible="True">
                                                                        <td style="text-align: right; width: 10em;">
                                                                            <strong>Run Date*</strong>
                                                                        </td>
                                                                        <td align="left">
                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="txtDate" runat="server" CssClass="NormalTextBox" AutoPostBack="true" OnTextChanged="txtDate_TextChanged"
                                                                                            Width="100px" BorderStyle="Solid" placeholder="dd/mm/yyyy"
                                                                                            BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                                                                                        <asp:ImageButton runat="server" ID="ibRunDate" ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false" />

                                                                                        <ajaxtoolkit:calendarextender id="CalendarExtender2" runat="server" targetcontrolid="txtDate"
                                                                                            format="dd/MM/yyyy" popupbuttonid="ibRunDate" firstdayofweek="Monday">
                                                                                        </ajaxtoolkit:calendarextender>

                                                                                        <asp:RangeValidator ID="ravRunDate" runat="server" ControlToValidate="txtDate"
                                                                                            ValidationGroup="MKE" ErrorMessage="Invalid Run date" Font-Bold="true" Display="Dynamic" Type="Date"
                                                                                            MinimumValue="1/1/1753" MaximumValue="1/1/3000">*</asp:RangeValidator>

                                                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" TargetControlID="txtDate" WatermarkText="dd/mm/yyyy"
                                                                                    runat="server" WatermarkCssClass="MaskText">
                                                                                </ajaxToolkit:TextBoxWatermarkExtender>--%>

                                                                                        <asp:RequiredFieldValidator ID="rfvRunDate" runat="server" Enabled="False" ControlToValidate="txtDate"
                                                                                            CssClass="failureNotification" ErrorMessage="Run Date is required." ToolTip="Run Date is required."
                                                                                            ValidationGroup="MKE">*</asp:RequiredFieldValidator>
                                                                                        <%--<asp:TextBox ID="txtToday" runat="server" CssClass="NormalTextBox"
                                                                            Width="100px" ></asp:TextBox>--%>
                                                                                        <asp:CompareValidator ID="cvDate" runat="server" ErrorMessage="Date must be greater than or equal to the current date."
                                                                                            ToolTip="Date must be greater than or equal to the current date."
                                                                                            ControlToValidate="txtDate"
                                                                                            Operator="GreaterThanEqual" Type="Date" ValidationGroup="MKE">*</asp:CompareValidator>

                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label Font-Bold="True" runat="server" ID="Label4">Time</asp:Label>
                                                                                        <asp:TextBox ID="txtTime" runat="server" CssClass="NormalTextBox"
                                                                                            Width="60px" BorderStyle="Solid" placeholder="hh:mm"
                                                                                            BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                                                                                        <asp:CustomValidator ID="cvRunTime" runat="server" ControlToValidate="txtTime"
                                                                                            ValidationGroup="MKE" ErrorMessage="Invalid time" Font-Bold="true" Display="Dynamic"
                                                                                            ClientValidationFunction="validateTime">*</asp:CustomValidator>
                                                                                        <ajaxtoolkit:maskededitextender id="meeRunTime" runat="server" targetcontrolid="txtTime"
                                                                                            autocompletevalue="00:00" mask="99:99" masktype="Time">
                                                                                        </ajaxtoolkit:maskededitextender>
                                                                                        <%--<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" TargetControlID="txtTime" WatermarkText="hh:mm"
                                                                                    runat="server" WatermarkCssClass="MaskText">
                                                                                </ajaxToolkit:TextBoxWatermarkExtender>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td></td>
                                                                    </tr>

                                                                    <tr runat="server" id="trFrequency">
                                                                        <td style="text-align: right; width: 10em;">
                                                                            <strong>Repeating</strong>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="ddlFrequencySchedule" CssClass="NormalTextBox"
                                                                                AutoPostBack="True" OnSelectedIndexChanged="ddlFrequencySchedule_OnSelectedIndexChanged">
                                                                                <%--<asp:ListItem Value="OneOff" Text="No Repeating" />--%>
                                                                                <asp:ListItem Value="Weekly" Text="Daily/Weekly" />
                                                                                <asp:ListItem Value="Monthly" Text="Monthly" />
                                                                                <asp:ListItem Value="Undefined" Text="Not scheduled" />
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <%-- <tr runat="server" id="trDate">
                                                                <td style="text-align: right; width: 10em;">
                                                                    <asp:Label Font-Bold="True" runat="server" ID="lblDate">Date</asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtDate" runat="server" CssClass="NormalTextBox"
                                                                        Width="100px" BorderStyle="Solid"
                                                                        BorderColor="#909090" BorderWidth="1" AutoPostBack="True" OnTextChanged="OnScheduleChangedSchedule" />
                                                                    <asp:ImageButton runat="server" ID="ibDate" ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false" />

                                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDate"
                                                                        Format="dd/MM/yyyy" PopupButtonID="ibDate" FirstDayOfWeek="Monday">
                                                                    </ajaxToolkit:CalendarExtender>

                                                                    <asp:RangeValidator ID="RangeValidatorDate" runat="server" ControlToValidate="txtDate"
                                                                        ValidationGroup="MKE" ErrorMessage="Invalid date" Font-Bold="true" Display="Dynamic" Type="Date"
                                                                        MinimumValue="1/1/1753" MaximumValue="1/1/3000">*</asp:RangeValidator>
                                                                    <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                                                                        CssClass="failureNotification" ErrorMessage="Date is required." ToolTip="Date is required."
                                                                        ValidationGroup="MKE">*</asp:RequiredFieldValidator>
                                                                    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderDate" TargetControlID="txtDate" WatermarkText="dd/mm/yyyy"
                                                                        runat="server" WatermarkCssClass="MaskText">
                                                                    </ajaxToolkit:TextBoxWatermarkExtender>

                                                                    <asp:Label Font-Bold="True" runat="server" ID="lblTime">Time</asp:Label>
                                                                    <asp:TextBox ID="txtTime" runat="server" CssClass="NormalTextBox"
                                                                        Width="60px" BorderStyle="Solid"
                                                                        BorderColor="#909090" BorderWidth="1" AutoPostBack="True" OnTextChanged="OnScheduleChangedSchedule"></asp:TextBox>
                                                                    <asp:CustomValidator ID="CustomValidatorTime" runat="server" ControlToValidate="txtTime"
                                                                        ValidationGroup="MKE" ErrorMessage="Invalid time" Font-Bold="true" Display="Dynamic"
                                                                        ClientValidationFunction="validateTime">*</asp:CustomValidator>
                                                                    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderTime" TargetControlID="txtTime" WatermarkText="hh:mm"
                                                                        runat="server" WatermarkCssClass="MaskText">
                                                                    </ajaxToolkit:TextBoxWatermarkExtender>
                                                                </td>
                                                            </tr>--%>
                                                                    <tr runat="server" id="trRepeatEvery">
                                                                        <td style="text-align: right; width: 10em;">
                                                                            <strong runat="server" id="strRepeatEvery">Every</strong>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="ddlRepeatEvery" CssClass="NormalTextBox"
                                                                                AutoPostBack="True" OnSelectedIndexChanged="OnScheduleChangedSchedule">
                                                                                <asp:ListItem Value="1" Text="1" />
                                                                                <asp:ListItem Value="2" Text="2" />
                                                                                <asp:ListItem Value="3" Text="3" />
                                                                                <asp:ListItem Value="4" Text="4" />
                                                                                <asp:ListItem Value="5" Text="5" />
                                                                                <asp:ListItem Value="6" Text="6" />
                                                                                <asp:ListItem Value="7" Text="7" />
                                                                                <asp:ListItem Value="8" Text="8" />
                                                                                <asp:ListItem Value="9" Text="9" />
                                                                                <asp:ListItem Value="10" Text="10" />
                                                                                <asp:ListItem Value="11" Text="11" />
                                                                                <asp:ListItem Value="12" Text="12" />
                                                                            </asp:DropDownList>
                                                                            <asp:Label runat="server" ID="lblRepeatInterval">weeks</asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr runat="server" id="trRepeatOnWeeek">
                                                                        <td style="text-align: right; width: 10em;">
                                                                            <strong>Send on</strong>
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox runat="server" ID="cbMonday" Text="M" AutoPostBack="True" OnCheckedChanged="OnScheduleChangedSchedule" />
                                                                            <asp:CheckBox runat="server" ID="cbTuesday" Text="T" AutoPostBack="True" OnCheckedChanged="OnScheduleChangedSchedule" />
                                                                            <asp:CheckBox runat="server" ID="cbWednesday" Text="W" AutoPostBack="True" OnCheckedChanged="OnScheduleChangedSchedule" />
                                                                            <asp:CheckBox runat="server" ID="cbThursday" Text="T" AutoPostBack="True" OnCheckedChanged="OnScheduleChangedSchedule" />
                                                                            <asp:CheckBox runat="server" ID="cbFriday" Text="F" AutoPostBack="True" OnCheckedChanged="OnScheduleChangedSchedule" />
                                                                            <asp:CheckBox runat="server" ID="cbSaturday" Text="S" AutoPostBack="True" OnCheckedChanged="OnScheduleChangedSchedule" />
                                                                            <asp:CheckBox runat="server" ID="cbSunday" Text="S" AutoPostBack="True" OnCheckedChanged="OnScheduleChangedSchedule" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr runat="server" id="trRepeatOnMonth">
                                                                        <td style="text-align: right; width: 10em;">
                                                                            <strong>Repeat on</strong>
                                                                        </td>
                                                                        <td>
                                                                            <asp:RadioButtonList runat="server" ID="rblRepeatOnMonth" RepeatDirection="Horizontal"
                                                                                AutoPostBack="True" OnSelectedIndexChanged="OnScheduleChangedSchedule">
                                                                                <asp:ListItem Value="DayOfMonth" Text="day of the month" Selected="True" />
                                                                                <asp:ListItem Value="DayOfWeek" Text="day of the week" />
                                                                            </asp:RadioButtonList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr runat="server" id="tr1">
                                                                        <td style="text-align: right; width: 10em; vertical-align: top;">
                                                                            <%--<strong runat="server" id="stgDescription">Summary</strong>--%>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label runat="server" ID="lblSummary"></asp:Label>
                                                                            <asp:HyperLink runat="server" NavigateUrl="#next10Runs" CssClass="popuplink10" ID="hlScheduledDates"
                                                                                ImageUrl="~/Images/Time_Trigger_Icon_16.png" ToolTip="Scheduled dates" />
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                            </fieldset>

                                                        </td>
                                                        <td style="width: 5px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                    </tr>

                                                </table>
                                            </div>

                                        </asp:Panel>

                                        <div style="display: none">
                                            <div id="next10Runs" style="width: 300px; height: 230px; overflow: auto;" class="ContentMain">
                                                <div style="font-weight: bold; margin-bottom: 10px;">Scheduled dates (first 10):</div>
                                                <asp:Repeater runat="server" ID="repeater10Runs">
                                                    <ItemTemplate>
                                                        <div style="margin: 5px 0;"><%# Container.DataItem.ToString() %></div>
                                                    </ItemTemplate>
                                                    <AlternatingItemTemplate>
                                                        <div style="margin: 5px 0; background-color: lightgray;"><%# Container.DataItem.ToString() %></div>
                                                    </AlternatingItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>

                                    </ContentTemplate>
                                    <%--<Triggers>
                                     <asp:AsyncPostBackTrigger ControlID="optReportDates" EventName="OnSelectedIndexChanged" />
                                 </Triggers>--%>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>

                <td style="vertical-align: top; padding-top: 20px;padding-left:20px;">

                    <table runat="server" id="tblSendReportTo" border="0" cellpadding="0" cellspacing="0">

                        <tr>
                            <td colspan="2">
                                <span style="font-weight: bold;">Send Report To</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:UpdatePanel ID="upReportRecipient" runat="server">
                                    <ContentTemplate>

                                        <dbg:dbggridview id="grdReportRecipient" allowpaging="True" runat="server" autogeneratecolumns="false"
                                            datakeynames="ReportRecipientID" headerstyle-horizontalalign="Center" pagesize="200" borderwidth="0"
                                            rowstyle-horizontalalign="Center" cssclass="gridviewnormal" onrowcommand="grdRepotRecipient_RowCommand"
                                            onrowdatabound="grdReportRecipient_RowDataBound" alternatingrowstyle-backcolor="#DCF2F0" width="300px">
                                            <PagerSettings Position="Top" />
                                            <RowStyle CssClass="gridview_row" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                    <HeaderTemplate>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/App_Themes/Default/Images/delete_s.png"
                                                            CommandName="deletetype" CommandArgument='<%# Eval("ReportRecipientID") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false">
                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblID" runat="server" Text='<%# Eval("ReportRecipientID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Email">
                                                    <ItemTemplate>
                                                        <div style="padding-left: 10px;">
                                                            <asp:Label runat="server" ID="lblEmail" Text='<%# Eval("Email") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridview_header" />
                                            <PagerTemplate>
                                                <asp:GridViewPager runat="server" ID="ReportRecipientPager"
                                                    HideFilter="true" HideRefresh="true" HideExport="true" HideDelete="true"
                                                    HideGo="true" HideNavigation="true" HidePageSize="true" HidePageSizeButton="true"
                                                    OnBindTheGridAgain="ReportRecipientPager_BindTheGridAgain" />
                                            </PagerTemplate>
                                        </dbg:dbggridview>


                                        <br />
                                        <div runat="server" id="divEmptyAddReportRecipient" visible="false" style="padding-left: 20px;">

                                            <table>
                                                <tr>
                                                    <td rowspan="2" style="padding-right: 10px;">
                                                        <asp:HyperLink runat="server" ID="hlAddReportRecipient2" Style="text-decoration: none; color: Black;"
                                                            ToolTip="New Recipient" CssClass="popuplinkRR">
                                                            <asp:Image runat="server" ID="Image1" ImageUrl="~/App_Themes/Default/images/add32.png" />
                                                        </asp:HyperLink>
                                                    </td>
                                                    <td>
                                                        <asp:HyperLink runat="server" ID="hlAddReportRecipient" Style="text-decoration: none; color: Black;"
                                                            CssClass="popuplinkRR">
                                                            <strong style="text-decoration: underline; color: Blue;">
                                                                Add new recipient now.
                                                            </strong>
                                                        </asp:HyperLink>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>No recipients have been added yet.
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>
                                        <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                                        <br />
                                        <div style="display: none; visibility: hidden">
                                            <asp:Button runat="server" ID="btnRefreshReportRecipient" ClientIDMode="Static"
                                                Style="display: none;" OnClick="btnRefreshReportRecipient_Click" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: left;">
                    <table runat="server" id="tblReportItems" border="0" cellpadding="0" cellspacing="0" align="center">
                        <%--<tr>
                        <td colspan="3" align="center">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left">
                                        <span class="TopTitle" style="padding-left: 20px; 
                                            <asp:Label runat="server" ID="Label3"></asp:Label></span>
                                    </td>
                                    <td align="left">
                                        <div style="width: 100%;">
                                            
                                            <div style="/*rp modified ticket 4285*/float: /*right*/ left; width: 55px; margin-top: 3px;" runat="server" id="divCopy" visible="true">
                                                <asp:HyperLink runat="server" ID="hlCopyLink" NavigateUrl="ReportList.aspx" CssClass="popuplinkCopy">
                                        <asp:Image runat="server" ID="Image6"  ImageUrl="~/App_Themes/Default/images/CopyItem.png"
                                                   ToolTip="Duplicate Sheets" />
                                                </asp:HyperLink>
                                            </div>
                                            <div style="/*rp modified ticket 4285*/float: /*right*/ left; width: 55px; margin-top: 3px;" runat="server" id="div5" visible="false">
                                                <asp:HyperLink runat="server" ID="HyperLink2">
                                        <asp:Image runat="server" ID="Image7" ImageUrl="~/App_Themes/Default/images/Edit_big.png"
                                                   ToolTip="Edit" />
                                                </asp:HyperLink>
                                            </div>
                                            <div id="divReport" style="/*rp modified ticket 4285*/float: /*right*/ left; width: 55px; margin-top: 3px; display: block; visibility: visible;">
                                                <asp:HyperLink runat="server" ID="hlReport">
                                        <asp:Image runat="server" ID="imgReport" ImageUrl="~/App_Themes/Default/images/export_excel.png"
                                                   ToolTip="Export to Excel" />
                                                </asp:HyperLink>
                                            </div>
                                            <div id="divReportDate" style="/*rp modified ticket 4285*/float: /*right*/ left; width: 55px; margin-top: 3px; display: block; visibility: visible;">
                                                <asp:HyperLink runat="server" ID="hlReportDate" CssClass="popuplinkRD">
                                        <asp:Image runat="server" ID="imgReport2" ImageUrl="~/App_Themes/Default/images/export_excel.png"
                                                   ToolTip="Export to Excel" />
                                                </asp:HyperLink>
                                            </div>
                                            <div style="/*rp modified ticket 4285*/float: /*right*/ left; width: 55px; margin-top: 3px;">
                                                <asp:HyperLink runat="server" ID="hlSchedule" CssClass="popuplinkSchedule">
                                        <asp:Image runat="server" ID="imgSchedule" ImageUrl="~/App_Themes/Default/images/clock_select_remain.png"
                                                   ToolTip="Schedule" />
                                                </asp:HyperLink>
                                            </div>
                                            <div style="/*rp modified ticket 4285*/float: /*right*/ left; width: 55px;" runat="server" id="divDetails" visible="true">
                                                <asp:HyperLink runat="server" ID="hlDetails" CssClass="popuplinkDetails">
                                        <asp:Image runat="server" ID="imdDetails" ImageUrl="~/App_Themes/Default/images/Config.png"
                                                   ToolTip="Properties" />
                                                </asp:HyperLink>
                                            </div>
                                            <div style="/*rp modified ticket 4285*/float: /*right*/ left; width: 55px;">
                                                <asp:HyperLink runat="server" ID="hlBack">
                                        <asp:Image runat="server" ID="Image8" ImageUrl="~/App_Themes/Default/images/Back.png"
                                                   ToolTip="Back" />
                                                </asp:HyperLink>
                                            </div>
                                           
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label runat="server" ID="Label4" CssClass="TopSubtitle" Style="padding: 20px/*RP Added Ticket 4285*/"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>--%>
                        <tr>
                            <td colspan="3" height="13"></td>
                        </tr>
                        <tr>
                            <td valign="top">&nbsp;
                            </td>
                            <td valign="top">
                                <div id="search" style="padding-bottom: 10px">
                                    <%--<asp:ValidationSummary ID="ValidationSummary3" runat="server" EnableClientScript="true"
                                    ShowMessageBox="true" ShowSummary="false" HeaderText="Please correct the following errors:" />--%>
                                </div>
                                <asp:Panel ID="Panel3" runat="server">

                                    <div runat="server" id="div6">

                                        <asp:UpdatePanel ID="upReportItem" runat="server">
                                            <ContentTemplate>

                                                <table>
                                                    <tr>
                                                        <td>
                                                            <br />
                                                            <div style="font-size: large; margin-left: 25px; margin-bottom: 0px;">Sheets</div>
                                                            <div style="padding-left: 20px; padding-top: 10px;" id="divReportItemSingleInstance">
                                                                <table>
                                                                    <tr>
                                                                        <td align="left">
                                                                            <asp:HiddenField runat="server" ID="hfAskReportDate" ClientIDMode="Static" />
                                                                            <asp:HiddenField runat="server" ID="hfReportItemIDForItemPosition" ClientIDMode="Static" />
                                                                            <asp:Button runat="server" ID="btnRefreshReportItem" ClientIDMode="Static"
                                                                                Style="display: none;" OnClick="btnRefreshReportItem_Click" />
                                                                            <asp:Button runat="server" ID="btnReportItemIDForItemPosition" ClientIDMode="Static"
                                                                                Style="display: none;" OnClick="btnReportItemIDForItemPosition_Click" />
                                                                            <%--RP MODIFIED Ticket 4267 Removed Datagrid Width  --%>
                                                                            <dbg:dbggridview id="grdReportItem" allowpaging="True" runat="server" autogeneratecolumns="false"
                                                                                datakeynames="ReportItemID" headerstyle-horizontalalign="Center" pagesize="200"
                                                                                rowstyle-horizontalalign="Center" cssclass="gridview" onrowcommand="grdReportItem_RowCommand"
                                                                                onrowdatabound="grdReportItem_RowDataBound" alternatingrowstyle-backcolor="#DCF2F0">
                                                                                <PagerSettings Position="Top" />
                                                                                <RowStyle CssClass="gridview_row" />
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                                        <HeaderTemplate>
                                                                                            <input id="chkAll" onclick="javascript: checkAll(this);" runat="server"
                                                                                                type="checkbox" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkDelete" runat="server" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField Visible="false">
                                                                                        <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="LblID" runat="server" Text='<%# Eval("ReportItemID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField>
                                                                                        <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <asp:HyperLink ID="EditHyperLink" runat="server" ToolTip="Edit"
                                                                                                NavigateUrl='<%# GetEditURLItem(Eval("ReportItemID").ToString()) %>'
                                                                                                ImageUrl="~/App_Themes/Default/Images/iconEdit.png" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField ItemStyle-CssClass="sortHandleVT">
                                                                                        <ItemStyle HorizontalAlign="Center" Width="10px" />
                                                                                        <ItemTemplate>
                                                                                            <asp:Image ID="Image2" runat="server" ImageUrl="~/App_Themes/Default/Images/MoveIcon.png"
                                                                                                ToolTip="Drag and drop to change order" />
                                                                                            <input type="hidden" id='hfReportItemID' value='<%# Eval("ReportItemID") %>'
                                                                                                class='ReportItemID' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Item Name">
                                                                                        <ItemTemplate>
                                                                                            <div style="padding-left: 10px;">
                                                                                                <asp:Label runat="server" ID="lblObjectName"></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Item Type">
                                                                                        <ItemTemplate>
                                                                                            <div style="padding-left: 10px;">
                                                                                                <asp:Label runat="server" ID="lblObjectType"></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Item Title">
                                                                                        <ItemTemplate>
                                                                                            <div style="padding-left: 10px;">
                                                                                                <asp:Label runat="server" ID="lblHeading"></asp:Label>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Position" Visible="false">
                                                                                        <ItemTemplate>
                                                                                            <div style="padding-left: 10px;">
                                                                                                <asp:TextBox runat="server" ID="txtItemPosition" CssClass="NormalTextBox" Width="50px"></asp:TextBox>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <HeaderStyle CssClass="gridview_header" />
                                                                                <PagerTemplate>
                                                                                    <asp:GridViewPager runat="server" ID="ReportItemPager" OnDeleteAction="ReportItemPager_DeleteAction"
                                                                                        DelConfirmation="Are you sure you want to remove selected report item(s)?"
                                                                                        ShowSaveChanges="False" HideFilter="true" HideRefresh="true" HideExport="true"
                                                                                        HideGo="true" HideNavigation="true" HidePageSize="true" HidePageSizeButton="true"
                                                                                        OnBindTheGridAgain="ReportItemPager_BindTheGridAgain" />
                                                                                </PagerTemplate>
                                                                            </dbg:dbggridview>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <br />
                                                            <div runat="server" id="divEmptyAddReportItem" visible="false" style="padding-left: 20px;">
                                                                <asp:HyperLink runat="server" ID="hlAddReportItem2" Style="text-decoration: none; color: Black;"
                                                                    ToolTip="New Report" CssClass="popuplinkRI">
                                                                    <asp:Image runat="server" ID="Image9" ImageUrl="~/App_Themes/Default/images/add32.png" />
                                                                </asp:HyperLink>
                                                                &nbsp;No items have been added yet.&nbsp;
                                                                <asp:HyperLink runat="server" ID="hlAddReportItem" Style="text-decoration: none; color: Black;"
                                                                    CssClass="popuplinkRI">
                                                                    <strong style="text-decoration: underline; color: Blue;">
                                                                        Add new item now.
                                                                    </strong>
                                                                </asp:HyperLink>
                                                            </div>
                                                            <br />
                                                            <div style="display: none; visibility: hidden">
                                                                <asp:LinkButton runat="server" ID="lbUpdateDetails" OnClick="lbUpdateDetails_OnClick"></asp:LinkButton>
                                                                <asp:LinkButton runat="server" ID="lbDuplicateReport" OnClick="lbDuplicateReport_OnClick"></asp:LinkButton>
                                                                <asp:TextBox runat="server" ID="txtSourceReportId"></asp:TextBox>
                                                                <asp:TextBox runat="server" ID="txtClickOnLoad" ClientIDMode="Static"></asp:TextBox>
                                                            </div>


                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center;">
                                                            <asp:Panel runat="server" ID="divRunReport">
                                                                <asp:LinkButton runat="server" ID="lnkRunReport" CssClass="btn" OnClick="lnkRunReport_Click">
                                                                  
                                                                                <strong runat="server" id="strRunReport" >Run Report</strong>
                                                                          
                                                                                <asp:Image runat="server" ID="imgRunReport" ImageUrl="~/App_Themes/Default/images/RunReport16.png" Style="padding-left: 5px; height:12px;" />
                                                                     
                                                                </asp:LinkButton>

                                                                  <asp:LinkButton runat="server" ID="lnkRunReportDate" OnClientClick=" $('.ajax-indicator-full').hide(); runReportDate(); return false;" CssClass="btn">
                                                                  
                                                                                <strong runat="server">Run Report</strong>
                                                                          
                                                                                <asp:Image runat="server" ID="Image6" ImageUrl="~/App_Themes/Default/images/RunReport16.png" Style="padding-left: 5px; height:12px;" />
                                                                     
                                                                </asp:LinkButton>
                                                            </asp:Panel>



                                                        </td>
                                                    </tr>

                                                </table>


                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <br />
                                    <asp:Label runat="server" ID="Label5" ForeColor="Red"></asp:Label>
                                    <br />
                                    <div style="display: none; visibility: hidden">
                                        <asp:HyperLink ID="hlOpenReportHidden" CssClass="popupRunReportDate" runat="server" ClientIDMode="Static"
                                            NavigateUrl="ReportHandler.ashx" Text="Open Report" />
                                        <asp:HiddenField ID="hfReportId" runat="server" ClientIDMode="Static" />
                                    </div>
                                </asp:Panel>
                            </td>
                            <td valign="top">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" height="13"></td>
                        </tr>
                    </table>

                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center; padding-bottom: 20px;">
                    <asp:LinkButton runat="server" ID="lnkNextStep" CssClass="btn" OnClick="lnkNextStep_Click" Visible="false" CausesValidation="true" ValidationGroup="MKE" OnClientClick="return validate();">
                        <strong runat="server" id="Strong1">Next Step</strong>
                    </asp:LinkButton>
                </td>
                <td></td>
            </tr>
        </table>

    </div>
    <script type="text/javascript">
        function OpenReportItem() {
            $("#hlAddReportItem").trigger('click');
        }

        //function adjustGridView2() {
        //    $(".gridview").each(function () {
        //        var grid = $(this);

        //        grid.parent().css({
        //            width: 100
        //        });
        //    });
        //}

        //adjustGridView2();



    </script>

</asp:Content>


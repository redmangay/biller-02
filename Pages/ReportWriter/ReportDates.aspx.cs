﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;

using DocGen.DAL;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;

public partial class Pages_ReportWriter_ReportDates : System.Web.UI.Page
{
    int? _iReportId = null;
    Report _report = null;


    protected void Page_Load(object sender, EventArgs e)
    {
        /*== Set the default dates == */
        if (!IsPostBack)
        {
            DateTime dtStartDate = DateTime.Today;
            string strMonthSeparator = "/";
            if (dtStartDate.Month.ToString().Length == 1)
            {
                strMonthSeparator = "/0";
            }
            string firstDayOfMonth = "01" + strMonthSeparator + dtStartDate.Month.ToString() + "/" + dtStartDate.Year.ToString();
            txtStartDate.Text = firstDayOfMonth;
            txtEndDate.Text = dtStartDate.ToString("dd/MM/yyyy");
            txtEndTime.Text = "";
            txtStartTime.Text = "";
        }
      
        /* Red 09092019: Lets validate the dates and generate the Report == */
        string jsRunReportDates = @" 

                            function showErrorMessage(message) {
                                document.getElementById('" + lblErrorMessage.ClientID + @"').style.display = 'inherit';
                                document.getElementById('" + lblErrorMessage.ClientID + @"').innerText = message;
                            }

                            function checkTime(time) {


                                // regular expression to match required time format
                                re = /^(\d{1,2}):(\d{2})([ap]m)?$/;

                                if (time != '') {
                                    if (regs = time.match(re)) {
                                        if (regs[3]) {
                                            // 12-hour value between 1 and 12
                                            if (regs[1] < 1 || regs[1] > 12) {
                                                showErrorMessage('Invalid value for hours: ' + regs[1]);
                                                return false;
                                            }
                                        } else {
                                            // 24-hour value between 0 and 23
                                            if (regs[1] > 23) {
                                                showErrorMessage('Invalid value for hours: ' + regs[1]);
                                                return false;
                                            }
                                        }
                                        // minute value between 0 and 59
                                        if (regs[2] > 59) {
                                            showErrorMessage('Invalid value for minutes: ' + regs[2]);
                                            return false;
                                        }
                                    } else {
                                        showErrorMessage('Invalid time format: ' + time);
                                        return false;

                                    }
                                }

                                //alert('All input fields have been validated!');
                                return true;
                            }

                            function checkDate(date) {
                                document.getElementById('" + lblErrorMessage.ClientID + @"').style.display = 'none';
                                // regular expression to match required date format
                                re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;

                                if (date != '') {
                                    if (regs = date.match(re)) {
                                        // day value between 1 and 31
                                        if (regs[1] < 1 || regs[1] > 31) {
                                            showErrorMessage('Invalid value for day: ' + regs[1]);
                                            return false;
                                        }
                                        // month value between 1 and 12
                                        if (regs[2] < 1 || regs[2] > 12) {
                                            showErrorMessage('Invalid value for month: ' + regs[2]);
                                            return false;
                                        }
                                        // year value between 1902 and 2019
                                        if (regs[3] < 1902 || regs[3] > (new Date()).getFullYear()) {
                                            showErrorMessage('Invalid value for year: ' + regs[3] + ' - must be between 1902 and ' + (new Date()).getFullYear());
                                            return false;
                                        }
                                    } else {

                                        showErrorMessage('Invalid date format: ' + date);
                                        return false;
                                    }
                                }

                                // regular expression to match required time format
                                re = /^(\d{1,2}):(\d{2})([ap]m)?$/;

                                return true;
                            }

                            function processDate(date) {
                                var parts = date.split('/');
                                return new Date(parts[2], parts[1] - 1, parts[0]);
                            }

                            $('#divRunDates').on('hide.bs.modal', function () {
                                // document.getElementById('lnkGoBack').click();
                            });




                            function rundateGetDates() {  

                                //$('#hlRunDate').prop('disabled', true);  
                                document.getElementById('" + lblErrorMessage.ClientID + @"').style.display = 'inherit';
                                var startDate = document.getElementById('" + txtStartDate.ClientID + @"').value;
                                var endDate = document.getElementById('" + txtEndDate.ClientID + @"').value;

                                var startTime = document.getElementById('" + txtStartTime.ClientID + @"').value;
                                var endTime = document.getElementById('" + txtEndTime.ClientID + @"').value;

                                if (checkDate(startDate)) {
                                    if (checkDate(endDate)) {
                                        // start is less the end
                                        if ((processDate(endDate) < processDate(startDate))) {
                                            document.getElementById('" + lblErrorMessage.ClientID + @"').style.display = 'inherit';
                                            document.getElementById('" + lblErrorMessage.ClientID + @"').innerText = 'End date must be after the start date';
                                            return false;
                                        }
                                        else {

                                            if (checkTime(startTime)) {

                                                if (checkTime(endTime)) {
                                                    jsReportDatesShowHideProgress();
                                                      document.getElementById('lnkRunReport').click();
                                                               // parent.$.fancybox.close();
                                                }
                                                else { return false; }
                                            }
                                            else { return false; }


                                        }

                                    }
                                    else {
                                        return false;
                                    }

                                }
                                else {
                                    return false;
                                }
                            }

                                    function runReportClose(){
                                    parent.$.fancybox.close();

                                    }                             

                            ";

        ScriptManager.RegisterStartupScript(this, this.GetType(), "jsRunReportDates", jsRunReportDates, true);



        if (Request.QueryString["ReportID"] != null)
        {
            _iReportId = int.Parse(Request.QueryString["ReportID"].ToString());
        }

        _report = ReportManager.ets_Report_Detail((int)_iReportId);

        if (!IsPostBack)
        {
            PopulateTerminology();
        }
    }

    protected void PopulateTerminology()
    {

    }

    

    protected void lnkRunReport_OnClick(object sender, EventArgs e)
    {
        /* == Red 04092019: Ticket 4818 == */
        string strStartTime = "";
        string strEndTime = "";
        strStartTime = txtStartTime.Text.Trim() == "" ? " 00:00:00" : " " + txtStartTime.Text.Trim();
        strEndTime = txtEndTime.Text.Trim() == "" ? " 23:59:00" : " " + txtStartTime.Text.Trim();

        string strStartDate = Common.ReturnDateStringFromToken(txtStartDate.Text.Trim());
        string strEndDate = Common.ReturnDateStringFromToken(txtEndDate.Text.Trim());

        DateTime dateStartValue;
        DateTime dateEndValue;
        string reportUrl = "";
        if (DateTime.TryParseExact(strStartDate.Trim() + strStartTime, Common.DateTimeformats,
                                        new CultureInfo("en-GB"),
                                        DateTimeStyles.None,
                                        out dateStartValue))
        {
            if (DateTime.TryParseExact(strEndDate.Trim() + strEndTime, Common.DateTimeformats,
                                       new CultureInfo("en-GB"),
                                       DateTimeStyles.None,
                                       out dateEndValue))
            {
                reportUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                         "Pages/ReportWriter/ReportHandler.ashx" +
                         "?ReportID=" + Cryptography.Encrypt(_iReportId.ToString()) +
                          "&StartDate=" + Cryptography.Encrypt(dateStartValue.ToString()) +
                          "&EndDate=" + Cryptography.Encrypt(dateEndValue.ToString());


                Response.Redirect(reportUrl);

            }

        }
        /* == End Red == */
    }
}
﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Globalization;
using System.Xml;

public partial class Pages_ReportWriter_Report : SecurePage
{
    Common_Pager _gvPager;

    int _iSearchCriteriaID = -1;
    int _iStartIndex = 0;
    int _iMaxRows = 10;
    string _strGridViewSortColumn = "ReportName";
    string _strGridViewSortDirection = "ASC";

    User ObjUser;

    public override void VerifyRenderingInServerForm(Control control)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "Reports";



        string jsReportDates = @" 

            function runReportDate(reportID) {
                     var linkReportDate = document.getElementById('runReportDate_');
                     linkReportDate.setAttribute('href', '/Pages/ReportWriter/ReportDates.aspx?ReportID='+ reportID);
                     linkReportDate.click();
                      $('.ajax-indicator-full').fadeOut();

                }
                            ";

        ScriptManager.RegisterStartupScript(this, this.GetType(), "jsReportDates_", jsReportDates, true);


        try
        {
            ObjUser = (User)Session["User"];

            if (!IsPostBack && Session["User"] != null) // Session["User"] == null means that user is not logged in yet
            {
                PopulatePeriodDDL();
                PopulateTableDDL();
                PopulateGraphDDL();

                if (Request.QueryString["SearchCriteria"] != null)
                {
                    PopulateSearchCriteria(int.Parse(Cryptography.Decrypt(Request.QueryString["SearchCriteria"].ToString())));
                }

                //if (Session["GridPageSize"] != null && Session["GridPageSize"].ToString() != "")
                //{
                //    gvTheGrid.PageSize = int.Parse(Session["GridPageSize"].ToString());
                //}

                if (Request.QueryString["SearchCriteria"] != null)
                {
                    //gvTheGrid.PageSize = _iMaxRows;
                    gvTheGrid.GridViewSortColumn = _strGridViewSortColumn;
                    if (_strGridViewSortDirection.ToUpper() == "ASC")
                    {
                        gvTheGrid.GridViewSortDirection = SortDirection.Ascending;
                    }
                    else
                    {
                        gvTheGrid.GridViewSortDirection = SortDirection.Descending;
                    }
                    BindTheGrid(_iStartIndex, _iMaxRows);
                }
                else
                {
                    gvTheGrid.GridViewSortColumn = "ReportName";
                    gvTheGrid.GridViewSortDirection = SortDirection.Ascending;
                    BindTheGrid(0, gvTheGrid.PageSize);
                }
            }
            else
            {
            }

            GridViewRow gvr = gvTheGrid.TopPagerRow;
            if (gvr != null)
                _gvPager = (Common_Pager)gvr.FindControl("Pager");
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Reports", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }

    protected void PopulateSearchCriteria(int searchCriteriaId)
    {
        try
        {
            SearchCriteria theSearchCriteria = SystemData.SearchCriteria_Detail(searchCriteriaId);

            if (theSearchCriteria != null)
            {

                XmlDocument xmlDoc = new XmlDocument();

                xmlDoc.Load(new StringReader(theSearchCriteria.SearchText));

                XmlElement element = null;
                element = xmlDoc.FirstChild[ddlTable.ID];
                if (element != null)
                    ddlTable.SelectedValue = element.InnerText;
                element = xmlDoc.FirstChild[ddlGraph.ID];
                if (element != null)
                    ddlGraph.SelectedValue = element.InnerText;

                element = xmlDoc.FirstChild[txtReportText.ID];
                if (element != null)
                    txtReportText.Text = element.InnerText;
                element = xmlDoc.FirstChild[ddlPeriod.ID];
                if (element != null)
                    ddlPeriod.SelectedValue = element.InnerText;

                element = xmlDoc.FirstChild["iStartIndex"];
                if (element != null)
                    _iStartIndex = int.Parse(element.InnerText);
                element = xmlDoc.FirstChild["iMaxRows"];
                if (element != null)
                    _iMaxRows = int.Parse(element.InnerText);
                element = xmlDoc.FirstChild["GridViewSortColumn"];
                if (element != null)
                    _strGridViewSortColumn = element.InnerText;
                element = xmlDoc.FirstChild["GridViewSortDirection"];
                if (element != null)
                    _strGridViewSortDirection = element.InnerText;
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
    }

    protected void BindTheGrid(int iStartIndex, int iMaxRows)
    {
        lblMsg.Text = "";

        //SearchCriteria 
        try
        {
            string xml = null;
            xml = @"<root>" +
                  " <" + ddlTable.ID + ">" + HttpUtility.HtmlEncode(ddlTable.SelectedValue) + "</" + ddlTable.ID + ">" +
                  " <" + ddlGraph.ID + ">" + HttpUtility.HtmlEncode(ddlGraph.SelectedValue) + "</" + ddlGraph.ID + ">" +

                  " <" + txtReportText.ID + ">" + HttpUtility.HtmlEncode(txtReportText.Text) + "</" + txtReportText.ID + ">" +
                  " <" + ddlPeriod.ID + ">" + HttpUtility.HtmlEncode(ddlPeriod.SelectedValue) + "</" + ddlPeriod.ID + ">" +

                  " <iStartIndex>" + HttpUtility.HtmlEncode(iStartIndex.ToString()) + "</iStartIndex>" +
                  " <iMaxRows>" + HttpUtility.HtmlEncode(iMaxRows.ToString()) + "</iMaxRows>" +
                  " <GridViewSortColumn>" + HttpUtility.HtmlEncode(gvTheGrid.GridViewSortColumn) + "</GridViewSortColumn>" +
                  " <GridViewSortDirection>" + HttpUtility.HtmlEncode(gvTheGrid.GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC") + "</GridViewSortDirection>" +
                  "</root>";

            SearchCriteria theSearchCriteria = new SearchCriteria(null, xml);
            _iSearchCriteriaID = SystemData.SearchCriteria_Insert(theSearchCriteria);
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
        //End Searchcriteria

        try
        {
            int iTN = 0;

            ViewState[gvTheGrid.ID + "PageIndex"] = (iStartIndex / gvTheGrid.PageSize) + 1;
            gvTheGrid.DataSource = ReportManager.ets_Report_Select(null, int.Parse(Session["AccountID"].ToString()),
                ddlTable.SelectedValue == "-1" ? (int?)null : int.Parse(ddlTable.SelectedValue),
                ddlGraph.SelectedValue == "-1" ? (int?)null : int.Parse(ddlGraph.SelectedValue),
                txtReportText.Text.Trim(),
                Enum.Parse(typeof(Report.ReportInterval), ddlPeriod.SelectedValue) as Report.ReportInterval?,
                null, null,
                null, null, null,
                gvTheGrid.GridViewSortColumn,
                gvTheGrid.GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC",
                iStartIndex, iMaxRows, out iTN);

            gvTheGrid.VirtualItemCount = iTN;
            gvTheGrid.DataBind();
            if (gvTheGrid.TopPagerRow != null)
                gvTheGrid.TopPagerRow.Visible = true;

            GridViewRow gvr = gvTheGrid.TopPagerRow;
            if (gvr != null)
            {
                _gvPager = (Common_Pager)gvr.FindControl("Pager");
                if (ViewState[gvTheGrid.ID + "PageIndex"] != null)
                    _gvPager.PageIndex = int.Parse(ViewState[gvTheGrid.ID + "PageIndex"].ToString());

                _gvPager.PageSize = gvTheGrid.PageSize;
                _gvPager.TotalRows = iTN;
                _gvPager.AddURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                  "Pages/ReportWriter/Reportfull.aspx" +
                                  "?mode=" + Cryptography.Encrypt("add") +
                                  "&SearchCriteria=" + Cryptography.Encrypt(_iSearchCriteriaID.ToString());
            }

            if (iTN == 0)
            {
                if (IsFiltered())
                {
                    divNoFilter.Visible = true;
                    divEmptyData.Visible = false;
                }
                else
                {
                    divEmptyData.Visible = true;
                    divNoFilter.Visible = false;
                }

                string addNewReportURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                         "Pages/ReportWriter/Reportfull.aspx" +
                                         "?mode=" + Cryptography.Encrypt("add") +
                                         "&SearchCriteria=" + Cryptography.Encrypt(_iSearchCriteriaID.ToString());

                hpNew.NavigateUrl = addNewReportURL;
                hpNew2.ImageUrl = Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "App_Themes/Default/Images/add32.png";
                hpNew2.NavigateUrl = addNewReportURL;

                hplNewDataFilter.NavigateUrl = addNewReportURL;
                hplNewDataFilter2.ImageUrl = Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "App_Themes/Default/Images/add32.png";
                hplNewDataFilter2.NavigateUrl = addNewReportURL;
            }
            else
            {
                divEmptyData.Visible = false;
                divNoFilter.Visible = false;
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Report", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }

    protected void gvTheGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
            e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");

            /* == Red 10092019: Ticke 4818 ==*/
            Label lblReportId = (Label)e.Row.FindControl("LblID");
            HyperLink runReport = (HyperLink)e.Row.FindControl("RunHyperLink");
            HyperLink lbOpen = (HyperLink)e.Row.FindControl("lbOpen");
            if (runReport != null)
            {
                if(lblReportId != null)
                {
                    Report report = ReportManager.ets_Report_Detail(int.Parse(lblReportId.Text));
                    if (report != null)
                    {
                        if (report.ReportDates == Report.ReportInterval.IntervalDateRange 
                            && report.ScheduledFrequency != null
                            && report.ScheduledFrequency == 0
                            && report.ReportDatesBefore == Report.ReportIntervalEnd.Undefined)
                        {
                            runReport.NavigateUrl = "javascript: runReportDate(" + lblReportId.Text.ToString() + "); ";
                            lbOpen.NavigateUrl = "javascript: runReportDate(" + lblReportId.Text.ToString() + "); ";
                        }
                        else
                        {
                            runReport.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                     "Pages/ReportWriter/ReportHandler.ashx" +
                                     "?ReportID=" + Cryptography.Encrypt(report.ReportID.ToString());

                            lbOpen.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                    "Pages/ReportWriter/ReportHandler.ashx" +
                                    "?ReportID=" + Cryptography.Encrypt(report.ReportID.ToString());
                        }
                    }
                }
            }
            /* == End Red == */
        }
    }

    protected void gvTheGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        BindTheGrid(0, gvTheGrid.PageSize);
    }
    
    protected void PopulatePeriodDDL()
    {
        ddlPeriod.Items.Add(new ListItem() { Text = "", Value = ((int)Report.ReportInterval.IntervalUndefined).ToString() });
        ddlPeriod.Items.Add(new ListItem() { Text = "Date Range", Value = Report.ReportInterval.IntervalDateRange.ToString() });
        ddlPeriod.Items.Add(new ListItem() { Text = "30 Days", Value = Report.ReportInterval.Interval30Days.ToString() });
        ddlPeriod.Items.Add(new ListItem() { Text = "60 Days", Value = Report.ReportInterval.Interval60Days.ToString() });
        ddlPeriod.Items.Add(new ListItem() { Text = "90 Days", Value = Report.ReportInterval.Interval90Days.ToString() });
        ddlPeriod.Items.Add(new ListItem() { Text = "1 Year", Value = Report.ReportInterval.Interval1Year.ToString() });
        ddlPeriod.Items.Add(new ListItem() { Text = "YTD", Value = Report.ReportInterval.IntervalYTD.ToString() });
    }

    protected void PopulateTableDDL()
    {
        int iTN = 0;
        ddlTable.DataSource = RecordManager.ets_Table_Select(null,
            null,
            null,
            int.Parse(Session["AccountID"].ToString()),
            null, null, true,
            "st.TableName", "ASC",
            null, null, ref iTN, Session["STs"].ToString());

        ddlTable.DataBind();
        ListItem liAny = new ListItem("", "-1");
        ddlTable.Items.Insert(0, liAny);
    }

    protected void PopulateGraphDDL()
    {
        int iTN = 0;
        ddlGraph.DataSource = GraphManager.ets_GraphOption_Select(
                int.Parse(Session["AccountID"].ToString()), null,
                null, null, true, "Heading", "ASC", null, null, ref iTN, ObjUser.UserID);

        ddlGraph.DataBind();
        ListItem liAny = new ListItem("", "-1");
        ddlGraph.Items.Insert(0, liAny);
    }

    protected void lnkSearch_OnClick(object sender, EventArgs e)
    {
        BindTheGrid(0, gvTheGrid.PageSize);      
    }

    protected void gvTheGrid_PreRender(object sender, EventArgs e)
    {
        GridView grid = (GridView)sender;
        if (grid != null)
        {
            GridViewRow pagerRow = grid.TopPagerRow;
            if (pagerRow != null)
            {
                pagerRow.Visible = true;
            }
        }
    }

    public string GetEditURL(string strReportId)
    {
        string strURL = "#";
        Report report = ReportManager.ets_Report_Detail(int.Parse(strReportId));
        if (report != null)
        {
            strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                     "Pages/ReportWriter/Reportfull.aspx" +
                     "?ReportID=" + Cryptography.Encrypt(strReportId) +
                     "&mode=" + Cryptography.Encrypt("edit") +
                     "&SearchCriteria=" + Cryptography.Encrypt(_iSearchCriteriaID.ToString());
        }
        else
        {
            //
        }
        return strURL;
    }

    public string GetOpenURL(string strReportId)
    {
        string strURL = "#";
        Report report = ReportManager.ets_Report_Detail(int.Parse(strReportId));
        if (report != null)
        {
            if ((report.ReportDates == Report.ReportInterval.IntervalDateRange) &&
                (!report.ReportStartDate.HasValue || !report.ReportEndDate.HasValue))
            {
                strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                         "Pages/ReportWriter/ReportDates.aspx" +
                         "?ReportID=" + Cryptography.Encrypt(strReportId);
            }
            else
            {
                strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                         "Pages/ReportWriter/ReportHandler.ashx" +
                         "?ReportID=" + Cryptography.Encrypt(strReportId);
            }
        }
        else
        {
            //
        }
        return strURL;
    }

    public string GetOpenCSS(string strReportId)
    {
        string strCSS = "";
        Report report = ReportManager.ets_Report_Detail(int.Parse(strReportId));
        if (report != null)
        {
            if ((report.ReportDates == Report.ReportInterval.IntervalDateRange) &&
                (!report.ReportStartDate.HasValue || !report.ReportEndDate.HasValue))
            {
                strCSS = "popuplinkRD";
            }
        }
        else
        {
            //
        }
        return strCSS;
    }

    public string GetPeriod(string period, string days = "")
    {
        string retValue = String.Empty;
        int intValue = -1;
        if (int.TryParse(period, out intValue))
        {
            if (Enum.IsDefined(typeof(Report.ReportInterval), intValue))
            {
                switch ((Report.ReportInterval) intValue)
                {
                    case Report.ReportInterval.IntervalUndefined:
                        retValue = "?";
                        break;
                    case Report.ReportInterval.IntervalDateRange:
                        retValue = "Date Range";
                        break;
                    case Report.ReportInterval.Interval30Days:
                        retValue = "30 Days";
                        break;
                    case Report.ReportInterval.Interval60Days:
                        retValue = "60 Days";
                        break;
                    case Report.ReportInterval.Interval90Days:
                        retValue = "90 Days";
                        break;
                    case Report.ReportInterval.Interval1Year:
                        retValue = "1 Year";
                        break;
                    case Report.ReportInterval.IntervalYTD:
                        retValue = "YTD";
                        break;
                    case Report.ReportInterval.IntervalCustom:
                        retValue = days + " Days";
                        break;
                    case Report.ReportInterval.IntervalCustom2:
                        retValue = days + " Days after";
                        break;

                    default:
                        retValue = "?";
                        break;
                }
            }
        }
        return retValue;
    }

    protected void Pager_BindTheGridToExport(object sender, EventArgs e)
    {
        _gvPager.ExportFileName = "Reports";
        BindTheGrid(0, _gvPager.TotalRows);
    }

    protected void Pager_BindTheGridAgain(object sender, EventArgs e)
    {
        BindTheGrid(_gvPager.StartIndex, _gvPager.PageSize);
    }

    protected void Pager_OnApplyFilter(object sender, EventArgs e)
    {
        ddlTable.SelectedValue = "-1";
        ddlGraph.SelectedValue = "-1";
        ddlPeriod.SelectedValue = ((int) Report.ReportInterval.IntervalUndefined).ToString();
        txtReportText.Text = "";
        gvTheGrid.GridViewSortColumn = "ReportName";
        gvTheGrid.GridViewSortDirection = SortDirection.Ascending;

        lnkSearch_OnClick(null, null);
    }

    protected void Pager_DeleteAction(object sender, EventArgs e)
    {
        string sCheck = "";
        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
        {
            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
            if (ischeck)
            {
                sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
            }
        }
        if (string.IsNullOrEmpty(sCheck))
        {
            ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select a record.');", true);
        }
        else
        {
            if (Common.HaveAccess(Session["roletype"].ToString(), "5"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Problem", "alert('Read Only Account. To delete please ask your Account Administrator for appropriate rights.');", true);
                return;
            }
            DeleteItem(sCheck);
            BindTheGrid(_gvPager.StartIndex, gvTheGrid.PageSize);
        }
    }

    private void DeleteItem(string keys)
    {
        try
        {
            if (!string.IsNullOrEmpty(keys))
            {
                foreach (string sTemp in keys.Split(','))
                {
                    if (!string.IsNullOrEmpty(sTemp))
                    {
                        int i =  ReportManager.ets_Report_Delete(int.Parse(sTemp));
                        if (i == 2)
                        {
                            ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "msg_delete", "alert('This report is scheduled to run automatically. Remove the schedule first and then delete it.');", true);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Reports", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }

    protected bool IsFiltered()
    {
        if (ddlTable.SelectedValue != "-1" || ddlGraph.SelectedValue != "-1" 
            || txtReportText.Text != "" || ddlPeriod.SelectedValue != ((int) Report.ReportInterval.IntervalUndefined).ToString())
        {
            return true;
        }

        return false;
    }
}

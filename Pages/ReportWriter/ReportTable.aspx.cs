﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Collections.Generic;

public partial class Pages_ReportWriter_ReportTable : SecurePage
{
    private string _strActionMode = "view";
    private int? _iReportID;
    private Report _theReport;
    private string _qsReportID = "";


    protected void PopulateTerminology()
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (!Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
            //    Response.Redirect("~/Default.aspx", false);
        }

        if (Request.QueryString["mode"] == null)
        {
            Server.Transfer("~/Default.aspx");
        }
        else
        {
            string qsMode = Cryptography.Decrypt(Request.QueryString["mode"]);

            if (qsMode == "add" ||
                qsMode == "view" ||
                qsMode == "edit")
            {
                _strActionMode = qsMode.ToLower();

                if (Request.QueryString["ReportID"] != null)
                {
                    _qsReportID = Cryptography.Decrypt(Request.QueryString["ReportID"]);

                    _iReportID = int.Parse(_qsReportID);
                    _theReport = ReportManager.ets_Report_Detail((int)_iReportID);
                    if (!IsPostBack)
                    {
                        SetPageButtons();
                        if (Request.QueryString["popupitem"] != null)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "PopItem", "  setTimeout(function () { OpenReportItem(); }, 1000); ", true);
                        }
                    }
                }
            }
            else
            {
                Server.Transfer("~/Default.aspx");
            }
        }

        switch (_strActionMode)
        {
            case "add":
                lblTitle.Text = "Edit Report";
                if (!IsPostBack)
                    PopulateTheRecord();
                break;
            case "view":
                lblTitle.Text = "View Report";
                if (!IsPostBack)
                    PopulateTheRecord();         
                EnableTheRecordControls(false);
                break;
            case "edit":
                lblTitle.Text = "Edit Report";
                if (!IsPostBack)
                    PopulateTheRecord();
                break;
            default:
                //?
                break;
        }

        Title = lblTitle.Text;

        if (!IsPostBack)
        {
            int itemsCount = 0;

            if (_iReportID.HasValue)
                itemsCount = PopulateReportItem(_iReportID.Value);

            PopulateTerminology();

            if (_strActionMode == "add" && itemsCount == 0 && _iReportID.HasValue)
                txtClickOnLoad.Text = hlAddReportItem.ClientID;
        }
    }

    protected void PopulateSearchCriteria(int iSearchCriteriaID)
    {
        try
        {
            SearchCriteria theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);
            if (theSearchCriteria != null)
            {
                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.Load(new StringReader(theSearchCriteria.SearchText));
            }
            else
            {
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
    }

    protected void ReportItemPager_DeleteAction(object sender, EventArgs e)
    {
        string sCheck = "";
     
        for (int i = 0; i < grdReportItem.Rows.Count; i++)
        {
            bool ischeck = ((CheckBox)grdReportItem.Rows[i].FindControl("chkDelete")).Checked;
            if (ischeck)
            {
                sCheck = sCheck + ((Label)grdReportItem.Rows[i].FindControl("LblID")).Text + ",";
            }
        }
        if (string.IsNullOrEmpty(sCheck))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message_alert", "alert('Please select a record.');", true);
            return;
        }

        sCheck = sCheck + "-1";
        Common.ExecuteText("DELETE ReportItem WHERE ReportItemID IN (" + sCheck + ") ");
        if (_iReportID.HasValue)
            PopulateReportItem(_iReportID.Value);
    }

    protected void btnReportItemIDForItemPosition_Click(object sender, EventArgs e)
    {
        if (hfReportItemIDForItemPosition.Value != "")
        {
            try
            {
                string strNewVIT = hfReportItemIDForItemPosition.Value.Substring(0, hfReportItemIDForItemPosition.Value.Length - 1);
                string[] newVT = strNewVIT.Split(',');
                DataTable dtDO = Common.DataTableFromText("SELECT ItemPosition, ReportItemID FROM [ReportItem] WHERE ReportItemID IN (" + strNewVIT + ") ORDER BY ItemPosition");
                if (newVT.Length == dtDO.Rows.Count)
                {
                    for (int i = 0; i < newVT.Length; i++)
                    {
                        Common.ExecuteText("UPDATE [ReportItem] SET ItemPosition = " + i.ToString() + " WHERE ReportItemID = " + newVT[i]);
                    }
                }
            }
            catch (Exception)
            {
                //
            }

            if (_iReportID.HasValue)
                PopulateReportItem(_iReportID.Value);
        }
    }


    public string GetAddReportItemURL(int reportId)
    {
        return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
               "/Pages/ReportWriter/AddReportItem.aspx?mode=" + Cryptography.Encrypt("add") + "&ReportID=" +
               Cryptography.Encrypt(reportId.ToString());
    }

    public string GetEditURL(string objectID)
    {
        string strURL = "";
        if (Request.QueryString["SearchCriteria"] != null)
        {
            strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                     "Pages/ReportWriter/ReportTableDetail.aspx" + 
                     "?mode=" + Request.QueryString["mode"] +
                     "&SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString() +
                     "&ReportID=" + Cryptography.Encrypt(_iReportID.ToString()) +
                     "&ReportItemID=" + Cryptography.Encrypt(objectID);
        }
        else
        {
            strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                     "Pages/ReportWriter/ReportTableDetail.aspx" +
                     "?mode=" + Request.QueryString["mode"] +
                     "&ReportID=" + Cryptography.Encrypt(_iReportID.ToString()) +
                     "&ReportItemID=" + Cryptography.Encrypt(objectID);
        }
        return strURL;
    }

    protected int PopulateReportItem(int reportId)
    {
        int iTN = 0;

        hlAddReportItem.NavigateUrl = GetAddReportItemURL(reportId);
        hlAddReportItem2.NavigateUrl = hlAddReportItem.NavigateUrl;
        DataTable dtReportItems = Common.DataTableFromText("SELECT * FROM ReportItem WHERE ReportID =" + reportId.ToString() + " ORDER BY ItemPosition");

        grdReportItem.DataSource = dtReportItems;
        iTN = dtReportItems.Rows.Count;
        grdReportItem.VirtualItemCount = iTN;
        grdReportItem.DataBind();

        if (_strActionMode == "view")
        {
            grdReportItem.Columns[2].Visible = false;
            grdReportItem.Columns[3].Visible = false;
        }

        if (grdReportItem.TopPagerRow != null)
            grdReportItem.TopPagerRow.Visible = true;

        GridViewRow gvr = grdReportItem.TopPagerRow;

        if (gvr != null)
        {
            Common_Pager reportItemPager = (Common_Pager)gvr.FindControl("ReportItemPager");
            reportItemPager.AddURL = GetAddReportItemURL(reportId);
            reportItemPager.HyperAdd_CSS = "popuplinkRI";
            reportItemPager.AddToolTip = "Add";
            reportItemPager.TotalRows = iTN;

            if (_strActionMode == "view")
            {
                reportItemPager.HideAdd = true;
                reportItemPager.HideDelete = true;
            }
        }

        if (iTN == 0)
        {
            if (_strActionMode != "view")
                divEmptyAddReportItem.Visible = true;
        }
        else
        {
            divEmptyAddReportItem.Visible = false;
        }

        return iTN;
    }

    protected void grdReportItem_RowCommand(object sender, GridViewCommandEventArgs e)
    {
    }

    protected void grdReportItem_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            bool objectNotFound = false;
            ReportItem.ReportItemType itemType = (ReportItem.ReportItemType) DataBinder.Eval(e.Row.DataItem, "ItemType");
            int objectId = (int) DataBinder.Eval(e.Row.DataItem, "ObjectID");
            string objectName = String.Empty;
            string objectTypeText = String.Empty;
            switch (itemType)
            {
                case ReportItem.ReportItemType.ItemTypeTable:
                    Table theTable = RecordManager.ets_Table_Details(objectId);


                    if (theTable == null)
                        objectNotFound = true;
                    else
                    {
                        objectName = theTable.TableName;
                        objectTypeText = "Table";
                    }
                   
                    break;
                case ReportItem.ReportItemType.ItemTypeGraph:
                    GraphOption theGraph = GraphManager.ets_GraphOption_Detail(objectId);
                    if (theGraph == null)
                        objectNotFound = true;
                    else
                    {
                        objectName = theGraph.Heading;
                        objectTypeText = "Chart";
                    }
                    break;
                case ReportItem.ReportItemType.ItemTypeView:
                    ReportView theView = ReportManager.ets_ReportView_Detail(objectId);
                    if (theView == null)
                        objectNotFound = true;
                    else
                    {
                        objectName = theView.ViewName;
                        objectTypeText = "View";
                    }
                    break;
                default:
                    break;
            }

            if (!objectNotFound)
            {
                Label lblObjectName = e.Row.FindControl("lblObjectName") as Label;
                if (lblObjectName != null)
                {
                    lblObjectName.Text = objectName;
                }
                Label lblObjectType = e.Row.FindControl("lblObjectType") as Label;
                if (lblObjectType != null)
                {
                    lblObjectType.Text = objectTypeText;
                }
            }

            Label lblHeading = e.Row.FindControl("lblHeading") as Label;
            if (lblHeading != null)
            {
                /* === Red 23042019: Lets blank the Item Title if it's a Data sheet === */
                if (itemType != ReportItem.ReportItemType.ItemTypeTable)
                {
                    lblHeading.Text = DataBinder.Eval(e.Row.DataItem, "ItemTitle").ToString();
                }
                /* === end Red === */
                if (_strActionMode == "view")
                    lblHeading.Enabled = false;
            }
        }
    }

    protected void btnRefreshReportItem_Click(object sender, EventArgs e)
    {
        if (_iReportID.HasValue)
            PopulateReportItem(_iReportID.Value);
    }

    protected void ReportItemPager_BindTheGridAgain(object sender, EventArgs e)
    {
        if (_iReportID.HasValue)
            PopulateReportItem(_iReportID.Value);
    }
     
    protected void PopulateTheRecord()
    {
        try
        {
            lblReportName.Text = _theReport.ReportName;

            if ((_strActionMode == "edit") || (_strActionMode == "add"))
            {
                ViewState["theReport"] = _theReport;
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Report Table", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }

    }

    protected void EnableTheRecordControls(bool enabled)
    {        
    }

    private void SetPageButtons()
    {
        switch (_strActionMode)
        {
            case "add":
                hlBack.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                     Request.ApplicationPath +
                                     "Pages/ReportWriter/Report.aspx" +
                                     "?SearchCriteria=" + Request.QueryString["SearchCriteria"];
                divEdit.Visible = false;
                divDetails.Visible = true;
                hlDetails.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                        Request.ApplicationPath +
                                        "Pages/ReportWriter/ReportDetail.aspx" +
                                        "?ReportID=" + Request.QueryString["ReportID"] +
                                        "&mode=" + Cryptography.Encrypt("edit") +
                                        "&popup=1" +
                                        "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
                hlSchedule.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                         Request.ApplicationPath +
                                         "Pages/ReportWriter/ReportSchedule.aspx" +
                                         "?ReportID=" + Request.QueryString["ReportID"] +
                                         "&mode=" + Cryptography.Encrypt("edit") +
                                         "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
                break;
            case "view":
                hlBack.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                     Request.ApplicationPath +
                                     "Pages/ReportWriter/Report.aspx" +
                                     "?SearchCriteria=" + Request.QueryString["SearchCriteria"];
                divEdit.Visible = true;
                hlEditLink.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                         "Pages/ReportWriter/ReportTable.aspx" +
                                         "?mode=" + Cryptography.Encrypt("edit") +
                                         "&ReportID=" + Request.QueryString["ReportID"].ToString() +
                                         "&SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString();
                divDetails.Visible = true;
                hlDetails.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                        Request.ApplicationPath +
                                        "Pages/ReportWriter/ReportDetail.aspx" +
                                        "?ReportID=" + Request.QueryString["ReportID"] +
                                        "&mode=" + Cryptography.Encrypt("view") +
                                        "&popup=1" +
                                        "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
                hlSchedule.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                         Request.ApplicationPath +
                                         "Pages/ReportWriter/ReportSchedule.aspx" +
                                         "?ReportID=" + Request.QueryString["ReportID"] +
                                         "&mode=" + Cryptography.Encrypt("view") +
                                         "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
                break;
            case "edit":
                hlBack.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                     Request.ApplicationPath +
                                     "Pages/ReportWriter/Report.aspx" +
                                     "?SearchCriteria=" + Request.QueryString["SearchCriteria"];
                divEdit.Visible = false;
                divDetails.Visible = true;
                hlDetails.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                        Request.ApplicationPath +
                                        "Pages/ReportWriter/ReportDetail.aspx" +
                                        "?ReportID=" + Request.QueryString["ReportID"] +
                                        "&mode=" + Cryptography.Encrypt("edit") +
                                        "&popup=1" +
                                        "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
                hlSchedule.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority +
                                         Request.ApplicationPath +
                                         "Pages/ReportWriter/ReportSchedule.aspx" +
                                         "?ReportID=" + Request.QueryString["ReportID"] +
                                         "&mode=" + Cryptography.Encrypt("edit") +
                                         "&SearchCriteria=" + Request.QueryString["SearchCriteria"];
                break;
        }
        if (Request.QueryString["fixedbackurl"] != null)
            hlBack.NavigateUrl = Cryptography.Decrypt(Request.QueryString["fixedbackurl"].ToString());

        if (_iReportID.HasValue)
        {
            hlReportDate.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                       "Pages/ReportWriter/ReportDates.aspx" +
                                       "?ReportID=" + Cryptography.Encrypt(_iReportID.Value.ToString());
            hlReport.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                   "Pages/ReportWriter/ReportHandler.ashx" +
                                   "?ReportID=" + Cryptography.Encrypt(_iReportID.Value.ToString());

            SetAskReportDate();
        }
    }

    private void SetAskReportDate()
    {
        if (_iReportID.HasValue)
        {
            Report report = ReportManager.ets_Report_Detail(_iReportID.Value);
            if (report != null)
            {
                if ((report.ReportDates == Report.ReportInterval.IntervalDateRange) &&
                    (!report.ReportStartDate.HasValue || !report.ReportEndDate.HasValue))
                {
                    hfAskReportDate.Value = "1";
                }
                else
                {
                    hfAskReportDate.Value = "0";
                }
            }
        }
    }

    protected void lbDuplicateReport_OnClick(object sender, EventArgs e)
    {
        int sourceReportId = -1;
        if (_iReportID.HasValue && int.TryParse(txtSourceReportId.Text, out sourceReportId) && sourceReportId != -1)
        {
            if (sourceReportId == _iReportID.Value)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "message_alert",
                    "alert('Cannot copy sheets from itself.');", true);
            else
            {
                ReportManager.ets_Report_Clone(sourceReportId, _iReportID.Value);
                PopulateReportItem(_iReportID.Value);
            }
        }
    }

    protected void lbUpdateDetails_OnClick(object sender, EventArgs e)
    {
        PopulateTheRecord();
        SetAskReportDate();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "set_report_button_visibility",
            "setReportButtonVisibility();", true);
    }
}

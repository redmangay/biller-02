﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master"
 AutoEventWireup="true" CodeFile="ReportTableItemDetail.aspx.cs" Inherits="Pages_ReportWriter_ReportTableItemDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" Runat="Server">
     
     <%-- <asp:UpdateProgress class="ajax-indicator-full" ID="UpdateProgress3" runat="server" AssociatedUpdatePanelID="upCommon">
        <ProgressTemplate>
            <table style="width:100%;  height:100%; text-align: center;" >
                <tr valign="middle">
                    <td>
                        <p style="font-weight:bold;"> Please wait...</p>
                        <asp:Image ID="ImageProcessing" runat="server" AlternateText="Processing..." ImageUrl="~/Images/ajax.gif" />
                    </td>
                </tr>
            </table>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    
    <br /> 
    <div style="width:100%;">
        <asp:UpdatePanel ID="upCommon" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div>
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="2">
                                <asp:Label runat="server" ID="lblDetailTitle" Font-Size="16px"
                                    Font-Bold="true" Text="Field Selector">
                                </asp:Label>
                            </td>
                            <td colspan="3" style="width: 50%;">
                                <div runat="server" id="divSave" style="float: right; width: 36px;">
                                    <asp:LinkButton runat="server" ID="lnkSaveNew" CausesValidation="true" OnClick="lnkSaveNew_Click">
                                        <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                   ToolTip="Save" />
                                    </asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 1em;">&nbsp;</td>
                            <td align="left" valign="top">
                                <strong>Not Selected:</strong><br />
                                <asp:ListBox runat="server" ID="lstNotUsed" style="width:250px;" Height="280px"
                                    SelectionMode="Multiple"></asp:ListBox>
                            </td>
                            <td align="center" style="width: 80px;">
                                <br />
                                <asp:LinkButton runat="server" ID="lnkAdd" OnClick="lnkAdd_Click">
                                    <asp:Image runat="server" ID="imgAdd" ImageUrl="~/App_Themes/Default/Images/arrow_right.png"/>
                                </asp:LinkButton>
                                <br />
                                <br />   
                                <asp:LinkButton runat="server" ID="lnkRemove" OnClick="lnkRemove_Click" >
                                    <asp:Image runat="server" ID="imgRemove" ImageUrl="~/App_Themes/Default/Images/arrow_left.png"/>
                                </asp:LinkButton>
                            </td>
                            <td align="left" valign="top">
                                <strong>Selected:</strong><br />
                                <asp:ListBox runat="server" ID="lstUsed" style="width:250px;" Height="280px"
                                    SelectionMode="Multiple"></asp:ListBox>
                            </td>
                            <td style="width: 1em;">&nbsp;</td>
                        </tr>
                    </table>
                </div>
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        function CloseAndRefresh() {
            window.parent.document.getElementById('btnRefreshReportTableColumn').click();
            parent.$.fancybox.close();
            // alert('ok');
        }
    </script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="ReportSchedule.aspx.cs" Inherits="Pages_ReportWriter_ReportSchedule" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>
<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
    <asp:Literal ID="ltTextStyles" runat="server"></asp:Literal>
    <asp:Literal ID="ltCommonStyles" runat="server"></asp:Literal>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <script type="text/javascript">
        $(document).ready(function() {
            InitElements();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            InitElements();
        });

        function InitElements() {
            $('.popuplink10').fancybox({
                toolbar: false,
                smallBtn: true,
                'titleShow': false
            });

            $('.popuplinkRR').fancybox({
                iframe: {
                    css: {
                        width: '520px',
                        height: '250px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                width: 520,
                height: 250,
                titleShow: false
            });
        }

        function isNumeric(value) {
            return /^\d+$/.test(value);
        }

        function validateTime(source, arg) {
            if (arg.Value.indexOf(":") === -1) {
                arg.IsValid = false;
            } else {
                var parts = arg.Value.split(":");
                if (parts.length !== 2) {
                    arg.IsValid = false;
                } else {
                    if (parts[0].length === 0 || !isNumeric(parts[0]) ||
                        parts[1].length === 0 || !isNumeric(parts[1])) {
                        arg.IsValid = false;
                    } else {
                        var h = 0;
                        var m = 0;
                        h = parseInt(parts[0]);
                        m = parseInt(parts[1]);
                        if (h < 0 || h > 23 || m < 0 || m > 59) {
                            arg.IsValid = false;
                        } else {
                            arg.IsValid = true;
                        }
                    }
                }
            }
        }

        function validate() {
            var isValid = Page_ClientValidate("MKE"); //parameter is the validation group
            if (!isValid)
                Page_BlockSubmit = false;
            return isValid;
        }

        function Close() {
            parent.$.fancybox.close();
        }

        function SavedAndRefresh() {
            var x = $("[id*='lbUpdateDetails']", parent.document); // jQuery click() method will not cause postback on parent page!
            if (x)
                parent.document.getElementById(x.attr("id")).click();
            parent.$.fancybox.close();
        }


        function checkAll(objRef) {
            var gridView = objRef.parentNode.parentNode.parentNode;
            var inputList = gridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type === "checkbox" && objRef !== inputList[i]) {
                    if (objRef.checked) {
                        //If the header checkbox is checked
                        //check all checkboxes                       
                        inputList[i].checked = true;
                    }
                    else {
                        //If the header checkbox is checked
                        //uncheck all checkboxes                        
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function SelectAllCheckboxes(spanChk) {
            checkAll(spanChk);
            // Added as ASPX uses SPAN for checkbox
            var oItem = spanChk.children;
            var theBox = (spanChk.type === "checkbox") ? spanChk : spanChk.children.item[0];
            var xState = theBox.checked;
            var elm = theBox.form.elements;
            for (var i = 0; i < elm.length; i++) {
                if (elm[i].type === "checkbox" && elm[i].id !== theBox.id) {
                    if (elm[i].checked !== xState)
                        elm[i].click();
                }
            }
        }
    </script>

    <table border="0" cellpadding="0" cellspacing="0" >
        <tr>
            <td height="40" style="width: 50%; text-align: left;">
                <span class="TopTitle">
                    <asp:Label runat="server" ID="lblTitle">Schedule Report</asp:Label>
                </span>
            </td>
            <td>
                <div style="width: 100%;">
                    <div style="float: right; width: 36px;" runat="server" id="divSave">
                        <asp:LinkButton runat="server" ID="lnkSave" OnClick="lnkSave_Click" CausesValidation="false"
                                        OnClientClick="return validate();" >
                            <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                       ToolTip="Save" />
                        </asp:LinkButton>
                    </div>
                    <div style="float: right; width: 36px;" runat="server" id="divEdit" visible="false">
                        <asp:HyperLink runat="server" ID="hlEditLink">
                            <asp:Image runat="server" ID="Image2"  ImageUrl="~/App_Themes/Default/images/Edit_big.png"
                                        ToolTip="Edit" />
                        </asp:HyperLink>
                    </div>
                    <div style="float: right; width: 36px;" runat="server" id="divBack">
                        <asp:LinkButton runat="server" ID="lbBack" OnClick="lbBack_OnClick" CausesValidation="False">
                            <asp:Image runat="server" ID="imgBack" ImageUrl="~/App_Themes/Default/images/Back.png"
                                       ToolTip="Back" />
                        </asp:LinkButton>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label runat="server" ID="lblReportName" CssClass="TopSubtitle"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" height="13">
            </td>
        </tr>
        <tr>
            <td valign="top" align="left" colspan="2">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div id="search" style="padding-bottom: 10px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true" ValidationGroup="MKE"
                                ShowMessageBox="true" ShowSummary="false" HeaderText="Please correct the following errors:" />
                        </div>
                        <asp:Panel ID="Panel2" runat="server" DefaultButton="lnkSave">
                            <div runat="server" id="divDetail">
                                <table>
                                    <tr>
                                        <td valign="top" style="padding-top:7px;" align="left" >
                                            <table cellpadding="3">
                                                <tr runat="server" id="trFrequency">
                                                    <td style="text-align: right; width: 10em;">
                                                        <strong>Frequency</strong>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList runat="server" ID="ddlFrequency" CssClass="NormalTextBox"
                                                                          AutoPostBack="True" OnSelectedIndexChanged="ddlFrequency_OnSelectedIndexChanged">
                                                            <asp:ListItem Value="OneOff" Text="One off"/>
                                                            <asp:ListItem Value="Weekly" Text="Weekly"/>
                                                            <asp:ListItem Value="Monthly" Text="Monthly"/>
                                                            <asp:ListItem Value="Undefined" Text="Not scheduled"/>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trDate">
                                                    <td style="text-align: right; width: 10em;">
                                                        <asp:Label Font-Bold="True" runat="server" ID="lblDate">Date</asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDate" runat="server" CssClass="NormalTextBox" 
                                                                     Width="100px"  BorderStyle="Solid"
                                                                     BorderColor="#909090" BorderWidth="1" AutoPostBack="True" OnTextChanged="OnScheduleChanged" />
                                                        <asp:ImageButton runat="server" ID="ibDate" ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false"/>  

                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDate"
                                                                                      Format="dd/MM/yyyy" PopupButtonID="ibDate" FirstDayOfWeek="Monday">
                                                        </ajaxToolkit:CalendarExtender>
                                                           
                                                        <asp:RangeValidator ID="RangeValidatorDate" runat="server" ControlToValidate="txtDate"
                                                                            ValidationGroup="MKE" ErrorMessage="Invalid date" Font-Bold="true" Display="Dynamic" Type="Date"
                                                                            MinimumValue="1/1/1753" MaximumValue="1/1/3000">*</asp:RangeValidator> 
                                                        <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                                                                                    CssClass="failureNotification" ErrorMessage="Date is required." ToolTip="Date is required."
                                                                                    ValidationGroup="MKE">*</asp:RequiredFieldValidator>
                                                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderDate" TargetControlID="txtDate" WatermarkText="dd/mm/yyyy"
                                                                                              runat="server" WatermarkCssClass="MaskText"></ajaxToolkit:TextBoxWatermarkExtender>
                                                        
                                                        <asp:Label Font-Bold="True" runat="server" ID="lblTime">Time</asp:Label>
                                                        <asp:TextBox ID="txtTime" runat="server" CssClass="NormalTextBox" 
                                                                     Width="60px"  BorderStyle="Solid"
                                                                     BorderColor="#909090" BorderWidth="1" AutoPostBack="True" OnTextChanged="OnScheduleChanged"></asp:TextBox>
                                                        <asp:CustomValidator ID="CustomValidatorTime" runat="server" ControlToValidate="txtTime"
                                                                             ValidationGroup="MKE" ErrorMessage="Invalid time" Font-Bold="true" Display="Dynamic"
                                                                             ClientValidationFunction="validateTime">*</asp:CustomValidator>
                                                        <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtenderTime" TargetControlID="txtTime" WatermarkText="hh:mm"
                                                                                              runat="server" WatermarkCssClass="MaskText"></ajaxToolkit:TextBoxWatermarkExtender>
                                                    </td>
                                                </tr>   
                                                <tr runat="server" id="trRepeatEvery">
                                                    <td style="text-align: right; width: 10em;">
                                                        <strong>Repeat every</strong>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList runat="server" ID="ddlRepeatEvery" CssClass="NormalTextBox"
                                                                          AutoPostBack="True" OnSelectedIndexChanged="OnScheduleChanged">
                                                            <asp:ListItem Value="1" Text="1" />
                                                            <asp:ListItem Value="2" Text="2" />
                                                            <asp:ListItem Value="3" Text="3" />
                                                            <asp:ListItem Value="4" Text="4" />
                                                            <asp:ListItem Value="5" Text="5" />
                                                            <asp:ListItem Value="6" Text="6" />
                                                            <asp:ListItem Value="7" Text="7" />
                                                            <asp:ListItem Value="8" Text="8" />
                                                            <asp:ListItem Value="9" Text="9" />
                                                            <asp:ListItem Value="10" Text="10" />
                                                            <asp:ListItem Value="11" Text="11" />
                                                            <asp:ListItem Value="12" Text="12" />
                                                        </asp:DropDownList>
                                                        <asp:Label runat="server" ID="lblRepeatInterval">weeks</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trRepeatOnWeeek">
                                                    <td style="text-align: right; width: 10em;">
                                                        <strong>Repeat on</strong>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox runat="server" ID="cbMonday" Text="M" AutoPostBack="True" OnCheckedChanged="OnScheduleChanged"/>
                                                        <asp:CheckBox runat="server" ID="cbTuesday" Text="T" AutoPostBack="True" OnCheckedChanged="OnScheduleChanged"/>
                                                        <asp:CheckBox runat="server" ID="cbWednesday" Text="W" AutoPostBack="True" OnCheckedChanged="OnScheduleChanged"/>
                                                        <asp:CheckBox runat="server" ID="cbThursday" Text="T" AutoPostBack="True" OnCheckedChanged="OnScheduleChanged"/>
                                                        <asp:CheckBox runat="server" ID="cbFriday" Text="F" AutoPostBack="True" OnCheckedChanged="OnScheduleChanged"/>
                                                        <asp:CheckBox runat="server" ID="cbSaturday" Text="S" AutoPostBack="True" OnCheckedChanged="OnScheduleChanged"/>
                                                        <asp:CheckBox runat="server" ID="cbSunday" Text="S" AutoPostBack="True" OnCheckedChanged="OnScheduleChanged"/>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trRepeatOnMonth">
                                                    <td style="text-align: right; width: 10em;">
                                                        <strong>Repeat on</strong>
                                                    </td>
                                                    <td>
                                                        <asp:RadioButtonList runat="server" ID="rblRepeatOnMonth" RepeatDirection="Horizontal"
                                                                             AutoPostBack="True" OnSelectedIndexChanged="OnScheduleChanged">
                                                            <asp:ListItem Value="DayOfMonth" Text="day of the month" Selected="True"/>
                                                            <asp:ListItem Value="DayOfWeek" Text="day of the week"/>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="tr1">
                                                    <td style="text-align: right; width: 10em; vertical-align: top;">
                                                        <strong runat="server" id="stgDescription">Summary</strong>
                                                    </td>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblSummary"></asp:Label>
                                                        <asp:HyperLink runat="server" NavigateUrl="#next10Runs" CssClass="popuplink10" ID="hlScheduledDates"
                                                                       ImageUrl="~/Images/Time_Trigger_Icon_16.png" ToolTip="Scheduled dates"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 5px;">
                                        </td>
                                    </tr>
                                    <tr><td colspan="2">&nbsp;</td></tr>
                                    <tr>
                                        <td>
                                            <span style="font-weight: bold;">Send Report To</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <dbg:dbgGridView ID="grdReportRecipient" AllowPaging="True" runat="server" AutoGenerateColumns="false"
                                                             DataKeyNames="ReportRecipientID" HeaderStyle-HorizontalAlign="Center" PageSize="200"
                                                             RowStyle-HorizontalAlign="Center" CssClass="gridview" OnRowCommand="grdRepotRecipient_RowCommand"
                                                             OnRowDataBound="grdReportRecipient_RowDataBound" AlternatingRowStyle-BackColor="#DCF2F0"
                                                             Width="500px">
                                                <PagerSettings Position="Top" />
                                                <RowStyle CssClass="gridview_row" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                        <HeaderTemplate>
                                                            <input id="chkAll" onclick="javascript: checkAll(this);" runat="server"
                                                                   type="checkbox" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkDelete" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="false">
                                                        <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="LblID" runat="server" Text='<%# Eval("ReportRecipientID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Email">
                                                        <ItemTemplate>
                                                            <div style="padding-left: 10px;">
                                                                <asp:Label runat="server" ID="lblEmail" Text='<%# Eval("Email") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="gridview_header" />
                                                <PagerTemplate>
                                                    <asp:GridViewPager runat="server" ID="ReportRecipientPager" OnDeleteAction="ReportRecipientPager_DeleteAction"
                                                                       DelConfirmation="Are you sure you want to remove selected recipient(s)?" 
                                                                       HideFilter="true" HideRefresh="true" HideExport="true"
                                                                       HideGo="true" HideNavigation="true" HidePageSize="true" HidePageSizeButton="true"
                                                                       OnBindTheGridAgain="ReportRecipientPager_BindTheGridAgain" />
                                                </PagerTemplate>    
                                            </dbg:dbgGridView>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <br />
                            <div runat="server" id="divEmptyAddReportRecipient" visible="false" style="padding-left: 20px;">
                                <asp:HyperLink runat="server" ID="hlAddReportRecipient2" Style="text-decoration: none; color: Black;"
                                               ToolTip="New Recipient" CssClass="popuplinkRR">
                                    <asp:Image runat="server" ID="Image1" ImageUrl="~/App_Themes/Default/images/add32.png" />
                                </asp:HyperLink>                             
                                &nbsp;No reipients have been added yet.&nbsp;
                                <asp:HyperLink runat="server" ID="hlAddReportRecipient" Style="text-decoration: none; color: Black;"
                                               CssClass="popuplinkRR">
                                    <strong style="text-decoration: underline; color: Blue;">
                                        Add new recipient now.
                                    </strong>
                                </asp:HyperLink>
                            </div>
                            <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                            <br />
                            <div style="display: none; visibility: hidden">
                                <asp:Button runat="server" ID="btnRefreshReportRecipient" ClientIDMode="Static"
                                            Style="display: none;" OnClick="btnRefreshReportRecipient_Click" />
                            </div>
                        </asp:Panel>
                        
                        <div style="display: none">
                            <div id="next10Runs" style="width:300px; height:230px; overflow:auto;" class="ContentMain">
                                <div style="font-weight: bold; margin-bottom: 10px;">Scheduled dates (first 10):</div>
                                <asp:Repeater runat="server" ID="repeater10Runs">
                                    <ItemTemplate>
                                        <div style="margin: 5px 0;"><%# Container.DataItem.ToString() %></div>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <div style="margin: 5px 0; background-color: lightgray;"><%# Container.DataItem.ToString() %></div>
                                    </AlternatingItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_ReportWriter_AddReportRecipient : System.Web.UI.Page
{
    int? _iReportId = null;
    Report _report = null;


    protected void Page_Load(object sender, EventArgs e)
    {
        _iReportId = int.Parse(Cryptography.Decrypt(Request.QueryString["ReportID"].ToString()));
        _report = ReportManager.ets_Report_Detail((int)_iReportId);

        if (!IsPostBack)
        {
            PopulateList();
            PopulateTerminology();
        }
    }

    protected void PopulateTerminology()
    {
    }

    private void PopulateList()
    {
        int iTotalRowsNum = 0;
        System.Data.DataTable users = SecurityManager.User_Select(null, null, null, null, null //, null //red Ticket 3059_2
            , true, null, null,
            int.Parse(Session["AccountID"].ToString()), "Email", "ASC", null,
            null, ref iTotalRowsNum);
        string s = "";
        foreach (DataRow user in users.Rows)
            s += user["Email"] + ";";
        s = s.Substring(0, s.Length - 1);
        lblEmailList.Text = s;
    }

    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        if (_iReportId.HasValue)
        {
            try
            {
                User user = (User)Session["User"];
                ReportRecipient reportRecipient = new ReportRecipient(_iReportId.Value, 0, txtEmail.Text);
                int reportRecipientId = ReportManager.ets_ReportRecipient_Insert(reportRecipient);
            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Add Report Recipient", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }
        }
        else
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Add Report Recipient", "Report ID is null", "", DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "RefreshGrid", "CloseAndRefresh();", true);
    }
}
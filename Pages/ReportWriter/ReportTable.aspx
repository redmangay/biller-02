﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true"
    CodeFile="ReportTable.aspx.cs" Inherits="Pages_ReportWriter_ReportTable" %>

<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>
<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <style type="text/css">
        .sortHandleVT
        {
            cursor: move;
        }
        
        .cssplaceholder
        {
            border-top: 2px solid #00FFFF;
            border-bottom: 2px solid #00FFFF;
        }
    </style>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            initElements();
            clickOnLoad();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initElements();
        });

        function initElements() {
            $('#divReportItemSingleInstance').sortable({
                items: '.gridview_row',
                cursor: 'crosshair',
                helper: fixHelper,
                cursorAt: { left: 10, top: 10 },
                connectWith: '#divReportItemSingleInstance',
                handle: '.sortHandleVT',
                axis: 'y',
                distance: 15,
                dropOnEmpty: true,
                receive: function(e, ui) {
                    $(this).find('tbody').append(ui.item);
                },
                start: function(e, ui) {
                    ui.placeholder.css('border-top', '2px solid #00FFFF');
                    ui.placeholder.css('border-bottom', '2px solid #00FFFF');
                },
                update: function(event, ui) {
                    var TC = '';
                    $('.ReportItemID').each(function() {
                        TC = TC + this.value.toString() + ',';
                    });
                    //alert(TC);
                    document.getElementById('hfReportItemIDForItemPosition').value = TC;
                    $('#btnReportItemIDForItemPosition').trigger('click');
                }
            });

            $('.popuplinkRI').fancybox({
                iframe: {
                    css: {
                        width: '420px',
                        height: '220px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                titleShow: false
            });

            $('.popuplinkDetails').fancybox({
                iframe: {
                    css: {
                        width: '680px',
                        height: '320px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                titleShow: false
            });

            $('.popuplinkCopy').fancybox({
                iframe: {
                    css: {
                        width: '420px',
                        height: '140px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                titleShow: false
            });

            $('.popuplinkRD').fancybox({
                iframe: {
                    css: {
                        width: '400px',
                        height: '200px'
                    }
                },
                toolbar: false,
                smallBtn: true,
                scrolling: 'auto',
                type: 'iframe',
                titleShow: false
            });

            setReportButtonVisibility();
        }

        function setReportButtonVisibility() {
            if ($("#hfAskReportDate").val() === "1") {
                $("#divReportDate").css("visibility", "visible").css("display", "block");
                $("#divReport").css("visibility", "hidden").css("display", "none");
            } else {
                $("#divReport").css("visibility", "visible").css("display", "block");
                $("#divReportDate").css("visibility", "hidden").css("display", "none");
            }
        }

        function clickOnLoad() {
            var x = $("#txtClickOnLoad").val();
            if (x !== "")
                $("#" + x).trigger("click");
        }

        var fixHelper = function (e, ui) {
            ui.children().each(function () {
                $(this).width($(this).width());
            });

            return ui;
        };

        function checkAll(objRef) {
            var gridView = objRef.parentNode.parentNode.parentNode;
            var inputList = gridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type === "checkbox" && objRef !== inputList[i]) {
                    if (objRef.checked) {
                        //If the header checkbox is checked
                        //check all checkboxes                       
                        inputList[i].checked = true;
                    }
                    else {
                        //If the header checkbox is checked
                        //uncheck all checkboxes                        
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function SelectAllCheckboxes(spanChk) {
            checkAll(spanChk);
            // Added as ASPX uses SPAN for checkbox
            var oItem = spanChk.children;
            var theBox = (spanChk.type === "checkbox") ? spanChk : spanChk.children.item[0];
            var xState = theBox.checked;
            var elm = theBox.form.elements;
            for (var i = 0; i < elm.length; i++) {
                if (elm[i].type === "checkbox" && elm[i].id !== theBox.id) {
                    if (elm[i].checked !== xState)
                        elm[i].click();
                }
            }
        }
    </script>
    
 
    <table border="0" cellpadding="0" cellspacing="0"  align="center">
        <tr>
            <td colspan="3" align="center">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <span class="TopTitle" style="padding-left:20px;/*RP Added Ticket 4285*/"><asp:Label runat="server" ID="lblTitle"></asp:Label></span>
                        </td>
                        <td align="left">
                            <div style="width: 100%;">
                                <%-- RP MOdified Ticket 4267 Set button sizes from 36px to 55px --%>
                                <div style="/*RP Modified Ticket 4285*/float: /*right*/ left; width: 55px; margin-top: 3px;" runat="server" id="divCopy" visible="true">
                                    <asp:HyperLink runat="server" ID="hlCopyLink" NavigateUrl="ReportList.aspx" CssClass="popuplinkCopy">
                                        <asp:Image runat="server" ID="Image3"  ImageUrl="~/App_Themes/Default/images/CopyItem.png"
                                                   ToolTip="Duplicate Sheets" />
                                    </asp:HyperLink>
                                </div>
                                <div style="/*RP Modified Ticket 4285*/float: /*right*/ left; width: 55px; margin-top: 3px;" runat="server" id="divEdit" visible="false">
                                    <asp:HyperLink runat="server" ID="hlEditLink">
                                        <asp:Image runat="server" ID="Image2" ImageUrl="~/App_Themes/Default/images/Edit_big.png"
                                                   ToolTip="Edit" />
                                    </asp:HyperLink>
                                </div>
                                <div id="divReport" style="/*RP Modified Ticket 4285*/float: /*right*/ left; width: 55px; margin-top: 3px; display: block; visibility: visible;">
                                    <asp:HyperLink runat="server" ID="hlReport">
                                        <asp:Image runat="server" ID="imgReport" ImageUrl="~/App_Themes/Default/images/export_excel.png"
                                                   ToolTip="Export to Excel" />
                                    </asp:HyperLink>
                                </div>
                                <div id="divReportDate" style="/*RP Modified Ticket 4285*/float: /*right*/ left; width: 55px; margin-top: 3px; display: block; visibility: visible;">
                                    <asp:HyperLink runat="server" ID="hlReportDate" CssClass="popuplinkRD">
                                        <asp:Image runat="server" ID="imgReport2" ImageUrl="~/App_Themes/Default/images/export_excel.png"
                                                   ToolTip="Export to Excel" />
                                    </asp:HyperLink>
                                </div>
                                <div style="/*RP Modified Ticket 4285*/float: /*right*/ left; width: 55px; margin-top: 3px;">
                                    <asp:HyperLink runat="server" ID="hlSchedule" CssClass="popuplinkSchedule">
                                        <asp:Image runat="server" ID="imgSchedule" ImageUrl="~/App_Themes/Default/images/clock_select_remain.png"
                                                   ToolTip="Schedule" />
                                    </asp:HyperLink>
                                </div>
                                <div style="/*RP Modified Ticket 4285*/float: /*right*/ left; width: 55px;" runat="server" id="divDetails" visible="true">
                                    <asp:HyperLink runat="server" ID="hlDetails" CssClass="popuplinkDetails">
                                        <asp:Image runat="server" ID="imdDetails" ImageUrl="~/App_Themes/Default/images/Config.png"
                                                   ToolTip="Properties" />
                                    </asp:HyperLink>
                                </div>
                                <div style="/*RP Modified Ticket 4285*/float: /*right*/ left; width: 55px;">
                                    <asp:HyperLink runat="server" ID="hlBack">
                                        <asp:Image runat="server" ID="imgBack" ImageUrl="~/App_Themes/Default/images/Back.png"
                                                   ToolTip="Back" />
                                    </asp:HyperLink>
                                </div>
                                <%-- End Modification --%>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label runat="server" ID="lblReportName" CssClass="TopSubtitle" Style="padding:20px/*RP Added Ticket 4285*/"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="13">
            </td>
        </tr>
        <tr>
            <td valign="top">
                &nbsp;
            </td>
            <td valign="top">
                <div id="search" style="padding-bottom: 10px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                        ShowMessageBox="true" ShowSummary="false" HeaderText="Please correct the following errors:" />
                </div>
                <asp:Panel ID="Panel2" runat="server">
                    <div runat="server" id="divDetail">

                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <br />
                                <div style="font-size:large; margin-left:25px; margin-bottom:0px;">Report Items:</div>
                                <div style="padding-left: 20px; padding-top: 10px;" id="divReportItemSingleInstance">
                                    <table>
                                        <tr>
                                            <td align="left">
                                                <asp:HiddenField runat="server" ID="hfAskReportDate" ClientIDMode="Static" />
                                                <asp:HiddenField runat="server" ID="hfReportItemIDForItemPosition" ClientIDMode="Static" />
                                                <asp:Button runat="server" ID="btnRefreshReportItem" ClientIDMode="Static"
                                                            Style="display: none;" OnClick="btnRefreshReportItem_Click" />
                                                <asp:Button runat="server" ID="btnReportItemIDForItemPosition" ClientIDMode="Static"
                                                            Style="display: none;" OnClick="btnReportItemIDForItemPosition_Click" />
                                                <%--RP MODIFIED Ticket 4267 Removed Datagrid Width  --%>
                                                <dbg:dbgGridView ID="grdReportItem" AllowPaging="True" runat="server" AutoGenerateColumns="false"
                                                                    DataKeyNames="ReportItemID" HeaderStyle-HorizontalAlign="Center" PageSize="200"
                                                                    RowStyle-HorizontalAlign="Center" CssClass="gridview" OnRowCommand="grdReportItem_RowCommand"
                                                                    OnRowDataBound="grdReportItem_RowDataBound" AlternatingRowStyle-BackColor="#DCF2F0">
                                                    <PagerSettings Position="Top" />
                                                    <RowStyle CssClass="gridview_row" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                            <HeaderTemplate>
                                                                <input id="chkAll" onclick="javascript: checkAll(this);" runat="server"
                                                                    type="checkbox" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkDelete" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField Visible="false">
                                                            <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="LblID" runat="server" Text='<%# Eval("ReportItemID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="EditHyperLink" runat="server" ToolTip="Edit"
                                                                                NavigateUrl='<%# GetEditURL(Eval("ReportItemID").ToString()) %>'
                                                                                ImageUrl="~/App_Themes/Default/Images/iconEdit.png" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-CssClass="sortHandleVT">
                                                            <ItemStyle HorizontalAlign="Center" Width="10px" />
                                                            <ItemTemplate>
                                                                <asp:Image ID="Image2" runat="server" ImageUrl="~/App_Themes/Default/Images/MoveIcon.png"
                                                                            ToolTip="Drag and drop to change order" />
                                                                <input type="hidden" id='hfReportItemID' value='<%# Eval("ReportItemID") %>'
                                                                        class='ReportItemID' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Item Name">
                                                            <ItemTemplate>
                                                                <div style="padding-left: 10px;">
                                                                    <asp:Label runat="server" ID="lblObjectName"></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Item Type">
                                                            <ItemTemplate>
                                                                <div style="padding-left: 10px;">
                                                                    <asp:Label runat="server" ID="lblObjectType"></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Item Title">
                                                            <ItemTemplate>
                                                                <div style="padding-left: 10px;">
                                                                    <asp:Label runat="server" ID="lblHeading"></asp:Label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Position" Visible="false">
                                                            <ItemTemplate>
                                                                <div style="padding-left: 10px;">
                                                                    <asp:TextBox runat="server" ID="txtItemPosition" CssClass="NormalTextBox" Width="50px"></asp:TextBox>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="gridview_header" />
                                                    <PagerTemplate>
                                                        <asp:GridViewPager runat="server" ID="ReportItemPager" OnDeleteAction="ReportItemPager_DeleteAction"
                                                                           DelConfirmation="Are you sure you want to remove selected report item(s)?" 
                                                                           ShowSaveChanges="False" HideFilter="true" HideRefresh="true" HideExport="true"
                                                                           HideGo="true" HideNavigation="true" HidePageSize="true" HidePageSizeButton="true"
                                                                           OnBindTheGridAgain="ReportItemPager_BindTheGridAgain" />
                                                    </PagerTemplate>    
                                                </dbg:dbgGridView>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <br />
                                <div runat="server" id="divEmptyAddReportItem" visible="false" style="padding-left: 20px;">
                                    <asp:HyperLink runat="server" ID="hlAddReportItem2" Style="text-decoration: none; color: Black;"
                                                    ToolTip="New Report" CssClass="popuplinkRI">
                                        <asp:Image runat="server" ID="Image1" ImageUrl="~/App_Themes/Default/images/add32.png" />
                                    </asp:HyperLink>                             
                                    &nbsp;No items have been added yet.&nbsp;
                                    <asp:HyperLink runat="server" ID="hlAddReportItem" Style="text-decoration: none; color: Black;"
                                                    CssClass="popuplinkRI">
                                        <strong style="text-decoration: underline; color: Blue;">
                                            Add new item now.
                                        </strong>
                                    </asp:HyperLink>
                                </div>
                                <br />
                                <div style="display: none; visibility: hidden">
                                    <asp:LinkButton runat="server" ID="lbUpdateDetails" OnClick="lbUpdateDetails_OnClick"></asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="lbDuplicateReport" OnClick="lbDuplicateReport_OnClick"></asp:LinkButton>
                                    <asp:TextBox runat="server" ID="txtSourceReportId"></asp:TextBox>
                                    <asp:TextBox runat="server" ID="txtClickOnLoad" ClientIDMode="Static"></asp:TextBox>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                    <br />
                    <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                    <br />
                    <div style="display: none; visibility: hidden">
                        <asp:HyperLink ID="hlOpenReportHidden" runat="server" ClientIDMode="Static"
                                       NavigateUrl="ReportHandler.ashx" Text="Open Report" />
                    </div>
                </asp:Panel>
            </td>
            <td valign="top">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3" height="13">
            </td>
        </tr>
    </table>
     

    <script type="text/javascript">
        function OpenReportItem() {
            $("#hlAddReportItem").trigger('click');
        }
    </script>
</asp:Content>

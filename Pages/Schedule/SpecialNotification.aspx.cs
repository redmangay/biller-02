﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class Pages_Schedule_SpecialNotification : SecurePage
{

    string _strActionMode = "view";
    int? _iDataReminderID;
    string _qsMode = "";
    string _qsDataReminderID = "-1";
    int _iColumnID = -1;
    User _ObjUser;


    protected void Page_Load(object sender, EventArgs e)
    {

        _ObjUser = (User)Session["User"];

        //Red Ticket 2013 WYSIWYG 
        InnovaStudio.ISTab tabBasic = default(InnovaStudio.ISTab);
        InnovaStudio.ISTab tabAdvanced = default(InnovaStudio.ISTab);

        InnovaStudio.ISGroup grpEdit1 = new InnovaStudio.ISGroup("grpEdit1", "", new string[] {
                            "RemoveFormat",
                             "Undo",
                             "Redo",
                             "Paragraph",
                             "FontName",
                             "FontSize",


            });
        InnovaStudio.ISGroup grpEdit2 = new InnovaStudio.ISGroup("grpEdit2", "", new string[] {
                            "JustifyLeft",
                             "JustifyCenter",
                             "JustifyRight",
                             "JustifyFull",
                            "Bold",
                            "Italic",
                            "Underline",
                            "Hyperlink",
                            "FullScreen"
            });
        InnovaStudio.ISGroup grpEdit3 = new InnovaStudio.ISGroup("grpEdit3", "", new string[] {
                "ForeColor",
                "BackColor"
            });
        InnovaStudio.ISGroup grpEdit4 = new InnovaStudio.ISGroup("grpEdit4", "", new string[] {
                "Image",
                "Media",
                "Table"
            });
        InnovaStudio.ISGroup grpEdit5 = new InnovaStudio.ISGroup("grpEdit5", "", new string[] {

                "Characters",
                "Line",
                "Bullets",
                "Indent",
                "Outdent",
                "StyleAndFormatting",
                "Search",
                "XHTMLSource"
                });

        tabBasic = new InnovaStudio.ISTab("tabHome", "Basic");
        tabAdvanced = new InnovaStudio.ISTab("tabStyle", "Advanced");

        tabBasic.Groups.AddRange(new InnovaStudio.ISGroup[] {
                        grpEdit1,
                        grpEdit2

                });
        tabAdvanced.Groups.AddRange(new InnovaStudio.ISGroup[] {
                        grpEdit3,
                        grpEdit4,
                        grpEdit5
                });

        edtContent.ToolbarTabs.Add(tabBasic);
        edtContent.ToolbarTabs.Add(tabAdvanced);

        
        if (Request.QueryString["mode"] == null)
        {
            Server.Transfer("~/Default.aspx");
        }
        else
        {
            _qsMode = Cryptography.Decrypt(Request.QueryString["mode"]);

            if (_qsMode == "add" ||
                _qsMode == "view" ||
                _qsMode == "edit")
            {
                _strActionMode = _qsMode;
            }
            else
            {
                Server.Transfer("~/Default.aspx");
            }


        }

        string strTitle = "Reminder Detail";
        


        // checking permission


        switch (_strActionMode.ToLower())
        {
            case "add":
                strTitle = "Add - Special Notification";
                if (!IsPostBack)
                {
                    PopulateDatabaseField();
                }

                break;
                         

            case "edit":

                strTitle = "Edit - Special Notification";
                if (!IsPostBack)
                {
                    PopulateNotification();
                    PopulateDatabaseField();
                }
                break;


            default:
                //?

                break;
        }


        Title = strTitle;
        lblTitle.Text = strTitle;

    }

   
  
    protected void PopulateNotification()
    {

        if (Request.QueryString["SpecialNotificationID"] != null)
        {
            int intNotificationID = int.Parse(Cryptography.Decrypt(Request.QueryString["SpecialNotificationID"].ToString()));
            SpecialNotification theSpecialNotification = null;

            theSpecialNotification = ReminderManager.SpecialNotification_Detail(intNotificationID);

            edtContent.Text = theSpecialNotification.strNotificationContent;
            txtNotificationHeader.Text = theSpecialNotification.strNotificationHeader;

        }

    }


    protected void lnkSave_Click(object sender, EventArgs e)
    {

        switch (_strActionMode.ToLower())
        {
            case "add":

                int iTableID = -1;
                if (Request.QueryString["TableID"] != null)
                {
                    iTableID = int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
                }


                SpecialNotification newNotification = new SpecialNotification(null, iTableID, txtNotificationHeader.Text, edtContent.Text,"");

                int newSpecialNotificationID = ReminderManager.SpecialNotification_Insert(newNotification);

                break;
            case "edit":

                if (Request.QueryString["SpecialNotificationID"] != null)
                {
                    int intNotificationID = int.Parse(Cryptography.Decrypt(Request.QueryString["SpecialNotificationID"].ToString()));
                    SpecialNotification editNotification = null;

                    editNotification = ReminderManager.SpecialNotification_Detail(intNotificationID);

                    editNotification.NotificationContent = edtContent.Text;
                    editNotification.NotificationHeader = txtNotificationHeader.Text;
                    //if (editNotification.Frequency != "once")
                    //{
                    //    editNotification.LastDateRun = DateTime.Now;
                    //}
                    //else
                    //{
                    //    editNotification.LastDateRun = null;
                    //}
                    
                    ReminderManager.SpecialNotification_Update(editNotification);

                }



                break;

            default:
                //?

                break;
        }
        string strSaveNotification = @"
        parent.$.fancybox.close();
        window.parent.document.getElementById('lnkSaveSpecialNotification').click();
        //parent.location.reload(true);
                                                    ";

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Save Action Nofification", strSaveNotification, true);
    }

  
    protected void PopulateDatabaseField()
    {

        int iTableID = int.Parse( Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
        ddlDatabaseField.Items.Clear();

        DataTable dtColumns = Common.DataTableFromText("SELECT DisplayName FROM [Column] WHERE IsStandard=0 AND   TableID=" + iTableID.ToString() + "  ORDER BY DisplayName");
        foreach (DataRow dr in dtColumns.Rows)
        {
            ListItem aItem = new ListItem(dr["DisplayName"].ToString(), dr["DisplayName"].ToString());
            ddlDatabaseField.Items.Add(aItem);
        }


        //Work with 1 top level Parent tables.
        DataTable dtPT = Common.DataTableFromText("SELECT distinct ParentTableID FROM TableChild WHERE ChildTableID=" + iTableID.ToString());

        if (dtPT.Rows.Count > 0)
        {
            foreach (DataRow dr in dtPT.Rows)
            {
                DataTable dtPColumns = Common.DataTableFromText(@"SELECT distinct TableName + ':' + DisplayName AS DP FROM [Column] INNER JOIN [Table]
                                        ON [Column].TableID=[Table].TableID WHERE IsStandard=0 AND TableTableID IS NULL AND  [Column].TableID=" + dr["ParentTableID"].ToString());
                foreach (DataRow drP in dtPColumns.Rows)
                {
                    ListItem aItem = new ListItem(drP["DP"].ToString(), drP["DP"].ToString());
                    ddlDatabaseField.Items.Add(aItem);
                }
            }
        }


    } 

               
    protected bool IsUserInputOK()
    {
        return true;
    }

}

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true" ValidateRequest="false"
    CodeFile="SpecialNotification.aspx.cs" Inherits="Pages_Schedule_SpecialNotification" %>

<%@ Register TagPrefix="editor" Assembly="WYSIWYGEditor" Namespace="InnovaStudio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <script language="javascript" type="text/javascript">
        function abc() {
            var b = document.getElementById('<%= lnkSave.ClientID %>');
            if (b && typeof (b.click) == 'undefined') {
                b.click = function () {
                    var result = true;
                    if (b.onclick) result = b.onclick();
                    if (typeof (result) == 'undefined' || result) {
                        eval(b.getAttribute('href'));
                    }
                }
            }

        }

       

    </script>
    <table border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td colspan="3" style="padding-top:20px">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left">
                            <span class="TopTitle">
                                <asp:Label runat="server" ID="lblTitle"></asp:Label></span>
                        </td>
                        
                        <td style="text-align:right">
                          <div runat="server" id="divSave">
                                                <asp:LinkButton runat="server" ID="lnkSave" CausesValidation="true" OnClick="lnkSave_Click">
                                                    <asp:Image runat="server" ID="Image1" ImageUrl="~/App_Themes/Default/images/Save.png"  ToolTip="Save" />
                                                </asp:LinkButton>
                                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="13">
            </td>
        </tr>
        <tr>
            <td valign="top">
            </td>
            <td valign="top">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div id="search" >
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                                ShowMessageBox="false" ShowSummary="false" HeaderText="Please correct the following errors:" />
                        </div>
                        <asp:Panel ID="Panel2" runat="server" DefaultButton="lnkSave">
                            <div runat="server" id="divDetail" onkeypress="abc();">
                                <table cellpadding="3">
                                    
                                    <tr>
                                        <td align="right">
                                            <strong>Header*</strong>
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtNotificationHeader" runat="server" Width="250px" CssClass="NormalTextBox"></asp:TextBox>
                                           <asp:RequiredFieldValidator ID="rfvReminderHeader" runat="server" ControlToValidate="txtNotificationHeader"
                                                ErrorMessage="Notification Header - Required"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>

                                     <tr>
                                      <td></td>
                                                <td colspan="2">
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                      <strong>Record Value: </strong>
                                                                </td>
                                                                <td style="padding-left:10px">
                                                                       <asp:DropDownList runat="server" ID="ddlDatabaseField" CssClass="NormalTextBox" DataTextField="Text"
                                                                    DataValueField="Value" ToolTip="Select database value and then click Add to add it to your content.">
                                                                </asp:DropDownList>
                                                                </td>
                                                                <td style="padding-left:20px">
                                                                     <div >
                                                                    <asp:LinkButton runat="server" ID="lnlAddDataBaseField" CssClass="btn" OnClientClick="InsertMergeField(); return false;"
                                                                        CausesValidation="true"> <strong>Add</strong></asp:LinkButton>
                                                                   
                                                                </div>
                                                                </td>
                                                            </tr>


                                                        </table>


                                                    </div>
                                                  
                                                </td>








                                     </tr>
                                    <tr>
                                        <td valign="top" align="right">
                                            <strong>Text</strong>
                                        </td>
                                        <td colspan="2">
                                            <editor:WYSIWYGEditor runat="server" scriptPath="../../Editor/scripts/" ID="edtContent"
                                            btnSave="true" EditorHeight="250" Height="250" EditorWidth="500" Width="500"
                                            AssetManager="../../assetmanager/assetmanager.aspx"  AssetManagerWidth="550" AssetManagerHeight="400"
                                            />
                                        </td>
                                    </tr>

                                    
                                    
                                </table>
                            </div>
                            <br />
                            <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                            <br />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="13">
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        var oEditor = null;
        function InsertMergeField() {
            oUtil.obj.insertHTML("[" + document.getElementById("ctl00_HomeContentPlaceHolder_ddlDatabaseField").value + "]")
        }
    </script>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true"  
    CodeFile="SpecialNotificationUsers.aspx.cs" EnableEventValidation="false" Inherits="Pages_Schedule_SpecialNotificationRecipient" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <script language="javascript" type="text/javascript">
      <%--  function abc() {
            var b = document.getElementById('<%= lnkSave.ClientID %>');
            if (b && typeof (b.click) == 'undefined') {
                b.click = function () {
                    var result = true;
                    if (b.onclick) result = b.onclick();
                    if (typeof (result) == 'undefined' || result) {
                        eval(b.getAttribute('href'));
                    }
                }
            }

        }--%>

       

    </script>
    <table>
         <tr>
             <td style="text-align:left; padding-left:30px; padding-top:30px">
              <span class="TopTitle">
              <asp:Label runat="server" ID="lblTitle" Text="Add Recipients"></asp:Label></span>
             </td>        
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" align="center">
       
        <tr>
            <td colspan="3" height="13">
            </td>
        </tr>
        <tr>
            <td valign="top">
            </td>
            <td valign="top">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <%--<asp:Panel ID="Panel2" runat="server" >--%>
                            <div runat="server" id="divDetail">
                                <table cellpadding="3">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="grdUsers" runat="server" AutoGenerateColumns="False" DataKeyNames="RecipientID"
                                                            HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" 
                                                            CssClass="gridviewborder" OnRowDataBound="grdUsers_RowDataBound"  
                                                            OnRowCommand="grdUsers_RowCommand"  BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" ShowHeaderWhenEmpty="true">
                                                            <Columns>
                                                                <asp:TemplateField Visible="false">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                        <asp:Label runat="server" ID="lblID" Text='<%# Eval("RecipientID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/App_Themes/Default/Images/delete.png"
                                                                            CommandName="deletetype" CommandArgument='<%# Eval("RecipientID") %>'  />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Recipient">
                                                                    <ItemTemplate>
                                                                        <div style="padding:2px 5px 2px 5px; text-align:left; min-width:200px">
                                                                            <asp:Label runat="server" ID="lblUsers" Text='<%# Eval("UserName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:TemplateField HeaderText="Userid" Visible="false">
                                                                    <ItemTemplate>
                                                                        <div style="padding-left: 10px;">
                                                                            <asp:Label runat="server" ID="lblUserID"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="Send To" >
                                                                    <ItemTemplate>
                                                                        <div style="padding:2px 5px 2px 5px; text-align:left; min-width:200px">
                                                                            <asp:Label runat="server" ID="lblSendTo"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField Visible="false">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                        <asp:Label runat="server" ID="lblRecipientOption" Text='<%# Eval("RecipientOption") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                 </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                --================** No Recipients **================--
                                                            </EmptyDataTemplate>
                                                             <HeaderStyle CssClass="gridviewborder_header" />
                                                            <RowStyle CssClass="gridviewborder_row" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table style="padding-top:30px;">
                                                            <tr >
                                                                <td >
                                                                    <asp:DropDownList ID="ddlUser" runat="server"
                                                                        CssClass="NormalTextBox" ClientIDMode="Static">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td runat="server" id="tdSpecificUser" clientidmode="Static" >
                                                                    <asp:DropDownList ID="ddlSpecificUser" runat="server" ClientIDMode="Static" DataTextField="UserName" DataValueField="UserID"
                                                                        CssClass="NormalTextBox">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                 <td runat="server" id="tdEmailFromRecord" clientidmode="Static" >
                                                                    <asp:DropDownList ID="ddlEmailFromRecord" runat="server" ClientIDMode="Static" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                 <td runat="server" id="tdReminderColumnTwo" clientidmode="Static" >
                                                                    <asp:DropDownList ID="ddlReminderColumnTwo" runat="server" ClientIDMode="Static" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td style="padding-left:20px">
                                                                    <div runat="server" id="div2">
                                                                        <asp:LinkButton runat="server" ID="lnkAddUser" CssClass="btn" CausesValidation="false"
                                                                            OnClick="lnkAddUser_Click"> <strong>Add</strong>   </asp:LinkButton>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <br />
                            <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                            <br />
                        <%--</asp:Panel>--%>
                     </ContentTemplate>
                     <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="grdUsers"/>
                    </Triggers>
                </asp:UpdatePanel>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="13">
            </td>
        </tr>
    </table>
    <%--<script type="text/javascript">
        var oEditor = null;
        function InsertMergeField() {
            oUtil.obj.insertHTML("[" + document.getElementById("ctl00_HomeContentPlaceHolder_ddlDatabaseField").value + "]")
        }
    </script>--%>
</asp:Content>

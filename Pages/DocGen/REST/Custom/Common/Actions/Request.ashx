﻿<%@ WebHandler Language="C#" Class="Request"  %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
public class Request : IHttpHandler,IRequiresSessionState {
    /// <summary>
    /// Red: Request.....
    /// </summary>
    /// <param name="context"></param>
    public void ProcessRequest (HttpContext context) {
        bool bSuccess = false;


        if (context.Request.QueryString["RequestName"] != null)
        {
            try
            {
                switch (context.Request.QueryString["RequestName"].ToString())
                {

                    case "ReadTable":
                        context.Response.Write(CommonProcess.ProcessRequest.ReadTable(
                            context.Request.QueryString["From"].ToString(),
                            context.Request.QueryString["Where"].ToString(),
                            context.Request.QueryString["ParamName001"].ToString(),
                            context.Request.QueryString["ParamValue001"].ToString(),
                            context.Request.QueryString["ParamName002"].ToString(),
                            context.Request.QueryString["ParamValue002"].ToString()
                           ));
                        return;


                        //and other RequestName
                        //and other ProcessRequest Model
                        //default:
                        //    context.Response.Write(Windtech.ProcessRequest.Read(int.Parse(context.Request.QueryString["RecordID"].ToString())));
                        //    return;

                }

            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Common: Request", ex.Message, ex.StackTrace, DateTime.Now, "Common/Action/Request");
                SystemData.ErrorLog_Insert(theErrorLog);
            }
        }

        context.Response.Write(bSuccess.ToString());
    }

    public bool IsReusable {
        get {
            return false;
        }
    }
}
﻿<%@ WebHandler Language="C#" Class="Request"  %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
public class Request : IHttpHandler,IRequiresSessionState {
    /// <summary>
    /// Red: Request.....
    /// </summary>
    /// <param name="context"></param>
    public void ProcessRequest (HttpContext context) {
        bool bSuccess = false;


        if (context.Request.QueryString["RequestName"] != null)
        {
            try
            {
                switch (context.Request.QueryString["RequestName"].ToString())
                {
                    case "Create":
                        //Create/Add code in processrequest.cs
                        return;

                    case "Read":
                       context.Response.Write(Windtech.ProcessRequest.Read(
                           int.Parse(context.Request.QueryString["RecordID"].ToString())
                           ));
                        return;

                    case "ReadSP":
                        context.Response.Write(Windtech.ProcessRequest.ReadSP(
                            int.Parse(context.Request.QueryString["RecordID"].ToString()),
                            context.Request.QueryString["spName"].ToString()
                            ));
                        return;

                    case "ReadSPByValue":
                        context.Response.Write(Windtech.ProcessRequest.ReadSPByValue(
                           context.Request.QueryString["sValue"].ToString(),
                            context.Request.QueryString["spName"].ToString()
                            ));
                        return;

                    case "ReadSPWithValues":
                        context.Response.Write(Windtech.ProcessRequest.ReadSPWithValues(
                            context.Request.QueryString["RecordID"] == null ? -1 : int.Parse(context.Request.QueryString["RecordID"].ToString()),
                            context.Request.QueryString["param001"] == null ? "" : context.Request.QueryString["param001"].ToString(),
                            context.Request.QueryString["param002"] == null ? "" : context.Request.QueryString["param002"].ToString(),
                            context.Request.QueryString["param003"] == null ? "" : context.Request.QueryString["param003"].ToString(),
                            context.Request.QueryString["param004"] == null ? "" : context.Request.QueryString["param004"].ToString(),
                            context.Request.QueryString["param005"] == null ? "" : context.Request.QueryString["param005"].ToString(),
                            context.Request.QueryString["param006"] == null ? "" : context.Request.QueryString["param006"].ToString(),
                            context.Request.QueryString["param007"] == null ? "" : context.Request.QueryString["param007"].ToString(),
                            context.Request.QueryString["param008"] == null ? "" : context.Request.QueryString["param008"].ToString(),
                            context.Request.QueryString["param009"] == null ? "" : context.Request.QueryString["param009"].ToString(),
                            context.Request.QueryString["param010"] == null ? "" : context.Request.QueryString["param010"].ToString(),
                            context.Request.QueryString["spName"] == null ? "" : context.Request.QueryString["spName"].ToString()
                            ));
                        return;

                    case "Update": // special case; but use the default update record module
                        //Update code in processrequest.cs
                        return;

                    case "UpdateSP": // special case; but use the default update record module
                        context.Response.Write(Windtech.ProcessRequest.UpdateSP(
                            int.Parse(context.Request.QueryString["RecordID"].ToString()),
                            context.Request.QueryString["spName"].ToString()
                            ));
                        return;

                    case "UpdateSPWithValues":
                        context.Response.Write(Windtech.ProcessRequest.UpdateSPWithValues(
                            context.Request.QueryString["RecordID"] == null ? -1 : int.Parse(context.Request.QueryString["RecordID"].ToString()),
                            context.Request.QueryString["param001"] == null ? "" : context.Request.QueryString["param001"].ToString(),
                            context.Request.QueryString["param002"] == null ? "" : context.Request.QueryString["param002"].ToString(),
                            context.Request.QueryString["param003"] == null ? "" : context.Request.QueryString["param003"].ToString(),
                            context.Request.QueryString["param004"] == null ? "" : context.Request.QueryString["param004"].ToString(),
                            context.Request.QueryString["param005"] == null ? "" : context.Request.QueryString["param005"].ToString(),
                            context.Request.QueryString["param006"] == null ? "" : context.Request.QueryString["param006"].ToString(),
                            context.Request.QueryString["param007"] == null ? "" : context.Request.QueryString["param007"].ToString(),
                            context.Request.QueryString["param008"] == null ? "" : context.Request.QueryString["param008"].ToString(),
                            context.Request.QueryString["param009"] == null ? "" : context.Request.QueryString["param009"].ToString(),
                            context.Request.QueryString["param010"] == null ? "" : context.Request.QueryString["param010"].ToString(),
                            context.Request.QueryString["spName"] == null ? "" : context.Request.QueryString["spName"].ToString()
                            ));
                        return;

                    case "UpdateUnallocatedPayments":
                        context.Response.Write(Windtech.ProcessRequest.UpdateUnallocatedPayments(
                            context.Request.QueryString["RecordID"].ToString(),
                            context.Request.QueryString["TableID"].ToString(),
                            context.Request.QueryString["Activity"].ToString(),
                            context.Request.QueryString["IsUnallocated"].ToString(),
                            context.Request.QueryString["JobNo"].ToString(),
                            context.Request.QueryString["Date"].ToString(),
                            context.Request.QueryString["Amount"].ToString(),
                            context.Request.QueryString["Description"].ToString()
                            ));

                        return;

                    case "AddUnallocatedPayments":
                        context.Response.Write(Windtech.ProcessRequest.AddUnallocatedPayments(
                        context.Request.QueryString["TableID"].ToString(),
                        context.Request.QueryString["Activity"].ToString(),
                        context.Request.QueryString["IsUnallocated"].ToString(),
                        context.Request.QueryString["JobNo"].ToString(),
                        context.Request.QueryString["Date"].ToString(),
                        context.Request.QueryString["Amount"].ToString(),
                        context.Request.QueryString["Description"].ToString()
                        ));

                        return;
                    case "Delete": // special case; but use the default delete record module
                                   //Update code in processrequest.cs
                        return;

                        //and other RequestName
                        //and other ProcessRequest Model
                        //default:
                        //    context.Response.Write(Windtech.ProcessRequest.Read(int.Parse(context.Request.QueryString["RecordID"].ToString())));
                        //    return;

                }

            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Windtech: Request", ex.Message, ex.StackTrace, DateTime.Now, "Windtech/Action/Request");
                SystemData.ErrorLog_Insert(theErrorLog);
            }
        }

        context.Response.Write(bSuccess.ToString());
    }

    public bool IsReusable {
        get {
            return false;
        }
    }
}
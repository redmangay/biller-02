﻿    <%@ WebHandler Language="C#" Class="Request"  %>

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
public class Request : IHttpHandler,IRequiresSessionState {
    /// <summary>
    /// Red: Request..... query service parameterized
    /// </summary>
    /// <param name="context"></param>
    public void ProcessRequest (HttpContext context) {
        bool bSuccess = false;

        if (context.Request.QueryString["RequestName"] != null)
        {
            try
            {
                switch (context.Request.QueryString["RequestName"].ToString())
                {
                    case "Read":
                        context.Response.Write(Windtech.ProcessRequest.Read(
                            int.Parse(context.Request.QueryString["RecordID"].ToString())
                            ));
                        return;

                    case "ReadSP":
                        context.Response.Write(Dev.ProcessRequest.ReadSP(
                            int.Parse(context.Request.QueryString["RecordID"].ToString()),
                            context.Request.QueryString["spName"].ToString()
                            ));
                        return;

                    case "ReadSPByValue":
                        context.Response.Write(Dev.ProcessRequest.ReadSPByValue(
                           context.Request.QueryString["sValue"].ToString(),
                            context.Request.QueryString["spName"].ToString()
                            ));
                        return;

                    case "ReadSPWithValues":
                        context.Response.Write(Dev.ProcessRequest.ReadSPWithValues(
                            context.Request.QueryString["RecordID"] == null ? -1 : int.Parse(context.Request.QueryString["RecordID"].ToString()),
                            context.Request.QueryString["param001"] == null ? "" : context.Request.QueryString["param001"].ToString(),
                            context.Request.QueryString["param002"] == null ? "" : context.Request.QueryString["param002"].ToString(),
                            context.Request.QueryString["param003"] == null ? "" : context.Request.QueryString["param003"].ToString(),
                            context.Request.QueryString["param004"] == null ? "" : context.Request.QueryString["param004"].ToString(),
                            context.Request.QueryString["param005"] == null ? "" : context.Request.QueryString["param005"].ToString(),
                            context.Request.QueryString["param006"] == null ? "" : context.Request.QueryString["param006"].ToString(),
                            context.Request.QueryString["param007"] == null ? "" : context.Request.QueryString["param007"].ToString(),
                            context.Request.QueryString["param008"] == null ? "" : context.Request.QueryString["param008"].ToString(),
                            context.Request.QueryString["param009"] == null ? "" : context.Request.QueryString["param009"].ToString(),
                            context.Request.QueryString["param010"] == null ? "" : context.Request.QueryString["param010"].ToString(),
                            context.Request.QueryString["spName"] == null ? "" : context.Request.QueryString["spName"].ToString()
                            ));
                        return;
                    case "updaterecord":

                        Record theRecord = RecordManager.ets_Record_Detail_Full(int.Parse(context.Request.QueryString["RecordID"].ToString()), null, true);

                        //need to check security here - MR

                        context.Response.Write("Yes");

                        return;
                        //and other ProcessRequest Model
                        //default:
                        //    context.Response.Write(Dev.ProcessRequest.Read(int.Parse(context.Request.QueryString["RecordID"].ToString())));
                        //    return;
                }
            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Dev: Request", ex.Message, ex.StackTrace, DateTime.Now, "Dev/Action/Request");
                SystemData.ErrorLog_Insert(theErrorLog);
            }
        }
        context.Response.Write(bSuccess.ToString());
    }

    public bool IsReusable {
        get {
            return false;
        }
    }
}
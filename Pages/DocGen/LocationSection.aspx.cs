﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocGen.DAL;
using System.Data;


namespace DocGen.Document.LocationSection
{



    public partial class Edit : SecurePage
    {

        protected void PopulateTableDDL()
        {
            int iTN = 0;

            //ddlTableMapPop.DataSource = RecordManager.ets_Table_Select(null,
            //        null,
            //        null,
            //        AccountID,
            //        null, null, true,
            //        "st.TableName", "ASC",
            //        null, null, ref  iTN, "");

            ddlTableMapPop.DataSource = Common.DataTableFromText(@"SELECT   DISTINCT  [Table].TableName, [Table].TableID
FROM         [Column] INNER JOIN
                      [Table] ON [Column].TableID = [Table].TableID 
                      WHERE [Column].ColumnType='location' AND [Table].IsActive=1  AND [Table].AccountID=" + AccountID.ToString());

            ddlTableMapPop.DataBind();


            System.Web.UI.WebControls.ListItem liSelect2 = new System.Web.UI.WebControls.ListItem("All", "-1");
            ddlTableMapPop.Items.Insert(0, liSelect2);

            //KG 20/11/17 Ticket 3154
            ddlTableMapPop.Items.Insert(1, new System.Web.UI.WebControls.ListItem("None", "0"));


        }

        public int DocumentSectionID
        {
            get
            {
                int _DocumentSectionID = 0;
                if (Request.QueryString["DocumentSectionID"] != null)
                {
                    Int32.TryParse(Convert.ToString(Request.QueryString["DocumentSectionID"]), out _DocumentSectionID);
                }
                return _DocumentSectionID;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ddlManualScale.Attributes.Add("onchange", "ChangeMapScale()");
                PopulateTableDDL();

                string strDefaultPin = "Pages/Record/PINImages/DefaultPin.png";

                hfImage.Value = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/" + strDefaultPin;
                hfFlag.Value = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Images/Flag.png";
                hfGunPoints.Value = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Images/gun_points.png";
                //gun_points.png


                if (DocumentSectionID <= 0)
                {
                    if (Request.QueryString["PrevID"].ToString() != "-1")
                    {
                        //hfRemoveSection.Value = "1";
                    }
                    Account theAccount = SecurityManager.Account_Details((int)AccountID);

                    if (theAccount != null)
                    {
                        if (theAccount.MapCentreLat != null)
                            hfCentreLat.Value = theAccount.MapCentreLat.ToString();

                        if (theAccount.MapCentreLong != null)
                            hfCentreLong.Value = theAccount.MapCentreLong.ToString();

                        if (theAccount.MapZoomLevel != null)
                            hfOtherZoomLevel.Value = theAccount.MapZoomLevel.ToString();

                        hfForceMapCenter.Value = "yes";
                    }

                    //add



                    //Response.Redirect("../Summary.aspx");
                }
                else
                {

                    using (DAL.DocGenDataContext ctx = new DAL.DocGenDataContext())
                    {



                        DAL.DocumentSection section = ctx.DocumentSections.SingleOrDefault<DAL.DocumentSection>(s => s.DocumentSectionID == DocumentSectionID);
                        if (section != null)
                        {
                            if (section.Details != "")
                            {
                                LocationSectionDetail mapDetail = JSONField.GetTypedObject<LocationSectionDetail>(section.Details);
                                if (mapDetail != null)
                                {
                                    txtAddress.Text = mapDetail.Address;

                                    if (mapDetail.MapTypeId != null && mapDetail.MapTypeId != "")
                                    {
                                        hfMaptype.Value = mapDetail.MapTypeId;
                                    }
                                    if(mapDetail.CustomService!=null)
                                    {
                                        chkCustomService.Checked = (bool)mapDetail.CustomService;
                                    }

                                    if (mapDetail.ShowStaff != null)
                                    {
                                        chkStaff.Checked = (bool)mapDetail.ShowStaff;
                                    }
                                    if (mapDetail.ShowOpenTask != null)
                                    {
                                        chkOpenTasks.Checked = (bool)mapDetail.ShowOpenTask;
                                    }
                                    if (mapDetail.ShowCompleted != null)
                                    {
                                        chkCompletedTasks.Checked = (bool)mapDetail.ShowCompleted;
                                    }

                                    if (mapDetail.Latitude != null)
                                    {
                                        txtLatitude.Text = mapDetail.Latitude.ToString();
                                        hfCentreLat.Value = mapDetail.Latitude.ToString();
                                        hfForceMapCenter.Value = "yes";
                                    }

                                    if (mapDetail.Longitude != null)
                                    {
                                        txtLongitude.Text = mapDetail.Longitude.ToString();
                                        hfCentreLong.Value = mapDetail.Longitude.ToString();
                                    }

                                    if (mapDetail.MapScale != null)
                                    {
                                        //hfOtherZoomLevel.Value = mapDetail.MapScale.ToString();
                                        hfOtherZoomLevel.Value = mapDetail.MapScale.ToString();
                                        if (ddlManualScale.Items.FindByValue(hfOtherZoomLevel.Value) != null)
                                        {
                                            ddlManualScale.SelectedValue = hfOtherZoomLevel.Value;
                                        }
                                    }

                                    if (mapDetail.AutoScale != null && (bool)mapDetail.AutoScale)
                                    {
                                        rbAutoScale.Checked = true;
                                        rbManual.Checked = false;
                                    }

                                    if (mapDetail.ShowLocation != null)
                                    {
                                        ddlTableMapPop.SelectedValue = mapDetail.ShowLocation.ToString();
                                        PopulateColumns();

                                    }
                                    if (int.Parse(ddlTableMapPop.SelectedValue)>0)
                                    {
                                        if(mapDetail.SearchColumnID!=null)
                                        {
                                            if (ddlSearchColumn.Items.FindByValue(mapDetail.SearchColumnID.ToString()) != null)
                                            {
                                                ddlSearchColumn.SelectedValue = mapDetail.SearchColumnID.ToString();
                                            }
                                        }
                                        if (mapDetail.PinControlColumnID != null)
                                        {
                                            if (ddlPinControl.Items.FindByValue(mapDetail.PinControlColumnID.ToString()) != null)
                                            {
                                                ddlPinControl.SelectedValue = mapDetail.PinControlColumnID.ToString();
                                                ddlPinControl_SelectedIndexChanged(null, null);
                                            }
                                            if(!string.IsNullOrEmpty( mapDetail.PinControlValueInfo))
                                            {
                                                fgPinControlValue.XMLText = mapDetail.PinControlValueInfo;
                                                fgPinControlValue.PopulateFilterGrid(mapDetail.PinControlValueInfo);
                                            }
                                        }
                                    }

                                    if (mapDetail.Height != null)
                                        txtHeight.Text = mapDetail.Height.ToString();

                                    if (mapDetail.Width != null)
                                        txtWidth.Text = mapDetail.Width.ToString();


                                    //if (mapDetail.Latitude != null && mapDetail.Longitude != null && mapDetail.MapScale != null)
                                    //{
                                    //    hlChoose.NavigateUrl = "~/Pages/Site/GoogleMap.aspx?type=mapsection&lat=" + mapDetail.Latitude.ToString() + "&lng=" + mapDetail.Longitude.ToString() + "&zoom=" + mapDetail.MapScale.ToString();

                                    //}
                                    //else if (mapDetail.Latitude != null && mapDetail.Longitude != null)
                                    //{
                                    //    hlChoose.NavigateUrl = "~/Pages/Site/GoogleMap.aspx?type=mapsection&lat=" + mapDetail.Latitude.ToString() + "&lng=" + mapDetail.Longitude.ToString();

                                    //}
                                    if (mapDetail.Latitude != null && mapDetail.Longitude != null)
                                    {
                                        hfForceMapCenter.Value = "yes";
                                    }
                                    else
                                    {
                                        hfForceMapCenter.Value = "no";
                                    }

                                }

                            }

                        }
                        else
                        {
                            //Response.Redirect("../Summary.aspx", true);
                        }
                    }



                    //ShowList();
                }



            }




        }

        protected void CheckPermission(int DocumentID)
        {
            using (DAL.DocGenDataContext ctx = new DAL.DocGenDataContext())
            {
                DAL.Document doc = ctx.Documents.SingleOrDefault<DAL.Document>(d => d.DocumentID == DocumentID && d.AccountID == this.AccountID);
                if (doc == null)
                {
                    Response.Redirect("~/Empty.aspx", false);
                }
            }
        }

        public int AccountID
        {
            get
            {
                int retVal = 0;
                if (Session["AccountID"] != null)
                    retVal = Convert.ToInt32(Session["AccountID"]);
                return retVal;
            }
        }


        protected void SaveButton_Click(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            try
            {

                LocationSectionDetail mapDetail = new LocationSectionDetail();

                mapDetail.Address = txtAddress.Text;
                mapDetail.Latitude = txtLatitude.Text == "" ? null : (double?)double.Parse(txtLatitude.Text);
                mapDetail.Longitude = txtLongitude.Text == "" ? null : (double?)double.Parse(txtLongitude.Text);
                mapDetail.MapScale = int.Parse(hfOtherZoomLevel.Value);
                mapDetail.ShowLocation = int.Parse(ddlTableMapPop.SelectedValue);
                mapDetail.MapTypeId = hfMaptype.Value;
                mapDetail.Height = txtHeight.Text == "" ? null : (int?)int.Parse(txtHeight.Text);
                mapDetail.Width = txtWidth.Text == "" ? null : (int?)int.Parse(txtWidth.Text);
                mapDetail.CustomService = chkCustomService.Checked;
                mapDetail.ShowStaff = chkStaff.Checked;
                mapDetail.ShowOpenTask = chkOpenTasks.Checked;
                mapDetail.ShowCompleted = chkCompletedTasks.Checked;

                if(rbAutoScale.Checked)
                {
                    mapDetail.AutoScale = rbAutoScale.Checked;
                }

                if(int.Parse(ddlTableMapPop.SelectedValue)>0)
                {
                    if (ddlSearchColumn.SelectedValue!="")
                        mapDetail.SearchColumnID =int.Parse( ddlSearchColumn.SelectedValue);

                    if (ddlPinControl.SelectedValue!="")
                    {
                        mapDetail.PinControlColumnID = int.Parse( ddlPinControl.SelectedValue);
                        mapDetail.PinControlValueInfo = fgPinControlValue.XMLText;
                    }
                    
                }


                Account theAccount = SecurityManager.Account_Details((int)AccountID);

                if (theAccount != null)
                {
                    theAccount.MapCentreLat = mapDetail.Latitude;
                    theAccount.MapCentreLong = mapDetail.Longitude;
                    SecurityManager.Account_Update(theAccount);
                }



                int DocumentSectionID = 0;
                if (Request.QueryString["PrevID"].ToString() != "-1")
                {

                    int DocumentID = 0;
                    Int32.TryParse(Convert.ToString(Request.QueryString["DocumentID"]), out DocumentID);

                    int iPosition = 1;

                    int NewSectionID = 0;

                    using (DAL.DocGenDataContext ctx = new DAL.DocGenDataContext())
                    {

                        if (Request.QueryString["PrevID"].ToString() != "0")
                        {

                            DAL.DocumentSection PreSection = ctx.DocumentSections.SingleOrDefault<DAL.DocumentSection>(s => s.DocumentSectionID == int.Parse(Request.QueryString["PrevID"].ToString()));

                            iPosition = PreSection.Position + 1;
                        }
                        else
                        {
                            iPosition = 1;
                        }

                        DAL.DocumentSection newSection = new DAL.DocumentSection();

                        //if (Request.QueryString["Position"] != null)
                        //{
                        ctx.ExecuteCommand("UPDATE DocumentSection SET Position=Position + 1 WHERE DocumentID={0}  AND Position>{1}", DocumentID.ToString(), (iPosition - 1).ToString());
                        //}

                        newSection.DocumentID = DocumentID;
                        //newSection.SectionName = txtTitle.Text;
                        newSection.DocumentSectionTypeID = 12; //Map
                        //newSection.DocumentSectionStyleID = int.Parse(ddlStyle.SelectedValue);
                        newSection.Content = "";
                        newSection.Details = mapDetail.GetJSONString();
                        newSection.Position = iPosition;
                        newSection.DateAdded = DateTime.Now;
                        newSection.DateUpdated = DateTime.Now;
                        ctx.DocumentSections.InsertOnSubmit(newSection);



                        ctx.SubmitChanges();

                        NewSectionID = newSection.DocumentSectionID;
                        DocumentSectionID = NewSectionID;
                        //hfRemoveSection.Value = "0";
                    }


                }
                else
                {

                    Int32.TryParse(Convert.ToString(Request.QueryString["DocumentSectionID"]), out DocumentSectionID);
                    using (DAL.DocGenDataContext ctx = new DAL.DocGenDataContext())
                    {
                        DAL.DocumentSection section = ctx.DocumentSections.SingleOrDefault<DAL.DocumentSection>(s => s.DocumentSectionID == DocumentSectionID);
                        section.Content = "";
                        //section.DocumentSectionStyleID = int.Parse(ddlStyle.SelectedValue);
                        section.Details = mapDetail.GetJSONString();
                        ctx.SubmitChanges();
                    }
                }
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "CloseScript", "window.parent.SectionUpdated(" + DocumentSectionID.ToString() + ");", true);

            }
            catch
            {
                lblMsg.Text = "";

            }

        }

        protected void PopulateColumns()
        {
            ddlSearchColumn.Items.Clear();
            ddlPinControl.Items.Clear();
            if (int.Parse(ddlTableMapPop.SelectedValue) > 0)
            {
                trSearch.Visible = true;
                trPinControl.Visible = true;
                trPinControlValue.Visible = true;

                DataTable dtColumns = Common.DataTableFromText("SELECT ColumnID,DisplayName FROM [Column] WHERE TableID=" + ddlTableMapPop.SelectedValue + " AND IsStandard=0 ORDER BY DisplayName");

                if (dtColumns != null)
                {
                    ddlSearchColumn.DataSource = dtColumns;
                    ddlSearchColumn.DataBind();
                    ddlSearchColumn.Items.Insert(0, new ListItem("--Please select--", ""));

                    ddlPinControl.DataSource = dtColumns;
                    ddlPinControl.DataBind();
                    ddlPinControl.Items.Insert(0, new ListItem("--Please select--", ""));
                }
            }
            else
            {
                trSearch.Visible = false;
                trPinControl.Visible = false;
                trPinControlValue.Visible = false;
            }
        }

        protected void ddlTableMapPop_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateColumns();
            //ddlPinControl_SelectedIndexChanged(null, null);
        }

        protected void ddlPinControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (int.Parse(ddlTableMapPop.SelectedValue) > 0 && ddlPinControl.SelectedValue!="")
            {
                fgPinControlValue.TableID = int.Parse(ddlTableMapPop.SelectedValue);
                fgPinControlValue.ColumnID = int.Parse(ddlPinControl.SelectedValue);
                fgPinControlValue.HideAndOr = true;
                fgPinControlValue.HideColumn = true;
                fgPinControlValue.HideCompareOperator = true;
                fgPinControlValue.HideHeader = true;
                fgPinControlValue.CustomColumn1 = "mappin";
                fgPinControlValue.PopulateFilterGrid("");
            }
             if (int.Parse(ddlTableMapPop.SelectedValue) > 0 && ddlPinControl.SelectedValue=="")
             {
                 fgPinControlValue.TableID = int.Parse(ddlTableMapPop.SelectedValue);
                 fgPinControlValue.ColumnID = 0;
                 fgPinControlValue.HideAndOr = true;
                 fgPinControlValue.HideColumn = true;
                 fgPinControlValue.HideCompareOperator = true;
                 fgPinControlValue.HideHeader = true;
                 fgPinControlValue.CustomColumn1 = "mappin";
                 fgPinControlValue.PopulateFilterGrid("");
             }
            
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true" ValidateRequest="false"
    CodeFile="CalendarSection.aspx.cs" Inherits="DocGen.Document.Calendar.Edit" %>

<%@ Register TagPrefix="editor" Assembly="WYSIWYGEditor" Namespace="InnovaStudio" %>
<%@ Register Src="~/Pages/UserControl/ControlByColumn.ascx" TagName="ControlByColumn" TagPrefix="dbg" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="server">

    <style type="text/css">
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 80%;
        }

        /* The Close Button */
        .close {
            color: #aaaaaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            // Get the modal
            var modal = document.getElementById('divAdvanced');

            // Get the button that opens the modal
            var btn = document.getElementById("lnkAdvanced");

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks the button, open the modal 
            btn.onclick = function () {
                modal.style.display = "block";
                return false;
            }

            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
                modal.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        });
    </script>

    <script type="text/javascript">
        function SavedAndRefresh() {
            window.parent.document.getElementById('btnRefresh').click();
            parent.$.fancybox.close();

        }

        function CloseAndRefresh() {
            if (document.getElementById('hfRemoveSection').value == '0') {
                parent.$.fancybox.close();
            }
            else {
                //                window.parent.document.getElementById('btnRefresh').click();
                window.parent.RemoveNoAddedSection();
                parent.$.fancybox.close();
            }

        }

    </script>

    <div >
        <div style="min-height: 400px;">
            <asp:HiddenField runat="server" ID="hfRemoveSection" ClientIDMode="Static" Value="0" />
            <br />
            <span class="failureNotification">
                <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
            </span>
            <asp:ValidationSummary ID="MainValidationSummary" runat="server" CssClass="failureNotification"
                ValidationGroup="MainValidationGroup" />
            <br />
            <div style="padding: 20px;">
                <table cellpadding="3">
                    <tr>
                        <td align="left">
                            <asp:Label runat="server" ID="Label1" CssClass="TopTitle" Text="Calendar"></asp:Label>
                        </td>
                        <td align="right">
                            <table>
                                <tr>

                                    <td></td>
                                    <td>
                                        <div runat="server" id="div2">
                                            <asp:LinkButton runat="server" ID="SaveButton" OnClick="SaveButton_Click" ValidationGroup="MainValidationGroup">
                                                <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                    ToolTip="Save" />
                                            </asp:LinkButton>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="right">
                            <strong>Title</strong>
                        </td>
                        <td align="left">
                            <asp:TextBox runat="server" ID="txtCalendarTitle" CssClass="NormalTextBox" Width="400px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <strong>Table </strong>
                        </td>
                        <td align="left">

                            <asp:DropDownList ID="ddlTable" runat="server" AutoPostBack="True" CssClass="NormalTextBox"
                                DataTextField="TableName" DataValueField="TableID" OnSelectedIndexChanged="ddlTable_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlTable"
                                CssClass="failureNotification" ErrorMessage="Table is required." ToolTip="Table is required."
                                ValidationGroup="MainValidationGroup">*</asp:RequiredFieldValidator>

                        </td>
                    </tr>
                    <tr runat="server" id="trStart" visible="false">
                        <td align="right">
                            <strong>Start*</strong>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlDateField" runat="server" AutoPostBack="false" CssClass="NormalTextBox"
                                DataTextField="DisplayName" DataValueField="ColumnID">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlDateField"
                                CssClass="failureNotification" ErrorMessage="Date Field is required." ToolTip="Date Field is required."
                                ValidationGroup="MainValidationGroup">*</asp:RequiredFieldValidator>

                        </td>
                    </tr>
                    <tr runat="server" id="trEnd" visible="false">
                        <td align="right">
                            <strong>End</strong>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlEndDateField" runat="server" AutoPostBack="false" CssClass="NormalTextBox"
                                DataTextField="DisplayName" DataValueField="ColumnID">
                            </asp:DropDownList>

                        </td>
                    </tr>
                    <tr>
                        <td align="right"><strong>Default</strong> </td>
                        <td align="left">
                            <asp:RadioButtonList runat="server" ID="radioCalendarDefaultView" RepeatDirection="Horizontal">
                                <asp:ListItem Value="month" Text="Month" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="week" Text="Week"></asp:ListItem>
                                <asp:ListItem Value="day" Text="Day"></asp:ListItem>
                            </asp:RadioButtonList>

                        </td>
                    </tr>
                    <tr runat="server" id="trDisplay" visible="false">
                        <td align="right">
                            <strong>Display</strong>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlDisplay" runat="server" AutoPostBack="true" CssClass="NormalTextBox"
                                DataTextField="DisplayName" DataValueField="ColumnID" OnSelectedIndexChanged="ddlDisplay_SelectedIndexChanged">
                            </asp:DropDownList>

                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:CheckBox runat="server" Checked="true" TextAlign="Right" Text="Show Add Record Icon" ID="chkShowAddRecordIcon" />
                        </td>
                    </tr>

                    <tr runat="server" id="trNameField" visible="false">
                        <td align="right">
                            <strong>Name Field</strong>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlNameField" runat="server" AutoPostBack="false" CssClass="NormalTextBox"
                                DataTextField="DisplayName" DataValueField="ColumnID">
                            </asp:DropDownList>

                        </td>
                    </tr>


                    <tr>
                        <td align="right">
                            <strong>Height</strong>
                        </td>
                        <td>

                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" style="width: 200px;">
                                        <asp:TextBox ID="txtHeight" runat="server" CssClass="NormalTextBox" Width="50px"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtHeight"
                                            runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,20}\.$)|(^-?\d{1,20}$)|(^-?\d{0,20}\.\d{1,10}$)">
                                        </asp:RegularExpressionValidator>
                                    </td>
                                    <td style="width: 100px;" align="right">
                                        <strong>Day Start</strong>
                                    </td>
                                    <td align="left" style="padding-left: 3px;">
                                        <asp:DropDownList runat="server" ID="ddlDayStart" CssClass="NormalTextBox">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <strong>Width</strong>
                        </td>
                        <td align="left">

                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" style="width: 200px;">
                                        <asp:TextBox ID="txtWidth" runat="server" CssClass="NormalTextBox" Width="50px"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtWidth"
                                            runat="server" ErrorMessage="Numeric!" Display="Dynamic" ValidationExpression="(^-?\d{1,20}\.$)|(^-?\d{1,20}$)|(^-?\d{0,20}\.\d{1,10}$)">
                                        </asp:RegularExpressionValidator>
                                    </td>
                                    <td style="width: 100px;" align="right">
                                        <strong>Day End</strong>
                                    </td>
                                    <td align="left" style="padding-left: 3px;">
                                        <asp:DropDownList runat="server" ID="ddlDayEnd" CssClass="NormalTextBox">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>



                    <tr>
                        <td align="right">
                            <strong></strong>
                        </td>
                        <td align="left"></td>
                    </tr>
                </table>
                <br />
                <%--<button id="lnkAdvanced">Advanced...</button>--%>
                <asp:LinkButton runat="server" ID="lnkAdvanced" ClientIDMode="Static">Advanced...</asp:LinkButton>
                <br />
                <div style="text-align: center;">
                    <div style="display: inline-block">
                        <div id="divAdvanced" class="modal">
                            <div class="modal-content">
                                <table>

                                    <tr>
                                        <td colspan="2">
                                            <h1>Calendar (Advanced)</h1>

                                        </td>
                                        <td style="width: 100px;">
                                            <span class="close">&times;</span>
                                        </td>
                                    </tr>
                                    <tr runat="server" id="trDisplayText">
                                        <td align="right" valign="middle">
                                            <strong>Display Text</strong>
                                        </td>
                                        <td align="left">
                                            <table>
                                                <tr>

                                                    <td align="left">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <strong>Database Value</strong>
                                                                </td>
                                                                <td>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:DropDownList runat="server" ID="ddlDatabaseField" CssClass="NormalTextBox" DataTextField="Text"
                                                                                    DataValueField="Value" ToolTip="Select database value and then click Add to add it to your content.">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>
                                                                                <div>
                                                                                    <asp:LinkButton runat="server" ID="lnlAddDataBaseField" CssClass="btn" OnClientClick="InsertMergeField(); return false;"
                                                                                        CausesValidation="true"> <strong>Add</strong></asp:LinkButton>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td>
                                                        <editor:WYSIWYGEditor runat="server" scriptPath="../../Editor/scripts/" ID="edtContent"
                                                            btnStyles="true" btnSave="false" EditorHeight="200" Height="200" EditorWidth="600"
                                                            Width="600" AssetManager="../../assetmanager/assetmanager.aspx" AssetManagerWidth="400"
                                                            AssetManagerHeight="400" Visible="true" ToolbarMode="0" btnPreview="False"
                                                            btnSearch="False" btnBookmark="False" btnAbsolute="False" btnForm="False" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr runat="server" id="trFilter" visible="false">
                                        <td align="right" valign="top" style="padding-top: 10px;">
                                            <strong>Filter</strong>
                                        </td>
                                        <td align="left">
                                            <asp:UpdatePanel runat="server" ID="upFilter" UpdateMode="Always">
                                                <ContentTemplate>
                                                    <table>
                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                                <dbg:ControlByColumn runat="server" ID="cbcFilter1" OnddlYAxis_Changed="cbcFilter1_OnddlYAxis_Changed" />
                                                            </td>
                                                            <td></td>
                                                            <td>
                                                                <asp:LinkButton runat="server" ID="lnkAddFilter1" CausesValidation="false" OnClick="lnkAddFilter1_Click">
                                                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png" />
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr id="trFilter2" runat="server" visible="false">
                                                            <td>
                                                                <asp:LinkButton runat="server" ID="lnkMinusFilter2" CausesValidation="false" OnClick="lnkMinusFilter2_Click">
                                                                    <asp:Image ID="Image6" runat="server" ImageUrl="~/App_Themes/Default/Images/Minus.png" />
                                                                </asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <dbg:ControlByColumn runat="server" ID="cbcFilter2" OnddlYAxis_Changed="cbcFilter2_OnddlYAxis_Changed" />
                                                            </td>
                                                            <td></td>
                                                            <td>
                                                                <asp:LinkButton runat="server" ID="lnkAddFilter2" CausesValidation="false" OnClick="lnkAddFilter2_Click">
                                                                    <asp:Image ID="Image5" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png" />
                                                                </asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                        <tr id="trFilter3" runat="server" visible="false">
                                                            <td>
                                                                <asp:LinkButton runat="server" ID="lnkMinusFilter3" CausesValidation="false" OnClick="lnkMinusFilter3_Click">
                                                                    <asp:Image ID="Image7" runat="server" ImageUrl="~/App_Themes/Default/Images/Minus.png" />
                                                                </asp:LinkButton>
                                                            </td>
                                                            <td>
                                                                <dbg:ControlByColumn runat="server" ID="cbcFilter3" OnddlYAxis_Changed="cbcFilter3_OnddlYAxis_Changed" />
                                                            </td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr runat="server" id="trColourText" visible="false">
                                        <td align="right" valign="top" style="padding-top: 10px;">
                                            <strong>Text Colour</strong>
                                        </td>
                                        <td align="left">
                                            <asp:UpdatePanel runat="server" ID="upColour" UpdateMode="Always">
                                                <ContentTemplate>

                                                    <div>
                                                        <asp:GridView ID="grdCalendarColor" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                                                            CssClass="gridview" OnRowCommand="grdCalendarColor_RowCommand" OnRowDataBound="grdCalendarColor_RowDataBound"
                                                            ShowHeaderWhenEmpty="true"
                                                            ShowFooter="true">
                                                            <Columns>
                                                                <asp:TemplateField Visible="false">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" ID="lblID" Text='<%# Eval("ID") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgbtnMinus" runat="server" ImageUrl="~/App_Themes/Default/Images/Minus.png"
                                                                            CommandName="minus" CommandArgument='<%# Eval("ID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                    <HeaderTemplate>
                                                                        <strong>Field and Value</strong>
                                                                    </HeaderTemplate>

                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>

                                                                        <dbg:ControlByColumn runat="server" ID="cbcColour" OnddlYAxis_Changed="CBCColorYAxisChanged" />

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                    <HeaderTemplate>
                                                                        <strong>Colour</strong>
                                                                    </HeaderTemplate>

                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:DropDownList runat="server" ID="ddlTextColour" CssClass="NormalTextBox">
                                                                            <asp:ListItem Value="000000" Text="Black"></asp:ListItem>
                                                                            <asp:ListItem Value="FFFFFF" Text="White"></asp:ListItem>
                                                                            <asp:ListItem Value="C0C0C0" Text="Silver"></asp:ListItem>
                                                                            <asp:ListItem Value="808080" Text="Gray"></asp:ListItem>
                                                                            <asp:ListItem Value="FF0000" Text="Red"></asp:ListItem>
                                                                            <asp:ListItem Value="800000" Text="Maroon"></asp:ListItem>
                                                                            <asp:ListItem Value="FFFF00" Text="Yellow"></asp:ListItem>
                                                                            <asp:ListItem Value="808000" Text="Olive"></asp:ListItem>
                                                                            <asp:ListItem Value="00FF00" Text="Lime"></asp:ListItem>
                                                                            <asp:ListItem Value="008000" Text="Green"></asp:ListItem>
                                                                            <asp:ListItem Value="00FFFF" Text="Aqua"></asp:ListItem>
                                                                            <asp:ListItem Value="008080" Text="Teal"></asp:ListItem>
                                                                            <asp:ListItem Value="0000FF" Text="Blue"></asp:ListItem>
                                                                            <asp:ListItem Value="000080" Text="Navy"></asp:ListItem>
                                                                            <asp:ListItem Value="FF00FF" Text="Fuchsia"></asp:ListItem>
                                                                            <asp:ListItem Value="800080" Text="Purple"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField>
                                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgbtnPlus" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png"
                                                                            CommandName="plus" Visible="false" CommandArgument='<%# Eval("ID") %>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <HeaderStyle CssClass="gridview_header" Height="25px" />
                                                            <RowStyle CssClass="gridview_row_NoPadding" />
                                                        </asp:GridView>

                                                        <div>
                                                            <asp:Label ID="lblMsgTab" runat="server" ForeColor="Red"></asp:Label>
                                                        </div>
                                                    </div>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>


    <script type="text/javascript">
        var oEditor = null;
        function InsertMergeField() {
            oUtil.obj.insertHTML("[" + document.getElementById("ctl00_HomeContentPlaceHolder_ddlDatabaseField").value + "]")
        }
    </script>
</asp:Content>

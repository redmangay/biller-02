﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeFile="EachCalendar.aspx.cs" Inherits="Pages_DocGen_EachCalendar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="server">
    <style type="text/css">
        body {
            font-size: 12px;
        }

        .detailtable {
            display: block;
            height: 100%;
            overflow: hidden;
        }

            .detailtable td {
                width: 30px;
                max-width: 30px;
                height:50px;
                /*height: 50px;
                max-height: 50px;*/
                border: 1px dotted black;
                overflow: visible;
                border-bottom: double;
                vertical-align: top;
            }

                .detailtable td:first-child {
                    width: 100px;
                    max-width: 200px;
                    /*height: 50px;
                    max-height: 50px;*/
                    border: 1px dotted black;
                    overflow: visible;
                    border-bottom: double;
                    border-right: 2px solid;
                    vertical-align: middle;
                    padding:5px;
                }

                .detailtable td:nth-child(2), .detailtable td:last-child {
                    width: 60px;
                    max-width: 60px;
                    /*height: 50px;
                    max-height: 50px;*/
                    /*border: 0px none;*/
                    /*border-bottom: double;*/
                    background-color: #EDEDED;
                }

            /*.detailtable td:last-child {
                    overflow: hidden;
                }*/
            .detailtable tr:first-child td {
                height: 30px;
                border-bottom: 2px solid;
            }

        .columnspan {
            display: block;
            background-color: #DDEBF7;
            border: 1px solid;
            height: 50px;
            max-height: 50px;
            width: 30px;
            position: relative;
            overflow: hidden;
            float: left;
            padding:5px;
            /*resize:horizontal;*/
        }
    </style>


    <style type="text/css">
        .OtherMonthDayStyle {
            background-color: #F2F2F2;
            padding: 0px;
            vertical-align: top;
        }

        .TodayDayStyle {
            vertical-align: top;
        }

        .WeekendDayStyle {
            vertical-align: top;
        }

        .maincalender {
            background-color: #FFFFFF;
            border-color: #D8D8D8;
        }

            .maincalender tr:first-child {
                display: none;
            }

        .DayStyle {
            padding: 0px;
            vertical-align: top;
        }



        .eachschedule {
            background-color: #ffffff;
            padding: 1px;
            font-weight: bold;
        }

        .TitleStyle {
            font-family: Verdana;
            font-size: 16pt;
            background-color: #FFFFFF;
        }

        .DayHeaderStyle {
            font-family: Verdana;
            font-size: 12px;
            background-color: #0299C6;
        }

        th.DayHeaderStyle:nth-child(6) {
            font-family: Verdana;
            font-size: 12px;
            background-color: #45A6EB;
        }

        th.DayHeaderStyle:nth-child(7) {
            font-family: Verdana;
            font-size: 12px;
            background-color: #45A6EB;
        }

        .TodayTop {
            padding-left: 5px;
            padding-top: 5px;
            text-align: left;
            background-color: #ffffff;
            font-size: 12px;
            font-family: Arial;
            color: Gray;
            vertical-align: top;
        }

        .OtherDayTop {
            padding-left: 5px;
            padding-top: 5px;
            text-align: left;
            background-color: #ffffff;
            font-size: 12px;
            font-family: Arial;
            color: Gray;
            vertical-align: top;
        }

        .OtherMonthDayTop {
            padding-left: 5px;
            padding-top: 5px;
            text-align: left;
            background-color: #F2F2F2;
            font-size: 12px;
            font-family: Arial;
            color: Gray;
            vertical-align: top;
        }

        .BottomRightLink {
            background-color: #0299C6;
            color: #ffffff;
            width: 30px;
            height: 30px;
            cursor: pointer;
        }
    </style>

    <script type="text/javascript">

        function SetWeekView(SourceDate) {
            document.getElementById('hfSourceDate').value = SourceDate;
            var clickButton = document.getElementById('btnRefresh');
            clickButton.click();
            //$("#btnRefresh").trigger("click");
        }

    </script>
    <script type="text/javascript">

        $(document).ready(function () {

            $('.columnspan').on("dragstart", function (event) {
                var dt = event.originalEvent.dataTransfer;
                dt.setData('Text', $(this).attr('id'));
                $(this).css('height', 30);

            });
            //eachdropcell
            //.detailtable td
            $('.eachdropcell').on("dragenter dragover drop", function (event) {


                event.preventDefault();

                if (event.type === 'drop' &&
                    (event.target == '[object HTMLTableCellElement]'
                        || event.target == '[object HTMLTableDataCellElement]')) {


                    var data = event.originalEvent.dataTransfer.getData('Text');
                    event.target.appendChild(document.getElementById(data));
                    $('#' + data).css('height', 50);
                    //var tdTarget = document.getElementById(data);
                    var recordid = $('#' + data).attr('recordid');
                    //var namefield = $('#' + data).attr('namefield');
                    var namefieldvalue = $('#' + data).attr('namefieldvalue');
                    var divContentid = $('#' + data).attr('id');
                    var hostcell = event.target;
                    var hostnamefieldvalue = $(hostcell).attr('hostnamefieldvalue');
                    var hoststarttime = $(hostcell).attr('hoststarttime');
                    var sDocumentSectionID = document.getElementById("hfDocumentSectionID").value;
                    $.ajax({
                        url: '<%=ResolveUrl("~/Pages/Record/ColumnRecordRes.ashx")%>?hostcellid=' + event.target.id + '&DocumentSectionID=' + sDocumentSectionID
                        + '&recordid=' + recordid + '&namefieldvalue=' + namefieldvalue
                        + '&divContentid=' + divContentid
                        + '&hostnamefieldvalue=' + hostnamefieldvalue + '&hoststarttime=' + hoststarttime,
                        cache: false,
                        success: function (content) {
                            if (content == 'False') {
                                // alert('Error!');
                            }
                            else { //alert('good');
                                document.getElementById("btnRefreshDayView").click();
                            }
                        },
                        error: function (a, b, c) {
                            //alert('Error!');
                        }
                    });



                }
                else if (event.type === 'drop') {
                    var data = event.originalEvent.dataTransfer.getData('Text');
                    $('#' + data).css('height', 50);
                    alert('Please drop into an empty cell or top white space.');
                    //alert(event.target);
                };


            });
        })
    </script>


    <asp:HiddenField runat="server" ID="hfDocumentSectionID" ClientIDMode="Static" />

    <asp:HiddenField runat="server" ID="hfSourceDate" ClientIDMode="Static" />
    <asp:Button ID="btnRefreshDayView" runat="server" ClientIDMode="Static" OnClick="btnRefreshDayView_Click"
         Style="display: none;" />
    <asp:Button ID="btnRefresh" runat="server" ClientIDMode="Static" OnClick="btnRefresh_Click"
        Style="display: none;" />
    <div style="overflow-x: auto; overflow-y: hidden;" class="section-calendar">
        <table width="100%"  class="section-calendar-table">
            <tr runat="server" id="trHeading">
                <td>
                    <table width="100%" runat="server" id="tblHeading" >
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblHeading" Text="" Font-Bold="true" Style="font-size: 20px; color: Black; font-family: Verdana;"></asp:Label>
                            </td>
                            <td align="right" style="padding-right: 10px;">
                                <asp:HyperLink runat="server" ID="hlAddNewRecord" ToolTip="Add New Record" Visible="false">
                                    <asp:Image runat="server" ID="imgAddNewRecord" ImageUrl="~/App_Themes/Default/images/BigAdd.png" />
                                </asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>

            </tr>
            <tr>
                <td>
                    <div>
                        <table runat="server" id="tblMainTable" width="100%">
                            <tr>
                                <td>
                                    <table width="100%" runat="server" id="tblNavigation">
                                        <tr>
                                            <td style="width: 25%;">
                                                <asp:LinkButton runat="server" ID="lnkPre" OnClick="lnkPre_OnClick" ToolTip="Go to the previous month">
                                                       <img src='../../Images/CalenderLeft.png' border='0'/>
                                                </asp:LinkButton>
                                            </td>
                                            <td style="width: 50%; text-align: center;">
                                                <asp:Label runat="server" CssClass="TitleStyle" ID="lblTitle"></asp:Label>
                                            </td>
                                            <td style="width: 20%;" align="right">
                                                <%--<asp:LinkButton runat="server" ID="lnkMonthWeekView" Style="font-family: Verdana; font-size: 11pt;"
                                                        ForeColor="Blue" OnClick="lnkMonthWeekView_OnClick">Week View</asp:LinkButton>--%>
                                                <asp:DropDownList runat="server" ID="ddlCalendarView" CssClass="NormalTextBox"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlCalendarView_SelectedIndexChanged">
                                                    <asp:ListItem Value="d" Text="Day"></asp:ListItem>
                                                    <asp:ListItem Value="w" Text="Week"></asp:ListItem>
                                                    <asp:ListItem Value="m" Text="Month"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="right" style="width: 5%;">
                                                <asp:LinkButton runat="server" ID="lnkNext" OnClick="lnkNext_OnClick" ToolTip="Go to the next month">
                                                       <img src='../../Images/CalenderRight.png' border='0'/>
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr runat="server" id="trCalendar">
                                <td>
                                    <asp:Calendar runat="server" ID="cldDate" Width="100%" OnDayRender="cldDate_DayRender"
                                        TitleStyle-CssClass="TitleStyle" DayHeaderStyle-CssClass="DayHeaderStyle" DayStyle-CssClass="DayStyle"
                                        NextPrevStyle-CssClass="NextPrevStyle" OtherMonthDayStyle-CssClass="OtherMonthDayStyle"
                                        SelectedDayStyle-CssClass="SelectedDayStyle" SelectorStyle-CssClass="SelectorStyle"
                                        TodayDayStyle-CssClass="TodayDayStyle" WeekendDayStyle-CssClass="WeekendDayStyle"
                                        CssClass="maincalender" OnSelectionChanged="cldDate_SelectionChanged" DayNameFormat="Full"
                                        BorderStyle="Solid" BorderWidth="1px" ForeColor="Black" SelectedDayStyle-BackColor="ActiveBorder"
                                        SelectedDayStyle-ForeColor="Black" ShowGridLines="true" OnVisibleMonthChanged="cldDate_VisibleMonthChanged">
                                        <DayHeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" Height="30px" ForeColor="White" />
                                        <TitleStyle CssClass="TitleStyle" BackColor="White" Height="0px" />
                                    </asp:Calendar>

                                </td>
                            </tr>
                            <tr runat="server" id="trDayCalendar">
                                <td>
                                    <div>
                                        <table runat="server" id="tblDetailTable" visible="true" cellpadding="0" cellspacing="0"
                                            class="detailtable">
                                        </table>
                                    </div>

                                </td>
                            </tr>
                        </table>


                    </div>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>



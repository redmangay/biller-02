﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true" EnableEventValidation="false"
    CodeFile="EachLocation.aspx.cs" Inherits="Pages_DocGen_EachLocation" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="server">
    <script type="text/javascript" src="<%=Request.Url.Scheme+@"://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCgc_Iim3AMzz7jYQ8bdqVKh1mnGxz7Y88&callback=initMap"%>"></script>
     <asp:Panel runat="server" ClientIDMode="Static" ID="pnlFullMap">
        <asp:HiddenField ID="hfMaptype" runat="server" Value="roadmap" ClientIDMode="Static" />
        <asp:HiddenField ID="hfLayout" runat="server" Value="1" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hfCentreLat" Value="-33.87365" ClientIDMode="Static"></asp:HiddenField>
        <asp:HiddenField runat="server" ID="hfCentreLong" Value="151.20688960000007" ClientIDMode="Static"></asp:HiddenField>
        <asp:HiddenField runat="server" ID="hfForceMapCenter" Value="no" ClientIDMode="Static"></asp:HiddenField>
        <asp:HiddenField runat="server" ID="hfOtherZoomLevel" Value="4" ClientIDMode="Static"></asp:HiddenField>
        <asp:HiddenField runat="server" ID="hfDocumentSectionID" ClientIDMode="Static"></asp:HiddenField>
          <asp:HiddenField runat="server" ID="hfDomainName" ClientIDMode="Static"></asp:HiddenField>
        <%--<asp:HiddenField runat="server" ID="hfSearchTableID" ClientIDMode="Static"></asp:HiddenField>--%>
        <asp:HiddenField runat="server" ID="hfAutoScale" Value="0" ClientIDMode="Static"></asp:HiddenField>
        <%-- KG 19/10/17 Ticket 3154 
            <asp:HiddenField runat="server" ID="hfMapDefaultTableID" Value="-1" ClientIDMode="Static"></asp:HiddenField>
        --%>
        <%--<asp:HiddenField runat="server" ID="hfGunPoints" ClientIDMode="Static" />--%>



        <asp:Panel runat="server" ClientIDMode="Static" ID="divMAP">
            <div id="map_canvas" style="height: 450px; width: 100%;" runat="server" clientidmode="Static">
            </div>
            
            <table>
            
                <tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td>
                                     <asp:CheckBox runat="server" ID="chkStaff" TextAlign="Right" Text="Staff" ClientIDMode="Static" onclick="ShowLocations()" />
                                </td>
                                <td style="padding-left:20px;">
                                     <asp:CheckBox runat="server" ID="chkOpenTasks" TextAlign="Right" Text="Open Tasks"  ClientIDMode="Static" onclick="ShowLocations()" />
                                </td>
                                <td style="padding-left:20px;">
                                    <asp:CheckBox runat="server" ID="chkCompletedTasks" TextAlign="Right" Text="Completed Tasks"  ClientIDMode="Static" onclick="ShowLocations()" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    
                </tr>
                <tr>
                    <td colspan="3">
                        <div id="divDetailinfo"  style="display:none;height:200px; border-style:solid; border-color:gray; padding:20px;">
                            
                        </div>
                    </td>
                </tr>
                    <%--<tr>
                    <td valign="top">
                        <asp:DropDownList runat="server" ID="ddlTableMap" DataTextField="TableName" ClientIDMode="Static"
                            DataValueField="TableID" CssClass="NormalTextBox" onchange="ShowSSWhenClicked()">
                        </asp:DropDownList>
                    </td>
                    <td valign="top">
                        <table runat="server" id="trMapSearch" visible="false" clientidmode="Static" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Panel runat="server" ID="pnlCustomSearch">
                                    </asp:Panel>
                                </td>
                                <td valign="top">
                                    <asp:TextBox runat="server" ID="txtMapSearch" CssClass="NormalTextBox" Width="200px" ClientIDMode="Static"></asp:TextBox>
                                </td>
                                <td valign="top">
                                    <asp:LinkButton runat="server" ID="lnkMapSearch" OnClientClick="ShowLocations();return false;">Search</asp:LinkButton>
                                </td>

                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                       
                        <div id="divChkShowRing">
                            <asp:CheckBox runat="server" ClientIDMode="Static" ID="chkShowDisRings" Text="Show distance rings" TextAlign="Right" Font-Size="Small" onclick="showRingHandler(false)" />
                        </div>
                      
                    </td>
                </tr>--%>

            </table>
        </asp:Panel>
   </asp:Panel>
   
    <script type="text/javascript">
        var map;
        var marker;
        var markersArray = [];
        var markerRingArray = [];
        var ringsArray = [];
        var onStartArray = [];
        var markersCArray = [];
        var line = [];
        var divDetailinfo = document.getElementById("divDetailinfo");
        var sDocumentSectionID = document.getElementById('hfDocumentSectionID').value;
        var sIFrameName = 'iframe' + sDocumentSectionID;
        //KG 19/10/17 Ticket 3154
        $(document).ready(function () {
            //    $("#ddlTableMap").val($("#hfMapDefaultTableID").val());
            //$('#divChkShowRing').hide();
        });


        function GetMapTypeID() {
            if (document.getElementById("hfMaptype").value == 'hybrid') {
                return google.maps.MapTypeId.HYBRID;

            }

            if (document.getElementById("hfMaptype").value == 'terrain') {
                return google.maps.MapTypeId.TERRAIN;

            }
            if (document.getElementById("hfMaptype").value == 'satellite') {
                return google.maps.MapTypeId.SATELLITE;

            }
            if (document.getElementById("hfMaptype").value == 'roadmap') {
                return google.maps.MapTypeId.ROADMAP;

            }

            return google.maps.MapTypeId.HYBRID;
        }

        function initialize() {

            var mapType = GetMapTypeID();
            var myOptions = {
                zoom: parseFloat(document.getElementById('hfOtherZoomLevel').value),
                maxZoom: 20, scrollwheel: false,
                center: new google.maps.LatLng(document.getElementById('hfCentreLat').value, document.getElementById('hfCentreLong').value),
                mapTypeId: mapType,
                optimized: false
            };
            if (document.getElementById('hfLayout').value == '1') {
                map = new google.maps.Map(document.getElementById('map_canvas'),
         myOptions);
            }
            else {
                map = new google.maps.Map(document.getElementById('map_canvas'),
         myOptions);
            }

            // Resize stuff...
            google.maps.event.addDomListener(window, "resize", function () {
                var center = map.getCenter();
                google.maps.event.trigger(map, "resize");
                map.setCenter(center);
            });           

        }
        var hfForceMapCenter = document.getElementById('hfForceMapCenter');

        google.maps.event.addDomListener(window, 'load', initialize);


        var bounds = new google.maps.LatLngBounds();


        function addMarker(lat, lng, no, image, Location, url, ssid, mappopup, order, markerIDCount, slable, userid, pintype,recordid) {

            var bAdd = true;           

            if (bAdd) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    map: map,
                    icon: image,
                    title: Location,
                    optimized: false,
                    id: "marker" + markerIDCount,
                    zIndex: google.maps.Marker.MAX_ZINDEX - order,
                    customInfo: userid,
                    label: slable
                });
                //marker.setTag(ssid);
                markersArray.push(marker);

                var ll = new google.maps.LatLng(lat, lng);
                bounds.extend(ll);
                if (document.getElementById('hfForceMapCenter').value == 'no'
                    || document.getElementById('hfAutoScale').value == '1') {
                    map.fitBounds(bounds);
                }

                // setssinfo(Location, marker, url, ssid);
                setinfo(mappopup, marker, url, userid, pintype, recordid);
            }


        };


               
        function setinfo(mappopup, marker, url, userid, pintype, recordid) {
            //var infowindow = new google.maps.InfoWindow({
            //    content: '<div > <strong><a target="_parent" style="text-decoration: none !important;color: inherit;" href="' + url + '">' + mappopup + '</a></strong></div>'
            //});
            //var infowindow = new google.maps.InfoWindow({
            //    content: '<div > ' + mappopup + '</div>'
            //});


            if (pintype == 's') {
                google.maps.event.addListener(marker, 'click', function () {    

                    

                    var sStaffPaleImage = document.getElementById("hfDomainName").value + "/Pages/Record/PinImages/Staff_pale.png";
                    var sStaffImage = document.getElementById("hfDomainName").value + "/Pages/Record/PinImages/Staff.png";

                    var sOpenTaskPaleImage = document.getElementById("hfDomainName").value + "/Pages/Record/PinImages/OpenTask_pale.png";
                    var sOpenTaskImage = document.getElementById("hfDomainName").value + "/Pages/Record/PinImages/OpenTask.png";

                    var sCompletedTaskPaleImage = document.getElementById("hfDomainName").value + "/Pages/Record/PinImages/CompletedTask_pale.png";
                    var sCompletedTaskImage = document.getElementById("hfDomainName").value + "/Pages/Record/PinImages/CompletedTask.png";


                    if (markersArray) {
                        for (var i = 0; i < markersArray.length; i++) {
                            if (markersArray[i].icon.indexOf("Staff.png") > 0) {
                                markersArray[i].setIcon(sStaffPaleImage);
                            }
                            if (markersArray[i].icon.indexOf("CompletedTask") > 0) {
                                if (markersArray[i].customInfo != userid) {
                                    markersArray[i].setIcon(sCompletedTaskPaleImage);
                                }
                                else {
                                    markersArray[i].setIcon(sCompletedTaskImage);
                                }
                                
                            } 
                            if (markersArray[i].icon.indexOf("OpenTask") > 0) {
                                if (markersArray[i].customInfo != userid) {
                                    markersArray[i].setIcon(sOpenTaskPaleImage);
                                }
                                else {
                                    markersArray[i].setIcon(sOpenTaskImage);
                                }

                                
                            } 
                        }
                    }
                    marker.setIcon(sStaffImage);

                    var d = new Date();
                    //alert(marker.customInfo);
                    $.ajax({
                        url: '../../GetLocation.ashx?userpath=yes&userid=' + userid + '&DocumentSectionID=' + document.getElementById('hfDocumentSectionID').value + '&t=' + d,
                        dataType: 'json',
                        success: function (res) {
                            var points = res;

                            if (points != null) {
                                //// markersArray = []; //By JV - Clear marker array before re adding markers.
                                //// markerRingArray = []; //By JV - Clear marker ring array before re adding markers rings.
                                //// onStartArray = [];
                                document.getElementById("divDetailinfo").innerHTML = points[0].mappopup;

                                var markerCIDCount = 0;
                                //if (markersArray) {
                                //    for (i in markersArray) {
                                //        if (markersArray[i].id != marker.id) {
                                //            markersArray[i].setMap(null);
                                //        }
                                //    }
                                //}

                                if (markersCArray) {
                                    for (i in markersCArray) {
                                        markersCArray[i].setMap(null);
                                    }
                                }
                                markersCArray = [];
                                //line = [];
                                for (i = 0; i < line.length; i++) {
                                    line[i].setMap(null); //or line[i].setVisible(false);
                                }


                                for (var i = 0; i < points.length; i++) {
                                    var lat = points[i].lat;
                                    var lon = points[i].lon;
                                    markerCIDCount++;
                                   
                                    addCircleMarker(lat, lon, i, points[i].pin, points[i].title, points[i].url, points[i].ssid, points[i].mappopup, points[i].order, markerCIDCount);

                                    if (i > 0) {
                                        var eachline = new google.maps.Polyline({
                                            path: [
                                                new google.maps.LatLng(points[i - 1].lat, points[i - 1].lon),
                                                new google.maps.LatLng(points[i].lat, points[i].lon)
                                            ],
                                            strokeColor: "#511BE9",
                                            strokeOpacity: 1.0,
                                            strokeWeight: 2,
                                            map: map
                                        });
                                        line.push(eachline);
                                    }

                                    //addMarker(lat, lon, i, points[i].pin, points[i].title, points[i].url, points[i].ssid, points[i].mappopup, points[i].order, markerIDCount);
                                    //markerRingArray.push(points[i].rings);
                                    //onStartArray.push(points[i].onStart)
                                }
                                //showRingHandler(false);
                            }

                        },
                        error: function (xhr, err) {
                            //alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                            //                           alert("responseText: " + xhr.responseText);
                        }
                    });
                    divDetailinfo.style.display = 'block';
                    parent.autoResize(sIFrameName);

                });
            }
            else {
                google.maps.event.addListener(marker, 'click', function () {
                           
                    //if (markersCArray) {
                    //    for (var i = 0; i < markersCArray.length; i++) {
                    //        markersCArray[i].setMap(null);
                    //    }
                    //}
                    //markersCArray = [];
                    //for (i = 0; i < line.length; i++) {
                    //    line[i].setMap(null); //or line[i].setVisible(false);
                    //}
                    //line = [];

                    var d = new Date();
                    //alert(marker.customInfo);
                    $.ajax({
                        url: '../../GetLocation.ashx?ontask=yes&taskrecordid=' + recordid + '&DocumentSectionID=' + document.getElementById('hfDocumentSectionID').value + '&t=' + d,
                        dataType: 'json',
                        success: function (res) {
                            var points = res;
                            if (points != null) {
                                document.getElementById("divDetailinfo").innerHTML = points[0].mappopup;
                            }
                            //var divHTML = res;                           

                            //document.getElementById("divDetailinfo").innerHTML = divHTML;
                            //$('#divDetailinfo').html(divHTML);
                            //alert(recordid);
                            //alert(res);

                        },
                        error: function (xhr, err) {
                            //alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                            //                           alert("responseText: " + xhr.responseText);
                            var responseTitle = $(xhr.responseText).filter('title').get(0);
                            alert($(responseTitle).text() + "\n" + formatErrorMessage(xhr, err)); 
                        }
                    });
                    
                    divDetailinfo.style.display = 'block';
                    parent.autoResize(sIFrameName);
                    //setTimeout(function () { parent.autoResize(sIFrameName); }, 250); 
                });
            }
           

        }
                

        google.maps.Map.prototype.clearOverlays = function () {
            if (markersArray) {
                for (var i = 0; i < markersArray.length; i++) {
                    markersArray[i].setMap(null);
                }
            }
            if (markersCArray) {
                for (var i = 0; i < markersCArray.length; i++) {
                    markersCArray[i].setMap(null);
                }
            }
        }

       

        function ShowLocations() {
            bounds = new google.maps.LatLngBounds();
            if (document.getElementById('hfForceMapCenter').value == 'no') {

                if (map != null) {
                    map.clearOverlays();
                }
            }
            if (document.getElementById('hfForceMapCenter').value == 'yes') {

                deleteOverlays();
            }
            divDetailinfo.style.display = 'none';
            setTimeout(function () { parent.autoResize(sIFrameName); }, 500);  
            //var TableID;
            //if (document.getElementById('hfLayout').value == '1') {
            //    TableID = parseInt($("#ddlTableMap").val());
            //}
            //else {
            //    TableID = parseInt($("#ddlTableMap2").val());
            //}
                     
            //var trMapSearch = document.getElementById('trMapSearch');

            //if (document.getElementById('hfSearchTableID').value == TableID) {
            //    if (trMapSearch != null) {
            //        trMapSearch.style.display = 'block';
            //    }
            //}
            //else {
            //    if (trMapSearch != null) {
            //        trMapSearch.style.display = 'none';
            //    }
            //}

            //if (TableID > -2) {
                //KG 27/10/17 - Ticket 3154 – Rings didn’t disappear
                //if (TableID == 0)
                //    showRingHandler(false);

                var d = new Date();               

                var chkStaff = document.getElementById("chkStaff");
                var chkOpenTasks = document.getElementById("chkOpenTasks");
            var chkCompletedTasks = document.getElementById("chkCompletedTasks");

            if (chkStaff.checked == false && line != null) {
                for (i = 0; i < line.length; i++) {
                    line[i].setMap(null); //or line[i].setVisible(false);
                }
            }


                $.ajax({
                    url: '../../GetLocation.ashx?ontask=yes&Staff=' + chkStaff.checked + '&OpenTasks=' + chkOpenTasks.checked
                        + '&CompletedTasks=' + chkCompletedTasks.checked  + '&DocumentSectionID=' + document.getElementById('hfDocumentSectionID').value + '&t=' + d,
                    dataType: 'json',
                    success: function (res) {
                        var points = res;

                        if (points != null) {
                            markersArray = []; //By JV - Clear marker array before re adding markers.
                            markerRingArray = []; //By JV - Clear marker ring array before re adding markers rings.
                            onStartArray = [];
                            var markerIDCount = 0;
                            for (var i = 0; i < points.length; i++) {
                                var lat = points[i].lat;
                                var lon = points[i].lon;
                                markerIDCount++;
                                addMarker(lat, lon, i, points[i].pin, points[i].title, points[i].url, points[i].ssid, points[i].mappopup, points[i].order, markerIDCount, points[i].label, points[i].userid, points[i].pintype, points[i].recordid);
                                markerRingArray.push(points[i].rings);
                                onStartArray.push(points[i].onStart)
                            }
                            //showRingHandler(true);
                        }

                    },
                    error: function (xhr, err) {
                        //alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
                        //                           alert("responseText: " + xhr.responseText);


                    }
                });

                if (document.getElementById('hfAutoScale').value == '1') {
                    // map.fitBounds(bounds);
                    // map.panToBounds(bounds);
                }
            //}


        }

       
       
       
        // Shows any overlays currently in the array
        function showOverlays() {
            if (markersArray) {
                for (i in markersArray) {
                    markersArray[i].setMap(map);
                }
            }
        }

        // Deletes all markers in the array by removing references to them
        function deleteOverlays() {
            if (markersArray) {
                for (i in markersArray) {
                    markersArray[i].setMap(null);
                }
                markersArray.length = 0;
            }
            if (markersCArray) {
                for (i in markersCArray) {
                    markersCArray[i].setMap(null);
                }
                markersCArray.length = 0;
            }
        }


        if (window.addEventListener)
            window.addEventListener("load", ShowLocations, false);
        else if (window.attachEvent)
            window.attachEvent("onload", ShowLocations);
        else if (document.getElementById)
            window.onload = ShowLocations;





        function addCircleMarker(lat, lng, no, image, Location, url, ssid, mappopup, order, markerIDCount) {

            var bAdd = true;
            var iScale = 3.5;
            if (Location.indexOf("-") > 0) {
                iScale = 7;
            }

            if (bAdd) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    map: map,
                    icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: iScale,
                        fillColor: "#3C87B9",
                        fillOpacity: 0.4,
                        strokeWeight: 0.1
                    },
                    title: Location,
                    optimized: false,
                    id: "circlemarker" + markerIDCount,
                    zIndex: google.maps.Marker.MAX_ZINDEX - order//,
                    //customInfo: ssid
                });
                markersCArray.push(marker);

                var ll = new google.maps.LatLng(lat, lng);
                bounds.extend(ll);
                if (document.getElementById('hfForceMapCenter').value == 'no'
                    || document.getElementById('hfAutoScale').value == '1') {
                    map.fitBounds(bounds);
                }
                //setinfo(mappopup, marker, url);
            }


        };



        //By JV - For Ticket 2566
        function showRingHandler(onLoad) {
            if (ringsArray != null) { ringsArray.forEach(function (element) { element.setMap(null); }) }

            if (document.getElementById("chkShowDisRings") == null) {
                return;
            }

            ringsArray = [];
            if (onLoad) {
                var noFalse = true;
                var noRings = true;
                document.getElementById("chkShowDisRings").checked = false;
                $('#divChkShowRing').show();
                for (var z = 0; z < markersArray.length; z++) {
                    drawRings(z);
                    if (onStartArray[z] == false || onStartArray[z] == null) {
                        noFalse = false;
                    }
                    if (markerRingArray[z] != null) {
                        noRings = false;
                        showRingHandler(false);
                    }
                }
                if (noFalse == true) {
                    document.getElementById("chkShowDisRings").checked = true;
                }
                if (noRings == true) {
                    $('#divChkShowRing').hide();
                }
            }
            else {
                for (var i = 0; i < markersArray.length; i++) {
                    if (document.getElementById("chkShowDisRings").checked == true) {
                        onStartArray[i] = true;
                    }
                    else {
                        onStartArray[i] = false;
                    }
                    drawRings(i);
                }
            }
        }
        function drawRings(counter) {
            var ring = null;
            var countRingArr = [];
            if (markerRingArray[counter] != null) {
                countRingArr = markerRingArray[counter].split(',');
            }
            for (var r = 0; r < countRingArr.length; r++) {
                ring = new google.maps.Circle({
                    strokeColor: '#0000FF',
                    strokeOpacity: 0.4,
                    strokeWeight: 3,
                    fillColor: '#0000FF',
                    fillOpacity: 0.11,
                    map: onStartArray[counter] == true ? map : null,
                    center: markersArray[counter].getPosition(),
                    radius: countRingArr[r] * 1000,
                    markerID: markersArray[counter].id
                });

                ringsArray.push(ring);
            }
        }
        //End Here
        function ShowSSWhenClicked() {
            //               var chk;
            //               if (document.getElementById('hfLayout').value == '1') {
            //                   chk = document.getElementById("chkShowWorkSite");
            //               }
            //               else {
            //                   chk = document.getElementById("chkShowWorkSite2");
            //               }
            //               chk.checked = false;
            //KG 20/11/17 Ticket 3154
            $('#divChkShowRing').hide();

            ShowLocations();
            if (document.getElementById('hfForceMapCenter').value == 'yes') {
                map.setCenter(new google.maps.LatLng(document.getElementById('hfCentreLat').value, document.getElementById('hfCentreLong').value));
            }

        }
        //function ShowEContratorLocations() {
        //    bounds = new google.maps.LatLngBounds();
        //    if (document.getElementById('hfForceMapCenter').value == 'no') {
        //        if (map != null) {
        //            map.clearOverlays();
        //        }
        //    }
        //    if (document.getElementById('hfForceMapCenter').value == 'yes') {
        //        deleteOverlays();
        //    }
        //    for (i = 0; i < line.length; i++) {
        //        line[i].setMap(null); //or line[i].setVisible(false);
        //    }
        //    //map.clear();
        //    var d = new Date();
        //    var txtClient = document.getElementById('txtClient');
        //    var txtJob = document.getElementById('txtJob');
        //    var txtContrator = document.getElementById('txtContrator');
        //    var txtDate = document.getElementById('txtDate');


        //    $.ajax({
        //        url: '../../GetLocation.ashx?econtractor=yes&sClient=' + txtClient.value + '&sJob=' + txtJob.value
        //            + '&sContrator=' + txtContrator.value + '&sDate=' + txtDate.value + '&DocumentSectionID=' + document.getElementById('hfDocumentSectionID').value + '&t=' + d,
        //        dataType: 'json',
        //        success: function (res) {
        //            var points = res;

        //            if (points != null) {
        //                markersArray = []; //By JV - Clear marker array before re adding markers.
        //                //markerRingArray = []; //By JV - Clear marker ring array before re adding markers rings.
        //                //onStartArray = [];
        //                var markerIDCount = 0;
        //                for (var i = 0; i < points.length; i++) {
        //                    var lat = points[i].lat;
        //                    var lon = points[i].lon;
        //                    markerIDCount++;
        //                    addMarker(lat, lon, i, points[i].pin, points[i].title, points[i].url, points[i].ssid, points[i].mappopup, points[i].order, markerIDCount, points[i].label, points[i].userid, points[i].pintype, points[i].recordid);

        //                    //markerRingArray.push(points[i].rings);
        //                    //onStartArray.push(points[i].onStart)
        //                }
        //                //showRingHandler(true);
        //            }

        //        },
        //        error: function (xhr, err) {
        //            //alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status);
        //            //                           alert("responseText: " + xhr.responseText);
        //        }
        //    });

        //    if (document.getElementById('hfAutoScale').value == '1') {
        //        // map.fitBounds(bounds);
        //        // map.panToBounds(bounds);
        //    }
        //}

        //var txtClient = document.getElementById('txtClient');

        //if (txtClient == null) {
        //    if (window.addEventListener)
        //        window.addEventListener("load", ShowLocations, false);
        //    else if (window.attachEvent)
        //        window.attachEvent("onload", ShowLocations);
        //    else if (document.getElementById)
        //        window.onload = ShowLocations;
        //}
        //else {
        //    if (window.addEventListener)
        //        window.addEventListener("load", ShowEContratorLocations, false);
        //    else if (window.attachEvent)
        //        window.attachEvent("onload", ShowEContratorLocations);
        //    else if (document.getElementById)
        //        window.onload = ShowEContratorLocations;
        //}



    </script>
</asp:Content>


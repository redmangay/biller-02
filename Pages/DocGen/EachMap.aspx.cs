﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DocGen.DAL;
public partial class Pages_DocGen_EachMap : SecurePage
{
    bool _bHaveService = false;
    string _strCustomServiceJSCode = "";
    DocGen.DAL.DocumentSection section = null;
    DocGen.DAL.MapSectionDetail mapDetail = null;
    int _DocumentSectionID = 0;
    protected override void OnPreInit(EventArgs e)
    {
        if (Common.GetValueFromSQL("SELECT TOP 1 ServiceID FROM [Service] WHERE ServiceType='EachMap_aspx' AND AccountID="
            + Session["AccountID"].ToString()) != "")
        {
            _bHaveService = true;
        }

    }


    protected void ProcessServiceForEvent(string strEventName, object sender, EventArgs e)
    {
        if (Common.GetValueFromSQL("SELECT top 1 ServiceID FROM [Service] WHERE ServiceJSON LIKE '%" +
                strEventName + "%' and ServiceType='EachMap_aspx' AND AccountID=" + Session["AccountID"].ToString()) != "")
        {
            string strEventSql = "";
            strEventSql = "SELECT ServiceID FROM [Service] WHERE ServiceJSON LIKE '%" +
              strEventName + "%' and ServiceType='EachMap_aspx' AND AccountID=" + Session["AccountID"].ToString();

            List<Service> lstService = SystemData.GetServiceList(strEventSql);

            ProcessServices(lstService, sender, e);
        }
    }


    protected void ProcessServices(List<Service> lstService, object sender, EventArgs e)
    {
        List<object> roList = new List<object>();
        if (lstService != null)
        {
            foreach (Service aService in lstService)
            {
                List<object> objList = new List<object>();
                objList.Add(this);
                //aService.Temp_DynamicPartName = _strDynamictabPart;
                objList.Add(aService);
                objList.Add(divMAP);
                objList.Add(sender);

                if(mapDetail!=null)
                {
                    objList.Add(mapDetail);
                }


                if (!string.IsNullOrEmpty(aService.ServiceDefinition))
                {
                    //add more to this
                }
                List<object> roOneList = new List<object>();
                if (!string.IsNullOrEmpty(aService.ServiceJSON))
                {
                    PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(aService.ServiceJSON);
                    if (thePageLifeCycleService != null)
                    {
                        string serviceToExecute = "";
                        System.Reflection.PropertyInfo servicenameProperty = sender.GetType().GetProperty("ServiceName");
                        if (servicenameProperty != null)
                        {
                            serviceToExecute = servicenameProperty.GetValue(sender, null).ToString();
                        }

                        if (thePageLifeCycleService.ServerControlEvent == "ButtonClick" && thePageLifeCycleService.DotNetMethod != serviceToExecute)
                        {
                            continue;
                        }

                        if (!string.IsNullOrEmpty(thePageLifeCycleService.SPName))
                        {
                            roOneList = CustomMethod.DotNetMethod(thePageLifeCycleService.SPName, objList);
                            if (roOneList != null && roOneList.Count > 0)
                            {
                                foreach (object oRO in roOneList)
                                {
                                    ServiceReturnObject(oRO);
                                    roList.Add(oRO);
                                }
                            }
                        }


                        if (!string.IsNullOrEmpty(thePageLifeCycleService.DotNetMethod))
                        {

                            List<object> rodnOneList = CustomMethod.DotNetMethod(thePageLifeCycleService.DotNetMethod, objList);
                            if (rodnOneList != null && rodnOneList.Count > 0)
                            {
                                foreach (object oRO in rodnOneList)
                                {
                                    ServiceReturnObject(oRO);
                                    roList.Add(oRO);
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(thePageLifeCycleService.JavaScriptFunction))
                        {


                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ServiceAutoJS" + aService.ServiceID.ToString(),
                             SystemData.ErrorGuardForServiceJS(thePageLifeCycleService.JavaScriptFunction), true);



                        }
                    }

                }

            }
        }
    }

    protected void ServiceReturnObject(object objR)
    {
        if (objR.GetType().Name == "Service")
        {
            Service rService = (Service)objR;
            if (rService != null)
            {
                if (rService.Temp_ControlEventList != null)
                {
                    List<ControlEvent> lstCtlEvt = rService.Temp_ControlEventList;
                    foreach (ControlEvent aControlEvent in lstCtlEvt)
                    {
                        if (aControlEvent.TheControl.GetType().Name == "LinkButton")
                        {
                            LinkButton lnkCustom = (LinkButton)aControlEvent.TheControl;

                            if (aControlEvent.EventName == "Service_Control_Click")
                                lnkCustom.Click += Service_Control_Click;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(rService.Temp_CustomJSCode))
                {
                    _strCustomServiceJSCode = _strCustomServiceJSCode + rService.Temp_CustomJSCode;
                }
            }
        }

    }

    protected void Service_Control_Click(object sender, EventArgs e)
    {
        try
        {
            if (sender.GetType().Name == "LinkButton")
            {
                LinkButton theButton = (LinkButton)sender;
                if (theButton != null && theButton.CommandArgument != "")
                {
                    Service theService = SystemData.Service_Detail(int.Parse(theButton.CommandArgument));

                    PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theService.ServiceJSON);
                    thePageLifeCycleService.ServerControlEvent = "Service_Control_Click";
                    theService.ServiceJSON = thePageLifeCycleService.GetJSONString();
                    List<Service> lstService = new List<Service>();
                    lstService.Add(theService);
                    ProcessServices(lstService, sender, e);
                }
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Button Column type click ", ex.Message, ex.StackTrace, DateTime.Now, Request.RawUrl);
            SystemData.ErrorLog_Insert(theErrorLog);
        }

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        if (_bHaveService)
        {

            ProcessServiceForEvent("ASP_Page_PreRender", sender, e);
        }


        if (_strCustomServiceJSCode != "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "_strCustomServiceJSCode", _strCustomServiceJSCode, true);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        //if (_bHaveService)
        //{

        //    ProcessServiceForEvent("ASP_Page_Start_Init", sender, e);
        //}
        if (Request.QueryString["DocumentSectionID"] != null)
        {
            Int32.TryParse(Convert.ToString(Request.QueryString["DocumentSectionID"]), out _DocumentSectionID);
        }
        using (DocGen.DAL.DocGenDataContext ctx = new DocGen.DAL.DocGenDataContext())
        {
            section = ctx.DocumentSections.SingleOrDefault<DocGen.DAL.DocumentSection>(s => s.DocumentSectionID == _DocumentSectionID);
            if (section != null)
            {
                if (section.Details != "")
                {
                    mapDetail = DocGen.DAL.JSONField.GetTypedObject<DocGen.DAL.MapSectionDetail>(section.Details);
                }
            }
        }

        if (mapDetail.CustomService == null || (mapDetail.CustomService != null && (bool)mapDetail.CustomService==false))
        {
            _bHaveService = false;
        }

        if (_bHaveService)
        {

            ProcessServiceForEvent("ASP_Page_Init", sender, e);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //hfGunPoints.Value = Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Images/gun_points.png";

                ////KG 19/10/17 - Ticket 3154 - Load Account Default Location
                //Account theAccount = SecurityManager.Account_Details((int)Session["AccountID"]);
                //if (theAccount.MapDefaultTableID != null)
                //{
                //     hfMapDefaultTableID.Value = theAccount.MapDefaultTableID.ToString();
                //}

                PopulateAllTableDDL();


                if (Request.QueryString["DocumentSectionID"] != null)
                {
                    // Int32.TryParse(Convert.ToString(Request.QueryString["DocumentSectionID"]), out _DocumentSectionID);
                }
                else
                {
                    //if (Request.QueryString["mobile"] != null)
                    //{
                    //    map_canvas.Style.Add("width", "250px");
                    //    map_canvas.Style.Add("height", "250px");
                    //    return;
                    //}
                }


                if (_DocumentSectionID <= 0)
                {
                    if (Request.QueryString["PrevID"].ToString() != "-1")
                    {
                        //hfRemoveSection.Value = "1";
                    }

                    //Response.Redirect("../Summary.aspx");
                }
                else
                {


                    //mapDetail = DocGen.DAL.JSONField.GetTypedObject<DocGen.DAL.MapSectionDetail>(section.Details);
                    if (mapDetail != null)
                    {
                        hfDocumentSectionID.Value = _DocumentSectionID.ToString();
                        if (mapDetail.MapTypeId != null && mapDetail.MapTypeId != "")
                        {
                            hfMaptype.Value = mapDetail.MapTypeId;
                        }

                        if (mapDetail.Latitude != null)
                        {
                            hfCentreLat.Value = mapDetail.Latitude.ToString();
                            hfForceMapCenter.Value = "yes";
                        }

                        if (mapDetail.Longitude != null)
                        {
                            hfCentreLong.Value = mapDetail.Longitude.ToString();
                        }

                        if (mapDetail.MapScale != null)
                        {
                            hfOtherZoomLevel.Value = mapDetail.MapScale.ToString();
                        }

                        if (mapDetail.ShowLocation != null)
                            ddlTableMap.SelectedValue = mapDetail.ShowLocation.ToString();

                        //if (mapDetail.Width != null)
                        //    map_canvas.Style.Add("max-width", mapDetail.Width.ToString() + "px");
                        if (mapDetail.Height != null)
                            map_canvas.Style.Add("height", mapDetail.Height.ToString() + "px");


                        if (mapDetail.Latitude != null && mapDetail.Longitude != null)
                        {
                            hfForceMapCenter.Value = "yes";
                        }
                        else
                        {
                            hfForceMapCenter.Value = "no";
                        }

                        if (mapDetail.SearchColumnID != null)
                        {
                            trMapSearch.Visible = true;
                            Column cSearchColumn = RecordManager.ets_Column_Details((int)mapDetail.SearchColumnID);
                            if (cSearchColumn != null)
                            {
                                hfSearchTableID.Value = cSearchColumn.TableID.ToString();
                                //lblSearchBy.Text = cSearchColumn.DisplayName;

                            }
                        }
                        if (mapDetail.AutoScale != null && (bool)mapDetail.AutoScale)
                        {
                            hfAutoScale.Value = "1";
                            hfOtherZoomLevel.Value = "18";
                        }


                    }


                }




            }

            if (_bHaveService)
            {

                ProcessServiceForEvent("ASP_Page_Load", sender, e);
            }

        }
        catch (Exception ex)
        {
            //
        }
        //cat

    }


    protected void PopulateAllTableDDL()
    {
        int iTN = 0;

        //ddlTableMap.DataSource = RecordManager.ets_Table_Select(null,
        //        null,
        //        null,
        //        int.Parse(Session["AccountID"].ToString()),
        //        null, null, true,
        //        "st.TableName", "ASC",
        //        null, null, ref  iTN, Session["STs"].ToString()); ;

        DataTable dtTableMap = Common.DataTableFromText(@"
                      SELECT DISTINCT  [Table].TableName, [Table].TableID
                      FROM [Column] INNER JOIN
                            [Table] ON [Column].TableID = [Table].TableID 
                      WHERE [Column].ColumnType='location' AND [Table].IsActive=1  
                            AND [Table].AccountID=" + Session["AccountID"].ToString());


        ddlTableMap.DataSource = dtTableMap;

        ddlTableMap.DataBind();

        if (dtTableMap.Rows.Count >= 1)
        {
            System.Web.UI.WebControls.ListItem liSelect = new System.Web.UI.WebControls.ListItem("All", "-1");
            ddlTableMap.Items.Insert(0, liSelect);

            //KG 19/10/17 Ticket 3154
            ddlTableMap.Items.Insert(1, new System.Web.UI.WebControls.ListItem("None", "0"));
        }
        else  //if (dtTableMap.Rows.Count == 1 || dtTableMap.Rows.Count==0)
        {
            //ddlTableMap.Visible = false;
            ddlTableMap.Style.Add("display", "none");
        }

    }

}
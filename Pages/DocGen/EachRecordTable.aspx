﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true" CodeFile="EachRecordTable.aspx.cs" Inherits="Pages_DocGen_EachRecordTable" %>
<%@ Register Src="~/Pages/UserControl/RecordList.ascx" TagName="RecordList" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
        <style type="text/css">
                @media (max-width: 1199px) {
                
                /*.gridview_row td:last-child {
                    padding-right:25px;
                }
                .gridview_header th:last-child {
                    padding-right:25px;
                }
                .pagertable td:last-child   {
                    padding-right:25px;
                }*/
                #DivMainContent{
                    padding-right:25px;
                }
                #ctl00_HomeContentPlaceHolder_rlOne_divSearch{
                     margin-right:25px;
                }
            }

        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" Runat="Server">
   <script type="text/javascript">

        /* == Red 10092019: Ticket 4818 == */
        function jsReportShowHideProgress(needpostback) {
            setTimeout(function () { }, 200);
            deleteCookie();

            var timeInterval = 200; // milliseconds (checks the cookie for every half second )

            var loop = setInterval(function () {
                if (IsCookieValid()) {

                    if (needpostback == "yes") { __doPostBack('ctl00$lkChangeAccount', ''); }
                    else { $('.ajax-indicator-full').fadeOut(); }
                    clearInterval(loop)
                }

            }, timeInterval);
        }

        // cookies
        function deleteCookie() {

            var cook = getCookie("ExportReport");
            if (cook != "") {
                document.cookie = "ExportReport=; Path=/; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            }
        }

        function IsCookieValid() {
            var cook = getCookie("ExportReport");
            return cook != '';
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        /* == End Red == */



    </script>
        <asp:RecordList runat="server" ID="rlOne" PageType="p" ShowAddButton="true" ShowEditButton="true" />
   
   
         
        
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocGen.DAL;
using System.Data;
using System.Globalization;
using System.Xml;
using System.IO;
using System.Web.UI.HtmlControls;
using DBG.Common;
public partial class Pages_DocGen_EachCalendar : SecurePage
{
    int _DocumentSectionID = 0;
    DataTable _dtMonthData;
    string _strTableID = "";
    bool _bResponsive = false;
    protected void lnkPre_OnClick(object sender, EventArgs e)
    {
        if (ViewState["MonthDate"] != null && ddlCalendarView.SelectedValue == "m")
        {
            ViewState["MonthDate"] = ((DateTime)ViewState["MonthDate"]).AddMonths(-1);
        }
        else if (ViewState["MonthDate"] != null && ddlCalendarView.SelectedValue == "w")
        {

            ViewState["MonthDate"] = ((DateTime)ViewState["MonthDate"]).AddDays(-7);
            SetMonday();
        }
        else if (ViewState["MonthDate"] != null && ddlCalendarView.SelectedValue == "d")
        {
            ViewState["MonthDate"] = ((DateTime)ViewState["MonthDate"]).AddDays(-1);
        }

       
        if (ddlCalendarView.SelectedValue == "d")
        {
            PopulateDayCalendar();
        }
        else
        {
            cldDate.TodaysDate = (DateTime)ViewState["MonthDate"];
            PopulateCalerdar();
        }


    }
    protected void lnkNext_OnClick(object sender, EventArgs e)
    {
        if (ViewState["MonthDate"] != null && ddlCalendarView.SelectedValue == "m")
        {
            ViewState["MonthDate"] = ((DateTime)ViewState["MonthDate"]).AddMonths(1);

        }
        else if (ViewState["MonthDate"] != null && ddlCalendarView.SelectedValue == "w")
        {

            ViewState["MonthDate"] = ((DateTime)ViewState["MonthDate"]).AddDays(7);
            SetMonday();
        }
        else if (ViewState["MonthDate"] != null && ddlCalendarView.SelectedValue == "d")
        {
            ViewState["MonthDate"] = ((DateTime)ViewState["MonthDate"]).AddDays(1);
        }

       
        if (ddlCalendarView.SelectedValue == "d")
        {
            PopulateDayCalendar();
        }
        else
        {
            cldDate.TodaysDate = (DateTime)ViewState["MonthDate"];
            PopulateCalerdar();
        }

    }

    //protected void lnkMonthWeekView_OnClick(object sender, EventArgs e)
    //{
    //    if (ViewState["ViewType"].ToString() == "m")
    //    {
    //        ViewState["ViewType"] = "w";
    //        lnkMonthWeekView.Text = "Month View";
    //        SetMonday();

    //    }
    //    else
    //    {
    //        ViewState["ViewType"] = "m";
    //        lnkMonthWeekView.Text = "Week View";
    //    }

    //    PopulateCalerdar();
    //}




    //protected bool IsDataInFilterRange(string strColumnType, string strNumberType, string strRecordValue, 
    //    string strFilterValue, string strCompareOperator)
    //{


    //    try
    //    {
    //        if (strCompareOperator == "")
    //            strCompareOperator = "=";

    //        if (strCompareOperator == "empty")
    //        {
    //            if(strRecordValue=="")
    //            {
    //                return true;
    //            }
    //            else
    //            {
    //                return false;
    //            }
    //        }

    //        if (strCompareOperator == "notempty")
    //        {
    //            if (strRecordValue != "")
    //            {
    //                return true;
    //            }
    //            else
    //            {
    //                return false;
    //            }
    //        }

    //        if (strRecordValue == "")
    //        {
    //            return false;
    //        }

    //        strColumnType = strColumnType.ToLower();
    //        if (strColumnType == "number")
    //        {
    //            if (strFilterValue.IndexOf("____") > -1)
    //            {
    //                int iRecordValue=int.Parse(strRecordValue);
    //                string strLowerLimit = strFilterValue.Substring(0, strFilterValue.IndexOf("____"));
    //                string strUpperLimit = strFilterValue.Substring(strFilterValue.IndexOf("____") + 4);
    //                if (strLowerLimit != "" && strUpperLimit != "")
    //                {

    //                    if(iRecordValue>=int.Parse(strLowerLimit) && iRecordValue<=int.Parse(strUpperLimit))
    //                    {
    //                        return true;
    //                    }                       
    //                }
    //                else
    //                {
    //                     if (strLowerLimit != "" )
    //                     {
    //                         if (strCompareOperator == "=")
    //                         {
    //                             if (iRecordValue == int.Parse(strLowerLimit))
    //                             {
    //                                 return true;
    //                             }
    //                         }
    //                         if (strCompareOperator == ">")
    //                         {
    //                             if (iRecordValue > int.Parse(strLowerLimit))
    //                             {
    //                                 return true;
    //                             }
    //                         }
    //                         if (strCompareOperator == "<")
    //                         {
    //                             if (iRecordValue < int.Parse(strLowerLimit))
    //                             {
    //                                 return true;
    //                             }
    //                         }

    //                         if (strCompareOperator == "<>")
    //                         {
    //                             if (iRecordValue != int.Parse(strLowerLimit))
    //                             {
    //                                 return true;
    //                             }
    //                         }
    //                     }
    //                     else if (strUpperLimit != "")
    //                     {
    //                         if (iRecordValue <= int.Parse(strUpperLimit))
    //                         {
    //                             return true;
    //                         }                           
    //                     }

    //                }

    //            }
    //        }
    //        else if (strColumnType == "datetime" || strColumnType == "date" || strColumnType == "time")
    //        {
    //            if (strFilterValue.IndexOf("____") > -1)
    //            {
    //                DateTime dtRecordValue = DateTime.Parse(strRecordValue);
    //                string strLowerLimit = strFilterValue.Substring(0, strFilterValue.IndexOf("____"));
    //                string strUpperLimit = strFilterValue.Substring(strFilterValue.IndexOf("____") + 4);
    //                strLowerLimit = Common.ReturnDateStringFromToken(strLowerLimit);
    //                strUpperLimit = Common.ReturnDateStringFromToken(strUpperLimit);
    //                if (strLowerLimit != "" && strUpperLimit != "")
    //                {

    //                    if (dtRecordValue >= DateTime.Parse(strLowerLimit) && dtRecordValue <= DateTime.Parse(strUpperLimit))
    //                    {
    //                        return true;
    //                    }
    //                }
    //                else
    //                {
    //                    if (strLowerLimit != "")
    //                    {
    //                        if(strCompareOperator=="=")
    //                        {
    //                            if (dtRecordValue == DateTime.Parse(strLowerLimit))
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                        if (strCompareOperator == ">")
    //                        {
    //                            if (dtRecordValue > DateTime.Parse(strLowerLimit))
    //                            {
    //                                return true;
    //                            }
    //                        }
    //                        if (strCompareOperator == "<")
    //                        {
    //                            if (dtRecordValue < DateTime.Parse(strLowerLimit))
    //                            {
    //                                return true;
    //                            }
    //                        }

    //                        if (strCompareOperator == "<>")
    //                        {
    //                            if (dtRecordValue != DateTime.Parse(strLowerLimit))
    //                            {
    //                                return true;
    //                            }
    //                        }

    //                    }
    //                    else if (strUpperLimit != "")
    //                    {
    //                        if (dtRecordValue <= DateTime.Parse(strUpperLimit))
    //                        {
    //                            return true;
    //                        }
    //                    }

    //                }

    //            }
    //        }
    //        else
    //        {
    //            if (strCompareOperator=="<>")
    //            {
    //                if (strRecordValue != strFilterValue)
    //                {
    //                    return true;
    //                }
    //            }
    //            else
    //            {
    //                if (strRecordValue == strFilterValue)
    //                {
    //                    return true;
    //                }
    //            }


    //        }
    //    }
    //    catch
    //    {
    //        return false;
    //    }



    //    return false;
    //}
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (ViewState["MonthDate"] != null)
            Session["MonthDate" + _DocumentSectionID.ToString()] = (DateTime)ViewState["MonthDate"];

        Session["ViewType" + _DocumentSectionID.ToString()] = ddlCalendarView.Text;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Int32.TryParse(Convert.ToString(Request.QueryString["DocumentSectionID"]), out _DocumentSectionID);
            if (Common.IsResponsive())
            {
                _bResponsive = true;
                if (Responsive.DeviceMaxXAxis() == 1)
                {
                    cldDate.DayNameFormat = DayNameFormat.Short;
                }
            }


            _dtMonthData = new DataTable();
            _dtMonthData.Clear();

            _dtMonthData.Columns.Add("RecordID", typeof(int));
            _dtMonthData.Columns.Add("RecordDate", typeof(DateTime));
            _dtMonthData.Columns.Add("DisplayField", typeof(string));


            string strCellToolTip = @"var mouseX;
                                var mouseY;
                                $(document).mousemove(function (e) {
                                    try
                                    {
                                        mouseX = e.pageX;
                                        mouseY = e.pageY;
                                    }
                                    catch (err)
                                    {
                                       // alert(err.message)
                                    }
        
                                });
    

                                $(function () {
        
                                    $('.js-tooltip-container').hover(function () {
                                        //$(this).find('.js-tooltip').show();
                                        try {
                                            $(this).find('.js-tooltip').addClass('ajax-tooltip');
                                            $(this).find('.ajax-tooltip').css({ 'top': mouseY, 'left': mouseX }).fadeIn('slow');
                                        }
                                        catch (err) {
                                           // alert(err.message);
                                        }
                                    }, function () {
                                        try {
                                            $(this).find('.js-tooltip').hide();
                                            $(this).find('.js-tooltip').removeClass('ajax-tooltip');
                                            $(this).find('.ajax-tooltip').css({ 'top': mouseY, 'left': mouseX }).fadeOut('slow');
                                        }
                                        catch (err) {
                                            //alert(err.message);
                                        }
                                    });
       
                                });";




            ScriptManager.RegisterStartupScript(this, this.GetType(), "strCellToolTip", strCellToolTip, true);



            if (!IsPostBack)
            {
                hfDocumentSectionID.Value = _DocumentSectionID.ToString();
                if (Session["ViewType" + _DocumentSectionID.ToString()] != null && Session["MonthDate" + _DocumentSectionID.ToString()] != null)
                {
                    ddlCalendarView.Text = Session["ViewType" + _DocumentSectionID.ToString()].ToString();
                    ViewState["MonthDate"] = (DateTime)Session["MonthDate" + _DocumentSectionID.ToString()];
                    cldDate.TodaysDate = (DateTime)ViewState["MonthDate"];
                }
                else
                {
                    ddlCalendarView.Text = GetDefaultView();
                    ViewState["MonthDate"] = DateTime.Today;
                }
                
                ddlCalendarView_SelectedIndexChanged(null, null);
            }




        }
        catch (Exception ex)
        {
            //

        }


    }

    protected string GetDefaultView()
    {
        using (DocGen.DAL.DocGenDataContext ctx = new DocGen.DAL.DocGenDataContext())
        {

            DocGen.DAL.DocumentSection section = ctx.DocumentSections.SingleOrDefault<DocGen.DAL.DocumentSection>(s => s.DocumentSectionID == _DocumentSectionID);
            if (section != null)
            {
                CalendarSectionDetail calDetail = JSONField.GetTypedObject<CalendarSectionDetail>(section.Details);
                if (calDetail != null)
                {
                    if (calDetail.CalendarDefaultView != null && calDetail.CalendarDefaultView != "")
                    {
                        if (calDetail.CalendarDefaultView == "week")
                        {
                            return "w";
                        }
                        if (calDetail.CalendarDefaultView == "day")
                        {
                            return "d";
                        }
                        if (calDetail.CalendarDefaultView == "month")
                        {
                            return "m";
                        }
                    }

                }
            }
        }


        return "m";
    }
    protected void SetMonday()
    {
        DateTime input = (DateTime)ViewState["MonthDate"];
        int delta = DayOfWeek.Monday - input.DayOfWeek;
        DateTime monday = input.AddDays(delta);
        if (delta > 0)
        {
            monday = monday.AddDays(-7);
        }
        ViewState["MonthDate"] = monday;
    }



    protected void PopulateCalerdar()
    {
        tblMainTable.Attributes.Add("width", "100%");
        tblHeading.Attributes.Add("width", "100%");
        //_bResponsive &&
        if ( Responsive.DeviceMaxXAxis() < 3)
        {
            int iResponsiveWidth = ((int)Responsive.DeviceWidth() * 85) / 100;
            tblHeading.Attributes.Add("width", iResponsiveWidth.ToString());
            tblNavigation.Attributes.Add("width", iResponsiveWidth.ToString());
            lblTitle.Font.Size = 10;
        }
        else
        {
            lblTitle.Font.Size = 16;
        }

        if (ddlCalendarView.SelectedValue == "m")
        {
            if (_bResponsive && Responsive.DeviceMaxXAxis() == 1)
            {
                lblTitle.Text = ((DateTime)ViewState["MonthDate"]).ToString("MMMM yyyy");
            }
            else
            {
                lblTitle.Text = ((DateTime)ViewState["MonthDate"]).ToString("MMMM yyyy");
            }
           

            lnkNext.ToolTip = "Go to the next month";
            lnkPre.ToolTip = "Go to the previous month";
        }
        else if (ddlCalendarView.SelectedValue == "w")
        {
            //lblTitle.Text="Week " + TheDatabaseS.GetIso8601WeekOfYear((DateTime)ViewState["MonthDate"]);
            lblTitle.Text = ((DateTime)ViewState["MonthDate"]).ToString("dd MMM") + " to " + ((DateTime)ViewState["MonthDate"]).AddDays(6).ToString("dd MMM yyyy");
            lnkNext.ToolTip = "Go to the next week";
            lnkPre.ToolTip = "Go to the previous week";
        }
        else if (ddlCalendarView.SelectedValue == "d")
        {
            return;
        }
       
        using (DocGen.DAL.DocGenDataContext ctx = new DocGen.DAL.DocGenDataContext())
        {

            DocGen.DAL.DocumentSection section = ctx.DocumentSections.SingleOrDefault<DocGen.DAL.DocumentSection>(s => s.DocumentSectionID == _DocumentSectionID);
            if (section != null)
            {
                CalendarSectionDetail calDetail = JSONField.GetTypedObject<CalendarSectionDetail>(section.Details);
                if (calDetail != null)
                {
                    if (calDetail.TableID != null & calDetail.DateFieldColumnID != null)
                    {
                        _strTableID = calDetail.TableID.ToString();

                        Table theTable = RecordManager.ets_Table_Details(int.Parse(_strTableID));
                        if (calDetail.CalendarTitle != null && calDetail.CalendarTitle != "")
                        {
                            lblHeading.Text = calDetail.CalendarTitle;
                        }
                        else
                        {
                            if (theTable != null)
                            {
                                lblHeading.Text = theTable.TableName;
                            }
                        }

                        if (calDetail.ShowAddRecordIcon != null)
                        {
                            if ((bool)calDetail.ShowAddRecordIcon)
                            {
                                hlAddNewRecord.Visible = true;
                                hlAddNewRecord.Target = "_parent";
                                hlAddNewRecord.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?fixedurl=" + Cryptography.Encrypt("~/Default.aspx") + "&stackzero=yes&mode="
                                   + Cryptography.Encrypt("add") + "&TableID=" + Cryptography.Encrypt(_strTableID) + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");
                            }
                        }

                        Column theDateColumn = RecordManager.ets_Column_Details((int)calDetail.DateFieldColumnID);

                        DateTime calMonthDate = (DateTime)ViewState["MonthDate"];
                        DateTime firstDayOfMonth = new DateTime(calMonthDate.Year, calMonthDate.Month, 1);


                        DateTime lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        firstDayOfMonth = firstDayOfMonth.AddDays(-7);
                        lastDayOfMonth = lastDayOfMonth.AddDays(12);

                        if (ddlCalendarView.SelectedValue == "w")
                        {
                            firstDayOfMonth = new DateTime(calMonthDate.Year, calMonthDate.Month, calMonthDate.Day);
                            lastDayOfMonth = firstDayOfMonth.AddDays(8);
                        }
                        string strTextSearch = " CONVERT(Datetime,Record." + theDateColumn.SystemName + ",103) >= CONVERT(Datetime,'" + firstDayOfMonth.ToShortDateString() + "',103)"; ;
                        strTextSearch = strTextSearch + " AND CONVERT(Datetime,Record." + theDateColumn.SystemName + ",103) <= CONVERT(Datetime,'" + lastDayOfMonth.ToShortDateString() + "',103)";


                        if (calDetail.FilterTextSearch != null && calDetail.FilterTextSearch != "")
                        {
                            strTextSearch = calDetail.FilterTextSearch + " AND (" + strTextSearch + ")";
                        }



                        //      DataTable dtRecords = Common.DataTableFromText("SELECT * FROM Record WHERE IsActive=1 AND TableID=" + calDetail.TableID.ToString()
                        //+ " AND " + theDateColumn.SystemName + " IS NOT NULL AND " + strTextSearch);  allcolumns

                        int iTN = 0;
                        //DataTable dtRecords = RecordManager.ets_Record_List((int)calDetail.TableID, null, true, null, null, null, "DBGSystemRecordID", "DESC", null, null, ref iTN, ref iTN, "nonstandard", "",
                        //    strTextSearch, null, null, "", "", "", null);
                        string strReturnSQL = "";
                        Exception exParam = null;
                        DataTable dtRecords = RecordManager.ets_Record_List((int)calDetail.TableID, null, true, null, null, null, "DBGSystemRecordID", "DESC", null, null, ref iTN, ref iTN, "allcolumns", "",
                           strTextSearch, null, null, "", "", "", null, ref strReturnSQL, ref strReturnSQL, ref exParam,false);

                        DataTable dtColumns = Common.DataTableFromText("SELECT ColumnID,SystemName,DisplayName,ColumnType,NumberType,LinkedParentColumnID,TableTableID,DisplayColumn  FROM [Column] WHERE IsStandard=0 AND   TableID="
                              + calDetail.TableID.ToString() + "  ORDER BY DisplayName");

                        DataTable dtColour = null;
                        if (calDetail.TextColourInfo != null && calDetail.TextColourInfo != "")
                        {
                            string strXML = calDetail.TextColourInfo;

                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.LoadXml(strXML);

                            XmlTextReader r = new XmlTextReader(new StringReader(xmlDoc.OuterXml));

                            DataSet ds = new DataSet();
                            ds.ReadXml(r);
                            if (ds.Tables[0] != null)
                            {
                                dtColour = ds.Tables[0];
                                bool bCompareOperatorFound = false;
                                foreach (DataColumn dc in dtColour.Columns)
                                {
                                    if (dc.ColumnName.ToLower() == "CompareOperator".ToLower())
                                    {
                                        bCompareOperatorFound = true;
                                        break;
                                    }
                                }
                                if (bCompareOperatorFound == false)
                                {
                                    dtColour.Columns.Add("CompareOperator");
                                    dtColour.AcceptChanges();
                                }
                            }

                        }

                        DataTable _dtRecordColums = RecordManager.ets_Table_Columns_All(int.Parse(_strTableID));
                        DataTable dtPT = Common.DataTableFromText(@"SELECT distinct TC.ParentTableID,TableName FROM TableChild TC INNER JOIN [Table] T
                                                ON TC.ParentTableID=T.TableID
                                                 WHERE ChildTableID=" + _strTableID); //AND DetailPageType<>'not'

                        if (dtRecords!=null)
                        {
                            foreach (DataRow dr in dtRecords.Rows)
                            {
                                if (dr[theDateColumn.DisplayName] != DBNull.Value)
                                {
                                    string strEachFieldDisplay = calDetail.FieldDisplay;

                                    foreach (DataRow drC in dtColumns.Rows)
                                    {

                                        if (strEachFieldDisplay.IndexOf("TIME[" + drC["DisplayName"].ToString() + "]") > -1)
                                        {
                                            if (dr[drC["DisplayName"].ToString()].ToString().LastIndexOf(" ") > -1)
                                            {
                                                strEachFieldDisplay = strEachFieldDisplay.Replace("TIME[" + drC["DisplayName"].ToString() + "]", "TIME:"
                                                    + dr[drC["DisplayName"].ToString()].ToString().Substring(dr[drC["DisplayName"].ToString()].ToString().LastIndexOf(" ")));
                                            }
                                            else
                                            {
                                                strEachFieldDisplay = strEachFieldDisplay.Replace("TIME[" + drC["DisplayName"].ToString() + "]", "TIME:"
                                                                                             + dr[drC["DisplayName"].ToString()].ToString());
                                            }
                                        }
                                    }


                                    foreach (DataRow drC in dtColumns.Rows)
                                    {
                                        strEachFieldDisplay = strEachFieldDisplay.Replace("[" + drC["DisplayName"].ToString() + "]", dr[drC["DisplayName"].ToString()].ToString());
                                    }



                                    try
                                    {

                                        //Work with 1 top level Parent tables.

                                        if (dtPT.Rows.Count > 0)
                                        {

                                            foreach (DataRow drPT in dtPT.Rows)
                                            {
                                                if (strEachFieldDisplay.IndexOf("[" + drPT["TableName"].ToString() + ":") == -1)
                                                {
                                                    continue;
                                                }

                                                for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                                                {
                                                    //     if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value && _dtRecordColums.Rows[i]["LinkedParentColumnID"] != DBNull.Value
                                                    //&& (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
                                                    //|| _dtRecordColums.Rows[i]["DropDownType"].ToString() == "tabledd")
                                                    // && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
                                                    //&& _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
                                                    //     {
                                                    if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
                                                        && dr.Table.Columns.Contains("**" + _dtRecordColums.Rows[i]["DisplayName"].ToString() + "_ID**")
                                                        && _dtRecordColums.Rows[i]["TableTableID"].ToString() == drPT["ParentTableID"].ToString())
                                                    {
                                                        if (dr["**" + _dtRecordColums.Rows[i]["DisplayName"].ToString() + "_ID**"].ToString() != "")
                                                        {
                                                            Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["LinkedParentColumnID"].ToString()));
                                                            DataTable dtParentRecord = null;
                                                            if (theLinkedColumn.SystemName.ToLower() == "recordid")
                                                            {
                                                                dtParentRecord = Common.DataTableFromText("SELECT * FROM Record WHERE RecordID=" + dr["**" + _dtRecordColums.Rows[i]["DisplayName"].ToString() + "_ID**"].ToString());
                                                            }
                                                            else
                                                            {
                                                                dtParentRecord = Common.DataTableFromText("SELECT * FROM Record WHERE TableID=" + theLinkedColumn.TableID.ToString() + " AND " + theLinkedColumn.SystemName + "='" +
                                                                    dr["**" + _dtRecordColums.Rows[i]["DisplayName"].ToString() + "_ID**"].ToString().Replace("'", "''") + "'");
                                                            }

                                                            if (dtParentRecord.Rows.Count > 0)
                                                            {

                                                                DataTable dtColumnsPT = Common.DataTableFromText(@"SELECT distinct SystemName, TableName + ':' + DisplayName AS DP FROM [Column] INNER JOIN [Table]
                                        ON [Column].TableID=[Table].TableID WHERE  [Column].IsStandard=0 AND  [Column].TableID=" + drPT["ParentTableID"].ToString());
                                                                foreach (DataRow drC in dtColumnsPT.Rows)
                                                                {
                                                                    strEachFieldDisplay = strEachFieldDisplay.Replace("[" + drC["DP"].ToString() + "]", dtParentRecord.Rows[0][drC["SystemName"].ToString()].ToString());

                                                                }
                                                            }
                                                        }
                                                    }
                                                    //}
                                                }
                                            }
                                        }


                                    }
                                    catch
                                    {
                                        //
                                    }




                                    DateTime dtTemp;



                                    if (dtColour != null)
                                    {
                                        foreach (DataRow drC in dtColumns.Rows)
                                        {
                                            foreach (DataRow drTC in dtColour.Rows)
                                            {
                                                if (drC["ColumnID"].ToString() == drTC[0].ToString()
                                                   && drTC[2].ToString() != "" && strEachFieldDisplay != "")
                                                {
                                                    string strCompareOperator = "";

                                                    if (drTC[3] != DBNull.Value)
                                                        strCompareOperator = drTC[3].ToString();

                                                    string strRecordDisplayValue = dr[drC["DisplayName"].ToString()].ToString();

                                                    if (drC["TableTableID"] != DBNull.Value && drC["DisplayColumn"] != DBNull.Value
                                                        && dtRecords.Columns.Contains("**" + drC["DisplayName"].ToString() + "_ID**"))
                                                    {
                                                        if (dr["**" + drC["DisplayName"].ToString() + "_ID**"] != DBNull.Value)
                                                        {
                                                            strRecordDisplayValue = dr["**" + drC["DisplayName"].ToString() + "_ID**"].ToString();
                                                        }

                                                    }

                                                    if (TheDatabase.IsDataInFilterRange(drC["ColumnType"].ToString(), drC["NumberType"].ToString(),
                                                        strRecordDisplayValue, drTC[1].ToString(), strCompareOperator))
                                                    {
                                                        strEachFieldDisplay = "<span style='color:#" + drTC[2].ToString() + "'>" + strEachFieldDisplay + "</span>";
                                                    }

                                                }

                                            }
                                        }
                                    }




                                    string strTempDate = dr[theDateColumn.DisplayName].ToString();
                                    if (strTempDate.IndexOf(" ") > -1)
                                    {
                                        strTempDate = strTempDate.Substring(0, strTempDate.IndexOf(" "));
                                    }

                                    if (DateTime.TryParseExact(strTempDate, Common.Dateformats, new CultureInfo("en-GB"), DateTimeStyles.None, out dtTemp))
                                    {
                                        _dtMonthData.Rows.Add(int.Parse(dr["DBGSystemRecordID"].ToString()), dtTemp, strEachFieldDisplay);

                                    }



                                    //_dtMonthData.Rows.Add(int.Parse(dr["RecordID"].ToString()),
                                }
                            }
                        }
                    }

                }

            }

        }




    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        if (hfSourceDate.Value != "")
        {
            DateTime dtTemp;
            if (DateTime.TryParseExact(hfSourceDate.Value, Common.Dateformats, new CultureInfo("en-GB"), DateTimeStyles.None, out dtTemp))
            {
                ViewState["MonthDate"] = dtTemp;
                //lnkMonthWeekView_OnClick(null, null);
                ddlCalendarView_SelectedIndexChanged(null, null);
            }
        }
        hfSourceDate.Value = "";
    }
    protected void cldDate_DayRender(object sender, DayRenderEventArgs e)
    {


        e.Cell.Controls.Clear();

        if (ddlCalendarView.SelectedValue == "w")
        {
            DateTime dtMonday = (DateTime)ViewState["MonthDate"];


            if (e.Day.Date >= dtMonday && e.Day.Date < dtMonday.AddDays(7))
            {
                //go ahead
                e.Cell.Height = 630;
            }
            else
            {
                e.Cell.Visible = false;
                return;
            }
        }
        else
        {
            // e.Cell.Style.Add("height","105");
            e.Cell.Height = 105;
        }


        DataRow[] rows = _dtMonthData.Select("RecordDate=#" + e.Day.Date.Month.ToString() + "/" +
            e.Day.Date.Day.ToString() + "/" + e.Day.Date.Year.ToString() + "#");

        string strDisplay = "";
        int i = 1;
        bool bSkip = false;
        foreach (DataRow dr in rows)
        {

            if (ddlCalendarView.SelectedValue == "m")
            {
                if (i > 2)
                {
                    //bSkip = true;
                }
            }

            if (bSkip)
            {

            }
            else
            {
                strDisplay = strDisplay + "<div style='padding-top:5px;padding-left:5px;padding-right:5px;color:#000000;font-size:10px;font-family:Arial;'><div ><a  target='_parent' href='"
                     + Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?fixedurl=" + Cryptography.Encrypt("~/Default.aspx") + "&stackzero=yes&mode="
                     + Cryptography.Encrypt("edit") + "&TableID=" + Cryptography.Encrypt(_strTableID) + "&SearchCriteriaID=" + Cryptography.Encrypt("-1")
                     + "&RecordID=" + Cryptography.Encrypt(dr["RecordID"].ToString()) +
                     "' style='text-decoration:none;'>" + dr["DisplayField"].ToString() + " </a> </div></div>";
            }
            i = i + 1;
            //break;
        }


        if (ddlCalendarView.SelectedValue == "m")
        {
            string strOriginalDisplay = strDisplay;

            if (strDisplay != "")//i > 3
            {

                //int j = i - 2-1;
                int j = i - 1;
                string strNoTagDislay = Common.StripTagsCharArray(strDisplay);

                int count = 0;
                string line;
                TextReader reader = new StringReader(strNoTagDislay);
                while ((line = reader.ReadLine()) != null)
                {
                    count++;
                }
                reader.Close();

                if (strNoTagDislay.Length > 75) ////|| count>4
                {

                    strDisplay = "<div style='padding-top:5px;display:block;overflow: hidden;font-weight: normal;'>" + strNoTagDislay.Substring(0, 74) + ".." + "</div>"; //height:55px;
                    string strClick = "<a onclick=\"SetWeekView('" + e.Day.Date.ToShortDateString() + "')\" class='BottomRightLink' >=" + j.ToString() + "</a>"; //width:135px;height:15px;

                    string strCellContent = "</br><div class='js-tooltip-container'> <div class='js-tooltip' style='display:none;'><span>" + strOriginalDisplay + "</span> </div>";
                    strCellContent = strCellContent + "<div  style='text-align:right;padding-right:7px;'><span>" + strClick + "</span></div>"; //
                    strDisplay = strDisplay + strCellContent;

                }
                else
                {
                    strDisplay = "<div style='font-weight: normal;'>" + strOriginalDisplay + "</div>"; //height:75px;
                }

            }
            else
            {
                strDisplay = "<div style='font-weight: normal;'>" + strOriginalDisplay + "</div>"; //height:75px;
            }
        }


        string strDateday = e.Day.Date.Day.ToString();

        if (ddlCalendarView.SelectedValue == "m")
        {
            if (e.Day.Date == DateTime.Today)
            {
                strDateday = "<a onclick=\"SetWeekView('" + e.Day.Date.ToShortDateString() + "')\" style='color:Gray;cursor: pointer;text-decoration:none;font-size:13px;' >" + e.Day.Date.Day.ToString() + " (today)</a>";
            }
            else
            {
                strDateday = "<a onclick=\"SetWeekView('" + e.Day.Date.ToShortDateString() + "')\" style='color:Gray;cursor: pointer;text-decoration:none;' >" + e.Day.Date.Day.ToString() + "</a>";
            }

        }
        else
        {
            if (e.Day.Date == DateTime.Today)
            {
                strDateday = "<span style='font-size:13px;' >" + e.Day.Date.Day.ToString() + " (today)</span>";
            }
        }

        strDisplay = strDateday + strDisplay;

        if (e.Day.Date == DateTime.Today)
        {
            e.Cell.Controls.Add(new LiteralControl("<div class='TodayTop'><strong>" + strDisplay + "</strong></div>"));

        }
        else if (e.Day.Date.Month == ((DateTime)ViewState["MonthDate"]).Date.Month)
        {
            e.Cell.Controls.Add(new LiteralControl("<div class='OtherDayTop'><strong>" + strDisplay + "</strong></div>"));

        }
        else
        {
            e.Cell.Controls.Add(new LiteralControl("<div class='OtherMonthDayTop'><strong>" + strDisplay + "</strong></div>"));
            //e.Cell.Height = 200;
            //e.Cell.Visible = false;
        }





        //foreach (DataRow dr in rows)
        //{
        //    e.Cell.Controls.Add(new LiteralControl("</br><div style='padding-left:5px;padding-right:5px;'><div class='eachschedule'><a  target='_parent' href='"
        //        + Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?stackzero=yes&mode="
        //        + Cryptography.Encrypt("edit") + "&TableID=" + Cryptography.Encrypt(_strTableID) + "&SearchCriteriaID="+ Cryptography.Encrypt("-1")
        //        + "&RecordID=" + Cryptography.Encrypt(dr["RecordID"].ToString()) +
        //        "' style='font-size:9pt;text-decoration:none;'>" + dr["DisplayField"].ToString() + " </a> </div></div>"));

        //    break;
        //}


    }


    protected void cldDate_SelectionChanged(object sender, EventArgs e)
    {


    }

    protected void cldDate_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
    {

        ViewState["MonthDate"] = e.NewDate;
        if (ddlCalendarView.SelectedValue == "d")
        {
            PopulateDayCalendar();
        }
        else
        {
            PopulateCalerdar();
        }


    }

   
    protected void PopulateDayCalendar()
    {
        string strMsg;
        if (ddlCalendarView.SelectedValue == "d")
        {           
            lblTitle.Text = ((DateTime)ViewState["MonthDate"]).ToString("dd MMM yyyy");

            lnkNext.ToolTip = "Go to the next day";
            lnkPre.ToolTip = "Go to the previous day";
        }
        else
        {
            return;
        }

        


        CalendarSectionDetail calDetail = null;
        DocGen.DAL.DocumentSection section = null;
        using (DocGen.DAL.DocGenDataContext ctx = new DocGen.DAL.DocGenDataContext())
        {
            section = ctx.DocumentSections.SingleOrDefault<DocGen.DAL.DocumentSection>(s => s.DocumentSectionID == _DocumentSectionID);
            if (section != null)
            {
               

                calDetail = JSONField.GetTypedObject<CalendarSectionDetail>(section.Details);
                if (!IsPostBack)
                {
                    SetCommonProperties(calDetail);
                }
            }
        }
        Table theTable = null;
        if (calDetail != null)
        {
            if (calDetail.TableID != null)
            {
                theTable = RecordManager.ets_Table_Details((int)calDetail.TableID);
            }
        }
        Column theStartColumn = null;
        if (calDetail.DateFieldColumnID != null)
        {
            theStartColumn = RecordManager.ets_Column_Details((int)calDetail.DateFieldColumnID);
        }
        else
        {
            strMsg = "Please enter start date in the calendar configuration.";
            if(!IsPostBack)
            {
                Session["poptdbmsg"] = strMsg;
            }
            else
            {
                Session["tdbmsgpb"] = strMsg;
            }
            

            return;
        }
        Column theEndColumn = null;
        if (calDetail.EndDateFieldColumnID != null)
        {
            theEndColumn = RecordManager.ets_Column_Details((int)calDetail.EndDateFieldColumnID);
        }
        else
        {
            strMsg = "Please enter end date in the calendar configuration.";
            if (!IsPostBack)
            {
                Session["poptdbmsg"] = strMsg;
            }
            else
            {
                Session["tdbmsgpb"] = strMsg;
            }
            
            return;
        }
        string strDayStart = "8:00";
        string strDayEnd = "18:30";

        if (!string.IsNullOrEmpty(calDetail.DayStart))
        {
            strDayStart = calDetail.DayStart;
        }

        if (!string.IsNullOrEmpty(calDetail.DayEnd))
        {
            strDayEnd = calDetail.DayEnd;
        }

        Column theNameField = null;
        if (calDetail.NameFieldColumnID != null)
        {
            theNameField = RecordManager.ets_Column_Details((int)calDetail.NameFieldColumnID);
        }
        else
        {
            strMsg = "Please enter name field in the calendar configuration.";
            if (!IsPostBack)
            {
                Session["poptdbmsg"] = strMsg;
            }
            else
            {
                Session["tdbmsgpb"] = strMsg;
            }
            return;
        }

        TimeSpan tsDayStart = TimeSpan.Parse(strDayStart);
        TimeSpan tsDayEnd = TimeSpan.Parse(strDayEnd);
        DateTime dtDayStart = (DateTime)ViewState["MonthDate"] + tsDayStart;
        DateTime dtDayEnd = (DateTime)ViewState["MonthDate"] + tsDayEnd;
        DateTime dtVisibleDayEnd = dtDayEnd;
        if(dtVisibleDayEnd.Minute>1)
        {
            dtVisibleDayEnd=dtVisibleDayEnd.AddMinutes(30);
        }
        int iStartHour = tsDayStart.Hours;
        int iEndHour = tsDayEnd.Hours;
        if (tsDayEnd.Minutes > 0)
        {
            iEndHour = iEndHour + 1;
        }

        //Session["ViewType" + _DocumentSectionID.ToString()] = ddlCalendarView.SelectedValue;
        //Session["MonthDate" + _DocumentSectionID.ToString()] = (DateTime)ViewState["MonthDate"];

        int iYmax = 4;//SELECT RecordID
        string strNameFieldHeader = "Name";//The column.DisplayName
        DataTable dtUqNameField = null;
        if (theNameField != null)
        {
            strNameFieldHeader = theNameField.DisplayName;
            dtUqNameField = Common.DataTableFromText("SELECT " + theNameField.SystemName + ",ROW_NUMBER() OVER(ORDER BY " + theNameField.SystemName
                + ") as RowNum  FROM (SELECT DISTINCT(" + theNameField.SystemName
                + ") FROM [Record] WHERE TableID=" + theNameField.TableID.ToString() + " AND " + theNameField.SystemName
                + " IS NOT NULL) AS RI");
            //dtUqNameField = Common.DataTableFromText("SELECT DISTINCT(" + theNameField.SystemName
            //    + "),ROW_NUMBER() OVER(ORDER BY " + theNameField.SystemName
            //    + ") as RowNum  FROM [Record] WHERE TableID=" + theNameField.TableID.ToString() + " AND " + theNameField.SystemName
            //    + " IS NOT NULL ORDER BY " + theNameField.SystemName + " ASC");
            if (dtUqNameField != null && dtUqNameField.Rows.Count > 0)
            {
                iYmax = dtUqNameField.Rows.Count;
            }
        }

        dtUqNameField.Columns.Add("Name");
        foreach(DataRow dr in dtUqNameField.Rows)
        {
            if(theNameField.LinkedParentColumnID!=null && theNameField.TableTableID!=null && (int)theNameField.TableTableID>0 && !string.IsNullOrEmpty(theNameField.DisplayColumn))
            {
                //Common.GetLinkedDisplayText(_dtColumnsDetail.Rows[i]["DisplayColumn"].ToString(), int.Parse(_dtColumnsDetail.Rows[i]["TableTableID"].ToString()), null, " AND Record.RecordID=" + strParentRecordID, "");
                dr["Name"] = Common.GetLinkedDisplayText(theNameField.DisplayColumn,(int) theNameField.TableTableID, null, " AND Record.RecordID=" + dr[theNameField.SystemName].ToString(), "");
            }
            else if(theNameField.TableTableID != null && (int)theNameField.TableTableID ==-1)
            {
                string sDisplayUser = "";
                if(theNameField.DisplayColumn.IndexOf("Email")>0)
                {
                    sDisplayUser = Common.GetValueFromSQL("SELECT Email FROM [User] WHERE UserID=" + dr[theNameField.SystemName].ToString()) ;
                }
                else
                {
                    sDisplayUser = Common.GetValueFromSQL("SELECT FirstName + ' ' + LastName  FROM [User] WHERE  UserID=" + dr[theNameField.SystemName].ToString()) ;
                }

                dr["Name"] = sDisplayUser;
            }
            else 
            {

                dr["Name"] = dr[theNameField.SystemName];
            }
        }
        dtUqNameField.AcceptChanges();
        //int iXmax = 24;
        iYmax = iYmax + 1; // Add the header
        for (int y = 1; y <= iYmax; y++)
        {
            HtmlTableRow trTemp = new HtmlTableRow();
            trTemp.ClientIDMode = ClientIDMode.Static;
            trTemp.ID = "tr_" + y.ToString();
            HtmlTableCell tdTemp = new HtmlTableCell();
            tdTemp.ClientIDMode = ClientIDMode.Static;
            tdTemp.ID = "td_Name_" + y.ToString();

            HtmlTableCell tdTemp2 = new HtmlTableCell();
            tdTemp2.ClientIDMode = ClientIDMode.Static;
            tdTemp2.ID = "td_Before_" + y.ToString();

            var span = new HtmlGenericControl("span");
            if (y == 1)
            {
                span.InnerHtml = "<strong>" + strNameFieldHeader + "</strong>";
                tdTemp.Controls.Add(span);

                var span2 = new HtmlGenericControl("span");
                span2.InnerHtml = "<strong>Before</strong>";
                tdTemp2.Controls.Add(span2);
            }
            else
            {
                //span.InnerHtml = "name " + y.ToString();
                foreach (DataRow drEachName in dtUqNameField.Rows)
                {
                    if (drEachName["RowNum"].ToString() == (y - 1).ToString())
                    {
                        span.InnerHtml = drEachName["Name"].ToString();
                    }
                }
                tdTemp.Controls.Add(span);
            }
            trTemp.Cells.Add(tdTemp);
            trTemp.Cells.Add(tdTemp2);
            tblDetailTable.Rows.Add(trTemp);
        }
        int iTableWidth = (iEndHour - iStartHour) * 60 + ((iEndHour - iStartHour) / 2) + 220;
        tblMainTable.Attributes.Add("width", iTableWidth.ToString());

        //
        if (_bResponsive && Responsive.DeviceMaxXAxis() < 3)
        {
            int iResponsiveWidth = ((int)Responsive.DeviceWidth() * 85) / 100;
            tblHeading.Attributes.Add("width", iResponsiveWidth.ToString());
            tblNavigation.Attributes.Add("width", iResponsiveWidth.ToString());
            lblTitle.Font.Size = 10;
        }
        else
        {
            if (section.ParentSectionID != null)
            {
                int iResponsiveWidth = ((int)Responsive.DeviceWidth() * 40) / 100;
                tblHeading.Attributes.Add("width", iResponsiveWidth.ToString());
                tblNavigation.Attributes.Add("width", iResponsiveWidth.ToString());
            }
            else
            {
                tblHeading.Attributes.Add("width", iTableWidth.ToString());
            }
            
            lblTitle.Font.Size = 16;
        }

        for (int y = 1; y <= iYmax; y++)
        {
            HtmlTableRow trTemp = (HtmlTableRow)tblDetailTable.FindControl("tr_" + y.ToString());
            for (int x = iStartHour; x < iEndHour; x++)
            {
                HtmlTableCell tdTemp = new HtmlTableCell();
                tdTemp.ClientIDMode = ClientIDMode.Static;


                tdTemp.ID = "td_" + y.ToString() + "_" + x.ToString() + "_00";
                trTemp.Cells.Add(tdTemp);
                if (y == 1)
                {
                    string strHeader = "";
                    if (x < 12)
                    {
                        strHeader = x.ToString() + "am";
                    }
                    else if (x == 12)
                    {
                        strHeader = "12pm";
                    }
                    else if (x > 12 && x < 24)
                    {
                        strHeader = (x - 12).ToString() + "pm";
                    }
                    else if (x == 24)
                    {
                        strHeader = "12am";
                    }
                    var span = new HtmlGenericControl("span");
                    span.InnerHtml = "<strong>" + strHeader + "</strong>";
                    tdTemp.Controls.Add(span);
                    tdTemp.Attributes.Add("colspan", "2");
                }
                else
                {
                    HtmlTableCell tdTemp2 = new HtmlTableCell();
                    tdTemp2.ClientIDMode = ClientIDMode.Static;
                    tdTemp2.ID = "td_" + y.ToString() + "_" + x.ToString() + "_30";
                    tdTemp2.Attributes.Add("class", "eachdropcell");
                    tdTemp.Attributes.Add("class", "eachdropcell");

                    foreach (DataRow drEachName in dtUqNameField.Rows)
                    {
                        if (drEachName["RowNum"].ToString() == (y - 1).ToString())
                        {

                            //DateTime dtSelectedDate = DateTime.Parse(((DateTime)ViewState["MonthDate"]).ToShortDateString() + " " + x.ToString() + ":00");
                            tdTemp.Attributes.Add("hostnamefieldvalue", drEachName[theNameField.SystemName].ToString());
                            tdTemp2.Attributes.Add("hostnamefieldvalue", drEachName[theNameField.SystemName].ToString());

                            tdTemp.Attributes.Add("hoststarttime", ((DateTime)ViewState["MonthDate"]).ToShortDateString() +" "+ x.ToString() + ":00:00");
                            tdTemp2.Attributes.Add("hoststarttime", ((DateTime)ViewState["MonthDate"]).ToShortDateString()+" " + x.ToString() + ":30:00");
                        }
                    }

                    trTemp.Cells.Add(tdTemp2);
                }
            }
            tblDetailTable.Rows.Add(trTemp);
        }

        for (int y = 1; y <= iYmax; y++)
        {
            HtmlTableRow trTemp = (HtmlTableRow)tblDetailTable.FindControl("tr_" + y.ToString());
            HtmlTableCell tdTemp = new HtmlTableCell();
            tdTemp.ClientIDMode = ClientIDMode.Static;
            tdTemp.ID = "td_After_" + y.ToString();
            var span = new HtmlGenericControl("span");
            if (y == 1)
            {
                span.InnerHtml = "<strong>After</strong>";
                tdTemp.Controls.Add(span);
            }
            else
            {

            }
            trTemp.Cells.Add(tdTemp);
            tblDetailTable.Rows.Add(trTemp);
        }


        string strTextSearch = "(CONVERT(date,Record." + theStartColumn.SystemName + ",103)<= CONVERT(date, '"
            + ((DateTime)ViewState["MonthDate"]).ToShortDateString() + "',103) AND CONVERT(date,Record." + theEndColumn.SystemName + ",103)>= CONVERT(date, '"
            + ((DateTime)ViewState["MonthDate"]).ToShortDateString() + "',103)) ";
        if (calDetail.FilterTextSearch != null && calDetail.FilterTextSearch != "")
        {
            strTextSearch = calDetail.FilterTextSearch + " AND (" + strTextSearch + ")";
        }
        int iTN = 0;
        string strReturnSQL = "";
        Exception exParam = null;
        string strSortOrderString = " CONVERT(Datetime, [dbo].[fnRemoveNonDate]([" + theStartColumn.DisplayName + "]),103) ";//"DBGSystemRecordID DESC"
        DataTable dtEventRecords = RecordManager.ets_Record_List((int)calDetail.TableID, null, true, null, null, null, strSortOrderString, "ASC", null, null, ref iTN, ref iTN, "allcolumns", "",
           strTextSearch, null, null, "", "", "", null, ref strReturnSQL, ref strReturnSQL, ref exParam,false);

        DataTable dtNameStartEnd = new DataTable();
        dtNameStartEnd.Columns.Add("namefield");
        dtNameStartEnd.Columns.Add("Start", System.Type.GetType("System.DateTime"));
        dtNameStartEnd.Columns.Add("End", System.Type.GetType("System.DateTime"));
        dtNameStartEnd.Columns.Add("TopPosition", typeof(int));
        dtNameStartEnd.Columns.Add("height", typeof(int));
        dtNameStartEnd.Columns.Add("hiddenpoition", typeof(int));
       
        dtNameStartEnd.AcceptChanges();
        if (dtEventRecords != null && dtEventRecords.Rows.Count > 0)
        {
            foreach (DataRow drEachEvent in dtEventRecords.Rows)
            {
                //Let's find the name and then the y
                foreach (DataRow drEachName in dtUqNameField.Rows)
                {
                    if (drEachName["Name"].ToString() == drEachEvent[theNameField.DisplayName].ToString())
                    {
                        DateTime dtEventStartDateTime;
                        DateTime dtEventEndDateTime;

                        if (DateTime.TryParse(drEachEvent[theStartColumn.DisplayName].ToString(), out dtEventStartDateTime) == false)
                        {
                            continue;
                        }
                        if (DateTime.TryParse(drEachEvent[theEndColumn.DisplayName].ToString(), out dtEventEndDateTime) == false)
                        {
                            continue;
                        }

                        DataRow drNameStartEnd = dtNameStartEnd.NewRow();
                        drNameStartEnd["namefield"] = drEachName["Name"].ToString();
                        drNameStartEnd["Start"] = dtEventStartDateTime;
                        drNameStartEnd["End"] = dtEventEndDateTime;
                        int iTopPosition = 0;
                        int ihiddenpoition = 0;
                        int iCellHeight = 50;
                       
                        foreach (DataRow drPreviousNSE in dtNameStartEnd.Rows)
                        {
                            if (drPreviousNSE["namefield"].ToString() == drNameStartEnd["namefield"].ToString())
                            {
                                DateTime dtPreStart = (DateTime)drPreviousNSE["Start"];
                                DateTime dtPreEnd = (DateTime)drPreviousNSE["End"];
                                if (dtEventStartDateTime == dtPreStart
                                    || dtEventStartDateTime < dtDayStart
                                    || dtDayEnd <= dtEventStartDateTime)
                                {
                                    iTopPosition = (int)drPreviousNSE["TopPosition"];
                                    iCellHeight = (int)drPreviousNSE["height"] + 50;
                                    ihiddenpoition = (int)drPreviousNSE["hiddenpoition"]+50;
                                }
                                else if ((dtEventStartDateTime > dtPreStart && dtEventStartDateTime <= dtPreEnd)//S In
                                    || (dtEventEndDateTime > dtPreStart && dtEventEndDateTime <= dtPreEnd)// E In
                                    || (dtEventStartDateTime > dtPreStart && dtEventEndDateTime <= dtPreEnd) //SE In
                                   // || (dtEventStartDateTime <= dtPreStart && dtEventEndDateTime >= dtPreEnd) //
                                    )
                                {
                                    iTopPosition = (int)drPreviousNSE["TopPosition"] + 50 + (int)drPreviousNSE["hiddenpoition"];
                                    ihiddenpoition = 0;// (int)drPreviousNSE["hiddenpoition"];
                                    iCellHeight = (int)drPreviousNSE["height"] + 50;
                                    
                                }
                                else
                                {
                                    iCellHeight = (int)drPreviousNSE["height"] + 50;
                                }
                                
                            }
                        }
                     
                        drNameStartEnd["TopPosition"] = iTopPosition;
                        drNameStartEnd["hiddenpoition"] = ihiddenpoition;
                        drNameStartEnd["height"] = iCellHeight;                        

                        dtNameStartEnd.Rows.Add(drNameStartEnd);

                        dtNameStartEnd.AcceptChanges();
                        string strTD = "td_" + (int.Parse(drEachName["RowNum"].ToString()) + 1).ToString()
                            + "_" + dtEventStartDateTime.Hour.ToString() + "_"
                            + (dtEventStartDateTime.Minute > 0 ? "30" : "00");

                        
                        var divContent = new HtmlGenericControl("div");
                        divContent.Attributes.Add("draggable", "true");
                        divContent.Attributes.Add("class", "columnspan");
                        divContent.Attributes.Add("id", "divContent_" + drEachEvent["DBGSystemRecordID"].ToString());
                        divContent.Attributes.Add("recordid", drEachEvent["DBGSystemRecordID"].ToString());
                        divContent.Attributes.Add("namefield", drEachEvent[theNameField.DisplayName].ToString());
                        string strContent = @"<a target='_parent' style='float:left;padding-right:5px;' title='Edit' href='" + Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath
                + @"/Pages/Record/RecordDetail.aspx?fixedurl=" + Cryptography.Encrypt("~/Default.aspx") + "&stackzero=yes&mode=" + Cryptography.Encrypt("edit") +
                                              "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt(calDetail.TableID.ToString()) + "&Recordid=" +
                                              Cryptography.Encrypt(drEachEvent["DBGSystemRecordID"].ToString()) + @"'> <img width='10px' title='Edit' src='../../App_Themes/Default/Images/iconEdit.png'
                    alt='' style='border-width:0px;vertical-align:top;float:left;'></a>";

                       

                        if (dtEventRecords.Columns.Contains("**" + theNameField.DisplayName + "_ID**"))
                        {
                            divContent.Attributes.Add("namefieldvalue", drEachEvent["**" + theNameField.DisplayName + "_ID**"].ToString());
                        }
                        else
                        {
                            divContent.Attributes.Add("namefieldvalue", drEachEvent[theNameField.DisplayName].ToString());
                        }
                        string strDisplay = calDetail.FieldDisplay;
                        strDisplay = GetDisplayText(calDetail, dtEventRecords, drEachEvent["DBGSystemRecordID"].ToString());

                        divContent.InnerHtml =strContent+ strDisplay;
                        divContent.Attributes.Add("title", ExportUtil.StripTagsCharArray( strDisplay));
                       
                        //if (iTopPosition > 0)
                        //    iCellHeight = iCellHeight + iTopPosition;

                        if(tblDetailTable.FindControl("td_Name_" + (int.Parse(drEachName["RowNum"].ToString()) + 1).ToString())!=null)
                        {
                            HtmlTableCell tdNameCell = (HtmlTableCell)tblDetailTable.FindControl("td_Name_" + (int.Parse(drEachName["RowNum"].ToString()) + 1).ToString());
                            tdNameCell.Style.Add("height", iCellHeight.ToString() + "px");
                        }
                        

                        if (tblDetailTable.FindControl(strTD) != null && dtEventStartDateTime>=dtDayStart)
                        {
                            //Inside
                            int iW = GetWidth(dtEventStartDateTime, dtEventEndDateTime);
                            divContent.Style.Add("width", iW.ToString() + "px");
                            HtmlTableCell tdInside = (HtmlTableCell)tblDetailTable.FindControl(strTD);
                            tdInside.Controls.Add(divContent);                         
                            divContent.Style.Add("top", iTopPosition.ToString() + "px");                            
                        }
                        else
                        {
                            
                            //before
                            if (dtDayStart>dtEventStartDateTime && dtDayStart>dtEventEndDateTime)
                            {
                                strTD = "td_Before_" + (int.Parse(drEachName["RowNum"].ToString()) + 1).ToString();
                                HtmlTableCell tdTempBefore = (HtmlTableCell)tblDetailTable.FindControl(strTD);
                                if (tblDetailTable.FindControl(strTD) != null)
                                {                                   
                                    divContent.Style.Add("width", "60px");
                                    tdTempBefore.Controls.Add(divContent);
                                    divContent.Style.Add("top", iTopPosition.ToString() + "px");
                                }
                            }
                            //before and inside
                            if (dtDayStart > dtEventStartDateTime && dtDayStart < dtEventEndDateTime && dtEventEndDateTime <= dtVisibleDayEnd)
                            {
                                strTD = "td_Before_" + (int.Parse(drEachName["RowNum"].ToString()) + 1).ToString();
                                if (tblDetailTable.FindControl(strTD) != null)
                                {
                                    HtmlTableCell tdTempBefore = (HtmlTableCell)tblDetailTable.FindControl(strTD);
                                    int iW = GetWidth(dtDayStart, dtEventEndDateTime);
                                    iW = iW + 60;
                                    divContent.Style.Add("width", iW.ToString() + "px");
                                    tdTempBefore.Controls.Add(divContent);
                                    divContent.Style.Add("top", iTopPosition.ToString() + "px");
                                    
                                }
                            }
                            //before - after
                            if (dtEventStartDateTime < dtDayStart && dtVisibleDayEnd < dtEventEndDateTime)
                            {
                                strTD = "td_Before_" + (int.Parse(drEachName["RowNum"].ToString()) + 1).ToString();
                                if (tblDetailTable.FindControl(strTD) != null)
                                {
                                    HtmlTableCell tdTempBefore = (HtmlTableCell)tblDetailTable.FindControl(strTD);
                                    int iW = GetWidth(dtEventStartDateTime, dtEventEndDateTime);
                                    divContent.Style.Add("width", iW.ToString() + "px");
                                    tdTempBefore.Controls.Add(divContent);
                                    divContent.Style.Add("top", iTopPosition.ToString() + "px");
                                }
                            }

                            //after
                            if (dtVisibleDayEnd <= dtEventStartDateTime)
                            {
                                strTD = "td_After_" + (int.Parse(drEachName["RowNum"].ToString()) + 1).ToString();
                                if (tblDetailTable.FindControl(strTD) != null)
                                {
                                    HtmlTableCell tdTempAfter = (HtmlTableCell)tblDetailTable.FindControl(strTD);
                                    int iW = GetWidth(dtEventStartDateTime, dtEventEndDateTime);
                                    divContent.Style.Add("width", iW.ToString() + "px");
                                    tdTempAfter.Controls.Add(divContent);
                                    divContent.Style.Add("top", iTopPosition.ToString() + "px");
                                        
                                }
                            }
                            
                           
                        }
                       

                        break;
                    }
                }
            }
        }
    }

    protected void SetCommonProperties(CalendarSectionDetail calDetail)
    {
        _strTableID = calDetail.TableID.ToString();
        
        Table theTable = RecordManager.ets_Table_Details(int.Parse(_strTableID));
        if (calDetail.CalendarTitle != null && calDetail.CalendarTitle != "")
        {
            lblHeading.Text = calDetail.CalendarTitle;
        }
        else
        {
            if (theTable != null)
            {
                lblHeading.Text = theTable.TableName;
            }
        }
        if (calDetail.ShowAddRecordIcon != null)
        {
            if ((bool)calDetail.ShowAddRecordIcon)
            {
                hlAddNewRecord.Visible = true;
                hlAddNewRecord.Target = "_parent";
                hlAddNewRecord.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?fixedurl=" + Cryptography.Encrypt("~/Default.aspx") + "&stackzero=yes&mode="
                   + Cryptography.Encrypt("add") + "&TableID=" + Cryptography.Encrypt(_strTableID) + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");
            }
        }
    }
    protected string GetDisplayText(CalendarSectionDetail calDetail, 
       DataTable dtRecords,string strTheRecordID)
    {
        _strTableID = calDetail.TableID.ToString();
        string strEachFieldDisplay = calDetail.FieldDisplay;
        DataTable dtColour = null;
        DataTable dtColumns = Common.DataTableFromText("SELECT ColumnID,SystemName,DisplayName,ColumnType,NumberType,LinkedParentColumnID,TableTableID,DisplayColumn  FROM [Column] WHERE IsStandard=0 AND   TableID="
                              + calDetail.TableID.ToString() + "  ORDER BY DisplayName");
        if (calDetail.TextColourInfo != null && calDetail.TextColourInfo != "")
        {
            string strXML = calDetail.TextColourInfo;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(strXML);

            XmlTextReader r = new XmlTextReader(new StringReader(xmlDoc.OuterXml));

            DataSet ds = new DataSet();
            ds.ReadXml(r);
            if (ds.Tables[0] != null)
            {
                dtColour = ds.Tables[0];
                bool bCompareOperatorFound = false;
                foreach (DataColumn dc in dtColour.Columns)
                {
                    if (dc.ColumnName.ToLower() == "CompareOperator".ToLower())
                    {
                        bCompareOperatorFound = true;
                        break;
                    }
                }
                if (bCompareOperatorFound == false)
                {
                    dtColour.Columns.Add("CompareOperator");
                    dtColour.AcceptChanges();
                }
            }

        }

        DataTable _dtRecordColums = RecordManager.ets_Table_Columns_All(int.Parse(_strTableID));
        DataTable dtPT = Common.DataTableFromText(@"SELECT distinct TC.ParentTableID,TableName FROM TableChild TC INNER JOIN [Table] T
                                                ON TC.ParentTableID=T.TableID
                                                 WHERE ChildTableID=" + _strTableID);

        Column theDateColumn = null;
        if (calDetail.DateFieldColumnID != null)
        {
            theDateColumn = RecordManager.ets_Column_Details((int)calDetail.DateFieldColumnID);
        }
        foreach (DataRow dr in dtRecords.Rows)
        {
            if (strTheRecordID!=dr["DBGSystemRecordID"].ToString())
            {
                continue;
            }
            if (dr[theDateColumn.DisplayName] != DBNull.Value)
            {
                //string strEachFieldDisplay = calDetail.FieldDisplay;

                foreach (DataRow drC in dtColumns.Rows)
                {

                    if (strEachFieldDisplay.IndexOf("TIME[" + drC["DisplayName"].ToString() + "]") > -1)
                    {
                        if (dr[drC["DisplayName"].ToString()].ToString().LastIndexOf(" ") > -1)
                        {
                            strEachFieldDisplay = strEachFieldDisplay.Replace("TIME[" + drC["DisplayName"].ToString() + "]", "TIME:"
                                + dr[drC["DisplayName"].ToString()].ToString().Substring(dr[drC["DisplayName"].ToString()].ToString().LastIndexOf(" ")));
                        }
                        else
                        {
                            strEachFieldDisplay = strEachFieldDisplay.Replace("TIME[" + drC["DisplayName"].ToString() + "]", "TIME:"
                                                                         + dr[drC["DisplayName"].ToString()].ToString());
                        }
                    }
                }


                foreach (DataRow drC in dtColumns.Rows)
                {
                    strEachFieldDisplay = strEachFieldDisplay.Replace("[" + drC["DisplayName"].ToString() + "]", dr[drC["DisplayName"].ToString()].ToString());
                }



                try
                {

                    //Work with 1 top level Parent tables.

                    if (dtPT.Rows.Count > 0)
                    {

                        foreach (DataRow drPT in dtPT.Rows)
                        {
                            if (strEachFieldDisplay.IndexOf("[" + drPT["TableName"].ToString() + ":") == -1)
                            {
                                continue;
                            }

                            for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                            {
                                //     if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value && _dtRecordColums.Rows[i]["LinkedParentColumnID"] != DBNull.Value
                                //&& (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
                                //|| _dtRecordColums.Rows[i]["DropDownType"].ToString() == "tabledd")
                                // && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
                                //&& _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
                                //     {
                                if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
                                    && dr.Table.Columns.Contains("**" + _dtRecordColums.Rows[i]["DisplayName"].ToString() + "_ID**")
                                    && _dtRecordColums.Rows[i]["TableTableID"].ToString() == drPT["ParentTableID"].ToString())
                                {
                                    if (dr["**" + _dtRecordColums.Rows[i]["DisplayName"].ToString() + "_ID**"].ToString() != "")
                                    {
                                        Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["LinkedParentColumnID"].ToString()));
                                        DataTable dtParentRecord = null;
                                        if (theLinkedColumn.SystemName.ToLower() == "recordid")
                                        {
                                            dtParentRecord = Common.DataTableFromText("SELECT * FROM Record WHERE RecordID=" + dr["**" + _dtRecordColums.Rows[i]["DisplayName"].ToString() + "_ID**"].ToString());
                                        }
                                        else
                                        {
                                            dtParentRecord = Common.DataTableFromText("SELECT * FROM Record WHERE TableID=" + theLinkedColumn.TableID.ToString() + " AND " + theLinkedColumn.SystemName + "='" +
                                                dr["**" + _dtRecordColums.Rows[i]["DisplayName"].ToString() + "_ID**"].ToString().Replace("'", "''") + "'");
                                        }

                                        if (dtParentRecord.Rows.Count > 0)
                                        {

                                            DataTable dtColumnsPT = Common.DataTableFromText(@"SELECT distinct SystemName, TableName + ':' + DisplayName AS DP FROM [Column] INNER JOIN [Table]
                                        ON [Column].TableID=[Table].TableID WHERE  [Column].IsStandard=0 AND  [Column].TableID=" + drPT["ParentTableID"].ToString());
                                            foreach (DataRow drC in dtColumnsPT.Rows)
                                            {
                                                strEachFieldDisplay = strEachFieldDisplay.Replace("[" + drC["DP"].ToString() + "]", dtParentRecord.Rows[0][drC["SystemName"].ToString()].ToString());

                                            }
                                        }
                                    }
                                }
                                //}
                            }
                        }
                    }


                }
                catch
                {
                    //
                }




                DateTime dtTemp;



                if (dtColour != null)
                {
                    foreach (DataRow drC in dtColumns.Rows)
                    {
                        foreach (DataRow drTC in dtColour.Rows)
                        {
                            if (drC["ColumnID"].ToString() == drTC[0].ToString()
                               && drTC[2].ToString() != "" && strEachFieldDisplay != "")
                            {
                                string strCompareOperator = "";

                                if (drTC[3] != DBNull.Value)
                                    strCompareOperator = drTC[3].ToString();

                                string strRecordDisplayValue = dr[drC["DisplayName"].ToString()].ToString();

                                if (drC["TableTableID"] != DBNull.Value && drC["DisplayColumn"] != DBNull.Value
                                    && dtRecords.Columns.Contains("**" + drC["DisplayName"].ToString() + "_ID**"))
                                {
                                    if (dr["**" + drC["DisplayName"].ToString() + "_ID**"] != DBNull.Value)
                                    {
                                        strRecordDisplayValue = dr["**" + drC["DisplayName"].ToString() + "_ID**"].ToString();
                                    }

                                }

                                if (TheDatabase.IsDataInFilterRange(drC["ColumnType"].ToString(), drC["NumberType"].ToString(),
                                    strRecordDisplayValue, drTC[1].ToString(), strCompareOperator))
                                {
                                    strEachFieldDisplay = "<span style='color:#" + drTC[2].ToString() + "'>" + strEachFieldDisplay + "</span>";
                                }

                            }

                        }
                    }
                }




                string strTempDate = dr[theDateColumn.DisplayName].ToString();
                if (strTempDate.IndexOf(" ") > -1)
                {
                    strTempDate = strTempDate.Substring(0, strTempDate.IndexOf(" "));
                }

                //if (DateTime.TryParseExact(strTempDate, Common.Dateformats, new CultureInfo("en-GB"), DateTimeStyles.None, out dtTemp))
                //{
                //    _dtMonthData.Rows.Add(int.Parse(dr["DBGSystemRecordID"].ToString()), dtTemp, strEachFieldDisplay);

                //}

                //_dtMonthData.Rows.Add(int.Parse(dr["RecordID"].ToString()),
            }
        }

        return strEachFieldDisplay;



    }


    protected int GetWidth(DateTime dt1, DateTime dt2)
    {
        int iTotalEventWidth = (int)((((TimeSpan)(dt2 - dt1)).TotalHours) * 60);

        if (iTotalEventWidth > 1700)
            iTotalEventWidth = 1700;

        return iTotalEventWidth;
    }
    protected void ddlCalendarView_SelectedIndexChanged(object sender, EventArgs e)
    {
        tblMainTable.Attributes.Add("width", "100%");
        if (ddlCalendarView.SelectedValue == "m")
        {
            trDayCalendar.Visible = false;
            trCalendar.Visible = true;
            lblTitle.Visible = true;
            PopulateCalerdar();
           
        }
        else if (ddlCalendarView.SelectedValue == "d")
        {
            trDayCalendar.Visible = true;
            trCalendar.Visible = false;
            //lblTitle.Visible = false;
            PopulateDayCalendar();
        }
        else
        {
            SetMonday();
            trDayCalendar.Visible = false;
            trCalendar.Visible = true;
            lblTitle.Visible = true;
            PopulateCalerdar();
        }

    }
    protected void btnRefreshDayView_Click(object sender, EventArgs e)
    {
        PopulateDayCalendar();
    }
}
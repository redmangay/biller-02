﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_DocGen_EachRecordTable : SecurePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["DocumentSectionID"] != null)
        {
            string strDocumentSectionID = Cryptography.Decrypt(Request.QueryString["DocumentSectionID"].ToString());

            string strWidth = "1";
            if (Request.QueryString["width"] != null)
            {
                strWidth = Request.QueryString["width"].ToString();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "DocumentSectionID" + strDocumentSectionID, "setTimeout(function () { parent.autoResizeRecord('iframe" + strDocumentSectionID + "'," + strWidth + ");}, 500);", true);
        }
        //if(Common.IsResponsive()==false)
        //{
        //    tdResponsiveOverflow.Visible = false;
        //}
    }
}
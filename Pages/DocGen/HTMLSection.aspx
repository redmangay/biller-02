﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true"
    ValidateRequest="false" CodeFile="HTMLSection.aspx.cs" Inherits="DocGen.Document.HTMLSection.Edit" %>

<%--<%@ Register assembly="FredCK.FCKeditorV2" namespace="FredCK.FCKeditorV2" tagprefix="FCKeditorV2" %>--%>
<%@ Register TagPrefix="editor" Assembly="WYSIWYGEditor" Namespace="InnovaStudio" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="server">



    <script type="text/javascript" src="<%=ResolveUrl("~/Script/pasteimage.js")%>"></script>
    
    
    <div style="text-align: center;">
        <div style="display: inline-block;">
            <br />
            <span class="failureNotification">
                <asp:Literal ID="ErrorMessage" runat="server"></asp:Literal>
            </span>
            <asp:ValidationSummary ID="MainValidationSummary" runat="server" CssClass="failureNotification"
                ValidationGroup="MainValidationGroup" />
            <div style="padding-left: 10px;">
                <table cellpadding="3">
                    <tr>
                        <td align="left">

                            <table>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="Label1" CssClass="TopTitle" Text=" Custom Section"></asp:Label>
                                    </td>
                                    <td style="width: 50px;"></td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <strong>Type</strong>
                                                </td>
                                                <td style="padding-left: 3px;">
                                                    <asp:DropDownList runat="server" ID="ddlCustomSectionType" CssClass="NormalTextBox"
                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlCustomSectionType_SelectedIndexChanged">
                                                        <asp:ListItem Value="custom_html" Text="Custom HTML" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Value="admin_notification" Text="Notification Table"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right">
                            <table>
                                <tr>
                                    <td></td>
                                    <td>
                                        <div runat="server" id="div11">
                                            <asp:LinkButton runat="server" ID="SaveButton" OnClick="SaveButton_Click" ValidationGroup="MainValidationGroup">
                                                <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png" ToolTip="Save" />
                                            </asp:LinkButton>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <editor:WYSIWYGEditor runat="server" scriptPath="../../Editor/scripts/" ID="fckeTemplate"
                                btnStyles="true" btnSave="false" editorheight="500" Height="550" editorwidth="810"
                                Width="1050" AssetManager="../../assetmanager/assetmanager.aspx" AssetManagerWidth="550"
                                AssetManagerHeight="400" Visible="true" ToolbarMode="0" btnPreview="False"
                                btnSearch="False" btnBookmark="False" btnAbsolute="False" btnForm="False" />
                            <asp:TextBox runat="server" ID="txtOtherHTML" ReadOnly="true" CssClass="NormalTextBox"
                                TextMode="MultiLine" Width="1050" Height="300" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <strong></strong>
                        </td>
                        <td align="left">
                            <asp:HiddenField runat="server" ID="hfRemoveSection" ClientIDMode="Static" Value="0" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>




    <script type="text/javascript">
        var oEditor = null;
        //        function InsertMergeField() {
        //            oUtil.obj.insertHTML("[" + document.getElementById("ctl00_HomeContentPlaceHolder_ddlReturnFields").value + "]")
        //        }

        function SavedAndRefresh() {
            window.parent.document.getElementById('btnRefresh').click();
            parent.$.fancybox.close();

        }

        function CloseAndRefresh() {
            if (document.getElementById('hfRemoveSection').value == '0') {
                parent.$.fancybox.close();
            }
            else {
                //                window.parent.document.getElementById('btnRefresh').click();
                window.parent.RemoveNoAddedSection();
                parent.$.fancybox.close();
            }

        }

        function paste(src) {
            oUtil.obj.insertHTML("<img src='" + src + "'>");
            //alert('paste test');
        }

        $(function () {
            $.pasteimage(paste);
            //oUtil.obj.pasteimage(paste);
            //ctl00_HomeContentPlaceHolder_fckeTemplate.$.pasteimage(paste);
            //oUtil.$.pasteimage(paste);
        });

    </script>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Stripe;
public partial class Pages_User_UpgradeAccount : System.Web.UI.Page
{
    public string stripePublishableKey = SystemData.SystemOption_ValueByKey_Account("StripePublishableKey", null, null);
    BillingAPI.Billing _theBilling;
    Account _theAccount;
    //int iOriginalAccountTypeID;
    //AccountType _theAccountType;
    User _theUser;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["User"] == null)
            return;
        _theAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));
        //iOriginalAccountTypeID = (int)_theAccount.AccountTypeID;
        _theUser = (User)Session["User"];

        if (!IsPostBack)
        {
            
            if (Session["PendingBillingID"] == null)
            {
                BillingAPI.Billing newBilling = new BillingAPI.Billing();
                newBilling.AccountID = (int)_theAccount.AccountID;

                //int? iAccountTypeID = null;
                //if (Session["SelectedAccountTypeID"] != null && Session["SelectedAccountTypeID"].ToString() != "")
                //{
                //    int iOut = 0;
                //    if (int.TryParse(Session["SelectedAccountTypeID"].ToString(), out iOut))
                //    {
                //        iAccountTypeID = iOut;
                //    }
                //}
                newBilling.Amount = BillingAPI.TotalChargeAmount((int)_theAccount.AccountID, null, 2, null);
                newBilling.IsYearlySubscription = BillingAPI.IsYearlySubscription(newBilling.AccountID);
                newBilling.BillingDate = DateTime.Now;
                newBilling.Currency = BillingAPI.GetAccountCurrency(newBilling.AccountID);
                newBilling.NoOfUsers = BillingAPI.UsersPerAccount(newBilling.AccountID);
                newBilling.PaymentMethod = BillingAPI.GetAccountPaymentMethod((int)_theAccount.AccountID);
                //newBilling.
                Int32 billingid = BillingAPI.CreateBilling(newBilling);
                Session["PendingBillingID"] = billingid;
                hfBillingID.Value = billingid.ToString();
            }
            else
            {
                hfBillingID.Value = Session["PendingBillingID"].ToString();
            }
        }
        if (hfBillingID.Value != "")
            _theBilling = BillingAPI.GetBilling(int.Parse(hfBillingID.Value));

        if (IsPostBack)
        {
            if (Request.Form["stripeToken"] != null)
            {
                string stripeEmail = Request.Form["stripeEmail"].ToString();               
                var secretKey = SystemData.SystemOption_ValueByKey_Account("StripeSecretKey", null, null);
                StripeConfiguration.SetApiKey(secretKey);


                var cus_options = new StripeCustomerCreateOptions
                {
                    Email = stripeEmail,//"jenny.rosen@example.com",
                    Metadata = new Dictionary<string, string>()
                        {
                            { "AccountID", Session["AccountID"].ToString() }
                        }
                };
                var cus_service = new StripeCustomerService();


                var optionsCusList = new StripeCustomerListOptions();
                optionsCusList.Limit = 10000;
                var customers = cus_service.List(optionsCusList);
                //string sTest = "";
                bool bUpdateSubscription = false;
                StripeCustomer customer = new StripeCustomer();
                foreach (var customerEach in customers)
                {
                    if (cus_options.Email.ToLower().Trim() == customerEach.Email.ToLower().Trim())
                    {
                        System.Collections.Generic.Dictionary<string, string> cusMetaData = customerEach.Metadata;
                        if (cusMetaData != null && cusMetaData["AccountID"].ToString() == Session["AccountID"].ToString())
                        {
                            customer = customerEach;
                            bUpdateSubscription = true;
                            break;
                        }

                    }
                }

                if (customer.Id == null)
                    customer = cus_service.Create(cus_options);


                if (bUpdateSubscription)
                {
                    BillingAPI.UpdateStripeSubscription(int.Parse(Session["AccountID"].ToString()),2);
                }
                else
                {
                    int iPlanQuantity = int.Parse(GetTotalPaymentAmount());
                    var items = new List<StripeSubscriptionItemOption> {
                    new StripeSubscriptionItemOption {
                            PlanId = BillingAPI.StripeAccountPlanID(int.Parse(Session["AccountID"].ToString())),
                            Quantity=iPlanQuantity
                        }
                    };

                    var optionsSubCreate = new StripeSubscriptionCreateOptions
                    {
                        Items = items,
                        Source = Request.Form["stripeToken"].ToString(),
                        Metadata = new Dictionary<string, string>()
                        {
                            { "AccountID", Session["AccountID"].ToString() }
                        }
                    };
                    var service = new StripeSubscriptionService();
                    StripeSubscription subscription = service.Create(customer.Id, optionsSubCreate);
                }
                _theBilling.TargetAccountTypeID = 2;
                BillingAPI.UpdateBilling(_theBilling);


                string jsReloadParentPage = @"  
                           
                        window.parent.location.reload(false);
                    ";

                ScriptManager.RegisterStartupScript(this, this.GetType(), "jsReloadParentPage", jsReloadParentPage, true);
                //_theAccount.AccountTypeID = 2;  
                //SecurityManager.Account_Update(_theAccount);
            }
        }

    }


    public string GetTotalPaymentAmount()
    {
        return (_theBilling.Amount * 100).ToString();
    }
    public string GetCurrency()
    {
        return _theBilling.Currency;
    }
    public string GetUserEmail()
    {
        if (_theUser == null)
        {
            _theUser = (User)Session["User"];
        }
        return _theUser.Email;
    }


    public string DataDescription()
    {
        string sDataDescription = "Ontask Monthly Charge";
        //if (_theBilling.IsYearlySubscription != null && (bool)_theBilling.IsYearlySubscription)
        //{
        //    sDataDescription = "OnTask Yearly Charge";
        //}

        return sDataDescription;
    }
    public string DataPanelLabel()
    {
        string sDataPanelLabel = "Pay Monthly {{amount}}";
        //if (_theBilling.IsYearlySubscription != null && (bool)_theBilling.IsYearlySubscription)
        //{
        //    sDataPanelLabel = "Pay Yearly {{amount}}";
        //}

        return sDataPanelLabel;
    }
    public string DataLabel()
    {

        string sDataLabel = "Upgrade now";

        var secretKey = SystemData.SystemOption_ValueByKey_Account("StripeSecretKey", null, null);
        StripeConfiguration.SetApiKey(secretKey);


        var cus_options = new StripeCustomerCreateOptions
        {
            Email = _theUser.Email,//"jenny.rosen@example.com",
            Metadata = new Dictionary<string, string>()
                        {
                            { "AccountID", Session["AccountID"].ToString() }
                        }
        };
        var cus_service = new StripeCustomerService();


        var optionsCusList = new StripeCustomerListOptions();
        optionsCusList.Limit = 10000;
        var customers = cus_service.List(optionsCusList);
        //string sTest = "";
        bool bUpdateSubscription = false;
        StripeCustomer customer = new StripeCustomer();
        foreach (var customerEach in customers)
        {
            if (cus_options.Email.ToLower().Trim() == customerEach.Email.ToLower().Trim())
            {
                System.Collections.Generic.Dictionary<string, string> cusMetaData = customerEach.Metadata;
                if (cusMetaData != null && cusMetaData["AccountID"].ToString() == Session["AccountID"].ToString())
                {
                    customer = customerEach;
                    bUpdateSubscription = true;
                    break;
                }

            }
        }
        if (bUpdateSubscription)
        {
            sDataLabel = "Update Subscription";
        }
        return sDataLabel;
    }


}
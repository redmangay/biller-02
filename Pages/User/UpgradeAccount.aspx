﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/PoprResponsive.master" AutoEventWireup="true" CodeFile="UpgradeAccount.aspx.cs" Inherits="Pages_User_UpgradeAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" Runat="Server">
    <script type="text/javascript"> 
        $(document).ready(function () {
            //window.parent.HideLoadingContent();
            window.parent.$(".ajax-indicator-full").fadeOut();
        });
    </script>
    <div style="padding:20px; text-align:center;">
        <table style="text-align:center;width:100%;">
            <tr>
                <td>
                    <h1>Upgrade</h1>
                </td>
            </tr>
            <tr>
                <td align="left" style="padding-left:50px;">
                    To add a new user you must first upgrade to a <strong>Team</strong> account
                </td>
            </tr>
            <tr>
                <td style="text-align:left;padding-left:50px;">
                    <asp:BulletedList ID="BulletedList1" runat="server">
                        <asp:ListItem Text="Unlilmited Users"></asp:ListItem>
                        <asp:ListItem Text="Progress Reporting"></asp:ListItem>
                        <asp:ListItem Text="See Staff On Map"></asp:ListItem>
                    </asp:BulletedList>
                </td>
            </tr>
            <tr>
                <td>
                    $10 USD per user/month
                </td>
            </tr>
            <tr>
                <td style="padding:20px;">

                      <div >

                                    <asp:Panel runat="server" ID="pnlStripButton">
                                        <script
                                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-label="Upgrade now"
                                            data-key="<%= stripePublishableKey %>"
                                            data-amount="<%= GetTotalPaymentAmount() %>"
                                            data-currency="<%= GetCurrency() %>"
                                            data-name="DBGurus Australia"
                                            data-description="<%=DataDescription() %>"
                                            data-email="<%= GetUserEmail() %>"
                                            data-locale="auto"
                                            data-zip-code="false"
                                            data-panel-label="<%=DataPanelLabel() %>">
                                        </script>
                                    </asp:Panel>

                                </div>

                      <asp:HiddenField runat="server" ID="hfBillingID" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HyperLink runat="server" ID="hlUpgradeTerms" NavigateUrl="https://www.ontaskhq.com.au/terms-of-service.pdf" Target="_blank" >Terms of Service</asp:HyperLink>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true"
    CodeFile="Detail.aspx.cs" Inherits="User_Detail" MaintainScrollPositionOnPostback="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>
<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

       <script type="text/javascript" src="<%=Request.Url.Scheme+@"://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCgc_Iim3AMzz7jYQ8bdqVKh1mnGxz7Y88" %>"></script>


    <style type="text/css">
  
        .headerlink a {
            text-decoration: none;
            color: Black;
            cursor: default;
        }

        #divTwoFactorAuthenticator {
            display: none;
        }

        #showPassword_icon {
            /*background: url("../../Images/eye-open-edited2.png") center center no-repeat #fff;*/
            border: none;
            box-shadow: none;
            width: 26px;
            height: 18px;
            display: block;
            /*float: right;*/
            margin-left: 224px;
            border: solid 1px #909090;
            border-left: none;
            font-size: 0px;
        }

        #ctl00_HomeContentPlaceHolder_TDBDetailPassword {
            float: left;
            /*margin-bottom:30px;*/
            border-right: none;
        }

        .ModalPopupBG
        {
            background-color: #2b2922;
            filter: alpha(opacity=50);
            opacity: 0.7;
        }
        
        .PopupBox
        {
            background:white;
            border-radius:4px;
            /*top: 150px !important;*/
        }
    </style>

    <script language="javascript" type="text/javascript">

        var roleNameDuplicate = false;
        /* == Red 10092019: Ticket 4818 == */
        function jsCheckRoleProgress() {
            //setTimeout(function () { document.getElementById('imgloadinggif').style.display = 'block'; }, 200);
            document.getElementById('imgloadinggif').style.display = 'block';
            document.getElementById('imgcheck').style.display = 'none';
            deleteCookieRole();

            var timeInterval = 500; // milliseconds (checks the cookie for every half second )

            var loop = setInterval(function () {
                if (IsCookieRoleValid()) {
                    if (roleNameDuplicate == false) {
                        document.getElementById('imgcheck').style.display = 'block';
                    }
                    else {
                        document.getElementById('imgcheck').style.display = 'none';
                    }
                   
                    document.getElementById('imgloadinggif').style.display = 'none';
                    clearInterval(loop)
                }

            }, timeInterval);
        }

        // cookies
        function deleteCookieRole() {

            var cook = getCookieRole("checkRole");
            if (cook != "") {
                document.cookie = "checkRole=; Path=/; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            }
        }

        function IsCookieRoleValid() {
            var cook = getCookieRole("checkRole");
            return cook != '';
        }

        function getCookieRole(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        /* == End Red == */


      

            function validateFields() {
                var errorMessage = document.getElementById('ctl00_HomeContentPlaceHolder_errorMessage');
                var dashboardTable = document.getElementById('rdioDashboardType_0').checked;
                var dashboardUser = document.getElementById('rdioDashboardType_1').checked;
                var dashboardLink = document.getElementById('rdioDashboardType_2').checked;
                //errorMessage.innerText = '';

                var dashboardTableID = $('#ctl00_HomeContentPlaceHolder_ddlDashboardTableID').val();               
                //var dashboardUserID = $('#ctl00_HomeContentPlaceHolder_ddlDashboard').val();
                var dashboardLinkText = document.getElementById('ctl00_HomeContentPlaceHolder_txtDashboardLink').value;
                var roleName = document.getElementById('ctl00_HomeContentPlaceHolder_txtRoleName').value;
                
                if (roleNameDuplicate == true) {
                    errorMessage.innerText = 'Error (Duplicate): ' + roleName + ' is already exists';
                    return false;
                }

                if (roleName == '') {
                    errorMessage.innerText = 'Error: Please enter a Role Name';
                    return false;
                }
                else if (dashboardTable == true) {
                    if (dashboardTableID == '') {
                        errorMessage.innerText = 'Error: Please select a table';
                        return false;
                    }
                    return true;
                  
                }
                else if (dashboardUser == true) {
                    //if (dashboardUserID == '') {
                    //    errorMessage.innerText = 'Error: Please select a User (email address) ';
                    //    return false;
                    //}
                   
                    return true;
                }
                else if (dashboardLink == true) {
                    if (dashboardLinkText == '') {
                        errorMessage.innerText = 'Error: Please enter a link';
                        return false;
                    }
                    return true;

                }

              
             
            }
            function validateFieldsShowButton() {
              
                var dashboardTable = document.getElementById('rdioDashboardType_0').checked;
                var dashboardUser = document.getElementById('rdioDashboardType_1').checked;
                var dashboardLink = document.getElementById('rdioDashboardType_2').checked;
                //errorMessage.innerText = '';

                var dashboardTableID = $('#ctl00_HomeContentPlaceHolder_ddlDashboardTableID').val();
                //var dashboardUserID = $('#ctl00_HomeContentPlaceHolder_ddlDashboard').val();
                var dashboardLinkText = document.getElementById('ctl00_HomeContentPlaceHolder_txtDashboardLink').value;
                var roleName = document.getElementById('ctl00_HomeContentPlaceHolder_txtRoleName').value;

                if (roleNameDuplicate == true) {
                    document.cookie = "checkRole=Flag"
                    return false;
                }

                if (roleName == '') {
                    return false;
                }
                else if (dashboardTable == true) {
                    if (dashboardTableID == '') {
                        return false;
                    }
                    return true;

                }
                else if (dashboardUser == true) {
                    //if (dashboardUserID == '') {
                    //    return false;
                    //}

                    return true;
                }
                else if (dashboardLink == true) {
                    if (dashboardLinkText == '') {
                        return false;
                    }
                    return true;

                }
                
               
               
               

            }

            function showHideSaveButton() {
                var errorMessage = document.getElementById('ctl00_HomeContentPlaceHolder_errorMessage');
                errorMessage.innerText = '';
                if (validateFieldsShowButton()) {
                    $('#lnkRoleGroupSave_Validator').hide();
                    $('#lnkRoleGroupSave').show();
                }
                else {
                    $('#lnkRoleGroupSave_Validator').show();
                    $('#lnkRoleGroupSave').hide();
                }
            }
                

        /* == Red 19092019: Ticket 4698 == */

            $(document).ready(function () {
                showHideSaveButton();

                $('#lnkRoleGroupSave_Validator').click(function (e) {
                    return validateFields();

                });

                $('#ctl00_HomeContentPlaceHolder_ddlDashboardTableID').change(function () {
                    showHideSaveButton();
                });

                //$('#ctl00_HomeContentPlaceHolder_ddlDashboard').change(function () {
                //    showHideSaveButton();
                //});

                //$('#ctl00_HomeContentPlaceHolder_txtDashboardLink').keydown(function () {
                //    showHideSaveButton();
                //});

                //$('#ctl00_HomeContentPlaceHolder_txtRoleName').keydown(function () {
                //    jsCheckRoleProgress();
                //});

                $('#ctl00_HomeContentPlaceHolder_txtDashboardLink').focusout(function () {
                    showHideSaveButton();
                });

                $('#ctl00_HomeContentPlaceHolder_txtRoleName').focusout(function () {
                    //document.getElementById('lnkRoleGroupSave_Validator').disabled = true;
                    jsCheckRoleProgress();
                    /* Red 20092019: Ticket 4698, lets check if the Role entered is existing == */
                    var mode = "edit";
                    var modeTile = document.getElementById('ctl00_HomeContentPlaceHolder_titleRole').innerHTML;
                    if (modeTile == "Add Role") {
                        mode = "add";
                    }
                    
                    var roleName = document.getElementById('ctl00_HomeContentPlaceHolder_txtRoleName').value;
                    var errorMessage = document.getElementById('ctl00_HomeContentPlaceHolder_errorMessage');
                    var where = "Role=@Role AND AccountID=@AccountID";
                    var url = "../DocGen/REST/Custom/Common/Actions/Request.ashx?" +
                       "RequestName=ReadTable" +
                       "&From=[Role]" +
                       "&Where=" + where +
                       "&ParamName001=@Role" +
                       "&ParamValue001=" + roleName +
                       "&ParamName002=@AccountID" +
                       "&ParamValue002=" + $('#hfAccountID').val();

                        $.getJSON(url, function (response) {
                          
                            data = response.data;
                            if (data.length > 0) {
                                if(mode == "add"){
                                    roleNameDuplicate = true;
                                }
                                else {
                                    roleNameDuplicate = false;
                                }                                
                            }
                            else {
                                roleNameDuplicate = false;
                            }
                            showHideSaveButton();

                        });
                });

                $('#rdioDashboardType_0').click(function () {
                    showHideSaveButton();
                });
                $('#rdioDashboardType_1').click(function () {
                    showHideSaveButton();
                });
                $('#rdioDashboardType_2').click(function () {
                    showHideSaveButton();
                });

              
                $('#ctl00_HomeContentPlaceHolder_gvUserTable_ctl01_Pager_cmdNext').click(function () {
                    if (Page_IsValid) {
                        $(".ajax-indicator-full").fadeIn();
                    }
                });


                $('#ctl00_HomeContentPlaceHolder_gvUserTable_ctl01_Pager_cmdPrev').click(function () {
                    if (Page_IsValid) {
                        $(".ajax-indicator-full").fadeIn();
                    }
                });

                $('#ctl00_HomeContentPlaceHolder_gvUserTable_ctl01_Pager_lnkGo').click(function () {
                    if (Page_IsValid) {
                        $(".ajax-indicator-full").fadeIn();
                    }
                });

                

                
            });


        

     
        /* == End Red == */


        //JA Ticket 2808
        //Added this code to turn off the autocomplete attribute of the form and the fields.
        $("#TDBDetailEmail").attr("autocomplete", "off");
        $("#ctl00_HomeContentPlaceHolder_TDBDetailPassword").attr("autocomplete", "off");

        function OpenAddAccount() {
            document.getElementById('hlAddAccount').click();
        }

        function OpenAddUserConfirm() {
            $('#hlAddUserLink').trigger('click');
        }

        function Submit() {

            document.forms["aspnetForm"].submit()

        }

        $(document).ready(function () {

            //jeRome Ticket 2808
            //Show Password Feature for the Password Field

            var ua = window.navigator.userAgent;
            var trident = ua.indexOf('Trident/');
            var edge = ua.indexOf('Edge/');
            var msie = ua.indexOf('MSIE ');

            if ((msie > 0) || (trident > 0) || (edge > 0)) {
                // IE 10 or older => return version number
                $('#ctl00_HomeContentPlaceHolder_TDBDetailPassword').css('width', '250px');
                $('#ctl00_HomeContentPlaceHolder_TDBDetailPassword').css('border-right', 'solid 1px #909090');
                $('#showPassword_icon').css('display', 'none');

            }

                // other browser
            else {

                $('#showPassword_icon').css('background', 'url("../../Images/eye-open-edited2.png")');
                $('#showPassword_icon').css('background-position-x', 'center');
                $('#showPassword_icon').css('background-position-y', 'center');
                $('#showPassword_icon').css('background-repeat', 'no-repeat');
                $('#showPassword_icon').css('background-color', 'white');

                $('#showPassword_icon').on('click', function () {

                    if ($(this).attr('value') == 1) {
                        $(this).attr('value', 0);
                        document.getElementById('ctl00_HomeContentPlaceHolder_TDBDetailPassword').type = 'text';
                        $(this).css('background', 'url("../../Images/eye-close-edited2.png")');
                        $(this).css('background-position-x', 'center');
                        $(this).css('background-position-y', 'center');
                        $(this).css('background-repeat', 'no-repeat');
                        $(this).css('background-color', 'white');


                    } else {
                        $(this).attr('value', 1);
                        document.getElementById('ctl00_HomeContentPlaceHolder_TDBDetailPassword').type = 'password';
                        $(this).css('background', 'url("../../Images/eye-open-edited2.png")');
                        $(this).css('background-position-x', 'center');
                        $(this).css('background-position-y', 'center');
                        $(this).css('background-repeat', 'no-repeat');
                        $(this).css('background-color', 'white');
                    }

                });

            }
            //end jeRome Ticket 2808 

            function SetFolder(iFolderID) {
                document.getElementById('hfParentFolderID').value = iFolderID;
                document.getElementById('btnRefreshFolder').click();
            }

            $(function () {
                $(".popuplink").fancybox({
                    iframe: {
                        css: {
                            width: '900px',
                            height: '350px'
                        }
                    },
                    toolbar: false,
                    smallBtn: true,
                    scrolling: 'auto',
                    type: 'iframe',
                    titleShow: false
                });
            });

            $(function () {
                $(".popuplink2").fancybox({
                    iframe: {
                        css: {
                            width: '900px',
                            height: '300px'
                        }
                    },
                    toolbar: false,
                    smallBtn: true,
                    scrolling: 'auto',
                    type: 'iframe',
                    titleShow: false
                });
            });

            $(function () {
                $(".rolepopuplink").fancybox({
                    iframe: {
                        css: {
                            width: '550px',
                            height: '250px'
                        }
                    },
                    toolbar: false,
                    smallBtn: true,
                    scrolling: 'auto',
                    type: 'iframe',
                    titleShow: false
                });
            });

            //function OpenAddAccount() {
            //    document.getElementById('hlAddAccount').click();
            //}


            function OpenDashResetConfirm() {
                $('#hlResetDashBoard').trigger('click');

            }



            function abc() {
                var b = document.getElementById('<%= lnkSave.ClientID %>');
                if (b && typeof (b.click) == 'undefined') {
                    b.click = function () {
                        var result = true;
                        if (b.onclick) result = b.onclick();
                        if (typeof (result) == 'undefined' || result) {
                            eval(b.getAttribute('href'));
                        }
                    }
                }

            }


            //ShowHideDashboardType();

            $(function () {
                hideDashboardOptions($("input[name='<%=rdioDashboardType.UniqueID %>']:checked").val());
            })

            $('#rdioDashboardType input').click(function () {
                hideDashboardOptions($("input[name='<%=rdioDashboardType.UniqueID %>']:checked").val());
            });

            function hideDashboardOptions(value)
            {
                switch (value)
                {
                    case "T":
                        $("#trDashboardDefaultFromUserID").hide();
                        $("#trDashboardLink").hide();
                        $("#trDashboardTableID").show();
                        break;
                    case "L":
                        $("#trDashboardDefaultFromUserID").hide();
                        $("#trDashboardLink").show();
                        $("#trDashboardTableID").hide();
                        break;
                    default:
                        $("#trDashboardDefaultFromUserID").show();
                        $("#trDashboardLink").hide();
                        $("#trDashboardTableID").hide();
                }
                console.log($("#trDashboardDefaultFromUserID"));
            }

        });
    </script>
    <asp:Panel ID="Panel2" runat="server" DefaultButton="lnkSave">
        <div runat="server" id="divDetail">
            <table border="0" cellpadding="0" cellspacing="0" style="width:50%;" align="center">              
                <tr>
                    <td>
                         <div class="container">
                               <div class="row">
                                   <%--Header section--%>
                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-12">
                              <%--Header section--%>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left">
                                    <span class="TopTitle">
                                        <asp:Label runat="server" ID="lblTitle"></asp:Label></span>
                                </td>
                                <td align="right">
                                    <table>
                                        <tr>
                                          
                                            <td style="text-align: right">
                                                <div style="float:right">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <div>
                                                                    <asp:HyperLink runat="server" ID="hlBack">
                                                                        <asp:Image runat="server" ID="imgBack" ImageUrl="~/App_Themes/Default/images/Back.png"  ToolTip="Back" />
                                                                    </asp:HyperLink>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div runat="server" id="divEdit" visible="false">
                                                                    <asp:HyperLink runat="server" ID="hlEditLink">
                                                                        <asp:Image runat="server" ID="Image2"  ImageUrl="~/App_Themes/Default/images/Edit_big.png"  ToolTip="Edit" />
                                                                    </asp:HyperLink>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div runat="server" id="divDelete" visible="false">
                                                                    <asp:LinkButton runat="server" ID="lnkDelete" OnClientClick="javascript:return confirm('Are you sure you want to delete this User?')"
                                                                        CausesValidation="false" OnClick="lnkDelete_Click">
                                                                        <asp:Image runat="server" ID="Image3"  ImageUrl="~/App_Themes/Default/images/delete_big.png"  ToolTip="Delete" />
                                                                    </asp:LinkButton>
                                                                </div>
                                                                <div runat="server" id="divUnDelete" visible="false">
                                                                    <asp:LinkButton runat="server" ID="lnkUnDelete" OnClientClick="javascript:return confirm('Are you sure you want to restore this User?')"
                                                                        CausesValidation="false" OnClick="lnkUnDelete_Click">
                                                                        <asp:Image runat="server" ID="Image4"  ImageUrl="~/App_Themes/Default/images/Restore_Big.png"  ToolTip="Restore" />
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div runat="server" id="divSave">
                                                                    <asp:LinkButton runat="server" ID="lnkSave" OnClick="lnkSave_Click" CausesValidation="true"
                                                                        ValidationGroup="msg">
                                                                        <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png"  ToolTip="Save" />
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                         </div>
                                   <%--End Header--%>
                                     <div class="col-xs-12 col-sm-8 col-md-8 col-lg-12">
                                           <h1></h1>
                                     </div>
                                  
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-7">
                                      <div class="col-xs-12 col-sm-8 col-md-8 col-lg-1">

                                    </div>
                                          <table>
                                               <tr>

                    <td valign="top"></td>
                    <td valign="top">
                        <div id="search" style="padding-bottom: 10px">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                                ValidationGroup="msg" ShowMessageBox="true" ShowSummary="false" HeaderText="Please correct following errors:" />
                        </div>
                        <table cellpadding="3">
                            <tr runat="server" id="trPrimaryAccount" visible="false">
                                <td align="right">
                                    <strong>Primary Account</strong>
                                </td>
                                <td align="left">
                                    <asp:Label runat="server" ID="lblPrimaryAccount"></asp:Label>
                                </td>
                            </tr>

                            <tr>
                                <td align="right">
                                    <strong>Email*</strong>
                                </td>
                                <td>
                                    <asp:TextBox ID="TDBDetailEmail" runat="server" Width="250px" placeholder="Email Address" CssClass="NormalTextBox "
                                        Text="" ClientIDMode="Static"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hfUserName" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfAccountID" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfMode" ClientIDMode="Static" />
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="TDBDetailEmail"
                                        ErrorMessage="Email - Required" Display="None" ValidationGroup="msg"></asp:RequiredFieldValidator>
                                    &nbsp;
                                    <asp:RegularExpressionValidator Display="Dynamic" ID="REVEmail" runat="server" ControlToValidate="TDBDetailEmail"
                                        ErrorMessage=" Invalid Email" ValidationExpression="^([a-zA-Z0-9_\-\.])+@(([0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5])|((([a-zA-Z0-9\-])+\.)+([a-zA-Z\-])+))$">
                                    </asp:RegularExpressionValidator>
                                </td>
                              
                            </tr>

                            <tr runat="server" id="trPassword">
                                <td align="right">
                                    <asp:Label runat="server" ID="lblPassword" Text="Password*" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TDBDetailPassword" runat="server" CssClass="NormalTextBox" TextMode="Password"
                                        Text="" Width="224px"></asp:TextBox>

                                    <input id="showPassword_icon" type="button" value="1" />
                                    <%--//jeRome Ticket 2808--%>

                                    <ajaxToolkit:PasswordStrength ID="PS" runat="server" TargetControlID="TDBDetailPassword"
                                        DisplayPosition="RightSide" StrengthIndicatorType="Text" PreferredPasswordLength="20"
                                        PrefixText="Strength:" TextCssClass="TextIndicator" MinimumNumericCharacters="2"
                                        MinimumSymbolCharacters="2" RequiresUpperAndLowerCaseCharacters="true" MinimumLowerCaseCharacters="2"
                                        MinimumUpperCaseCharacters="1" TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent"
                                        TextStrengthDescriptionStyles="TextIndicator_Strength1;TextIndicator_Strength2;TextIndicator_Strength3;TextIndicator_Strength4;TextIndicator_Strength5"
                                        CalculationWeightings="50;15;15;20" />
                                    <asp:RegularExpressionValidator Display="None" ID="revPasswordLength" runat="server"
                                        ControlToValidate="TDBDetailPassword" ValidationGroup="msg" ErrorMessage="Password Minimum length is 6."
                                        ValidationExpression=".{6,30}"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="TDBDetailPassword"
                                        ErrorMessage="Password - Required" Display="None" ValidationGroup="msg"></asp:RequiredFieldValidator>
                                </td>
                            </tr>

                            <%-- //red Ticket 3059--%>
                            <tr id="trhlChangePassword" runat="server">
                                <td align="right">&nbsp;</td>
                                <td>
                                    <asp:HyperLink runat="server" ID="hlChangePassword">Reset Password</asp:HyperLink>
                                </td>
                            </tr>

                            <tr>
                                <td align="right">
                                    <strong>First Name*</strong>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtFirstName" runat="server" Width="200px" CssClass="NormalTextBox"
                                        Text=""></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtFirstName"
                                        ErrorMessage="First Name - Required" Display="None" ValidationGroup="msg"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <strong>Last Name*</strong>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtLastName" runat="server" CssClass="NormalTextBox" Text="" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastName"
                                        ErrorMessage="Last Name - Required" Display="None" ValidationGroup="msg"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <%--<tr runat="server" id="trPassword">
                                <td align="right">
                                    <asp:Label runat="server" ID="lblPassword" Text="Password*" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="NormalTextBox" TextMode="Password"
                                        Text="" Width="200px"></asp:TextBox>
                                    <ajaxToolkit:PasswordStrength ID="PS" runat="server" TargetControlID="txtPassword"
                                        DisplayPosition="RightSide" StrengthIndicatorType="Text" PreferredPasswordLength="20"
                                        PrefixText="Strength:" TextCssClass="TextIndicator" MinimumNumericCharacters="2"
                                        MinimumSymbolCharacters="2" RequiresUpperAndLowerCaseCharacters="true" MinimumLowerCaseCharacters="2"
                                        MinimumUpperCaseCharacters="1" TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent"
                                        TextStrengthDescriptionStyles="TextIndicator_Strength1;TextIndicator_Strength2;TextIndicator_Strength3;TextIndicator_Strength4;TextIndicator_Strength5"
                                        CalculationWeightings="50;15;15;20" />
                                    <asp:RegularExpressionValidator Display="None" ID="revPasswordLength" runat="server"
                                        ControlToValidate="txtPassword" ValidationGroup="msg" ErrorMessage="Password Minimum length is 6."
                                        ValidationExpression=".{6,30}"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                        ErrorMessage="Password - Required" Display="None" ValidationGroup="msg"></asp:RequiredFieldValidator>
                                </td>
                            </tr>--%>
                            <%--<tr>
                                <td align="right">
                                    <strong>Email*</strong>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmail" runat="server" Width="250px" CssClass="NormalTextBox"
                                        Text="" ClientIDMode="Static"></asp:TextBox>                                    
                                    <asp:HiddenField runat="server" ID="hfUserName" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfMode" ClientIDMode="Static" />
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                                        ErrorMessage="Email - Required" Display="None" ValidationGroup="msg"></asp:RequiredFieldValidator>
                                    &nbsp;
                                    <asp:RegularExpressionValidator Display="Dynamic" ID="REVEmail" runat="server" ControlToValidate="txtEmail"
                                        ErrorMessage=" Invalid Email" ValidationExpression="^([a-zA-Z0-9_\-\.])+@(([0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5])|((([a-zA-Z0-9\-])+\.)+([a-zA-Z\-])+))$">
                                    </asp:RegularExpressionValidator>
                                </td>
                            </tr>--%>
                            <tr>
                                <td align="right" style="width:30%">
                                    <strong>Phone Number</strong>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="NormalTextBox" Text=""
                                        Width="200px"></asp:TextBox>
                                </td>
                            </tr>
                            <%--KG 30/11/17 Ticket 3374--%>
                           <%-- <tr id="trPrimaryDDL" runat="server">
                                <td align="right">
                                    <strong>Primary Account</strong>
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlPrimaryAccount" CssClass="NormalTextBox">
                                    </asp:DropDownList>
                                </td>
                            </tr>--%>
                            <tr>
                                <td align="right">
                                    <strong>Role*</strong>
                                </td>
                                <td style="width:400px">
                                    <asp:DropDownList ID="ddlBasicRoles" runat="server" CssClass="NormalTextBox" DataTextField="Role" DataValueField="RoleID"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlBasicRoles_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    &nbsp;
                                                             <div id="divRole" runat="server" visible ="false">
                                    <asp:LinkButton runat="server" ID="lnkRoleNew" Text="New" CausesValidation="false" Visible="true" OnClick="lnkRoleNew_Click" />
                                    &nbsp;
                                    <asp:LinkButton runat="server" ID="lnkRoleEdit" CausesValidation="false" Text="Edit" Visible="false" OnClick="lnkRoleEdit_Click" />
                                    &nbsp;
                                    <asp:HyperLink runat="server" ID="hlRoleGroupNew" CssClass="rolepopuplink" Text="New" style="visibility:hidden" />
                                    </div>
                                </td>
                            </tr>
                            <%--== Transferred here, per requirements: Ticket 4698 ==--%>
                              <tr id="trPrimaryDDL" runat="server">
                                <td align="right">
                                    <strong>Primary Account</strong>
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlPrimaryAccount" CssClass="NormalTextBox">
                                    </asp:DropDownList>
                                </td>
                            </tr>


                            <%--<tr >
                                <td align="right">
                                   
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" id="chkUserRoleEditDashBoard" TextAlign="Right" 
                                        Text="Edit Dashboard" Font-Bold="true"/>
                                </td>
                            </tr>--%>

                            <tr runat="server" id="trTwoFactorAuthenticatorButton" visible="false">
                                <td align="right">&nbsp;</td>
                                <td>
                                    <asp:LinkButton ID="lnkTwoFactorAuthGenerator" CssClass="btn" Visible="false" runat="server" OnClientClick="TFA(); return false;">Reset Two Factor Authentication</asp:LinkButton>
                                </td>
                            </tr>
                            <%-- 
                            <tr runat="server" id="trDataScopeTable" visible="false">
                                <td align="right">
                                    <strong>Data Scope</strong>
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlScopeTable" DataTextField="TableName"
                                        DataValueField="TableID" CssClass="NormalTextBox" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlScopeTable_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>


                            <tr runat="server" id="trDataScopeValue" visible="false">
                                <td align="right">
                                    <strong>Field</strong>
                                </td>
                                <td>
                                    <table cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddlScopeField" DataTextField="DisplayName" DataValueField="ColumnID"
                                                    CssClass="NormalTextBox" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlScopeField_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddlScopeValue" CssClass="NormalTextBox">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            --%>
                            <tr runat="server" visible="false">
                                <td align="right"></td>
                                <td>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chkNotifyUser" runat="server" Checked="false" />
                                            </td>
                                            <td>
                                                &nbsp;<strong>Notify user of account details</strong>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr runat="server" id="trAccountHolder" visible="false">
                                <td align="right">
                                    <strong>Account Holder</strong>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkAccountHolder" runat="server" Checked="false" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right"></td>
                                <td>
                                    <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <br />
                      
                    </td>
                    <td></td>
                </tr>
                                         </table> 
                                        
                                </div>
                                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-4">
                                    <table>
                                       
                                        <tr>
                                           <td>
                                              
                                               <div style="padding-bottom:10px; text-align: center">
                                                     <strong>Profile Picture</strong>
                                               </div>
                                                <asp:Panel ID="pnlPhoto" runat="server">
                                                    <asp:Image ID="imgPhoto" runat="server" ImageUrl="~/Images/NoPhoto.jpg" Width="150px" BorderStyle="Solid" BorderColor="#565656" BorderWidth="1px" />
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>
                                                <asp:FileUpload ID="fuPhoto" runat="server" Font-Size="11px" onchange="javascript:Submit()"
                                                    size="45" Style="width: 150px;" />
                                                <asp:HiddenField runat="server" ID="hfPhoto" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:Label runat="server" ID="lblImageMsg" ForeColor="Red"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                     <div class="col-xs-12 col-sm-8 col-md-8 col-lg-12">
                                           
                                <div class="col-md-12">
                                    <ajaxToolkit:TabContainer id="TabContainer1" runat="server" cssclass="DBGTab">
                            <ajaxToolkit:TabPanel ID="TabDataScope" runat="server" Visible="false">
                                <HeaderTemplate>
                                    <strong>Data Scope</strong>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div style="padding:30px">
                                        <table style="min-width: 300px;" cellpadding="3" cellspacing="3">
                                            <tr runat="server" id="trDataScopeTable">
                                                <td align="right">
                                                    <strong>Table</strong>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlScopeTable" DataTextField="TableName"
                                                        DataValueField="TableID" CssClass="NormalTextBox" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddlScopeTable_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr runat="server" id="trDataScopeValue">
                                                <td align="right">
                                                    <strong>Field</strong>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlScopeField" DataTextField="DisplayName" DataValueField="ColumnID"
                                                        CssClass="NormalTextBox" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddlScopeField_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <strong>Value</strong>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlScopeValue" CssClass="NormalTextBox">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="TabRole" runat="server" Visible="false">
                                <HeaderTemplate>
                                    <strong>User Roles</strong>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="tabDocuments" runat="server">
                                <HeaderTemplate>
                                    <strong>Documents</strong>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div style="padding: 10px;">
                                        <br />
                                        <div runat="server" id="divBasicDocSec">
                                            <asp:DropDownList ID="ddlBasicDocSec" runat="server" CssClass="NormalTextBox">
                                                <asp:ListItem Value="none" Text="None"></asp:ListItem>
                                                <asp:ListItem Value="read" Text="Read Only"></asp:ListItem>
                                                <asp:ListItem Value="upload" Text="Read and Upload"></asp:ListItem>
                                                <asp:ListItem Value="full" Text="Administrator" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div runat="server" id="divDocAdvancedSec" visible="false">
                                            <p>
                                                <asp:Label Visible="false" runat="server" ID="lblCurrentFolder" Text="<a href='javascript:SetFolder(-1)'>Home</a>/"></asp:Label>
                                            </p>
                                            <asp:GridView ID="gvDocAdvancedSec" runat="server" AutoGenerateColumns="False" DataKeyNames="FolderID"
                                                CssClass="gridview" GridLines="Both" OnRowDataBound="gvDocAdvancedSec_RowDataBound">
                                                <HeaderStyle CssClass="gridview_header" />
                                                <Columns>
                                                    <asp:TemplateField Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="LblID" Text='<%# Eval("FolderID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <%--<asp:HyperLink ID="hlFolderIcon" runat="server" ToolTip="Folder" NavigateUrl='#'
                                                                ImageUrl="~/App_Themes/Default/Images/Folder.png">
                                           
                                                            </asp:HyperLink>--%>
                                                            <asp:Image runat="server" ID="imgFolderIcon" ToolTip="Folder"
                                                                ImageUrl="~/App_Themes/Default/Images/Folder.png" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField Visible="true" HeaderText="Folder">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblFolderName" CssClass="NormalText" Text='<%# Eval("FolderName") %>'></asp:Label>
                                                            <%--<asp:LinkButton ID="lnkFolderName" runat="server" OnClick="GoToFolder" Text='<%# Eval("FolderName") %>'></asp:LinkButton>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Right">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlBasicDocSecEach" runat="server" CssClass="NormalTextBox">
                                                                <asp:ListItem Value="none" Text="None"></asp:ListItem>
                                                                <asp:ListItem Value="read" Text="Read Only"></asp:ListItem>
                                                                <asp:ListItem Value="upload" Text="Read and Upload"></asp:ListItem>
                                                                <asp:ListItem Value="full" Text="Administrator" Selected="True"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <br />
                                        <asp:CheckBox runat="server" ID="chkDocAdvancedSec" Text="Advanced Security" Font-Bold="true"
                                            OnCheckedChanged="chkDocAdvancedSec_CheckedChanged" AutoPostBack="true" />
                                        <br />
                                    </div>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                            <ajaxToolkit:TabPanel ID="TabLinkedAccounts" runat="server">
                                <HeaderTemplate>
                                    <strong>Linked Accounts</strong>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="upLinkedAccounts" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="padding: 10px;">
                                                <asp:HyperLink runat="server" ID="hlAddAccount" CausesValidation="false" ClientIDMode="Static"
                                                    Text="Add Account" NavigateUrl="#" CssClass="popuplink2" Style="display: none;" />
                                                <br />

                                                <div id="divGridMain" style="padding-right: 10px;">
                                                    <dbg:dbgGridView ID="gvTheGrid" runat="server" GridLines="Both" CssClass="gridview"
                                                        HeaderStyle-HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" AllowPaging="True"
                                                        AllowSorting="false" DataKeyNames="UserRoleID" HeaderStyle-ForeColor="Black"
                                                        Width="100%" AutoGenerateColumns="false" PageSize="15"
                                                        OnPreRender="gvTheGrid_PreRender" OnRowDataBound="gvTheGrid_RowDataBound">
                                                        <PagerSettings Position="Top" />
                                                        <Columns>
                                                            <asp:TemplateField Visible="false">
                                                                <ItemStyle Width="10px" HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LblID" runat="server" Text='<%# Eval("UserRoleID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemStyle Width="10px" HorizontalAlign="Left" />
                                                                <HeaderTemplate>
                                                                    <input id="chkAll" onclick="DoMasterSelect(this, 'divGridMain')" runat="server" type="checkbox" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkDelete" runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Account Name" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAccountName" runat="server" Text='<%# Eval("AccountName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Role" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemStyle HorizontalAlign="Left" />
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="gridview_header" />
                                                        <RowStyle CssClass="gridview_row" />
                                                        <PagerTemplate>
                                                            <asp:GridViewPager runat="server" ID="Pager" HideDelete="false" HideAdd="false" HideExport="false"
                                                                OnExportForCSV="Pager_OnExportForCSV" OnDeleteAction="Pager_DeleteAction"
                                                                OnBindTheGridToExport="Pager_BindTheGridToExport" OnBindTheGridAgain="Pager_BindTheGridAgain"
                                                                HideFilter="true" />
                                                        </PagerTemplate>
                                                    </dbg:dbgGridView>
                                                </div>
                                                <br />
                                                <div runat="server" id="divEmptyData" visible="false" style="padding-left: 100px;">
                                                    <asp:HyperLink runat="server" ID="hplNewData" Style="text-decoration: none; color: Black;">
                                              <asp:Image runat="server" ID="imgAddNewRecord" ImageUrl="~/App_Themes/Default/images/BigAdd.png"  />
                                              No accounts have been added yet. <strong style="text-decoration: underline; color: Blue;">
                                                  Add new account now.</strong>
                                                    </asp:HyperLink>
                                                </div>
                                                <br />
                                                <br />



                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="gvTheGrid" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="tpAttributes" runat="server" Visible="false">
                                <HeaderTemplate>
                                    <strong>Attributes</strong>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div  style="padding: 10px;">
                                        <h4>Custom  Attributes</h4>
                                        <div style="border:1px solid;">
                                            <table cellpadding="3">
                                                <tr>
                                                    <td align="right">
                                                        <strong> Rate:</strong>
                                                    </td>
                                                    <td align="left">
                                                       <asp:TextBox runat="server" ID="txtRate" CssClass="NormalTextBox"></asp:TextBox>
                                                        <asp:RegularExpressionValidator runat="server" ID="revRate"  ErrorMessage="Numeric value please." ValidationGroup="msg" 
                                                            ControlToValidate="txtRate" ValidationExpression="(^-?\d{1,20}\.$)|(^-?\d{1,20}$)|(^-?\d{0,20}\.\d{1,15}$)"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <strong> Last Seen Location:</strong>
                                                    </td>
                                                    <td align="left">
                                                        <div id="mapLastseenlocation" style="height:300px;width:500px;"></div>
                                                        <br />
                                                        <asp:Label runat="server" ID="lblLastSeenAddress"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <strong>Last Seen Time: </strong>
                                                    </td>
                                                    <td align="left">
                                                       <asp:Label runat="server" ID="lblLastSeenTime"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>

                        </ajaxToolkit:TabContainer>

                                </div>
                                       
                                         </div>
                               </div>
                          </div>

                    </td>
                </tr>
              
                <tr>
                    <td>
                        <asp:HiddenField runat="server" ID="hfUserID" ClientIDMode="Static" Value="-1" />
                        <asp:HiddenField runat="server" ID="hfParentFolderID" Value="-1" ClientIDMode="Static" />
                        <asp:Button runat="server" ID="btnRefreshLinkedUser" Style="display: none;" OnClick="btnRefreshLinkedUser_Click"
                            ClientIDMode="Static" />
                        <asp:Button ID="btnRefreshFolder" runat="server" Visible="true" ClientIDMode="Static"
                            Text="" Height="1px" Width="1px" Style="display: none;" OnClick="btnFolderSaved_Click" />

                        <asp:Button ID="btnRoleSaved" runat="server" Visible="true" ClientIDMode="Static" Text=""
                            Height="1px" Width="1px" Style="display: none;" OnClick="btnRoleSaved_Click" />
                        <asp:HiddenField runat="server" ID="hfRoleGroupID" Value="" ClientIDMode="Static" />
                        <asp:Button runat="server" ID="btnAddUserLinkOK" ClientIDMode="Static" Style="display: none;" OnClick="btnAddUserLinkOK_Click" CausesValidation="false" />
                        <asp:Button runat="server" ID="btnRestoreUser" ClientIDMode="Static" Style="display: none;" OnClick="btnRestoreUser_Click" />
                        <asp:HyperLink ID="hlAddUserLink" ClientIDMode="Static" runat="server" CssClass="popupaddlinkuser" Style="display: none;"></asp:HyperLink>
                        <asp:HyperLink ID="hlResetDashBoard" ClientIDMode="Static" runat="server" CssClass="popupaddlinkuser" Style="display: none;"></asp:HyperLink>
                        <asp:Button runat="server" ID="btnResetDashBoard" ClientIDMode="Static" Style="display: none;" OnClick="btnResetDashBoard_Click" />

                    </td>
                </tr>
            </table>
        </div>

        <%-- Red Ticket 2653 22may2017 --%>
        <div id="divTwoFactorAuthenticator">

            <table>
                <tr>
                    <td>
                        <div style="text-align: center">
                            <h2 class="headerTimeout">Two Factor Authenticator</h2>

                        </div>
                    </td>
                </tr>

                <tr style="text-align: center">
                    <td>
                        <div style="text-align: center">

                            <br />
                        </div>
                        <div style="text-align: center">

                            <asp:Label ID="lblResetQuestion" runat="server" Text="Label"></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr style="text-align: center">
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <div style="text-align: center">
                            <asp:LinkButton ID="lnkReset" CausesValidation="false" CssClass="btn" runat="server" ForeColor="White" OnClick="lnkReset_Click">Reset</asp:LinkButton>

                            &nbsp;
                            
			           <asp:LinkButton ID="lnkCancel" OnClientClick="TFAClose(); return false;" CausesValidation="false" ClientIDMode="Static" CssClass="btn" runat="server" ForeColor="White">Cancel</asp:LinkButton>
                        </div>



                    </td>
                </tr>
            </table>

        </div>


        


        <%-- end Red--%>

        <%-- RP Modified Ticket 4698 --%>
        <asp:Button ID="btnPopUp" runat="server" Visible="true" Text="POP" style="visibility:hidden" />
        <ajaxToolkit:modalpopupextender id="modalRolePopUp" runat="server" 
                cancelcontrolid="btnCancel" okcontrolid="btnOK" targetcontrolid="btnPopUp" popupcontrolid="panelRole" 
                drag="true" BackgroundCssClass="ModalPopupBG"> 
        </ajaxToolkit:modalpopupextender>
        <asp:Panel id="panelRole" style="display:none; min-height:400px; " CssClass="PopupBox"  runat="server">
            <br />
                
            <div runat="server" id="divBasicRoles">
                 <table style="width: 100%" cellpadding="5">
                     <tr>
                        <td colspan="3" style="padding-left:20px; padding-bottom: 30px">
                            <asp:Label ID="lblDescription" runat="server" Visible="false" Text="Edit Role" /> 
                            <h3 runat="server" id="titleRole">Edit Role</h3>                          
                        </td>
                        <td align="right">
                             <asp:Button ID="btnOK" runat="server" style="visibility:hidden" />
                            <asp:LinkButton ID="btnCancel" runat="server" OnClick="btnCancel_Click" ><asp:Image runat="server" ID="Image1" ImageUrl="~/App_Themes/Default/images/Back.png"  ToolTip="Back" /></asp:LinkButton>
                            <input type="image" id="lnkRoleGroupSave_Validator" src="../../App_Themes/Default/Images/save.png" />   
                              <asp:LinkButton  runat="server" ID="lnkRoleGroupSave" ClientIDMode="Static" ValidationGroup="RoleGroup" OnClick="lnkRoleGroupSave_Click">
                                   <asp:Image runat="server" ID="Image5" ImageUrl="~/App_Themes/Default/images/Save.png"  ToolTip="Save" />
                            </asp:LinkButton>                       
                            <%-- 
                            <asp:LinkButton runat="server" ID="lnkSaveNewRole" OnClick="lnkSaveNewRole_Click" Visible="false" ValidationGroup="RoleGroup">
                                <asp:Image runat="server" ID="Image6" ImageUrl="~/App_Themes/Default/images/Save.png"  ToolTip="Save" />
                            </asp:LinkButton>
                            --%>
                            <asp:LinkButton runat="server" ID="lnkRoleDelete" OnClientClick="javascript:return confirm('Are you sure you want to delete this Role?')"
                                CausesValidation="false" OnClick="lnkRoleDelete_Click" Visible="false">
                                <asp:Image runat="server" ID="Image6"  ImageUrl="~/App_Themes/Default/images/delete_big.png"  ToolTip="Delete" />
                            </asp:LinkButton>
                            &nbsp;&nbsp;
                            <asp:LinkButton runat="server" ID="lnkRoleGroupDelete" CssClass="btn"
                                OnClick="lnkRoleGroupDelete_Click" OnClientClick="return confirm('Are you sure you wish to delete the current role?');"><strong>Delete Role</strong>  </asp:LinkButton>
                        </td>                          
                     </tr>
                     <tr>
                         <td style="text-align:right">
                             <strong>Role Name:*</strong>
                         </td>
                         <td style="width:300px">
                            <asp:TextBox ID="txtRoleName" Width="300px" runat="server" />  
                         </td>
                         <td style="text-align:left">  
                             <img style="display: none" id="imgloadinggif" align="left" src="../../Images/loading_sm.gif" alt="loading.." />
                             <img style="display: none" id="imgcheck" align="left" src="../../mobile-images/check.png" alt="check" />

                         </td>
                           <td></td>
                     </tr>
                     <tr id="trAfterLoginDisplay" runat="server">
                          <td style="text-align:right"><strong>After Login Display:</strong></td>
                         <td colspan="3">
                            <asp:RadioButtonList runat="server" ID="rdioDashboardType" ClientIDMode="Static"
                                RepeatDirection="Horizontal" OnSelectedIndexChanged="rdioDashboardType_SelectedIndexChanged" AutoPostBack="false">
                                <asp:ListItem Value="T" Text="Table"></asp:ListItem>
                                <asp:ListItem Value="U" Text="User Dashboard" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="L" Text="Link"></asp:ListItem>
                            </asp:RadioButtonList>
                         </td>
                     </tr>
                     <tr id="trDashboardDefaultFromUserID" clientidmode="Static" runat="server">
                        <td style="text-align:right; vertical-align:top"><strong>User Dashboard:</strong></td>
                        <td colspan="3">
                            <asp:DropDownList runat="server" ID="ddlDashboard" DataTextField="Email"
                                DataValueField="UserID" CssClass="NormalTextBox">
                            </asp:DropDownList>
                            
                            <br />
                            <asp:CheckBox runat="server" ID="chkRoleEditDashboard" Text="Edit Dashboard"
                                             TextAlign="Right" />
                            <asp:LinkButton runat="server" ID="lnkResetDashBoard"
                                             OnClick="lnkResetDashBoard_Click">Reset</asp:LinkButton>
                        </td>
                     </tr>
                     <tr id="trDashboardTableID" clientidmode="Static" runat="server">
                        <td style="text-align:right"><strong>Table:*</strong></td>
                        <td colspan="3">
                            <asp:DropDownList runat="server" ID="ddlDashboardTableID" DataTextField="TableName"
                                DataValueField="TableID" CssClass="NormalTextBox">
                            </asp:DropDownList>
                                   
                        </td>
                     </tr>
                     <tr id="trDashboardLink" clientidmode="Static" runat="server">
                         <td style="text-align:right; vertical-align:top"><strong>Link:*</strong></td>
                         <td colspan="3">
                            <asp:TextBox runat="server" ID="txtDashboardLink" CssClass="NormalTextBox" Width="400px"></asp:TextBox>
                               
                         </td>
                     </tr>
                     <tr id="trViewAllTables" runat="server">
                         <td style="text-align:right"><strong>View(all tables):</strong></td>
                         <td colspan="3">
                             <asp:DropDownList runat="server" ID="ddlRole_ViewsDefaultFromUserID" DataTextField="Email"
                                 DataValueField="UserID" CssClass="NormalTextBox">
                             </asp:DropDownList>
                         </td>
                     </tr>
                     <tr  id="divUserRole" visible="false" runat="server">
                         <td>&nbsp;</td>
                         <td>
                             <asp:CheckBox runat="server" ID="chkRole_AllowEditView" Text="Edit View" TextAlign="Right" />
                         </td>
                     </tr>
                     <tr id="divRoleSpecialRights" visible="false" runat="server" clientidmode="Static">
                         <td valign="top"><strong>User Rights:</strong></td>
                         <td>
                             <asp:CheckBox runat="server" ID="chkAllowDeleteTable" TextAlign="Right" Text="Delete Tables" />
                             <br />
                             <asp:CheckBox runat="server" ID="chkAllowDeleteColumn" TextAlign="Right" Text="Delete Fields" />
                             <br />
                             <asp:CheckBox runat="server" ID="chkAllowDeleteRecord" TextAlign="Right" Text="Permanently Delete Records" />
                         </td>
                     </tr>
                 </table>
            </div>
            <br />
            <table runat="server" visible="false">
                <tr>
                    <td>
                        <div>
                            <table>
                                <tr>
                                    <td colspan="2" valign="top">

                                        <div style="padding: 5px; border: solid 2px #909090; margin-left: 30px; height: 150px;">
                                            <strong>Role</strong>
                                            <br />
                                            <br />
                                            <div style="padding-left: 20px;">

                                                <table>
                                                    <tr>
                                                        <td></td>
                                                        <td colspan="3">
                                                            

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <strong>Table</strong>
                                                        </td>
                                                        <td>

                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <strong>Link</strong>
                                                        </td>

                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr runat="server" id="trViewAllTable">
                                                        <td align="right">
                                                            <strong>View(all tables)</strong>
                                                        </td>
                                                        <td>

                                                        </td>
                                                        <td>
                                                            
                                                        </td>
                                                        <td>
                                                            <%--  <asp:LinkButton runat="server" ID="lnkResetViews"
                                OnClick="lnkResetViews_Click">Reset</asp:LinkButton>--%>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </div>

                                        </div>
                                    </td>

                                </tr>

                            </table>
                        </div>
                    </td>
                    <td>
                        <div>
                            <table>
                                <tr>
                                    <td>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <br />
              <div style="padding-left:20px" class="col-xs-12 col-sm-8 col-md-8 col-lg-12">
                                         <label runat="server" id="errorMessage" style="color:red; font-style:italic"></label>
                                     </div>
             
      <div runat="server" id="divUserTable" style="overflow-x:auto; padding: 20px 20px 20px 20px" >
          
                <asp:UpdatePanel runat="server" ID="panelUserTable" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="gridWrapper">
                           <dbg:dbgGridView ID="gvUserTable" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="TableID" 
                    CssClass="gridview_user" GridLines="Both" PageSize ="15" OnPreRender="gvUserTable_PreRender" 
                      OnRowDataBound="gvUserTable_RowDataBound" Width="100%" AlternatingRowStyle-BackColor="#DCF2F0">
                      <PagerSettings Position="Top" />
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <HeaderStyle Width="200px" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblTableID" Text='<%# Eval("TableID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="true" HeaderText="Table">
                            <ItemStyle HorizontalAlign="Center" Width="200px" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblTableName" CssClass="NormalText" Text='<%# Eval("TableName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Right">
                            <ItemStyle HorizontalAlign="Center" Width="200px" />
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlRecordRightID" runat="server" CssClass="NormalTextBox">
                                    <asp:ListItem Value="6" Text="None"></asp:ListItem>
                                      <asp:ListItem Value="7" Text="Add Record Data Only"></asp:ListItem>
                                      <asp:ListItem Selected="True" Value="4" Text="Add and Edit Record Data"></asp:ListItem>
                                      <asp:ListItem Value="8" Text="Own Data Only"></asp:ListItem>
                                     <asp:ListItem Value="9" Text="Edit Own Data, View Others"></asp:ListItem>                                   
                                    <asp:ListItem Value="5" Text="Read Only"></asp:ListItem>
                                     <asp:ListItem Value="2" Text="Administrator"></asp:ListItem>                                
                                    <%--<asp:ListItem Value="3" Text="Edit Record and Site Data"></asp:ListItem>--%>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="true" HeaderText="Can Export" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle Width="100px" />
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkCaExport" Checked="true" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User View" Visible="false" >
                            <ItemStyle Width="200px" />
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlViewsDefaultFromUserID" runat="server" CssClass="NormalTextBox" DataTextField="Email"
                                    DataValueField="UserID">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="true" HeaderText="Edit View" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle Width="100px" />
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkAllowEditView" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--== Red 24062019: Menu, Ticket 4689 ==--%>
                        <asp:TemplateField Visible="false" HeaderText="Show Menu" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle Width="100px" />
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkShowMenu" Checked="true" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                      <HeaderStyle CssClass="gridview_header" />
                            <RowStyle CssClass="gridview_row" />
                          
                    <PagerTemplate>
                                <asp:GridViewPager runat="server" ID="Pager" OnExportForCSV="Pager_OnExportForCSV"
                                    HideAdd="true" HideAllExport="true" HideRefresh="true" HideDelete="true"
                                     HideExcelExport="true" HideExport="true" HideFilter="true" 
                                     OnBindTheGridAgain="Pager_BindTheGridUserTableAgain" />
                            </PagerTemplate>
                </dbg:dbgGridView>
                        </div>
                    </ContentTemplate>
                      <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="gvUserTable" />
                    </Triggers>
                </asp:UpdatePanel>
             
            </div>
          
            <br />
            <%--<asp:CheckBox runat="server" ID="chkAdvancedSecurity" Text="Advanced Security" Font-Bold="true"
                OnCheckedChanged="chkAdvancedSecurity_CheckedChanged" AutoPostBack="true" />--%>
            <br />
        </asp:Panel>
        <%-- End Modification --%>
        <asp:Panel runat="server" ID="pnlOntaskAddusermessage" Visible="false">
            <div style="width:100%;text-align:center;">
                <asp:Label runat="server" ForeColor="Red" ID="lblOntaskUpgradeMessage">
                    On pressing Save you will be charged [Amount] and your [MonthOrYear] subscription updated accordingly.
                </asp:Label>
                <br />
                See <asp:HyperLink runat="server" NavigateUrl="#">Terms of Service</asp:HyperLink>  for details.
                <br />
                <br />
            </div>
        </asp:Panel>
    </asp:Panel>
   
</asp:Content>

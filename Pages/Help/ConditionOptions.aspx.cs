﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data;
using System.Linq;
using System.Text;


public partial class Pages_Help_ConditionOptions : System.Web.UI.Page
{
    //private int _iReportItemID = -1;
    //private int _iTableID = -1;
    //private int _iColumnID = -1;
    //private string _sConditionType = "";

    int _iTableID = -1;
    int _iColumnID = -1;
    int _iDocumentSectionID = -1;
    int _iSpecialNotificationID = -1;
    int _iTableTabID = -1;
    string _strContext = "field";
    string _strMode = "";
    string containsConditions = "";
    string strBackURL = "";

    private static Pages_UserControl_ConditionsCondition2 _lastOrControl = null;


    protected void Page_Load(object sender, EventArgs e)
    {
        Content theContent = SystemData.Content_Details_ByKey("AdvancedConditionHelp", null);
        if (theContent != null)
        {
            HelpText.Text = theContent.ContentP;
        }

      


        if (Request.QueryString["TableID"] != null)
            _iTableID = int.Parse(Request.QueryString["TableID"].ToString());

        if (Request.QueryString["ColumnID"] != null)
            _iColumnID = int.Parse(Request.QueryString["ColumnID"].ToString());

        if (Request.QueryString["DocumentSectionID"] != null)
            _iDocumentSectionID = int.Parse(Request.QueryString["DocumentSectionID"].ToString());

        if (Request.QueryString["TableTabID"] != null)
            _iTableTabID = int.Parse(Request.QueryString["TableTabID"].ToString());

        if (Request.QueryString["SpecialNotificationID"] != null)
            _iSpecialNotificationID = int.Parse(Request.QueryString["SpecialNotificationID"].ToString());

        if (Request.QueryString["mode"] != null)
            _strMode = Cryptography.Decrypt(Request.QueryString["mode"].ToString());

        if (Request.QueryString["Context"] != null)
            _strContext = Request.QueryString["Context"].ToString().ToLower();




        //if (Request.QueryString["hlBack"] != null)
        //    strBackURL = Cryptography.Decrypt(Request.QueryString["hlBack"].ToString());


        //string showWhenFancy = @"
           
        //     $(function () {
        //        $("".imageSection"").fancybox({
        //         'transitionIn': 'elastic',
        //         'transitionOut': 'none',
        //         scrolling: 'auto',
        //         type: 'iframe',
        //         iframe: {
        //            css: {
        //                width: 1300,
        //                height: 800
        //            }
        //         },
        //         toolbar: false,
        //         smallBtn: true

        //    });
        //    });

        //    ";

        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "showImageSectionFancy", showWhenFancy, true);

        if (!IsPostBack)
        {
            //if (strBackURL != "")
            //{
            //    hlBack.Visible = true;
            //    hlBack.NavigateUrl = strBackURL;
            //    hlBack.CssClass = "imageSection";
            //}
              
            PopulateCondition();

        }



    }

    public string ParentCheckboxName()
    {
        string sName = "";
        if (containsConditions == "Yes")
        {
            sName = "hfCondition";
            if (_strContext == "readonly")
            {
                sName = "hfCondition";
            }
        }

        return sName;
    }

    protected void SetAdvancedConditionRowData()
    {
        //if (ViewState["dtAdvancedConditions"] != null)
        //{
        //    DataTable dtAdvancedConditions = (DataTable)ViewState["dtAdvancedConditions"];
        //    dtAdvancedConditions.Rows.Clear();

        //    int iDisplayOrder = 0;
        //    foreach (RepeaterItem item in repConditions.Items)
        //    {
        //        Pages_UserControl_ConditionsCondition2 swcCondition = item.FindControl("swcCondition") as Pages_UserControl_ConditionsCondition2;
        //        if (swcCondition != null)
        //        {
        //            dtAdvancedConditions.Rows.Add(swcCondition.ControlId, 0,
        //                swcCondition.ddlHideColumnV, swcCondition.hfConditionValueV,
        //                swcCondition.ddlOperatorV, iDisplayOrder, swcCondition.ddlJoinOperatorV);
        //            iDisplayOrder = iDisplayOrder + 1;
        //        }
        //    }

        //    ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
        //}
    }

    protected void PopulateCondition()
    {
        string descriptionTemplate = "";
        switch (_strContext)
        {
            case "readonly":
                lblTitle.Text = "Read Only When";
                descriptionTemplate = "Conditions setup to enable or disable a field:";
                break;
            case "specialnotification":
                lblTitle.Text = "Send When";
                descriptionTemplate = "Conditions setup to send a special notifications:";
                break;
            default:
                lblTitle.Text = "Show When";
                descriptionTemplate = "Conditions setup to hide or show a field:";
                break;
        }

        Column column = RecordManager.ets_Column_Details(_iColumnID);
        if (column != null)
        {
            lblDescription.Text = String.Format(descriptionTemplate, column.DisplayName);
        }
        else
        {
            lblDescription.Text = descriptionTemplate;
        }

        //DataTable dtConditions = new DataTable();
        //dtConditions.Columns.Add("ID");
        //dtConditions.Columns.Add("ConditionsID");
        //dtConditions.Columns.Add("ColumnID");
        //dtConditions.Columns.Add("ConditionColumnID");
        //dtConditions.Columns.Add("ConditionValue");
        //dtConditions.Columns.Add("ConditionOperator");
        //dtConditions.Columns.Add("DisplayOrder");
        //dtConditions.Columns.Add("JoinOperator");
        //dtConditions.Columns.Add("DocumentSectionID");
        //dtConditions.Columns.Add("Context");
        //dtConditions.Columns.Add("TableTabID");
        //dtConditions.Columns.Add("SpecialNotificationID");
        //dtConditions.AcceptChanges();

        if ((_iColumnID != -1 || _iDocumentSectionID != -1 || _iTableTabID != -1 || (_iSpecialNotificationID != -1 && _strMode == "old")) && ViewState["dtConditions"] == null)
        {


            DataTable dtConditionsDB = RecordManager.dbg_Conditions_ForGrid((_strContext == "field" || _strContext == "readonly") ? (int?)_iColumnID : null,
                                                                    _strContext == "dashboard" ? (int?)_iDocumentSectionID : null,
                                                                    _strContext == "tabletab" ? (int?)_iTableTabID : null,
                                                                     _strContext == "specialnotification" ? (int?)_iSpecialNotificationID : null,
                                                                     _strContext);

            if (dtConditionsDB.Rows.Count > 0)
            {
                containsConditions = "Yes";
            }
            else
            {
                containsConditions = "No";
            }



                repConditions.DataSource = dtConditionsDB;
                repConditions.DataBind();

        }



    }


    //protected void lnkSave_Click(object sender, EventArgs e)
    //{

    //    //SetAdvancedConditionRowData();

    //    //if (ViewState["dtAdvancedConditions"] != null)
    //    //{
    //    //    DataTable dtOldAdvancedConditions = UploadWorld.ets_AdvancedCondition_Select(_iColumnID, "notification", _sConditionType);

    //    //    DataTable dtAdvancedConditions = (DataTable) ViewState["dtAdvancedConditions"];

    //    //    int iOldRowsCount = dtOldAdvancedConditions.Rows.Count;
    //    //    string strActiveAdvancedConditionIDs = "-1";
    //    //    int iDO = 1;

    //    //    foreach (DataRow drSW in dtAdvancedConditions.Rows)
    //    //    {
    //    //        if (!String.IsNullOrEmpty(drSW["ColumnID"].ToString()) &&
    //    //            ((iDO == 1) || !String.IsNullOrEmpty(drSW["JoinOperator"].ToString())))
    //    //        {
    //    //            bool bInsert = iOldRowsCount < iDO;

    //    //            AdvancedCondition theAdvancedCondition = new AdvancedCondition()
    //    //            {
    //    //                AdvancedConditionID = bInsert
    //    //                    ? -1
    //    //                    : int.Parse(dtOldAdvancedConditions.Rows[iDO - 1]["AdvancedConditionID"].ToString()),
    //    //                ConditionType = "notification",
    //    //                ConditionSubType = _sConditionType,
    //    //                ColumnID = _iColumnID,
    //    //                ConditionColumnID = int.Parse(drSW["ColumnID"].ToString()),
    //    //                ConditionColumnValue = drSW["ColumnValue"].ToString(),
    //    //                ConditionOperator = drSW["Operator"].ToString(),
    //    //                DisplayOrder = iDO,
    //    //                JoinOperator = (iDO == 1) ? "" : drSW["JoinOperator"].ToString()
    //    //            };
    //    //            if (bInsert)
    //    //            {
    //    //                theAdvancedCondition.AdvancedConditionID =
    //    //                    UploadWorld.ets_AdvancedCondition_Insert(theAdvancedCondition);
    //    //            }
    //    //            else
    //    //            {
    //    //                UploadWorld.ets_AdvancedCondition_Update(theAdvancedCondition);
    //    //            }

    //    //            strActiveAdvancedConditionIDs = strActiveAdvancedConditionIDs + "," +
    //    //                                            theAdvancedCondition.AdvancedConditionID.ToString();
    //    //            iDO = iDO + 1;
    //    //        }
    //    //    }

    //    //    Common.ExecuteText("DELETE FROM AdvancedCondition WHERE ColumnID = " + _iColumnID.ToString() +
    //    //                       " AND AdvancedConditionID NOT IN (" + strActiveAdvancedConditionIDs + ")" +
    //    //                       " AND ConditionType LIKE 'notification'" +
    //    //                       " AND ConditionSubType = '" + _sConditionType + "'");
    //    //}

    //    //if (ViewState["dtAdvancedConditions"] != null)
    //    //{
    //    //    Session["dtAdvancedConditions"] = (DataTable) ViewState["dtAdvancedConditions"];
    //    //}

    //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Save Action", String.Format("GetBackValueEdit('{0}');", _sConditionType), true);
    //}

    protected void repConditions_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        Session["ConditionContext"] = _strContext;
      

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Pages_UserControl_ConditionsCondition2 swcCondition = e.Item.FindControl("swcCondition") as Pages_UserControl_ConditionsCondition2;
            ImageButton btnRemove = swcCondition.FindControl("btnRemove") as ImageButton;
            Label lblValue = e.Item.FindControl("lblValue") as Label;
            Label lblField = e.Item.FindControl("lblField") as Label;
            Label lblField2 = e.Item.FindControl("lblField2") as Label;
            Label lblOperator = e.Item.FindControl("lblOperator") as Label;

            /* == Red 13092019: change text when adding so to can read well == */
            switch (lblOperator.Text)
            {
                case "equals":
                    lblOperator.Text = "Equals";
                    break;
                case "notequal":
                    lblOperator.Text = "Not Equals";
                    break;
                case "greaterthan":
                    lblOperator.Text = this.GetConditionsColumnType((Int32)DataBinder.Eval(e.Item.DataItem, "ConditionsID")) == true ? "Greater Than (Date)" : "Greater Than";
                    break;
                case "greaterthandays":
                    lblOperator.Text = "Greater Than (Days)";
                    break;
                case "greaterthanequal":
                    lblOperator.Text = "Greater or Equal to";
                    break;
                case "lessthan":
                    lblOperator.Text = this.GetConditionsColumnType((Int32)DataBinder.Eval(e.Item.DataItem, "ConditionsID")) == true ? "Less Than (Date)" : "Less Than";
                    break;
                case "lessthandays":
                    lblOperator.Text = "Less Than (Days)";
                    break;
                case "lessthanequal":
                    lblOperator.Text = "Less or Equal to";
                    break;
                case "contains":
                    lblOperator.Text = "Contains";
                    break;
                case "notcontains":
                    lblOperator.Text = "Does Not Contain";
                    break;
                case "empty":
                    lblOperator.Text = "Is Empty";
                    break;
                case "notempty":
                    lblOperator.Text = "Is Not Empty";
                    break;

            }
            /* == End Red == */




            HtmlGenericControl panelTable = e.Item.FindControl("panelTable") as HtmlGenericControl;

            lblValue.Text = this.GetConditionsValue((Int32)DataBinder.Eval(e.Item.DataItem, "ConditionsID"));
            btnRemove.Visible = false;
            swcCondition.Visible = false;

            string sOperator = DataBinder.Eval(e.Item.DataItem, "JoinOperator").ToString();
            string joinOperator = DataBinder.Eval(e.Item.DataItem, "JoinOperator").ToString();
            if (joinOperator == "or" || e.Item.ItemIndex == 0)
            {
                Panel orSeparator = e.Item.FindControl("orSeparator") as Panel;
                if (orSeparator != null && e.Item.ItemIndex > 0)
                    orSeparator.Visible = true;
                _lastOrControl = swcCondition;
                swcCondition.EnableColumnList = false;
            }
            else
            {
                if (_lastOrControl != null)
                    _lastOrControl.ShowRemoveButton = false;
                swcCondition.EnableColumnList = true;
            }

            if (_strContext == "dashboard")
            {
                lblField.Visible = false;
                lblField2.Visible = true;
                panelTable.Visible = true;
            }
            else
            {
                lblField.Visible = true;
                lblField2.Visible = false;
                panelTable.Visible = false;
            }

            //implement userrole DataBinder.Eval(e.Item.DataItem, "ConditionsID")
            try
            {
                if (DataBinder.Eval(e.Item.DataItem, "ConditionColumnID").ToString() == "-1" && DataBinder.Eval(e.Item.DataItem, "ConditionValue").ToString() != "")
                {
                    Role theRole = SecurityManager.Role_Details(int.Parse(DataBinder.Eval(e.Item.DataItem, "ConditionValue").ToString()));
                    if (theRole != null)
                    {
                        lblValue.Text = theRole.RoleName;
                        lblField.Text = "User Role";
                    }
                }
            }
            catch(Exception ex)
            {
                //
            }
            
        }



        if (e.Item.ItemType == ListItemType.Header)
        {
            HtmlGenericControl headerTable = e.Item.FindControl("headerTable") as HtmlGenericControl;

            if (_strContext == "dashboard")
            {
                headerTable.Visible = true;
            }
            else
            {
                headerTable.Visible = false;
            }
        }

        if (e.Item.ItemType == ListItemType.Footer)
        {
            HtmlGenericControl footerTable = e.Item.FindControl("footerTable") as HtmlGenericControl;

            if (_strContext == "dashboard")
            {
                footerTable.Visible = true;
            }
            else
            {
                footerTable.Visible = false;
            }
        }
    }

    protected void lbNewCondition_OnClick(object sender, EventArgs e)
    {
        //if (Request.QueryString["TableID"] != null)
        //    _iTableID = int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
        //if (Request.QueryString["ColumnID"] != null)
        //    _iColumnID = int.Parse(Request.QueryString["ColumnID"].ToString());

        //AdvancedCondition advancecondition = new AdvancedCondition();
        //advancecondition.ConditionType = "notification";
        //advancecondition.ConditionSubType = _sConditionType;
        //advancecondition.ColumnID = _iColumnID;
        //advancecondition.ConditionColumnID = _iColumnID;
        //advancecondition.ConditionColumnValue = "0";
        //advancecondition.ConditionOperator = "equals";
        //advancecondition.DisplayOrder = repConditions.Items.Count + 1;
        //advancecondition.JoinOperator = "and";
        //Int32 newid = UploadWorld.ets_AdvancedCondition_Insert(advancecondition);
        //PopulateAdvancedCondition();

        string maxDisplayOrder = "";
        if (_strContext == "specialnotification")
        {
            maxDisplayOrder = Common.GetValueFromSQL("Select MAX(DisplayOrder) FROM Conditions WHERE SpecialNotificationID=" + _iSpecialNotificationID.ToString());
        }
        else if (_strContext == "dashboard")
        {
            maxDisplayOrder = Common.GetValueFromSQL("Select MAX(DisplayOrder) FROM Conditions WHERE DocumentSectionID =" + _iDocumentSectionID.ToString());
        }
        else if (_strContext == "tabletab")
        {
            maxDisplayOrder = Common.GetValueFromSQL("Select MAX(DisplayOrder) FROM Conditions WHERE TableTabID =" + _iTableTabID.ToString());
        }
        else
        {
            maxDisplayOrder = Common.GetValueFromSQL("Select MAX(DisplayOrder) FROM Conditions WHERE ColumnID =" + _iColumnID.ToString());
        }

        int maxorder = 0;

        if (!string.IsNullOrEmpty(maxDisplayOrder))
        {
            maxorder = int.Parse(maxDisplayOrder);
        }

        ViewState["mode"] = "add";
        Conditions theConditions1 = new Conditions();
        
        theConditions1.ColumnID = (_strContext == "field" || _strContext == "readonly") ? (int?)_iColumnID : null;
        theConditions1.DocumentSectionID = _strContext == "dashboard" ? (int?)_iDocumentSectionID : null;
        theConditions1.TableTabID = _strContext == "tabletab" ? (int?)_iTableTabID : null;
        theConditions1.Context = _strContext;

        theConditions1.ConditionColumnID = _iColumnID == -1 ? null : (int?)_iColumnID;
        theConditions1.ConditionValue = "0";
        theConditions1.ConditionOperator = "equals";
        theConditions1.DisplayOrder = maxorder + 1;
        theConditions1.JoinOperator = "and";
        theConditions1.SpecialNotificationID = _strContext == "specialnotification" ? (int?)_iSpecialNotificationID : null;

        int newid = RecordManager.dbg_Conditions_Insert(theConditions1);

        PopulateCondition();
        int item = repConditions.Items.Count == 0 ? 1 : repConditions.Items.Count;

        RepeaterCommandEventArgs args = new RepeaterCommandEventArgs(repConditions.Items[repConditions.Items.Count - 1], null, new CommandEventArgs("Edit", newid));
        repConditions_ItemCommand(repConditions, args);




    }

    protected void swcCondition_OnItemRemoved(object sender, Pages_UserControl_ItemRemovedEventArgs e)
    {
        //SetAdvancedConditionRowData();

        //if (ViewState["dtAdvancedConditions"] != null)
        //{
        //    DataTable dtAdvancedConditions = (DataTable) ViewState["dtAdvancedConditions"];
        //    for (int i = dtAdvancedConditions.Rows.Count - 1; i >= 0; i--)
        //    {
        //        DataRow dr = dtAdvancedConditions.Rows[i];
        //        if (dr["ID"].ToString() == e.ItemId)
        //        {
        //            dr.Delete();
        //            break;
        //        }
        //    }
        //    ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
        //    PopulateAdvancedCondition();
        //}
    }

    protected void lbHelp_OnClick(object sender, EventArgs e)
    {
    }

    protected void swcCondition_OnJoinChanged(object sender, Pages_UserControl_JoinChangedEventArgs e)
    {
        //if (ViewState["dtAdvancedConditions"] != null)
        //{
        //    DataTable dtAdvancedConditions = (DataTable) ViewState["dtAdvancedConditions"];
        //    for (int i = dtAdvancedConditions.Rows.Count - 1; i >= 0; i--)
        //    {
        //        DataRow dr = dtAdvancedConditions.Rows[i];
        //        if (dr["ID"].ToString() == e.ItemId)
        //        {
        //            dr["JoinOperator"] = e.JoinOperator;
        //            dr["ColumnID"] = _iColumnID.ToString();
        //            break;
        //        }
        //    }
        //    ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
        //}
        //PopulateAdvancedCondition();
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {

    }

    protected void repConditions_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

       

        if (e.CommandArgument != null)
        {
            switch (e.CommandName)
            {
                case "Delete":
                    ViewState["mode"] = null;
                    DeleteItem(e);
                    break;
                case "Edit":
                    EditItem(e);
                    break;
                case "Update":
                    ViewState["mode"] = null;
                    SaveItem(e);
                    break;
                case "Cancel":                  
                    CancelItem(e);
                    break;
            }
        }
    }

    protected void SaveItem(RepeaterCommandEventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlGenericControl panelLabels = e.Item.FindControl("panelLabels") as System.Web.UI.HtmlControls.HtmlGenericControl;
        Pages_UserControl_ConditionsCondition2 swcCondition = e.Item.FindControl("swcCondition") as Pages_UserControl_ConditionsCondition2;

        //AdvancedCondition advance = UploadWorld.ets_AdvancedCondition_Detail(Int32.Parse(e.CommandArgument.ToString()));
        //if (advance != null)
        //{
        //    advance.ConditionColumnID = Int32.Parse(swcCondition.ddlHideColumnV);
        //    advance.ConditionColumnValue = swcCondition.hfConditionValueV;
        //    advance.ConditionOperator = swcCondition.ddlOperatorV;
        //    advance.JoinOperator = swcCondition.ddlJoinOperatorV;
        //    Int32 newid = UploadWorld.ets_AdvancedCondition_Update(advance);
        //    PopulateCondition();
        //}

        Conditions conditions = RecordManager.dbg_Conditions_Detail(int.Parse(e.CommandArgument.ToString()));
        if (conditions != null)
        {
            conditions.ConditionColumnID = Int32.Parse(swcCondition.ddlHideColumnV);
            conditions.ConditionValue = swcCondition.hfConditionValueV;
            conditions.ConditionOperator = swcCondition.ddlOperatorV;
            conditions.JoinOperator = swcCondition.ddlJoinOperatorV;
            Int32 newid = RecordManager.dbg_Conditions_Update(conditions);
            PopulateCondition();

        }
    
    }

    protected void EditItem(RepeaterCommandEventArgs e)
    {
        LinkButton lnkEdit = e.Item.FindControl("lnkEdit") as LinkButton;
        LinkButton lnkSave = e.Item.FindControl("lnkSave") as LinkButton;
        LinkButton lnkCancel = e.Item.FindControl("lnkCancel") as LinkButton;
        LinkButton lnkDelete = e.Item.FindControl("lnkDelete") as LinkButton;
        Label lblBrackerSeparatorDelete = e.Item.FindControl("lblBrackerSeparatorDelete") as Label;
        Label lblBrackerSeparatorCancel = e.Item.FindControl("lblBrackerSeparatorCancel") as Label;



        System.Web.UI.HtmlControls.HtmlGenericControl panelLabels = e.Item.FindControl("panelLabels") as System.Web.UI.HtmlControls.HtmlGenericControl;
        Pages_UserControl_ConditionsCondition2 swcCondition = e.Item.FindControl("swcCondition") as Pages_UserControl_ConditionsCondition2;

        Conditions condition = RecordManager.dbg_Conditions_Detail(int.Parse(e.CommandArgument.ToString()));

        string maxDisplayOrder = Common.GetValueFromSQL("Select COUNT(*) FROM Conditions WHERE SpecialNotificationID=" + _iSpecialNotificationID.ToString());
        int countNotif = 0;

        if (!string.IsNullOrEmpty(maxDisplayOrder))
        {
            countNotif = int.Parse(maxDisplayOrder);
        }

        if (swcCondition != null)
        {
            swcCondition.TableID = _iTableID;
            swcCondition.ColumnID = -1;
            swcCondition.ShowTable = true;

            if (e.Item.ItemIndex != 0)
                swcCondition.ddlJoinOperatorV = condition.JoinOperator == null ? "" : condition.JoinOperator;

            swcCondition.ShowJoinOperator = e.Item.ItemIndex != 0;


            string sOperator = condition.ConditionOperator == null ? "" : condition.ConditionOperator;
            string sValue = condition.ConditionValue == null ? "" : condition.ConditionValue;
            bool setColumnValue = true;
            if (condition.JoinOperator == "or" /*|| e.Item.ItemIndex == 0*/)
            {

                Panel orSeparator = e.Item.FindControl("orSeparator") as Panel;
                if (orSeparator != null && e.Item.ItemIndex > 0)
                    orSeparator.Visible = true;
                _lastOrControl = swcCondition;

                swcCondition.EnableColumnList = false;
                if (String.IsNullOrEmpty(sValue) && sOperator != "empty" && sOperator != "notempty")
                {
                    setColumnValue = false;
                }
            }
            else
            {
                //if (_lastOrControl != null)
                //    _lastOrControl.ShowRemoveButton = false;
                swcCondition.EnableColumnList = true;
            }

            //swcCondition.ColumnDefID = DataBinder.Eval(e.Item.DataItem, "ConditionColumnID").ToString();
            swcCondition.ddlHideColumnV = condition.ConditionColumnID.ToString();
            swcCondition.ddlOperatorV = sOperator;
           
            if (setColumnValue) // empty string hides value control
                swcCondition.hfConditionValueV = sValue;
        }

        panelLabels.Visible = false;
        swcCondition.Visible = true;

        lnkSave.Visible = true;
        lblBrackerSeparatorCancel.Visible = true;
        lnkCancel.Visible = true;
        lblBrackerSeparatorDelete.Visible = false;
        lnkDelete.Visible = false;
        lnkEdit.Visible = false;
    }


    protected void DeleteItem(RepeaterCommandEventArgs e)
    {
        String sql = "DELETE FROM Conditions WHERE ConditionsID=" + e.CommandArgument.ToString();
        Common.ExecuteText(sql);
        PopulateCondition();
    }

    protected void lnkCancel_Click(object sender, EventArgs e, RepeaterCommandEventArgs ee)
    {
       
        PopulateCondition();
    }

    protected void CancelItem(RepeaterCommandEventArgs e)
    {

        if (ViewState["mode"] != null)
        {
            DeleteItem(e);
            ViewState["mode"] = null;
        }
        
        PopulateCondition();
    }

    protected string GetConditionsValue(Int32 ConditionsID)
    {
        String value = "--*--";
        Conditions condition = RecordManager.dbg_Conditions_Detail(ConditionsID);

        if (condition != null)
        {
            int? conColId = condition.ConditionColumnID == null ? -1 : condition.ConditionColumnID;
            Column col = RecordManager.ets_Column_Details(Int32.Parse(conColId.ToString()));
            if(col != null)
            {
                if (col.ColumnType != null)
                {
                    if (col.ColumnType.ToString() == "dropdown" && col.DropDownType.ToString() == "tabledd")
                    {
                        value = Common.GetDisplayTextFromColumnAndValue(col, condition.ConditionValue);
                    }
                    else
                    {
                        value = condition.ConditionValue;
                    }
                }
            }
          

        }

        return value;
    }

    protected bool GetConditionsColumnType(Int32 ConditionsID)
    {
        Conditions conditions = RecordManager.dbg_Conditions_Detail(ConditionsID);

        if (conditions != null)
        {
            int? conColId = conditions.ConditionColumnID == null ? -1 : conditions.ConditionColumnID;
            Column col = RecordManager.ets_Column_Details(Int32.Parse(conColId.ToString()));
            if (col.ColumnType == "date" || col.ColumnType == "datetime")
            {
                return true;
            }
        }

        return false;
    }

}
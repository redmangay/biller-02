﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Pages_Help_TableNameConfirm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if(!IsPostBack)
        {


            string strchkUpdate = "";
            string strChangeName = "";

            if (Request.QueryString["message"] != null)
            {
                lblMessage.Text = Cryptography.Decrypt(Request.QueryString["message"].ToString());
                
            }

            if (Request.QueryString["okbutton"] != null)
            {
                chkboxUpdateMenu.Checked = true;
                chkboxUpdateView.Checked = true;

                strChangeName = strChangeName + " var p_hftochkUpdateMenuName =  window.parent.document.getElementById('" + Session["MenuChange"] + @"'); ";
                strChangeName = strChangeName + " var p_hftochkUpdateViewName =  window.parent.document.getElementById('" + Session["ViewChange"] + @"'); ";


                string strOkButton = Cryptography.Decrypt(Request.QueryString["okbutton"].ToString());
                strChangeName = strChangeName + " var p_okButton =  window.parent.document.getElementById('" + strOkButton + @"'); ";

                strchkUpdate = @"
                            function CloseAndRefresh()
                                {                             
                                p_hftochkUpdateMenuName.value = document.getElementById('" + chkboxUpdateMenu.ClientID + @"').checked.toString();
                                p_hftochkUpdateViewName.value = document.getElementById('" + chkboxUpdateView.ClientID + @"').checked.toString();         
                                  $(p_okButton).trigger('click');
                                   parent.$.fancybox.close();
                                }
                                ";
            }
            //Red Ticket 634
            string   strTrigger = @"
                       

                         $(document).ready(function () {
                                                                
                                        
                                    " + strChangeName  + strchkUpdate + @"

                                       $('#" + lnkOK.ClientID + @"').click(function () {

                                                    CloseAndRefresh();return false;
                                    });
                                });

                ";


             
                ScriptManager.RegisterStartupScript(this, this.GetType(), "strTrigger", strTrigger, true);
                         

            if (Request.QueryString["nobutton"] != null)
            {
                string strNoButton = Cryptography.Decrypt(Request.QueryString["nobutton"].ToString());

                string strCloseAndRefreshNo = @"function CloseAndRefreshNo() {                                                        
                               window.parent.document.getElementById('" + strNoButton + @"').click();
                                parent.$.fancybox.close();
                                            
                                      }";
                
                lnkNo.OnClientClick = "CloseAndRefreshNo();return false;";
                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "strCloseAndRefreshNo", strCloseAndRefreshNo, true);

                
            }
            
            else
            {
                lnkNo.OnClientClick = "parent.$.fancybox.close();return false;";
            }

        }

    }
    
    
}
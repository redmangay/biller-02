﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true"
    CodeFile="AdvancedCondition.aspx.cs" Inherits="Pages_Help_AdvancedCondition" %>

<%@ Register Src="~/Pages/UserControl/ConditionsCondition2.ascx" TagName="SWCon2" TagPrefix="dbg" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <link href="../../Select2/css/select2.min.css" rel="stylesheet" />
    <script src="../../Select2/js/select2.full.min.js"></script>

    <style type="text/css">
        .condition-header {
            display: block;
            float: left;
            background-color: #EDF3F7;
            margin-left: 2px;
            padding: 5px 5px;
            font-size: 1.1em;
            font-weight: bold;
        }

        .condition-row {
            display: block;
            float: left;
            margin-left: 2px;
            padding: 5px 5px;
            font-size: 1.1em;
        }

            .condition-row select {
                width: 100%;
            }

            .condition-row input[type="text"] {
                width: 87%;
                margin-right: 5px;
            }

        .condition-footer {
            display: block;
            float: left;
            margin-left: 2px;
            margin-top: 10px;
            padding: 5px 5px;
            font-size: 1.1em;
        }

        .condition-when-column {
            clear: left;
            width: 65px; /*5em;*/
        }

        .condition-field-column {
            width: 225px; /*17em;*/
        }

        .condition-operator-column {
            width: 135px; /*10em;*/
        }

        .condition-value-column {
            width: 265px; /*20em;*/
        }

            .condition-value-column input[type="text"] {
                height: 24px;
                border-radius: 4px;
                border: 1px solid #aaa;
            }

        .condition-remove-column {
            text-align: center;
            /*width: 65px; */ /*5em;*/
        }

        .select2-container-class {
            font-size: 0.9em;
        }

        .select2-dropdown-class {
            font-size: 0.8em;
            line-height: 0.9;
            text-wrap: none;
        }

        .dialog-textbox {
            border-radius: 4px;
        }


        .separator-line {
            overflow: hidden;
            clear: left;
            float: left;
            margin-left: 4px;
            margin-top: 4px;
            width: 67em;
            background: silver;
            height: 1px;
            z-index: 10;
        }
    </style>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            initElements();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initElements();
        });

        function initElements() {
            $("select").each(function () {
                var $el = $(this);
                if ($el.children("select option[data-category]")) {
                    var groups = {};
                    $el.children("select option[data-category]").each(function () {
                        groups[$.trim($(this).attr("data-category"))] = true;
                    });
                    $.each(groups, function (c) {
                        $el.children("select option[data-category='" + c + "']").wrapAll('<optgroup label="' + c + '">');
                    });
                }

            });
            var options = {
                placeholder: {
                    id: "", // the value of the option
                    text: '-- Please Select --'
                },
                allowClear: false, // cross sign at the just before down arrow
                minimumResultsForSearch: 12,
                dropdownAutoWidth: true,
                containerCssClass: "select2-container-class",
                dropdownCssClass: "select2-dropdown-class"
            };
            $("select").select2(options);

            $("#lbHelp").fancybox({
                onStart: function () {
                    $("#divHelp").css("display", "block")
                        .css("width", "600px").css("height", "400px");
                },
                afterClose: function () { $("#divHelp").css("display", "none"); }
            });
        }

        function GetBackValueEdit(conditionType) {
            var checkBox = "";
            switch (conditionType) {
                case 'V':
                    checkBox = "chkValidConditions";
                    break;
                case 'W':
                    checkBox = "chkWarningConditions";
                    break;
                case 'E':
                    checkBox = "chkExceedanceConditions";
                    break;
            }
            if (checkBox.length !== 0)
                window.parent.document.getElementById(checkBox).checked = true;
            window.parent.document.getElementById("hfRevalidationRequired").value = "yes";
            parent.$.fancybox.close();
        }

        function GetBackValueAdd() {
            parent.$.fancybox.close();
        }
    </script>

    <div style="text-align: center;">
        <div style="display: inline-block;">
            <div style="padding-top: 10px;">
                <asp:UpdatePanel ID="upFilter" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblTitle" Font-Bold="True" Font-Size="16px" Style="white-space: nowrap;">Condition</asp:Label>
                                </td>
                                <td style="width: 25%;">
                                    <div runat="server" id="divSave" style="float: right; width: 36px;">
                                        <asp:LinkButton runat="server" ID="lnkSaveNew" CausesValidation="true" OnClick="lnkSave_Click">
                                            <asp:Image runat="server" ID="ImageSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                ToolTip="Save" />
                                        </asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label runat="server" ID="lblDescription" Style="white-space: nowrap;">Conditions</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="padding-left: 0px; padding-top: 10px;">
                                        <asp:Repeater ID="repConditions" runat="server" OnItemDataBound="repConditions_OnItemDataBound">
                                            <HeaderTemplate>
                                                <span class="condition-when-column condition-header">When</span>
                                                <span class="condition-field-column condition-header">Field</span>
                                                <span class="condition-operator-column condition-header">Operator</span>
                                                <span class="condition-value-column condition-header">Value</span>
                                                <span class="condition-remove-column condition-header">Remove</span>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Panel runat="server" ID="orSeparator" CssClass="separator-line" Visible="False">
                                                </asp:Panel>
                                                <dbg:SWCon2 runat="server" ID="swcCondition"
                                                    OnItemRemoved="swcCondition_OnItemRemoved" OnJoinChanged="swcCondition_OnJoinChanged"></dbg:SWCon2>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <span class="condition-when-column condition-footer"></span>
                                                <span class="condition-field-column condition-footer">
                                                    <asp:LinkButton runat="server" ID="lbNewCondition" Style="text-decoration: none; color: Black;"
                                                        OnClick="lbNewCondition_OnClick">
                                                        <asp:Image Style="vertical-align: middle;" runat="server" ID="imgAddNewRecord" ImageUrl="~/App_Themes/Default/images/Add32.png" />
                                                        <span style="text-decoration: underline; color: Blue; font-weight: bold;">Add condition</span>
                                                    </asp:LinkButton>
                                                </span>
                                                <span class="condition-operator-column condition-footer"></span>
                                                <span class="condition-value-column condition-footer"></span>
                                                <span class="condition-remove-column condition-footer">
                                                    <a id="lbHelp" href="#divHelp">
                                                        <asp:Image Style="vertical-align: middle;" runat="server" ID="Image1" ImageUrl="~/App_Themes/Default/images/help.png" />
                                                    </a>
                                                </span>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <div>
                                            <asp:Label ID="lblMsgTab" runat="server" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                    </Triggers>
                </asp:UpdatePanel>
                <div id="divHelp" style="display: none">
                    <asp:Label runat="server" ID="HelpText"></asp:Label>
                </div>
            </div>
        </div>
    </div>   

</asp:Content>

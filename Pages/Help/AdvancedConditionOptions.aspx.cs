﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class Pages_Help_AdvancedConditionOptions : System.Web.UI.Page
{
    private int _iReportItemID = -1;
    private int _iTableID = -1;
    private int _iColumnID = -1;
    private string _sConditionType = "";

    private static Pages_UserControl_ConditionsCondition2 _lastOrControl = null;


    protected void Page_Load(object sender, EventArgs e)
    {
        Content theContent = SystemData.Content_Details_ByKey("AdvancedConditionHelp", null);
        if (theContent != null)
        {
            HelpText.Text = theContent.ContentP;
        }

        if (Request.QueryString["TableID"] != null)
            _iTableID = int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
        if (Request.QueryString["ColumnID"] != null)
            _iColumnID = int.Parse(Request.QueryString["ColumnID"].ToString());
        _sConditionType = Request.QueryString["ConditionType"];

        if (!IsPostBack)
        {
            PopulateAdvancedCondition();
        }
    }

    protected void SetAdvancedConditionRowData()
    {
        //if (ViewState["dtAdvancedConditions"] != null)
        //{
        //    DataTable dtAdvancedConditions = (DataTable)ViewState["dtAdvancedConditions"];
        //    dtAdvancedConditions.Rows.Clear();

        //    int iDisplayOrder = 0;
        //    foreach (RepeaterItem item in repConditions.Items)
        //    {
        //        Pages_UserControl_ConditionsCondition2 swcCondition = item.FindControl("swcCondition") as Pages_UserControl_ConditionsCondition2;
        //        if (swcCondition != null)
        //        {
        //            dtAdvancedConditions.Rows.Add(swcCondition.ControlId, 0,
        //                swcCondition.ddlHideColumnV, swcCondition.hfConditionValueV,
        //                swcCondition.ddlOperatorV, iDisplayOrder, swcCondition.ddlJoinOperatorV);
        //            iDisplayOrder = iDisplayOrder + 1;
        //        }
        //    }

        //    ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
        //}
    }

    protected void PopulateAdvancedCondition()
    {
        //RP Added Ticket 5040
        //String sql = "DELETE FROM AdvancedCondition WHERE [Status]='A' AND ColumnID=" + _iColumnID.ToString();
        //Common.ExecuteText(sql);
        //End Modification

        string descriptionTemplate = "";
        switch (_sConditionType)
        {
            case "V":
                lblTitle.Text = "Advanced Validation Conditions";
                descriptionTemplate = "Consider data as invalid if any of below conditions are met:";
                break;
            case "W":
                lblTitle.Text = "Advanced Warning Conditions";
                descriptionTemplate = "Issue a Data Warning if any of below conditions are met:";
                break;
            case "E":
                lblTitle.Text = "Advanced Exceedance Conditions";
                descriptionTemplate = "Issue a Data Exceedance if any of below conditions are met:";
                break;
        }

        Column column = RecordManager.ets_Column_Details(_iColumnID);
        if (column != null)
        {
            lblDescription.Text = String.Format(descriptionTemplate, column.DisplayName);
        }

        //DataTable dtAdvancedConditions = new DataTable();
        //dtAdvancedConditions.Columns.Add("ID"); // 0
        //dtAdvancedConditions.Columns.Add("AdvancedConditionID"); // 1
        //dtAdvancedConditions.Columns.Add("ColumnID"); // 2
        //dtAdvancedConditions.Columns.Add("ColumnValue"); // 3
        //dtAdvancedConditions.Columns.Add("Operator"); // 4
        //dtAdvancedConditions.Columns.Add("DisplayOrder"); // 5
        //dtAdvancedConditions.Columns.Add("JoinOperator"); // 6
        //dtAdvancedConditions.AcceptChanges();

        //if (ViewState["dtAdvancedConditions"] == null)
        //{
            DataTable dtAdvancedConditionDB = UploadWorld.ets_AdvancedCondition_Select(_iColumnID, "notification", _sConditionType);
        dtAdvancedConditionDB.DefaultView.Sort = "DisplayOrder DESC";
        dtAdvancedConditionDB.DefaultView.ToTable(true);

        //    if (dtAdvancedConditions != null && dtAdvancedConditionDB != null)
        //    {
        //        //foreach (DataRow dr in dtAdvancedConditionDB.Rows)
        //        //{
        //        //    DataRow newRow = dtAdvancedConditions.NewRow();
        //        //    newRow["ID"] = Guid.NewGuid().ToString();
        //        //    newRow["AdvancedConditionID"] = dr["AdvancedConditionID"].ToString();
        //        //    newRow["ColumnID"] = dr["ConditionColumnID"].ToString();
        //        //    newRow["ColumnValue"] = dr["ConditionColumnValue"].ToString();
        //        //    newRow["Operator"] = dr["ConditionOperator"].ToString();
        //        //    newRow["DisplayOrder"] = dr["DisplayOrder"].ToString();
        //        //    newRow["JoinOperator"] = dr["JoinOperator"].ToString();
        //        //    dtAdvancedConditions.Rows.Add(newRow);
        //        //}

        repConditions.DataSource = dtAdvancedConditionDB;
        repConditions.DataBind();

        //RP Added Ticket 5040 - Hide Delete Button of First condition
        if (repConditions.Items.Count > 1)
        {
            LinkButton lnkDelete = repConditions.Items[repConditions.Items.Count - 1].FindControl("lnkDelete") as LinkButton;
            lnkDelete.ForeColor = System.Drawing.Color.Gray;
            lnkDelete.OnClientClick = "";
            lnkDelete.Enabled = false;
        }
        //End Modification

        //ViewState["dtAdvancedConditions"] = dtAdvancedConditionDB;
        //    }
        //}
        //else
        //{
        //    if (ViewState["dtAdvancedConditions"] == null)
        //    {
        //        if (Session["dtAdvancedConditions"] == null)
        //        {
        //            dtAdvancedConditions.Rows.Add(Guid.NewGuid().ToString(), "-1", "", "", "equals", "1", "");
        //            repConditions.DataSource = dtAdvancedConditions;
        //            repConditions.DataBind();

        //            ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
        //        }
        //        else
        //        {
        //            dtAdvancedConditions = (DataTable) Session["dtAdvancedConditions"];
        //            repConditions.DataSource = dtAdvancedConditions;
        //            repConditions.DataBind();

        //            ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
        //        }
        //    }
        //    else
        //    {
        //        dtAdvancedConditions = (DataTable) ViewState["dtAdvancedConditions"];
        //        repConditions.DataSource = dtAdvancedConditions;
        //        repConditions.DataBind();
        //    }
        //}
    }


    //protected void lnkSave_Click(object sender, EventArgs e)
    //{

    //    //SetAdvancedConditionRowData();

    //    //if (ViewState["dtAdvancedConditions"] != null)
    //    //{
    //    //    DataTable dtOldAdvancedConditions = UploadWorld.ets_AdvancedCondition_Select(_iColumnID, "notification", _sConditionType);

    //    //    DataTable dtAdvancedConditions = (DataTable) ViewState["dtAdvancedConditions"];

    //    //    int iOldRowsCount = dtOldAdvancedConditions.Rows.Count;
    //    //    string strActiveAdvancedConditionIDs = "-1";
    //    //    int iDO = 1;

    //    //    foreach (DataRow drSW in dtAdvancedConditions.Rows)
    //    //    {
    //    //        if (!String.IsNullOrEmpty(drSW["ColumnID"].ToString()) &&
    //    //            ((iDO == 1) || !String.IsNullOrEmpty(drSW["JoinOperator"].ToString())))
    //    //        {
    //    //            bool bInsert = iOldRowsCount < iDO;

    //    //            AdvancedCondition theAdvancedCondition = new AdvancedCondition()
    //    //            {
    //    //                AdvancedConditionID = bInsert
    //    //                    ? -1
    //    //                    : int.Parse(dtOldAdvancedConditions.Rows[iDO - 1]["AdvancedConditionID"].ToString()),
    //    //                ConditionType = "notification",
    //    //                ConditionSubType = _sConditionType,
    //    //                ColumnID = _iColumnID,
    //    //                ConditionColumnID = int.Parse(drSW["ColumnID"].ToString()),
    //    //                ConditionColumnValue = drSW["ColumnValue"].ToString(),
    //    //                ConditionOperator = drSW["Operator"].ToString(),
    //    //                DisplayOrder = iDO,
    //    //                JoinOperator = (iDO == 1) ? "" : drSW["JoinOperator"].ToString()
    //    //            };
    //    //            if (bInsert)
    //    //            {
    //    //                theAdvancedCondition.AdvancedConditionID =
    //    //                    UploadWorld.ets_AdvancedCondition_Insert(theAdvancedCondition);
    //    //            }
    //    //            else
    //    //            {
    //    //                UploadWorld.ets_AdvancedCondition_Update(theAdvancedCondition);
    //    //            }

    //    //            strActiveAdvancedConditionIDs = strActiveAdvancedConditionIDs + "," +
    //    //                                            theAdvancedCondition.AdvancedConditionID.ToString();
    //    //            iDO = iDO + 1;
    //    //        }
    //    //    }

    //    //    Common.ExecuteText("DELETE FROM AdvancedCondition WHERE ColumnID = " + _iColumnID.ToString() +
    //    //                       " AND AdvancedConditionID NOT IN (" + strActiveAdvancedConditionIDs + ")" +
    //    //                       " AND ConditionType LIKE 'notification'" +
    //    //                       " AND ConditionSubType = '" + _sConditionType + "'");
    //    //}

    //    //if (ViewState["dtAdvancedConditions"] != null)
    //    //{
    //    //    Session["dtAdvancedConditions"] = (DataTable) ViewState["dtAdvancedConditions"];
    //    //}

    //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Save Action", String.Format("GetBackValueEdit('{0}');", _sConditionType), true);
    //}

    protected void repConditions_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Pages_UserControl_ConditionsCondition2 swcCondition = e.Item.FindControl("swcCondition") as Pages_UserControl_ConditionsCondition2;
            ImageButton btnRemove = swcCondition.FindControl("btnRemove") as ImageButton;
            Label lblValue = e.Item.FindControl("lblValue") as Label;
            //RP Added Ticket 5040 - For User Role
            Label lblField = e.Item.FindControl("lblField") as Label;
            Int32 conditioncolumnid = (Int32)DataBinder.Eval(e.Item.DataItem, "ConditionColumnID");
            if (conditioncolumnid == -1)
            {
                Int32 roleid = 0;
                Int32.TryParse(DataBinder.Eval(e.Item.DataItem, "ConditionColumnValue").ToString(), out roleid);
                Role _theRole = SecurityManager.Role_Details(roleid);
                if (_theRole != null)
                {
                    lblField.Text = "User Role";
                    lblValue.Text = _theRole.RoleName;
                }
            }
            else
            {
                lblValue.Text = this.GetAdvancedConditionValue((Int32)DataBinder.Eval(e.Item.DataItem, "AdvancedConditionID"));
            }
            
            //End Modification
            btnRemove.Visible = false;
            swcCondition.Visible = false;
            string sOperator = DataBinder.Eval(e.Item.DataItem, "JoinOperator").ToString();
            string joinOperator = DataBinder.Eval(e.Item.DataItem, "JoinOperator").ToString();
            if (joinOperator == "or" || e.Item.ItemIndex == 0)
            {
                Panel orSeparator = e.Item.FindControl("orSeparator") as Panel;
                if (orSeparator != null && e.Item.ItemIndex > 0)
                    orSeparator.Visible = true;
                _lastOrControl = swcCondition;
                swcCondition.EnableColumnList = true;
            }
            else
            {
                if (_lastOrControl != null)
                    _lastOrControl.ShowRemoveButton = false;
                swcCondition.EnableColumnList = true;
            }

        }

    }

    protected void lbNewCondition_OnClick(object sender, EventArgs e)
    {
        //RP Added Ticket 5040
        String sql = "DELETE FROM AdvancedCondition WHERE [Status]='A' AND ColumnID=" + _iColumnID.ToString();
        Common.ExecuteText(sql);
        //End Modification

        //if (Request.QueryString["TableID"] != null)
        //    _iTableID = int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
        //if (Request.QueryString["ColumnID"] != null)
        //    _iColumnID = int.Parse(Request.QueryString["ColumnID"].ToString());
        string maxDisplayOrder = Common.GetValueFromSQL("Select MAX(DisplayOrder) FROM AdvancedCondition WHERE ColumnID=" + _iColumnID + " AND ConditionType='notification' AND ConditionSubType='" + _sConditionType + "'");
        int maxorder = 0;

        if(!string.IsNullOrEmpty(maxDisplayOrder))
        {
            maxorder = int.Parse(maxDisplayOrder);
        }

        /* Red: changed above and commented below, i am getting error */
        //if (dt.Rows.Count >= 1)
        //{
        //    maxorder = int.Parse(dt.Rows[0][0].ToString());
        //}
        
        AdvancedCondition advancecondition = new AdvancedCondition();
        advancecondition.ConditionType = "notification";
        advancecondition.ConditionSubType = _sConditionType;
        advancecondition.ColumnID = _iColumnID;
        advancecondition.ConditionColumnID = _iColumnID;
        advancecondition.ConditionColumnValue = "0";
        advancecondition.ConditionOperator = "equals";
        advancecondition.DisplayOrder = maxorder + 1;
        //RP Modified Ticket 5040 - Added Add New flagging Status for Deletion
        advancecondition.Status = "A";
        //RP Modified Ticket 3427 - change default value from "and" to blank
        advancecondition.JoinOperator = !string.IsNullOrEmpty(maxDisplayOrder) && maxorder != 0 ? "and" : "";
        Int32 newid = UploadWorld.ets_AdvancedCondition_Insert(advancecondition);
        PopulateAdvancedCondition();

        RepeaterCommandEventArgs args = new RepeaterCommandEventArgs(repConditions.Items[0], null, new CommandEventArgs("Edit", newid));
        repConditions_ItemCommand(repConditions, args);
    }

    protected void swcCondition_OnItemRemoved(object sender, Pages_UserControl_ItemRemovedEventArgs e)
    {
        //SetAdvancedConditionRowData();

        //if (ViewState["dtAdvancedConditions"] != null)
        //{
        //    DataTable dtAdvancedConditions = (DataTable) ViewState["dtAdvancedConditions"];
        //    for (int i = dtAdvancedConditions.Rows.Count - 1; i >= 0; i--)
        //    {
        //        DataRow dr = dtAdvancedConditions.Rows[i];
        //        if (dr["ID"].ToString() == e.ItemId)
        //        {
        //            dr.Delete();
        //            break;
        //        }
        //    }
        //    ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
        //    PopulateAdvancedCondition();
        //}
    }

    protected void lbHelp_OnClick(object sender, EventArgs e)
    {
    }

    protected void swcCondition_OnJoinChanged(object sender, Pages_UserControl_JoinChangedEventArgs e)
    {
        //if (ViewState["dtAdvancedConditions"] != null)
        //{
        //    DataTable dtAdvancedConditions = (DataTable) ViewState["dtAdvancedConditions"];
        //    for (int i = dtAdvancedConditions.Rows.Count - 1; i >= 0; i--)
        //    {
        //        DataRow dr = dtAdvancedConditions.Rows[i];
        //        if (dr["ID"].ToString() == e.ItemId)
        //        {
        //            dr["JoinOperator"] = e.JoinOperator;
        //            dr["ColumnID"] = _iColumnID.ToString();
        //            break;
        //        }
        //    }
        //    ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
        //}
        //PopulateAdvancedCondition();
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {

    }

    protected void repConditions_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        if (e.CommandArgument != null)
        {
            switch (e.CommandName)
            {
                case "Delete":
                    DeleteItem(e);
                    break;
                case "Edit":
                    EditOnlyAnItem();
                    EditItem(e);
                    break;
                case "Update":
                    SaveItem(e);
                    break;
                //RP Added Ticket 5040 - Cancel link clicked
                case "Cancel":
                    CancelItem(e);
                    break;
                //End Modification
            }
        }
    }

    //RP Added Ticket 5040
    protected void EditOnlyAnItem()
    {
        foreach (RepeaterItem e in repConditions.Items)
        {
            LinkButton lnkEdit = e.FindControl("lnkEdit") as LinkButton;
            LinkButton lnkSave = e.FindControl("lnkSave") as LinkButton;
            LinkButton lnkCancel = e.FindControl("lnkCancel") as LinkButton;
            LinkButton lnkDelete = e.FindControl("lnkDelete") as LinkButton;

            Panel panelActionLinks = e.FindControl("panelActionLinks") as Panel;
            Panel panelSaveLinks = e.FindControl("panelSaveLinks") as Panel;
            System.Web.UI.HtmlControls.HtmlGenericControl panelLabels = e.FindControl("panelLabels") as System.Web.UI.HtmlControls.HtmlGenericControl;
            Pages_UserControl_ConditionsCondition2 swcCondition = e.FindControl("swcCondition") as Pages_UserControl_ConditionsCondition2;

            panelLabels.Visible = true;
            swcCondition.Visible = false;

            lnkSave.Visible = false;
            lnkCancel.Visible = false;
            lnkEdit.Visible = true;
            lnkDelete.Visible = true;

            panelActionLinks.Visible = true;
            panelSaveLinks.Visible = false;
        }
    }

    protected void CancelItem(RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemIndex == 0)
        {
            String sql = "DELETE FROM AdvancedCondition WHERE [Status]='A' AND ColumnID=(SELECT ColumnID FROM AdvancedCondition WHERE AdvancedConditionID=" + e.CommandArgument.ToString() + ")";
            Common.ExecuteText(sql);
            PopulateAdvancedCondition();
        }
        else
        {
            LinkButton lnkEdit = e.Item.FindControl("lnkEdit") as LinkButton;
            LinkButton lnkSave = e.Item.FindControl("lnkSave") as LinkButton;
            LinkButton lnkCancel = e.Item.FindControl("lnkCancel") as LinkButton;
            LinkButton lnkDelete = e.Item.FindControl("lnkDelete") as LinkButton;

            Panel panelActionLinks = e.Item.FindControl("panelActionLinks") as Panel;
            Panel panelSaveLinks = e.Item.FindControl("panelSaveLinks") as Panel;
            System.Web.UI.HtmlControls.HtmlGenericControl panelLabels = e.Item.FindControl("panelLabels") as System.Web.UI.HtmlControls.HtmlGenericControl;
            Pages_UserControl_ConditionsCondition2 swcCondition = e.Item.FindControl("swcCondition") as Pages_UserControl_ConditionsCondition2;

            panelLabels.Visible = true;
            swcCondition.Visible = false;

            lnkSave.Visible = false;
            lnkCancel.Visible = false;
            lnkEdit.Visible = true;
            lnkDelete.Visible = true;

            panelActionLinks.Visible = true;
            panelSaveLinks.Visible = false;

        }
    }
    //End Modification

    protected void SaveItem(RepeaterCommandEventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlGenericControl panelLabels = e.Item.FindControl("panelLabels") as System.Web.UI.HtmlControls.HtmlGenericControl;
        Pages_UserControl_ConditionsCondition2 swcCondition = e.Item.FindControl("swcCondition") as Pages_UserControl_ConditionsCondition2;

        AdvancedCondition advance = UploadWorld.ets_AdvancedCondition_Detail(Int32.Parse(e.CommandArgument.ToString()));
        if (advance != null)
        {
            advance.ConditionColumnID = Int32.Parse(swcCondition.ddlHideColumnV);
            advance.ConditionColumnValue = swcCondition.hfConditionValueV;
            advance.ConditionOperator = swcCondition.ddlOperatorV;
            advance.JoinOperator = swcCondition.ddlJoinOperatorV;
            Int32 newid = UploadWorld.ets_AdvancedCondition_Update(advance);
            PopulateAdvancedCondition();
        }
    }

    protected void EditItem(RepeaterCommandEventArgs e)
    {
        LinkButton lnkEdit = e.Item.FindControl("lnkEdit") as LinkButton;
        LinkButton lnkSave = e.Item.FindControl("lnkSave") as LinkButton;
        LinkButton lnkCancel = e.Item.FindControl("lnkCancel") as LinkButton;
        LinkButton lnkDelete = e.Item.FindControl("lnkDelete") as LinkButton;

        Panel panelActionLinks = e.Item.FindControl("panelActionLinks") as Panel;
        Panel panelSaveLinks = e.Item.FindControl("panelSaveLinks") as Panel;

        System.Web.UI.HtmlControls.HtmlGenericControl panelLabels = e.Item.FindControl("panelLabels") as System.Web.UI.HtmlControls.HtmlGenericControl;
        Pages_UserControl_ConditionsCondition2 swcCondition = e.Item.FindControl("swcCondition") as Pages_UserControl_ConditionsCondition2;

        AdvancedCondition advance = UploadWorld.ets_AdvancedCondition_Detail(Int32.Parse(e.CommandArgument.ToString()));
        if (swcCondition != null)
        {
            swcCondition.TableID = _iTableID;
            swcCondition.ColumnID = -1;
            swcCondition.ShowTable = true;

            //RP Modified Ticket 5040
            //if (e.Item.ItemIndex != 0)
            //    swcCondition.ddlJoinOperatorV = advance.JoinOperator;
            ////swcCondition.ShowJoinOperator = e.Item.ItemIndex != 0;
            if (e.Item.ItemIndex == repConditions.Items.Count - 1)
            {
                swcCondition.ddlJoinOperatorV = advance.JoinOperator;
                swcCondition.ShowJoinOperator = false;
            }
            else
            {
                swcCondition.ddlJoinOperatorV = advance.JoinOperator;
                swcCondition.ShowJoinOperator = true;
            }
            //End Modification

            string sOperator = advance.ConditionOperator;
            string sValue = advance.ConditionColumnValue;
            bool setColumnValue = true;
            if (advance.JoinOperator == "or" /*|| e.Item.ItemIndex == 0*/)
            {
                Panel orSeparator = e.Item.FindControl("orSeparator") as Panel;
                if (orSeparator != null && e.Item.ItemIndex > 0)
                    orSeparator.Visible = true;
                _lastOrControl = swcCondition;

                swcCondition.EnableColumnList = true;
                if (String.IsNullOrEmpty(sValue) && sOperator != "empty" && sOperator != "notempty")
                {
                    setColumnValue = false;
                }
            }
            else
            {
                //if (_lastOrControl != null)
                //    _lastOrControl.ShowRemoveButton = false;
                if (e.Item.ItemIndex == repConditions.Items.Count - 1)
                    swcCondition.EnableColumnList = false;
                else
                    swcCondition.EnableColumnList = true;
            }

            //swcCondition.ColumnDefID = DataBinder.Eval(e.Item.DataItem, "ConditionColumnID").ToString();

            swcCondition.ddlOperatorV = sOperator;
            swcCondition.ddlHideColumnV = advance.ConditionColumnID.ToString();
            if (setColumnValue) // empty string hides value control
                swcCondition.hfConditionValueV = sValue;
        }

        panelLabels.Visible = false;
        swcCondition.Visible = true;

        lnkSave.Visible = true;
        lnkCancel.Visible = true;
        lnkEdit.Visible = false;
        lnkDelete.Visible = false;

        panelActionLinks.Visible = false;
        panelSaveLinks.Visible = true;

    }


    protected void DeleteItem(RepeaterCommandEventArgs e)
    {
        String sql = "DELETE FROM AdvancedCondition WHERE AdvancedConditionID=" + e.CommandArgument.ToString();
        Common.ExecuteText(sql);
        PopulateAdvancedCondition();
    }

    protected void lnkCancel_Click(object sender, EventArgs e)
    {
        //RP Added Ticket 5040 - Add New Condition is not working
        String sql = "DELETE FROM AdvancedCondition WHERE [Status]='A' AND ColumnID=" + _iColumnID.ToString();
        Common.ExecuteText(sql);
        //End Modification
        PopulateAdvancedCondition();
    }

    protected string GetAdvancedConditionValue(Int32 advancedConditionID)
    {
        String value = "";
        AdvancedCondition advancedcondition = UploadWorld.ets_AdvancedCondition_Detail(advancedConditionID);

        if (advancedcondition != null)
        {
            Column col = RecordManager.ets_Column_Details(Int32.Parse(advancedcondition.ConditionColumnID.ToString()));
            if (col != null)
            {
                if (col.ColumnType != null)
                {
                    if (col.ColumnType.ToString() == "dropdown" && col.DropDownType.ToString() == "tabledd")
                    {
                        value = Common.GetDisplayTextFromColumnAndValue(col, advancedcondition.ConditionColumnValue);
                    }
                    else
                    {
                        value = advancedcondition.ConditionColumnValue;
                    }
                }
            }
        }

        return value;
    }

}
﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class Pages_Help_AdvancedCondition : System.Web.UI.Page
{
    private int _iReportItemID = -1;
    private int _iTableID = -1;
    private int _iColumnID = -1;
    private string _sConditionType = "";

    private static Pages_UserControl_ConditionsCondition2 _lastOrControl = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        Content theContent = SystemData.Content_Details_ByKey("AdvancedConditionHelp", null);
        if (theContent != null)
        {
            HelpText.Text = theContent.ContentP;
        }

        if (Request.QueryString["TableID"] != null)
            _iTableID = int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
        if (Request.QueryString["ColumnID"] != null)
            _iColumnID = int.Parse(Request.QueryString["ColumnID"].ToString());
        _sConditionType = Request.QueryString["ConditionType"];

        if (!IsPostBack)
        {
            PopulateAdvancedCondition();
        }
    }

    protected void SetAdvancedConditionRowData()
    {
        if (ViewState["dtAdvancedConditions"] != null)
        {
            DataTable dtAdvancedConditions = (DataTable)ViewState["dtAdvancedConditions"];
            dtAdvancedConditions.Rows.Clear();

            int iDisplayOrder = 0;
            foreach (RepeaterItem item in repConditions.Items)
            {
                Pages_UserControl_ConditionsCondition2 swcCondition = item.FindControl("swcCondition") as Pages_UserControl_ConditionsCondition2;
                if (swcCondition != null)
                {
                    dtAdvancedConditions.Rows.Add(swcCondition.ControlId, 0,
                        swcCondition.ddlHideColumnV, swcCondition.hfConditionValueV,
                        swcCondition.ddlOperatorV, iDisplayOrder, swcCondition.ddlJoinOperatorV);
                    iDisplayOrder = iDisplayOrder + 1;
                }
            }

            ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
        }
    }

    protected void PopulateAdvancedCondition()
    {
        string descriptionTemplate = "";
        switch (_sConditionType)
        {
            case "V":
                lblTitle.Text = "Advanced Validation Conditions";
                descriptionTemplate = "Consider data as invalid if any of below conditions are met:";
                break;
            case "W":
                lblTitle.Text = "Advanced Warning Conditions";
                descriptionTemplate = "Issue a Data Warning if any of below conditions are met:";
                break;
            case "E":
                lblTitle.Text = "Advanced Exceedance Conditions";
                descriptionTemplate = "Issue a Data Exceedance if any of below conditions are met:";
                break;
        }

        Column column = RecordManager.ets_Column_Details(_iColumnID);
        if (column != null)
        {
            lblDescription.Text = String.Format(descriptionTemplate, column.DisplayName);
        }

        DataTable dtAdvancedConditions = new DataTable();
        dtAdvancedConditions.Columns.Add("ID"); // 0
        dtAdvancedConditions.Columns.Add("AdvancedConditionID"); // 1
        dtAdvancedConditions.Columns.Add("ColumnID"); // 2
        dtAdvancedConditions.Columns.Add("ColumnValue"); // 3
        dtAdvancedConditions.Columns.Add("Operator"); // 4
        dtAdvancedConditions.Columns.Add("DisplayOrder"); // 5
        dtAdvancedConditions.Columns.Add("JoinOperator"); // 6
        dtAdvancedConditions.AcceptChanges();

        if (ViewState["dtAdvancedConditions"] == null)
        {
            DataTable dtAdvancedConditionDB = UploadWorld.ets_AdvancedCondition_Select(_iColumnID, "notification", _sConditionType);

            if (dtAdvancedConditions != null && dtAdvancedConditionDB != null)
            {
                foreach (DataRow dr in dtAdvancedConditionDB.Rows)
                {
                    DataRow newRow = dtAdvancedConditions.NewRow();
                    newRow["ID"] = Guid.NewGuid().ToString();
                    newRow["AdvancedConditionID"] = dr["AdvancedConditionID"].ToString();
                    newRow["ColumnID"] = dr["ConditionColumnID"].ToString();
                    newRow["ColumnValue"] = dr["ConditionColumnValue"].ToString();
                    newRow["Operator"] = dr["ConditionOperator"].ToString();
                    newRow["DisplayOrder"] = dr["DisplayOrder"].ToString();
                    newRow["JoinOperator"] = dr["JoinOperator"].ToString();
                    dtAdvancedConditions.Rows.Add(newRow);
                }

                repConditions.DataSource = dtAdvancedConditions;
                repConditions.DataBind();

                ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
            }
        }
        else
        {
            if (ViewState["dtAdvancedConditions"] == null)
            {
                if (Session["dtAdvancedConditions"] == null)
                {
                    dtAdvancedConditions.Rows.Add(Guid.NewGuid().ToString(), "-1", "", "", "equals", "1", "");
                    repConditions.DataSource = dtAdvancedConditions;
                    repConditions.DataBind();

                    ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
                }
                else
                {
                    dtAdvancedConditions = (DataTable) Session["dtAdvancedConditions"];
                    repConditions.DataSource = dtAdvancedConditions;
                    repConditions.DataBind();

                    ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
                }
            }
            else
            {
                dtAdvancedConditions = (DataTable) ViewState["dtAdvancedConditions"];
                repConditions.DataSource = dtAdvancedConditions;
                repConditions.DataBind();
            }
        }
    }


    protected void lnkSave_Click(object sender, EventArgs e)
    {

        SetAdvancedConditionRowData();

        if (ViewState["dtAdvancedConditions"] != null)
        {
            DataTable dtOldAdvancedConditions = UploadWorld.ets_AdvancedCondition_Select(_iColumnID, "notification", _sConditionType);

            DataTable dtAdvancedConditions = (DataTable) ViewState["dtAdvancedConditions"];

            int iOldRowsCount = dtOldAdvancedConditions.Rows.Count;
            string strActiveAdvancedConditionIDs = "-1";
            int iDO = 1;

            foreach (DataRow drSW in dtAdvancedConditions.Rows)
            {
                if (!String.IsNullOrEmpty(drSW["ColumnID"].ToString()) &&
                    ((iDO == 1) || !String.IsNullOrEmpty(drSW["JoinOperator"].ToString())))
                {
                    bool bInsert = iOldRowsCount < iDO;

                    AdvancedCondition theAdvancedCondition = new AdvancedCondition()
                    {
                        AdvancedConditionID = bInsert
                            ? -1
                            : int.Parse(dtOldAdvancedConditions.Rows[iDO - 1]["AdvancedConditionID"].ToString()),
                        ConditionType = "notification",
                        ConditionSubType = _sConditionType,
                        ColumnID = _iColumnID,
                        ConditionColumnID = int.Parse(drSW["ColumnID"].ToString()),
                        ConditionColumnValue = drSW["ColumnValue"].ToString(),
                        ConditionOperator = drSW["Operator"].ToString(),
                        DisplayOrder = iDO,
                        JoinOperator = (iDO == 1) ? "" : drSW["JoinOperator"].ToString()
                    };
                    if (bInsert)
                    {
                        theAdvancedCondition.AdvancedConditionID =
                            UploadWorld.ets_AdvancedCondition_Insert(theAdvancedCondition);
                    }
                    else
                    {
                        UploadWorld.ets_AdvancedCondition_Update(theAdvancedCondition);
                    }

                    strActiveAdvancedConditionIDs = strActiveAdvancedConditionIDs + "," +
                                                    theAdvancedCondition.AdvancedConditionID.ToString();
                    iDO = iDO + 1;
                }
            }

            Common.ExecuteText("DELETE FROM AdvancedCondition WHERE ColumnID = " + _iColumnID.ToString() +
                               " AND AdvancedConditionID NOT IN (" + strActiveAdvancedConditionIDs + ")" +
                               " AND ConditionType LIKE 'notification'" +
                               " AND ConditionSubType = '" + _sConditionType + "'");
        }

        if (ViewState["dtAdvancedConditions"] != null)
        {
            Session["dtAdvancedConditions"] = (DataTable) ViewState["dtAdvancedConditions"];
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Save Action", String.Format("GetBackValueEdit('{0}');", _sConditionType), true);
    }

    protected void repConditions_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Pages_UserControl_ConditionsCondition2 swcCondition =
                    e.Item.FindControl("swcCondition") as Pages_UserControl_ConditionsCondition2;

                if (swcCondition != null)
                {
                    swcCondition.TableID = _iTableID;
                    swcCondition.ColumnID = -1;
                    swcCondition.ShowTable = true;

                    swcCondition.ddlJoinOperatorV = DataBinder.Eval(e.Item.DataItem, "JoinOperator").ToString();
                    swcCondition.ShowJoinOperator = e.Item.ItemIndex != 0;

                    string sOperator = DataBinder.Eval(e.Item.DataItem, "Operator").ToString();
                    string sValue = DataBinder.Eval(e.Item.DataItem, "ColumnValue").ToString();
                    bool setColumnValue = true;
                    if (DataBinder.Eval(e.Item.DataItem, "JoinOperator").ToString() == "or" || e.Item.ItemIndex == 0)
                    {
                        Panel orSeparator = e.Item.FindControl("orSeparator") as Panel;
                        if (orSeparator != null && e.Item.ItemIndex > 0)
                            orSeparator.Visible = true;
                        _lastOrControl = swcCondition;
                        swcCondition.EnableColumnList = false;
                        if (String.IsNullOrEmpty(sValue) && sOperator != "empty" && sOperator != "notempty")
                        {
                            setColumnValue = false;
                        }
                    }
                    else
                    {
                        if (_lastOrControl != null)
                            _lastOrControl.ShowRemoveButton = false;
                        swcCondition.EnableColumnList = true;
                    }

                    swcCondition.ddlOperatorV = sOperator;
                    swcCondition.ddlHideColumnV = DataBinder.Eval(e.Item.DataItem, "ColumnID").ToString();
                    if (setColumnValue) // empty string hides value control
                        swcCondition.hfConditionValueV = sValue;
                }
            }
        }
        catch (Exception ex)
        {
            //
        }
    }

    protected void lbNewCondition_OnClick(object sender, EventArgs e)
    {
        SetAdvancedConditionRowData();

        if (ViewState["dtAdvancedConditions"] != null)
        {
            DataTable dtAdvancedConditions = (DataTable) ViewState["dtAdvancedConditions"];
            DataRow newRow = dtAdvancedConditions.NewRow();
            newRow["ID"] = Guid.NewGuid().ToString();
            newRow["AdvancedConditionID"] = "-1";
            newRow["ColumnID"] = dtAdvancedConditions.Rows.Count == 0 ? _iColumnID.ToString() : "";
            newRow["ColumnValue"] = "";
            newRow["Operator"] = "equals";
            newRow["DisplayOrder"] = (dtAdvancedConditions.Rows.Count + 1).ToString();
            newRow["JoinOperator"] = dtAdvancedConditions.Rows.Count == 0 ? "or" : "and";

            dtAdvancedConditions.Rows.Add(newRow);
            ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
            PopulateAdvancedCondition();
        }
    }

    protected void swcCondition_OnItemRemoved(object sender, Pages_UserControl_ItemRemovedEventArgs e)
    {
        SetAdvancedConditionRowData();

        if (ViewState["dtAdvancedConditions"] != null)
        {
            DataTable dtAdvancedConditions = (DataTable) ViewState["dtAdvancedConditions"];
            for (int i = dtAdvancedConditions.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = dtAdvancedConditions.Rows[i];
                if (dr["ID"].ToString() == e.ItemId)
                {
                    dr.Delete();
                    break;
                }
            }
            ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
            PopulateAdvancedCondition();
        }
    }

    protected void lbHelp_OnClick(object sender, EventArgs e)
    {
    }

    protected void swcCondition_OnJoinChanged(object sender, Pages_UserControl_JoinChangedEventArgs e)
    {
        if (ViewState["dtAdvancedConditions"] != null)
        {
            DataTable dtAdvancedConditions = (DataTable) ViewState["dtAdvancedConditions"];
            for (int i = dtAdvancedConditions.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = dtAdvancedConditions.Rows[i];
                if (dr["ID"].ToString() == e.ItemId)
                {
                    dr["JoinOperator"] = e.JoinOperator;
                    dr["ColumnID"] = _iColumnID.ToString();
                    break;
                }
            }
            ViewState["dtAdvancedConditions"] = dtAdvancedConditions;
        }
        PopulateAdvancedCondition();
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true" CodeFile="TableNameConfirm.aspx.cs" Inherits="Pages_Help_TableNameConfirm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
       
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" Runat="Server">

    <div>
        <asp:Label runat="server" ID="lblTopTitle" CssClass="TopTitle">Rename Table</asp:Label>
        <br />
        <br />
        <br />
        <div style="text-align:center;">
            <table>
                <tr>
                    <td colspan="2">
                          <asp:Label runat="server" ID="lblMessage" ForeColor="Red"
                        Text="Please confirm that you want to rename the table"></asp:Label>
                        
                    </td>

                </tr>
                <tr><td colspan="2" align="left" style="height:30px;">
                    </td></tr>
            
                <tr>
                    <td colspan="2" align="left" style="height:30px;">
                        <%--<input id="chkboxUpdateMenu" value="Red" checked="checked" AutoPostBack="false" runat="server" type="checkbox" /> Update the Menus to use the new table name.--%>
                        <asp:CheckBox runat="server" ID="chkboxUpdateMenu" TextAlign="Right"  Text=" Update the <b> menu </b> to use the new table name"  AutoPostBack="false"    />
                    </td>
                </tr>              
                <tr>
                    <td colspan="2" align="left" style="height:30px;">
                        <%--<input id="chkboxUpdateView" value="Red" checked="checked"  runat="server" type="checkbox" /> Update the view titles to use the new table name.--%>
                        <asp:CheckBox runat="server" ID="chkboxUpdateView" TextAlign="Right"  Text=" Update the <b> view </b> titles to use the new table name"  />
                    </td>
                </tr>
                
                <tr><td colspan="2" align="left" style="height:30px;">
                    
                    </td></tr>
                 <tr >
                    <td style="width:40%;">


                    </td>
                    <td>
                        <table>

                            <tr>
                                <td>
                                    <asp:LinkButton runat="server" ID="lnkOK"  CssClass="btn" >
                                           <strong>OK</strong>
                                    </asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton runat="server" ID="lnkNo" CssClass="btn">
                                           <strong>Cancel</strong>
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
           
        </div>


    </div>

</asp:Content>


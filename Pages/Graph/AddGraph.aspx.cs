﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Pages_Graph_AddGraph : System.Web.UI.Page
{
    User _objUser;
    Account _theAccount = null;
    UserRole _theUserRole;
    Role _theRole;
    protected void Page_Load(object sender, EventArgs e)
    {
        _objUser = (User)Session["User"];
        _theUserRole = (UserRole)Session["UserRole"];
        _theAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));
        _theRole = SecurityManager.Role_Details((int)_theUserRole.RoleID);
        if (!IsPostBack)
        {
            Session["AddGraphPopupInfo"] = null;
            if (Request.QueryString["type"]!=null && Request.QueryString["type"].ToString()=="t")
            {
                lblTitle.Text = "Add Table";
            }
           
            PopulateReport();
        }

    }
    protected void PopulateReport()
    {

        ddlReport.Items.Add(new ListItem() { Text = "- Please Select -", Value = "" });
        ddlReport.Items.Add(new ListItem() { Text = "(New Report)", Value = "-1" });

        DataTable dtReport = Common.DataTableFromText("SELECT ReportID,ReportName FROM [Report] WHERE AccountID="+Session["AccountID"].ToString()+" ORDER BY ReportName");

        if(dtReport!=null && dtReport.Rows.Count>0)
        {
            foreach(DataRow dr in dtReport.Rows)
            {
                ddlReport.Items.Add(new ListItem() { Text = dr[1].ToString(), Value = dr[0].ToString() });
            }
        }

    }

    protected void optAddGraph_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(optAddGraph.SelectedValue== "report")
        {
            ddlReport.Visible = true;
            rfvReport.Enabled = true;
        }
        else
        {
            ddlReport.Visible = false;
            rfvReport.Enabled = false;
        }
    }



    protected void lnkSave_Click(object sender, ImageClickEventArgs e)
    {
        string xmlAddGraph = @"<root>" +
                                " <optAddGraph>" + HttpUtility.HtmlEncode(optAddGraph.SelectedValue) + "</optAddGraph>" +
                                " <ddlReport>" + HttpUtility.HtmlEncode(ddlReport.SelectedValue.ToString()) + "</ddlReport>" +
                                   "</root>";

        Session["AddGraphPopupInfo"] = xmlAddGraph;
        string strXX = @"
                 
                               var p_btnAddGraph = window.parent.document.getElementById('btnAddGraph');                                   
                               $(p_btnAddGraph).trigger('click');
                               parent.$.fancybox.close();                   
                        ";

        if (Request.QueryString["type"] != null && Request.QueryString["type"].ToString() == "t")
        {
            strXX = @"
                            $(document).ready(function () {
                                   var p_btnAddGraph = window.parent.document.getElementById('btnAddGraph');                                   
                                   $(p_btnAddGraph).trigger('click');
                                   parent.$.fancybox.close();  
                             });
                        ";
        }


        ScriptManager.RegisterStartupScript(this, this.GetType(), "AddGraphPopupInfo", strXX, true);
    }
}
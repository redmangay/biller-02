﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/PoprResponsive.master" AutoEventWireup="true" CodeFile="AddGraph.aspx.cs" Inherits="Pages_Graph_AddGraph" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <script type="text/javascript">

        function GetBack() {
            parent.$.fancybox.close();
        }

    </script>
    <div style="padding-left: 20px; padding-right: 20px;">
        <asp:updatepanel id="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table border="0" width="100%" cellpadding="0" cellspacing="0"  align="center">
                <tr>
                    <td colspan="3">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" >
                                   <asp:Label runat="server" ID="lblTitle" CssClass="TopTitle" Text="Add Graph"></asp:Label>
                                </td>
                                <td align="right">
                                     <asp:ImageButton runat="server" ID="lnkSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                            OnClick="lnkSave_Click" ToolTip="Save" CausesValidation="true" ValidationGroup="addgraph" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" height="26">
                         <asp:ValidationSummary ID="ValidationSummary" runat="server" EnableClientScript="true" ValidationGroup="addgraph"
                                    ShowMessageBox="true" ShowSummary="false" HeaderText="Please correct the following errors:" />
                    </td>
                </tr>
            
               
                <tr>
                    <td colspan="3">
                        <asp:RadioButtonList runat="server" ID="optAddGraph"
                            RepeatDirection="Horizontal" AutoPostBack="true"
                            OnSelectedIndexChanged="optAddGraph_SelectedIndexChanged" >
                            <asp:ListItem Text="To Report" Value="report" Selected="True" style="margin-right:50px" ></asp:ListItem>
                            <asp:ListItem Text="To Dashboard" Value="dash"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:DropDownList ID="ddlReport" runat="server" AutoPostBack="false" Width="155px"
                            CssClass="NormalTextBox" >
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvReport" runat="server" ControlToValidate="ddlReport"
                        CssClass="failureNotification" ErrorMessage="Report name is required." ToolTip="Report name is required."
                        ValidationGroup="addgraph">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
               

            </table>
        </ContentTemplate>
        <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="gvTheGrid" />--%>
        </Triggers>
    </asp:updatepanel>
    </div>

</asp:Content>


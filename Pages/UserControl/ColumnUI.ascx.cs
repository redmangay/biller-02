﻿using System;

public partial class Pages_UserControl_ColumnUI : System.Web.UI.UserControl
{
    string _strFilesLocation = "";
    string _strFilesPhisicalPath = "";
    //KG Ticket 2500 7/5/18
    bool _AllowMultiDropdown = false;
    string _operatorValue = "";

    public int? ColumnID
    {
        get
        {
            if (ViewState["theColumn"] == null)
            {
                return null;
            }
            else
            {
                Column theColumn = (Column)ViewState["theColumn"];
                return theColumn.ColumnID;
            }

        }
        set
        {
            if (value != null && value != -1)
            {
                Column theColumn = RecordManager.ets_Column_Details((int)value);

                if (theColumn != null)
                {
                    ViewState["theColumn"] = theColumn;
                    ShowColumnControls();
                }
                else
                {
                    HideAndDisableAllControls();
                }

            }
            else if (value == -1)
            {
                HideAndDisableAllControls();

                ddlDropdownCommon.Visible = true;
                revddlDropdownCommon.Enabled = true;
                ddlDropdownCommon.Items.Clear();

                RecordManager.PopulateTableDropDown(-1, ref ddlDropdownCommon, "", ""); //3611

                //Create artificial column for User Role
                Column theColumn = new Column();
                theColumn.ColumnID = -1;
                theColumn.ColumnType = "dropdown";

                ViewState["theColumn"] = theColumn;
                //======================================
            }
            else
            {
                HideAndDisableAllControls();
            }



        }
    }

    public string ColumnValue
    {
        get
        {
            return GetColumnInputValue();
        }
        set
        {
            if (value != null)
                SetColumnValue(value);
        }

    }

    //KG Ticket 2500 7/5/18
    public bool AllowMultiDropdown
    {
        get
        {
            return _AllowMultiDropdown;
        }
        set
        {
            _AllowMultiDropdown = value;
        }
    }

    /*red*/
    public string OperatorValue
    {
        get
        {
            return hfOperatorV.Value;
        }
        set
        {

            hfOperatorV.Value = value;
            if (value != "" && value != "empty" && value != "notempty")
                ShowColumnControls();
        }
    }

    public bool ShowControls
    {
        get
        {
            return pnlUIMain.Visible;
        }
        set
        {
            if (value != null)
                pnlUIMain.Visible = value;
        }

    }
    protected void SetColumnValue(string strValue)
    {
        try
        {
            if (ViewState["theColumn"] != null && strValue.Trim() != "")
            {
                Column theColumn = (Column)ViewState["theColumn"];
                pnlUIMain.Visible = true;
                if (theColumn != null)
                {
                    switch (theColumn.ColumnType)
                    {
                        case "listbox":

                            if (theColumn.DateCalculationType == "")
                            {

                                if (theColumn.DropDownType == "values")
                                {
                                    Common.SetListValues(strValue, ref lstListbox, theColumn.DropdownValues);
                                }
                                else if (theColumn.DropDownType == "value_text")
                                {
                                    Common.SetListValues_Text(strValue, ref lstListbox, theColumn.DropdownValues);
                                }
                                else
                                {
                                    if (theColumn.DropDownType == "table" && theColumn.TableTableID != null && theColumn.DisplayColumn != "")
                                    {
                                        Common.SetListValues_ForTable(strValue, ref lstListbox, (int)theColumn.TableTableID,
                                            theColumn.LinkedParentColumnID, theColumn.DisplayColumn);
                                    }

                                }

                            }
                            if (theColumn.DateCalculationType == "checkbox")
                            {
                                cblListbox.Visible = true;

                                if (theColumn.DropDownType == "values")
                                {
                                    Common.SetCheckBoxListValues(strValue, ref cblListbox, theColumn.DropdownValues);
                                }
                                else if (theColumn.DropDownType == "value_text")
                                {
                                    Common.SetCheckBoxListValues_Text(strValue, ref cblListbox, theColumn.DropdownValues);
                                }
                                else
                                {
                                    if (theColumn.DropDownType == "table" && theColumn.TableTableID != null && theColumn.DisplayColumn != "")
                                    {
                                        Common.SetCheckBoxListValues_ForTable(strValue, ref cblListbox, (int)theColumn.TableTableID,
                                            theColumn.LinkedParentColumnID, theColumn.DisplayColumn);
                                    }

                                }

                            }

                            break;
                        case "radiobutton":

                            radioRadiobutton.SelectedValue = strValue;


                            break;
                        case "checkbox":
                            Common.SetCheckBoxValue(theColumn.DropdownValues, strValue, ref chkCheckbox);
                            break;
                        case "number":
                            txtNumber.Text = strValue;
                            break;
                        case "datetime":

                            if (hfOperatorV.Value == "greaterthandays" || hfOperatorV.Value == "lessthandays")
                            {
                                txtNumber.Text = strValue;
                            }
                            else
                            {
                                DateTime dtTempDateTime = DateTime.Parse(strValue);
                                txtDTDate.Text = dtTempDateTime.Day.ToString("00") + "/" + dtTempDateTime.Month.ToString("00") + "/" + dtTempDateTime.Year.ToString();
                                txtDTTime.Text = Convert.ToDateTime(dtTempDateTime.ToString()).ToString("HH:m");
                            }
                            break;

                        case "date":
                            if (hfOperatorV.Value == "greaterthandays" || hfOperatorV.Value == "lessthandays")
                            {
                                txtNumber.Text = strValue;
                            }
                            else
                            {
                                DateTime dtTempDate = DateTime.Parse(strValue);
                                txtDate.Text = dtTempDate.Day.ToString() + "/" + dtTempDate.Month.ToString("00") + "/" + dtTempDate.Year.ToString();
                            }
                           

                            break;

                        case "time":
                            txtTime.Text = Convert.ToDateTime(strValue).ToString("HH:m");
                            break;

                        case "dropdown":
                            ddlDropdownCommon.SelectedValue = strValue;
                            break;

                        default:
                            txtTextCommon.Text = strValue;
                            break;
                    }

                }
            }
            else if (ViewState["theColumn"] != null && strValue.Trim() == "")
            {
                Column theColumn = (Column)ViewState["theColumn"];
                pnlUIMain.Visible = false;
                if (theColumn != null)
                {
                    switch (theColumn.ColumnType)
                    {
                        case "listbox":
                            if (theColumn.DateCalculationType == "")
                            {
                                lstListbox.ClearSelection();
                            }
                            if (theColumn.DateCalculationType == "checkbox")
                            {
                                cblListbox.Visible = true;
                                cblListbox.ClearSelection();
                            }

                            break;
                        case "radiobutton":
                            radioRadiobutton.ClearSelection();

                            break;
                        case "checkbox":
                            chkCheckbox.Checked = false;
                            break;
                        case "number":
                            txtNumber.Text = strValue;
                            break;
                        case "datetime":
                            txtDTDate.Text = "";
                            txtDTTime.Text = "";
                            break;

                        case "date":
                            txtDate.Text = "";
                            break;

                        case "time":
                            txtTime.Text = "";
                            break;

                        case "dropdown":
                            ddlDropdownCommon.ClearSelection();
                            break;

                        default:
                            txtTextCommon.Text = "";
                            break;
                    }

                }
            }
        }
        catch
        {

        }




    }
    protected string GetColumnInputValue()
    {
        string strInputValue = "";

        try
        {

            if (ViewState["theColumn"] != null)
            {
                Column theColumn = (Column)ViewState["theColumn"];

                if (theColumn != null)
                {
                    switch (theColumn.ColumnType)
                    {
                        case "listbox":

                            if (theColumn.DateCalculationType == "")
                            {
                                strInputValue = Common.GetListValues(lstListbox);

                            }
                            if (theColumn.DateCalculationType == "checkbox")
                            {
                                strInputValue = Common.GetCheckBoxListValues(cblListbox);

                            }

                            break;
                        case "radiobutton":

                            if (radioRadiobutton.SelectedItem != null)
                                strInputValue = radioRadiobutton.SelectedItem.Value;

                            break;
                        case "checkbox":
                            strInputValue = Common.GetCheckBoxValue(theColumn.DropdownValues, ref chkCheckbox);
                            break;
                        case "number":
                            strInputValue = txtNumber.Text;
                            break;
                        case "datetime":
                            if (hfOperatorV.Value == "greaterthandays" || hfOperatorV.Value == "lessthandays")
                            {
                                strInputValue = txtNumber.Text.Trim();
                            }
                            else
                            {
                                strInputValue = Common.GetDateTimeFromDnT(txtDTDate.Text, txtDTTime.Text);
                            }
                            break;

                        case "date":
                            if (hfOperatorV.Value == "greaterthandays" || hfOperatorV.Value == "lessthandays")
                            {
                                strInputValue = txtNumber.Text.Trim();
                            }
                            else
                            {
                                strInputValue = Common.GetDatestringFromD(txtDate.Text);
                            }
                           
                            break;

                        case "time":
                            strInputValue = txtTime.Text.Trim();
                            break;

                        case "dropdown":
                            //KG Ticket 2500 7/5/18
                            if (_AllowMultiDropdown)
                                strInputValue = hfSelected_DDL.Value;
                            else
                                strInputValue = ddlDropdownCommon.SelectedValue;
                            break;

                        default:
                            strInputValue = txtTextCommon.Text.Trim();
                            break;
                    }

                }
            }
        }
        catch
        {
            //
        }

        return strInputValue;
    }
    protected void ShowColumnControls()
    {
        if (ViewState["theColumn"] != null)
        {
            Column theColumn = (Column)ViewState["theColumn"];
            pnlUIMain.Visible = true;
            if (theColumn != null)
            {
                HideAndDisableAllControls();

                switch (theColumn.ColumnType)
                {
                    case "listbox":

                        if (theColumn.DateCalculationType == "")
                        {
                            lstListbox.Visible = true;
                            if (theColumn.DropDownType == "values")
                            {
                                Common.PutListValues(theColumn.DropdownValues, ref lstListbox);
                            }
                            else if (theColumn.DropDownType == "value_text")
                            {
                                Common.PutListValues_Text(theColumn.DropdownValues, ref lstListbox);
                            }
                            else
                            {
                                if (theColumn.DropDownType == "table" && theColumn.TableTableID != null && theColumn.DisplayColumn != "")
                                {
                                    Common.PutList_FromTable((int)theColumn.TableTableID, null, theColumn.DisplayColumn,
                                        ref lstListbox);
                                }

                            }

                        }
                        if (theColumn.DateCalculationType == "checkbox")
                        {
                            cblListbox.Visible = true;
                            if (theColumn.DropDownType == "values")
                            {
                                Common.PutCheckBoxListValues(theColumn.DropdownValues, ref cblListbox);
                            }
                            else if (theColumn.DropDownType == "value_text")
                            {
                                Common.PutCheckBoxListValues_Text(theColumn.DropdownValues, ref cblListbox);
                            }
                            else
                            {
                                if (theColumn.DropDownType == "table" && theColumn.TableTableID != null && theColumn.DisplayColumn != "")
                                {
                                    Common.PutCheckBoxList_ForTable((int)theColumn.TableTableID, null, theColumn.DisplayColumn,
                                        ref cblListbox);
                                }

                            }

                        }


                        break;
                    case "radiobutton":

                        radioRadiobutton.Visible = true;
                        if (theColumn.DropDownType == "values")
                        {
                            Common.PutRadioList(theColumn.DropdownValues, ref radioRadiobutton);
                        }
                        else if (theColumn.DropDownType == "value_text")
                        {
                            Common.PutRadioListValue_Text(theColumn.DropdownValues, ref radioRadiobutton);
                        }
                        else
                        {
                            Common.PutRadioListValue_Image(theColumn.DropdownValues, ref radioRadiobutton, _strFilesLocation);
                        }

                        break;
                    case "checkbox":
                        chkCheckbox.Visible = true;
                        Common.PutCheckBoxDefault(theColumn.DropdownValues, ref chkCheckbox);
                        break;
                    case "number":
                        txtNumber.Visible = true;
                        revtxtNumber.Enabled = true;
                        revNumber.Enabled = true;
                        break;
                    case "datetime":
                        if (hfOperatorV.Value == "greaterthandays" || hfOperatorV.Value == "lessthandays")
                        {
                            txtNumber.Visible = true;
                            revtxtNumber.Enabled = true;
                            revNumber.Enabled = true;
                        }
                        else
                        {
                            divDateTime.Visible = true;
                            revtxtDTDate.Enabled = true;
                            revtxtDTTime.Enabled = true;

                            ceDTDate.Enabled = true;
                            meeDTTime.Enabled = true;
                        }
                        break;

                    case "date":
                        if (hfOperatorV.Value == "greaterthandays" || hfOperatorV.Value == "lessthandays")
                        {
                            txtNumber.Visible = true;
                            revtxtNumber.Enabled = true;
                            revNumber.Enabled = true;
                        }
                        else
                        {
                            divDate.Visible = true;
                            revtxtDate.Enabled = true;
                            ceDate.Enabled = true;
                        }
                       
                        break;

                    case "time":
                        txtTime.Visible = true;
                        revtxtTime.Enabled = true;
                        meeTime.Enabled = true;
                        break;

                    case "dropdown":
                        ddlDropdownCommon.Visible = true;
                        revddlDropdownCommon.Enabled = true;
                        ddlDropdownCommon.Items.Clear();

                        //KG Ticket 2500 7/5/18
                        hfSelected_DDL.Value = "";
                        ddlDropdownCommon.Attributes.Remove("multiple");

                        if (theColumn.DropDownType == "values")
                        {
                            Common.PutDDLValues(theColumn.DropdownValues, ref ddlDropdownCommon);

                        }
                        else if (theColumn.DropDownType == "value_text")
                        {
                            Common.PutDDLValue_Text(theColumn.DropdownValues, ref ddlDropdownCommon);

                        }
                        else
                        {
                            RecordManager.PopulateTableDropDown((int)theColumn.ColumnID, ref ddlDropdownCommon, "", "");//3611

                            //KG Ticket 2500 7/5/18
                            if (_AllowMultiDropdown)
                            {
                                if (ddlDropdownCommon.Items.Count > 0)
                                {
                                    ddlDropdownCommon.Items.RemoveAt(0);
                                }

                                ddlDropdownCommon.Attributes.Add("multiple", "multiple");
                                ddlDropdownCommon.Width = 300;
                                string stringSearch = @"
                                    $(document).ready(function () {  
	                                $('#" + ddlDropdownCommon.ClientID + @"').multipleSelect({
                                                placeholder: ""--Select All--"" ,
                                                filter: true,                                    
                                                onClose: function () {
                                                      $('#" + hfSelected_DDL.ClientID + @"').val($('#" + ddlDropdownCommon.ClientID + @"').multipleSelect('getSelects'));

                                                }
                                    });

                                });
                            ";

                                System.Web.UI.ScriptManager.RegisterStartupScript(pnlUIMain, pnlUIMain.GetType(), ddlDropdownCommon.ID.ToString(), stringSearch, true);
                            }
                            // end ticket 2500

                        }
                        break;

                    default:
                        txtTextCommon.Visible = true;
                        revtxtCommon.Enabled = true;
                        break;
                }

            }
        }
    }

    protected void HideAndDisableAllControls()
    {

        txtTextCommon.Visible = false;
        revtxtCommon.Enabled = false;
        txtNumber.Visible = false;
        revtxtNumber.Enabled = false;

        divDateTime.Visible = false;
        revtxtDTDate.Enabled = false ;
        revtxtDTTime.Enabled = false;

        divDate.Visible = false;
        revtxtDate.Enabled = false;
        txtTime.Visible = false;
        revtxtTime.Enabled = false;

        lstListbox.Visible = false;
        cblListbox.Visible = false;

        ddlDropdownCommon.Visible = false;
        revddlDropdownCommon.Enabled = false;

        chkCheckbox.Visible = false;
        radioRadiobutton.Visible = false;


        revNumber.Enabled = false;

        ceDTDate.Enabled = false;
        meeDTTime.Enabled = false;

        ceDate.Enabled = false;
        meeTime.Enabled = false;


    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_Init(object sender, EventArgs e)
    {
        _strFilesLocation = Session["FilesLocation"].ToString();
        _strFilesPhisicalPath = Session["FilesPhisicalPath"].ToString();
    }
}
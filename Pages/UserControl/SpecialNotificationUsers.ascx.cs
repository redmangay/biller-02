﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class Pages_UserControl_SpecialNotificationUsers : System.Web.UI.UserControl
{
    string _strActionMode = "view";
    int? _iDataReminderID;
    string _qsMode = "";
    int _qsSpecialNotificationID = -1;
    int _iColumnID = -1;
    User _ObjUser;
    int counter = 1;
    protected void Page_Load(object sender, EventArgs e)
    {


        _ObjUser = (User)Session["User"];



        if (Session["newSpecialNotificationID"] != null)
        {
            _qsSpecialNotificationID = int.Parse(Session["newSpecialNotificationID"].ToString());

        }

        if (!IsPostBack)
        {
          

            PopulateUsersGrid();
            PopulateChoicesDropDown();
            PopulateEmailFromRecordDropdown();
            PopulateUserDropDown();
        }

    }

    protected void grdUsers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            ImageButton ib = (ImageButton)e.Row.FindControl("imgbtnDelete");
            ib.Attributes.Add("onclick", "javascript:return " +
            "confirm('Are you sure you want to remove Special Notification: " +
            DataBinder.Eval(e.Row.DataItem, "UserName") + DataBinder.Eval(e.Row.DataItem, "DisplayName") + "?');");

            Label lblUsers = (Label)e.Row.FindControl("lblUsers");
            Label lblSendTo = (Label)e.Row.FindControl("lblSendTo");

            if (DataBinder.Eval(e.Row.DataItem, "RecipientOption").ToString() == "EOCR")
            {
                lblSendTo.Text = "Current Record: " + DataBinder.Eval(e.Row.DataItem, "DisplayName").ToString();
            }
            if (DataBinder.Eval(e.Row.DataItem, "RecipientOption").ToString() == "UWCTR")
            {
                lblSendTo.Text = "User who created the record";
            }
            if (DataBinder.Eval(e.Row.DataItem, "RecipientOption").ToString() == "UWLETR")
            {
                lblSendTo.Text = "User who last edited the record";
            }

        }
    }

    protected void grdUsers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "deletetype")
        {
            try
            {
                ReminderManager.SpecialNotificationRecipient_Delete(Convert.ToInt32(e.CommandArgument));
                PopulateUsersGrid();
                PopulateChoicesDropDown();
                PopulateEmailFromRecordDropdown();
                PopulateUserDropDown();
            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Table Special Notification", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }
        }
    }


    protected void PopulateUsersGrid()
    {
        try
        {
            grdUsersWiz.DataSource = ReminderManager.SpecialNotificationRecipient_Select(_qsSpecialNotificationID);
            grdUsersWiz.DataBind();
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Special Notification Load Grid View", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }

    }


    protected void lnkAddUser_Click(object sender, EventArgs e)
    {
        if (ddlUserWiz.Text == "")
        {
            //ddlUser.Focus();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Problem", "alert('Please select a user.');", true);
            return;
        }

        if (ddlUserWiz.Text == "SU" && ddlSpecificUserWiz.Text == "")
        {
            //ddlUser.Focus();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Problem", "alert('Please select a specific user.');", true);
            return;
        }
        if (ddlUserWiz.Text == "EOCR" && ddlEmailFromRecordWiz.Text == "")
        {
            //ddlUser.Focus();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Problem", "alert('Please select a field.');", true);
            return;
        }



        lblMsg.Text = "";

        int? UserID = null;
        int? ColumnID = null;

        if (ddlUserWiz.SelectedValue == "SU")
        {
            UserID = int.Parse(ddlSpecificUserWiz.SelectedValue);
        }
        else if (ddlUserWiz.SelectedValue == "EOCR")
        {
            ColumnID = int.Parse(ddlEmailFromRecordWiz.SelectedValue);
        }


        SpecialNotificationRecipient newSpecialNotificationRecipient =
                new SpecialNotificationRecipient(
                        null,
                        _qsSpecialNotificationID,
                        UserID,
                        ColumnID,
                        ddlUserWiz.SelectedValue);


        int newSpecialNotificationID = ReminderManager.SpecialNotificationRecipient_Insert(newSpecialNotificationRecipient);

        PopulateUsersGrid();
        PopulateChoicesDropDown();
        PopulateEmailFromRecordDropdown();
        PopulateUserDropDown();


    }


    protected void PopulateUserDropDown()
    {

        ddlSpecificUserWiz.Items.Clear();

        ddlSpecificUserWiz.DataSource = Common.DataTableFromText(@"SELECT ([User].FirstName + ' ' + [User].LastName ) As UserName, [User].Userid as UserID
	                    FROM [User] INNER JOIN UserRole ON [User].UserID=[UserRole].UserID WHERE [User].IsActive=1 and
	                    [UserRole].AccountID=" + Session["AccountID"].ToString() + @" AND [User].UserID NOT IN (SELECT DISTINCT SpecialNotificationRecipient.UserID
                        FROM SpecialNotificationRecipient 
                        WHERE UserID is not null and  SpecialNotificationID=" + _qsSpecialNotificationID + @" )
	                    ORDER BY [User].FirstName");

        ddlSpecificUserWiz.DataBind();

        System.Web.UI.WebControls.ListItem liSelect = new System.Web.UI.WebControls.ListItem(" - Please select user-", "");
        ddlSpecificUserWiz.Items.Insert(0, liSelect);

    }

    protected void PopulateChoicesDropDown()
    {
        ddlUserWiz.Items.Clear();

        //System.Web.UI.WebControls.ListItem liSelect = new System.Web.UI.WebControls.ListItem("-Please select-", "");
        //ddlUser.Items.Insert(0, liSelect);
        System.Web.UI.WebControls.ListItem liSpecificUser = new System.Web.UI.WebControls.ListItem("Specific User", "SU");
        ddlUserWiz.Items.Insert(0, liSpecificUser);
        System.Web.UI.WebControls.ListItem liEmailOnRecord = new System.Web.UI.WebControls.ListItem("Email on current record", "EOCR");
        ddlUserWiz.Items.Insert(1, liEmailOnRecord);
        System.Web.UI.WebControls.ListItem liCreatedRecord = new System.Web.UI.WebControls.ListItem("User who created the record", "UWCTR");
        ddlUserWiz.Items.Insert(2, liCreatedRecord);
        System.Web.UI.WebControls.ListItem liEditedRecord = new System.Web.UI.WebControls.ListItem("User who last edited the record", "UWLETR");
        ddlUserWiz.Items.Insert(3, liEditedRecord);
    }

    protected void PopulateEmailFromRecordDropdown()
    {
        int iTableID = -1;
        if (Session["SpecialNotificationTableID"] != null)
        {
            iTableID  = int.Parse(Session["SpecialNotificationTableID"].ToString());
        }
     

        ddlEmailFromRecordWiz.Items.Clear();
        ddlEmailFromRecordWiz.DataSource = Common.DataTableFromText(@"SELECT C1.ColumnID,C1.DisplayName FROM [Column] C1 
	                                WHERE C1.ColumnType = 'text' AND C1.TextType = 'email'   
                                    AND C1.TableID=" + iTableID.ToString() + @" UNION ALL SELECT C2.ColumnID,C2.DisplayName FROM [Column] C2
                                    WHERE  C2.ColumnType = 'dropdown' AND C2.DropDownType like 'table%'
                                    AND C2.TableTableID = -1 AND C2.TableTableID IS NOT Null AND C2.TableID=" + iTableID.ToString());
        ddlEmailFromRecordWiz.DataBind();
        System.Web.UI.WebControls.ListItem liSelect = new System.Web.UI.WebControls.ListItem(" -- Please select -- ", "");
        ddlEmailFromRecordWiz.Items.Insert(0, liSelect);
    }
}

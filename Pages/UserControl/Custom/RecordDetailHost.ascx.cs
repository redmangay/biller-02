﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_UserControl_Custom_RecordDetailHost : System.Web.UI.UserControl
{
   
    public string VXXX
    {
        get
        {
            return ctdHost.RecordID==null?"":ctdHost.RecordID.ToString();
        }
        set
        {
            int iRID = 0;
            if(int.TryParse(value,out iRID))
            {
                RecordID = iRID;
            }
           
        }
    }
    public int? RecordID
    {
        get
        {
            
                return ctdHost.RecordID;
        }
        set
        {
      
                ctdHost.RecordID =value;

                string strTableID = Common.GetValueFromSQL("SELECT TableID FROM [Record] WHERE RecordID=" + value.ToString());
                if (strTableID != "")
                {
                    int iTID = 0;
                    if (int.TryParse(strTableID, out iTID))
                    {

                        TableID = iTID;
                    }
                }
        }
    }
    public int? TableID
    {
        get
        {
            if (ctdHost.TableID == 0)
                return null;
            else
                return ctdHost.TableID;
        }
        set
        {
            if(value!=null)
                ctdHost.TableID =(int) value;
        }
    }
    public string ColumnNotIn
    {
        get
        {
            if (string.IsNullOrEmpty( ctdHost.ColumnNotIn))
                return "";
            else
                return ctdHost.ColumnNotIn;
        }
        set
        {
            if (value != null)
                ctdHost.ColumnNotIn = (string)value;
        }
    }
    public int? BaseColumnID
    {
        get
        {
            return ctdHost.DisplayInColumnID;
        }
        set
        {
            if(value!=null)
            {
                _baseColumn = RecordManager.ets_Column_Details((int)value);

                if (RecordID == null && TableID == null && _baseColumn != null)
                {
                    if (!string.IsNullOrEmpty(_baseColumn.ButtonInfo))
                    {
                        DocGen.DAL.UserControlInfo theUserControlInfo = DocGen.DAL.JSONField.GetTypedObject<DocGen.DAL.UserControlInfo>(_baseColumn.ButtonInfo);
                        if (theUserControlInfo != null && theUserControlInfo.TableID != null)
                        {

                            TableID = theUserControlInfo.TableID;

                        }

                        if (TableID == null && theUserControlInfo != null && theUserControlInfo.RefColumnID != null)
                        {
                            Column refColumn = RecordManager.ets_Column_Details((int)theUserControlInfo.RefColumnID);
                            if (refColumn != null && refColumn.TableTableID != null)
                            {
                                TableID = refColumn.TableTableID;
                            }
                        }
                        if (theUserControlInfo != null && !string.IsNullOrEmpty(theUserControlInfo.ColumnNotIn))
                        {

                            ColumnNotIn = theUserControlInfo.ColumnNotIn;

                        }
                    }


                }


                if(string.IsNullOrEmpty(ColumnNotIn) && _baseColumn!=null &&!string.IsNullOrEmpty(_baseColumn.ButtonInfo))
                {
                    DocGen.DAL.UserControlInfo theUserControlInfo = DocGen.DAL.JSONField.GetTypedObject<DocGen.DAL.UserControlInfo>(_baseColumn.ButtonInfo);
                    if (theUserControlInfo != null && !string.IsNullOrEmpty(theUserControlInfo.ColumnNotIn ))
                    {

                        ColumnNotIn = theUserControlInfo.ColumnNotIn;

                    }
                }

            }
            ctdHost.DisplayInColumnID = value;
        }
    }
    public Column _baseColumn;

    //This should be VXXX
    //This only applicable Parent Record
    //Trying Dropdown now -- must need to implement Listbox or "one at a time" or naviagation enabled

    public int? BaseRecordID
    {
        get
        {
            return ViewState["BaseRecordID"] == null ? null : (int?)int.Parse(ViewState["BaseRecordID"].ToString());
        }
        set
        {
            ViewState["BaseRecordID"] = value;
            if(value!=null && _baseColumn!=null)
            {
                Record theBaseRecord = RecordManager.ets_Record_Detail_Full((int)value,null,false);

                if (!string.IsNullOrEmpty(_baseColumn.ButtonInfo) && theBaseRecord!=null)
                {
                    DocGen.DAL.UserControlInfo theUserControlInfo = DocGen.DAL.JSONField.GetTypedObject<DocGen.DAL.UserControlInfo>(_baseColumn.ButtonInfo);
                    

                    if (theUserControlInfo != null && theUserControlInfo.RefColumnID != null)
                    {
                        Column refColumn = RecordManager.ets_Column_Details((int)theUserControlInfo.RefColumnID);
                        if (refColumn != null )
                        {
                            string strRefValue = RecordManager.GetRecordValue(ref theBaseRecord, refColumn.SystemName);
                            if(!string.IsNullOrEmpty( strRefValue))
                            {
                                int iRID = 0;
                                if (int.TryParse(strRefValue, out iRID))
                                {
                                    RecordID = iRID;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //ctdHost.ClientIDMode = ClientIDMode.AutoID;
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    
}
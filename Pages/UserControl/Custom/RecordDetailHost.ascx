﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecordDetailHost.ascx.cs"
     Inherits="Pages_UserControl_Custom_RecordDetailHost" %>
<%@ Register Src="~/Pages/UserControl/DetailEdit.ascx" TagName="CTDetail" TagPrefix="dbg" %>

<div>
    <dbg:CTDetail runat="server" ID="ctdHost" HideHistory="true" OnlyOneRecord="true"  ClientIDMode="AutoID"
        ShowAddButton="false" ShowEditButton="false" ContentPage="any" DetailTabIndex="0" Mode="view"
         IsPublic="false"  />
</div>
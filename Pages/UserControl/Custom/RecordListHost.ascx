﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecordListHost.ascx.cs"
     Inherits="Pages_UserControl_Custom_RecordListHost" %>
<%@ Register Src="~/Pages/UserControl/RecordList.ascx" TagName="CTList" TagPrefix="dbg" %>

<div>
    <dbg:CTList runat="server" ID="ctHost" ClientIDMode="AutoID" PageType="any" />
</div>
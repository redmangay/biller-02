﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_UserControl_Custom_CustomTest : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public string VXXX
    {
        get
        {
            return txtVXXX.Text;
        }
        set
        {
            txtVXXX.Text = value;
        }
    }
    public int? BaseColumnID
    {
        get
        {
            return ViewState["BaseColumnID"] == null ? null : (int?)int.Parse(ViewState["BaseColumnID"].ToString());
        }
        set
        {
            ViewState["BaseColumnID"] = value;
        }
    }

    public int? BaseRecordID
    {
        get
        {
            return ViewState["BaseRecordID"] == null ? null : (int?)int.Parse(ViewState["BaseRecordID"].ToString());
        }
        set
        {
            ViewState["BaseRecordID"] = value;
        }
    }


    public string TextProperty
    {
        get
        {
            return lblDisplayMethod.Text;
        }
        set
        {
            lblDisplayMethod.Text =lblDisplayMethod.Text+ value;
        }
    }
}
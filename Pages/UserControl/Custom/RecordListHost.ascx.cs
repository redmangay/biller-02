﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial  class Pages_UserControl_Custom_RecordListHost : System.Web.UI.UserControl
{
   
    public string VXXX
    {
        get
        {
            return ctHost.RecordID==null?"":ctHost.RecordID.ToString();
        }
        set
        {
            int iRID = 0;
            if(int.TryParse(value,out iRID))
            {
                RecordID = iRID;
            }
           
        }
    }
    public int? RecordID
    {
        get
        {
            
                return ctHost.RecordID;
        }
        set
        {
      
                ctHost.RecordID =value;

                string strTableID = Common.GetValueFromSQL("SELECT TableID FROM [Record] WHERE RecordID=" + value.ToString());
                if (strTableID != "")
                {
                    int iTID = 0;
                    if (int.TryParse(strTableID, out iTID))
                    {

                        TableID = iTID;
                    }
                }
        }
    }
    public int? TableID
    {
        get
        {
            if (ctHost.TableID == 0)
                return null;
            else
                return ctHost.TableID;
        }
        set
        {
            if(value!=null)
                ctHost.TableID =(int) value;
        }
    }
   
    public int? BaseColumnID
    {
        get
        {
            return ctHost.DisplayInColumnID;
        }
        set
        {
            if(value!=null)
            {
                _baseColumn = RecordManager.ets_Column_Details((int)value);

                if (RecordID == null && TableID == null && _baseColumn != null)
                {
                    if (!string.IsNullOrEmpty(_baseColumn.ButtonInfo))
                    {
                        DocGen.DAL.UserControlInfo theUserControlInfo = DocGen.DAL.JSONField.GetTypedObject<DocGen.DAL.UserControlInfo>(_baseColumn.ButtonInfo);
                        if (theUserControlInfo != null && theUserControlInfo.TableID != null)
                        {

                            TableID = theUserControlInfo.TableID;

                        }

                        if (TableID == null && theUserControlInfo != null && theUserControlInfo.RefColumnID != null)
                        {
                            Column refColumn = RecordManager.ets_Column_Details((int)theUserControlInfo.RefColumnID);
                            if (refColumn != null && refColumn.TableTableID != null)
                            {
                                TableID = refColumn.TableTableID;
                            }
                        }
                        
                    }


                }


               

            }
            ctHost.DisplayInColumnID = value;
        }
    }
    public Column _baseColumn;

    //This should be VXXX
    //This only applicable Parent Record
    //Trying Dropdown now -- must need to implement Listbox or "one at a time" or naviagation enabled

    public int? BaseRecordID
    {
        get
        {
            return ViewState["BaseRecordID"] == null ? null : (int?)int.Parse(ViewState["BaseRecordID"].ToString());
        }
        set
        {
            ViewState["BaseRecordID"] = value;
            if(value!=null && _baseColumn!=null)
            {
                Record theBaseRecord = RecordManager.ets_Record_Detail_Full((int)value,null,false);

                if (!string.IsNullOrEmpty(_baseColumn.ButtonInfo) && theBaseRecord!=null)
                {
                    DocGen.DAL.UserControlInfo theUserControlInfo = DocGen.DAL.JSONField.GetTypedObject<DocGen.DAL.UserControlInfo>(_baseColumn.ButtonInfo);
                    

                    if (theUserControlInfo != null && theUserControlInfo.RefColumnID != null)
                    {
                        Column refColumn = RecordManager.ets_Column_Details((int)theUserControlInfo.RefColumnID);
                        if (refColumn != null )
                        {
                            string strRefValue = RecordManager.GetRecordValue(ref theBaseRecord, refColumn.SystemName);
                            if(!string.IsNullOrEmpty( strRefValue))
                            {
                                int iRID = 0;
                                if (int.TryParse(strRefValue, out iRID))
                                {
                                    RecordID = iRID;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //ctHost.ClientIDMode = ClientIDMode.AutoID;
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    
}
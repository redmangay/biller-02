﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.IO;
//using DocGen.DAL;
public partial class Pages_UserControl_FilterGrid : System.Web.UI.UserControl
{
    public int TableID
    {
        set
        {
            ViewState["TableID"] = value;
        }
        get
        {
            if (ViewState["TableID"] == null)
                ViewState["TableID"] = 0;
          return int.Parse( ViewState["TableID"].ToString());
        }
    }

    public string CustomColumn1
    {
        set
        {
            ViewState["CustomColumn1"] = value;
        }
        get
        {
            if (ViewState["CustomColumn1"] == null)
                ViewState["CustomColumn1"] = "";
            return ViewState["CustomColumn1"].ToString();
        }
    }

    public string XMLText
    {
        set
        {
            ViewState["XMLText"] = value;
        }
        get
        {
            ViewState["XMLText"] = GetXMLText();
            return GetXMLText();
        }
    }
    public int ColumnID
    {
        set
        {
            ViewState["ColumnID"] = value;
        }
        get
        {
            if (ViewState["ColumnID"] == null)
                ViewState["ColumnID"] = 0;
            return int.Parse(ViewState["ColumnID"].ToString());
        }
    }

    public bool HideColumn
    {
        set
        {
            ViewState["HideColumn"] = value;
        }
        get
        {
            if (ViewState["HideColumn"] == null)
                ViewState["HideColumn"] = false;
            return bool.Parse(ViewState["HideColumn"].ToString());
        }
    }

    public bool HideCompareOperator
    {
        set
        {
            ViewState["HideCompareOperator"] = value;
        }
        get
        {
            if (ViewState["HideCompareOperator"] == null)
                ViewState["HideCompareOperator"] = false;
            return bool.Parse(ViewState["HideCompareOperator"].ToString());
        }
    }
    public bool HideAndOr
    {
        set
        {
            ViewState["HideAndOr"] = value;
        }
        get
        {
            if (ViewState["HideAndOr"] == null)
                ViewState["HideAndOr"] = false;
            return bool.Parse(ViewState["HideAndOr"].ToString());
        }
    }

    //public string CustomColumn1 = "";
    //public int? ColumnID=null;
    //public bool HideColumn = false;
    //public bool HideCompareOperator = false;
    //public bool HideAndOr = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (ViewState["XMLText"]==null)
                PopulateFilterGrid("");
        }
        
    }

    // OnddlYAxis_Changed="grdFilter_cbcFilter_YAxisChanged"
    protected void grdFilter_cbcFilter_YAxisChanged(object sender, EventArgs e)
    {
        Pages_UserControl_ControlByColumn cbcFilter = sender as Pages_UserControl_ControlByColumn;

        if (cbcFilter != null)
        {
            GridViewRow row = cbcFilter.NamingContainer as GridViewRow;
            Label lblID = row.FindControl("lblID") as Label;
            //ImageButton imgbtnMinus = row.FindControl("imgbtnMinus") as ImageButton;
            //DropDownList ddlTextColour = row.FindControl("ddlTextColour") as DropDownList;

            //if (cbcFilter.ddlYAxisV == "")
            //{
            //    imgbtnPlus.Visible = false;
            //}
            //else
            //{
            //    imgbtnPlus.Visible = true;
            //}
        }


    }

    public bool HideHeader
    {
        set
        {
            grdFilter.ShowHeader = !value;
        }
    }
    protected void grdFilter_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if(CustomColumn1!="")
                {
                    Label lblCustom1 = e.Row.FindControl("lblCustom1") as Label;
                    lblCustom1.Visible = true;
                    if (CustomColumn1=="mappin")
                    {
                        lblCustom1.Text = "Map Pin";
                    }
                   
                }
                
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblID = e.Row.FindControl("lblID") as Label;
                HiddenField hfAndOr = e.Row.FindControl("hfAndOr") as HiddenField;
                LinkButton lnkAndOr = e.Row.FindControl("lnkAndOr") as LinkButton;
                lnkAndOr.Attributes.Add("onclick", "toggleAndOr(this,'" + hfAndOr.ClientID + "');return false;");

                if (DataBinder.Eval(e.Row.DataItem, "andor") != null && DataBinder.Eval(e.Row.DataItem, "andor").ToString() != "")
                {
                    hfAndOr.Value = DataBinder.Eval(e.Row.DataItem, "andor").ToString();
                    lnkAndOr.Text = hfAndOr.Value;
                }
                if(HideAndOr)
                {
                    hfAndOr.Value = "";
                    lnkAndOr.Visible = false;
                }
                ImageButton imgbtndelete = e.Row.FindControl("imgbtndelete") as ImageButton;
               
                //DropDownList ddlTextColour = e.Row.FindControl("ddlTextColour") as DropDownList;              
                


                Pages_UserControl_ControlByColumn cbcFilter = e.Row.FindControl("cbcFilter") as Pages_UserControl_ControlByColumn;
                cbcFilter.TableID = TableID;
                cbcFilter.ddlYAxisV = DataBinder.Eval(e.Row.DataItem, "columnid").ToString();
                if(ColumnID!=null)
                {
                    cbcFilter.ColumnID = ColumnID;
                    if(HideColumn)
                    {
                        cbcFilter.HideColumn = true;
                    }
                    if(HideCompareOperator)
                    {
                        cbcFilter.HideCompareOperator = true;
                    }
                }
                cbcFilter.ColumnTypeOut = "'file','image','calculation'";
               
                cbcFilter.TextValue = DataBinder.Eval(e.Row.DataItem, "value").ToString();
                if (DataBinder.Eval(e.Row.DataItem, "CompareOperator") != null)
                    cbcFilter.CompareOperator = DataBinder.Eval(e.Row.DataItem, "CompareOperator").ToString();

                //if (DataBinder.Eval(e.Row.DataItem, "colour").ToString() != "")
                //    ddlTextColour.SelectedValue = DataBinder.Eval(e.Row.DataItem, "colour").ToString();
                if (CustomColumn1 != "")
                {
                    DropDownList ddlCustom1 = e.Row.FindControl("ddlCustom1") as DropDownList;
                    PopulateCustomColumn1(ref ddlCustom1);
                    ddlCustom1.Visible = true;
                    if (DataBinder.Eval(e.Row.DataItem, "custom1") != null)
                    {
                        if (ddlCustom1.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "custom1").ToString()) != null)
                        {
                            ddlCustom1.SelectedValue = DataBinder.Eval(e.Row.DataItem, "custom1").ToString();
                        }
                    }
                }

                lblID.Text = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
                imgbtndelete.CommandArgument = DataBinder.Eval(e.Row.DataItem, "ID").ToString();
                //imgbtnPlus.CommandArgument = DataBinder.Eval(e.Row.DataItem, "ID").ToString();

                if (e.Row.RowIndex == 0)
                {
                    imgbtndelete.Visible = false;
                    hfAndOr.Visible = false;
                    lnkAndOr.Visible = false;
                }
            }
        }
        catch
        {
            //
        }
    }

    protected void PopulateCustomColumn1(ref DropDownList ddl)
    {
        ddl.Items.Clear();

        if(CustomColumn1.ToLower()=="mappin")
        {
            ddl.Items.Add(new ListItem("--Please Select--", "Pages/Record/PINImages/DefaultPin.png"));
            ddl.Items.Add(new ListItem("Pin Blue", "Pages/Record/PINImages/PinBlue.png"));
            ddl.Items.Add(new ListItem("Pin Black", "Pages/Record/PINImages/PinBlack.png"));
            ddl.Items.Add(new ListItem("Pin Gray", "Pages/Record/PINImages/PinGray.png"));
            ddl.Items.Add(new ListItem("Pin Green", "Pages/Record/PINImages/PinGreen.png"));
            ddl.Items.Add(new ListItem("Pin Light Blue", "Pages/Record/PINImages/PinLBlue.png"));
            ddl.Items.Add(new ListItem("Pin Orange", "Pages/Record/PINImages/PinOrange.png"));
            ddl.Items.Add(new ListItem("Pin Purple", "Pages/Record/PINImages/PinPurple.png"));
            ddl.Items.Add(new ListItem("Pin Red", "Pages/Record/PINImages/PinRed.png"));
            ddl.Items.Add(new ListItem("Pin Yellow", "Pages/Record/PINImages/PinYellow.png"));
            ddl.Items.Add(new ListItem("Round Blue", "Pages/Record/PINImages/RoundBlue.png"));
            ddl.Items.Add(new ListItem("Round Gray", "Pages/Record/PINImages/RoundGray.png"));
            ddl.Items.Add(new ListItem("Round Green", "Pages/Record/PINImages/RoundGreen.png"));
            ddl.Items.Add(new ListItem("Round Light Blue", "Pages/Record/PINImages/RoundLightBlue.png"));
            ddl.Items.Add(new ListItem("Round Orange", "Pages/Record/PINImages/RoundOrange.png"));
            ddl.Items.Add(new ListItem("Round Purple", "Pages/Record/PINImages/RoundPurple.png"));
            ddl.Items.Add(new ListItem("Round Red", "Pages/Record/PINImages/RoundRed.png"));
            ddl.Items.Add(new ListItem("Round White", "Pages/Record/PINImages/RoundWhite.png"));
            ddl.Items.Add(new ListItem("Round Yellow", "Pages/Record/PINImages/RoundYellow.png"));            

        }
    }


    //public List<FilterEachCondition> FilterList()
    //{
    //    List<FilterEachCondition> AllFilterRow=new List<FilterEachCondition>();

    //    if (grdFilter.Rows.Count > 0)
    //    {
    //        foreach (GridViewRow gvRow in grdFilter.Rows)
    //        {
    //            Label lblID = gvRow.FindControl("lblID") as Label;
    //            HiddenField hfAndOr = gvRow.FindControl("hfAndOr") as HiddenField;
    //            Pages_UserControl_ControlByColumn cbcFilter = gvRow.FindControl("cbcFilter") 
    //                as Pages_UserControl_ControlByColumn;
    //            if (cbcFilter.ddlYAxisV != "")
    //            {
    //                FilterEachCondition tempFilterEachCondition=new FilterEachCondition();
    //                tempFilterEachCondition.AndOr = hfAndOr.Value;
    //                tempFilterEachCondition.TableID = cbcFilter.TableID;
    //                tempFilterEachCondition.ColumnID =int.Parse( cbcFilter.ddlYAxisV);
    //                tempFilterEachCondition.CompareOperator = cbcFilter.CompareOperator;
    //                tempFilterEachCondition.Value = cbcFilter.TextValue;
    //                tempFilterEachCondition.EachTextSearch = cbcFilter.TextSearch;
    //                AllFilterRow.Add(tempFilterEachCondition);
    //            }
    //        }
    //    }


    //    return AllFilterRow;
    //}
    protected string GetXMLText()
    {
        string strXMLText = "<?xml version='1.0' encoding='utf-8' ?><items>";
        bool bFoundFilter = false;
        if (grdFilter.Rows.Count > 0)
        {
            foreach (GridViewRow gvRow in grdFilter.Rows)
            {
                Label lblID = gvRow.FindControl("lblID") as Label;
                //DropDownList ddlTextColour = gvRow.FindControl("ddlTextColour") as DropDownList;
                HiddenField hfAndOr = gvRow.FindControl("hfAndOr") as HiddenField;
                Pages_UserControl_ControlByColumn cbcFilter = gvRow.FindControl("cbcFilter") as Pages_UserControl_ControlByColumn;
                //dtFilter.Rows.Add(lblID.Text.ToString(), cbcFilter.ddlYAxisV, cbcFilter.TextValue, ddlTextColour.SelectedValue);
                string strCustom1 = "";
                if (CustomColumn1 != "")
                {
                    DropDownList ddlCustom1 = gvRow.FindControl("ddlCustom1") as DropDownList;
                    if(ddlCustom1!=null && ddlCustom1.SelectedValue!=null)
                    {
                        strCustom1 = ddlCustom1.SelectedValue;
                    }
                }
                if (cbcFilter.ddlYAxisV != "")
                {
                    bFoundFilter = true;
                    strXMLText = strXMLText
                            + "<item>"
                             + "<tableid>" + HttpUtility.HtmlEncode(cbcFilter.ddlYAxisV) + "</tableid>"
                                + "<andor>" + HttpUtility.HtmlEncode(hfAndOr.Value) + "</andor>"
                                + "<columnid>" + HttpUtility.HtmlEncode(cbcFilter.ddlYAxisV) + "</columnid>"
                                 + "<CompareOperator>" + HttpUtility.HtmlEncode(cbcFilter.CompareOperator) + "</CompareOperator>"
                                + "<value>" + HttpUtility.HtmlEncode(cbcFilter.TextValue) + "</value>"
                                + "<textsearch>" + HttpUtility.HtmlEncode(cbcFilter.TextSearch) + "</textsearch>"
                                + "<custom1>" + HttpUtility.HtmlEncode(strCustom1) + "</custom1>"
                            + "</item>";
                }
            }
        }

        if (bFoundFilter)
        {
            strXMLText += "</items>";
        }
        else
        {
            strXMLText = "";
        }

       
        return strXMLText;

    }
    protected void SetFilterRowData()
    {

        if (ViewState["dtFilter"] != null)
        {
            DataTable dtFilter = (DataTable)ViewState["dtFilter"];
            dtFilter.Rows.Clear();

            int iID = 0;
            if (grdFilter.Rows.Count > 0)
            {
                foreach (GridViewRow gvRow in grdFilter.Rows)
                {

                    string strCustom1 = "";
                    if (CustomColumn1 != "")
                    {
                        DropDownList ddlCustom1 = gvRow.FindControl("ddlCustom1") as DropDownList;
                        if (ddlCustom1 != null && ddlCustom1.SelectedValue != null)
                        {
                            strCustom1 = ddlCustom1.SelectedValue;
                        }
                    }
                    Label lblID = gvRow.FindControl("lblID") as Label;
                    //DropDownList ddlTextColour = gvRow.FindControl("ddlTextColour") as DropDownList;
                    HiddenField hfAndOr = gvRow.FindControl("hfAndOr") as HiddenField;
                    Pages_UserControl_ControlByColumn cbcFilter = gvRow.FindControl("cbcFilter") as Pages_UserControl_ControlByColumn;
                    //dtFilter.Rows.Add(iID, cbcFilter.ddlYAxisV, cbcFilter.TextValue, ddlTextColour.SelectedValue);
                    dtFilter.Rows.Add(lblID.Text.ToString(), hfAndOr.Value, cbcFilter.TableID, cbcFilter.ddlYAxisV, 
                        cbcFilter.CompareOperator, cbcFilter.TextValue, cbcFilter.TextSearch, strCustom1);
                    iID = iID + 1;
                }

            }
            dtFilter.AcceptChanges();
            ViewState["dtFilter"] = dtFilter;


        }
    }
    //protected void PopulateFilterGrid(List<FilterEachCondition> FilterRows)
    //{
    //    if(FilterRows!=null)
    //    {
    //        grdFilter.DataSource = FilterRows;
    //        grdFilter.DataBind();
    //    }

    //}
    public void PopulateFilterGrid(string strXMLText)
    {

        //string strXMLText = "";
        //if (ViewState["XMLText"] != null)
        //    strXMLText = ViewState["XMLText"].ToString();

        if (strXMLText != "")
        {
            string strXML = strXMLText;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(strXML);

            XmlTextReader r = new XmlTextReader(new StringReader(xmlDoc.OuterXml));

            DataSet ds = new DataSet();
            ds.ReadXml(r);


            if (ds.Tables[0] != null)
            {
                bool bCompareOperatorFound = false;
                foreach (DataColumn dc in ds.Tables[0].Columns)
                {
                    if (dc.ColumnName.ToLower() == "CompareOperator".ToLower())
                    {
                        bCompareOperatorFound = true;
                        break;
                    }
                }

                if (bCompareOperatorFound == false)
                {
                    ds.Tables[0].Columns.Add("CompareOperator");
                }
                ds.Tables[0].Columns.Add("ID");

                ds.Tables[0].AcceptChanges();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    dr["ID"] = Guid.NewGuid().ToString();
                }
                ds.Tables[0].AcceptChanges();

                grdFilter.DataSource = ds.Tables[0];
                grdFilter.DataBind();

            }
        }
        else
        {
            if (ViewState["dtFilter"] == null)
            {
                DataTable dtFilter = new DataTable();
                dtFilter.Columns.Add("ID");
                dtFilter.Columns.Add("andor");
                dtFilter.Columns.Add("tableid");
                dtFilter.Columns.Add("columnid");               
                dtFilter.Columns.Add("CompareOperator");
                dtFilter.Columns.Add("value");
                dtFilter.Columns.Add("textsearch");
                dtFilter.Columns.Add("custom1");
                dtFilter.AcceptChanges();

                dtFilter.Rows.Add(Guid.NewGuid().ToString(), "", "", "", "","","","");
                grdFilter.DataSource = dtFilter;
                grdFilter.DataBind();

                ViewState["dtFilter"] = dtFilter;
            }
            else
            {
                DataTable dtFilter = (DataTable)ViewState["dtFilter"];
                grdFilter.DataSource = dtFilter;
                grdFilter.DataBind();
            }


        }
    }


    protected void grdFilter_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            SetFilterRowData();

            if (e.CommandName == "delete_row")
            {
                if (ViewState["dtFilter"] != null)
                {
                    DataTable dtFilter = (DataTable)ViewState["dtFilter"];

                    for (int i = dtFilter.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dtFilter.Rows[i];
                        if (dr["id"].ToString() == e.CommandArgument.ToString())
                        {
                            dr.Delete();
                            break;
                        }
                    }
                    dtFilter.AcceptChanges();
                    ViewState["dtFilter"] = dtFilter;
                    PopulateFilterGrid("");
                }
            }
        }
        catch
        {
            //
        }
    }



    protected void lnkAddItem_Click(object sender, EventArgs e)
    {
        SetFilterRowData();
        if (ViewState["dtFilter"] != null)
        {
            DataTable dtFilter = (DataTable)ViewState["dtFilter"];
            int iPos = dtFilter.Rows.Count;


            DataRow newRow = dtFilter.NewRow();
            newRow[0] = Guid.NewGuid().ToString();
            newRow[1] = "";
            newRow[2] = "";
            newRow[3] = "";
            newRow[4] = "";
            newRow[5] = "";
            newRow[6] = "";
            newRow[7] = "";
            dtFilter.Rows.InsertAt(newRow, iPos);
            dtFilter.AcceptChanges();

            ViewState["dtFilter"] = dtFilter;
            PopulateFilterGrid("");
        }
    }
}
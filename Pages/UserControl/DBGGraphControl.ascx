﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DBGGraphControl.ascx.cs"
    Inherits="DBGGraphControl" %>
<%@ Register Assembly="netchartdir" Namespace="ChartDirector" TagPrefix="chart" %>
<%--<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>--%>
<%--<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Pages/UserControl/ColorPicker.ascx" TagName="CP" TagPrefix="asp" %>

<link href="<%=ResolveUrl("~/fancybox3/dist/jquery.fancybox.min.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/fancybox3/dist/jquery.fancybox.min.js")%>"></script>

<script type="text/javascript" src="<%=ResolveUrl("~/Script/jquery-csv.js")%>"></script>

<script src="<%=ResolveUrl("~/highcharts/highcharts.js")%>"></script>
<script src="<%=ResolveUrl("~/highcharts/highcharts-more.js")%>"></script>
<script src="<%=ResolveUrl("~/highcharts/modules/data.js")%>"></script>
<script src="<%=ResolveUrl("~/highcharts/modules/exporting.js")%>"></script>
<script src="<%=ResolveUrl("~/highcharts/modules/no-data-to-display.js")%>"></script>
<script src="<%=ResolveUrl("~/highcharts/regression.js")%>"></script>

<%--<script src="<%=ResolveUrl("~/highcharts/themes/sand-signika.js")%>"></script>--%>

<style type="text/css">
    .highchartData {
        display: none;
        visibility: hidden;
    }

    #axis_tab {
        border: 1px solid #505050;
        border-right: none;
        background-color: #E7E7E7;
        padding-top: 4px;
        padding-bottom: 4px;
        margin-top: 10px;
        display: inline-block;
    }

    .noneMulti {
        border: none !important;
        margin-left: -2.5% !important;
    }

    .disabledLink {
        cursor: default;
        color: gray;
    }

    .span_yAxis {
        cursor: pointer;
        font-weight: bold;
        color: black;
        border: solid 1px;
        border-left: none;
        padding: 4px;
    }

    .span_yAxis_selected {
        background-color: #FFFFFF;
    }
</style>

<script type="text/javascript">
    var selectedYAxis = -1;
    var isSelectedAxisChange = false;
    var yAxisDictionary = null;
    var seriesName = [];

    //$(window).load(function () {
    //    $(window).keydown(function (event) {
    //        if (event.keyCode == 13) {
    //            event.preventDefault();
    //            return false;
    //        }
    //    });
    //});

    function ProcessData(nAnalyte) {
        console.log("Process Data!!!!")
        var csvArr = [];

        var yAxisDictionaryCurr = yAxisDictionary == null ? null : yAxisDictionary[nAnalyte];
        if (yAxisDictionaryCurr == null && nAnalyte != 0 && selectedYAxis != nAnalyte) {
            return [];
        }
        if (selectedYAxis == -1 || selectedYAxis == nAnalyte || yAxisDictionaryCurr == null) {
            csvArr = $.csv.toArrays($('#CSVData').prop('textContent'));
        }
        else {
            csvArr = $.csv.toArrays(yAxisDictionaryCurr.filter(function (obj) {
                return obj.key == "data";
            })[0].value);
        }
        seriesName = csvArr.splice(0, 1);

        var seriesLength;

        if (csvArr[0] == null) {
            seriesLength = 1;
        }
        else {
            seriesLength = csvArr[0].length - 1;
        }

        var seriesArr = new Array(seriesLength);
        //Parse array here for series.
        for (var x = 0; x < csvArr.length; x++) {
            for (var y = 1; y < csvArr[x].length; y++) {
                var tempArr = [];
                var date = new Date(csvArr[x][0]);
                tempArr.push(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
                tempArr.push(parseFloat(csvArr[x][y]));
                if (seriesArr[y - 1] == null) {
                    seriesArr[y - 1] = [];
                }
                seriesArr[y - 1].push(tempArr);
            }
        }
        return seriesArr;
    }

    function getName(nAnalyte, nData) {
        console.log("getName!!!!")
        var dataName;

        var yAxisDictionaryCurr = yAxisDictionary == null ? null : yAxisDictionary[nAnalyte];
        if (selectedYAxis == -1 || selectedYAxis == nAnalyte || yAxisDictionaryCurr == null) {
            dataName = $('#YAxisTile').prop('textContent');
        }
        else {
            var fieldCurr = yAxisDictionaryCurr.filter(function (obj) {
                return obj.key == "field";
            })[0].value;
            dataName = fieldCurr;
        }
        dataName = dataName + " (" + seriesName[0][nData + 1] + ")";
        return dataName;
    }

    function DisableK() {
        Highcharts.setOptions({
            lang: {
                numericSymbols: null
            }
        });
    }

    function ExecuteMultiChart()
    {
        if (yAxisDictionary == null) {
            yAxisDictionary = new Array(numberOfAnalytes);

            var multiChartData = document.getElementById("hfMultiChartData");
            if (multiChartData.value != "") {
                yAxisDictionary = JSON.parse(multiChartData.value);
            }

            var multiChartSelected = document.getElementById("hfSelectedAxis");
            if (multiChartSelected.value != "") {
                selectedYAxis = multiChartSelected.value;
            }
        }

        RecreateYAxisLinks(numberOfAnalytes);
        var multiChartFlag = document.getElementById("hfMultiChartFlag");
        multiChartFlag.value = "true";
    }

    function ClearMultiChart() {
        yAxisDictionary = null;
    }

    function RecreateYAxisLinks(count) {
        $(".span_yAxis").remove();
        $(".span_yAxis_selected").remove();
        $("#axis_tab").show();
        $(".noneMulti").removeClass("noneMulti");

        if (selectedYAxis != 0 && selectedYAxis != -1) {
            $("#trMultiTable").show();
        }
        else {
            $("#trMultiTable").hide();
        }

        for (var x = 0; x < count; x++) {
            var newSpan = document.createElement("a");
            newSpan.id = "yAxis_" + (x + 1);
            if ((selectedYAxis == -1 ? x == 0 : x == selectedYAxis) || count == 1) {
                if (count == 1) {
                    selectedYAxis = -1;
                }
                newSpan.className = "span_yAxis span_yAxis_selected";
            }
            else {
                newSpan.className = "span_yAxis";
            }
            newSpan.innerHTML = "Y-Axis " + (x + 1);
            newSpan.setAttribute("index", x);
            newSpan.onclick = function () {
                if (!$("#" + this.id).hasClass("span_yAxis_selected")) {
                    SaveYAxisValues($(".span_yAxis_selected").attr("index"));
                    SaveLimits($(".span_yAxis_selected").attr("index"));
                    $(".span_yAxis_selected").attr("class", "span_yAxis");
                    $("#" + this.id).addClass("span_yAxis_selected");
                    LoadYAxisValues(this.getAttribute("index"));
                    LoadLimits(this.getAttribute("index"));
                }
            }
            $("#td_yAxisLink").append(newSpan);
        }
    }

    function SaveLimits(index) {
        //Limits
        var txtWarningMin = document.getElementById("warningLow");
        var txtWarningMax = document.getElementById("warningHigh");
        var txtExceedanceMin = document.getElementById("errorLow");
        var txtExceedanceMax = document.getElementById("errorHigh");
        //======

        yAxisDictionary[index].push(
            { key: "warning-low", value: txtWarningMin.value },
            { key: "warning-high", value: txtWarningMax.value },
            { key: "exceedance-low", value: txtExceedanceMin.value },
            { key: "exceedance-high", value: txtExceedanceMax.value }
        );

        document.getElementById("hfMultiChartData").value = JSON.stringify(yAxisDictionary);
    }


    function SaveYAxisValues(index) {
        var ddlAnalyteField = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_ddlEachAnalyte_Simple");
        var ddlMultiTable = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_ddlMultiTable");
        var txtYAxisMax = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtYHighestValue");
        var txtYAxisMin = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtYLowestValue");
        var txtYAxisInterval = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtYInterval");
        var txtCsvData = document.getElementById("CSVData");

        //For series
        var selectedSeries = [];
        var checkBoxes = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_SampleSitesList").getElementsByTagName("input");
        for (var i = 0; i < checkBoxes.length; i++)
        {
            if (checkBoxes[i].type == "checkbox")
            {
                if (checkBoxes[i].checked)
                {
                    var lblChkBox = checkBoxes[i].nextSibling.innerHTML;
                    selectedSeries.push(lblChkBox);
                }
            }
        }
        //==========
        var tableid;
        if (index == 0) {
            tableid = document.getElementById("hfTableID").value;
        }
        else {
            tableid = ddlMultiTable.options[ddlMultiTable.selectedIndex].value;
        }

        yAxisDictionary[index] = [
            { key: "field", value: ddlAnalyteField.options[ddlAnalyteField.selectedIndex].text },
            { key: "max", value: txtYAxisMax.value },
            { key: "min", value: txtYAxisMin.value },
            { key: "interval", value: txtYAxisInterval.value },
            { key: "data", value: txtCsvData.innerHTML },
            { key: "series", value: selectedSeries },
            { key: "table", value: tableid }
        ];

        document.getElementById("hfMultiChartData").value = JSON.stringify(yAxisDictionary);
    }

    function LoadLimits(index) {
        //Limits
        //var txtWarningMin = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtWarningValueMin_Simple");
        //var txtWarningMax = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtWarningValue_Simple");
        //var txtExceedanceMin = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtExceedanceValueMin_Simple");
        //var txtExceedanceMax = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtExceedanceValue_Simple");

        var hfWarningMin = document.getElementById("warningLow");
        var hfWarningMax = document.getElementById("warningHigh");
        var hfExceedanceMin = document.getElementById("errorLow");
        var hfExceedanceMax = document.getElementById("errorHigh");
        //======

        if (yAxisDictionary[index] != null) {
            //Warning Low
            var bWarningMin = true;
            if (yAxisDictionary[index].filter(function (obj) {
                return obj.key == "warning-low";
            })[0] != null) {
                hfWarningMin.value = yAxisDictionary[index].filter(function (obj) {
                    return obj.key == "warning-low";
                })[0].value;
            }
            else {
                hfWarningMin.value = "";
                bWarningMin = false;
            }

            //Warning High
            var bWarningMax = true;
            if (yAxisDictionary[index].filter(function (obj) {
                return obj.key == "warning-high";
            })[0] != null) {
                hfWarningMax.value = yAxisDictionary[index].filter(function (obj) {
                    return obj.key == "warning-high";
                })[0].value;
            }
            else {
                hfWarningMax.value = "";
                bWarningMax = false;
            }

            //Exceedance Low
            var bExceedanceMin = true;
            if (yAxisDictionary[index].filter(function (obj) {
                return obj.key == "exceedance-low";
            })[0] != null) {
                hfExceedanceMin.value = yAxisDictionary[index].filter(function (obj) {
                    return obj.key == "exceedance-low";
                })[0].value;
            }
            else {
                hfExceedanceMin.value = "";
                bExceedanceMin = false;
            }

            //Exceedance High
            var bExceedanceMax = true;
            if (yAxisDictionary[index].filter(function (obj) {
                return obj.key == "exceedance-high";
            })[0] != null) {
                hfExceedanceMax.value = yAxisDictionary[index].filter(function (obj) {
                    return obj.key == "exceedance-high";
                })[0].value;
            }
            else {
                hfExceedanceMax.value = "";
                bExceedanceMax = false;
            }
        }

        if (bWarningMin || bWarningMax || bExceedanceMin || bExceedanceMax) {
            document.getElementById("hfLimitFlag").value = "true";
        }
    }

    function LoadYAxisValues(index) {
        selectedYAxis = index;

        document.getElementById("hfSelectedAxis").value = selectedYAxis;

        var ddlAnalyteField = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_ddlEachAnalyte_Simple");
        var ddlMultiTable = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_ddlMultiTable");
        var txtYAxisMax = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtYHighestValue");
        var txtYAxisMin = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtYLowestValue");
        var txtYAxisInterval = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtYInterval");
        var refresh = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_lnkRefresh");
        var checkBoxes = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_SampleSitesList").getElementsByTagName("input");

        if (yAxisDictionary[index] != null) {
            //Table
            var fieldVal = yAxisDictionary[index].filter(function (obj) {
                return obj.key == "field";
            })[0].value;
            var selectedSeries = yAxisDictionary[index].filter(function (obj) {
                return obj.key == "series";
            })[0].value;
            var tableid = yAxisDictionary[index].filter(function (obj) {
                return obj.key == "table";
            })[0].value;
            var lastMultiTableSelected = ddlMultiTable.options[ddlMultiTable.selectedIndex].value;
            if (lastMultiTableSelected != tableid) {
                for (var t = 0; t < ddlMultiTable.options.length; t++) {
                    if (ddlMultiTable.options[t].text == tableid || ddlMultiTable.options[t].value == tableid) {
                        ddlMultiTable.selectedIndex = t;
                        break;
                    }
                }

                document.getElementById("hfAnalyte").value = fieldVal;
                document.getElementById("hfSeries").value = selectedSeries.join(",,");

                isSelectedAxisChange = true;
                ddlMultiTable.onchange();
            }
            else {
                //Field
                if (ddlAnalyteField.options[ddlAnalyteField.selectedIndex].text != fieldVal) {
                    for (var x = 0; x < ddlAnalyteField.options.length; x++) {
                        if (ddlAnalyteField.options[x].text == fieldVal || ddlAnalyteField.options[x].value == fieldVal) {
                            ddlAnalyteField.selectedIndex = x;
                            break;
                        }
                    }

                    isSelectedAxisChange = true;
                    ddlAnalyteField.onchange();
                }
                else {
                    refresh.click();
                    isSelectedAxisChange = false;
                }
                //Series
                for (var y = 0; y < checkBoxes.length; y++) {
                    if (selectedSeries != null && selectedSeries.length != 0) {
                        var lblChkBox = checkBoxes[y].nextSibling.innerHTML;
                        if (selectedSeries.indexOf(lblChkBox) != -1) {
                            checkBoxes[y].checked = true;
                        }
                        else {
                            checkBoxes[y].checked = false;
                        }
                    }
                    else {
                        checkBoxes[y].checked = false;
                    }
                }
            }
            //Highest Value
            txtYAxisMax.value = yAxisDictionary[index].filter(function (obj) {
                return obj.key == "max";
            })[0].value;
            //Lowest Value
            txtYAxisMin.value = yAxisDictionary[index].filter(function (obj) {
                return obj.key == "min";
            })[0].value;
            //Interval
            txtYAxisInterval.value = yAxisDictionary[index].filter(function (obj) {
                return obj.key == "interval";
            })[0].value;
            
        }
        else {
            //Set record list series filter if axis is selected for the first time.
            var recordListSeries = document.getElementById("hfRecordListSeries");
            for (var y = 0; y < checkBoxes.length; y++) {
                if (recordListSeries.value != "") {
                    var lblChkBox = checkBoxes[y].nextSibling.innerHTML;
                    if (recordListSeries.value == lblChkBox) {
                        checkBoxes[y].checked = true;
                    }
                    else {
                        checkBoxes[y].checked = false;
                    }
                }
                else {
                    checkBoxes[y].checked = false;
                }
            }
            //Change ddlAnalyteField selected value if already selected by other axis.
            var isFieldSelected;
            for (var z = 0; z < ddlAnalyteField.options.length; z++) {
                var ddlAnalyteValue = ddlAnalyteField.options[z].text;
                for (var c = 0; c < index; c++) {
                    if (yAxisDictionary[c] != null) {
                        isFieldSelected = yAxisDictionary[c].filter(function (obj) {
                            return obj.value == ddlAnalyteValue;
                        })[0];
                        if (isFieldSelected != null) {
                            break;
                        }
                    }
                }
                if (isFieldSelected == null) {
                    ddlAnalyteField.options[z].selected = true;
                    SaveYAxisValues(index);
                    ddlAnalyteField.onchange();
                    break;
                }
            }
        }
    }

    function changeSubTitle() {
        var multiChartFlag = document.getElementById("hfMultiChartFlag");
        if (multiChartFlag.value == "true" && selectedYAxis != -1) {
            //Subtitle
            var ddlAnalyteField = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_ddlEachAnalyte_Simple");
            var sSubtitle = "";
            for (var i = 0; i < yAxisDictionary.length; i++)
            {
                if (selectedYAxis == i) {
                    sSubtitle = sSubtitle + "--" + ddlAnalyteField.options[ddlAnalyteField.selectedIndex].text;
                }
                else {
                    if (yAxisDictionary[i] != null) {
                        var tempField = yAxisDictionary[i].filter(function (obj) {
                            return obj.key == "field";
                        })[0].value;
                        sSubtitle = sSubtitle + "--" + tempField;
                    }
                }
            }
            document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtGraphSubtitle_Simple").value = sSubtitle.substring(2);

            //Clear max, min and interval values if analyte is changed but no change in axis.
            if (!isSelectedAxisChange) {
                var txtYAxisMax = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtYHighestValue");
                var txtYAxisMin = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtYLowestValue");
                var txtYAxisInterval = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtYInterval");

                txtYAxisMax.value = "";
                txtYAxisMin.value = "";
                txtYAxisInterval.value = "";
            }

            isSelectedAxisChange = false;
        }
    }

    function ChangeTimePeriod(dtvalue) {

        document.getElementById("hfDateTimeValue").value = dtvalue;

        if (dtvalue != '') {
            document.getElementById('btnRefreshChart').click();
        }
    }

    function CheckFromTime(sender, args) {
        var compare = RegExp("^([0-1]?[0-9]|2[0-3]):([0-5][0-9])(:[0-5][0-9])?$");
        args.IsValid = compare.test(args.Value);
        return;
    }

    function CheckToTime(sender, args) {
        var compare = RegExp("^([0-1]?[0-9]|2[0-3]):([0-5][0-9])(:[0-5][0-9])?$");
        args.IsValid = compare.test(args.Value);
        return;
    }

    function AddNewGraphOptionDetail() {

        //document.getElementById('btnAddNewDetail').click();
        $("#hlAddNewDetail").trigger('click');
    }

    function FolderOpenClick() {

        $('#hlFolderOpen').trigger('click');
    }

    function OnWordWrap_CheckedChanged() {
        if ($('#chkWordWrap').is(':checked')) {
            $('textarea').css('white-space', '').css('word-wrap', '').css('overflow-x', '');
        }
        else {
            $('textarea').css('white-space', 'pre').css('word-wrap', 'normal').css('overflow-x', 'scroll');
        }
    }

    function OnlbHCSEdit_OK_click() {
        if ($('[id*=editableHCS]').val().length === 0) {
            alert("Script is required");
            return false;
        }
        $('[id*=editableHCS]').val(
            $('[id*=editableHCS]').val().
            replace(new RegExp('<', 'g'), '&lt;').
            replace(new RegExp('>', 'g'), '&gt;'));
        $.fancybox.close();
        return true;
    }

    var oldScript;

    function OnlnkHcscript_click() {
        oldScript = $('[id*=editableHCS]').val();
        $('[id*=editableHCS]').val(
            $('[id*=editableHCS]').val().
            replace(new RegExp('&lt;', 'g'), '<').
            replace(new RegExp('&gt;', 'g'), '>'));
    }

    function OnlbHCSEdit_Cancel_click() {
        $('[id*=editableHCS]').val(oldScript);
        $.fancybox.close();
    }

    function SendGraphEmail() {
        document.getElementById("hfGraphImageURL").value = '';
        var chart = $('#WebChartViewer1').highcharts();
        var imageURL = '';
        var svg = chart.getSVG();
        var txtGraphTitle = document.getElementById("ctl00_HomeContentPlaceHolder_gcTest_txtGraphTitle_Simple");
        var dataString = 'type=image/png&filename=' + encodeURIComponent(txtGraphTitle.value) + '&width=500&svg=' + svg;
        $.ajax({
            type: 'POST',
            data: dataString,
            url: 'ExportGraph.ashx',
            async: false,
            success: function (data) {
                document.getElementById("hfGraphImageURL").value = data;
                document.getElementById('btnSendEmail').click();
            }
        });

       

    }
</script>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<%--<asp:UpdateProgress class="ajax-indicator" ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
    <ProgressTemplate>
        <div style="position: absolute; visibility: visible; border: none; z-index: 100; width: 100%; height: 100%; background: #999; filter: alpha(opacity=80); -moz-opacity: .8; opacity: .8;">
            <asp:Image Style="top: 48%; left: 42%; position: relative;" runat="server" ID="imgAjax"
                ImageUrl="~/Images/ajax.gif" AlternateText="Processing..." />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>--%>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Panel ID="Panel1" runat="server" DefaultButton="lnkRefresh">
          

            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
                       <div runat="server" if="divGraph">
                            <table>
                                <tr>
                                    <td colspan="3">
                                        <table>
                                            <tr>
                                                <td>
                                                    <%--<chart:WebChartViewer ID="WebChartViewer1" runat="server" />--%>
                                                    <div id="WebChartViewer1" clientidmode="Static" runat="server" style="display: block;"></div>

                                                    <asp:HiddenField ID="chartHeight" ClientIDMode="Static" runat="server" />
                                                    <asp:HiddenField ID="chartWidth" ClientIDMode="Static" runat="server" />

                                                    <asp:Label ID="CSVData" ClientIDMode="Static" runat="server" CssClass="highchartData" />

                                                    <asp:Label ID="chartTitle" ClientIDMode="Static" runat="server" CssClass="highchartData" />
                                                    <asp:Label ID="chartSubtitle" ClientIDMode="Static" runat="server" CssClass="highchartData" />
                                                    <asp:Label ID="YAxisTile" ClientIDMode="Static" runat="server" CssClass="highchartData" />

                                                    <asp:HiddenField ID="YAxisMin" ClientIDMode="Static" runat="server" />
                                                    <asp:HiddenField ID="YAxisMax" ClientIDMode="Static" runat="server" />
                                                    <asp:HiddenField ID="YAxisInterval" ClientIDMode="Static" runat="server" />

                                                    <asp:HiddenField ID="showWarnings" ClientIDMode="Static" runat="server" />
                                                    <asp:HiddenField ID="warningHigh" ClientIDMode="Static" runat="server" />
                                                    <asp:HiddenField ID="warningLow" ClientIDMode="Static" runat="server" />
                                                    <asp:HiddenField ID="warningHighColor" ClientIDMode="Static" runat="server" />
                                                    <asp:HiddenField ID="warningHighCaption" ClientIDMode="Static" runat="server" />

                                                    <asp:HiddenField ID="showErrors" ClientIDMode="Static" runat="server" />
                                                    <asp:HiddenField ID="errorHigh" ClientIDMode="Static" runat="server" />
                                                    <asp:HiddenField ID="errorLow" ClientIDMode="Static" runat="server" />
                                                    <asp:HiddenField ID="errorHighColor" ClientIDMode="Static" runat="server" />
                                                    <asp:HiddenField ID="errorHighCaption" ClientIDMode="Static" runat="server" />

                                                    <asp:HiddenField ID="showTrendline" ClientIDMode="Static" runat="server" />

                                                    <asp:HiddenField ID="hfMultiChartFlag" ClientIDMode="Static" runat="server" Value="false" />
                                                    <asp:HiddenField ID="hfMultiChartData" ClientIDMode="Static" runat="server" Value="" />
                                                    <asp:HiddenField ID="hfSelectedAxis" ClientIDMode="Static" runat="server" Value="" />
                                                    <asp:HiddenField ID="hfLimitFlag" ClientIDMode="Static" runat="server" Value="false" />
                                                    <asp:HiddenField ID="hfTableID" ClientIDMode="Static" runat="server" />
                                                    <asp:HiddenField ID="hfAnalyte" ClientIDMode="Static" runat="server" />
                                                    <asp:HiddenField ID="hfSeries" ClientIDMode="Static" runat="server" />

                                                    <%--<asp:HiddenField ID="hfSeriesSymbols" ClientIDMode="Static" runat="server" />--%>
                                                    <asp:HiddenField ID="hfSeriesColors" ClientIDMode="Static" runat="server" />
                                                </td>
                                                <td valign="top">
                                                    <asp:LinkButton runat="server" ID="lnkZoom" OnClick="lnkZoom_Click" CausesValidation="false">
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/App_Themes/Default/Images/zoom.png" />
                                                    </asp:LinkButton>
                                                    <asp:LinkButton runat="server" ID="lnkCloseZoom" OnClick="lnkCloseZoom_Click" Visible="false"
                                                        CausesValidation="false">
                                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/App_Themes/Default/Images/zoomclose.png" />
                                                    </asp:LinkButton>
                                                    <asp:HyperLink runat="server" ID="hlOpenGraphLink" Target="_blank" Visible="false" style="left:-100px;">
                                                         <asp:Image ID="imgOpenGraphLink" runat="server" ImageUrl="~/Images/openlink.png" />
                                                    </asp:HyperLink>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td></td>
                                </tr>

                                <tr runat="server" id="tdClickHelp" visible="false">
                                    <td colspan="4" align="center">Click on a data point to drill-down into that date
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                            <asp:DropDownList runat="server" ID="ddlGraphOption" AutoPostBack="true" DataTextField="TableName" Visible="false"
                                Width="180px" DataValueField="TableID" CssClass="NormalTextBox" OnSelectedIndexChanged="ddlGrpahOption_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:HiddenField runat="server" ID="hfDateTimeValue" ClientIDMode="Static" Value="" />
                            <asp:Button runat="server" ID="btnRefreshChart" ClientIDMode="Static" Style="display: none;"
                                OnClick="btnRefreshChart_Click" CausesValidation="false" />
                            <asp:Button runat="server" ID="btnRefreshChartPop" ClientIDMode="Static" Style="display: none;"
                                OnClick="btnRefreshChartPop_Click" />
                            <asp:HiddenField runat="server" ID="hfDetailSearchID" ClientIDMode="Static" Value="" />
                            <asp:HyperLink ID="hlAddNewDetail" ClientIDMode="Static" runat="server" NavigateUrl="~/Pages/UserControl/GraphOptionDetail.aspx"
                                CssClass="popuplink" Style="display: none;"></asp:HyperLink>
                        </div>
            </div>
              
             <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
                      <div runat="server" id="divOptionControls" class="col-xs-12 col-sm-8 col-md-8 col-lg-12 grp-controls">
                            <table cellpadding="3" cellspacing="3" border="0">
                                <tr runat="server" id="trMainTitle">
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="grp-buttons">
                                        <table>
                                            <tr>
                                                <td>
                                                    <div runat="server" id="divBack">
                                                        <asp:HyperLink runat="server" ID="hlBack" CssClass="ButtonLink" CausesValidation="false">
                                                            <asp:Image runat="server" ID="imgBack" ImageUrl="~/App_Themes/Default/images/Back.png"
                                                                ToolTip="Back" />
                                                        </asp:HyperLink>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div runat="server" id="divRemoveFilter" visible="false">
                                                        <asp:ImageButton runat="server" ID="ibRemoveFilter" ImageUrl="~/App_Themes/Default/Images/filter_delete.png"
                                                            OnClick="ibRemoveFilter_Click" ToolTip="Remove Filter"/>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div runat="server" id="div1">
                                                        <asp:ImageButton runat="server" ID="lnkExcel" ImageUrl="~/App_Themes/Default/images/chart2xl.png"
                                                            OnClick="ibExcel_Click" ToolTip="Export to Excel" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <div runat="server" id="divRefresh">

                                                        <asp:ImageButton runat="server" ID="lnkRefresh" ImageUrl="~/App_Themes/Default/images/Refresh2.png"
                                                            OnClick="lnkRefresh_Click" ToolTip="Refresh" CausesValidation="false" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <div runat="server" id="divEmail" visible="false">


                                                        <asp:ImageButton runat="server" ID="ibEmail" ImageUrl="~/App_Themes/Default/images/email.png"
                                                            OnClientClick="SendGraphEmail();return false;" ToolTip="Email" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <div runat="server" id="divGraphsave">
                                                          <asp:HyperLink runat="server" ID="hlGraphsave" class="popupaddgraph" ClientIDMode="Static">
                                                            <asp:Image runat="server" ImageUrl="~/App_Themes/Default/images/graph_export.png" ToolTip="Add Graph" />
                                                        </asp:HyperLink>
                                                         <%--<asp:ImageButton runat="server" ID="ibAddGraph" ImageUrl="~/App_Themes/Default/images/graphsave.png"
                                                            OnClick="ibAddGraph_Click" ToolTip="Add Graph" CausesValidation="true" ValidationGroup="main" />--%>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div runat="server" id="divSave">

                                                        <asp:ImageButton runat="server" ID="lnkSave" ImageUrl="~/App_Themes/Default/images/Save.png"
                                                            OnClick="lnkSave_Click" ToolTip="Save" CausesValidation="true" ValidationGroup="main" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <div runat="server" id="divFolderOpen">
                                                        <asp:HyperLink runat="server" ID="hlFolderOpen" class="popupgraph" ClientIDMode="Static">
                                                            <asp:Image runat="server" ImageUrl="~/App_Themes/Default/images/FolderOpen.png" ToolTip="Open Graph" />
                                                        </asp:HyperLink>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:MultiView runat="server" ID="divMain" ActiveViewIndex="0">
                                            <asp:View runat="server" ID="viewBasic">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td align="right">
                                                            <strong>Graph&nbsp;Title</strong>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="txtGraphTitle_Simple" CssClass="NormalTextBox" Width="300px"
                                                                ValidationGroup="main"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtGraphTitle_Simple"
                                                                ErrorMessage="Graph Title is required" Display="Dynamic" ValidationGroup="main"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <strong>Graph&nbsp;Subtitle</strong>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox runat="server" ID="txtGraphSubtitle_Simple" CssClass="NormalTextBox" Width="300px">
                                                            </asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:right;">
                                                            <strong>Graph&nbsp;Type</strong>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList runat="server" ID="ddlGraphType_Simple" CssClass="NormalTextBox" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlGraphType_Simple_SelectedIndexChanged"
                                                                Width="240px">
                                                            </asp:DropDownList>
                                                            <%--<a id="lnkHcscript" href="#hcscript" onclick="return OnlnkHcscript_click();">
                                                                <asp:ImageButton runat="server" ID="ibEdit"
                                                                    ImageUrl="~/App_Themes/Default/images/iconEdit.png"
                                                                    ToolTip="Edit" Height="15px" Style="position: relative; top: 3px" />
                                                            </a>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align:right;">
                                                            <strong>Field</strong></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlEachAnalyte_Simple" runat="server" AutoPostBack="true" CssClass="NormalTextBox" onchange="changeSubTitle()" OnSelectedIndexChanged="ddlEachAnalyte_Simple_SelectedIndexChanged" style="margin-bottom: 0px" Width="300px">
                                                                <asp:ListItem Text="--None--" Value="-1"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr style="vertical-align:top;">
                                                        <td style="text-align:right;">
                                                            <asp:Label runat="server" ID="lblTimePeriodDisplay_Simple" Font-Bold="true" Text="Plot"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:DropDownList runat="server" ID="ddlTimePeriodDisplay_Simple" AutoPostBack="true" CssClass="NormalTextBox"
                                                                            OnSelectedIndexChanged="ddlTimePeriodDisplay_Simple_SelectedIndexChanged">
                                                                            <asp:ListItem Text="Date Range" Value="C" Selected="True"></asp:ListItem>
                                                                            <asp:ListItem Text="One Year" Value="Y"></asp:ListItem>
                                                                            <asp:ListItem Text="One Month" Value="M"></asp:ListItem>
                                                                            <asp:ListItem Text="One Week" Value="W"></asp:ListItem>
                                                                            <asp:ListItem Text="One Day" Value="D"></asp:ListItem>
                                                                            <asp:ListItem Text="One Hour" Value="H"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:DropDownList runat="server" ID="ddlCount" Visible="false"  CssClass="NormalTextBox">
                                                                            <asp:ListItem Text="Count" Value="count"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="padding-left: 10px;">
                                                                        <asp:LinkButton Visible="false" runat="server" ID="lnkTPPrev2_Simple" Text="Prev" OnClick="lnkTPPrev2_Click"
                                                                            CausesValidation="false"></asp:LinkButton>
                                                                        <asp:Label Visible="false" runat="server" ID="lblTPPrev2_Simple" CssClass="disabledLink" Text="Prev" />
                                                                    </td>
                                                                    <td style="padding-left: 10px;">
                                                                        <asp:LinkButton Visible="false" runat="server" ID="lnkTPNext2_Simple" Text="Next" OnClick="lnkTPNext2_Click"
                                                                            CausesValidation="false"></asp:LinkButton>
                                                                        <asp:Label Visible="false" runat="server" ID="lblTPNext2_Simple" CssClass="disabledLink" Text="Next" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trStartDate_Simple">
                                                        <td style="text-align:right;">
                                                            <strong>From</strong>
                                                        </td>
                                                        <td style="text-align:left;">
                                                            <asp:TextBox ID="txtStartDate_Simple" runat="server" CssClass="NormalTextBox" ClientIDMode="Static"
                                                                Width="100px" BorderStyle="Solid" BorderColor="#909090" BorderWidth="1">
                                                            </asp:TextBox>
                                                            <asp:ImageButton runat="server" ID="ibStartDate_Simple" ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false" />
                                                            <asp:TextBox runat="server" ID="txtFromTime_Simple" ClientIDMode="Static" Style="width: 80px"
                                                                CssClass="NormalTextBox"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trEndDate_Simple">
                                                        <td style="text-align:right; vertical-align:top">
                                                            <strong>To</strong>
                                                        </td>
                                                        <td style="text-align:left;">
                                                            <asp:TextBox ID="txtEndDate_Simple" runat="server" CssClass="NormalTextBox" ClientIDMode="Static"
                                                                Width="100px" BorderStyle="Solid" BorderColor="#909090" BorderWidth="1">
                                                            </asp:TextBox>
                                                            <asp:ImageButton runat="server" ID="ibEndDate_Simple" ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false" />
                                                            <asp:TextBox runat="server" ID="txtToTime_Simple" Style="width: 80px" ClientIDMode="Static"
                                                                CssClass="NormalTextBox"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td style="text-align:right;">&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr runat="server" id="trddlSeriesColumns">
                                                        <td style="text-align:right;"><strong>Series</strong></td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSeriesColumns" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSeriesColumns_SelectedIndexChanged" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                            </asp:DropDownList>
                                                        </td>

                                                    </tr>
                                                    <tr runat="server" id="trHighestLowest">
                                                        <td colspan=2>
                                                            <div id="axis_tab" style="display: none">
                                                                <div id="td_yAxisLink"></div>
                                                            </div>
                                                            <div class="noneMulti" style="border: solid 1px">
                                                                <div id="trMultiTable" style="margin-top: 10px; margin-bottom: -5px; margin-left: 14.4%; display: none">
                                                                    <strong>Table</strong>
                                                                    <asp:DropDownList runat="server" ID="ddlMultiTable" AutoPostBack="true" DataTextField="TableName" Width="180px" 
                                                                        DataValueField="TableID" CssClass="NormalTextBox" OnSelectedIndexChanged="ddlMultiTable_SelectedIndexChanged" onchange="changeSubTitle()">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div runat="server" id="trSeries" style="margin-top: 5px; margin-left: 77px; padding-right: 5px">
                                                                    <strong style="float: left; padding-right: 3px">Data</strong>
                                                                    <asp:CheckBoxList runat="server" ID="SampleSitesList" AutoPostBack="false" OnSelectedIndexChanged="SampleSitesList_SelectedIndexChanged"
                                                                        Style="display: block; overflow: auto; min-width: 300px; max-width: 300px; height: 140px; border: solid 1px #909090;">
                                                                    </asp:CheckBoxList>
                                                                    <asp:HiddenField ID="hfRecordListSeries" ClientIDMode="Static" runat="server" />
                                                                    
                                                                    <div style="padding-left:35px;">
                                                                        <asp:LinkButton runat="server" ID="LinkButton1" OnClick="lnkTickAllExport_Click">Tick All</asp:LinkButton>
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:LinkButton runat="server" ID="lnkUntickAllExport" OnClick="lnkUntickAllExport_Click">Untick All</asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                                <div runat="server" id="trHighestVal" style="margin-top: 5px; margin-left: 14px">
                                                                    <asp:Label runat="server" ID="Label2" Font-Bold="true" Text="Highest Value"></asp:Label>
                                                                    <asp:TextBox ID="txtYHighestValue" runat="server" CssClass="NormalTextBox"
                                                                        Width="70px" BorderStyle="Solid" BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                                                                </div>
                                                                <div runat="server" id="trLowestVal" style="margin-top: 5px; margin-left: 17px">
                                                                    <asp:Label runat="server" ID="Label3" Font-Bold="true" Text="Lowest Value"></asp:Label>
                                                                    <asp:TextBox ID="txtYLowestValue" runat="server" CssClass="NormalTextBox"
                                                                        Width="70px" BorderStyle="Solid" BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                                                                </div>
                                                                <div runat="server" id="trInterval" style="margin-top: 5px; margin-left: 52px; margin-bottom: 5px">
                                                                    <asp:Label runat="server" ID="Label4" Font-Bold="true" Text="Interval"></asp:Label>
                                                                    <asp:TextBox ID="txtYInterval" runat="server" CssClass="NormalTextBox"
                                                                        Width="70px" BorderStyle="Solid" BorderColor="#909090" BorderWidth="1"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr runat="server" id="trTrendline">
                                                        <td align="right">
                                                            <strong>Add Trendline</strong>
                                                        </td>
                                                        <td valign="top" style="vertical-align: top">
                                                            <asp:CheckBox runat="server" ID="chkAddTrendline" Font-bold="true" AutoPostBack="true" 
                                                                OnCheckedChanged="chkAddTrendline_CheckedChanged"/>
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trLimits">
                                                        <td align="right">
                                                            <strong>Limits</strong>
                                                        </td>
                                                        <td valign="top" style="vertical-align: top;">
                                                            <asp:CheckBox runat="server" ID="chkShowLimits_Simple" Text="" TextAlign="Right" Font-Bold="true"
                                                                AutoPostBack="true" OnCheckedChanged="chkShowLimits_Simple_CheckedChanged" />
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trShowLimitsOnGraphOne_Simple" visible="true">
                                                        <td align="left" colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td style="width: 35px;">&nbsp;</td>
                                                                    <td align="left">
                                                                        <table>
                                                                            <tr runat="server" id="trLimit_Label" visible="false">
                                                                                <td></td>
                                                                                <td>Lower</td>
                                                                                <td>Upper</td>
                                                                                <td></td>
                                                                            </tr>
                                                                            <tr runat="server" id="trWarning_Simple" visible="false">
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtWarningCaption_Simple" Width="150px" Text="Warning"></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtWarningValueMin_Simple" Width="55px" OnTextChanged="txtWarningValueMin_Simple_TextChanged"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtWarningValueMin_Simple"
                                                                                        ErrorMessage="*" MaximumValue="1000000" MinimumValue="-1000000" Text="*" Type="Double" Display="Dynamic"></asp:RangeValidator>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtWarningValue_Simple" Width="55px" OnTextChanged="txtWarningValue_Simple_TextChanged"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="txtWarningValue_Simple"
                                                                                        ErrorMessage="*" MaximumValue="1000000" MinimumValue="-1000000" Text="*" Type="Double" Display="Dynamic"></asp:RangeValidator>
                                                                                </td>
                                                                                <td>
                                                                                    <select style="width: 110px" id="ddlWarningColor_Simple" class="NormalTextBox" runat="server">
                                                                                        <option style="background-color: Aqua;" value="Aqua">Aqua</option>
                                                                                        <option style="background-color: Black; color: White;" value="Black">Black</option>
                                                                                        <option style="background-color: Blue;" value="Blue" selected="selected">Blue</option>
                                                                                        <option style="background-color: Fuchsia;" value="Fuchsia">Fuchsia</option>
                                                                                        <option style="background-color: Gray;" value="Gray">Gray</option>
                                                                                        <option style="background-color: Green;" value="Green">Green</option>
                                                                                        <option style="background-color: Lime;" value="Lime">Lime</option>
                                                                                        <option style="background-color: Maroon;" value="Maroon">Maroon</option>
                                                                                        <option style="background-color: Navy; color: White;" value="Navy">Navy</option>
                                                                                        <option style="background-color: Olive;" value="Olive">Olive</option>
                                                                                        <option style="background-color: Orange;" value="Orange">Orange</option>
                                                                                        <option style="background-color: Purple;" value="Purple">Purple</option>
                                                                                        <option style="background-color: Red;" value="Red">Red</option>
                                                                                        <option style="background-color: Silver;" value="Silver">Silver</option>
                                                                                        <option style="background-color: Teal;" value="Teal">Teal</option>
                                                                                        <option style="background-color: Yellow;" value="Yellow">Yellow</option>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>
                                                                            <tr runat="server" id="trExceedance_Simple" visible="false">
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtExceedanceCaption_Simple" Width="150px" Text="Exceedance"></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtExceedanceValueMin_Simple" Width="55px" OnTextChanged="txtExceedanceValueMin_Simple_TextChanged"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtExceedanceValueMin_Simple"
                                                                                        ErrorMessage="*" MaximumValue="1000000" MinimumValue="-1000000" Text="*" Type="Double" Display="Dynamic"></asp:RangeValidator>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox runat="server" ID="txtExceedanceValue_Simple" Width="55px" OnTextChanged="txtExceedanceValue_Simple_TextChanged"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtExceedanceValue_Simple"
                                                                                        ErrorMessage="*" MaximumValue="1000000" MinimumValue="-1000000" Text="*" Type="Double" Display="Dynamic"></asp:RangeValidator>
                                                                                </td>
                                                                                <td>
                                                                                    <select style="width: 110px" id="ddlExceedanceColor_Simple" class="NormalTextBox" runat="server">
                                                                                        <option style="background-color: Aqua;" value="Aqua">Aqua</option>
                                                                                        <option style="background-color: Black; color: White;" value="Black">Black</option>
                                                                                        <option style="background-color: Blue;" value="Blue">Blue</option>
                                                                                        <option style="background-color: Fuchsia;" value="Fuchsia">Fuchsia</option>
                                                                                        <option style="background-color: Gray;" value="Gray">Gray</option>
                                                                                        <option style="background-color: Green;" value="Green">Green</option>
                                                                                        <option style="background-color: Lime;" value="Lime">Lime</option>
                                                                                        <option style="background-color: Maroon;" value="Maroon">Maroon</option>
                                                                                        <option style="background-color: Navy; color: White;" value="Navy">Navy</option>
                                                                                        <option style="background-color: Olive;" value="Olive">Olive</option>
                                                                                        <option style="background-color: Orange;" value="Orange">Orange</option>
                                                                                        <option style="background-color: Purple;" value="Purple">Purple</option>
                                                                                        <option style="background-color: Red;" value="Red" selected="selected">Red</option>
                                                                                        <option style="background-color: Silver;" value="Silver">Silver</option>
                                                                                        <option style="background-color: Teal;" value="Teal">Teal</option>
                                                                                        <option style="background-color: Yellow;" value="Yellow">Yellow</option>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>
                                                            <%-- Date From --%>
                                                            <%--                                                            <asp:RequiredFieldValidator ID="StartDateRequired_Simple" runat="server" ControlToValidate="txtStartDate_Simple"
                                                                CssClass="failureNotification" ErrorMessage="Start date is required." ToolTip="Start date is required.">*</asp:RequiredFieldValidator>--%>
                                                            <asp:RangeValidator ID="rngDateFrom_Simple" runat="server" ControlToValidate="txtStartDate_Simple"
                                                                CssClass="failureNotification" ErrorMessage="*" ToolTip="From date is not ok." Display="Dynamic"
                                                                Type="Date" MinimumValue="1/1/1753" MaximumValue="1/1/3000"></asp:RangeValidator>
                                                            <ajaxToolkit:TextBoxWatermarkExtender ID="tbwStartDate_Simple" TargetControlID="txtStartDate_Simple"
                                                                WatermarkText="dd/mm/yyyy" runat="server" WatermarkCssClass="MaskText">
                                                            </ajaxToolkit:TextBoxWatermarkExtender>
                                                            <ajaxToolkit:CalendarExtender ID="ce_txtDateFrom_Simple" runat="server" TargetControlID="txtStartDate_Simple"
                                                                Format="dd/MM/yyyy" PopupButtonID="ibStartDate_Simple" FirstDayOfWeek="Monday">
                                                            </ajaxToolkit:CalendarExtender>
                                                            <%-- Time From --%>
                                                            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtFromTime_Simple"
                                                                AutoCompleteValue="00:00" Mask="99:99" AcceptAMPM="false" runat="server" MaskType="Time">
                                                            </ajaxToolkit:MaskedEditExtender>
                                                            <asp:CustomValidator runat="server" ID="cvTest_Simple" ControlToValidate="txtFromTime_Simple"
                                                                ClientValidationFunction="CheckFromTime"
                                                                CssClass="failureNotification" ErrorMessage="hh:mm format (24 hrs) please!"></asp:CustomValidator>
                                                            <%-- Date To --%>
                                                            <%--                                                            <asp:RequiredFieldValidator ID="EndDateRequired_Simple" runat="server" ControlToValidate="txtEndDate_Simple"
                                                                CssClass="failureNotification" ErrorMessage="End date is required." ToolTip="End date is required.">*</asp:RequiredFieldValidator>--%>
                                                            <asp:RangeValidator ID="rngDateTo_Simple" runat="server" ControlToValidate="txtEndDate_Simple"
                                                                CssClass="failureNotification" ErrorMessage="*" ToolTip="To date is not ok." Display="Dynamic"
                                                                Type="Date" MinimumValue="1/1/1753" MaximumValue="1/1/3000"></asp:RangeValidator>
                                                            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" TargetControlID="txtEndDate_Simple"
                                                                WatermarkText="dd/mm/yyyy" runat="server" WatermarkCssClass="MaskText">
                                                            </ajaxToolkit:TextBoxWatermarkExtender>
                                                            <ajaxToolkit:CalendarExtender ID="ce_txtDateTo_Simple" runat="server" TargetControlID="txtEndDate_Simple"
                                                                Format="dd/MM/yyyy" PopupButtonID="ibEndDate_Simple" FirstDayOfWeek="Monday">
                                                            </ajaxToolkit:CalendarExtender>
                                                            <%-- Time To --%>
                                                            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3" TargetControlID="txtToTime_Simple"
                                                                AutoCompleteValue="00:00" Mask="99:99" AcceptAMPM="false" runat="server" MaskType="Time">
                                                            </ajaxToolkit:MaskedEditExtender>
                                                            <asp:CustomValidator runat="server" ID="CustomValidator3" ControlToValidate="txtToTime_Simple"
                                                                ClientValidationFunction="CheckToTime"
                                                                CssClass="failureNotification" ErrorMessage="hh:mm format (24 hrs) please!"></asp:CustomValidator>
                                                            <%-- Compare Dates --%>
                                                            <asp:CompareValidator ID="EndDateCompare_Simple" runat="server" Type="Date" ControlToValidate="txtEndDate_Simple"
                                                                ControlToCompare="txtStartDate_Simple" Operator="GreaterThanEqual" ToolTip="End date must be after the start date"
                                                                ErrorMessage="End date must be after the start date" CssClass="failureNotification">*</asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </asp:View>
                                            <asp:View runat="server" ID="viewAdvanced">
                                            </asp:View>
                                            <asp:View runat="server" ID="viewDashboard">
                                                <div style="text-align: center;">
                                                    <br />
                                                    <br />
                                                    <br />


                                                    <table>
                                                        <tr id="trSavedGraph" runat="server" visible="false">
                                                            <td>
                                                                <strong>Saved Graph:</strong>
                                                            </td>
                                                            <td>
                                                                <asp:Label runat="server" ID="lblSavedGraphName"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr style="height: 100px;">
                                                            <td colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox runat="server" ID="chkDateRange" Font-Bold="true" Text="Date Range" TextAlign="Right" />
                                                            </td>
                                                            <td>
                                                                <br />
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:TextBox runat="server" CssClass="NormalTextBox" Width="50px" ID="txtRecentNumber"></asp:TextBox>
                                                                            <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtRecentNumber"
                                                                                ErrorMessage="Numeric only!" MaximumValue="1000000" MinimumValue="0" Type="Double"
                                                                                Display="Dynamic" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="ddlRecentPeriod" CssClass="NormalTextBox">

                                                                                <asp:ListItem Text="Years" Value="Y" Selected="True"></asp:ListItem>
                                                                                <asp:ListItem Text="Months" Value="M"></asp:ListItem>
                                                                                <asp:ListItem Text="Weeks" Value="W"></asp:ListItem>
                                                                                <asp:ListItem Text="Days" Value="D"></asp:ListItem>
                                                                                <asp:ListItem Text="Hours" Value="H"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </div>
                                            </asp:View>
                                        </asp:MultiView>
                                    </td>
                                </tr>
                            </table>
                        </div>
             </div>
             <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                   <asp:HiddenField runat="server" ID="hfSelectedGraphs" ClientIDMode="Static" />
                        <asp:Button runat="server" ID="btnUpdateGraphs" ClientIDMode="Static" OnClick="btnUpdateGraphs_Click"
                            Style="display: none;" />
                        <asp:Button runat="server" ID="HiddenButtonRefresh" ClientIDMode="Static"
                            OnClick="HiddenButtonRefresh_Click"
                            Style="display: none;" />
                        <asp:HiddenField runat="server" ID="hfGraphImageURL" ClientIDMode="Static" />
                        <asp:Button runat="server" ID="btnSendEmail" ClientIDMode="Static" OnClick="btnSendEmail_Click"
                            Style="display: none;" />
            </div>
            <div style="display: none">
                <div id="hcscript"
                    style="width: 500px; height: 550px; text-align: center;">
                    <div style="position: absolute; top: 15px; left: 25px; font-size: 0.8em;" class="ContentMain">
                        <label for="chkWordWrap">Word Wrap</label>
                        <input id="chkWordWrap" type="checkbox" checked="checked" onchange="OnWordWrap_CheckedChanged();" />
                    </div>
                    <textarea runat="server" id="editableHCS"
                        style="position: absolute; top: 40px; left: 25px; height: 450px; width: 450px;">
                    </textarea>
                    <br />
                    <div style="position: absolute; bottom: 10px; right: 25px;">
                        <asp:LinkButton runat="server" ID="lbHCSEdit_OK" CssClass="btn"
                            OnClientClick="return OnlbHCSEdit_OK_click();"
                            OnClick="lbHCSEdit_OK_Click">
                            <strong>OK</strong>
                        </asp:LinkButton>
                        <asp:LinkButton runat="server" ID="lbHCSEdit_Cancel" CssClass="btn"
                            Style="margin-left: 0.5em;"
                            OnClientClick="OnlbHCSEdit_Cancel_click(); return false;">
                            <strong>Cancel</strong>
                        </asp:LinkButton>
                    </div>
                </div>
        </asp:Panel>

        <asp:HiddenField runat="server" ID="hfInputErrors" />

        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" ClientIDMode="Static" runat="server"
            BehaviorID="popup" TargetControlID="hfInputErrors" PopupControlID="pnlPopup"
            BackgroundCssClass="modalBackground"/>

        <asp:Panel ID="pnlPopup" runat="server" Style="display: none">
            <div style="border-width: 2px; background-color: #ffffff; border-color: #d4d4d4; padding-bottom: 12px; border-style: outset;">
                <div style="padding-top: 50px; padding: 20px;">
                    <asp:Label ID="lblModalMessage" runat="server" Text="Save Graph" style="font-weight: bold; font-size: 18px"/>
                </div>
                <div style="text-align: left; padding-left: 18px; padding-right: 15px">
                    <table>
                        <tr>
                            <td>
                                <strong>Graph&nbsp;Name:</strong>
                                <asp:TextBox runat="server" ID="txtGraphName_Simple" CssClass="NormalTextBox" Width="300px" ValidationGroup="graphName"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtGraphName_Simple" 
                                    ErrorMessage="Required!" Display="Dynamic" ValidationGroup="graphName"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Visible to:</strong>
                                <asp:RadioButtonList ID="rblVisible" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Just Me" Value="own" selected="True"/>
                                    <asp:ListItem Text="Everyone" Value="all" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr runat="server" id="trError" visible="false">
                            <td>
                                <strong style="color: red">A graph with that name already exists. Please try another one.</strong>
                            </td>
                        </tr>
                        <tr runat="server" id="trErrorSpace" visible="false">
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:LinkButton runat="server" ID="btnModalOK" CssClass="btn" CausesValidation="true"
                                    OnClick="btnModalOK_Click" ValidationGroup="graphName"> <strong>OK</strong></asp:LinkButton>
                                <asp:LinkButton runat="server" ID="btnModalNo" CssClass="btn" CausesValidation="false" OnClick="btnModalNo_Click"> <strong>Cancel</strong></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
        <%--<a href="AddGraph.aspx" id="openAddgraph" class="popupaddgraph" style="visibility: hidden; display: none;"></a>--%>
         <asp:Button runat="server" ID="btnAddGraph" ClientIDMode="Static" Style="display: none;" OnClick="btnAddGraph_Click" />

    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="lnkExcel" />
    </Triggers>
</asp:UpdatePanel>

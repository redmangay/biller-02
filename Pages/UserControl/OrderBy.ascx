﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OrderBy.ascx.cs"
    Inherits="Pages_UserControl_OrderBy" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hfOrderByID" Value="" />
            <table>
                <tr runat="server" id="trThenBy">
                    <td colspan="3">
                        <asp:Label Text="Then By" Font-Bold="True" runat="server" ID="lblThenBy">
                        </asp:Label>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr style="text-align: left;">
                    <td style="width: 50px;"></td>
                    <td valign="top">
                        <asp:DropDownList runat="server" ID="ddlTable" CssClass="NormalTextBox" ClientIDMode="Static"
                            DataValueField="TableID" DataTextField="TableName" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlTable_SelectedIndexChanged" Visible="false">
                        </asp:DropDownList>
                    </td>
                    <td align="right" valign="top" style="width: 60px;">
                        <strong runat="server" id="stgField">Field:</strong>
                    </td>
                    <td valign="top">
                        <asp:DropDownList runat="server" ID="ddlColumn" CssClass="NormalTextBox"
                            DataValueField="ColumnID" DataTextField="DisplayName" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlColumn_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td valign="top">
                        <asp:DropDownList runat="server" ID="ddlDirection" AutoPostBack="true" OnSelectedIndexChanged="ddlDirection_SelectedIndexChanged" 
                            CssClass="NormalTextBox" ClientIDMode="Static">
                            <asp:ListItem Value="asc" Text="Ascending" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="desc" Text="Descending"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

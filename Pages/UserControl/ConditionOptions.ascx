﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConditionOptions.ascx.cs"
     Inherits="Pages_UserControl_ConditionOptions" %>

<%@ Register Src="~/Pages/UserControl/ConditionsCondition2.ascx" TagName="SWCon2" TagPrefix="dbg" %>

<%--<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">--%>
    <link href="../../Select2/css/select2.min.css" rel="stylesheet" />
    <script src="../../Select2/js/select2.full.min.js"></script>

    <style type="text/css">
        .condition-header {
            display: block;
            float: left;
            background-color: #EDF3F7;
            margin-left: 2px;
            padding: 5px 5px;
            font-size: 1.1em;
            font-weight: bold;
        }

        .condition-row {
            display: block;
            float: left;
            margin-left: 2px;
            padding: 5px 5px;
            font-size: 1.1em;
        }

            .condition-row select {
                width: 100%;
            }

            .condition-row input[type="text"] {
                width: 87%;
                margin-right: 5px;
            }

        .condition-footer {
            display: block;
            float: left;
            margin-left: 2px;
            margin-top: 10px;
            padding: 5px 5px;
            font-size: 1.1em;
        }

        .condition-when-column {
            clear: left;
            width: 65px; /*5em;*/
        }
        
        .condition-table-column {
            width: 200px; /*5em;*/
        }

        .condition-field-column {
            width: 200px; /*17em;*/
        }

        .condition-operator-column {
            width: 215px; /*10em;*/
        }
         .condition-operator-column-date-choice {
            width: 100px; /*10em;*/
        }

        .condition-value-column {
            width: 230px; /*20em;*/
        }

            .condition-value-column input[type="text"] {
                height: 24px;
                border-radius: 4px;
                border: 1px solid #aaa;
            }

        .condition-remove-column {
            text-align: center;
            /*width: 65px; */ /*5em;*/
        }

        .select2-container-class {
            font-size: 0.9em;
        }

        .select2-dropdown-class {
            font-size: 0.8em;
            line-height: 0.9;
            text-wrap: none;
        }

        .dialog-textbox {
            border-radius: 4px;
        }


        .separator-line {
            overflow: hidden;
            clear: left;
            float: left;
            margin-left: 4px;
            margin-top: 4px;
            width: 67em;
            background: silver;
            height: 1px;
            z-index: 10;
        }
    </style>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            initElements();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initElements();
        });

        function initElements() {

            $("select").each(function () {
                var $el = $(this);
                if ($el.children("select option[data-category]")) {
                    var groups = {};
                    $el.children("select option[data-category]").each(function () {
                        groups[$.trim($(this).attr("data-category"))] = true;
                    });
                    $.each(groups, function (c) {
                        $el.children("select option[data-category='" + c + "']").wrapAll('<optgroup label="' + c + '">');
                    });
                }
            });
            var options = {
                placeholder: {
                    id: "", // the value of the option
                    text: '-- Please Select --'
                },
                allowClear: false, // cross sign at the just before down arrow
                minimumResultsForSearch: 12,
                dropdownAutoWidth: true,
                containerCssClass: "select2-container-class",
                dropdownCssClass: "select2-dropdown-class"
            };
            $("select").select2(options);
            //$('select').on('select2:opening', function (e) {
            //    __doPostBack(this.name,'');
            //});
            $("#lbHelp").fancybox({
                onStart: function () {
                    $("#divHelp").css("display", "block")
                        .css("width", "600px").css("height", "400px");
                },
                afterClose: function () { $("#divHelp").css("display", "none"); }
            });
        }

        function GetBackValueEdit(conditionType) {
            window.parent.document.getElementById('hfCondition').value = "Yes";
                //if (p_Checkbox != null){
                //    p_Checkbox.checked = true;
                //}
                 parent.$.fancybox.close();
        }

        function GetBackValueAdd() {
            var p_Checkbox = window.parent.document.getElementById('<%=ParentCheckboxName()%>');
                if (p_Checkbox != null) {
                    p_Checkbox.checked = true;
                }       
       
        parent.$.fancybox.close();
        }

        function setcontextkey(ctrl, value) {
            $find(ctrl).set_contextKey(value);
        }

    </script>

    <div style="text-align: center;">
        <div style="display: inline-block;">
            <div style="padding-top: 10px;">
                <asp:UpdatePanel ID="upFilter" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <table style="padding-bottom:50px;">
                            <tr>
                                <td colspan="2" style="text-align:left; padding-bottom:50px; padding-top:20px">
                                    <asp:Label runat="server" ID="lblTitle" Font-Bold="True" Font-Size="16px" Style="white-space: nowrap;">Condition</asp:Label>
                                </td>
                               <%--  <td  style="text-align:right; padding-bottom:50px; padding-top:20px">
                                      <asp:HyperLink runat="server" CssClass="showWhenLinkDashboard"
                                            ID="hlBack" Font-Underline="true" Visible="false" ClientIDMode="Static"><strong>Back</strong> </asp:HyperLink>
                                </td>--%>
                                
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <asp:LinkButton runat="server" ID="lbNewCondition" Style="text-decoration: none; color: Black;"
                                        OnClick="lbNewCondition_OnClick" OnClientClick="window.scrollTo(0, document.body.scrollHeight);">
                                        <asp:Image Style="vertical-align: middle;" runat="server" ID="imgAddNewRecord" ImageUrl="~/App_Themes/Default/images/Add32.png" />
                                        <span style="text-decoration: underline; color: Blue; font-weight: bold;">Add new condition</span>
                                    </asp:LinkButton>
                                    <a id="lbHelp" href="#divHelp">
                                        <asp:Image Style="vertical-align: middle;" runat="server" ID="Image1" ImageUrl="~/App_Themes/Default/images/help.png" />
                                    </a>
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td colspan="2">
                                    <asp:Label runat="server" ID="lblDescription" Style="white-space: nowrap;">Conditions</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="padding-left: 0px; padding-top: 10px;">
                                        <asp:Repeater ID="repConditions" runat="server" OnItemDataBound="repConditions_OnItemDataBound" OnItemCommand="repConditions_ItemCommand">
                                            <HeaderTemplate>
                                                <span class="condition-when-column condition-header">When</span>
                                                <span id="headerTable" runat="server" class="condition-table-column condition-header">Table</span>
                                                <span class="condition-field-column condition-header">Field</span>
                                                <span class="condition-operator-column condition-header">Operator</span>
                                                <span class="condition-value-column condition-header">Value</span>
                                                <span class="condition-remove-column condition-header" style="width:200px">Edit / Delete</span>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                  <asp:Panel runat="server" ID="orSeparator" CssClass="separator-line" Visible="false">
                                                </asp:Panel>
                                                <div id="panelLabels" runat="server">
                                                    <div id="panelWhen" runat="server" class="condition-when-column condition-row">
                                                        <asp:Label ID="lblWhen" runat="server" Text='<%# Eval("JoinOperator").ToString().ToUpper() %>' />
                                                    </div>
                                                    <div id="panelTable" runat="server" class="condition-table-column condition-row">
                                                        <asp:Label ID="lblTable" runat="server" Text='<%# Eval("TableName").ToString().ToUpper() %>' />
                                                    </div>
                                                    <div id="panelField" runat="server"  class="condition-field-column condition-row">
                                                        <asp:Label ID="lblField" runat="server" Text='<%# "[" + Eval("TableName") + "].[" + Eval("DisplayName") + "]" %>' />
                                                        <asp:Label ID="lblField2" runat="server" Text='<%# "[" + Eval("DisplayName") + "]" %>' />
                                                    </div>
                                                    <div id="panelOperator" runat="server"  class="condition-operator-column condition-row">
                                                        <asp:Label ID="lblOperator" runat="server" Text='<%# Eval("ConditionOperator") %>' />
                                                    </div>
                                                    <div id="panelValue" runat="server"  class="condition-value-column condition-row">
                                                        <asp:Label ID="lblValue" runat="server" />
                                                    </div>
                                                </div>
                                                <dbg:SWCon2 runat="server" ID="swcCondition" Visible="true" OnItemRemoved="swcCondition_OnItemRemoved" OnJoinChanged="swcCondition_OnJoinChanged"></dbg:SWCon2>
                                                <div class="condition-remove-column condition-row" style="width:200px">
                                                    <center>
                                                        <asp:LinkButton ID="lnkEdit"  runat="server"  Font-Underline="true" Text="Edit" CommandName="Edit" CommandArgument='<%# Eval("ConditionsID") %>' />
                                                        <asp:LinkButton ID="lnkSave" Visible="false" ForeColor="Red" Font-Underline="true"  runat="server" Text="Save" CommandName="Update" CommandArgument='<%# Eval("ConditionsID") %>' /> <asp:Label ID="lblBrackerSeparatorCancel" Visible="false" runat="server">&nbsp;|&nbsp;</asp:Label>
                                                        <asp:LinkButton ID="lnkCancel" Visible="false" ForeColor="Red" Font-Underline="true" runat="server" Text="Cancel" CommandName="Cancel" CommandArgument='<%# Eval("ConditionsID") %>'  CausesValidation="false" /> <asp:Label ID="lblBrackerSeparatorDelete" runat="server">&nbsp;|&nbsp;</asp:Label>
                                                        <asp:LinkButton ID="lnkDelete" CommandName="Delete" Font-Underline="true" CommandArgument='<%# Eval("ConditionsID") %>'  runat="server" Text="Delete" CausesValidation="false" OnClientClick="return confirm('Are you sure you want to delete this condition?')" />
                                                    </center>
                                                </div>
                                              
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <span class="condition-when-column condition-footer"></span>
                                                <span id="footerTable" runat="server" class="condition-table-column condition-footer"></span>
                                                <span class="condition-operator-column condition-footer"></span>
                                                <span class="condition-value-column condition-footer"></span>
                                                <span class="condition-remove-column condition-footer">
                                                </span>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        <div>
                                            <asp:Label ID="lblMsgTab" runat="server" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                    </Triggers>
                </asp:UpdatePanel>
                <div id="divHelp" style="display: none">
                    <asp:Label runat="server" ID="HelpText"></asp:Label>
                </div>
            </div>
        </div>
    </div>   

<%--</asp:Content>--%>

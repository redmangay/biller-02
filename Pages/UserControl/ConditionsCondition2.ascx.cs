﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
//using System.Linq;
using System.Web.UI.WebControls;

public class Pages_UserControl_ItemRemovedEventArgs : EventArgs
{
    public string ItemId { get; set; }
}

public class Pages_UserControl_JoinChangedEventArgs : EventArgs
{
    public string ItemId { get; set; }
    public string JoinOperator { get; set; }
}

public partial class Pages_UserControl_ConditionsCondition2 : UserControl
{
    public int? DocumentSectionID { get; set; }
    public int? TableID { get; set; }
    public int? ColumnID { get; set; }
    public bool? ShowTable { get; set; }

    public event EventHandler ddlHideColumn_Changed;
    public event EventHandler<Pages_UserControl_ItemRemovedEventArgs> ItemRemoved;
    public event EventHandler<Pages_UserControl_JoinChangedEventArgs> JoinChanged;

    public string ControlId
    {
        get { return hfConditionsID.Value; }
        set { hfConditionsID.Value = value; }
    }

    public bool ShowJoinOperator
    {
       set
        {
            if(value == false)
            {
                ddlJoinOperator.SelectedValue = "";
                ddlJoinOperator.Visible = false;
            }
            else
            {
                ddlJoinOperator.Visible = true;
            }
        }
    }

    public bool ShowRemoveButton
    {
        set { btnRemove.Visible = value != false; }
    }

    public bool EnableColumnList
    {
        set { ddlHideColumn.Enabled = value; }
    }

    public string ddlJoinOperatorV
    {
        get { return ddlJoinOperator.SelectedValue ?? ""; }
        set { ddlJoinOperator.SelectedValue = value; }
    }

    public string ddlOperatorV
    {
        get { return ddlOperator.SelectedValue; }
        set
        {
            ddlOperator.SelectedValue = value;
            cuiConditionValue.OperatorValue = value;
        }
    }

    public string ddlHideColumnV
    {
        get
        {
            return ddlHideColumn.SelectedValue ?? "";
        }
        set
        {

            if (ShowTable != null && (bool)ShowTable && value != "")
            {
                if (ddlTable.SelectedItem == null)
                    PopulateTable();

                string strTableID = Common.GetValueFromSQL("SELECT TableID FROM [Column] WHERE ColumnID=" + value);
                if (strTableID != "" && ddlTable.Items.FindByValue(strTableID) != null)
                {
                    ddlTable.SelectedValue = strTableID;
                    TableID = int.Parse(ddlTable.SelectedValue);
                }
            }


            PopulateHideColumns();
            if (ddlHideColumn.Items.FindByValue(value) != null)
            {
                ddlHideColumn.SelectedValue = value;
                ddlHideColumn_SelectedIndexChanged(null, null);
            }
            else
            {
                ddlHideColumn.SelectedValue = "";
                ddlHideColumn_SelectedIndexChanged(null, null);
            }
        }
    }

    public string hfConditionValueV
    {
        get
        {
            hfConditionValue.Value = "";
            if (ddlHideColumn.SelectedValue != "")
            {
                hfConditionValue.Value = cuiConditionValue.ColumnValue;
            }

            return hfConditionValue.Value;
        }
        set
        {
            hfConditionValue.Value = value;

            if (ddlHideColumn.SelectedValue != "")
            {
                cuiConditionValue.ColumnValue = hfConditionValue.Value;
            }
        }
    }


    public string ColumnFilterValue { get; set; }

    public string ColumnFilterID { get; set; }

    public string ConditionColumnValue { get; set; }
    public string ConditionColumnID { get; set; }

    protected override void OnInit(EventArgs e)
    {
        Page.RegisterRequiresControlState(this);
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            PopulateTerminology();

            if (ddlTable.SelectedItem == null)
                PopulateTable();
            if (ShowTable != null && (bool)ShowTable)
            {
                ddlTable.Visible = true;
            }
        }
        PopulateHideColumns();
    }


    protected void PopulateTable()
    {
        if (TableID != null)
            return;

        ddlTable.Items.Clear();
        DataTable dtTemp = Common.DataTableFromText(@"SELECT T.TableID,T.TableName FROM [Table] T INNER JOIN [Column] C 
    ON T.TableID=C.TableID WHERE C.TableTableID=-1 AND T.AccountID=" + Session["AccountID"].ToString() + " ORDER BY T.TableName");
        ddlTable.DataSource = dtTemp;
        ddlTable.DataBind();

        if (ddlTable.SelectedItem != null)
        {
            TableID = int.Parse(ddlTable.SelectedValue);
        }
        else
        {
            return;
        }
        PopulateHideColumns();
        
        //if (DocumentSectionID != null && ddlTable.Visible == false)

        if (Session["ConditionContext"] != null && Session["ConditionContext"].ToString() == "dashboard" && ddlTable.Visible == false)
            {
            ddlTable.Visible = true;
            spanDDLTable.Visible = true;
        }

        //ListItem li = new ListItem("--Please Select--", "");
        //ddlHideColumn.Items.Insert(0, li);

    }

    protected void ddlTable_SelectedIndexChanged(object sender, EventArgs e)
    {
        TableID = int.Parse(ddlTable.SelectedValue);
        PopulateHideColumns();

    }


    protected void PopulateTerminology()
    {
    }

    protected void PopulateHideColumns()
    {
        if (TableID == null)
            PopulateTable();

        if (TableID > -1)
        {

            string value = ddlHideColumn.SelectedValue;
            string strExtra = "";
            ddlHideColumn.Items.Clear();

            ddlHideColumn.Items.Add(new ListItem("-- Please select --", ""));

            DataTable dtTemp = Common.DataTableFromText(
                @"SELECT ColumnID, DisplayName FROM [Column] WHERE IsStandard = 0 AND TableID = " +
                TableID.ToString() + strExtra + " ORDER BY DisplayName");
            foreach (DataRow row in dtTemp.Rows)
            {
                ListItem li = new ListItem(row["DisplayName"].ToString(), row["ColumnID"].ToString());
                li.Attributes.Add("data-category", "");
                ddlHideColumn.Items.Add(li);
            }

            if (ShowTable.HasValue && ShowTable.Value)
            {
                List<Tuple<int, string>> listTables = new List<Tuple<int, string>>();
                DataTable dtColumns = RecordManager.ets_Table_Columns_All(TableID.Value);
                foreach (DataRow column in dtColumns.Rows)
                {
                    int tableTableId = -1;
                    if (!column.IsNull("ColumnType") && column["ColumnType"].ToString() == "dropdown" &&
                        !column.IsNull("DropDownType") && column["DropDownType"].ToString() == "tabledd" &&
                        !column.IsNull("TableTableID") &&
                        int.TryParse(column["TableTableID"].ToString(), out tableTableId))
                    {
                        Table tableTable = RecordManager.ets_Table_Details(tableTableId);
                        if (tableTable != null)
                            listTables.Add(new Tuple<int, string>(tableTableId, tableTable.TableName));
                    }
                }

                foreach (Tuple<int, string> tableTable in listTables)
                {
                    DataTable dtTemp2 = Common.DataTableFromText(
                        @"SELECT ColumnID, DisplayName FROM [Column] WHERE IsStandard = 0 AND TableID = " +
                        tableTable.Item1.ToString() + strExtra + " ORDER BY DisplayName");
                    foreach (DataRow row in dtTemp2.Rows)
                    {
                        ListItem li = new ListItem(row["DisplayName"].ToString(), row["ColumnID"].ToString());
                        li.Attributes.Add("data-category", tableTable.Item2);
                        ddlHideColumn.Items.Add(li);
                    }
                }
            }

            ddlHideColumn.Items.Add(new ListItem("User Role", "-1"));

            ddlHideColumn.SelectedValue = value;

        }
    }


    protected void ddlOperator_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateHideColumns();
        ManageControls(ddlOperator.SelectedValue);

        cuiConditionValue.OperatorValue = ddlOperator.SelectedValue.ToString();
    }


    protected void ddlHideColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlHideColumn_Changed != null)
            ddlHideColumn_Changed(this, EventArgs.Empty);

        if (ddlHideColumn.SelectedValue == "")
        {
            cuiConditionValue.ColumnID = null;
        }
        else
        {
            cuiConditionValue.ColumnID = int.Parse(ddlHideColumn.SelectedValue);

            Column theHideColumn = RecordManager.ets_Column_Details(int.Parse(ddlHideColumn.SelectedValue));
            if (theHideColumn != null)
            {
                if (theHideColumn.ColumnType == "radiobutton")
                {
                    ddlOperator.SelectedValue = "equals";
                    ddlOperator.Enabled = false;
                }
                else
                {
                    ddlOperator.Enabled = true;
                    if (theHideColumn.ColumnType == "date" || theHideColumn.ColumnType == "datetime")
                    {
                        ddlOperator.Items.Clear();
                        System.Web.UI.WebControls.ListItem liSelectEquals = new System.Web.UI.WebControls.ListItem("Equals", "equals");
                        ddlOperator.Items.Insert(0, liSelectEquals);

                        System.Web.UI.WebControls.ListItem liSelectNotEquals = new System.Web.UI.WebControls.ListItem("Not Equals", "notequal");
                        ddlOperator.Items.Insert(1, liSelectNotEquals);

                        System.Web.UI.WebControls.ListItem liSelectGreaterThanDate = new System.Web.UI.WebControls.ListItem("Greater Than (Date)", "greaterthan");
                        ddlOperator.Items.Insert(2, liSelectGreaterThanDate);

                        System.Web.UI.WebControls.ListItem liSelectGreaterThanDays = new System.Web.UI.WebControls.ListItem("Greater Than (Days)", "greaterthandays");
                        ddlOperator.Items.Insert(3, liSelectGreaterThanDays);

                        System.Web.UI.WebControls.ListItem liSelectGreaterThanEqual = new System.Web.UI.WebControls.ListItem("Greater or Equal to", "greaterthanequal");
                        ddlOperator.Items.Insert(4, liSelectGreaterThanEqual);

                        System.Web.UI.WebControls.ListItem liSelectLessThanDate = new System.Web.UI.WebControls.ListItem("Less Than (Date)", "lessthan");
                        ddlOperator.Items.Insert(5, liSelectLessThanDate);

                        System.Web.UI.WebControls.ListItem liSelectLessThanDays = new System.Web.UI.WebControls.ListItem("Less Than (Days)", "lessthandays");
                        ddlOperator.Items.Insert(6, liSelectLessThanDays);

                        System.Web.UI.WebControls.ListItem liSelectLessThanEqual = new System.Web.UI.WebControls.ListItem("Less or Equal to", "lessthanequal");
                        ddlOperator.Items.Insert(7, liSelectLessThanEqual);

                        System.Web.UI.WebControls.ListItem liSelectContains = new System.Web.UI.WebControls.ListItem("Contains", "contains");
                        ddlOperator.Items.Insert(8, liSelectContains);

                        System.Web.UI.WebControls.ListItem liSelectNotContains = new System.Web.UI.WebControls.ListItem("Does Not Contain", "notcontains");
                        ddlOperator.Items.Insert(9, liSelectNotContains);

                        System.Web.UI.WebControls.ListItem liSelectEmpty = new System.Web.UI.WebControls.ListItem("Is Empty", "empty");
                        ddlOperator.Items.Insert(10, liSelectEmpty);

                        System.Web.UI.WebControls.ListItem liSelectNotEmpty = new System.Web.UI.WebControls.ListItem("Is Not Empty", "notempty");
                        ddlOperator.Items.Insert(11, liSelectNotEmpty);
                     

                      
                       
                    }
                    else
                    {
                        //ddlOperator.Items.Remove(ddlOperator.Items.FindByValue("lessthandays"));
                        //ddlOperator.Items.Remove(ddlOperator.Items.FindByValue("greaterthandays"));
                        ddlOperator.Items.Clear();
                        System.Web.UI.WebControls.ListItem liSelectEquals = new System.Web.UI.WebControls.ListItem("Equals", "equals");
                        ddlOperator.Items.Insert(0, liSelectEquals);

                        System.Web.UI.WebControls.ListItem liSelectNotEquals = new System.Web.UI.WebControls.ListItem("Not Equals", "notequal");
                        ddlOperator.Items.Insert(1, liSelectNotEquals);

                        System.Web.UI.WebControls.ListItem liSelectGreaterThanDate = new System.Web.UI.WebControls.ListItem("Greater Than", "greaterthan");
                        ddlOperator.Items.Insert(2, liSelectGreaterThanDate);

                        System.Web.UI.WebControls.ListItem liSelectGreaterThanEqual = new System.Web.UI.WebControls.ListItem("Greater or Equal to", "greaterthanequal");
                        ddlOperator.Items.Insert(3, liSelectGreaterThanEqual);

                        System.Web.UI.WebControls.ListItem liSelectLessThanDate = new System.Web.UI.WebControls.ListItem("Less Than", "lessthan");
                        ddlOperator.Items.Insert(4, liSelectLessThanDate);

                        System.Web.UI.WebControls.ListItem liSelectLessThanEqual = new System.Web.UI.WebControls.ListItem("Less or Equal to", "lessthanequal");
                        ddlOperator.Items.Insert(5, liSelectLessThanEqual);

                        System.Web.UI.WebControls.ListItem liSelectContains = new System.Web.UI.WebControls.ListItem("Contains", "contains");
                        ddlOperator.Items.Insert(6, liSelectContains);

                        System.Web.UI.WebControls.ListItem liSelectNotContains = new System.Web.UI.WebControls.ListItem("Does Not Contain", "notcontains");
                        ddlOperator.Items.Insert(7, liSelectNotContains);

                        System.Web.UI.WebControls.ListItem liSelectEmpty = new System.Web.UI.WebControls.ListItem("Is Empty", "empty");
                        ddlOperator.Items.Insert(8, liSelectEmpty);

                        System.Web.UI.WebControls.ListItem liSelectNotEmpty = new System.Web.UI.WebControls.ListItem("Is Not Empty", "notempty");
                        ddlOperator.Items.Insert(9, liSelectNotEmpty);


                    }

                }

                /*== Red Test*/
            }
        }
    }


    protected void btnRemove_OnClick(object sender, ImageClickEventArgs e)
    {
        if (ItemRemoved != null)
        {
            Pages_UserControl_ItemRemovedEventArgs ea = new Pages_UserControl_ItemRemovedEventArgs { ItemId = hfConditionsID.Value };
            ItemRemoved(this, ea);
        }
    }

    protected override object SaveControlState()
    {
        return new Tuple<int?, bool?>(TableID, ShowTable);
    }

    protected override void LoadControlState(object state)
    {
        Tuple<int?, bool?> tupleState = state as Tuple<int?, bool?>;
        if (tupleState != null)
        {
            TableID = tupleState.Item1;
            ShowTable = tupleState.Item2;
        }
    }

    protected void ddlJoinOperator_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (JoinChanged != null)
        {
            Pages_UserControl_JoinChangedEventArgs ea = new Pages_UserControl_JoinChangedEventArgs
            {
                ItemId = hfConditionsID.Value,
                JoinOperator = ddlJoinOperator.SelectedValue
            };
            JoinChanged(this, ea);
        }
    }

    private void ManageControls(string op)
    {
        if (op == "empty" || op == "notempty")
        {
            cuiConditionValue.ColumnValue = "";
            cuiConditionValue.OperatorValue = "";
            cuiConditionValue.ShowControls = false;
            hfConditionValue.Value = "";
        }
        else
        {
            cuiConditionValue.ShowControls = true;
        }
    }

    private void GetAdvanceConditionDetails(Int32 advanceconditionid)
    {
        String strSQL = "SELECT [AdvancedConditionID] " +
                        ",[ConditionType] " +
                        ",[ConditionSubType] " +
                        ",[ColumnID] " +
                        ",[ConditionColumnID] " +
                        ",(SELECT DisplayName FROM[Column] WHERE[Column].[ColumnID] = [AdvancedCondition].[ConditionColumnID]) [ConditionColumnText] " +
                        ",[ConditionColumnValue] " +
                        ",[ConditionOperator] " +
                        ",[DisplayOrder] " +
                        ",[JoinOperator] " +
                        "FROM[AdvancedCondition] " +
                        "WHERE[AdvancedConditionID] = " + advanceconditionid.ToString();
        DataTable dt = Common.DataTableFromText(strSQL);

        if (dt.Rows.Count == 1)
        {
            ColumnFilterID = dt.Rows[0]["ConditionColumnID"].ToString();
            ColumnFilterValue = dt.Rows[0]["ConditionColumnText"].ToString();
        }
    }
}
﻿using System;
using System.Data;
//using System.Linq;
using System.Web.UI.WebControls;

public partial class Pages_UserControl_OrderBy : System.Web.UI.UserControl
{
    public int? TableID { get; set; }
    public int? ColumnID { get; set; }
    
    public int? TableTabID { get; set; }
    public bool? ShowTable { get; set; }

    public event EventHandler ddlSortOrder_Changed;

    public bool ShowThenByLabel
    {
        set
        {
            lblThenBy.Visible = value;
        }
    }

    public string ddlDirectionV
    {
        get
        {
            return ddlDirection.SelectedValue;
        }
        set
        {
            ddlDirection.SelectedValue = value;
        }
    }

    public string ddlColumnV
    {
        get
        {
            return ddlColumn.SelectedValue ?? "";
        }
        set
        {
            if (ShowTable != null && (bool)ShowTable && value != "")
            {
                if (ddlTable.SelectedItem == null)
                    PopulateTable();

                string strTableID = Common.GetValueFromSQL("SELECT TableID FROM [Column] WHERE ColumnID=" + value);
                if (strTableID != "" && ddlTable.Items.FindByValue(strTableID) != null)
                {
                    ddlTable.SelectedValue = strTableID;
                    TableID = int.Parse(ddlTable.SelectedValue);
                }
            }

            PopulateColumns();
            if(ddlColumn.Items.FindByValue(value)!=null)
            {
                ddlColumn.SelectedValue = value;
                ddlColumn_SelectedIndexChanged(null, null);
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            PopulateTerminology();

            if(ddlTable.SelectedItem == null)
                PopulateTable();
            if(ShowTable != null && (bool)ShowTable)
            {
                ddlTable.Visible = true;
            }
        }
    }

    protected void PopulateTerminology()
    {
        stgField.InnerText = stgField.InnerText.Replace("Field", SecurityManager.etsTerminology(Request.Path.Substring(
            Request.Path.LastIndexOf("/") + 1), "Field", "Field"));
    }

    protected void PopulateColumns()
    {
        if (TableID == null)
            PopulateTable();

        if (TableID == null)
        {
            return;
        }
        string strExtra = "";

        ddlColumn.Items.Clear();
        DataTable dtTemp = Common.DataTableFromText(@"SELECT ColumnID, DisplayName FROM [Column] 
                WHERE IsStandard = 0 AND TableID = " + TableID.ToString() + strExtra+ " ORDER BY DisplayName");
        ddlColumn.DataSource = dtTemp;
        ddlColumn.DataBind();
        ListItem li = new ListItem("--Please Select--", "");
        ddlColumn.Items.Insert(0, li);
    }

    protected void PopulateTable()
    {
        if (TableID != null)
            return;

        ddlTable.Items.Clear();
        DataTable dtTemp = Common.DataTableFromText(@"SELECT T.TableID,T.TableName FROM [Table] T INNER JOIN [Column] C 
    ON T.TableID = C.TableID WHERE C.TableTableID = -1 AND T.AccountID = " + Session["AccountID"].ToString() + " ORDER BY T.TableName");
        ddlTable.DataSource = dtTemp;
        ddlTable.DataBind();

        if(ddlTable.SelectedItem!=null)
        {
            TableID = int.Parse(ddlTable.SelectedValue);
        }
        else
        {
            return;
        }
        PopulateColumns();
    }

    protected void ddlTable_SelectedIndexChanged(object sender, EventArgs e)
    {
        TableID = int.Parse(ddlTable.SelectedValue);
        PopulateColumns();
    }

    protected void ddlDirection_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void ddlColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSortOrder_Changed != null)
            ddlSortOrder_Changed(this, EventArgs.Empty);

        ddlDirection.Enabled = ddlColumn.SelectedValue != "";
    }
}
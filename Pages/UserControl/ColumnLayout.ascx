﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ColumnLayout.ascx.cs"
    Inherits="Pages_UserControl_ColumnLayout" %>

<style>
    .detailtable th, .detailtable td {
        min-width: 150px;
        max-width: 350px;
        height: 50px;
        max-height: 100px;
        border: 1px dotted black;
    }

    .columnspan {
        display: block;
        background-color: #ccc;
        border: 1px solid;
        color: #fff;
        height: 100%;
        width: 100%;
    }

    .hiddencolumnspan {
        display: block;
        background-color: #dbbdbd;
        border: 1px solid;
        color: #fff;
        min-width: 150px;
        max-width: 350px;
        min-height: 50px;
        max-height: 100px;
    }

    .detailtable {
        display: block;
        border: 1px solid;
        height: 100%;
        width: 100%;
    }
</style>
<script type="text/javascript">

    $(document).ready(function () {
        
        $('.columnspan').on("dragstart", function (event) {
            var dt = event.originalEvent.dataTransfer;
            dt.setData('Text', $(this).attr('id'));
        });

        $('.detailtable td').on("dragenter dragover drop", function (event) {


            event.preventDefault();

            if (event.type === 'drop' &&
                (event.target == '[object HTMLTableCellElement]'
                    || event.target == '[object HTMLTableDataCellElement]')) {


                var data = event.originalEvent.dataTransfer.getData('Text');
                event.target.appendChild(document.getElementById(data));

                $.ajax({
                    url: 'ColumnRecordRes.ashx?Coordinate=' + event.target.id + '&ColumnID=' + data,
                    cache: false,
                    success: function (content) {
                        if (content == 'False') {
                            alert('Error!');
                        }
                        else { //alert('good');
                        }
                    },
                    error: function (a, b, c) {
                        alert('Error!');
                    }
                });



            }
            else if (event.type === 'drop') {
                alert('Please use an empty cell.');
                //alert(event.target);
            };


        });
    })
</script>

<div>

    <table>
        <tr>
            <td style="text-align: right">Table:
            </td>
            <td style="text-align: left">
                <asp:DropDownList runat="server" ID="ddlTable" DataTextField="TableName" DataValueField="TableID"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlTable_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td style="text-align: right">Columns:
            </td>
            <td style="text-align: left">
                <asp:DropDownList runat="server" ID="ddlNoOfColumns" AutoPostBack="true" OnSelectedIndexChanged="ddlNoOfColumns_SelectedIndexChanged">
                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td style="text-align: right">Pages:
            </td>
            <td style="text-align: left">
                <asp:DropDownList runat="server" ID="ddlPage" DataTextField="TabName" DataValueField="TableTabID"
                    AutoPostBack="true" OnSelectedIndexChanged="ddlPage_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td style="text-align: left">
                <asp:CheckBox runat="server" ID="chkShowSystemFields" TextAlign="Right" Font-Bold="true"
                    Text="Show System Fields" AutoPostBack="true" OnCheckedChanged="chkShowSystemFields_OnCheckedChanged" />

            </td>

        </tr>
    </table>

    <br />
    <br />

    <asp:Panel runat="server" ID="pnlDynamicHost">


        <table>
            <tr>
                <td>
                    <table runat="server" id="tblDetailTable" visible="true" cellpadding="3" enableviewstate="false"
                         class="detailtable">
                    </table>
                    <%--<asp:Panel runat="server" ID="pnlHost"></asp:Panel>--%>
                </td>
                <td style="vertical-align: bottom;">
                    <asp:Button runat="server" ID="btnAddRow" CssClass="btn btn-primary"
                        Text="Add Row" OnClick="btnAddRow_Click" />
                </td>
            </tr>
            <tr>
                <td style="text-align: left; height: 50px;">Hidden Fields</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="divHiddenFields" Visible="true" CssClass="row">
                    </asp:Panel>

                </td>


            </tr>
        </table>
        <br />

    </asp:Panel>


    <br />
    <br />


</div>

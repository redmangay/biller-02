﻿//using System.Data;
//using System.Data.SqlClient;
//using System.Configuration;
//using System.Collections;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;
//using System.Globalization;
//using System.IO;
//using System.IO.Compression;
//using System.Net.Mail;
//using System.Collections.Generic;
//using System.Text;
//using System.Text.RegularExpressions;
//using DocGen.DAL;

//using AjaxControlToolkit;


using AjaxControlToolkit;
using DocGen.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;




public partial class Pages_UserControl_RecordList : System.Web.UI.UserControl
{
    bool bUseArchiveData = false;
    string _strJSDynamicShowHide = "";
    string _strCommonJS = "";
    bool _bResponsive = false;
    //Label[] _lbl;
    //TextBox[] _txtValue;   
    string _strServiceTypeWHR = "";
    //DropDownList[] _ddlValue;
    //bool _bDBGSortColumnHide = false;\
    bool _bHaveService = false;
    System.Xml.XmlDocument _xmlView_FC_Doc;
    string _strEqualOrGreaterOperator = " = ";
    bool _bFixedHeader = false;
    bool _bBindWithSC = true;
    bool _bShowGraphIcon = true;
    bool _bReset = false;
    bool _bHideAllExport = false;
    bool _bDeleteReason = false;
    string _strNoAjaxView = "";
    int? _iParentRecordID = null;
    int? _iParentTableID = null;
    bool _bOpenInParent = false;
    //int? _iViewID = null;
    View _theView = null;
    string _strListType = "";
    string _strViewName = "";
    Table _theTable;
    bool _bCustomDDL = false;
    //bool _bDynamicSearch = false;
    int _iTotalDynamicColumns = 0;
    int _iMaxCharactersInCell = 250;
    Common_Pager _gvPager;
    DataTable _dtDataSource;
    DataTable _dtRecordColums;
    DataTable _dtDynamicSearchColumns;
    DataTable _dtSearchGroup;
    DataTable _dtColumnsAll;
    DataTable _dtViewTotal;
    bool _bIsForExport = false;
    User _ObjUser;
    UserRole _theUserRole;
    Role _theRole;
    RoleTable _theRoleTable;
    string _strRecordRightID = Common.UserRoleType.None;
    string _strNumericSearch = "";
    //string _strTextSearch = "";

    //int _iStartIndex = 0;
    //int _iGridPageSize = 10;
    int _iSearchCriteriaID = -1;
    int _iStartIndex = 0;
    int _iMaxRows = 10;
    string _strGridViewSortColumn = "DBGSystemRecordID";
    string _strGridViewSortDirection = "DESC";
    string _qsTableID = "-1";

    string _strRecordFolder = "Record";
    int iSCidForDeleteRecord = -1; //Red 3745
    bool _bAllowedToDelete = false;

    //bool _bReadOnly = false;
    //public string[] formats = {"d/M/yyyy",
    //"dd/MM/yyyy","dd/M/yyyy","d/M/yy","dd/MM/yyyy","dd/M/yy","d/MM/yyyy","d/MM/yy"};

    public string _strTextSearch; //why public?
    public bool _bEqualOrGreaterOperator = false;//why public?
    bool _bEmptyRecord = false;
    string _strFunctionUQ = "";

    public bool ShowTitle = true;
    public bool IsRecordDetail = false;
    public string AnyTextSearch { get; set; }
    public int TableID { get; set; }
    public int? DisplayInColumnID { get; set; }
    public int? RecordID { get; set; }
    public int? TableChildID { get; set; }
    public int? ParentRecordIDList { get; set; } /* Red 28112018 see Ref below */
    public bool IsChildTable { get; set; } /* Red 12092019 see Ref below */
    bool gvNoCheckAll = false;
    private List<ReportItemFilter> lstReportItemFilter = new List<ReportItemFilter>();


    public string TextSearch
    {
        set
        {
            //if (_strTextSearch != value)
            //    System.Diagnostics.Debugger.Break();

            _strTextSearch = value;
        }
        get
        {
            return _strTextSearch;
        }
    }

    public string RefreshMe
    {
        set
        {
            if(value=="b")//b=BindTheGrid
            {
                lnkSearch_Click(null, null);
            }
            else if (value == "r")// reset
            {
                lnkReset_Click(null, null);
            }
                
        }
        
    }

    public string TextSearchParent { get; set; }

    public bool ShowAddButton { get; set; }
    public bool ShowAddButtonOnAdd { get; set; }
    public bool ShowEditButton { get; set; }


    public string PageType { get; set; }
    public int DetailTabIndex { get; set; }

    Account _theAccount;

    DateTime? _dtDateFrom = null;
    DateTime? _dtDateTo = null;

    Dictionary<int, List<Tuple<int, string, string, string, string>>> _dictCellColourRules = null; // Cell Colour cache
    Dictionary<int, string> _dictCellMap = null;
    Dictionary<int, string> _dictSessionRecordListSQL = null;

    string _strFilesLocation = "";
    string _strViewID = "";
    string _strViewPageType = "";
    //bool _bDashView = false;
    string _strViewSession = "";
    string _strViewSessionExtra = "";
    string _strTodayShortDate = DateTime.Today.ToShortDateString();
    string _strDynamictabPart = "";

    public string GridClientID
    {
        get
        {
           return gvTheGrid.ClientID;
        }
    }
    public int SearchCriteriaID
    {
        get
        {
            if (ViewState["_iSearchCriteriaID"] != null)
            {
                return int.Parse(ViewState["_iSearchCriteriaID"].ToString());
            }
            else
            {
                return -1;
            }
        }
    }

    public int ViewID
    {
        get
        {
            if (hfViewID.Value != "")
            {
                return int.Parse(hfViewID.Value);
            }
            else
            {
                return -1;
            }
        }
    }


    public void CallSearch()
    {

        Page_Init(null, null);
        Page_Load(null, null);
        PopulateUser();
        PopulateBatch();
        lnkSearch_Click(null, null);
    }

    public string GetEditURL()
    {
        if (PageType == "p")
        {

            string strExtra = "";

            //if (Request.QueryString["viewname"] != null)
            //{
            //    strExtra = "&viewname=" + Request.QueryString["viewname"].ToString();
            //}
            if (Request.QueryString["View"] != null && Request.RawUrl.IndexOf("RecordList.aspx") > -1)
            {
                strExtra = "&View=" + Request.QueryString["View"].ToString();
            }

            return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?mode=" + Cryptography.Encrypt("edit") + "&SearchCriteriaID=" + Cryptography.Encrypt(SearchCriteriaID.ToString()) + "&TableID=" + Cryptography.Encrypt(TableID.ToString()) + strExtra + "&Recordid=";
        }
        else
        {
            if (Request.QueryString["Recordid"] == null)
            {
                return "";
            }
            else
            {
                return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/" + _strRecordFolder + "/RecordDetail.aspx?mode=" + Cryptography.Encrypt("edit") + "&SearchCriteriaID=" + Cryptography.Encrypt(SearchCriteriaID.ToString()) + "&TableID=" + Cryptography.Encrypt(TableID.ToString()) + "&parentRecordid=" + Request.QueryString["Recordid"].ToString() + "&Recordid=";
            }

        }
    }


    public string GetViewURL()
    {

        string strExtra = "";

        //if (Request.QueryString["viewname"] != null)
        //{
        //    strExtra = "&viewname=" + Request.QueryString["viewname"].ToString();
        //}
        if (Request.QueryString["View"] != null && Request.RawUrl.IndexOf("RecordList.aspx") > -1)
        {
            strExtra = "&View=" + Request.QueryString["View"].ToString();
        }

        return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/" + _strRecordFolder + "/RecordDetail.aspx?mode=" + Cryptography.Encrypt("view") + "&SearchCriteriaID=" + Cryptography.Encrypt(SearchCriteriaID.ToString()) + "&TableID=" + Cryptography.Encrypt(TableID.ToString()) + strExtra + "&Recordid=";

    }

    public string GetAccountViewURL()
    {

        return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Security/AccountDetail.aspx?mode=" + Cryptography.Encrypt("view") + "&accountid=";

    }





    protected void GetRoleRight()
    {
        _ObjUser = (User)Session["User"];
        _theUserRole = (UserRole)Session["UserRole"];

        _theRole = SecurityManager.Role_Details((int)_theUserRole.RoleID);

        if (Request.QueryString["Dashboard"] != null)
        {
            _theTable.HideAdvancedOption = true;
            chkShowAdvancedOptions.Checked = false;
            chkShowAdvancedOptions.Visible = false;
            /* chkUseArchive.Checked = false; //red Ticket 3358 */ // Disabled by Red - Ticket 4371 
            bUseArchiveData = false; // Red 08012018
            trUseArchive.Visible = false; //red Ticket 3358
            tdFilterYAxis.Visible = false;
            lblAdvancedCaption.Visible = false;
        }



        if ((bool)_theUserRole.IsAdvancedSecurity)
        {

            DataTable dtUserTable = null;
            dtUserTable = SecurityManager.dbg_RoleTable_Select(null,
           int.Parse(_qsTableID), _theUserRole.RoleID, null);

            if (dtUserTable.Rows.Count > 0)
            {
                _strRecordRightID = dtUserTable.Rows[0]["RoleType"].ToString();
                Session["TableRoleType"] = _strRecordRightID; //red 3259
                _bHideAllExport = !bool.Parse(dtUserTable.Rows[0]["CanExport"].ToString());
                if (_theRole.IsSystemRole == null || (_theRole.IsSystemRole != null && (bool)_theRole.IsSystemRole == false))
                {
                    _theRoleTable = SecurityManager.dbg_RoleTable_Detail(int.Parse(dtUserTable.Rows[0]["RoleTableID"].ToString()));
                }
            }
        }
        else
        {
            _strRecordRightID = Session["roletype"].ToString();
        }
    }

    protected void FindTheTable()
    {

        if (PageType == "p")
        {
            if (Request.QueryString["TableID"] != null && DisplayInColumnID == null)
                TableID = int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
        }
        else
        {
            if (Request.QueryString["RecordID"] != null)
                _iParentRecordID = int.Parse(Cryptography.Decrypt(Request.QueryString["RecordID"].ToString()));
        }


        _qsTableID = TableID.ToString();

        //RP Added Ticket 4363
        Session["BatchUploadID"] = _qsTableID;
        //End Modification

        if (TableID != null)
        {
            _theTable = RecordManager.ets_Table_Details((int)TableID);
            if (_theTable != null)
            {
                _dtColumnsAll = RecordManager.ets_Table_Columns_All((int)_theTable.TableID);
            }
        }
    }


    //protected void SetViewItemAlignment(int iViewID, int iViewTableID)
    //{
    //    //Red Ticket 1679
    //    //<Begin>
    //    string strDefaultAlignment = "";
    //    string strAccountID = Common.GetValueFromSQL("SELECT AccountID FROM [Table] WHERE [TableID]=" + iViewTableID);
    //    strDefaultAlignment = Common.SO_ViewAlignmentDefault(int.Parse(strAccountID), iViewTableID);

    //    bool bCompareRight = string.Equals("Right", strDefaultAlignment);
    //    bool bCompareLeft = string.Equals("Left", strDefaultAlignment);
    //    bool bCompareCenter = string.Equals("Center", strDefaultAlignment);
    //    bool bCompareAuto = string.Equals("Auto", strDefaultAlignment);


    //    if (!bCompareAuto && !bCompareCenter && !bCompareLeft && !bCompareRight)
    //    {
    //        strDefaultAlignment = "Auto";
    //    }

    //    DataTable dtViewItem = Common.DataTableFromText("SELECT [ViewItemID] FROM [ViewItem] WHERE [ViewID] =" + iViewID);
    //    if (dtViewItem != null)
    //    {
    //        foreach (DataRow dr in dtViewItem.Rows)
    //        {

    //            Common.ExecuteText("UPDATE [ViewItem] SET [Alignment] ='" + strDefaultAlignment + "' WHERE [ViewItemID]=" + int.Parse(dr["ViewItemID"].ToString()));
    //        }

    //    }
    //    //<End>
    //}

    protected void FindTheView()
    {
        if (Request.QueryString["View"] != null && Request.RawUrl.IndexOf("RecordList.aspx") > -1)
        {
            _strViewID = Request.QueryString["View"].ToString();
        }
        _strViewPageType = "";

        if (_theTable != null)
        {
            if (Request.RawUrl.IndexOf("RecordTableSection.aspx") > -1
                || Request.RawUrl.IndexOf("EachRecordTable.aspx") > -1)
            {
                _strViewPageType = "dash";
                //_bDashView = true;
                if (Request.QueryString["DocumentSectionID"] != null)
                {
                    _strViewSessionExtra = "_ds" + Request.QueryString["DocumentSectionID"].ToString();
                }
                else
                {
                    if (Request.QueryString["DocumentID"] != null)
                    {
                        _strViewSessionExtra = "_d" + Request.QueryString["DocumentID"].ToString();
                    }
                }

            }
            else
            {
                if (PageType == "c")
                {
                    _strViewPageType = "child"; //same table apear more than once as a child table then
                    if (TableChildID != null)
                    {
                        _strViewSessionExtra = _strViewSessionExtra + "_" + TableChildID.ToString();
                    }
                    else if (_iParentTableID != null)
                    {
                        _strViewSessionExtra = "_pt" + _iParentTableID.ToString();
                    }
                }
                if (PageType == "p")
                {
                    _strViewPageType = "list";

                }
                if (PageType == "any")//"c"
                {
                    _strViewPageType = "list";// "child"
                    pnlSearch.Visible = false;
                    divRecordListTop.Visible = false;
                    PageType = "p";
                }
            }
        }

        if (DisplayInColumnID != null)
        {
            _strViewSessionExtra = _strViewSessionExtra + "_" + DisplayInColumnID.ToString();
        }

        _strViewSession = "View" + _strViewPageType + _qsTableID + _strViewSessionExtra;


        if (Request.QueryString["ViewID"] != null)
        {
            _strViewID = Cryptography.Decrypt(Request.QueryString["ViewID"].ToString());
        }

        if (_strViewID == "" && Request.QueryString["View"] != null && Request.RawUrl.IndexOf("RecordList.aspx") > -1)
        {
            _strViewID = Request.QueryString["View"].ToString();
        }

        if (!IsPostBack && _strViewID != "" && (Request.QueryString["ViewID"] != null || Request.QueryString["View"] != null))
        {
            _theView = ViewManager.dbg_View_Detail(int.Parse(_strViewID));
            if (_theView == null)
            {
                Response.Redirect("~/Default.aspx", false);//
                return;
            }
        }
        if (_strViewPageType == "dash")
        {
            if (Request.QueryString["DocumentSectionID"] == null || Request.QueryString["DocumentSectionID"].ToString() == "")
            {
                Session[_strViewSession] = null;
            }

        }

        if (!IsPostBack && _strViewID == "" && Session[_strViewSession] != null)// && _strViewPageType != "dash"
        {
            _strViewID = Session[_strViewSession].ToString();
            _theView = ViewManager.dbg_View_Detail(int.Parse(_strViewID));
            if (_theView == null)
            {
                Session[_strViewSession] = null;
            }
        }

        if (_strViewID == "" && hfViewID.Value != "")// && _strViewPageType != "dash"
        {
            _strViewID = hfViewID.Value;
        }
        if (!IsPostBack && _strViewID == "" && _strViewPageType == "dash" && Request.QueryString["ViewID"] == null)
        {
            int? iNewViewID = ViewManager.dbg_View_CreateDash((int)_ObjUser.UserID, (int)_theTable.TableID);
            //Red Ticket 1679
            //<Begin>
            //SetViewItemAlignment(int.Parse(iNewViewID.ToString()), (int)_theTable.TableID);
            //<End>

            if (iNewViewID != null)
                _strViewID = iNewViewID.ToString();
        }

        if (_strViewID != "")
        {
            _theView = ViewManager.dbg_View_Detail(int.Parse(_strViewID));
        }
        if (_theView != null)
        {
            if ((int)_theView.TableID != (int)_theTable.TableID)
            {
                _theView = null;
                Session[_strViewSession] = null;
            }
        }
        if (_theView == null)
        {
            if(DisplayInColumnID!= null && Request.QueryString["TableID"] != null)
            {
                _iParentTableID = int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
            }
            
            int? iBfViewID = ViewManager.dbg_View_BestFittingNew((int)_ObjUser.UserID, int.Parse(_qsTableID),
                _iParentTableID, TableChildID, _strViewPageType, DisplayInColumnID);

            //Red Ticket 1679
            //<Begin>
            //SetViewItemAlignment(int.Parse(iBfViewID.ToString()), (int)_theTable.TableID);
            //<End>

            if (iBfViewID != null)
            {
                _theView = ViewManager.dbg_View_Detail((int)iBfViewID);
            }
        }
        if (_theView != null)
        {
            _strListType = "view";
            if (!IsPostBack)
            {
                Session[_strViewSession] = _theView.ViewID.ToString();
                hfViewID.Value = _theView.ViewID.ToString();
            }

            if (IsPostBack)
            {
                if (Session["ViewItemID" + _theView.ViewID.ToString()] != null)
                {
                    string strOldViewItemID = Session["ViewItemID" + _theView.ViewID.ToString()].ToString();
                    Session["ViewItemID" + _theView.ViewID.ToString()] = null;
                    string strViewItemID = Common.Get_Comma_Sep_IDs("ViewItemID", "[ViewItem]", " ViewID=" + _theView.ViewID.ToString() + " AND SearchField=1");
                    if (strOldViewItemID != strViewItemID)
                    {
                        try
                        {
                            Response.Redirect(Request.RawUrl, true);
                        }
                        catch
                        {
                            return;
                        }
                    }


                    Session["ViewItemID" + _theView.ViewID.ToString()] = null;
                }
            }
        }

    }

    protected void JSCode()
    {

        string strMouseoverBC = "76BAF2";
        string strCheckedBC = "96FFFF";
        string strAlterBC1 = "ffffff";
        string strAlterBC2 = "DCF2F0";

        if (this.Page.MasterPageFile != null && this.Page.MasterPageFile.ToLower().IndexOf("rrp") > -1)
        {
            strMouseoverBC = "D3D9EA";
            strCheckedBC = "D3E8E4";
            strAlterBC2 = "ECECED";

        }





        string strMainListJS = "";

        if (PageType == "p" || DisplayInColumnID!=null)
        {
            strMainListJS = @"
                         function MouseEvents(objRef, evt) {
                                    var checkbox = objRef.getElementsByTagName('input')[0];



                                    if (evt.type == 'mouseover') {
                                        objRef.style.backgroundColor = '#" + strMouseoverBC + @"';
                                        objRef.style.cursor = 'pointer';
                                    }
                                    else {          

                                        if (checkbox != null && checkbox.checked) {
                                            objRef.style.backgroundColor = '#" + strCheckedBC + @"';
                                        }
                                        else if (evt.type == 'mouseout') {
                                            if (objRef.rowIndex % 2 == 0) {
                                                //Alternating Row Color
                                                objRef.style.backgroundColor = '#" + strAlterBC1 + @"';
                                            }
                                            else {
                                                objRef.style.backgroundColor = '#" + strAlterBC2 + @"';
                                            }
                                        }
                                    }
                                }
                               //Red 3715
                               //function toggleAndOr(t, hf) {
                               //         if (t.text == 'and') {
                               //             t.text = 'or';
                               //         } else {
                               //             t.text = 'and';
                               //         }
                               //         document.getElementById(hf).value = t.text;
                               //     }
                               
                               //Red: changed to dd from hyperlink
                                function toggleAndOr(t, hf) {     
                       
                                        document.getElementById(hf).value = document.getElementById(t).value;
                                    }

                                      function CreateFile() {

                                            document.getElementById('btnEmail').click();

                                        }


                                function Check_Click(objRef,gvNoCheckAll) {

                                    //Get the Row based on checkbox
                                    var row = objRef.parentNode.parentNode;
                                    if (objRef.checked) {
                                        //If checked change color to Aqua
                                        row.style.backgroundColor = '#" + strCheckedBC + @"';
                                    }
                                    else {
                                        //If not checked change back to original color
                                        if (row.rowIndex % 2 == 0) {
                                            //Alternating Row Color
                                            row.style.backgroundColor = '#" + strAlterBC1 + @"';

                                        }
                                        else {
                                            row.style.backgroundColor = '#" + strAlterBC2 + @"';
                                        }
                                }

                                    //Get the reference of GridView
                                    var GridView = row.parentNode;
         
                                    //Get all input elements in Gridview
                                    var inputList = GridView.getElementsByTagName('input');
                                     var tblHR = document.getElementById('ctl00_HomeContentPlaceHolder_rlOne_gvTheGridHR');
                                     var headerCheckBox;
                                    for (var i = 0; i < inputList.length; i++) {
                                        //The First element is the Header Checkbox
                                        if(headerCheckBox==null 
                                                && inputList[i].type == 'checkbox' 
                                                )
                                            headerCheckBox = inputList[i];


                                        //Based on all or none checkboxes
                                        //are checked check/uncheck Header Checkbox
                                        var checked = true;
                                        if(tblHR==null)
                                        {
                                            if (inputList[i].type == 'checkbox' && inputList[i] != headerCheckBox) {
                                                        if (!inputList[i].checked) {
                                                            checked = false;
                                                            break;
                                                        }
                                                    }
                                        }
                                        else
                                        {
                                            if (inputList[i].type == 'checkbox') {
                                                        if (!inputList[i].checked) {
                                                            checked = false;
                                                            break;
                                                        }
                                                    }
                                        }

           
                                    }

                                     if(tblHR==null)
                                        {
                                            if(headerCheckBox!=null && gvNoCheckAll == 'False') //headerCheckBox.id != 'ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid_ctl03_chkDelete')
                                                {

                                                     headerCheckBox.checked = checked;
                                                }
            
                                        }
        
                                    if (objRef.checked==false)
                                        {
                                            if(tblHR==null && headerCheckBox!=null && gvNoCheckAll == 'False') //headerCheckBox.id != 'ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid_ctl03_chkDelete')
                                            {
                                                headerCheckBox.checked=false;
                   
                                            } 
                                            else
                                            {
                                                 //var tbl = document.getElementById('ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid');
                                                  if(tblHR!=null)
                                                        {
                                                            var chkAll =  document.getElementById('ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid_ctl02_chkAll');
                                                            if(chkAll!=null)
                                                            {
                                                                chkAll.checked=false;
                                                                //alert(chkAll.id);
                                                            }
                                                        }         
                                            }                  
                                        }

                                }

                                function checkAll(objRef) {
                                    var GridView = objRef.parentNode.parentNode.parentNode;
                                    var inputList = GridView.getElementsByTagName('input');
                            //alert(inputList.length);
                                    for (var i = 0; i < inputList.length; i++) {
                                        //Get the Cell To find out ColumnIndex
                                        var row = inputList[i].parentNode.parentNode;
                                        if (inputList[i].type == 'checkbox' && objRef != inputList[i]) {
                                            if (objRef.checked) {
                                                //If the header checkbox is checked
                                                //check all checkboxes
                                                //and highlight all rows
                                                row.style.backgroundColor = '#" + strCheckedBC + @"';
                                                inputList[i].checked = true;
                                            }
                                            else {
                                                //If the header checkbox is checked
                                                //uncheck all checkboxes
                                                //and change rowcolor back to original 
                                                if (row.rowIndex % 2 == 0) {
                                                    //Alternating Row Color
                                                    row.style.backgroundColor = '#" + strAlterBC1 + @"';

                                                }
                                                else {
                                                    row.style.backgroundColor = '#" + strAlterBC2 + @"';
                                                }
                                                inputList[i].checked = false;
                                            }
                                        }
                                    }
                                }

                            function checkAllHR(objRef,GridView) {
                                    var inputList = GridView.getElementsByTagName('input');
                            //alert(inputList.length);
                                    for (var i = 0; i < inputList.length; i++) {
                                        //Get the Cell To find out ColumnIndex
                                        var row = inputList[i].parentNode.parentNode;
                                        if (inputList[i].type == 'checkbox' && objRef != inputList[i]) {
                                            if (objRef.checked) {
                                                //If the header checkbox is checked
                                                //check all checkboxes
                                                //and highlight all rows
                                                row.style.backgroundColor = '#" + strCheckedBC + @"';
                                                inputList[i].checked = true;
                                            }
                                            else {
                                                //If the header checkbox is checked
                                                //uncheck all checkboxes
                                                //and change rowcolor back to original 
                                                if (row.rowIndex % 2 == 0) {
                                                    //Alternating Row Color
                                                    row.style.backgroundColor = '#" + strAlterBC1 + @"';

                                                }
                                                else {
                                                    row.style.backgroundColor = '#" + strAlterBC2 + @"';
                                                }
                                                inputList[i].checked = false;
                                            }
                                        }
                                    }
                                }

                              function SelectAllCheckboxes(spanChk) {

                                        // alert($(spanChk).attr('id'));
                                        checkAll(spanChk);
                                        var GridView = spanChk.parentNode.parentNode.parentNode;

                                        //alert($(GridView).attr('id'));
                                        // alert(GridView.id);

                                        var inputList = GridView.getElementsByTagName('input');
                                        for (var i = 0; i < inputList.length; i++) {
                                            var row = inputList[i].parentNode.parentNode;
                                            if (inputList[i].type == 'checkbox' && spanChk != inputList[i]) {
                                                if (spanChk.checked) {
                                                    inputList[i].checked = true;
                                                }
                                                else {
                                                    inputList[i].checked = false;
                                                }
                                            }

                                        }
                                    }


                              function SelectAllCheckboxesHR(spanChk, GridView) {

                                        checkAllHR(spanChk, GridView);
                                        var inputList = GridView.getElementsByTagName('input');
                                        for (var i = 0; i < inputList.length; i++) {
                                            var row = inputList[i].parentNode.parentNode;
                                            if (inputList[i].type == 'checkbox' && spanChk != inputList[i]) {
                                                if (spanChk.checked) {
                                                    inputList[i].checked = true;
                                                }
                                                else {
                                                    inputList[i].checked = false;
                                                }
                                            }

                                        }
                                    }

                            ";
        }
        //ltScriptHere.Text
        _strCommonJS = @"
                           
                               
                                    function abc() {
                                        var b = document.getElementById('" + lnkSearch.ClientID + @"');
                                        if (b && typeof (b.click) == 'undefined') {
                                            b.click = function () {
                                                var result = true;
                                                if (b.onclick) result = b.onclick();
                                                if (typeof (result) == 'undefined' || result) {
                                                    eval(b.getAttribute('href'));
                                                }
                                            }
                                        }

                                    }

//                                    function AddClick() {
//
//                                        $('#ctl00_HomeContentPlaceHolder_rlOne_btnAdd').trigger('click');
//                                    }

                              
                                   " + strMainListJS + @"



                           
                            ";

    }

    protected void PopulateDynamicControls()
    {
        if (_dtDynamicSearchColumns == null)
            return;


        if (_dtDynamicSearchColumns != null && _dtDynamicSearchColumns.Rows.Count == 0)
            pnlSearch.Visible = false;

        if(DisplayInColumnID!=null)
        {
            pnlSearch.Visible = false;
        }

        foreach (DataRow dr in _dtDynamicSearchColumns.Rows)
        {
            int iOneColumnConCount = 0;

            if (dr["ColumnType"].ToString() == "number" || dr["ColumnType"].ToString() == "calculation")
            {

                string strValue = "";
                string strOperator = "=";
                string strWaterViewmark = " ";
                if (_xmlView_FC_Doc != null)
                {
                    TextBox txtLowerLimit = (TextBox)tblSearchControls.FindControl("txtLowerLimit_" + dr["SystemName"].ToString());
                    strValue = GetValueFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());
                    strOperator = GetOperatorFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());
                    string strLowerValue = "";
                    string strUpperValue = "";
                    if (strValue != null && strValue.IndexOf("____") > -1)
                    {
                        strLowerValue = strValue.Substring(0, strValue.IndexOf("____"));
                        strUpperValue = strValue.Substring(strValue.IndexOf("____") + 4);
                    }
                    strWaterViewmark = GetMultipleCondition(_xmlView_FC_Doc, dr["ColumnID"].ToString(), ref iOneColumnConCount, "");
                    if (iOneColumnConCount < 2 && ThisIsOR(strWaterViewmark) == false)
                    {
                        if ((strOperator == "=" && _strEqualOrGreaterOperator == "=") || strOperator == "between")
                        {
                            TextBox txtUpperLimit = (TextBox)tblSearchControls.FindControl("txtUpperLimit_" + dr["SystemName"].ToString());
                            txtLowerLimit.Text = strLowerValue;
                            txtUpperLimit.Text = strUpperValue;

                            //ViewState[txtLowerLimit.ID + dr["ColumnID"].ToString()] = strLowerValue;
                            //ViewState[txtUpperLimit.ID + dr["ColumnID"].ToString()] = strUpperValue;

                        }
                    }
                    TextBoxWatermarkExtender twmNumberLowerLimit = (TextBoxWatermarkExtender)tblSearchControls.FindControl("twmNumberLowerLimit_" + dr["SystemName"].ToString());
                    twmNumberLowerLimit.WatermarkText = strWaterViewmark == "" ? " " : strWaterViewmark;
                    txtLowerLimit.ToolTip = strWaterViewmark;
                }
            }

            else if (dr["ColumnType"].ToString() == "text")
            {


                string strValue = "";
                string strOperator = "=";
                string strWaterViewmark = " ";

                if (_xmlView_FC_Doc != null)
                {
                    TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                    strValue = GetValueFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());
                    strOperator = GetOperatorFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());

                    strWaterViewmark = GetMultipleCondition(_xmlView_FC_Doc, dr["ColumnID"].ToString(), ref iOneColumnConCount, "");

                    if (iOneColumnConCount < 2 && strValue != null && strOperator == "=" && ThisIsOR(strWaterViewmark) == false)
                    {
                        txtSearch.Text = strValue;
                        //ViewState[txtSearch.ID + dr["ColumnID"].ToString()] = strValue;
                    }
                    TextBoxWatermarkExtender twmTextSearch = (TextBoxWatermarkExtender)tblSearchControls.FindControl("twmTextSearch_" + dr["SystemName"].ToString());
                    twmTextSearch.WatermarkText = strWaterViewmark == "" ? " " : strWaterViewmark;
                    txtSearch.ToolTip = strWaterViewmark;
                }
            }
            else if (dr["ColumnType"].ToString() == "date")
            {

                string strValue = "";
                string strOperator = "=";
                string strWaterViewmark = " ";

                if (_xmlView_FC_Doc != null)
                {
                    TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                    strValue = GetValueFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());

                    strOperator = GetOperatorFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());
                    string strLowerValue = "";
                    string strUpperValue = "";
                    if (strValue != null && strValue.IndexOf("____") > -1)
                    {
                        strLowerValue = strValue.Substring(0, strValue.IndexOf("____"));
                        strUpperValue = strValue.Substring(strValue.IndexOf("____") + 4);
                    }

                    strWaterViewmark = GetMultipleCondition(_xmlView_FC_Doc, dr["ColumnID"].ToString(), ref iOneColumnConCount, "");

                    if (iOneColumnConCount < 2 && ThisIsOR(strWaterViewmark) == false)
                    {
                        if ((strOperator == "=" && _strEqualOrGreaterOperator == "=") || strOperator == "between")
                        {
                            TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());
                            txtLowerDate.Text = strLowerValue;
                            txtUpperDate.Text = strUpperValue;

                            //ViewState[txtLowerDate.ID + dr["ColumnID"].ToString()] = strLowerValue;
                            //ViewState[txtUpperDate.ID + dr["ColumnID"].ToString()] = strUpperValue;
                        }
                    }

                    if (strWaterViewmark.Trim() != "")
                    {
                        TextBoxWatermarkExtender twmLowerDate = (TextBoxWatermarkExtender)tblSearchControls.FindControl("twmLowerDate_" + dr["SystemName"].ToString());
                        twmLowerDate.WatermarkText = strWaterViewmark;
                        txtLowerDate.ToolTip = strWaterViewmark;
                    }
                }
            }
            else if (dr["ColumnType"].ToString() == "datetime")
            {

                string strValue = "";

                string strOperator = "=";
                string strWaterViewmark = " ";

                if (_xmlView_FC_Doc != null)
                {

                    TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                    TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());
                    TextBoxWatermarkExtender twmLowerDate = (TextBoxWatermarkExtender)tblSearchControls.FindControl("twmLowerDate_" + dr["SystemName"].ToString());

                    TextBox txtLowerTime = (TextBox)tblSearchControls.FindControl("txtLowerTime_" + dr["SystemName"].ToString());
                    TextBox txtUpperTime = (TextBox)tblSearchControls.FindControl("txtUpperTime_" + dr["SystemName"].ToString());

                    strValue = GetValueFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());
                    strOperator = GetOperatorFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());
                    string strLowerDatetime = "";
                    string strUpperDateTime = "";

                    if (strValue != null && strValue.IndexOf("____") > -1)
                    {
                        strLowerDatetime = strValue.Substring(0, strValue.IndexOf("____"));
                        strUpperDateTime = strValue.Substring(strValue.IndexOf("____") + 4);

                    }

                    strWaterViewmark = GetMultipleCondition(_xmlView_FC_Doc, dr["ColumnID"].ToString(), ref iOneColumnConCount, "");
                    if (iOneColumnConCount < 2 && ThisIsOR(strWaterViewmark) == false)
                    {
                        if ((strOperator == "=" && _strEqualOrGreaterOperator == "=") || strOperator == "between")
                        {
                            if (strLowerDatetime.IndexOf(" ") > -1)
                            {
                                string strTempTime = "";
                                txtLowerDate.Text = strLowerDatetime.Substring(0, strLowerDatetime.IndexOf(" "));
                                strTempTime = strLowerDatetime.Substring(strLowerDatetime.IndexOf(" ") + 1);
                                if (strTempTime.Length == 5)
                                    strTempTime = strTempTime + ":00";
                                txtLowerTime.Text = strTempTime;
                                //ViewState[txtLowerDate.ID + dr["ColumnID"].ToString()] = txtLowerDate.Text;
                                //ViewState[txtLowerTime.ID + dr["ColumnID"].ToString()] = txtLowerTime.Text;
                            }

                            if (strUpperDateTime.IndexOf(" ") > -1)
                            {
                                string strTempTime = "";
                                txtUpperDate.Text = strUpperDateTime.Substring(0, strUpperDateTime.IndexOf(" "));
                                strTempTime = strUpperDateTime.Substring(strUpperDateTime.IndexOf(" ") + 1);

                                if (strTempTime.Length == 5)
                                    strTempTime = strTempTime + ":00";
                                txtUpperTime.Text = strTempTime;
                                //ViewState[txtUpperDate.ID + dr["ColumnID"].ToString()] = txtUpperDate.Text;
                                //ViewState[txtUpperTime.ID + dr["ColumnID"].ToString()] = txtUpperTime.Text;
                            }
                        }
                    }

                    if (strWaterViewmark.Trim() != "")
                    {
                        twmLowerDate.WatermarkText = strWaterViewmark;
                        txtLowerDate.ToolTip = strWaterViewmark;
                    }
                }
            }
            else if (dr["ColumnType"].ToString() == "time")
            {

                string strValue = "";
                string strOperator = "=";
                string strWaterViewmark = " ";

                if (_xmlView_FC_Doc != null)
                {
                    TextBox txtLowerTime = (TextBox)tblSearchControls.FindControl("txtLowerTime_" + dr["SystemName"].ToString());
                    TextBox txtUpperTime = (TextBox)tblSearchControls.FindControl("txtUpperTime_" + dr["SystemName"].ToString());

                    strValue = GetValueFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());
                    strOperator = GetOperatorFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());
                    string strLowertime = "";
                    string strUpperTime = "";

                    if (strValue != null && strValue.IndexOf("____") > -1)
                    {
                        string strTempTime = "";
                        strTempTime = strValue.Substring(0, strValue.IndexOf("____"));


                        if (strTempTime.Length == 5)
                            strTempTime = strTempTime + ":00";
                        strLowertime = strTempTime;

                        strTempTime = strValue.Substring(strValue.IndexOf("____") + 4);
                        if (strTempTime.Length == 5)
                            strTempTime = strTempTime + ":00";
                        strUpperTime = strTempTime;
                    }

                    strWaterViewmark = GetMultipleCondition(_xmlView_FC_Doc, dr["ColumnID"].ToString(), ref iOneColumnConCount, "");
                    if (iOneColumnConCount < 2 && ThisIsOR(strWaterViewmark) == false)
                    {
                        if ((strOperator == "=" && _strEqualOrGreaterOperator == "=") || strOperator == "between")
                        {
                            txtLowerTime.Text = strLowertime;
                            txtUpperTime.Text = strUpperTime;

                            //ViewState[txtLowerTime.ID + dr["ColumnID"].ToString()] = txtLowerTime.Text;
                            //ViewState[txtUpperTime.ID + dr["ColumnID"].ToString()] = txtUpperTime.Text;
                        }
                    }
                    TextBoxWatermarkExtender twmLowerTime = (TextBoxWatermarkExtender)tblSearchControls.FindControl("twmTime_" + dr["SystemName"].ToString());

                    twmLowerTime.WatermarkText = strWaterViewmark.Trim() == "" ? "HH:mm:ss" : strWaterViewmark;
                    txtLowerTime.ToolTip = strWaterViewmark;
                }


            }
            else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "values"
                || dr["DropDownType"].ToString() == "value_text"))
            {
                DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                string strVT = "";
                if (dr["DropDownType"].ToString() == "values")
                {
                    Common.PutDDLValues(dr["DropdownValues"].ToString(), ref ddlSearch);
                }
                else
                {
                    strVT = dr["DropdownValues"].ToString();
                    Common.PutDDLValue_Text(dr["DropdownValues"].ToString(), ref ddlSearch);
                }

                string strValue = "";
                string strOperator = "=";
                string strWaterViewmark = " ";

                if (_xmlView_FC_Doc != null)
                {
                    strValue = GetValueFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());
                    strOperator = GetOperatorFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());

                    strWaterViewmark = GetMultipleCondition(_xmlView_FC_Doc, dr["ColumnID"].ToString(), ref iOneColumnConCount, strVT);

                    if (strWaterViewmark != "")
                    {
                        if (ddlSearch.Items.Count > 0)
                        {
                            ddlSearch.Items.RemoveAt(0);

                            ListItem liViewCondition = new ListItem(strWaterViewmark, "");
                            ddlSearch.Items.Insert(0, liViewCondition);

                            ListItem liSelect = new ListItem("--Show All--", "vf_vf_vf");
                            ddlSearch.Items.Insert(1, liSelect);

                            ddlSearch.ToolTip = strWaterViewmark;
                        }
                    }


                    if (iOneColumnConCount < 2 && strValue != null && strOperator == "=" && ThisIsOR(strWaterViewmark) == false)
                    {
                        if (ddlSearch.Items.FindByValue(strValue) != null)
                        {
                            ddlSearch.SelectedValue = strValue;
                            ViewState[ddlSearch.ID + dr["ColumnID"].ToString()] = strValue;
                        }
                    }
                }

            }
            //Red Ticket 3611
            else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "table" || dr["DropDownType"].ToString() == "tabledd") &&
            dr["TableTableID"] != DBNull.Value && dr["DisplayColumn"].ToString() != "")
            {
                // lets get the Default value from ViewEdit... - Red
                PopulateDropDownSearchOnLoad(int.Parse(dr["ColumnID"].ToString()));
                
            }
            else if (dr["ColumnType"].ToString() == "radiobutton" || dr["ColumnType"].ToString() == "checkbox" ||
                dr["ColumnType"].ToString() == "listbox")
            {

                DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                string strVT = "";

                if (dr["ColumnType"].ToString() == "radiobutton")
                {
                    if (dr["DropDownType"].ToString() == "values")
                    {
                        TheDatabase.PutDDLValues(dr["DropdownValues"].ToString(), ref ddlSearch);
                    }
                    else if (dr["DropDownType"].ToString() == "value_text")
                    {
                        strVT = dr["DropdownValues"].ToString();
                        TheDatabase.PutDDLValue_Text(dr["DropdownValues"].ToString(), ref ddlSearch);
                    }
                    else
                    {
                        //strVT
                        Common.PutRadioImageInto_DDL(dr["DropdownValues"].ToString(), ref ddlSearch);
                    }

                }
                else if (dr["ColumnType"].ToString() == "listbox")
                {
                    if (dr["DropDownType"].ToString() == "values")
                    {
                        TheDatabase.PutDDLValues(dr["DropdownValues"].ToString(), ref ddlSearch);
                    }
                    else if (dr["DropDownType"].ToString() == "value_text")
                    {
                        strVT = dr["DropdownValues"].ToString();
                        TheDatabase.PutDDLValue_Text(dr["DropdownValues"].ToString(), ref ddlSearch);
                    }
                    else
                    {
                        if (dr["DropDownType"].ToString() == "table" && dr["TableTableID"] != DBNull.Value
                          && dr["DisplayColumn"].ToString() != "") //&& dr["LinkedParentColumnID"] != DBNull.Value
                        {
                            strVT = "ColumnID";
                            RecordManager.PopulateTableDropDown(int.Parse(dr["ColumnID"].ToString()), ref ddlSearch, "", "", Boolean.Parse(dr["ShowAllValuesOnSearch"].ToString())); //3611
                        }
                    }
                }
                else
                {
                    TheDatabase.PutCheckboxIntoDDL(dr["DropdownValues"].ToString(), ref ddlSearch);
                }


                string strValue = "";
                string strOperator = "=";
                string strWaterViewmark = " ";

                if (_xmlView_FC_Doc != null)
                {
                    strValue = GetValueFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());
                    strOperator = GetOperatorFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());

                    strWaterViewmark = GetMultipleCondition(_xmlView_FC_Doc, dr["ColumnID"].ToString(), ref iOneColumnConCount, strVT);
                    if (strWaterViewmark != "")
                    {
                        if (ddlSearch.Items.Count > 0)
                        {
                            ddlSearch.Items.RemoveAt(0);

                            ListItem liViewCondition = new ListItem(strWaterViewmark, "");
                            ddlSearch.Items.Insert(0, liViewCondition);
                            ListItem liSelect = new ListItem("--Show All--", "vf_vf_vf");
                            ddlSearch.Items.Insert(1, liSelect);

                            ddlSearch.ToolTip = strWaterViewmark;
                        }
                    }


                    if (iOneColumnConCount < 2 && strValue != null && strOperator == "=" && ThisIsOR(strWaterViewmark) == false)
                    {
                        if (!IsPostBack)
                        {
                            if (ddlSearch.Items.FindByValue(strValue) != null)
                            {
                                ddlSearch.SelectedValue = strValue;
                                //ViewState[ddlSearch.ID + dr["ColumnID"].ToString()] = strValue;
                            }
                        }

                    }
                }

            }
            else
            {

                TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                TextBoxWatermarkExtender twmTextSearch = (TextBoxWatermarkExtender)tblSearchControls.FindControl("twmTextSearch_" + dr["SystemName"].ToString());
                string strValue = "";
                string strOperator = "=";
                string strWaterViewmark = " ";

                if (_xmlView_FC_Doc != null)
                {
                    strValue = GetValueFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());
                    strOperator = GetOperatorFromViewFilter(_xmlView_FC_Doc, dr["ColumnID"].ToString());

                    strWaterViewmark = GetMultipleCondition(_xmlView_FC_Doc, dr["ColumnID"].ToString(), ref iOneColumnConCount, "");

                    if (iOneColumnConCount < 2 && strValue != null && strOperator == "=" && ThisIsOR(strWaterViewmark) == false)
                    {
                        txtSearch.Text = strValue;
                        //ViewState[txtSearch.ID + dr["ColumnID"].ToString()] = strValue;
                    }

                    twmTextSearch.WatermarkText = strWaterViewmark == "" ? " " : strWaterViewmark;
                    txtSearch.ToolTip = strWaterViewmark;
                    //txtSearch.Text = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                }
            }


        }


    }


    protected void CreateDynamicControls()
    {
        string strOnlyAdmin = "";
        //do we need showwhen in the list

        //if (!Common.HaveAccess(_strRecordRightID, "1,2"))
        //{
        //    strOnlyAdmin = " AND (OnlyForAdmin=" + _theRole.RoleID.ToString() + " OR OnlyForAdmin IS NULL OR OnlyForAdmin=0 ) ";
        //}

        if (_theView == null)
        {
            _dtDynamicSearchColumns = Common.DataTableFromText("SELECT top 5 C.*,c.DisplayTextSummary as Heading FROM [Column] C WHERE TableID=" + TableID.ToString()
                + strOnlyAdmin + " AND ColumnID NOT IN (SELECT ColumnID FROM SearchGroupColumn SGC INNER JOIN SearchGroup SG " +
                     " ON SGC.SearchGroupID=SG.SearchGroupID WHERE TableID=" + TableID.ToString() + " ) ORDER BY DisplayOrder");
        }
        else
        {

            if ((bool)_theView.ShowSearchFields == false)
            {
                strOnlyAdmin = strOnlyAdmin + " AND C.ColumnID=-1 ";
                //if (Request.QueryString["Dashboard"] != null)
                //{
                //    pnlSearch.Visible = false;
                //}
            }

            /* === Red 05/04/2019: Added ColumnIndex here === */
            _dtDynamicSearchColumns = Common.DataTableFromText(@"SELECT C.*,ISNULL(C.DisplayTextSummary,C.DisplayName) as Heading, VT.ColumnIndex FROM [Column] C 
                INNER JOIN ViewItem VT ON C.ColumnID=VT.ColumnID
                WHERE VT.ViewID=" + _theView.ViewID.ToString() + @" AND C.TableID=" + TableID.ToString() + strOnlyAdmin +
                  @" AND VT.SearchField=1  AND VT.ColumnID NOT IN 
                 (SELECT ColumnID FROM SearchGroupColumn SGC INNER JOIN SearchGroup SG 
                  ON SGC.SearchGroupID=SG.SearchGroupID WHERE TableID=" + TableID.ToString() + @" ) ORDER BY ColumnIndex");



        }





        _dtSearchGroup = Common.DataTableFromText("SELECT SearchGroupID,GroupName FROM SearchGroup WHERE SummarySearch=1  AND TableID=" + TableID.ToString() + " ORDER BY DisplayOrder");





        _xmlView_FC_Doc = new System.Xml.XmlDocument();

        if (_theView.Filter != "" && _theView.FilterControlsInfo != "")
        {

            _xmlView_FC_Doc.Load(new StringReader(_theView.FilterControlsInfo));

        }

        HtmlTableRow theRow = new HtmlTableRow();
        int s = 0;

        _strJSDynamicShowHide = "";

        foreach (DataRow dr in _dtDynamicSearchColumns.Rows)
        {
            int iOneColumnConCount = 0;

            if (dr["ColumnType"].ToString() == "number" || dr["ColumnType"].ToString() == "calculation")
            {

                if (s == 0)
                {
                    HtmlTableCell cellBoxS = new HtmlTableCell();
                    cellBoxS.Controls.Add(new LiteralControl("<br/><strong>Search:</strong>"));
                    cellBoxS.Attributes.Add("class", "first-cell");
                    theRow.Cells.Add(cellBoxS);
                }

                s = s + 1;
                TextBox txtLowerLimit = new TextBox();
                txtLowerLimit.ID = "txtLowerLimit_" + dr["SystemName"].ToString();
                txtLowerLimit.CssClass = "NormalTextBox";
                txtLowerLimit.Width = 105;
                txtLowerLimit.Attributes.Add("onblur", "this.value=this.value.trim()");

                TextBox txtUpperLimit = new TextBox();
                txtUpperLimit.ID = "txtUpperLimit_" + dr["SystemName"].ToString();
                txtUpperLimit.CssClass = "NormalTextBox";
                txtUpperLimit.Width = 105;
                txtUpperLimit.Attributes.Add("placeholder", "To Upper Limit");
                txtUpperLimit.Attributes.Add("onblur", "this.value=this.value.trim()");


                TextBoxWatermarkExtender twmNumberLowerLimit = new TextBoxWatermarkExtender();
                twmNumberLowerLimit.ID = "twmNumberLowerLimit_" + dr["SystemName"].ToString();
                twmNumberLowerLimit.TargetControlID = txtLowerLimit.ID;
                twmNumberLowerLimit.WatermarkText = " ";
                twmNumberLowerLimit.WatermarkCssClass = "MaskText";


                HyperLink hlSetDateRange = new HyperLink();
                hlSetDateRange.ID = "hlSetDateRange_" + dr["SystemName"].ToString();
                hlSetDateRange.Text = "Set range";
                hlSetDateRange.Style.Add("cursor", "pointer");

                HiddenField hfSetDateRange = new HiddenField();
                hfSetDateRange.ID = "hfSetDateRange_" + dr["SystemName"].ToString();
                hfSetDateRange.Value = "0";


                RegularExpressionValidator revNumberLowerLimit = new RegularExpressionValidator();
                revNumberLowerLimit.ID = "revNumberLowerLimit_" + dr["SystemName"].ToString();
                revNumberLowerLimit.ControlToValidate = txtLowerLimit.ID;
                revNumberLowerLimit.ErrorMessage = "*";
                revNumberLowerLimit.ToolTip = "Number only!";
                revNumberLowerLimit.ValidationExpression = @"(^-?\d{1,20}\.$)|(^-?\d{1,20}$)|(^-?\d{0,20}\.\d{1,10}$)";

                RegularExpressionValidator revNumberUpperLimit = new RegularExpressionValidator();
                revNumberUpperLimit.ID = "revNumberUpperLimit_" + dr["SystemName"].ToString();
                revNumberUpperLimit.ControlToValidate = txtUpperLimit.ID;
                revNumberUpperLimit.ErrorMessage = "*";
                revNumberUpperLimit.ToolTip = "Number only!";
                revNumberUpperLimit.ValidationExpression = @"(^-?\d{1,20}\.$)|(^-?\d{1,20}$)|(^-?\d{0,20}\.\d{1,10}$)";

                HtmlTableCell cellBox = new HtmlTableCell();
                cellBox.Controls.Add(new LiteralControl(String.Format("<label>{0}</label>", String.IsNullOrEmpty(dr["Heading"].ToString()) ? dr["DisplayName"] : dr["Heading"])));


                HtmlTable tblDate = new HtmlTable();
                HtmlTableRow rowOne = new HtmlTableRow();
                HtmlTableRow rowTwo = new HtmlTableRow();
                HtmlTableRow rowThree = new HtmlTableRow();

                rowOne.ID = "numrowOne_" + dr["SystemName"].ToString();
                rowTwo.ID = "numrowTwo_" + dr["SystemName"].ToString();
                rowThree.ID = "rowThree_" + dr["SystemName"].ToString();


                tblDate.CellPadding = 0;
                tblDate.CellSpacing = 0;


                HtmlTableCell cellBox1 = new HtmlTableCell();
                HtmlTableCell cellBox12 = new HtmlTableCell();
                HtmlTableCell cellBox2 = new HtmlTableCell();
                HtmlTableCell cellBox3 = new HtmlTableCell();
                HtmlTableCell cellBox22 = new HtmlTableCell();


                cellBox1.Controls.Add(txtLowerLimit);
                cellBox12.Controls.Add(revNumberLowerLimit);
                cellBox12.Controls.Add(twmNumberLowerLimit);

                //cellBox.Controls.Add(new LiteralControl("</br>"));
                txtUpperLimit.Style.Add("margin-top", "5px");


                cellBox2.Controls.Add(txtUpperLimit);
                cellBox22.Controls.Add(revNumberUpperLimit);
                cellBox3.Controls.Add(hlSetDateRange);
                cellBox3.Controls.Add(hfSetDateRange);

                //cellBox22.Controls.Add(twmNumberUpperLimit);

                rowOne.Cells.Add(cellBox1);
                rowOne.Cells.Add(cellBox12);

                rowTwo.Cells.Add(cellBox2);
                rowTwo.Cells.Add(cellBox22);
                rowThree.Cells.Add(cellBox3);


                tblDate.Rows.Add(rowOne);
                tblDate.Rows.Add(rowTwo);
                tblDate.Rows.Add(rowThree);
                cellBox.Controls.Add(tblDate);





                theRow.Cells.Add(cellBox);


                //                strJSDynamicShowHide = strJSDynamicShowHide + @"
                //                        $('#" + _strDynamictabPart + txtLowerLimit.ID + @"').on('keyup',function () {
                //                                                                var strLowerValue = $('#" + _strDynamictabPart + txtLowerLimit.ID + @"').val();
                //                                                                 if (strLowerValue.trim() != '') {
                //                                                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').show();
                //                                                                }
                //                                                                else {
                //                                                                     $('#" + _strDynamictabPart + txtUpperLimit.ID + @"').val('');
                //                                                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').hide(); 
                //                                                                }
                //                                                            });
                //                         $('#" + _strDynamictabPart + txtLowerLimit.ID + @"').trigger('keyup');
                //
                //                    ";



                //if (strLowerValue.trim() != '') {     and style.display<>'none' if we want to avoid
                _strJSDynamicShowHide = _strJSDynamicShowHide + @"
                    function ShowHideUP_" + _strDynamictabPart + txtUpperLimit.ID + @"(keepvalue) {  
                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').hide();
                                    $('#" + _strDynamictabPart + rowThree.ID + @"').hide();                                                               
                            try
                            {
                                    var strLowerValue = $('#" + _strDynamictabPart + txtLowerLimit.ID + @"').val();
                                    var strUpperValue = $('#" + _strDynamictabPart + txtUpperLimit.ID + @"').val();
                                      strLowerValue=strLowerValue.trim();
                                    strUpperValue=strUpperValue.trim();
                                        if(isNaN(strLowerValue)==true)
                                            {strLowerValue=''};
                                         if(isNaN(strUpperValue)==true)
                                            {strUpperValue=''};                            
                                    if (strLowerValue.trim() != '') {   

                                        if (strUpperValue.trim() == '' && keepvalue=='0') {
                                          $('#" + _strDynamictabPart + txtUpperLimit.ID + @"').val(strLowerValue);
                                            strUpperValue = strLowerValue;
                                        }  
  
                                        /* =================================================
                                        Red: changing the value from higher to lower, 
                                        upper value should always follow the lower value 
                                        when the user did not set the range yet
                                        ==================================================== */  
                                        if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '0') { 
                                            
                                            $('#" + _strDynamictabPart + txtUpperLimit.ID + @"').val(strLowerValue);  
                                            if (keepvalue == '1') {
                                             if (strUpperValue.trim() != '' && strUpperValue != strLowerValue) {
                                                $('#" + _strDynamictabPart + txtUpperLimit.ID + @"').val(strUpperValue);  
                                                }
                                            }
                                        }      
                                        
                                            if (parseFloat(strLowerValue) > parseFloat(strUpperValue)) { 
                                         $('#" + _strDynamictabPart + txtUpperLimit.ID + @"').val(strLowerValue);   
                                         strUpperValue = strLowerValue;
                                         }   

                                if (strUpperValue.trim() != '' && strUpperValue == strLowerValue) { 
                                    if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '0') { 
                                    $('#" + _strDynamictabPart + rowThree.ID + @"').show(); }
                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').hide();
                                }
                                else if (strUpperValue.trim() != '' && strUpperValue != strLowerValue) { 
                                      $('#" + _strDynamictabPart + rowThree.ID + @"').show();
                                                        if (keepvalue == '1') {   
                                                                $('#" + _strDynamictabPart + rowTwo.ID + @"').show(); 
                                                                $('#" + _strDynamictabPart + rowThree.ID + @"').hide();    
                                                                $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('1');
                                                              }
                                }

                     
                                        //$('#" + _strDynamictabPart + rowTwo.ID + @"').show();
                                        // var strUpperValue = $('#" + _strDynamictabPart + txtUpperLimit.ID + @"').val();
                                         
                                                
                                    }
                                    else {
                                            $('#" + _strDynamictabPart + txtUpperLimit.ID + @"').val('');
                                            $('#" + _strDynamictabPart + rowTwo.ID + @"').hide();
                                            $('#" + _strDynamictabPart + rowThree.ID + @"').hide();
                                    }

                            if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '1') {
                                $('#" + _strDynamictabPart + rowTwo.ID + @"').show();
                                $('#" + _strDynamictabPart + rowThree.ID + @"').hide();
                                 if (strLowerValue.trim() == '') {   $('#" + _strDynamictabPart + rowTwo.ID + @"').hide();}
                            } 
                            }
                            catch(err)
                            {
                                 //
                            }
                         };

                          
                            $('#" + _strDynamictabPart + txtLowerLimit.ID + @"').on('keyup',function () {
                                    ShowHideUP_" + _strDynamictabPart + txtUpperLimit.ID + @"('0');                                                          
                            });
                             $('#" + _strDynamictabPart + txtLowerLimit.ID + @"').change(function () {
                                    ShowHideUP_" + _strDynamictabPart + txtUpperLimit.ID + @"('0');                                                          
                            });

                            $('#" + _strDynamictabPart + txtUpperLimit.ID + @"').change(function () {                                                
                            ShowHideUP_" + _strDynamictabPart + txtUpperLimit.ID + @"('0');                                                          
                            });  
                            $('#" + _strDynamictabPart + lnkReset.ID + @"').click(function () {    
                                $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('0');                                              
                            });   
                         
                        
                            ShowHideUP_" + _strDynamictabPart + txtUpperLimit.ID + @"('1');  
                            $('#" + _strDynamictabPart + hlSetDateRange.ID + @"').click(function(){
                                $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('1');
                            ShowHideUP_" + _strDynamictabPart + txtUpperLimit.ID + @"('0'); 
                    });

                                ";



            }

            else if (dr["ColumnType"].ToString() == "text")
            {

                if (s == 0)
                {
                    HtmlTableCell cellBoxS = new HtmlTableCell();
                    cellBoxS.Controls.Add(new LiteralControl("<br/><strong>Search:</strong>"));
                    cellBoxS.Attributes.Add("class", "first-cell");
                    theRow.Cells.Add(cellBoxS);
                }

                s = s + 1;
                TextBox txtSearch = new TextBox();
                txtSearch.ID = "txtSearch_" + dr["SystemName"].ToString();
                txtSearch.CssClass = "NormalTextBox";
                txtSearch.Width = 105;
                txtSearch.Attributes.Add("onblur", "this.value=this.value.trim()");

                TextBoxWatermarkExtender twmTextSearch = new TextBoxWatermarkExtender();
                twmTextSearch.ID = "twmTextSearch_" + dr["SystemName"].ToString();
                twmTextSearch.TargetControlID = txtSearch.ID;
                twmTextSearch.WatermarkText = " ";
                twmTextSearch.WatermarkCssClass = "MaskText";

                HtmlTableCell cellBox = new HtmlTableCell();
                cellBox.Controls.Add(new LiteralControl(String.Format("<label>{0}</label>", String.IsNullOrEmpty(dr["Heading"].ToString()) ? dr["DisplayName"] : dr["Heading"])));
                cellBox.Controls.Add(txtSearch);
                cellBox.Controls.Add(twmTextSearch);

                theRow.Cells.Add(cellBox);
            }
            else if (dr["ColumnType"].ToString() == "date")
            {

                if (s == 0)
                {
                    HtmlTableCell cellBoxS = new HtmlTableCell();
                    cellBoxS.Controls.Add(new LiteralControl("<br/><strong>Search:</strong>"));
                    cellBoxS.Attributes.Add("class", "first-cell");
                    theRow.Cells.Add(cellBoxS);
                }

                s = s + 1;
                TextBox txtLowerDate = new TextBox();
                txtLowerDate.ID = "txtLowerDate_" + dr["SystemName"].ToString();
                txtLowerDate.CssClass = "NormalTextBox";
                txtLowerDate.Width = 105;
                txtLowerDate.AutoCompleteType = AutoCompleteType.Disabled;
                txtLowerDate.Attributes.Add("onblur", "this.value=this.value.trim()");
                txtLowerDate.ToolTip = Common.ToolTip_Today;

                TextBox txtUpperDate = new TextBox();
                txtUpperDate.ID = "txtUpperDate_" + dr["SystemName"].ToString();
                txtUpperDate.CssClass = "NormalTextBox";
                txtUpperDate.Width = 105;
                txtUpperDate.AutoCompleteType = AutoCompleteType.Disabled;
                txtUpperDate.Attributes.Add("placeholder", "To dd/mm/yyyy");
                //txtUpperDate.Attributes.Add("onfocus", "if(this.value=='To dd/mm/yyyy') {this.value = '';this.style.color='black';}");
                txtUpperDate.Attributes.Add("onblur", "this.value=this.value.trim()");
                txtUpperDate.ToolTip = Common.ToolTip_Today;

                ImageButton imgLowerDate = new ImageButton();
                imgLowerDate.ID = "imgLowerDate_" + dr["SystemName"].ToString();
                imgLowerDate.ImageUrl = "~/Images/Calendar.png";
                imgLowerDate.Style.Add("padding-left", "3px");
                imgLowerDate.CausesValidation = false;


                CalendarExtender ceLowerDate = new CalendarExtender();
                ceLowerDate.ID = "ceLowerDate_" + dr["SystemName"].ToString();
                ceLowerDate.TargetControlID = txtLowerDate.ID;
                ceLowerDate.Format = "dd/MM/yyyy";
                ceLowerDate.PopupButtonID = imgLowerDate.ID;
                ceLowerDate.FirstDayOfWeek = FirstDayOfWeek.Monday;


                TextBoxWatermarkExtender twmLowerDate = new TextBoxWatermarkExtender();
                twmLowerDate.ID = "twmLowerDate_" + dr["SystemName"].ToString();
                twmLowerDate.TargetControlID = txtLowerDate.ID;
                twmLowerDate.WatermarkText = "dd/mm/yyyy";
                twmLowerDate.WatermarkCssClass = "MaskText";







                ImageButton imgUpperDate = new ImageButton();
                imgUpperDate.ID = "imgUpperDate_" + dr["SystemName"].ToString();
                imgUpperDate.ImageUrl = "~/Images/Calendar.png";
                imgUpperDate.Style.Add("padding-left", "3px");
                imgUpperDate.CausesValidation = false;


                CalendarExtender ceUpperDate = new CalendarExtender();
                ceUpperDate.ID = "ceUpperDate_" + dr["SystemName"].ToString();
                ceUpperDate.TargetControlID = txtUpperDate.ID;
                ceUpperDate.Format = "dd/MM/yyyy";
                ceUpperDate.PopupButtonID = imgUpperDate.ID;
                ceUpperDate.FirstDayOfWeek = FirstDayOfWeek.Monday;

                HyperLink hlSetDateRange = new HyperLink();
                hlSetDateRange.ID = "hlSetDateRange_" + dr["SystemName"].ToString();
                hlSetDateRange.Text = "Set date range";
                hlSetDateRange.Style.Add("cursor","pointer");

                HiddenField hfSetDateRange = new HiddenField();
                hfSetDateRange.ID = "hfSetDateRange_" + dr["SystemName"].ToString();
                hfSetDateRange.Value = "0";
                
                


                //TextBoxWatermarkExtender twmUpperDate = new TextBoxWatermarkExtender();
                //twmUpperDate.ID = "twmUpperDate_" + dr["SystemName"].ToString();
                //twmUpperDate.TargetControlID = txtUpperDate.ID;
                //twmUpperDate.WatermarkText = "To dd/mm/yyyy";
                //twmUpperDate.WatermarkCssClass = "MaskText";
                //twmUpperDate.BehaviorID = _strDynamictabPart + "BIDtwmUpperDate_" + dr["SystemName"].ToString();



                HtmlTable tblDate = new HtmlTable();
                HtmlTableRow rowOne = new HtmlTableRow();
                HtmlTableRow rowTwo = new HtmlTableRow();
                HtmlTableRow rowThree = new HtmlTableRow();

                rowOne.ID = "rowOne_" + dr["SystemName"].ToString();
                rowTwo.ID = "rowTwo_" + dr["SystemName"].ToString();
                rowThree.ID = "rowThree_" + dr["SystemName"].ToString();

                tblDate.CellPadding = 0;
                tblDate.CellSpacing = 0;

                HtmlTableCell cellBox = new HtmlTableCell();
                cellBox.Controls.Add(new LiteralControl(String.Format("<label>{0}</label>", String.IsNullOrEmpty(dr["Heading"].ToString()) ? dr["DisplayName"] : dr["Heading"])));

                HtmlTableCell cellBox1 = new HtmlTableCell();
                HtmlTableCell cellBox12 = new HtmlTableCell();
                HtmlTableCell cellBox2 = new HtmlTableCell();
                HtmlTableCell cellBox3 = new HtmlTableCell();
                HtmlTableCell cellBox22 = new HtmlTableCell();

                cellBox1.Controls.Add(txtLowerDate);
                cellBox12.Controls.Add(imgLowerDate);
                cellBox12.Controls.Add(ceLowerDate);
                cellBox12.Controls.Add(twmLowerDate);
                //cellBox12.Controls.Add(rvLowerDate);
                rowOne.Cells.Add(cellBox1);
                rowOne.Cells.Add(cellBox12);
                //cellBox.Controls.Add(new LiteralControl("</br>"));
                txtUpperDate.Style.Add("margin-top", "5px");
                imgUpperDate.Style.Add("margin-top", "5px");

                cellBox2.Controls.Add(txtUpperDate);
                cellBox22.Controls.Add(imgUpperDate);
                cellBox22.Controls.Add(ceUpperDate);
                cellBox3.Controls.Add(hlSetDateRange);
                cellBox3.Controls.Add(hfSetDateRange);

                //cellBox22.Controls.Add(twmUpperDate);
                //cellBox22.Controls.Add(rvUpperDate);

                rowTwo.Cells.Add(cellBox2);
                rowTwo.Cells.Add(cellBox22);
                //rowTwo.Cells.Add(cellBox3);
                rowThree.Cells.Add(cellBox3);




                tblDate.Rows.Add(rowOne);
                tblDate.Rows.Add(rowTwo);
                tblDate.Rows.Add(rowThree);
                cellBox.Controls.Add(tblDate);
                theRow.Cells.Add(cellBox);



                //                strJSDynamicShowHide = strJSDynamicShowHide + @"
                //                        $('#" + _strDynamictabPart + txtLowerDate.ID + @"').on('keyup',function () {
                //                                                                var strLowerValue = $('#" + _strDynamictabPart + txtLowerDate.ID + @"').val();
                //                                                                 if (strLowerValue.trim() != '') {
                //                                                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').show();
                //                                                                }
                //                                                                else {
                //                                                                     $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val('');
                //                                                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').hide(); 
                //                                                                }
                //                                                            });
                //                         $('#" + _strDynamictabPart + txtLowerDate.ID + @"').trigger('keyup');
                //
                //                    ";

                _strJSDynamicShowHide = _strJSDynamicShowHide + @"
                        /* == Red 14032019: Ticket 4392 - More Intelligent Date Filter  ==*/       
                    function ShowHideUP_" + _strDynamictabPart + txtUpperDate.ID + @"(keepvalue) {    
                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').hide();
                                    $('#" + _strDynamictabPart + rowThree.ID + @"').hide();  
                       
                    try
                    {                                                
                            var strLowerValue = $('#" + _strDynamictabPart + txtLowerDate.ID + @"').val();
                            var strUpperValue = $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val();

                            var splittedDateLowerValue = strLowerValue.split(""/"");
                            var splittedDateUpperValue = strUpperValue.split(""/"");
                            var dateLowerValue = new Date(splittedDateLowerValue[1] + ""/"" + splittedDateLowerValue[0] + ""/"" + splittedDateLowerValue[2] );
                            var dateUpperValue = new Date(splittedDateUpperValue[1] + ""/"" + splittedDateUpperValue[0] + ""/"" + splittedDateUpperValue[2] );
                                                
                            if (strLowerValue.trim() == 'dd/mm/yyyy') {strLowerValue = '';}
                            if (strLowerValue.trim() != '') { 
                                if (strUpperValue.trim() == '' && keepvalue=='0') 
                                    {
                                    $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val(strLowerValue);
                                    strUpperValue = strLowerValue;
                                    }     

                                    /* =================================================
                                    Red: changing the value from higher to lower, 
                                    upper value should always follow the lower value 
                                    when the user did not set the range yet
                                    ==================================================== */  
                                        if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '0') { 
                                            $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val(strLowerValue);  
                                            if (keepvalue == '1') {
                                             if (strUpperValue.trim() != '' && strUpperValue != strLowerValue) {
                                                $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val(strUpperValue);  
                                                }
                                            }
                                            }
      
                                if (dateLowerValue > dateUpperValue) { 
                                    $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val(strLowerValue);   
                                    strUpperValue = strLowerValue;
                                }    
                                if (strUpperValue.trim() != '' && strUpperValue == strLowerValue) { 
                                    if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '0') { 
                                    $('#" + _strDynamictabPart + rowThree.ID + @"').show(); }
                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').hide();
                                }
                                else if (strUpperValue.trim() != '' && strUpperValue != strLowerValue) { 
                                     $('#" + _strDynamictabPart + rowThree.ID + @"').show();
                                                    if (keepvalue == '1') {   
                                                                $('#" + _strDynamictabPart + rowTwo.ID + @"').show();
                                                                $('#" + _strDynamictabPart + rowThree.ID + @"').hide();    
                                                                $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('1');
                                                              }
                                }

                                //alert(strLowerValue);
                                //alert(strUpperValue);                                                     
                            }
                            else {
                                    $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val('');
                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').hide();
                                    $('#" + _strDynamictabPart + rowThree.ID + @"').hide();

                            }


                            if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '1') {
                                $('#" + _strDynamictabPart + rowTwo.ID + @"').show();
                                $('#" + _strDynamictabPart + rowThree.ID + @"').hide();
                                 if (strLowerValue.trim() == '') {   $('#" + _strDynamictabPart + rowTwo.ID + @"').hide();}
                            } 


                                                
                    }
                    catch(err)
                    {
                            //
                    }
                    };

                    $('#" + _strDynamictabPart + txtLowerDate.ID + @"').change(function () {
                                                
                            ShowHideUP_" + _strDynamictabPart + txtUpperDate.ID + @"('0');                                                          
                    });

                    /* == Red 14032019: Added script below to check if the entered Upper Date is greater than the Lower Date    ==*/
                    $('#" + _strDynamictabPart + txtUpperDate.ID + @"').change(function () {                                                
                            ShowHideUP_" + _strDynamictabPart + txtUpperDate.ID + @"('0');                                                          
                    });    

                    $('#" + _strDynamictabPart + lnkReset.ID + @"').click(function () {    
                            $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('0');                                              
                    });                                         
                                        
                    //$('#" + _strDynamictabPart + txtLowerDate.ID + @"').on('keyup',function () {
                    //        ShowHideUP_" + _strDynamictabPart + txtUpperDate.ID + @"('0');     
                    //});
                                        
                    ShowHideUP_" + _strDynamictabPart + txtUpperDate.ID + @"('1');  
                                        
                    /* ==================================================== 
                        Red 14032019: Click dynamic hyperlink to hide or show the 'To Date'
                        ==================================================== */
                    $('#" + _strDynamictabPart + hlSetDateRange.ID + @"').click(function(){
                        $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('1');
                        ShowHideUP_" + _strDynamictabPart + txtUpperDate.ID + @"('0'); 
                    });

                        ";

            } // end date
            else if (dr["ColumnType"].ToString() == "datetime")
            {

                if (s == 0)
                {
                    HtmlTableCell cellBoxS = new HtmlTableCell();
                    cellBoxS.Controls.Add(new LiteralControl("<br/><strong>Search:</strong>"));
                    cellBoxS.Attributes.Add("class", "first-cell");
                    theRow.Cells.Add(cellBoxS);
                }

                s = s + 1;
                TextBox txtLowerDate = new TextBox();
                txtLowerDate.ID = "txtLowerDate_" + dr["SystemName"].ToString();
                txtLowerDate.CssClass = "NormalTextBox";
                txtLowerDate.Width = 105;
                txtLowerDate.AutoCompleteType = AutoCompleteType.Disabled;
                txtLowerDate.Attributes.Add("onblur", "this.value=this.value.trim()");
                txtLowerDate.ToolTip = Common.ToolTip_Today;

                TextBox txtUpperDate = new TextBox();
                txtUpperDate.ID = "txtUpperDate_" + dr["SystemName"].ToString();
                txtUpperDate.CssClass = "NormalTextBox";
                txtUpperDate.Width = 105;
                txtUpperDate.AutoCompleteType = AutoCompleteType.Disabled;
                txtUpperDate.Attributes.Add("placeholder", "To dd/mm/yyyy");
                txtUpperDate.Attributes.Add("onblur", "this.value=this.value.trim()");
                txtUpperDate.ToolTip = Common.ToolTip_Today;


                TextBox txtLowerTime = new TextBox();
                txtLowerTime.ID = "txtLowerTime_" + dr["SystemName"].ToString();
                txtLowerTime.CssClass = "NormalTextBox";
                txtLowerTime.Width = 70;
                txtLowerTime.AutoCompleteType = AutoCompleteType.Disabled;
                txtLowerTime.Attributes.Add("placeholder", "HH:mm:ss");

                TextBox txtUpperTime = new TextBox();
                txtUpperTime.ID = "txtUpperTime_" + dr["SystemName"].ToString();
                txtUpperTime.CssClass = "NormalTextBox";
                txtUpperTime.Width = 70;
                txtUpperTime.AutoCompleteType = AutoCompleteType.Disabled;
                txtUpperTime.Attributes.Add("placeholder", "HH:mm:ss");




                ImageButton imgLowerDate = new ImageButton();
                imgLowerDate.ID = "imgLowerDate_" + dr["SystemName"].ToString();
                imgLowerDate.ImageUrl = "~/Images/Calendar.png";
                imgLowerDate.Style.Add("padding-left", "3px");
                imgLowerDate.CausesValidation = false;


                CalendarExtender ceLowerDate = new CalendarExtender();
                ceLowerDate.ID = "ceLowerDate_" + dr["SystemName"].ToString();
                ceLowerDate.TargetControlID = txtLowerDate.ID;
                ceLowerDate.Format = "dd/MM/yyyy";
                ceLowerDate.PopupButtonID = imgLowerDate.ID;
                ceLowerDate.FirstDayOfWeek = FirstDayOfWeek.Monday;



                TextBoxWatermarkExtender twmLowerDate = new TextBoxWatermarkExtender();
                twmLowerDate.ID = "twmLowerDate_" + dr["SystemName"].ToString();
                twmLowerDate.TargetControlID = txtLowerDate.ID;
                twmLowerDate.WatermarkText = "dd/mm/yyyy";
                twmLowerDate.WatermarkCssClass = "MaskText";

                ImageButton imgUpperDate = new ImageButton();
                imgUpperDate.ID = "imgUpperDate_" + dr["SystemName"].ToString();
                imgUpperDate.ImageUrl = "~/Images/Calendar.png";
                imgUpperDate.Style.Add("padding-left", "3px");
                imgUpperDate.CausesValidation = false;


                CalendarExtender ceUpperDate = new CalendarExtender();
                ceUpperDate.ID = "ceUpperDate_" + dr["SystemName"].ToString();
                ceUpperDate.TargetControlID = txtUpperDate.ID;
                ceUpperDate.Format = "dd/MM/yyyy";
                ceUpperDate.PopupButtonID = imgUpperDate.ID;
                ceUpperDate.FirstDayOfWeek = FirstDayOfWeek.Monday;

                HyperLink hlSetDateRange = new HyperLink();
                hlSetDateRange.ID = "hlSetDateRange_" + dr["SystemName"].ToString();
                hlSetDateRange.Text = "Set datetime range";
                hlSetDateRange.Style.Add("cursor", "pointer");

                HiddenField hfSetDateRange = new HiddenField();
                hfSetDateRange.ID = "hfSetDateRange_" + dr["SystemName"].ToString();
                hfSetDateRange.Value = "0";


                MaskedEditExtender meeLowerTime = new MaskedEditExtender();
                meeLowerTime.ID = "meeLowerTime" + dr["SystemName"].ToString();
                meeLowerTime.TargetControlID = txtLowerTime.ClientID; //"ctl00_HomeContentPlaceHolder_txtTime";
                meeLowerTime.AutoCompleteValue = "00:00:00"; //"00:00:00"
                meeLowerTime.Mask = "99:99:99"; //99:99:99
                meeLowerTime.MaskType = MaskedEditType.Time;

                MaskedEditExtender meeUpperTime = new MaskedEditExtender();
                meeUpperTime.ID = "meeUpperTime" + dr["SystemName"].ToString();
                meeUpperTime.TargetControlID = txtUpperTime.ClientID; //"ctl00_HomeContentPlaceHolder_txtTime";
                meeUpperTime.AutoCompleteValue = "00:00:00"; //"00:00:00"
                meeUpperTime.Mask = "99:99:99"; //99:99:99
                meeUpperTime.MaskType = MaskedEditType.Time;


                HtmlTable tblDate = new HtmlTable();
                HtmlTableRow rowOne = new HtmlTableRow();
                HtmlTableRow rowTwo = new HtmlTableRow();
                HtmlTableRow rowThree = new HtmlTableRow();

                rowOne.ID = "rowOne_" + dr["SystemName"].ToString();
                rowTwo.ID = "rowTwo_" + dr["SystemName"].ToString();
                rowThree.ID = "rowThree_" + dr["SystemName"].ToString();

                tblDate.CellPadding = 0;
                tblDate.CellSpacing = 0;

                HtmlTableCell cellBox = new HtmlTableCell();
                cellBox.Controls.Add(new LiteralControl(String.Format("<label>{0}</label>", String.IsNullOrEmpty(dr["Heading"].ToString()) ? dr["DisplayName"] : dr["Heading"])));

                HtmlTableCell cellBox1 = new HtmlTableCell();
                HtmlTableCell cellBox12 = new HtmlTableCell();
                HtmlTableCell cellBox13 = new HtmlTableCell();
                HtmlTableCell cellBox2 = new HtmlTableCell();
                HtmlTableCell cellBox22 = new HtmlTableCell();
                HtmlTableCell cellBox23 = new HtmlTableCell();
                HtmlTableCell cellBox3 = new HtmlTableCell();

                txtLowerTime.Style.Add("margin-left", "5px");

                cellBox1.Controls.Add(txtLowerDate);
                cellBox12.Controls.Add(imgLowerDate);
                cellBox13.Controls.Add(txtLowerTime);
                cellBox12.Controls.Add(ceLowerDate);
                cellBox12.Controls.Add(twmLowerDate);
                //cellBox12.Controls.Add(rvLowerDate);
                cellBox13.Controls.Add(meeLowerTime);

                rowOne.Cells.Add(cellBox1);
                rowOne.Cells.Add(cellBox12);
                rowOne.Cells.Add(cellBox13);
                //cellBox.Controls.Add(new LiteralControl("</br>"));
                txtUpperDate.Style.Add("margin-top", "5px");
                imgUpperDate.Style.Add("margin-top", "5px");
                txtUpperTime.Style.Add("margin-top", "5px");
                txtUpperTime.Style.Add("margin-left", "5px");

                cellBox2.Controls.Add(txtUpperDate);
                cellBox22.Controls.Add(imgUpperDate);
                cellBox23.Controls.Add(txtUpperTime);
                cellBox22.Controls.Add(ceUpperDate);
                //cellBox22.Controls.Add(twmUpperDate);
                //cellBox22.Controls.Add(rvUpperDate);
                cellBox23.Controls.Add(meeUpperTime);
                cellBox3.Controls.Add(hlSetDateRange);
                cellBox3.Controls.Add(hfSetDateRange);

                rowTwo.Cells.Add(cellBox2);
                rowTwo.Cells.Add(cellBox22);
                rowTwo.Cells.Add(cellBox23);
                rowThree.Cells.Add(cellBox3);



                tblDate.Rows.Add(rowOne);
                tblDate.Rows.Add(rowTwo);
                tblDate.Rows.Add(rowThree);
                cellBox.Controls.Add(tblDate);
                theRow.Cells.Add(cellBox);



                //                strJSDynamicShowHide = strJSDynamicShowHide + @"
                //                        $('#" + _strDynamictabPart + txtLowerDate.ID + @"').on('keyup',function () {
                //                                                                var strLowerValue = $('#" + _strDynamictabPart + txtLowerDate.ID + @"').val();
                //                                                                 if (strLowerValue.trim() != '') {
                //                                                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').show();
                //                                                                }
                //                                                                else {
                //                                                                     $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val('');
                //                                                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').hide(); 
                //                                                                }
                //                                                            });
                //                         $('#" + _strDynamictabPart + txtLowerDate.ID + @"').trigger('keyup');
                //
                //                    ";

                //                strJSDynamicShowHide = strJSDynamicShowHide + @"
                //                        $('#" + _strDynamictabPart + txtLowerDate.ID + @"').change(function () {
                //                                                                var strLowerValue = $('#" + _strDynamictabPart + txtLowerDate.ID + @"').val();
                //                                                                 if (strLowerValue.trim() != '') {
                //                                                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').show();
                //                                                                }
                //                                                                else {
                //                                                                     $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val('');
                //                                                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').hide(); 
                //                                                                }
                //                                                            });
                //                         $('#" + _strDynamictabPart + txtLowerDate.ID + @"').trigger('change');
                //
                //                    ";

                _strJSDynamicShowHide = _strJSDynamicShowHide + @"


                                function ShowHideUP_" + _strDynamictabPart + txtUpperDate.ID + @"(keepvalue) {     
                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').hide();
                                    $('#" + _strDynamictabPart + rowThree.ID + @"').hide();                                                               
                                        try
                                        {
                                                var strLowerValue = $('#" + _strDynamictabPart + txtLowerDate.ID + @"').val();
                                                var strLowerValueT = $('#" + _strDynamictabPart + txtLowerTime.ID + @"').val();   
                                                var strUpperValueT = $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val();   
                                                var strUpperValue = $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val();    

                                                var splittedDateLowerValue = strLowerValue.split(""/"");
                                                var splittedDateUpperValue = strUpperValue.split(""/"");
                                                var dateLowerValue = new Date(splittedDateLowerValue[1] + ""/"" + splittedDateLowerValue[0] + ""/"" + splittedDateLowerValue[2] );
                                                var dateUpperValue = new Date(splittedDateUpperValue[1] + ""/"" + splittedDateUpperValue[0] + ""/"" + splittedDateUpperValue[2] );
                                                if (strLowerValue.trim() == 'dd/mm/yyyy') {strLowerValue = '';}
                                                if (strLowerValue.trim() != '' ) {                      
                                                         if (strUpperValue.trim() == '' && keepvalue=='0') {
                                                                $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val(strLowerValue);
                                                                //$('#" + _strDynamictabPart + txtUpperTime.ID + @"').val(strLowerValueT);   
                                                                strUpperValue = strLowerValue;                                                               
                                                                //if (strUpperValueT.trim() == ''){
                                                                //                 $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val(strLowerValueT);
                                                                //            }                                   
                                                        }   

                                                        /* =================================================
                                                        Red: changing the value from higher to lower, 
                                                        upper value should always follow the lower value 
                                                        when the user did not set the range yet
                                                        ==================================================== */    
                                                        if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '0') { 
                                                            $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val(strLowerValue); 
                                                            if (keepvalue == '1') {
                                                             if (strUpperValue.trim() != '' && strUpperValue != strLowerValue) {
                                                                $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val(strUpperValue);  
                                                                }
                                                            } 
                                                        }    

                                                        if (dateLowerValue > dateUpperValue ) { 
                                                            $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val(strLowerValue);   
                                                            strUpperValue = strLowerValue;
                                                        }    
                                                        if (strLowerValueT > strUpperValueT ) { 
                                                            $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val(strLowerValueT);   
                                                            strUpperValue = strLowerValue;
                                                        }       
                                                        if (strUpperValue.trim() != '' && strUpperValue == strLowerValue ) {  
                                                            if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '0') {
                                                            $('#" + _strDynamictabPart + rowThree.ID + @"').show(); }  
                                                            $('#" + _strDynamictabPart + rowTwo.ID + @"').hide();
                                                        }
                                                        else if (strUpperValue.trim() != '' && strUpperValue != strLowerValue ) { 
                                                            $('#" + _strDynamictabPart + rowThree.ID + @"').show();
                                                            if (keepvalue == '1') {   
                                                                $('#" + _strDynamictabPart + rowTwo.ID + @"').show();
                                                                $('#" + _strDynamictabPart + rowThree.ID + @"').hide();    
                                                                $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('1');
                                                              }
                                                        }
                                                       
                                                     //alert(strLowerValue);
                                                     //alert(strUpperValue);  
                                                     //alert(strLowerValueT);
                                                     //alert(strUpperValueT);           
                                                }
                                                else {
                                                        
                                                        $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val('');
                                                        $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val('');  
                                                        $('#" + _strDynamictabPart + txtLowerTime.ID + @"').val('');  
                                                        $('#" + _strDynamictabPart + rowTwo.ID + @"').hide();
                                                        $('#" + _strDynamictabPart + rowThree.ID + @"').hide();
                                                }
                                                
                                                if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '1') {                                                    
                                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').show();
                                                    $('#" + _strDynamictabPart + rowThree.ID + @"').hide();
                                                }
                                        }
                                        catch(err)
                                        {
                                             //
                                        }
                                     };

                                        $('#" + _strDynamictabPart + txtLowerDate.ID + @"').change(function () {
                                                ShowHideUP_" + _strDynamictabPart + txtUpperDate.ID + @"('0');                                                          
                                        });

                                        /* == Red 14032019: Added script below to check if the entered Upper Date is greater than the Lower Date  ==*/
                                        $('#" + _strDynamictabPart + txtUpperDate.ID + @"').change(function () {                                                
                                                ShowHideUP_" + _strDynamictabPart + txtUpperDate.ID + @"('0');                                                          
                                        });   


                                        $('#" + _strDynamictabPart + lnkReset.ID + @"').click(function () {    
                                                $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('0');                                              
                                        });   
    
                                       

                                    ShowHideUP_" + _strDynamictabPart + txtUpperDate.ID + @"('1');  

                                    /* ==================================================== 
                                        Red 14032019: Click dynamic hyperlink to hide or show the 'To Date'
                                    ==================================================== */
                                    $('#" + _strDynamictabPart + hlSetDateRange.ID + @"').click(function(){
                                        $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('1');
                                        ShowHideUP_" + _strDynamictabPart + txtUpperDate.ID + @"('0'); 
                                    });

                                            ";


                _strJSDynamicShowHide = _strJSDynamicShowHide + @"


                                function ShowHideUP_" + _strDynamictabPart + txtUpperTime.ID + @"(keepvalue) {    
                                                    //$('#" + _strDynamictabPart + rowTwo.ID + @"').hide();
                                                    //$('#" + _strDynamictabPart + rowThree.ID + @"').hide();                                                         
                                        try
                                        {
                                                var strLowerValueTT = $('#" + _strDynamictabPart + txtLowerTime.ID + @"').val();
                                                var strUpperValueD = $('#" + _strDynamictabPart + txtUpperDate.ID + @"').val();  
                                                var strUpperValueTT = $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val();   
                                                 
                                                 if (strLowerValueTT.trim() == 'HH:mm:ss') {strLowerValueTT = '';}
                                                 if (strLowerValueTT.trim() != '') {     
                                                     if (strUpperValueTT.trim() == '' && keepvalue=='0') {
                                                        $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val(strLowerValueTT);
                                                        strUpperValueTT = strLowerValueTT;     
                                                    } 

                                                    /* =================================================
                                                    Red: changing the value from higher to lower, 
                                                    upper value should always follow the lower value 
                                                    when the user did not set the range yet
                                                    ==================================================== */
                                                    if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '0') { 
                                                            $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val(strLowerValueTT); 
                                                            if (keepvalue == '1') {
                                                             if (strUpperValueTT.trim() != '' && strUpperValueTT != strLowerValueTT) {
                                                                $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val(strUpperValueTT);  
                                                                }
                                                            } 
                                                        }   
 
                                                        if (strLowerValueTT > strUpperValueTT) {
                                                            $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val(strLowerValueTT);
                                                        }     

                                                        if (strUpperValueTT.trim() != '' && strLowerValueTT == strUpperValueTT) {   
                                                                    if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '0') {
                                                                    $('#" + _strDynamictabPart + rowThree.ID + @"').show(); }  
                                                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').hide();
                                                                }
                                                                else if (strUpperValueTT.trim() != '' && strLowerValueTT != strUpperValueTT) { 
                                                                      $('#" + _strDynamictabPart + rowThree.ID + @"').show();
                                                                    if (keepvalue == '1') {   
                                                                        $('#" + _strDynamictabPart + rowTwo.ID + @"').show();
                                                                        $('#" + _strDynamictabPart + rowThree.ID + @"').hide();    
                                                                        $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('1');
                                                                      }
                                                                }       
                                                         
                                                }
                                                else {
                                                        if (strLowerValueTT.trim() == '' && strUpperValueD.trim() == '') {
                                                            $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val('');   
                                                            //$('#" + _strDynamictabPart + rowTwo.ID + @"').hide();
                                                            //$('#" + _strDynamictabPart + rowThree.ID + @"').hide();                                                              
                                                         }   
                                                            //$('#" + _strDynamictabPart + rowTwo.ID + @"').hide();
                                                            //$('#" + _strDynamictabPart + rowThree.ID + @"').hide();
                                                }

                                                if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '1') { 
                                                    $('#" + _strDynamictabPart + rowTwo.ID + @"').show();
                                                    $('#" + _strDynamictabPart + rowThree.ID + @"').hide();
                                                }
                                        }
                                        catch(err)
                                        {
                                             //
                                        }
                                     };

                                        $('#" + _strDynamictabPart + txtLowerTime.ID + @"').change(function () {
                                                ShowHideUP_" + _strDynamictabPart + txtUpperTime.ID + @"('0');                                                          
                                        });


                                            $('#" + _strDynamictabPart + lnkReset.ID + @"').click(function () {    
                                                    $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('0');                                              
                                            });   

                                        $('#" + _strDynamictabPart + txtUpperTime.ID + @"').change(function () {
                                                ShowHideUP_" + _strDynamictabPart + txtUpperTime.ID + @"('0');                                                          
                                        });
    
                                    ShowHideUP_" + _strDynamictabPart + txtUpperTime.ID + @"('1');  

                                            ";


            }
            else if (dr["ColumnType"].ToString() == "time")
            {

                if (s == 0)
                {
                    HtmlTableCell cellBoxS = new HtmlTableCell();
                    cellBoxS.Controls.Add(new LiteralControl("<br/><strong>Search:</strong>"));
                    cellBoxS.Attributes.Add("class", "first-cell");
                    theRow.Cells.Add(cellBoxS);
                }

                s = s + 1;
                TextBox txtLowerTime = new TextBox();
                txtLowerTime.ID = "txtLowerTime_" + dr["SystemName"].ToString();
                txtLowerTime.CssClass = "NormalTextBox";
                txtLowerTime.Width = 70;
                txtLowerTime.AutoCompleteType = AutoCompleteType.Disabled;



                TextBox txtUpperTime = new TextBox();
                txtUpperTime.ID = "txtUpperTime_" + dr["SystemName"].ToString();
                txtUpperTime.CssClass = "NormalTextBox";
                txtUpperTime.Width = 70;
                txtUpperTime.AutoCompleteType = AutoCompleteType.Disabled;
                txtUpperTime.Attributes.Add("placeholder", "To HH:mm:ss");

                MaskedEditExtender meeLowerTime = new MaskedEditExtender();
                meeLowerTime.ID = "meeLowerTime" + dr["SystemName"].ToString();
                meeLowerTime.TargetControlID = txtLowerTime.ClientID; //"ctl00_HomeContentPlaceHolder_txtTime";
                meeLowerTime.AutoCompleteValue = "00:00:00"; //"00:00:00"
                meeLowerTime.Mask = "99:99:99"; //99:99:99
                meeLowerTime.MaskType = MaskedEditType.Time;

                MaskedEditExtender meeUpperTime = new MaskedEditExtender();
                meeUpperTime.ID = "meeUpperTime" + dr["SystemName"].ToString();
                meeUpperTime.TargetControlID = txtUpperTime.ClientID; //"ctl00_HomeContentPlaceHolder_txtTime";
                meeUpperTime.AutoCompleteValue = "00:00:00"; //"00:00:00"
                meeUpperTime.Mask = "99:99:99"; //99:99:99
                meeUpperTime.MaskType = MaskedEditType.Time;

                HyperLink hlSetDateRange = new HyperLink();
                hlSetDateRange.ID = "hlSetDateRange_" + dr["SystemName"].ToString();
                hlSetDateRange.Text = "Set time range";
                hlSetDateRange.Style.Add("cursor", "pointer");

                HiddenField hfSetDateRange = new HiddenField();
                hfSetDateRange.ID = "hfSetDateRange_" + dr["SystemName"].ToString();
                hfSetDateRange.Value = "0";


                TextBoxWatermarkExtender twmLowerTime = new TextBoxWatermarkExtender();
                twmLowerTime.ID = "twmTime_" + dr["SystemName"].ToString();
                twmLowerTime.TargetControlID = txtLowerTime.ID;
                twmLowerTime.WatermarkText = "HH:mm:ss";
                twmLowerTime.WatermarkCssClass = "MaskText";

                HtmlTableCell cellBox = new HtmlTableCell();
                cellBox.Controls.Add(new LiteralControl(String.Format("<label>{0}</label>", String.IsNullOrEmpty(dr["Heading"].ToString()) ? dr["DisplayName"] : dr["Heading"])));
                cellBox.Controls.Add(txtLowerTime);
                cellBox.Controls.Add(new LiteralControl("</br>"));
                txtUpperTime.Style.Add("margin-top", "5px");
                cellBox.Controls.Add(txtUpperTime);
                cellBox.Controls.Add(meeLowerTime);
                cellBox.Controls.Add(meeUpperTime);
                cellBox.Controls.Add(twmLowerTime);

                cellBox.Controls.Add(hlSetDateRange);
                cellBox.Controls.Add(hfSetDateRange);


                theRow.Cells.Add(cellBox);

                //                strJSDynamicShowHide = strJSDynamicShowHide + @"
                //                        $('#" + _strDynamictabPart + txtLowerTime.ID + @"').on('keyup',function () {
                //                                                                var strLowerValue = $('#" + _strDynamictabPart + txtLowerTime.ID + @"').val();
                //                                                                 if (strLowerValue.trim() != '') {
                //                                                                    $('#" + _strDynamictabPart + txtUpperTime.ID + @"').show();
                //                                                                }
                //                                                                else {
                //                                                                     $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val('');
                //                                                                    $('#" + _strDynamictabPart + txtUpperTime.ID + @"').hide(); 
                //                                                                }
                //                                                            });
                //                         $('#" + _strDynamictabPart + txtLowerTime.ID + @"').trigger('keyup');
                //
                //                    ";

                //                strJSDynamicShowHide = strJSDynamicShowHide + @"
                //                        $('#" + _strDynamictabPart + txtLowerTime.ID + @"').change(function () {
                //                                                                var strLowerValue = $('#" + _strDynamictabPart + txtLowerTime.ID + @"').val();
                //                                                                 if (strLowerValue.trim() != '') {
                //                                                                    $('#" + _strDynamictabPart + txtUpperTime.ID + @"').show();
                //                                                                }
                //                                                                else {
                //                                                                     $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val('');
                //                                                                    $('#" + _strDynamictabPart + txtUpperTime.ID + @"').hide(); 
                //                                                                }
                //                                                            });
                //                         $('#" + _strDynamictabPart + txtLowerTime.ID + @"').trigger('change');
                //
                //                    ";


                _strJSDynamicShowHide = _strJSDynamicShowHide + @"


                                function ShowHideUP_" + _strDynamictabPart + txtUpperTime.ID + @"(keepvalue) {      
                                                        $('#" + _strDynamictabPart + txtUpperTime.ID + @"').hide();
                                                        $('#" + _strDynamictabPart + hlSetDateRange.ID + @"').hide();                                                               
                                        try
                                        {
                                                var strLowerValue = $('#" + _strDynamictabPart + txtLowerTime.ID + @"').val();
                                                var strUpperValue = $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val();
                                                
                                                if (strLowerValue.trim() == 'HH:mm:ss') {strLowerValue = '';}
                                                if (strLowerValue.trim() != '' ) { 
                                                    if (strUpperValue.trim() == '' && keepvalue=='0') {
                                                      $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val(strLowerValue);
                                                        strUpperValue = strLowerValue;   
                                                    } 
                                                    
                                                    /* =================================================
                                                    Red: changing the value from higher to lower, 
                                                    upper value should always follow the lower value 
                                                    when the user did not set the range yet
                                                    ==================================================== */
                                                    if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '0') { 
                                                        $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val(strLowerValue);  
                                                        if (keepvalue == '1') {
                                                         if (strUpperValue.trim() != '' && strUpperValue != strLowerValue) {
                                                            $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val(strUpperValue);  
                                                            }
                                                        }
                                                    }  

                                                    if (strLowerValue > strUpperValue) {
                                                            $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val(strLowerValue);
                                                        }   

                                                                if (strUpperValue.trim() != '' && strLowerValue == strUpperValue) {   
                                                                    if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '0') {
                                                                    $('#" + _strDynamictabPart + hlSetDateRange.ID + @"').show(); }  
                                                                    $('#" + _strDynamictabPart + txtUpperTime.ID + @"').hide();
                                                                }
                                                                else if (strUpperValue.trim() != '' && strLowerValue != strUpperValue) { 
                                                                    $('#" + _strDynamictabPart + hlSetDateRange.ID + @"').show();
                                                    if (keepvalue == '1') {   
                                                                $('#" + _strDynamictabPart + txtUpperTime.ID + @"').show();
                                                                $('#" + _strDynamictabPart + hlSetDateRange.ID + @"').hide();
                                                                $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('1');
                                                              }
                                                                }     
                                                              
                                                }
                                                else { 

                                                    if (strLowerValue.trim() == '') {
                                                            $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val('');                                                                 
                                                         }   
                                                        $('#" + _strDynamictabPart + txtUpperTime.ID + @"').hide();
                                                        $('#" + _strDynamictabPart + hlSetDateRange.ID + @"').hide();           
                                                        $('#" + _strDynamictabPart + txtUpperTime.ID + @"').val('');  
                                                }

                                                if ($('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val() == '1') { 
                                                    $('#" + _strDynamictabPart + txtUpperTime.ID + @"').show();
                                                    $('#" + _strDynamictabPart + hlSetDateRange.ID + @"').hide();
                                                }


                                        }
                                        catch(err)
                                        {
                                             //
                                        }
                                     };

                                        $('#" + _strDynamictabPart + txtLowerTime.ID + @"').change(function () {
                                                ShowHideUP_" + _strDynamictabPart + txtUpperTime.ID + @"('0');                                                          
                                        });

                                        $('#" + _strDynamictabPart + lnkReset.ID + @"').click(function () {    
                                                    $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('0');                                              
                                            });   
                                        // $('#" + _strDynamictabPart + txtLowerTime.ID + @"').on('keyup',function () {
                                        //        ShowHideUP_" + _strDynamictabPart + txtUpperTime.ID + @"('1');                                                          
                                        //});

                                        $('#" + _strDynamictabPart + txtUpperTime.ID + @"').change(function () {
                                                ShowHideUP_" + _strDynamictabPart + txtUpperTime.ID + @"('0');                                                          
                                        });
    
                                    ShowHideUP_" + _strDynamictabPart + txtUpperTime.ID + @"('1');  

                                        $('#" + _strDynamictabPart + hlSetDateRange.ID + @"').click(function(){
                                        $('#" + _strDynamictabPart + hfSetDateRange.ID + @"').val('1');
                                        ShowHideUP_" + _strDynamictabPart + txtUpperTime.ID + @"('0'); 
                                    });

                                            ";

            }
            else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "values"
                || dr["DropDownType"].ToString() == "value_text"))
            {
                if (s == 0)
                {
                    HtmlTableCell cellBoxS = new HtmlTableCell();
                    cellBoxS.Controls.Add(new LiteralControl("<br/><strong>Search:</strong>"));
                    cellBoxS.Attributes.Add("class", "first-cell");
                    theRow.Cells.Add(cellBoxS);
                }
                s = s + 1;

                DropDownList ddlSearch = new DropDownList();
                ddlSearch.ID = "ddlSearch_" + dr["SystemName"].ToString();
                ddlSearch.CssClass = "NormalTextBox";
                ddlSearch.Width = 115;

                ddlSearch.AutoPostBack = true;
                ddlSearch.SelectedIndexChanged += new EventHandler(ddl_search);

                HtmlTableCell cellBox = new HtmlTableCell();
                cellBox.Controls.Add(new LiteralControl(String.Format("<label>{0}</label>", String.IsNullOrEmpty(dr["Heading"].ToString()) ? dr["DisplayName"] : dr["Heading"])));
                cellBox.Controls.Add(ddlSearch);

                theRow.Cells.Add(cellBox);
            }
            else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "table" || dr["DropDownType"].ToString() == "tabledd") &&
            dr["TableTableID"] != DBNull.Value && dr["DisplayColumn"].ToString() != "")
            {
                if (s == 0)
                {
                    HtmlTableCell cellBoxS = new HtmlTableCell();
                    cellBoxS.Controls.Add(new LiteralControl("<br/><strong>Search:</strong>"));
                    cellBoxS.Attributes.Add("class", "first-cell");
                    theRow.Cells.Add(cellBoxS);
                }

                s = s + 1;

                DropDownList ddlParentSearch = new DropDownList();
                ddlParentSearch.ID = _strDynamictabPart + "ddlParentSearch_" + dr["SystemName"].ToString();
                ddlParentSearch.CssClass = "NormalTextBox";
                ddlParentSearch.Width = 230;
                ddlParentSearch.AutoPostBack = false;
                ddlParentSearch.ClientIDMode = ClientIDMode.Static;
                ddlParentSearch.Attributes.Add("multiple", "multiple");

                HiddenField hfParentSearch = new HiddenField();
                hfParentSearch.ID = _strDynamictabPart + "hfParentSearch_" + dr["SystemName"].ToString();
                hfParentSearch.ClientIDMode = ClientIDMode.Static;

                HiddenField hfSystemName = new HiddenField();
                hfSystemName.ID = _strDynamictabPart + "hfSystemName_" + dr["SystemName"].ToString();
                hfSystemName.ClientIDMode = ClientIDMode.Static;

                HtmlTableCell cellBox = new HtmlTableCell();
                cellBox.Controls.Add(new LiteralControl(String.Format("<label>{0}</label>", String.IsNullOrEmpty(dr["Heading"].ToString()) ? dr["DisplayName"] : dr["Heading"])));
                cellBox.Controls.Add(ddlParentSearch);
                cellBox.Controls.Add(hfParentSearch);

                string stringParentSearch = @"
                        $(document).ready(function () {  

                                    var IsOpen = false;
                                    if(selectedValue == '" + dr["SystemName"].ToString() + @"'){
                                        IsOpen = true;
                                        selectedValue='';
                                    }

                                        $('#" + ddlParentSearch.ID + @"').multipleSelect({
                                            onOpen: function () {
                                                        if (document.getElementById('" + ddlParentSearch.ID + @"').options.length < 1 && document.getElementById('" + hfRecordCount.ClientID + @"').value!='0' ) {
                                                        $('#" + _strDynamictabPart + "multipleSelect_" + dr["SystemName"].ToString() + @"').parent().text("" Loading..."");
                                                        $('#" + hfSystemName.ID + @"').val('" + dr["SystemName"].ToString() + @"');
                                                        selectedValue = '" + dr["SystemName"].ToString() + @"';
                                                        document.getElementById('" + lnkDDLPopulate.ClientID + @"').click();
                                                                        }
                                                },
                                            //lets show the DisplayTextSummary
                                            placeholder: ""Select " + dr["DisplayTextSummary"].ToString() + @""" ,
                                            filter: true,
                                            isOpen: IsOpen,
                                            //RP Added Ticket 4310
                                             " + (Boolean.Parse(dr["ShowAllValuesOnSearch"].ToString()) ? "selectResetAll: true," : "") + @"
                                            //End Modification
                                            alternateSelectAllAndReset: true,
                                            onReset: function () {
                                            $('#" + hfddlShowAll.ClientID + @"').val('" + dr["SystemName"].ToString() + @"');                                            
                                                document.getElementById('" + lnkReset.ClientID + @"').click();                                              
                                                        },
                                            onClose: function () {

                                                $('#" + hfParentSearch.ID + @"').val($('#" + ddlParentSearch.ID + @"').multipleSelect('getSelects'));
                                                    if (document.getElementById('" + ddlParentSearch.ID + @"').options.length > 0  ) {
                                                        $('#" + hfSystemNamePrevsious.ID + @"').val('" + dr["SystemName"].ToString() + @"');
                                                     document.getElementById('" + btnSearch.ClientID + @"').click();
                                                        }
                                           }
                                        });  
                                         $('.ms-choice').children().last().attr('id', '" + _strDynamictabPart + "multipleSelect_" + dr["SystemName"].ToString() + @"');
                                });


                        ";
                ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "jsddlParentSearch" + _strDynamictabPart + dr["SystemName"].ToString(), stringParentSearch, true);

                cellBox.Controls.Add(hfSystemName);
                theRow.Cells.Add(cellBox);

                /*RP Added Ticket 4310*/
                //if(dr["ShowAllValuesOnSearch"] != null)
                //{
                //    if ((Boolean)dr["ShowAllValuesOnSearch"] == true)
                //    {
                //        DropDownList ddlShowAllValuesDDL = new DropDownList();
                //        ddlShowAllValuesDDL.ID = _strDynamictabPart + "ddlShowAllParentSearch_" + dr["SystemName"].ToString();
                //        ddlShowAllValuesDDL.CssClass = "NormalTextBox";
                //        ddlShowAllValuesDDL.Width = 230;
                //        ddlShowAllValuesDDL.AutoPostBack = false;
                //        ddlShowAllValuesDDL.ClientIDMode = ClientIDMode.Static;

                //        PopulateDropDownSearch(ddlShowAllValuesDDL, (Int32)dr["LinkedParentColumnID"]);
                //        ddlShowAllValuesDDL.Items.Add(new ListItem("-- Show All --", ""));

                //        HtmlTableCell showAllcellBox = new HtmlTableCell();
                //        showAllcellBox.Controls.Add(new LiteralControl(String.Format("<label>{0}</label>", String.IsNullOrEmpty(dr["Heading"].ToString()) ? dr["DisplayName"] : dr["Heading"])));
                //        showAllcellBox.Controls.Add(ddlShowAllValuesDDL);
                //        theRow.Cells.Add(showAllcellBox);
                //    }
                //}
                /*End Modification*/
            }
            else if (dr["ColumnType"].ToString() == "radiobutton" || dr["ColumnType"].ToString() == "checkbox" ||
                dr["ColumnType"].ToString() == "listbox")
            {
                if (s == 0)
                {
                    HtmlTableCell cellBoxS = new HtmlTableCell();
                    cellBoxS.Controls.Add(new LiteralControl("<br/><strong>Search:</strong>"));
                    cellBoxS.Attributes.Add("class", "first-cell");
                    theRow.Cells.Add(cellBoxS);
                }
                s = s + 1;

                DropDownList ddlSearch = new DropDownList();
                ddlSearch.ID = "ddlSearch_" + dr["SystemName"].ToString();
                ddlSearch.CssClass = "NormalTextBox";
                ddlSearch.Width = 115;

                ddlSearch.AutoPostBack = true;
                ddlSearch.SelectedIndexChanged += new EventHandler(ddl_search);

                HtmlTableCell cellBox = new HtmlTableCell();
                cellBox.Controls.Add(new LiteralControl(String.Format("<label>{0}</label>", String.IsNullOrEmpty(dr["Heading"].ToString()) ? dr["DisplayName"] : dr["Heading"])));
                cellBox.Controls.Add(ddlSearch);

                theRow.Cells.Add(cellBox);
            }
            else
            {
                if (s == 0)
                {
                    HtmlTableCell cellBoxS = new HtmlTableCell();
                    cellBoxS.Controls.Add(new LiteralControl("<br/><strong>Search:</strong>"));
                    cellBoxS.Attributes.Add("class", "first-cell");
                    theRow.Cells.Add(cellBoxS);
                }

                s = s + 1;
                TextBox txtSearch = new TextBox();
                txtSearch.ID = "txtSearch_" + dr["SystemName"].ToString();
                txtSearch.CssClass = "NormalTextBox";
                txtSearch.Width = 105;
                txtSearch.Attributes.Add("onblur", "this.value=this.value.trim()");

                TextBoxWatermarkExtender twmTextSearch = new TextBoxWatermarkExtender();
                twmTextSearch.ID = "twmTextSearch_" + dr["SystemName"].ToString();
                twmTextSearch.TargetControlID = txtSearch.ID;
                twmTextSearch.WatermarkText = " ";
                twmTextSearch.WatermarkCssClass = "MaskText";

                HtmlTableCell cellBox = new HtmlTableCell();
                cellBox.Controls.Add(new LiteralControl(String.Format("<label>{0}</label>", String.IsNullOrEmpty(dr["Heading"].ToString()) ? dr["DisplayName"] : dr["Heading"])));
                cellBox.Controls.Add(txtSearch);
                cellBox.Controls.Add(twmTextSearch);

                theRow.Cells.Add(cellBox);

            }

            if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "table" || dr["DropDownType"].ToString() == "tabledd") &&
           dr["TableTableID"] != DBNull.Value && dr["DisplayColumn"].ToString() != "")
            {

            HtmlTableCell cellBoxx = new HtmlTableCell();
            //Main multiselect Red 2501
            cbcSearchMain.ControlB_DDL.Attributes.Add("multiple", "multiple");
            cbcSearchMain.ControlB_DDL.CssClass = "NormalTextBox";
            cbcSearchMain.ControlB_DDL.Width = 230;

            string stringParentSearchAdv = @"
                         $(document).ready(function () { 
                                        var placeHolder = ""--Select All--"";
                                        if ($('#" + _strDynamictabPart + "cbcSearchMain_ddlCompareOperator" + @"').val() == '<>') {placeHolder = '--Please Select--'};
                                        $('#" + _strDynamictabPart + "cbcSearchMain_" + cbcSearchMain.ControlB_DDL.ID + @"').multipleSelect({
                                                    onOpen: function () {
                                                        //if (document.getElementById('" + cbcSearchMain.ControlB_DDL.ClientID + @"').options.length < 1) {
                                                        //$('#" + _strDynamictabPart + "multipleSelect_" + dr["SystemName"].ToString() + @"').parent().text("" Loading..."");
                                                        //document.getElementById('" + lnkDDLPopulate.ClientID + @"').click();
                                                        //                }

                                                },
                                            //lets show the DisplayTextSummary
                                            placeholder: placeHolder,   //""--Select All--"" ,
                                            filter: true, // " + dr["Displayname"] + " \n " +
                                            //RP Added Ticket 4310
                                            (Boolean.Parse(dr["ShowAllValuesOnSearch"].ToString()) ? "selectResetAll: true," : "") + @"
                                            //End Modification  
                                            alternateSelectAllAndReset: true,
                                            onReset: function () {
                                            document.getElementById('" + lnkReset.ClientID + @"').click();

                                                        },                                

                                                onClose: function () {
                                                $('#" + hfControlB_DDL.ClientID + @"').val($('#" + _strDynamictabPart + "cbcSearchMain_" + cbcSearchMain.ControlB_DDL.ID + @"').multipleSelect('getSelects'));
                                                //alert($('#" + hfControlB_DDL.ClientID + @"').val());
                                                document.getElementById('" + btnSearch.ClientID + @"').click();
                                            }
                                        });               
                                    });

                        ";
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "jsddlParentSearchx" + _strDynamictabPart + dr["SystemName"].ToString(), stringParentSearchAdv, true);

            //cbc1 multiselect
            cbcSearch1.ControlB_DDL.Attributes.Add("multiple", "multiple");
            cbcSearch1.ControlB_DDL.CssClass = "NormalTextBox";
            cbcSearch1.ControlB_DDL.Width = 230;


            string stringParentSearch_cbcSearch1 = @"
                         $(document).ready(function () {   
                                        var placeHolder = ""--Select All--"";
                                        if ($('#" + _strDynamictabPart + "cbcSearch1_ddlCompareOperator" + @"').val() == '<>') {placeHolder = '--Please Select--'};         
                                        $('#" + _strDynamictabPart + "cbcSearch1_" + cbcSearch1.ControlB_DDL.ID + @"').multipleSelect({
                                                onOpen: function () {
                                                        //if (document.getElementById('" + cbcSearchMain.ControlB_DDL.ClientID + @"').options.length < 1) {
                                                        //$('#" + _strDynamictabPart + "multipleSelect_" + dr["SystemName"].ToString() + @"').parent().text("" Loading..."");
                                                        //document.getElementById('" + lnkDDLPopulate.ClientID + @"').click();
                                                        //                }

                                                },
                                            //lets show the DisplayTextSummary
                                             placeholder: placeHolder, //""--Select All--"" ,
                                            filter: true, // " + dr["Displayname"] + " \n " +
                                            //RP Added Ticket 4310
                                            (Boolean.Parse(dr["ShowAllValuesOnSearch"].ToString()) ? "selectResetAll: true," : "") + @"
                                            //End Modification
                                            alternateSelectAllAndReset: true,
                                            onReset: function () {
                                            document.getElementById('" + lnkReset.ClientID + @"').click();

                                                        },                                             

                                                onClose: function () {
                                                $('#" + hfDDLcbcSearch1.ClientID + @"').val($('#" + _strDynamictabPart + "cbcSearch1_" + cbcSearch1.ControlB_DDL.ID + @"').multipleSelect('getSelects'));
                                                document.getElementById('" + btnSearch.ClientID + @"').click();
                                            }
                                        });               
                                    });

                        ";
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "jsddlParentSearch_cbcSearch1" + _strDynamictabPart + dr["SystemName"].ToString(), stringParentSearch_cbcSearch1, true);

            //cbc2 multiselect
            cbcSearch2.ControlB_DDL.Attributes.Add("multiple", "multiple");
            cbcSearch2.ControlB_DDL.CssClass = "NormalTextBox";
            cbcSearch2.ControlB_DDL.Width = 230;


            string stringParentSearch_cbcSearch2 = @"



                         $(document).ready(function () {  
                                        var placeHolder = ""--Select All--"";
                                        if ($('#" + _strDynamictabPart + "cbcSearch2_ddlCompareOperator" + @"').val() == '<>') {placeHolder = '--Please Select--'};             
                                        $('#" + _strDynamictabPart + "cbcSearch2_" + cbcSearch2.ControlB_DDL.ID + @"').multipleSelect({
                                                onOpen: function () {
                                                        //if (document.getElementById('" + cbcSearchMain.ControlB_DDL.ID + @"').options.length < 1) {
                                                        //$('#" + _strDynamictabPart + "multipleSelect_" + dr["SystemName"].ToString() + @"').parent().text("" Loading..."");
                                                        //document.getElementById('" + lnkDDLPopulate.ClientID + @"').click();
                                                        //                }

                                                },
                                            //lets show the DisplayTextSummary
                                            placeholder: placeHolder, //""--Select All--"" ,
                                            filter: true, // " + dr["Displayname"] + " \n " +
                                            //RP Added Ticket 4310
                                            (Boolean.Parse(dr["ShowAllValuesOnSearch"].ToString()) ? "selectResetAll: true," : "") + @"
                                            //End Modification
                                            alternateSelectAllAndReset: true,
                                            onReset: function () {
                                            document.getElementById('" + lnkReset.ClientID + @"').click();

                                                        },                                          
                                                        onClose: function () {
                                                $('#" + hfDDLcbcSearch2.ClientID + @"').val($('#" + _strDynamictabPart + "cbcSearch2_" + cbcSearch2.ControlB_DDL.ID + @"').multipleSelect('getSelects'));
                                                document.getElementById('" + btnSearch.ClientID + @"').click();
                                            }
                                        });               
                                    });

                        ";
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "jsddlParentSearch_cbcSearch2" + _strDynamictabPart + dr["SystemName"].ToString(), stringParentSearch_cbcSearch2, true);

            //cbc3 multiselect
            cbcSearch3.ControlB_DDL.Attributes.Add("multiple", "multiple");
            cbcSearch3.ControlB_DDL.CssClass = "NormalTextBox";
            cbcSearch3.ControlB_DDL.Width = 230;


            string stringParentSearch_cbcSearch3 = @"
                         $(document).ready(function () {   
                                        var placeHolder = ""--Select All--"";
                                        if ($('#" + _strDynamictabPart + "cbcSearch3_ddlCompareOperator" + @"').val() == '<>') {placeHolder = '--Please Select--'};            
                                        $('#" + _strDynamictabPart + "cbcSearch3_" + cbcSearch3.ControlB_DDL.ID + @"').multipleSelect({
                                            onOpen: function () {
                                                        //if (document.getElementById('" + cbcSearchMain.ControlB_DDL.ID + @"').options.length < 1) {
                                                        //$('#" + _strDynamictabPart + "multipleSelect_" + dr["SystemName"].ToString() + @"').parent().text("" Loading..."");
                                                        //document.getElementById('" + lnkDDLPopulate.ClientID + @"').click();
                                                        //                }

                                                },
                                            //lets show the DisplayTextSummary
                                             placeholder: placeHolder, //""--Select All--"" ,
                                            filter: true, // " + dr["Displayname"] + " \n "  +
                                            //RP Added Ticket 4310
                                            (Boolean.Parse(dr["ShowAllValuesOnSearch"].ToString()) ? "selectResetAll: true," : "") + @"
                                            //End Modification
                                            alternateSelectAllAndReset: true,
                                            onReset: function () {
                                            document.getElementById('" + lnkReset.ClientID + @"').click();

                                                        },                                         
                                            onClose: function () {
                                                $('#" + hfDDLcbcSearch3.ClientID + @"').val($('#" + _strDynamictabPart + "cbcSearch3_" + cbcSearch3.ControlB_DDL.ID + @"').multipleSelect('getSelects'));
                                                document.getElementById('" + btnSearch.ClientID + @"').click();
                                            }
                                        });               
                                    });

                        ";
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "jsddlParentSearch_cbcSearch3" + _strDynamictabPart + dr["SystemName"].ToString(), stringParentSearch_cbcSearch3, true);

            cellBoxx.Controls.Add(hfControlB_DDL);
            cellBoxx.Controls.Add(hfDDLcbcSearch1);
            cellBoxx.Controls.Add(hfDDLcbcSearch2);
            cellBoxx.Controls.Add(hfDDLcbcSearch3);
            theRow.Cells.Add(cellBoxx);
            }

        }



        foreach (DataRow dr in _dtSearchGroup.Rows)
        {
            if (s == 0)
            {
                HtmlTableCell cellBoxS = new HtmlTableCell();
                cellBoxS.Controls.Add(new LiteralControl("<br/><strong>Search:</strong>"));
                cellBoxS.Attributes.Add("class", "first-cell");
                theRow.Cells.Add(cellBoxS);
            }

            s = s + 1;
            TextBox txtSearch = new TextBox();
            txtSearch.ID = "txtSearch_" + dr["SearchGroupID"].ToString();
            txtSearch.CssClass = "NormalTextBox";
            txtSearch.Width = 105;
            HtmlTableCell cellBox = new HtmlTableCell();
            cellBox.Controls.Add(new LiteralControl("<label>" + dr["GroupName"].ToString() + "</label><br/>"));
            cellBox.Controls.Add(txtSearch);

            theRow.Cells.Add(cellBox);

        }

        int v = 0;
        foreach (HtmlTableCell eachCell in theRow.Cells)
        {
            if (v > 0)
                eachCell.Style.Add("vertical-align", "bottom");

            if (_bResponsive && Responsive.DeviceMaxXAxis() == 1)
            {

                if (v == 2)
                {
                    eachCell.Controls.Add(new LiteralControl("<br/><br/><a href='#' id='lnksearchmore" + _theTable.TableID.ToString() + "' onclick='ShowSearchMore" +
                        _theTable.TableID.ToString() + "()'>more...</a>"));
                    string strShowSearchMore = @"

                                                $('.searchmore" + _theTable.TableID.ToString() + @"').hide();
                                                function ShowSearchMore" + _theTable.TableID.ToString() + @"(a,b)
                                                    {
                                                         try
                                                        {
                                                            $('#lnksearchmore" + _theTable.TableID.ToString() + @"').hide();
                                                            $('.searchmore" + _theTable.TableID.ToString() + @"').show();
                                                        }
                                                         catch(err)
                                                        {
                                                            //
                                                        }
                                                    };
                                                ";
                    ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "jsShowSearchMore" + _strDynamictabPart, strShowSearchMore, true);

                }
                if (v > 2)
                {
                    eachCell.Attributes.Add("class", "searchmore" + _theTable.TableID.ToString());
                }

            }


            v = v + 1;
        }


        tblSearchControls.Rows.Add(theRow);




        if (_strJSDynamicShowHide != "")
        {
            _strJSDynamicShowHide = @"$(document).ready(function () { 
                        try {  
                                " + _strJSDynamicShowHide + @" 
                            }
                        catch(err) {
                                //do ntohing
                                }
                            });";

            //ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "strJSDynamicShowHide" + _strDynamictabPart, _strJSDynamicShowHide, true);
        }




    }





    protected void Page_PreRender(object sender, EventArgs e)
    {


        //reset when  view is updated
        try
        {
            if(!IsPostBack)
            {
                if(Session["updatedview" + hfViewID.Value]!=null)
                {
                    lnkReset_Click(null, null);
                    Session["updatedview" + hfViewID.Value] = null;
                }
            }

        }
        catch
        {
            Session["updatedview" + hfViewID.Value] = null;
        }


        //if (PageType.ToLower() == "c")
        //{
        //    Child_Control_Dynamic("populate");
        //}
        ////Child_Control_PreRender();


        ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "strJSDynamicShowHide" + _strDynamictabPart, _strCommonJS + _strJSDynamicShowHide, true);

        if (_bHaveService)
        {

            ProcessServiceForEvent("ASP_Page_PreRender", sender, e);
        }
    }
    protected void SetCosmetic()
    {
        //need to use class instead of fixed color
        string strAlterBC2 = "DCF2F0";

        if (this.Page.MasterPageFile != null && this.Page.MasterPageFile.ToLower().IndexOf("rrp") > -1)
        {

            strAlterBC2 = "ECECED";

        }
        if (_theTable.FilterTopColour != "")
        {
            trFiletrTop.Style.Add("background-color", "#" + _theTable.FilterTopColour);
        }
        if (_theTable.FilterBottomColour != "")
        {
            trFilterBottom.Style.Add("background-color", "#" + _theTable.FilterBottomColour);
        }

        if (this.Page.MasterPageFile != null && this.Page.MasterPageFile.ToLower().IndexOf("rrp") > -1)
        {
            _bCustomDDL = true;
            if (PageType == "p")
            {
                if (_theTable.FilterTopColour == "")
                {
                    trFiletrTop.Style.Add("background-color", "#238DA3");
                }
                if (_theTable.FilterBottomColour == "")
                {
                    trFilterBottom.Style.Add("background-color", "#40A5B7");
                }

                ddlEnteredBy.Width = 220;
                divEnteredBy.Width = 200;

                ddlEnteredBy.CssClass = "ddlrrp";
                divEnteredBy.CssClass = "ddlDIV";
                stgFilter.Style.Add("color", "#ffffff");
                tblAdvancedOptionChk.Style.Add("color", "#ffffff");
                imgShowGraph.ImageUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Pager/Images/rrp/Graph.png";
                imgUpload.ImageUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Pager/Images/rrp/upload.png";
                ibEmail.ImageUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Pager/Images/rrp/email.png";
                imgConfig.ImageUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Pager/Images/rrp/config.png";

                if (_theTable.HeaderColor != "")
                {
                    ltTextStyles.Text = "<style>.pagerstyle{ background: #" + _theTable.HeaderColor + ";}.pagergradient{ background: #" + _theTable.HeaderColor + ";}.TopTitle{color:#FFFFFF;}</style>";
                }
                else
                {
                    ltTextStyles.Text = "<style>.pagerstyle{ background: #0089a5;}.pagergradient{ background: #0089a5;}.TopTitle{color:#FFFFFF;}</style>";
                }


                divSearch.Attributes["class"] = "searchcornerRRP";
                divRecordListTop.Style.Add("padding-left", "170px");
                if (_theTable.HeaderColor != "")
                {
                    divRecordListTop.Style.Add("background-color", "#" + _theTable.HeaderColor);

                }
                else
                {
                    divRecordListTop.Style.Add("background-color", "#0089a5");
                }
            }
            gvTheGrid.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#" + strAlterBC2);
        }
        else
        {
            gvTheGrid.HeaderStyle.ForeColor = System.Drawing.Color.Black;
        }
    }

    protected void DoOtherInit()
    {

    }

    // get last minute controls
    //protected override void OnPreRender(EventArgs e)
    //{
    //    base.OnPreRender(e);

    //    // start scanning from page subcontrols
    //    ControlCollection _collection = this.Controls;
    //    lblMsg.Visible = true;
    //    lblMsg.Text =TheDatabase.GetCode(_collection,_strDynamictabPart).Replace("\r\n", "<br/>");
    //}
    protected void Page_Init(object sender, EventArgs e)
    {
        //MR
        if (Page.MasterPageFile != null && Page.MasterPageFile.IndexOf("Responsive") > -1)
            _bResponsive = true;

        if (Session["User"] == null || Session["FilesLocation"] == null)
        {
            try
            {
                //JA 09 JAN 2017
                //Response.Redirect("~/Login.aspx", true);
                Response.Redirect("~/Login.aspx?" + "ReturnURL =" + Request.QueryString["ReturnURL"].ToString(), true);
            }
            catch
            {
                return;
            }
        }


        _strDynamictabPart = lnkSearch.ClientID.Substring(0, lnkSearch.ClientID.Length - 9);

        _strFilesLocation = Session["FilesLocation"].ToString();

        FindTheTable();

        if (_theTable == null)
            return;

        if (Request.PhysicalPath.IndexOf("Pages\\Record\\RecordList.aspx") > -1 && Common.GetValueFromSQL("SELECT TOP 1 ServiceID FROM [Service] WHERE  ServiceType='RecordList_P' AND TableID=" + _qsTableID) != "")
        {
            _bHaveService = true;
            _strServiceTypeWHR = "ServiceType='RecordList_P' AND TableID=" + _qsTableID;
        }
        //RecordDetail.aspx


        if (TableChildID != null)
        {
            _strFunctionUQ = TableChildID.ToString();
        }
        else
        {
            _strFunctionUQ = _theTable.TableID.ToString();
        }

        /* Red 28112018 */
        hfParentRecordIDList.Value = "-1";

        if (TableChildID != null && Request.PhysicalPath.IndexOf("Pages\\Record\\RecordDetail.aspx") > -1 && Common.GetValueFromSQL("SELECT TOP 1 ServiceID FROM [Service] WHERE ServiceType='RecordList_C' AND TableChildID=" + TableChildID.ToString()) != "")
        {
            _bHaveService = true;
            _strServiceTypeWHR = "ServiceType='RecordList_C' AND TableChildID=" + TableChildID.ToString();

            /* This is for Child table as tab list; 
            * when this child table Add record; 
            * the ParentRecordIDList value is came from RecordDetail.aspx.cs 
            * passed through function Make child tables dyncamically - Red 28112018 */
            if (ParentRecordIDList != null)
            {
                hfParentRecordIDList.Value = ParentRecordIDList.ToString();
            }
            /* =End= */
        }

        GetRoleRight();

        _bEqualOrGreaterOperator = Common.SO_SearchAllifToIsNull(_theTable.AccountID, _theTable.TableID);

        if (_bEqualOrGreaterOperator == true)// && strLowerDate.Trim() != "" && strUpperDate.Trim() == "")
        {
            _strEqualOrGreaterOperator = " >= ";
        }
        /* encountered a problem here; why did we "ELSE IF" below codes?
         * i think theyre not related with the "IF" condition above? 
         * so i removed and transferred my codes below*/
        else if (Request.QueryString["RecordID"] != null && Request.QueryString["TableID"] != null)
        {
            _iParentTableID = int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
        }

       if (Request.QueryString["RecordID"] != null)
       {
          /* This is for Add or Edit a parent Record; 
          * and the child table (as tab) is configured 
          * show when add parent record - Red 28112018 */
            hfParentRecordIDList.Value = Cryptography.Decrypt(Request.QueryString["RecordID"].ToString());
          /* =End= */
        }

        FindTheView();
        if (_theView == null)
            return;

        if (_theTable == null)
        {
            goto EndSub;//why?
        }

        SetCosmetic();

        JSCode();

        CreateDynamicControls();


        if (_bHaveService)
        {
            ProcessServiceForEvent("ASP_Page_Init", sender, e);
        }


        EndSub:
        int Fxx;
        //
    }

    public string[] GetDataKeyNames()
    {
        string[] strRecordID = new string[1];
        strRecordID[0] = "DBGSystemRecordID";
        return strRecordID;
    }

    protected string GetTextFromValueForDD(string strDropdownValues, string strMainValue)
    {
        if (strMainValue == "")
            return "";

        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        string strValue = "";
        string strText = "";

        strMainValue = Server.HtmlDecode(strMainValue);
        foreach (string s in result)
        {
            strValue = "";
            strText = "";
            if (s.IndexOf(",") > -1)
            {
                strValue = s.Substring(0, s.IndexOf(","));
                strText = s.Substring(strValue.Length + 1);
                if (strValue == strMainValue)
                {
                    return strText;
                }
            }
        }
        return strText;
    }


    //protected string GetImageFromValueForDD(string strDropdownValues, string strMainValue)
    //{
    //    OptionImageList theOptionImageList = JSONField.GetTypedObject<OptionImageList>(strDropdownValues);
    //    foreach (OptionImage aOptionImage in theOptionImageList.ImageList)
    //    {
    //        if(aOptionImage.Value==strMainValue)
    //        {
    //            return "<img src='" + _strFilesLocation + "/UserFiles/AppFiles/" + aOptionImage.UniqueFileName + "' title='" + aOptionImage .Value+ "' />";
    //        }

    //    }
    //    return strMainValue;
    //}
    //protected string GetTextFromValueForList(string strDropdownValues, string strMainValue)
    //{
    //    string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
    //    string[] values = strMainValue.Split(new string[] { ","}, StringSplitOptions.RemoveEmptyEntries);

    //    string strValue = "";
    //    string strText = "";
    //    string strTotalText = "";
    //    foreach (string s in result)
    //    {
    //        strValue = "";
    //        strText = "";
    //        if (s.IndexOf(",") > -1)
    //        {
    //            strValue = s.Substring(0, s.IndexOf(","));
    //            strText = s.Substring(strValue.Length + 1);

    //            foreach (string v in values)
    //            {
    //                if (strValue == v)
    //                {
    //                    strTotalText = strTotalText + strText + ",";
    //                }
    //            }


    //        }
    //    }
    //    if(strTotalText.Length>0)
    //        strTotalText=strTotalText.Substring(0,strTotalText.Length-1);

    //    return strTotalText;
    //}

    //protected string GetTextFromTableForList(int iTableTableID, int? iLinkedParentColumnID, string strDisplayColumn, string strMainValue)
    //{

    //    //it's a new dev so iLinkedParentColumnID must be RecordID

    //    DataTable dtParents = Common.spGetLinkedRecordIDnDisplayText(strDisplayColumn, iTableTableID, Common.MaxRowForListBoxTable, null, null);

    //    string[] values = strMainValue.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);


    //    string strTotalText = "";
    //    foreach (DataRow dr in dtParents.Rows)
    //    {            
    //            foreach (string v in values)
    //            {
    //                if (dr[0].ToString() == v)
    //                {
    //                    strTotalText = strTotalText + dr[1].ToString() + ",";
    //                }
    //            }            
    //    }
    //    if (strTotalText.Length > 0)
    //        strTotalText = strTotalText.Substring(0, strTotalText.Length - 1);

    //    return strTotalText;
    //}


    protected string GetValueFromTextForList(string strDropdownValues, string strMainText)
    {
        if (strMainText == "")
            return "";

        string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);


        string strValue = "";
        string strText = "";

        foreach (string s in result)
        {
            strValue = "";
            strText = "";
            if (s.IndexOf(",") > -1)
            {
                strValue = s.Substring(0, s.IndexOf(","));
                strText = s.Substring(strValue.Length + 1);

                if (strText == strMainText || strText.IndexOf(strMainText) > -1)
                {
                    return strValue;
                }

            }
        }


        return "";
    }

    protected void PopulateUser()
    {
        //int iTN = 0;


        //ddlEnteredBy.DataSource = Common.DataTableFromText("SELECT (FirstName + ' ' + LastName) as FullName, UserID FROM [User]" +
        //   " WHERE AccountID=" + Session["AccountID"].ToString() + " OR UserID=" + SystemData.SystemOption_ValueByKey("AutoUploadUserID") +
        //    " ORDER BY FirstName", null, null);
        ddlEnteredBy.Items.Clear();
        ddlEnteredBy.DataSource = Common.DataTableFromText("SELECT (FirstName + ' ' + LastName) as FullName, [User].UserID FROM [User] INNER JOIN UserRole ON [User].UserID=UserRole.UserID " +
           " WHERE UserRole.AccountID=" + Session["AccountID"].ToString() + " OR UserRole.UserID=" + SystemData.SystemOption_ValueByKey_Account("AutoUploadUserID", null, TableID) +
            " ORDER BY FirstName");

        ddlEnteredBy.DataBind();
        ListItem liAll = new ListItem("All", "-1");
        ddlEnteredBy.Items.Insert(0, liAll);

    }





    protected void PopulateBatch()
    {
        //RP Removed Ticket 4363
        //ddlUploadedBatch.Items.Clear();
        //ddlUploadedBatch.DataSource = Common.DataTableFromText("SELECT BatchID,BatchDescription FROM Batch WHERE  IsImported=1 and  TableID=" + _qsTableID);
        //ddlUploadedBatch.DataBind();
        //ListItem liAll = new ListItem("All", "");
        //ddlUploadedBatch.Items.Insert(0, liAll);
        //End Modification
    }


    //protected void PopulateRecordGroupFilter()
    //{
    //    int iTN = 0;
    //    ddlRecordGroupFilter.DataSource = RecordManager.ets_Menu_Select(null, "", null,
    //    int.Parse(Session["AccountID"].ToString()), true,
    //    "Menu", "ASC", null, null, ref iTN);
    //    ddlRecordGroupFilter.DataBind();
    //    ListItem liAll = new ListItem("All", "-1");
    //    ddlRecordGroupFilter.Items.Insert(0, liAll);

    //    PopulateTableDDL();

    //}


    //protected string GetDDLValueFromText(string strDropdownValues, string strSearchText)
    //{

    //    string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

    //    string strValue = "";
    //    string strText = "";

    //    foreach (string s in result)
    //    {
    //        //ListItem liTemp = new ListItem(s, s.ToLower());
    //        strValue = "";
    //        strText = "";
    //        if (s.IndexOf(",") > -1)
    //        {
    //            strValue = s.Substring(0, s.IndexOf(","));
    //            strText = s.Substring(strValue.Length + 1);
    //            if (strValue != "" && strText != "")
    //            {
    //                if (strText.ToLower() == strSearchText.ToLower())
    //                {
    //                    return strValue;
    //                }
    //            }
    //        }
    //    }

    //    return "";

    //}


    protected void PopulateTableDDL()
    {
        int iTN = 0;
        ddlTable.DataSource = RecordManager.ets_Table_Select(null,
                null, null,
                int.Parse(Session["AccountID"].ToString()),
                null, null, null,
                "st.TableName", "ASC",
                null, null, ref iTN, Session["STs"].ToString());

        ddlTable.DataBind();
        if (iTN == 0)
        {
            ListItem liAll = new ListItem("None", "-1");
            ddlTable.Items.Insert(0, liAll);
        }

    }



    //public override void VerifyRenderingInServerForm(Control control)
    //{
    //    return;
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Session["SearchCriteriaStatus"] = null;
            Session["ExportClass"] = null;
            if (_theView != null)
            {
                Session["ViewItemID" + _theView.ViewID.ToString()] = null;
            }
            PopulateDynamicControls();
            hlAddTableSave.NavigateUrl = "~/Pages/Graph/AddGraph.aspx?type=t";

        }

        if (TableChildID == null)
        {
            divAddTable.Visible = true;
            string sFancyAddTableToReport = @"$(function () {
                                                $('.popupaddtable').fancybox({
                                                    iframe : {
                                                        css : {
                                                            width : '800px',
                                                            height: '400px'
                                                        }
                                                    },       
                                                    toolbar  : false,
	                                                smallBtn : true, 
                                                    scrolling: 'auto',
                                                    type: 'iframe',
                                                    titleShow: false
                                                });
                                            });";

            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "sFancyAddTableToReport", sFancyAddTableToReport, true);
        }

        //RP Added Ticket 4363
        String batchuploadautocomplete = "function GetUploadedBatchID(source, eventArgs) { var hdnkey = eventArgs.get_value();document.getElementById('" + hiddenUploadedBatchID.ClientID + "').value = hdnkey; }" +
                                          "function ChangeIconPopulating(s,e){document.getElementById('uploadbatchpopulated').style.display = 'none';document.getElementById('uploadbatchpopulating').style.display = 'inline';}" +
                                          "function ChangeIconPopulated(s,e){document.getElementById('uploadbatchpopulating').style.display = 'none';document.getElementById('uploadbatchpopulated').style.display = 'inline';}";
        ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "", batchuploadautocomplete, true);
        //End Modification

        //RP Added Ticket 4363
        //String changeicon = "function ChangeIcon(source, eventArgs){console.log('Populating');}";
        //ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "", changeicon, true);
        //End Modification

        if (_theTable == null)
            return;

        Session["cbcRecordList"] = null;

        /* === Red: lets check the page type
        * RecorDetail as parent possibly used the session already === */
        if (PageType == "p")
        {
            Session["RecordListSeries"] = null;
        }

        Session["RecordListFrom"] = null;
        Session["RecordListTo"] = null;
        Session["RecordListSQL"] = null;

        if (IsRecordDetail == false)
        {
            Session["DictionaryRecordListSQL"] = null;
        }


        if (Request.RawUrl.IndexOf("EachRecordTable.aspx") > -1)
        {
            _strNoAjaxView = "noajax=yes&";
        }
        else
        {
            _strNoAjaxView = "";
        }

        try
        {

            string strMaxCharactersInCell = SystemData.SystemOption_ValueByKey_Account("MaxCharactersInCell", _theTable.AccountID, _theTable.TableID);
            if (strMaxCharactersInCell != "")
                _iMaxCharactersInCell = int.Parse(strMaxCharactersInCell);

            if (!IsPostBack)
            {
                ViewState["ViewAlignmentDefault"] = Common.SO_ViewAlignmentDefault(_theTable.AccountID, _theTable.TableID);
                string HidePagerGoButton = SystemData.SystemOption_ValueByKey_Account("Hide Pager Go Button", _theTable.AccountID, _theTable.TableID);
                if (HidePagerGoButton != "")
                {
                    if (HidePagerGoButton.ToLower() == "yes")
                    {
                        hfHidePagerGoButton.Value = "yes";
                    }
                }

            }

        }
        catch
        {
            //
        }
        // int.Parse(System.Web.HttpContext.Current.Session["AccountID"].ToString())
        string strDeleteReasonRequired = SystemData.SystemOption_ValueByKey_Account("DeleteReasonRequired",
          _theTable.AccountID, int.Parse(TableID.ToString()));

        if (strDeleteReasonRequired != "")
        {
            if (strDeleteReasonRequired.ToLower() == "yes")
            {
                _bDeleteReason = true;
            }
        }

        if (_theTable != null && _theTable.ArchiveMonths == null)
        {
            try
            {
                // chkUseArchive.Checked = false;
                trUseArchive.Visible = false;
            }
            catch
            {
                //red
            }

        }
        //red Ticket 3358 - this is from recordetail page
        if (Session["useArchiveData_RecordDetail" + _theTable.TableID] != null)
        {
            if (Session["useArchiveData_RecordDetail" + _theTable.TableID].ToString() == _theTable.TableID.ToString())
            {
                /* chkUseArchive.Checked = true; 
                  lblUseArchive.Font.Bold = true; */ // Disabled by Red - Ticket 4371

                bUseArchiveData = true; // Red 08012018               
                Session["useArchiveData_RecordDetail" + _theTable.TableID] = null;
            }

        }

        //if (chkUseArchive.Checked)
        //{
        //    lblUseArchive.Font.Bold = true;
        //}
        //else
        //{
        //    chkUseArchive_CheckedChanged(null, null);
        //}

        //int.Parse(System.Web.HttpContext.Current.Session["AccountID"].ToString())
        string strGraphIcon = SystemData.SystemOption_ValueByKey_Account("Graph Icon", _theTable.AccountID, int.Parse(TableID.ToString()));

        if (strGraphIcon != "")
        {
            if (strGraphIcon.ToLower() == "no")
            {
                _bShowGraphIcon = false;
            }
        }

        if (_bShowGraphIcon == false)
        {
            divShowGraph.Visible = false;
        }
        else
        {
            divShowGraph.Visible = true;
        }


        //Title = "Records";
        if (Request.RawUrl.IndexOf("Default.aspx") > -1)
        {
            _bOpenInParent = true;

        }
        //gvTheGrid.Style.Add("width", "100%");
        if (Request.QueryString["Dashboard"] != null)
        {
            _bOpenInParent = true;
            //pnlSearch.Visible = false;
            //chkShowAdvancedOptions.Checked = false;
            //hplNewData.Target = "_parent";
            //hplNewDataFilter.Target = "_parent";
            //hplNewDataFilter2.Target = "_parent";

            string strW = "1";
            if (Request.QueryString["width"] != null)
            {
                if (_bResponsive == false)
                    gvTheGrid.Style.Add("min-width", "450px");

                strW = "2";
            }
            else
            {
                //MR
                if (_bResponsive == false)
                    gvTheGrid.Style.Add("min-width", "1000px");

            }

            //ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "autoResizeMe", "<script>autoResizeMe('" + UpdatePanel1.ClientID + "'," + strW + " );autoResizeMe('" + tblTopCaption.ClientID + "'," + strW + " ); </script>", false);
        }
        else
        {

            //MR
            if (_bResponsive == false)
                gvTheGrid.Style.Add("min-width", "1000px");

        }




        try
        {

            _theAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));

            if (PageType == "p")
            {
                if (_strViewPageType == "dash")
                {
                    tdTopButtons.Visible = false;
                    tdTopTitile.Style.Add("width", "100%");
                }
                else
                {
                    //tblTopCaption.Style.Add("width", "1000px;");
                }

                if (!IsPostBack)
                {

                    //Standardised_Field_Table
                    if ((int)_theTable.AccountID != (int)_theUserRole.AccountID && !Common.HaveAccess(_strRecordRightID, "1"))
                    {
                        Response.Redirect("~/Empty.aspx", true);
                        return;
                    }

                    if (!Common.HaveAccess(_strRecordRightID, "1"))
                    {
                        string strSFTID = SystemData.SystemOption_ValueByKey_Account("Standardised_Field_Table", _theTable.AccountID, _theTable.TableID);
                        if (strSFTID != "")
                        {
                            if (strSFTID == _theTable.TableID.ToString())
                            {
                                Response.Redirect("~/Empty.aspx", true);
                                return;
                            }
                        }
                    }


                }
            }
            else
            {
                trRecordListTitle.Visible = false;
            }


            hlBatches.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/Batches.aspx?TableID=" + Cryptography.Encrypt(TableID.ToString());
            _qsTableID = TableID.ToString();

            //RP Added Ticket 4363
            Session["BatchUploadID"] = _qsTableID;
            //End Modification

            cbcSearch1.TableID = TableID;
            cbcSearch2.TableID = TableID;
            cbcSearch3.TableID = TableID;
            cbcSearchMain.TableID = TableID;

            if (_theView != null)
            {
                cbcSearch1.ViewID = _theView.ViewID;
                cbcSearch2.ViewID = _theView.ViewID;
                cbcSearch3.ViewID = _theView.ViewID;
                cbcSearchMain.ViewID = _theView.ViewID;
                //cbcvSumFilter.ViewID = _theView.ViewID;
            }



            if (_strRecordRightID == Common.UserRoleType.None) //none role
            {
                Response.Redirect(Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Empty.aspx", false);
                return;
            }



            //Ticket 846: Removing dropdown when in mobile
            //modified by: Ismael
            ddlTableMenu.Visible = false;
            if (ShowTitle)
            {
                lblTitle.Visible = true;
            }
            //End Ticket 846


            if (!IsPostBack)
            {



                if (Request.QueryString["TextSearch"] != null)
                {
                    hfTextSearch.Value = Cryptography.Decrypt(Request.QueryString["TextSearch"].ToString());
                }
                //if( !string.IsNullOrEmpty(TextSearch) && PageType=="p" && Request.RawUrl.IndexOf("RecordDetail.aspx")>-1 )
                //{
                //    hfTextSearch.Value = TextSearch;
                //}

                ddlJoinOperator1.Attributes.Add("onchange", "toggleAndOr('" + ddlJoinOperator1.ClientID + "','" + hfAndOr1.ClientID + "');return false;"); //Red 3715
                ddlJoinOperator2.Attributes.Add("onchange", "toggleAndOr('" + ddlJoinOperator2.ClientID + "','" + hfAndOr2.ClientID + "');return false;"); //Red 3715
                ddlJoinOperator3.Attributes.Add("onchange", "toggleAndOr('" + ddlJoinOperator3.ClientID + "','" + hfAndOr3.ClientID + "');return false;"); //Red 3715

                //lnkAndOr1.Attributes.Add("text", "and");

                lnkAddSearch1.Attributes.Add("onclick", "$('#" + trSearch1.ClientID + "').show();$('#" + trSearch1a.ClientID + "').hide();$('#" + trSearch2a.ClientID + "').hide();$('#" + trSearch3a.ClientID + "').hide();$('#" + lnkAddSearch1.ClientID + "').hide();if ($('#" + hfAndOr1.ClientID + "').val()==''){ $('#" + hfAndOr1.ClientID + "').val(document.getElementById('" + ddlJoinOperator1.ClientID + "').value)};");//return false;
                lnkAddSearch2.Attributes.Add("onclick", "$('#" + trSearch2.ClientID + "').show();$('#" + trSearch2a.ClientID + "').hide();$('#" + trSearch3a.ClientID + "').hide();$('#" + lnkAddSearch2.ClientID + "').hide();if ($('#" + hfAndOr2.ClientID + "').val()==''){$('#" + hfAndOr2.ClientID + "').val(document.getElementById('" + ddlJoinOperator2.ClientID + "').value)};");//return false;
                lnkAddSearch3.Attributes.Add("onclick", "$('#" + trSearch3.ClientID + "').show();$('#" + trSearch3a.ClientID + "').hide();$('#" + lnkAddSearch3.ClientID + "').hide();if ($('#" + hfAndOr3.ClientID + "').val()==''){$('#" + hfAndOr3.ClientID + "').val(document.getElementById('" + ddlJoinOperator3.ClientID + "').value)};");//return false;

                lnkMinusSearch1.Attributes.Add("onclick", "$('#" + trSearch1.ClientID + "').hide();$('#" + trSearch1a.ClientID + "').show();$('#" + trSearch2a.ClientID + "').hide();$('#" + trSearch3a.ClientID + "').hide();$('#" + lnkAddSearch1.ClientID + "').show();$('#" + hfAndOr1.ClientID + "').val('');$('#" + cbcSearch1.ddlYAxisClientID + "').val('');$('#" + cbcSearch2.ddlYAxisClientID + "').val('');$('#" + cbcSearch3.ddlYAxisClientID + "').val('');return false;");
                lnkMinusSearch2.Attributes.Add("onclick", "$('#" + trSearch2.ClientID + "').hide();$('#" + trSearch2a.ClientID + "').show();$('#" + trSearch3a.ClientID + "').hide();$('#" + lnkAddSearch2.ClientID + "').show();$('#" + hfAndOr2.ClientID + "').val('');$('#" + cbcSearch2.ddlYAxisClientID + "').val('');$('#" + cbcSearch3.ddlYAxisClientID + "').val('');return false;");
                lnkMinusSearch3.Attributes.Add("onclick", "$('#" + trSearch3.ClientID + "').hide();$('#" + trSearch3a.ClientID + "').show();$('#" + lnkAddSearch3.ClientID + "').show();$('#" + hfAndOr3.ClientID + "').val('');$('#" + cbcSearch3.ddlYAxisClientID + "').val('');return false;");


                //ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "PutDefaultSearcUI", "$('#" + trSearch1.ClientID + "').hide();$('#" + trSearch2.ClientID + "').hide();$('#" + trSearch3.ClientID + "').hide();", true);


                PopulateTerminology();

                if (Session["GridPageSize"] != null && Session["GridPageSize"].ToString() != "")
                { gvTheGrid.PageSize = int.Parse(Session["GridPageSize"].ToString()); }

                if (_theView != null)
                {
                    if (_theView.RowsPerPage != null)
                    {
                        gvTheGrid.PageSize = (int)_theView.RowsPerPage;
                        _iMaxRows = gvTheGrid.PageSize;
                    }
                }

                if (TableID != null)
                {
                    if (Request.QueryString["warning"] != null)
                    {
                        chkShowOnlyWarning.Checked = true;
                    }

                    Table qsTable = RecordManager.ets_Table_Details(TableID);

                    if (qsTable.HideAdvancedOption != null)
                    {
                        if ((bool)qsTable.HideAdvancedOption)
                        {
                            hfHideAdvancedOption.Value = "yes";
                        }
                    }


                    hlConfig.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/TableDetail.aspx?mode=" + Cryptography.Encrypt("edit") + "&TableID=" + Cryptography.Encrypt(TableID.ToString()) + "#topline";

                    if (Common.HaveAccess(_strRecordRightID, "1,2"))
                    {
                        divConfig.Visible = true;
                    }


                    PopulateUser();
                    PopulateBatch();


                    PopulateMenuTableDDL();
                    ddlTableMenu.Text = qsTable.TableID.ToString();

                    PopulateTableDDL();
                    ddlTable.Text = TableID.ToString();

                }
                else
                {

                    PopulateUser();
                    PopulateBatch();
                    PopulateTableDDL();

                }

                if (Session["SCid" + hfViewID.Value] != null)
                {
                    if((Int32)Session["SCid" + hfViewID.Value] == -1)
                    Session["SCid" + hfViewID.Value] = null;
                    
                }

                if (Request.QueryString["SearchCriteriaID"] != null && Session["SCid" + hfViewID.Value] == null && PageType == "p") //&& PageType == "p"
                {
                    PopulateSearchCriteria(int.Parse(Cryptography.Decrypt(Request.QueryString["SearchCriteriaID"].ToString())), true);

                }
                else if (Session["SCid" + hfViewID.Value] != null)// || Session["SCid" + hfViewID.Value].ToString() != "-1") //RP Modified Ticket 4305 - Filter disappear here because of -1 value in Search Criteria ID
                {
                    //if(Page.IsPostBack) /* Red 18/03/2019: Commented this line out. It prevents the system to show the search history. */
                    PopulateSearchCriteria(int.Parse(Session["SCid" + hfViewID.Value].ToString()), true);
                }
                else if (Session["SCid" + hfViewID.Value] == null)
                {
                    int? iGetNewSCid = SystemData.UserSearch_GetNewSCid(new UserSearch(null, _ObjUser.UserID, int.Parse(hfViewID.Value), "", null));

                    if (iGetNewSCid != null && iGetNewSCid > 0)
                    {
                        Session["SCid" + hfViewID.Value] = iGetNewSCid;
                        PopulateSearchCriteria(int.Parse(Session["SCid" + hfViewID.Value].ToString()), false);
                    }
                    else
                    {
                        _bBindWithSC = false;
                    }
                }
                else
                {
                    _bBindWithSC = false;

                }

                if (_bBindWithSC)
                {
                    gvTheGrid.PageSize = _iMaxRows;
                    gvTheGrid.GridViewSortColumn = _strGridViewSortColumn;
                    if (_strGridViewSortDirection.ToUpper() == "ASC")
                    {
                        gvTheGrid.GridViewSortDirection = SortDirection.Ascending;
                    }
                    else
                    {
                        gvTheGrid.GridViewSortDirection = SortDirection.Descending;
                    }
                    //BindTheGrid(_iStartIndex, _iMaxRows);
                }
                else
                {

                    if (_qsTableID == null || _theView == null)
                        return;

                    //this is 1st screen

                    Table qsTable = RecordManager.ets_Table_Details(TableID);

                    SetGridFromView();

                }
            }
            else
            {
            }


            string strJSSearchShowHide = "";

            if (hfAndOr1.Value != "")
            {
                strJSSearchShowHide = "$('#" + trSearch1.ClientID + "').show();$('#" + trSearch1a.ClientID + "').hide();$('#" + lnkAddSearch1.ClientID + "').hide();";
            }

            if (hfAndOr2.Value != "")
            {
                //strJSSearchShowHide = "$('#" + trSearch1.ClientID + "').show();$('#" + lnkAddSearch1.ClientID + "').hide();$('#" + trSearch2.ClientID + "').show();$('#" + lnkAddSearch2.ClientID + "').hide();";
                strJSSearchShowHide = strJSSearchShowHide + "$('#" + trSearch2.ClientID + "').show();$('#" + trSearch2a.ClientID + "').hide();$('#" + lnkAddSearch2.ClientID + "').hide();";
            }

            if (hfAndOr3.Value != "")
            {
                //strJSSearchShowHide = "$('#" + trSearch1.ClientID + "').show();$('#" + lnkAddSearch1.ClientID + "').hide();$('#" + trSearch2.ClientID + "').show();$('#" + lnkAddSearch2.ClientID + "').hide();$('#" + trSearch3.ClientID + "').show();$('#" + lnkAddSearch3.ClientID + "').hide();";
                strJSSearchShowHide = strJSSearchShowHide + "$('#" + trSearch3.ClientID + "').show();$('#" + trSearch3a.ClientID + "').hide();$('#" + lnkAddSearch3.ClientID + "').hide();";
            }


            if (strJSSearchShowHide != "")
                ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "PutDefaultSearcUI_PB" + _strDynamictabPart, strJSSearchShowHide, true);


            if (PageType == "p")
            {
                //Page.Title = _theTable.TableName + " - " + "Records";
                if (_theView != null)
                {
                    Page.Title = _theView.ViewName;
                    lblTitle.Text = _theView.ViewName;
                }
                else
                {
                    Page.Title = _theTable.TableName + " - " + "Records";
                    lblTitle.Text = "Records - " + _theTable.TableName; ;
                }
            }

            //Title = _theTable.TableName;// +" - Records"; 
            //lblTitle.Text = Title;
            if (!IsPostBack)
            {
                BindTheGrid(_iStartIndex, gvTheGrid.PageSize);
            }
            GridViewRow gvr = gvTheGrid.TopPagerRow;
            if (gvr != null)
            {
               _gvPager = (Common_Pager)gvr.FindControl("Pager");
            }

        }
        catch (Exception ex)
        {
                       ErrorLog theErrorLog = new ErrorLog(null, "Record List: Page Load", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                        SystemData.ErrorLog_Insert(theErrorLog);
        }

        string strJS_SearchBox = @"

                 $(document).ready(function () {
                                  function ShowHide() {

                                    var chk = document.getElementById('ctl00_HomeContentPlaceHolder_rlOne_chkShowAdvancedOptions');
                                    var x = document.getElementById('" + tblAdvancedOption.ClientID + @"');
                                    var trChkOnlyWarning = document.getElementById('" + trChkShowOnlyWarning.ClientID + @"');

                                    if(chk==null)
                                    {
                                        return;
                                    }
                                   
                                    if (chk.checked == false) {
                                            $('#" + tdFilterDynamic.ClientID + @"').show();
                                            $('#" + tdFilterYAxis.ClientID + @"').hide();
                                            trChkOnlyWarning.style.display = 'none';
                                        }  
                                    if (chk.checked == true) {
                                             $('#" + tdFilterYAxis.ClientID + @"').show();
                                             $('#" + tdFilterDynamic.ClientID + @"').hide();
                                            trChkOnlyWarning.style.display = 'table-row';
                                        } 

                                     if (chk.checked == false) { x.style.display = 'none'; }
                                    if (chk.checked == true) { x.style.display = 'inline'; }   




                                    var hfHideAdvancedOption = document.getElementById('" + hfHideAdvancedOption.ClientID + @"');
                                    if (hfHideAdvancedOption.value == 'yes') {
                                        $('#" + tblAdvancedOption.ClientID + @"').hide();
                                        $('#" + chkShowAdvancedOptions.ClientID + @"').hide(); 
                                        $('#" + /*tblAdvancedOptionChk*/ lblAdvancedCaption.ClientID + @"').hide();
                                        $('#" + tblAdvancedOptionChkC.ClientID + @"').hide();
                                        
                                    }   
                                    else
                                    {
                                        
                                    }      
                                }
                                ShowHide();
                                   if (window.addEventListener)
                                    window.addEventListener('load', ShowHide, false);
                                else if (window.attachEvent)
                                    window.attachEvent('onload', ShowHide);
                                else if (document.getElementById)
                                    window.onload = ShowHide;
                         });



                         $(document).ready(function () {


                                    $('#ctl00_HomeContentPlaceHolder_rlOne_chkShowAdvancedOptions').click(function () {
                                    var chk = document.getElementById('ctl00_HomeContentPlaceHolder_rlOne_chkShowAdvancedOptions');

                                        if (chk.checked == true) {
                                            $('#" + tblAdvancedOption.ClientID + @"').show();
                                            $('#" + tdFilterYAxis.ClientID + @"').show();
                                            $('#" + tdFilterDynamic.ClientID + @"').hide();
                                        }
                                        else {
                                            $('#" + tblAdvancedOption.ClientID + @"').hide();
                                            $('#" + tdFilterDynamic.ClientID + @"').show();
                                            $('#" + tdFilterYAxis.ClientID + @"').hide();
                                        }

                                    });
                                
                                });

                ";




        string strCellToolTip = @"var mouseX;
                                var mouseY;
                                $(document).mousemove(function (e) {
                                    try
                                    {
                                        mouseX = e.pageX;
                                        mouseY = e.pageY;
                                    }
                                    catch (err)
                                    {
                                       // alert(err.message)
                                    }
        
                                });
    

                                $(function () {
        
                                    $('.js-tooltip-container').hover(function () {
                                        //$(this).find('.js-tooltip').show();
                                        try {
                                            $(this).find('.js-tooltip').addClass('ajax-tooltip');
                                            $(this).find('.ajax-tooltip').css({ 'top': mouseY, 'left': mouseX }).fadeIn('slow');
                                        }
                                        catch (err) {
                                           // alert(err.message);
                                        }
                                    }, function () {
                                        try {
                                            $(this).find('.js-tooltip').hide();
                                            $(this).find('.js-tooltip').removeClass('ajax-tooltip');
                                            $(this).find('.ajax-tooltip').css({ 'top': mouseY, 'left': mouseX }).fadeOut('slow');
                                        }
                                        catch (err) {
                                            //alert(err.message);
                                        }
                                    });
       
                                });";



        if (PageType == "p")
        {
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "strCellToolTip", strCellToolTip, true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "strCellToolTip" + (DetailTabIndex - 1).ToString(), strCellToolTip, true);
        }

        // ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "strCellToolTip", strCellToolTip, true);


        if (PageType == "c")
        {
            strJS_SearchBox = @"

                              $(document).ready(function () {
                                  function ShowHide" + (DetailTabIndex - 1).ToString() + @"() {

                                    var chk = document.getElementById('" + chkShowAdvancedOptions.ClientID + @"');
                                    var x = document.getElementById('" + tblAdvancedOption.ClientID + @"');
                                    var trChkOnlyWarning = document.getElementById('" + trChkShowOnlyWarning.ClientID + @"');
                                    
                                    var tdFilterDynamic = document.getElementById('" + tdFilterDynamic.ClientID + @"');
                                    var tdFilterYAxis = document.getElementById('" + tdFilterYAxis.ClientID + @"');

                                    if (chk!=null &&  chk.checked == false) {  
                                           tdFilterDynamic.style.display = 'inline';tdFilterYAxis.style.display = 'none';
                                           trChkOnlyWarning.style.display = 'none';
                                        }
                                    if (chk!=null && chk.checked == true) { 
                                            tdFilterDynamic.style.display = 'none';tdFilterYAxis.style.display = 'inline';
                                            trChkOnlyWarning.style.display = 'table-row';
                                        } 

 
                                    if (chk!=null && chk.checked == false) { x.style.display = 'none'; }
                                    if (chk!=null && chk.checked == true) { x.style.display = 'inline'; } 
                                        var hfHideAdvancedOption = document.getElementById('" + hfHideAdvancedOption.ClientID + @"');
                                    if (hfHideAdvancedOption!=null && hfHideAdvancedOption.value == 'yes') {
                                        //$('#" + tblAdvancedOption.ClientID + @"').hide();
                                       // $('#" + tblAdvancedOptionChk.ClientID + @"').hide();
                                        var x1 = document.getElementById('" + tblAdvancedOptionChk.ClientID + @"');
                                        x1.style.display = 'none';
                                        var x2 = document.getElementById('" + tblAdvancedOptionChkC.ClientID + @"');
                                        x2.style.display = 'none';
                                    }            
                                }
                                ShowHide" + (DetailTabIndex - 1).ToString() + @"();
                         });


                         $(document).ready(function () {


                                    $('#" + chkShowAdvancedOptions.ClientID + @"').click(function () {
                                        var chk = document.getElementById('" + chkShowAdvancedOptions.ClientID + @"');

                                    var tdFilterDynamic = document.getElementById('" + tdFilterDynamic.ClientID + @"');
                                     var tdFilterYAxis = document.getElementById('" + tdFilterYAxis.ClientID + @"');

                                        if (chk.checked == true) {
                                            $('#" + tblAdvancedOption.ClientID + @"').show();
                                             tdFilterDynamic.style.display = 'none';tdFilterYAxis.style.display = 'inline';

                                        }
                                        else {
                                            $('#" + tblAdvancedOption.ClientID + @"').hide();
                                           tdFilterDynamic.style.display = 'inline';tdFilterYAxis.style.display = 'none';

                                        }
                                    });                         

                                });

                        ";
        }

        if (this.Page.MasterPageFile != null && this.Page.MasterPageFile.ToLower().IndexOf("his") > -1)
        {
            trRecordListTitle.Visible = false;
        }

        if (PageType == "p")
        {
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "JSCode", strJS_SearchBox, true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "JSCode" + (DetailTabIndex - 1).ToString(), strJS_SearchBox, true);
        }



        //beforeClose onClosed
        string strFancy = @"
                    $(function () {
                            $('.popuplinkEV" + _strFunctionUQ + @"').fancybox({
                                 iframe : {
                                    css : {
                                        width : '1000px',
                                        height: '800px'
                                    }
                                },       
                                toolbar  : false,
	                            smallBtn : true,  
                                scrolling: 'auto',
                                type: 'iframe',
                                'transitionIn': 'elastic',
                                'transitionOut': 'none',
                                titleShow: false,                        
                              afterClose: function () { $('.ajax-indicator-full').show();window.parent.location.reload();}
                            });
                        });
                    
                ";

        // onClosed: function () { window.parent.document.getElementById('btnReloadMe').click();}
        // onClosed: function () { $('.ajax-indicator-full').show(); window.parent.location.reload();}
        //hlEditView.CssClass = "popuplinkEV" + _strFunctionUQ; window.parent.document.getElementById('"+btnRefreshViewChange.ClientID+@"').click();
        //hlEditView2.CssClass = hlEditView.CssClass;
        if (Request.RawUrl.IndexOf("EachRecordTable.aspx") > -1)
        {
            //_strNoAjaxView = "noajax=yes&";
        }
        else
        {
            //_strNoAjaxView = "";
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "popuplinkEV" + _strFunctionUQ, strFancy, true);
        }

        ControlCollection _collection = this.Controls;
        TheDatabase.SetValidationGroup(_collection, _strDynamictabPart);
        SetOtherValidationGroup();
        //put speed test here       


        if(chkShowAdvancedOptions.Checked)
        {
            string strJSSearchShowHideorig = "$('#" + trSearchMain.ClientID + "').show();";
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "PutDefaultSearcUI_CS_adv", strJSSearchShowHideorig, true);
        }

        if (_bHaveService)
        {

            ProcessServiceForEvent("ASP_Page_Load", sender, e);
        }

        

        if(!IsPostBack && DisplayInColumnID!=null)
        {
            gvTheGrid.CssClass = "gridviewnormal";
        }


        if (!IsPostBack)
        {
            string multipleSelectSelectedValue = @"
                var selectedValue = '';
                ";
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "multipleSelectSelectedValue", multipleSelectSelectedValue, true);
        }
    }



    protected void ProcessServiceForEvent(string strEventName, object sender, EventArgs e)
    {
        if (Common.GetValueFromSQL("SELECT TOP 1 ServiceID FROM [Service] WHERE ServiceJSON LIKE '%" + strEventName
            + "%' and " + _strServiceTypeWHR) != "")
        {

            string strEventSql = "SELECT ServiceID FROM [Service] WHERE ServiceJSON LIKE '%" + strEventName
                   + "%' and " + _strServiceTypeWHR;

            List<Service> lstService = SystemData.GetServiceList(strEventSql);
            List<object> roList = new List<object>();
            if (lstService != null)
            {
                foreach (Service aService in lstService)
                {
                    List<object> objList = new List<object>();
                    objList.Add(this);//THIS
                    aService.Temp_DynamicPartName = _strDynamictabPart;
                    objList.Add(aService);
                    if (!string.IsNullOrEmpty(aService.ServiceDefinition))
                    {
                        //add more to this
                    }

                    if (!string.IsNullOrEmpty(aService.ServiceJSON))
                    {
                        PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(aService.ServiceJSON);
                        if (thePageLifeCycleService != null)
                        {
                            if (!string.IsNullOrEmpty(thePageLifeCycleService.SPName))
                            {
                                List<object> roOneList = CustomMethod.DotNetMethod(thePageLifeCycleService.SPName, objList);
                                if (roOneList != null && roOneList.Count > 0)
                                {
                                    foreach (object oRO in roOneList)
                                    {
                                        roList.Add(oRO);
                                    }
                                }
                            }


                            if (!string.IsNullOrEmpty(thePageLifeCycleService.DotNetMethod))
                            {
                                List<object> rodnOneList = CustomMethod.DotNetMethod(thePageLifeCycleService.DotNetMethod, objList);
                                if (rodnOneList != null && rodnOneList.Count > 0)
                                {
                                    foreach (object oRO in rodnOneList)
                                    {
                                        roList.Add(oRO);
                                    }
                                }
                            }
                            if (!string.IsNullOrEmpty(thePageLifeCycleService.JavaScriptFunction))
                            {

                                ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "ServiceAutoJS" + aService.ServiceID.ToString(),
                                 SystemData.ErrorGuardForServiceJS(thePageLifeCycleService.JavaScriptFunction), true);

                            }
                        }
                    }
                }
            }
        }
    }



    // public static void ProcessEventServices(string strEventSql, List<object> poList)
    //{
    //    List<Service> lstService = SystemData.GetServiceList(strEventSql);

    //    if (lstService != null)
    //    {
    //        List<object> objList = new List<object>();




    //        if (poList != null && poList.Count > 0 && poList[0] != null)
    //        {
    //            try
    //            {
    //                thePage = (System.Web.UI.Page)poList[0];
    //            }
    //            catch
    //            {
    //                //
    //            }

    //        }

    //        foreach (object poEach in poList)
    //        {
    //            objList.Add(poEach);
    //        }
    //        //process each service
    //        List<object> roAllList = new List<object>();
    //        foreach (Service aService in lstService)
    //        {
    //            objList.Add(aService);
    //            List<object> roOneList = new List<object>();
    //            if (!string.IsNullOrEmpty(aService.ServiceJSON))
    //            {
    //                PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(aService.ServiceJSON);
    //                if (thePageLifeCycleService != null)
    //                {

    //                    if (!string.IsNullOrEmpty(thePageLifeCycleService.SPName))
    //                    {
    //                        roOneList = CustomMethod.DotNetMethod(thePageLifeCycleService.SPName, objList);
    //                        if (roOneList != null)
    //                            roAllList.Add(roOneList);
    //                    }


    //                    if (!string.IsNullOrEmpty(thePageLifeCycleService.DotNetMethod))
    //                    {
    //                        if (roOneList != null)
    //                            objList.Add(roOneList);

    //                        List<object> rodnOneList = CustomMethod.DotNetMethod(thePageLifeCycleService.DotNetMethod, objList);
    //                        if (rodnOneList != null)
    //                            roAllList.Add(rodnOneList);
    //                    }
    //                    if (!string.IsNullOrEmpty(thePageLifeCycleService.JavaScriptFunction))
    //                    {

    //                        if (thePage != null)
    //                        {
    //                            thePage.ClientScript.RegisterStartupScript(thePage.GetType(), "ServiceAutoJS" + aService.ServiceID.ToString(),
    //                                 ErrorGuardForServiceJS(thePageLifeCycleService.JavaScriptFunction), true);
    //                        }


    //                    }
    //                }

    //            }

    //        }
    //    }
    //}


    protected void EnsureSecurity(int iTN)
    {
        //GridViewRow gvr = gvTheGrid.TopPagerRow;

        if (_gvPager == null)
        {
            GridViewRow gvr = gvTheGrid.TopPagerRow;
            if (gvr != null)
            {
                _gvPager = (Common_Pager)gvr.FindControl("Pager");
            }
        }

        bool bShowAdd = false;
        bool bShowEdit = false;
        bool bShowDelete = false;
        bool bShowParmaDelete = false;
        bool bShowEditView = false;
        bool bShowViewIcon = false;
        bool bShowBulkEdit = false;

        if (_theView != null)
        {
            if ((bool)_theView.ShowBulkUpdateIcon == true)
                bShowBulkEdit = true;
            if ((bool)_theView.ShowAddIcon == true)
                bShowAdd = true;
            if ((bool)_theView.ShowDeleteIcon == true)
                bShowDelete = true;
            //RP MOdified Ticket 4305
            //if ((bool)_theView.ShowEditIcon == true 
            //    || _strRecordRightID != Common.UserRoleType.ReadOnly 
            //    || _strRecordRightID != Common.UserRoleType.AddRecord)
            //    bShowEdit = true;
            //End Modification
            if ((bool)_theView.ShowViewIcon == true)
                bShowViewIcon = true;
        }


        if (_strRecordRightID == Common.UserRoleType.None)
        {
            try
            {
                Response.Redirect("~/Empty.aspx", true);
                return;
            }
            catch
            {

            }

        }
        else if (_strRecordRightID == Common.UserRoleType.ReadOnly)
        {
            bShowAdd = false;
            bShowEdit = false;
            bShowDelete = false;
            bShowParmaDelete = false;
            bShowBulkEdit = false;
            //gvTheGrid.Columns[0].Visible = false;//delete check

            gvTheGrid.Columns[2].Visible = false;//edit   


            gvTheGrid.Columns[3].Visible = bShowViewIcon;//view

            divUpload.Visible = false;

        }
        else if (_strRecordRightID == Common.UserRoleType.OwnData)
        {
            bShowDelete = false;
            bShowParmaDelete = false;
            bShowBulkEdit = false;

            gvTheGrid.Columns[2].Visible = bShowEdit;//edit   
            gvTheGrid.Columns[3].Visible = bShowViewIcon;//view

            divUpload.Visible = true;
            divEmail.Visible = true;
        }
        else if (_strRecordRightID == Common.UserRoleType.EditOwnViewOther)
        {
            bShowDelete = false;
            bShowParmaDelete = false;
            bShowBulkEdit = false;

            gvTheGrid.Columns[2].Visible = bShowEdit;//edit   
            gvTheGrid.Columns[3].Visible = bShowViewIcon;//view

            divUpload.Visible = true;

            divEmail.Visible = true;
        }
        else if (_strRecordRightID == Common.UserRoleType.AddRecord)
        {
            bShowDelete = false;
            bShowParmaDelete = false;
            bShowBulkEdit = false;
            gvTheGrid.Columns[2].Visible = false;//edit   
            gvTheGrid.Columns[3].Visible = bShowViewIcon;//view

            divUpload.Visible = true;
        }
        else if (_strRecordRightID == Common.UserRoleType.AddEditRecord)
        {
            bShowParmaDelete = false;
            //RP Modified Ticket 5113
            //gvTheGrid.Columns[2].Visible = bShowEdit;//edit   
            gvTheGrid.Columns[2].Visible = bShowEdit || _strRecordRightID == Common.UserRoleType.AddEditRecord;
            //End Modification
            gvTheGrid.Columns[3].Visible = false;//view

        }
        else if (_strRecordRightID == Common.UserRoleType.Administrator)
        {
            if ((bool)_theUserRole.IsAccountHolder)
            {
                bShowParmaDelete = true;
            }
            else
            {
                bShowParmaDelete = false;

                if (_theUserRole.AllowDeleteRecord != null && (bool)_theUserRole.AllowDeleteRecord)
                {
                    bShowParmaDelete = true;
                }
            }
            gvTheGrid.Columns[2].Visible = true;//edit   
            gvTheGrid.Columns[3].Visible = false;//view

        }
        else if (_strRecordRightID == Common.UserRoleType.GOD)
        {
            bShowParmaDelete = true;
            gvTheGrid.Columns[2].Visible = true;//edit   
            gvTheGrid.Columns[3].Visible = false;//view

        }

        if (PageType == "c")
        {
            if (ShowAddButton == false)
            {
                bShowAdd = false;

            }

            //red 3285 06112017
            if (ShowEditButton == false)
            {
                gvTheGrid.Columns[2].Visible = false;//edit   
                gvTheGrid.Columns[3].Visible = true;//view

            }
            //red end
        }

        if (_gvPager != null)
        {
            _bAllowedToDelete = false;
            _gvPager.HideAdd = true;
            _gvPager.HideEditMany = true;
            _gvPager.HideDelete = true;
            _gvPager.ShowCopyRecord = false;
            _gvPager.HideUnDelete = true;
            _gvPager.HideParmanentDelete = true;
            if (chkShowDeletedRecords.Checked == false)
            {
                if (bShowDelete)
                {
                    _gvPager.HideDelete = false;
                    _bAllowedToDelete = true;

                }
                    
            }
            else
            {
                if (bShowDelete)
                {
                    _gvPager.HideUnDelete = false;
                    _bAllowedToDelete = true;

                }
                   
                if (bShowParmaDelete)
                {
                    _gvPager.HideParmanentDelete = false;
                    _bAllowedToDelete = true;
                }
                    
            }

            if (_theTable.AllowCopyRecords != null && (bool)_theTable.AllowCopyRecords && bShowAdd)
            {
                _gvPager.ShowCopyRecord = true;
            }
            if (bShowAdd)
                _gvPager.HideAdd = false;
            if (bShowBulkEdit)
                _gvPager.HideEditMany = false;

            if (bShowBulkEdit)
                _gvPager.HideEditMany = false;


        }

        if (iTN == 0)
        {


            //if (IsFiltered())
            //{

            //   divNoFilter.Visible = true;
            //    divEmptyData.Visible = false;

            //}
            //else
            //{
            //    if (_strRecordRightID == Common.UserRoleType.ReadOnly)
            //    {
            //        divEmptyData.Visible = false;
            //    }
            //    else
            //    {
            //        divEmptyData.Visible = true;
            //    }
            //    divNoFilter.Visible = false;
            //}
            //hplNewData.NavigateUrl = GetAddURL();

            //hplNewDataFilter.NavigateUrl = GetAddURL();
            //hplNewDataFilter2.NavigateUrl = hplNewDataFilter.NavigateUrl;

            //if (_theView != null)
            //{
            //    if ((bool)_theView.ShowAddIcon == false)
            //    {
            //        divEmptyData.Visible = false;
            //        divNoFilter.Visible = false;
            //    }
            //}
            //if (bShowAdd == false && bShowEditView==false)
            //{
            //    divEmptyData.Visible = false;
            //    divNoFilter.Visible = false;
            //}
            //else if (bShowAdd == false && bShowEditView == true)
            //{
            //    hplNewData.Visible = false;
            //    hplNewDataFilter.Visible = false;
            //    hplNewDataFilter2.Visible = false;

            //}
        }
        else
        {
            //divEmptyData.Visible = false;
            //divNoFilter.Visible = false;
        }


        if (_bHideAllExport)
        {
            _gvPager.HideAllExport = true;
            divEmail.Visible = false;
        }
        if (_gvPager != null && Request.QueryString["RecordTable"] != null)
        {
            divShowGraph.Visible = false;
            divUpload.Visible = false;
            divEmail.Visible = false;
            divConfig.Visible = false;
            divBatches.Visible = false;

        }


        if (_theView != null)
        {
            if ((bool)_theView.ShowExportIcon == false && _gvPager != null)
            {
                _gvPager.HideAllExport = true;
            }
               

            if ((bool)_theView.ShowViewIcon == false)
                gvTheGrid.Columns[3].Visible = false;//view
            //RP Removed Ticket 4305
            //if ((bool)_theView.ShowEditIcon == false)
            //    gvTheGrid.Columns[2].Visible = false;//edit
            //End Modification
            if ((bool)_theView.ShowDeleteIcon == false)
            {
                // gvTheGrid.Columns[0].Visible = false;//delete check
                if (_gvPager != null)
                {
                     _gvPager.HideDelete = true;
                    _bAllowedToDelete = false;
                }
                   
            }

            if ((bool)_theView.ShowAddIcon == false)
            {
                //divEmptyData.Visible = false;
                //divNoFilter.Visible = false;
                if (_gvPager != null)
                    _gvPager.HideAdd = true;
            }

            if ((bool)_theView.ShowBulkUpdateIcon == false)
            {
                if (_gvPager != null)
                    _gvPager.HideEditMany = true;
            }


        }

        if (_theRoleTable != null)
        {
            if (_theRoleTable.AllowEditView != null && (bool)_theRoleTable.AllowEditView == false)
            {
                if (_gvPager != null)
                    _gvPager.HideEditView = true;

                //hlEditView.Visible = false;
                //hlEditView2.Visible = false;

            }
        }
        else
        {
            if (_theRole != null && _theUserRole.IsAccountHolder != null && (bool)_theUserRole.IsAccountHolder == false)
            {
                if (_theRole.AllowEditView != null && (bool)_theRole.AllowEditView == false)
                {
                    if (_gvPager != null)
                        _gvPager.HideEditView = true;

                    //hlEditView.Visible = false;
                    //hlEditView2.Visible = false;

                }

            }
        }


        if (Request.QueryString["ViewID"] != null || Request.QueryString["View"] != null)
        {
            if ((bool)_theUserRole.IsAccountHolder)
            {
                if (_gvPager != null)
                    _gvPager.HideEditView = false;

                //hlEditView.Visible = true;
                //hlEditView2.Visible = true;
            }
            else
            {

                if (_gvPager != null && _strViewPageType != "dash")
                    _gvPager.HideEditView = true;

                //hlEditView.Visible = false;
                //hlEditView2.Visible = false;
            }

        }

        if (Request.RawUrl.IndexOf("RecordTableSection.aspx") > -1)
        {
            if (_gvPager != null)
                _gvPager.HideEditView = true;

        }

        if (_strViewPageType == "dash" && Session["EditDashboard"] == null)
        {
            //if (_gvPager != null)
            //    _gvPager.HideEditView = true;

        }

        if (_gvPager != null)
        {
            _gvPager.TableID = TableID;
        }
        //if (divEmptyData.Visible == true & divNoFilter.Visible == true)
        //{
        //    divEmptyData.Visible = false;
        //}

        //red Ticket 3358
        /* (chkUseArchive.Checked || bUseArchiveData == true)) Red Ticket 4371 */
        if (bUseArchiveData == true)
        {
            _gvPager.HideAdd = true;
            _gvPager.HideDelete = true;
           _bAllowedToDelete = false;
            _gvPager.HideEditMany = true;
            gvTheGrid.Columns[2].Visible = false;//edit   
            gvTheGrid.Columns[3].Visible = true;//view            
        }
                
        if (DisplayInColumnID != null && Request.Path.IndexOf("Record/RecordDetail.aspx") > -1 && _gvPager!=null
                    && Request.QueryString["mode"] != null && Cryptography.Decrypt(Request.QueryString["mode"].ToString()) == "add" )
        {
            //_gvPager.HideAdd = true;
            _gvPager.Visible = false;
        }

    }
    protected void DoPagerFancyThings()
    {
        if (_gvPager != null)
        {

            int iViewID = -1;
            if (hfViewID.Value != "")
            {
                iViewID = int.Parse(hfViewID.Value);
            }

            _gvPager.EditViewCSSClass = "popuplinkEV" + _strFunctionUQ;
            _gvPager.EditViewToolTip = "Edit View";
            _gvPager.EditViewURL = GetEditViewPageURL();

            if (IsChildTable)
            {
                _gvPager.EditViewURL = _gvPager.EditViewURL + "&IsChildTable=yes";
            }

            if (_strNoAjaxView != "")
            {
                _gvPager.EditViewTarget = "_parent";
            }

        }
    }
    protected string GetRecursiveDataScope(int iScopeTableID, string strScopeRecordID)
    {
        //BY JV
        //Re wrote the function.
        //For this function instead of looking for children, we look for parents since the tables
        //has less parents than children. I also find looking for parent tables who is using current
        //table a much shorter process because less table ids are involved.
        int iCurrentTableID = int.Parse(_qsTableID);
        bool bFoundLevel = false;
        Dictionary<string, string> relationDictionary = new Dictionary<string, string>();

        //Check for parent tables of the current table that connects to scope.
        DataTable dtParentTables = Common.DataTableFromText("SELECT DISTINCT ParentTableID, ChildTableID FROM TableChild WHERE ChildTableID = " + iCurrentTableID);

        if (dtParentTables.Rows.Count > 0)
        {
            //This list makes sure that every branch is searched in the heirarchy.
            List<string> lstTableID = new List<string>();

            //Skip checking first level.
            //Check all parents in the heirarchy 1 at a time.
            foreach (DataRow drPT in dtParentTables.Rows)
            {
                string stParentTableID = drPT["ParentTableID"].ToString();
                bool restart = true;
                //Clear dictionary since previous direct parent is not related to scope.
                relationDictionary.Clear();
                relationDictionary.Add(drPT["ParentTableID"].ToString(), drPT["ChildTableID"].ToString());

                do
                {
                    DataTable dtGrandParentTable = Common.DataTableFromText("SELECT DISTINCT ParentTableID, ChildTableID FROM TableChild WHERE ChildTableID = " + stParentTableID);
                    if (dtGrandParentTable.Rows.Count > 0)
                    {
                        foreach (DataRow drGPT in dtGrandParentTable.Rows)
                        {
                            if (!relationDictionary.ContainsKey(drGPT["ParentTableID"].ToString()))
                            {
                                relationDictionary.Add(drGPT["ParentTableID"].ToString(), drGPT["ChildTableID"].ToString());
                            }

                            if (iScopeTableID == int.Parse(drGPT["ParentTableID"].ToString()))
                            {
                                //Scope table found.
                                bFoundLevel = true;
                                restart = false;
                                break;
                            }
                            else
                            {
                                lstTableID.Add(drGPT["ParentTableID"].ToString());
                            }
                        }
                    }

                    if (lstTableID.Count > 0)
                    {
                        stParentTableID = lstTableID.First();
                        lstTableID.RemoveAt(0);
                    }
                    else
                    {
                        restart = false;
                    }
                }
                while (restart == true);

                if (bFoundLevel == true)
                {
                    string myKey = iScopeTableID.ToString();
                    string strLinkRecordIDs = string.Empty;
                    string strLinkedValues = string.Empty;
                    bool bContinue = true;
                    do
                    {
                        string myValue;
                        if (relationDictionary.TryGetValue(myKey, out myValue))
                        {
                            DataTable dtLinkColumn = Common.DataTableFromText(@"SELECT ColumnID FROM [Column] WHERE ColumnType='dropdown' 
                            AND (Dropdowntype='table' OR Dropdowntype='tabledd')
	                        AND TableID=" + myValue + @" AND TableTableID=" + myKey);

                            if (dtLinkColumn.Rows.Count > 0)
                            {
                                Column theChildColumn = RecordManager.ets_Column_Details(int.Parse(dtLinkColumn.Rows[0][0].ToString()));
                                Column theLinkedColumn = RecordManager.ets_Column_Details((int)theChildColumn.LinkedParentColumnID);

                                if (myKey == iScopeTableID.ToString())
                                {
                                    Record theLinkedRecord = RecordManager.ets_Record_Detail_Full(int.Parse(strScopeRecordID), int.Parse(_qsTableID), bUseArchiveData);  // Red 08012018
                                    string strLinkedColumnValue = RecordManager.GetRecordValue(ref theLinkedRecord, theLinkedColumn.SystemName);
                                    strLinkedColumnValue = strLinkedColumnValue.Replace("'", "''");

                                    DataTable dtChildRecordID = Common.DataTableFromText("SELECT RecordID FROM Record WHERE TableID=" + theChildColumn.TableID.ToString() + " AND  " +
                                        theChildColumn.SystemName + "='" + strLinkedColumnValue + "'");

                                    foreach (DataRow dr in dtChildRecordID.Rows)
                                    {
                                        strLinkRecordIDs = strLinkRecordIDs + dr[0].ToString() + ",";
                                    }

                                    if (strLinkRecordIDs != "")
                                    {
                                        strLinkRecordIDs = strLinkRecordIDs.Substring(0, strLinkRecordIDs.LastIndexOf(','));
                                    }
                                }
                                else
                                {
                                    if (strLinkRecordIDs != string.Empty)
                                    {
                                        foreach (string strRecordID in strLinkRecordIDs.Split(','))
                                        {
                                            if (!string.IsNullOrEmpty(strRecordID))
                                            {
                                                Record theLinkedRecord = RecordManager.ets_Record_Detail_Full(int.Parse(strRecordID), int.Parse(_qsTableID), bUseArchiveData);  // Red 08012018
                                                string strLinkedColumnValue = RecordManager.GetRecordValue(ref theLinkedRecord, theLinkedColumn.SystemName);
                                                strLinkedColumnValue = strLinkedColumnValue.Replace("'", "''");

                                                strLinkedValues = strLinkedValues + strLinkedColumnValue + ",";
                                            }
                                        }
                                        if (strLinkedValues != "")
                                        {
                                            strLinkedValues = strLinkedValues.Substring(0, strLinkedValues.LastIndexOf(','));
                                        }

                                        DataTable dtChildRecordID = Common.DataTableFromText("SELECT RecordID FROM Record WHERE TableID=" + theChildColumn.TableID.ToString() + " AND  " +
                                            theChildColumn.SystemName + " IN (" + strLinkedValues + ")");

                                        strLinkRecordIDs = string.Empty;
                                        foreach (DataRow dr in dtChildRecordID.Rows)
                                        {
                                            strLinkRecordIDs = strLinkRecordIDs + dr[0].ToString() + ",";
                                        }

                                        if (strLinkRecordIDs != "")
                                        {
                                            strLinkRecordIDs = strLinkRecordIDs.Substring(0, strLinkRecordIDs.LastIndexOf(','));

                                            if (iCurrentTableID == int.Parse(myValue))
                                            {
                                                return " AND Record.RecordID IN (" + strLinkRecordIDs + ")";
                                            }
                                        }
                                    }
                                }
                            }
                            myKey = myValue;
                        }
                        else
                        {
                            bContinue = false;
                        }
                    }
                    while (bContinue == true);

                    //break;
                }
            }
        }
        //================================


        //if (dtChildTable.Rows.Count > 0)
        //{

        //    foreach (DataRow drCT in dtChildTable.Rows)
        //    {
        //        if (iCurrentTableID == int.Parse(drCT[0].ToString()))
        //        {
        //            bFoundLevel = true;
        //        }
        //    }

        //    foreach (DataRow drCT in dtChildTable.Rows)
        //    {
        //        int iChildTableID = int.Parse(drCT[0].ToString());
        //        DataTable dtChildColumn = Common.DataTableFromText(@"	SELECT ColumnID FROM [Column] WHERE ColumnType='dropdown' 
        //            AND (Dropdowntype='table' OR Dropdowntype='tabledd')
        //         AND TableID=" + iChildTableID.ToString() + @" AND TableTableID=" + iParentTableID.ToString());

        //        if (dtChildColumn.Rows.Count > 0)
        //        {

        //            //we have a column
        //            Column theChildColumn = RecordManager.ets_Column_Details(int.Parse(dtChildColumn.Rows[0][0].ToString()));
        //            Column theLinkedColumn = RecordManager.ets_Column_Details((int)theChildColumn.LinkedParentColumnID);
        //            string strLinkedValues = "";
        //            foreach (string strARecord in strReocrdIDs.Split(','))
        //            {
        //                if (!string.IsNullOrEmpty(strARecord))
        //                {
        //                    Record theLinkedRecord = RecordManager.ets_Record_Detail_Full(int.Parse(strARecord));
        //                    string strLinkedColumnValue = RecordManager.GetRecordValue(ref theLinkedRecord, theLinkedColumn.SystemName);
        //                    if (theLinkedColumn.SystemName.ToLower() == "recordid")
        //                    {
        //                        //
        //                    }
        //                    else
        //                    {
        //                        strLinkedColumnValue = "'" + strLinkedColumnValue.Replace("'", "''") + "'";
        //                    }
        //                    strLinkedValues = strLinkedValues + strLinkedColumnValue + ",";
        //                }
        //            }
        //            if (strLinkedValues != "")
        //                strLinkedValues = strLinkedValues.Substring(0, strLinkedValues.LastIndexOf(','));

        //            DataTable dtChildRecordID = Common.DataTableFromText("SELECT RecordID FROM Record WHERE TableID=" + theChildColumn.TableID.ToString() + " AND  " + theChildColumn.SystemName + " IN (" + strLinkedValues + ")");

        //            //here we will return filter SQL

        //            string strChildRecordIDs = "";
        //            foreach (DataRow dr in dtChildRecordID.Rows)
        //            {
        //                strChildRecordIDs = strChildRecordIDs + dr[0].ToString() + ",";
        //            }
        //            if (strChildRecordIDs != "")
        //                strChildRecordIDs = strChildRecordIDs.Substring(0, strChildRecordIDs.LastIndexOf(','));


        //            if (iCurrentTableID == (int)theChildColumn.TableID)
        //            {
        //                return " AND Record.RecordID IN (" + strChildRecordIDs + ")";
        //            }
        //            else
        //            {
        //                if (bFoundLevel == false)
        //                {
        //                    string strWhere = GetRecursiveDataScope((int)theChildColumn.TableID, iScopeTableID, strChildRecordIDs);
        //                    if (strWhere != "")
        //                    {
        //                        return strWhere;
        //                    }
        //                }
        //            }


        //        }
        //        else
        //        {
        //            //no link
        //        }
        //    }
        //}
        //else
        //{
        //    //no child tables
        //}

        return "";
    }

    protected string GetDataScopeWhere(int iParentTableID, int iScopeTableID, string strScopeSystemName)
    {
        int iCurrentTableID = int.Parse(_qsTableID);
        //Get RecordID of scope value for link reference.
        DataTable dtRecordID = Common.DataTableFromText("SELECT RecordID FROM Record WHERE TableID=" + iScopeTableID + " AND  " + strScopeSystemName + "='" + _theUserRole.DataScopeValue.Replace("'", "''") + "' AND IsActive = 1");
        string strScopeRecordID = "";
        if (dtRecordID.Rows.Count > 0)
        {
            strScopeRecordID = dtRecordID.Rows[0][0].ToString();
        }

        if (iCurrentTableID == iScopeTableID)
        {
            return " AND Record." + strScopeSystemName + " ='" + _theUserRole.DataScopeValue.Replace("'", "''") + "'";
        }
        else
        {
            //Check for child tables of the scope table.
            DataTable dtChildTable = Common.DataTableFromText("	SELECT DISTINCT ChildTableID FROM TableChild WHERE ParentTableID=" + iParentTableID);

            bool bFoundLevel = false;

            if (dtChildTable.Rows.Count > 0)
            {
                foreach (DataRow drCT in dtChildTable.Rows)
                {
                    if (iCurrentTableID == int.Parse(drCT[0].ToString()))
                    {
                        //Current table found as a direct child of scope.
                        bFoundLevel = true;
                        break;
                    }
                }

                //By JV
                //Comments: Previous code gets data for all child tables, which i found unnecessary.
                //Changed it to get only the data of the current table since this function gets called
                //when A record table is viewed or rendered. Gets called multiple times on the case
                //of multiple record table rendered (dashboard).
                if (bFoundLevel == true)
                {
                    //Check if column/field that links the current table and the parent exists.
                    DataTable dtChildColumn = Common.DataTableFromText(@"	SELECT ColumnID FROM [Column] WHERE ColumnType='dropdown' 
                    AND (Dropdowntype='table' OR Dropdowntype='tabledd')
	                AND TableID=" + iCurrentTableID.ToString() + @" AND TableTableID=" + iParentTableID.ToString());

                    if (dtChildColumn.Rows.Count > 0)
                    {
                        //Get column details of the field used to link to parent.
                        Column theChildColumn = RecordManager.ets_Column_Details(int.Parse(dtChildColumn.Rows[0][0].ToString()));
                        //Get column details of parent field (Data Scope).
                        Column theLinkedColumn = RecordManager.ets_Column_Details((int)theChildColumn.LinkedParentColumnID);

                        //Get record detail of the selected scope.
                        Record theLinkedRecord = RecordManager.ets_Record_Detail_Full(int.Parse(strScopeRecordID), int.Parse(_qsTableID), bUseArchiveData);  // Red 08012018
                        //Get the value of the scope record.
                        string strLinkedColumnValue = RecordManager.GetRecordValue(ref theLinkedRecord, theLinkedColumn.SystemName);
                        strLinkedColumnValue = strLinkedColumnValue.Replace("'", "''");

                        //Get records that are within the scope.
                        DataTable dtChildRecordID = Common.DataTableFromText("SELECT RecordID FROM Record WHERE TableID=" + theChildColumn.TableID.ToString() + " AND  " +
                            theChildColumn.SystemName + "='" + strLinkedColumnValue + "'");

                        string strChildRecordIDs = "";
                        foreach (DataRow dr in dtChildRecordID.Rows)
                        {
                            strChildRecordIDs = strChildRecordIDs + dr[0].ToString() + ",";
                        }
                        //If record is not empty, remove last comma.
                        if (strChildRecordIDs != "")
                        {
                            strChildRecordIDs = strChildRecordIDs.Substring(0, strChildRecordIDs.LastIndexOf(','));
                            return " AND Record.RecordID IN (" + strChildRecordIDs + ")";
                        }
                    }
                }
                else
                {
                    //If current table is not a direct child of scope then use GetRecursiveDataScope.
                    string strWhere = GetRecursiveDataScope(iScopeTableID, strScopeRecordID);
                    if (strWhere != "")
                    {
                        return strWhere;
                    }
                }
                //=============================================

                //Previous code commented by JV.
                //foreach (DataRow drCT in dtChildTable.Rows)
                //{


                //   int iChildTableID = int.Parse(drCT[0].ToString());
                //   DataTable dtChildColumn = Common.DataTableFromText(@"	SELECT ColumnID FROM [Column] WHERE ColumnType='dropdown' 
                //   AND (Dropdowntype='table' OR Dropdowntype='tabledd')
                //AND TableID=" + iChildTableID.ToString() + @" AND TableTableID=" + iParentTableID.ToString());



                //if (dtChildColumn.Rows.Count > 0)
                //{

                //we have a column
                //Column theChildColumn = RecordManager.ets_Column_Details(int.Parse(dtChildColumn.Rows[0][0].ToString()));
                //Column theLinkedColumn = RecordManager.ets_Column_Details((int)theChildColumn.LinkedParentColumnID);


                //Record theLinkedRecord = RecordManager.ets_Record_Detail_Full(int.Parse(strScopeRecordID));
                //string strLinkedColumnValue = RecordManager.GetRecordValue(ref theLinkedRecord, theLinkedColumn.SystemName);
                //strLinkedColumnValue = strLinkedColumnValue.Replace("'", "''");

                //DataTable dtChildRecordID = Common.DataTableFromText("SELECT RecordID FROM Record WHERE TableID=" + theChildColumn.TableID.ToString() + " AND  " + theChildColumn.SystemName + "='" + strLinkedColumnValue + "'");

                //here we will return filter SQL

                //string strChildRecordIDs = "";
                //foreach (DataRow dr in dtChildRecordID.Rows)
                //{
                //    strChildRecordIDs = strChildRecordIDs + dr[0].ToString() + ",";
                //}
                //if (strChildRecordIDs != "")
                //    strChildRecordIDs = strChildRecordIDs.Substring(0, strChildRecordIDs.LastIndexOf(','));


                //if (iCurrentTableID == (int)theChildColumn.TableID)
                //{
                //    if (strChildRecordIDs == "")
                //        strChildRecordIDs = "-1";
                //    return " AND Record.RecordID IN (" + strChildRecordIDs + ")";
                //}
                //else
                //{
                //    if (bFoundLevel == false)
                //    {
                //        string strWhere = GetRecursiveDataScope((int)theChildColumn.TableID, iScopeTableID, strChildRecordIDs);
                //        if (strWhere != "")
                //        {
                //            return strWhere;
                //        }
                //    }
                //}


                //    }
                //            else
                //            {
                //                //no link
                //            }
                //        }
                //    }
                //    else
                //    {
                //        //no child tables
                //    }

            }

            //return "";
        }
        return "";
    }

    //protected void PopulateFilterColumn(Table theSammpleType)
    //{
    //    if (theSammpleType.FilterColumnID != null)
    //    {
    //        Column theColumn = RecordManager.ets_Column_Details((int)theSammpleType.FilterColumnID);


    //            tblFilterByColumn.Visible = true;

    //            if (theSammpleType.HideFilter != null)
    //            {
    //                if ((bool)theSammpleType.HideFilter == true)
    //                {
    //                    trSummaryFilter.Visible = false;
    //                }
    //            }

    //            lblFilterColumnName.Text = theColumn.DisplayTextSummary + ":";
    //            hfFilterColumnSystemName.Value = theColumn.SystemName;

    //            cbcvSumFilter.ddlYAxisV = theSammpleType.FilterColumnID.ToString();

    //            if (theSammpleType.FilterDefaultValue != null && theSammpleType.FilterDefaultValue != "")
    //            {
    //                cbcvSumFilter.SetValue = theSammpleType.FilterDefaultValue;
    //            }          



    //    }

    //}

    protected string GetEditViewPageURL()
    {
        string strExtra = (((_theView.UserID == _ObjUser.UserID) || _strViewPageType == "dash") ? "" : "&Copy=Yes");

        if (Request.QueryString["ViewID"] != null || Request.QueryString["View"] != null)
        {
            strExtra = "";
        }
        if (PageType == "c" && TableChildID != null)
        {
            strExtra = strExtra + "&TableChildID=" + Cryptography.Encrypt(TableChildID.ToString());
        }

        if (DisplayInColumnID != null)
        {
            strExtra = strExtra + "&DisplayInColumnID=" + Cryptography.Encrypt(DisplayInColumnID.ToString());
        }

        return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/ViewEditPage.aspx?" + _strNoAjaxView + "TableID=" + Cryptography.Encrypt(_qsTableID)
                        + "&ViewSession=" + Cryptography.Encrypt(_strViewSession)
                        + "&ViewID=" + _theView.ViewID.ToString() + (PageType == "c" ? "&tabindex=" + DetailTabIndex.ToString() : "")
                        + ((_iParentTableID != null) ? "&ParentTableID=" + Cryptography.Encrypt(_iParentTableID.ToString()) : "")
                        + strExtra;
    }

    protected void BindTheGrid(int iStartIndex, int iMaxRows)
    {
        //bool useArchiveData = false;

        //if (chkUseArchive.Checked)
        //{
        //    useArchiveData = true;
        //}      

        Exception exParam = null;
        Session["SearchCriteriaStatus"] = null;
        ViewState["bUseArchiveData"] = false;
        hfchkDelateAllEvery.Value = "";
        hfchkDeleteParmanent.Value = "";
        hfchkDeleteParmanent.Value = "";
        hfchkUndo.Value = "";
        hftxtDeleteReason.Value = "";
        hlEditView.Visible = false; //Red Ticket 2145 - Removed this... returned 02/02/2017
        hlEditView.NavigateUrl = GetEditViewPageURL();
        hlEditView.CssClass = "popuplinkEV" + _strFunctionUQ;
        ProcessXMLDef();
        if (_bHaveService)
        {

            ProcessServiceForEvent("Before_Search", null, null);
        }

        //Session["DateTimeSearchFrom"] = null; //red Ticket 3358; use one session 3358_2
        Session["DateSearchFrom"] = null; //red Ticket 3358
        PopulateSearchParams();

        //if (hfViewID.Value != "")
        //{
        //    hlEditView.NavigateUrl = GetEditViewPageURL();
        //    hlEditView2.NavigateUrl = hlEditView.NavigateUrl;

        //    if (_strNoAjaxView != "")
        //    {
        //        hlEditView.Target = "_parent";
        //        hlEditView2.Target = "_parent";

        //    }
        //}

        //red Ticket 3358
        try
        {

            // Red Ticket 3358_2
            if (_theTable.ArchiveMonths != null)
            {
                if (!bUseArchiveData && Session["DateSearchFrom"] == null)
                {
                    /* Except Graph icon - Red Ticket 4371 */
                    // tdTopButtons.Visible = false;
                    divUpload.Visible = true;
                    divEmail.Visible = true;
                    divConfig.Visible = true;
                    /* end Red Ticket 4371 */

                }
                else if (!bUseArchiveData && Session["DateSearchFrom"] != null)
                {
                    DateTime dRecord_LastArchive;
                    int? iArchiveMonths = _theTable.ArchiveMonths;
                    iArchiveMonths = iArchiveMonths * -1;
                    //string strRecord_MinDate = Common.GetValueFromSQL("SELECT MIN(CONVERT(DATE, " + strArchiveDateColumnID + ",103)) FROM [Record] WHERE TableID=" + _qsTableID);
                    //string strRecord_LastArchive = Common.GetValueFromSQL("SELECT  CONVERT(DATE, DATEADD(M, " + iArchiveMonths + " , GETDATE()),103)");
                    string strRecord_LastArchive = Common.GetValueFromSQL("SELECT  CONVERT(date, DATEADD(M, " + iArchiveMonths + " , CONVERT(datetime,'" + _theTable.ArchiveLastRun + "',103)),103)");

                    if (DateTime.TryParseExact(DateTime.Parse(strRecord_LastArchive).ToShortDateString(), Common.Dateformats,
                                             new CultureInfo("en-GB"),
                                             DateTimeStyles.None,
                                             out dRecord_LastArchive))
                    {
                        if (dRecord_LastArchive > DateTime.Parse(Session["DateSearchFrom"].ToString()))
                        {
                            bUseArchiveData = true;
                            /* Except Graph icon - Red Ticket 4371 */
                            // tdTopButtons.Visible = false;
                            divUpload.Visible = false;
                            divEmail.Visible = false;
                            divConfig.Visible = false;
                            /* end Red Ticket 4371 */

                            Session["DateSearchFrom"] = null;

                            if (IsPostBack)
                                Session["tdbmsgpb"] = "The data requested is in the archive.";
                        }

                        else // Red Ticket 3358_2
                        {
                            /* Except Graph icon - Red Ticket 4371 */
                            // tdTopButtons.Visible = false;
                            divUpload.Visible = true;
                            divEmail.Visible = true;
                            divConfig.Visible = true;
                            /* end Red Ticket 4371 */
                            Session["DateSearchFrom"] = null;
                        }

                    }
                }                               
                else if (bUseArchiveData) // page retruned from RecordDetail page - Red Ticket 4371
                {
                    /* Except Graph icon - Red Ticket 4371 */
                    // tdTopButtons.Visible = false;
                    divUpload.Visible = false;
                    divEmail.Visible = false;
                    divConfig.Visible = false;
                    /* end Red Ticket 4371 */

                }

                /* lets get the Archive status - to use for other function e.g. Export to Excel */
                ViewState["bUseArchiveData"] = bUseArchiveData;

                /* Disabled by Red - Ticket 4371 */
                /*
                if (chkUseArchive.Checked)
                {
                    bUseArchiveData = true;
                }
                */
            }

        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "BindTheGrid Filter Date/Time", ex.Message, ex.StackTrace, DateTime.Now, "");
            SystemData.ErrorLog_Insert(theErrorLog);
        }
        //end red

        _bIsForExport = false;
        lblMsg.Text = "";
        if (TableID.ToString() == "")
            return;





        try
        {

            int iTN = 0;

            string strOrderDirection = "DESC";
            string sOrder = GetDataKeyNames()[0];
            string sParentColumnSortSQL = "";
            if (gvTheGrid.GridViewSortDirection == SortDirection.Ascending)
            {
                strOrderDirection = "ASC";
            }
            if (gvTheGrid.GridViewSortColumn != "")
            {
                String sort =  gvTheGrid.GridViewSortColumn.Trim();
                String firstsort = "";
                String secondsort = "";
                if (sort.Contains("sortBreaker"))
                {
                    firstsort = sort.Substring(0, sort.IndexOf("[sortBreaker]"));
                    secondsort = sort.Substring(sort.IndexOf("[sortBreaker]"), sort.Replace(firstsort, "").Length);

                    if (!firstsort.Contains("CONVERT"))
                    {
                        firstsort = "[" + firstsort;
                        if (firstsort.Contains("DESC") || firstsort.Contains("ASC"))
                        {
                            firstsort = firstsort.Replace("ASC", "] ASC");
                            firstsort = firstsort.Replace("DESC", "] DESC");
                        }
                        else
                        {
                            firstsort = firstsort + "]";
                        }
                    }

                    if (!secondsort.Contains("CONVERT"))
                    {
                        secondsort = secondsort.Replace("[sortBreaker]  ", "[sortBreaker] [");
                        if (secondsort.Contains("DESC") || secondsort.Contains("ASC"))
                        {
                            secondsort = secondsort.Replace("ASC", "] ASC");
                            secondsort = secondsort.Replace("DESC", "] DESC");
                        }
                        else
                        {
                            secondsort = secondsort + "]";
                        }

                    }
                    sOrder = firstsort + secondsort;
                }
                else
                {
                    //if (sort.Contains("DESC"))
                    //{
                    //    if(!sort.Contains("CONVERT"))
                    //        sort = "[" + sort.Trim() + "] ";
                    //}
                    //else
                    //{
                    //    if (!sort.Contains("CONVERT"))
                    //        sort = "[" + sort.Trim() + "] ";
                    //}
                    sOrder = sort;
                }

                //sOrder = gvTheGrid.GridViewSortColumn.Trim();
                //sOrder = firstsort + secondsort;
            }
            else
            {
                strOrderDirection = "DESC";
                gvTheGrid.GridViewSortDirection = SortDirection.Descending;
            }


            //Red transferred below

            //string strDynamicSearchXMLPart = "";
            //if (chkShowAdvancedOptions.Checked == false)
            //{
            //    strDynamicSearchXMLPart = GetDynamicSeachXMLPart();
            //}




            //put speed test here       

            if (!string.IsNullOrEmpty(AnyTextSearch))
            {
                TextSearch = TextSearch + AnyTextSearch;
            }

            string strReturnSQL = "";
            string sReturnHeaderSQL = "";

            _dtDataSource = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
                ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
                !chkShowDeletedRecords.Checked,
                chkShowOnlyWarning.Checked == false ? null : (bool?)true, null, null,
                sOrder, strOrderDirection, iStartIndex, iMaxRows, ref iTN, ref _iTotalDynamicColumns, _strListType, _strNumericSearch, TextSearch + TextSearchParent,
               _dtDateFrom, _dtDateTo, sParentColumnSortSQL, "", _strViewName, int.Parse(hfViewID.Value),
               ref strReturnSQL, ref sReturnHeaderSQL, ref exParam, bUseArchiveData);
            hfRecordCount.Value = iTN.ToString();
            //Updates By JV = Transferred code here to include Session RecordListSQL if filtered.
            //Red Ticket 2060
            //<Begin>
            if (IsFiltered())
            {
                Session["SearchCriteriaStatus"] = "Yes";
                Session["RecordListSQL"] = strReturnSQL;

                //for dropdown list loading as filtered... put it here rather than call the PopulateSearchParams() again..
                ViewState["strKeepTextSearch"] = TextSearch;       //3611 
            }
            //<End>

            if (IsRecordDetail)
            {
                _dictSessionRecordListSQL = (Dictionary<int, string>)Session["DictionaryRecordListSQL"];

                if (_dictSessionRecordListSQL == null)
                {
                    _dictSessionRecordListSQL = new Dictionary<int, string>();
                }

                if (!_dictSessionRecordListSQL.ContainsKey(TableID))
                {
                    _dictSessionRecordListSQL.Add(TableID, strReturnSQL);
                }
                else
                {
                    _dictSessionRecordListSQL[TableID] = strReturnSQL;
                }

                Session["DictionaryRecordListSQL"] = _dictSessionRecordListSQL;
            }

            ViewState["strReturnSQL"] = strReturnSQL;


            //Red 3611 transferred here to get the searched hfdropdowns value and include in searchcriteria saving...
            string strDynamicSearchXMLPart = "";
            if (chkShowAdvancedOptions.Checked == false)
            {
                Session["iTN"] = null;
                ViewState["iTN"] = iTN;
                lnkDDLPopulate_Click(null, null);
                strDynamicSearchXMLPart = GetDynamicSeachXMLPart();
            }
            else
            {
                Session["iTN"] = iTN;
                ViewState["iTN"] = null;
            }

            string strOtherXMLTags = @" <iStartIndex>" + HttpUtility.HtmlEncode(iStartIndex.ToString()) + "</iStartIndex>" +
                  " <iMaxRows>" + HttpUtility.HtmlEncode(iMaxRows.ToString()) + "</iMaxRows>" +
                " <sOrder>" + HttpUtility.HtmlEncode(sOrder) + "</sOrder>" +
                " <strOrderDirection>" + HttpUtility.HtmlEncode(strOrderDirection) + "</strOrderDirection>" +
                " <sParentColumnSortSQL>" + HttpUtility.HtmlEncode(sParentColumnSortSQL) + "</sParentColumnSortSQL>" +
                  " <strReturnSQL>" + HttpUtility.HtmlEncode(strReturnSQL.Replace("RowNum >= " + iStartIndex.ToString(), "RowNum >= 1")) + "</strReturnSQL>" +
                " <sReturnHeaderSQL>" + HttpUtility.HtmlEncode(sReturnHeaderSQL) + "</sReturnHeaderSQL>" +
                " <seriesColumnID>" + HttpUtility.HtmlEncode(ViewState["seriesColumnID"] == null ? "-1" : ViewState["seriesColumnID"].ToString()) + "</seriesColumnID>" +
                " <summaryTextSearch>" + HttpUtility.HtmlEncode(TextSearch) + "</summaryTextSearch>"
                ;

            strOtherXMLTags = strOtherXMLTags + strDynamicSearchXMLPart;

            UpdateSearchCriteriaForTheGrid(strOtherXMLTags);

            /* === Red 05/04/2019: Reset the series dummies */
            ViewState["seriesColumnID"] = null;
            ViewState["seriesColumnIndex"] = null;
            /* === end Red === */

            //put speed test here       


            //remove the parent sort column here

            if (sParentColumnSortSQL != "" && _dtDataSource != null)
            {
                _dtDataSource.Columns.RemoveAt(_dtDataSource.Columns.Count - 2);
                _dtDataSource.AcceptChanges();
                _iTotalDynamicColumns = _iTotalDynamicColumns - 1;
            }

            _dtRecordColums = RecordManager.ets_Table_Columns_Summary(TableID, int.Parse(hfViewID.Value));
            if (iTN == 0)
            {
                _bEmptyRecord = true;
                _dtDataSource.Rows.Add(_dtDataSource.NewRow());
                //if(IsPostBack)
                //{
                //    Session["tdbmsgpb"] = "No records matched your search";
                //}
            }


            gvTheGrid.VirtualItemCount = iTN;

            //Red Ticket 2106 - Replaced and add
            //<Begin>
            // ViewState[gvTheGrid.ID + "PageIndex"] = (iStartIndex / gvTheGrid.PageSize) + 1;
            ViewState[gvTheGrid.ID + "PageIndex"] = (iStartIndex / iMaxRows) + 1;
            gvTheGrid.PageSize = iMaxRows;
            //<End>

            _dtDataSource.AcceptChanges();

            ClearCellColourCache();
            gvTheGrid.DataSource = _dtDataSource;

            //Here we can pass the Datatable _dtDataSource
            // if (!string.IsNullOrEmpty(_theTable.ListDataTableCustomMethod))
            //{
            //try
            //{
            //    List<object> objList = new List<object>();
            //    objList.Add(_dtDataSource);

            //    List<object> roList = CustomMethod.DotNetMethod(_theTable.ListDataTableCustomMethod, objList);
            //    if (roList != null && roList.Count > 0)
            //    {
            //        foreach (object obj in roList)
            //        {
            //            if (obj.GetType().Name == "DataTable")
            //            {
            //                _dtDataSource = (DataTable)obj;
            //                break;
            //            }
            //        }
            //    }
            //}
            //catch
            //{
            //custom error
            //}
            //}


            gvTheGrid.DataBind();
            ///
            DoFixedHeaderThing(iTN);
            ////

            AdjustMenu();

            if (gvTheGrid.TopPagerRow != null)
                gvTheGrid.TopPagerRow.Visible = true;

            GridViewRow gvr = gvTheGrid.TopPagerRow;
            if (gvr != null)
            {
                _gvPager = (Common_Pager)gvr.FindControl("Pager");
                if (_gvPager != null)
                {
                    _gvPager.AddURL = GetAddURL();
                    if (ViewState[gvTheGrid.ID + "PageIndex"] != null)
                        _gvPager.PageIndex = int.Parse(ViewState[gvTheGrid.ID + "PageIndex"].ToString());

                    _gvPager.PageSize = gvTheGrid.PageSize;
                    _gvPager.TotalRows = iTN;
                    if (hfHidePagerGoButton.Value == "yes")
                    {
                        _gvPager.HidePagerGoButton = true;
                    }

                }

            }

            EnsureSecurity(iTN);

            if (_dtDataSource == null)
            {
                return;
            }

            DoPagerFancyThings();
            if (!IsPostBack && _gvPager != null)
            {
                string strBulkUpdateSQL = SystemData.SystemOption_ValueByKey_Account("BulkUpdateSQL", null, int.Parse(_qsTableID));

                if (strBulkUpdateSQL != "")
                {
                    string strEditManyTooltip = SystemData.SystemOption_NotesByKey_Account("BulkUpdateSQL", null, int.Parse(_qsTableID)); ;
                    if (strEditManyTooltip != "")
                    {
                        _gvPager.EditManyToolTip = strEditManyTooltip;
                    }
                }
            }

            if (_gvPager != null)
            {
                DataTable dtSendEmailCol = Common.DataTableFromText(@"SELECT ColumnID FROM [Column] WHERE TableID=" + TableID.ToString() + @" AND ColumnType='text' 
                                        AND (TextType='email' OR TextType='mobile')");

                if (dtSendEmailCol.Rows.Count > 0)
                {
                    _gvPager.HideSendEmail = false;
                    if ((bool)_theView.ShowEmailIcon == false)
                    {
                        _gvPager.HideSendEmail = true;
                    }
                }

            }

            if (iTN == 0 && _bEmptyRecord && _gvPager != null)
            {
                _gvPager.HideDelete = true;
                _gvPager.HideEditMany = true;
                _gvPager.HideAllExport = true;
                //_gvPager.HideFilter = true;
                //_gvPager.HideNavigation = true;
                _gvPager.HideGo = true;
                _gvPager.HidePageSizeButton = true;
                _gvPager.HideRefresh = true;
                _gvPager.HideSendEmail = true;
                _gvPager.HideUnDelete = true;
                _gvPager.HideParmanentDelete = true;
                _gvPager.ShowCopyRecord = false;
                if (chkShowDeletedRecords.Checked)
                {
                    _gvPager.HideAdd = true;
                    _gvPager.ShowAdd2 = false;
                    _gvPager.HideEditView = true;
                }
                //gvTheGrid.Columns[0].Visible = false;
                //gvTheGrid.Columns[1].Visible = false;
                //gvTheGrid.Columns[2].Visible = false;
                //gvTheGrid.Columns[3].Visible = false;
            }

            //_gvPager.HidePageSize = true;
            if (_gvPager != null && _bResponsive)
            {
                if (Responsive.DeviceMaxXAxis() < 3)
                {
                    _gvPager.HideControlsSmallDevice = true;
                }
            }

        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Records", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);


            bool bFixViewSortOrder = false;
            if(_theView!=null && ViewState["fixviewsortorder" + _theView.ViewID.ToString()] == null 
                && _theView.SortOrder!="" && _theView.FilterControlsInfo!="")
            {
                string sErrorLogID = Common.GetValueFromSQL(@"SELECT TOP 1 ErrorLogID FROM ErrorLog WHERE ErrorMessage 
                                LIKE '%Invalid column name%' AND ErrorTrack LIKE '%@nViewID: "+_theView.ViewID.ToString()+@"%' 
                                AND CONVERT(date, ErrorTime) = CONVERT(date, getdate())");
                if(sErrorLogID!="")
                {
                    bFixViewSortOrder = true;
                }

            }


            //if (_theView != null && Session["fixviewsortorder" + _theView.ViewID.ToString()] == null &&
            //    ((ex.Message.IndexOf("Invalid column name") > -1 && ex.Message.IndexOf("ORDER BY") > -1)
            //    ||
            //    (exParam != null && exParam.Message.IndexOf("Invalid column name") > -1))
            //     && _theView.FilterControlsInfo != "")
            if(bFixViewSortOrder)
            {
                try
                {
                    ViewState["fixviewsortorder" + _theView.ViewID.ToString()] = "done";
                    System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                    xmlDoc.Load(new StringReader(_theView.FilterControlsInfo));
                    Pages_UserControl_ViewDetail vdFilter = new Pages_UserControl_ViewDetail();
                    vdFilter = (Pages_UserControl_ViewDetail)LoadControl("~/Pages/UserControl/ViewDetail.ascx");
                    vdFilter.PopulateFilterControl(xmlDoc.OuterXml, ((int)_theView.TableID));
                    _theView.SortOrder = "";
                    _theView.SortOrder2 = "";

                    if (xmlDoc.FirstChild["ddlViewSortOrder"] != null && xmlDoc.FirstChild["rbSortOrderDirection"] != null)
                        _theView.SortOrder = vdFilter.GetViewOrderStringByValue(xmlDoc.FirstChild["ddlViewSortOrder"].InnerText,
                            xmlDoc.FirstChild["rbSortOrderDirection"].InnerText);


                    if (xmlDoc.FirstChild["ddlViewSortOrder2"] != null && xmlDoc.FirstChild["rbSortOrderDirection2"] != null)
                        _theView.SortOrder2 = vdFilter.GetViewOrderStringByValue(xmlDoc.FirstChild["ddlViewSortOrder2"].InnerText,
                            xmlDoc.FirstChild["rbSortOrderDirection2"].InnerText);
                    
                }
                catch
                {
                    _theView.SortOrder = "";
                    _theView.SortOrder2 = "";
                }

                ViewManager.dbg_View_Update(_theView);

                //if (_theView.SortOrder != "" || _theView.SortOrder2 != "")
                //{
                //    _theView.SortOrder = "";
                //    _theView.SortOrder2 = "";
                //    ViewManager.dbg_View_Update(_theView);
                //}



                if (_iSearchCriteriaID > 0)
                {
                    Common.ExecuteText("DELETE SearchCriteria WHERE SearchCriteriaID=" + _iSearchCriteriaID.ToString());
                    Common.ExecuteText("DELETE UserSearch WHERE UserID=" + _ObjUser.UserID.ToString() + " AND ViewID=" + _theView.ViewID.ToString());

                }
                Session["SCid" + hfViewID.Value] = null;
                ViewState["_iSearchCriteriaID"] = null;

                lblMsg.Text = "Please try again. If the error still persist please update this view sort order and try again.";
                hlEditView.Visible = true;

                try
                {

                    //if (_theUserRole.IsAccountHolder != null && (bool)_theUserRole.IsAccountHolder)
                    //{
                    //    Session["tdbmsg"] = "Column name has been changed, please click the Save button of the view page again to restore the sort order of the view.";
                    //    //Send email to DBGurus
                    //}


                    Response.Redirect(Request.RawUrl, true);
                }
                catch
                {
                    return;
                }
            }
            else
            {
                lblMsg.Text = "Error! Please check filter number/date format/view sort order.";
                hlEditView.Visible = true;
                if(DisplayInColumnID!=null && Request.Path.IndexOf("Record/RecordDetail.aspx")>-1
                    && Request.QueryString["mode"]!=null && Cryptography.Decrypt(Request.QueryString["mode"].ToString())=="add")
                {
                    lblMsg.Text = "";
                    hlEditView.Visible = false;
                }
                //if (exParam == null)
                //    lnkReset_Click(null, null);
            }

            //divEmptyData.Visible = true;
        }


        if (_theTable != null)
        {
            string strHideEmailButton = SystemData.SystemOption_ValueByKey_Account("Hide Record Email Button", _theTable.AccountID, _theTable.TableID);

            if (strHideEmailButton != "" && strHideEmailButton.ToLower() == "yes")
            {
                divEmail.Visible = false;
            }
        }

    }


    protected bool IsFiltered()
    {
        if (txtDateFrom.Text != "" || txtDateTo.Text != "" || ddlEnteredBy.SelectedIndex != 0
            || chkShowDeletedRecords.Checked != false || chkShowOnlyWarning.Checked != false
            || cbcSearchMain.ddlYAxisV != "" || /*RP Modified Ticket 4363*/ /*ddlUploadedBatch.SelectedValue != ""*/hiddenUploadedBatchID.Value != "" || !string.IsNullOrEmpty(_theView.Filter)
            //|| ddlYAxis.SelectedIndex != 0 || txtSearchText.Text != "" || txtLowerLimit.Text != ""
            //|| txtUpperLimit.Text != "" || ddlDropdownColumnSearch.SelectedValue!="" 
            )
        {


            return true;
        }

        if (chkShowAdvancedOptions.Checked == false)//_bDynamicSearch
        {
            foreach (DataRow dr in _dtDynamicSearchColumns.Rows)
            {

                if (dr["ColumnType"].ToString() == "number" || dr["ColumnType"].ToString() == "calculation")
                {
                    TextBox txtLowerLimit = (TextBox)tblSearchControls.FindControl("txtLowerLimit_" + dr["SystemName"].ToString());
                    TextBox txtUpperLimit = (TextBox)tblSearchControls.FindControl("txtUpperLimit_" + dr["SystemName"].ToString());
                    if (txtLowerLimit != null && txtUpperLimit != null)
                    {
                        if (txtLowerLimit.Text != "" || txtUpperLimit.Text != "")
                        {
                            return true;
                        }
                    }

                }
                else if (dr["ColumnType"].ToString() == "text")
                {
                    TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                    if (txtSearch != null)
                    {
                        if (txtSearch.Text != "")
                        {
                            return true;
                        }
                    }

                }
                else if (dr["ColumnType"].ToString() == "date" || dr["ColumnType"].ToString() == "datetime")
                {
                    TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                    TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());
                    if (txtLowerDate != null && txtUpperDate != null)
                    {
                        if (txtLowerDate.Text != "" || txtUpperDate.Text != "")
                        {
                            return true;
                        }
                    }

                }
                else if (dr["ColumnType"].ToString() == "time")
                {
                    TextBox txtLowerTime = (TextBox)tblSearchControls.FindControl("txtLowerTime_" + dr["SystemName"].ToString());
                    TextBox txtUpperTime = (TextBox)tblSearchControls.FindControl("txtUpperTime_" + dr["SystemName"].ToString());
                    if (txtLowerTime != null && txtUpperTime != null)
                    {
                        if (txtLowerTime.Text != "" || txtUpperTime.Text != "")
                        {
                            return true;
                        }
                    }

                }
                else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "values" || dr["DropDownType"].ToString() == "value_text"))
                {
                    DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                    if (ddlSearch != null)
                    {
                        if (ddlSearch.SelectedValue != "")
                        {
                            return true;
                        }
                    }
                }


                else if (dr["ColumnType"].ToString() == "radiobutton" && (dr["DropDownType"].ToString() == "values" || dr["DropDownType"].ToString() == "value_text"))
                {
                    DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                    if (ddlSearch != null)
                    {
                        if (ddlSearch.SelectedValue != "")
                        {
                            return true;
                        }
                    }
                }
                //red 04092017
                else if (dr["ColumnType"].ToString() == "listbox" || dr["ColumnType"].ToString() == "checkbox")
                {
                    DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                    if (ddlSearch != null)
                    {
                        if (ddlSearch.SelectedValue != "")
                        {
                            return true;
                        }
                    }
                }
                //end red

                else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "table" || dr["DropDownType"].ToString() == "tabledd") &&
           dr["TableTableID"] != DBNull.Value && dr["DisplayColumn"].ToString() != "")
                {
                    //DropDownList ddlParentSearch = (DropDownList)tblSearchControls.FindControl(_strDynamictabPart+"ddlParentSearch_" + dr["SystemName"].ToString());

                    //if (ddlParentSearch != null)
                    //{
                    //    if (ddlParentSearch.Text != "")
                    //    {
                    //        return true;
                    //    }
                    //}

                    HiddenField hfParentSearch = (HiddenField)tblSearchControls.FindControl(_strDynamictabPart + "hfParentSearch_" + dr["SystemName"].ToString());

                    if (hfParentSearch != null)
                    {
                        if (hfParentSearch.Value != "")
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                    if (txtSearch != null)
                    {
                        if (txtSearch.Text != "")
                        {
                            return true;
                        }
                    }
                }
            }

            //
            foreach (DataRow dr in _dtSearchGroup.Rows)
            {

                TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SearchGroupID"].ToString());
                if (txtSearch != null)
                {
                    if (txtSearch.Text != "")
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }


    protected bool IsFilteredStandard()
    {
        foreach (DataRow dr in _dtDynamicSearchColumns.Rows)
        {

            if (dr["ColumnType"].ToString() == "number" || dr["ColumnType"].ToString() == "calculation")
            {
                TextBox txtLowerLimit = (TextBox)tblSearchControls.FindControl("txtLowerLimit_" + dr["SystemName"].ToString());
                TextBox txtUpperLimit = (TextBox)tblSearchControls.FindControl("txtUpperLimit_" + dr["SystemName"].ToString());
                if (txtLowerLimit != null && txtUpperLimit != null)
                {
                    if (txtLowerLimit.Text != "" || txtUpperLimit.Text != "")
                    {
                        return true;
                    }
                }

            }
            else if (dr["ColumnType"].ToString() == "text")
            {
                TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                if (txtSearch != null)
                {
                    if (txtSearch.Text != "")
                    {
                        return true;
                    }
                }

            }
            else if (dr["ColumnType"].ToString() == "date" || dr["ColumnType"].ToString() == "datetime")
            {
                TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());
                if (txtLowerDate != null && txtUpperDate != null)
                {
                    if (txtLowerDate.Text != "" || txtUpperDate.Text != "")
                    {
                        return true;
                    }
                }

            }
            else if (dr["ColumnType"].ToString() == "time")
            {
                TextBox txtLowerTime = (TextBox)tblSearchControls.FindControl("txtLowerTime_" + dr["SystemName"].ToString());
                TextBox txtUpperTime = (TextBox)tblSearchControls.FindControl("txtUpperTime_" + dr["SystemName"].ToString());
                if (txtLowerTime != null && txtUpperTime != null)
                {
                    if (txtLowerTime.Text != "" || txtUpperTime.Text != "")
                    {
                        return true;
                    }
                }

            }
            else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "values" || dr["DropDownType"].ToString() == "value_text"))
            {
                DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                if (ddlSearch != null)
                {
                    if (ddlSearch.SelectedValue != "")
                    {
                        return true;
                    }
                }
            }


            else if (dr["ColumnType"].ToString() == "radiobutton" && (dr["DropDownType"].ToString() == "values" || dr["DropDownType"].ToString() == "value_text"))
            {
                DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                if (ddlSearch != null)
                {
                    if (ddlSearch.SelectedValue != "")
                    {
                        return true;
                    }
                }
            }
            else if (dr["ColumnType"].ToString() == "listbox" || dr["ColumnType"].ToString() == "checkbox")
            {
                DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                if (ddlSearch != null)
                {
                    if (ddlSearch.SelectedValue != "")
                    {
                        return true;
                    }
                }
            }

            else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "table" || dr["DropDownType"].ToString() == "tabledd") &&
       dr["TableTableID"] != DBNull.Value && dr["DisplayColumn"].ToString() != "")
            {
                //DropDownList ddlParentSearch = (DropDownList)tblSearchControls.FindControl("ddlParentSearch_" + dr["SystemName"].ToString());

                //if (ddlParentSearch != null)
                //{
                //    if (ddlParentSearch.Text != "")
                //    {
                //        return true;
                //    }
                //}
                HiddenField hfParentSearch = (HiddenField)tblSearchControls.FindControl(_strDynamictabPart + "hfParentSearch_" + dr["SystemName"].ToString());

                if (hfParentSearch != null)
                {
                    if (hfParentSearch.Value != "")
                    {
                        return true;
                    }
                }
            }
            else
            {
                TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                if (txtSearch != null)
                {
                    if (txtSearch.Text != "")
                    {
                        return true;
                    }
                }
            }
        }

        foreach (DataRow dr in _dtSearchGroup.Rows)
        {

            TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SearchGroupID"].ToString());
            if (txtSearch != null)
            {
                if (txtSearch.Text != "")
                {
                    return true;
                }
            }
        }

        return false;
    }



    protected string GetDynamicSeachXMLPart()
    {
        string strDynamicSearch = "";
        try
        {
            foreach (DataRow dr in _dtDynamicSearchColumns.Rows)
            {

                if (dr["ColumnType"].ToString() == "number" || dr["ColumnType"].ToString() == "calculation")
                {
                    TextBox txtLowerLimit = (TextBox)tblSearchControls.FindControl("txtLowerLimit_" + dr["SystemName"].ToString());
                    TextBox txtUpperLimit = (TextBox)tblSearchControls.FindControl("txtUpperLimit_" + dr["SystemName"].ToString());
                    if (txtLowerLimit != null && txtUpperLimit != null)
                    {
                        strDynamicSearch = strDynamicSearch + " <" + txtLowerLimit.ID + ">" + HttpUtility.HtmlEncode(txtLowerLimit.Text) + "</" + txtLowerLimit.ID + ">";
                        strDynamicSearch = strDynamicSearch + " <" + txtUpperLimit.ID + ">" + HttpUtility.HtmlEncode(txtUpperLimit.Text) + "</" + txtUpperLimit.ID + ">";
                    }

                }


                else if (dr["ColumnType"].ToString() == "text")
                {
                    TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                    if (txtSearch != null)
                    {
                        strDynamicSearch = strDynamicSearch + " <" + txtSearch.ID + ">" + HttpUtility.HtmlEncode(txtSearch.Text) + "</" + txtSearch.ID + ">";
                    }

                }

                else if (dr["ColumnType"].ToString() == "date")
                {

                    TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                    TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());

                    if (txtLowerDate != null && txtUpperDate != null)
                    {
                        strDynamicSearch = strDynamicSearch + " <" + txtLowerDate.ID + ">" + HttpUtility.HtmlEncode(txtLowerDate.Text) + "</" + txtLowerDate.ID + ">";
                        strDynamicSearch = strDynamicSearch + " <" + txtUpperDate.ID + ">" + HttpUtility.HtmlEncode(txtUpperDate.Text) + "</" + txtUpperDate.ID + ">";
                    }

                }
                else if (dr["ColumnType"].ToString() == "datetime")
                {

                    TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                    TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());

                    TextBox txtLowerTime = (TextBox)tblSearchControls.FindControl("txtLowerTime_" + dr["SystemName"].ToString());
                    TextBox txtUpperTime = (TextBox)tblSearchControls.FindControl("txtUpperTime_" + dr["SystemName"].ToString());

                    if (txtLowerDate != null && txtUpperDate != null)
                    {
                        strDynamicSearch = strDynamicSearch + " <" + txtLowerDate.ID + ">" + HttpUtility.HtmlEncode(txtLowerDate.Text) + "</" + txtLowerDate.ID + ">";
                        strDynamicSearch = strDynamicSearch + " <" + txtUpperDate.ID + ">" + HttpUtility.HtmlEncode(txtUpperDate.Text) + "</" + txtUpperDate.ID + ">";
                    }

                    if (txtLowerTime != null && txtUpperTime != null)
                    {
                        strDynamicSearch = strDynamicSearch + " <" + txtLowerTime.ID + ">" + HttpUtility.HtmlEncode(txtLowerTime.Text) + "</" + txtLowerTime.ID + ">";
                        strDynamicSearch = strDynamicSearch + " <" + txtUpperTime.ID + ">" + HttpUtility.HtmlEncode(txtUpperTime.Text) + "</" + txtUpperTime.ID + ">";
                    }

                }
                else if (dr["ColumnType"].ToString() == "time")
                {

                    TextBox txtLowerTime = (TextBox)tblSearchControls.FindControl("txtLowerTime_" + dr["SystemName"].ToString());
                    TextBox txtUpperTime = (TextBox)tblSearchControls.FindControl("txtUpperTime_" + dr["SystemName"].ToString());

                    if (txtLowerTime != null && txtUpperTime != null)
                    {
                        strDynamicSearch = strDynamicSearch + " <" + txtLowerTime.ID + ">" + HttpUtility.HtmlEncode(txtLowerTime.Text) + "</" + txtLowerTime.ID + ">";
                        strDynamicSearch = strDynamicSearch + " <" + txtUpperTime.ID + ">" + HttpUtility.HtmlEncode(txtUpperTime.Text) + "</" + txtUpperTime.ID + ">";
                    }

                }

                //if (dr["ColumnType"].ToString() == "datetime")
                //{
                //    TextBox txtDate = (TextBox)tblSearchControls.FindControl("txtDate_" + dr["SystemName"].ToString());
                //    if (txtDate != null)
                //    {
                //        strDynamicSearch = strDynamicSearch + " <" + txtDate.ID + ">" + HttpUtility.HtmlEncode(txtDate.Text) + "</" + txtDate.ID + ">";
                //    }

                //    TextBox txtTime = (TextBox)tblSearchControls.FindControl("txtTime_" + dr["SystemName"].ToString());
                //    if (txtTime != null)
                //    {
                //        strDynamicSearch = strDynamicSearch + " <" + txtTime.ID + ">" + HttpUtility.HtmlEncode(txtTime.Text) + "</" + txtTime.ID + ">";
                //    }

                //}


                //else if (dr["ColumnType"].ToString() == "time")
                //{

                //    TextBox txtTime = (TextBox)tblSearchControls.FindControl("txtTime_" + dr["SystemName"].ToString());
                //    if (txtTime != null)
                //    {
                //        strDynamicSearch = strDynamicSearch + " <" + txtTime.ID + ">" + HttpUtility.HtmlEncode(txtTime.Text) + "</" + txtTime.ID + ">";
                //    }

                //}

                else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "values" || dr["DropDownType"].ToString() == "value_text"))
                {
                    DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                    if (ddlSearch != null)
                    {
                        strDynamicSearch = strDynamicSearch + " <" + ddlSearch.ID + ">" + HttpUtility.HtmlEncode(ddlSearch.Text) + "</" + ddlSearch.ID + ">";
                    }

                }
                //else if (dr["ColumnType"].ToString() == "radiobutton" && (dr["DropDownType"].ToString() == "values" || dr["DropDownType"].ToString() == "value_text"))
                else if (dr["ColumnType"].ToString() == "radiobutton" || dr["ColumnType"].ToString() == "listbox"
                               || dr["ColumnType"].ToString() == "checkbox")
                {
                    DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                    if (ddlSearch != null)
                    {
                        strDynamicSearch = strDynamicSearch + " <" + ddlSearch.ID + ">" + HttpUtility.HtmlEncode(ddlSearch.Text) + "</" + ddlSearch.ID + ">";
                    }

                }

                else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "table" || dr["DropDownType"].ToString() == "tabledd") &&
           dr["TableTableID"] != DBNull.Value && dr["DisplayColumn"].ToString() != "")
                {
                    //DropDownList ddlParentSearch = (DropDownList)tblSearchControls.FindControl(_strDynamictabPart+ "ddlParentSearch_" + dr["SystemName"].ToString());

                    //if (ddlParentSearch != null)
                    //{
                    //    strDynamicSearch = strDynamicSearch + " <" + ddlParentSearch.ID + ">" + HttpUtility.HtmlEncode(ddlParentSearch.Text) + "</" + ddlParentSearch.ID + ">";
                    //}

                    HiddenField hfParentSearch = (HiddenField)tblSearchControls.FindControl(_strDynamictabPart + "hfParentSearch_" + dr["SystemName"].ToString());

                    if (hfParentSearch != null)
                    {
                        strDynamicSearch = strDynamicSearch + " <" + hfParentSearch.ID + ">" + HttpUtility.HtmlEncode(hfParentSearch.Value) + "</" + hfParentSearch.ID + ">";
                    }

                }
                else
                {
                    TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                    if (txtSearch != null)
                    {
                        strDynamicSearch = strDynamicSearch + " <" + txtSearch.ID + ">" + HttpUtility.HtmlEncode(txtSearch.Text) + "</" + txtSearch.ID + ">";
                    }

                }
            }

            //

            foreach (DataRow dr in _dtSearchGroup.Rows)
            {

                TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SearchGroupID"].ToString());
                if (txtSearch != null)
                {
                    strDynamicSearch = strDynamicSearch + " <" + txtSearch.ID + ">" + HttpUtility.HtmlEncode(txtSearch.Text) + "</" + txtSearch.ID + ">";
                }
            }
        }
        catch
        {
            //
        }
        return strDynamicSearch;


    }

    protected void UpdateSearchCriteriaForTheGrid(string strOtherXMLTags)
    {
        try
        {
            bool? bIsNumericY = null;
            string xml = null;
            xml = @"<root>" +
                   " <" + txtDateFrom.ID + ">" + HttpUtility.HtmlEncode(txtDateFrom.Text) + "</" + txtDateFrom.ID + ">" +
                   " <" + txtDateTo.ID + ">" + HttpUtility.HtmlEncode(txtDateTo.Text) + "</" + txtDateTo.ID + ">" +

                    " <" + hfAndOr1.ID + ">" + HttpUtility.HtmlEncode(hfAndOr1.Value) + "</" + hfAndOr1.ID + ">" +
                     " <" + hfAndOr2.ID + ">" + HttpUtility.HtmlEncode(hfAndOr2.Value) + "</" + hfAndOr2.ID + ">" +
                      " <" + hfAndOr3.ID + ">" + HttpUtility.HtmlEncode(hfAndOr3.Value) + "</" + hfAndOr3.ID + ">" +

                     " <" + cbcSearchMain.ID + ">" + HttpUtility.HtmlEncode(GetSearchCriteriaIDForCBC(cbcSearchMain)) + "</" + cbcSearchMain.ID + ">" +
                       " <" + cbcSearch1.ID + ">" + HttpUtility.HtmlEncode(GetSearchCriteriaIDForCBC(cbcSearch1)) + "</" + cbcSearch1.ID + ">" +
                         " <" + cbcSearch2.ID + ">" + HttpUtility.HtmlEncode(GetSearchCriteriaIDForCBC(cbcSearch2)) + "</" + cbcSearch2.ID + ">" +
                           " <" + cbcSearch3.ID + ">" + HttpUtility.HtmlEncode(GetSearchCriteriaIDForCBC(cbcSearch3)) + "</" + cbcSearch3.ID + ">" +
                   " <bIsNumericY>" + HttpUtility.HtmlEncode(bIsNumericY.ToString()) + "</bIsNumericY>" +

                   " <" + ddlEnteredBy.ID + ">" + HttpUtility.HtmlEncode(ddlEnteredBy.Text) + "</" + ddlEnteredBy.ID + ">" +
                   //" <" + cbcvSumFilter.ID + ">" + HttpUtility.HtmlEncode(cbcvSumFilter.GetValue) + "</" + cbcvSumFilter.ID + ">" +
                   " <" + chkShowDeletedRecords.ID + ">" + HttpUtility.HtmlEncode(chkShowDeletedRecords.Checked.ToString()) + "</" + chkShowDeletedRecords.ID + ">" +
                   " <" + chkShowOnlyWarning.ID + ">" + HttpUtility.HtmlEncode(chkShowOnlyWarning.Checked.ToString()) + "</" + chkShowOnlyWarning.ID + ">" +
                   " <" + chkShowAdvancedOptions.ID + ">" + HttpUtility.HtmlEncode(chkShowAdvancedOptions.Checked.ToString()) + "</" + chkShowAdvancedOptions.ID + ">" +
                   " <" + hfTextSearch.ID + ">" + HttpUtility.HtmlEncode(hfTextSearch.Value) + "</" + hfTextSearch.ID + ">" +
                   " <GridViewSortColumn>" + HttpUtility.HtmlEncode(gvTheGrid.GridViewSortColumn) + "</GridViewSortColumn>" +
                   " <GridViewSortDirection>" + HttpUtility.HtmlEncode(gvTheGrid.GridViewSortDirection == SortDirection.Ascending ? "ASC" : "DESC") + "</GridViewSortDirection>" +


                   //" <_strNumericSearch>" + HttpUtility.HtmlEncode(_strNumericSearch) + "</_strNumericSearch>" +
                   " <TextSearch>" + HttpUtility.HtmlEncode(TextSearch) + "</TextSearch>" +
                   " <TextSearchParent>" + HttpUtility.HtmlEncode(TextSearchParent) + "</TextSearchParent>" +

                   " <_strViewName>" + HttpUtility.HtmlEncode(_strViewName) + "</_strViewName>" +
                   " <strViewID>" + HttpUtility.HtmlEncode(hfViewID.Value) + "</strViewID>" +
                    //RP Modified Ticket 4363
                    //" <" + ddlUploadedBatch.ID + ">" + HttpUtility.HtmlEncode(ddlUploadedBatch.Text) + "</" + ddlUploadedBatch.ID + ">" +
                    " <" + ddlUploadedBatch.ID + ">" + HttpUtility.HtmlEncode(txtUploadedBatch.Text) + "</" + ddlUploadedBatch.ID + ">" +
                    strOtherXMLTags +
                  //" <" + ddlDropdownColumnSearch.ID + ">" + HttpUtility.HtmlEncode(ddlDropdownColumnSearch.SelectedValue == null ? "" : ddlDropdownColumnSearch.SelectedValue) + "</" + ddlDropdownColumnSearch.ID + ">" + strDynamicSearch +
                  "</root>";

            SearchCriteria theSearchCriteria = new SearchCriteria(null, xml);
            _iSearchCriteriaID = SystemData.SearchCriteria_Insert(theSearchCriteria);
            ViewState["_iSearchCriteriaID"] = _iSearchCriteriaID;
            Session["SCid" + hfViewID.Value] = _iSearchCriteriaID;
            SystemData.UserSearch_InsertUpdate(new UserSearch(null, _ObjUser.UserID, _theView.ViewID, theSearchCriteria.SearchText, null));

            //if(!string.IsNullOrEmpty( _theTable.SPSearchGo))
            //{
            //    SystemData.Table_SPSearchGo(_theTable.SPSearchGo, _ObjUser.UserID, _theView.ViewID, Request.RawUrl);
            //}


            hlShowGraph.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Graph/RecordChart.aspx?SearchCriteriaID=" + Cryptography.Encrypt(SearchCriteriaID.ToString()) + "&TableID=" + Cryptography.Encrypt(TableID.ToString());
            //hlSchedule.NavigateUrl = Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Schedule/MonitorSchedules.aspx?SearchCriteriaID=" + Cryptography.Encrypt(SearchCriteriaID.ToString()) + "&TableID=" + Cryptography.Encrypt(TableID.ToString());

            hlUpload.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordUpload.aspx?SearchCriteriaID=" + Cryptography.Encrypt(SearchCriteriaID.ToString()) + "&TableID=" + Cryptography.Encrypt(TableID.ToString());

            //hlDocuments.NavigateUrl = Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/Document.aspx?SSearchCriteriaID=" + Cryptography.Encrypt(SearchCriteriaID.ToString()) + "&TableID=" + Cryptography.Encrypt(TableID.ToString());

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }
    }

    protected void DoFixedHeaderThing(int iTN)
    {
        hfUsingScrol.Value = "";
        if (PageType == "p" && Request.RawUrl.IndexOf("RecordList.aspx") > -1 && gvTheGrid.PageSize > 20 && iTN > 20
            && _theView != null && _theView.ShowFixedHeader != null && (bool)_theView.ShowFixedHeader)
        {
            hfUsingScrol.Value = "yes";
            _bFixedHeader = true;
            //ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "Key", "<script>SetStyleEvent();MakeStaticHeader('" + gvTheGrid.ClientID + "', 600, 1100 , 90 ,false); </script>", false);
            string strStaticheaderJS = @"

                             $(document).ready(function () {
        
                                        $.fn.visibleHeight = function () {
                                            var elBottom, elTop, scrollBot, scrollTop, visibleBottom, visibleTop;
                                            scrollTop = $(window).scrollTop();
                                            scrollBot = scrollTop + $(window).height();
                                            elTop = this.offset().top;
                                            elBottom = elTop + this.outerHeight();
                                            visibleTop = elTop < scrollTop ? scrollTop : elTop;
                                            visibleBottom = elBottom > scrollBot ? scrollBot : elBottom;
                                            return visibleBottom - visibleTop
                                        }

                                function SetStyleEvent() {

                                   // $('#ctl00_HomeContentPlaceHolder_rlOne_UpdateProgress1').show();

                                    var DivPR = document.getElementById('DivPagerRow');
                                    var DivHR = document.getElementById('DivHeaderRow');
                                    var DivMC = document.getElementById('DivMainContent');
                                    var DivFR = document.getElementById('DivFooterRow');
                                    var DivR = document.getElementById('DivRoot');

                                    //DivMC.style.overflowY = 'auto';
                                    //DivMC.style.overflowX = 'hidden';
                                  

                                    //DivMC.style.paddingRight = '17px';
                                    //DivHR.style.paddingRight = '17px'; 
                                    //DivPR.style.paddingRight = '17px';
                                    //DivMC.style.marginRight=-17px;


                                    var tbl = document.getElementById('ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid'); 


                                    var chkAll = $(tbl.rows[1]).find('#ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid_ctl02_chkAll');
                                    $(chkAll).attr('onclick', 'javascript: SelectAllCheckboxesHR(this,ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid);');
                    

                                }


                                 function MakeStaticHeader(gridId, height, width, headerHeight, isFooter) {
                                    var tbl = document.getElementById(gridId);
                                    height = $('#DivMainContent').visibleHeight();
                                    height = height - 65;
                                    if (tbl) {
                                        var DivPR = document.getElementById('DivPagerRow');
                                        var DivHR = document.getElementById('DivHeaderRow');
                                        var DivMC = document.getElementById('DivMainContent');
                                        var DivFR = document.getElementById('DivFooterRow');
                                        var DivR = document.getElementById('DivRoot');

                                        //*** Set divheaderRow Properties ****
                                        var oWidth = $(tbl).outerWidth();
                                        width = $(tbl).width();
                                       var iWidth = $(tbl).innerWidth();
                                        headerHeight = DivHR.style.height;
                                        var paregHeight = 45;

                                        //pager
                                        DivPR.style.height = paregHeight + 'px'; // headerHeight / 2

                                        DivPR.style.position = 'relative';
                                        DivPR.style.top = '0px';
                                        //DivPR.style.verticalAlign = 'top';

                                        DivHR.style.height = headerHeight + 'px';// headerHeight/2

                                        DivHR.style.position = 'relative';
                                        DivHR.style.top = '0px';
                                        // DivHR.style.verticalAlign = 'top';
                                        //DivHR.rules = 'none';


                                        //*** Set divMainContent Properties ****
                                        DivMC.style.height = height + 'px';
                                        DivMC.style.position = 'relative';
                                        DivMC.style.top = '0px'; //(headerHeight) + 'px';// //
                                        DivMC.style.zIndex = '0';




                                        if (isFooter) {
                                            //*** Set divFooterRow Properties ****
                                            DivFR.style.width = (parseInt(width)) + 'px';
                                            DivFR.style.position = 'relative';
                                            DivFR.style.top = -(headerHeight) + 'px';
                                            DivFR.style.verticalAlign = 'top';
                                            DivFR.style.paddingtop = '2px';


                                            var tblfr = tbl.cloneNode(true);
                                            tblfr.removeChild(tblfr.getElementsByTagName('tbody')[0]);
                                            var tblBody = document.createElement('tbody');
                                            tblfr.style.width = '100%';
                                            tblfr.cellSpacing = '0';
                                            tblfr.border = '0px';
                                            tblfr.rules = 'none';
                                            //*****In the case of Footer Row *******
                                            tblBody.appendChild(tbl.rows[tbl.rows.length - 1]);
                                            tblfr.appendChild(tblBody);
                                            DivFR.appendChild(tblfr);
                                        }
                                        //****Copy Header in divHeaderRow****

                                        var tblPR = tbl.cloneNode(true);
                                        tblPR.removeChild(tblPR.getElementsByTagName('tbody')[0]);
                                        $(tblPR).attr('id', gridId + 'PR');

                                        //var tblHR = tbl.cloneNode(true);
                                        //tblHR.removeChild(tblHR.getElementsByTagName('tbody')[0]);
                                        //$(tblHR).attr('id', gridId + 'HR');


                                        var tblBodyPR = document.createElement('tbody');
                                        //var tblBodyHR = document.createElement('tbody');
                                        //var tblBodyHR = document.createElement('thead');                                    

                                        tblBodyPR.appendChild(tbl.rows[0]);
                                        //tblBodyHR.appendChild(tbl.rows[0]);
                                        //tblBodyHR.appendChild(tbl.rows[0]);

                                        tblPR.appendChild(tblBodyPR);
                                        //tblHR.appendChild(tblBodyHR);
                                        //tbl.appendChild(tblBodyHR);                                        

                                        DivPR.appendChild(tblPR);
                                        //DivHR.appendChild(tblHR);
                                       
                                    var iTD0 = 0;
                                    $(tbl.rows[0]).find('td').each(function () {

                                        var aTH = $(tblHR.rows[0]).find('th').eq(iTD0);
                                        var aTD = $(tbl.rows[0]).find('td').eq(iTD0);                
                                        var iFirstWidth=$(tbl.rows[0]).find('td').eq(iTD0).width();
                                        var iColumnWidth=$(tblHR.rows[0]).find('th').eq(iTD0).width();
                                        if(iColumnWidth>iFirstWidth)
                                            {iFirstWidth=iColumnWidth;}
                                        iFirstWidth = iFirstWidth + 10
                                        if(iTD0>0)
                                        {
                                                $(aTD).css({ minWidth: iFirstWidth +'px' });
                                                $(aTD).css({ maxWidth: iFirstWidth +'px' });
                                                $(aTH).css({ minWidth: iFirstWidth +'px' });
                                                $(aTH).css({ maxWidth: iFirstWidth +'px' });

                                        }
                                       

                                        iTD0 = iTD0 + 1;
                                    }); 
                                     //RP Removed - Ticket 4305
                                        //$('#ctl00_HomeContentPlaceHolder_rlOne_UpdateProgress1').hide();
                                    //RP Added - Ticket 4305
                                    //$('ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid').removeAttr('width').removeAttr('height').css({ width: '', height: '' });
                                    //$('#ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid').find('.gridview_row').removeClass('gridview_row')
                                    //$('#ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid').find('.gridview_header').removeClass('gridview_header')
                                    $('#ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid').table_scroll({
                                        fixedColumnsLeft: 1,
                                        //fixedColumnsRight: 1,
                                        columnsInScrollableArea: 5,
                                        scrollX: 5,
                                        scrollY: 10
                                    });                                    
                                    }
                                }

                                     $('.ajax-indicator-full').show();
                                    SetStyleEvent();
                                    MakeStaticHeader('" + gvTheGrid.ClientID + @"', 600, 1600 , 90 ,false);
                                    $('.ajax-indicator-full').hide();
                                    //$('.gridview').mCustomScrollBar('destroy');

                            });
 
                        ";


            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "strStaticheaderJS_key", strStaticheaderJS, true);


            //lnkEditManyCancel.Visible = false;
            //lnkEditManyCancel2.Visible = true;
            //mpeEditMany.OkControlID = "";


            //lnkAddRecordCancel.Visible = false;
            //lnkAddRecordCancel2.Visible = true;
            //mpeAddRecord.OkControlID = "";

            //lnkExportRecordsCancel.Visible = false;
            //lnkExportRecordsCancel2.Visible = true;
            //mpeExportRecords.OkControlID = "";
        }
    }
    //protected void BindTheGridForExport(int iStartIndex, int iMaxRows)
    //{
    //    lblMsg.Text = "";
    //    _bIsForExport = true;
    //    try
    //    {
    //        int iTN = 0;

    //        string strOrderDirection = "DESC";
    //        string sOrder = "DBGSystemRecordID";

    //        _dtRecordColums = RecordManager.ets_Table_Columns_Summary_Export(TableID, int.Parse(hfViewID.Value));

    //        bool bSortColumnFound = false;
    //        string strNameOnExport1 = "";
    //        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //        {

    //            if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() != "" && strNameOnExport1 == "")
    //            {
    //                strNameOnExport1 = _dtRecordColums.Rows[i]["NameOnExport"].ToString();
    //            }

    //            if (gvTheGrid.GridViewSortColumn.IndexOf(_dtRecordColums.Rows[i]["Heading"].ToString(), 0) >= 0
    //                && _dtRecordColums.Rows[i]["NameOnExport"].ToString() != "")
    //            {
    //                SortDirection sdTEmp = gvTheGrid.GridViewSortDirection;
    //                gvTheGrid.GridViewSortColumn = gvTheGrid.GridViewSortColumn.Replace(_dtRecordColums.Rows[i]["Heading"].ToString(), _dtRecordColums.Rows[i]["NameOnExport"].ToString());
    //                gvTheGrid.GridViewSortDirection = sdTEmp;
    //                bSortColumnFound = true;
    //            }
    //        }

    //        if (bSortColumnFound == false && strNameOnExport1 != "")
    //        {
    //            gvTheGrid.GridViewSortColumn = strNameOnExport1;
    //        }

    //        if (gvTheGrid.GridViewSortDirection == SortDirection.Ascending)
    //        {
    //            strOrderDirection = "ASC";
    //        }
    //        if (gvTheGrid.GridViewSortColumn != "")
    //        {
    //            sOrder = gvTheGrid.GridViewSortColumn;
    //        }

    //        TextSearch = TextSearch + hfTextSearch.Value;

    //        if ((bool)_theUserRole.IsAdvancedSecurity)
    //        {
    //            if (_strRecordRightID == Common.UserRoleType.OwnData)
    //            {
    //                TextSearch = TextSearch + " AND (Record.OwnerUserID=" + _ObjUser.UserID.ToString() + " OR Record.EnteredBy=" + _ObjUser.UserID.ToString() + ")";
    //            }
    //        }
    //        else
    //        {
    //            if (Session["roletype"].ToString() == Common.UserRoleType.OwnData)
    //            {
    //                TextSearch = TextSearch + " AND (Record.OwnerUserID=" + _ObjUser.UserID.ToString() + " OR Record.EnteredBy=" + _ObjUser.UserID.ToString() + ")";
    //            }
    //        }

    //        PopulateDateAddedSearch();

    //        if (chkShowAdvancedOptions.Checked && ddlUploadedBatch.SelectedValue != "")
    //        {
    //            TextSearch = TextSearch + "  AND Record.BatchID=" + ddlUploadedBatch.SelectedValue + "";
    //        }
    //        string strReturnSQL = "";
    //        _dtDataSource = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
    //            ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
    //            !chkIsActive.Checked,
    //            chkShowOnlyWarning.Checked == false ? null : (bool?)true,
    //            null, null,
    //            sOrder, strOrderDirection, iStartIndex, iStartIndex, ref iTN, ref _iTotalDynamicColumns, "export", _strNumericSearch, TextSearch + TextSearchParent,
    //           _dtDateFrom, _dtDateTo, "", "", "", null, ref strReturnSQL, ref strReturnSQL);



    //        //now lets play with Record list and columns list
    //        _dtRecordColums = RecordManager.ets_Table_Columns_Summary(TableID, int.Parse(hfViewID.Value));

    //        DataRow drFooter = _dtDataSource.NewRow();

    //        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //        {
    //            for (int j = 0; j < _dtDataSource.Columns.Count; j++)
    //            {

    //                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == _dtDataSource.Columns[j].ColumnName)
    //                {
    //                    if (_dtRecordColums.Rows[i]["ShowTotal"].ToString().ToLower() == "true")
    //                    {
    //                        drFooter[_dtRecordColums.Rows[i]["NameOnExport"].ToString()] = CalculateTotalForAColumn(_dtDataSource, _dtDataSource.Columns[j].ColumnName, bool.Parse(_dtRecordColums.Rows[i]["IgnoreSymbols"].ToString().ToLower()));
    //                    }
    //                }
    //            }
    //        }

    //        _dtDataSource.Rows.Add(drFooter);

    //        gvTheGrid.ShowFooter = false;
    //        gvTheGrid.DataSource = _dtDataSource;
    //        gvTheGrid.VirtualItemCount = iTN;//+ 1
    //        gvTheGrid.DataBind();
    //        if (gvTheGrid.TopPagerRow != null)
    //            gvTheGrid.TopPagerRow.Visible = true;

    //        GridViewRow gvr = gvTheGrid.TopPagerRow;
    //        if (gvr != null)
    //        {
    //            _gvPager = (Common_Pager)gvr.FindControl("Pager");
    //            _gvPager.AddURL = GetAddURL();

    //            if (_gvPager != null)
    //            {
    //                int iViewID = -1;
    //                if (hfViewID.Value != "")
    //                    iViewID = int.Parse(hfViewID.Value);


    //                _gvPager.EditViewCSSClass = "popuplinkEV" + _strFunctionUQ;
    //                _gvPager.EditViewToolTip = "Edit View";
    //                _gvPager.EditViewURL = GetEditViewPageURL();

    //                if (_strNoAjaxView != "")
    //                {
    //                    _gvPager.EditViewTarget = "_parent";
    //                }
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorLog theErrorLog = new ErrorLog(null, "Records", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //        SystemData.ErrorLog_Insert(theErrorLog);
    //        lblMsg.Text = ex.Message;
    //    }

    //}

    protected void ddl_search(Object sender, EventArgs e)
    {
        lnkSearch_Click(null, null);
    }

    protected double CalculateTotalForAColumn(DataTable dt, string strColumn, bool bIgnoreSymbols)
    {
        double amount = 0;

        foreach (DataRow row in dt.Rows)
        {
            if (row[strColumn].ToString() != "")
            {
                try
                {
                    //if (bIgnoreSymbols)
                    //{
                    amount += double.Parse(Common.IgnoreSymbols(row[strColumn].ToString()), System.Globalization.NumberStyles.Any);
                    //}
                    //else
                    //{
                    //    amount += double.Parse(row[strColumn].ToString(), System.Globalization.NumberStyles.Any);
                    //}
                }
                catch (Exception ex)
                {
                    //
                }
            }
        }

        return amount;
    }

    public string GetAddURL()
    {
        if (PageType == "p" && DisplayInColumnID==null)
        {
            string strExtra = "";

            //if (Request.QueryString["viewname"] != null)
            //{
            //    strExtra = "&viewname=" + Request.QueryString["viewname"].ToString();
            //}
            if (Request.QueryString["View"] != null && Request.RawUrl.IndexOf("RecordList.aspx") > -1)
            {
                strExtra = "&View=" + Request.QueryString["View"].ToString();
            }


            if (_bOpenInParent)
            {
                strExtra = strExtra + "&fixedurl=" + Cryptography.Encrypt("~/Default.aspx");
            }

            string strAddURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?mode=" + Cryptography.Encrypt("add") + "&TableID=" + Cryptography.Encrypt(TableID.ToString()) + "&SearchCriteriaID=" + Cryptography.Encrypt(SearchCriteriaID.ToString()) + strExtra;

            if (_theTable.AddOpensForm != null && (bool)_theTable.AddOpensForm && _theTable.AddRecordSP != "")
            {
                strAddURL = "javascript:AddClick();";
            }
            return strAddURL;
        }
        else if (DisplayInColumnID != null)
        {
            string strParentRecordID = "";
            if(Request.QueryString["Recordid"]!=null)
            {
                strParentRecordID = "&parentRecordid=" + Request.QueryString["Recordid"].ToString();
            }
            return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/" + _strRecordFolder 
                + "/RecordDetail.aspx?mode=" + Cryptography.Encrypt("add") + "&tabindex=" + DetailTabIndex.ToString() + "&onlyback=yes" 
                + strParentRecordID + "&TableID=" + Cryptography.Encrypt(TableID.ToString()) + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");
        }
        else
        {
            if (Request.QueryString["Recordid"] == null)
            {
                return "";
            }
            else
            {
                return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/" + _strRecordFolder + "/RecordDetail.aspx?mode=" + Cryptography.Encrypt("add") + "&tabindex=" + DetailTabIndex.ToString() + "&onlyback=yes&parentRecordid=" + Request.QueryString["Recordid"].ToString() + "&TableID=" + Cryptography.Encrypt(TableID.ToString()) + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");
            }
        }

    }

    protected void lnkAddRecordCancel2_Click(object sender, EventArgs e)
    {
        lnkSearch_Click(null, null);
    }

    protected void AdjustMenu()
    {
        if (_iTotalDynamicColumns > 15)
        {
            try
            {
                if (_iTotalDynamicColumns > 15 && Request.Url.ToString().IndexOf("RecordList.aspx") > -1)
                {
                    HtmlTableCell tdMenuCell = (HtmlTableCell)this.Page.Master.FindControl("tdMenuCell");
                    if (tdMenuCell != null)
                    {
                        tdMenuCell.Align = "left";
                        tdMenuCell.Style.Add("padding-left", "400px");
                    }
                }
            }
            catch
            {
                //
            }

        }
    }
    protected void SetGridFromView()
    {
        gvTheGrid.GridViewSortColumn = "DBGSystemRecordID";
        gvTheGrid.GridViewSortDirection = SortDirection.Descending;

        string strSortColumn = "";
        string strDirection = "";
        if (_theView.SortOrder != "")
        {
            strSortColumn = _theView.SortOrder.Substring(0, _theView.SortOrder.LastIndexOf(" ") + 1);
            strDirection = _theView.SortOrder.Substring(_theView.SortOrder.LastIndexOf(" ") + 1);
        }
        string strSortColumn2 = "";
        string strDirection2 = "";
        if (_theView.SortOrder2 != "")
        {
            strSortColumn2 = _theView.SortOrder2.Substring(0, _theView.SortOrder2.LastIndexOf(" ") + 1);
            strDirection2 = _theView.SortOrder2.Substring(_theView.SortOrder2.LastIndexOf(" ") + 1);
        }

        string strMainSort = strSortColumn + " " + strDirection;
        string strSortExpression;
        if (strMainSort.Trim() == "")
        {
            strSortExpression = strSortColumn2;
        }
        else
        {
            if (strSortColumn2 == "")
            {
                strSortExpression = strSortColumn;
                strDirection2 = strDirection;
            }
            else
            {
                strSortExpression = strMainSort + "[sortBreaker] " + strSortColumn2;
            }
        }

        if (strSortExpression.Trim() != "")
        {
            gvTheGrid.GridViewSortColumn = strSortExpression;
            if (strDirection2.Trim().ToLower() == "asc")
            {
                gvTheGrid.GridViewSortDirection = SortDirection.Ascending;
                //gvTheGrid.Sort(strSortExpression, SortDirection.Descending);
            }
            else
            {
                gvTheGrid.GridViewSortDirection = SortDirection.Descending;
                //gvTheGrid.Sort(strSortExpression, SortDirection.Ascending);
            }
        }

        //if (_theView.SortOrder != "")
        //{
        //    string strSortColumn = _theView.SortOrder.Substring(0, _theView.SortOrder.LastIndexOf(" ") + 1);
        //    string strDirection = _theView.SortOrder.Substring(_theView.SortOrder.LastIndexOf(" ") + 1);

        //    gvTheGrid.GridViewSortColumn = strSortColumn.Trim();

        //    if (strDirection.Trim().ToLower() == "desc")
        //    {
        //        gvTheGrid.GridViewSortDirection = SortDirection.Descending;
        //    }
        //    else
        //    {
        //        gvTheGrid.GridViewSortDirection = SortDirection.Ascending;
        //    }

        //}
    }
    protected void btnRefreshViewChange_Click(object sender, EventArgs e)
    {

        if (Session["SCid" + hfViewID.Value] != null)
        {
            PopulateSearchCriteria(int.Parse(Session["SCid" + hfViewID.Value].ToString()), false);
            PopulateDynamicControls();


            if (Session["SCid" + hfViewID.Value] != null && Session["SCid" + hfViewID.Value].ToString() == "-1")
                SetGridFromView();

            BindTheGrid(0, gvTheGrid.PageSize);
        }
    }
    protected void lnkExportRecordsCancel2_Click(object sender, EventArgs e)
    {
        lnkSearch_Click(null, null);
    }

    //protected void lnkAddRecordOK_Click(object sender, EventArgs e)
    //{
    //    lblMsgAddRecord.Text = "";
    //    if (ddlFormSet.SelectedItem == null)
    //    {
    //        lblMsgAddRecord.Text = "Please select a Form.";
    //        mpeAddRecord.Show();
    //        return;
    //    }



    //    if (SecurityManager.IsRecordsExceeded((int)_theUserRole.AccountID))
    //    {
    //        Session["DoNotAllow"] = "true";
    //        ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "DoNotAllow", "alert('" + Common.RecordExceededMessage.Replace("'", "''") + "');", true);
    //        return;
    //    }


    //    //add a record and goto the edit record page

    //    Record theRecord = new Record();
    //    theRecord.TableID = _theTable.TableID;
    //    theRecord.IsActive = true;
    //    theRecord.EnteredBy = _ObjUser.UserID;
    //    //DataTable _dtColumnsAll = RecordManager.ets_Table_Columns_All((int)_theTable.TableID, null, null);
    //    for (int i = 0; i < _dtColumnsAll.Rows.Count; i++)
    //    {
    //        if (_dtColumnsAll.Rows[i]["ColumnType"].ToString() == "number")
    //        {
    //            if (_dtColumnsAll.Rows[i]["NumberType"] != null)
    //            {
    //                if (_dtColumnsAll.Rows[i]["NumberType"].ToString() == "8")
    //                {
    //                    string strValue = "1";
    //                    try
    //                    {
    //                        string strMax = Common.GetValueFromSQL("SELECT MAX(CONVERT(INT," + _dtColumnsAll.Rows[i]["SystemName"].ToString() + ")) FROM  Record  WHERE  IsNumeric(" + _dtColumnsAll.Rows[i]["SystemName"].ToString() + ")=1 AND  TableID=" + _qsTableID);
    //                        if (strMax == "")
    //                        {
    //                            strValue = "1";
    //                        }
    //                        else
    //                        {
    //                            strValue = (int.Parse(strMax) + 1).ToString();
    //                        }
    //                    }
    //                    catch
    //                    {
    //                        strValue = "1";
    //                    }
    //                    RecordManager.MakeTheRecord(ref theRecord, _dtColumnsAll.Rows[i]["SystemName"].ToString(), strValue);
    //                }

    //            }
    //        }

    //    }
    //    for (int i = 0; i < _dtColumnsAll.Rows.Count; i++)
    //    {
    //        if (_dtColumnsAll.Rows[i]["DefaultValue"].ToString() != "")
    //        {
    //            if (_dtColumnsAll.Rows[i]["ColumnType"].ToString().Trim().ToLower() == "datetime"
    //                || _dtColumnsAll.Rows[i]["ColumnType"].ToString().Trim().ToLower() == "date"
    //                || _dtColumnsAll.Rows[i]["ColumnType"].ToString().Trim().ToLower() == "time")
    //            {


    //                RecordManager.MakeTheRecord(ref theRecord, _dtColumnsAll.Rows[i]["SystemName"].ToString(), DateTime.Now);
    //            }
    //            else
    //            {



    //                RecordManager.MakeTheRecord(ref theRecord, _dtColumnsAll.Rows[i]["SystemName"].ToString(),
    //                    _dtColumnsAll.Rows[i]["DefaultValue"].ToString());
    //            }

    //        }

    //    }




    //    int iNewRecordID = RecordManager.ets_Record_Insert(theRecord);


    //    //call the SP

    //    RecordManager.AddRecordSP(_theTable.AddRecordSP, iNewRecordID, int.Parse(ddlFormSet.SelectedValue));

    //    string strSearch = Cryptography.Encrypt("-1");
    //    if (Request.QueryString["SearchCriteriaID"] != null)
    //    {
    //        strSearch = Request.QueryString["SearchCriteriaID"].ToString();
    //    }
    //    string strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/FormSetWizard.aspx?SearchCriteriaID=" + strSearch
    //+ "&FormSetID=" + Cryptography.Encrypt(ddlFormSet.SelectedValue)
    //+ "&ParentTableID=" + Cryptography.Encrypt(_theTable.TableID.ToString())
    //+ "&ParentRecordID=" + Cryptography.Encrypt(iNewRecordID.ToString()) + "&ps=0";
    //    Response.Redirect(strURL);

    //}

    protected HorizontalAlign GetHorizontalAlign(string strAlignment, string strColumntype)
    {
        switch (strAlignment.ToLower())
        {
            case "left":
                return HorizontalAlign.Left;
            case "right":
                return HorizontalAlign.Right;
            case "center":
                return HorizontalAlign.Center;

        }

        //string strViewAlignmentDefault = "Auto";
        //if (ViewState["ViewAlignmentDefault"] != null)
        //    strViewAlignmentDefault = ViewState["ViewAlignmentDefault"].ToString();


        //switch (strViewAlignmentDefault.ToLower())
        //{
        //    case "left":
        //        return HorizontalAlign.Left;
        //    case "right":
        //        return HorizontalAlign.Right;
        //    case "center":
        //        return HorizontalAlign.Center;

        //}

        switch (strColumntype.ToLower())
        {
            case "calculation":
                return HorizontalAlign.Right;
            case "number":
                return HorizontalAlign.Right;
            case "text":
                return HorizontalAlign.Left;
            case "staticcontent":
                return HorizontalAlign.Left;
            case "content":
                return HorizontalAlign.Left;
            case "location":
                return HorizontalAlign.Left;

        }

        return HorizontalAlign.Center;

    }

    protected void gvTheGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        //gvTheGrid.PageIndex = 0;// why???
        _dtRecordColums = RecordManager.ets_Table_Columns_Summary(TableID, int.Parse(hfViewID.Value));

        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
        {
            if (_dtRecordColums.Rows[i]["Heading"].ToString() == gvTheGrid.GridViewSortColumn)
            {

                ViewState["SortOrderColumnID"] = _dtRecordColums.Rows[i]["ColumnID"].ToString();
                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "number" || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "calculation")
                {


                    gvTheGrid.GridViewSortColumn = "CONVERT(decimal(38,2), dbo.RemoveNonNumericChar([" + _dtRecordColums.Rows[i]["Heading"].ToString() + "])) ";


                }
                else if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "date"
                    || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "datetime"
                    || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "time")
                {

                    //DateTime dateValue;
                    //if (DateTime.TryParseExact(_dtRecordColums.Rows[i]["Heading"].ToString(), Common.Dateformats,
                    //             new CultureInfo("en-GB"),
                    //             DateTimeStyles.None,
                    //             out dateValue))
                    //{
                    //    gvTheGrid.GridViewSortColumn = "CONVERT(Datetime, [" + dateValue.ToShortDateString() + "],103) ";

                    //}


                    gvTheGrid.GridViewSortColumn = "CONVERT(Datetime,[dbo].[fnRemoveNonDate]( [" + _dtRecordColums.Rows[i]["Heading"].ToString() + "]),103) ";




                    // return;
                }
                else
                {
                    //
                }

                if (ViewState[_dtRecordColums.Rows[i]["Heading"].ToString()] == null)
                {
                    ViewState[_dtRecordColums.Rows[i]["Heading"].ToString()] = "ASC";
                    gvTheGrid.GridViewSortDirection = SortDirection.Ascending;
                }
                else
                {
                    if (ViewState[_dtRecordColums.Rows[i]["Heading"].ToString()].ToString() == "ASC")
                    {
                        ViewState[_dtRecordColums.Rows[i]["Heading"].ToString()] = "DESC";
                        gvTheGrid.GridViewSortDirection = SortDirection.Descending;
                    }
                    else
                    {
                        gvTheGrid.GridViewSortDirection = SortDirection.Ascending;
                        ViewState[_dtRecordColums.Rows[i]["Heading"].ToString()] = "ASC";
                    }

                }
                break;
            }
        }

        BindTheGrid(0, gvTheGrid.PageSize);

    }


    //protected void bntShowGraph_Click(object sender, ImageClickEventArgs e)
    //{
    //    SearchCriteria newSearchCriteria= new SearchCriteria(null,
    //        txtDateFrom.Text.Trim() == "" ? null : (DateTime?)DateTime.ParseExact(txtDateFrom.Text, "d/M/yyyy", CultureInfo.InvariantCulture),
    //            txtDateTo.Text.Trim() == "" ? null : (DateTime?)DateTime.ParseExact(txtDateTo.Text,"d/M/yyyy",CultureInfo.InvariantCulture),
    //            GetLocationIDs(),
    //            null,null,int.Parse(Session["AccountID"].ToString()));

    //    int iSearchCriteriaID = RecordManager.ets_SearchCriteria_Insert(newSearchCriteria);
    //    Response.Redirect(Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Graph/RecordChart.aspx?SearchCriteriaID="+iSearchCriteriaID.ToString()+"&TableID=" + TableID.ToString() );

    //}



    //protected void btnSearch_Click(object sender, ImageClickEventArgs e)


    protected void lnkReset_Click(object sender, EventArgs e)
    {
        //reset individual ddl search standard
        //this is when we reset/show all the dropdown... - Red 13082018
        if (hfddlShowAll.Value != "")
        {
            PopulateDropDownSearchShowAll();
            hfddlShowAll.Value = "";
        }      
        else
        {
            Pager_OnApplyFilter(sender, e);
        }
           

        //Red Ticket 3611 for Search DDL
        if (!chkShowAdvancedOptions.Checked) //3611
        {
            ViewState["strKeepTextSearch"] = null;
            lnkDDLPopulate_Click(null, null);
        }
             

    }

    protected void lnkShowAllDDLSearch_Click(object sender, EventArgs e)
    {
        PopulateDropDownSearchShowAll();

        //Red Ticket 3611 for Search DDL
        if (!chkShowAdvancedOptions.Checked) //3611
        {
            ViewState["strKeepTextSearch"] = null;
            lnkDDLPopulate_Click(null, null);
        }

    }


    protected void lnkSearch_Click(object sender, EventArgs e)
    {
        //gvTheGrid.GridViewSortColumn = "";

        //gvTheGrid.PageIndex = 0;// why???    

        BindTheGrid(0, gvTheGrid.PageSize);
               

    }

    protected void lnkDDLPopulate_Click(object sender, EventArgs e)
    {
        PopulateDropDownSearch(sender, null);

    }

    //this is for ViewEdit ddl values only... - Red
    //called when !IsPostback and clicked Reset button only... - Red
    protected void PopulateDropDownSearchOnLoad(int? ddlColumnIDSearch)
    {

        foreach (DataRow dtr in _dtDynamicSearchColumns.Rows)
        {
            if (dtr["ColumnType"].ToString() == "dropdown" && (dtr["DropDownType"].ToString() == "table" || dtr["DropDownType"].ToString() == "tabledd") &&
                dtr["TableTableID"] != DBNull.Value && dtr["DisplayColumn"].ToString() != "" && int.Parse(dtr["ColumnID"].ToString()) == ddlColumnIDSearch)
            {
                DropDownList ddlParentSearch = (DropDownList)tblSearchControls.FindControl(_strDynamictabPart + "ddlParentSearch_" + dtr["SystemName"].ToString());
                HiddenField hfParentSearch = (HiddenField)tblSearchControls.FindControl(_strDynamictabPart + "hfParentSearch_" + dtr["SystemName"].ToString());

                string strVT = "ColumnID";

                int iOneColumnConCount = 0;
                string strDDLMultipleValue = "";
                string strValue = "";
                string strOperator = "=";
                string strWaterViewmark = " ";
                if (_xmlView_FC_Doc != null)
                {
                    strDDLMultipleValue = GetDDLMultipleValueFromViewFilter(_xmlView_FC_Doc, dtr["ColumnID"].ToString());
                    strValue = GetValueFromViewFilter(_xmlView_FC_Doc, dtr["ColumnID"].ToString());
                    strOperator = GetOperatorFromViewFilter(_xmlView_FC_Doc, dtr["ColumnID"].ToString());

                    strWaterViewmark = GetMultipleCondition(_xmlView_FC_Doc, dtr["ColumnID"].ToString(), ref iOneColumnConCount, strVT);

                    if (strWaterViewmark != "")
                    {
                        if (ddlParentSearch.Items.Count > 0)
                        {
                            ddlParentSearch.ToolTip = strWaterViewmark;
                        }
                    }


                    if (strValue == "-user-" && dtr["LinkedParentColumnID"] != DBNull.Value)
                    {
                        string strColumnuserID = Common.GetValueFromSQL(@"SELECT TOP 1 ColumnID FROM [Column] WHERE TableID=" + dtr["TableTableID"].ToString() + @" AND 
                                ColumnType='dropdown' AND DisplayColumn IS NOT NULL
                                            AND TableTableID=-1");

                        if (strColumnuserID != "")
                        {
                            string strLoginText = "";
                            SecurityManager.ProcessLoginUserDefault(dtr["TableTableID"].ToString(), "",
                            dtr["LinkedParentColumnID"].ToString(), _ObjUser.UserID.ToString(), ref strValue, ref strLoginText);

                        }
                    }
                    //check the "and" and "or" in View Edit with the columnid, check the condition i made that says where clause is exist alaredy.... check how the other column search treat the 2 conditions 
                    if (iOneColumnConCount < 2 && strValue != null && strOperator == "=" && ThisIsOR(strWaterViewmark) == false)
                    {
                        RecordManager.PopulateTableDropDown(int.Parse(dtr["ColumnID"].ToString()), ref ddlParentSearch, "", "", Boolean.Parse(dtr["ShowAllValuesOnSearch"].ToString()));
                        if (ddlParentSearch.Items.Count > 0)
                        {
                            ddlParentSearch.Items.RemoveAt(0);
                        }
                        if (ddlParentSearch.Items.FindByValue(strValue) != null)
                            {
                            //ddlParentSearch.SelectedValue = strValue;
                            ddlParentSearch.Items.FindByValue(strValue).Text = strWaterViewmark;
                            
                                hfParentSearch.Value = strValue;
                            }


                    }
                    else if (iOneColumnConCount > 1 && strDDLMultipleValue != "" && ThisIsOR(strWaterViewmark) == false)
                    {
                        RecordManager.PopulateTableDropDown(int.Parse(dtr["ColumnID"].ToString()), ref ddlParentSearch, "", "", Boolean.Parse(dtr["ShowAllValuesOnSearch"].ToString()));
                        if (ddlParentSearch.Items.Count > 0)
                        {
                            ddlParentSearch.Items.RemoveAt(0);
                        }

                            hfParentSearch.Value = strDDLMultipleValue;

                    }
                   
                }
            }
        }
    }

    //here we reset the selected ddl, when nothing is selected therein... - Red 13082018
    protected void PopulateDropDownSearchShowAll()
    {
        if (hfddlShowAll.Value != "")
        {
            foreach (DataRow dtr in _dtDynamicSearchColumns.Rows)
            {
                if (dtr["ColumnType"].ToString() == "dropdown" && (dtr["DropDownType"].ToString() == "table" || dtr["DropDownType"].ToString() == "tabledd") &&
                    dtr["TableTableID"] != DBNull.Value && dtr["DisplayColumn"].ToString() != "" && dtr["SystemName"].ToString() == hfddlShowAll.Value)
                {
                    HiddenField hfParentSearch = (HiddenField)tblSearchControls.FindControl(_strDynamictabPart + "hfParentSearch_" + dtr["SystemName"].ToString());

                        if (hfParentSearch != null)
                        {
                            hfParentSearch.Value = "";

                        }
                }
            }

        }

      BindTheGrid(0, gvTheGrid.PageSize);


    }

    protected void PopulateDropDownSearch(object sender, int? ColumnID)
    {
        //lets populate the dropdown list
        if (sender != null)
        {
            foreach (DataRow dtr in _dtDynamicSearchColumns.Rows)
            {
                HiddenField hfSystemName = (HiddenField)tblSearchControls.FindControl(_strDynamictabPart + "hfSystemName_" + dtr["SystemName"].ToString());
                string strSystemName = "";
                if (hfSystemName != null)
                {
                    strSystemName = hfSystemName.Value.ToString();
                    hfSystemName.Value = "";
                }

                if (!string.IsNullOrEmpty(strSystemName))
                {
                    if (dtr["ColumnType"].ToString() == "dropdown" && (dtr["DropDownType"].ToString() == "table" || dtr["DropDownType"].ToString() == "tabledd") &&
                        dtr["TableTableID"] != DBNull.Value && dtr["DisplayColumn"].ToString() != "" && dtr["SystemName"].ToString() == strSystemName)
                    {
                        DropDownList ddlParentSearch = (DropDownList)tblSearchControls.FindControl(_strDynamictabPart + "ddlParentSearch_" + dtr["SystemName"].ToString());
                        if (ddlParentSearch.Items.Count == 0)
                        {

                            string strDDLTextSearch = "";
                            if (ViewState["strKeepTextSearch"] != null)
                                strDDLTextSearch = ViewState["strKeepTextSearch"].ToString();

                            string strTextSearchParent = "";
                            if (TextSearchParent == null)
                            {
                                strTextSearchParent = "";
                            }
                            else
                            {
                                strTextSearchParent = TextSearchParent.Replace("'", "") + strDDLTextSearch;
                                strDDLTextSearch = "";

                            }

                            if (PageType == "p")
                            {
                                if (ViewState["strReturnSQL"] != null)
                                {
                                    strTextSearchParent = strTextSearchParent + strDDLTextSearch;

                                    strTextSearchParent = " AND Record.RecordID IN (" + ViewState["strReturnSQL"].ToString().Replace("SET DATEFORMAT dmy;", "").Replace("SELECT *", "SELECT [DBGSystemRecordID]") + ")";

                                    strDDLTextSearch = "";
                                }

                            }

                            int iRecordsInGridView = 0;
                            if (ViewState["iTN"] != null)
                                iRecordsInGridView = int.Parse(ViewState["iTN"].ToString());

                            if (iRecordsInGridView > 0)
                            {
                                RecordManager.PopulateTableDropDown(int.Parse(dtr["ColumnID"].ToString()), ref ddlParentSearch, strDDLTextSearch, strTextSearchParent, (Boolean)dtr["ShowAllValuesOnSearch"]);//strTextSearchParent); //3611
                            }
                            else
                            {
                                //lets drop down the list
                                string strPlaceHolderLoad = @" 
                            $('#" + _strDynamictabPart + "multipleSelect_" + dtr["SystemName"].ToString() + @"').parent().text("" -- No valid records --"");";
                                ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "thePlaceHoldLoading" + _strDynamictabPart + dtr["SystemName"].ToString(), strPlaceHolderLoad, true);
                            }

                        }
                        if (ddlParentSearch.Items.Count > 0) 
                        {

                            if (ddlParentSearch.Items.Count > 1)
                            {
                                //lets drop down the list
                                string strPlaceHolderLoad = @" 
                                $('#" + _strDynamictabPart + "multipleSelect_" + dtr["SystemName"].ToString() + @"').parent().last().click();   ";
                                ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "thePlaceHoldLoading" + _strDynamictabPart + dtr["SystemName"].ToString(), strPlaceHolderLoad, true);
                            }

                            ddlParentSearch.Items.RemoveAt(0);
                        }
                    }
                }
            }
        }
        
        //reset other ddl or all ddl here
        else if (!string.IsNullOrEmpty(hfSystemNamePrevsious.Value.ToString()) && sender == null)   // we are going to update other dropdwon fields lists; if it has already loaded previously
        {
            HiddenField hfParentSearchActiveSelected = (HiddenField)tblSearchControls.FindControl(_strDynamictabPart + "hfParentSearch_" + hfSystemNamePrevsious.Value.ToString());

            if (hfParentSearchActiveSelected != null)
            {
                if (!IsFilteredStandard() || hfParentSearchActiveSelected.Value == "")
                { //lets update or reload the dropdowns 

                    ViewState["strKeepTextSearch"] = null;
                    hfSystemNamePrevsious.Value = "";
                }

            }
           


            foreach (DataRow dtr in _dtDynamicSearchColumns.Rows)
            {
                if (dtr["ColumnType"].ToString() == "dropdown" && (dtr["DropDownType"].ToString() == "table" || dtr["DropDownType"].ToString() == "tabledd") &&
                    dtr["TableTableID"] != DBNull.Value && dtr["DisplayColumn"].ToString() != "" && dtr["SystemName"].ToString() != hfSystemNamePrevsious.Value/*RP Added Ticket 4310*//*&& (Boolean)dtr["ShowAllValuesOnSearch"] == false*//*End Modification*/)
                {
                    HiddenField hfParentSearch = (HiddenField)tblSearchControls.FindControl(_strDynamictabPart + "hfParentSearch_" + dtr["SystemName"].ToString());
                    DropDownList ddlParentSearch = (DropDownList)tblSearchControls.FindControl(_strDynamictabPart + "ddlParentSearch_" + dtr["SystemName"].ToString());
                    if (ddlParentSearch.Items.Count > 0)
                    {

                        string strDDLTextSearch = "";
                        if (ViewState["strKeepTextSearch"] != null)
                            strDDLTextSearch = ViewState["strKeepTextSearch"].ToString();

                        string strTextSearchParent = "";
                        if (TextSearchParent == null)
                        {
                            strTextSearchParent = "";
                        }
                        else
                        {
                            strTextSearchParent = TextSearchParent.Replace("'", "") + strDDLTextSearch;
                            strDDLTextSearch = "";
                        }

                        if (PageType == "p")
                        {
                            if (ViewState["strReturnSQL"] != null)
                            {
                                string sdsdaads = ViewState["strReturnSQL"].ToString();
                                strTextSearchParent = strTextSearchParent + strDDLTextSearch;

                                strTextSearchParent = " AND Record.RecordID IN (" + ViewState["strReturnSQL"].ToString().Replace("SET DATEFORMAT dmy;", "").Replace("SELECT *", "SELECT [DBGSystemRecordID]") + ")";

                                strDDLTextSearch = "";
                            }

                        }

                        int iRecordsInGridView = 0;
                        if (ViewState["iTN"] != null)
                            iRecordsInGridView = int.Parse(ViewState["iTN"].ToString());

                        if (iRecordsInGridView > 0)
                        {
                            RecordManager.PopulateTableDropDown(int.Parse(dtr["ColumnID"].ToString()), ref ddlParentSearch, strDDLTextSearch, strTextSearchParent, (Boolean)dtr["ShowAllValuesOnSearch"]);//strTextSearchParent); //3611
                        }
                        else
                        {
                            ddlParentSearch.Items.Clear();
                            string strPlaceHolderLoad = @" 
                            $('#" + _strDynamictabPart + "multipleSelect_" + dtr["SystemName"].ToString() + @"').parent().text("" -- No valid records --"");";
                            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "thePlaceHoldLoading" + _strDynamictabPart + dtr["SystemName"].ToString(), strPlaceHolderLoad, true);


                        }
                    }
                    if (ddlParentSearch.Items.Count > 0)
                    {
                        ddlParentSearch.Items.RemoveAt(0);

                    }
                }
            }

            //lets reset the system name
            hfSystemNamePrevsious.Value = "";
        }

        else if (sender == null) //when we search and the previous search has nothing to do with dropdown but click the search Go
        {
            foreach (DataRow dtr in _dtDynamicSearchColumns.Rows)
            {
                if (dtr["ColumnType"].ToString() == "dropdown" && (dtr["DropDownType"].ToString() == "table" || dtr["DropDownType"].ToString() == "tabledd") &&
                    dtr["TableTableID"] != DBNull.Value && dtr["DisplayColumn"].ToString() != "" /*RP Added Ticket 4310*//*&& (Boolean)dtr["ShowAllValuesOnSearch"] == false*//*End Modification*/)
                {
                    string strDDLTextSearch = "";
                    if (ViewState["strKeepTextSearch"] != null)
                        strDDLTextSearch = ViewState["strKeepTextSearch"].ToString();

                    string strTextSearchParent = "";
                    if (TextSearchParent == null)
                    {
                        strTextSearchParent = "";
                    }
                    else
                    {
                        strTextSearchParent = TextSearchParent.Replace("'", "") + strDDLTextSearch;
                        strDDLTextSearch = "";
                    }

                    if (PageType == "p")
                    {
                        if (ViewState["strReturnSQL"] != null)
                        {
                            string sdsdaads = ViewState["strReturnSQL"].ToString();
                            strTextSearchParent = strTextSearchParent + strDDLTextSearch;


                            strTextSearchParent = " AND Record.RecordID IN (" + ViewState["strReturnSQL"].ToString().Replace("SET DATEFORMAT dmy;", "").Replace("SELECT *", "SELECT [DBGSystemRecordID]") + ")";

                            strDDLTextSearch = "";
                        }

                    }

                    DropDownList ddlParentSearch = (DropDownList)tblSearchControls.FindControl(_strDynamictabPart + "ddlParentSearch_" + dtr["SystemName"].ToString());
                    if (ddlParentSearch.Items.Count > 0)
                    {

                        int iRecordsInGridView = 0;
                        if (ViewState["iTN"] != null)
                            iRecordsInGridView = int.Parse(ViewState["iTN"].ToString());

                        if (iRecordsInGridView > 0)
                        {
                            RecordManager.PopulateTableDropDown(int.Parse(dtr["ColumnID"].ToString()), ref ddlParentSearch, strDDLTextSearch, strTextSearchParent, (Boolean)dtr["ShowAllValuesOnSearch"]);//strTextSearchParent); //3611
                        }
                        else
                        {
                            ddlParentSearch.Items.Clear();
                            string strPlaceHolderLoad = @" 
                            $('#" + _strDynamictabPart + "multipleSelect_" + dtr["SystemName"].ToString() + @"').parent().text("" -- No valid records --"");";
                            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "thePlaceHoldLoading" + _strDynamictabPart + dtr["SystemName"].ToString(), strPlaceHolderLoad, true);
                        }

                    }
                    if (ddlParentSearch.Items.Count > 0)
                    {
                        ddlParentSearch.Items.RemoveAt(0);
                    }
                }
            }

        }

        

    }
  

    protected string GetSearchCriteriaIDForCBC(Pages_UserControl_ControlByColumn cbcX)
    {
        string strXML = "";
        if (cbcX.ddlYAxisV != "")
        {
            //3611

            strXML = @"<root>" +
                  " <ddlYAxisV>" + HttpUtility.HtmlEncode(cbcX.ddlYAxisV) + "</ddlYAxisV>" +
                  " <txtUpperLimitV>" + HttpUtility.HtmlEncode(cbcX.txtUpperLimitV) + "</txtUpperLimitV>" +
                  " <txtLowerLimitV>" + HttpUtility.HtmlEncode(cbcX.txtLowerLimitV) + "</txtLowerLimitV>" +
                  " <hfTextSearchV>" + HttpUtility.HtmlEncode(cbcX.hfTextSearchV) + "</hfTextSearchV>" +
                  " <txtLowerDateV>" + HttpUtility.HtmlEncode(cbcX.txtLowerDateV) + "</txtLowerDateV>" +
                  " <txtUpperDateV>" + HttpUtility.HtmlEncode(cbcX.txtUpperDateV) + "</txtUpperDateV>" +
                  " <ddlDropdownColumnSearchV>" + HttpUtility.HtmlEncode(cbcX.ddlDropdownColumnSearchV) + "</ddlDropdownColumnSearchV>" +
                     " <hfControlB_DDL>" + HttpUtility.HtmlEncode(hfControlB_DDL.Value) + "</hfControlB_DDL>" + //3611
                   " <hfDDLcbcSearch1>" + HttpUtility.HtmlEncode(hfDDLcbcSearch1.Value) + "</hfDDLcbcSearch1>" + //3611
                   " <hfDDLcbcSearch2>" + HttpUtility.HtmlEncode(hfDDLcbcSearch2.Value) + "</hfDDLcbcSearch2>" + //3611
                   " <hfDDLcbcSearch3>" + HttpUtility.HtmlEncode(hfDDLcbcSearch3.Value) + "</hfDDLcbcSearch3>" + //3611
                     " <ColIDControlB_DDL>" + HttpUtility.HtmlEncode(cbcSearchMain.ddlYAxisV) + "</ColIDControlB_DDL>" + //3611
                   " <ColIDDDLcbcSearch1>" + HttpUtility.HtmlEncode(cbcSearch1.ddlYAxisV) + "</ColIDDDLcbcSearch1>" + //3611
                   " <ColIDDDLcbcSearch2>" + HttpUtility.HtmlEncode(cbcSearch2.ddlYAxisV) + "</ColIDDDLcbcSearch2>" + //3611
                   " <ColIDDDLcbcSearch3>" + HttpUtility.HtmlEncode(cbcSearch3.ddlYAxisV) + "</ColIDDDLcbcSearch3>" + //3611
                  " <txtSearchTextV>" + HttpUtility.HtmlEncode(cbcX.txtSearchTextV) + "</txtSearchTextV>" +
                   " <CompareOperator>" + HttpUtility.HtmlEncode(cbcX.CompareOperator) + "</CompareOperator>" +
                 "</root>";



        }

        return strXML;
    }


    //protected int GetSearchCriteriaIDForCBC(Pages_UserControl_ControlByColumn cbcX)
    //{
    //    if (cbcX.ddlYAxisV != "")
    //    {
    //        string xml = null;
    //        xml = @"<root>" +
    //               " <ddlYAxisV>" + HttpUtility.HtmlEncode(cbcX.ddlYAxisV) + "</ddlYAxisV>" +
    //               " <txtUpperLimitV>" + HttpUtility.HtmlEncode(cbcX.txtUpperLimitV) + "</txtUpperLimitV>" +
    //               " <txtLowerLimitV>" + HttpUtility.HtmlEncode(cbcX.txtLowerLimitV) + "</txtLowerLimitV>" +
    //               " <hfTextSearchV>" + HttpUtility.HtmlEncode(cbcX.hfTextSearchV) + "</hfTextSearchV>" +
    //               " <txtLowerDateV>" + HttpUtility.HtmlEncode(cbcX.txtLowerDateV) + "</txtLowerDateV>" +
    //               " <txtUpperDateV>" + HttpUtility.HtmlEncode(cbcX.txtUpperDateV) + "</txtUpperDateV>" +
    //               " <ddlDropdownColumnSearchV>" + HttpUtility.HtmlEncode(cbcX.ddlDropdownColumnSearchV) + "</ddlDropdownColumnSearchV>" +
    //               " <txtSearchTextV>" + HttpUtility.HtmlEncode(cbcX.txtSearchTextV) + "</txtSearchTextV>" +
    //                " <CompareOperator>" + HttpUtility.HtmlEncode(cbcX.CompareOperator) + "</CompareOperator>" +
    //              "</root>";

    //        SearchCriteria theSearchCriteria = new SearchCriteria(null, xml);
    //        return SystemData.SearchCriteria_Insert(theSearchCriteria);
    //    }

    //    return -1;
    //}
    protected void gvTheGrid_PreRender(object sender, EventArgs e)
    {
        GridView grid = (GridView)sender;
        if (grid != null)
        {
            GridViewRow pagerRow = (GridViewRow)grid.TopPagerRow;
            if (pagerRow != null)
            {
                pagerRow.Visible = true;
            }
        }
        //gvTheGrid.Columns[10].Visible = false;

    }



    protected void gvTheGrid_DataBound(object sender, EventArgs e)
    {

        //if (Session["RunSpeedLog"] != null && _theTable != null)
        //{

        //    SpeedLog theSpeedLog = new SpeedLog();
        //    theSpeedLog.FunctionName = _theTable.TableName + " Databound Event - START ";
        //    theSpeedLog.FunctionLineNumber = 3058;
        //    SecurityManager.AddSpeedLog(theSpeedLog);

        //}




        if (_bIsForExport)
        {


            //if (gvTheGrid.Rows.Count > 0)
            //{

            //    for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
            //    {
            //        for (int j = 0; j < _dtDataSource.Columns.Count; j++)
            //        {
            //            if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == _dtDataSource.Columns[j].ColumnName)
            //            {
            //                if (_dtRecordColums.Rows[i]["ShowTotal"].ToString().ToLower() == "true")
            //                {

            //                    double amount = 0;

            //                    foreach (GridViewRow row in gvTheGrid.Rows)
            //                    {
            //                        if (row.Cells[i + 3].Text != "")
            //                        {
            //                            try
            //                            {
            //                                amount += double.Parse(row.Cells[i + 3].Text, System.Globalization.NumberStyles.Any);
            //                            }
            //                            catch (Exception ex)
            //                            {
            //                                //
            //                            }
            //                        }
            //                    }
            //                    //update footer
            //                    gvTheGrid.FooterRow.Cells[i + 3].Text = string.Format("{0:N2}", amount);

            //                }
            //            }

            //        }

            //    }

            //}


        }
        else
        {
            if (gvTheGrid.Rows.Count > 0)
            {                          
               
                int m = 0;
                if (chkShowOnlyWarning.Checked)
                {
                    m = 4;//5;
                }
                else
                {
                    m = 3;// 4;
                }

                //JA 13 DEC 2016
                if (chkShowDeletedRecords.Checked)
                {
                    m = m + 2;
                }

                bool iFirstTotal = false;

                //for (int j = 0; j < _dtDataSource.Rows.Count; j++)
                //{
                //    if (_dtDataSource.Rows[j]["SystemName"].ToString() == "RecordID")
                //   {
                //       m = m + 1;
                //       break;
                //   }
                //}

                for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                {
                    for (int j = 0; j < _dtDataSource.Columns.Count; j++)
                    {
                        if (_dtRecordColums.Rows[i]["Heading"].ToString() == _dtDataSource.Columns[j].ColumnName)
                        {
                            if (_dtRecordColums.Rows[i]["SystemName"].ToString() == "RecordID")
                            {
                                m = m + 1;
                                break;
                            }
                        }
                    }
                }

                for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                {
                    for (int j = 0; j < _dtDataSource.Columns.Count; j++)
                    {
                        if (_dtRecordColums.Rows[i]["Heading"].ToString() == _dtDataSource.Columns[j].ColumnName)
                        {



                            if (_dtRecordColums.Rows[i]["ShowTotal"].ToString().ToLower() == "true" &&
                                (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "number"
                                || _dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "calculation"))
                            {

                                double amount = 0;

                                //Red 16 May 2017
                                _dtViewTotal = RecordManager.ets_Record_List_ViewTotal;

                                foreach (DataRow dr in _dtViewTotal.Rows)
                                {
                                    if (dr["ColumnID"].ToString() == _dtRecordColums.Rows[i]["ColumnID"].ToString())
                                    {
                                        amount = double.Parse(Common.IgnoreSymbols(dr["Total"].ToString()), System.Globalization.NumberStyles.Any);
                                    }
                                }
                                //end Red

                                //Red Removed 16 May 2017
                                //foreach (GridViewRow row in gvTheGrid.Rows)
                                //{
                                //    //JA 20 DEC 2016
                                //    //if (row.Cells[i + m].Text != "" && row.Cells[i + m].Text != "&nbsp;")
                                //    if (row.Cells[i + m + 1].Text != "" && row.Cells[i + m + 1].Text != "&nbsp;")
                                //    {
                                //        try
                                //        {

                                //            //amount += double.Parse(Common.IgnoreSymbols(row.Cells[i + m].Text), System.Globalization.NumberStyles.Any);
                                //            //JA 20 DEC 2016
                                //            //amount += double.Parse(Common.IgnoreSymbols(row.Cells[i + m].Text), System.Globalization.NumberStyles.Any);
                                //            amount += double.Parse(Common.IgnoreSymbols(row.Cells[i + m + 1].Text), System.Globalization.NumberStyles.Any);

                                //        }
                                //        catch (Exception ex)
                                //        {
                                //            //
                                //        }
                                //    }
                                //}
                                //end Red

                                //update footer
                                if (chkShowDeletedRecords.Checked == true)
                                {
                                    gvTheGrid.FooterRow.Cells[i + m + 1].Text = string.Format("{0:N2}", amount);
                                }
                                else
                                {
                                    gvTheGrid.FooterRow.Cells[i + m].Text = string.Format("{0:N2}", amount);
                                }

                                double dGamount = 0;
                                bool bGotGrandTotal = false;

                                if (iFirstTotal == false)
                                {
                                    if (chkShowDeletedRecords.Checked == true)
                                    {
                                        gvTheGrid.FooterRow.Cells[i + m].Text = "Total";// +"</br>" + "Grand Total";
                                    }
                                    else
                                    {
                                        gvTheGrid.FooterRow.Cells[i + m - 1].Text = "Total";// +"</br>" + "Grand Total";
                                    }

                                    iFirstTotal = true;
                                }



                                //if (_dtRecordColums.Rows[i]["Alignment"] == DBNull.Value)
                                //{
                                //    if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() != "number")
                                //    {
                                //        gvTheGrid.FooterRow.Cells[i + m].HorizontalAlign = HorizontalAlign.Left;
                                //    }
                                //    else
                                //    {
                                //        gvTheGrid.FooterRow.Cells[i + m].HorizontalAlign = HorizontalAlign.Right;
                                //    }

                                //    if (_dtRecordColums.Rows[i]["SystemName"].ToString() == "RecordID")
                                //    {
                                //        gvTheGrid.FooterRow.Cells[i + m].HorizontalAlign = HorizontalAlign.Right;
                                //    }

                                //    if (_dtRecordColums.Rows[i]["SystemName"].ToString() == "IsActive"
                                //   || _dtRecordColums.Rows[i]["SystemName"].ToString() == "DateTimeRecorded"
                                //        || _dtRecordColums.Rows[i]["SystemName"].ToString() == "EnteredBy")
                                //    {

                                //        gvTheGrid.FooterRow.Cells[i + m].HorizontalAlign = HorizontalAlign.Center;
                                //    }

                                //    if (_dtRecordColums.Rows[i]["SystemName"].ToString() == "Notes")
                                //    {
                                //        gvTheGrid.FooterRow.Cells[i + m].HorizontalAlign = HorizontalAlign.Left;
                                //    }

                                //}
                                //else
                                //{
                                gvTheGrid.FooterRow.Cells[i + m].HorizontalAlign = GetHorizontalAlign(_dtRecordColums.Rows[i]["Alignment"].ToString(), _dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower());
                                //}


                                //if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "number"
                                //   && _dtRecordColums.Rows[i]["NumberType"] != DBNull.Value)
                                //{
                                //    if (_dtRecordColums.Rows[i]["NumberType"].ToString() == "6")
                                //    {                                      

                                //            if (_dtRecordColums.Rows[i]["TextType"].ToString() != ""
                                //                && gvTheGrid.FooterRow.Cells[i + m].Text != "" && gvTheGrid.FooterRow.Cells[i + m].Text.ToString() != "&nbsp;")
                                //            {

                                //                //gvTheGrid.FooterRow.Cells[i + m].Text = _dtRecordColums.Rows[i]["TextType"].ToString()
                                //                //    + gvTheGrid.FooterRow.Cells[i + m].Text;

                                //                try
                                //                {
                                //                    string strMoney=gvTheGrid.FooterRow.Cells[i + m].Text;
                                //                    strMoney = Common.IgnoreSymbols(strMoney);
                                //                    gvTheGrid.FooterRow.Cells[i + m].Text = _dtRecordColums.Rows[i]["TextType"].ToString()
                                //                   + double.Parse(strMoney).ToString("C").Substring(1);



                                //                    //double dGamount = 0;
                                //                    //get the grand total

                                //                    //if (chkShowAdvancedOptions.Checked && ddlUploadedBatch.SelectedValue != "")
                                //                    //{
                                //                    //    TextSearch = TextSearch + "  AND TempRecordID IN  (SELECT RecordID FROM TempRecord WHERE BatchID=" + ddlUploadedBatch.SelectedValue + ")";
                                //                    //}


                                //                    int iTN = 1;
                                //                    DataTable dtGrand = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
                                //                       ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
                                //                       !chkIsActive.Checked,
                                //                       chkShowOnlyWarning.Checked == false ? null : (bool?)true, null, null,
                                //                       "", "", null, null, ref iTN, ref _iTotalDynamicColumns, "view", _strNumericSearch, TextSearch + TextSearchParent,
                                //                      _dtDateFrom, _dtDateTo, "", "", _strViewName, _theView.ViewID);

                                //                    for (int x = 0; x < _dtRecordColums.Rows.Count; x++)
                                //                    {
                                //                        for (int y = 0; y < dtGrand.Columns.Count; y++)
                                //                        {
                                //                            if (_dtRecordColums.Rows[x]["Heading"].ToString() == dtGrand.Columns[y].ColumnName
                                //                                && _dtRecordColums.Rows[i]["Heading"].ToString() == _dtRecordColums.Rows[x]["Heading"].ToString())
                                //                            {
                                //                                if      (_dtRecordColums.Rows[x]["ColumnType"].ToString().ToLower() == "number"
                                //                                    && _dtRecordColums.Rows[x]["NumberType"] != DBNull.Value)
                                //                                {


                                //                                    foreach (DataRow row in dtGrand.Rows)
                                //                                    {
                                //                                        try
                                //                                        {

                                //                                            dGamount += double.Parse(Common.IgnoreSymbols(row[dtGrand.Columns[y].ColumnName].ToString()),
                                //                                                System.Globalization.NumberStyles.Any);

                                //                                        }
                                //                                        catch (Exception ex)
                                //                                        {
                                //                                            //
                                //                                        }
                                //                                    }

                                //                                }

                                //                            }
                                //                        }

                                //                    }



                                //                        gvTheGrid.FooterRow.Cells[i + m].Text = gvTheGrid.FooterRow.Cells[i + m].Text +
                                //                            "</br>" + _dtRecordColums.Rows[i]["TextType"].ToString()
                                //                   + dGamount.ToString("C").Substring(1);

                                //                        bGotGrandTotal = true;


                                //                }
                                //                catch
                                //                {
                                //                    //

                                //                }
                                //            }

                                //    }
                                //}


                                //if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "calculation"
                                //   && _dtRecordColums.Rows[i]["TextType"].ToString() == "f"
                                //    && _dtRecordColums.Rows[i]["RegEx"].ToString() != "")
                                //{

                                //        if (gvTheGrid.FooterRow.Cells[i + m].Text != "" && gvTheGrid.FooterRow.Cells[i + m].Text.ToString() != "&nbsp;")
                                //        {
                                //            //gvTheGrid.FooterRow.Cells[i + m].Text = _dtRecordColums.Rows[i]["TextType"].ToString()
                                //            //    + gvTheGrid.FooterRow.Cells[i + m].Text;

                                //            try
                                //            {
                                //                string strMoney = gvTheGrid.FooterRow.Cells[i + m].Text;
                                //                strMoney = Common.IgnoreSymbols(strMoney);
                                //                gvTheGrid.FooterRow.Cells[i + m].Text = _dtRecordColums.Rows[i]["RegEx"].ToString()
                                //               + double.Parse(strMoney).ToString("C").Substring(1);

                                //                //double dGamount = 0;
                                //                //get the grand total

                                //                //if (chkShowAdvancedOptions.Checked && ddlUploadedBatch.SelectedValue != "")
                                //                //{
                                //                //    TextSearch = TextSearch + "  AND TempRecordID IN  (SELECT RecordID FROM TempRecord WHERE BatchID=" + ddlUploadedBatch.SelectedValue + ")";
                                //                //}

                                //                int iTN = 1;
                                //               DataTable dtGrand = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
                                //                  ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
                                //                  !chkIsActive.Checked,
                                //                  chkShowOnlyWarning.Checked == false ? null : (bool?)true, null, null,
                                //                  "", "", null, null, ref iTN, ref _iTotalDynamicColumns, "view", _strNumericSearch, TextSearch + TextSearchParent,
                                //                 _dtDateFrom, _dtDateTo, "", "", _strViewName, _theView.ViewID);

                                //               for (int x = 0; x < _dtRecordColums.Rows.Count; x++)
                                //               {
                                //                   for (int y = 0; y < dtGrand.Columns.Count; y++)
                                //                   {
                                //                       if (_dtRecordColums.Rows[x]["Heading"].ToString() == dtGrand.Columns[y].ColumnName
                                //                            && _dtRecordColums.Rows[i]["Heading"].ToString() == _dtRecordColums.Rows[x]["Heading"].ToString())
                                //                       {
                                //                           if (_dtRecordColums.Rows[x]["ShowTotal"].ToString().ToLower() == "true" &&
                                //                               (_dtRecordColums.Rows[x]["ColumnType"].ToString().ToLower() == "number"
                                //                               || _dtRecordColums.Rows[x]["ColumnType"].ToString().ToLower() == "calculation"))
                                //                           {


                                //                               foreach (DataRow row in dtGrand.Rows)
                                //                               {
                                //                                   try
                                //                                   {

                                //                                       //dGamount += double.Parse(Common.IgnoreSymbols(row[dtGrand.Columns[y].ColumnName].ToString()),
                                //                                       //    System.Globalization.NumberStyles.Any);

                                //                                       string strValue = "";

                                //                                       string strFormula = TheDatabaseS.GetCalculationFormula(int.Parse(_qsTableID), _dtRecordColums.Rows[x]["Calculation"].ToString());

                                //                                       //strValue = Common.GetValueFromSQL("SELECT " + strFormula + " FROM Record WHERE RecordID=" + row["DBGSystemRecordID"].ToString());
                                //                                       strValue = TheDatabaseS.GetCalculationResult(_dtColumnsAll, strFormula, int.Parse(row["DBGSystemRecordID"].ToString()), x, _iParentRecordID);
                                //                                       dGamount += double.Parse(Common.IgnoreSymbols(strValue),
                                //                                           System.Globalization.NumberStyles.Any);

                                //                                   }
                                //                                   catch (Exception ex)
                                //                                   {
                                //                                       //
                                //                                   }
                                //                               }

                                //                           }

                                //                       }
                                //                   }

                                //               }



                                //                   gvTheGrid.FooterRow.Cells[i + m].Text = gvTheGrid.FooterRow.Cells[i + m].Text +
                                //                       "</br>" + _dtRecordColums.Rows[i]["RegEx"].ToString()
                                //              + dGamount.ToString("C").Substring(1);

                                //                   bGotGrandTotal = true;
                                //            }
                                //            catch
                                //            {
                                //                //

                                //            }
                                //        }                                    
                                //}

                                //if (bGotGrandTotal == false && amount>=0)
                                //{
                                //    //
                                //    if (gvTheGrid.FooterRow.Cells[i + m].Text != "" && gvTheGrid.FooterRow.Cells[i + m].Text.ToString() != "&nbsp;")
                                //    {
                                //        //gvTheGrid.FooterRow.Cells[i + m].Text = _dtRecordColums.Rows[i]["TextType"].ToString()
                                //        //    + gvTheGrid.FooterRow.Cells[i + m].Text;

                                //        try
                                //        {

                                //            //if (chkShowAdvancedOptions.Checked && ddlUploadedBatch.SelectedValue != "")
                                //            //{
                                //            //    TextSearch = TextSearch + "  AND TempRecordID IN  (SELECT RecordID FROM TempRecord WHERE BatchID=" + ddlUploadedBatch.SelectedValue + ")";
                                //            //}

                                //            int iTN = 1;
                                //            DataTable dtGrand = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
                                //               ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
                                //               !chkIsActive.Checked,
                                //               chkShowOnlyWarning.Checked == false ? null : (bool?)true, null, null,
                                //               "", "", null, null, ref iTN, ref _iTotalDynamicColumns, "view", _strNumericSearch, TextSearch + TextSearchParent,
                                //              _dtDateFrom, _dtDateTo, "", "", _strViewName, _theView.ViewID);

                                //            for (int x = 0; x < _dtRecordColums.Rows.Count; x++)
                                //            {
                                //                for (int y = 0; y < dtGrand.Columns.Count; y++)
                                //                {
                                //                    if (_dtRecordColums.Rows[x]["Heading"].ToString() == dtGrand.Columns[y].ColumnName
                                //                         && _dtRecordColums.Rows[i]["Heading"].ToString() == _dtRecordColums.Rows[x]["Heading"].ToString())
                                //                    {
                                //                        if (_dtRecordColums.Rows[x]["ShowTotal"].ToString().ToLower() == "true" &&
                                //                            (_dtRecordColums.Rows[x]["ColumnType"].ToString().ToLower() == "number"))
                                //                        {


                                //                            foreach (DataRow row in dtGrand.Rows)
                                //                            {
                                //                                try
                                //                                {
                                //                                    if (row[dtGrand.Columns[y].ColumnName].ToString().Trim() != "")
                                //                                    {
                                //                                        dGamount += double.Parse(Common.IgnoreSymbols(row[dtGrand.Columns[y].ColumnName].ToString()),
                                //                                           System.Globalization.NumberStyles.Any);
                                //                                    }



                                //                                }
                                //                                catch (Exception ex)
                                //                                {
                                //                                    //
                                //                                }
                                //                            }

                                //                        }

                                //                        if (_dtRecordColums.Rows[x]["ShowTotal"].ToString().ToLower() == "true" &&
                                //                            (_dtRecordColums.Rows[x]["ColumnType"].ToString().ToLower() == "calculation"))
                                //                        {


                                //                            foreach (DataRow row in dtGrand.Rows)
                                //                            {
                                //                                try
                                //                                {


                                //                                    string strValue = "";

                                //                                    string strFormula = TheDatabaseS.GetCalculationFormula(int.Parse(_qsTableID), _dtRecordColums.Rows[x]["Calculation"].ToString());

                                //                                    //strValue = Common.GetValueFromSQL("SELECT " + strFormula + " FROM Record WHERE RecordID=" + row["DBGSystemRecordID"].ToString());
                                //                                    strValue = TheDatabaseS.GetCalculationResult(_dtColumnsAll, strFormula, int.Parse(row["DBGSystemRecordID"].ToString()), x, _iParentRecordID);
                                //                                    dGamount += double.Parse(Common.IgnoreSymbols(strValue),
                                //                                        System.Globalization.NumberStyles.Any);

                                //                                }
                                //                                catch (Exception ex)
                                //                                {
                                //                                    //
                                //                                }
                                //                            }

                                //                        }

                                //                    }
                                //                }

                                //            }



                                //            gvTheGrid.FooterRow.Cells[i + m].Text = gvTheGrid.FooterRow.Cells[i + m].Text +
                                //                "</br>" 
                                //       + dGamount.ToString("C").Substring(1);

                                //            bGotGrandTotal = true;
                                //        }
                                //        catch
                                //        {
                                //            //

                                //        }
                                //    }   

                                //}


                            }
                        }

                    }

                }



            }
        }
        if (_bHaveService)
        {
            ProcessServiceForEvent("RecordList_GridView_DataBound", sender, e);
        }

        //if (HttpContext.Current.Session["RunSpeedLog"] != null && _theTable != null)
        //{
        //    SpeedLog theSpeedLog = new SpeedLog();
        //    theSpeedLog.FunctionName = _theTable.TableName + " Databound Event - End ";
        //    theSpeedLog.FunctionLineNumber = 1950;
        //    SecurityManager.AddSpeedLog(theSpeedLog);

        //}

    }

    protected string getSystemNameValue(string recordId, string url)
    {
        bool IsLinked;
        string recordIdDestination;
        string valueDestination;
        string valueSource;


        foreach (DataRow dr in _dtRecordColums.Rows)
        {
            IsLinked = false;
            recordIdDestination = "";
            valueDestination = "";
            valueSource = "";

            if (url.IndexOf("[" + dr["SystemName"].ToString() + "]") > -1)
            {

                if (dr["ColumnType"].ToString() == "dropdown" && dr["TableTableID"] != DBNull.Value)
                {
                    IsLinked = true;
                }

                if (!IsLinked)
                {
                    valueSource = Common.GetValueFromSQL("SELECT " +
                       dr["SystemName"].ToString() +
                       " FROM Record WHERE RecordID = " + recordId.ToString());

                    url = url.Replace("[" + dr["SystemName"].ToString() + "]", valueSource);
                }
                else
                {
                    recordIdDestination = Common.GetValueFromSQL("SELECT " +
                       dr["SystemName"].ToString() +
                       " FROM Record WHERE RecordID = " + recordId.ToString());
                }
            }
           
            if (!string.IsNullOrEmpty(recordIdDestination) && IsLinked)
            {
                url = url.Replace("[" + dr["SystemName"].ToString() + "]", recordIdDestination);
                if (url.IndexOf("[" + dr["TableTableID"].ToString() + "]") > -1)
                {
                    DataTable dtColumnsDestination = Common.DataTableFromText("SELECT SystemName FROM [Column] WHERE TableID = " + dr["TableTableID"].ToString());
                    foreach (DataRow drx in dtColumnsDestination.Rows)
                    {
                        if (url.IndexOf(drx["SystemName"].ToString() + "-" + recordIdDestination) > -1)
                        {
                            valueDestination = Common.GetValueFromSQL("SELECT " +
                               drx["SystemName"].ToString() +
                               " FROM Record WHERE RecordID = " + recordIdDestination.ToString());
                            break;
                        }
                    }
                    url = url.Replace(recordIdDestination, valueDestination);
                }
            }
        }
        
        return url;
    }

    protected void gvTheGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    try
        //    {
        //        List<object> objList = new List<object>();
        //        List<object> roList = CustomMethod.DotNetMethod("windtech_divfooter_" + _qsTableID, objList);
        //        if (roList != null)
        //        {
        //            CheckBox cbgvTheGrid = (CheckBox)e.Row.FindControl("chkDelete");
        //            string js = string.Format("javascript:gvTheGrid_OnRowChecked();");

        //            cbgvTheGrid.Attributes["onclick"] = js;

        //        }
        //    }
        //    catch
        //    {

        //    }
        //}


        //if (HttpContext.Current.Session["RunSpeedLog"] != null && _theTable != null)
        //{

        //    SpeedLog theSpeedLog = new SpeedLog();
        //    theSpeedLog.FunctionName = _theTable.TableName + " " + e.Row.RowType.ToString() + " Event - RowDataBound - START ";
        //    theSpeedLog.FunctionLineNumber = 3259;
        //    SecurityManager.AddSpeedLog(theSpeedLog);

        //}




        if (e.Row.RowType == DataControlRowType.DataRow)
        {


           

            e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
            e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");

            //JA 19 DEC 2016            
            if (chkShowDeletedRecords.Checked == false)
            {
                e.Row.Cells[4].Visible = false;
                gvTheGrid.HeaderRow.Cells[4].Visible = false;
            }
        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[_iTotalDynamicColumns + 3].Visible = false;
            e.Row.Cells[_iTotalDynamicColumns + 2].Visible = false;
            //if (_bDBGSortColumnHide)
            //    e.Row.Cells[_iTotalDynamicColumns + 1].Visible = false;

           


            for (int j = 0; j < _dtDataSource.Columns.Count; j++)
            {
                if (_dtDataSource.Columns[j].ColumnName.IndexOf("_ID**") > -1
                    || _dtDataSource.Columns[j].ColumnName.IndexOf("_Colour**") > -1)
                {
                    e.Row.Cells[j + 4].Visible = false;
                }
            }

            for (int j = 0; j < _dtDataSource.Columns.Count; j++)
            {
                if (_dtDataSource.Columns[j].ColumnName == "DBGSystemRecordID")
                {
                    e.Row.Cells[j + 4].Visible = false;
                }
            }

            if (DisplayInColumnID != null && Request.Path.IndexOf("Record/RecordDetail.aspx") > -1 
                  && Request.QueryString["mode"] != null && Cryptography.Decrypt(Request.QueryString["mode"].ToString()) == "add")
            {
                //_gvPager.HideAdd = true;
                e.Row.Visible = false;
            }




        }

        if (e.Row.RowType == DataControlRowType.Header)
        {

            /* == Red 12092019: Ticket 4856 == */
            Label lbl_ = (Label)e.Row.FindControl("lbl_");
            HyperLink hlEditViewHeader = (HyperLink)e.Row.FindControl("hlEditViewHeader");
            HyperLink hlAddRecordHeader = (HyperLink)e.Row.FindControl("hlAddRecordHeader");
            LinkButton DeleteLinkButtonHeader = (LinkButton)e.Row.FindControl("DeleteLinkButtonHeader");
            CheckBox chkAll = (CheckBox)e.Row.FindControl("chkAll");
            if (_theView != null)
            {
                if (_theView.ShowHeader == false)
                {
                    if (hlEditViewHeader != null)
                    {
                        lbl_.Visible = false;
                        hlEditViewHeader.Visible = true;
                        hlEditViewHeader.NavigateUrl = hlEditView.NavigateUrl;
                        if (IsChildTable)
                        {
                                hlEditViewHeader.NavigateUrl = hlEditView.NavigateUrl + "&IsChildTable=yes";
                        }

                        hlEditViewHeader.CssClass = hlEditView.CssClass;

                        if (_strNoAjaxView != "")
                        {
                            hlEditViewHeader.Target = "_parent";
                        }

                    }


                    /* == Red 24092019: Ticket 4877 == */
                    chkAll.Visible = true;                   
                    bool bShowAdd = false;
                    bool bShowDelete = false;

                    if ((bool)_theView.ShowAddIcon == true)
                        bShowAdd = true;

                    if ((bool)_theView.ShowDeleteIcon == true)
                        bShowDelete = true;


                    if (_strRecordRightID == Common.UserRoleType.ReadOnly)
                    {
                        bShowAdd = false;
                        bShowDelete = false;

                    }
                    else if (_strRecordRightID == Common.UserRoleType.OwnData)
                    {
                        bShowDelete = false;
                    }
                    else if (_strRecordRightID == Common.UserRoleType.EditOwnViewOther)
                    {
                        bShowDelete = false;
                    }
                    else if (_strRecordRightID == Common.UserRoleType.AddRecord)
                    {
                        bShowDelete = false;
                    }

                    if (bShowAdd)
                    {
                        hlAddRecordHeader.Visible = true;
                        hlAddRecordHeader.NavigateUrl = GetAddURL();
                        chkAll.Visible = false;
                        e.Row.Cells[0].Width = 40;
                        gvNoCheckAll = true;
                    }

                    if (bShowDelete)
                    {
                        DeleteLinkButtonHeader.Visible = true;
                        chkAll.Visible = false;
                        e.Row.Cells[0].Width = 40;
                        gvNoCheckAll = true;
                    }

                    if (bShowAdd && bShowDelete)
                    {
                        e.Row.Cells[0].Width = 70;
                        gvNoCheckAll = true;
                    }

                    /* == End red == */

                    GridViewRow gvr = gvTheGrid.TopPagerRow;
                    if (gvr != null)
                    {
                        _gvPager = (Common_Pager)gvr.FindControl("Pager");
                        _gvPager.Visible = false;
                    }

                }
                else
                {
                    hlEditViewHeader.Visible = false;
                    lbl_.Visible = true;

                    hlAddRecordHeader.Visible = false;
                    DeleteLinkButtonHeader.Visible = false;
                    chkAll.Visible = true;

                }

                
                if (ShowAddButtonOnAdd == true && DisplayInColumnID != null && Request.Path.IndexOf("Record/RecordDetail.aspx") > -1 
                    && Request.QueryString["mode"] != null && Cryptography.Decrypt(Request.QueryString["mode"].ToString()) == "add")
                {
                    hlAddRecordHeader.Visible = true;
                    //hlAddRecordHeader.Attributes.Add("onclick", "$('#ctl00_HomeContentPlaceHolder_lnkSaveClose').trigger('click');");
                    //hlAddRecordHeader.NavigateUrl = "";
                    hlAddRecordHeader.NavigateUrl = "javascript:SaveClick(" + DisplayInColumnID.ToString() + ");";
                    //hlAddRecordHeader.NavigateUrl = "javascript:SaveClick(this);";
                    chkAll.Visible = false;
                    DeleteLinkButtonHeader.Visible = false;
                }
            }
            /* == End Red == */



            //Show Warning is checked
            if (chkShowOnlyWarning.Checked == true)
            {
                //Show warning and Show deleted records ids checked
                if (chkShowDeletedRecords.Checked)
                {
                    //JA 15 DEC 2016
                    e.Row.Cells[7].ForeColor = System.Drawing.Color.Blue;
                    e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                    e.Row.Cells[5].ForeColor = System.Drawing.Color.Red;
                    e.Row.Cells[4].ForeColor = System.Drawing.Color.Red;

                    if (_bDeleteReason == false)
                    {
                        e.Row.Cells[6].Visible = false;
                    }
                }
                else
                {
                    //e.Row.Cells[4].ForeColor = System.Drawing.Color.Blue;

                    //JA 19 DEC 2016
                    e.Row.Cells[5].ForeColor = System.Drawing.Color.Blue;
                }
            }
            else
            {
                if (chkShowDeletedRecords.Checked)
                {
                    //JA 13 DEC 2016
                    e.Row.Cells[5].ForeColor = System.Drawing.Color.Red;
                    e.Row.Cells[4].ForeColor = System.Drawing.Color.Red;

                    //JA 13 DEC 2016
                    e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;

                    if (_bDeleteReason == false)
                    {
                        e.Row.Cells[5].Visible = false;
                    }
                }
            }

            e.Row.Cells[_iTotalDynamicColumns + 3].Visible = false;
            e.Row.Cells[_iTotalDynamicColumns + 2].Visible = false;
            //if (_bDBGSortColumnHide)
            //    e.Row.Cells[_iTotalDynamicColumns + 1].Visible = false;

            for (int j = 0; j < _dtDataSource.Columns.Count; j++)
            {
                if (_dtDataSource.Columns[j].ColumnName == "DBGSystemRecordID")
                {
                    e.Row.Cells[j + 4].Visible = false;
                }
            }

            /* == Red 12092019: Disable all except others. This time only Edit View when pager is hidden*/
            for (int j = 0; j < _dtDataSource.Columns.Count; j++)
            {
                if(_bEmptyRecord)
                    e.Row.Cells[j+ 4].Enabled = false;
            }
            /* == End Red == */

            for (int j = 0; j < _dtDataSource.Columns.Count; j++)
            {
                if (_dtDataSource.Columns[j].ColumnName.IndexOf("_ID**") > -1
                    || _dtDataSource.Columns[j].ColumnName.IndexOf("_Colour**") > -1)
                {
                    e.Row.Cells[j + 4].Visible = false;
                }
            }

            for (int j = 0; j < _dtDataSource.Columns.Count; j++)
            {
                if ((e.Row.Cells[j + 4].Controls.Count > 0) && (e.Row.Cells[j + 4].Controls[0].GetType().BaseType.Name == "LinkButton"))
                {
                    string s = (e.Row.Cells[j + 4].Controls[0] as LinkButton).Text;
                    if (s.EndsWith("~"))
                        (e.Row.Cells[j + 4].Controls[0] as LinkButton).Text = s.Remove(s.Length - 1);
                }
            }

            //
            for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
            {
                for (int j = 0; j < _dtDataSource.Columns.Count; j++)
                {
                    if (_dtRecordColums.Rows[i]["Heading"].ToString() == _dtDataSource.Columns[j].ColumnName)
                    {
                        if (_bIsForExport == false)
                        {
                            if (_dtRecordColums.Rows[i]["SummaryCellBackColor"] != DBNull.Value)
                            {
                                if (_dtRecordColums.Rows[i]["SummaryCellBackColor"].ToString() != "")
                                {
                                    e.Row.Cells[j + 4].Style.Add("background-color", _dtRecordColums.Rows[i]["SummaryCellBackColor"].ToString());

                                }

                            }
                        }


                        if (_dtRecordColums.Rows[i]["Width"] != DBNull.Value)
                        {
                            //e.Row.Cells[j + 4].Width = int.Parse(_dtRecordColums.Rows[i]["Width"].ToString());
                            //RP Modified Ticket -4501    
                            if (_bResponsive)
                            {
                                e.Row.Cells[j + 4].Width = new Unit(_dtRecordColums.Rows[i]["Width"].ToString() + "%");
                            }
                            else
                            {
                                e.Row.Cells[j + 4].Width = int.Parse(_dtRecordColums.Rows[i]["Width"].ToString()); //This was the orignal line
                            }
                            //End Modification
                        }





                        //if (_dtRecordColums.Rows[i]["Alignment"] == DBNull.Value)
                        //{
                        //    if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() != "number")
                        //    {
                        //        e.Row.Cells[j + 4].HorizontalAlign = HorizontalAlign.Left;
                        //    }
                        //    else
                        //    {
                        //        e.Row.Cells[j + 4].HorizontalAlign = HorizontalAlign.Right;
                        //    }

                        //    if (_dtRecordColums.Rows[i]["SystemName"].ToString() == "RecordID")
                        //    {
                        //        e.Row.Cells[j + 4].HorizontalAlign = HorizontalAlign.Right;
                        //    }

                        //    if (_dtRecordColums.Rows[i]["SystemName"].ToString() == "IsActive"
                        //   || _dtRecordColums.Rows[i]["SystemName"].ToString() == "DateTimeRecorded"
                        //        || _dtRecordColums.Rows[i]["SystemName"].ToString() == "EnteredBy")
                        //    {

                        //        e.Row.Cells[j + 4].HorizontalAlign = HorizontalAlign.Center;
                        //    }

                        //    if (_dtRecordColums.Rows[i]["SystemName"].ToString() == "Notes")
                        //    {
                        //        e.Row.Cells[j + 4].HorizontalAlign = HorizontalAlign.Left;
                        //    }

                        //}
                        //else
                        //{
                        e.Row.Cells[j + 4].HorizontalAlign = GetHorizontalAlign(_dtRecordColums.Rows[i]["Alignment"].ToString(), _dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower());
                        //}




                        //Help Text

                        if (_dtRecordColums.Rows[i]["Notes"] != DBNull.Value)
                        {
                            if (_dtRecordColums.Rows[i]["Notes"].ToString() != "")
                            {
                                e.Row.Cells[j + 4].ToolTip = _dtRecordColums.Rows[i]["Notes"].ToString();
                            }

                        }



                        //diable sorting for table column

                        //if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
                        //        && (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
                        //        || _dtRecordColums.Rows[i]["DropDownType"].ToString() == "tabledd")
                        //         && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
                        //        && _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
                        //{

                        //    e.Row.Cells[j + 4].CssClass = "headerlink";
                        //    e.Row.Cells[j + 4].Enabled = false;

                        //}

                        if (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "value_text"
                                 && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown")
                        {

                            e.Row.Cells[j + 4].CssClass = "headerlink";
                            e.Row.Cells[j + 4].Enabled = false;

                        }

                        if (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "value_text"
                                && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "radiobutton")
                        {

                            e.Row.Cells[j + 4].CssClass = "headerlink";
                            e.Row.Cells[j + 4].Enabled = false;

                        }

                        //if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "calculation")
                        //{

                        //    e.Row.Cells[j + 4].CssClass = "headerlink";
                        //    e.Row.Cells[j + 4].Enabled = false;

                        //}

                //        if (_dtRecordColums.Rows[i]["OnlyForAdmin"] != DBNull.Value
                //             && _dtRecordColums.Rows[i]["OnlyForAdmin"].ToString().ToLower() != "0"
                //&& !Common.HaveAccess(_strRecordRightID, "1,2"))
                //        {

                //            bool bHide = false;
                //            if (_dtRecordColums.Rows[i]["OnlyForAdmin"].ToString().ToLower() == _theRole.RoleID.ToString())
                //            {
                //                if (_theRole.RoleType == "8")
                //                {
                //                    //
                //                }
                //            }
                //            else
                //            {
                //                bHide = true;
                //            }


                //            if (bHide)
                //            {
                //                e.Row.Cells[j + 4].Visible = false;
                //            }
                //        }

                    }

                }
            }


            /* == red 120902019: Transferred above the disabling... == */
            //if (_bEmptyRecord)
            //{
            //    e.Row.Enabled = false;
            //    hlEditViewHeader.Enabled = true;
            //}
            /* == End Red == */

            if (_bIsForExport)
            {
                for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                {
                    for (int j = 0; j < _dtDataSource.Columns.Count; j++)
                    {
                        if (_dtRecordColums.Rows[i]["Heading"].ToString() == _dtDataSource.Columns[j].ColumnName)
                        {
                            if (_dtRecordColums.Rows[i]["Heading"] == DBNull.Value)
                            {
                                e.Row.Cells[j + 4].Visible = false;

                                break;
                            }
                            else
                            {
                                e.Row.Cells[j + 4].Text = _dtRecordColums.Rows[i]["Heading"].ToString();

                                break;
                            }

                        }

                    }
                }
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            /* == Red 24092019: Ticket 4877, need to pass if there's no CheckAll == */
            CheckBox cbgvTheGrid = (CheckBox)e.Row.FindControl("chkDelete");
            string js = string.Format("javascript:Check_Click(this,'" + gvNoCheckAll.ToString() + "');");

            cbgvTheGrid.Attributes["onclick"] = js;
            /* == End Red == */


            Record theRecord = null;
            for (int j = 0; j < _dtDataSource.Columns.Count; j++)
            {
                if (_dtDataSource.Columns[j].ColumnName.IndexOf("_ID**") > -1
                    || _dtDataSource.Columns[j].ColumnName.IndexOf("_Colour**") > -1)
                {
                    e.Row.Cells[j + 4].Visible = false;
                }
            }


            DataRowView rowView = (DataRowView)e.Row.DataItem;
            string strDBGSystemRecordID = rowView["DBGSystemRecordID"].ToString();
            if (strDBGSystemRecordID == "" && _bEmptyRecord)
            {
                e.Row.Visible = false;
                return;
            }


            string strMode = "edit";

            string strURLEdit = GetEditURL() + Cryptography.Encrypt(strDBGSystemRecordID);
            string strURLView = GetViewURL() + Cryptography.Encrypt(strDBGSystemRecordID);
            string strURL = strURLEdit;
            System.Drawing.Color colWarningColor = System.Drawing.Color.Blue;

            if (_strRecordRightID == Common.UserRoleType.ReadOnly ||
                _strRecordRightID == Common.UserRoleType.AddRecord
                || ShowEditButton == false)
            {
                strURL = strURLView;
                strMode = "view";
            }

           

            HyperLink viewHyperLink = (HyperLink)e.Row.FindControl("viewHyperLink");
            HyperLink EditHyperLink = (HyperLink)e.Row.FindControl("EditHyperLink");

            if (EditHyperLink != null)
            {
                if (_bCustomDDL)
                {
                    EditHyperLink.ImageUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Pager/Images/rrp/edit_s.png";
                }
            }

            theRecord = RecordManager.ets_Record_Detail_Full(int.Parse(rowView["DBGSystemRecordID"].ToString()), int.Parse(_qsTableID), bUseArchiveData);  // Red 08012018

            if (_strRecordRightID == Common.UserRoleType.EditOwnViewOther
                || _strRecordRightID == Common.UserRoleType.OwnData)
            {
                if (theRecord != null)
                {
                    if (_ObjUser.UserID == theRecord.EnteredBy
                        || (theRecord.OwnerUserID != null && (_ObjUser.UserID == theRecord.OwnerUserID)))
                    {
                        strURL = strURLEdit;
                        if (viewHyperLink != null)
                            viewHyperLink.Visible = false;

                        if (EditHyperLink != null)
                            EditHyperLink.Visible = true;
                    }
                    else
                    {

                        strURL = strURLView;
                        if (EditHyperLink != null)
                            EditHyperLink.Visible = false;
                        if (viewHyperLink != null)
                            viewHyperLink.Visible = true;

                    }
                }
            }





            if (PageType == "c" || DisplayInColumnID!=null)
            {
                strURL = strURL + "&backurl=" + Cryptography.Encrypt(Request.RawUrl.ToString()) + "&onlyback=yes&tabindex=" + DetailTabIndex.ToString();
            }
            //e.Row.Attributes.Add("onClick", "window.open('" + strURL + "', '_self')");

            if (_theView != null)
            {
                if (strMode == "view")
                {
                    if ((bool)_theView.ShowViewIcon == false)
                        strURL = "#";
                }
                //if (strMode == "edit")
                //{
                //    if ((bool)_theView.ShowEditIcon == false)
                //        strURL = "#";
                //}
            }

            if (_bOpenInParent)
            {
                strURL = strURL + "&fixedurl=" + Cryptography.Encrypt("~/Default.aspx");
            }

            if (theRecord != null)
            {

                if ((theRecord.IsReadOnly != null
                    && (bool)theRecord.IsReadOnly) ||
                    /* (chkUseArchive.Checked || bUseArchiveData == true)) Red Ticket 4371 */ 
                    (bUseArchiveData == true))
                {
                    strURL = strURL.Replace("mode=" + Cryptography.Encrypt("edit"), "mode=" + Cryptography.Encrypt("view"));
                    if (viewHyperLink != null && EditHyperLink != null)
                        EditHyperLink.ImageUrl = viewHyperLink.ImageUrl;
                    EditHyperLink.ToolTip = "Read Only Record";

                    //red Ticket 3358  
                    /* (chkUseArchive.Checked || bUseArchiveData == true)) Red Ticket 4371 */
                    if (bUseArchiveData == true)
                    {
                        strURL = strURL + "&useArchiveData=yes";
                    }

                }
            }


            if (viewHyperLink != null)
            {
                viewHyperLink.NavigateUrl = strURL;

                if (_bOpenInParent)
                    viewHyperLink.Target = "_parent";
            }



            if (EditHyperLink != null)
            {
                EditHyperLink.NavigateUrl = strURL;
                if (_bOpenInParent)
                    EditHyperLink.Target = "_parent";
            }

            e.Row.Cells[_iTotalDynamicColumns + 3].Visible = false;
            e.Row.Cells[_iTotalDynamicColumns + 2].Visible = false;

            for (int j = 0; j < _dtDataSource.Columns.Count; j++)
            {
                if (_dtDataSource.Columns[j].ColumnName == "DBGSystemRecordID")
                {
                    e.Row.Cells[j + 4].Visible = false;
                }
            }




            //if (_bDBGSortColumnHide)
            //e.Row.Cells[_iTotalDynamicColumns + 1].Visible = false;

            if (chkShowOnlyWarning.Checked == true)
            {
                string strWarning = "";

                if (rowView.Row.Table.Columns.Contains("Warning"))
                {
                    if (rowView["Warning"] != DBNull.Value && rowView["Warning"].ToString() != "")
                    {
                        strWarning = rowView["Warning"].ToString();

                        if (strWarning.IndexOf("EXCEEDANCE:") > -1 && strWarning.IndexOf("WARNING:") > -1)
                        {
                            for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                            {
                                if (strWarning.IndexOf("EXCEEDANCE: " + _dtRecordColums.Rows[i]["DisplayName"].ToString()) > -1)
                                {
                                    strWarning = strWarning.Replace("EXCEEDANCE: " + _dtRecordColums.Rows[i]["DisplayName"].ToString() + " – Value outside accepted range.",
                                       "<span style='color:#ffa500;'>" + "EXCEEDANCE: " + _dtRecordColums.Rows[i]["DisplayName"].ToString() + " – Value outside accepted range." + "</span>");
                                }
                            }
                        }
                        else if (strWarning.IndexOf("EXCEEDANCE:") > -1)
                        {
                            /* == Red: Requested by Jon == */
                            //colWarningColor = System.Drawing.Color.Orange;
                            colWarningColor = System.Drawing.Color.Red;
                        }
                        else if (strWarning.IndexOf("INVALID (and ignored):") > -1)
                        {
                            colWarningColor = System.Drawing.Color.Red;
                        }
                    }
                }

                //JA 19 DEC 2016
                if (chkShowDeletedRecords.Checked)
                {


                    //e.Row.Cells[5].ForeColor = colWarningColor;
                    //e.Row.Cells[5].Text = strWarning;

                    //JA 19 DEC 2016
                    e.Row.Cells[7].ForeColor = colWarningColor;
                    e.Row.Cells[7].Text = strWarning;

                    if (_bDeleteReason == false)
                    {
                        e.Row.Cells[6].Visible = false;
                    }
                }
                else
                {
                    //e.Row.Cells[4].ForeColor = colWarningColor;
                    //e.Row.Cells[4].Text = strWarning;

                    //JA 19 DEC 2016
                    e.Row.Cells[5].ForeColor = colWarningColor;
                    e.Row.Cells[5].Text = strWarning;
                }
            }
            else
            {
                if (chkShowDeletedRecords.Checked)
                {

                    if (_bDeleteReason == false)
                    {
                        e.Row.Cells[5].Visible = false;
                    }
                }
            }




            if (_bIsForExport)
            {
                //export to pdf or word

                //Hide columns
                for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                {
                    for (int j = 0; j < _dtDataSource.Columns.Count; j++)
                    {
                        if (_dtRecordColums.Rows[i]["Heading"].ToString() == _dtDataSource.Columns[j].ColumnName)
                        {
                            if (_dtRecordColums.Rows[i]["Heading"] == DBNull.Value)
                            {
                                e.Row.Cells[j + 4].Visible = false;
                                break;
                            }
                            else
                            {

                                break;
                            }

                        }

                    }
                }

                //Round export
                for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                {
                    for (int j = 0; j < _dtDataSource.Columns.Count; j++)
                    {
                        //DisplayTextSummary
                        if (_dtRecordColums.Rows[i]["Heading"].ToString() == _dtDataSource.Columns[j].ColumnName)
                        {
                            if (IsStandard(_dtRecordColums.Rows[i]["SystemName"].ToString()) == false)
                            {

                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "file"
                                    || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "image")
                                {
                                    if (e.Row.Cells[j + 4].Text.ToString() != "" && e.Row.Cells[j + 4].Text.ToString() != "&nbsp;")
                                    {
                                        try
                                        {
                                            if (e.Row.Cells[j + 4].Text.ToString().Length > 37)
                                            {
                                                e.Row.Cells[j + 4].Text = e.Row.Cells[j + 4].Text.Substring(37);

                                            }
                                        }
                                        catch
                                        {
                                            //
                                        }
                                    }

                                }



                                if (_dtRecordColums.Rows[i]["IsRound"] != DBNull.Value)
                                {
                                    if (_dtRecordColums.Rows[i]["IsRound"].ToString().ToLower() == "true")
                                    {
                                        if (e.Row.Cells[j + 4].Text.ToString() != "" && e.Row.Cells[j + 4].Text.ToString() != "&nbsp;")
                                        {
                                            try
                                            {
                                                if (Common.HasSymbols(e.Row.Cells[j + 4].Text) == false)
                                                    e.Row.Cells[j + 4].Text = Math.Round(double.Parse(e.Row.Cells[j + 4].Text),
                                                   int.Parse(_dtRecordColums.Rows[i]["RoundNumber"].ToString()), MidpointRounding.AwayFromZero).ToString("N" + _dtRecordColums.Rows[i]["RoundNumber"].ToString());
                                            }
                                            catch
                                            {

                                            }

                                        }
                                    }
                                }
                            }

                        }

                        //hh:mm
                        if (_dtRecordColums.Rows[i]["SystemName"].ToString().ToLower() == "datetimerecorded")
                        {
                            if (_dtRecordColums.Rows[i]["Heading"].ToString() == _dtDataSource.Columns[j].ColumnName)
                            {
                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "datetime")
                                {
                                    if (e.Row.Cells[j + 4].Text.Length > 18)
                                    {
                                        e.Row.Cells[j + 4].Text = e.Row.Cells[j + 4].Text.Substring(0, 19);
                                    }
                                }
                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "date")
                                {
                                    if (e.Row.Cells[j + 4].Text.Length > 9)
                                    {
                                        e.Row.Cells[j + 4].Text = e.Row.Cells[j + 4].Text.Substring(0, 10);
                                    }
                                }
                            }
                        }


                    }
                }






            }
            else
            {
                //Not export


                //if (DataBinder.Eval(e.Row.DataItem, "DBGSystemRecordID") != DBNull.Value)
                //{
                //    strDBGSystemRecordID = DataBinder.Eval(e.Row.DataItem, "DBGSystemRecordID").ToString();
                //}

                /*
                 * Cell Colour cache
                 */
                if (_dictCellColourRules == null)
                {
                    CreateCellColourCache();
                }

                bool hasRecordID = false;
                int _recordId = -1;

                for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                {
                    for (int j = 0; j < _dtDataSource.Columns.Count; j++)
                    {

                        if (_dtRecordColums.Rows[i]["SystemName"].ToString() == "RecordID")
                        {
                            if (_dtRecordColums.Rows[i]["Heading"].ToString() == "" && _dtDataSource.Columns[j].ColumnName == "DBGSystemRecordID")
                            {

                                //strRecordID = e.Row.Cells[j + 4].Text;
                            }

                        }


                        if (_dtRecordColums.Rows[i]["Heading"].ToString() == _dtDataSource.Columns[j].ColumnName)
                        {

                            if (_dtRecordColums.Rows[i]["SystemName"].ToString() == "RecordID")
                            {
                                e.Row.Cells[j + 4].HorizontalAlign = GetHorizontalAlign(_dtRecordColums.Rows[i]["Alignment"].ToString(), _dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower());
                                hasRecordID = true;
                            }



                            if (rowView[_dtRecordColums.Rows[i]["Heading"].ToString()] != DBNull.Value && rowView[_dtRecordColums.Rows[i]["Heading"].ToString()].ToString() != "")
                            {
                                string strValueAsString = rowView[_dtRecordColums.Rows[i]["Heading"].ToString()].ToString();

                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "text" && (_dtRecordColums.Rows[i]["TextType"] == DBNull.Value
                                                 || (_dtRecordColums.Rows[i]["TextType"] != DBNull.Value && _dtRecordColums.Rows[i]["TextType"].ToString() == "text")))
                                {
                                    string strDisplaText = Common.StripTagsCharArray(strValueAsString);
                                    if (strDisplaText.Length > _iMaxCharactersInCell)
                                    {
                                        strDisplaText = strDisplaText.Substring(0, _iMaxCharactersInCell) + "...";
                                        e.Row.Cells[j + 4].Text = "";

                                        string strCellContent = "<div class='js-tooltip-container'> <div class='js-tooltip' style='display:none;'><span>" + strValueAsString + "</span> </div>";
                                        strCellContent = strCellContent + "<span>" + strDisplaText + "</span></div>";

                                        e.Row.Cells[j + 4].Text = strCellContent;
                                    }
                                }

                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() != "file"
                                                        && _dtRecordColums.Rows[i]["ColumnType"].ToString() != "image")
                                {
                                    if ((_dtRecordColums.Rows[i]["ColumnType"].ToString() == "text"
                                            && _dtRecordColums.Rows[i]["TextType"].ToString() == "link"
                                        )
                                        || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "content"
                                        || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "staticcontent"
                                        ||
                                            (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
                                                && _dtRecordColums.Rows[i]["ShowViewLink"] != DBNull.Value
                                                && 
                                                (_dtRecordColums.Rows[i]["ShowViewLink"].ToString().ToLower() == "summary"
                                                    || _dtRecordColums.Rows[i]["ShowViewLink"].ToString().ToLower() == "both"
                                                )
                                            )
                                        )
                                    {

                                    }
                                    else
                                    {
                                        if (_bOpenInParent)
                                        {
                                            e.Row.Cells[j + 4].Attributes.Add("onClick", "window.open('" + strURL + "', '_parent')");
                                        }
                                        else
                                        {
                                            e.Row.Cells[j + 4].Attributes.Add("onClick", "window.open('" + strURL + "', '_self')");
                                        }
                                    }
                                }

                               
                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "text" && _dtRecordColums.Rows[i]["TextType"].ToString() == "link")
                                {
                                    string strLinkURL = strValueAsString;

                                    if (strLinkURL.IndexOf("http") == -1)
                                    {
                                        strLinkURL = Request.Url.Scheme + "://" + strLinkURL;
                                    }

                                    e.Row.Cells[j + 4].Text = "<a target='_blank' href='" + strLinkURL + "'>"
                                                        + e.Row.Cells[j + 4].Text + "</a>";
                                }

                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "content" || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "staticcontent")
                                {
                                    int addColumn = 4;
                                    
                                    if (hasRecordID)
                                    {
                                        addColumn = 5;
                                    }
                                    Label lblRecordID = (Label)e.Row.FindControl("LblID");
                                    if (lblRecordID != null)
                                    {
                                        _recordId = int.Parse(lblRecordID.Text.ToString());
                                    }
                                    string htmlUrl = getSystemNameValue(_recordId.ToString(), strValueAsString);

                                    e.Row.Cells[i + addColumn].Text = "<div>  " + htmlUrl + " </div>";
                                }

                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "text"
                                    && _dtRecordColums.Rows[i]["TextHeight"] != DBNull.Value && int.Parse(_dtRecordColums.Rows[i]["TextHeight"].ToString()) > 1)
                                {
                                    e.Row.Cells[j + 4].Text = e.Row.Cells[j + 4].Text.Replace("\n", "<br />");
                                }

                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "time")
                                {
                                    TimeSpan ts = new TimeSpan();
                                    if (TimeSpan.TryParse(strValueAsString, out ts))
                                    {
                                        if (_dtRecordColums.Rows[i]["HideTimeSecond"].ToString() != "" &&
                                            (bool)_dtRecordColums.Rows[i]["HideTimeSecond"])
                                            e.Row.Cells[j + 4].Text = ts.ToString(@"hh\:mm");
                                        else
                                            e.Row.Cells[j + 4].Text = ts.ToString(@"hh\:mm\:ss");
                                    }
                                }

                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "datetime")
                                {
                                    if (_dtRecordColums.Rows[i]["HideTimeSecond"].ToString() != "" &&
                                        (bool) _dtRecordColums.Rows[i]["HideTimeSecond"])
                                    {
                                        string[] parts = strValueAsString.Split(' ');
                                        if (parts.Length > 1)
                                        {
                                            TimeSpan ts = new TimeSpan();
                                            if (TimeSpan.TryParse(parts[1], out ts))
                                            {
                                                parts[1] = ts.ToString(@"hh\:mm");
                                            }

                                            e.Row.Cells[j + 4].Text = parts[0] + ' ' + parts[1];
                                        }
                                    }
                                }

                                if (IsStandard(_dtRecordColums.Rows[i]["SystemName"].ToString()) == false)
                                {
                                    if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "file" && strValueAsString.Length > 37)
                                    {
                                        try
                                        {
                                            string strFilePath = Cryptography.Encrypt(_strFilesLocation + "/UserFiles/AppFiles/" + strValueAsString);
                                            string strFileName = Cryptography.Encrypt(strValueAsString.Substring(37));
                                            string strFileURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Security/Filedownload.aspx?FilePath="
                            + strFilePath + "&FileName=" + strFileName;

                                            e.Row.Cells[j + 4].Text = "<a target='_blank' href='" + strFileURL + "'>"
                                                + strValueAsString.Substring(37) + "</a>";

                                        }
                                        catch
                                        {
                                            //
                                        }

                                    }//File



                                    if (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "value_image" && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "radiobutton"
                                  && _dtRecordColums.Rows[i]["DropdownValues"] != DBNull.Value && _dtRecordColums.Rows[i]["DropdownValues"].ToString() != ""
                                  && _dtRecordColums.Rows[i]["ImageOnSummary"] != DBNull.Value && _dtRecordColums.Rows[i]["ImageOnSummary"].ToString().ToLower() == "true")
                                    {
                                        string strText = Common.GetImageFromValueForDD(_dtRecordColums.Rows[i]["DropdownValues"].ToString(), strValueAsString, _strFilesLocation,
                                              _dtRecordColums.Rows[i]["TextHeight"] == DBNull.Value ? "" : _dtRecordColums.Rows[i]["TextHeight"].ToString());
                                        if (strText != "")
                                            e.Row.Cells[j + 4].Text = strText;
                                    }


                                    if (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "value_text" && (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
                                    || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "radiobutton")
                                   && _dtRecordColums.Rows[i]["DropdownValues"].ToString() != "")
                                    {
                                        string strText = GetTextFromValueForDD(_dtRecordColums.Rows[i]["DropdownValues"].ToString(), strValueAsString);
                                        if (strText != "")
                                            e.Row.Cells[j + 4].Text = strText;

                                    }


                                    if (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "value_text"
                                  && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "listbox"
                                 && _dtRecordColums.Rows[i]["DropdownValues"] != DBNull.Value && _dtRecordColums.Rows[i]["DropdownValues"].ToString() != "")
                                    {
                                        string strText = Common.GetTextFromValueForList(_dtRecordColums.Rows[i]["DropdownValues"].ToString(), strValueAsString);
                                        if (strText != "")
                                            e.Row.Cells[j + 4].Text = strText;

                                    }


                                    if (_dtRecordColums.Rows[i]["IsRound"] != DBNull.Value
                                       && (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "number"
                                       || _dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "calculation")
                                       && _dtRecordColums.Rows[i]["IsRound"].ToString().ToLower() == "true")
                                    {
                                        try
                                        {
                                            bool bDate = false;

                                            if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "calculation"
                                                && _dtRecordColums.Rows[i]["TextType"] != DBNull.Value
                                                    && _dtRecordColums.Rows[i]["TextType"].ToString().ToLower() == "d")
                                            {
                                                bDate = true;
                                            }

                                            if (bDate == false)
                                            {
                                                double dTempTest = 0;
                                                if (Common.HasSymbols(strValueAsString) == false)
                                                {
                                                    if (double.TryParse(Common.IgnoreSymbols(strValueAsString), out dTempTest))
                                                    {
                                                        strValueAsString = Math.Round(dTempTest,
                                                           int.Parse(_dtRecordColums.Rows[i]["RoundNumber"].ToString()), 
                                                           MidpointRounding.AwayFromZero).ToString("N" + _dtRecordColums.Rows[i]["RoundNumber"].ToString());
                                                        e.Row.Cells[j + 4].Text = strValueAsString;
                                                    }
                                                }


                                            }

                                        }
                                        catch
                                        {
                                            //
                                        }
                                    }


                                    if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "number"
                                        && _dtRecordColums.Rows[i]["TextType"] != DBNull.Value && _dtRecordColums.Rows[i]["TextType"].ToString() != ""
                                       && _dtRecordColums.Rows[i]["NumberType"] != DBNull.Value && _dtRecordColums.Rows[i]["NumberType"].ToString() == "6")
                                    {

                                        try
                                        {
                                            string strMoney = strValueAsString;
                                            strMoney = Common.IgnoreSymbols(strMoney);
                                            //RP Modified - Ticket 4305 Display commas on Summary Page
                                            //string strMoneyFormatted = string.Format("{0:$#.##;$-#.##}", strMoney);
                                            string strMoneyFormatted = Double.Parse(strMoney).ToString("N");
                                            //End Of Modification

                                            e.Row.Cells[j + 4].Text = _dtRecordColums.Rows[i]["TextType"].ToString()
                                                + strMoneyFormatted;
                                        }
                                        catch
                                        {
                                            //
                                        }

                                    }

                                    if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "calculation"
                                       && _dtRecordColums.Rows[i]["TextType"].ToString() == "f"
                                        && _dtRecordColums.Rows[i]["RegEx"].ToString() != "")
                                    {
                                        try
                                        {
                                            string strMoney = strValueAsString;
                                            strMoney = Common.IgnoreSymbols(strMoney);

                                            e.Row.Cells[j + 4].Text = _dtRecordColums.Rows[i]["RegEx"].ToString()
                                                + double.Parse(strMoney).ToString("C").Substring(1);
                                        }
                                        catch
                                        {
                                            //
                                        }
                                    }

                                    if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "location")
                                    {

                                        try
                                        {
                                            string strFullString = "";

                                            LocationColumn theLocationColumn = JSONField.GetTypedObject<LocationColumn>(strValueAsString);
                                            if (theLocationColumn != null)
                                            {

                                                if (theLocationColumn.Address != null)
                                                {
                                                    strFullString = theLocationColumn.Address;
                                                }
                                                else
                                                {
                                                    if (theLocationColumn.Latitude != null
                                                        && theLocationColumn.Longitude != null)
                                                    {
                                                        strFullString = "Lat:" + theLocationColumn.Latitude.ToString()
                                                            + ",Long:" + theLocationColumn.Longitude.ToString();
                                                    }
                                                }
                                            }
                                            e.Row.Cells[j + 4].Text = strFullString;
                                        }
                                        catch
                                        {
                                            //
                                            e.Row.Cells[j + 4].Text = "";
                                        }

                                    }






                                }//IsStandard==false



                            }


                            //image
                            if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "image")
                            {
                                int dtCurrentRecordColumnID = -1;
                                if (int.TryParse(_dtRecordColums.Rows[i]["ColumnID"].ToString(), out dtCurrentRecordColumnID))
                                {
                                    try
                                    {
                                        string strFilePath = retrieve_current_photo(dtCurrentRecordColumnID);
                                        if (strFilePath == "")
                                        {
                                            string strValueAsString = rowView[_dtRecordColums.Rows[i]["Heading"].ToString()].ToString();
                                            if (strValueAsString != "")
                                                strFilePath = _strFilesLocation + "/UserFiles/AppFiles/" + strValueAsString;
                                            else
                                                strFilePath = "";
                                        }

                                        if (strFilePath != "")
                                        {
                                            string strMaxHeight = "30";
                                            if (_dtRecordColums.Rows[i]["TextWidth"] != DBNull.Value)
                                            {
                                                strMaxHeight = _dtRecordColums.Rows[i]["TextWidth"].ToString();
                                            }
                                            e.Row.Cells[j + 4].Text = "<a target='_blank' href='" + strFilePath + "'>"
                                                        + "<img style='max-height:" + strMaxHeight + "px;"
                                            + "' src='" + strFilePath + "' />" + "</a>";
                                        }
                                    }
                                    catch
                                    {
                                        //
                                    }
                                }
                            }


                            if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "trafficlight"
                       && _dtRecordColums.Rows[i]["TrafficLightColumnID"] != DBNull.Value
                       && _dtRecordColums.Rows[i]["TrafficLightValues"] != DBNull.Value)
                            {
                                Column theTrafficLightColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["TrafficLightColumnID"].ToString()));
                                if (theTrafficLightColumn != null)
                                {
                                    string strTLValue = Common.GetValueFromSQL("SELECT " + theTrafficLightColumn.SystemName + " FROM [Record] WHERE RecordID=" + strDBGSystemRecordID);
                                    string strImageURL = Common.TrafficLightURL(theTrafficLightColumn, strTLValue, _dtRecordColums.Rows[i]["TrafficLightValues"].ToString());


                                    if (strImageURL != "")
                                    {
                                        e.Row.Cells[j + 4].Text = "<img alt='Traffic Light' src='" + Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + strImageURL + "' />";
                                    }

                                }

                            }



                            // C S
                            if (IsStandard(_dtRecordColums.Rows[i]["SystemName"].ToString()) == false)
                            {

                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "number"
                                    && _dtRecordColums.Rows[i]["NumberType"] != DBNull.Value)
                                {
                                    if (_dtRecordColums.Rows[i]["NumberType"].ToString() == "5")//record count
                                    {


                                        Table theTable = RecordManager.ets_Table_Details(int.Parse(_dtRecordColums.Rows[i]["TableTableID"].ToString()));
                                        string strTextSearch = "";
                                        if (theTable != null)
                                        {
                                            DataTable dtTemp = Common.DataTableFromText("SELECT SystemName,ColumnID FROM [Column] WHERE   TableID=" + _dtRecordColums.Rows[i]["TableTableID"].ToString() + " AND TableTableID=" + TableID.ToString());
                                            foreach (DataRow drCT in dtTemp.Rows)
                                            {
                                                Column theChildColumn = RecordManager.ets_Column_Details(int.Parse(drCT["ColumnID"].ToString()));
                                                Column theLinkedColumn = RecordManager.ets_Column_Details((int)theChildColumn.LinkedParentColumnID);
                                                Record theLinkedRecord = RecordManager.ets_Record_Detail_Full(int.Parse(strDBGSystemRecordID), int.Parse(_qsTableID), bUseArchiveData);  // Red 08012018
                                                string strLinkedColumnValue = RecordManager.GetRecordValue(ref theLinkedRecord, theLinkedColumn.SystemName);
                                                strLinkedColumnValue = strLinkedColumnValue.Replace("'", "''");

                                                if (strTextSearch == "")
                                                {
                                                    strTextSearch = " Record." + drCT["SystemName"].ToString() + "='" + strLinkedColumnValue + "' ";
                                                }
                                                else
                                                {
                                                    strTextSearch = strTextSearch + " OR " + " Record." + drCT["SystemName"].ToString() + "='" + strLinkedColumnValue + "' ";
                                                }

                                            }

                                            if (strTextSearch.Trim() != "")
                                                strTextSearch = " AND (" + strTextSearch + ")";
                                            int _iTotalDynamicColumnsTem = 0;
                                            int iTNTemp = 0;

                                            string strReturnSQL = "";
                                            Exception exParam = null;
                                            RecordManager.ets_Record_List(int.Parse(_dtRecordColums.Rows[i]["TableTableID"].ToString()), null, true,
                                           false, null, null,
                                           "", "", 0, 1, ref iTNTemp, ref _iTotalDynamicColumnsTem, "view", "",
                                           strTextSearch, null, null, "", "", "", _theView.ViewID, ref strReturnSQL,
                                           ref strReturnSQL, ref exParam, bUseArchiveData);  // Red 08012018

                                            if (_dtRecordColums.Rows[i]["DropDownValues"].ToString() == "no")
                                            {
                                                if (iTNTemp == 0)
                                                {
                                                    //e.Row.Cells[j + 4].Text = iTNTemp.ToString();
                                                }
                                                else
                                                {
                                                    e.Row.Cells[j + 4].Text = iTNTemp.ToString();
                                                }
                                            }
                                            else
                                            {
                                                if (iTNTemp == 0)
                                                {
                                                    //e.Row.Cells[j + 4].Text = iTNTemp.ToString();
                                                }
                                                else
                                                {
                                                    string strChildTableLink = " <a href='RecordList.aspx?TableID=" + Cryptography.Encrypt(_dtRecordColums.Rows[i]["TableTableID"].ToString()) + "&TextSearch=" + Cryptography.Encrypt(strTextSearch) + "' target='_blank'>" + iTNTemp.ToString() + "</a>";
                                                    e.Row.Cells[j + 4].Text = Server.HtmlDecode(strChildTableLink);
                                                    e.Row.Cells[j + 4].Attributes.Add("onClick", "");

                                                }

                                            }

                                        }

                                        //}


                                    }


                                }//Record Count Column 







                                //if (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
                                //    && (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "listbox")
                                //   && _dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
                                //    && _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
                                //{


                                //    if (e.Row.Cells[j + 4].Text.ToString() != "" && e.Row.Cells[j + 4].Text.ToString() != "&nbsp;")
                                //    {
                                //        //string strText = GetTextFromTableForList((int)_dtRecordColums.Rows[i]["TableTableID"], null, _dtRecordColums.Rows[i]["DisplayColumn"].ToString(), e.Row.Cells[j + 4].Text.ToString());
                                //        //if (strText != "")
                                //        if (e.Row.Cells[j + 4].Text.Trim().Substring(0,1)==",")
                                //            e.Row.Cells[j + 4].Text = e.Row.Cells[j + 4].Text.Trim().Substring(1);
                                //    }

                                //}





                                if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
                                    && (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
                                    || _dtRecordColums.Rows[i]["DropDownType"].ToString() == "tabledd")
                                     && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
                                      && _dtRecordColums.Rows[i]["LinkedParentColumnID"] != DBNull.Value
                                    && _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
                                {


                                    if (_dtRecordColums.Rows[i]["ShowViewLink"] != DBNull.Value
                                        && (_dtRecordColums.Rows[i]["ShowViewLink"].ToString().ToLower() == "summary"
                                || _dtRecordColums.Rows[i]["ShowViewLink"].ToString().ToLower() == "both"))
                                    {
                                        if (e.Row.Cells[j + 4].Text.ToString() != "" && e.Row.Cells[j + 4].Text.ToString() != "&nbsp;")
                                        {
                                            try
                                            {
                                                string strPaRecordID = e.Row.Cells[_dtDataSource.Columns["**" + _dtDataSource.Columns[j].ColumnName + "_ID**"].Ordinal + 4].Text.ToString();
                                                int iPRecordID = 0;
                                                bool bIsRecord = false;
                                                if (int.TryParse(strPaRecordID, out iPRecordID))
                                                {
                                                    Record thePaRecord = RecordManager.ets_Record_Detail_Full(int.Parse(strPaRecordID), int.Parse(_qsTableID), bUseArchiveData);  // Red 08012018

                                                    if (thePaRecord != null)
                                                    {

                                                        bIsRecord = true;
                                                    }

                                                }

                                                string strTarget = "_parent";

                                                string strFixedURL = "";

                                                if (_bOpenInParent)
                                                {
                                                    strFixedURL = "&fixedurl=" + Cryptography.Encrypt("~/Default.aspx");
                                                }

                                                if (strPaRecordID != "" && bIsRecord)
                                                {

                                                    string strLink = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?mode=" +
                                                Cryptography.Encrypt(strMode) + "&TableID=" + Cryptography.Encrypt(_dtRecordColums.Rows[i]["TableTableID"].ToString())
                                                + "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&RecordID=" + Cryptography.Encrypt(strPaRecordID) + "&UrlReferrer=y" + strFixedURL;
                                                    string strViewHTML = "<a  href='" + strLink + "' target='" + strTarget + "'> " + e.Row.Cells[j + 4].Text.ToString() + " <a>";

                                                    e.Row.Cells[j + 4].Text = strViewHTML;
                                                }
                                                else
                                                {
                                                    if (strPaRecordID != "" && bIsRecord == true) //bIsRecord=false
                                                    {
                                                        try
                                                        {
                                                            Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["LinkedParentColumnID"].ToString()));

                                                            DataTable dtTheRecord = Common.DataTableFromText("SELECT RecordID FROM Record WHERE TableID=" + theLinkedColumn.TableID.ToString() + " AND " + theLinkedColumn.SystemName + "='" + strPaRecordID.Replace("'", "''") + "'");

                                                            if (dtTheRecord.Rows.Count > 0)
                                                            {
                                                                strPaRecordID = dtTheRecord.Rows[0]["RecordID"].ToString();
                                                                string strLink = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?mode=" +
                                               Cryptography.Encrypt(strMode) + "&TableID=" + Cryptography.Encrypt(_dtRecordColums.Rows[i]["TableTableID"].ToString())
                                               + "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&RecordID=" + Cryptography.Encrypt(strPaRecordID) + "&UrlReferrer=y" + strFixedURL;
                                                                string strViewHTML = "<a  href='" + strLink + "'  target='" + strTarget + "'> " + e.Row.Cells[j + 4].Text.ToString() + " <a>";

                                                                e.Row.Cells[j + 4].Text = strViewHTML;
                                                            }

                                                        }
                                                        catch
                                                        {
                                                            //

                                                        }

                                                    }
                                                }
                                            }
                                            catch
                                            {
                                                //
                                            }
                                        }
                                    }


                                    //if (e.Row.Cells[j + 4].Text.ToString() != "" && e.Row.Cells[j + 4].Text.ToString() != "&nbsp;")
                                    //{
                                    //    try
                                    //    {

                                    //        Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["LinkedParentColumnID"].ToString()));


                                    //        //int iTableRecordID = int.Parse(e.Row.Cells[j + 4].Text);
                                    //        string strPaRecordID = e.Row.Cells[j + 4].Text;
                                    //        string strLinkedColumnValue = e.Row.Cells[j + 4].Text;
                                    //        DataTable dtTableTableSC = Common.DataTableFromText("SELECT SystemName,DisplayName FROM [Column] WHERE   TableID ="
                                    //         + _dtRecordColums.Rows[i]["TableTableID"].ToString());

                                    //        string strDisplayColumn = _dtRecordColums.Rows[i]["DisplayColumn"].ToString();
                                    //        string sstrDisplayColumnOrg = strDisplayColumn;

                                    //        foreach (DataRow dr in dtTableTableSC.Rows)
                                    //        {
                                    //            strDisplayColumn = strDisplayColumn.Replace("[" + dr["DisplayName"].ToString() + "]", "[" + dr["SystemName"].ToString() + "]");

                                    //        }

                                    //        sstrDisplayColumnOrg = strDisplayColumn;
                                    //        string strFilterSQL = "";
                                    //        if (theLinkedColumn.SystemName.ToLower() == "recordid")
                                    //        {

                                    //            strFilterSQL = strLinkedColumnValue;
                                    //        }
                                    //        else
                                    //        {
                                    //            strFilterSQL = "'" + strLinkedColumnValue.Replace("'", "''") + "'";
                                    //        }

                                    //        DataTable dtTheRecord = Common.DataTableFromText("SELECT * FROM Record WHERE TableID=" + theLinkedColumn.TableID.ToString() + " AND " + theLinkedColumn.SystemName + "=" + strFilterSQL);
                                    //        if (dtTheRecord.Rows.Count > 0)
                                    //        {

                                    //            strPaRecordID = dtTheRecord.Rows[0]["RecordID"].ToString();
                                    //            foreach (DataColumn dc in dtTheRecord.Columns)
                                    //            {
                                    //                Column theColumn = RecordManager.ets_Column_Details_By_Sys((int)theLinkedColumn.TableID, dc.ColumnName);
                                    //                if (theColumn != null)
                                    //                {
                                    //                    if (theColumn.ColumnType == "date")
                                    //                    {
                                    //                        string strDatePartOnly = dtTheRecord.Rows[0][dc.ColumnName].ToString();

                                    //                        if (strDatePartOnly.Length > 9)
                                    //                        {
                                    //                            strDatePartOnly = strDatePartOnly.Substring(0, 10);
                                    //                        }

                                    //                        strDisplayColumn = strDisplayColumn.Replace("[" + dc.ColumnName + "]", strDatePartOnly);
                                    //                    }
                                    //                    else
                                    //                    {
                                    //                        strDisplayColumn = strDisplayColumn.Replace("[" + dc.ColumnName + "]", dtTheRecord.Rows[0][dc.ColumnName].ToString());
                                    //                    }
                                    //                }

                                    //            }
                                    //        }
                                    //        if (sstrDisplayColumnOrg != strDisplayColumn)
                                    //            e.Row.Cells[j + 4].Text = strDisplayColumn;

                                    //        if (_dtRecordColums.Rows[i]["ShowViewLink"] != DBNull.Value
                                    //            && _dtRecordColums.Rows[i]["ShowViewLink"].ToString().ToLower() == "true")
                                    //        {
                                    //            if (strPaRecordID != "")
                                    //            {
                                    //                string strLink = Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?mode=" +
                                    //            Cryptography.Encrypt(strMode) + "&TableID=" + Cryptography.Encrypt(_dtRecordColums.Rows[i]["TableTableID"].ToString())
                                    //            + "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&RecordID=" + Cryptography.Encrypt(strPaRecordID) + "&UrlReferrer=y";
                                    //                string strViewHTML = "<a  href='" + strLink + "'> " + strDisplayColumn + " <a>";

                                    //                if (sstrDisplayColumnOrg != strDisplayColumn)
                                    //                    e.Row.Cells[j + 4].Text = strViewHTML;
                                    //            }
                                    //        }

                                    //    }
                                    //    catch
                                    //    {
                                    //        //
                                    //    }


                                    //}


                                }

                            }


                //            if (_dtRecordColums.Rows[i]["OnlyForAdmin"] != DBNull.Value
                //             && _dtRecordColums.Rows[i]["OnlyForAdmin"].ToString().ToLower() != "0"
                //&& !Common.HaveAccess(_strRecordRightID, "1,2"))
                //            {
                //                bool bHide = false;
                //                if (_dtRecordColums.Rows[i]["OnlyForAdmin"].ToString().ToLower() == _theRole.RoleID.ToString())
                //                {
                //                    if (_theRole.RoleType == "8")
                //                    {
                //                        //
                //                    }
                //                }
                //                else
                //                {
                //                    bHide = true;
                //                }

                //                if (bHide)
                //                {
                //                    e.Row.Cells[j + 4].Visible = false;
                //                }

                //            }



                            if (IsStandard(_dtRecordColums.Rows[i]["SystemName"].ToString()) == false)
                            {


                                if (_dtRecordColums.Rows[i]["SummaryCellBackColor"] != DBNull.Value && _dtRecordColums.Rows[i]["SummaryCellBackColor"].ToString() != "")
                                {
                                    e.Row.Cells[j + 4].Style.Add("background-color", _dtRecordColums.Rows[i]["SummaryCellBackColor"].ToString());
                                }


                                //if (_dtRecordColums.Rows[i]["Alignment"] == DBNull.Value)
                                //{
                                //    if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() != "number")
                                //    {
                                //        e.Row.Cells[j + 4].HorizontalAlign = HorizontalAlign.Left;
                                //    }
                                //    else
                                //    {
                                //        e.Row.Cells[j + 4].HorizontalAlign = HorizontalAlign.Right;
                                //    }
                                //}
                                //else
                                //{
                                e.Row.Cells[j + 4].HorizontalAlign = GetHorizontalAlign(_dtRecordColums.Rows[i]["Alignment"].ToString(), _dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower());
                                //}

                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "date")
                                {
                                    if (e.Row.Cells[j + 4].Text.Length > 9)
                                    {
                                        e.Row.Cells[j + 4].Text = e.Row.Cells[j + 4].Text.Substring(0, 10);
                                    }
                                }

                            }

                            //mm:hh
                            if (_dtRecordColums.Rows[i]["SystemName"].ToString().ToLower() == "datetimerecorded")
                            {

                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "datetime")
                                {
                                    if (e.Row.Cells[j + 4].Text.Length > 18)
                                    {
                                        e.Row.Cells[j + 4].Text = e.Row.Cells[j + 4].Text.Substring(0, 19);
                                    }
                                }
                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower() == "date")
                                {
                                    if (e.Row.Cells[j + 4].Text.Length > 9)
                                    {
                                        e.Row.Cells[j + 4].Text = e.Row.Cells[j + 4].Text.Substring(0, 10);
                                    }
                                }

                            }

                            if (_dtRecordColums.Rows[i]["SystemName"].ToString() == "IsActive"
                            || _dtRecordColums.Rows[i]["SystemName"].ToString() == "DateTimeRecorded"
                            || _dtRecordColums.Rows[i]["SystemName"].ToString() == "EnteredBy")
                            {

                                //if (_dtRecordColums.Rows[i]["Alignment"] == DBNull.Value)
                                //{
                                //    e.Row.Cells[j + 4].HorizontalAlign = HorizontalAlign.Center;
                                //}
                                //else
                                //{
                                e.Row.Cells[j + 4].HorizontalAlign = GetHorizontalAlign(_dtRecordColums.Rows[i]["Alignment"].ToString(), _dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower());
                                //}

                            }

                            if (_dtRecordColums.Rows[i]["SystemName"].ToString() == "Notes")
                            {

                                //if (_dtRecordColums.Rows[i]["Alignment"] == DBNull.Value)
                                //{
                                //    e.Row.Cells[j + 4].HorizontalAlign = HorizontalAlign.Left;
                                //}
                                //else
                                //{
                                e.Row.Cells[j + 4].HorizontalAlign = GetHorizontalAlign(_dtRecordColums.Rows[i]["Alignment"].ToString(), _dtRecordColums.Rows[i]["ColumnType"].ToString().ToLower());
                                //}


                            }


                            if (strDBGSystemRecordID != "-1")
                            {
                                Record aRecord = RecordManager.ets_Record_Detail_Full(int.Parse(strDBGSystemRecordID), int.Parse(_qsTableID), bUseArchiveData);  // Red 08012018

                                if (aRecord != null)
                                {
                                    if (aRecord.WarningResults != "")
                                    {


                                        if (IsStandard(_dtRecordColums.Rows[i]["SystemName"].ToString()) == false)
                                        {
                                            string strToolTips = "";


                                            string strEachFormulaV = "";
                                            string strEachFormulaW = "";
                                            string strEachFormulaE = "";

                                            if (_dtRecordColums.Rows[i]["AdvConV"] != DBNull.Value)
                                            {
                                                strEachFormulaV = "---advanced---";
                                            }
                                            //else if (_dtRecordColums.Rows[i]["ConV"] != DBNull.Value)
                                            //{
                                            //    Column theCheckColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["ConV"].ToString()));
                                            //    if (theCheckColumn != null)
                                            //    {
                                            //        string strCheckValue = RecordManager.GetRecordValue(ref aRecord, theCheckColumn.SystemName);
                                            //        strEachFormulaV = UploadWorld.Condition_GetFormula(int.Parse(_dtRecordColums.Rows[i]["ColumnID"].ToString()), theCheckColumn.ColumnID,
                                            //            "V", strCheckValue);
                                            //    }
                                            //}
                                            else
                                            {
                                                if (_dtRecordColums.Rows[i]["ValidationOnEntry"] != DBNull.Value && _dtRecordColums.Rows[i]["ValidationOnEntry"].ToString().Length > 0)
                                                {
                                                    strEachFormulaV = _dtRecordColums.Rows[i]["ValidationOnEntry"].ToString();
                                                }
                                            }

                                            if (_dtRecordColums.Rows[i]["AdvConW"] != DBNull.Value)
                                            {
                                                strEachFormulaW = "---advanced---";
                                            }
                                            //else if (_dtRecordColums.Rows[i]["ConW"] != DBNull.Value)
                                            //{
                                            //    Column theCheckColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["ConW"].ToString()));
                                            //    if (theCheckColumn != null)
                                            //    {
                                            //        string strCheckValue = RecordManager.GetRecordValue(ref aRecord, theCheckColumn.SystemName);
                                            //        strEachFormulaW = UploadWorld.Condition_GetFormula(int.Parse(_dtRecordColums.Rows[i]["ColumnID"].ToString()), theCheckColumn.ColumnID,
                                            //            "W", strCheckValue);
                                            //    }
                                            //}
                                            else
                                            {
                                                if (_dtRecordColums.Rows[i]["ValidationOnWarning"] != DBNull.Value && _dtRecordColums.Rows[i]["ValidationOnWarning"].ToString().Length > 0)
                                                {
                                                    strEachFormulaW = _dtRecordColums.Rows[i]["ValidationOnWarning"].ToString();
                                                }
                                            }

                                            if (_dtRecordColums.Rows[i]["AdvConE"] != DBNull.Value)
                                            {
                                                strEachFormulaE = "---advanced---";
                                            }
                                            //else if (_dtRecordColums.Rows[i]["ConE"] != DBNull.Value)
                                            //{
                                            //    Column theCheckColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["ConE"].ToString()));
                                            //    if (theCheckColumn != null)
                                            //    {
                                            //        string strCheckValue = RecordManager.GetRecordValue(ref aRecord, theCheckColumn.SystemName);
                                            //        strEachFormulaE = UploadWorld.Condition_GetFormula(int.Parse(_dtRecordColums.Rows[i]["ColumnID"].ToString()), theCheckColumn.ColumnID,
                                            //            "E", strCheckValue);
                                            //    }
                                            //}
                                            else
                                            {
                                                if (_dtRecordColums.Rows[i]["ValidationOnExceedance"] != DBNull.Value && _dtRecordColums.Rows[i]["ValidationOnExceedance"].ToString().Length > 0)
                                                {
                                                    strEachFormulaE = _dtRecordColums.Rows[i]["ValidationOnExceedance"].ToString();
                                                }
                                            }





                                            if (aRecord.WarningResults.IndexOf("EXCEEDANCE: " + _dtRecordColums.Rows[i]["DisplayName"].ToString()) >= 0)
                                            {
                                                /* == Red: Requested by Jon == */
                                                //e.Row.Cells[j + 4].ForeColor = System.Drawing.Color.Orange;
                                                e.Row.Cells[j + 4].ForeColor = System.Drawing.Color.Red;
                                                strToolTips = strToolTips + Common.GetFromulaMsg("e", _dtRecordColums.Rows[i]["DisplayName"].ToString(), strEachFormulaE); //"EXCEEDANCE: Value outside accepted range(" + strEachFormulaE + "). ";
                                            }
                                            else if (aRecord.WarningResults.IndexOf("INVALID (and ignored): " + _dtRecordColums.Rows[i]["DisplayName"].ToString()) >= 0)
                                            {
                                                e.Row.Cells[j + 4].ForeColor = System.Drawing.Color.Red;
                                                strToolTips = Common.GetFromulaMsg("i", _dtRecordColums.Rows[i]["DisplayName"].ToString(), strEachFormulaV);// "INVALID (and ignored):" + strEachFormulaV + ". ";
                                            }
                                            else if (aRecord.WarningResults.IndexOf("WARNING: " + _dtRecordColums.Rows[i]["DisplayName"].ToString()) >= 0)
                                            {
                                                e.Row.Cells[j + 4].ForeColor = System.Drawing.Color.Blue;
                                                strToolTips = "";
                                                if (aRecord.WarningResults.IndexOf("SENSOR WARNING: " + _dtRecordColums.Rows[i]["DisplayName"].ToString() + " - Value below minimum detectable limit") >= 0)
                                                {
                                                    strToolTips = "SENSOR: Value below minimum detectable limit. ";
                                                }
                                                if (aRecord.WarningResults.IndexOf("SENSOR WARNING: " + _dtRecordColums.Rows[i]["DisplayName"].ToString() + " - Sensor out of Calibration") >= 0)
                                                {
                                                    strToolTips = strToolTips + "SENSOR: Sensor out of Calibration. ";
                                                }
                                                if (aRecord.WarningResults.IndexOf("WARNING: " + _dtRecordColums.Rows[i]["DisplayName"].ToString() + " – Value outside accepted range") >= 0)
                                                {
                                                    strToolTips = strToolTips + Common.GetFromulaMsg("w", _dtRecordColums.Rows[i]["DisplayName"].ToString(), strEachFormulaW);// "WARNING: Value outside accepted range(" + strEachFormulaW + "). ";
                                                }

                                                if (aRecord.WarningResults.IndexOf("WARNING: " + _dtRecordColums.Rows[i]["DisplayName"].ToString() + " – Unlikely data – outside 3 standard deviations.") >= 0)
                                                {
                                                    strToolTips = strToolTips + "WARNING:  – Unlikely data – outside 3 standard deviations.";
                                                }


                                            }



                                            if (aRecord.ValidationResults != "" && chkShowDeletedRecords.Checked == false)
                                            {
                                                if (IsStandard(_dtRecordColums.Rows[i]["SystemName"].ToString()) == false)
                                                {

                                                    if (aRecord.ValidationResults.IndexOf("INVALID (and ignored): " + _dtRecordColums.Rows[i]["DisplayName"].ToString()) >= 0
                                                        || aRecord.ValidationResults.IndexOf("INVALID: " + _dtRecordColums.Rows[i]["DisplayName"].ToString()) >= 0)
                                                    {
                                                        e.Row.Cells[j + 4].ForeColor = System.Drawing.Color.Red;
                                                        strToolTips = Common.GetFromulaMsg("i", _dtRecordColums.Rows[i]["DisplayName"].ToString(), strEachFormulaV);// "INVALID (and ignored):" + strEachFormulaV + ". ";

                                                    }

                                                }
                                            }


                                            e.Row.Cells[j + 4].ToolTip = strToolTips;
                                        }
                                        else
                                        {
                                            if (_dtRecordColums.Rows[i]["SystemName"].ToString() == "DateTimeRecorded")
                                            {

                                                if (aRecord.WarningResults.IndexOf("" + WarningMsg.MaxtimebetweenRecords + "") >= 0)
                                                {
                                                    e.Row.Cells[j + 4].ForeColor = System.Drawing.Color.Blue;
                                                    e.Row.Cells[j + 4].ToolTip = "WARNING: " + WarningMsg.MaxtimebetweenRecords + ".";
                                                }


                                            }
                                        }


                                    }

                                    //validation





                                }

                            }

                            if (_dtRecordColums.Rows[i]["ColourCells"] != null && _dtRecordColums.Rows[i]["ColourCells"].ToString().ToLower() == "true")
                            {
                                int thisColumnId = (int)_dtRecordColums.Rows[i]["ColumnID"];
                                Record thisRecord = RecordManager.ets_Record_Detail_Full((int)rowView["DBGSystemRecordID"], int.Parse(_qsTableID), bUseArchiveData);  // Red 08012018
                                string sColourText = ""; //GetCellColour(thisColumnId, thisRecord);
                                string sColourCell = "";
                                GetCellColour(thisColumnId, thisRecord, ref sColourText, ref sColourCell);
                                if (!String.IsNullOrEmpty(sColourText))
                                {
                                    e.Row.Cells[j + 4].Style.Add("color", "#" + sColourText);
                                }
                                if (!String.IsNullOrEmpty(sColourCell))
                                {
                                    e.Row.Cells[j + 4].Style.Add("background", "#" + sColourCell);
                                }
                            }


                            //inline editing -- PageType == "p" && 
                            if (_dtRecordColums.Rows[i]["EditableOnSummary"] != DBNull.Value
                                   && _dtRecordColums.Rows[i]["EditableOnSummary"].ToString().ToLower() == "true")
                            {
                                e.Row.Cells[j + 4].CssClass = "inlineeditingclass";
                                e.Row.Cells[j + 4].Attributes.Add("systemname", _dtRecordColums.Rows[i]["SystemName"].ToString());
                                e.Row.Cells[j + 4].Attributes.Add("recordid", strDBGSystemRecordID);
                                e.Row.Cells[j + 4].Attributes.Remove("onClick");
                                e.Row.Cells[j + 4].Style.Add("cursor", "default");
                                e.Row.Cells[j + 4].ToolTip = "Double click to edit." + e.Row.Cells[j + 4].ToolTip;
                            }



                        }  //            
                    }
                }








            }
        }

        //if (e.Row.RowType == DataControlRowType.Footer)
        //{
        //Here we can pass sender, e & theRecord
        // if (!string.IsNullOrEmpty(theRDB.FooterCustomMethod))
        //{
        //try
        //{
        //    List<object> objList = new List<object>();
        //    objList.Add(sender);
        //    objList.Add(e);
        //       //  objList.Add(theRecord);//This is when DataControlRowType.DataRow
        //    List<object> roList = CustomMethod.DotNetMethod(theRDB.FooterCustomMethod, objList);
        //    if (roList != null && roList.Count > 0)
        //    {
        //        foreach (object obj in roList)
        //        {
        //may be no need as control are passed as by ref
        //        }
        //    }
        //}
        //catch
        //{
        //custom error
        //}
        //}

        //}




        //if (Session["RunSpeedLog"] != null && _theTable != null)
        //{


        //    SpeedLog theSpeedLog = new SpeedLog();
        //    theSpeedLog.FunctionName = _theTable.TableName + " " + e.Row.RowType.ToString() + " RowDataBound - END ";
        //    theSpeedLog.FunctionLineNumber = 4385;
        //    SecurityManager.AddSpeedLog(theSpeedLog);

        //}


        if (_bHaveService)
        {

            ProcessServiceForEvent("RecordList_GridView_RowDataBound", sender, e);
        }


    }


    protected bool IsStandard(string strColumnName)
    {
        if (strColumnName.Substring(0, 1).ToUpper() == "V")
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    protected void ddlAccountFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        //PopulateRecordGroupFilter();
        //PopulateLocationList();
        PopulateUser();
        PopulateBatch();

    }


    protected void ddlTable_SelectedIndexChanged(object sender, EventArgs e)
    {
        lnkSearch_Click(null, null);

    }


    protected void Pager_BindTheGridToExport(object sender, EventArgs e)
    {


        _gvPager.ExportFileName = lblTitle.Text.Replace("Records - ", "") + " " + DateTime.Today.ToString("yyyyMMdd");
        //gvTheGrid.PageIndex = 0;// why???
        //BindTheGridForExport(0, _gvPager.TotalRows);
        BindTheGrid(0, _gvPager.TotalRows);

    }

    protected void cbcSearchMain_OnddlCompareOperator_Changed(object sender, EventArgs e)
    {
        hfControlB_DDL.Value = "";       
        lnkSearch_Click(null, null);
    }
    protected void cbcSearch1_OnddlCompareOperator_Changed(object sender, EventArgs e)
    {
        hfDDLcbcSearch1.Value = "";      
        lnkSearch_Click(null, null);
    }
    protected void cbcSearch2_OnddlCompareOperator_Changed(object sender, EventArgs e)
    {
        hfDDLcbcSearch2.Value = "";
        lnkSearch_Click(null, null);
    }
    protected void cbcSearch3_OnddlCompareOperator_Changed(object sender, EventArgs e)
    {
        hfDDLcbcSearch3.Value = "";       
        lnkSearch_Click(null, null);
    }
    protected void cbcSearchMain_OnddlYAxis_Changed(object sender, EventArgs e)
    {
      
        if (cbcSearchMain.ddlYAxisV == "")
        {
            //cbcSearchMain.PopulateSearchParams();
            trSearch1a.Visible = false;
            lnkAddSearch1.Visible = false;
            lnkSearch_Click(null, null);
            

        }
        else
        {
            Session["cbcRecordList"] = ViewState["strReturnSQL"]; //Red 2501 3611
            trSearch1a.Visible = true;
            lnkAddSearch1.Visible = true;
         
        }
    }
    protected void cbcSearch1_OnddlYAxis_Changed(object sender, EventArgs e)
    {
       
        if (cbcSearch1.ddlYAxisV == "")
        {
            trSearch2a.Visible = false;
            lnkAddSearch2.Visible = false;
            lnkSearch_Click(null, null);
            hfDDLcbcSearch1.Value = "";          

        }
        else
        {
            Session["cbcRecordList"] = ViewState["strReturnSQL"];  //Red 2501
            lnkAddSearch2.Visible = true;
            trSearch2a.Visible = true;
            //2501 reload the multipleselect
            //AdvancedMultipleSelect();
            ProcessAdvancedMultipleSelect(hfControlB_DDL, cbcSearchMain, "cbcSearchMain_");
        }
    }

    protected void cbcSearch2_OnddlYAxis_Changed(object sender, EventArgs e)
    {
        
        if (cbcSearch2.ddlYAxisV == "")
        {
            trSearch3a.Visible = false;
            lnkAddSearch3.Visible = false;
            lnkSearch_Click(null, null);
            hfDDLcbcSearch2.Value = "";
          
        }
        else
        {
            Session["cbcRecordList"] = ViewState["strReturnSQL"];  //Red 2501
            lnkAddSearch3.Visible = true;
            trSearch3a.Visible = true;
            //2501 reload the multipleselect
            //AdvancedMultipleSelect();
            ProcessAdvancedMultipleSelect(hfDDLcbcSearch1, cbcSearch1, "cbcSearch1_");
                ProcessAdvancedMultipleSelect(hfControlB_DDL, cbcSearchMain, "cbcSearchMain_");
        }
    }

    //Red 2501
    protected void cbcSearch3_OnddlYAxis_Changed(object sender, EventArgs e)
    {
        if (cbcSearch3.ddlYAxisV == "")
            hfDDLcbcSearch3.Value = "";
      
        Session["cbcRecordList"] = ViewState["strReturnSQL"];  // //Red 2501
        AdvancedMultipleSelect();

        

    }


    protected void Pager_BindTheGridAgain(object sender, EventArgs e)
    {

        BindTheGrid(_gvPager.StartIndex, _gvPager.PageSize);
    }



    protected void PopulateDateAddedSearch()
    {


        if (txtDateFrom.Text != "")
        {
            DateTime dtTemp;
            if (DateTime.TryParseExact(txtDateFrom.Text.Trim(), Common.Dateformats, new CultureInfo("en-GB"), DateTimeStyles.None, out dtTemp))
            {
                _dtDateFrom = dtTemp;
            }
        }




        if (txtDateTo.Text != "")
        {
            DateTime dtTemp;
            if (DateTime.TryParseExact(txtDateTo.Text.Trim(), Common.Dateformats, new CultureInfo("en-GB"), DateTimeStyles.None, out dtTemp))
            {
                _dtDateTo = dtTemp;
                _dtDateTo = _dtDateTo.Value.AddHours(23).AddMinutes(59);
            }
        }

    }

    //Red Ticket 2501
    protected void ProcessAdvancedMultipleSelect(HiddenField hfSelected, Pages_UserControl_ControlByColumn cbcSearc, string cbcx)
    {
       
        string strddlParentSearchSet = "";     
            if (hfSelected.Value != "")
            {
                string strValue = "'" + hfSelected.Value + "'";
                strValue = strValue.Replace(",", "','");

                strddlParentSearchSet = "$('#" + _strDynamictabPart + cbcx + cbcSearc.ControlB_DDL.ID + "').multipleSelect('setSelects',[" + strValue + "]);";
            }
            else
            {
                strddlParentSearchSet = "$('#" + _strDynamictabPart + cbcx + cbcSearc.ControlB_DDL.ID + "').multipleSelect('uncheckAll');";
            }
            strddlParentSearchSet = @" $(document).ready(function () {  "
                + strddlParentSearchSet + @"
                                                 });
                                                ";
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "jsddlParentSearchSet_cbcSearch" + _strDynamictabPart + cbcx + "Control_DDL", strddlParentSearchSet, true);

          //  ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "jsrefresh_Multipleselect" , "$('#" + _strDynamictabPart + cbcx + cbcSearc.ControlB_DDL.ID + "').multipleSelect('refresh');", true);

    }

    protected void AdvancedMultipleSelect()
    {

        if (cbcSearchMain.ddlYAxisV != "" && hfControlB_DDL != null)
            ProcessAdvancedMultipleSelect(hfControlB_DDL, cbcSearchMain, "cbcSearchMain_");

        if (cbcSearch1.ddlYAxisV != "" && hfDDLcbcSearch1 != null)
            ProcessAdvancedMultipleSelect(hfDDLcbcSearch1, cbcSearch1, "cbcSearch1_");

        if (cbcSearch2.ddlYAxisV != "" && hfDDLcbcSearch2 != null)
            ProcessAdvancedMultipleSelect(hfDDLcbcSearch2, cbcSearch2, "cbcSearch2_");

        if (cbcSearch3.ddlYAxisV != "" && hfDDLcbcSearch3 != null)
            ProcessAdvancedMultipleSelect(hfDDLcbcSearch3, cbcSearch3, "cbcSearch3_");


    }
    //end Red
    protected void Pager_OnApplyFilter(object sender, EventArgs e)
    {
        Session["SCid" + hfViewID.Value] = null;
        ViewState["_iSearchCriteriaID"] = null;

        txtDateFrom.Text = "";
        txtDateTo.Text = "";
        ddlEnteredBy.SelectedIndex = 0;
        //Red Ticket 2070 Added
        //<Begin>
        ddlUploadedBatch.SelectedIndex = 0;
        //RP Added Ticket 4363
        txtUploadedBatch.Text = "";
        hiddenUploadedBatchID.Value = "";
        //End Modification
        //<End> 
        //chkIsActive.Checked = false;
        //chkShowOnlyWarning.Checked = false;

        _strNumericSearch = "";
        TextSearch = "";

        if (_theView != null)
        {
            _bReset = true;
        }

       

        //chk if reset button is raised
        //LinkButton lnkResetSender = (LinkButton)sender;
        //if (lnkResetSender != null && lnkResetSender == lnkReset)
        //{
        //    chkShowDeletedRecords.Checked = false;
        //    chkShowOnlyWarning.Checked = false;
        //}

        if (chkShowAdvancedOptions.Checked == false)//_bDynamicSearch
        {
            chkShowOnlyWarning.Checked = false;

            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

            if (_bReset)
            {
                if (_theView.Filter != "" && _theView.FilterControlsInfo != "")
                {

                    xmlDoc.Load(new StringReader(_theView.FilterControlsInfo));

                }
            }

            foreach (DataRow dr in _dtDynamicSearchColumns.Rows)
            {

                if (dr["ColumnType"].ToString() == "number" || dr["ColumnType"].ToString() == "calculation")
                {

                    TextBox txtLowerLimit = (TextBox)tblSearchControls.FindControl("txtLowerLimit_" + dr["SystemName"].ToString());
                    TextBox txtUpperLimit = (TextBox)tblSearchControls.FindControl("txtUpperLimit_" + dr["SystemName"].ToString());

                    if (txtLowerLimit != null && txtUpperLimit != null)
                    {
                        txtLowerLimit.Text = "";
                        txtUpperLimit.Text = "";

                        //if (xmlDoc != null && _bReset)
                        //{
                        //    if (ViewState[txtLowerLimit.ID + dr["ColumnID"].ToString()] != null)
                        //        txtLowerLimit.Text = ViewState[txtLowerLimit.ID + dr["ColumnID"].ToString()].ToString();

                        //    if (ViewState[txtUpperLimit.ID + dr["ColumnID"].ToString()] != null)
                        //        txtUpperLimit.Text = ViewState[txtUpperLimit.ID + dr["ColumnID"].ToString()].ToString();
                        //}

                    }

                }
                else if (dr["ColumnType"].ToString() == "text")
                {
                    TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                    if (txtSearch != null)
                    {
                        txtSearch.Text = "";

                        //if (xmlDoc != null && _bReset)
                        //{
                        //    if (ViewState[txtSearch.ID + dr["ColumnID"].ToString()] != null)
                        //        txtSearch.Text = ViewState[txtSearch.ID + dr["ColumnID"].ToString()].ToString();
                        //    //txtSearch.Text = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                        //}
                    }

                }

                else if (dr["ColumnType"].ToString() == "date")
                {
                    TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                    TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());
                    if (txtLowerDate != null && txtUpperDate != null)
                    {
                        txtLowerDate.Text = "";
                        txtUpperDate.Text = "";
                        //if (xmlDoc != null && _bReset)
                        //{
                        //    if (ViewState[txtLowerDate.ID + dr["ColumnID"].ToString()] != null)
                        //        txtLowerDate.Text = ViewState[txtLowerDate.ID + dr["ColumnID"].ToString()].ToString();

                        //    if (ViewState[txtUpperDate.ID + dr["ColumnID"].ToString()] != null)
                        //        txtUpperDate.Text = ViewState[txtUpperDate.ID + dr["ColumnID"].ToString()].ToString();

                        //}
                    }

                }
                else if (dr["ColumnType"].ToString() == "datetime")
                {
                    TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                    TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());

                    TextBox txtLowerTime = (TextBox)tblSearchControls.FindControl("txtLowerTime_" + dr["SystemName"].ToString());
                    TextBox txtUpperTime = (TextBox)tblSearchControls.FindControl("txtUpperTime_" + dr["SystemName"].ToString());

                    if (txtLowerDate != null && txtUpperDate != null && txtLowerTime != null && txtUpperTime != null)
                    {
                        txtLowerDate.Text = "";
                        txtUpperDate.Text = "";
                        txtLowerTime.Text = "";
                        txtUpperTime.Text = "";

                        //if (xmlDoc != null && _bReset)
                        //{
                        //    if (ViewState[txtLowerDate.ID + dr["ColumnID"].ToString()] != null)
                        //        txtLowerDate.Text = ViewState[txtLowerDate.ID + dr["ColumnID"].ToString()].ToString();
                        //    if (ViewState[txtUpperDate.ID + dr["ColumnID"].ToString()] != null)
                        //        txtUpperDate.Text = ViewState[txtUpperDate.ID + dr["ColumnID"].ToString()].ToString();
                        //    if (ViewState[txtLowerTime.ID + dr["ColumnID"].ToString()] != null)
                        //        txtLowerTime.Text = ViewState[txtLowerTime.ID + dr["ColumnID"].ToString()].ToString();
                        //    if (ViewState[txtUpperTime.ID + dr["ColumnID"].ToString()] != null)
                        //        txtUpperTime.Text = ViewState[txtUpperTime.ID + dr["ColumnID"].ToString()].ToString();
                        //}
                    }

                }
                else if (dr["ColumnType"].ToString() == "time")
                {
                    TextBox txtLowerTime = (TextBox)tblSearchControls.FindControl("txtLowerTime_" + dr["SystemName"].ToString());
                    TextBox txtUpperTime = (TextBox)tblSearchControls.FindControl("txtUpperTime_" + dr["SystemName"].ToString());

                    if (txtLowerTime != null && txtUpperTime != null)
                    {
                        txtLowerTime.Text = "";
                        txtUpperTime.Text = "";
                        //if (xmlDoc != null && _bReset)
                        //{
                        //    if (ViewState[txtLowerTime.ID + dr["ColumnID"].ToString()] != null)
                        //        txtLowerTime.Text = ViewState[txtLowerTime.ID + dr["ColumnID"].ToString()].ToString();
                        //    if (ViewState[txtUpperTime.ID + dr["ColumnID"].ToString()] != null)
                        //        txtUpperTime.Text = ViewState[txtUpperTime.ID + dr["ColumnID"].ToString()].ToString();
                        //}
                    }

                }

                //if (dr["ColumnType"].ToString() == "datetime")
                //{
                //    TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                //    TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());

                //    TextBox txtLowerTime = (TextBox)tblSearchControls.FindControl("txtLowerTime_" + dr["SystemName"].ToString());
                //    TextBox txtUpperTime = (TextBox)tblSearchControls.FindControl("txtUpperTime_" + dr["SystemName"].ToString());

                //    if (txtDate != null)
                //    {
                //        txtDate.Text = "";
                //    }

                //    TextBox txtTime = (TextBox)tblSearchControls.FindControl("txtTime_" + dr["SystemName"].ToString());
                //    if (txtTime != null)
                //    {
                //        txtTime.Text = "";
                //    }
                //}

                //else if (dr["ColumnType"].ToString() == "time")
                //{

                //    TextBox txtTime = (TextBox)tblSearchControls.FindControl("txtTime_" + dr["SystemName"].ToString());
                //    if (txtTime != null)
                //    {
                //        txtTime.Text = "";
                //        if (xmlDoc != null && _bReset)
                //        {
                //            txtTime.Text = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                //        }
                //    }
                //}

                else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "values" || dr["DropDownType"].ToString() == "value_text"))
                {
                    DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                    if (ddlSearch != null)
                    {
                        ddlSearch.SelectedIndex = 0;
                        if (xmlDoc != null && _bReset)
                        {

                            //if (ViewState[ddlSearch.ID + dr["ColumnID"].ToString()] != null)
                            //{
                            //    if (ddlSearch.Items.FindByValue(ViewState[ddlSearch.ID + dr["ColumnID"].ToString()].ToString()) != null)
                            //        ddlSearch.SelectedValue = ViewState[ddlSearch.ID + dr["ColumnID"].ToString()].ToString();
                            //}



                        }
                    }

                }

                //else if (dr["ColumnType"].ToString() == "radiobutton" && (dr["DropDownType"].ToString() == "values" || dr["DropDownType"].ToString() == "value_text"))
                //{
                //    DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                //    if (ddlSearch != null)
                //    {
                //        ddlSearch.SelectedIndex = 0;
                //        if (xmlDoc != null && _bReset)
                //        {

                //            if (ViewState[ddlSearch.ID + dr["ColumnID"].ToString()] != null)
                //            {
                //                if (ddlSearch.Items.FindByValue(ViewState[ddlSearch.ID + dr["ColumnID"].ToString()].ToString()) != null)
                //                    ddlSearch.SelectedValue = ViewState[ddlSearch.ID + dr["ColumnID"].ToString()].ToString();
                //            }



                //        }
                //    }

                //}

                else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "table" || dr["DropDownType"].ToString() == "tabledd") &&
           dr["TableTableID"] != DBNull.Value && dr["DisplayColumn"].ToString() != "")
                {
                   
                    HiddenField hfParentSearch = (HiddenField)tblSearchControls.FindControl(_strDynamictabPart + "hfParentSearch_" + dr["SystemName"].ToString());
                    if (hfParentSearch != null)
                    {
                        hfParentSearch.Value = "";
                    }

                    //DropDownList ddlParentSearch = (DropDownList)tblSearchControls.FindControl(_strDynamictabPart+ "ddlParentSearch_" + dr["SystemName"].ToString());

                    //if (ddlParentSearch != null)
                    //{
                    //    ddlParentSearch.SelectedIndex = 0;
                    //    if (xmlDoc != null && _bReset)
                    //    {
                    //        //if (ViewState[ddlParentSearch.ID + dr["ColumnID"].ToString()] != null)
                    //        //{
                    //        //    if (ddlParentSearch.Items.FindByValue(ViewState[ddlParentSearch.ID + dr["ColumnID"].ToString()].ToString()) != null)
                    //        //        ddlParentSearch.SelectedValue = ViewState[ddlParentSearch.ID + dr["ColumnID"].ToString()].ToString();
                    //        //}
                    //    }
                    //}

                }
                else if (dr["ColumnType"].ToString() == "radiobutton" || dr["ColumnType"].ToString() == "listbox"
                    || dr["ColumnType"].ToString() == "checkbox")
                {
                    DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                    if (ddlSearch != null)
                    {
                        ddlSearch.SelectedIndex = 0;
                        if (xmlDoc != null && _bReset)
                        {
                            //if (ViewState[ddlSearch.ID + dr["ColumnID"].ToString()] != null)
                            //{
                            //    if (ddlSearch.Items.FindByValue(ViewState[ddlSearch.ID + dr["ColumnID"].ToString()].ToString()) != null)
                            //        ddlSearch.SelectedValue = ViewState[ddlSearch.ID + dr["ColumnID"].ToString()].ToString();
                            //}
                        }
                    }
                }
                else
                {
                    TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                    if (txtSearch != null)
                    {
                        txtSearch.Text = "";

                        //if (xmlDoc != null && _bReset)
                        //{
                        //    if (ViewState[txtSearch.ID + dr["ColumnID"].ToString()] != null)
                        //        txtSearch.Text = ViewState[txtSearch.ID + dr["ColumnID"].ToString()].ToString();
                        //}
                    }
                }
            }
            //
            foreach (DataRow dr in _dtSearchGroup.Rows)
            {

                TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SearchGroupID"].ToString());
                if (txtSearch != null)
                {
                    if (txtSearch.Text != "")
                    {
                        txtSearch.Text = "";
                    }
                }
            }

        }
        else
        {
            //Red Ticket 2501 reset control columnsearch when unticked advanced search        
            if (!IsFilteredStandard()) { 
            cbcSearchMain.ddlYAxisV = "";
            cbcSearch1.ddlYAxisV = "";
            cbcSearch2.ddlYAxisV = "";
            cbcSearch3.ddlYAxisV = "";
            hfAndOr1.Value = "";
            hfAndOr2.Value = "";
            hfAndOr3.Value = "";
                hfControlB_DDL.Value = "";
                hfDDLcbcSearch1.Value = "";
                hfDDLcbcSearch2.Value = "";
                hfDDLcbcSearch3.Value = "";


                string strJSSearchShowHideorig = "$('#" + trSearch1.ClientID + "').hide();$('#" + trSearch2.ClientID + "').hide();$('#" + trSearch3.ClientID + "').hide();";
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "PutDefaultSearcUI_CS", strJSSearchShowHideorig, true);
            }

            else // standard not filtered
            {

                //Red Ticket 2501
                foreach (DataRow dr in _dtDynamicSearchColumns.Rows)
                {

                    if (dr["ColumnType"].ToString() == "number" || dr["ColumnType"].ToString() == "calculation")
                    {
                        TextBox txtLowerLimit = (TextBox)tblSearchControls.FindControl("txtLowerLimit_" + dr["SystemName"].ToString());
                        TextBox txtUpperLimit = (TextBox)tblSearchControls.FindControl("txtUpperLimit_" + dr["SystemName"].ToString());
                        if (txtLowerLimit != null && txtUpperLimit != null)
                        {
                            if (txtLowerLimit.Text != "" || txtUpperLimit.Text != "")
                            {

                                ProcessStandardToAdvancedSearch(int.Parse(dr["ColumnID"].ToString()), txtLowerLimit.Text, txtUpperLimit.Text, "", false, "between");
                                //lets reset them
                                txtLowerLimit.Text = "";
                                txtUpperLimit.Text = "";
                            }

                        }

                    }
                    else if (dr["ColumnType"].ToString() == "text")
                    {
                        TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                        if (txtSearch != null)
                        {
                            if (txtSearch.Text != "")
                            {
                                ProcessStandardToAdvancedSearch(int.Parse(dr["ColumnID"].ToString()), "", "", txtSearch.Text, false, "=");
                                //lets reset this
                                txtSearch.Text = "";
                            }
                        }

                    }
                    else if (dr["ColumnType"].ToString() == "date" || dr["ColumnType"].ToString() == "datetime")
                    {
                        TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                        TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());
                        if (txtLowerDate != null && txtUpperDate != null)
                        {
                            if (txtLowerDate.Text != "" || txtUpperDate.Text != "")
                            {
                                ProcessStandardToAdvancedSearch(int.Parse(dr["ColumnID"].ToString()), txtLowerDate.Text, txtUpperDate.Text, "", true, "between");
                                //lets reset them
                                txtUpperDate.Text = "";
                                txtLowerDate.Text = "";
                            }



                        }

                    }
                   
                    else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "values" || dr["DropDownType"].ToString() == "value_text"))
                    {
                        DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                        if (ddlSearch != null)
                        {
                            if (ddlSearch.SelectedValue != "")
                            {
                                ProcessStandardToAdvancedSearch(int.Parse(dr["ColumnID"].ToString()), "", "", ddlSearch.SelectedValue, false, "=");
                                
                                //lets reset this
                                ddlSearch.SelectedValue = "";
                            }
                        }
                    }
                    else if (dr["ColumnType"].ToString() == "radiobutton" || dr["ColumnType"].ToString() == "checkbox" ||
                           dr["ColumnType"].ToString() == "listbox") //3641_2
                    {
                        DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                        if (ddlSearch != null)
                        {
                            if (ddlSearch.SelectedValue != "")
                            {
                                ProcessStandardToAdvancedSearch(int.Parse(dr["ColumnID"].ToString()), "", "", ddlSearch.SelectedValue, false, "=");

                                //lets reset this
                                ddlSearch.SelectedValue = "";
                            }
                        }

                    }

                    else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "table" || dr["DropDownType"].ToString() == "tabledd") &&
               dr["TableTableID"] != DBNull.Value && dr["DisplayColumn"].ToString() != "")
                    {

                        //Red Ticket 2501

                        HiddenField hfParentSearch = (HiddenField)tblSearchControls.FindControl(_strDynamictabPart + "hfParentSearch_" + dr["SystemName"].ToString());

                        if (hfParentSearch != null)
                        {
                            if (hfParentSearch.Value != "")
                            {
                                ProcessStandardToAdvancedSearch(int.Parse(dr["ColumnID"].ToString()), "", "", hfParentSearch.Value, false, "=");

                                //lets reset this
                                hfParentSearch.Value = "";
                            }

                        }

                        //DropDownList ddlParentSearch = (DropDownList)tblSearchControls.FindControl("ddlParentSearch_" + dr["SystemName"].ToString());

                        //if (ddlParentSearch != null)
                        //{
                        //    if (ddlParentSearch.Text != "")
                        //    {
                        //        ProcessStandardToAdvancedSearch(int.Parse(dr["ColumnID"].ToString()), "", "", ddlParentSearch.Text, "=");
                        //    }
                        //}
                    }
                    else
                    {
                        TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                        if (txtSearch != null)
                        {
                            if (txtSearch.Text != "")
                            {
                                ProcessStandardToAdvancedSearch(int.Parse(dr["ColumnID"].ToString()), "", "", txtSearch.Text, false, "=");

                                //lets reset this
                                txtSearch.Text = "";
                            }
                        }
                    }
                }
                //end Red
            }
            //if isfilterstandard() end


        }

        gvTheGrid.GridViewSortColumn = "DBGSystemRecordID";
        gvTheGrid.GridViewSortDirection = SortDirection.Descending;

        if (_bReset)
        {
            SetGridFromView();
            //if (_theView.SortOrder != "")
            //{
            //    string strSortColumn = _theView.SortOrder.Substring(0, _theView.SortOrder.LastIndexOf(" ") + 1);
            //    string strDirection = _theView.SortOrder.Substring(_theView.SortOrder.LastIndexOf(" ") + 1);

            //    gvTheGrid.GridViewSortColumn = strSortColumn.Trim();

            //    if (strDirection.Trim().ToLower() == "desc")
            //    {
            //        gvTheGrid.GridViewSortDirection = SortDirection.Descending;
            //    }
            //    else
            //    {
            //        gvTheGrid.GridViewSortDirection = SortDirection.Ascending;
            //    }

            //}

        }

       

        //PopulateSearchParams();
        //lnkSearch_Click(null, null);
        BindTheGrid(0, gvTheGrid.PageSize);

        //if (!string.IsNullOrEmpty(_theTable.SPSearchReset))
        //{
        //    SystemData.Table_SPSearchReset(_theTable.SPSearchReset, _ObjUser.UserID, _theView.ViewID, Request.RawUrl);
        //}

    }

    //protected void Pager_DeleteAction(object sender, EventArgs e)
    //{
    //    //Ticket 1013
    //    trUndo.Style.Add("display", "none");
    //    //End

    //    EnsureSecurity();
    //    bool bIsAllCheckeD = false;

    //    bool bHeaderChecked = ((CheckBox)gvTheGrid.HeaderRow.FindControl("chkAll")).Checked;
    //    string sCheck = "";
    //    if (bHeaderChecked)
    //    {
    //        bIsAllCheckeD = true;
    //        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
    //        {
    //            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
    //            if (ischeck)
    //            {
    //                sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
    //            }
    //            else
    //            {
    //                bIsAllCheckeD = false;
    //            }
    //        }

    //    }
    //    else
    //    {
    //        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
    //        {
    //            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
    //            if (ischeck)
    //            {
    //                sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
    //            }
    //        }

    //    }



    //    if (string.IsNullOrEmpty(sCheck))
    //    {
    //        Session["tdbmsgpb"] = "Please select a record.";
    //        if (hfUsingScrol.Value == "yes" && _gvPager != null)
    //        {
    //            BindTheGrid(_gvPager.StartIndex, gvTheGrid.PageSize);
    //        }
    //        //ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select a record.');", true);
    //        return;
    //    }


    //    chkDelateAllEvery.Checked = false;
    //    lblDeleteRestoreMessage.Text = "Are you sure you want to delete selected item(s)?";
    //    chkDelateAllEvery.Text = "I would like to delete EVERY item in this table";
    //    hfParmanentDelete.Value = "no";
    //    lblDeleteMessageNote.Visible = true;

    //    chkDelateAllEvery.Checked = false;
    //    chkDeleteParmanent.Checked = false;
    //    chkUndo.Checked = false;


    //    if (bIsAllCheckeD)
    //    {
    //        trDeleteAllEvery.Visible = true;
    //    }
    //    else
    //    {
    //        trDeleteAllEvery.Visible = false;
    //    }

    //    //trDeleteParmanent.Visible = true;
    //    trUndo.Visible = true;

    //    if (_strRecordRightID == Common.UserRoleType.Administrator
    //                   || _strRecordRightID == Common.UserRoleType.GOD)
    //    {
    //    }
    //    else
    //    {
    //        trDeleteParmanent.Visible = false;
    //        trUndo.Visible = false;
    //    }




    //    if (_bDeleteReason)
    //    {
    //        trDeleteRestoreMessage.Visible = false;
    //        trDeleteReason.Visible = true;
    //    }

    //    txtDeleteReason.Text = "";
    //    mpeDeleteAll.Show();



    //}
    protected void DeleteAction()
    {
        string strExtraWHERE = "";
        if (PageType == "c")
        {
            strExtraWHERE = " " + TextSearchParent;
        }
        bool bchkDelateAllEvery = false;
        if (hfchkDelateAllEvery.Value != "")
            bchkDelateAllEvery = bool.Parse(hfchkDelateAllEvery.Value);

        bool bchkDeleteParmanent = false;
        if (hfchkDeleteParmanent.Value != "")
            bchkDeleteParmanent = bool.Parse(hfchkDeleteParmanent.Value);

        string strDeleteReason = "";

        if (hftxtDeleteReason.Value != "")
        {
            strDeleteReason = hftxtDeleteReason.Value;
        }

        bool bAllChecked = true;
        string sCheck = "";
        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
        {
            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
            if (ischeck)
            {
                sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
            }
            else
            {
                bAllChecked = false;
            }
        }

        /* == Red 20032020: Removing this, makes no sense to include the filter search 
         * because you cant delete what you cant see in the list == */
        //PopulateSearchParams(); // lets get the filter/search
        //strExtraWHERE = strExtraWHERE + TextSearch;
        /* == End Red == */

        // this use to delete virtually and permanent
        DeleteItem(sCheck, bchkDelateAllEvery, strExtraWHERE);
        // lets refresh the grid
        if (bAllChecked)
        {
            lnkReset_Click(null, null);
        }
        else
        {
            BindTheGrid(_gvPager.StartIndex, gvTheGrid.PageSize);
        }
       
    }


    protected void DeleteParmanentAction()
    {

        bool bchkDelateAllEvery = false;
        if (hfchkDelateAllEvery.Value != "")
            bchkDelateAllEvery = bool.Parse(hfchkDelateAllEvery.Value);

        bool bchkDeleteParmanent = false;
        if (hfchkDeleteParmanent.Value != "")
            bchkDeleteParmanent = bool.Parse(hfchkDeleteParmanent.Value);

        string strDeleteReason = "";

        if (hftxtDeleteReason.Value != "")
        {
            strDeleteReason = hftxtDeleteReason.Value;
        }


        string sCheck = "";
        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
        {
            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
            if (ischeck)
            {
                sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
            }
        }


        //if (chkDelateAllEvery.Checked == true)
        //{
        //    DataTable dtAllRecordIDs;

        //    if (PageType == "c")
        //    {
        //        dtAllRecordIDs = Common.DataTableFromText("SELECT RecordID FROM Record WHERE IsActive=0 AND TableID=" + _qsTableID + " " + TextSearchParent);
        //    }
        //    else
        //    {

        //        dtAllRecordIDs = Common.DataTableFromText("SELECT RecordID FROM Record WHERE IsActive=0 AND TableID=" + _qsTableID);
        //    }

        //    sCheck = "";
        //    foreach (DataRow dr in dtAllRecordIDs.Rows)
        //    {
        //        sCheck = sCheck + dr[0].ToString() + ",";
        //    }
        //}



        if (string.IsNullOrEmpty(sCheck))
        {
            ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select a record.');", true);
        }
        else
        {
            if (bchkDelateAllEvery)
            {
                if (PageType == "c")
                {
                    Common.ExecuteText("DELETE Record WHERE IsActive=0 AND TableID=" + _qsTableID + " " + TextSearchParent);
                }
                else
                {

                    Common.ExecuteText("DELETE Record WHERE IsActive=0 AND TableID=" + _qsTableID);
                }
            }
            else
            {
                DeleteParmanentItem(sCheck);
            }


        }

        BindTheGrid(_gvPager.StartIndex, gvTheGrid.PageSize);
        //_gvPager._gridView.PageIndex = _gvPager.PageIndex - 1;
        //if (_gvPager._gridView.Rows.Count == 0 && _gvPager._gridView.PageIndex > 0)
        //{
        //    BindTheGrid(_gvPager.StartIndex - gvTheGrid.PageSize, gvTheGrid.PageSize);
        //}


    }


    //protected void Pager_ShowCog(object sender, EventArgs e)
    //{
    //    mpeCog.Show();
    //}
    protected void SetOtherValidationGroup()
    {

        //edit many related
        //TheDatabase.SetValidationGroup(pnlEditMany.Controls, "EM" + _strDynamictabPart);


        //delete related
        //rfvDeleteReason.ValidationGroup = "DR";
        //lnkDeleteAllOK.ValidationGroup = "DR";
    }
    //protected void Pager_AllExport(object sender, EventArgs e)
    //{
    //    EnsureSecurity();
    //    if (ddlTemplate.Items.Count == 0)
    //    {
    //        //no template, so lets create one

    //        ExportManager.CreateDefaultExportTemplate(TableID);
    //        PopulateExportTemplate(TableID);

    //        ddlTemplate_SelectedIndexChanged(null, null);


    //    }


    //    //mpeExportRecords.Show();
    //}

    //protected void lnkCogOK_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        if (txtPageSize.Text != "")
    //        {
    //            gvTheGrid.PageSize = int.Parse(txtPageSize.Text);
    //            BindTheGrid(0, gvTheGrid.PageSize);
    //        }

    //    }
    //    catch
    //    {

    //    }

    //    mpeCog.Hide();
    //}

    protected void lnkExportRecords_Click(object sender, EventArgs e)
    {
        try
        {
            //mpeExportRecords.Hide();
            //UpdatePanel1.Update();
            //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "hide_AllExpoerMPE", "$find('" + mpeExportRecords.BehaviorID + "').hide();", true);


            //lblExportRecords.Text = "";
            PopulateSearchParams();
            string ddlExportFiletype = "";
            if (Session["ExportClass"] != null)
            {
                ExportClass aExportClass = (ExportClass)Session["ExportClass"];
                ddlExportFiletype = aExportClass.strExportFiletype;
            }

            switch (ddlExportFiletype)
            {
                case "e":
                    Pager_OnExportForExcel(null, null);
                    break;
                case "c":
                    Pager_OnExportForCSV(null, null);
                    break;
                case "w":

                    if (_gvPager != null)
                        _gvPager.ExportWord();

                    break;
                case "p":
                    if (_gvPager != null)
                        _gvPager.ExportPDF();

                    break;
                default:
                    break;
            }

            // Register fancy plugin after exporting
            DoPagerFancyThings();

            Session["ExportClass"] = null;
        }
        catch (Exception ex)
        {
            //
            if (ex.Message.IndexOf("Thread was being aborted") == -1)
            {
                ErrorLog theErrorLog = new ErrorLog(null, "Record export", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }

        }

        //mpeExportRecords.Hide();
    }
    //protected void lnkEditManyCancel2_Click(object sender, EventArgs e)
    //{
    //    lnkSearch_Click(null, null);
    //}


    protected void lnkEditManyOK_Click(object sender, EventArgs e)
    {
        //lblMsgBullk.Text = "";
        string strValue = "";
        bool bchkUpdateEveryItem = false;
        if (hfchkUpdateEveryItem.Value != "")
            bchkUpdateEveryItem = bool.Parse(hfchkUpdateEveryItem.Value);


        if (hfddlYAxisBulk.Value == "")
        {
            Session["tdbgmsgpb"] = "Please select a column.";
            //mpeEditMany.Show();
            return;
        }
        else
        {
            Column theColumn = RecordManager.ets_Column_Details(int.Parse(hfddlYAxisBulk.Value));

            strValue = hfBulkValue.Value;

            if (strValue == "")
            {
                Session["tdbgmsgpb"] = "Please enter the new value.";
                //mpeEditMany.Show();
                return;
            }

            string sCheck = "";

            bool bAll = ((CheckBox)gvTheGrid.HeaderRow.FindControl("chkAll")).Checked;


            if (bchkUpdateEveryItem && bAll)
            {
                //DataTable dtTable
                //Common.ExecuteText("UPDATE Record SET " + theColumn.SystemName + "='" + strValue.Replace("'", "''") 
                //    + "' WHERE TableID=" + theColumn.TableID.ToString());

                //gvTheGrid.PageIndex = 0;// why???
                //BindTheGridForExport(0, _gvPager.TotalRows);
                int iOldPS = gvTheGrid.PageSize;
                gvTheGrid.PageSize = _gvPager.TotalRows + 1;
                BindTheGrid(0, _gvPager.TotalRows);
                for (int i = 0; i < gvTheGrid.Rows.Count; i++)
                {

                    sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";

                }
                sCheck = sCheck + "-1";
                gvTheGrid.PageSize = iOldPS;
            }
            else
            {
                for (int i = 0; i < gvTheGrid.Rows.Count; i++)
                {
                    bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
                    if (ischeck)
                    {
                        sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
                    }
                }
                sCheck = sCheck + "-1";

            }
            DateTime dtRightNow = DateTime.Now;
            RecordManager.Record_Audit(null, sCheck, false, dtRightNow);

            //KG 25/10/17 Ticket 3251  --  //",DateUpdated='" + DateTime.Now.ToString() + "'" +  <<-- removed by MR
            //Common.ExecuteText("UPDATE Record SET " + theColumn.SystemName + "='" + strValue.Replace("'", "''") + "' WHERE RecordID IN (" + sCheck + ")");
            Common.ExecuteText("UPDATE Record SET " + theColumn.SystemName + "='" + strValue.Replace("'", "''") + "'" +
                ",LastUpdatedUserID=" + _ObjUser.UserID.ToString() +
                ",DateUpdated=GETDATE()" +
                " WHERE RecordID IN (" + sCheck + ")");


            //KG 11/6/18 run SPSaveRecord on each updated record
            if (_theTable.SPSaveRecord != "")
            {
                try
                {
                    string[] ids = sCheck.Split(',');
                    foreach (string id in ids)
                    {
                        if (id != "-1")
                            RecordManager.Table_SPSaveRecord(_theTable.SPSaveRecord,
                                  int.Parse(id), (int)_ObjUser.UserID,"M", theColumn.ColumnID);
                    }
                }
                catch
                {
                    //
                }
            }


            //            DataTable dtRecords = Common.DataTableFromText(@"SELECT " + theColumn.SystemName + @",RecordID,WarningResults,ValidationResults FROM Record
            //	                    WHERE  RecordID IN (" + sCheck + @")");
            if (_theTable.UniqueColumnID != null && (theColumn.ColumnID == _theTable.UniqueColumnID ||
                (_theTable.UniqueColumnID2 != null && theColumn.ColumnID == _theTable.UniqueColumnID2)))
            {
                UploadManager.Record_Set_UniqueKey("R", _theTable.TableID, null, null, sCheck);
            }
            string strInvalidRecordIDs = "";
            if (theColumn.ColumnType == "number")
            {
                bool bHasValidation = false;
                string strHasAdvancedCondition = Common.GetValueFromSQL(
                    "SELECT TOP 1 AdvancedConditionID FROM AdvancedCondition WHERE ConditionType = 'notification' AND ColumnID = " +
                    theColumn.ColumnID.ToString());
                if (!String.IsNullOrEmpty(strHasAdvancedCondition))
                {
                    bHasValidation = true;
                }
                else if (theColumn.ValidationOnEntry != "" || theColumn.ValidationOnExceedance != "" || theColumn.ValidationOnWarning != "")
                {
                    bHasValidation = true;
                }
                else
                {
                    string strHasCondition = Common.GetValueFromSQL("SELECT TOP 1 ConditionID FROM Condition WHERE ColumnID=" + theColumn.ColumnID.ToString());
                    if (strHasCondition != "")
                    {
                        bHasValidation = true;
                    }
                }
                if (bHasValidation)
                {
                    string strValidRecordIDs = "";
                    string strSQL = @"SELECT " + theColumn.SystemName + @",RecordID,WarningResults,ValidationResults FROM Record
	                    WHERE  RecordID IN (" + sCheck + @")";
                    RecordManager.ets_AdjustValidFormulaChanges(theColumn, ref strInvalidRecordIDs, ref strValidRecordIDs, true, strSQL);
                }
            }


            //ticket 1818
            //UpdateAllCalculationFields();

            //skip columns that are not related with Calculation - MR
            DataTable dtCalColumns = Common.DataTableFromText("SELECT ColumnID FROM [Column] WHERE TableID="
                + theColumn.TableID.ToString() + " AND CHARINDEX('" + theColumn.SystemName + "]',Calculation,0)>0");

            if (dtCalColumns != null && dtCalColumns.Rows.Count > 0)
            {
                if (bchkUpdateEveryItem && bAll)
                {
                    UpdateAllCalculationFields();
                }
                else
                {
                    UpdateSelectedCalculationFields();
                }
            }


            RecordManager.Record_Audit(null, sCheck, true, dtRightNow);

            if (strInvalidRecordIDs != "")
            {
                Session["tdbmsgpb"] = "Total invalid records:" + (strInvalidRecordIDs.Split(',').Length - 1).ToString();
            }

            //need this to reload the page... -- Red
            //return the BindTheGrid... -  Red
            //Response.Redirect(Request.RawUrl, true);

            BindTheGrid(_gvPager.StartIndex, gvTheGrid.PageSize);
            //lnkSearch_Click(null, null);
            //mpeEditMany.Hide();

        }



    }



    ////ticket 1818
    //public void UpdateAllCalculationFields()
    //{
    //    DataTable dtColumn = Common.DataTableFromText("SELECT ColumnID,  SystemName, Calculation,RoundNumber,TextType,DateCalculationType FROM [Column] WHERE Calculation IS NOT NULL AND TableID=" + _qsTableID);
    //    DataTable dtRecord = Common.DataTableFromText("SELECT RecordID FROM [Record] WHERE TableID=" + _qsTableID);

    //    _dtColumnsAll = RecordManager.ets_Table_Columns_All(int.Parse(_qsTableID));
    //    _theTable = RecordManager.ets_Table_Details(int.Parse(_qsTableID));

    //    //int RoundNum = int.Parse(dtColumn.Rows[0][2].ToString());
    //    if (dtColumn.Rows.Count > 0)
    //    {
    //        for (int x = 0; x < dtColumn.Rows.Count; x++)
    //        {
    //            string ColumnID = dtColumn.Rows[x][0].ToString();
    //            string SystemName = dtColumn.Rows[x][1].ToString();
    //            string CalcFormula = dtColumn.Rows[x][2].ToString();
    //            int RoundNumber = int.Parse(dtColumn.Rows[x][3].ToString());
    //            string TextType = dtColumn.Rows[x][4].ToString();
    //            string DateCalculationType = dtColumn.Rows[x][5].ToString();
    //            Column theColumn = RecordManager.ets_Column_Details(int.Parse(ColumnID));

    //            for (int i = 0; i < dtRecord.Rows.Count; i++)
    //            {
    //                string RecordID = dtRecord.Rows[i][0].ToString();
    //                Record editRecord = RecordManager.ets_Record_Detail_Full(int.Parse(RecordID));
    //                _iParentRecordID = int.Parse(RecordID);


    //                string strValue = "";
    //                string strTempValue = "";
    //                //string strOrginalValue = "";

    //                //if (TextType != "" && _dtColumnsDetail.Rows[i]["TextType"].ToString().ToLower() == "d")
    //                if (TextType == "d")
    //                {
    //                    //datetime calculation
    //                    string strCalculation = CalcFormula;// _dtColumnsDetail.Rows[i]["Calculation"].ToString();
    //                    try
    //                    {
    //                        strTempValue = TheDatabaseS.GetDateCalculationResult(ref _dtColumnsAll, strCalculation, null, editRecord, _iParentRecordID,
    //                       DateCalculationType == "" ? "" : DateCalculationType,
    //                       null, _theTable, false);

    //                        strValue = strTempValue;
    //                        Common.ExecuteText("UPDATE Record SET " + SystemName + "=" + "'" + strValue + "'" + " WHERE RecordID=" + RecordID);
    //                    }
    //                    catch
    //                    {
    //                        //
    //                    }

    //                }
    //                else if (TextType == "t")
    //                {
    //                    //text calculation
    //                    try
    //                    {
    //                        string strFormula = Common.GetCalculationSystemNameOnly(CalcFormula, (int)_theTable.TableID);

    //                        //Column theColumn = RecordManager.ets_Column_Details(int.Parse(ColumnID));
    //                        strTempValue = TheDatabaseS.GetTextCalculationResult(ref _dtColumnsAll, strFormula, null, editRecord, _iParentRecordID, null, _theTable, theColumn);
    //                        strValue = strTempValue;
    //                        Common.ExecuteText("UPDATE Record SET " + SystemName + "=" + "'" + strValue + "'" + " WHERE RecordID=" + RecordID);
    //                    }
    //                    catch
    //                    {
    //                        //
    //                    }

    //                }
    //                else
    //                {
    //                    //number calculation
    //                    try
    //                    {
    //                        Common.ExecuteText("UPDATE Record SET " + SystemName + "=" + CalcFormula + " WHERE RecordID=" + RecordID);
    //                    }
    //                    catch
    //                    {
    //                        //
    //                    }
    //                }
    //                formatOutput(SystemName, RecordID);
    //            }


    //        }
    //    }
    //}

    //ticket 1818
    public void formatOutput(string SystemName, string RecordID)
    {
        try
        {
            DataTable dtRecord = Common.DataTableFromText("SELECT  " + SystemName + " FROM [Record] WHERE RecordID=" + RecordID);
            if (dtRecord.Rows.Count > 0)
            {
                string Value = dtRecord.Rows[0][0].ToString();
                double dTemp = 0;
                if (double.TryParse(Value, out dTemp))
                {
                    Value = string.Format("{0:#,##0.00}", double.Parse(Value));
                }

                Common.ExecuteText("UPDATE Record SET " + SystemName + "=" + "'" + Value + "'" + " WHERE RecordID=" + RecordID);
            }
        }
        catch
        { }

    }


    //ticket 1818
    public void UpdateAllCalculationFields()
    {
        Column theUpdatingColumn = RecordManager.ets_Column_Details(int.Parse(hfddlYAxisBulk.Value));
        DataTable dtColumn = Common.DataTableFromText("SELECT  ColumnID,  SystemName, Calculation,RoundNumber,TextType,DateCalculationType FROM [Column] WHERE Calculation IS NOT NULL AND TableID="
            + _qsTableID + " AND CHARINDEX('" + theUpdatingColumn.SystemName + "]',Calculation,0)>0");

        DataTable dtRecord = Common.DataTableFromText("SELECT RecordID FROM [Record] WHERE TableID=" + _qsTableID);
        _dtColumnsAll = RecordManager.ets_Table_Columns_All(int.Parse(_qsTableID));
        _theTable = RecordManager.ets_Table_Details(int.Parse(_qsTableID));
        if (dtColumn.Rows.Count > 0)
        {
            for (int x = 0; x < dtColumn.Rows.Count; x++)
            {
                string ColumnID = dtColumn.Rows[x][0].ToString();
                string strCalSystemName = dtColumn.Rows[x][1].ToString();
                string CalcFormula = dtColumn.Rows[x][2].ToString();
                int RoundNumber = int.Parse(dtColumn.Rows[x][3].ToString());
                string TextType = dtColumn.Rows[x][4].ToString();
                string DateCalculationType = dtColumn.Rows[x][5].ToString();
                Column theCalColumn = RecordManager.ets_Column_Details(int.Parse(ColumnID));

                for (int i = 0; i < dtRecord.Rows.Count; i++)
                {
                    string RecordID = dtRecord.Rows[i][0].ToString();
                    Record editRecord = RecordManager.ets_Record_Detail_Full(int.Parse(RecordID), int.Parse(_qsTableID), bUseArchiveData);  // Red 08012018
                    _iParentRecordID = int.Parse(RecordID);


                    string strValue = "";
                    string strTempValue = "";

                    if (TextType == "d")
                    {
                        //datetime calculation
                        string strCalculation = CalcFormula;
                        try
                        {
                            strTempValue = TheDatabaseS.GetDateCalculationResult(ref _dtColumnsAll, strCalculation, null, editRecord, _iParentRecordID,
                            DateCalculationType == "" ? "" : DateCalculationType,
                            null, _theTable, false);
                            strValue = strTempValue;
                        }
                        catch
                        { }
                    }
                    else if (TextType == "t")
                    {
                        //text calculation
                        try
                        {
                            string strFormula = Common.GetCalculationSystemNameOnly(CalcFormula, (int)_theTable.TableID);
                            strTempValue = TheDatabaseS.GetTextCalculationResult(ref _dtColumnsAll, strFormula, null, editRecord, _iParentRecordID, null, _theTable, theCalColumn);
                            strValue = strTempValue;

                        }
                        catch
                        { }

                    }
                    else
                    {
                        try
                        {

                            if (CalcFormula.Contains("{"))
                            {
                                Regex r = new Regex(@"\{(V\d{3})\}");
                                MatchCollection matches = r.Matches(CalcFormula);
                                for (int nMatch = 0; nMatch < matches.Count; nMatch++)
                                {
                                    string v = String.Empty;
                                    v = RecordManager.GetRecordValue(ref editRecord, matches[nMatch].Value.Replace("{", "").Replace("}", ""));

                                    CalcFormula = CalcFormula.Replace(matches[nMatch].Value, "'" + v + "'");
                                }
                                // "dbo.": a simple protection against SQL Injection
                                strValue = Common.GetValueFromSQL(String.Format("SELECT dbo.{0}", CalcFormula));
                            }
                            else
                            {
                                string strFormula = Common.GetCalculationSystemNameOnly(CalcFormula, (int)_theTable.TableID);

                                strTempValue = TheDatabaseS.GetCalculationResult(ref _dtColumnsAll, strFormula, null, editRecord, _iParentRecordID, null, _theTable, theCalColumn);
                                strValue = strTempValue;
                            }
                        }
                        catch
                        { }

                    }

                    double dTemp = 0;
                    if (double.TryParse(strValue, out dTemp))
                    {
                        strValue = string.Format("{0:#,##0.00}", dTemp);
                    }
                    RecordManager.MakeTheRecord(ref editRecord, strCalSystemName, strValue);
                    RecordManager.ets_Record_Update(editRecord, false);


                }


            }
        }
    }

    //ticket 1818
    public void UpdateSelectedCalculationFields()
    {
        Column theUpdatingColumn = RecordManager.ets_Column_Details(int.Parse(hfddlYAxisBulk.Value));
        DataTable dtColumn = Common.DataTableFromText("SELECT  ColumnID,  SystemName, Calculation,RoundNumber,TextType,DateCalculationType FROM [Column] WHERE Calculation IS NOT NULL AND TableID="
            + _qsTableID + " AND CHARINDEX('" + theUpdatingColumn.SystemName + "]',Calculation,0)>0");

        _dtColumnsAll = RecordManager.ets_Table_Columns_All(int.Parse(_qsTableID));
        _theTable = RecordManager.ets_Table_Details(int.Parse(_qsTableID));

        if (dtColumn.Rows.Count > 0)
        {
            for (int x = 0; x < dtColumn.Rows.Count; x++)
            {
                string strCalColumnID = dtColumn.Rows[x][0].ToString();
                string strCalSystemName = dtColumn.Rows[x][1].ToString();
                string CalcFormula = dtColumn.Rows[x][2].ToString();
                int RoundNumber = int.Parse(dtColumn.Rows[x][3].ToString());
                string TextType = dtColumn.Rows[x][4].ToString();
                string DateCalculationType = dtColumn.Rows[x][5].ToString();
                Column theCalColumn = RecordManager.ets_Column_Details(int.Parse(strCalColumnID));

                for (int i = 0; i < gvTheGrid.Rows.Count; i++)
                {

                    //string RecordID = dtRecord.Rows[i][0].ToString();


                    bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;

                    //Update if checkbox is check
                    if (ischeck)
                    {

                        string RecordID = ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text;
                        Record editRecord = RecordManager.ets_Record_Detail_Full(int.Parse(RecordID), int.Parse(_qsTableID), bUseArchiveData);  // Red 08012018
                        //_iParentRecordID = int.Parse(RecordID);

                        string strValue = "";
                        string strTempValue = "";
                        //string strOrginalValue = "";

                        //if (TextType != "" && _dtColumnsDetail.Rows[i]["TextType"].ToString().ToLower() == "d")
                        if (TextType == "d")
                        {
                            //datetime calculation
                            string strCalculation = CalcFormula;// _dtColumnsDetail.Rows[i]["Calculation"].ToString();
                            try
                            {
                                strTempValue = TheDatabaseS.GetDateCalculationResult(ref _dtColumnsAll, strCalculation, null, editRecord, _iParentRecordID,
                               DateCalculationType == "" ? "" : DateCalculationType,
                               null, _theTable, false);

                                strValue = strTempValue;
                                //Common.ExecuteText("UPDATE Record SET " + strCalSystemName + "=" + "'" + strValue + "'" + " WHERE RecordID=" + RecordID);
                            }
                            catch
                            {
                                //
                            }

                        }
                        else if (TextType == "t")
                        {
                            //text calculation
                            try
                            {
                                string strFormula = Common.GetCalculationSystemNameOnly(CalcFormula, (int)_theTable.TableID);

                                //Column theColumn = RecordManager.ets_Column_Details(int.Parse(ColumnID));
                                strTempValue = TheDatabaseS.GetTextCalculationResult(ref _dtColumnsAll, strFormula, null, editRecord, _iParentRecordID, null, _theTable, theCalColumn);
                                strValue = strTempValue;
                                //Common.ExecuteText("UPDATE Record SET " + strCalSystemName + "=" + "'" + strValue + "'" + " WHERE RecordID=" + RecordID);
                            }
                            catch
                            {
                                //
                            }

                        }
                        else
                        {
                            //number calculation
                            try
                            {

                                if (CalcFormula.Contains("{"))
                                {
                                    Regex r = new Regex(@"\{(V\d{3})\}");
                                    MatchCollection matches = r.Matches(CalcFormula);
                                    for (int nMatch = 0; nMatch < matches.Count; nMatch++)
                                    {
                                        string v = String.Empty;
                                        v = RecordManager.GetRecordValue(ref editRecord, matches[nMatch].Value.Replace("{", "").Replace("}", ""));

                                        CalcFormula = CalcFormula.Replace(matches[nMatch].Value, "'" + v + "'");
                                    }
                                    // "dbo.": a simple protection against SQL Injection
                                    strValue = Common.GetValueFromSQL(String.Format("SELECT dbo.{0}", CalcFormula));
                                }
                                else
                                {
                                    string strFormula = Common.GetCalculationSystemNameOnly(CalcFormula, (int)_theTable.TableID);

                                    strTempValue = TheDatabaseS.GetCalculationResult(ref _dtColumnsAll, strFormula, null, editRecord, _iParentRecordID, null, _theTable, theCalColumn);
                                    strValue = strTempValue;
                                }
                            }
                            catch
                            {
                                //
                            }
                            //try
                            //{
                            //    Common.ExecuteText("UPDATE Record SET " + SystemName + "=" + CalcFormula + " WHERE RecordID=" + RecordID);
                            //}
                            //catch
                            //{
                            //    //
                            //}
                        }
                        double dTemp = 0;
                        if (double.TryParse(strValue, out dTemp))
                        {
                            strValue = string.Format("{0:#,##0.00}", dTemp);
                        }
                        RecordManager.MakeTheRecord(ref editRecord, strCalSystemName, strValue);
                        RecordManager.ets_Record_Update(editRecord, false);
                        //formatOutput(strCalSystemName, RecordID);

                    }


                }
            }
        }
    }


    protected void lnkDeleteAllOK_Click(object sender, EventArgs e)
    {
        if (chkShowDeletedRecords.Checked && hfParmanentDelete.Value == "no")
        {
            UnDeleteAction(); // i think we do not need to worry this
        }
        else
        {
            DeleteAction();
        }

    }



    //protected void btnAdd_Click(object sender, EventArgs e)
    //{
    //    mpeAddRecord.Show();
    //}
    //protected void lnkDeleteAllNo_Click(object sender, EventArgs e)
    //{
    //    if (hfUsingScrol.Value == "yes")
    //    {
    //        lnkSearch_Click(null, null);
    //    }
    //    else
    //    {

    //        if (chkIsActive.Checked)
    //        {
    //            if (_gvPager != null)
    //            {
    //                _gvPager.HideDelete = true;
    //                _gvPager.HideUnDelete = false;
    //                if (_strRecordRightID == Common.UserRoleType.Administrator
    //                        || _strRecordRightID == Common.UserRoleType.GOD)
    //                {

    //                    _gvPager.HideParmanentDelete = false;

    //                }
    //                ShowHidePermanentDelete();
    //            }

    //        }
    //    }


    //    chkDelateAllEvery.Checked = false;
    //    mpeDeleteAll.Hide();
    //}

    protected void Pager_CopyRecordAction(object sender, EventArgs e)
    {
        //EnsureSecurity();
        string sCheck = "";
        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
        {
            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
            if (ischeck)
            {
                sCheck = ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text;
                break;
            }
        }


        if (string.IsNullOrEmpty(sCheck))
        {
            ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select (tick) a record you want to copy.');", true);
            return;
        }
        else
        {
            //record found
            string strCopyAddURL = GetAddURL();// +"&CopyRecordID=" + Cryptography.Encrypt(sCheck);

            Session["CopyRecordID"] = Cryptography.Encrypt(sCheck);

            if (_bOpenInParent == false)
            {
                Response.Redirect(strCopyAddURL, false);
            }
            else
            {
                //this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "myUniqueKey",
                //    "self.parent.location='" + GetAddURL() + "&CopyRecordID=" + Cryptography.Encrypt(sCheck) + "';", true);

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "strCopyAddURL", "window.parent.location.href='" + strCopyAddURL + "';", true);

            }
        }
    }
    protected void Pager_EditManyAction(object sender, EventArgs e)
    {
        //EnsureSecurity();
        string sCheck = "";
        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
        {
            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
            if (ischeck)
            {
                sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
            }
        }

        if (string.IsNullOrEmpty(sCheck))
        {
            ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select (tick) the records you want to change.');", true);
            return;
        }


        string strBulkUpdateSQL = SystemData.SystemOption_ValueByKey_Account("BulkUpdateSQL", null, int.Parse(TableID.ToString()));

        //string sCheck = "";
        if (strBulkUpdateSQL != "")
        {
            for (int i = 0; i < gvTheGrid.Rows.Count; i++)
            {
                bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
                if (ischeck)
                {
                    string strRecordID = ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text;
                    try
                    {
                        Common.ExecuteText(strBulkUpdateSQL.Replace("@RecordID", strRecordID));
                    }
                    catch
                    {
                        //
                    }

                }
            }
            lnkSearch_Click(null, null);


            ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Records have been updated.');", true);

            return;

        }



        bool bAll = ((CheckBox)gvTheGrid.HeaderRow.FindControl("chkAll")).Checked;

        //if (bAll)
        //{
        //    trUpdateEveryItem.Visible = true;
        //}
        //else
        //{
        //    trUpdateEveryItem.Visible = false;
        //}

        //mpeEditMany.Show();


    }

    protected void Pager_OnSendEmailAction(object sender, EventArgs e)
    {
        //EnsureSecurity();
        string sCheck = "";

        bool bHeaderChecked = ((CheckBox)gvTheGrid.HeaderRow.FindControl("chkAll")).Checked;

        if (bHeaderChecked == false)// || /*RP Added Ticket 4378*/gvTheGrid.Rows.Count == 1/*End Modification*/)
        {
            for (int i = 0; i < gvTheGrid.Rows.Count; i++)
            {
                bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
                if (ischeck)
                {
                    sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
                }
            }
        }
        else
        {
            //RP Added Ticket 4378 - Set TextSearch value based on Search Parameters
            PopulateSearchParams();
            //End Modification
            int iTN = 0;
            string strReturnSQL = "";
            Exception exParam = null;
            DataTable dtTemp = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
                       ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
                       !chkShowDeletedRecords.Checked,
                       chkShowOnlyWarning.Checked == false ? null : (bool?)true,
                       null, null,
                         "DBGSystemRecordID", "DESC", 0, _gvPager.TotalRows, ref iTN, ref _iTotalDynamicColumns,
                         _strListType, _strNumericSearch, TextSearch + TextSearchParent,
                       _dtDateFrom, _dtDateTo, "", "", "", int.Parse(hfViewID.Value), ref strReturnSQL, ref strReturnSQL, ref exParam, bool.Parse(ViewState["bUseArchiveData"].ToString()));  // Red 08012018

            foreach (DataRow drR in dtTemp.Rows)
            {
                sCheck = sCheck + drR["DBGSystemRecordID"].ToString() + ",";
            }
        }


        if (string.IsNullOrEmpty(sCheck))
        {
            ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select a record.');", true);
            return;
        }

        string strSC = "";

        if (PageType == "p")
        {
            strSC = "&SearchCriteria=" + Cryptography.Encrypt(SearchCriteriaID.ToString());
        }
        else
        {
            if (Request.QueryString["SearchCriteria"] != null)
            {
                strSC = "&SearchCriteria=" + Request.QueryString["SearchCriteria"].ToString();
            }
        }


        string xml = null;
        xml = @"<root>" +
              " <recordids>" + HttpUtility.HtmlEncode(sCheck) + "</recordids>" +
                "</root>";

        SearchCriteria theSearchCriteria = new SearchCriteria(null, xml);
        int iSearchCriteriaID = SystemData.SearchCriteria_Insert(theSearchCriteria);
        string strSendEmailURl = "";
        if (PageType == "p")
        {
            //Response.Redirect(Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/SendEmail.aspx?TableID=" + Cryptography.Encrypt(TableID.ToString()) + "&recordids=" + Cryptography.Encrypt(iSearchCriteriaID.ToString()) + strSC, false);
            strSendEmailURl = "/Pages/Record/SendEmail.aspx?TableID=" + Cryptography.Encrypt(TableID.ToString()) + "&recordids=" + Cryptography.Encrypt(iSearchCriteriaID.ToString()) + strSC;
        }
        else
        {
            //if (Session["stackURL"] != null)
            //{
            //    Stack<string> stack = (Stack<string>)Session["stackURL"];
            //    if (stack.Count > 0)
            //    {
            //        stack.Pop();
            //        Session["stackURL"] = stack;
            //    }
            //}
            strSendEmailURl = "/Pages/Record/SendEmail.aspx?TableID=" + Cryptography.Encrypt(TableID.ToString()) + "&recordids=" + Cryptography.Encrypt(iSearchCriteriaID.ToString()) + strSC + "&fixedurl=" + Cryptography.Encrypt(Request.RawUrl) + "&tabindex=" + DetailTabIndex.ToString();
            //Response.Redirect(Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/SendEmail.aspx?TableID=" + Cryptography.Encrypt(TableID.ToString()) + "&recordids=" + Cryptography.Encrypt(iSearchCriteriaID.ToString()) + strSC + "&fixedurl=" + Cryptography.Encrypt(Request.RawUrl) + "&tabindex=" + DetailTabIndex.ToString(), false);

        }

        if (strSendEmailURl != "")
        {
            strSendEmailURl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + strSendEmailURl;
            if (_bOpenInParent == false)
            {
                Response.Redirect(strSendEmailURl, false);
            }
            else
            {
                strSendEmailURl = strSendEmailURl + "&fixedurl=" + Cryptography.Encrypt("~/Default.aspx?a=1");
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "strSendEmailURl", "window.parent.location.href='" + strSendEmailURl + "';", true);

            }
        }

    }
    //protected void Pager_UnDeleteAction(object sender, EventArgs e)
    //{
    //    EnsureSecurity();
    //    bool bIsAllCheckeD = false;

    //    bool bHeaderChecked = ((CheckBox)gvTheGrid.HeaderRow.FindControl("chkAll")).Checked;
    //    string sCheck = "";
    //    if (bHeaderChecked)
    //    {
    //        bIsAllCheckeD = true;
    //        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
    //        {
    //            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
    //            if (ischeck)
    //            {
    //                sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
    //            }
    //            else
    //            {
    //                bIsAllCheckeD = false;
    //            }
    //        }

    //    }
    //    else
    //    {
    //        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
    //        {
    //            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
    //            if (ischeck)
    //            {
    //                sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
    //            }
    //        }

    //    }

    //    if (bIsAllCheckeD)
    //    {
    //        trDeleteAllEvery.Visible = true;
    //    }
    //    else
    //    {
    //        trDeleteAllEvery.Visible = false;
    //    }

    //    if (_gvPager != null)
    //    {
    //        _gvPager.HideDelete = true;
    //        _gvPager.HideUnDelete = false;
    //        if (_strRecordRightID == Common.UserRoleType.Administrator
    //                   || _strRecordRightID == Common.UserRoleType.GOD)
    //        {
    //            if (chkIsActive.Checked)
    //            {
    //                _gvPager.HideParmanentDelete = false;
    //            }
    //        }
    //        ShowHidePermanentDelete();
    //    }

    //    if (string.IsNullOrEmpty(sCheck))
    //    {
    //        ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select a record.');", true);
    //        return;
    //    }


    //    chkDelateAllEvery.Checked = false;

    //    lblDeleteRestoreMessage.Text = "Are you sure you want to restore selected item(s)?";
    //    chkDelateAllEvery.Text = "I would like to restore EVERY item in this table";
    //    lblDeleteMessageNote.Visible = false;

    //    hfParmanentDelete.Value = "no";


    //    chkDelateAllEvery.Checked = false;
    //    chkDeleteParmanent.Checked = false;
    //    chkUndo.Checked = false;

    //    trDeleteParmanent.Visible = false;
    //    trUndo.Visible = false;

    //    if (_strRecordRightID == Common.UserRoleType.Administrator
    //                   || _strRecordRightID == Common.UserRoleType.GOD)
    //    {
    //    }
    //    else
    //    {
    //        trDeleteParmanent.Visible = false;
    //        trUndo.Visible = false;
    //    }


    //    trDeleteRestoreMessage.Visible = true;
    //    trDeleteReason.Visible = false;

    //    //mpeDeleteAll.Show();


    //}




    //protected void Pager_OnParmanenetDelAction(object sender, EventArgs e)
    //{

    //    bool bIsAllCheckeD = false;

    //    bool bHeaderChecked = ((CheckBox)gvTheGrid.HeaderRow.FindControl("chkAll")).Checked;
    //    string sCheck = "";
    //    if (bHeaderChecked)
    //    {
    //        //Ticket 1013
    //        trUndo.Style.Add("display", "table-row");
    //        //End

    //        bIsAllCheckeD = true;
    //        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
    //        {
    //            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
    //            if (ischeck)
    //            {
    //                sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
    //            }
    //            else
    //            {
    //                bIsAllCheckeD = false;
    //            }
    //        }

    //    }
    //    else
    //    {
    //        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
    //        {
    //            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
    //            if (ischeck)
    //            {
    //                sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
    //            }
    //        }
    //    }



    //    if (_gvPager != null)
    //    {
    //        _gvPager.HideDelete = true;
    //        _gvPager.HideUnDelete = false;
    //        _gvPager.HideParmanentDelete = false;

    //        ShowHidePermanentDelete();
    //    }

    //    if (string.IsNullOrEmpty(sCheck))
    //    {
    //        ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select a record.');", true);
    //        return;
    //    }

    //    trDeleteAllEvery.Visible = true;
    //    chkDelateAllEvery.Checked = false;

    //    lblDeleteRestoreMessage.Text = "Are you sure you want to PERMANENTLY delete the selected item(s)?";
    //    chkDelateAllEvery.Text = "I would like to PERMANENTLY delete EVERY item in this table";

    //    hfParmanentDelete.Value = "yes";

    //    chkDelateAllEvery.Checked = false;
    //    chkDeleteParmanent.Checked = false;
    //    chkUndo.Checked = false;


    //    trDeleteParmanent.Visible = false;
    //    trUndo.Visible = true;
    //    if (bIsAllCheckeD)
    //    {
    //        trDeleteAllEvery.Visible = true;
    //    }
    //    else
    //    {
    //        trDeleteAllEvery.Visible = false;
    //    }


    //    if (_strRecordRightID == Common.UserRoleType.Administrator
    //                   || _strRecordRightID == Common.UserRoleType.GOD)
    //    {
    //    }
    //    else
    //    {
    //        trDeleteParmanent.Visible = false;
    //        trUndo.Visible = false;
    //    }

    //    mpeDeleteAll.Show();

    //}

    protected void chkShowAdvancedOptions_OnCheckedChanged(Object sender, EventArgs args)
    {
        lnkReset_Click(null, null);

        if (!chkShowAdvancedOptions.Checked)
        {
            //Red Ticket 2501 reset control columnsearch when unticked advanced search        
            cbcSearchMain.ddlYAxisV = "";
            cbcSearch1.ddlYAxisV = "";
            cbcSearch2.ddlYAxisV = "";
            cbcSearch3.ddlYAxisV = "";
            hfAndOr1.Value = "";
            hfAndOr2.Value = "";
            hfAndOr3.Value = "";

            hfControlB_DDL.Value = "";
            hfDDLcbcSearch1.Value = "";
            hfDDLcbcSearch2.Value = "";
            hfDDLcbcSearch3.Value = "";

        
            string strJSSearchShowHideorig = "$('#" + trSearch1.ClientID + "').hide();$('#" + trSearch2.ClientID + "').hide();$('#" + trSearch3.ClientID + "').hide();";
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "PutDefaultSearcUI_CS", strJSSearchShowHideorig, true);
        //}
        }      
        
    }

    //protected void chkDeleteParmanent_OnCheckedChanged(Object sender, EventArgs args)
    //{
    //    if (chkDeleteParmanent.Checked)
    //    {
    //        trUndo.Visible = false;
    //    }
    //    else
    //    {
    //        trUndo.Visible = true;
    //    }

    //    mpeDeleteAll.Show();
    //}
    protected void UnDeleteAction()
    {
        bool bchkDelateAllEvery = false;
        if (hfchkDelateAllEvery.Value != "")
            bchkDelateAllEvery = bool.Parse(hfchkDelateAllEvery.Value);

        bool bAllChecked = true;
        string sCheck = "";
        for (int i = 0; i < gvTheGrid.Rows.Count; i++)
        {
            bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
            if (ischeck)
            {
                sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
            }
            else
            {
                bAllChecked = false;
            }
        }

        if (bAllChecked && bchkDelateAllEvery)
        {
            DataTable dtAllRecordIDs;

            if (PageType == "c")
            {
                dtAllRecordIDs = Common.DataTableFromText("SELECT RecordID FROM Record WHERE IsActive=0 AND TableID=" + _qsTableID + " " + TextSearchParent);
            }
            else
            {
                dtAllRecordIDs = Common.DataTableFromText("SELECT RecordID FROM Record WHERE IsActive=0 AND TableID=" + _qsTableID);
            }

            sCheck = "";
            foreach (DataRow dr in dtAllRecordIDs.Rows)
            {
                sCheck = sCheck + dr[0].ToString() + ",";
            }

            //string strExtraWHERE = "";
            //if (PageType == "c")
            //    strExtraWHERE = " " + TextSearchParent;
        }

        if (string.IsNullOrEmpty(sCheck))
        {
            // ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select a record.');", true);
            Session["tdbmsgpb"] = "Please select a record.";
        }
        else
        {
            RestoreRecords(sCheck);

        }


        BindTheGrid(_gvPager.StartIndex, gvTheGrid.PageSize);
        //_gvPager._gridView.PageIndex = _gvPager.PageIndex - 1;
        //if (_gvPager._gridView.Rows.Count == 0 && _gvPager._gridView.PageIndex > 0)
        //{
        //    BindTheGrid(_gvPager.StartIndex - gvTheGrid.PageSize, gvTheGrid.PageSize);
        //}

    }


    private void DeleteItem(string strRecordIDs, bool bDeleteAll, string strExtraWhere)
    {
        try
        {
            //red 3090_2 09112017
            //red 04092017
            //string strPreventWhere = "";
            //string strPreventValue = _theTable.PreventSpecificColumnDeletionValue;
            //int? strParentColumnID = _theTable.PreventSpecificColumnIDDeletion;
            //string PreventOperator = _theTable.PreventSpecificColumnDeletionOperator;



            //if (strParentColumnID != null && strPreventValue != "")
            //{
            //    Column theFilterParentColumnID = RecordManager.ets_Column_Details(strParentColumnID ?? 0);

            //    if (PreventOperator == "contains")
            //    {
            //        strPreventWhere = " AND Record." + theFilterParentColumnID.SystemName + " NOT LIKE '%" + strPreventValue.Replace("'", "''") + "%'";
            //    }
            //    else if (PreventOperator == "greaterthan")
            //    {
            //        strPreventWhere = " AND ( IsNumeric(Record." + theFilterParentColumnID.SystemName + ")=1 AND  CONVERT(decimal(20,10),Record." + theFilterParentColumnID.SystemName + ") < " + Common.IgnoreSymbols(strPreventValue) + ")";
            //    }
            //    else if (PreventOperator == "lessthan")
            //    {
            //        strPreventWhere = " AND ( IsNumeric(Record." + theFilterParentColumnID.SystemName + ")=1 AND  CONVERT(decimal(20,10),Record." + theFilterParentColumnID.SystemName + ") > " + Common.IgnoreSymbols(strPreventValue) + ")";
            //    }
            //    else if (PreventOperator == "empty")
            //    {
            //        strPreventWhere = " AND (Record." + theFilterParentColumnID.SystemName + " IS NOT NULL OR LEN(Record." + theFilterParentColumnID.SystemName + ")>0) ";
            //    }
            //    else if (PreventOperator == "notempty")
            //    {
            //        strPreventWhere = " AND (Record." + theFilterParentColumnID.SystemName + " IS NULL AND LEN(Record." + theFilterParentColumnID.SystemName + ")=0) ";
            //    }
            //    else
            //    {
            //        strPreventWhere = " AND Record." + theFilterParentColumnID.SystemName + " NOT IN ('" + strPreventValue.Replace("'", "''").Replace(",", "','") + "')";

            //    }
            //}

            //end red

            bool bDeletePermanent = false;
            bool bchkDelateAllEvery = false;
            if (hfchkDelateAllEvery.Value != "")
                bchkDelateAllEvery = bool.Parse(hfchkDelateAllEvery.Value);

            bool bchkDeleteParmanent = false;
            if (hfchkDeleteParmanent.Value != "")
                bchkDeleteParmanent = bool.Parse(hfchkDeleteParmanent.Value);

            string strDeleteReason = "";

            if (hftxtDeleteReason.Value != "")
            {
                strDeleteReason = hftxtDeleteReason.Value;
            }
            bool bchkUndo = false;
            if (hfchkUndo.Value != "")
                bchkUndo = bool.Parse(hfchkUndo.Value);

            if (chkShowDeletedRecords.Checked && bchkUndo && bchkDeleteParmanent)
            {
                bDeletePermanent = true;
            }

            strRecordIDs = strRecordIDs + "-1";
            //strExtraWhere = strExtraWhere + " " + strPreventWhere; //red 3090_2

            //RP Added Ticket 4406 - Alarming with 3 consecutive trigger exceedences
            //This update will execute the Before Delete SP if BeforeDeleteSP is not null
            if (_theTable != null)
            {
                if (_theTable.SPBeforeDeleteRecord != null)
                {
                    //RecordManager.BeforeDeleteRecordSP(_theTable.BeforeDeleteRecordSP, )
                    //Response.Write(strRecordIDs);
                    System.Collections.ArrayList ids = new System.Collections.ArrayList(strRecordIDs.Split(','));
                    foreach (String id in ids)
                    {
                        RecordManager.SPBeforeDeleteRecord(_theTable.SPBeforeDeleteRecord, Int32.Parse(id), _theTable.TableID);
                    }
                }
            }
            //End of Modification

            DataTable dtDeletedAndNotDeletedRecords = RecordManager.ets_Record_Delete(strRecordIDs, (int)_ObjUser.UserID,
                int.Parse(_qsTableID.ToString()), bDeleteAll, bDeletePermanent, strDeleteReason.Replace("'", "''"), strExtraWhere);

            foreach (DataRow dr in dtDeletedAndNotDeletedRecords.Rows)
            {
                string strDeletedRecords = dr["DeletedRecordIDs"].ToString(); // virtually and permanent
                string strPreventedDelete = dr["NotDeletedRecordIDs"].ToString();
                string strProcessedRecordIDs = dr["RecordIDs"].ToString();
                bool bWithChild = bool.Parse(dr["WithChild"].ToString());
                string strDeleteAction = dr["DeleteAction"].ToString();

                if (strDeleteAction.ToLower() == "delete children records" || string.IsNullOrEmpty(strDeleteAction))
                {
                    if (bWithChild)
                    {
                        Session["tdbmsgpb"] = strDeletedRecords + " " + _theTable.TableName + " record(s) have been deleted. Please note the child record(s) have also been deleted.";
                    }
                    else // No child and is null or empty
                        Session["tdbmsgpb"] = strDeletedRecords + " " + _theTable.TableName + " record(s) have been deleted.";
                }
                if (strDeleteAction.ToLower() == "prevent deletion")
                {
                    //red 04092017
                    if (strDeletedRecords == "0" && _theTable.CustomPreventDeleteNotification != "") //red 07092017
                    {
                        Session["tdbmsgpb"] = _theTable.CustomPreventDeleteNotification.ToString();
                    }
                    //red 04092017
                    else
                        Session["tdbmsgpb"] = strDeletedRecords + " " + _theTable.TableName + " record(s) have been deleted. " + strPreventedDelete + " record(s) could not be deleted.";
                }
            }

        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Record Delete", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }



    private void DeleteParmanentItem(string keys)
    {
        try
        {

            if (!string.IsNullOrEmpty(keys))
            {
                keys = keys + "-1";

                Common.ExecuteText("DELETE [Record] WHERE RecordID IN(" + keys + ")");
                return;
            }

        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Record", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;

            //ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "msg_delete", "alert('Delete User has failed!');", true);
        }
    }

    private void RestoreRecords(string strRecordIDs)
    {
        try
        {
            if (!string.IsNullOrEmpty(strRecordIDs))
            {

                _dtRecordColums = RecordManager.ets_Table_Columns_All(TableID);

                bool _bShowExceedances = false;
                string strShowExceedances = SystemData.SystemOption_ValueByKey_Account("Show Exceedances", _theTable.AccountID, _theTable.TableID);

                if (strShowExceedances != "" && strShowExceedances.ToLower() == "yes")
                {
                    _bShowExceedances = true;
                }

                int iDuplicateRecord = 0;
                int iTotalInvalid = 0;
                int iTotalRestoredRecords = 0;


                string strUniqueColumnIDSys = "";
                string strUniqueColumnID2Sys = "";

                if (_theTable.UniqueColumnID != null)
                    strUniqueColumnIDSys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE ColumnID=" + _theTable.UniqueColumnID.ToString());

                if (_theTable.UniqueColumnID2 != null)
                    strUniqueColumnID2Sys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE ColumnID=" + _theTable.UniqueColumnID2.ToString());



                string strTempErr = "";
                foreach (string sTemp in strRecordIDs.Split(','))
                {

                    if (!string.IsNullOrEmpty(sTemp))
                    {
                        //get the Record
                        Record aRecord = RecordManager.ets_Record_Detail_Full(int.Parse(sTemp), int.Parse(_qsTableID), bUseArchiveData);  // Red 08012018
                        bool bIsValid = true;

                        if (TheDatabase.IsRecordDuplicate(aRecord, strUniqueColumnIDSys, strUniqueColumnID2Sys, (int)aRecord.RecordID))
                        {
                            iDuplicateRecord = iDuplicateRecord + 1;
                            bIsValid = false;
                        }
                        DataTable dtValidWarning = null;
                        string strtempRef = "";
                        string _strInValidResults = "";
                        string _strExceedanceResults = "";
                        string _strWarningResults = "";
                        int _iWarningColumnCount = 0;
                        int _iExceedanceColumnCount = 0;

                        TheDatabase.PerformAllValidation(ref aRecord, ref dtValidWarning, false, false, _dtColumnsAll, ref strtempRef, ref _strInValidResults, _bShowExceedances,
                            ref _strExceedanceResults, (int)_theTable.AccountID, strtempRef, ref strtempRef, ref strtempRef, ref _iExceedanceColumnCount, ref _strWarningResults,
                            ref strtempRef, ref strtempRef, ref _iWarningColumnCount);

                        if (_strInValidResults != "")
                        {
                            aRecord.ValidationResults = _strExceedanceResults;
                            bIsValid = false;
                            iTotalInvalid = iTotalInvalid + 1;
                        }
                        else
                        {
                            aRecord.ValidationResults = "";
                        }
                        aRecord.WarningResults = "";
                        if (_strWarningResults.Length > 0)
                        {
                            aRecord.WarningResults = _strWarningResults.Trim();
                        }

                        if (_bShowExceedances && _strExceedanceResults.Length > 0)
                        {
                            aRecord.WarningResults = aRecord.WarningResults == "" ? _strExceedanceResults : aRecord.WarningResults + " " + _strExceedanceResults;
                        }


                        if (bIsValid)
                        {
                            aRecord.IsActive = true;
                            iTotalRestoredRecords = iTotalRestoredRecords + 1;
                        }
                        else
                        {
                            aRecord.IsActive = false;

                        }
                        aRecord.LastUpdatedUserID = _ObjUser.UserID;
                        RecordManager.ets_Record_Update(aRecord, false);
                        RecordManager.Table_SPSaveRecord(_theTable.SPSaveRecord, aRecord.RecordID, _ObjUser.UserID);
                    }
                }

                string strNotification = "";

                if (iDuplicateRecord > 0)
                    strNotification = "Total duplicate records:" + iDuplicateRecord.ToString() + ". ";
                if (iTotalInvalid > 0)
                    strNotification = strNotification + "Total invalid records:" + iTotalInvalid.ToString() + ". ";
                if (iTotalRestoredRecords > 0)
                    strNotification = strNotification + "Total restored records:" + iTotalRestoredRecords.ToString() + ".";

                if (strNotification != "")
                    Session["tdbmsgpb"] = strNotification;
            }
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Record", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            lblMsg.Text = ex.Message;
        }
    }


    protected void Pager_OnExportForCSV(object sender, EventArgs e)
    {
        ExportExcelorCSV(sender, e, "csv");

    }
    //    protected void Pager_OnExportForCSV(object sender, EventArgs e)
    //    {

    //        //DataTable dtExportColumn = RecordManager.ets_Table_Columns_Export(int.Parse(TableID.ToString()), null, null);

    //        //if (dtExportColumn.Rows.Count == 0)
    //        //{
    //        //    ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "NoExportMSG", "alert('Sorry it is not possible to export this table because none of the fields have been marked for export. Please check the table configuration and try again');", true);
    //        //    return;
    //        //}

    //        if (gvTheGrid.VirtualItemCount > Common.MaxRecordsExport)
    //        {

    //            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page),
    //                "message_alert", "alert('There are " + gvTheGrid.VirtualItemCount.ToString() + " Records, we are going to send this file to your email address.');", true);

    //            string strBulkExportPath = SystemData.SystemOption_ValueByKey("BulkExportPath");
    //            string strFileName = Guid.NewGuid().ToString() + "_" + lblTitle.Text.Replace(" ", "").ToString() + ".csv";
    //            string strFullFileName = strBulkExportPath + "\\" + strFileName;
    //            PopulateDateAddedSearch();
    //            int iIsBulkExportOK = RecordManager.ets_Record_List_BulkExport(int.Parse(TableID.ToString()),
    //                ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
    //                !chkIsActive.Checked,
    //                chkShowOnlyWarning.Checked == false ? null : (bool?)true,
    //                _dtDateFrom, _dtDateTo,
    //               strFullFileName);


    //            if (iIsBulkExportOK == 1)
    //            {
    //                //lets zip the file

    //                string filename = strFullFileName;


    //                FileStream infile = File.OpenRead(filename);
    //                byte[] buffer = new byte[infile.Length];
    //                infile.Read(buffer, 0, buffer.Length);
    //                infile.Close();

    //                //FileStream outfile = File.Create(Path.ChangeExtension(filename, "zip"));
    //                FileStream outfile = File.Create(filename + ".zip");

    //                GZipStream gzipStream = new GZipStream(outfile, CompressionMode.Compress);
    //                gzipStream.Write(buffer, 0, buffer.Length);
    //                gzipStream.Close();

    //                //now lets email this to the user.

    //                string strEmail = SystemData.SystemOption_ValueByKey("EmailFrom");
    //                string strEmailServer = SystemData.SystemOption_ValueByKey("EmailServer");
    //                string strEmailUserName = SystemData.SystemOption_ValueByKey("EmailUserName");
    //                string strEmailPassword = SystemData.SystemOption_ValueByKey("EmailPassword");
    //                MailMessage msg = new MailMessage();
    //                msg.From = new MailAddress(strEmail);
    //                msg.Subject = lblTitle.Text + " - data file";
    //                msg.IsBodyHtml = true;

    //                string strBulkExportHTTPPath = SystemData.SystemOption_ValueByKey("BulkExportHTTPPath");

    //                string strTheBody = "<div>Please click the file to download.<a href='" + strBulkExportHTTPPath + "/" + strFileName + ".zip" + "'>" + strFileName + ".zip" + "</a></div>";

    //                msg.Body = strTheBody;
    //                msg.To.Add(_ObjUser.Email);

    //                SmtpClient smtpClient = new SmtpClient(strEmailServer);
    //                smtpClient.Timeout = 99999;
    //                smtpClient.Credentials = new System.Net.NetworkCredential(strEmailUserName, strEmailPassword);

    //#if (!DEBUG)
    //                smtpClient.Send(msg);
    //#endif

    //                if (System.Web.HttpContext.Current.Session["AccountID"] != null)
    //                {

    //                    SecurityManager.Account_SMS_Email_Count(int.Parse(System.Web.HttpContext.Current.Session["AccountID"].ToString()), true, null, null, null);
    //                }

    //            }
    //            else
    //            {
    //                //failed
    //            }

    //            return;
    //        }


    //        //gvTheGrid.AllowPaging = false;
    //        //gvTheGrid.PageIndex = 0;


    //        //BindTheGridForExport(0, _gvPager.TotalRows);



    //        StringWriter sw = new StringWriter();
    //        HtmlTextWriter hw = new HtmlTextWriter(sw);



    //        int iTN = 0;
    //        gvTheGrid.PageIndex = 0;

    //        string strOrderDirection = "DESC";
    //        string sOrder = GetDataKeyNames()[0];

    //        if (gvTheGrid.GridViewSortDirection == SortDirection.Ascending)
    //        {
    //            strOrderDirection = "ASC";
    //        }
    //        sOrder = gvTheGrid.GridViewSortColumn + " ";


    //        if (sOrder.Trim() == "")
    //        {
    //            sOrder = "DBGSystemRecordID";
    //        }




    //        TextSearch = TextSearch + hfTextSearch.Value;
    //        if ((bool)_theUserRole.IsAdvancedSecurity)
    //        {
    //            if (_strRecordRightID == Common.UserRoleType.OwnData)
    //            {
    //                TextSearch = TextSearch + " AND (Record.OwnerUserID=" + _ObjUser.UserID.ToString() + " OR Record.EnteredBy=" + _ObjUser.UserID.ToString() + ")";
    //            }
    //        }
    //        else
    //        {
    //            if (Session["roletype"].ToString() == Common.UserRoleType.OwnData)
    //            {
    //                TextSearch = TextSearch + " AND (Record.OwnerUserID=" + _ObjUser.UserID.ToString() + " OR Record.EnteredBy=" + _ObjUser.UserID.ToString() + ")";
    //            }
    //        }
    //        PopulateDateAddedSearch();

    //        if (chkShowAdvancedOptions.Checked && ddlUploadedBatch.SelectedValue != "")
    //        {
    //            TextSearch = TextSearch + "  AND TempRecordID IN  (SELECT RecordID FROM TempRecord WHERE BatchID=" + ddlUploadedBatch.SelectedValue + ")";
    //        }

    //        string strHeaderXML = "";
    //        if (rdbRecords.SelectedValue == "a")
    //        {
    //            TextSearch = "";
    //            _strNumericSearch = "";
    //            _dtDateFrom = null;
    //            _dtDateTo = null;
    //        }
    //        if (rdbRecords.SelectedValue == "t")
    //        {
    //            TextSearch = "";
    //            _strNumericSearch = "";
    //            _dtDateFrom = null;
    //            _dtDateTo = null;


    //            string sCheck = "";
    //            for (int i = 0; i < gvTheGrid.Rows.Count; i++)
    //            {
    //                bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
    //                if (ischeck)
    //                {
    //                    sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
    //                }

    //            }

    //            if (string.IsNullOrEmpty(sCheck))
    //            {
    //                ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select a record.');", true);
    //                return;
    //            }

    //            sCheck = sCheck + "-1";
    //            TextSearch = " AND RecordID IN(" + sCheck + ")";

    //        }


    //        Response.Clear();
    //        Response.Buffer = true;
    //        Response.AddHeader("content-disposition",
    //        "attachment;filename=\"" + lblTitle.Text.Replace("Records - ", "") + " " + DateTime.Today.ToString("yyyyMMdd") + ".csv\"");
    //        Response.Charset = "";
    //        Response.ContentType = "text/csv";


    //        if (rdbRecords.SelectedValue == "d")
    //        {

    //            try
    //            {
    //                DataTable dtDump = TheDatabaseS.spExportAllTables(TableID);
    //                //DBG.Common.ExportUtil.ExportToExcel(dtDump, "\"" + lblTitle.Text.Replace("Records - ", "") + " " + DateTime.Today.ToString("yyyyMMdd") + ".xls" + "\"");
    //                int iColCountD = dtDump.Columns.Count;
    //                //for (int i = 0; i < iColCountD; i++)
    //                //{
    //                //    sw.Write(dtDump.Columns[i]);

    //                //}

    //                //sw.Write(sw.NewLine);



    //                foreach (DataRow dr in dtDump.Rows)
    //                {
    //                    for (int i = 0; i < iColCountD; i++)
    //                    {
    //                        if (!Convert.IsDBNull(dr[i]))
    //                        {
    //                            //sw.Write("\"" + dr[i].ToString().Replace("\"", "'") + "\"");
    //                            sw.Write(dr[i].ToString());
    //                        }
    //                    }

    //                    sw.Write(sw.NewLine);

    //                }

    //                sw.Close();

    //                Response.Output.Write(sw.ToString());
    //                Response.Flush();
    //                Response.End();

    //                return;
    //            }
    //            catch (Exception ex)
    //            {
    //                ErrorLog theErrorLog = new ErrorLog(null, "Dump Export", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //                SystemData.ErrorLog_Insert(theErrorLog);
    //                return;
    //            }

    //        }

    //        bool bFoundHeader = false;

    //        foreach (ListItem item in chklstFields.Items)
    //        {
    //            if (item.Selected)
    //            {
    //                bFoundHeader = true;
    //                break;
    //            }
    //        }

    //        if (bFoundHeader == false)
    //        {
    //            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "NoExportMSG", "alert('Sorry it is not possible to export this table because none of the fields have been marked for export.Please select fields for export.');", true);
    //            return;
    //        }
    //        else
    //        {
    //            strHeaderXML = "<ExportXML>";
    //            foreach (ListItem item in chklstFields.Items)
    //            {
    //                if (item.Selected)
    //                {
    //                    strHeaderXML = strHeaderXML + "<Records>";

    //                    Column theColumn = RecordManager.ets_Column_Details(int.Parse(item.Value));
    //                    strHeaderXML = strHeaderXML + "<ColumnID>" + item.Value + "</ColumnID>";
    //                    strHeaderXML = strHeaderXML + "<DisplayText>" + System.Security.SecurityElement.Escape(item.Text) + "</DisplayText>";
    //                    strHeaderXML = strHeaderXML + "<SystemName>" + theColumn.SystemName + "</SystemName>";

    //                    strHeaderXML = strHeaderXML + "</Records>";

    //                }
    //            }

    //            strHeaderXML = strHeaderXML + "</ExportXML>";
    //        }


    //        _dtRecordColums = RecordManager.ets_Table_Columns_Summary(TableID, int.Parse(hfViewID.Value));

    //        bool bFoundExportColumn = false;
    //        if (sOrder != "DBGSystemRecordID" && sOrder != "" && strHeaderXML != "" && ViewState["SortOrderColumnID"] != null)
    //        {
    //            DataSet ds = new DataSet();
    //            StringReader sr = new StringReader(strHeaderXML);
    //            ds.ReadXml(sr);
    //            DataTable dtHeader = ds.Tables[0];


    //            Column theSortColumn = RecordManager.ets_Column_Details(int.Parse(ViewState["SortOrderColumnID"].ToString()));

    //            if (theSortColumn != null)
    //            {
    //                for (int j = 0; j < dtHeader.Rows.Count; j++)
    //                {
    //                    for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //                    {
    //                        if (_dtRecordColums.Rows[i]["ColumnID"].ToString() == dtHeader.Rows[j]["ColumnID"].ToString()
    //                            && theSortColumn.ColumnID.ToString() == _dtRecordColums.Rows[i]["ColumnID"].ToString())
    //                        {

    //                            if (sOrder.IndexOf("CONVERT") > -1)
    //                            {
    //                                sOrder = sOrder.Replace("[" + _dtRecordColums.Rows[i]["Heading"].ToString() + "]",
    //                                    "[" + dtHeader.Rows[j]["DisplayText"].ToString() + "]");
    //                                bFoundExportColumn = true;
    //                                break;

    //                            }
    //                            else
    //                            {
    //                                sOrder = sOrder.Replace(_dtRecordColums.Rows[i]["Heading"].ToString(), dtHeader.Rows[j]["DisplayText"].ToString());
    //                                bFoundExportColumn = true;
    //                                break;
    //                            }


    //                        }
    //                    }

    //                    if (bFoundExportColumn)
    //                    {
    //                        break;
    //                    }
    //                }
    //            }

    //        }

    //        if (bFoundExportColumn == false)
    //        {

    //            sOrder = "DBGSystemRecordID";
    //        }


    //        if (TextSearchParent == null)
    //            TextSearchParent = "";



    //        DataTable dt = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
    //                ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
    //                !chkIsActive.Checked,
    //                chkShowOnlyWarning.Checked == false ? null : (bool?)true,
    //                null, null,
    //                  sOrder, strOrderDirection, 0,null, ref iTN, ref _iTotalDynamicColumns, "export", _strNumericSearch, TextSearch + TextSearchParent,
    //                  _dtDateFrom, _dtDateTo, "", strHeaderXML, "", null);








    //        if (strHeaderXML != "")
    //        {

    //            DataSet ds = new DataSet();
    //            StringReader sr = new StringReader(strHeaderXML);
    //            ds.ReadXml(sr);
    //            DataTable dtHeader = ds.Tables[0];

    //            for (int j = 0; j < dtHeader.Rows.Count; j++)
    //            {
    //                for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //                {
    //                    if (_dtRecordColums.Rows[i]["ColumnID"].ToString() == dtHeader.Rows[j]["ColumnID"].ToString())
    //                    {
    //                        _dtRecordColums.Rows[i]["NameOnExport"] = dtHeader.Rows[j]["DisplayText"];
    //                    }
    //                }
    //            }


    //            _dtRecordColums.AcceptChanges();

    //        }





    //        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //        {
    //            for (int j = 0; j < dt.Columns.Count; j++)
    //            {
    //                //if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "data_retriever")
    //                //{
    //                //    if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                //    {
    //                //        DataRetriever theDataRetriever = DocumentManager.dbg_DataRetriever_Detail(int.Parse(_dtRecordColums.Rows[i]["DataRetrieverID"].ToString()), null, null);

    //                //        if (theDataRetriever.CodeSnippet != "")
    //                //        {
    //                //            foreach (DataRow drDS in dt.Rows)
    //                //            {
    //                //                if (drDS["DBGSystemRecordID"].ToString() != "")
    //                //                {
    //                //                    drDS[dt.Columns[j].ColumnName] = Common.GetValueFromSQL(theDataRetriever.CodeSnippet.Replace("#ID#",
    //                //                        drDS["DBGSystemRecordID"].ToString()));
    //                //                }
    //                //            }

    //                //        }
    //                //    }
    //                //}


    //                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "calculation")
    //                {
    //                    if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                    {

    //                        if (_dtRecordColums.Rows[i]["Calculation"] != DBNull.Value)
    //                        {

    //                            bool bDateCal = false;
    //                            if (_dtRecordColums.Rows[i]["TextType"] != DBNull.Value
    //                                && _dtRecordColums.Rows[i]["TextType"].ToString().ToLower() == "d")
    //                            {
    //                                bDateCal = true;
    //                            }

    //                            foreach (DataRow drDS in dt.Rows)
    //                            {
    //                                if (drDS["DBGSystemRecordID"].ToString() != "")
    //                                {

    //                                    if (bDateCal == true)
    //                                    {
    //                                        try
    //                                        {
    //                                            string strCalculation = _dtRecordColums.Rows[i]["Calculation"].ToString();
    //                                            drDS[_dtDataSource.Columns[j].ColumnName] = TheDatabaseS.GetDateCalculationResult(_dtColumnsAll, strCalculation, int.Parse(drDS["DBGSystemRecordID"].ToString()), _iParentRecordID,
    //                                                _dtRecordColums.Rows[i]["DateCalculationType"] == DBNull.Value ? "" : _dtRecordColums.Rows[i]["DateCalculationType"].ToString());
    //                                        }
    //                                        catch
    //                                        {

    //                                        }
    //                                    }
    //                                    else
    //                                    {
    //                                        string strFormula = TheDatabaseS.GetCalculationFormula(int.Parse(_qsTableID), _dtRecordColums.Rows[i]["Calculation"].ToString());
    //                                        //drDS[dt.Columns[j].ColumnName] = Common.GetValueFromSQL("SELECT " + strFormula + " FROM Record WHERE RecordID=" + drDS["DBGSystemRecordID"].ToString());

    //                                        drDS[dt.Columns[j].ColumnName] = TheDatabaseS.GetCalculationResult(_dtColumnsAll, strFormula, int.Parse(drDS["DBGSystemRecordID"].ToString()), i, _iParentRecordID);
    //                                    }

    //                                    if (bDateCal == false && _dtRecordColums.Rows[i]["IsRound"] != DBNull.Value && _dtRecordColums.Rows[i]["RoundNumber"] != DBNull.Value)
    //                                    {
    //                                        try
    //                                        {

    //                                            drDS[dt.Columns[j].ColumnName] = Math.Round(double.Parse(drDS[dt.Columns[j].ColumnName].ToString()), int.Parse(_dtRecordColums.Rows[i]["RoundNumber"].ToString())).ToString();


    //                                        }
    //                                        catch
    //                                        {
    //                                            //
    //                                        }
    //                                    }
    //                                }
    //                            }

    //                        }

    //                    }
    //                }


    //            }
    //        }

    //        //}
    //        dt.AcceptChanges();

    //        if (chkShowAdvancedOptions.Checked == true)//_bDynamicSearch
    //        {
    //            //if (ddlYAxis.SelectedValue != "")
    //            //{
    //            //    Column theColumn = RecordManager.ets_Column_Details(int.Parse(ddlYAxis.SelectedValue));
    //            //    if (theColumn != null)
    //            //    {
    //            //        if (theColumn.ColumnType == "calculation")
    //            //        {
    //            //            if (txtSearchText.Text != "")
    //            //            {
    //            //                DataView dtView = new DataView(dt);

    //            //                dtView.RowFilter = theColumn.DisplayTextSummary + " LIKE '%" + txtSearchText.Text.Replace("'", "''") + "%'";
    //            //                dt = dtView.ToTable();
    //            //            }
    //            //        }

    //            //    }
    //            //}

    //        }


    //        DataRow drFooter = dt.NewRow();

    //        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //        {
    //            for (int j = 0; j < dt.Columns.Count; j++)
    //            {
    //                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                {
    //                    if (_dtRecordColums.Rows[i]["ShowTotal"].ToString().ToLower() == "true")
    //                    {
    //                        //drFooter[_dtRecordColums.Rows[i]["NameOnExport"].ToString()] = _dtDataSource.Compute("SUM([" + _dtRecordColums.Rows[i]["NameOnExport"].ToString() + "])", "[" + _dtRecordColums.Rows[i]["NameOnExport"].ToString() + "]<>''");
    //                        drFooter[_dtRecordColums.Rows[i]["NameOnExport"].ToString()] = CalculateTotalForAColumn(dt, dt.Columns[j].ColumnName, bool.Parse(_dtRecordColums.Rows[i]["IgnoreSymbols"].ToString().ToLower()));

    //                    }


    //                }

    //            }

    //        }

    //        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //        {
    //            for (int j = dt.Columns.Count - 1; j >= 0; j--)
    //            {
    //                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                {
    //                    if (_dtRecordColums.Rows[i]["OnlyForAdmin"].ToString().ToLower() == "1")
    //                    {
    //                        if (!Common.HaveAccess(_strRecordRightID, "1,2"))
    //                        {
    //                            dt.Columns.RemoveAt(j);
    //                        }
    //                    }


    //                }

    //            }

    //        }


    //        dt.Rows.Add(drFooter);




    //        //Round export

    //        foreach (DataRow dr in dt.Rows)
    //        {
    //            for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //            {
    //                for (int j = 0; j < dt.Columns.Count; j++)
    //                {
    //                    //DisplayTextSummary
    //                    if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                    {
    //                        if (IsStandard(_dtRecordColums.Rows[i]["SystemName"].ToString()) == false)
    //                        {

    //                            if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "file"
    //                                || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "image")
    //                            {
    //                                if (dr[j].ToString() != "" && dr[j].ToString() != "&nbsp;")
    //                                {
    //                                    try
    //                                    {
    //                                        if (dr[j].ToString().Length > 37)
    //                                        {
    //                                            dr[j] = dr[j].ToString().Substring(37);

    //                                        }
    //                                    }
    //                                    catch
    //                                    {
    //                                        //
    //                                    }
    //                                }

    //                            }

    //                            if (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "value_text"
    //                                  && (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
    //                                 || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "radiobutton")
    //                                 && _dtRecordColums.Rows[i]["DropdownValues"].ToString() != "")
    //                            {
    //                                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                                {

    //                                    if (dr[j].ToString() != "")
    //                                    {
    //                                        string strText = GetTextFromValueForDD(_dtRecordColums.Rows[i]["DropdownValues"].ToString(), dr[j].ToString());
    //                                        if (strText != "")
    //                                            dr[j] = strText;
    //                                    }
    //                                }

    //                            }


    //                            if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "time")
    //                            {
    //                                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                                {

    //                                    if (dr[j].ToString() != "")
    //                                    {

    //                                        TimeSpan ts = TimeSpan.Parse(dr[j].ToString());
    //                                        dr[j] = ts.ToString(@"hh\:mm");
    //                                    }
    //                                }

    //                            }

    //                            //if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "data_retriever"
    //                            //    && _dtRecordColums.Rows[i]["DataRetrieverID"] != DBNull.Value)
    //                            //{
    //                            //    if (_dtRecordColums.Rows[i]["Heading"].ToString() == dt.Columns[j].ColumnName)
    //                            //    {
    //                            //        DataRetriever theDataRetriever = DocumentManager.dbg_DataRetriever_Detail(int.Parse(_dtRecordColums.Rows[i]["DataRetrieverID"].ToString()), null, null);

    //                            //        if (theDataRetriever.CodeSnippet != "")
    //                            //        {
    //                            //            dr[j] = Common.GetValueFromSQL(theDataRetriever.CodeSnippet.Replace("#ID#",
    //                            //                dr["DBGSystemRecordID"].ToString()));
    //                            //        }
    //                            //    }
    //                            //}

    //                            if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
    //                                && (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
    //                                || _dtRecordColums.Rows[i]["DropDownType"].ToString() == "tabledd")
    //                                 && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
    //                                && _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
    //                            {
    //                                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                                {

    //                                    if (dr[j].ToString() != "" && dr[j].ToString() != "&nbsp;")
    //                                    {
    //                                        try
    //                                        {

    //                                            Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["LinkedParentColumnID"].ToString()));

    //                                            //int iTableRecordID = int.Parse(dr[j].ToString());
    //                                            string strLinkedColumnValue = dr[j].ToString();
    //                                            DataTable dtTableTableSC = Common.DataTableFromText("SELECT SystemName,DisplayName FROM [Column] WHERE   TableID ="
    //                                             + _dtRecordColums.Rows[i]["TableTableID"].ToString());

    //                                            string strDisplayColumn = _dtRecordColums.Rows[i]["DisplayColumn"].ToString();

    //                                            foreach (DataRow dr2 in dtTableTableSC.Rows)
    //                                            {
    //                                                strDisplayColumn = strDisplayColumn.Replace("[" + dr2["DisplayName"].ToString() + "]", "[" + dr2["SystemName"].ToString() + "]");

    //                                            }
    //                                            string sstrDisplayColumnOrg = strDisplayColumn;
    //                                            string strFilterSQL = "";
    //                                            if (theLinkedColumn.SystemName.ToLower() == "recordid")
    //                                            {
    //                                                strFilterSQL = strLinkedColumnValue;
    //                                            }
    //                                            else
    //                                            {
    //                                                strFilterSQL = "'" + strLinkedColumnValue.Replace("'", "''") + "'";
    //                                            }

    //                                            //DataTable dtTheRecord = Common.DataTableFromText("SELECT * FROM Record WHERE RecordID=" + iTableRecordID.ToString());

    //                                            DataTable dtTheRecord = Common.DataTableFromText("SELECT * FROM Record WHERE TableID=" + theLinkedColumn.TableID.ToString() + " AND " + theLinkedColumn.SystemName + "=" + strFilterSQL);

    //                                            if (dtTheRecord.Rows.Count > 0)
    //                                            {
    //                                                foreach (DataColumn dc in dtTheRecord.Columns)
    //                                                {
    //                                                    strDisplayColumn = strDisplayColumn.Replace("[" + dc.ColumnName + "]", dtTheRecord.Rows[0][dc.ColumnName].ToString());
    //                                                }
    //                                            }
    //                                            if (sstrDisplayColumnOrg != strDisplayColumn)
    //                                                dr[j] = strDisplayColumn;
    //                                        }
    //                                        catch
    //                                        {
    //                                            //
    //                                        }


    //                                    }
    //                                }

    //                            }



    //                            if (_dtRecordColums.Rows[i]["IsRound"] != DBNull.Value)
    //                            {
    //                                if (_dtRecordColums.Rows[i]["IsRound"].ToString().ToLower() == "true")
    //                                {
    //                                    if (dr[j].ToString() != "")
    //                                    {
    //                                        dr[j] = Math.Round(double.Parse(dr[j].ToString()), int.Parse(_dtRecordColums.Rows[i]["RoundNumber"].ToString())).ToString();
    //                                    }
    //                                }

    //                            }
    //                        }

    //                    }

    //                    //mm:hh
    //                    if (_dtRecordColums.Rows[i]["SystemName"].ToString().ToLower() == "datetimerecorded")
    //                    {

    //                        if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "datetime")
    //                        {
    //                            if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                            {
    //                                if (dr[j].ToString().Length > 15)
    //                                {
    //                                    dr[j] = dr[j].ToString().Substring(0, 16);
    //                                }
    //                            }
    //                        }

    //                        if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "date")
    //                        {
    //                            if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                            {
    //                                if (dr[j].ToString().Length > 9)
    //                                {
    //                                    dr[j] = dr[j].ToString().Substring(0, 10);
    //                                }
    //                            }
    //                        }


    //                    }

    //                }
    //            }
    //        }

    //        // First we will write the headers.

    //        int iColCount = dt.Columns.Count;



    //        for (int i = 0; i < iColCount - 2; i++)
    //        {
    //            sw.Write(dt.Columns[i]);
    //            if (i < iColCount - 3)
    //            {
    //                sw.Write(",");
    //            }

    //        }

    //        sw.Write(sw.NewLine);



    //        // Now write all the rows.


    //        foreach (DataRow dr in dt.Rows)
    //        {
    //            for (int i = 0; i < iColCount - 2; i++)
    //            {
    //                if (!Convert.IsDBNull(dr[i]))
    //                {
    //                    sw.Write("\"" + dr[i].ToString().Replace("\"", "'") + "\"");
    //                }

    //                if (i < iColCount - 3)
    //                {
    //                    sw.Write(",");
    //                }
    //            }

    //            sw.Write(sw.NewLine);

    //        }

    //        sw.Close();

    //        Response.Output.Write(sw.ToString());
    //        Response.Flush();
    //        Response.End();
    //    }


    protected void Pager_OnExportForExcel(object sender, EventArgs e)
    {
        ExportExcelorCSV(sender, e, "excel");

    }

    protected void ExportExcelorCSV(object sender, EventArgs e, string strExportType)
    {


        try
        {

            //if (gvTheGrid.VirtualItemCount > Common.MaxRecordsExport(_theTable.AccountID, _theTable.TableID))
            //{

            //    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page),
            //        "message_alert", "alert('There are " + gvTheGrid.VirtualItemCount.ToString() + " Records, we are going to send this file to your email address.');", true);

            //    string strBulkExportPath = SystemData.SystemOption_ValueByKey_Account("BulkExportPath", _theTable.AccountID, _theTable.TableID);
            //    string strFileName = Guid.NewGuid().ToString() + "_" + lblTitle.Text.Replace(" ", "").ToString() + ".csv";
            //    string strFullFileName = strBulkExportPath + "\\" + strFileName;
            //    PopulateDateAddedSearch();
            //    int iIsBulkExportOK = RecordManager.ets_Record_List_BulkExport(int.Parse(TableID.ToString()),
            //        ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
            //        !chkIsActive.Checked,
            //        chkShowOnlyWarning.Checked == false ? null : (bool?)true,
            //        _dtDateFrom, _dtDateTo,
            //       strFullFileName);


            //    if (iIsBulkExportOK == 1)
            //    {
            //        //lets zip the file

            //        string filename = strFullFileName;

            //        FileStream infile = File.OpenRead(filename);
            //        byte[] buffer = new byte[infile.Length];
            //        infile.Read(buffer, 0, buffer.Length);
            //        infile.Close();

            //        FileStream outfile = File.Create(filename + ".zip");

            //        GZipStream gzipStream = new GZipStream(outfile, CompressionMode.Compress);
            //        gzipStream.Write(buffer, 0, buffer.Length);
            //        gzipStream.Close();

            //        string strSubject = lblTitle.Text + " - data file";
            //        string strBulkExportHTTPPath = SystemData.SystemOption_ValueByKey_Account("BulkExportHTTPPath", _theTable.AccountID, _theTable.TableID);

            //        string strTheBody = "<div>Please click the file to download.<a href='" + strBulkExportHTTPPath + "/" + strFileName + ".zip" + "'>" + strFileName + ".zip" + "</a></div>";

            //        string strTo = _ObjUser.Email;
            //        string strError = "";
            //        DBGurus.SendEmail("Bulk Export", true, null, strSubject, strTheBody, "", strTo, "", "", null, null, out strError);

            //    }
            //    else
            //    {
            //        //failed
            //    }

            //    return;
            //}

            //Red 3341 17112017
            int iExportTemplateID = -1;

            HttpContext.Current.Response.Clear();

            if (strExportType == "email")
            {
                if (_gvPager == null)
                {
                    lblMsg.Text = "There is no records to email.";
                    return;

                }
            }

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);




            int iTN = 0;
            //gvTheGrid.PageIndex = 0;

            string strOrderDirection = "DESC";
            string sOrder = GetDataKeyNames()[0];

            if (gvTheGrid.GridViewSortDirection == SortDirection.Ascending)
            {
                strOrderDirection = "ASC";
            }
            sOrder = gvTheGrid.GridViewSortColumn + " ";


            if (sOrder.Trim() == "")
            {
                sOrder = "DBGSystemRecordID";
            }

            string rdbRecords = "a";
            List<IDnText> chklstFields = null;

            if (Session["ExportClass"] != null)
            {
                ExportClass aExportClass = (ExportClass)Session["ExportClass"];
                rdbRecords = aExportClass.strRecords;
                chklstFields = aExportClass.objCheckBoxList;
                iExportTemplateID = aExportClass.iExportTemplateID; //red 3341
                Session["ExportClass"] = null;
            }
            DataTable dtExportTemplate = Common.DataTableFromText("SELECT E.ColumnID,C.ColumnType, C.IsRound , C.RoundNumber, C.HideTimeSecond, E.[Option], ExportHeaderName, C.TextType FROM [ExportTemplateItem] E JOIN [COLUMN] C ON E.ColumnID = C.ColumnID WHERE E.[ExportTemplateID] = " + iExportTemplateID);

            string strHeaderXML = "";
            if (rdbRecords == "a" && strExportType != "email")
            {
                TextSearch = "";
                _strNumericSearch = "";
                _dtDateFrom = null;
                _dtDateTo = null;
            }


            if ((rdbRecords == "t" || rdbRecords == "d") && strExportType != "email")
            {
                TextSearch = "";
                _strNumericSearch = "";
                _dtDateFrom = null;
                _dtDateTo = null;


                string sCheck = "";
                for (int i = 0; i < gvTheGrid.Rows.Count; i++)
                {
                    bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
                    if (ischeck)
                    {
                        sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
                    }

                }

                if (string.IsNullOrEmpty(sCheck) && rdbRecords == "t")
                {
                    Session["tdbmsgpb"] = "Please select a record.";
                    //ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select a record.');", true);
                    return;
                }


                if (rdbRecords == "t")
                {
                    sCheck = sCheck + "-1";
                    TextSearch = TextSearch + " AND Record.RecordID IN(" + sCheck + ")";
                }

                if (rdbRecords == "d")
                {
                    if (sCheck != "")
                    {
                        sCheck = sCheck.Substring(0, sCheck.Length - 1);
                        TextSearch = TextSearch + " AND Record.RecordID IN(" + sCheck + ")";
                    }


                }


            }

            //if (rdbRecords.SelectedValue == "d")
            //{
            //    DataTable dtDump = TheDatabaseS.spExportAllTables(TableID);
            //    DBG.Common.ExportUtil.ExportToExcel(dtDump, "\"" + lblTitle.Text.Replace("Records - ", "") + " " + DateTime.Today.ToString("yyyyMMdd") + ".xls" + "\"");

            //    return;
            //}



            bool bFoundHeader = false;

            if (strExportType != "email" && rdbRecords != "d")
            {

                foreach (IDnText item in chklstFields)
                {

                    bFoundHeader = true;
                    break;

                }

                if (bFoundHeader == false)
                {
                    Session["tdbmsgpb"] = "Sorry it is not possible to export this table because none of the fields have been marked for export.Please select fields for export.";
                    //ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "NoExportMSG", "alert('Sorry it is not possible to export this table because none of the fields have been marked for export.Please select fields for export.');", true);
                    return;
                }
                else
                {
                    strHeaderXML = "<ExportXML>";
                    foreach (IDnText item in chklstFields)
                    {

                        //oliver <begin> Ticket 1461
                        //strHeaderXML = strHeaderXML + "<Records>";
                        //oliver <end>

                        Column theColumn = RecordManager.ets_Column_Details(int.Parse(item.ID));

                        //red 3341
                        string strExportColumnOption = Common.GetValueFromSQL("SELECT [Option] FROM [ExportTemplateItem] WHERE ColumnID=" + item.ID + " AND ExportTemplateID=" + iExportTemplateID);


                        string strParentJoinColumnName = "";
                        string strChildJoinColumnName = "";
                        string strParentTableID = "";
                        string strShowViewLink = "";
                        string strFieldsToShow = "";
                        if (theColumn.TableTableID != null && theColumn.DisplayColumn != "" &&
                            (theColumn.ColumnType == "dropdown" || theColumn.ColumnType == "listbox")) //||theColumn.ColumnType=="listbox"
                        {

                            strParentTableID = theColumn.TableTableID.ToString();

                            if (theColumn.LinkedParentColumnID != null & (int)theColumn.TableTableID != -1)
                            {
                                Column theLinkedParentColumn = RecordManager.ets_Column_Details(((int)theColumn.LinkedParentColumnID));
                                strParentJoinColumnName = theLinkedParentColumn.SystemName;
                                strChildJoinColumnName = theColumn.SystemName;
                                strFieldsToShow = RecordManager.fnReplaceDisplayColumns(theColumn.DisplayColumn, (int)theColumn.TableTableID, theColumn.ColumnID);

                                strShowViewLink = theColumn.ShowViewLink;

                            }
                            else
                            {
                                if ((int)theColumn.TableTableID == -1)
                                {
                                    strFieldsToShow = theColumn.DisplayColumn;
                                }
                            }
                        }

                        if ((theColumn.ColumnType == "datetime")
                               //  && (theColumn.SystemName.ToString().ToLower() != "datetimerecorded")
                               && (strExportColumnOption.ToLower() == "separate" //red 3341
                               || string.IsNullOrEmpty(strExportColumnOption))  //red 3341_2
                               )
                        {
                            //oliver <begin> Ticket 1461

                            //split the date/time by creating separate export field for Date and Time

                            //Create export header for Date:
                            strHeaderXML = strHeaderXML + "<Records>";
                            strHeaderXML = strHeaderXML + "<ColumnID>" + item.ID + "</ColumnID>";
                            strHeaderXML = strHeaderXML + "<DisplayText>" + System.Security.SecurityElement.Escape(item.Text) + "-DDAATTEE" + "</DisplayText>";
                            strHeaderXML = strHeaderXML + "<SystemName>" + theColumn.SystemName + "</SystemName>";
                            strHeaderXML = strHeaderXML + "<FieldsToShow>" + System.Security.SecurityElement.Escape(strFieldsToShow) + "</FieldsToShow>";
                            strHeaderXML = strHeaderXML + "<ParentTableID>" + strParentTableID + "</ParentTableID>";
                            strHeaderXML = strHeaderXML + "<ParentJoinColumnName>" + strParentJoinColumnName + "</ParentJoinColumnName>";
                            strHeaderXML = strHeaderXML + "<ChildJoinColumnName>" + strChildJoinColumnName + "</ChildJoinColumnName>";
                            strHeaderXML = strHeaderXML + "<ShowViewLink>" + strShowViewLink + "</ShowViewLink>";
                            strHeaderXML = strHeaderXML + "<ColumnType>" + theColumn.ColumnType + "</ColumnType>";
                            strHeaderXML = strHeaderXML + "</Records>";

                            //Create export header for Time:
                            strHeaderXML = strHeaderXML + "<Records>";
                            strHeaderXML = strHeaderXML + "<ColumnID>" + item.ID + "</ColumnID>";
                            strHeaderXML = strHeaderXML + "<DisplayText>" + System.Security.SecurityElement.Escape(item.Text) + "-TTIIMMEE" + "</DisplayText>";
                            strHeaderXML = strHeaderXML + "<SystemName>" + theColumn.SystemName + "</SystemName>";
                            strHeaderXML = strHeaderXML + "<FieldsToShow>" + System.Security.SecurityElement.Escape(strFieldsToShow) + "</FieldsToShow>";
                            strHeaderXML = strHeaderXML + "<ParentTableID>" + strParentTableID + "</ParentTableID>";
                            strHeaderXML = strHeaderXML + "<ParentJoinColumnName>" + strParentJoinColumnName + "</ParentJoinColumnName>";
                            strHeaderXML = strHeaderXML + "<ChildJoinColumnName>" + strChildJoinColumnName + "</ChildJoinColumnName>";
                            strHeaderXML = strHeaderXML + "<ShowViewLink>" + strShowViewLink + "</ShowViewLink>";
                            strHeaderXML = strHeaderXML + "<ColumnType>" + theColumn.ColumnType + "</ColumnType>";
                            strHeaderXML = strHeaderXML + "</Records>";

                            continue;

                            //oliver <end>
                        }
                        else
                        {
                            strHeaderXML = strHeaderXML + "<Records>";
                            strHeaderXML = strHeaderXML + "<ColumnID>" + item.ID + "</ColumnID>";
                            strHeaderXML = strHeaderXML + "<DisplayText>" + System.Security.SecurityElement.Escape(item.Text) + "</DisplayText>";
                            strHeaderXML = strHeaderXML + "<SystemName>" + theColumn.SystemName + "</SystemName>";
                            strHeaderXML = strHeaderXML + "<FieldsToShow>" + System.Security.SecurityElement.Escape(strFieldsToShow) + "</FieldsToShow>";
                            strHeaderXML = strHeaderXML + "<ParentTableID>" + strParentTableID + "</ParentTableID>";
                            strHeaderXML = strHeaderXML + "<ParentJoinColumnName>" + strParentJoinColumnName + "</ParentJoinColumnName>";
                            strHeaderXML = strHeaderXML + "<ChildJoinColumnName>" + strChildJoinColumnName + "</ChildJoinColumnName>";
                            strHeaderXML = strHeaderXML + "<ShowViewLink>" + strShowViewLink + "</ShowViewLink>";
                            strHeaderXML = strHeaderXML + "<ColumnType>" + theColumn.ColumnType + "</ColumnType>";
                            strHeaderXML = strHeaderXML + "</Records>";
                        }

                        //strHeaderXML = strHeaderXML + "<ColumnID>" + item.Value + "</ColumnID>";
                        //strHeaderXML = strHeaderXML + "<DisplayText>" + System.Security.SecurityElement.Escape(item.Text) + "</DisplayText>";
                        //strHeaderXML = strHeaderXML + "<SystemName>" + theColumn.SystemName + "</SystemName>";
                        //strHeaderXML = strHeaderXML + "<FieldsToShow>" + System.Security.SecurityElement.Escape(strFieldsToShow) + "</FieldsToShow>";
                        //strHeaderXML = strHeaderXML + "<ParentTableID>" + strParentTableID + "</ParentTableID>";
                        //strHeaderXML = strHeaderXML + "<ParentJoinColumnName>" + strParentJoinColumnName + "</ParentJoinColumnName>";
                        //strHeaderXML = strHeaderXML + "<ChildJoinColumnName>" + strChildJoinColumnName + "</ChildJoinColumnName>";
                        //strHeaderXML = strHeaderXML + "<ShowViewLink>" + strShowViewLink + "</ShowViewLink>";
                        //strHeaderXML = strHeaderXML + "<ColumnType>" + theColumn.ColumnType + "</ColumnType>";

                        //strHeaderXML = strHeaderXML + "</Records>";
                    }
                    strHeaderXML = strHeaderXML + "</ExportXML>";
                }
            }




            //_dtRecordColums = RecordManager.ets_Table_Columns_Summary(TableID, int.Parse(hfViewID.Value));

            _dtRecordColums = RecordManager.ets_Table_Columns_All((int)_theTable.TableID);

            bool bFoundExportColumn = false;

            if (sOrder != "DBGSystemRecordID" && sOrder != "" && strHeaderXML != "" && ViewState["SortOrderColumnID"] != null && strExportType != "email")
            {
                DataSet ds = new DataSet();
                StringReader sr = new StringReader(strHeaderXML);
                ds.ReadXml(sr);
                DataTable dtHeader = ds.Tables[0];


                Column theSortColumn = RecordManager.ets_Column_Details(int.Parse(ViewState["SortOrderColumnID"].ToString()));

                if (theSortColumn != null)
                {
                    for (int j = 0; j < dtHeader.Rows.Count; j++)
                    {
                        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                        {
                            if (_dtRecordColums.Rows[i]["ColumnID"].ToString() == dtHeader.Rows[j]["ColumnID"].ToString()
                                && theSortColumn.ColumnID.ToString() == _dtRecordColums.Rows[i]["ColumnID"].ToString())
                            {

                                if (sOrder.IndexOf("CONVERT") > -1)
                                {
                                    sOrder = sOrder.Replace("[" + _dtRecordColums.Rows[i]["Heading"].ToString() + "]",
                                        "[" + dtHeader.Rows[j]["DisplayText"].ToString() + "]");
                                    bFoundExportColumn = true;
                                    break;

                                }
                                else
                                {
                                    sOrder = sOrder.Replace(_dtRecordColums.Rows[i]["Heading"].ToString(), dtHeader.Rows[j]["DisplayText"].ToString());
                                    bFoundExportColumn = true;
                                    break;
                                }
                            }
                        }
                        if (bFoundExportColumn)
                        {
                            break;
                        }
                    }
                }

            }

            if (strExportType != "email")
            {

                if (bFoundExportColumn == false)
                {

                    sOrder = "DBGSystemRecordID";
                }

                if (TextSearchParent == null)
                    TextSearchParent = "";

            }

            DataTable dt = new DataTable();

            if (strExportType == "csv" && rdbRecords == "d")
                strHeaderXML = "";

            //Is it a bulk export?
            string strReturnSQL = "";
            string sReturnHeaderSQL = "";
            Exception exParam = null;
            //RP Modified Ticket 3427
           // DataTable dtBulk = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
           //ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
           //!chkShowDeletedRecords.Checked,
           //chkShowOnlyWarning.Checked == false ? null : (bool?)true,
           // null, null,
           //  sOrder, strOrderDirection, 0, 10, ref iTN, ref _iTotalDynamicColumns, "SQLOnly", _strNumericSearch, TextSearch + TextSearchParent,
           //  _dtDateFrom, _dtDateTo, "", strHeaderXML, "", null, ref strReturnSQL, ref sReturnHeaderSQL, ref exParam, bool.Parse(ViewState["bUseArchiveData"].ToString()));  // Red 08012018

            DataTable dtBulk = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
                       ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
                       !chkShowDeletedRecords.Checked,
                       chkShowOnlyWarning.Checked == false ? null : (bool?)true,
                        null, null,
                         sOrder, strOrderDirection, 0, 10, ref iTN, ref _iTotalDynamicColumns, "SQLOnly", _strNumericSearch, TextSearch + TextSearchParent,
                         _dtDateFrom, _dtDateTo, "", strHeaderXML, "", null, ref strReturnSQL, ref sReturnHeaderSQL, ref exParam, rdbRecords == "a" ? false : bool.Parse(ViewState["bUseArchiveData"].ToString()));  // Red 08012018
            //End Modification




            if (strExportType == "csv" && rdbRecords == "d")
            {

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.AddHeader("content-disposition",
                "attachment;filename=\"" + lblTitle.Text.Replace("Records - ", "") + " " + DateTime.Today.ToString("yyyyMMdd") + ".csv\"");
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "text/csv";
                string strSelectedColumnIDs = "";
                //Red Bulk Export Data
                //<Begin>
                string strSelectedRecordIDs = "";
                DataTable dtBulkExportRecordIDs = Common.DataTableFromText(strReturnSQL);
                foreach (DataRow dr in dtBulkExportRecordIDs.Rows)
                {
                    strSelectedRecordIDs = strSelectedRecordIDs + dr["DBGSystemRecordID"].ToString() + ",";

                }
                //<End>



                foreach (IDnText item in chklstFields)
                {

                    strSelectedColumnIDs = strSelectedColumnIDs + item.ID + ",";

                }
                if (strSelectedColumnIDs != "")
                    strSelectedColumnIDs = strSelectedColumnIDs.Substring(0, strSelectedColumnIDs.Length - 1);

                try
                {
                    //DataTable dtDump = TheDatabaseS.spExportAllTables(TableID);

                    DataTable dtDump = TheDatabase.spBulkExportData(_theTable.TableID.ToString(), strSelectedRecordIDs, strSelectedColumnIDs, 1);

                    if (dtDump != null) // Red Added this per Jarrod raise error
                    {

                        int iColCountD = dtDump.Columns.Count;

                        foreach (DataRow dr in dtDump.Rows)
                        {
                            for (int i = 0; i < iColCountD; i++)
                            {
                                if (!Convert.IsDBNull(dr[i]))
                                {
                                    //sw.Write("\"" + dr[i].ToString().Replace("\"", "'") + "\"");
                                    sw.Write(dr[i].ToString());
                                }
                            }

                            sw.Write(sw.NewLine);

                        }

                        sw.Close();

                        sw.Close();

                        HttpContext.Current.Response.Output.Write(sw.ToString());

                        /* == Red 09092019: Append cookie */
                        HttpCookie cookie = new HttpCookie("ExportReport");
                        cookie.Value = "Flag";
                        cookie.Expires = DateTime.Now.AddDays(1);
                        HttpContext.Current.Response.AppendCookie(cookie);
                        /* == End Red == */


                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();

                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        return;
                    }

                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "Dump Export", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                    SystemData.ErrorLog_Insert(theErrorLog);
                    return;
                }


            }

            //RP Added Ticket 4523
            if (iTN > 1048576)
            {
                Session["tdbmsgpb"] = "The maximum number of records that can be exported is 1,048,576 rows. Please adjust your filter and try again.";
                return;
            }
            //End Modification

            if (iTN > Common.MaxRecordsExport(_theTable.AccountID, _theTable.TableID))
            {
                if (strReturnSQL != "" && sReturnHeaderSQL != "")
                {
                    //OfflineTaskParameters jsonOTParam = new OfflineTaskParameters();
                    MessageParameters jsonOTParam = new MessageParameters();
                    //RP Modified Ticket 4559
                    //jsonOTParam.ReturnSQL = strReturnSQL.Replace("SELECT *", "SELECT " + AddDoubleQuotes(strReturnSQL));
                    //End Modification

                    /* == Red 16012020: Ticket 5017 == */
                    jsonOTParam.ReturnSQL = strReturnSQL;
                    jsonOTParam.ExportTemplateID = iExportTemplateID;
                    /* == End Red == */

                    jsonOTParam.ReturnHeaderSQL = sReturnHeaderSQL.Replace("SELECT  ","").Replace("'\"\"","").Replace("\"\"'","");
                    jsonOTParam.UniqueFileName = Guid.NewGuid().ToString();
                    jsonOTParam.FileFriendlyName = _theTable.TableName + " - Data File";
                    jsonOTParam.TableID = _theTable.TableID;
                    jsonOTParam.TableName = _theTable.TableName;
                    jsonOTParam.TotalNumberOfRecords = iTN;
                    jsonOTParam.UserID = _ObjUser.UserID;                    

                     //Uncommented by RP - Ticket 4559
                     //OfflineTask newOfflineTask = new OfflineTask(null, _theTable.AccountID, _ObjUser.UserID, 3,
                     //    "ExportRecords", jsonOTParam.GetJSONString(), null, DateTime.Now, null, null);
                     //OfflineTaskManager.dbg_OfflineTask_Insert(newOfflineTask);
                     //End Modification
                     Message newMessage = new Message(null, null, _theTable.TableID, _theTable.AccountID, DateTime.Now,
                        "E", "E", false, "", jsonOTParam.FileFriendlyName, "", "", "");
                    newMessage.Parameters = jsonOTParam.GetJSONString();
                    newMessage.AttachmentFileName = jsonOTParam.UniqueFileName + ".csv";
                    EmailManager.Message_Insert(newMessage);
                    //Uncommented By RP - Ticket 4559
                    ////celan up  task
                    //newOfflineTask.Parameters = jsonOTParam.UniqueFileName + ".csv";
                    //newOfflineTask.ScheduledToRun = DateTime.Now.AddHours(25);
                    //newOfflineTask.Processtorun = "DeleteFile";
                    //OfflineTaskManager.dbg_OfflineTask_Insert(newOfflineTask);
                    //End Modification

                    Session["tdbmsgpb"] = "There are " + iTN.ToString() + " Records, we are going to send this file to your email address.";
                }
                else
                {
                    //send error email to coder
                }

                return;
            }

            //string strReturnSQL = "";
            if (strExportType == "email")
            {
                Exception exParamEmail = null;
                dt = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
                       ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
                       !chkShowDeletedRecords.Checked,
                       chkShowOnlyWarning.Checked == false ? null : (bool?)true,
                       null, null,
                         sOrder, strOrderDirection, 0, _gvPager.TotalRows, ref iTN, ref _iTotalDynamicColumns,
                         _strListType, _strNumericSearch, TextSearch + TextSearchParent,
                       _dtDateFrom, _dtDateTo, "", "", "", int.Parse(hfViewID.Value), ref strReturnSQL,
                       ref strReturnSQL, ref exParamEmail, rdbRecords == "a" ? false : /*RP Modified Ticket 4523 */ bool.Parse(ViewState["bUseArchiveData"].ToString()));  // Red 08012018
            }
            else
            {

                Exception exParamNotEmail = null;
                dt = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
                       ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
                       !chkShowDeletedRecords.Checked,
                       chkShowOnlyWarning.Checked == false ? null : (bool?)true,
                        null, null,
                         sOrder, strOrderDirection, 0, null, ref iTN, ref _iTotalDynamicColumns, "export", _strNumericSearch, TextSearch + TextSearchParent,
                         _dtDateFrom, _dtDateTo, "", strHeaderXML, "", null, ref strReturnSQL, ref strReturnSQL, 
                         ref exParamNotEmail, rdbRecords == "a" ? false : rdbRecords == "a" ? false : /*RP Modified Ticket 4523 */ bool.Parse(ViewState["bUseArchiveData"].ToString()));  // Red 08012018

            }





            if (strHeaderXML != "" && strExportType != "email")
            {

                DataSet ds = new DataSet();
                StringReader sr = new StringReader(strHeaderXML);
                ds.ReadXml(sr);
                DataTable dtHeader = ds.Tables[0];

                for (int j = 0; j < dtHeader.Rows.Count; j++)
                {
                    for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                    {
                        if (_dtRecordColums.Rows[i]["ColumnID"].ToString() == dtHeader.Rows[j]["ColumnID"].ToString())
                        {

                            _dtRecordColums.Rows[i]["Heading"] = dtHeader.Rows[j]["DisplayText"];
                        }
                    }
                }


                _dtRecordColums.AcceptChanges();

            }



            //for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
            //{
            //    for (int j = 0; j < dt.Columns.Count; j++)
            //    {

            //        if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "calculation")
            //        {
            //            if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
            //            {

            //                if (_dtRecordColums.Rows[i]["Calculation"] != DBNull.Value)
            //                {

            //                    bool bDateCal = false;
            //                    if (_dtRecordColums.Rows[i]["TextType"] != DBNull.Value
            //                        && _dtRecordColums.Rows[i]["TextType"].ToString().ToLower() == "d")
            //                    {
            //                        bDateCal = true;
            //                    }


            //                    foreach (DataRow drDS in dt.Rows)
            //                    {
            //                        if (drDS["DBGSystemRecordID"].ToString() != "")
            //                        {

            //                            try
            //                            {
            //                                if (bDateCal == true)
            //                                {
            //                                    string strCalculation = _dtColumnsAll.Rows[i]["Calculation"].ToString();
            //                                    drDS[dt.Columns[j].ColumnName] = TheDatabaseS.GetDateCalculationResult(_dtColumnsAll, strCalculation, int.Parse(drDS["DBGSystemRecordID"].ToString()), _iParentRecordID,
            //                                        _dtRecordColums.Rows[i]["DateCalculationType"] == DBNull.Value ? "" : _dtRecordColums.Rows[i]["DateCalculationType"].ToString());
            //                                }
            //                                else
            //                                {
            //                                    string strFormula = TheDatabaseS.GetCalculationFormula(int.Parse(_qsTableID), _dtRecordColums.Rows[i]["Calculation"].ToString());

            //                                    //drDS[dt.Columns[j].ColumnName] = Common.GetValueFromSQL("SELECT " + strFormula + " FROM Record WHERE RecordID=" + drDS["DBGSystemRecordID"].ToString());
            //                                    drDS[dt.Columns[j].ColumnName] = TheDatabaseS.GetCalculationResult(_dtColumnsAll, strFormula, int.Parse(drDS["DBGSystemRecordID"].ToString()), i, _iParentRecordID);


            //                                }
            //                                if (bDateCal == false && _dtRecordColums.Rows[i]["IsRound"] != DBNull.Value && _dtRecordColums.Rows[i]["RoundNumber"] != DBNull.Value)
            //                                {
            //                                    drDS[dt.Columns[j].ColumnName] = Math.Round(double.Parse(drDS[dt.Columns[j].ColumnName].ToString()), int.Parse(_dtRecordColums.Rows[i]["RoundNumber"].ToString())).ToString("N" + _dtRecordColums.Rows[i]["RoundNumber"].ToString());

            //                                }
            //                            }
            //                            catch
            //                            {
            //                                //
            //                            }
            //                        }
            //                    }

            //                }

            //            }
            //        }
            //    }
            //}

            //}
            dt.AcceptChanges();


            if (chkShowAdvancedOptions.Checked == true)//_bDynamicSearch
            {


            }



            DataRow drFooter = dt.NewRow();

            for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
            {
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (_dtRecordColums.Rows[i]["Heading"].ToString() == dt.Columns[j].ColumnName)
                    {
                        if (_dtRecordColums.Rows[i]["ShowTotal"].ToString().ToLower() == "true")
                        {

                            //drFooter[_dtRecordColums.Rows[i]["NameOnExport"].ToString()] = dt.Compute("SUM([" + _dtRecordColums.Rows[i]["NameOnExport"].ToString() + "])", "[" + _dtRecordColums.Rows[i]["NameOnExport"].ToString() + "]<>''");
                            drFooter[_dtRecordColums.Rows[i]["Heading"].ToString()] = CalculateTotalForAColumn(dt, dt.Columns[j].ColumnName, bool.Parse(_dtRecordColums.Rows[i]["IgnoreSymbols"].ToString().ToLower()));

                        }
                    }

                }

            }


            //for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
            //{
            //    for (int j = dt.Columns.Count - 1; j >= 0; j--)
            //    {
            //        if (_dtRecordColums.Rows[i]["Heading"].ToString() == dt.Columns[j].ColumnName)
            //        {
            //            if (_dtRecordColums.Rows[i]["OnlyForAdmin"].ToString().ToLower() == "1")
            //            {
            //                if (!Common.HaveAccess(_strRecordRightID, "1,2"))
            //                {
            //                    dt.Columns.RemoveAt(j);
            //                }
            //            }


            //        }

            //    }

            //}




            if (strExportType == "email")
            {
                for (int j = dt.Columns.Count - 1; j >= 0; j--)
                {
                    if (dt.Columns[j].ColumnName.IndexOf("_ID**") > -1)
                    {
                        dt.Columns.RemoveAt(j);
                    }

                }

            }

            dt.Rows.Add(drFooter);

            //Round export

            //Red: convert datetimerecorded to string for timespan
            DataTable dtCloned = dt.Clone();
            dtCloned = dt.Clone();
            for (int j = 0; j < dtCloned.Columns.Count; j++)
            {

                if (dtCloned.Columns[j].ColumnName.IndexOf("TTIIMMEE") > -1 && dtCloned.Columns[j].ColumnName.IndexOf("Recorded") > -1)
                {
                    dtCloned.Columns[j].DataType = typeof(string);

                }
                if (dtCloned.Columns[j].ColumnName.IndexOf("DDAATTEE") > -1 && dtCloned.Columns[j].ColumnName.IndexOf("Recorded") > -1)
                {
                    dtCloned.Columns[j].DataType = typeof(string);

                }

            }
            foreach (DataRow row in dt.Rows)
            {
                dtCloned.ImportRow(row);
            }

            foreach (DataRow dr in dtCloned.Rows)
            {
                for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                {
                    for (int j = 0; j < dtCloned.Columns.Count; j++)
                    {
                        //DisplayTextSummary
                        if (_dtRecordColums.Rows[i]["Heading"].ToString() == dtCloned.Columns[j].ColumnName)
                        {
                            if (IsStandard(_dtRecordColums.Rows[i]["SystemName"].ToString()) == false)
                            {

                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "file"
                                    || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "image")
                                {
                                    if (dr[j].ToString() != "" && dr[j].ToString() != "&nbsp;")
                                    {
                                        try
                                        {
                                            if (dr[j].ToString().Length > 37)
                                            {
                                                dr[j] = dr[j].ToString().Substring(37);

                                            }
                                        }
                                        catch
                                        {
                                            //
                                        }
                                    }

                                }

                                if (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "value_text"
                                      && (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
                                     || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "radiobutton")
                                     && _dtRecordColums.Rows[i]["DropdownValues"].ToString() != "")
                                {
                                    if (_dtRecordColums.Rows[i]["Heading"].ToString() == dtCloned.Columns[j].ColumnName)
                                    {

                                        if (dr[j].ToString() != "")
                                        {
                                            string strText = GetTextFromValueForDD(_dtRecordColums.Rows[i]["DropdownValues"].ToString(), dr[j].ToString());
                                            if (strText != "")
                                                dr[j] = strText;
                                        }
                                    }

                                }

                                //if (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
                                //      && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "listbox"
                                //     && _dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
                                //     && _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
                                //{
                                //    if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
                                //    {

                                //        if (dr[j].ToString() != "")
                                //        {
                                //            if (dr[j].ToString().Substring(0,1)==",")
                                //                dr[j] = dr[j].ToString().Substring(1);
                                //        }
                                //    }

                                //}



                                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "time")
                                {
                                    if (_dtRecordColums.Rows[i]["Heading"].ToString() == dtCloned.Columns[j].ColumnName)
                                    {

                                        if (dr[j].ToString() != "")
                                        {
                                            try
                                            {
                                                TimeSpan ts = TimeSpan.Parse(dr[j].ToString());
                                                dr[j] = ts.ToString(@"hh\:mm\:ss");
                                            }
                                            catch
                                            {
                                                //
                                            }


                                        }
                                    }

                                }


                                //if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
                                //    && (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
                                //    || _dtRecordColums.Rows[i]["DropDownType"].ToString() == "tabledd")
                                //     && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
                                //    && _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
                                //{
                                //    if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
                                //    {

                                //        if (dr[j].ToString() != "" && dr[j].ToString() != "&nbsp;")
                                //        {
                                //            try
                                //            {
                                //                Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["LinkedParentColumnID"].ToString()));

                                //                //int iTableRecordID = int.Parse(dr[j].ToString());
                                //                string strLinkedColumnValue = dr[j].ToString();
                                //                DataTable dtTableTableSC = Common.DataTableFromText("SELECT SystemName,DisplayName FROM [Column] WHERE   TableID ="
                                //                 + _dtRecordColums.Rows[i]["TableTableID"].ToString());

                                //                string strDisplayColumn = _dtRecordColums.Rows[i]["DisplayColumn"].ToString();

                                //                foreach (DataRow dr2 in dtTableTableSC.Rows)
                                //                {
                                //                    strDisplayColumn = strDisplayColumn.Replace("[" + dr2["DisplayName"].ToString() + "]", "[" + dr2["SystemName"].ToString() + "]");

                                //                }
                                //                string sstrDisplayColumnOrg = strDisplayColumn;
                                //                string strFilterSQL = "";
                                //                if (theLinkedColumn.SystemName.ToLower() == "recordid")
                                //                {
                                //                    strFilterSQL = strLinkedColumnValue;
                                //                }
                                //                else
                                //                {
                                //                    strFilterSQL = "'" + strLinkedColumnValue.Replace("'", "''") + "'";
                                //                }

                                //                //DataTable dtTheRecord = Common.DataTableFromText("SELECT * FROM Record WHERE RecordID=" + iTableRecordID.ToString());
                                //                DataTable dtTheRecord = Common.DataTableFromText("SELECT * FROM Record WHERE TableID=" + theLinkedColumn.TableID.ToString() + " AND " + theLinkedColumn.SystemName + "=" + strFilterSQL);

                                //                if (dtTheRecord.Rows.Count > 0)
                                //                {
                                //                    foreach (DataColumn dc in dtTheRecord.Columns)
                                //                    {
                                //                        strDisplayColumn = strDisplayColumn.Replace("[" + dc.ColumnName + "]", dtTheRecord.Rows[0][dc.ColumnName].ToString());
                                //                    }
                                //                }
                                //                if (sstrDisplayColumnOrg != strDisplayColumn)
                                //                    dr[j] = strDisplayColumn;
                                //            }
                                //            catch
                                //            {
                                //                //
                                //            }


                                //        }
                                //    }

                                //}



                                if (_dtRecordColums.Rows[i]["IsRound"] != DBNull.Value)
                                {
                                    if (_dtRecordColums.Rows[i]["IsRound"].ToString().ToLower() == "true")
                                    {
                                        if (dr[j].ToString() != "")
                                        {
                                            try
                                            {
                                                if (Common.HasSymbols(dr[j].ToString()) == false)
                                                    //Ticket 4225 - Decimal places - need to retain the data by JV
                                                    //Original data should not be manipulated. Data should show original number regardless of decimal places.
                                                    //Decimal places should only be applied on display (RecordList).
                                                    //dr[j] = Math.Round(double.Parse(Common.IgnoreSymbols(dr[j].ToString())), int.Parse(_dtRecordColums.Rows[i]["RoundNumber"].ToString())).ToString("N" + _dtRecordColums.Rows[i]["RoundNumber"].ToString());
                                                    dr[j] = double.Parse(Common.IgnoreSymbols(dr[j].ToString()));
                                            }
                                            catch
                                            {
                                                //
                                            }
                                        }
                                    }

                                }
                            }

                        }

                        if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "datetime")
                        {
                            if (dr[j].ToString() != "")
                            {
                                DateTime chkDateTime;
                                if (DateTime.TryParse(dr[j].ToString(), out chkDateTime))
                                {
                                    string[] ColumnTypeSplit = dtCloned.Columns[j].ColumnName.ToString().Split('-');
                                    if (ColumnTypeSplit.Length > 1)
                                    //for precise result
                                    //separate date                                   
                                    {
                                        if (ColumnTypeSplit[1].ToLower() == "ddaattee")
                                        {
                                            DateTime dtDate = Convert.ToDateTime(Convert.ToDateTime(dr[j]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));
                                            dr[j] = dtDate.ToShortDateString();
                                        }
                                        if (ColumnTypeSplit[1].ToLower() == "ttiimmee")
                                        {

                                            TimeSpan dtTime = TimeSpan.Parse(Convert.ToDateTime(dr[j]).ToString("HH:mm:ss", CultureInfo.InvariantCulture));
                                            dr[j] = dtTime.ToString(@"hh\:mm\:ss");

                                        }
                                    }


                                    else //together date
                                    {
                                        if (_dtRecordColums.Rows[i]["Heading"].ToString() == dtCloned.Columns[j].ColumnName)
                                        {
                                            if (dr[j].ToString().Length > 18)
                                            {
                                                dr[j] = dr[j].ToString().Substring(0, 19);
                                            }
                                        }

                                    }

                                }

                            }
                        }
                        //  else
                        //   {
                        if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "date")
                        {
                            if (_dtRecordColums.Rows[i]["Heading"].ToString() == dtCloned.Columns[j].ColumnName)
                            {
                                if (dr[j].ToString().Length > 9)
                                {
                                    dr[j] = dr[j].ToString().Substring(0, 10);
                                }
                            }
                        }

                        //  }
                        //oliver <end>


                        //  }

                    }
                }
            }

            if (strExportType == "excel")
            {
                dtCloned.Columns.RemoveAt(dtCloned.Columns.Count - 1);
                dtCloned.Columns.RemoveAt(dtCloned.Columns.Count - 1);

                dtCloned.AcceptChanges();

                //oliver <begin> Ticket 1461
                //DBG.Common.ExportUtil.ExportToExcel2(dt, "\"" + lblTitle.Text.Replace("Records - ", "") + " " + DateTime.Today.ToString("yyyyMMdd") + ".xls" + "\"");
                //oliver <end>

                //Red Ticket 1990
                //<Begin>
                DBG.Common.ExportUtil.ExportToExcel(dtCloned, "\"" + lblTitle.Text.Replace("Records - ", "") + " " + DateTime.Today.ToString("yyyyMMdd") + ".xls" + "\"", dtExportTemplate); //RP uncommented Ticket 4956
                //DBG.Common.ExportUtil.ExportToExcel(dtCloned, "\"" + lblTitle.Text.Replace("Records - ", "") + " " + DateTime.Today.ToString("yyyyMMdd") + ".xlsx" + "\"", dtExportTemplate); //RP commented Ticket 4956
                //<End>
            }
            if (strExportType == "csv" || strExportType == "email")
            {

                int iColCount = dtCloned.Columns.Count;



                for (int i = 0; i < iColCount - 2; i++)
                {
                    sw.Write(dtCloned.Columns[i].ToString().Replace("-DDAATTEE", " (Date)").Replace("-TTIIMMEE", " (Time)"));
                    if (i < iColCount - 3)
                    {
                        sw.Write(",");
                    }

                }

                sw.Write(sw.NewLine);



                // Now write all the rows.


                foreach (DataRow dr in dtCloned.Rows)
                {
                    for (int i = 0; i < iColCount - 2; i++)
                    {
                        if (!Convert.IsDBNull(dr[i]))
                        {
                            sw.Write("\"" + dr[i].ToString().Replace("\"", "'") + "\"");
                        }

                        if (i < iColCount - 3)
                        {
                            sw.Write(",");
                        }
                    }

                    sw.Write(sw.NewLine);

                }

                sw.Close();

                if (strExportType == "csv")
                {

                    try
                    {

                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.Buffer = true;
                        HttpContext.Current.Response.AddHeader("content-disposition",
                        "attachment;filename=\"" + lblTitle.Text.Replace("Records - ", "") + " " + DateTime.Today.ToString("yyyyMMdd") + ".csv\"");
                        HttpContext.Current.Response.Charset = "";
                        HttpContext.Current.Response.ContentType = "text/csv";

                        HttpContext.Current.Response.Output.Write(sw.ToString());

                        /* == Red 09092019: Append cookie */
                        HttpCookie cookie = new HttpCookie("ExportReport");
                        cookie.Value = "Flag";
                        cookie.Expires = DateTime.Now.AddDays(1);
                        HttpContext.Current.Response.AppendCookie(cookie);
                        /* == End Red == */

                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                        return;
                    }
                    catch
                    {
                        //
                        return;
                    }

                }
                if (strExportType == "email")
                {
                    //KG 21/11/17 Ticket 977
                    //string strFolderPath = Server.MapPath("~\\ExportedFiles");
                    string strFolderPath = SystemData.SystemOption_ValueByKey_Account("BulkExportPath", int.Parse(Session["AccountID"].ToString()), null);

                    string strFileName = Guid.NewGuid().ToString() + ".csv";
                    string strFullFileName = strFolderPath + "\\" + strFileName;

                    FileStream Fs = new FileStream(strFullFileName, FileMode.Create);

                    //MemoryStream stream = new MemoryStream();
                    //StreamWriter csvWriter = new StreamWriter(stream, Encoding.UTF8);



                    //////BinaryWriter BWriter = new BinaryWriter(Fs, Encoding.ASCII);  .GetEncoding("UTF-8")
                    //Fs.Flush();
                    //BinaryWriter BWriter = new BinaryWriter(Fs,Encoding.Default);
                    ////BWriter.Flush();
                    //BWriter.Write(sw.ToString());
                    //BWriter.Close();

                    StreamWriter csvWriter = new StreamWriter(Fs, Encoding.UTF8);

                    csvWriter.Write(sw.ToString());
                    csvWriter.Close();

                    Fs.Close();
                    HttpContext.Current.Response.Redirect(Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/User/SendEmail.aspx?Source=" + Cryptography.Encrypt("Recordlist") + "&SearchCriteriaID=" + Cryptography.Encrypt(SearchCriteriaID.ToString()) + "&TableID=" + Cryptography.Encrypt(TableID.ToString()) + "&FileName=" + Cryptography.Encrypt(strFileName), false);

                }
            }

        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "ExportExcelorCSV", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            return;
            //
        }




    }

    //    protected void Pager_OnExportForExcel(object sender, EventArgs e)
    //    {

    //        //DataTable dtExportColumn = RecordManager.ets_Table_Columns_Export(int.Parse(TableID.ToString()),null,null);

    //        //if (dtExportColumn.Rows.Count == 0)
    //        //{
    //        //    ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "NoExportMSG", "alert('Sorry it is not possible to export this table because none of the fields have been marked for export. Please check the table configuration and try again');", true);
    //        //    return;
    //        //}


    //        if (gvTheGrid.VirtualItemCount > Common.MaxRecordsExport)
    //        {

    //            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page),
    //                "message_alert", "alert('There are " + gvTheGrid.VirtualItemCount.ToString() + " Records, we are going to send this file to your email address.');", true);

    //            string strBulkExportPath = SystemData.SystemOption_ValueByKey("BulkExportPath");
    //            string strFileName = Guid.NewGuid().ToString() + "_" + lblTitle.Text.Replace(" ", "").ToString() + ".csv";
    //            string strFullFileName = strBulkExportPath + "\\" + strFileName;
    //            PopulateDateAddedSearch();
    //            int iIsBulkExportOK = RecordManager.ets_Record_List_BulkExport(int.Parse(TableID.ToString()),
    //                ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
    //                !chkIsActive.Checked,
    //                chkShowOnlyWarning.Checked == false ? null : (bool?)true,
    //                _dtDateFrom, _dtDateTo,
    //               strFullFileName);


    //            if (iIsBulkExportOK == 1)
    //            {
    //                //lets zip the file

    //                string filename = strFullFileName;


    //                FileStream infile = File.OpenRead(filename);
    //                byte[] buffer = new byte[infile.Length];
    //                infile.Read(buffer, 0, buffer.Length);
    //                infile.Close();

    //                //FileStream outfile = File.Create(Path.ChangeExtension(filename, "zip"));
    //                FileStream outfile = File.Create(filename + ".zip");

    //                GZipStream gzipStream = new GZipStream(outfile, CompressionMode.Compress);
    //                gzipStream.Write(buffer, 0, buffer.Length);
    //                gzipStream.Close();

    //                //now lets email this to the user.

    //                string strEmail = SystemData.SystemOption_ValueByKey("EmailFrom");
    //                string strEmailServer = SystemData.SystemOption_ValueByKey("EmailServer");
    //                string strEmailUserName = SystemData.SystemOption_ValueByKey("EmailUserName");
    //                string strEmailPassword = SystemData.SystemOption_ValueByKey("EmailPassword");
    //                MailMessage msg = new MailMessage();
    //                msg.From = new MailAddress(strEmail);
    //                msg.Subject = lblTitle.Text + " - data file";
    //                msg.IsBodyHtml = true;

    //                string strBulkExportHTTPPath = SystemData.SystemOption_ValueByKey("BulkExportHTTPPath");

    //                string strTheBody = "<div>Please click the file to download.<a href='" + strBulkExportHTTPPath + "/" + strFileName + ".zip" + "'>" + strFileName + ".zip" + "</a></div>";

    //                msg.Body = strTheBody;
    //                msg.To.Add(_ObjUser.Email);

    //                SmtpClient smtpClient = new SmtpClient(strEmailServer);
    //                smtpClient.Timeout = 99999;
    //                smtpClient.Credentials = new System.Net.NetworkCredential(strEmailUserName, strEmailPassword);

    //#if (!DEBUG)
    //                smtpClient.Send(msg);
    //#endif


    //                if (System.Web.HttpContext.Current.Session["AccountID"] != null)
    //                {

    //                    SecurityManager.Account_SMS_Email_Count(int.Parse(System.Web.HttpContext.Current.Session["AccountID"].ToString()), true, null, null, null);
    //                }

    //            }
    //            else
    //            {
    //                //failed
    //            }

    //            return;
    //        }



    //        Response.Clear();


    //        int iTN = 0;
    //        gvTheGrid.PageIndex = 0;

    //        string strOrderDirection = "DESC";
    //        string sOrder = GetDataKeyNames()[0];

    //        if (gvTheGrid.GridViewSortDirection == SortDirection.Ascending)
    //        {
    //            strOrderDirection = "ASC";
    //        }
    //        sOrder = gvTheGrid.GridViewSortColumn + " ";


    //        if (sOrder.Trim() == "")
    //        {
    //            sOrder = "DBGSystemRecordID";
    //        }




    //        TextSearch = TextSearch + hfTextSearch.Value;
    //        if ((bool)_theUserRole.IsAdvancedSecurity)
    //        {
    //            if (_strRecordRightID == Common.UserRoleType.OwnData)
    //            {
    //                TextSearch = TextSearch + " AND (Record.OwnerUserID=" + _ObjUser.UserID.ToString() + " OR Record.EnteredBy=" + _ObjUser.UserID.ToString() + ")";
    //            }
    //        }
    //        else
    //        {
    //            if (Session["roletype"].ToString() == Common.UserRoleType.OwnData)
    //            {
    //                TextSearch = TextSearch + " AND (Record.OwnerUserID=" + _ObjUser.UserID.ToString() + " OR Record.EnteredBy=" + _ObjUser.UserID.ToString() + ")";
    //            }
    //        }
    //        PopulateDateAddedSearch();

    //        if (chkShowAdvancedOptions.Checked && ddlUploadedBatch.SelectedValue != "")
    //        {
    //            TextSearch = TextSearch + "  AND TempRecordID IN  (SELECT RecordID FROM TempRecord WHERE BatchID=" + ddlUploadedBatch.SelectedValue + ")";
    //        }

    //        string strHeaderXML = "";
    //        if (rdbRecords.SelectedValue == "a")
    //        {
    //            TextSearch = "";
    //            _strNumericSearch = "";
    //            _dtDateFrom = null;
    //            _dtDateTo = null;
    //        }


    //        if (rdbRecords.SelectedValue == "t")
    //        {
    //            TextSearch = "";
    //            _strNumericSearch = "";
    //            _dtDateFrom = null;
    //            _dtDateTo = null;


    //            string sCheck = "";
    //            for (int i = 0; i < gvTheGrid.Rows.Count; i++)
    //            {
    //                bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
    //                if (ischeck)
    //                {
    //                    sCheck = sCheck + ((Label)gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
    //                }               

    //            }

    //            if (string.IsNullOrEmpty(sCheck))
    //            {
    //                ScriptManager.RegisterClientScriptBlock(gvTheGrid, typeof(Page), "message_alert", "alert('Please select a record.');", true);
    //                return;
    //            }

    //            sCheck = sCheck + "-1";
    //            TextSearch = " AND RecordID IN(" + sCheck + ")";

    //        }

    //        //if (rdbRecords.SelectedValue == "d")
    //        //{
    //        //    DataTable dtDump = TheDatabaseS.spExportAllTables(TableID);
    //        //    DBG.Common.ExportUtil.ExportToExcel(dtDump, "\"" + lblTitle.Text.Replace("Records - ", "") + " " + DateTime.Today.ToString("yyyyMMdd") + ".xls" + "\"");

    //        //    return;
    //        //}

    //        bool bFoundHeader = false;

    //        foreach (ListItem item in chklstFields.Items)
    //        {
    //            if (item.Selected)
    //            {
    //                bFoundHeader = true;
    //                break;
    //            }
    //        }

    //        if (bFoundHeader == false)
    //        {
    //            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "NoExportMSG", "alert('Sorry it is not possible to export this table because none of the fields have been marked for export.Please select fields for export.');", true);
    //            return;
    //        }
    //        else
    //        {
    //            strHeaderXML = "<ExportXML>";
    //            foreach (ListItem item in chklstFields.Items)
    //            {
    //                if (item.Selected)
    //                {
    //                    strHeaderXML = strHeaderXML + "<Records>";

    //                    Column theColumn = RecordManager.ets_Column_Details(int.Parse(item.Value));

    //                    string strParentJoinColumnName = "";
    //                    string strChildJoinColumnName = "";
    //                    string strParentTableID = "";
    //                    string strShowViewLink = "0";
    //                    string strFieldsToShow = "";
    //                    if(theColumn.LinkedParentColumnID!=null && theColumn.TableTableID!=null && theColumn.DisplayColumn!="" &&
    //                        (theColumn.ColumnType == "dropdown" || theColumn.ColumnType == "listbox")) //||theColumn.ColumnType=="listbox"
    //                    {
    //                        Column theLinkedParentColumn = RecordManager.ets_Column_Details(((int)theColumn.LinkedParentColumnID));
    //                        strParentJoinColumnName = theLinkedParentColumn.SystemName;
    //                        strChildJoinColumnName = theColumn.SystemName;
    //                        strParentTableID = theColumn.TableTableID.ToString();
    //                        strFieldsToShow = RecordManager.fnReplaceDisplayColumns(theColumn.DisplayColumn, (int)theColumn.TableTableID);
    //                        if(theColumn.ShowViewLink!=null && (bool)theColumn.ShowViewLink)
    //                        {
    //                            strShowViewLink = "1";
    //                        }
    //                    }

    //                    strHeaderXML = strHeaderXML + "<ColumnID>"+item.Value+"</ColumnID>";
    //                    strHeaderXML = strHeaderXML + "<DisplayText>"+ System.Security.SecurityElement.Escape(item.Text)+"</DisplayText>";
    //                    strHeaderXML = strHeaderXML + "<SystemName>" + theColumn.SystemName + "</SystemName>";
    //                    strHeaderXML = strHeaderXML + "<FieldsToShow>" + System.Security.SecurityElement.Escape(strFieldsToShow) + "</FieldsToShow>";
    //                    strHeaderXML = strHeaderXML + "<ParentTableID>" + strParentTableID + "</ParentTableID>";
    //                    strHeaderXML = strHeaderXML + "<ParentJoinColumnName>" + strParentJoinColumnName + "</ParentJoinColumnName>";
    //                    strHeaderXML = strHeaderXML + "<ChildJoinColumnName>" + strChildJoinColumnName + "</ChildJoinColumnName>";
    //                    strHeaderXML = strHeaderXML + "<ShowViewLink>" + strShowViewLink + "</ShowViewLink>";
    //                    strHeaderXML = strHeaderXML + "<ColumnType>" + theColumn.ColumnType + "</ColumnType>";

    //                    strHeaderXML = strHeaderXML + "</Records>";

    //                }
    //            }

    //            strHeaderXML = strHeaderXML + "</ExportXML>";
    //        }


    //        _dtRecordColums = RecordManager.ets_Table_Columns_Summary(TableID, int.Parse(hfViewID.Value));

    //         bool bFoundExportColumn = false;

    //        if (sOrder != "DBGSystemRecordID" && sOrder != "" && strHeaderXML != "" && ViewState["SortOrderColumnID"]!=null)
    //        {
    //            DataSet ds = new DataSet();
    //            StringReader sr = new StringReader(strHeaderXML);
    //            ds.ReadXml(sr);
    //            DataTable dtHeader = ds.Tables[0];


    //            Column theSortColumn = RecordManager.ets_Column_Details(int.Parse(ViewState["SortOrderColumnID"].ToString()));

    //            if (theSortColumn != null)
    //            {
    //                for (int j = 0; j < dtHeader.Rows.Count; j++)
    //                {
    //                    for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //                    {
    //                        if (_dtRecordColums.Rows[i]["ColumnID"].ToString() == dtHeader.Rows[j]["ColumnID"].ToString()
    //                            && theSortColumn.ColumnID.ToString() == _dtRecordColums.Rows[i]["ColumnID"].ToString())
    //                        {

    //                            if (sOrder.IndexOf("CONVERT") > -1)
    //                            {
    //                                sOrder=sOrder.Replace("[" + _dtRecordColums.Rows[i]["Heading"].ToString() + "]",
    //                                    "[" + dtHeader.Rows[j]["DisplayText"].ToString() + "]");
    //                                bFoundExportColumn = true;
    //                                break;

    //                            }
    //                            else
    //                            {
    //                               sOrder= sOrder.Replace(_dtRecordColums.Rows[i]["Heading"].ToString(), dtHeader.Rows[j]["DisplayText"].ToString());
    //                                bFoundExportColumn = true;
    //                                break;
    //                            }


    //                        }
    //                    }

    //                    if (bFoundExportColumn)
    //                    {
    //                        break;
    //                    }
    //                }
    //            }

    //        }

    //        if (bFoundExportColumn==false)
    //        {

    //            sOrder = "DBGSystemRecordID";
    //        }

    //        if (TextSearchParent == null)
    //            TextSearchParent = "";




    //        DataTable dt = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
    //                ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
    //                !chkIsActive.Checked,
    //                chkShowOnlyWarning.Checked == false ? null : (bool?)true,
    //                 null, null,
    //                  sOrder, strOrderDirection, 0, null, ref iTN, ref _iTotalDynamicColumns, "export", _strNumericSearch, TextSearch + TextSearchParent,
    //                  _dtDateFrom, _dtDateTo, "", strHeaderXML, "", null);







    //        if (strHeaderXML != "")
    //        {

    //            DataSet ds = new DataSet();
    //            StringReader sr = new StringReader(strHeaderXML);
    //            ds.ReadXml(sr);
    //            DataTable dtHeader = ds.Tables[0];

    //            for (int j = 0; j < dtHeader.Rows.Count; j++)
    //            {
    //                for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //                {
    //                    if (_dtRecordColums.Rows[i]["ColumnID"].ToString() == dtHeader.Rows[j]["ColumnID"].ToString())
    //                    {

    //                        _dtRecordColums.Rows[i]["NameOnExport"] = dtHeader.Rows[j]["DisplayText"];
    //                    }
    //                }
    //            }


    //            _dtRecordColums.AcceptChanges();

    //        }



    //        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //        {
    //            for (int j = 0; j < dt.Columns.Count; j++)
    //            {               

    //                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "calculation")
    //                {
    //                    if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                    {

    //                        if (_dtRecordColums.Rows[i]["Calculation"] != DBNull.Value)
    //                        {

    //                            bool bDateCal = false;
    //                            if (_dtRecordColums.Rows[i]["TextType"] != DBNull.Value
    //                                && _dtRecordColums.Rows[i]["TextType"].ToString().ToLower() == "d")
    //                            {
    //                                bDateCal = true;
    //                            }


    //                            foreach (DataRow drDS in dt.Rows)
    //                            {
    //                                if (drDS["DBGSystemRecordID"].ToString() != "")
    //                                {

    //                                    try
    //                                    {
    //                                        if (bDateCal == true)
    //                                        {
    //                                            string strCalculation = _dtRecordColums.Rows[i]["Calculation"].ToString();
    //                                            drDS[_dtDataSource.Columns[j].ColumnName] = TheDatabaseS.GetDateCalculationResult(_dtColumnsAll, strCalculation, int.Parse(drDS["DBGSystemRecordID"].ToString()),_iParentRecordID,
    //                                                _dtRecordColums.Rows[i]["DateCalculationType"] == DBNull.Value ? "" : _dtRecordColums.Rows[i]["DateCalculationType"].ToString());
    //                                        }
    //                                        else
    //                                        {
    //                                            string strFormula = TheDatabaseS.GetCalculationFormula(int.Parse(_qsTableID), _dtRecordColums.Rows[i]["Calculation"].ToString());

    //                                            //drDS[dt.Columns[j].ColumnName] = Common.GetValueFromSQL("SELECT " + strFormula + " FROM Record WHERE RecordID=" + drDS["DBGSystemRecordID"].ToString());
    //                                            drDS[dt.Columns[j].ColumnName] = TheDatabaseS.GetCalculationResult(_dtColumnsAll, strFormula, int.Parse(drDS["DBGSystemRecordID"].ToString()), i, _iParentRecordID);


    //                                        }
    //                                        if (bDateCal == false && _dtRecordColums.Rows[i]["IsRound"] != DBNull.Value && _dtRecordColums.Rows[i]["RoundNumber"] != DBNull.Value)
    //                                        {
    //                                            drDS[dt.Columns[j].ColumnName] = Math.Round(double.Parse(drDS[dt.Columns[j].ColumnName].ToString()), int.Parse(_dtRecordColums.Rows[i]["RoundNumber"].ToString())).ToString();

    //                                        }
    //                                    }
    //                                    catch
    //                                    {
    //                                        //
    //                                    }
    //                                }
    //                            }

    //                        }

    //                    }
    //                }
    //            }
    //        }

    //        //}
    //        dt.AcceptChanges();


    //        if (chkShowAdvancedOptions.Checked == true)//_bDynamicSearch
    //        {


    //        }



    //        DataRow drFooter = dt.NewRow();

    //        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //        {
    //            for (int j = 0; j < dt.Columns.Count; j++)
    //            {
    //                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                {
    //                    if (_dtRecordColums.Rows[i]["ShowTotal"].ToString().ToLower() == "true")
    //                    {

    //                        //drFooter[_dtRecordColums.Rows[i]["NameOnExport"].ToString()] = dt.Compute("SUM([" + _dtRecordColums.Rows[i]["NameOnExport"].ToString() + "])", "[" + _dtRecordColums.Rows[i]["NameOnExport"].ToString() + "]<>''");
    //                        drFooter[_dtRecordColums.Rows[i]["NameOnExport"].ToString()] = CalculateTotalForAColumn(dt, dt.Columns[j].ColumnName, bool.Parse(_dtRecordColums.Rows[i]["IgnoreSymbols"].ToString().ToLower()));

    //                    }
    //                }

    //            }

    //        }


    //        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //        {
    //            for (int j = dt.Columns.Count - 1; j >= 0; j--)
    //            {
    //                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                {
    //                    if (_dtRecordColums.Rows[i]["OnlyForAdmin"].ToString().ToLower() == "1")
    //                    {
    //                        if (!Common.HaveAccess(_strRecordRightID, "1,2"))
    //                        {
    //                            dt.Columns.RemoveAt(j);
    //                        }
    //                    }


    //                }

    //            }

    //        }
    //        dt.Rows.Add(drFooter);

    //        //Round export

    //        foreach (DataRow dr in dt.Rows)
    //        {
    //            for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //            {
    //                for (int j = 0; j < dt.Columns.Count; j++)
    //                {
    //                    //DisplayTextSummary
    //                    if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                    {
    //                        if (IsStandard(_dtRecordColums.Rows[i]["SystemName"].ToString()) == false)
    //                        {

    //                            if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "file"
    //                                || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "image")
    //                            {
    //                                if (dr[j].ToString() != "" && dr[j].ToString() != "&nbsp;")
    //                                {
    //                                    try
    //                                    {
    //                                        if (dr[j].ToString().Length > 37)
    //                                        {
    //                                            dr[j] = dr[j].ToString().Substring(37);

    //                                        }
    //                                    }
    //                                    catch
    //                                    {
    //                                        //
    //                                    }
    //                                }

    //                            }

    //                            if (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "value_text"
    //                                  && (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
    //                                 || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "radiobutton")
    //                                 && _dtRecordColums.Rows[i]["DropdownValues"].ToString() != "")
    //                            {
    //                                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                                {

    //                                    if (dr[j].ToString() != "")
    //                                    {
    //                                        string strText = GetTextFromValueForDD(_dtRecordColums.Rows[i]["DropdownValues"].ToString(), dr[j].ToString());
    //                                        if (strText != "")
    //                                            dr[j] = strText;
    //                                    }
    //                                }

    //                            }

    //                            if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "time")
    //                            {
    //                                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                                {

    //                                    if (dr[j].ToString() != "" )
    //                                    {

    //                                        TimeSpan ts = TimeSpan.Parse(dr[j].ToString());
    //                                        dr[j] = ts.ToString(@"hh\:mm");
    //                                    }
    //                                }

    //                            }


    //                            //if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
    //                            //    && (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
    //                            //    || _dtRecordColums.Rows[i]["DropDownType"].ToString() == "tabledd")
    //                            //     && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
    //                            //    && _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
    //                            //{
    //                            //    if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                            //    {

    //                            //        if (dr[j].ToString() != "" && dr[j].ToString() != "&nbsp;")
    //                            //        {
    //                            //            try
    //                            //            {
    //                            //                Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["LinkedParentColumnID"].ToString()));

    //                            //                //int iTableRecordID = int.Parse(dr[j].ToString());
    //                            //                string strLinkedColumnValue = dr[j].ToString();
    //                            //                DataTable dtTableTableSC = Common.DataTableFromText("SELECT SystemName,DisplayName FROM [Column] WHERE   TableID ="
    //                            //                 + _dtRecordColums.Rows[i]["TableTableID"].ToString());

    //                            //                string strDisplayColumn = _dtRecordColums.Rows[i]["DisplayColumn"].ToString();

    //                            //                foreach (DataRow dr2 in dtTableTableSC.Rows)
    //                            //                {
    //                            //                    strDisplayColumn = strDisplayColumn.Replace("[" + dr2["DisplayName"].ToString() + "]", "[" + dr2["SystemName"].ToString() + "]");

    //                            //                }
    //                            //                string sstrDisplayColumnOrg = strDisplayColumn;
    //                            //                string strFilterSQL = "";
    //                            //                if (theLinkedColumn.SystemName.ToLower() == "recordid")
    //                            //                {
    //                            //                    strFilterSQL = strLinkedColumnValue;
    //                            //                }
    //                            //                else
    //                            //                {
    //                            //                    strFilterSQL = "'" + strLinkedColumnValue.Replace("'", "''") + "'";
    //                            //                }

    //                            //                //DataTable dtTheRecord = Common.DataTableFromText("SELECT * FROM Record WHERE RecordID=" + iTableRecordID.ToString());
    //                            //                DataTable dtTheRecord = Common.DataTableFromText("SELECT * FROM Record WHERE TableID=" + theLinkedColumn.TableID.ToString() + " AND " + theLinkedColumn.SystemName + "=" + strFilterSQL);

    //                            //                if (dtTheRecord.Rows.Count > 0)
    //                            //                {
    //                            //                    foreach (DataColumn dc in dtTheRecord.Columns)
    //                            //                    {
    //                            //                        strDisplayColumn = strDisplayColumn.Replace("[" + dc.ColumnName + "]", dtTheRecord.Rows[0][dc.ColumnName].ToString());
    //                            //                    }
    //                            //                }
    //                            //                if (sstrDisplayColumnOrg != strDisplayColumn)
    //                            //                    dr[j] = strDisplayColumn;
    //                            //            }
    //                            //            catch
    //                            //            {
    //                            //                //
    //                            //            }


    //                            //        }
    //                            //    }

    //                            //}



    //                            if (_dtRecordColums.Rows[i]["IsRound"] != DBNull.Value)
    //                            {
    //                                if (_dtRecordColums.Rows[i]["IsRound"].ToString().ToLower() == "true")
    //                                {
    //                                    if (dr[j].ToString() != "")
    //                                    {
    //                                        try
    //                                        {
    //                                            dr[j] = Math.Round(double.Parse(Common.IgnoreSymbols( dr[j].ToString())), int.Parse(_dtRecordColums.Rows[i]["RoundNumber"].ToString())).ToString();
    //                                        }
    //                                        catch
    //                                        {
    //                                            //
    //                                        }
    //                                    }
    //                                }

    //                            }
    //                        }

    //                    }

    //                    //mm:hh
    //                    if (_dtRecordColums.Rows[i]["SystemName"].ToString().ToLower() == "datetimerecorded")
    //                    {

    //                        if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "datetime")
    //                        {
    //                            if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                            {
    //                                if (dr[j].ToString().Length > 15)
    //                                {
    //                                    dr[j] = dr[j].ToString().Substring(0, 16);
    //                                }
    //                            }
    //                        }

    //                        if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "date")
    //                        {
    //                            if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                            {
    //                                if (dr[j].ToString().Length > 9)
    //                                {
    //                                    dr[j] = dr[j].ToString().Substring(0, 10);
    //                                }
    //                            }
    //                        }


    //                    }

    //                }
    //            }
    //        }

    //        dt.Columns.RemoveAt(dt.Columns.Count - 1);
    //        dt.Columns.RemoveAt(dt.Columns.Count - 1);

    //        dt.AcceptChanges();

    //        DBG.Common.ExportUtil.ExportToExcel(dt, "\"" + lblTitle.Text.Replace("Records - ", "") + " " + DateTime.Today.ToString("yyyyMMdd") + ".xls" + "\"");


    //    }
    protected void ddlEnteredBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        lnkSearch_Click(null, null);
    }



    protected void ddlUploadedBatch_SelectedIndexChanged(object sender, EventArgs e)
    {
        lnkSearch_Click(null, null);
    }

    protected void chkIsActive_CheckedChanged(object sender, EventArgs e)
    {
        lnkSearch_Click(null, null);
    }

    protected void chkShowOnlyWarning_CheckedChanged(object sender, EventArgs e)
    {
        lnkSearch_Click(null, null);
    }

    //protected void PopulateYAxis()
    //{

    //    //DataTable dtSCs = RecordManager.ets_Table_Columns_Summary(TableID);
    //    DataTable dtSCs = Common.DataTableFromText("SELECT * FROM [Column] WHERE SummarySearch=1 AND TableID=" + TableID.ToString() + " ORDER BY DisplayOrder");


    //    foreach (DataRow dr in dtSCs.Rows)
    //    {
    //        if (bool.Parse(dr["IsStandard"].ToString()) == false)
    //        {
    //            System.Web.UI.WebControls.ListItem aItem = new System.Web.UI.WebControls.ListItem(
    //                dr["Heading"].ToString() == "" ? dr["DisplayName"].ToString() : dr["Heading"].ToString(), dr["ColumnID"].ToString());

    //            ddlYAxis.Items.Insert(ddlYAxis.Items.Count, aItem);
    //        }

    //    }

    //    System.Web.UI.WebControls.ListItem fItem = new System.Web.UI.WebControls.ListItem("-- None --", "-1");

    //    ddlYAxis.Items.Insert(0, fItem);

    //}




    //protected void ddlFilterValue_SelectedIndexChanged(object sender, EventArgs e)
    //{

    //    lnkSearch_Click(null, null);
    //}

    //protected void cbcvSumFilter_OnddlYAxis_Changed(object sender, EventArgs e)
    //{

    //    lnkSearch_Click(null, null);
    //}
    //protected void PutDDLValue_Text(string strDropdownValues, ref  DropDownList ddl)
    //{
    //    ddl.Items.Clear();
    //    string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

    //    string strValue = "";
    //    string strText = "";

    //    foreach (string s in result)
    //    {
    //        //ListItem liTemp = new ListItem(s, s.ToLower());
    //        strValue = "";
    //        strText = "";
    //        if (s.IndexOf(",") > -1)
    //        {
    //            strValue = s.Substring(0, s.IndexOf(","));
    //            strText = s.Substring(strValue.Length + 1);
    //            if (strValue != "" && strText != "")
    //            {
    //                ListItem liTemp = new ListItem(strText, strValue);
    //                ddl.Items.Add(liTemp);
    //            }
    //        }
    //    }

    //    ListItem liSelect = new ListItem("--Please Select--", "");
    //    ddl.Items.Insert(0, liSelect);

    //}

    //protected void PutDDLValues(string strDropdownValues, ref  DropDownList ddl)
    //{
    //    ddl.Items.Clear();

    //    string[] result = strDropdownValues.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

    //    foreach (string s in result)
    //    {
    //        //ListItem liTemp = new ListItem(s, s.ToLower());
    //        ListItem liTemp = new ListItem(s, s);
    //        ddl.Items.Add(liTemp);
    //    }

    //    ListItem liSelect = new ListItem("--Please Select--", "");
    //    ddl.Items.Insert(0, liSelect);

    //}
    //protected void ddlYAxis_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    //do the show hide

    //    if (ddlYAxis.SelectedValue == "-1")
    //    {
    //        txtLowerLimit.Visible = false;
    //        txtUpperLimit.Visible = false;
    //        lblTo.Visible = false;
    //        txtSearchText.Visible = false;
    //        ddlDropdownColumnSearch.Visible = false;

    //        txtLowerLimit.Text = "";
    //        txtUpperLimit.Text = "";
    //        txtSearchText.Text = "";
    //        if (ddlDropdownColumnSearch.Items.Count > 0)
    //            ddlDropdownColumnSearch.SelectedIndex = 0;

    //        Pager_OnApplyFilter(null, null);
    //        PopulateSearchParams();
    //        lnkSearch_Click(null, null);

    //    }
    //    else
    //    {
    //        Column theColumn = RecordManager.ets_Column_Details(int.Parse(ddlYAxis.SelectedValue));

    //        if (theColumn.ColumnType == "number" && theColumn.IgnoreSymbols == false)
    //        {
    //            txtLowerLimit.Visible = true;
    //            txtUpperLimit.Visible = true;
    //            lblTo.Visible = true;
    //            txtSearchText.Visible = false;

    //            txtLowerDate.Visible = false;
    //            txtUpperDate.Visible = false;
    //            ibLowerDate.Visible = false;
    //            ibUpperDate.Visible = false;
    //            ddlDropdownColumnSearch.Visible = false;
    //        }
    //        else if (theColumn.ColumnType == "dropdown" && theColumn.DropdownValues!=""  && (theColumn.DropDownType == "values" || theColumn.DropDownType == "value_text"))
    //        {
    //            txtLowerLimit.Visible = false;
    //            txtUpperLimit.Visible = false;
    //            lblTo.Visible = false;
    //            txtSearchText.Visible = false;

    //            txtLowerDate.Visible = false;
    //            txtUpperDate.Visible = false;
    //            ibLowerDate.Visible = false;
    //            ibUpperDate.Visible = false;
    //            ddlDropdownColumnSearch.Visible = true;

    //            if (theColumn.DropDownType == "values")
    //            {
    //                PutDDLValues(theColumn.DropdownValues, ref ddlDropdownColumnSearch);
    //            }
    //            else
    //            {
    //                PutDDLValue_Text(theColumn.DropdownValues, ref ddlDropdownColumnSearch);
    //            }

    //        }
    //        else if (theColumn.ColumnType == "date" || theColumn.ColumnType == "datetime")
    //        {
    //            txtLowerLimit.Visible = false;
    //            txtUpperLimit.Visible = false;
    //            lblTo.Visible = true;
    //            txtSearchText.Visible = false;

    //            txtLowerDate.Visible = true;
    //            txtUpperDate.Visible = true;
    //            ibLowerDate.Visible = true;
    //            ibUpperDate.Visible = true;
    //            ddlDropdownColumnSearch.Visible = false;
    //        }
    //        else
    //        {
    //            txtLowerLimit.Visible = false;
    //            txtUpperLimit.Visible = false;
    //            lblTo.Visible = false;
    //            txtSearchText.Visible = true;

    //            txtLowerDate.Visible = false;
    //            txtUpperDate.Visible = false;
    //            ibLowerDate.Visible = false;
    //            ibUpperDate.Visible = false;
    //            ddlDropdownColumnSearch.Visible = false;
    //        }
    //        txtLowerLimit.Text = "";
    //        txtUpperLimit.Text = "";
    //        txtSearchText.Text = "";
    //        txtLowerDate.Text = "";
    //        txtUpperDate.Text = "";
    //        if (ddlDropdownColumnSearch.Items.Count > 0)
    //            ddlDropdownColumnSearch.SelectedIndex = 0;

    //    }

    //}

    //protected void ProcessXMLDef()
    //{
    //    try
    //    {
    //         if (Request.QueryString["XMLDefID"] != null && !IsPostBack && Request.RawUrl.IndexOf("RecordList.aspx")>-1)
    //         {
    //             SearchCriteria aSC = SystemData.SearchCriteria_Detail(int.Parse(Cryptography.Decrypt(Request.QueryString["XMLDefID"].ToString())));

    //             if(aSC!=null)
    //             {
    //                 System.Xml.XmlDocument XMLDef_Doc = new System.Xml.XmlDocument();

    //                 XMLDef_Doc.Load(new StringReader(aSC.SearchText));
    //                 System.Xml.XmlNodeList xnList = XMLDef_Doc.SelectNodes("/root/defaultvalues/dynamic");
    //                 foreach (System.Xml.XmlNode xn in xnList)
    //                 {
    //                     if (xn["controlname"] != null)
    //                     {
    //                         if (xn["controlname"].InnerText == "DropDownList")
    //                         {
    //                             if (pnlFullListControl.FindControl(xn["ID"].InnerText) != null)
    //                             {
    //                                 DropDownList ddlXMLDef_Doc1 = (DropDownList)pnlFullListControl.FindControl(xn["ID"].InnerText);
    //                                 if (ddlXMLDef_Doc1 != null && ddlXMLDef_Doc1.Items.FindByValue(xn["defaultvalue"].InnerText) != null)
    //                                 {
    //                                     ddlXMLDef_Doc1.SelectedValue = xn["defaultvalue"].InnerText;
    //                                 }
    //                             }
    //                         }
    //                         if (xn["controlname"].InnerText == "TextBox")
    //                         {
    //                             //later by MR
    //                         }
    //                     }
    //                 }

    //             }
    //         }
    //    }
    //    catch(Exception ex)
    //    {
    //        //
    //    }

    //}
    protected void ProcessXMLDef()
    {
        try
        {
            if (Request.QueryString["XMLDefID"] != null && !IsPostBack && Request.RawUrl.IndexOf("RecordList.aspx") > -1)
            {
                SearchCriteria aSC = SystemData.SearchCriteria_Detail(int.Parse(Cryptography.Decrypt(Request.QueryString["XMLDefID"].ToString())));

                if (aSC != null)
                {
                    System.Xml.XmlDocument XMLDef_Doc = new System.Xml.XmlDocument();

                    XMLDef_Doc.Load(new StringReader(aSC.SearchText));
                    System.Xml.XmlNodeList xnList = XMLDef_Doc.SelectNodes("/root/defaultvalues/dynamic");
                    foreach (System.Xml.XmlNode xn in xnList)
                    {
                        if (xn["ID"] != null)
                        {
                            if (pnlFullListControl.FindControl(xn["ID"].InnerText) != null
                                && xn["Property"] != null && xn["defaultvalue"] != null)
                            {
                                Control aControl = (Control)pnlFullListControl.FindControl(xn["ID"].InnerText);
                                TheDatabase.SetControlProperty(aControl, xn["Property"].InnerText, xn["defaultvalue"].InnerText);

                            }
                        }
                    }

                }
            }
        }
        catch (Exception ex)
        {
            //
        }

    }

    protected string setSearchFilter(string systemName, string searchValues)
    {
        string strValue = "";
        string[] searchValuesArray = null;

        searchValuesArray = searchValues.Split(';').Select(sValue => sValue.Trim()).ToArray();

        foreach (string strSearchValuesSplitted in searchValuesArray)
        {
            string[] values = strSearchValuesSplitted.Split('-');
            if (values[0].ToString() == systemName)
            {
                strValue = values[1];
                break;
            }           
        }

        return strValue;
    }


    protected void PopulateSearchCriteria(int iSearchCriteriaID, bool bGetNewSCid)
    {
        try
        {
            SearchCriteria theSearchCriteria = null;
            lstReportItemFilter.Clear();
            //if(bGetNewSCid)
            //{
            //    int? iGetNewSCid = SystemData.UserSearch_GetNewSCid(new UserSearch(null, _ObjUser.UserID, int.Parse(hfViewID.Value), "", null));
            //    if (iGetNewSCid != null && iGetNewSCid > 0)
            //    {
            //        iSearchCriteriaID = (int)iGetNewSCid;
            //        _iSearchCriteriaID = iSearchCriteriaID;
            //        ViewState["_iSearchCriteriaID"] = iSearchCriteriaID;
            //        Session["SCid" + hfViewID.Value] = iSearchCriteriaID;
            //    }
            //    else
            //    {
            //        theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);
            //        SystemData.UserSearch_InsertUpdate(new UserSearch(null, _ObjUser.UserID, int.Parse(hfViewID.Value), theSearchCriteria.SearchText, null));
            //        iGetNewSCid = SystemData.UserSearch_GetNewSCid(new UserSearch(null, _ObjUser.UserID, int.Parse(hfViewID.Value), "", null));

            //        if (iGetNewSCid != null && iGetNewSCid > 0)
            //        {
            //            iSearchCriteriaID = (int)iGetNewSCid;
            //            _iSearchCriteriaID = iSearchCriteriaID;
            //            ViewState["_iSearchCriteriaID"] = iSearchCriteriaID;
            //            Session["SCid" + hfViewID.Value] = iSearchCriteriaID;
            //        }
            //    }
            //}

            //RP Modified - Ticket 4305 Change Field Name Breaks Sort Order
            //if (theSearchCriteria == null)
            //     theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);
            Boolean searchnull = false;
            if (theSearchCriteria == null)
            {
                searchnull = true;
                theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);
            }


            if (theSearchCriteria != null)
            {

                System.Xml.XmlDocument xmlSC_Doc = new System.Xml.XmlDocument();

                xmlSC_Doc.Load(new StringReader(theSearchCriteria.SearchText));

                txtDateFrom.Text = xmlSC_Doc.FirstChild[txtDateFrom.ID].InnerText;
                txtDateTo.Text = xmlSC_Doc.FirstChild[txtDateTo.ID].InnerText;

                if (xmlSC_Doc.FirstChild[hfAndOr1.ID] != null)
                    hfAndOr1.Value = xmlSC_Doc.FirstChild[hfAndOr1.ID].InnerText;

                if (xmlSC_Doc.FirstChild[hfAndOr2.ID] != null)
                    hfAndOr2.Value = xmlSC_Doc.FirstChild[hfAndOr2.ID].InnerText;

                if (xmlSC_Doc.FirstChild[hfAndOr3.ID] != null)
                    hfAndOr3.Value = xmlSC_Doc.FirstChild[hfAndOr3.ID].InnerText;

                string strcbcSearchMain = xmlSC_Doc.FirstChild[cbcSearchMain.ID].InnerText;

                if (strcbcSearchMain != "-1" && strcbcSearchMain != "")
                {
                    //PopulateSearchCriteriaCBCMain(int.Parse(strcbcSearchMain));
                    PopulateSearchCriteriaCBC(strcbcSearchMain, cbcSearchMain);
                }

                string strcbcSearch1 = xmlSC_Doc.FirstChild[cbcSearch1.ID].InnerText;

                if (strcbcSearch1 != "-1" && strcbcSearch1 != "")
                {
                    //PopulateSearchCriteriaCBC1(int.Parse(strcbcSearch1));
                    PopulateSearchCriteriaCBC(strcbcSearch1, cbcSearch1);
                }

                string strcbcSearch2 = xmlSC_Doc.FirstChild[cbcSearch2.ID].InnerText;

                if (strcbcSearch2 != "-1" && strcbcSearch2 != "")
                {
                    //PopulateSearchCriteriaCBC2(int.Parse(strcbcSearch2));
                    PopulateSearchCriteriaCBC(strcbcSearch2, cbcSearch2);
                }

                string strcbcSearch3 = xmlSC_Doc.FirstChild[cbcSearch3.ID].InnerText;

                if (strcbcSearch3 != "-1" && strcbcSearch3 != "")
                {
                    //PopulateSearchCriteriaCBC3(int.Parse(strcbcSearch3));
                    PopulateSearchCriteriaCBC(strcbcSearch3, cbcSearch3);
                }

                hfTextSearch.Value = xmlSC_Doc.FirstChild[hfTextSearch.ID].InnerText;

                bool? bIsNumericY = null;
                if (xmlSC_Doc.FirstChild["bIsNumericY"].InnerText != "")
                {
                    bIsNumericY = bool.Parse(xmlSC_Doc.FirstChild["bIsNumericY"].InnerText);
                }
                //txtSearchText.Text = xmlDoc.FirstChild[txtSearchText.ID].InnerText;
                ddlEnteredBy.Text = xmlSC_Doc.FirstChild[ddlEnteredBy.ID].InnerText;

                ddlUploadedBatch.Text = xmlSC_Doc.FirstChild[ddlUploadedBatch.ID].InnerText;
                //RP Added Ticket 4363
                txtUploadedBatch.Text = xmlSC_Doc.FirstChild[ddlUploadedBatch.ID].InnerText;
                //End Modification
                //ddlDropdownColumnSearch.Items.FindByValue(xmlDoc.FirstChild[ddlDropdownColumnSearch.ID].InnerText);
                if (xmlSC_Doc.FirstChild[chkShowDeletedRecords.ID] != null)
                    //Red Removed this
                    // chkShowDeletedRecords.Checked = bool.Parse(xmlSC_Doc.FirstChild[chkShowDeletedRecords.ID].InnerText);
                    //Red Ticket 1941
                    chkShowDeletedRecords.Checked = false;

                chkShowOnlyWarning.Checked = bool.Parse(xmlSC_Doc.FirstChild[chkShowOnlyWarning.ID].InnerText);

                chkShowAdvancedOptions.Checked = bool.Parse(xmlSC_Doc.FirstChild[chkShowAdvancedOptions.ID].InnerText);

                //ddlFilterValue.Text = xmlDoc.FirstChild[ddlFilterValue.ID].InnerText;
                //cbcvSumFilter.SetValue = xmlSC_Doc.FirstChild[cbcvSumFilter.ID].InnerText;

                _iStartIndex = int.Parse(xmlSC_Doc.FirstChild["iStartIndex"].InnerText);
                _iMaxRows = int.Parse(xmlSC_Doc.FirstChild["iMaxRows"].InnerText);

                if (_theView != null)
                {
                    if (_theView.RowsPerPage != null)
                    {
                        _iMaxRows = (int)_theView.RowsPerPage;
                    }
                }

                _strGridViewSortColumn = xmlSC_Doc.FirstChild["GridViewSortColumn"].InnerText;
                _strGridViewSortDirection = xmlSC_Doc.FirstChild["GridViewSortDirection"].InnerText;

                //RP Added (Ticket 4305 - Change Field Name Breaks Sort Order)
                //if (searchnull && _theView != null) //This will capture if search criteria session lost then it will 
                //{
                //    _strGridViewSortColumn = "DBGSystemRecordID";
                //    _strGridViewSortDirection = "DESC";
                //}
                // ^ Above code is "comment out" by MR: Fixing View Sort Order is Being Ignored - 09-Aug-2019


                if (!IsPostBack && ViewState["fixviewsortorder" + _theView.ViewID.ToString()] != null
                 && _theView != null && string.IsNullOrEmpty(_theView.SortOrder) && string.IsNullOrEmpty(_theView.SortOrder2))
                {
                    _strGridViewSortColumn = "DBGSystemRecordID";
                    _strGridViewSortDirection = "DESC";
                }

                if (chkShowAdvancedOptions.Checked == false)
                {
                    foreach (DataRow dr in _dtDynamicSearchColumns.Rows)
                    {

                        try
                        {

                            if (dr["ColumnType"].ToString() == "number" || dr["ColumnType"].ToString() == "calculation")
                            {
                                TextBox txtLowerLimit = (TextBox)tblSearchControls.FindControl("txtLowerLimit_" + dr["SystemName"].ToString());
                                TextBox txtUpperLimit = (TextBox)tblSearchControls.FindControl("txtUpperLimit_" + dr["SystemName"].ToString());

                                if (txtLowerLimit != null && txtUpperLimit != null)
                                {

                                    /* == Red 04022020: Ticket 5116 == */
                                    if (Request.QueryString["SearchValues"] != null)
                                    {
                                        string searchValue = setSearchFilter(dr["SystemName"].ToString() + "_0", Request.QueryString["SearchValues"].ToString());
                                        if (searchValue != "")
                                        {
                                            txtLowerLimit.Text = searchValue;
                                        }

                                        searchValue = setSearchFilter(dr["SystemName"].ToString() + "_1", Request.QueryString["SearchValues"].ToString());
                                        if (searchValue != "")
                                        {
                                            txtUpperLimit.Text = searchValue;
                                        }
                                    }
                                    else
                                    {
                                        if (xmlSC_Doc.FirstChild[txtLowerLimit.ID] != null)
                                            txtLowerLimit.Text = xmlSC_Doc.FirstChild[txtLowerLimit.ID].InnerText;

                                        if (xmlSC_Doc.FirstChild[txtUpperLimit.ID] != null)
                                            txtUpperLimit.Text = xmlSC_Doc.FirstChild[txtUpperLimit.ID].InnerText;
                                    }
                                    /* == End Red == */
                                }

                            }
                            else if (dr["ColumnType"].ToString() == "text")
                            {
                                TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                                if (txtSearch != null)
                                {

                                    /* == Red 04022020: Ticket 5116 == */
                                    if (Request.QueryString["SearchValues"] != null)
                                    {
                                        string searchValue = setSearchFilter(dr["SystemName"].ToString(), Request.QueryString["SearchValues"].ToString());
                                        if (searchValue != "")
                                        {
                                            txtSearch.Text = searchValue;
                                        }

                                    }
                                    else
                                    {
                                        if (xmlSC_Doc.FirstChild[txtSearch.ID] != null)
                                            txtSearch.Text = xmlSC_Doc.FirstChild[txtSearch.ID].InnerText;
                                    }
                                    /* == End Red == */
                                }

                            }
                            else if (dr["ColumnType"].ToString() == "date")
                            {
                                TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                                TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());

                                if (txtLowerDate != null && txtUpperDate != null)
                                {
                                   

                                    /* == Red 04022020: Ticket 5116 == */
                                    if (Request.QueryString["SearchValues"] != null)
                                    {
                                        string searchValue = setSearchFilter(dr["SystemName"].ToString() + "_0", Request.QueryString["SearchValues"].ToString());
                                        if (searchValue != "")
                                        {
                                            txtLowerDate.Text = searchValue;
                                        }

                                        searchValue = setSearchFilter(dr["SystemName"].ToString() + "_1", Request.QueryString["SearchValues"].ToString());
                                        if (searchValue != "")
                                        {
                                            txtUpperDate.Text = searchValue;
                                        }

                                    }
                                    else
                                    {
                                        if (xmlSC_Doc.FirstChild[txtLowerDate.ID] != null)
                                            txtLowerDate.Text = xmlSC_Doc.FirstChild[txtLowerDate.ID].InnerText;

                                        if (xmlSC_Doc.FirstChild[txtUpperDate.ID] != null)
                                            txtUpperDate.Text = xmlSC_Doc.FirstChild[txtUpperDate.ID].InnerText;
                                    }
                                    /* == End Red == */
                                }

                            }
                            else if (dr["ColumnType"].ToString() == "datetime")
                            {
                                TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                                TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());

                                TextBox txtLowerTime = (TextBox)tblSearchControls.FindControl("txtLowerTime_" + dr["SystemName"].ToString());
                                TextBox txtUpperTime = (TextBox)tblSearchControls.FindControl("txtUpperTime_" + dr["SystemName"].ToString());

                               


                                /* == Red 04022020: Ticket 5116 == */
                                if (Request.QueryString["SearchValues"] != null)
                                {
                                    string searchValue = setSearchFilter(dr["SystemName"].ToString() + "_0d", Request.QueryString["SearchValues"].ToString());
                                    if (searchValue != "")
                                    {
                                        txtLowerDate.Text = searchValue;
                                    }

                                    searchValue = setSearchFilter(dr["SystemName"].ToString() + "_1d", Request.QueryString["SearchValues"].ToString());
                                    if (searchValue != "")
                                    {
                                        txtUpperDate.Text = searchValue;
                                    }

                                    searchValue = setSearchFilter(dr["SystemName"].ToString() + "_0t", Request.QueryString["SearchValues"].ToString());
                                    if (searchValue != "")
                                    {
                                        txtLowerTime.Text = searchValue;
                                    }

                                    searchValue = setSearchFilter(dr["SystemName"].ToString() + "_1t", Request.QueryString["SearchValues"].ToString());
                                    if (searchValue != "")
                                    {
                                        txtUpperTime.Text = searchValue;
                                    }
                                }
                                else
                                {
                                    if (txtLowerDate != null && txtUpperDate != null)
                                    {
                                        if (xmlSC_Doc.FirstChild[txtLowerDate.ID] != null)
                                            txtLowerDate.Text = xmlSC_Doc.FirstChild[txtLowerDate.ID].InnerText;

                                        if (xmlSC_Doc.FirstChild[txtUpperDate.ID] != null)
                                            txtUpperDate.Text = xmlSC_Doc.FirstChild[txtUpperDate.ID].InnerText;
                                    }
                                    if (txtLowerTime != null && txtUpperTime != null)
                                    {
                                        if (xmlSC_Doc.FirstChild[txtLowerTime.ID] != null)
                                            txtLowerTime.Text = xmlSC_Doc.FirstChild[txtLowerTime.ID].InnerText;

                                        if (xmlSC_Doc.FirstChild[txtUpperTime.ID] != null)
                                            txtUpperTime.Text = xmlSC_Doc.FirstChild[txtUpperTime.ID].InnerText;
                                    }
                                }
                                /* == End Red == */
                            }
                            else if (dr["ColumnType"].ToString() == "time")
                            {
                                TextBox txtLowerTime = (TextBox)tblSearchControls.FindControl("txtLowerTime_" + dr["SystemName"].ToString());
                                TextBox txtUpperTime = (TextBox)tblSearchControls.FindControl("txtUpperTime_" + dr["SystemName"].ToString());

                                /* == Red 04022020: Ticket 5116 == */
                                if (Request.QueryString["SearchValues"] != null)
                                {
                                    string searchValue = setSearchFilter(dr["SystemName"].ToString() + "_0", Request.QueryString["SearchValues"].ToString());
                                    if (searchValue != "")
                                    {
                                        txtLowerTime.Text = searchValue;
                                    }

                                    searchValue = setSearchFilter(dr["SystemName"].ToString() + "_1", Request.QueryString["SearchValues"].ToString());
                                    if (searchValue != "")
                                    {
                                        txtUpperTime.Text = searchValue;
                                    }

                                }
                                else
                                {
                                    if (txtLowerTime != null && txtUpperTime != null)
                                    {
                                        if (xmlSC_Doc.FirstChild[txtLowerTime.ID] != null)
                                            txtLowerTime.Text = xmlSC_Doc.FirstChild[txtLowerTime.ID].InnerText;

                                        if (xmlSC_Doc.FirstChild[txtUpperTime.ID] != null)
                                            txtUpperTime.Text = xmlSC_Doc.FirstChild[txtUpperTime.ID].InnerText;
                                    }
                                }
                                /* == End Red == */

                            }
                            else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "values" || dr["DropDownType"].ToString() == "value_text"))
                            {
                                DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                                if (ddlSearch != null)
                                {
                                    try
                                    {
                                        /* == Red 04022020: Ticket 5116 == */
                                        if (Request.QueryString["SearchValues"] != null)
                                        {
                                            string searchValue = setSearchFilter(dr["SystemName"].ToString(), Request.QueryString["SearchValues"].ToString());
                                            if (searchValue != "")
                                            {
                                                ddlSearch.Text = searchValue;
                                            }
                                        }
                                        else
                                        {
                                            ddlSearch.Text = xmlSC_Doc.FirstChild[ddlSearch.ID].InnerText;
                                        }
                                        /* == End Red == */
                                    }
                                    catch
                                    {
                                        //
                                    }
                                }

                            }
                            //else if (dr["ColumnType"].ToString() == "radiobutton" && (dr["DropDownType"].ToString() == "values" || dr["DropDownType"].ToString() == "value_text"))
                            else if (dr["ColumnType"].ToString() == "radiobutton" || dr["ColumnType"].ToString() == "listbox"
                                        || dr["ColumnType"].ToString() == "checkbox")
                            {
                                DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                                if (ddlSearch != null)
                                {
                                    

                                    /* == Red 04022020: Ticket 5116 == */
                                    if (Request.QueryString["SearchValues"] != null)
                                    {
                                        string searchValue = setSearchFilter(dr["SystemName"].ToString(), Request.QueryString["SearchValues"].ToString());
                                        if (searchValue != "")
                                        {
                                            ddlSearch.Text = searchValue;
                                        }
                                    }
                                    else
                                    {
                                        ddlSearch.Text = xmlSC_Doc.FirstChild[ddlSearch.ID].InnerText;
                                    }
                                    /* == End Red == */
                                }

                            }
                            else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "table" || dr["DropDownType"].ToString() == "tabledd") &&
                       dr["TableTableID"] != DBNull.Value && dr["DisplayColumn"].ToString() != "")
                            {
                                DropDownList ddlParentSearch = (DropDownList)tblSearchControls.FindControl(_strDynamictabPart + "ddlParentSearch_" + dr["SystemName"].ToString());

                                //if (ddlParentSearch != null && xmlSC_Doc.FirstChild[ddlParentSearch.ID] != null)
                                //{
                                //    ddlParentSearch.Text = xmlSC_Doc.FirstChild[ddlParentSearch.ID].InnerText;
                                //}


                                HiddenField hfParentSearch = (HiddenField)tblSearchControls.FindControl(_strDynamictabPart + "hfParentSearch_" + dr["SystemName"].ToString());

                                if (hfParentSearch != null && xmlSC_Doc.FirstChild[hfParentSearch.ID] != null)
                                {
                                    /* == Red04022020: Ticket 5116 == */
                                    if (Request.QueryString["SearchValues"] != null)
                                    {
                                        string searchValue = setSearchFilter(dr["SystemName"].ToString(), Request.QueryString["SearchValues"].ToString());
                                        if (searchValue != "")
                                        {
                                            hfParentSearch.Value = searchValue;
                                        }
                                        
                                    }
                                    else
                                    {
                                        hfParentSearch.Value = xmlSC_Doc.FirstChild[hfParentSearch.ID].InnerText;


                                        if (hfParentSearch.Value == "[object Object]")
                                        {
                                            hfParentSearch.Value = "";
                                        }
                                    }
                                   /* == End Red == */
                                }
                            }
                            else
                            {
                                TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                                if (txtSearch != null)
                                {

                                    /* == Red 04022020: Ticket 5116 == */
                                    if (Request.QueryString["SearchValues"] != null)
                                    {
                                        string searchValue = setSearchFilter(dr["SystemName"].ToString(), Request.QueryString["SearchValues"].ToString());
                                        if (searchValue != "")
                                        {
                                            txtSearch.Text = searchValue;
                                        }
                                    }
                                    else
                                    {
                                        if (xmlSC_Doc.FirstChild[txtSearch.ID] != null)
                                            txtSearch.Text = xmlSC_Doc.FirstChild[txtSearch.ID].InnerText;
                                    }
                                    /* == End Red == */

                                }

                            }
                        }
                        catch
                        {
                            //
                        }
                    }

                    //
                    foreach (DataRow dr in _dtSearchGroup.Rows)
                    {

                        TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SearchGroupID"].ToString());
                        if (txtSearch != null)
                        {

                            txtSearch.Text = xmlSC_Doc.FirstChild[txtSearch.ID].InnerText;

                        }
                    }

                }

                //PopulateSearchParams();

            }//theSearchCriteria
            else
            {
                // theSearchCriteria is null
                _bBindWithSC = false;
                Session["SCid" + hfViewID.Value] = null;
                ViewState["_iSearchCriteriaID"] = null;
            }
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }


    }

    //protected void Before_Search()
    //{
    //    try
    //    {
    //        DataTable dtBS = Common.DataTableFromText("SELECT ColumnID,ListService FROM [Column] WHERE ListService LIKE '%Before_Search%' AND TableID=" + _theTable.TableID.ToString());
    //        if (dtBS != null && dtBS.Rows.Count > 0)
    //        {
    //            foreach(DataRow drBS in dtBS.Rows)
    //            {
    //                Column theColumn = RecordManager.ets_Column_Details(int.Parse(drBS["ColumnID"].ToString()));
    //                 if (theColumn != null && !string.IsNullOrEmpty(theColumn.ListService))
    //                 {
    //                     string strMessage = "";
    //                     PageLifeCycleService thePageLifeCycleService = JSONField.GetTypedObject<PageLifeCycleService>(theColumn.ListService);
    //                     if (thePageLifeCycleService != null && !string.IsNullOrEmpty(thePageLifeCycleService.ServerControlEvent) && thePageLifeCycleService.ServerControlEvent == "Before_Search")
    //                      {
    //                          List<object> roList = new List<object>();
    //                          List<object> ObjList = new List<object>();
    //                          ObjList.Add(theColumn);
    //                          ObjList.Add(tblSearchControls);
    //                          roList = CustomMethod.DotNetMethod(thePageLifeCycleService.DotNetMethod, ObjList);
    //                          if (roList != null)
    //                          {
    //                              foreach (object obj in roList)
    //                              {
    //                                  //if (obj.GetType().Name == "TextBox")
    //                                  //{
    //                                  //    TextBox aTextBox = (TextBox)obj;
    //                                  //    if(aTextBox!=null)
    //                                  //    {
    //                                  //        if(tblSearchControls.FindControl(aTextBox.ID)!=null)
    //                                  //        {
    //                                  //            TextBox bTextBox = (TextBox)tblSearchControls.FindControl(aTextBox.ID);
    //                                  //            if (bTextBox != null)
    //                                  //            {
    //                                  //                bTextBox.Text = aTextBox.Text;
    //                                  //            }
    //                                  //        }
    //                                  //    }
    //                                  //}
    //                                  //if (obj.GetType().Name == "DropDownList")
    //                                  //{
    //                                  //    DropDownList aDropDownList = (DropDownList)obj;
    //                                  //    if (aDropDownList != null && aDropDownList.SelectedItem!=null)
    //                                  //    {
    //                                  //        if (tblSearchControls.FindControl(aDropDownList.ID) != null)
    //                                  //        {
    //                                  //            DropDownList bDropDownList = (DropDownList)tblSearchControls.FindControl(aDropDownList.ID);
    //                                  //            if (bDropDownList != null)
    //                                  //            {
    //                                  //                bDropDownList.Items.Clear();                                                    

    //                                  //                foreach(ListItem liOne in aDropDownList.Items)
    //                                  //                {
    //                                  //                    bDropDownList.Items.Add(liOne);
    //                                  //                }

    //                                  //                bDropDownList.Text = aDropDownList.SelectedValue;
    //                                  //            }
    //                                  //        }
    //                                  //    }
    //                                  //}
    //                                  if (obj.GetType().Name == "string")
    //                                  {
    //                                      strMessage = (string)obj;

    //                                      if(!IsPostBack)
    //                                      {

    //                                          Session["tdbmsg"] = Session["tdbmsg"] == null ? strMessage : Session["tdbmsg"].ToString()+ " " + strMessage;
    //                                      }
    //                                      else
    //                                      {

    //                                          Session["tdbmsgpb"] = Session["tdbmsgpb"] == null ? strMessage : Session["tdbmsgpb"].ToString() + " " + strMessage;
    //                                      }
    //                                  }
    //                              }
    //                          }
    //                      }//



    //                 }


    //            }



    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorLog theErrorLog = new ErrorLog(null, "before_search in ListService", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //        SystemData.ErrorLog_Insert(theErrorLog);
    //    }

    //}

    protected void PopulateSearchCriteriaCBC(string sSearchText, Pages_UserControl_ControlByColumn cbcX)
    {
        try
        {

            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

            xmlDoc.Load(new StringReader(sSearchText));

            //3611          
            if (xmlDoc.FirstChild["hfControlB_DDL"].InnerText != null) //check this for old users with no xml tag
            {
                if (xmlDoc.FirstChild["hfControlB_DDL"].InnerText != "")
                {
                    hfControlB_DDL.Value = xmlDoc.FirstChild["hfControlB_DDL"].InnerText;
                    Session["hfstrColIDControlDDL"] = xmlDoc.FirstChild["ColIDControlB_DDL"].InnerText;
                    Session["hfControlDDLValue"] = hfControlB_DDL.Value;
                }

                if (xmlDoc.FirstChild["hfDDLcbcSearch1"].InnerText != "")
                {
                    hfDDLcbcSearch1.Value = xmlDoc.FirstChild["hfDDLcbcSearch1"].InnerText;
                    Session["hfstrColIDDDLcbcSearch1"] = xmlDoc.FirstChild["ColIDDDLcbcSearch1"].InnerText;
                    Session["hfDDLcbcSearch1"] = hfDDLcbcSearch1.Value;
                }

                if (xmlDoc.FirstChild["hfDDLcbcSearch2"].InnerText != "")
                {
                    hfDDLcbcSearch2.Value = xmlDoc.FirstChild["hfDDLcbcSearch2"].InnerText;
                    Session["hfstrColIDDDLcbcSearch2"] = xmlDoc.FirstChild["ColIDDDLcbcSearch2"].InnerText;
                    Session["hfDDLcbcSearch2"] = hfDDLcbcSearch2.Value;
                }

                if (xmlDoc.FirstChild["hfDDLcbcSearch3"].InnerText != "")
                {
                    hfDDLcbcSearch3.Value = xmlDoc.FirstChild["hfDDLcbcSearch3"].InnerText;
                    Session["hfstrColIDDDLcbcSearch3"] = xmlDoc.FirstChild["ColIDDDLcbcSearch3"].InnerText;
                    Session["hfDDLcbcSearch3"] = hfDDLcbcSearch3.Value;
                }

            }

            cbcX.ddlYAxisV = xmlDoc.FirstChild["ddlYAxisV"].InnerText;

            cbcX.txtUpperLimitV = xmlDoc.FirstChild["txtUpperLimitV"].InnerText;
            cbcX.txtLowerLimitV = xmlDoc.FirstChild["txtLowerLimitV"].InnerText;
            cbcX.hfTextSearchV = xmlDoc.FirstChild["hfTextSearchV"].InnerText;
            cbcX.txtLowerDateV = xmlDoc.FirstChild["txtLowerDateV"].InnerText;
            cbcX.txtUpperDateV = xmlDoc.FirstChild["txtUpperDateV"].InnerText;
            cbcX.ddlDropdownColumnSearchV = xmlDoc.FirstChild["ddlDropdownColumnSearchV"].InnerText;
            cbcX.txtSearchTextV = xmlDoc.FirstChild["txtSearchTextV"].InnerText;

            if (xmlDoc.FirstChild["CompareOperator"] != null)
                cbcX.CompareOperator = xmlDoc.FirstChild["CompareOperator"].InnerText;

        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.Message;
        }


    }

    //protected void PopulateSearchCriteriaCBC(int iSearchCriteriaID, Pages_UserControl_ControlByColumn cbcX)
    //{
    //    try
    //    {
    //        SearchCriteria theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);


    //        if (theSearchCriteria != null)
    //        {

    //            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

    //            xmlDoc.Load(new StringReader(theSearchCriteria.SearchText));

    //            cbcX.ddlYAxisV = xmlDoc.FirstChild["ddlYAxisV"].InnerText;
    //            cbcX.txtUpperLimitV = xmlDoc.FirstChild["txtUpperLimitV"].InnerText;
    //            cbcX.txtLowerLimitV = xmlDoc.FirstChild["txtLowerLimitV"].InnerText;
    //            cbcX.hfTextSearchV = xmlDoc.FirstChild["hfTextSearchV"].InnerText;
    //            cbcX.txtLowerDateV = xmlDoc.FirstChild["txtLowerDateV"].InnerText;
    //            cbcX.txtUpperDateV = xmlDoc.FirstChild["txtUpperDateV"].InnerText;
    //            cbcX.ddlDropdownColumnSearchV = xmlDoc.FirstChild["ddlDropdownColumnSearchV"].InnerText;
    //            cbcX.txtSearchTextV = xmlDoc.FirstChild["txtSearchTextV"].InnerText;

    //            if (xmlDoc.FirstChild["CompareOperator"] != null)
    //                cbcX.CompareOperator = xmlDoc.FirstChild["CompareOperator"].InnerText;


    //            //PopulateSearchParams();


    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        lblMsg.Text = ex.Message;
    //    }


    //}

    //    protected void PopulateSearchCriteriaCBCMain(int iSearchCriteriaID)
    //    {
    //        try
    //        {
    //            SearchCriteria theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);


    //            if (theSearchCriteria != null)
    //            {

    //                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

    //                xmlDoc.Load(new StringReader(theSearchCriteria.SearchText));

    //                cbcSearchMain.ddlYAxisV = xmlDoc.FirstChild["ddlYAxisV"].InnerText;
    //                cbcSearchMain.txtUpperLimitV = xmlDoc.FirstChild["txtUpperLimitV"].InnerText;
    //                cbcSearchMain.txtLowerLimitV = xmlDoc.FirstChild["txtLowerLimitV"].InnerText;
    //                cbcSearchMain.hfTextSearchV = xmlDoc.FirstChild["hfTextSearchV"].InnerText;
    //                cbcSearchMain.txtLowerDateV = xmlDoc.FirstChild["txtLowerDateV"].InnerText;
    //                cbcSearchMain.txtUpperDateV = xmlDoc.FirstChild["txtUpperDateV"].InnerText;
    //                cbcSearchMain.ddlDropdownColumnSearchV = xmlDoc.FirstChild["ddlDropdownColumnSearchV"].InnerText;
    //                cbcSearchMain.txtSearchTextV = xmlDoc.FirstChild["txtSearchTextV"].InnerText;


    //                //PopulateSearchParams();


    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            lblMsg.Text = ex.Message;
    //        }


    //    }



    //    protected void PopulateSearchCriteriaCBC1(int iSearchCriteriaID)
    //    {
    //        try
    //        {
    //            SearchCriteria theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);


    //            if (theSearchCriteria != null)
    //            {

    //                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

    //                xmlDoc.Load(new StringReader(theSearchCriteria.SearchText));

    //                cbcSearch1.ddlYAxisV = xmlDoc.FirstChild["ddlYAxisV"].InnerText;
    //                cbcSearch1.txtUpperLimitV = xmlDoc.FirstChild["txtUpperLimitV"].InnerText;
    //                cbcSearch1.txtLowerLimitV = xmlDoc.FirstChild["txtLowerLimitV"].InnerText;
    //                cbcSearch1.hfTextSearchV = xmlDoc.FirstChild["hfTextSearchV"].InnerText;
    //                cbcSearch1.txtLowerDateV = xmlDoc.FirstChild["txtLowerDateV"].InnerText;
    //                cbcSearch1.txtUpperDateV = xmlDoc.FirstChild["txtUpperDateV"].InnerText;
    //                cbcSearch1.ddlDropdownColumnSearchV = xmlDoc.FirstChild["ddlDropdownColumnSearchV"].InnerText;
    //                cbcSearch1.txtSearchTextV = xmlDoc.FirstChild["txtSearchTextV"].InnerText;




    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            lblMsg.Text = ex.Message;
    //        }


    //    }


    //    protected void PopulateSearchCriteriaCBC2(int iSearchCriteriaID)
    //    {
    //        try
    //        {
    //            SearchCriteria theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);


    //            if (theSearchCriteria != null)
    //            {

    //                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

    //                xmlDoc.Load(new StringReader(theSearchCriteria.SearchText));

    //                cbcSearch2.ddlYAxisV = xmlDoc.FirstChild["ddlYAxisV"].InnerText;
    //                cbcSearch2.txtUpperLimitV = xmlDoc.FirstChild["txtUpperLimitV"].InnerText;
    //                cbcSearch2.txtLowerLimitV = xmlDoc.FirstChild["txtLowerLimitV"].InnerText;
    //                cbcSearch2.hfTextSearchV = xmlDoc.FirstChild["hfTextSearchV"].InnerText;
    //                cbcSearch2.txtLowerDateV = xmlDoc.FirstChild["txtLowerDateV"].InnerText;
    //                cbcSearch2.txtUpperDateV = xmlDoc.FirstChild["txtUpperDateV"].InnerText;
    //                cbcSearch2.ddlDropdownColumnSearchV = xmlDoc.FirstChild["ddlDropdownColumnSearchV"].InnerText;
    //                cbcSearch2.txtSearchTextV = xmlDoc.FirstChild["txtSearchTextV"].InnerText;




    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            lblMsg.Text = ex.Message;
    //        }


    //    }


    //    protected void PopulateSearchCriteriaCBC3(int iSearchCriteriaID)
    //    {
    //        try
    //        {
    //            SearchCriteria theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);


    //            if (theSearchCriteria != null)
    //            {

    //                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

    //                xmlDoc.Load(new StringReader(theSearchCriteria.SearchText));

    //                cbcSearch3.ddlYAxisV = xmlDoc.FirstChild["ddlYAxisV"].InnerText;
    //                cbcSearch3.txtUpperLimitV = xmlDoc.FirstChild["txtUpperLimitV"].InnerText;
    //                cbcSearch3.txtLowerLimitV = xmlDoc.FirstChild["txtLowerLimitV"].InnerText;
    //                cbcSearch3.hfTextSearchV = xmlDoc.FirstChild["hfTextSearchV"].InnerText;
    //                cbcSearch3.txtLowerDateV = xmlDoc.FirstChild["txtLowerDateV"].InnerText;
    //                cbcSearch3.txtUpperDateV = xmlDoc.FirstChild["txtUpperDateV"].InnerText;
    //                cbcSearch3.ddlDropdownColumnSearchV = xmlDoc.FirstChild["ddlDropdownColumnSearchV"].InnerText;
    //                cbcSearch3.txtSearchTextV = xmlDoc.FirstChild["txtSearchTextV"].InnerText;                

    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            lblMsg.Text = ex.Message;
    //        }


    //    }

    protected void PopulateSearchGroup(int iC, ref TextBox txtSearchText)
    {

        if (txtSearchText.Text == "")
            return;

        //if (TextSearch == "")
        TextSearch = TextSearch + " OR ";

        Column theColumn = RecordManager.ets_Column_Details(iC);





        if (theColumn.ColumnType == "dropdown" && (theColumn.DropDownType == "values" || theColumn.DropDownType == "value_text"))
        {
            if (theColumn.DropDownType == "values")
            {
                if (txtSearchText.Text != "")
                    TextSearch = TextSearch + " Record." + theColumn.SystemName + " like '%" + txtSearchText.Text.Trim().Replace("'", "''") + "%'";
            }
            if (theColumn.DropDownType == "value_text")
            {
                string strSearchValue = Common.GetDDLValueFromText(theColumn.DropdownValues, txtSearchText.Text);
                if (strSearchValue != "")
                {
                    TextSearch = TextSearch + " Record." + theColumn.SystemName + " like '%" + strSearchValue.Trim().Replace("'", "''") + "%'";
                }
            }
        }
        else if (theColumn.ColumnType == "dropdown" && (theColumn.DropDownType == "table" || theColumn.DropDownType == "tabledd") &&
            theColumn.TableTableID != null && theColumn.DisplayColumn != "")
        {


            if (txtSearchText.Text != "")
            {



                //string search = txtSearchText.Text.Replace("'", "''");
                string search = txtSearchText.Text;

                if (search.Trim() == "")
                {
                    return;
                }

                string regex = @"\[(.*?)\]";
                string strDisplayColumn = theColumn.DisplayColumn;
                string text = theColumn.DisplayColumn;

                string strDCForSQL = strDisplayColumn.Replace("'", "''");
                int i = 1;
                string strFirstSystemName = "";
                string strFirstDisplayName = "";
                string strSecondSystemName = "";
                string strSecondDisplayName = "";
                bool bHaveSecond = false;
                //List<string> lstDisplayName = new List<string>();
                //List<string> lstSystemName = new List<string>();
                foreach (Match match in Regex.Matches(text, regex))
                {
                    string strEachDisplayName = match.Groups[1].Value;

                    //lstDisplayName.Add(strEachDisplayName);

                    DataTable dtTableTableSC = Common.DataTableFromText("SELECT SystemName FROM [Column] WHERE   TableID ="
                        + theColumn.TableTableID.ToString() + " AND DisplayName='" + strEachDisplayName + "'");

                    string strEachSystemName = "";
                    if (dtTableTableSC.Rows.Count > 0)
                    {
                        strEachSystemName = dtTableTableSC.Rows[0]["SystemName"].ToString();
                        //lstSystemName.Add(strEachSystemName);
                    }


                    if (i == 1)
                    {

                        strFirstDisplayName = strEachDisplayName;
                        strFirstSystemName = strEachSystemName;
                        strDCForSQL = strDCForSQL.Replace("[" + strEachDisplayName + "]",
                            "ISNULL(CAST(" + strEachSystemName + " AS VARCHAR(MAX)),'') +'");
                    }
                    if (i == 2)
                    {
                        bHaveSecond = true;
                        strSecondDisplayName = strEachDisplayName;
                        strSecondSystemName = strEachSystemName;
                        strDCForSQL = strDCForSQL.Replace("[" + strEachDisplayName + "]",
                            "'+ ISNULL(CAST(" + strEachSystemName + " AS VARCHAR(MAX)),'') +'");
                    }
                    else
                    {
                        strDCForSQL = strDCForSQL.Replace("[" + strEachDisplayName + "]",
                            "'+ ISNULL(CAST(" + strEachSystemName + " AS VARCHAR(MAX)),'') +'");
                    }
                    i = i + 1;
                }
                strDCForSQL = strDCForSQL.Trim() + "'";

                //strDCForSQL = strDCForSQL.Substring(0, strDCForSQL.Length - 2);
                string strSecondSQL = "";
                if (bHaveSecond)
                {
                    strSecondSQL = " OR CAST(" + strSecondSystemName + " AS VARCHAR(MAX)) like '%" + search.Replace("'", "''") + "%'";
                }

                Column theLinkedColumn = RecordManager.ets_Column_Details((int)theColumn.LinkedParentColumnID);

                DataTable dtData = Common.DataTableFromText(@"SELECT TOP 1000 " + theLinkedColumn.SystemName + "," + strDCForSQL + @"
                    FROM Record WHERE  IsActive= 1 AND 
                    TableID=" + theColumn.TableTableID.ToString() + " and (CAST(" + strFirstSystemName + @" AS VARCHAR(MAX)) like '%" + search.Replace("'", "''") + @"%'" + strSecondSQL + ")");

                string strRecordIDs = "";
                foreach (DataRow dr in dtData.Rows)
                {

                    strRecordIDs = strRecordIDs + "'" + dr[0].ToString() + "'" + ",";
                }

                strRecordIDs = strRecordIDs + "'---1---'";


                TextSearch = TextSearch + " Record." + theColumn.SystemName + " IN (" + strRecordIDs + ")";

            }

        }
        else
        {

            if (txtSearchText.Text != "")
            {
                TextSearch = TextSearch + " Record." + theColumn.SystemName + " like'%" + txtSearchText.Text.Trim().Replace("'", "''") + "%'";
            }

        }
    }


    protected void AddVisible(bool bVisible)
    {

    }

    protected void PopulateSearchParams()
    {
        //if (ddlYAxis.SelectedValue == "-1")
        //{
        //_strNumericSearch = "";
        ViewState["seriesColumnID"] = null;
        ViewState["seriesColumnIndex"] = null;
        TextSearch = "";

        System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

        if (_theView.Filter != "" && _theView.FilterControlsInfo != "")
            xmlDoc.Load(new StringReader(_theView.FilterControlsInfo));



        if (chkShowAdvancedOptions.Checked == false)//_bDynamicSearch
        {
            foreach (DataRow dr in _dtDynamicSearchColumns.Rows)
            {
                string strLowerOperator = "=";
                string strViewOperator = "";
                string strActiveOperator = "=";
                bool bFromView = false;
                if (xmlDoc != null)
                {
                    strViewOperator = GetOperatorFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                    if (strViewOperator == "between" || strViewOperator == "")
                        strLowerOperator = "=";

                }

                if (strViewOperator != "")
                    bFromView = true;

                if (strLowerOperator == "empty")
                {

                    TextSearch = TextSearch + " AND (Record." + dr["SystemName"].ToString() + " IS NULL OR LEN(Record." + dr["SystemName"].ToString() + ")=0)";
                }
                else if (strLowerOperator == "notempty")
                {

                    TextSearch = " (Record." + dr["SystemName"].ToString() + " IS NOT NULL AND LEN(Record." + dr["SystemName"].ToString() + ")>0)";
                }


                if (dr["ColumnType"].ToString() == "text")
                {
                    TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                    if (txtSearch != null)
                    {
                        //if (txtSearch.Text != "")
                        //{
                        string strViewFiltervalue = "";

                        string strTextSearch = "";
                        strActiveOperator = strLowerOperator;

                        if (strViewOperator == "" || strViewOperator == "between")
                            strViewOperator = "=";

                        if (txtSearch.Text != "")
                        {
                            strActiveOperator = "=";
                            strLowerOperator = "=";
                            if (strLowerOperator == "=")
                            {
                                strTextSearch = " Record." + dr["SystemName"].ToString() + " LIKE'%" + txtSearch.Text.Trim().Replace("'", "''") + "%'";
                            }
                            /*
                            //Ticket2221 Change By JV = Setting default value in graph page base on Record List search.
                            if (_theTable.GraphSeriesColumnID == int.Parse(dr["ColumnID"].ToString()))
                            {
                                Session["RecordListSeries"] = txtSearch.Text.Trim();
                            }
                            //====================================================================================
                            */
                            //if (strLowerOperator == "<>")
                            //{
                            //    strTextSearch = " AND  CHARINDEX('" + txtSearch.Text.Trim().Replace("'", "''") + "',Record." + dr["SystemName"].ToString()+")=0 ";
                            //}
                            //else
                            //{
                            //    strTextSearch =  " AND Record." + dr["SystemName"].ToString() + " " + strLowerOperator + "'" + txtSearch.Text.Trim().Replace("'", "''") + "'";
                            //}
                          
                        }

                        if (xmlDoc != null)
                        {
                            strViewFiltervalue = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                            if (strViewFiltervalue != null)
                            {
                                string strActiveValue = strViewFiltervalue;

                                if (strTextSearch != "")
                                {
                                    strActiveValue = txtSearch.Text;
                                    bFromView = false;
                                }
                                else
                                {
                                    strActiveOperator = strViewOperator;
                                }
                                UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), strActiveValue, strActiveOperator, bFromView);
                            }
                        }

                        if (strViewFiltervalue == null)
                        {
                            if (strTextSearch != "")
                                TextSearch = TextSearch + " AND " + strTextSearch;
                        }



                        //}
                    }
                }
                else if (dr["ColumnType"].ToString() == "date")
                {
                    TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                    TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());

                    if (txtLowerDate != null && txtUpperDate != null)
                    {

                        string strViewFiltervalue = "";
                        string strDateTextSearch = "";

                        if (txtLowerDate.Text != "" && txtUpperDate.Text == "")
                        {
                            strLowerOperator = _strEqualOrGreaterOperator.Trim();
                            strActiveOperator = strLowerOperator;
                        }

                        if (txtLowerDate.Text != "" && txtUpperDate.Text != "")
                        {
                            strLowerOperator = ">=";
                            strActiveOperator = "between";

                            //Ticket2221 Change By JV = Setting default value in graph page base on Record List search.
                            if (_theTable.GraphXAxisColumnID == int.Parse(dr["ColumnID"].ToString()))
                            {
                                Session["RecordListFrom"] = txtLowerDate.Text.Trim();
                                Session["RecordListTo"] = txtUpperDate.Text.Trim();
                            }
                            //====================================================================================

                        }


                        //if (strViewFiltervalue == "")
                        //{
                        DateTime dateValue;
                        //string strDateTextSearch = "";
                        string strLowerPart = "";
                        txtLowerDate.Text = txtLowerDate.Text.Replace(" ", "");
                        txtUpperDate.Text = txtUpperDate.Text.Replace(" ", "");

                        string strLowerDate = Common.ReturnDateStringFromToken(txtLowerDate.Text.Trim());
                        string strUpperDate = Common.ReturnDateStringFromToken(txtUpperDate.Text.Trim());


                        if (_bEqualOrGreaterOperator == true && strLowerDate.Trim() != "" && strUpperDate.Trim() == "")
                        {
                            if (xmlDoc != null)
                                RemoveViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString());


                        }


                        if (strLowerDate != "")
                        {
                            if (DateTime.TryParseExact(strLowerDate, Common.Dateformats,
                                         new CultureInfo("en-GB"),
                                         DateTimeStyles.None,
                                         out dateValue))
                            {

                                strDateTextSearch = " CONVERT(Datetime,Record." + dr["SystemName"].ToString() + ",103) " + _strEqualOrGreaterOperator + " CONVERT(Datetime,'" + dateValue.ToShortDateString() + "',103)";
                                strLowerPart = " CONVERT(Datetime,Record." + dr["SystemName"].ToString() + ",103) >= CONVERT(Datetime,'" + dateValue.ToShortDateString() + "',103)";

                                //lets check if the user needs the archived data...
                                Session["DateSearchFrom"] = dateValue.ToShortDateString(); //red Ticket 3358_2
                            }
                            else
                            {
                                txtLowerDate.Text = "";
                            }
                        }

                        if (txtUpperDate.Text != "" && txtLowerDate.Text != "")
                        {
                            if (DateTime.TryParseExact(strUpperDate, Common.Dateformats,
                                         new CultureInfo("en-GB"),
                                         DateTimeStyles.None,
                                         out dateValue))
                            {

                                strDateTextSearch = strLowerPart + " AND CONVERT(Datetime,Record." + dr["SystemName"].ToString() + ",103) <= CONVERT(Datetime,'" + dateValue.ToShortDateString() + "',103)";
                            }
                        }
                        else
                        {
                            if (txtUpperDate.Text != "")
                            {
                                if (DateTime.TryParseExact(strUpperDate, Common.Dateformats,
                                             new CultureInfo("en-GB"),
                                             DateTimeStyles.None,
                                             out dateValue))
                                {
                                    strDateTextSearch = " CONVERT(Datetime,Record." + dr["SystemName"].ToString() + ",103) <= CONVERT(Datetime,'" + dateValue.ToShortDateString() + "',103)";
                                }
                                else
                                {
                                    txtUpperDate.Text = "";
                                    strDateTextSearch = "";
                                }
                            }
                        }


                        //if (txtLowerDate.Text != "" && txtUpperDate.Text != "")
                        //{
                        if (strDateTextSearch != "")
                            strDateTextSearch = " (" + strDateTextSearch + ")";
                        //}


                        if (xmlDoc != null)
                        {
                            strViewFiltervalue = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                            if (strViewFiltervalue != null)
                            {
                                string strActiveValue = strViewFiltervalue;
                                if (strDateTextSearch != "")
                                {
                                    strActiveValue = txtLowerDate.Text + "____" + txtUpperDate.Text;
                                    bFromView = false;
                                }
                                else
                                {
                                    strActiveOperator = strViewOperator;
                                }

                                UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), strActiveValue, strActiveOperator, bFromView);
                            }

                        }

                        if (strViewFiltervalue == null)
                        {
                            string strTimeGarbage = " ISDATE(Record." + dr["SystemName"].ToString() + ")=1 AND ";

                            if (strDateTextSearch != "")
                                TextSearch = TextSearch + " AND (" + strTimeGarbage + strDateTextSearch + ")"; ;
                        }


                    }
                }
                else if (dr["ColumnType"].ToString() == "datetime")
                {
                    TextBox txtLowerDate = (TextBox)tblSearchControls.FindControl("txtLowerDate_" + dr["SystemName"].ToString());
                    TextBox txtUpperDate = (TextBox)tblSearchControls.FindControl("txtUpperDate_" + dr["SystemName"].ToString());

                    TextBox txtLowerTime = (TextBox)tblSearchControls.FindControl("txtLowerTime_" + dr["SystemName"].ToString());
                    TextBox txtUpperTime = (TextBox)tblSearchControls.FindControl("txtUpperTime_" + dr["SystemName"].ToString());

                    string strLowerTime = " 00:00:00";
                    string strUpperTime = " 23:59:00";
                    if (txtLowerTime != null && txtUpperTime != null)
                    {

                        strLowerTime = txtLowerTime.Text.Trim() == "" ? " 00:00:00" : " " + txtLowerTime.Text.Trim();
                        strUpperTime = txtUpperTime.Text.Trim() == "" ? " 23:59:00" : " " + txtUpperTime.Text.Trim();
                    }



                    if (txtLowerDate != null && txtUpperDate != null)
                    {
                        string strViewFiltervalue = "";
                        string strDateTextSearch = "";

                        if (txtLowerDate.Text != "" && txtUpperDate.Text == "")
                        {
                            strLowerOperator = _strEqualOrGreaterOperator.Trim();
                            strActiveOperator = strLowerOperator;
                        }

                        if (txtLowerDate.Text != "" && txtUpperDate.Text != "")
                        {
                            strLowerOperator = ">=";
                            strActiveOperator = "between";

                            //Ticket2221 Change By JV = Setting default value in graph page base on Record List search.
                            if (_theTable.GraphXAxisColumnID == int.Parse(dr["ColumnID"].ToString()))
                            {
                                Session["RecordListFrom"] = txtLowerDate.Text.Trim() + " " + strLowerTime;
                                Session["RecordListTo"] = txtUpperDate.Text.Trim() + " " + strUpperTime;
                            }
                            //====================================================================================
                        }


                        DateTime dateValue;
                        string strLowerPart = "";

                        txtLowerDate.Text = txtLowerDate.Text.Replace(" ", "");
                        txtUpperDate.Text = txtUpperDate.Text.Replace(" ", "");

                        string strLowerDate = Common.ReturnDateStringFromToken(txtLowerDate.Text.Trim());
                        string strUpperDate = Common.ReturnDateStringFromToken(txtUpperDate.Text.Trim());

                        if (_bEqualOrGreaterOperator == true && txtLowerDate.Text.Trim() != "" && txtUpperDate.Text.Trim() == "")
                        {
                            if (xmlDoc != null)
                                RemoveViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString());
                        }

                        if (txtLowerDate.Text != "")
                        {
                            if (DateTime.TryParseExact(strLowerDate.Trim() + strLowerTime, Common.DateTimeformats,
                                         new CultureInfo("en-GB"),
                                         DateTimeStyles.None,
                                         out dateValue))
                            {

                                strDateTextSearch = " CONVERT(Datetime,Record." + dr["SystemName"].ToString() + ",103) " + _strEqualOrGreaterOperator + " CONVERT(Datetime,'" + dateValue.ToString() + "',103)";

                                strLowerPart = " CONVERT(Datetime,Record." + dr["SystemName"].ToString() + ",103) >= CONVERT(Datetime,'" + dateValue.ToString() + "',103)";

                                //lets check if the user needs the archived data...
                                //Session["DateTimeSearchFrom"] = dateValue; //red Ticket 3358
                                Session["DateSearchFrom"] = dateValue.ToShortDateString(); ; //red Ticket 3358_2
                            }
                        }

                        if (txtUpperDate.Text != "" && txtLowerDate.Text != "")
                        {
                            if (DateTime.TryParseExact(strUpperDate.Trim() + strUpperTime, Common.DateTimeformats,
                                         new CultureInfo("en-GB"),
                                         DateTimeStyles.None,
                                         out dateValue))
                            {
                                strDateTextSearch = strLowerPart + " AND CONVERT(Datetime,Record." + dr["SystemName"].ToString() + ",103) <= CONVERT(Datetime,'" + dateValue.ToString() + "',103)";
                            }
                        }
                        else
                        {
                            if (txtUpperDate.Text != "")
                            {
                                if (DateTime.TryParseExact(strUpperDate.Trim() + strUpperTime, Common.DateTimeformats,
                                             new CultureInfo("en-GB"),
                                             DateTimeStyles.None,
                                             out dateValue))
                                {
                                    strDateTextSearch = " CONVERT(Datetime,Record." + dr["SystemName"].ToString() + ",103) <= CONVERT(Datetime,'" + dateValue.ToString() + "',103)";
                                }
                            }
                        }


                        //if (txtLowerDate.Text != "" && txtUpperDate.Text != "")
                        //{
                        if (strDateTextSearch != "")
                            strDateTextSearch = " (" + strDateTextSearch + ")";
                        //}


                        if (xmlDoc != null)
                        {
                            strViewFiltervalue = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                            if (strViewFiltervalue != null)
                            {
                                string strActiveValue = strViewFiltervalue;
                                if (strDateTextSearch != "")
                                {
                                    strActiveValue = txtLowerDate.Text + strLowerTime + "____" + txtUpperDate.Text + strUpperTime;
                                    bFromView = false;
                                }
                                else
                                {
                                    strActiveOperator = strViewOperator;
                                }

                                UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), strActiveValue, strActiveOperator, bFromView);
                            }



                        }


                        if (strViewFiltervalue == null)
                        {
                            string strTimeGarbage = " ISDATE(Record." + dr["SystemName"].ToString() + ")=1 AND ";
                            if (strDateTextSearch != "")
                                TextSearch = TextSearch + " AND (" + strTimeGarbage + strDateTextSearch + ")";

                        }


                    }
                }
                else if (dr["ColumnType"].ToString() == "time")
                {
                    TextBox txtLowerTime = (TextBox)tblSearchControls.FindControl("txtLowerTime_" + dr["SystemName"].ToString());
                    TextBox txtUpperTime = (TextBox)tblSearchControls.FindControl("txtUpperTime_" + dr["SystemName"].ToString());

                    if (txtLowerTime != null && txtUpperTime != null)
                    {

                        string strLowerPart = "";
                        string strViewFiltervalue = "";
                        string strDateTextSearch = "";

                        if (txtLowerTime.Text != "" && txtUpperTime.Text.Trim() == "")
                        {
                            strLowerOperator = _strEqualOrGreaterOperator.Trim();
                            strActiveOperator = strLowerOperator;
                        }

                        if (txtLowerTime.Text != "" && txtUpperTime.Text != "")
                        {
                            strLowerOperator = ">=";
                            strActiveOperator = "between";
                        }


                        if (_bEqualOrGreaterOperator == true && txtLowerTime.Text.Trim() != "" && txtUpperTime.Text.Trim() == "")
                        {
                            if (xmlDoc != null)
                                RemoveViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString());

                        }

                        DateTime dateValue;

                        if (txtLowerTime.Text != "")
                        {
                            if (DateTime.TryParseExact(_strTodayShortDate + " " + txtLowerTime.Text.Trim(), Common.DateTimeformats,
                                         new CultureInfo("en-GB"),
                                         DateTimeStyles.None,
                                         out dateValue))
                            {
                                strDateTextSearch = " CONVERT(Datetime,CONVERT(varchar(11),getdate(),103) + ' ' + Record." + dr["SystemName"].ToString() + ",103) " + _strEqualOrGreaterOperator + " CONVERT(Datetime,'" + dateValue.ToString() + "',103)";

                            }
                        }

                        if (txtUpperTime.Text != "" && txtLowerTime.Text != "")
                        {
                            if (DateTime.TryParseExact(_strTodayShortDate + " " + txtLowerTime.Text.Trim(), Common.DateTimeformats,
                                        new CultureInfo("en-GB"),
                                        DateTimeStyles.None,
                                        out dateValue))
                            {
                                strLowerPart = " CONVERT(Datetime,CONVERT(varchar(11),getdate(),103) + ' ' + Record." + dr["SystemName"].ToString() + ",103) >= CONVERT(Datetime,'" + dateValue.ToString() + "',103)";
                            }

                            if (DateTime.TryParseExact(_strTodayShortDate + " " + txtUpperTime.Text.Trim(), Common.DateTimeformats,
                                         new CultureInfo("en-GB"),
                                         DateTimeStyles.None,
                                         out dateValue))
                            {
                                strDateTextSearch = strLowerPart + " AND CONVERT(Datetime,CONVERT(varchar(11),getdate(),103) + ' ' + Record." + dr["SystemName"].ToString() + ",103) <= CONVERT(Datetime,'" + dateValue.ToString() + "',103)";
                            }
                        }
                        else
                        {
                            if (txtUpperTime.Text != "")
                            {
                                if (DateTime.TryParseExact(_strTodayShortDate + " " + txtUpperTime.Text.Trim(), Common.DateTimeformats,
                                             new CultureInfo("en-GB"),
                                             DateTimeStyles.None,
                                             out dateValue))
                                {
                                    strDateTextSearch = " CONVERT(Datetime,CONVERT(varchar(11),getdate(),103) + ' ' + Record." + dr["SystemName"].ToString() + ",103) <= CONVERT(Datetime,'" + dateValue.ToString() + "',103)";
                                }
                            }
                        }


                        //if (txtLowerTime.Text != "" && txtUpperTime.Text != "")
                        //{
                        if (strDateTextSearch != "")
                            strDateTextSearch = " (" + strDateTextSearch + ")";
                        //}





                        if (xmlDoc != null)
                        {
                            strViewFiltervalue = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                            if (strViewFiltervalue != null)
                            {
                                string strActiveValue = strViewFiltervalue;
                                if (strDateTextSearch != "")
                                {
                                    strActiveValue = txtLowerTime.Text + "____" + txtUpperTime.Text;
                                    bFromView = false;
                                }
                                else
                                {
                                    strActiveOperator = strViewOperator;
                                }

                                UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), strActiveValue, strActiveOperator, bFromView);

                            }
                        }

                        if (strViewFiltervalue == null)
                        {
                            string strTimeGarbage = " ISDATE(Record." + dr["SystemName"].ToString() + ")=1 AND ";

                            if (strDateTextSearch != "")
                                TextSearch = TextSearch + " AND (" + strTimeGarbage + strDateTextSearch + ")"; ;
                        }



                    }
                }

                else if (dr["ColumnType"].ToString() == "number" || dr["ColumnType"].ToString() == "calculation")
                {
                    TextBox txtLowerLimit = (TextBox)tblSearchControls.FindControl("txtLowerLimit_" + dr["SystemName"].ToString());
                    TextBox txtUpperLimit = (TextBox)tblSearchControls.FindControl("txtUpperLimit_" + dr["SystemName"].ToString());

                    if (txtLowerLimit != null && txtUpperLimit != null)
                    {

                        string strViewFiltervalue = "";
                        string strNumberTextSearch = "";


                        if (txtLowerLimit.Text != "" && txtUpperLimit.Text.Trim() == "")
                        {
                            strLowerOperator = _strEqualOrGreaterOperator.Trim();
                            strActiveOperator = strLowerOperator;
                        }


                        if (txtLowerLimit.Text.Trim() != "" && txtUpperLimit.Text.Trim() != "")
                        {
                            strLowerOperator = ">=";
                            strActiveOperator = "between";
                        }



                        if (_bEqualOrGreaterOperator == true && txtLowerLimit.Text.Trim() != "" && txtUpperLimit.Text.Trim() == "")
                        {
                            if (xmlDoc != null)
                                RemoveViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString());
                        }


                        if (txtLowerLimit.Text != "")
                        {
                            strNumberTextSearch = " dbo.RemoveNonNumericChar(Record." + dr["SystemName"].ToString() + ") " + _strEqualOrGreaterOperator + " CONVERT(decimal(38,2)," + txtLowerLimit.Text.Trim() + ")";
                        }

                        if (txtLowerLimit.Text != "" && txtUpperLimit.Text != "")
                        {
                            strActiveOperator = "between";
                            strNumberTextSearch = " dbo.RemoveNonNumericChar(Record." + dr["SystemName"].ToString() + ") >= CONVERT(decimal(38,2)," + txtLowerLimit.Text.Trim() + ")";
                            strNumberTextSearch = strNumberTextSearch + " AND dbo.RemoveNonNumericChar(Record." + dr["SystemName"].ToString() + ") <= CONVERT(decimal(38,2)," + txtUpperLimit.Text.Trim() + ")";
                        }
                        else
                        {
                            //if (txtLowerLimit.Text != "")
                            //{
                            //    strNumberTextSearch = " dbo.RemoveNonNumericChar(Record." + dr["SystemName"].ToString() + ") = CONVERT(decimal(20,10)," + txtLowerLimit.Text.Trim() + ")";
                            //}

                            if (txtUpperLimit.Text != "")
                            {

                                strNumberTextSearch = "  dbo.RemoveNonNumericChar(Record." + dr["SystemName"].ToString() + ") <= CONVERT(decimal(38,2)," + txtUpperLimit.Text.Trim() + ")";
                            }
                        }


                        if (strNumberTextSearch != "")
                        {
                            strNumberTextSearch = " dbo.RemoveNonNumericChar(Record." + dr["SystemName"].ToString() + ")<>'' AND " + strNumberTextSearch;
                            strNumberTextSearch = "(" + strNumberTextSearch + ")";
                        }


                        if (xmlDoc != null)
                        {
                            strViewFiltervalue = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                            if (strViewFiltervalue != null)
                            {
                                //UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), txtLowerLimit.Text + "____" + txtUpperLimit.Text);

                                string strActiveValue = strViewFiltervalue;

                                if (strNumberTextSearch != "")
                                {
                                    strActiveValue = txtLowerLimit.Text + "____" + txtUpperLimit.Text;
                                    bFromView = false;
                                }
                                else
                                {
                                    strActiveOperator = strViewOperator;
                                }

                                UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), strActiveValue, strActiveOperator, bFromView);
                            }


                        }

                        if (strViewFiltervalue == null)
                        {
                            if (strNumberTextSearch != "")
                                TextSearch = TextSearch + " AND " + strNumberTextSearch;
                        }




                    }
                }

                else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "values"
                    || dr["DropDownType"].ToString() == "value_text"))
                {

                    DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                    if (ddlSearch != null)
                    {
                        string strViewFiltervalue = "";
                        string strTextSearch = "";
                        strActiveOperator = strLowerOperator;

                        if (strViewOperator == "" || strViewOperator == "between")
                            strViewOperator = "=";

                        if (ddlSearch.SelectedItem != null)
                        {
                            if (ddlSearch.SelectedValue != "" && ddlSearch.SelectedValue != "vf_vf_vf")
                            {
                                strActiveOperator = "=";
                                strLowerOperator = "=";
                                if (strLowerOperator == "=")
                                {
                                    strTextSearch = " Record." + dr["SystemName"].ToString() + " ='" + ddlSearch.SelectedValue.Trim().Replace("'", "''") + "'";
                                }

                                /* === Red: Commented this one out; use the code below instead === */
                                //Ticket2221 Change By JV = Setting default value in graph page base on Record List search.
                                //if (_theTable.GraphSeriesColumnID == int.Parse(dr["ColumnID"].ToString()))
                                //{
                                //    Session["RecordListSeries"] = ddlSearch.SelectedValue;
                                //}

                                if (ViewState["seriesColumnID"] == null && ViewState["seriesColumnIndex"] == null)
                                {
                                    ViewState["seriesColumnID"] = dr["ColumnID"].ToString();
                                    ViewState["seriesColumnIndex"] = dr["ColumnIndex"].ToString();
                                    Session["RecordListSeries"] = ddlSearch.SelectedValue;
                                }                              
                                else if (int.Parse(dr["ColumnIndex"].ToString()) < int.Parse(ViewState["seriesColumnIndex"].ToString()))
                                {
                                    ViewState["seriesColumnID"] = dr["ColumnID"].ToString();
                                    ViewState["seriesColumnIndex"] = dr["ColumnIndex"].ToString();
                                    Session["RecordListSeries"] = ddlSearch.SelectedValue;
                                }
                                /* === end Red === */
                                //====================================================================================
                            }

                            if (xmlDoc != null)
                            {
                                strViewFiltervalue = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                                if (strViewFiltervalue != null)
                                {
                                    string strActiveValue = strViewFiltervalue;

                                    if (strTextSearch != "")
                                    {
                                        strActiveValue = ddlSearch.SelectedValue;
                                        bFromView = false;
                                    }
                                    else
                                    {
                                        strActiveOperator = strViewOperator;
                                    }
                                    if (ddlSearch.SelectedValue == "vf_vf_vf")
                                    {
                                        strActiveValue = "";
                                        bFromView = false;
                                        strActiveOperator = "=";
                                    }
                                    UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), strActiveValue, strActiveOperator, bFromView);
                                }

                            }

                            if (strViewFiltervalue == null && ddlSearch.SelectedValue != "")
                            {
                                TextSearch = TextSearch + " AND Record." + dr["SystemName"].ToString() + " ='" + ddlSearch.SelectedValue.Trim().Replace("'", "''") + "'";
                            }
                        }
                    }
                }

                //else if (dr["ColumnType"].ToString() == "radiobutton" && (dr["DropDownType"].ToString() == "values" || dr["DropDownType"].ToString() == "value_text"))
                //{

                //    DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                //    if (ddlSearch != null)
                //    {
                //        string strViewFiltervalue = "";
                //        string strTextSearch = "";
                //        strActiveOperator = strLowerOperator;

                //        if (strViewOperator == "" || strViewOperator == "between")
                //            strViewOperator = "=";

                //        if (ddlSearch.SelectedItem != null) // && ddlSearch.SelectedValue != ""
                //        {
                //            if (ddlSearch.SelectedValue != "")
                //            {
                //                strActiveOperator = "=";
                //                strLowerOperator = "=";
                //                if (strLowerOperator == "=")
                //                {
                //                    strTextSearch = " Record." + dr["SystemName"].ToString() + " ='" + ddlSearch.SelectedValue.Trim().Replace("'", "''") + "'";
                //                }
                //            }

                //            if (xmlDoc != null)
                //            {
                //                strViewFiltervalue = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                //                if (strViewFiltervalue != null)
                //                {
                //                    string strActiveValue = strViewFiltervalue;

                //                    if (strTextSearch != "")
                //                    {
                //                        strActiveValue = ddlSearch.SelectedValue;
                //                        bFromView = false;
                //                    }
                //                    else
                //                    {
                //                        strActiveOperator = strViewOperator;
                //                    }

                //                    UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), strActiveValue, strActiveOperator, bFromView);
                //                }

                //            }

                //            if (strViewFiltervalue == null && ddlSearch.SelectedValue != "")
                //            {
                //                TextSearch = TextSearch + " AND Record." + dr["SystemName"].ToString() + " ='" + ddlSearch.SelectedValue.Trim().Replace("'", "''") + "'";
                //            }

                //        }
                //    }
                //}

                else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "table" || dr["DropDownType"].ToString() == "tabledd") &&
                    dr["TableTableID"] != DBNull.Value && dr["DisplayColumn"].ToString() != "")
                {
                    //here the Reset button has been clicked...
                    //let get the Default value from ViewEdit... - Red                   
                    if (_bReset)
                    {
                        PopulateDropDownSearchOnLoad(int.Parse(dr["ColumnID"].ToString()));
                    }
                    
                    HiddenField hfParentSearch = (HiddenField)tblSearchControls.FindControl(_strDynamictabPart + "hfParentSearch_" + dr["SystemName"].ToString());
                    DropDownList ddlParentSearch = (DropDownList)tblSearchControls.FindControl(_strDynamictabPart + "ddlParentSearch_" + dr["SystemName"].ToString());
                   
                    //if (ddlParentSearch != null)
                    if (hfParentSearch != null)
                    {
                        if (hfParentSearch.Value == "[object Object]")
                        {
                            hfParentSearch.Value = "";
                        }
                        string strViewFiltervalue = "";
                        string strTextSearch = "";
                        strActiveOperator = strLowerOperator;

                        if (strViewOperator == "" || strViewOperator == "between")
                            strViewOperator = "=";

                        //if (ddlParentSearch.SelectedItem != null)
                        if (hfParentSearch.Value != "")
                        {
                          //  if (ddlParentSearch.SelectedValue != "" && ddlParentSearch.SelectedValue != "vf_vf_vf")
                            if (hfParentSearch.Value != "" )
                            {
                                strActiveOperator = "=";
                                strLowerOperator = "=";
                                if (strLowerOperator == "=")
                                {
                                    if (hfParentSearch.Value.IndexOf(",")>-1)
                                    {
                                        string strTemp = "'" + hfParentSearch.Value + "'";
                                        strTemp = strTemp.Replace(",", "','");
                                        strTextSearch = " Record." + dr["SystemName"].ToString() + " IN (" + strTemp + ")";
                                    }
                                    else
                                    {
                                        strTextSearch = " Record." + dr["SystemName"].ToString() + " ='" + hfParentSearch.Value + "'";
                                    }
                                    //strTextSearch = " Record." + dr["SystemName"].ToString() + " ='" + ddlParentSearch.SelectedValue.Trim().Replace("'", "''") + "'";
                                }

                                /* === Red: commented this one out; use the code below instead === */
                                //Ticket2221 Change By JV = Setting default value in graph page base on Record List search.
                                //if (_theTable.GraphSeriesColumnID == int.Parse(dr["ColumnID"].ToString()))
                                //{
                                //    //Session["RecordListSeries"] = ddlParentSearch.SelectedValue;
                                //    Session["RecordListSeries"] = hfParentSearch.Value;
                                //}

                                if (ViewState["seriesColumnID"] == null && ViewState["seriesColumnIndex"] == null)
                                {
                                    ViewState["seriesColumnID"] = dr["ColumnID"].ToString();
                                    ViewState["seriesColumnIndex"] = dr["ColumnIndex"].ToString();
                                    Session["RecordListSeries"] = hfParentSearch.Value;
                                }
                                else if (int.Parse(dr["ColumnIndex"].ToString()) < int.Parse(ViewState["seriesColumnIndex"].ToString()))
                                {
                                    ViewState["seriesColumnID"] = dr["ColumnID"].ToString();
                                    ViewState["seriesColumnIndex"] = dr["ColumnIndex"].ToString();
                                    Session["RecordListSeries"] = hfParentSearch.Value;
                                }
                                /* === end Red === */
                                //====================================================================================
                            }



                            if (xmlDoc != null)
                            {

                                strViewFiltervalue = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());

                                if (strViewFiltervalue != null)
                                {
                                    string strActiveValue = strViewFiltervalue;

                                    if (strActiveValue == "-user-" && dr["LinkedParentColumnID"] != DBNull.Value)
                                    {
                                        string strColumnuserID = Common.GetValueFromSQL(@"SELECT TOP 1 ColumnID FROM [Column] WHERE TableID=" + dr["TableTableID"].ToString() + @" AND 
                                ColumnType='dropdown' AND DisplayColumn IS NOT NULL
                                            AND TableTableID=-1");

                                        if (strColumnuserID != "")
                                        {
                                            string strLoginText = "";
                                            SecurityManager.ProcessLoginUserDefault(dr["TableTableID"].ToString(), "",
                                            dr["LinkedParentColumnID"].ToString(), _ObjUser.UserID.ToString(), ref strActiveValue, ref strLoginText);

                                        }
                                    }

                                    if (strTextSearch != "")
                                    {
                                        strActiveValue = hfParentSearch.Value;// ddlParentSearch.SelectedValue;
                                        bFromView = false;
                                    }
                                    else
                                    {
                                        strActiveOperator = strViewOperator;
                                    }
                                    if (hfParentSearch.Value == "" && _bReset==false) //ddlParentSearch.SelectedValue
                                    {
                                        strActiveValue = "";
                                        bFromView = false;
                                        strActiveOperator = "=";
                                    }
                                    if (strActiveValue.IndexOf(",") > -1)
                                    {
                                        string[] strActiveValueArray = strActiveValue.Split(',').Select(sValue => sValue.Trim()).ToArray(); ;
                                        int? iCBCSearch = 1;
                                        string strSearchValue = "";
                                        foreach (string ActiveValueSplitted in strActiveValueArray)
                                        {
                                            
                                            bool bExists = false;

                                            UpdateViewMultipleDDLFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), ActiveValueSplitted, strActiveOperator, bFromView, iCBCSearch, ref bExists);
                                            
                                            //here we add a query because it is not in the View
                                            if (!bExists)
                                            {
                                                strSearchValue = strSearchValue + ActiveValueSplitted + ",";

                                                //here we reset the View Edit, not needed... 
                                                //all where clause should be outside View where cluase
                                                bFromView = false;
                                                UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), "", "=", bFromView);
                                            }

                                            iCBCSearch += 1;


                                        }
                                        
                                        if (strSearchValue != "")
                                        {
                                            if (strSearchValue.IndexOf(",") > -1)
                                            {
                                                string strTemp = "'" + strActiveValue + "'";
                                                strTemp = strTemp.Replace(",", "','");
                                                strTextSearch = " Record." + dr["SystemName"].ToString() + " IN (" + strTemp + ")";
                                            }
                                            else
                                            {
                                                strTextSearch = " Record." + dr["SystemName"].ToString() + " ='" + strActiveValue + "'";
                                            }

                                            TextSearch = TextSearch + " AND " + strTextSearch;

                                        }

                                    }
                                    else
                                    {
                                        UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), strActiveValue, strActiveOperator, bFromView);
                                    }
                                     
                                }
                            }

                            if (strViewFiltervalue == null && hfParentSearch.Value != "") //ddlParentSearch.SelectedValue
                            {
                                if (hfParentSearch.Value.IndexOf(",") > -1)
                                {
                                    string strTemp = "'" + hfParentSearch.Value + "'";
                                    strTemp = strTemp.Replace(",", "','");
                                    strTextSearch = " Record." + dr["SystemName"].ToString() + " IN (" + strTemp + ")";
                                }
                                else
                                {
                                    strTextSearch = " Record." + dr["SystemName"].ToString() + " ='" + hfParentSearch.Value + "'";
                                }

                                //TextSearch = TextSearch + " AND Record." + dr["SystemName"].ToString() + " ='" + ddlParentSearch.SelectedValue.Trim().Replace("'", "''") + "'";
                                TextSearch = TextSearch + " AND " + strTextSearch;
                            }
                        }
                        else
                        {
                            //here we did not Reset the dropdown; but unselected default from ViewEdit... - Red
                            if (xmlDoc != null)
                            {

                                strViewFiltervalue = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                                string strOperator = GetOperatorFromViewFilter(xmlDoc, dr["ColumnID"].ToString());

                                if (strViewFiltervalue != null)
                                {
                                    string strActiveValue = strViewFiltervalue;

                                    if (hfParentSearch.Value == "" && _bReset == false && strOperator == "=") //ddlParentSearch.SelectedValue
                                    {
                                        strActiveValue = "";
                                        bFromView = true;
                                        strActiveOperator = "=";
                                        UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), strActiveValue, strActiveOperator, bFromView);
                                    }
                                    
                                }
                            }
                        }

                        string strddlParentSearchSet = "";

                        if (hfParentSearch.Value != "")
                        {
                           if (ddlParentSearch != null)
                            {
                                if (ddlParentSearch.Items.Count == 0)
                                {
                                    RecordManager.PopulateTableDropDown(int.Parse(dr["ColumnID"].ToString()), ref ddlParentSearch, "", "", Boolean.Parse(dr["ShowAllValuesOnSearch"].ToString()));
                                }
                            }
                                 
                            string strValue = "'" + hfParentSearch.Value + "'";
                            strValue = strValue.Replace(",", "','");

                            strddlParentSearchSet = "$('#" + ddlParentSearch.ID + "').multipleSelect('setSelects',[" + strValue + "]);";
                        }
                        else
                        {
                            strddlParentSearchSet = "$('#" + ddlParentSearch.ID + "').multipleSelect('uncheckAll');";
                        }
                        strddlParentSearchSet = @" $(document).ready(function () {  "
                            + strddlParentSearchSet + @"
                                                 });
                                                ";
                        ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "jsddlParentSearchSet" + _strDynamictabPart + dr["SystemName"].ToString(), strddlParentSearchSet, true);

                    }
                }
                //else if (dr["ColumnType"].ToString() == "listbox" && (dr["DropDownType"].ToString() == "value_text" ))
                //{


                //    TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                //    if (txtSearch != null)
                //    {

                //        //if (txtSearch.Text != "")
                //        //{
                //            string strText = GetValueFromTextForList(dr["DropdownValues"].ToString(), txtSearch.Text);

                //            string strViewFiltervalue = "";
                //            if (xmlDoc != null)
                //            {
                //                strViewFiltervalue = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                //                if (strViewFiltervalue != "")
                //                    UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), strText, strLowerOperator, bFromView);
                //            }

                //            if (strViewFiltervalue == null && strText != "")
                //                TextSearch = TextSearch + " AND Record." + dr["SystemName"].ToString() + " like'%" + strText.Trim().Replace("'", "''") + "%'";
                //        //}
                //    }
                //}
                else if (dr["ColumnType"].ToString() == "radiobutton" || dr["ColumnType"].ToString() == "listbox"
                    || dr["ColumnType"].ToString() == "checkbox")
                {

                    DropDownList ddlSearch = (DropDownList)tblSearchControls.FindControl("ddlSearch_" + dr["SystemName"].ToString());
                    if (ddlSearch != null)
                    {
                        string strViewFiltervalue = "";
                        string strTextSearch = "";
                        strActiveOperator = strLowerOperator;

                        if (strViewOperator == "" || strViewOperator == "between")
                            strViewOperator = "=";

                        if (ddlSearch.SelectedItem != null)
                        {
                            if (ddlSearch.SelectedValue != "" && ddlSearch.SelectedValue != "vf_vf_vf")
                            {
                                strActiveOperator = "=";
                                strLowerOperator = "=";
                                if (strLowerOperator == "=")
                                {

                                    if (dr["ColumnType"].ToString() == "listbox")
                                    {
                                        strTextSearch = " CHARINDEX('," + ddlSearch.SelectedValue.Trim().Replace("'", "''") + ",' ,',' + Record." + dr["SystemName"].ToString() + " + ',')>0";
                                    }
                                    else
                                    {
                                        strTextSearch = " Record." + dr["SystemName"].ToString() + " ='" + ddlSearch.SelectedValue.Trim().Replace("'", "''") + "'";
                                    }

                                }
                            }

                            if (xmlDoc != null)
                            {
                                strViewFiltervalue = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                                if (strViewFiltervalue != null)
                                {
                                    string strActiveValue = strViewFiltervalue;

                                    if (strTextSearch != "")
                                    {
                                        strActiveValue = ddlSearch.SelectedValue;
                                        bFromView = false;
                                    }
                                    else
                                    {
                                        strActiveOperator = strViewOperator;
                                    }
                                    if (ddlSearch.SelectedValue == "vf_vf_vf")
                                    {
                                        strActiveValue = "";
                                        bFromView = false;
                                        strActiveOperator = "=";
                                    }
                                    UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), strActiveValue, strActiveOperator, bFromView);
                                }

                            }

                            if (strViewFiltervalue == null && ddlSearch.SelectedValue != "")
                            {
                                //TextSearch = TextSearch + " AND Record." + dr["SystemName"].ToString() + " ='" + ddlSearch.SelectedValue.Trim().Replace("'", "''") + "'";
                                if (dr["ColumnType"].ToString() == "listbox")
                                {
                                    TextSearch = TextSearch + " AND CHARINDEX('," + ddlSearch.SelectedValue.Trim().Replace("'", "''") + ",' ,',' + Record." + dr["SystemName"].ToString() + " + ',')>0";
                                }
                                else
                                {
                                    TextSearch = TextSearch + " AND Record." + dr["SystemName"].ToString() + " ='" + ddlSearch.SelectedValue.Trim().Replace("'", "''") + "'";
                                }

                            }

                        }
                    }
                }
                else
                {
                    TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SystemName"].ToString());
                    if (txtSearch != null)
                    {

                        string strViewFiltervalue = "";

                        string strTextSearch = "";
                        strActiveOperator = strLowerOperator;

                        if (strViewOperator == "" || strViewOperator == "between")
                            strViewOperator = "=";

                        if (txtSearch.Text != "")
                        {
                            strActiveOperator = "=";
                            strLowerOperator = "=";
                            if (strLowerOperator == "=")
                            {
                                strTextSearch = " Record." + dr["SystemName"].ToString() + " LIKE'%" + txtSearch.Text.Trim().Replace("'", "''") + "%'";
                            }
                        }

                        if (xmlDoc != null)
                        {
                            strViewFiltervalue = GetValueFromViewFilter(xmlDoc, dr["ColumnID"].ToString());
                            if (strViewFiltervalue != null)
                            {
                                string strActiveValue = strViewFiltervalue;

                                if (strTextSearch != "")
                                {
                                    strActiveValue = txtSearch.Text;
                                    bFromView = false;
                                }
                                else
                                {
                                    strActiveOperator = strViewOperator;
                                }

                                UpdateViewFilterControlsInfo(ref xmlDoc, dr["ColumnID"].ToString(), strActiveValue, strActiveOperator, bFromView);
                            }

                        }

                        if (strViewFiltervalue == null)
                        {
                            if (strTextSearch != "")
                                TextSearch = TextSearch + " AND " + strTextSearch;
                        }
                    }

                }



            }

            //

            foreach (DataRow dr in _dtSearchGroup.Rows)
            {

                TextBox txtSearch = (TextBox)tblSearchControls.FindControl("txtSearch_" + dr["SearchGroupID"].ToString());
                if (txtSearch != null)
                {
                    if (txtSearch.Text != "")
                    {

                        DataTable dtSearchGroupClumn = Common.DataTableFromText(" SELECT * FROM SearchGroupColumn WHERE SearchGroupID=" + dr["SearchGroupID"].ToString());
                        foreach (DataRow drC in dtSearchGroupClumn.Rows)
                        {
                            PopulateSearchGroup(int.Parse(drC["ColumnID"].ToString()), ref txtSearch);
                        }
                    }
                }
            }
        }



        //lets check 4 search control

        if (_theView != null)
        {
            if (chkShowAdvancedOptions.Checked == false)
            {
                if (xmlDoc != null)
                {
                    Pages_UserControl_ViewDetail vdFilter = new Pages_UserControl_ViewDetail();
                    vdFilter = (Pages_UserControl_ViewDetail)LoadControl("~/Pages/UserControl/ViewDetail.ascx");
                    vdFilter.PopulateFilterControl(xmlDoc.OuterXml, ((int)_theView.TableID));

                    TextSearch = TextSearch + " " + vdFilter.GetViewFilter();
                    vdFilter = null;
                }
                else
                {
                    TextSearch = TextSearch + " " + _theView.Filter;
                }
            }
            else
            {
                TextSearch = TextSearch + " " + _theView.Filter;
            }

        }

        if (chkShowAdvancedOptions.Checked)
        {
            
          
            cbcSearchMain.GraphSeriesColumnID = _theTable.GraphSeriesColumnID;
            cbcSearchMain.GraphXAxisColumnID = _theTable.GraphXAxisColumnID;

            string strTSA = "";
            string strTSB = "";
            string strTSC = "";
            string strTSD = "";

            string strAO1 = "";
            string strAO2 = "";
            string strAO3 = "";

            string strDDLMultipleSelectMain = "";
            string strDDLMultipleSelect1 = "";
            string strDDLMultipleSelect2 = "";
            string strDDLMultipleSelect3 = "";

            string stringTotalTS = " AND ((([AAAAAAA] [AO1] [BBBBBBB]) [AO2] [CCCCCCC]) [AO3] [DDDDDDD])";
            string strOneOne = " 1=1 ";
                      
                    if (cbcSearchMain.ddlYAxisV != "")
                        strDDLMultipleSelectMain = GetMultipleSelectSearch(hfControlB_DDL, cbcSearchMain);

                    if (strDDLMultipleSelectMain != "")
                    {
                        strTSA = "(" + strDDLMultipleSelectMain + ")";

                        ProcessDateSearchToArchive(int.Parse(cbcSearchMain.ddlYAxisV), cbcSearchMain.txtLowerDateV.ToString());
                    }

                    string strAndOr1 = hfAndOr1.Value;
                    string strAndOr2 = hfAndOr2.Value;
                    string strAndOr3 = hfAndOr3.Value;

                    if (strAndOr1 != "")
                ddlJoinOperator1.Text = strAndOr1;

            if (strAndOr2 != "")
                ddlJoinOperator2.Text = strAndOr2;

            if (strAndOr3 != "")
                ddlJoinOperator3.Text = strAndOr3;

            if (cbcSearch1.ddlYAxisV != "")
                        strDDLMultipleSelect1 = GetMultipleSelectSearch(hfDDLcbcSearch1, cbcSearch1);

                    if (strDDLMultipleSelect1 != "" && hfAndOr1.Value != "")
                    {
                        if (strTSA == "")
                        {
                            strTSA = "(" + strDDLMultipleSelect1 + ")";
                        }
                        else
                        {
                            strTSB = "(" + strDDLMultipleSelect1 + ")";
                        }

                        ProcessDateSearchToArchive(int.Parse(cbcSearch1.ddlYAxisV), cbcSearch1.txtLowerDateV.ToString());
                //Red 3715
                strAO1 = hfAndOr1.Value;
            }

                    if (cbcSearch2.ddlYAxisV != "")
                        strDDLMultipleSelect2 = GetMultipleSelectSearch(hfDDLcbcSearch2, cbcSearch2);

                    if (strDDLMultipleSelect2 != "" && hfAndOr2.Value != "")
                    {

                        if (strTSA == "")
                        {
                            strTSA = "(" + strDDLMultipleSelect2 + ")";
                        }
                        else if (strTSB == "")
                        {
                            strTSB = "(" + strDDLMultipleSelect2 + ")";
                        }
                        else
                        {
                            strTSC = "(" + strDDLMultipleSelect2 + ")";
                        }

                        ProcessDateSearchToArchive(int.Parse(cbcSearch2.ddlYAxisV), cbcSearch2.txtLowerDateV.ToString());
                //Red 3715
                if (strAO1 == "")
                {
                    strAO1 = hfAndOr2.Value;
                }
                else
                {
                    strAO2 = hfAndOr2.Value;
                }
            }

                    if (cbcSearch3.ddlYAxisV != "")
                        strDDLMultipleSelect3 = GetMultipleSelectSearch(hfDDLcbcSearch3, cbcSearch3);

                    if (strDDLMultipleSelect3 != "" && hfAndOr3.Value != "")
                    {
                        if (strTSA == "")
                        {
                            strTSA = "(" + strDDLMultipleSelect3 + ")";
                        }
                        else if (strTSB == "")
                        {
                            strTSB = "(" + strDDLMultipleSelect3 + ")";
                        }
                        else if (strTSC == "")
                        {
                            strTSC = "(" + strDDLMultipleSelect3 + ")";
                        }
                        else
                        {
                            strTSD = "(" + strDDLMultipleSelect3 + ")";
                        }

                        ProcessDateSearchToArchive(int.Parse(cbcSearch3.ddlYAxisV), cbcSearch3.txtLowerDateV.ToString());
                //Red 3715
                if (strAO1 == "")
                {
                    strAO1 = hfAndOr3.Value;
                }
                else if (strAO2 == "")
                {
                    strAO2 = hfAndOr3.Value;
                }
                else
                {
                    strAO3 = hfAndOr3.Value;
                }
            }


            AdvancedMultipleSelect();

            if (strTSA == "")
                strTSA = strOneOne;
            if (strTSB == "")
                strTSB = strOneOne;
            if (strTSC == "")
                strTSC = strOneOne;
            if (strTSD == "")
                strTSD = strOneOne;


            if (strAO1 == "")
                strAO1 = " AND ";

            if (strAO2 == "")
                strAO2 = " AND ";

            if (strAO3 == "")
                strAO3 = " AND ";

            stringTotalTS = stringTotalTS.Replace("[AO1]", strAO1);
            stringTotalTS = stringTotalTS.Replace("[AO2]", strAO2);
            stringTotalTS = stringTotalTS.Replace("[AO3]", strAO3);

            stringTotalTS = stringTotalTS.Replace("[AAAAAAA]", strTSA);
            stringTotalTS = stringTotalTS.Replace("[BBBBBBB]", strTSB);
            stringTotalTS = stringTotalTS.Replace("[CCCCCCC]", strTSC);
            stringTotalTS = stringTotalTS.Replace("[DDDDDDD]", strTSD);

            TextSearch = TextSearch + stringTotalTS;

        }

        TextSearch = TextSearch + hfTextSearch.Value;

        if ((bool)_theUserRole.IsAdvancedSecurity)
        {
            if (_strRecordRightID == Common.UserRoleType.OwnData)
            {
                //TextSearch = TextSearch + " AND Record.OwnerUserID=" + _ObjUser.UserID.ToString();
                TextSearch = TextSearch + " AND (Record.OwnerUserID=" + _ObjUser.UserID.ToString() + " OR Record.EnteredBy=" + _ObjUser.UserID.ToString() + ")";
            }

        }
        else
        {
            if (Session["roletype"].ToString() == Common.UserRoleType.OwnData)
            {
                //TextSearch = TextSearch + " AND Record.OwnerUserID=" + _ObjUser.UserID.ToString();
                TextSearch = TextSearch + " AND (Record.OwnerUserID=" + _ObjUser.UserID.ToString() + " OR Record.EnteredBy=" + _ObjUser.UserID.ToString() + ")";
            }


        }

        //lets play with data scope

        if (_theAccount.UseDataScope != null)
        {
            if ((bool)_theAccount.UseDataScope)
            {
                if (_theUserRole.DataScopeColumnID != null && _theUserRole.DataScopeValue != "")
                {
                    //this user need Data Scope

                    Column theDataScopeColumn = RecordManager.ets_Column_Details((int)_theUserRole.DataScopeColumnID);

                    if (theDataScopeColumn != null)
                    {

                        TextSearch = TextSearch + GetDataScopeWhere((int)theDataScopeColumn.TableID, (int)theDataScopeColumn.TableID, theDataScopeColumn.SystemName);

                    }

                }
            }
        }

        if (TextSearchParent == null)
            TextSearchParent = "";

        //ddlActiveFilter.SelectedValue == "-1" ? null : (bool?)bool.Parse(ddlActiveFilter.SelectedValue)


        PopulateDateAddedSearch();



        string strSummaryTableFilterText = SystemData.SystemOption_ValueByKey_Account("SummaryTableFilterText", null, int.Parse(TableID.ToString()));

        if (strSummaryTableFilterText != "")
            TextSearch = TextSearch + " " + strSummaryTableFilterText;


        //if (Request.QueryString["viewname"] == null)
        //{
        //    TextSearch = TextSearch + " " + SystemData.SystemOption_ValueByKey_Account("NoViewName", null, int.Parse(TableID.ToString()));
        //}
        //else
        //{
        //    TextSearch = TextSearch + " " + SystemData.SystemOption_ValueByKey_Account(Request.QueryString["viewname"].ToString(), null, int.Parse(TableID.ToString()));

        //}
        //RP Modified Ticket 4363
        //if (chkShowAdvancedOptions.Checked && ddlUploadedBatch.SelectedValue != "")
        if (chkShowAdvancedOptions.Checked && hiddenUploadedBatchID.Value != "")
        {
            //TextSearch = TextSearch + "  AND Record.BatchID=" + ddlUploadedBatch.SelectedValue;
            TextSearch = TextSearch + "  AND Record.BatchID=" + hiddenUploadedBatchID.Value;
        }

        if (TextSearch.Trim() == "")
            TextSearch = "";







    }

    //Red Ticket 2501
    protected string GetMultipleSelectSearch(HiddenField hfCBCX, Pages_UserControl_ControlByColumn CBCSearch)
    {
        string strDDLSearchMultiple = "";

        if (hfCBCX.Value != "")
        {
            if (hfCBCX.Value != "vf_vf_vf")
            {
                string strWhereOperator = "";

                if (CBCSearch.CompareOperator == "=" || CBCSearch.CompareOperator == "<>")
                {
                    if (CBCSearch.CompareOperator == "=")
                        strWhereOperator = " IN ";

                    if (CBCSearch.CompareOperator == "<>")
                        strWhereOperator = " NOT IN ";

                    //lets get systemname and type                                          
                    string strSytemName = Common.GetValueFromSQL("SELECT SystemName  FROM [Column] WHERE ColumnID=" + CBCSearch.ddlYAxisV);
                    string strColumnType = Common.GetValueFromSQL("SELECT ColumnType  FROM [Column] WHERE ColumnID=" + CBCSearch.ddlYAxisV);

                    if (hfCBCX.Value.IndexOf(",") > -1)
                    {
                        string strTemp = "'" + hfCBCX.Value + "'";
                        strTemp = strTemp.Replace(",", "','");

                        strDDLSearchMultiple = " Record." + strSytemName.ToString() + strWhereOperator + "  (" + strTemp + ")";
                        if (ViewState["seriesColumnID"] == null)
                        {
                            ViewState["seriesColumnID"] = CBCSearch.ddlYAxisV;
                            Session["RecordListSeries"] = strTemp.Trim().Replace("'","");
                        }
                    }
                    else
                    {
                        if (strColumnType == "text")
                        {
                            strDDLSearchMultiple = " Record." + strSytemName.ToString() + " " + " LIKE'%" + hfCBCX.Value.Trim().Replace("'", "''") + "%'";
                        }
                        else
                        {
                            strDDLSearchMultiple = " Record." + strSytemName.ToString() + " " + CBCSearch.CompareOperator + " '" + hfCBCX.Value.Trim().Replace("'", "''") + "'";
                            if (ViewState["seriesColumnID"] == null)
                            {
                                ViewState["seriesColumnID"] = CBCSearch.ddlYAxisV;
                                Session["RecordListSeries"] = "allSeries";
                            }
                        }
                    }
                }
            }
        }
        else
        {
            DataTable _dtColumns = new DataTable();
            
            //lets get what we need
            if (CBCSearch.ddlYAxisV != "")
                _dtColumns = Common.DataTableFromText("SELECT ColumnID, ColumnType, DropDownType, DropDownType, TableTableID, DisplayColumn FROM [Column] WHERE ColumnID=" + int.Parse(CBCSearch.ddlYAxisV.ToString()));

            foreach (DataRow dr in _dtColumns.Rows)
            {

                if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "table" || dr["DropDownType"].ToString() == "tabledd") &&
                        dr["TableTableID"] != DBNull.Value && dr["DisplayColumn"].ToString() != "")
                {
                    if (CBCSearch.CompareOperator == "empty" || CBCSearch.CompareOperator == "notempty")
                    {
                        strDDLSearchMultiple = CBCSearch.TextSearch;
                        /* === Red: Charting, get columnID for the series as the default onload === */
                        if (ViewState["seriesColumnID"] == null)
                        {
                            ViewState["seriesColumnID"] = dr["ColumnID"].ToString();
                            Session["RecordListSeries"] = "allSeries";
                        }
                    }
                    else
                        strDDLSearchMultiple = "";

                }
                else if (dr["ColumnType"].ToString() == "radiobutton" || dr["ColumnType"].ToString() == "checkbox" ||
               dr["ColumnType"].ToString() == "listbox")
                {
                    if (CBCSearch.CompareOperator == "empty" || CBCSearch.CompareOperator == "notempty")
                        strDDLSearchMultiple = CBCSearch.TextSearch;
                    else
                        strDDLSearchMultiple = "";

                }
                else if (dr["ColumnType"].ToString() == "dropdown" && (dr["DropDownType"].ToString() == "values"
               || dr["DropDownType"].ToString() == "value_text"))
                {
                    if (CBCSearch.CompareOperator == "empty" || CBCSearch.CompareOperator == "notempty")
                    {
                        strDDLSearchMultiple = CBCSearch.TextSearch;
                        /* === Red: Charting, get columnID for the series as the default onload === */
                        if (ViewState["seriesColumnID"] == null)
                        {
                            ViewState["seriesColumnID"] = dr["ColumnID"].ToString();
                            Session["RecordListSeries"] = "allSeries";
                        }
                    }
                    else
                        strDDLSearchMultiple = "";
                }
                else
                {
                    strDDLSearchMultiple = CBCSearch.TextSearch;
                }
            }
            _dtColumns.Clear();
            _dtColumns.Dispose();
        }
        return strDDLSearchMultiple;
    }
    
    //Red Ticket 2501
    protected void ProcessStandardToAdvancedSearch(int ColID, string strLower, string strUpper, string strSingle, bool IsDate,  string strCompare)
    {     
    
        if (ViewState["iTN"] != null)
            Session["iTN"] = ViewState["iTN"];


        if (cbcSearchMain.ddlYAxisV != "")
        {
            if (cbcSearch1.ddlYAxisV != "")
            {
                if (cbcSearch2.ddlYAxisV != "")
                {
                    cbcSearch3.ddlYAxisV = ColID.ToString();
                    
                    if (!IsDate) /* it's a numeric */
                    {
                        cbcSearch3.txtLowerLimitV = strLower;
                        cbcSearch3.txtUpperLimitV = strUpper;
                    }
                    else /* it's a date */
                    {
                        cbcSearch3.txtLowerDateV = strLower;
                        cbcSearch3.txtUpperDateV = strUpper;
                    }
                                       
                    cbcSearch3.txtSearchTextV = strSingle;
                    hfDDLcbcSearch3.Value = strSingle;

                    cbcSearch3.CompareOperator = strCompare;

                }
                else
                {
                    cbcSearch2.ddlYAxisV = ColID.ToString();
                    if (!IsDate) /* it's a numeric */
                    {
                        cbcSearch2.txtLowerLimitV = strLower;
                        cbcSearch2.txtUpperLimitV = strUpper;
                    }
                    else /* it's a date */
                    {
                        cbcSearch2.txtLowerDateV = strLower;
                        cbcSearch2.txtUpperDateV = strUpper;
                    }                      
                   
                    cbcSearch2.txtSearchTextV = strSingle;
                        hfDDLcbcSearch2.Value = strSingle;

                    cbcSearch2.CompareOperator = strCompare;
                    hfAndOr3.Value = "and";

                    string strJSSearchShowHide = "$('#" + lnkAddSearch3.ClientID + "').click();";
                    ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "PutDefaultSearcUI_CS3", strJSSearchShowHide, true);

                }
            }
            else
            {
                cbcSearch1.ddlYAxisV = ColID.ToString();
                if (!IsDate) /* it's a numeric */
                {
                    cbcSearch1.txtLowerLimitV = strLower;
                    cbcSearch1.txtUpperLimitV = strUpper;

                } /* it's a date */
                else
                {
                    cbcSearch1.txtLowerDateV = strLower;
                    cbcSearch1.txtUpperDateV = strUpper;

                }
                                                 
                cbcSearch1.txtSearchTextV = strSingle;
                    hfDDLcbcSearch1.Value = strSingle;

                cbcSearch1.CompareOperator = strCompare;
                hfAndOr2.Value = "and";
                string strJSSearchShowHide = "$('#" + lnkAddSearch2.ClientID + "').click();";
                ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "PutDefaultSearcUI_CS2", strJSSearchShowHide, true);

            }

        }
        else 
        {
            cbcSearchMain.ddlYAxisV = ColID.ToString();
            if (!IsDate) /* it's a numeric */
            {
                cbcSearchMain.txtLowerLimitV = strLower;
                cbcSearchMain.txtUpperLimitV = strUpper;

            }
            else /* it's a date */
            {
                cbcSearchMain.txtLowerDateV = strLower;
                cbcSearchMain.txtUpperDateV = strUpper;

            }
                                            
            cbcSearchMain.txtSearchTextV = strSingle;
                hfControlB_DDL.Value = strSingle;

            cbcSearchMain.CompareOperator = strCompare;
            hfAndOr1.Value = "and";

            string strJSSearchShowHide = "$('#" + lnkAddSearch1.ClientID + "').click();";
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "PutDefaultSearcUI_CS1", strJSSearchShowHide, true);           
        }

        Session["iTN"] = null;
    }
    

    protected void ProcessDateSearchToArchive(int ColID, string strLowerDateV)
    {
        if (strLowerDateV != "")
        {
            string strIsDate = Common.GetValueFromSQL("SELECT ColumnType FROM [Column] WHERE [ColumnID]=" + ColID);

            if (strIsDate == "date" || strIsDate == "datetime")
            {
                DateTime dateValue;
                if (DateTime.TryParseExact(strLowerDateV.Trim(), Common.Dateformats,
                              new CultureInfo("en-GB"),
                              DateTimeStyles.None,
                              out dateValue))
                {

                    if (Session["DateSearchFrom"] != null)
                    {
                        if (DateTime.Parse(Session["DateSearchFrom"].ToString()) > dateValue)
                        {
                            Session["DateSearchFrom"] = dateValue.ToShortDateString();
                        }
                    }
                    else
                    {
                        Session["DateSearchFrom"] = dateValue.ToShortDateString();
                    }
                }
            }
        }
    }
    //end Red
    protected void UpdateViewMultipleDDLFilterControlsInfo(ref System.Xml.XmlDocument xmlDoc, string strColumnID, string strValue, string strOperator, bool bFromView, int? cbcSearch, ref bool bExist)
    {
        if (xmlDoc == null || xmlDoc.FirstChild == null)
            return;

        try
        {


            if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"] != null &&
                    xmlDoc.FirstChild["cbcSearchMain_TextValue"] != null && xmlDoc.FirstChild["cbcSearchMain_CompareOperator"] != null &&
                    cbcSearch == 1)
            {
                if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"].InnerText == strColumnID)
                {
                    bExist = true;
                    xmlDoc.FirstChild["cbcSearchMain_TextValue"].InnerText = strValue;
                    if (strOperator != "")
                        xmlDoc.FirstChild["cbcSearchMain_CompareOperator"].InnerText = strOperator;

                    if (bFromView)
                    {
                        return;
                    }

                }
            }
            if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch1_TextValue"] != null &&
                   cbcSearch == 2)
            {
               

                if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"].InnerText == strColumnID)
                {
                    bExist = true;
                    xmlDoc.FirstChild["cbcSearch1_TextValue"].InnerText = strValue;
                    if (strOperator != "")
                        xmlDoc.FirstChild["cbcSearch1_CompareOperator"].InnerText = strOperator;
                }
            }

            if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch2_TextValue"] != null &&
                   cbcSearch == 3)
            {
               

                if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"].InnerText == strColumnID)
                {
                    bExist = true;
                    xmlDoc.FirstChild["cbcSearch2_TextValue"].InnerText = strValue;
                    if (strOperator != "")
                        xmlDoc.FirstChild["cbcSearch2_CompareOperator"].InnerText = strOperator;
                }
            }


            if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch3_TextValue"] != null &&
                   cbcSearch == 4)
            {
               
                if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"].InnerText == strColumnID)
                {
                    bExist = true;
                    xmlDoc.FirstChild["cbcSearch3_TextValue"].InnerText = strValue;
                    if (strOperator != "")
                        xmlDoc.FirstChild["cbcSearch3_CompareOperator"].InnerText = strOperator;
                }
            }

        }
        catch
        {
            // Red
        }

        return;
    }



    protected void UpdateViewFilterControlsInfo(ref System.Xml.XmlDocument xmlDoc, string strColumnID, string strValue, string strOperator, bool bFromView)
    {
        if (xmlDoc == null || xmlDoc.FirstChild == null)
            return;


        try
        {


            if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"] != null &&
                    xmlDoc.FirstChild["cbcSearchMain_TextValue"] != null && xmlDoc.FirstChild["cbcSearchMain_CompareOperator"] != null)
            {
                if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"].InnerText == strColumnID)
                {
                    xmlDoc.FirstChild["cbcSearchMain_TextValue"].InnerText = strValue;
                    if (strOperator != "")
                        xmlDoc.FirstChild["cbcSearchMain_CompareOperator"].InnerText = strOperator;

                    if (bFromView)
                    {
                        return;
                    }

                }
            }
            if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch1_TextValue"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"].InnerText == strColumnID)
                {
                    xmlDoc.FirstChild["cbcSearch1_TextValue"].InnerText = strValue;
                    if (strOperator != "")
                        xmlDoc.FirstChild["cbcSearch1_CompareOperator"].InnerText = strOperator;
                    if (bFromView)
                    {
                        return;
                    }
                    else
                    {
                        if (xmlDoc.FirstChild["hfAndOr1"] != null)
                            xmlDoc.FirstChild["hfAndOr1"].InnerText = "and";
                    }
                }
            }

            if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch2_TextValue"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"].InnerText == strColumnID)
                {
                    xmlDoc.FirstChild["cbcSearch2_TextValue"].InnerText = strValue;
                    if (strOperator != "")
                        xmlDoc.FirstChild["cbcSearch2_CompareOperator"].InnerText = strOperator;
                    if (bFromView)
                    {
                        return;
                    }
                    else
                    {
                        if (xmlDoc.FirstChild["hfAndOr2"] != null)
                            xmlDoc.FirstChild["hfAndOr2"].InnerText = "and";
                    }
                }
            }


            if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch3_TextValue"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"].InnerText == strColumnID)
                {
                    xmlDoc.FirstChild["cbcSearch3_TextValue"].InnerText = strValue;
                    if (strOperator != "")
                        xmlDoc.FirstChild["cbcSearch3_CompareOperator"].InnerText = strOperator;
                    if (bFromView)
                    {
                        return;
                    }
                    else
                    {
                        if (xmlDoc.FirstChild["hfAndOr3"] != null)
                            xmlDoc.FirstChild["hfAndOr3"].InnerText = "and";
                    }
                }
            }

        }
        catch
        {
            //
        }

        return;
    }


    protected void RemoveViewFilterControlsInfo(ref System.Xml.XmlDocument xmlDoc, string strColumnID)
    {
        if (xmlDoc == null || xmlDoc.FirstChild == null)
            return;
        try
        {
            if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"] != null &&
                    xmlDoc.FirstChild["cbcSearchMain_TextValue"] != null && xmlDoc.FirstChild["cbcSearchMain_CompareOperator"] != null)
            {
                if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"].InnerText == strColumnID)
                {
                    xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"].InnerText = "";
                    xmlDoc.FirstChild["cbcSearchMain_TextValue"].InnerText = "";
                    xmlDoc.FirstChild["cbcSearchMain_CompareOperator"].InnerText = "";
                }
            }
            if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch1_TextValue"] != null)
            {
                if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"].InnerText == strColumnID)
                {
                    xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"].InnerText = "";
                    xmlDoc.FirstChild["cbcSearch1_TextValue"].InnerText = "";
                    xmlDoc.FirstChild["cbcSearch1_CompareOperator"].InnerText = "";
                    if (xmlDoc.FirstChild["hfAndOr1"] != null)
                        xmlDoc.FirstChild["hfAndOr1"].InnerText = "and";
                }
            }

            if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch2_TextValue"] != null)
            {
                if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"].InnerText == strColumnID)
                {
                    xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"].InnerText = "";
                    xmlDoc.FirstChild["cbcSearch2_TextValue"].InnerText = "";
                    xmlDoc.FirstChild["cbcSearch2_CompareOperator"].InnerText = "";
                    if (xmlDoc.FirstChild["hfAndOr2"] != null)
                        xmlDoc.FirstChild["hfAndOr2"].InnerText = "and";
                }
            }


            if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch3_TextValue"] != null)
            {
                if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"].InnerText == strColumnID)
                {
                    xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"].InnerText = "";
                    xmlDoc.FirstChild["cbcSearch3_TextValue"].InnerText = "";
                    xmlDoc.FirstChild["cbcSearch3_CompareOperator"].InnerText = "";
                    if (xmlDoc.FirstChild["hfAndOr3"] != null)
                        xmlDoc.FirstChild["hfAndOr3"].InnerText = "and";
                }
            }

        }
        catch
        {
            //
        }

        return;
    }



    //protected void  UpdateViewFilterControlsInfo(ref System.Xml.XmlDocument xmlDoc, string strColumnID,string strValue)
    //{
    //    try
    //    {
    //        if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"] != null &&
    //                xmlDoc.FirstChild["cbcSearchMain_TextValue"] != null)
    //        {               
    //            if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"].InnerText == strColumnID)
    //            {
    //                xmlDoc.FirstChild["cbcSearchMain_TextValue"].InnerText = strValue;
    //                return;
    //            }
    //        }
    //        if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"] != null &&
    //               xmlDoc.FirstChild["cbcSearch1_TextValue"] != null)
    //        {

    //            if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"].InnerText == strColumnID)
    //            {
    //                xmlDoc.FirstChild["cbcSearch1_TextValue"].InnerText=strValue;
    //                return;
    //            }
    //        }

    //        if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"] != null &&
    //               xmlDoc.FirstChild["cbcSearch2_TextValue"] != null)
    //        {

    //            if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"].InnerText == strColumnID)
    //            {
    //                 xmlDoc.FirstChild["cbcSearch2_TextValue"].InnerText=strValue;
    //                 return;
    //            }
    //        }


    //        if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"] != null &&
    //               xmlDoc.FirstChild["cbcSearch3_TextValue"] != null)
    //        {

    //            if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"].InnerText == strColumnID)
    //            {
    //                xmlDoc.FirstChild["cbcSearch3_TextValue"].InnerText=strValue;
    //                return;
    //            }
    //        }

    //    }
    //    catch
    //    {
    //        //
    //    }

    //    return;
    //}


    protected string GetDDLMultipleValueFromViewFilter(System.Xml.XmlDocument xmlDoc, string strColumnID)
    {
        try
        {
            if (xmlDoc.OuterXml == "")
                return "";

            string multipleDDLValue = "";
            if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"] != null &&
                    xmlDoc.FirstChild["cbcSearchMain_TextValue"] != null &&
                    xmlDoc.FirstChild["cbcSearchMain_CompareOperator"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"].InnerText == strColumnID &&
                    xmlDoc.FirstChild["cbcSearchMain_CompareOperator"].InnerText == "=")
                {
                    multipleDDLValue = xmlDoc.FirstChild["cbcSearchMain_TextValue"].InnerText;
                }
            }
            if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch1_TextValue"] != null &&
                    xmlDoc.FirstChild["cbcSearch1_CompareOperator"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"].InnerText == strColumnID &&
                    xmlDoc.FirstChild["cbcSearch1_CompareOperator"].InnerText == "=")
                {
                    if (multipleDDLValue != "")
                    {
                        multipleDDLValue = multipleDDLValue + ",";
                    }

                    multipleDDLValue = multipleDDLValue + xmlDoc.FirstChild["cbcSearch1_TextValue"].InnerText;
                }
            }

            if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch2_TextValue"] != null &&
                    xmlDoc.FirstChild["cbcSearch2_CompareOperator"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"].InnerText == strColumnID &&
                     xmlDoc.FirstChild["cbcSearch2_CompareOperator"].InnerText == "=")
                {
                    if (multipleDDLValue != "")
                    {
                        multipleDDLValue = multipleDDLValue + ",";
                    }

                    multipleDDLValue = multipleDDLValue + xmlDoc.FirstChild["cbcSearch2_TextValue"].InnerText;
                }
            }


            if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch3_TextValue"] != null &&
                    xmlDoc.FirstChild["cbcSearch3_CompareOperator"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"].InnerText == strColumnID &&
                     xmlDoc.FirstChild["cbcSearch3_CompareOperator"].InnerText == "=")
                {
                    if (multipleDDLValue != "")
                    {
                        multipleDDLValue = multipleDDLValue + ",";
                    }

                    multipleDDLValue = multipleDDLValue + xmlDoc.FirstChild["cbcSearch3_TextValue"].InnerText;
                }
            }

            return multipleDDLValue;
        }
        catch
        {
            //
        }

        return "";

    }





    protected string GetValueFromViewFilter(System.Xml.XmlDocument xmlDoc, string strColumnID)
    {
        try
        {
            if (xmlDoc.OuterXml == "")
                return null;

            if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"] != null &&
                    xmlDoc.FirstChild["cbcSearchMain_TextValue"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"].InnerText == strColumnID)
                {
                    return xmlDoc.FirstChild["cbcSearchMain_TextValue"].InnerText;
                }
            }
            if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch1_TextValue"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"].InnerText == strColumnID)
                {
                    return xmlDoc.FirstChild["cbcSearch1_TextValue"].InnerText;
                }
            }

            if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch2_TextValue"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"].InnerText == strColumnID)
                {
                    return xmlDoc.FirstChild["cbcSearch2_TextValue"].InnerText;
                }
            }


            if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch3_TextValue"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"].InnerText == strColumnID)
                {
                    return xmlDoc.FirstChild["cbcSearch3_TextValue"].InnerText;
                }
            }

        }
        catch
        {
            //
        }

        return null;
    }



    protected string GetOneCondition(string strOneOperator, string strOneValue, string strColumnID, string strVT)
    {
        string strOneCondition = "";
        if (strOneValue.IndexOf("____") > -1)
        {
            if (strOneOperator == "between")
            {
                strOneValue = strOneValue.Replace("____", " to ");
            }
            else
            {
                strOneValue = strOneValue.Replace("____", "");
            }

        }

        if (strOneValue != "" && strVT != "")
        {
            if (strVT == "ColumnID" && strColumnID != "")
            {
                try
                {
                    Column theColumn = RecordManager.ets_Column_Details(int.Parse(strColumnID));
                    if (theColumn != null && theColumn.TableTableID != null && theColumn.DisplayColumn != "" && theColumn.LinkedParentColumnID != null
                        && (int)theColumn.TableTableID > -1)
                    {
                        //Column theLinkedParentColumn = RecordManager.ets_Column_Details(((int)theColumn.LinkedParentColumnID));

                        // string strFieldsToShow = RecordManager.fnReplaceDisplayColumns(theColumn.DisplayColumn, (int)theColumn.TableTableID, theColumn.ColumnID);

                        //string strFieldsToShow = RecordManager.fnReplaceDisplayColumns_NoAlias((int)theColumn.ColumnID);

                        //if (theLinkedParentColumn != null && strFieldsToShow != "")
                        //{
                        //string strFilterSQL = "";

                        if (strOneValue == "-user-")
                        {
                            //need RnD
                            Column theLinkedParentColumn = RecordManager.ets_Column_Details(((int)theColumn.LinkedParentColumnID));
                            string strFieldsToShow = RecordManager.fnReplaceDisplayColumns_NoAlias((int)theColumn.ColumnID);

                            string strFilterSQL = "";
                            string strColumnuserID = Common.GetValueFromSQL(@"SELECT TOP 1 ColumnID FROM [Column] WHERE TableID=" + theColumn.TableTableID.ToString() + @" AND 
                                ColumnType='dropdown' AND DisplayColumn IS NOT NULL
                                            AND TableTableID=-1");

                            if (strColumnuserID != "")
                            {
                                string strLoginText = "";
                                SecurityManager.ProcessLoginUserDefault(theColumn.TableTableID.ToString(), "",
                                theColumn.LinkedParentColumnID.ToString(), _ObjUser.UserID.ToString(), ref strOneValue, ref strLoginText);

                            }

                            if (theLinkedParentColumn.SystemName.ToLower() == "recordid")
                            {
                                strFilterSQL = strOneValue;
                            }
                            else
                            {
                                strFilterSQL = "'" + strOneValue.Replace("'", "''") + "'";
                            }

                            string strDisplayColumnText = Common.GetValueFromSQL("SELECT (" + strFieldsToShow + ") as DisplayColumnText FROM Record WHERE TableID=" + theColumn.TableTableID.ToString() + " AND " + theLinkedParentColumn.SystemName + "=" + strFilterSQL);

                            if (strDisplayColumnText != "")
                            {
                                strOneValue = strDisplayColumnText;
                            }
                        }
                        else
                        {
                            strOneValue = Common.GetLinkedDisplayText(theColumn.DisplayColumn, (int)theColumn.TableTableID, null, " AND Record.RecordID=" + strOneValue, "");
                        }

                        //if (theLinkedParentColumn.SystemName.ToLower() == "recordid")
                        //{
                        //    strFilterSQL = strOneValue;
                        //}
                        //else
                        //{
                        //    strFilterSQL = "'" + strOneValue.Replace("'", "''") + "'";
                        //}

                        //string strDisplayColumnText = Common.GetValueFromSQL("SELECT (" + strFieldsToShow + ") as DisplayColumnText FROM Record WHERE TableID=" + theColumn.TableTableID.ToString() + " AND " + theLinkedParentColumn.SystemName + "=" + strFilterSQL);

                        //if (strDisplayColumnText != "")
                        //{
                        //    strOneValue = strDisplayColumnText;
                        //}

                        //}
                    }
                    else if (theColumn != null && theColumn.TableTableID != null && theColumn.DisplayColumn != "" && (int)theColumn.TableTableID == -1)
                    {
                        if (strOneValue != "")
                            strOneValue = RecordManager.fnGetSystemUserDisplayText(theColumn.DisplayColumn, strOneValue);

                    }

                }
                catch
                {
                    //
                }


            }
            else
            {
                strOneValue = Common.GetTextFromValue(strVT, strOneValue);
            }
        }

        if (strOneOperator == "between")
        {
            strOneCondition = strOneValue;
        }
        else if (strOneOperator == "empty")
        {
            strOneCondition = "Empty";
        }
        else if (strOneOperator == "notempty")
        {
            strOneCondition = "Not Empty";
        }
        else if (strOneOperator == "<>")
        {
            strOneCondition = "Not Equal " + strOneValue;
        }
        else if (strOneOperator == "=")
        {
            strOneCondition = strOneValue;
        }
        else
        {
            strOneCondition = strOneOperator + " " + strOneValue;
        }
        return strOneCondition;
    }

    protected bool ThisIsOR(string strWaterViewmark)
    {
        if (strWaterViewmark.Trim() == "")
        {
            return false;
        }
        else
        {
            if (strWaterViewmark.Length > 2)
            {
                if (strWaterViewmark.Substring(0, 2) == "OR")
                {
                    return true;
                }
            }
        }
        return false;
    }
    protected string GetMultipleCondition(System.Xml.XmlDocument xmlDoc, string strColumnID, ref int iConditionCount, string strVT)
    {
        iConditionCount = 0;
        string strConditions = "";
        string strJoinOperator = "";
        try
        {
            if (xmlDoc.OuterXml == "")
                return "";


            string strEachCondition = "";
            //string strOneOperator = "=";
            //string strOneValue = "";
            if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"] != null &&
                    xmlDoc.FirstChild["cbcSearchMain_TextValue"] != null && xmlDoc.FirstChild["cbcSearchMain_CompareOperator"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"].InnerText == strColumnID)
                {
                    iConditionCount = iConditionCount + 1;
                    strEachCondition = GetOneCondition(xmlDoc.FirstChild["cbcSearchMain_CompareOperator"].InnerText,
                        xmlDoc.FirstChild["cbcSearchMain_TextValue"].InnerText, strColumnID, strVT);
                    if (strEachCondition != "")
                        strConditions = "(" + strEachCondition + ")";


                }
            }
            if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch1_TextValue"] != null && xmlDoc.FirstChild["cbcSearch1_CompareOperator"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"].InnerText == strColumnID)
                {
                    iConditionCount = iConditionCount + 1;
                    strEachCondition = GetOneCondition(xmlDoc.FirstChild["cbcSearch1_CompareOperator"].InnerText,
                      xmlDoc.FirstChild["cbcSearch1_TextValue"].InnerText, strColumnID, strVT);
                    if (strEachCondition != "")
                    {
                        if (strConditions != "")
                        {
                            if (xmlDoc.FirstChild["hfAndOr1"] != null)
                            {

                                strConditions = strConditions + " " + xmlDoc.FirstChild["hfAndOr1"].InnerText + " ";
                            }
                        }
                        else
                        {
                            if (xmlDoc.FirstChild["hfAndOr1"] != null)
                                strJoinOperator = xmlDoc.FirstChild["hfAndOr1"].InnerText;
                        }

                        strConditions = strConditions + "(" + strEachCondition + ")";
                    }

                }
            }

            if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch2_TextValue"] != null && xmlDoc.FirstChild["cbcSearch2_CompareOperator"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"].InnerText == strColumnID)
                {
                    iConditionCount = iConditionCount + 1;
                    strEachCondition = GetOneCondition(xmlDoc.FirstChild["cbcSearch2_CompareOperator"].InnerText,
                        xmlDoc.FirstChild["cbcSearch2_TextValue"].InnerText, strColumnID, strVT);
                    if (strEachCondition != "")
                    {
                        if (strConditions != "")
                        {
                            if (xmlDoc.FirstChild["hfAndOr2"] != null)
                            {
                                strConditions = strConditions + " " + xmlDoc.FirstChild["hfAndOr2"].InnerText + " ";
                            }
                        }
                        else
                        {
                            if (xmlDoc.FirstChild["hfAndOr2"] != null)
                                strJoinOperator = xmlDoc.FirstChild["hfAndOr2"].InnerText;
                        }

                        strConditions = strConditions + "(" + strEachCondition + ")";
                    }
                }
            }


            if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch3_TextValue"] != null && xmlDoc.FirstChild["cbcSearch3_CompareOperator"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"].InnerText == strColumnID)
                {
                    iConditionCount = iConditionCount + 1;
                    strEachCondition = GetOneCondition(xmlDoc.FirstChild["cbcSearch3_CompareOperator"].InnerText,
                        xmlDoc.FirstChild["cbcSearch3_TextValue"].InnerText, strColumnID, strVT);
                    if (strEachCondition != "")
                    {
                        if (strConditions != "")
                        {
                            if (xmlDoc.FirstChild["hfAndOr3"] != null)
                            {
                                strConditions = strConditions + " " + xmlDoc.FirstChild["hfAndOr3"].InnerText + " ";
                            }
                        }
                        else
                        {
                            if (xmlDoc.FirstChild["hfAndOr3"] != null)
                                strJoinOperator = xmlDoc.FirstChild["hfAndOr3"].InnerText;
                        }

                        strConditions = strConditions + "(" + strEachCondition + ")";
                    }
                }
            }

        }
        catch
        {
            //
        }

        if (strConditions != "" && iConditionCount == 1 && strJoinOperator.Trim() == "or")
        {
            strConditions = "OR " + strConditions;
        }

        return strConditions;
    }
    protected string GetOperatorFromViewFilter(System.Xml.XmlDocument xmlDoc, string strColumnID)
    {
        try
        {
            if (xmlDoc.OuterXml == "")
                return "";

            if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"] != null &&
                    xmlDoc.FirstChild["cbcSearchMain_CompareOperator"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearchMain_ddlYAxisV"].InnerText == strColumnID)
                {
                    return xmlDoc.FirstChild["cbcSearchMain_CompareOperator"].InnerText;
                }
            }
            if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch1_CompareOperator"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch1_ddlYAxisV"].InnerText == strColumnID)
                {
                    return xmlDoc.FirstChild["cbcSearch1_CompareOperator"].InnerText;
                }
            }

            if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch2_CompareOperator"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch2_ddlYAxisV"].InnerText == strColumnID)
                {
                    return xmlDoc.FirstChild["cbcSearch2_CompareOperator"].InnerText;
                }
            }


            if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"] != null &&
                   xmlDoc.FirstChild["cbcSearch3_CompareOperator"] != null)
            {

                if (xmlDoc.FirstChild["cbcSearch3_ddlYAxisV"].InnerText == strColumnID)
                {
                    return xmlDoc.FirstChild["cbcSearch3_CompareOperator"].InnerText;
                }
            }

        }
        catch
        {
            //
        }

        return "";
    }
    protected void ibEmail_Click(object sender, ImageClickEventArgs e)
    {
        ExportExcelorCSV(sender, e, "email");

    }
    //protected void ibEmail_Click_2(object sender, ImageClickEventArgs e)
    //{
    //    try
    //    {
    //        lblMsg.Text = "";
    //        //string strFolderPath = SystemData.SystemOption_ValueByKey("BulkExportPath");
    //        string strFolderPath = Server.MapPath("~\\ExportedFiles");
    //        //string strFileName = Guid.NewGuid().ToString() + "_" + lblTitle.Text.Replace(" ", "").ToString() + ".csv";
    //        string strFileName = Guid.NewGuid().ToString() + ".csv";
    //        string strFullFileName = strFolderPath + "\\" + strFileName;


    //        //gvTheGrid.AllowPaging = false;
    //        //gvTheGrid.PageIndex = 0;

    //        if (_gvPager != null)
    //        {
    //            //BindTheGridForExport(0, _gvPager.TotalRows);
    //        }
    //        else
    //        {
    //            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Problem", "alert('There is not records to email.');", true);
    //            lblMsg.Text = "There is no records to email.";
    //            return;
    //        }


    //        StringWriter sw = new StringWriter();
    //        HtmlTextWriter hw = new HtmlTextWriter(sw);

    //        int iTN = 0;
    //        gvTheGrid.PageIndex = 0;

    //        string strOrderDirection = "DESC";
    //        string sOrder = GetDataKeyNames()[0];

    //        if (gvTheGrid.GridViewSortDirection == SortDirection.Ascending)
    //        {
    //            strOrderDirection = "ASC";
    //        }
    //        sOrder = gvTheGrid.GridViewSortColumn + " ";


    //        if (sOrder.Trim() == "")
    //        {
    //            sOrder = "DBGSystemRecordID";
    //        }




    //        TextSearch = TextSearch + hfTextSearch.Value;

    //        if ((bool)_theUserRole.IsAdvancedSecurity)
    //        {
    //            if (_strRecordRightID == Common.UserRoleType.OwnData)
    //            {
    //                //TextSearch = TextSearch + " AND Record.OwnerUserID=" + _ObjUser.UserID.ToString();
    //                TextSearch = TextSearch + " AND (Record.OwnerUserID=" + _ObjUser.UserID.ToString() + " OR Record.EnteredBy=" + _ObjUser.UserID.ToString() + ")";
    //            }
    //        }
    //        else
    //        {
    //            if (Session["roletype"].ToString() == Common.UserRoleType.OwnData)
    //            {
    //                //TextSearch = TextSearch + " AND Record.OwnerUserID=" + _ObjUser.UserID.ToString();
    //                TextSearch = TextSearch + " AND (Record.OwnerUserID=" + _ObjUser.UserID.ToString() + " OR Record.EnteredBy=" + _ObjUser.UserID.ToString() + ")";
    //            }
    //        }

    //        PopulateDateAddedSearch();
    //        //DataTable dt = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
    //        //        ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
    //        //        !chkIsActive.Checked,
    //        //        chkShowOnlyWarning.Checked == false ? null : (bool?)true,
    //        //        null, null,
    //        //          sOrder, strOrderDirection, 0, _gvPager.TotalRows, ref iTN, ref _iTotalDynamicColumns, "export", _strNumericSearch, TextSearch + TextSearchParent,
    //        //        _dtDateFrom, _dtDateTo, "", "", "", null);
    //        if (chkShowAdvancedOptions.Checked && ddlUploadedBatch.SelectedValue != "")
    //        {
    //            TextSearch = TextSearch + "  AND Record.TempRecordID IN  (SELECT TempRecord.RecordID FROM TempRecord WHERE TempRecord.BatchID=" + ddlUploadedBatch.SelectedValue + ")";
    //        }

    //        DataTable dt = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
    //               ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
    //               !chkIsActive.Checked,
    //               chkShowOnlyWarning.Checked == false ? null : (bool?)true,
    //               null, null,
    //                 sOrder, strOrderDirection, 0, _gvPager.TotalRows, ref iTN, ref _iTotalDynamicColumns,
    //                 _strListType, _strNumericSearch, TextSearch + TextSearchParent,
    //               _dtDateFrom, _dtDateTo, "", "", "", int.Parse(hfViewID.Value));


    //        _dtRecordColums = RecordManager.ets_Table_Columns_Summary(TableID, int.Parse(hfViewID.Value));


    //        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //        {
    //            for (int j = 0; j < dt.Columns.Count; j++)
    //            {
    //                //if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "data_retriever")
    //                //{
    //                //    if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                //    {
    //                //        DataRetriever theDataRetriever = DocumentManager.dbg_DataRetriever_Detail(int.Parse(_dtRecordColums.Rows[i]["DataRetrieverID"].ToString()), null, null);

    //                //        if (theDataRetriever.CodeSnippet != "")
    //                //        {
    //                //            foreach (DataRow drDS in dt.Rows)
    //                //            {
    //                //                if (drDS["DBGSystemRecordID"].ToString() != "")
    //                //                {
    //                //                    drDS[dt.Columns[j].ColumnName] = Common.GetValueFromSQL(theDataRetriever.CodeSnippet.Replace("#ID#",
    //                //                        drDS["DBGSystemRecordID"].ToString()));
    //                //                }
    //                //            }

    //                //        }
    //                //    }
    //                //}


    //                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "calculation")
    //                {
    //                    if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                    {
    //                        if (_dtRecordColums.Rows[i]["Calculation"] != DBNull.Value)
    //                        {

    //                            bool bDateCal = false;
    //                            if (_dtRecordColums.Rows[i]["TextType"] != DBNull.Value
    //                                && _dtRecordColums.Rows[i]["TextType"].ToString().ToLower() == "d")
    //                            {
    //                                bDateCal = true;
    //                            }

    //                            foreach (DataRow drDS in dt.Rows)
    //                            {
    //                                if (drDS["DBGSystemRecordID"].ToString() != "")
    //                                {
    //                                    try
    //                                    {
    //                                        if (bDateCal == true)
    //                                        {
    //                                            string strCalculation = _dtRecordColums.Rows[i]["Calculation"].ToString();
    //                                            drDS[_dtDataSource.Columns[j].ColumnName] = TheDatabaseS.GetDateCalculationResult(_dtColumnsAll, strCalculation, int.Parse(drDS["DBGSystemRecordID"].ToString()),_iParentRecordID,
    //                                                _dtRecordColums.Rows[i]["DateCalculationType"] == DBNull.Value ? "" : _dtRecordColums.Rows[i]["DateCalculationType"].ToString());
    //                                        }
    //                                        else
    //                                        {
    //                                            string strFormula = TheDatabaseS.GetCalculationFormula(int.Parse(_qsTableID), _dtRecordColums.Rows[i]["Calculation"].ToString());

    //                                            //drDS[dt.Columns[j].ColumnName] = Common.GetValueFromSQL("SELECT " + strFormula + " FROM Record WHERE RecordID=" + drDS["DBGSystemRecordID"].ToString());

    //                                            drDS[dt.Columns[j].ColumnName] = TheDatabaseS.GetCalculationResult(_dtColumnsAll, strFormula, int.Parse(drDS["DBGSystemRecordID"].ToString()), i, _iParentRecordID);

    //                                        }
    //                                        if (bDateCal == false && _dtRecordColums.Rows[i]["IsRound"] != DBNull.Value && _dtRecordColums.Rows[i]["RoundNumber"] != DBNull.Value)
    //                                        {
    //                                            drDS[dt.Columns[j].ColumnName] = Math.Round(double.Parse(drDS[dt.Columns[j].ColumnName].ToString()), int.Parse(_dtRecordColums.Rows[i]["RoundNumber"].ToString())).ToString();

    //                                        }
    //                                    }
    //                                    catch
    //                                    {
    //                                        //
    //                                    }
    //                                }
    //                            }

    //                        }
    //                    }
    //                }

    //            }
    //        }

    //        //}
    //        dt.AcceptChanges();


    //        if (chkShowAdvancedOptions.Checked == true)//_bDynamicSearch
    //        {
    //            //if (ddlYAxis.SelectedValue != "")
    //            //{
    //            //    Column theColumn = RecordManager.ets_Column_Details(int.Parse(ddlYAxis.SelectedValue));
    //            //    if (theColumn != null)
    //            //    {
    //            //        if (theColumn.ColumnType == "calculation")
    //            //        {
    //            //            if (txtSearchText.Text != "")
    //            //            {
    //            //                DataView dtView = new DataView(dt);

    //            //                dtView.RowFilter = theColumn.DisplayTextSummary + " LIKE '%" + txtSearchText.Text.Replace("'", "''") + "%'";
    //            //                dt = dtView.ToTable();
    //            //            }
    //            //        }

    //            //    }
    //            //}

    //        }




    //        DataRow drFooter = dt.NewRow();

    //        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //        {
    //            for (int j = 0; j < dt.Columns.Count; j++)
    //            {
    //                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                {
    //                    if (_dtRecordColums.Rows[i]["ShowTotal"].ToString().ToLower() == "true")
    //                    {

    //                        //drFooter[_dtRecordColums.Rows[i]["NameOnExport"].ToString()] = dt.Compute("SUM([" + _dtRecordColums.Rows[i]["NameOnExport"].ToString() + "])", "[" + _dtRecordColums.Rows[i]["NameOnExport"].ToString() + "]<>''");
    //                        drFooter[_dtRecordColums.Rows[i]["NameOnExport"].ToString()] = CalculateTotalForAColumn(dt, dt.Columns[j].ColumnName, bool.Parse(_dtRecordColums.Rows[i]["IgnoreSymbols"].ToString().ToLower()));

    //                    }
    //                }

    //            }

    //        }

    //        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //        {
    //            for (int j = dt.Columns.Count - 1; j >= 0; j--)
    //            {
    //                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                {
    //                    if (_dtRecordColums.Rows[i]["OnlyForAdmin"].ToString().ToLower() == "1")
    //                    {
    //                        if (!Common.HaveAccess(_strRecordRightID, "1,2"))
    //                        {
    //                            dt.Columns.RemoveAt(j);
    //                        }
    //                    }
    //                }

    //            }

    //        }

    //        for (int j = dt.Columns.Count - 1; j >= 0; j--)
    //        {
    //            if (dt.Columns[j].ColumnName.IndexOf("_ID**") > -1)
    //            {
    //                dt.Columns.RemoveAt(j);
    //            }

    //        }

    //        dt.Rows.Add(drFooter);

    //        //Round export

    //        foreach (DataRow dr in dt.Rows)
    //        {
    //            for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
    //            {
    //                for (int j = 0; j < dt.Columns.Count; j++)
    //                {
    //                    if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                    {
    //                        if (IsStandard(_dtRecordColums.Rows[i]["SystemName"].ToString()) == false)
    //                        {

    //                            if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "file"
    //                            || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "image")
    //                            {
    //                                if (dr[j].ToString() != "" && dr[j].ToString() != "&nbsp;")
    //                                {
    //                                    try
    //                                    {
    //                                        if (dr[j].ToString().Length > 37)
    //                                        {
    //                                            dr[j] = dr[j].ToString().Substring(37);

    //                                        }
    //                                    }
    //                                    catch
    //                                    {

    //                                        //
    //                                    }
    //                                }

    //                            }

    //                            if (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "value_text"
    //                            && (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
    //                             || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "radiobutton")
    //                           && _dtRecordColums.Rows[i]["DropdownValues"].ToString() != "")
    //                            {

    //                                if (dr[j].ToString() != "")
    //                                {
    //                                    string strText = GetTextFromValueForDD(_dtRecordColums.Rows[i]["DropdownValues"].ToString(), dr[j].ToString());
    //                                    if (strText != "")
    //                                        dr[j] = strText;
    //                                }

    //                            }

    //                            if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "time")
    //                            {
    //                                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                                {

    //                                    if (dr[j].ToString() != "")
    //                                    {

    //                                        TimeSpan ts = TimeSpan.Parse(dr[j].ToString());
    //                                        dr[j] = ts.ToString(@"hh\:mm");
    //                                    }
    //                                }

    //                            }

    //                            //if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "data_retriever"
    //                            //&& _dtRecordColums.Rows[i]["DataRetrieverID"] != DBNull.Value)
    //                            //{
    //                            //    if (_dtRecordColums.Rows[i]["Heading"].ToString() == dt.Columns[j].ColumnName)
    //                            //    {
    //                            //        DataRetriever theDataRetriever = DocumentManager.dbg_DataRetriever_Detail(int.Parse(_dtRecordColums.Rows[i]["DataRetrieverID"].ToString()), null, null);

    //                            //        if (theDataRetriever.CodeSnippet != "")
    //                            //        {
    //                            //            dr[j] = Common.GetValueFromSQL(theDataRetriever.CodeSnippet.Replace("#ID#",
    //                            //                dr["DBGSystemRecordID"].ToString()));
    //                            //        }
    //                            //    }
    //                            //}

    //                            //if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
    //                            //    && (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
    //                            //    || _dtRecordColums.Rows[i]["DropDownType"].ToString() == "tabledd")
    //                            //     && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
    //                            //    && _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
    //                            //{
    //                            //    if (_dtRecordColums.Rows[i]["Heading"].ToString() == dt.Columns[j].ColumnName)
    //                            //    {

    //                            //        if (dr[j].ToString() != "" && dr[j].ToString() != "&nbsp;")
    //                            //        {
    //                            //            try
    //                            //            {

    //                            //                Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["LinkedParentColumnID"].ToString()));

    //                            //                //int iTableRecordID = int.Parse(dr[j].ToString());
    //                            //                string strLinkedColumnValue = dr[j].ToString();
    //                            //                DataTable dtTableTableSC = Common.DataTableFromText("SELECT SystemName,DisplayName FROM [Column] WHERE   TableID ="
    //                            //                 + _dtRecordColums.Rows[i]["TableTableID"].ToString());

    //                            //                string strDisplayColumn = _dtRecordColums.Rows[i]["DisplayColumn"].ToString();

    //                            //                foreach (DataRow dr2 in dtTableTableSC.Rows)
    //                            //                {
    //                            //                    strDisplayColumn = strDisplayColumn.Replace("[" + dr2["DisplayName"].ToString() + "]", "[" + dr2["SystemName"].ToString() + "]");

    //                            //                }
    //                            //                string sstrDisplayColumnOrg = strDisplayColumn;
    //                            //                string strFilterSQL = "";
    //                            //                if (theLinkedColumn.SystemName.ToLower() == "recordid")
    //                            //                {
    //                            //                    strFilterSQL = strLinkedColumnValue;
    //                            //                }
    //                            //                else
    //                            //                {
    //                            //                    strFilterSQL = "'" + strLinkedColumnValue.Replace("'", "''") + "'";
    //                            //                }

    //                            //                //DataTable dtTheRecord = Common.DataTableFromText("SELECT * FROM Record WHERE RecordID=" + iTableRecordID.ToString());

    //                            //                DataTable dtTheRecord = Common.DataTableFromText("SELECT * FROM Record WHERE TableID=" + theLinkedColumn.TableID.ToString() + " AND " + theLinkedColumn.SystemName + "=" + strFilterSQL);

    //                            //                if (dtTheRecord.Rows.Count > 0)
    //                            //                {
    //                            //                    foreach (DataColumn dc in dtTheRecord.Columns)
    //                            //                    {
    //                            //                        strDisplayColumn = strDisplayColumn.Replace("[" + dc.ColumnName + "]", dtTheRecord.Rows[0][dc.ColumnName].ToString());
    //                            //                    }
    //                            //                }
    //                            //                if (sstrDisplayColumnOrg != strDisplayColumn)
    //                            //                    dr[j] = strDisplayColumn;
    //                            //            }
    //                            //            catch
    //                            //            {
    //                            //                //
    //                            //            }


    //                            //        }
    //                            //    }

    //                            //}


    //                            if (_dtRecordColums.Rows[i]["IsRound"] != DBNull.Value)
    //                            {
    //                                if (_dtRecordColums.Rows[i]["IsRound"].ToString().ToLower() == "true")
    //                                {
    //                                    if (dr[j].ToString() != "")
    //                                    {
    //                                        dr[j] = Math.Round(double.Parse(dr[j].ToString()), int.Parse(_dtRecordColums.Rows[i]["RoundNumber"].ToString())).ToString();
    //                                    }
    //                                }

    //                            }
    //                        }

    //                    }

    //                    //mm:hh
    //                    if (_dtRecordColums.Rows[i]["SystemName"].ToString().ToLower() == "datetimerecorded")
    //                    {

    //                        if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "datetime")
    //                        {
    //                            if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                            {
    //                                if (dr[j].ToString().Length > 15)
    //                                {
    //                                    dr[j] = dr[j].ToString().Substring(0, 16);
    //                                }
    //                            }
    //                        }

    //                        if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "date")
    //                        {
    //                            if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
    //                            {
    //                                if (dr[j].ToString().Length > 9)
    //                                {
    //                                    dr[j] = dr[j].ToString().Substring(0, 10);
    //                                }
    //                            }
    //                        }


    //                    }

    //                }
    //            }
    //        }


    //        // First we will write the headers.

    //        int iColCount = dt.Columns.Count;


    //        for (int i = 0; i < iColCount - 2; i++)
    //        {
    //            sw.Write(dt.Columns[i]);
    //            if (i < iColCount - 3)
    //            {
    //                sw.Write(",");
    //            }

    //        }

    //        sw.Write(sw.NewLine);



    //        // Now write all the rows.


    //        foreach (DataRow dr in dt.Rows)
    //        {
    //            for (int i = 0; i < iColCount - 2; i++)
    //            {
    //                if (!Convert.IsDBNull(dr[i]))
    //                {
    //                    sw.Write("\"" + dr[i].ToString().Replace("\"", "'") + "\"");
    //                }
    //                if (i < iColCount - 3)
    //                {
    //                    sw.Write(",");
    //                }
    //            }
    //            sw.Write(sw.NewLine);
    //        }
    //        sw.Close();

    //        FileStream Fs = new FileStream(strFullFileName, FileMode.Create);
    //        BinaryWriter BWriter = new BinaryWriter(Fs, Encoding.GetEncoding("UTF-8"));
    //        BWriter.Write(sw.ToString());
    //        BWriter.Close();
    //        Fs.Close();


    //        //now lets email this to the user.

    //        Response.Redirect(Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/User/SendEmail.aspx?Source=" + Cryptography.Encrypt("Recordlist") + "&SearchCriteriaID=" + Cryptography.Encrypt(SearchCriteriaID.ToString()) + "&TableID=" + Cryptography.Encrypt(TableID.ToString()) + "&FileName=" + Cryptography.Encrypt(strFileName), false);

    //    }
    //    catch (Exception ex)
    //    {
    //        lblMsg.Text = ex.Message + ex.StackTrace;
    //    }


    //}


    protected void ddlTableMenu_SelectedIndexChanged(object sender, EventArgs e)
    {
        Response.Redirect(Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/" + _strRecordFolder + "/RecordList.aspx?TableID=" + Cryptography.Encrypt(ddlTableMenu.SelectedValue), false);
    }


    protected void PopulateTerminology()
    {

        //stgFieldToUpdate.InnerText = stgFieldToUpdate.InnerText.Replace("Field", SecurityManager.etsTerminology(Request.Path.Substring(Request.Path.LastIndexOf("/") + 1), "Field", "Field"));

        //lblLocations.Text = SecurityManager.etsTerminology( Request.Path.Substring(Request.Path.LastIndexOf("/")+1), lblLocations.Text, lblLocations.Text);
        //imgSchedule.ToolTip = SecurityManager.etsTerminology( Request.Path.Substring(Request.Path.LastIndexOf("/")+1), imgSchedule.ToolTip, imgSchedule.ToolTip);
        //imgSites.ToolTip = SecurityManager.etsTerminology(Request.Path.Substring(Request.Path.LastIndexOf("/")+1), imgSites.ToolTip, imgSites.ToolTip);
        //imgDoc.ToolTip = SecurityManager.etsTerminology( Request.Path.Substring(Request.Path.LastIndexOf("/")+1), imgDoc.ToolTip, imgDoc.ToolTip);

    }

    protected void PopulateMenuTableDDL()
    {
        int iTN = 0;
        ddlTableMenu.DataSource = RecordManager.ets_Table_Select(null,
                null, null,
                int.Parse(Session["AccountID"].ToString()),
                null, null, null,
                "st.TableName", "ASC",
                null, null, ref iTN, Session["STs"].ToString());

        ddlTableMenu.DataBind();

    }

    private void ClearCellColourCache()
    {
        if (_dictCellColourRules != null)
        {
            foreach (int key in _dictCellColourRules.Keys)
                _dictCellColourRules[key].Clear();
            _dictCellColourRules.Clear();
            _dictCellColourRules = null;
        }
        if (_dictCellMap != null)
        {
            _dictCellMap.Clear();
            _dictCellMap = null;
        }
    }

    private void CreateCellColourCache()
    {
        _dictCellMap = new Dictionary<int, string>();
        int iTRN = 0;
        List<Column> tableColumns = null;
        if (_theTable.TableID.HasValue)
            tableColumns = RecordManager.ets_Table_Columns(_theTable.TableID.Value, null, null, ref iTRN);
        if (tableColumns != null)
        {
            foreach (Column column in tableColumns)
            {
                _dictCellMap.Add(column.ColumnID.Value, column.SystemName);
            }
        }

        _dictCellColourRules = new Dictionary<int, List<Tuple<int, string, string, string, string>>>();
        string strColumnIdList = String.Empty;
        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
        {
            int columnId = (int)_dtRecordColums.Rows[i]["ColumnID"];
            if (_dtRecordColums.Rows[i]["ColourCells"] != null && _dtRecordColums.Rows[i]["ColourCells"].ToString().ToLower() == "true")
            {
                strColumnIdList += columnId.ToString() + ",";
            }
        }
        if (strColumnIdList.Length > 0)
        {
            strColumnIdList = strColumnIdList.Remove(strColumnIdList.Length - 1);
            DataTable dtCellColourRules = Common.DataTableFromText("SELECT * FROM [ColumnColour] WHERE [Context]='columnid' AND [ID] IN (" + strColumnIdList + ")");
            if (dtCellColourRules != null)
            {
                foreach (DataRow row in dtCellColourRules.Rows)
                {
                    int key = (int)row["ID"];
                    Tuple<int, string, string, string, string> rule = new Tuple<int, string, string, string, string>(
                        (int)row["ControllingColumnID"], (string)row["Operator"], (string)row["Value"], (string)row["ColourText"], row["ColourCell"] == DBNull.Value ? "" : (string)row["ColourCell"]); //red 3802-2
                    if (_dictCellColourRules.ContainsKey(key))
                        _dictCellColourRules[key].Add(rule);
                    else
                    {
                        List<Tuple<int, string, string, string, string>> listRules = new List<Tuple<int, string, string, string, string>>();
                        listRules.Add(rule);
                        _dictCellColourRules.Add(key, listRules);
                    }
                }
            }
        }
    }

    private void GetCellColour(int columnId, Record record, ref string colourText, ref string colourCell)
    {
        /*Update June 21 2018 by Red*/
        /* Updated June 2018 by Alex */
        //string ret = String.Empty;

        if (_dictCellColourRules.ContainsKey(columnId))
        {
            //Column column = RecordManager.ets_Column_Details(columnId); should be below
            foreach (Tuple<int, string, string, string, string> rule in _dictCellColourRules[columnId])
            {
                Column column = RecordManager.ets_Column_Details(rule.Item1);
                bool ruleResult = false;

                if (_dictCellMap.ContainsKey(rule.Item1))
                {
                    string sControllingColumnSystemName = _dictCellMap[rule.Item1];
                    string sControllingColumnValue = RecordManager.GetRecordValue(ref record, sControllingColumnSystemName);
                    string sRuleValue = rule.Item3;

                    bool boolResult = false;
                    int intResult = 0;
                    bool isEmptyValue = false;

                    if (!String.IsNullOrEmpty(rule.Item2))
                    {
                        if ((rule.Item2 == "empty") || (rule.Item2 == "notempty"))
                            boolResult = String.IsNullOrEmpty(sControllingColumnValue);
                        else if ((rule.Item2 == "contains") || (rule.Item2 == "notcontains"))
                        {
                            if (String.IsNullOrEmpty(sControllingColumnValue))
                                isEmptyValue = true;
                            else
                                boolResult = sControllingColumnValue.Contains(sRuleValue);
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(sControllingColumnValue))
                                isEmptyValue = true;
                            else
                                switch (column.ColumnType)
                                {
                                    case "number":
                                        decimal dControllingColumnValue = 0;
                                        decimal dRuleValue = 0;
                                        if (decimal.TryParse(Common.IgnoreSymbols(sControllingColumnValue), out dControllingColumnValue) &&
                                            decimal.TryParse(Common.IgnoreSymbols(sRuleValue), out dRuleValue))
                                        {
                                            switch (rule.Item2)
                                            {
                                                case "equals":
                                                    if (dControllingColumnValue == dRuleValue)
                                                        intResult = 0;
                                                    else
                                                        intResult = 1;
                                                    break;
                                                case "notequal":
                                                    if (dControllingColumnValue == dRuleValue)
                                                        intResult = 0;
                                                    else
                                                        intResult = 1;
                                                    break;
                                                case "greaterthan":
                                                    if (dControllingColumnValue > dRuleValue)
                                                        intResult = 1;
                                                    else
                                                        intResult = -1;
                                                    break;
                                                case "greaterthanequal":
                                                    if (dControllingColumnValue >= dRuleValue)
                                                        intResult = 1;
                                                    else
                                                        intResult = -1;
                                                    break;
                                                case "lessthan":
                                                    if (dControllingColumnValue < dRuleValue)
                                                        intResult = -1;
                                                    else
                                                        intResult = 1;
                                                    break;
                                                case "lessthanequal":
                                                    if (dControllingColumnValue <= dRuleValue)
                                                        intResult = -1;
                                                    else
                                                        intResult = 1;
                                                    break;
                                            }
                                        }
                                        else
                                            isEmptyValue = true;
                                        break;
                                    default:
                                        intResult = String.Compare(sControllingColumnValue, sRuleValue, StringComparison.OrdinalIgnoreCase);
                                        break;
                                }
                        }

                        if (!isEmptyValue)
                        {
                            switch (rule.Item2)
                            {
                                case "equals":
                                    ruleResult = intResult == 0;
                                    break;
                                case "notequal":
                                    ruleResult = intResult != 0;
                                    break;
                                case "greaterthan":
                                    ruleResult = intResult > 0;
                                    break;
                                case "greaterthanequal":
                                    ruleResult = intResult >= 0;
                                    break;
                                case "lessthan":
                                    ruleResult = intResult < 0;
                                    break;
                                case "lessthanequal":
                                    ruleResult = intResult <= 0;
                                    break;
                                case "contains":
                                    ruleResult = boolResult;
                                    break;
                                case "notcontains":
                                    ruleResult = !boolResult;
                                    break;
                                case "empty":
                                    ruleResult = boolResult;
                                    break;
                                case "notempty":
                                    ruleResult = !boolResult;
                                    break;
                            }
                        }
                    }
                }
                if (ruleResult)
                {
                    //ret = rule.Item4;
                    colourText = rule.Item4;
                    colourCell = rule.Item5;
                    //break;
                }
            }
        }
        //return ret;

    }

    /* J Bosker 20 June 2018 
    Ticket 2004: Found a bug in GetCellColour() so Alex provided a fix (pasted above)
    Remove this code after July 2018

    private string GetCellColour(int columnId, Record record)
    {
        string ret = String.Empty;

        if (_dictCellColourRules.ContainsKey(columnId))
        {
            foreach (Tuple<int, string, string, string> rule in _dictCellColourRules[columnId])
            {
                bool ruleResult = false;

                if (_dictCellMap.ContainsKey(rule.Item1))
                {
                    string sControllingColumnSystemName = _dictCellMap[rule.Item1];
                    string sControllingColumnValue = RecordManager.GetRecordValue(ref record, sControllingColumnSystemName);
                    string sRuleValue = rule.Item3;

                    bool boolResult = false;
                    int intResult = 0;
                    bool isEmptyValue = false;

                    if (!String.IsNullOrEmpty(rule.Item2))
                    {
                        if ((rule.Item2 == "empty") || (rule.Item2 == "notempty"))
                            boolResult = String.IsNullOrEmpty(sControllingColumnValue);
                        else if ((rule.Item2 == "contains") || (rule.Item2 == "notcontains"))
                        {
                            if (String.IsNullOrEmpty(sControllingColumnValue))
                                isEmptyValue = true;
                            else
                                boolResult = sControllingColumnValue.Contains(sRuleValue);
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(sControllingColumnValue))
                                isEmptyValue = true;
                            else
                                intResult = String.Compare(sControllingColumnValue, sRuleValue, StringComparison.OrdinalIgnoreCase);
                        }

                        if (!isEmptyValue)
                        {
                            switch (rule.Item2)
                            {
                                case "equals":
                                    ruleResult = intResult == 0;
                                    break;
                                case "notequal":
                                    ruleResult = intResult != 0;
                                    break;
                                case "greaterthan":
                                    ruleResult = intResult > 0;
                                    break;
                                case "greaterthanequal":
                                    ruleResult = intResult >= 0;
                                    break;
                                case "lessthan":
                                    ruleResult = intResult < 0;
                                    break;
                                case "lessthanequal":
                                    ruleResult = intResult <= 0;
                                    break;
                                case "contains":
                                    ruleResult = boolResult;
                                    break;
                                case "notcontains":
                                    ruleResult = !boolResult;
                                    break;
                                case "empty":
                                    ruleResult = boolResult;
                                    break;
                                case "notempty":
                                    ruleResult = !boolResult;
                                    break;
                            }
                        }
                    }
                }
                if (ruleResult)
                {
                    ret = rule.Item4;
                    //break;
                }
            }
        }
        return ret;
    }
    */
    public string retrieve_current_photo(int? ColumnID)
    {
        //string strFilesFolder = "../../FileSite/UserFiles/AppFiles/";
        string strFilesFolder = "/UserFiles/AppFiles/";

        try
        {
            DataTable dtTheRecord = Common.DataTableFromText("SELECT * FROM DetailPhoto WHERE TableID=" + _theTable.TableID + " AND ColumnID=" + ColumnID);
            if (dtTheRecord.Rows.Count > 0)
            {
                RecordManager.detail_photo_list((int)_theTable.TableID, ColumnID);
                foreach (detail_photo_class x in RecordManager.detail_photoLst)
                {
                    return _strFilesLocation + strFilesFolder + x.Filename;
                }
                //ImageDetail.ImageUrl = _strFilesLocation + strFilesFolder + fileName;
            }
        }
        catch
        { }

        return "";
    }


    //red Ticket 3358
    protected void chkUseArchive_CheckedChanged(object sender, EventArgs e)
    {
        /* Disabled by Red - Ticket 4371 */
        //if (chkUseArchive.Checked)
        //{

        //    /* Except Graph icon - Red Ticket 4371 */
        //    // tdTopButtons.Visible = false; // hide top controls
        //    divUpload.Visible = false;
        //    divEmail.Visible = false;
        //    divConfig.Visible = false;
        //    /* end Red Ticket 4371 */

        //    lblUseArchive.Font.Bold = true;
        //    //Session["useArchiveData" + _theTable.TableID] = _theTable.TableID;
        //    lnkSearch_Click(null, null);
        //}
        //else
        //{
        //    /* Except Graph icon - Red Ticket 4371 */
        //    // tdTopButtons.Visible = false; // hide top controls
        //    divUpload.Visible = false;
        //    divEmail.Visible = false;
        //    divConfig.Visible = false;
        //    /* end Red Ticket 4371 */

        //    lblUseArchive.Font.Bold = false;
        //    //Session["useArchiveData" + _theTable.TableID] = null;
        //    lnkSearch_Click(null, null);
        //}


    }

    //Red Transferred all here; encountered bug in child tab list
    protected void DeleteFancy(string strType)
    {

        EnsureSecurity(ViewState["iTN"] != null ? int.Parse(ViewState["iTN"].ToString()) : int.Parse(Session["iTN"].ToString()));
        string strChkAllClientID = "ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid_ctl02_chkAll";
        if (_bFixedHeader)
        {
            //
        }
        else
        {
            if (gvTheGrid.HeaderRow.FindControl("chkAll") != null)
            {
                strChkAllClientID = ((CheckBox)gvTheGrid.HeaderRow.FindControl("chkAll")).ClientID;
            }
        }
       
        if (_bAllowedToDelete)
        {
            bool bAllOrWithChecked = ((CheckBox)gvTheGrid.HeaderRow.FindControl("chkAll")).Checked;
            if (!bAllOrWithChecked)           
            {
                for (int i = 0; i < gvTheGrid.Rows.Count; i++)
                {
                    bool ischeck = ((CheckBox)gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
                    if (ischeck)
                    {
                        bAllOrWithChecked = true;
                        break;
                    }
                }
            }
          
            string xml = null;
            xml = @"<root>" +
                      " <TableName>" + HttpUtility.HtmlEncode(_theTable.TableName) + "</TableName>" +
                   " <DynamictabPart>" + HttpUtility.HtmlEncode(_strDynamictabPart) + "</DynamictabPart>" +
                   " <lnkDeleteAllOK>" + HttpUtility.HtmlEncode(lnkDeleteAllOK.ClientID) + "</lnkDeleteAllOK>" +
                    " <chkAll>" + HttpUtility.HtmlEncode(strChkAllClientID) + "</chkAll>" +
                   " <DeleteReason>" + HttpUtility.HtmlEncode(_bDeleteReason.ToString()) + "</DeleteReason>" +
                   " <RecordRightID>" + HttpUtility.HtmlEncode(_strRecordRightID) + "</RecordRightID>" +
                    " <hfParmanentDelete>" + HttpUtility.HtmlEncode(hfParmanentDelete.ClientID) + "</hfParmanentDelete>" +
                     " <hfchkDeleteParmanent>" + HttpUtility.HtmlEncode(hfchkDeleteParmanent.ClientID) + "</hfchkDeleteParmanent>" +
                      " <hfchkUndo>" + HttpUtility.HtmlEncode(hfchkUndo.ClientID) + "</hfchkUndo>" +
                      " <hfchkUndo>" + HttpUtility.HtmlEncode(hfchkUpdateEveryItem.ClientID) + "</hfchkUndo>" +
                       " <hfchkDelateAllEvery>" + HttpUtility.HtmlEncode(hfchkDelateAllEvery.ClientID) + "</hfchkDelateAllEvery>" +
                        " <hftxtDeleteReason>" + HttpUtility.HtmlEncode(hftxtDeleteReason.ClientID) + "</hftxtDeleteReason>" +
                         " <TableID>" + HttpUtility.HtmlEncode(_qsTableID) + "</TableID>" + //red 27082017
                        " <IsFiltered>" + HttpUtility.HtmlEncode(IsFiltered()) + "</IsFiltered>" + //red 27082017  
                        "</root>";
            SearchCriteria theSearchCriteria = new SearchCriteria(null, xml);
            iSCidForDeleteRecord = SystemData.SearchCriteria_Insert(theSearchCriteria);

            string strDelFancy = String.Empty;
            if (!bAllOrWithChecked)
            {
                strDelFancy = @"
                $(function () {
                    CommonAlertMessage('Please select a record.',2000);
                    return false;
                });
            ";
            }
            else
            {
                strDelFancy = @"
                                                $(function () {
                                                        $('.popuplinkDEL').fancybox({  
                                                            iframe : {
                                                                css : {
                                                                    width : '490px',
                                                                    height: '470px'
                                                                }
                                                            },       
                                                            toolbar  : false,
	                                                        smallBtn : true,  
                                                            scrolling: 'auto',
                                                            type: 'iframe',
                                                            'transitionIn': 'elastic',
                                                            'transitionOut': 'none',
                                                           //  width: 490, //red 27082017
                                                           //  height: 470, //red 27082017
                                                            titleShow: false                       
                                                        });
                                                        var onDeleteAction = document.getElementById('openDeleteAction');
                                                        onDeleteAction.setAttribute('href', '/Pages/Record/DeleteRecord.aspx?type=" + strType + @"&sc_id=" + iSCidForDeleteRecord.ToString() + @"');
                                                        onDeleteAction.click();
                                                    });

                                            ";
            }
            
            ViewState["strDelFancy"] = "ok";
            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "strDelFancy" + _strFunctionUQ, strDelFancy, true);
        }
    }

    

    protected void DeleteLinkButtonHeader_Click(object sender, EventArgs e)
    {
        hfParmanentDelete.Value = "no";
        DeleteFancy("d");

    }

    protected void Pager_OnDeleteAction(object sender, EventArgs e)
    {
        hfParmanentDelete.Value = "no";
        DeleteFancy("d");

    }
    protected void Pager_OnParmanenetDelAction(object sender, EventArgs e)
    {
        DeleteFancy("p");
    }

    protected void Pager_OnUnDeleteAction(object sender, EventArgs e)
    {
        DeleteFancy("r");

    }

    protected void Pager_OnExportAction(object sender, EventArgs e)
    {
        string strChkAllClientID = "ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid_ctl02_chkAll";
        if (_bFixedHeader)
        {
            //
        }
        else
        {
            if (gvTheGrid.HeaderRow.FindControl("chkAll") != null)
            {
                strChkAllClientID = ((CheckBox)gvTheGrid.HeaderRow.FindControl("chkAll")).ClientID;
            }
        }

        string xmlExp = @"<root>" +
            " <TableID>" + HttpUtility.HtmlEncode(_theTable.TableID.ToString()) + "</TableID>" +
            " <ViewID>" + HttpUtility.HtmlEncode(_theView.ViewID.ToString()) + "</ViewID>" +
            " <TableName>" + HttpUtility.HtmlEncode(_theTable.TableName) + "</TableName>" +
            " <DynamictabPart>" + HttpUtility.HtmlEncode(_strDynamictabPart) + "</DynamictabPart>" +
            " <lnkExportRecords>" + HttpUtility.HtmlEncode(lnkExportRecords.ClientID) + "</lnkExportRecords>" +
                " <chkAll>" + HttpUtility.HtmlEncode(strChkAllClientID) + "</chkAll>" +
            " <RecordRightID>" + HttpUtility.HtmlEncode(_strRecordRightID) + "</RecordRightID>" +
               "</root>";
        SearchCriteria theSearchCriteriaExp = new SearchCriteria(null, xmlExp);
        int iSCidForExport = SystemData.SearchCriteria_Insert(theSearchCriteriaExp);

        string strExpFancy = @"
                  
                    $(function () {
                            $('.popuplinkExp').fancybox({
                                 iframe : {
                                    css : {
                                        width : '500px',
                                        height: '560px'
                                    }
                                },       
                                toolbar  : false,
	                            smallBtn : true,  
                                scrolling: 'auto',
                                type: 'iframe',
                                'transitionIn': 'elastic',
                                'transitionOut': 'none', 
                                titleShow: false                       
                            });
                        var onExport = document.getElementById('openExportAction');
                        onExport.setAttribute('href', '/Pages/Record/ExportRecord.aspx?sc_id=" + iSCidForExport.ToString() + @"');
                        onExport.click();
                        });

                                        ";
        ViewState["strExpFancy"] = "ok";
        ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "strExpFancy" + _strFunctionUQ, strExpFancy, true);

    }


    protected void Pager_OnEditManyAction(object sender, EventArgs e)
    {
        string strChkAllClientID = "ctl00_HomeContentPlaceHolder_rlOne_gvTheGrid_ctl02_chkAll";
        if (_bFixedHeader)
        {
            //
        }
        else
        {
            if (gvTheGrid.HeaderRow.FindControl("chkAll") != null)
            {
                strChkAllClientID = ((CheckBox)gvTheGrid.HeaderRow.FindControl("chkAll")).ClientID;
            }
        }

        bool bchkUpdateEveryItem = false;
        if (hfchkUpdateEveryItem.Value != "")
            bchkUpdateEveryItem = bool.Parse(hfchkUpdateEveryItem.Value);

        string sCheck = "";
        bool bAll = ((CheckBox) gvTheGrid.HeaderRow.FindControl("chkAll")).Checked;
        if (bchkUpdateEveryItem && bAll)
        {
            int iOldPS = gvTheGrid.PageSize;
            gvTheGrid.PageSize = _gvPager.TotalRows + 1;
            BindTheGrid(0, _gvPager.TotalRows);
            for (int i = 0; i < gvTheGrid.Rows.Count; i++)
            {
                sCheck = sCheck + ((Label) gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
            }
            sCheck = sCheck + "-1";
            gvTheGrid.PageSize = iOldPS;
        }
        else
        {
            for (int i = 0; i < gvTheGrid.Rows.Count; i++)
            {
                bool ischeck = ((CheckBox) gvTheGrid.Rows[i].FindControl("chkDelete")).Checked;
                if (ischeck)
                {
                    sCheck = sCheck + ((Label) gvTheGrid.Rows[i].FindControl("LblID")).Text + ",";
                }
            }
            sCheck = sCheck + "-1";
        }

        string strEMFancy = String.Empty;
        if (sCheck.Length == 2)
        {
            strEMFancy = @"
                $(function () {
                    CommonAlertMessage('Please select a record.',2000);
                    return false;
                });
            ";
        }
        else
        {
            string xmlEM = @"<root>" +
                           " <TableID>" + HttpUtility.HtmlEncode(_theTable.TableID.ToString()) + "</TableID>" +
                           " <ViewID>" + HttpUtility.HtmlEncode(_theView.ViewID.ToString()) + "</ViewID>" +
                           " <TableName>" + HttpUtility.HtmlEncode(_theTable.TableName) + "</TableName>" +
                           " <DynamictabPart>" + HttpUtility.HtmlEncode(_strDynamictabPart) + "</DynamictabPart>" +
                           " <lnkEditManyOK>" + HttpUtility.HtmlEncode(lnkEditManyOK.ClientID) + "</lnkEditManyOK>" +
                           " <chkAll>" + HttpUtility.HtmlEncode(strChkAllClientID) + "</chkAll>" +
                           " <RecordRightID>" + HttpUtility.HtmlEncode(_strRecordRightID) + "</RecordRightID>" +
                           " <hfddlYAxisBulk>" + HttpUtility.HtmlEncode(hfddlYAxisBulk.ClientID) + "</hfddlYAxisBulk>" +
                           " <hfBulkValue>" + HttpUtility.HtmlEncode(hfBulkValue.ClientID) + "</hfBulkValue>" +
                           " <hfchkUpdateEveryItem>" + HttpUtility.HtmlEncode(hfchkUpdateEveryItem.ClientID) + "</hfchkUpdateEveryItem>" +
                           " <ItemList>" + HttpUtility.HtmlEncode(sCheck) + "</ItemList>" +
                           "</root>";
            SearchCriteria theSearchCriteriaEM = new SearchCriteria(null, xmlEM);
            int iSCidForEditMany = SystemData.SearchCriteria_Insert(theSearchCriteriaEM);

            strEMFancy = @"
                $(function () {
                    $('.popuplinkEM').fancybox({    
                         iframe : {
                            css : {
                                width : '600px',
                                height: '400px'
                            }
                        },       
                        toolbar  : false,
	                    smallBtn : true,  
                        scrolling: 'auto',
                        type: 'iframe',
                        'transitionIn': 'elastic',
                        'transitionOut': 'none',
                        titleShow: false                       
                    });
                    var a = document.getElementById('openEditMany');
                    a.setAttribute('href', '/Pages/Record/EditMany.aspx?sc_id=" + iSCidForEditMany.ToString() + @"');
                    a.click();
                });
            ";
        }

        ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "strEMFancy", strEMFancy, true);
    }

    //RP Added Ticket 4363
    protected void hiddenUploadedBatchID_ValueChanged(object sender, EventArgs e)
    {
        lnkSearch_Click(null, null);
    }
    //End Modification

    //RP Added Ticket 4363
    protected void txtUploadedBatch_TextChanged(object sender, EventArgs e)
    {
        if (txtUploadedBatch.Text == "")
            hiddenUploadedBatchID.Value = "";
        if (hiddenUploadedBatchID.Value == "")
            txtUploadedBatch.Text = "";
        lnkSearch_Click(null, null);
    }
    //End Modification

    //RP Added Ticket 4559
    protected String AddDoubleQuotes(String SQL)
    {
        String sqlConverted = SQL.Replace("SELECT *", "SELECT TOP 1 *");
        System.Collections.ArrayList columns = new System.Collections.ArrayList();


        DataTable dt = Common.DataTableFromText(SQL);

        foreach (System.Data.DataColumn col in dt.Columns)
        {
            String doublequotes = "\"\"";
            columns.Add("CONCAT('" + doublequotes + "', [" + col.ColumnName + "], '" + doublequotes + "') AS [" + col.ColumnName + "]");
        }

        return String.Join(",", columns.ToArray());
    }
    //End Modification
    protected void btnAddGraph_Click(object sender, EventArgs e)
    {

        BindTheGrid(0, gvTheGrid.PageSize);
        if (Session["AddGraphPopupInfo"] == null)
            return;

        System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();

        xmlDoc.Load(new StringReader(Session["AddGraphPopupInfo"].ToString()));

        string sAddGraph = xmlDoc.FirstChild["optAddGraph"].InnerText;
        string sReport = xmlDoc.FirstChild["ddlReport"].InnerText;

        UserRole theUserRole = (UserRole)Session["UserRole"];
        if (sAddGraph == "report")
        {
            //report
            int iReportID;
            int reportItemId;
            if (sReport == "-1")
            {
                iReportID = -1;
                //create a new report
                Report.ReportInterval reportInterval = Report.ReportInterval.IntervalUndefined;
                Report.ReportIntervalEnd reportIntervalEnd = Report.ReportIntervalEnd.Undefined;
                DateTime? dtDateFrom = null;
                DateTime? dtDateTo = null;
                reportInterval = Report.ReportInterval.IntervalDateRange;
                dtDateFrom = DateTime.Now.AddYears(-1);
                dtDateTo = DateTime.Now;

                string sReportName = "New Report created by " + _ObjUser.FirstName + " " + _ObjUser.LastName + " on " + DateTime.Now.ToString("dd MMM yyyy, h:mm tt");
                Report newReport = new Report(null, _theTable.AccountID,
                                sReportName, "",
                                reportInterval, reportIntervalEnd, dtDateFrom, dtDateTo,
                                null, null, _ObjUser.UserID, 0);

                iReportID = ReportManager.ets_Report_Insert(newReport);
                ReportManager.ets_ReportSchedule_SetNotScheduled(iReportID);

            }
            else
            {
                iReportID = int.Parse(sReport);

            }

            ReportItem newReportItem = new ReportItem(null, iReportID,
                        (int)_theTable.TableID,
                        ReportItem.ReportItemType.ItemTypeTable,
                        _theTable.TableName, false, false, false, false, null, null,
                        null, null, _ObjUser.UserID);
            newReportItem.ViewID = _theView.ViewID;
            reportItemId = ReportManager.ets_ReportItem_Insert(newReportItem);
            newReportItem.ReportItemID = reportItemId;
            bool bApplyFilter = false;
            bool bApplySortOrder = false;
            //let's add filter and sort info
            if (!string.IsNullOrEmpty(_theView.FilterControlsInfo))
            {
                Pages_UserControl_ViewDetail vdFilter = new Pages_UserControl_ViewDetail();
                vdFilter = (Pages_UserControl_ViewDetail)LoadControl("~/Pages/UserControl/ViewDetail.ascx");
                System.Xml.XmlDocument xmlFilterDoc = new System.Xml.XmlDocument();
                xmlFilterDoc.Load(new StringReader(_theView.FilterControlsInfo));
                vdFilter.SetView((int)newReportItem.ViewID);
                vdFilter.PopulateFilterControl(xmlFilterDoc.OuterXml, ((int)_theView.TableID));
                

                DataTable dtViewFilterInfo = vdFilter.GetViewFilterDataTable();
                int iDisplayOrder = 1;
                if (dtViewFilterInfo != null && dtViewFilterInfo.Rows.Count > 0)
                {
                    foreach (DataRow drViewFilterInfo in dtViewFilterInfo.Rows)
                    {
                        ReportItemFilter aReportItemFilter = new ReportItemFilter();
                        ReportItemFilter aReportItemFilter2 = new ReportItemFilter();
                        aReportItemFilter.FilterColumnID = int.Parse(drViewFilterInfo["FilterColumnID"].ToString());
                        aReportItemFilter2.FilterColumnID = aReportItemFilter.FilterColumnID;
                        aReportItemFilter.ReportItemID = reportItemId;
                        aReportItemFilter2.ReportItemID = aReportItemFilter.ReportItemID;

                        bool bHasUpperLimit = false;
                        string sNewFilterOperator = drViewFilterInfo["FilterOperator"].ToString();
                        string sFilterColumnValue = drViewFilterInfo["FilterColumnValue"].ToString();
                        switch (drViewFilterInfo["FilterOperator"].ToString())
                        {
                            case ">":
                                sNewFilterOperator = "greaterthan";
                                break;
                            case "<":
                                sNewFilterOperator = "lessthan";
                                break;
                            case "=":
                                sNewFilterOperator = "equals";
                                break;
                            case "<>":
                                sNewFilterOperator = "notequal";
                                break;
                            case "between":

                                if (sFilterColumnValue.IndexOf("____") > -1)
                                {
                                    //FilterColumnID      
                                    sNewFilterOperator = "equals";
                                    string sLowerLimit = sFilterColumnValue.Substring(0, sFilterColumnValue.IndexOf("____"));
                                    string sUpperLimit = sFilterColumnValue.Substring(sFilterColumnValue.IndexOf("____") + 4);
                                    if (sUpperLimit.Trim() != "")
                                    {
                                        //insert another one
                                        sNewFilterOperator = "greaterthanequal";
                                        bHasUpperLimit = true;
                                        sFilterColumnValue = sLowerLimit;


                                        aReportItemFilter2.FilterColumnValue = sUpperLimit;
                                        aReportItemFilter2.FilterOperator = "lessthanequal";
                                    }

                                }

                                break;
                        }
                        aReportItemFilter.FilterOperator = sNewFilterOperator;
                        aReportItemFilter.FilterColumnValue = sFilterColumnValue.Replace("____", "");

                        if (iDisplayOrder > 1)
                            aReportItemFilter.JoinOperator = drViewFilterInfo["JoinOperator"].ToString();
                        else
                            aReportItemFilter.JoinOperator = "";

                        aReportItemFilter.DisplayOrder = iDisplayOrder;

                        ReportManager.ets_ReportItemFilter_Insert(aReportItemFilter);
                        bApplyFilter = true;
                        //if(drViewFilterInfo["FilterOperator"].ToString()==">")

                        if (bHasUpperLimit)
                        {
                            iDisplayOrder = iDisplayOrder + 1;

                            aReportItemFilter2.DisplayOrder = iDisplayOrder;

                            ReportManager.ets_ReportItemFilter_Insert(aReportItemFilter2);
                        }

                        iDisplayOrder = iDisplayOrder + 1;
                    }
                }



                //apply sort order
                DataTable dtViewSortOrderInfo = vdFilter.GetViewSortOrederDataTable();
                if(dtViewSortOrderInfo!=null && dtViewSortOrderInfo.Rows.Count>0)
                {
                    foreach (DataRow drViewSortOrderInfo in dtViewSortOrderInfo.Rows)
                    {
                        ReportItemSortOrder aReportItemSortOrder = new ReportItemSortOrder();
                        aReportItemSortOrder.ReportItemID = reportItemId;
                        aReportItemSortOrder.SortColumnID = int.Parse(drViewSortOrderInfo["SortColumnID"].ToString());
                        aReportItemSortOrder.IsDescending = bool.Parse(drViewSortOrderInfo["IsDescending"].ToString());
                        aReportItemSortOrder.DisplayOrder = int.Parse(drViewSortOrderInfo["DisplayOrder"].ToString());
                        bApplySortOrder = true;
                        ReportManager.ets_ReportItemSortOrder_Insert(aReportItemSortOrder);

                    }
                }
                

            }

            if(bApplyFilter || bApplySortOrder)
            {
                newReportItem = ReportManager.ets_ReportItem_Detail((int)newReportItem.ReportItemID);
                newReportItem.ApplyFilter = bApplyFilter;
                newReportItem.ApplySort = bApplySortOrder;
                ReportManager.ets_ReportItem_Update(newReportItem);
            }

            if(iReportID>0 && reportItemId>0)
            {
                Response.Redirect("~/Pages/ReportWriter/ReportTableDetail.aspx?mode="+Cryptography.Encrypt("edit")+"&SearchCriteria="+Cryptography.Encrypt("-1")+"&ReportID="+
                    Cryptography.Encrypt(iReportID.ToString())+"&ReportItemID="+Cryptography.Encrypt(reportItemId.ToString()), false);
            }

        }
        else
        {
            //add this to the dashboard
            //Get the dashboard id
            int? DashID = DocumentManager.dbg_Dashboard_BestFitting("", (int)_ObjUser.UserID, (int)theUserRole.RoleID);
            if (DashID != null)
            {
                Document theDocument = DocumentManager.ets_Document_Detail((int)DashID);
                if (theDocument != null)
                {
                    int iPosition = 0;
                    string sPrePosition = Common.GetValueFromSQL("SELECT MAX(Position) FROM [DocumentSection] WHERE DocumentID=" + theDocument.DocumentID.ToString());

                    if (sPrePosition == "")
                        sPrePosition = "0";
                    iPosition = int.Parse(sPrePosition) + 1;

                    using (DocGen.DAL.DocGenDataContext ctx = new DocGen.DAL.DocGenDataContext())
                    {
                        DocGen.DAL.DocumentSection newSection = new DocGen.DAL.DocumentSection();
                        //ctx.ExecuteCommand("UPDATE DocumentSection SET Position=Position + 1 WHERE DocumentID={0}  AND Position>{1}", DocumentID.ToString(), (iPosition - 1).ToString());

                        newSection.DocumentID = (int)theDocument.DocumentID;
                        newSection.SectionName = _theView.ViewName;

                        newSection.DocumentSectionTypeID = 10; //Table Table                        

                        //newSection.Content = txtContent.Text;
                        newSection.Position = iPosition;
                        newSection.DateAdded = DateTime.Now;
                        newSection.DateUpdated = DateTime.Now;

                        RecordTableSectionDetail rtSec = new RecordTableSectionDetail();

                        rtSec.TableID = (int)_theTable.TableID;
                        //rtSec.SearchCriteriaID = rlOne.SearchCriteriaID;
                        rtSec.ViewID = (int)ViewManager.dbg_View_Copy((int)_theView.ViewID, (int)_ObjUser.UserID);
                        string sUserSearhXML = Common.GetValueFromSQL("SELECT SearchXML FROM UserSearch WHERE UserID=" + _ObjUser.UserID.ToString() + " AND ViewID=" + _theView.ViewID.ToString());
                        if (!string.IsNullOrEmpty(sUserSearhXML))
                        {
                            SystemData.UserSearch_InsertUpdate(new UserSearch(null, _ObjUser.UserID, rtSec.ViewID, sUserSearhXML, null));
                        }
                        Common.ExecuteText("UPDATE [View] SET ViewPageType='dash' WHERE ViewID=" + rtSec.ViewID.ToString());
                        newSection.Details = rtSec.GetJSONString();
                        ctx.DocumentSections.InsertOnSubmit(newSection);

                        ctx.SubmitChanges();

                        Response.Redirect("~/Default.aspx", false);
                        //ID = newSection.DocumentSectionID;
                    }
                }
            }

        }
    }

}



//public partial class Pages_UserControl_RecordList : System.Web.UI.UserControl
//{
//    protected void Page_Load(object sender, EventArgs e)
//    {

//    }
//}


//  protected string GetDataScopeWhere(int iParentTableID,int iScopeTableID,string strScopeSystemName)
//    {
//        int iCurrentTableID = int.Parse(_qsTableID);

//        DataTable dtRecordID = Common.DataTableFromText("SELECT RecordID FROM Record WHERE TableID=" + iScopeTableID + " AND  " + strScopeSystemName + "='"+_ObjUser.DataScopeValue.Replace("'","''")+"'");
//        string strScopeRecordID = "";
//        if (dtRecordID.Rows.Count > 0)
//        {
//            strScopeRecordID = dtRecordID.Rows[0][0].ToString();
//        }

//        if (iCurrentTableID == iScopeTableID)
//        {
//            return " AND Record." + strScopeSystemName + " ='" + _ObjUser.DataScopeValue + "'";
//        }
//        else
//        {
//            DataTable dtChildTable = Common.DataTableFromText("	SELECT DISTINCT ChildTableID FROM TableChild WHERE ParentTableID=" + iParentTableID);

//            bool bFoundLevel = false;

//            if (dtChildTable.Rows.Count > 0)
//            {

//                foreach (DataRow drCT in dtChildTable.Rows)
//                {
//                    if (iCurrentTableID == int.Parse(drCT[0].ToString()))
//                    {
//                        bFoundLevel = true;
//                    }
//                }

//                foreach (DataRow drCT in dtChildTable.Rows)
//                {


//                    int iChildTableID = int.Parse(drCT[0].ToString());
//                    DataTable dtChildColumn = Common.DataTableFromText(@"	SELECT ColumnID FROM [Column] WHERE ColumnType='dropdown' 
//                    AND (Dropdowntype='table' OR Dropdowntype='tabledd')
//	                AND TableID=" + iChildTableID.ToString() + @" AND TableTableID=" + iParentTableID.ToString());



//                    if (dtChildColumn.Rows.Count > 0)
//                    {



//                        //we have a column
//                        Column theChildColumn = RecordManager.ets_Column_Details(int.Parse(dtChildColumn.Rows[0][0].ToString()));

//                        DataTable dtChildRecordID = Common.DataTableFromText("SELECT RecordID FROM Record WHERE TableID=" + theChildColumn.TableID.ToString() + " AND  " + theChildColumn.SystemName + "='" + strScopeRecordID + "'");

//                        //here we will return filter SQL

//                        string strChildRecordIDs = "";
//                        foreach (DataRow dr in dtChildRecordID.Rows)
//                        {
//                            strChildRecordIDs = strChildRecordIDs + dr[0].ToString() + ",";
//                        }
//                        if (strChildRecordIDs != "")
//                            strChildRecordIDs = strChildRecordIDs.Substring(0, strChildRecordIDs.LastIndexOf(','));


//                        if (iCurrentTableID == (int)theChildColumn.TableID)
//                        {
//                            return " AND Record.RecordID IN (" + strChildRecordIDs + ")";
//                        }
//                        else
//                        {
//                            if (bFoundLevel == false)
//                            {
//                              string strWhere= GetRecursiveDataScope((int)theChildColumn.TableID, iScopeTableID, strChildRecordIDs);
//                              if (strWhere != "")
//                              {
//                                  return strWhere;
//                              }
//                            }
//                        }


//                    }
//                    else
//                    {
//                        //no link
//                    }
//                }
//            }
//            else
//            {
//                //no child tables
//            }

//        }

//        return "";
//    }

//protected string GetRecursiveDataScope(int iParentTableID,int iScopeTableID, string strReocrdIDs)
//    {
//        int iCurrentTableID = int.Parse(_qsTableID);

//        DataTable dtChildTable = Common.DataTableFromText("	SELECT DISTINCT ChildTableID FROM TableChild WHERE ParentTableID=" + iParentTableID);

//        bool bFoundLevel = false;



//        if (dtChildTable.Rows.Count > 0)
//        {

//            foreach (DataRow drCT in dtChildTable.Rows)
//            {
//                if (iCurrentTableID == int.Parse(drCT[0].ToString()))
//                {
//                    bFoundLevel = true;
//                }
//            }

//            foreach (DataRow drCT in dtChildTable.Rows)
//            {
//                int iChildTableID = int.Parse(drCT[0].ToString());
//                DataTable dtChildColumn = Common.DataTableFromText(@"	SELECT ColumnID FROM [Column] WHERE ColumnType='dropdown' 
//                    AND (Dropdowntype='table' OR Dropdowntype='tabledd')
//	                AND TableID=" + iChildTableID.ToString() + @" AND TableTableID=" + iParentTableID.ToString());

//                if (dtChildColumn.Rows.Count > 0)
//                {

//                    //we have a column
//                    Column theChildColumn = RecordManager.ets_Column_Details(int.Parse(dtChildColumn.Rows[0][0].ToString()));

//                    DataTable dtChildRecordID = Common.DataTableFromText("SELECT RecordID FROM Record WHERE TableID=" + theChildColumn.TableID.ToString() + " AND  " + theChildColumn.SystemName + " IN (" + strReocrdIDs + ")");

//                    //here we will return filter SQL

//                    string strChildRecordIDs = "";
//                    foreach (DataRow dr in dtChildRecordID.Rows)
//                    {
//                        strChildRecordIDs = strChildRecordIDs + dr[0].ToString() + ",";
//                    }
//                    if (strChildRecordIDs != "")
//                        strChildRecordIDs = strChildRecordIDs.Substring(0, strChildRecordIDs.LastIndexOf(','));


//                    if (iCurrentTableID == (int)theChildColumn.TableID)
//                    {
//                        return " AND Record.RecordID IN (" + strChildRecordIDs + ")";
//                    }
//                    else
//                    {
//                        if (bFoundLevel == false)
//                        {
//                            string strWhere = GetRecursiveDataScope((int)theChildColumn.TableID, iScopeTableID, strChildRecordIDs);
//                            if (strWhere != "")
//                            {
//                                return strWhere;
//                            }
//                        }
//                    }


//                }
//                else
//                {
//                    //no link
//                }
//            }
//        }
//        else
//        {
//            //no child tables
//        }

//        return "";
//    }






// protected void Pager_OnExportForExcel(object sender, EventArgs e)
//    {

//        DataTable dtExportColumn = RecordManager.ets_Table_Columns_Export(int.Parse(TableID.ToString()),null,null);

//        if (dtExportColumn.Rows.Count == 0)
//        {
//            ScriptManager.RegisterStartupScript(upMain, upMain.GetType(), "NoExportMSG", "alert('Sorry it is not possible to export this table because none of the fields have been marked for export. Please check the table configuration and try again');", true);
//            return;
//        }


//        if (gvTheGrid.VirtualItemCount > Common.MaxRecordsExport)
//        {

//            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page),
//                "message_alert", "alert('There are " + gvTheGrid.VirtualItemCount.ToString() + " Records, we are going to send this file to your email address.');", true);

//            string strBulkExportPath = SystemData.SystemOption_ValueByKey("BulkExportPath");
//            string strFileName = Guid.NewGuid().ToString() + "_" + lblTitle.Text.Replace(" ", "").ToString() + ".csv";
//            string strFullFileName = strBulkExportPath + "\\" + strFileName;
//            PopulateDateAddedSearch();
//            int iIsBulkExportOK = RecordManager.ets_Record_List_BulkExport(int.Parse(TableID.ToString()),
//                ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
//                !chkIsActive.Checked,
//                chkShowOnlyWarning.Checked == false ? null : (bool?)true,
//                _dtDateFrom, _dtDateTo,
//               strFullFileName);


//            if (iIsBulkExportOK == 1)
//            {
//                //lets zip the file

//                string filename = strFullFileName;


//                FileStream infile = File.OpenRead(filename);
//                byte[] buffer = new byte[infile.Length];
//                infile.Read(buffer, 0, buffer.Length);
//                infile.Close();

//                //FileStream outfile = File.Create(Path.ChangeExtension(filename, "zip"));
//                FileStream outfile = File.Create(filename + ".zip");

//                GZipStream gzipStream = new GZipStream(outfile, CompressionMode.Compress);
//                gzipStream.Write(buffer, 0, buffer.Length);
//                gzipStream.Close();

//                //now lets email this to the user.

//                string strEmail = SystemData.SystemOption_ValueByKey("EmailFrom");
//                string strEmailServer = SystemData.SystemOption_ValueByKey("EmailServer");
//                string strEmailUserName = SystemData.SystemOption_ValueByKey("EmailUserName");
//                string strEmailPassword = SystemData.SystemOption_ValueByKey("EmailPassword");
//                MailMessage msg = new MailMessage();
//                msg.From = new MailAddress(strEmail);
//                msg.Subject = lblTitle.Text + " - data file";
//                msg.IsBodyHtml = true;

//                string strBulkExportHTTPPath = SystemData.SystemOption_ValueByKey("BulkExportHTTPPath");

//                string strTheBody = "<div>Please click the file to download.<a href='" + strBulkExportHTTPPath + "/" + strFileName + ".zip" + "'>" + strFileName + ".zip" + "</a></div>";

//                msg.Body = strTheBody;
//                msg.To.Add(_ObjUser.Email);

//                SmtpClient smtpClient = new SmtpClient(strEmailServer);
//                smtpClient.Timeout = 99999;
//                smtpClient.Credentials = new System.Net.NetworkCredential(strEmailUserName, strEmailPassword);

//#if (!DEBUG)
//                smtpClient.Send(msg);
//#endif


//                if (System.Web.HttpContext.Current.Session["AccountID"] != null)
//                {

//                    SecurityManager.Account_SMS_Email_Count(int.Parse(System.Web.HttpContext.Current.Session["AccountID"].ToString()), true, null, null, null);
//                }

//            }
//            else
//            {
//                //failed
//            }

//            return;
//        }



//        Response.Clear();


//        int iTN = 0;
//        gvTheGrid.PageIndex = 0;

//        string strOrderDirection = "DESC";
//        string sOrder = GetDataKeyNames()[0];

//        if (gvTheGrid.GridViewSortDirection == SortDirection.Ascending)
//        {
//            strOrderDirection = "ASC";
//        }
//        sOrder = gvTheGrid.GridViewSortColumn + " ";


//        if (sOrder.Trim() == "")
//        {
//            sOrder = "DBGSystemRecordID";
//        }




//        TextSearch = TextSearch + hfTextSearch.Value;
//        if ((bool)_ObjUser.IsAdvancedSecurity)
//        {
//            if (_strRecordRightID == Common.UserRoleType.OwnData)
//            {
//                TextSearch = TextSearch + " AND (Record.OwnerUserID=" + _ObjUser.UserID.ToString() + " OR Record.EnteredBy=" + _ObjUser.UserID.ToString() + ")";
//            }
//        }
//        else
//        {
//            if (Session["roletype"].ToString() == Common.UserRoleType.OwnData)
//            {
//                TextSearch = TextSearch + " AND (Record.OwnerUserID=" + _ObjUser.UserID.ToString() + " OR Record.EnteredBy=" + _ObjUser.UserID.ToString() + ")";
//            }
//        }
//        PopulateDateAddedSearch();
//        DataTable dt = RecordManager.ets_Record_List(int.Parse(TableID.ToString()),
//                ddlEnteredBy.SelectedValue == "-1" ? null : (int?)int.Parse(ddlEnteredBy.SelectedValue),
//                !chkIsActive.Checked,
//                chkShowOnlyWarning.Checked == false ? null : (bool?)true,
//                 null, null,
//                  sOrder, strOrderDirection, 0, _gvPager.TotalRows, ref iTN, ref _iTotalDynamicColumns, "export", _strNumericSearch, TextSearch + TextSearchParent,
//                  _dtDateFrom, _dtDateTo, "", "", "", null);




//        _dtRecordColums = RecordManager.ets_Table_Columns_Summary(TableID, int.Parse(hfViewID.Value));


//        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
//        {
//            for (int j = 0; j < dt.Columns.Count; j++)
//            {               

//                if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "calculation")
//                {
//                    if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
//                    {

//                        if (_dtRecordColums.Rows[i]["Calculation"] != DBNull.Value)
//                        {

//                            bool bDateCal = false;
//                            if (_dtRecordColums.Rows[i]["TextType"] != DBNull.Value
//                                && _dtRecordColums.Rows[i]["TextType"].ToString().ToLower() == "d")
//                            {
//                                bDateCal = true;
//                            }


//                            foreach (DataRow drDS in dt.Rows)
//                            {
//                                if (drDS["DBGSystemRecordID"].ToString() != "")
//                                {

//                                    try
//                                    {
//                                        if (bDateCal == true)
//                                        {
//                                            string strCalculation = _dtRecordColums.Rows[i]["Calculation"].ToString();
//                                            drDS[_dtDataSource.Columns[j].ColumnName] = TheDatabaseS.GetDateCalculationResult(_dtColumnsAll, strCalculation, int.Parse(drDS["DBGSystemRecordID"].ToString()),_iParentRecordID,
//                                                _dtRecordColums.Rows[i]["DateCalculationType"] == DBNull.Value ? "" : _dtRecordColums.Rows[i]["DateCalculationType"].ToString());
//                                        }
//                                        else
//                                        {
//                                            string strFormula = TheDatabaseS.GetCalculationFormula(int.Parse(_qsTableID), _dtRecordColums.Rows[i]["Calculation"].ToString());

//                                            //drDS[dt.Columns[j].ColumnName] = Common.GetValueFromSQL("SELECT " + strFormula + " FROM Record WHERE RecordID=" + drDS["DBGSystemRecordID"].ToString());
//                                            drDS[dt.Columns[j].ColumnName] = TheDatabaseS.GetCalculationResult(_dtColumnsAll, strFormula, int.Parse(drDS["DBGSystemRecordID"].ToString()), i, _iParentRecordID);


//                                        }
//                                        if (bDateCal == false && _dtRecordColums.Rows[i]["IsRound"] != DBNull.Value && _dtRecordColums.Rows[i]["RoundNumber"] != DBNull.Value)
//                                        {
//                                            drDS[dt.Columns[j].ColumnName] = Math.Round(double.Parse(drDS[dt.Columns[j].ColumnName].ToString()), int.Parse(_dtRecordColums.Rows[i]["RoundNumber"].ToString())).ToString();

//                                        }
//                                    }
//                                    catch
//                                    {
//                                        //
//                                    }
//                                }
//                            }

//                        }

//                    }
//                }
//            }
//        }

//        //}
//        dt.AcceptChanges();


//        if (chkShowAdvancedOptions.Checked == true)//_bDynamicSearch
//        {


//        }



//        DataRow drFooter = dt.NewRow();

//        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
//        {
//            for (int j = 0; j < dt.Columns.Count; j++)
//            {
//                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
//                {
//                    if (_dtRecordColums.Rows[i]["ShowTotal"].ToString().ToLower() == "true")
//                    {

//                        //drFooter[_dtRecordColums.Rows[i]["NameOnExport"].ToString()] = dt.Compute("SUM([" + _dtRecordColums.Rows[i]["NameOnExport"].ToString() + "])", "[" + _dtRecordColums.Rows[i]["NameOnExport"].ToString() + "]<>''");
//                        drFooter[_dtRecordColums.Rows[i]["NameOnExport"].ToString()] = CalculateTotalForAColumn(dt, dt.Columns[j].ColumnName, bool.Parse(_dtRecordColums.Rows[i]["IgnoreSymbols"].ToString().ToLower()));

//                    }
//                }

//            }

//        }


//        for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
//        {
//            for (int j = dt.Columns.Count - 1; j >= 0; j--)
//            {
//                if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
//                {
//                    if (_dtRecordColums.Rows[i]["OnlyForAdmin"].ToString().ToLower() == "1")
//                    {
//                        if (!Common.HaveAccess(_strRecordRightID, "1,2"))
//                        {
//                            dt.Columns.RemoveAt(j);
//                        }
//                    }


//                }

//            }

//        }
//        dt.Rows.Add(drFooter);

//        //Round export

//        foreach (DataRow dr in dt.Rows)
//        {
//            for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
//            {
//                for (int j = 0; j < dt.Columns.Count; j++)
//                {
//                    //DisplayTextSummary
//                    if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
//                    {
//                        if (IsStandard(_dtRecordColums.Rows[i]["SystemName"].ToString()) == false)
//                        {

//                            if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "file"
//                                || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "image")
//                            {
//                                if (dr[j].ToString() != "" && dr[j].ToString() != "&nbsp;")
//                                {
//                                    try
//                                    {
//                                        if (dr[j].ToString().Length > 37)
//                                        {
//                                            dr[j] = dr[j].ToString().Substring(37);

//                                        }
//                                    }
//                                    catch
//                                    {
//                                        //
//                                    }
//                                }

//                            }

//                            if (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "value_text"
//                                  && (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
//                                 || _dtRecordColums.Rows[i]["ColumnType"].ToString() == "radiobutton")
//                                 && _dtRecordColums.Rows[i]["DropdownValues"].ToString() != "")
//                            {
//                                if (_dtRecordColums.Rows[i]["Heading"].ToString() == dt.Columns[j].ColumnName)
//                                {

//                                    if (dr[j].ToString() != "")
//                                    {
//                                        string strText = GetTextFromValueForDD(_dtRecordColums.Rows[i]["DropdownValues"].ToString(), dr[j].ToString());
//                                        if (strText != "")
//                                            dr[j] = strText;
//                                    }
//                                }

//                            }

//                            if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "time")
//                            {
//                                if (_dtRecordColums.Rows[i]["Heading"].ToString() == dt.Columns[j].ColumnName)
//                                {

//                                    if (dr[j].ToString() != "" )
//                                    {

//                                        TimeSpan ts = TimeSpan.Parse(dr[j].ToString());
//                                        dr[j] = ts.ToString(@"hh\:mm");
//                                    }
//                                }

//                            }


//                            if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
//                                && (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
//                                || _dtRecordColums.Rows[i]["DropDownType"].ToString() == "tabledd")
//                                 && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
//                                && _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
//                            {
//                                if (_dtRecordColums.Rows[i]["Heading"].ToString() == dt.Columns[j].ColumnName)
//                                {

//                                    if (dr[j].ToString() != "" && dr[j].ToString() != "&nbsp;")
//                                    {
//                                        try
//                                        {
//                                            Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["LinkedParentColumnID"].ToString()));

//                                            //int iTableRecordID = int.Parse(dr[j].ToString());
//                                            string strLinkedColumnValue = dr[j].ToString();
//                                            DataTable dtTableTableSC = Common.DataTableFromText("SELECT SystemName,DisplayName FROM [Column] WHERE   TableID ="
//                                             + _dtRecordColums.Rows[i]["TableTableID"].ToString());

//                                            string strDisplayColumn = _dtRecordColums.Rows[i]["DisplayColumn"].ToString();

//                                            foreach (DataRow dr2 in dtTableTableSC.Rows)
//                                            {
//                                                strDisplayColumn = strDisplayColumn.Replace("[" + dr2["DisplayName"].ToString() + "]", "[" + dr2["SystemName"].ToString() + "]");

//                                            }
//                                            string sstrDisplayColumnOrg = strDisplayColumn;
//                                            string strFilterSQL = "";
//                                            if (theLinkedColumn.SystemName.ToLower() == "recordid")
//                                            {
//                                                strFilterSQL = strLinkedColumnValue;
//                                            }
//                                            else
//                                            {
//                                                strFilterSQL = "'" + strLinkedColumnValue.Replace("'", "''") + "'";
//                                            }

//                                            //DataTable dtTheRecord = Common.DataTableFromText("SELECT * FROM Record WHERE RecordID=" + iTableRecordID.ToString());
//                                            DataTable dtTheRecord = Common.DataTableFromText("SELECT * FROM Record WHERE TableID=" + theLinkedColumn.TableID.ToString() + " AND " + theLinkedColumn.SystemName + "=" + strFilterSQL);

//                                            if (dtTheRecord.Rows.Count > 0)
//                                            {
//                                                foreach (DataColumn dc in dtTheRecord.Columns)
//                                                {
//                                                    strDisplayColumn = strDisplayColumn.Replace("[" + dc.ColumnName + "]", dtTheRecord.Rows[0][dc.ColumnName].ToString());
//                                                }
//                                            }
//                                            if (sstrDisplayColumnOrg != strDisplayColumn)
//                                                dr[j] = strDisplayColumn;
//                                        }
//                                        catch
//                                        {
//                                            //
//                                        }


//                                    }
//                                }

//                            }



//                            if (_dtRecordColums.Rows[i]["IsRound"] != DBNull.Value)
//                            {
//                                if (_dtRecordColums.Rows[i]["IsRound"].ToString().ToLower() == "true")
//                                {
//                                    if (dr[j].ToString() != "")
//                                    {
//                                        try
//                                        {
//                                            dr[j] = Math.Round(double.Parse(Common.IgnoreSymbols( dr[j].ToString())), int.Parse(_dtRecordColums.Rows[i]["RoundNumber"].ToString())).ToString();
//                                        }
//                                        catch
//                                        {
//                                            //
//                                        }
//                                    }
//                                }

//                            }
//                        }

//                    }

//                    //mm:hh
//                    if (_dtRecordColums.Rows[i]["SystemName"].ToString().ToLower() == "datetimerecorded")
//                    {

//                        if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "datetime")
//                        {
//                            if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
//                            {
//                                if (dr[j].ToString().Length > 15)
//                                {
//                                    dr[j] = dr[j].ToString().Substring(0, 16);
//                                }
//                            }
//                        }

//                        if (_dtRecordColums.Rows[i]["ColumnType"].ToString() == "date")
//                        {
//                            if (_dtRecordColums.Rows[i]["NameOnExport"].ToString() == dt.Columns[j].ColumnName)
//                            {
//                                if (dr[j].ToString().Length > 9)
//                                {
//                                    dr[j] = dr[j].ToString().Substring(0, 10);
//                                }
//                            }
//                        }


//                    }

//                }
//            }
//        }

//        dt.Columns.RemoveAt(dt.Columns.Count - 1);
//        dt.Columns.RemoveAt(dt.Columns.Count - 1);

//        dt.AcceptChanges();

//        DBG.Common.ExportUtil.ExportToExcel(dt, "\"" + lblTitle.Text.Replace("Records - ", "") + " " + DateTime.Today.ToString("yyyyMMdd") + ".xls" + "\"");


//    }
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SpecialNotificationUsers.ascx.cs"
        Inherits="Pages_UserControl_SpecialNotificationUsers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">--%>
    <script type="text/javascript">



        $(document).ready(function () {

            function ShowHideDropDownUserNotif() {
                var sUserWiz = $('#ddlUserWiz').val();
                $('#tdEmailFromRecordWiz').hide();
                $('#tdSpecificUserWiz').hide();

                if (sUserWiz == 'SU') {
                    $('#tdSpecificUserWiz').show();
                }
                if (sUserWiz == 'EOCR') {
                    $('#tdEmailFromRecordWiz').show();
                }
            }



            ShowHideDropDownUserNotif();
            $('#ddlUserWiz').on('change', function () {

                ShowHideDropDownUserNotif();
            });
        });

        jQuery(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(
                function () {
                    function ShowHideDropDownUserNotif() {
                        var sUserWiz = $('#ddlUserWiz').val();
                        $('#tdEmailFromRecordWiz').hide();
                        $('#tdSpecificUserWiz').hide();

                        if (sUserWiz == 'SU') {
                            $('#tdSpecificUserWiz').show();
                        }
                        if (sUserWiz == 'EOCR') {
                            $('#tdEmailFromRecordWiz').show();
                        }
                    }

                    ShowHideDropDownUserNotif();
                    $('#ddlUserWiz').on('change', function () {

                        ShowHideDropDownUserNotif();
                    });
                }
            )
        });


        //$(document).ready(function () {

        //    function ShowHideDropDown() { 
        //        var sUser = $('#ddlUser').val();
        //        alert(sUser);

        //        $('#tdReminderColumnTwo').hide();
        //        $('#tdEmailFromRecord').hide();
        //        $('#tdSpecificUser').hide();

        //        if (sUser == 'SU') {

        //            $('#tdSpecificUser').show();

        //        }
        //        if (sUser == 'EOCR') {
        //            $('#tdEmailFromRecord').show();

        //        };

        //    }

        //      ShowHideDropDown();
        //    $('#ddlUser').change(function () {
                
        //        ShowHideDropDown();   
                
        //    });


        //});

        //jQuery(document).ready(function () {
        //    Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded()

        //    function ShowHideDropDown() {
        //        var sUser = $('#ddlUser').val();

        //        $('#tdReminderColumnTwo').hide();
        //        $('#tdEmailFromRecord').hide();
        //        $('#tdSpecificUser').hide();

        //        if (sUser == 'SU') {

        //            $('#tdSpecificUser').show();

        //        }
        //        if (sUser == 'EOCR') {
        //            $('#tdEmailFromRecord').show();

        //        };

        //    }

        //    ShowHideDropDown();
        //    $('#ddlUser').change(function () {

        //        ShowHideDropDown();

        //    });

        //});

          
                
       

    </script>
    
   <table border="0" cellpadding="0" cellspacing="0" align="center" style="padding-top:30px;">
        <tr>
            <td colspan="2" style="padding-top:20px">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" style="padding-bottom:25px">
                            <span class="TopTitle">
                                <asp:Label runat="server" ID="lblTitle" Text="Add Recipient"></asp:Label></span>
                        </td>
                     
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
           
            <td colspan="2" style="vertical-align:top">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <%--<asp:Panel ID="Panel2" runat="server" >--%>
                            <div runat="server" id="divDetail">
                                <table cellpadding="3">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="grdUsersWiz" runat="server" AutoGenerateColumns="False" DataKeyNames="RecipientID"
                                                            HeaderStyle-HorizontalAlign="Center" RowStyle-HorizontalAlign="Center" 
                                                            CssClass="gridviewborder" OnRowDataBound="grdUsers_RowDataBound"  
                                                            OnRowCommand="grdUsers_RowCommand"  BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" ShowHeaderWhenEmpty="true">
                                                            <Columns>
                                                                <asp:TemplateField Visible="false">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                        <asp:Label runat="server" ID="lblID" Text='<%# Eval("RecipientID") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemStyle Width="10px" HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgbtnDelete" runat="server" ImageUrl="~/App_Themes/Default/Images/delete.png"
                                                                            CommandName="deletetype" CommandArgument='<%# Eval("RecipientID") %>'  />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Recipient">
                                                                    <ItemTemplate>
                                                                        <div style="padding:2px 5px 2px 5px; text-align:left; min-width:200px">
                                                                            <asp:Label runat="server" ID="lblUsers" Text='<%# Eval("UserName") %>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:TemplateField HeaderText="Userid" Visible="false">
                                                                    <ItemTemplate>
                                                                        <div style="padding-left: 10px;">
                                                                            <asp:Label runat="server" ID="lblUserID"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="Send To" >
                                                                    <ItemTemplate>
                                                                        <div style="padding:2px 5px 2px 5px; text-align:left; min-width:200px">
                                                                            <asp:Label runat="server" ID="lblSendTo"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField Visible="false">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                        <asp:Label runat="server" ID="lblRecipientOption" Text='<%# Eval("RecipientOption") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                 </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                --==========================** No Recipients **==========================--
                                                            </EmptyDataTemplate>
                                                             <HeaderStyle CssClass="gridviewborder_header" />
                                                            <RowStyle CssClass="gridviewborder_row" />
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr >
                                                                <td style="padding-top:30px;" >
                                                                    <asp:DropDownList ID="ddlUserWiz" runat="server"
                                                                        CssClass="NormalTextBox" ClientIDMode="Static">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td runat="server" id="tdSpecificUserWiz" clientidmode="Static"  style="padding-top:30px;" >
                                                                    <asp:DropDownList ID="ddlSpecificUserWiz" runat="server" ClientIDMode="Static" DataTextField="UserName" DataValueField="UserID"
                                                                        CssClass="NormalTextBox">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                 <td runat="server" id="tdEmailFromRecordWiz" clientidmode="Static"  style="padding-top:30px;" >
                                                                    <asp:DropDownList ID="ddlEmailFromRecordWiz" runat="server" ClientIDMode="Static" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <%-- <td runat="server" id="tdReminderColumnTwoWiz" clientidmode="Static" >
                                                                    <asp:DropDownList ID="ddlReminderColumnTwoWiz" runat="server" ClientIDMode="Static" CssClass="NormalTextBox" DataTextField="DisplayName" DataValueField="ColumnID">
                                                                    </asp:DropDownList>
                                                                </td>--%>
                                                                <td style="padding-left:20px; padding-top:30px;" >
                                                                    <div runat="server" id="div2">
                                                                        <asp:LinkButton runat="server" ID="lnkAddUserWiz" CssClass="btn" CausesValidation="false"
                                                                            OnClick="lnkAddUser_Click"> <strong>Add</strong>   </asp:LinkButton>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <br />
                            <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>
                            <br />
                        <%--</asp:Panel>--%>
                     </ContentTemplate>
                     <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="grdUsersWiz"/>
                    </Triggers>
                </asp:UpdatePanel>
            </td>
           
        </tr>
        
    </table>
    <%--<script type="text/javascript">
        var oEditor = null;
        function InsertMergeField() {
            oUtil.obj.insertHTML("[" + document.getElementById("ctl00_HomeContentPlaceHolder_ddlDatabaseField").value + "]")
        }
    </script>--%>
<%--</asp:Content>--%>

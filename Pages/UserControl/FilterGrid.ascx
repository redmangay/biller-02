﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FilterGrid.ascx.cs" Inherits="Pages_UserControl_FilterGrid" %>
<%@ Register Src="~/Pages/UserControl/ControlByColumn.ascx" TagName="ControlByColumn" TagPrefix="dbg" %>

 <script type="text/javascript">
            function toggleAndOr(t, hf) {
                if (t.text == 'and') {
                    t.text = 'or';
                } else {
                    t.text = 'and';
                }
                document.getElementById(hf).value = t.text;
            }
        </script>
        <asp:UpdatePanel runat="server" ID="upFilter" UpdateMode="Always">
            <ContentTemplate>
                <div>
                    <table>
                        <tr>
                            <td>
                                <asp:GridView ID="grdFilter" runat="server" AutoGenerateColumns="False" DataKeyNames="ID"
                                    CssClass="gridview" OnRowCommand="grdFilter_RowCommand" OnRowDataBound="grdFilter_RowDataBound"
                                    ShowHeaderWhenEmpty="true"
                                    ShowFooter="true">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblID" Text='<%# Eval("ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemStyle Width="10px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:HiddenField runat="server" ID="hfAndOr" Value="" />
                                                <asp:LinkButton runat="server" ID="lnkAndOr">and</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                                <strong>Field and Value</strong>
                                            </HeaderTemplate>

                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <dbg:ControlByColumn runat="server" ID="cbcFilter" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <HeaderTemplate>
                                               <asp:Label runat="server" ID="lblCustom1" Visible="false"></asp:Label>
                                            </HeaderTemplate>

                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:DropDownList runat="server" ID="ddlCustom1" Visible="false"></asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField>
                                            <ItemStyle Width="10px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtndelete" runat="server" ImageUrl="~/App_Themes/Default/Images/DeleteItem.png"
                                                    CommandName="delete_row"  CommandArgument='<%# Eval("ID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle CssClass="gridview_header" Height="25px" />
                                    <RowStyle CssClass="gridview_row_NoPadding" />
                                </asp:GridView>


                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:LinkButton runat="server" ID="lnkAddItem" OnClick="lnkAddItem_Click">
                                    <asp:Image runat="server" AlternateText="add item" ImageUrl="~/App_Themes/Default/Images/AddItem.png" />Add Item
                                </asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblMsgTab" runat="server" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                    </table>


                </div>
            </ContentTemplate>
        </asp:UpdatePanel>


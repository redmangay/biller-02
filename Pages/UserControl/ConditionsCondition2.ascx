﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConditionsCondition2.ascx.cs"
    Inherits="Pages_UserControl_ConditionsCondition2" %>
<%@ Register Src="~/Pages/UserControl/ColumnUI.ascx" TagName="cUI" TagPrefix="dbg" %>
<div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hfConditionsID" Value='<%# Eval("ID") %>' />
            <span class="condition-when-column condition-row">
                <asp:DropDownList runat="server" ID="ddlJoinOperator" CssClass="NormalTextBox" ClientIDMode="AutoID"
                                  AutoPostBack="True" OnSelectedIndexChanged="ddlJoinOperator_OnSelectedIndexChanged">
                    <asp:ListItem Text="" Value=""></asp:ListItem>
                    <asp:ListItem Text="AND" Value="and"></asp:ListItem>
                    <asp:ListItem Text="OR" Value="or"></asp:ListItem>
                </asp:DropDownList>
            </span>
            <span id="spanDDLTable" runat="server" visible="false" class="condition-table-column condition-row">
                <asp:DropDownList runat="server" ID="ddlTable" CssClass="NormalTextBox" ClientIDMode="AutoID"
                                DataValueField="TableID" DataTextField="TableName" AutoPostBack="true" Visible="false"
                                OnSelectedIndexChanged="ddlTable_SelectedIndexChanged">
                            </asp:DropDownList>
            </span>
                   
            <span class="condition-field-column condition-row">
        
                <asp:DropDownList runat="server" ID="ddlHideColumn" CssClass="NormalTextBox" ClientIDMode="AutoID"
                    AutoPostBack="true"
                    OnSelectedIndexChanged="ddlHideColumn_SelectedIndexChanged">
                </asp:DropDownList>
                <%--<asp:TextBox ID="txtHideColumn" runat="server" CssClass="NormalTextbox" Width="190px" ClientIDMode="AutoID" AutoPostBack="true" />
                <asp:Image id="imguploadbatchpopulated" runat="server" title="start typing and matching values will be shown" src="../../app_themes/default/images/dropdown.png" style="border-width:0px;display:inline" />
                <asp:Image id="uploadbatchpopulating" runat="server" title="start typing and matching values will be shown" src="../../images/ajax.gif" style="border-width:0px;width:15px;height:15px;display:none;"/>
                <asp:HiddenField ID="hiddenColumn" runat="server" ClientIDMode="AutoID" />--%>
                <%--<ajaxToolkit:AutoCompleteExtender ID="autocompleteUploadedBatch" ServiceMethod="GetBatchUploadValues" ServicePath="~/CascadeDropdown.asmx"
                                                  CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtUploadedBatch" 
                                                  runat="server" FirstRowSelected="false" MinimumPrefixLength="1" OnClientItemSelected="GetUploadedBatchID" ShowOnlyCurrentWordInCompletionListItem="true" OnClientPopulating="ChangeIconPopulating" 
                                                  OnClientPopulated="ChangeIconPopulated" OnClientHiding="ChangeIconPopulated"/>--%>

                <asp:RequiredFieldValidator ID="revddlHideColumn" ControlToValidate="ddlHideColumn" 
                                            Display="Dynamic" ErrorMessage="Required!" runat="server" ForeColor="Red" >
                </asp:RequiredFieldValidator>
            </span>
            <span class="condition-operator-column condition-row">
                <asp:DropDownList runat="server" ID="ddlOperator" AutoPostBack="true" OnSelectedIndexChanged="ddlOperator_SelectedIndexChanged" 
                    CssClass="NormalTextBox" ClientIDMode="AutoID">
                    <asp:ListItem Value="equals" Text="Equals" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="notequal" Text="Not Equals"></asp:ListItem>
                    <asp:ListItem Value="greaterthan" Text="Greater Than"></asp:ListItem>
                    <asp:ListItem Value="greaterthanequal" Text="Greater or Equal to"></asp:ListItem>
                    <asp:ListItem Value="lessthan" Text="Less Than"></asp:ListItem>
                    <asp:ListItem Value="lessthanequal" Text="Less or Equal to"></asp:ListItem>
                    <asp:ListItem Value="contains" Text="Contains"></asp:ListItem>
                    <asp:ListItem Value="notcontains" Text="Does Not Contain"></asp:ListItem>
                        <asp:ListItem  Value="empty" Text="Is Empty"></asp:ListItem>  
                    <asp:ListItem Value="notempty" Text="Is Not Empty" ></asp:ListItem> 
                </asp:DropDownList>
            </span>
           <%-- <span id="spacerValue" runat="server" style="height:30px;" class="condition-value-column condition-row">
                 <dbg:cUI runat="server" ID="cuiDummy" />
            </span>--%>
            <span id="spacerValue" runat="server" style="height:30px;" class="condition-value-column condition-row">
                <dbg:cUI runat="server" ID="cuiConditionValue" />
                <asp:HiddenField runat="server" ID="hfConditionValue" Value="" ClientIDMode="AutoID" />
            </span>
            <span class="condition-remove-column condition-row">
                <asp:ImageButton runat="server" ID="btnRemove" ImageUrl="~/App_Themes/Default/Images/delete_s.png"
                                 OnClick="btnRemove_OnClick" CausesValidation="False" />
            </span>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

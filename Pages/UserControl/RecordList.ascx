﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecordList.ascx.cs" Inherits="Pages_UserControl_RecordList" %>
<%@ Register Namespace="DBGServerControl" Assembly="DBGServerControl" TagPrefix="dbg" %>
<%@ Register Src="~/Pages/Pager/Pager.ascx" TagName="GridViewPager" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Pages/UserControl/ControlByColumn.ascx" TagName="ControlByColumn"
    TagPrefix="dbg" %>
<%@ Register Src="~/Pages/UserControl/ViewDetail.ascx" TagName="OneView" TagPrefix="dbg" %>
<asp:ScriptManagerProxy runat="server" ID="ScriptManagerProxy1"></asp:ScriptManagerProxy>
<asp:Literal ID="ltScriptHere" runat="server"></asp:Literal>
<asp:Literal ID="ltTextStyles" runat="server"></asp:Literal>
<style type="text/css">
    .headerlink a {
        text-decoration: none;
    }

    tr.gridview_footer > td:last-child {
        display: none;
    }
    .ms-choice
    {
        height:20px;
        line-height: 18px;
        font-size: inherit;
    }
</style>


<asp:UpdatePanel ID="upMain" runat="server">
    <ContentTemplate>


        <asp:Panel runat="server" ID="pnlFullListControl">



            <div runat="server" id="divRecordListTop">
                <table style="width: 100%; border-collapse: collapse; border-spacing: 0;">
                    <tr>
                        <td colspan="3" align="left">
                            <table runat="server" id="tblTopCaption" width="100%" class="top-caption">
                                <%-- 1000px--%>
                                <tr runat="server" id="trRecordListTitle">
                                    <td align="left" runat="server" id="tdTopTitile">
                                        <span class="TopTitle">
                                            <asp:Label ID="lblTitle" runat="server" Text="Records" Visible="false"> </asp:Label>
                                            <%--<asp:Label ID="lblRecords" runat="server" Text="Records:" Visible="true"> </asp:Label>--%>
                                            <asp:HiddenField runat="server" ID="hfViewID" />
                                             <%-- Red 28112018 --%>
                                            <asp:HiddenField runat="server" ID="hfParentRecordIDList" ClientIDMode="Static" />
                                             <%-- =End= --%>
                                            <asp:HiddenField runat="server" ID="hfHidePagerGoButton" />
                                            <asp:HiddenField runat="server" ID="hfRecordCount" Value="0" />
                                        </span>
                                        <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlTableMenu" CssClass="TopTitle"
                                            DataValueField="TableID" DataTextField="TableName" OnSelectedIndexChanged="ddlTableMenu_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td></td>
                                    <td runat="server" id="tdTopButtons">
                                        <table style="width: 100%; text-align: right; padding-right: 42px;" class="ListConfigControl">
                                            <tr>

                                                <td></td>
                                                <td style="width: 50px;">
                                                    <div runat="server" id="divShowGraph" visible="true">
                                                        <asp:HyperLink runat="server" ID="hlShowGraph" CssClass="ButtonLink">
                                                            <asp:Image runat="server" ID="imgShowGraph" ImageUrl="~/App_Themes/Default/images/Graph.png"
                                                                ToolTip="Graph" />
                                                        </asp:HyperLink>
                                                    </div>
                                                </td>
                                                <td style="width: 50px;">
                                                    <div runat="server" id="divUpload" visible="true">
                                                        <asp:HyperLink runat="server" ID="hlUpload" CssClass="ButtonLink">
                                                            <asp:Image runat="server" ID="imgUpload" ImageUrl="~/App_Themes/Default/images/Upload.png"
                                                                ToolTip="Upload" />
                                                        </asp:HyperLink>
                                                    </div>
                                                </td>
                                                <td style="width: 50px;" runat="server" id="divEmail">
                                                    <div>
                                                        <asp:ImageButton ID="ibEmail" runat="server" ImageUrl="~/App_Themes/Default/images/email.png"
                                                            ToolTip="Email" OnClick="ibEmail_Click" />
                                                    </div>
                                                </td>
                                                <td style="width: 50px;" runat="server" id="divAddTable" visible="false" >
                                                    <div >
                                                          <asp:HyperLink runat="server" ID="hlAddTableSave" class="popupaddtable" ClientIDMode="Static">
                                                            <asp:Image runat="server" ImageUrl="~/App_Themes/Default/images/table_export.png" ToolTip="Add Table" />
                                                        </asp:HyperLink>
                                                         <%--<asp:ImageButton runat="server" ID="ibAddGraph" ImageUrl="~/App_Themes/Default/images/graphsave.png"
                                                            OnClick="ibAddGraph_Click" ToolTip="Add Graph" CausesValidation="true" ValidationGroup="main" />--%>
                                                    </div>
                                                </td>
                                                <td style="width: 50px;" class="not-mobile">
                                                    <div runat="server" id="divConfig" visible="false">
                                                        <asp:HyperLink runat="server" ID="hlConfig" CssClass="ButtonLink">
                                                            <asp:Image runat="server" ID="imgConfig" ImageUrl="~/App_Themes/Default/images/Config.png"
                                                                ToolTip="Configure" />
                                                        </asp:HyperLink>
                                                    </div>
                                                </td>
                                                <td runat="server" id="divBatches" visible="false">
                                                    <div>
                                                        <asp:HyperLink runat="server" ID="hlBatches" CssClass="btn"> <strong>My Batches</strong> </asp:HyperLink>
                                                    </div>
                                                </td>

                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="padding: 5px;">
                <table border="0" cellpadding="0" cellspacing="0" align="left" style="width: 100%;">
                    <%-- 1000px--%>
                    <tr>
                        <td colspan="3" height="13"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Panel runat="server" ID="pnlListTop">
                            </asp:Panel>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:Panel runat="server" ID="pnlListLeft">
                            </asp:Panel>

                        </td>
                        <td valign="top">

                            <asp:Panel ID="pnlSearch" runat="server" DefaultButton="lnkSearch" Style="width: 100%">
                                <div runat="server" id="divSearch" onclick="abc();" class="searchcorner">
                                    <div style="max-width: 1200px; float: left;">
                                        <table cellpadding="0" cellspacing="0" width="100%" class="search-container">
                                            <tr runat="server" id="trFiletrTop">
                                                <td>
                                                    <table runat="server" id="tblFilterByColumn" visible="false">
                                                        <tr runat="server" id="trSummaryFilter">
                                                            <td align="right">
                                                                <asp:Label runat="server" ID="lblFilterColumnName" Font-Bold="true"></asp:Label>
                                                                <asp:HiddenField runat="server" ID="hfFilterColumnSystemName" />
                                                            </td>
                                                            <td>
                                                                <%--<asp:DropDownList runat="server" ID="ddlFilterValue" AutoPostBack="true" CssClass="NormalTextBox"
                                                                OnSelectedIndexChanged="ddlFilterValue_SelectedIndexChanged">
                                                            </asp:DropDownList>--%>
                                                                <%--<dbg:cbcValue runat="server" ID="cbcvSumFilter" OnddlYAxis_Changed="cbcvSumFilter_OnddlYAxis_Changed" />--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="text-align:left">
                                                     <div runat="server" id="tdFilterYAxis" style="padding-top: 12px;">
                                                        <table>
                                                            <tr runat="server" id="trSearchMain" style="display:none;">
                                                                            <td style="width:20px"></td>
                                                                
                                                                <td align="right" style="padding-top: 8px;">
                                                                    <strong runat="server" id="stgFilter">Filter:</strong>
                                                                </td>
                                                                <td>
                                                                     
                                                                  <%--  <table>
                                                                        <td>--%>
                                                                            <dbg:controlbycolumn runat="server" id="cbcSearchMain" onddlyaxis_changed="cbcSearchMain_OnddlYAxis_Changed"
                                                                                onddlcompareoperator_changed="cbcSearchMain_OnddlCompareOperator_Changed" />
                                                                        </td>
                                                               
                                                                        <td></td>
                                                                           
                                                                      <%--  </td>
                                                                    </table>--%>
                                                                
                                                            </tr>
                                                             <tr runat="server" id="trSearch1a" visible="false">
                                                                   <td></td>
                                                                   <td></td>
                                                                <td style="text-align:left; padding-left:5px">
                                                                     <asp:LinkButton runat="server" ID="lnkAddSearch1" Visible="false" OnClick="lnkSearch_Click">
                                                                        <asp:Image  runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png"/> Add filter
                                                                            </asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="trSearch1" style="display: none;">
                                                                    <td style="width:20px"></td>
                                                                <td align="right">
                                                                    <asp:HiddenField runat="server" ID="hfAndOr1" Value="" />
                                                                    <%--<asp:LinkButton runat="server" ID="lnkAndOr1" Text="and"></asp:LinkButton>--%>
                                                                      <asp:DropDownList runat="server" ID="ddlJoinOperator1" CssClass="NormalTextBox">
                                                                                            <asp:ListItem Text="AND" Value="and"></asp:ListItem>
                                                                                            <asp:ListItem Text="OR" Value="or"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                    
                                                                </td>
                                                                <td>
                                                                   <%-- <table>
                                                                        <tr>
                                                                            <td>--%>
                                                                                <dbg:ControlByColumn ID="cbcSearch1" runat="server" onddlcompareoperator_changed="cbcSearch1_OnddlCompareOperator_Changed" onddlyaxis_changed="cbcSearch1_OnddlYAxis_Changed" />
                                                                          <%--  </td>--%>
                                                                            <%--<td>--%>
                                                                               
<%--                                                                            </td>
                                                                        </tr>
                                                                    </table>--%>
                                                                </td>
                                                                   
                                                               <td style="text-align:center; padding-left:10px; padding-right:10px">
                                                                    <asp:LinkButton title="Remove this filter"  runat="server" ID="lnkMinusSearch1">
                                                                        <asp:Image ID="Image4" runat="server" ImageUrl="~/App_Themes/Default/Images/delete-small-icon.png" />
                                                                    </asp:LinkButton>
                                                                </td>
                                                                 <td style="width:20px"></td>
                                                            </tr>
                                                             <tr runat="server" id="trSearch2a" visible="false">
                                                                <td></td>
                                                                <td></td>
                                                                <td style="text-align:left; padding-left:5px">
                                                                     <asp:LinkButton ID="lnkAddSearch2" runat="server" OnClick="lnkSearch_Click" Visible="false">
                                                                                <asp:Image ID="Image2" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png" />
                                                                         Add filter
                                                                            </asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="trSearch2" style="display: none;">
                                                                  <td style="width:20px"></td>
                                                                <td align="right">
                                                                    <asp:HiddenField runat="server" ID="hfAndOr2" Value="" />
                                                                    <%--<asp:LinkButton runat="server" ID="lnkAndOr2">and</asp:LinkButton>--%>
                                                                      <asp:DropDownList runat="server" ID="ddlJoinOperator2" CssClass="NormalTextBox">
                                                                                            <asp:ListItem Text="AND" Value="and"></asp:ListItem>
                                                                                            <asp:ListItem Text="OR" Value="or"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                    
                                                                </td>
                                                                <td>
                                                                   <%-- <table>
                                                                        <tr>
                                                                            <td>--%>
                                                                                <dbg:ControlByColumn ID="cbcSearch2" runat="server" onddlcompareoperator_changed="cbcSearch2_OnddlCompareOperator_Changed" onddlyaxis_changed="cbcSearch2_OnddlYAxis_Changed" />
                                                                          <%--  </td>
                                                                            <td>--%>
                                                                               
                                                                           <%-- </td>
                                                                        </tr>
                                                                    </table>--%>
                                                                </td>
                                                                  
                                                                <td style="text-align:center; padding-left:10px; padding-right:10px">
                                                                    <asp:LinkButton title="Remove this filter"  runat="server" ID="lnkMinusSearch2">
                                                                        <asp:Image ID="Image5" runat="server" ImageUrl="~/App_Themes/Default/Images/delete-small-icon.png" />
                                                                    </asp:LinkButton>
                                                                </td>
                                                                 <td style="width:20px"></td>
                                                            </tr>
                                                             <tr runat="server" id="trSearch3a" visible="false">
                                                                <td></td>
                                                                <td></td>
                                                               <td style="text-align:left; padding-left:5px">
                                                                     <asp:LinkButton ID="lnkAddSearch3" runat="server" OnClick="lnkSearch_Click" Visible="false">
                                                                                <asp:Image ID="Image3" runat="server" ImageUrl="~/App_Themes/Default/Images/PlusAdd.png" />
                                                                         Add filter
                                                                            </asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="trSearch3" style="display: none;">
                                                                 <td style="width:20px"></td>
                                                                <td align="right">
                                                                    <asp:HiddenField runat="server" ID="hfAndOr3" Value="" />
                                                                    <%--<asp:LinkButton runat="server" ID="lnkAndOr3">and</asp:LinkButton>--%>
                                                                      <asp:DropDownList runat="server" ID="ddlJoinOperator3" CssClass="NormalTextBox" ClientIDMode="AutoID">
                                                                                            <asp:ListItem Text="AND" Value="and"></asp:ListItem>
                                                                                            <asp:ListItem Text="OR" Value="or"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                    
                                                                </td>
                                                                <td>
                                                                    <%--<table>
                                                                        <tr>
                                                                            <td>--%>
                                                                                <dbg:controlbycolumn runat="server" id="cbcSearch3" onddlyaxis_changed="cbcSearch3_OnddlYAxis_Changed" onddlcompareoperator_changed="cbcSearch3_OnddlCompareOperator_Changed" />
                                                                          <%--  </td>
                                                                            <td></td>
                                                                        </tr>
                                                                    </table>--%>
                                                                </td>
                                                                
                                                                 <td style="text-align:center; padding-left:10px; padding-right:10px">
                                                                    <asp:LinkButton title="Remove this filter" runat="server" ID="lnkMinusSearch3">
                                                                        <asp:Image ID="Image6" runat="server" ImageUrl="~/App_Themes/Default/Images/delete-small-icon.png" />
                                                                    </asp:LinkButton>
                                                                </td>
                                                                 <td style="width:20px"></td>
                                                            </tr>

                                                        </table>
                                                        <asp:HiddenField runat="server" ID="hfTextSearch" Value="" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <div runat="server" id="tdFilterDynamic" class="dynamicFilters">
                                                          <%--Red Ticket 3611--%>
                                                      <%--  <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfSystemName" />--%>
                                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfSystemNamePrevsious" />
                                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfddlShowAll" />
                                                        <asp:HiddenField runat="server"  ID="hfControlB_DDL" />
                                                        <asp:HiddenField runat="server"  ID="hfDDLcbcSearch1" />
                                                        <asp:HiddenField runat="server"  ID="hfDDLcbcSearch2" />
                                                        <asp:HiddenField runat="server"  ID="hfDDLcbcSearch3" />
                                                        <table runat="server" id="tblSearchControls" visible="true" cellpadding="3" cellspacing="0">
                                                        </table>
                                                    </div>
                                                </td>
                                                <td class="buttons-options" style="padding-top: 10px;">
                                                    <div class="buttons" style="min-width: 120px;">
                                                        <asp:LinkButton runat="server" ID="lnkSearch" CssClass="btn" OnClick="lnkSearch_Click"> <strong>Go</strong></asp:LinkButton>
                                                        <asp:LinkButton runat="server" ID="lnkReset" CssClass="btn" OnClick="lnkReset_Click"
                                                            CausesValidation="false"> <strong>Reset</strong></asp:LinkButton>
                                                    </div>
                                                     <%--Red Ticket 3611--%>
                                                    <div style="display: none" >
                                                         <asp:LinkButton runat="server"  ID="lnkDDLPopulate" OnClick="lnkDDLPopulate_Click" CssClass="btn"> </asp:LinkButton>
                                                    </div>
                                                    <table id="tblAdvancedOptionChk" class="opts" runat="server">
                                                        <tr>
                                                            <td valign="top">
                                                                <table>
                                                                    <%-- Advanced Search Checkbox --%>
                                                                    <tr>
                                                                        <td style="text-align: right;">
                                                                            <asp:CheckBox runat="server" ID="chkShowAdvancedOptions" AutoPostBack="true" OnCheckedChanged="chkShowAdvancedOptions_OnCheckedChanged" />

                                                                        </td>
                                                                        <td style="text-align: left;">
                                                                            <asp:Label runat="server" ID="lblAdvancedCaption" Text="Advanced Search"></asp:Label>
                                                                            <asp:HiddenField runat="server" ID="hfHideAdvancedOption" />

                                                                        </td>
                                                                    </tr>
                                                                    <%-- Show Deleted Records Checkbox--%>
                                                                    <tr>
                                                                        <td style="text-align: right;">
                                                                            <asp:CheckBox ID="chkShowDeletedRecords" Checked="false" runat="server" AutoPostBack="true" OnCheckedChanged="chkIsActive_CheckedChanged" />
                                                                        </td>
                                                                        <td style="text-align: left;">Show Deleted
                                                                        </td>
                                                                    </tr>
                                                                    <%-- Show Warning Only checkbox --%>
                                                                    <tr id="trChkShowOnlyWarning" runat="server">
                                                                        <td style="text-align: right;">
                                                                            <asp:CheckBox ID="chkShowOnlyWarning" Checked="false" runat="server" AutoPostBack="true" OnCheckedChanged="chkShowOnlyWarning_CheckedChanged" /></td>
                                                                        <td style="text-align: left;">Show Only Warning</td>
                                                                    </tr>
                                                                    <%-- Use Archive Database 3358--%>
                                                                    <tr id="trUseArchive" runat="server" visible="false">
                                                                        <td style="text-align: right;">
                                                                            <asp:CheckBox ID="chkUseArchive" Checked="false" runat="server" AutoPostBack="true" OnCheckedChanged="chkUseArchive_CheckedChanged" /></td>
                                                                        <td style="text-align: left;">
                                                                            <asp:Label runat="server" ID="lblUseArchive" Text="Use Archive"></asp:Label>
                                                                            <asp:HiddenField runat="server" ID="hfuseArchiveData" Value="false" />
                                                                        </td>

                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr runat="server" id="trFilterBottom">
                                                <td colspan="4">
                                                    <table id="tblAdvancedOptionChkC" runat="server" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <div id="search" style="padding-bottom: 10px;" runat="server" visible="true">
                                                                    <table runat="server" id="tblAdvancedOption" style="border-collapse: collapse; display: none;"
                                                                        cellpadding="4">
                                                                        <tr id="trRecordGroup" runat="server" visible="false">
                                                                            <td colspan="3"></td>
                                                                            <td align="right">
                                                                                <strong>Record Group</strong>
                                                                            </td>
                                                                            <td colspan="2"></td>
                                                                        </tr>
                                                                        <tr id="Tr1" runat="server" visible="false">
                                                                            <td colspan="3"></td>
                                                                            <td align="right">
                                                                                <strong>Table</strong>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlTable" runat="server" AutoPostBack="true" DataTextField="TableName"
                                                                                    DataValueField="TableID" OnSelectedIndexChanged="ddlTable_SelectedIndexChanged"
                                                                                    CssClass="NormalTextBox">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="3"></td>
                                                                            <td align="right"></td>
                                                                             <td>
                                                                                <table>
                                                                                    <tr style="vertical-align: bottom;">
                                                                                        <td>
                                                                                            <table>
                                                                                                <tr style="vertical-align: bottom;">

                                                                                                    <td>
                                                                                                        <strong>Date Added:</strong>
                                                                                                        <br />
                                                                                                        <asp:TextBox runat="server" ID="txtDateFrom" Width="100px" CssClass="NormalTextBox"
                                                                                                            BorderWidth="1" BorderStyle="Solid" BorderColor="#909090" />

                                                                                                    </td>
                                                                                                    <td style="padding-bottom: 7px;">
                                                                                                        <br />
                                                                                                        <asp:ImageButton runat="server" ID="imgDateForm" ImageUrl="~/Images/Calendar.png"
                                                                                                            AlternateText="Click to show calendar" CausesValidation="false" />
                                                                                                        <ajaxtoolkit:calendarextender id="ce_txtDateFrom" runat="server" targetcontrolid="txtDateFrom"
                                                                                                            format="dd/MM/yyyy" popupbuttonid="imgDateForm" firstdayofweek="Monday">
                                                                                                        </ajaxtoolkit:calendarextender>
                                                                                                        <asp:RangeValidator ID="rngDateFrom" runat="server" ControlToValidate="txtDateFrom"
                                                                                                            ErrorMessage="*" Font-Bold="true" Display="Dynamic" Type="Date"
                                                                                                            MinimumValue="1/1/1753" MaximumValue="1/1/3000"></asp:RangeValidator>
                                                                                                        <ajaxtoolkit:textboxwatermarkextender id="TextBoxWatermarkExtender3" targetcontrolid="txtDateFrom"
                                                                                                            watermarktext="dd/mm/yyyy" runat="server" watermarkcssclass="MaskText">
                                                                                                        </ajaxtoolkit:textboxwatermarkextender>

                                                                                                    </td>
                                                                                                    <td style="padding-bottom: 5px; padding-left: 2px; padding-right: 2px;">
                                                                                                        <br />
                                                                                                        To

                                                                                                    </td>
                                                                                                    <td>
                                                                                                        <br />
                                                                                                        <asp:TextBox runat="server" ID="txtDateTo" Width="100px" CssClass="NormalTextBox"
                                                                                                            BorderWidth="1" BorderStyle="Solid" BorderColor="#909090" />

                                                                                                    </td>
                                                                                                    <td style="padding-bottom: 7px;">
                                                                                                        <br />
                                                                                                        <asp:ImageButton runat="server" ID="imgDateTo" ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar"
                                                                                                            CausesValidation="false" />
                                                                                                        <ajaxtoolkit:calendarextender id="ce_txtDateTo" runat="server" targetcontrolid="txtDateTo"
                                                                                                            format="dd/MM/yyyy" popupbuttonid="imgDateTo" firstdayofweek="Monday">
                                                                                                        </ajaxtoolkit:calendarextender>
                                                                                                        <asp:RangeValidator ID="rngDateTo" runat="server" ControlToValidate="txtDateTo"
                                                                                                            ErrorMessage="*" Font-Bold="true" Display="Dynamic" Type="Date" MinimumValue="1/1/1753"
                                                                                                            MaximumValue="1/1/3000"></asp:RangeValidator>
                                                                                                        <ajaxtoolkit:textboxwatermarkextender id="TextBoxWatermarkExtender4" targetcontrolid="txtDateTo"
                                                                                                            watermarktext="dd/mm/yyyy" runat="server" watermarkcssclass="MaskText">
                                                                                                        </ajaxtoolkit:textboxwatermarkextender>

                                                                                                    </td>
                                                                                                    <td style="width: 10px;"></td>
                                                                                        <td>
                                                                                            <strong>Entered By:</strong>
                                                                                            <br />                                                                                          
                                                                                            <asp:Panel runat="server" ID="divEnteredBy">
                                                                                                <asp:DropDownList ID="ddlEnteredBy" runat="server" AutoPostBack="true" DataTextField="FullName" Style="max-width: 200px;"
                                                                                                    DataValueField="UserID" CssClass="NormalTextBox" OnSelectedIndexChanged="ddlEnteredBy_SelectedIndexChanged">
                                                                                                </asp:DropDownList>
                                                                                            </asp:Panel>
                                                                                        </td>

                                                                                        <td style="width: 10px;"></td>
                                                                                        <td>
                                                                                            <strong>Upload Batch:</strong>
                                                                                            <br />
                                                                                            <asp:Panel runat="server" ID="divUploadedBatch">
                                                                                                <%-- RP Modified Ticket 4363 --%>                                                                                                
                                                                                                <asp:DropDownList ID="ddlUploadedBatch" runat="server" AutoPostBack="true" DataTextField="BatchDescription" Style="max-width: 200px;"
                                                                                                    DataValueField="BatchID" CssClass="NormalTextBox" OnSelectedIndexChanged="ddlUploadedBatch_SelectedIndexChanged" Visible="false">
                                                                                                    <asp:ListItem Text="All" Value="" />
                                                                                                </asp:DropDownList>
                                                                                                <%-- 
                                                                                                <ajaxToolkit:CascadingDropDown ID="cddUploadedBatch" runat="server" ServicePath="~/CascadeDropdown.asmx" ServiceMethod="GetBatchUploadValues"
                                                                                                    Category="BatchID" TargetControlID="ddlUploadedBatch" LoadingText="Loading..." />                                                                                                            
                                                                                                --%>
                                                                                                <asp:TextBox ID="txtUploadedBatch" ClientIDMode="Static" runat="server" AutoPostBack="true" CssClass="NormalTextBox" Width="300px" PlaceHolder="All" 
                                                                                                    OnTextChanged="txtUploadedBatch_TextChanged" />
                                                                                                <asp:HiddenField ID="hiddenUploadedBatchID" runat="server" ClientIDMode="Static" OnValueChanged="hiddenUploadedBatchID_ValueChanged" />
                                                                                                <ajaxToolkit:AutoCompleteExtender ID="autocompleteUploadedBatch" ServiceMethod="GetBatchUploadValues" ServicePath="~/CascadeDropdown.asmx"
                                                                                                    CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtUploadedBatch" 
                                                                                                    runat="server" FirstRowSelected="false" MinimumPrefixLength="1" OnClientItemSelected="GetUploadedBatchID" ShowOnlyCurrentWordInCompletionListItem="true" OnClientPopulating="ChangeIconPopulating" 
                                                                                                    OnClientPopulated="ChangeIconPopulated" OnClientHiding="ChangeIconPopulated"/>
                                                                                                <img id="uploadbatchpopulated" title="Start typing and matching values will be shown" src="../../App_Themes/Default/Images/dropdown.png" style="border-width:0px;display:inline">
                                                                                                <img id="uploadbatchpopulating" title="Start typing and matching values will be shown" src="../../Images/ajax.gif" style="border-width:0px;width:15px;height:15px;display:none;">
                                                                                                <!-- End Modification -->
                                                                                            </asp:Panel>
                                                                                        </td>


                                                                                        <td style="width: 10px;"></td>
                                                                                        <td>

                                                                                            <%-- Moved checboxes here --%>
                                                                                        </td>
                                                                                                </tr>

                                                                                            </table>





                                                                                        </td>
                                                                                        
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td></td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div style="clear: both"></div>
                                </div>
                            </asp:Panel>

                            <%-- <asp:Panel ID="gridPanel" runat="server" Style="max-height: 600px" Width="100%" ScrollBars="Auto">--%>
                            <%--<div style="overflow-y: auto; overflow-x: hidden;max-height:600px;">--%>
                            <%--   <div style="overflow: scroll;" onscroll="OnScrollDiv(this)" id="DivMainContent">--%>



                            <div id="DivRoot">
                                <div id="DivPagerRow">
                                </div>
                                <div style="overflow: hidden;" id="DivHeaderRow">
                                </div>
                                <div id="DivMainContent">
                                    <dbg:dbggridview id="gvTheGrid" runat="server" gridlines="Both" cssclass="gridview" showheaderwhenempty="true"
                                        headerstyle-horizontalalign="Center" rowstyle-horizontalalign="Center" allowpaging="True"
                                        allowsorting="True" datakeynames="<%# GetDataKeyNames() %>"
                                        width="100%" autogeneratecolumns="true" pagesize="15" onsorting="gvTheGrid_Sorting"
                                        onprerender="gvTheGrid_PreRender" onrowdatabound="gvTheGrid_RowDataBound" showfooter="true"
                                        ondatabound="gvTheGrid_DataBound" alternatingrowstyle-backcolor="#DCF2F0">
                                        <PagerSettings Position="Top" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemStyle Width="20px" HorizontalAlign="Center" />
                                                <HeaderStyle Width="20px" />
                                                <HeaderTemplate>
                                                    <%--<input id="chkAll" onclick="javascript:SelectAllCheckboxes(this);" runat="server"
                                                type="checkbox" />--%>
                                                    <asp:CheckBox ID="chkAll" Visible="false" runat="server" onclick="javascript:SelectAllCheckboxes(this);" />
                                                    <asp:HyperLink ID="hlAddRecordHeader" Visible="false"  runat="server" ToolTip="Add" ImageUrl="~/Pages/Pager/Images/add.png" />
                                                      <asp:LinkButton ID="DeleteLinkButtonHeader" runat="server" Visible="false" CausesValidation="false"
                                                            ToolTip="Delete" OnClick="DeleteLinkButtonHeader_Click">
                                                    <img alt="" id="ImgDelete" runat="server" src="~/Pages/Pager/Images/delete.png" style="border-width: 0" />
                                                </asp:LinkButton>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDelete" runat="server"  />
                                                    <asp:HiddenField ID="key" runat="server" Value='<%# Eval("DBGSystemRecordID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblID" runat="server" Text='<%# Eval("DBGSystemRecordID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField Visible="false">
                                                <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <HeaderStyle Width="30px" VerticalAlign="Middle" HorizontalAlign="Center" />

                                                <HeaderTemplate>
                                                    <asp:Label ID="lbl_" Visible="false"  runat="server" Width="30px"></asp:Label>
                                                    <asp:HyperLink ID="hlEditViewHeader" Visible="false"  runat="server" ToolTip="Edit" ImageUrl="~/Pages/Pager/Images/cog.png" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="EditHyperLink" runat="server" ToolTip="Edit" ImageUrl="~/App_Themes/Default/Images/iconEdit.png" />
                                                    <%--NavigateUrl='<%# GetEditURL() + Cryptography.Encrypt(Eval("DBGSystemRecordID").ToString()) %>'--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField SortExpression="DBGSystemRecordID" Visible="false">
                                                <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                <HeaderStyle Width="30px" />
                                                <HeaderTemplate>
                                                    <asp:Label runat="server" Width="30px"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="viewHyperLink" runat="server" ToolTip="View" ImageUrl="~/App_Themes/Default/Images/iconShow.png" />
                                                    <%--NavigateUrl='<%# GetViewURL() + Cryptography.Encrypt(Eval("DBGSystemRecordID").ToString())  %>'--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="gridview_header" />
                                        <%--gridview_header/HeaderFreez/FixedHeader--%>
                                        <RowStyle CssClass="gridview_row" />
                                        <AlternatingRowStyle CssClass="gridview_row" />
                                        <FooterStyle HorizontalAlign="Left" Font-Bold="true" CssClass="gridview_footer" />

                                        <PagerTemplate>

                                            <asp:GridViewPager runat="server" ID="Pager" TableName="Recordlist" OnExportForCSV="Pager_OnExportForCSV"
                                                OnExportForExcel="Pager_OnExportForExcel" OnEditManyAction="Pager_OnEditManyAction" HidePagerGoButton="false"
                                                HideSendEmail="true" HideFilter="true"  HidePagerSizeOnly="true" 
                                                OnDeleteAction="Pager_OnDeleteAction" OnUnDeleteAction="Pager_OnUnDeleteAction" 
                                                OnParmanenetDelAction="Pager_OnParmanenetDelAction" OnAllExport="Pager_OnExportAction"
                                                OnApplyFilter="Pager_OnApplyFilter" OnBindTheGridToExport="Pager_BindTheGridToExport"
                                                OnSendEmailAction="Pager_OnSendEmailAction" OnBindTheGridAgain="Pager_BindTheGridAgain" OnCopyRecordAction="Pager_CopyRecordAction"
                                                HideExcelExport="true" HideExport="true" HideAllExport="false" HideRefresh="true" HideGo="true"
                                                HideEditView="false" TableID="<%#TableID%>" />
                                        </PagerTemplate>
                                    </dbg:dbggridview>
                                </div>
                                <div id="DivFooterRow" style="overflow: hidden">
                                </div>
                                <div id="CustomDivFooterRow" runat="server" style="overflow: hidden">
                                </div>
                            </div>
                            
                            <a href="../Record/EditMany.aspx" id="openEditMany" class="popuplinkEM" style="visibility: hidden; display: none;">Edit Many</a>
                            <a href="../Record/DeleteRecord.aspx" id="openDeleteAction" class="popuplinkDEL" style="visibility: hidden; display: none;">Delete Action</a>
                            <a href="../Record/ExportRecord.aspx" id="openExportAction" class="popuplinkExp" style="visibility: hidden; display: none;">Export Action</a>
                            <asp:HyperLink runat="server" ID="hlEditView" Text="Edit View" Font-Bold="true" CssClass="popuplink2" Visible="false"> </asp:HyperLink>
                            <%--</asp:Panel>--%>

                            <%--<div runat="server" id="divEmptyData" visible="false" style="padding-left: 100px;">
                                    <asp:HyperLink runat="server" ID="hplNewData" Style="text-decoration: none; color: Black;">
                                <asp:Image runat="server" ID="imgAddNewRecord" ImageUrl="~/App_Themes/Default/images/BigAdd.png" />
                                No records have been added yet. <strong style="text-decoration: underline; color: Blue;">
                                    Add new record now.</strong> 
                                    </asp:HyperLink>
                                    &nbsp or  &nbsp   
                            <asp:HyperLink runat="server" ID="hlEditView2" Text="Edit View" Font-Bold="true" CssClass="popuplink2"> </asp:HyperLink>

                                </div>--%>
                            <%--<div runat="server" id="divNoFilter" visible="false" style="padding-left: 100px;">
                                    <table>
                                        <tr>
                                            <td>No records matched your search. &nbsp<asp:LinkButton runat="server" ID="lnkNoFilter"
                                                OnClick="Pager_OnApplyFilter" Font-Bold="true" Text="Clear Search"> </asp:LinkButton>

                                            </td>
                                            <td>
                                                <div runat="server" id="divAddAndViewControls">
                                                    <table style="width: 100%; border-collapse: collapse; border-spacing: 0;">
                                                        <tr>
                                                            <td>,
                                                            </td>
                                                            <td>
                                                                <asp:HyperLink runat="server" ID="hlEditView" Text="Edit View" Font-Bold="true" CssClass="popuplink2"> </asp:HyperLink>
                                                            </td>
                                                            <td>or
                                                            </td>
                                                            <td>
                                                                <asp:HyperLink runat="server" ID="hplNewDataFilter" Text="Add Record" Font-Bold="true"> </asp:HyperLink>
                                                            </td>
                                                            <td>
                                                                <asp:HyperLink runat="server" ID="hplNewDataFilter2">
                                <asp:Image runat="server" ID="Image1" ImageUrl="~/Pages/Pager/Images/add.png" />
                                                                </asp:HyperLink>
                                                            </td>
                                                        </tr>
                                                    </table>




                                                </div>

                                            </td>
                                        </tr>
                                    </table>

                                </div>--%>
                            <br />
                            <asp:Label runat="server" ID="lblMsg" ForeColor="Red"></asp:Label>



                            <asp:Button runat="server" ID="lnkDeleteAllOK" Style="display: none;" CausesValidation="false"
                                OnClick="lnkDeleteAllOK_Click"></asp:Button>

                            <asp:HiddenField runat="server" ID="hfParmanentDelete" Value="no" />
                            <asp:HiddenField runat="server" ID="hfchkUndo" Value="false" />
                            <asp:HiddenField runat="server" ID="hfchkDeleteParmanent" Value="false" />
                            <asp:HiddenField runat="server" ID="hfchkDelateAllEvery" Value="false" />
                            <asp:HiddenField runat="server" ID="hftxtDeleteReason" Value="no" />
                            <br />

                            <asp:Button runat="server" ID="lnkEditManyOK" Style="display: none;" CausesValidation="false"
                                OnClick="lnkEditManyOK_Click"></asp:Button>
                            <asp:HiddenField runat="server" ID="hfddlYAxisBulk" />
                            <asp:HiddenField runat="server" ID="hfBulkValue" />
                            <asp:HiddenField runat="server" ID="hfchkUpdateEveryItem" />

                            <asp:Button runat="server" ID="lnkExportRecords" OnClientClick="$('.ajax-indicator-full').fadeIn(); jsReportShowHideProgress('no')" CausesValidation="false" Style="display: none;"
                                OnClick="lnkExportRecords_Click"></asp:Button>

                            <asp:HiddenField runat="server" ID="hfUsingScrol" />
                            <asp:Button runat="server" ID="btnRefreshViewChange" CausesValidation="false" Style="display: none;"
                                OnClick="btnRefreshViewChange_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnSearch" CausesValidation="false" Style="display: none;"
                                OnClick="ddl_search"></asp:Button>


                        </td>
                        <td>
                            <asp:Panel runat="server" ID="pnlListRight">
                            </asp:Panel>

                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Panel runat="server" ID="pnlBottom">
                            </asp:Panel>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3" height="13">
                            <%--<asp:Button runat="server" ID="btnAdd" OnClick="btnAdd_Click" Width="1" Style="display: none;" CausesValidation="false" />--%>
                             <asp:Button runat="server" ID="btnAddGraph" ClientIDMode="Static" Style="display: none;" OnClick="btnAddGraph_Click" CausesValidation="false" />
                        </td>
                    </tr>
                </table>
            </div>
            <br />
        </asp:Panel>

    </ContentTemplate>
    <Triggers>
        <%--<asp:AsyncPostBackTrigger ControlID="gvTheGrid" />--%>
        <asp:PostBackTrigger ControlID="lnkExportRecords" />
         <%--<asp:PostBackTrigger ControlID="btnAddGraph" />--%>
    </Triggers>
</asp:UpdatePanel>






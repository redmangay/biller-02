﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
public partial class Pages_UserControl_ColumnLayout : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulateTables();
            if(Request.QueryString["TableID"]!=null)
            {
                string strTableID = Cryptography.Decrypt(Request.QueryString["TableID"].ToString());
                if (ddlTable.Items.FindByValue(strTableID) != null)
                {
                    ddlTable.SelectedValue = strTableID;
                    ddlTable.Enabled = false;
                }
            }
            int iTableID = -1;
            if (int.TryParse(ddlTable.SelectedValue, out iTableID))
            {
                Responsive.Column_Fix_NullCoordinate(iTableID);
            }
            PopulatePages();

            PopulateColumnsInCells();
        }
    }
    protected void PopulateTables()
    {
        ddlTable.DataSource = Common.DataTableFromText("SELECT T.TableID,T.TableName FROM [Table] T WHERE T.AccountID="
            + Session["AccountID"].ToString() + " AND T.IsActive=1 ORDER BY T.TableName");
        ddlTable.DataBind();
        //PopulatePages();
    }
    protected void PopulatePages()
    {
        ddlPage.Items.Clear();
        if (ddlTable.Items.Count > 0)
        {
            ddlPage.DataSource = Common.DataTableFromText(@"SELECT TT.TableTabID,TT.TabName 
            FROM [TableTab] TT JOIN [Table] T 
            ON TT.TableID=T.TableID
            WHERE T.TableID=" + ddlTable.SelectedValue + @" ORDER BY TT.DisplayOrder");
            ddlPage.DataBind();
        }

    }


    protected void ddlTable_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlNoOfColumns.Text = "1";
        int iTableID = -1;
        if (int.TryParse(ddlTable.SelectedValue, out iTableID))
        {
            Responsive.Column_Fix_NullCoordinate(iTableID);
        }

        PopulatePages();
        PopulateColumnsInCells();
    }
    protected void chkShowSystemFields_OnCheckedChanged(Object sender, EventArgs args)
    {
        PopulateColumnsInCells();
    }
    protected void PopulateColumnsInCells()
    {
        string strTableID = ddlTable.SelectedValue;
        //HtmlTable tblDetailTable = new HtmlTable();
        //tblDetailTable.Rows.Clear();
        //pnlHost.Controls.Add(tblDetailTable);
        //tblDetailTable.Attributes.Add("cellpadding", "3");
        //tblDetailTable.Attributes.Add("class", "detailtable");
        //tblDetailTable.ID = "tblDetailTable";

        //tblDetailTable.ViewStateMode = ViewStateMode.Disabled;

        string strPageFilter = "";
        string strFilter = " SystemName<>'TableID' AND SystemName<>'IsActive' AND TableID=" + strTableID + " ";
        if (ddlPage.SelectedIndex == 0)
        {
            strPageFilter = strPageFilter + " AND (TableTabID is null OR TableTabID=" + ddlPage.SelectedValue + ") ";
        }
        else if (ddlPage.SelectedIndex > 0)
        {
            strPageFilter = strPageFilter + " AND (TableTabID=" + ddlPage.SelectedValue + ") ";
        }
        //if (chkShowSystemFields.Checked == false)
        //    strFilter = strFilter + " AND IsStandard=0 ";
        strFilter = strFilter + strPageFilter;



        //do we need this
        int iXmax = 1;
        int iYmax = 0;

        string strXmax = Common.GetValueFromSQL("SELECT MAX(Xcoordinate) FROM [Column] WHERE TableID="
            + strTableID + strPageFilter);
        if (strXmax != "")
        {
            iXmax = int.Parse(strXmax);
        }
        string strYmax = Common.GetValueFromSQL("SELECT MAX(Ycoordinate) FROM [Column] WHERE TableID="
            + strTableID + strPageFilter);
        if (strYmax != "")
        {
            iYmax = int.Parse(strYmax);
        }
        iYmax = iYmax + 1;
        //
        int iNoOfColumns = int.Parse(ddlNoOfColumns.SelectedValue);
        if (iNoOfColumns > iXmax)
        {
            iXmax = iNoOfColumns;
        }
        else
        {
            if (ddlNoOfColumns.Items.FindByValue(iXmax.ToString()) != null)
            {
                ddlNoOfColumns.Text = iXmax.ToString();
            }
        }


        for (int y = 1; y <= iYmax; y++)
        {
            HtmlTableRow trTemp = new HtmlTableRow();
            trTemp.ClientIDMode = ClientIDMode.Static;
            trTemp.ID = "tr_" + y.ToString();
            for (int x = 1; x <= iXmax; x++)
            {
                HtmlTableCell tdTemp = new HtmlTableCell();
                tdTemp.ClientIDMode = ClientIDMode.Static;
                tdTemp.ID = "td_" + x.ToString() + "_" + y.ToString();
                //tdTemp.Attributes.Remove("colspan");
                //tdTemp.Attributes.Remove("rowspan");
                trTemp.Cells.Add(tdTemp);
            }
            tblDetailTable.Rows.Add(trTemp);
        }

        DataTable dtCurrentPageColumns = Common.DataTableFromText("SELECT * FROM [Column] WHERE( Xcoordinate>0 or Xcoordinate is null)   AND DisplayTextDetail IS NOT NULL  AND " + strFilter
            + "  ORDER BY Xcoordinate,Ycoordinate");
        foreach (DataRow drEachColumn in dtCurrentPageColumns.Rows)
        {
            var span = new HtmlGenericControl("span");
            span.Attributes.Add("draggable", "true");
            span.Attributes.Add("class", "columnspan");
            span.Attributes.Add("id", "span_" + drEachColumn["ColumnID"].ToString());
            span.InnerHtml = @"<a title='Edit' href='" + Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath
                + @"/Pages/Record/RecordColumnDetail.aspx?mode=ap70n78xI6o=&typemode=ap70n78xI6o=&SearchCriteria2=ZbLGwVAkfi8=&SearchCriteria=&ColumnID="
                + Cryptography.Encrypt(drEachColumn["ColumnID"].ToString()) + @"'> <img title='Edit' src='../../App_Themes/Default/Images/iconEdit.png'
                    alt='' style='border-width:0px;vertical-align:top;float:left;'></a><p style='padding-left:30px;'>" +
                drEachColumn["DisplayName"].ToString() + "</p>";
            string strCellName = "td_" + drEachColumn["Xcoordinate"].ToString() + "_" + drEachColumn["Ycoordinate"].ToString();
            if (tblDetailTable.FindControl(strCellName) != null)
            {
                HtmlTableCell tdTempCol = (HtmlTableCell)tblDetailTable.FindControl(strCellName);
                tdTempCol.Controls.Add(span);
            }
        }

        //rowspan & colspan
        try
        {


            foreach (DataRow drEachColumn in dtCurrentPageColumns.Rows)
            {
                if (drEachColumn["ColSpan"].ToString() != "")
                {
                    int iColSpan = 0;
                    if (int.TryParse(drEachColumn["ColSpan"].ToString(), out iColSpan))
                    {
                        string strCellName = "td_" + drEachColumn["Xcoordinate"].ToString() + "_" + drEachColumn["Ycoordinate"].ToString();
                        if (tblDetailTable.FindControl(strCellName) != null)
                        {
                            HtmlTableCell tdTempCol = (HtmlTableCell)tblDetailTable.FindControl(strCellName);
                            tdTempCol.Attributes.Add("colspan", iColSpan.ToString());
                            int iXStart = int.Parse(drEachColumn["Xcoordinate"].ToString()) + 1;
                            int iXEnd = iXStart + iColSpan - 1;
                            for (int i = iXStart; i < iXEnd; i++)
                            {
                                string strTempCellName = "td_" + i.ToString() + "_" + drEachColumn["Ycoordinate"].ToString();
                                if (tblDetailTable.FindControl(strTempCellName) != null)
                                {
                                    HtmlTableCell tdTempColspan = (HtmlTableCell)tblDetailTable.FindControl(strTempCellName);
                                    if (tdTempColspan != null)
                                    {
                                        tdTempColspan.Style.Add("display", "none");
                                    }
                                }
                            }
                        }
                    }
                }

            }


            foreach (DataRow drEachColumn in dtCurrentPageColumns.Rows)
            {
                if (drEachColumn["RowSpan"].ToString() != "")
                {
                    int iRowSpan = 0;
                    if (int.TryParse(drEachColumn["RowSpan"].ToString(), out iRowSpan))
                    {
                        string strCellName = "td_" + drEachColumn["Xcoordinate"].ToString() + "_" + drEachColumn["Ycoordinate"].ToString();
                        if (tblDetailTable.FindControl(strCellName) != null)
                        {
                            HtmlTableCell tdTempCol = (HtmlTableCell)tblDetailTable.FindControl(strCellName);
                            tdTempCol.Attributes.Add("rowspan", iRowSpan.ToString());
                            int iYStart = int.Parse(drEachColumn["Ycoordinate"].ToString()) + 1;
                            int iYEnd = iYStart + iRowSpan - 1;
                            for (int i = iYStart; i < iYEnd; i++)
                            {
                                string strTempCellName = "td_" + drEachColumn["Xcoordinate"].ToString() + "_" + i.ToString();
                                if (tblDetailTable.FindControl(strTempCellName) != null)
                                {
                                    HtmlTableCell tdTempColspan = (HtmlTableCell)tblDetailTable.FindControl(strTempCellName);
                                    if (tdTempColspan != null)
                                    {
                                        tdTempColspan.Style.Add("display", "none");
                                    }
                                }
                            }
                        }
                    }
                }

            }





        }
        catch
        {
            //
        }
       


        //end of rowspan and colspan



        if (chkShowSystemFields.Checked == false)
            strFilter = strFilter + " AND IsStandard=0 ";

        DataTable dtHiddenFields = Common.DataTableFromText("SELECT * FROM [Column] WHERE DisplayTextDetail IS NULL AND  " + strFilter);
        foreach (DataRow drHF in dtHiddenFields.Rows)
        {
            Panel pnlHF = new Panel();
            pnlHF.CssClass = "col";
            var span = new HtmlGenericControl("span");
            //span.Attributes.Add("draggable", "true");
            span.Attributes.Add("class", "hiddencolumnspan");
            span.InnerHtml = @"<a title='Edit' href='" + Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath
                + @"/Pages/Record/RecordColumnDetail.aspx?mode=ap70n78xI6o=&typemode=ap70n78xI6o=&SearchCriteria2=ZbLGwVAkfi8=&SearchCriteria=&ColumnID="
                + Cryptography.Encrypt(drHF["ColumnID"].ToString()) + @"'> <img title='Edit' src='../../App_Themes/Default/Images/iconEdit.png'
                    alt='' style='border-width:0px;vertical-align:top;float:left;'></a><p style='padding-left:30px;'>" +
                drHF["DisplayName"].ToString() + "</p>";

            pnlHF.Controls.Add(span);
            divHiddenFields.Controls.Add(pnlHF);
        }
    }




    protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlNoOfColumns.SelectedIndex = 0;
        PopulateColumnsInCells();
    }
    protected void ddlNoOfColumns_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateColumnsInCells();
    }
    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        PopulateColumnsInCells();
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class Pages_Custom_JCU_Participants :SecurePage
{
    int iSiteTableID = 3140;
    int iParticipantTableID = 3376;
    int iVisitTableID = 3377;
    int iQueryTableID = 3392;
    int iUSTableID = 3435;
    string _strparticipatURL = "";
    string _strparticipatUSCountURL = "";
    string _strVisitEditURL = "";
    string _strEditURL = "";
    string _strViewURL = "";
    string _strVisitAddURL="";
    bool disableAddUV;

    Account _theAccount;
    UserRole _theUserRole;
    Role _theRole;

    protected void Page_Load(object sender, EventArgs e)
    {
        Title = lblTitle.Text;

        _theAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));
        _theUserRole = (UserRole)Session["UserRole"];
        _theRole = SecurityManager.Role_Details((int)_theUserRole.RoleID);

        _strEditURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?mode=" + Cryptography.Encrypt("edit") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=[TableID]&Recordid=";
        _strViewURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?mode=" + Cryptography.Encrypt("view") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=[TableID]&Recordid=";

        //Participant
        _strparticipatURL = _strEditURL.Replace("[TableID]", Cryptography.Encrypt(iParticipantTableID.ToString()));
        DataTable dtUserTableParticipant = SecurityManager.dbg_RoleTable_Select(null, iParticipantTableID, _theUserRole.RoleID, null);
        if (dtUserTableParticipant.Rows.Count > 0)
        {
            string _strRecordRightID = dtUserTableParticipant.Rows[0]["RoleType"].ToString();
            if (_strRecordRightID == Common.UserRoleType.ReadOnly)
            {
                _strparticipatURL = _strViewURL.Replace("[TableID]", Cryptography.Encrypt(iParticipantTableID.ToString()));
            }
        }

        //Visit
        _strVisitEditURL = _strEditURL.Replace("[TableID]", Cryptography.Encrypt(iVisitTableID.ToString()));
        DataTable dtUserTableVisit = SecurityManager.dbg_RoleTable_Select(null, iVisitTableID, _theUserRole.RoleID, null);
        if (dtUserTableVisit.Rows.Count > 0)
        {
            string _strRecordRightID = dtUserTableVisit.Rows[0]["RoleType"].ToString();
            if (_strRecordRightID == Common.UserRoleType.ReadOnly)
            {
                _strVisitEditURL = _strViewURL.Replace("[TableID]", Cryptography.Encrypt(iVisitTableID.ToString()));
            }
        }

        _strVisitAddURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?mode=" + Cryptography.Encrypt("add") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID="+Cryptography.Encrypt(iVisitTableID.ToString())+"&parentRecordid=";

        if (Request.QueryString["Dashboard"] == null && Request.QueryString["DashboardID"] == null)
        {
            if (!string.IsNullOrEmpty(_theRole.DashboardType))
            {
                //if (_theRole.DashboardType == "T" && _theRole.DashboardTableID != null)
                //{
                //    Response.Redirect("~/Pages/Record/RecordList.aspx?TableID=" +
                //        Cryptography.Encrypt(_theRole.DashboardTableID.ToString()), false);
                //    return;
                //}
                if (_theRole.DashboardType == "L" && !string.IsNullOrEmpty(_theRole.DashboardLink))
                {
                    if (Request.Url.LocalPath != _theRole.DashboardLink)
                    {
                        Response.Redirect(_theRole.DashboardLink, false);
                        return;
                    }
                }
            }
        }

        if (!IsPostBack)
        {
            Session["stackURL"] = null;
            Session["stackTabIndex"] = null;
            Session["stackTableTabID"] = null;
            if (Session["jcu_role_key"] == null)
            {
                CustomMethod.DotNetMethod("jcu_login_custom_role", null);
            }

            PopulateSite();
            if(Session["jcu_participant_scID"]!=null)
            {
                PopulateSearchCriteria(int.Parse(Session["jcu_participant_scID"].ToString()));
              
            }
            lnkSearch_Click(null, null);
        }

    }
    protected void PopulateSite()
    {
        DataTable dtSite = Common.DataTableFromText("SELECT RecordID,V001 FROM [Record] WHERE TableID=" + iSiteTableID.ToString() + " AND IsActive=1 ORDER BY V001");

        ListItem aSelect = new ListItem("--Please Select--", "");
        ddlSite.Items.Add(aSelect);
        foreach(DataRow dr in dtSite.Rows)
        {
            ListItem aItem = new ListItem( dr["V001"].ToString(),dr["RecordID"].ToString());
            ddlSite.Items.Add(aItem);
        }
      
    }
    protected void lnkSearch_Click(object sender, EventArgs e)
    {
        string strParticipantTextSearch = "";
        bool dataScopeUsed = false;

        if (_theAccount.UseDataScope != null)
        {
            if ((bool)_theAccount.UseDataScope)
            {
                if (_theUserRole.DataScopeColumnID != null && _theUserRole.DataScopeValue != "")
                {
                    dataScopeUsed = true;
                    tdDdlSite.Visible = false;
                    tdlblSite.Visible = false;
                    ListItem siteItem = ddlSite.Items.FindByText(_theUserRole.DataScopeValue);
                    strParticipantTextSearch = strParticipantTextSearch + " AND V021='" + siteItem.Value + "'";
                }
            }
        }
        if (dataScopeUsed == false)
        {
            if (ddlSite.SelectedValue != "")
            {
                strParticipantTextSearch = strParticipantTextSearch + " AND V021='" + ddlSite.SelectedValue + "'";
            }
        }
        if (txtParticipantID.Text!="")
        {
            strParticipantTextSearch = strParticipantTextSearch + " AND V002 LIKE '%" + txtParticipantID.Text.Replace("'","''")  + "%'";
        }
        if (txtInitials.Text != "")
        {
            strParticipantTextSearch = strParticipantTextSearch + " AND V001 LIKE '%" + txtInitials.Text.Replace("'", "''") + "%'";
        }

        //if (ddlSite.SelectedValue == "" && txtParticipantID.Text == "" && txtInitials.Text == "")
        //{
        //    iParticipantTableID = -1;
        //}
        //     DataTable dtParticipants = RecordManager.ets_Record_List(iParticipantTableID,
        // null,true,null, null, null,
        // "", "", null, null, ref iTN, ref _iTotalDynamicColumns, "allcolumns", "", strTextSearch,
        //null, null, "", "", "", null, ref strReturnSQL, ref sReturnHeaderSQL);

        grdVisit.DataSource = Common.DataTableFromText(@"SELECT RecordID,V001,V002,V021 FROM [Record] WHERE TableID=" + iParticipantTableID.ToString() + " AND IsActive=1 " + strParticipantTextSearch);
        grdVisit.DataBind();

        try
        {
            string xml = null;
            xml = @"<root>" +
                   " <" + ddlSite.ID + ">" + HttpUtility.HtmlEncode(ddlSite.Text) + "</" + ddlSite.ID + ">" +
                   " <" + txtParticipantID.ID + ">" + HttpUtility.HtmlEncode(txtParticipantID.Text) + "</" + txtParticipantID.ID + ">" +

                    " <" + txtInitials.ID + ">" + HttpUtility.HtmlEncode(txtInitials.Text) + "</" + txtInitials.ID + ">" +
                      "</root>";
            SearchCriteria theSearchCriteria = new SearchCriteria(null, xml);
            int? iSCID = SystemData.SearchCriteria_Insert(theSearchCriteria);
            if (iSCID != null)
                Session["jcu_participant_scID"] = iSCID;
        }
        catch
        {

        }

         

        
    }
    protected void PopulateSearchCriteria(int iSearchCriteriaID)
    {
        try
        {
              SearchCriteria theSearchCriteria = SystemData.SearchCriteria_Detail(iSearchCriteriaID);


              if (theSearchCriteria != null)
              {
                  System.Xml.XmlDocument xmlSC_Doc = new System.Xml.XmlDocument();

                  xmlSC_Doc.Load(new StringReader(theSearchCriteria.SearchText));

                  if (xmlSC_Doc.FirstChild[ddlSite.ID]!=null && ddlSite.Items.FindByValue(xmlSC_Doc.FirstChild[ddlSite.ID].InnerText) != null)
                  {
                      ddlSite.Text = xmlSC_Doc.FirstChild[ddlSite.ID].InnerText;
                  }


                  txtParticipantID.Text = xmlSC_Doc.FirstChild[txtParticipantID.ID].InnerText;
                  txtInitials.Text = xmlSC_Doc.FirstChild[txtInitials.ID].InnerText;
              }
        }
        catch
        {

        }
    }
    protected void lnkReset_Click(object sender, EventArgs e)
    {
        ddlSite.SelectedIndex = 0;
        txtParticipantID.Text = "";
        txtInitials.Text = "";
        lnkSearch_Click(null, null);
    }
    protected void grdTable_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            

            string strParticipantRecordID=DataBinder.Eval(e.Row.DataItem, "RecordID").ToString();
            HyperLink hfIDnInitils = e.Row.FindControl("hfIDnInitils") as HyperLink;
            hfIDnInitils.Text = DataBinder.Eval(e.Row.DataItem, "V002").ToString();
            hfIDnInitils.NavigateUrl = _strparticipatURL + Cryptography.Encrypt(strParticipantRecordID);

            string strParticipantLocked = Common.GetValueFromSQL("SELECT IsReadOnly FROM [Record] WHERE RecordID = " + strParticipantRecordID);
            bool bIsReadOnly;
            if (Boolean.TryParse(strParticipantLocked, out bIsReadOnly))
            {
                //Do nothing
            }

            if (bIsReadOnly)
            {
                disableAddUV = true;
            }
            else
            {
                disableAddUV = false;
            }

            //visit V002=visit template,V140=Visit Status,V139=Monitor Reviewed-Yes/No
            DataTable dtVisit = Common.DataTableFromText(@"SELECT RecordID,V002,V140,V139,IsReadOnly FROM [Record] WHERE TableID=" + iVisitTableID.ToString()
                + " AND IsActive=1 AND V001='" + strParticipantRecordID + "'");

           //string strUS = Common.GetValueFromSQL(@"SELECT COUNT(RecordID) FROM [Record] WHERE TableID=" + iUSTableID.ToString()
           //     + " AND IsActive=1 AND V001='" + strParticipantRecordID + "'");

            string strUS = Common.GetValueFromSQL(@"SELECT COUNT(RecordID) FROM [Record] WHERE TableID=" + iVisitTableID.ToString()
                + " AND IsActive=1 AND V002='1822546' AND V001='" + strParticipantRecordID + "' AND EnteredBy!=2");


            _strparticipatUSCountURL=_strparticipatUSCountURL+Cryptography.Encrypt(strParticipantRecordID);
            string strVisitEdit = "<a href='" + _strVisitEditURL + "[RecordID]" + "'><img src='jcu_images/[imagename].png' title='[imagename]' /></a>[query]";

            Label lbl6 = e.Row.FindControl("lbl6") as Label;//1755822
            Label lbl2 = e.Row.FindControl("lbl2") as Label;//1755823
            Label lbl0 = e.Row.FindControl("lbl0") as Label;//1755824
            Label lbl8 = e.Row.FindControl("lbl8") as Label;//1755825
            Label lbl12 = e.Row.FindControl("lbl12") as Label;//1755826
            Label lbl24 = e.Row.FindControl("lbl24") as Label;//1755827
            Label lbl36 = e.Row.FindControl("lbl36") as Label;//1755828
            Label lbl42 = e.Row.FindControl("lbl42") as Label;//1755829
            Label lbl44 = e.Row.FindControl("lbl44") as Label;//1755830
            Label lbl46 = e.Row.FindControl("lbl46") as Label;//1755831
            Label lbl66 = e.Row.FindControl("lbl66") as Label;//1755832
            Label lbl94 = e.Row.FindControl("lbl94") as Label;//1755833
            Label lblET = e.Row.FindControl("lblET") as Label;//1889656
            Label lbl96 = e.Row.FindControl("lbl96") as Label;//1755835
            Label lbluv = e.Row.FindControl("lbluv") as Label;//1822546 Unscheduled (Week -0)

            foreach(DataRow drV in dtVisit.Rows)
            {
                string strOneLabel = strVisitEdit.Replace("[RecordID]", Cryptography.Encrypt(drV["RecordID"].ToString()));
                string strImageName = "";
                if (drV["V140"].ToString() != "")
                {
                    strImageName = drV["V140"].ToString();
                }

                if (drV["V139"].ToString() != "" && drV["V139"].ToString().ToLower() == "yes")
                {
                    strImageName = "verified by monitor";
                }                
                string strQuery = "";
                string strQueryCount = Common.GetValueFromSQL("SELECT COUNT(RecordID) FROM [Record] WHERE TableID=" + iQueryTableID.ToString()
                    + " AND IsActive=1 AND V001='" + drV["RecordID"].ToString() + "' AND V004<>'Yes'");

                if (strQueryCount != "" && int.Parse(strQueryCount) > 0)
                {
                    strQuery = strQueryCount;
                }
                if (strImageName == "")
                {
                    strImageName = "Visit has not started yet";
                }

                if (strImageName == "Visit data is complete")
                {
                    string forReview = Common.GetValueFromSQL("SELECT COUNT(RecordID) FROM [Record] WHERE " +
                        "V258 = 'Yes' AND RecordID = " + drV["RecordID"].ToString());
                    if (forReview != "" && int.Parse(forReview) > 0)
                    {
                        strQuery = "*";
                    }
                }

                if (drV["IsReadOnly"].ToString() != "" && drV["IsReadOnly"].ToString().ToLower() == "true")
                {
                    strImageName = "Locked";
                }  

                strOneLabel = strOneLabel.Replace("[imagename]", strImageName);
                //if (strImageName.IndexOf("missing")>-1)
                //{
                //    strQuery = "";
                //}
                strOneLabel = strOneLabel.Replace("[query]","<strong>" +strQuery + "</strong>");
                switch (drV["V002"].ToString())
                {
                    case "1755822":
                        lbl6.Text = strOneLabel;
                        break;
                    case "1755823":
                        lbl2.Text = strOneLabel;
                        break;
                    case "1755824":
                        lbl0.Text = strOneLabel;
                        break;
                    case "1755825":
                        lbl8.Text = strOneLabel;
                        break;
                    case "1755826":
                        lbl12.Text = strOneLabel;
                        break;
                    case "1755827":
                        lbl24.Text = strOneLabel;
                        break;
                    case "1755828":
                        lbl36.Text = strOneLabel;
                        break;
                    case "1755829":
                        lbl42.Text = strOneLabel;
                        break;
                    case "1755830":
                        lbl44.Text = strOneLabel;
                        break;
                    case "1755831":
                        lbl46.Text = strOneLabel;
                        break;
                    case "1755832":
                        lbl66.Text = strOneLabel;
                        break;
                    case "1755833":
                        lbl94.Text = strOneLabel;
                        break;
                    case "1889656":
                        lblET.Text = strOneLabel;
                        break;
                    case "1755835":
                        lbl96.Text = strOneLabel;
                        break;
                    //case "1822546":
                    //    lbluv.Text = strOneLabel;
                    //    break;
                }
              
            }
            if(strUS!="")
            {

                string strUS_XMLDefID = @"<root>" +
                                                "<defaultvalues>" +
                                                     "<dynamic>" +
                                                        "<ID>ddlParentSearch_V001</ID>" +
                                                          "<Property>Text</Property>" +
                                                        "<defaultvalue>"+strParticipantRecordID.ToString() +"</defaultvalue>" +
                                                    "</dynamic>" +
                                                      "<dynamic>" +
                                                        "<ID>ddlParentSearch_V002</ID>" +
                                                         "<Property>Text</Property>" +
                                                        "<defaultvalue>1822546</defaultvalue>" +
                                                    "</dynamic>" +
                                                "</defaultvalues>" +
                                            "</root>";
                SearchCriteria usSearchCriteria = new SearchCriteria(null, strUS_XMLDefID);
                int iUS_XMLDefID = SystemData.SearchCriteria_Insert(usSearchCriteria);
                _strparticipatUSCountURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Record/RecordList.aspx?TableID=" + Cryptography.Encrypt(iVisitTableID.ToString()) 
                    + "&XMLDefID=" + Cryptography.Encrypt(iUS_XMLDefID.ToString()) ;



                lbluv.Text = "<a href='" + _strparticipatUSCountURL + "'><strong>" + strUS + "<strong></a>" + lbluv.Text;
            }
            string strAddVisit_XMLDef = @"<root>" +          
                    "<controlname>DropDownList</controlname>" +
                    "<ID>ctl00_HomeContentPlaceHolder_ddlV002</ID>" +
                    "<defaultvalue>[visitrecordid]</defaultvalue>" +            
            "</root>";
            
            string strAddVisit_XMLDef_temp="";
            string strVisitAdd = "<a href='" + _strVisitAddURL + Cryptography.Encrypt(strParticipantRecordID) 
                + "&XMLDef=[strAddVisit_XMLDef]' ><img src='jcu_images/Visit has not started yet.png' title='Visit has not started yet' /></a>";

            string strUSVisitAdd = "<a href='" + _strVisitAddURL + Cryptography.Encrypt(strParticipantRecordID) 
                + "&XMLDefID=[XMLDefID]' ><img src='../Pager/Images/add.png' title='Add unscheduled visit.' /></a>";

            
            if(lbl6.Text=="")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1755822");
                lbl6.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]",Cryptography.Encrypt( strAddVisit_XMLDef_temp) );
            }
            if (lbl2.Text == "")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1755823");
                lbl2.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]", Cryptography.Encrypt(strAddVisit_XMLDef_temp));

            }
            if (lbl0.Text == "")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1755824");
                lbl0.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]", Cryptography.Encrypt(strAddVisit_XMLDef_temp));

            }
            if (lbl8.Text == "")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1755825");
                lbl8.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]", Cryptography.Encrypt(strAddVisit_XMLDef_temp));

            }
            if (lbl12.Text == "")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1755826");
                lbl12.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]", Cryptography.Encrypt(strAddVisit_XMLDef_temp));

            }
            if (lbl24.Text == "")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1755827");
                lbl24.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]", Cryptography.Encrypt(strAddVisit_XMLDef_temp));

            }
            if (lbl36.Text == "")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1755828");
                lbl36.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]", Cryptography.Encrypt(strAddVisit_XMLDef_temp));

            }
            if (lbl42.Text == "")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1755829");
                lbl42.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]", Cryptography.Encrypt(strAddVisit_XMLDef_temp));

            }
            if (lbl44.Text == "")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1755830");
                lbl44.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]", Cryptography.Encrypt(strAddVisit_XMLDef_temp));

            }
            if (lbl46.Text == "")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1755831");
                lbl46.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]", Cryptography.Encrypt(strAddVisit_XMLDef_temp));

            }
            if (lbl66.Text == "")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1755832");
                lbl66.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]", Cryptography.Encrypt(strAddVisit_XMLDef_temp));

            }
            if (lbl94.Text == "")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1755833");
                lbl94.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]", Cryptography.Encrypt(strAddVisit_XMLDef_temp));

            }
            if (lblET.Text == "")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1889656");
                lblET.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]", Cryptography.Encrypt(strAddVisit_XMLDef_temp));

            }
            if (lbl96.Text == "")
            {
                strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1755835");
                lbl96.Text = strVisitAdd.Replace("[strAddVisit_XMLDef]", Cryptography.Encrypt(strAddVisit_XMLDef_temp));

            }
            //strAddVisit_XMLDef_temp = strAddVisit_XMLDef.Replace("[visitrecordid]", "1822546");
            string strUS_XMLDefID2 = @"<root>" +
                                               "<defaultvalues>" +
                                                    "<dynamic>" +
                                                       "<ID>ctl00_HomeContentPlaceHolder_ddlV002</ID>" +
                                                         "<Property>Text</Property>" +
                                                       "<defaultvalue>1822546</defaultvalue>" +
                                                   "</dynamic>" +                                                   
                                               "</defaultvalues>" +
                                           "</root>";
            SearchCriteria usSearchCriteria2 = new SearchCriteria(null, strUS_XMLDefID2);
            int iUS_XMLDefID2 = SystemData.SearchCriteria_Insert(usSearchCriteria2);
            lbluv.Text = lbluv.Text + "&nbsp;&nbsp;" + strUSVisitAdd.Replace("[XMLDefID]", Cryptography.Encrypt(iUS_XMLDefID2.ToString()));

            if (disableAddUV == true)
            {
                lbluv.CssClass = "disable";
            }
            //if(strUS!="")
            //{
            //    lbluv.Text = strUS;
            //}
        }
    }
}
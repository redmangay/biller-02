﻿<%@ Page Language="C#" %>
<%@ Import Namespace="DBGReportExecutionService" %>
<%@ Import Namespace="System.IO" %>

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string reportId = Request["reportId"];
            List<ParameterValue> parameters = new List<ParameterValue>();
            int paramIndex = 0;

            while (Request["Parameters[" + paramIndex + "][Name]"] != null)
            {
                string name = Request["Parameters[" + paramIndex + "][Name]"];
                string value = Request["Parameters[" + paramIndex + "][Value]"];
                                
                if (value.Contains("|"))
                {
                    string[] multiValues = value.Split('|');
                    
                    foreach (string multiValue in multiValues)
                    {
                        parameters.Add(new ParameterValue() { Name = name, Value = multiValue });
                    }
                }
                else
                {
                    if (value == "NULL")
                    {
                        value = null;
                    }
                    
                    parameters.Add(new ParameterValue() { Name = name, Value = value });
                }
                
                paramIndex++;
            }

            /* == as per Jon, we will create an option to select if pdf or excel later when a client requires it == */
            ReportManager.ReportFormat reportFormat = ReportManager.ReportFormat.PDF;

            Response.Write(ReportManager.GenerateReport(int.Parse(reportId), parameters.ToArray(), reportFormat));
        }
        catch (Exception ex)
        {
            Response.Write("Error: " + ex.Message);
        }
    }
</script>
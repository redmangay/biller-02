﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class Pages_Generic_Datatable : SecurePage
{
    SearchCriteria theSearchCriteria = null;

    protected void gvTheGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Red
        //all information about the row data bound row attrib 
        //can be put in searchcriteria prior to calling this page
        //Ref: 
        //NoOfColumns: Defined in the output of the SP to generate
        //alignCell: Left, Right, Center
        //widthCell: width of the cell column
        //we can add more... no need to store in db like view

        int NoOfColumns = 0;
        if (theSearchCriteria != null)
        {
            System.Xml.XmlDocument xmlSC_Doc = new System.Xml.XmlDocument();
            xmlSC_Doc.Load(new StringReader(theSearchCriteria.SearchText));

            if (xmlSC_Doc.FirstChild["NoOfColumns"] != null)
                NoOfColumns = int.Parse(xmlSC_Doc.FirstChild["NoOfColumns"].InnerText);

            if (e.Row.RowType == DataControlRowType.Header)
            {
                for (int j = 0; j < NoOfColumns; j++)
                {
                    if (xmlSC_Doc.FirstChild["alignCell" + j].InnerText != null)
                    {
                        if (xmlSC_Doc.FirstChild["alignCell" + j].InnerText == "Left")
                        {
                            e.Row.Cells[j].HorizontalAlign = HorizontalAlign.Left;

                            if (xmlSC_Doc.FirstChild["widthCell" + j].InnerText != null)
                                e.Row.Cells[j].Width = Unit.Percentage(int.Parse(xmlSC_Doc.FirstChild["widthCell" + j].InnerText.ToString()));
                        }
                        else
                        {
                            e.Row.Cells[j].HorizontalAlign = HorizontalAlign.Right;
                            if (xmlSC_Doc.FirstChild["widthCell" + j].InnerText != null)
                                e.Row.Cells[j].Width = Unit.Percentage(int.Parse(xmlSC_Doc.FirstChild["widthCell" + j].InnerText.ToString()));
                        }
                    }

                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                for (int j = 0; j < NoOfColumns; j++)
                {
                    if (xmlSC_Doc.FirstChild["alignCell" + j].InnerText != null)
                    {
                        if (xmlSC_Doc.FirstChild["alignCell" + j].InnerText == "Left")
                        {
                            e.Row.Cells[j].HorizontalAlign = HorizontalAlign.Left;
                        }
                        else
                        {
                            e.Row.Cells[j].HorizontalAlign = HorizontalAlign.Right;
                        }
                    }
                }
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if (Request.QueryString["scid"] != null)
            {
                theSearchCriteria = SystemData.SearchCriteria_Detail(int.Parse(Request.QueryString["scid"].ToString()));
                if (theSearchCriteria != null)
                {
                    System.Xml.XmlDocument xmlSC_Doc = new System.Xml.XmlDocument();

                    xmlSC_Doc.Load(new StringReader(theSearchCriteria.SearchText));
                    GenericParam theGenericParam = new GenericParam();
                    if (xmlSC_Doc.FirstChild["TopTitle"] != null)
                        lblTopTitle.Text = xmlSC_Doc.FirstChild["TopTitle"].InnerText;
                    if (xmlSC_Doc.FirstChild["RecordID"] != null)
                        theGenericParam.RecordID = int.Parse(xmlSC_Doc.FirstChild["RecordID"].InnerText);

                    if (xmlSC_Doc.FirstChild["spname"] != null)
                        theGenericParam.SPName = xmlSC_Doc.FirstChild["spname"].InnerText;

                    /* Red: Real time from current page value as parameter 
                            Client-side scripting...
                     */
                    if (Request.QueryString["param001"] != null)
                        theGenericParam.param001 = Request.QueryString["param001"].ToString();

                    if (Request.QueryString["param002"] != null)
                        theGenericParam.param002 = Request.QueryString["param002"].ToString();

                    if (Request.QueryString["param003"] != null)
                        theGenericParam.param003 = Request.QueryString["param003"].ToString();

                    if (Request.QueryString["param004"] != null)
                        theGenericParam.param004 = Request.QueryString["param004"].ToString();

                    if (Request.QueryString["param005"] != null)
                        theGenericParam.param005 = Request.QueryString["param005"].ToString();
                    /*End Red*/

                    DataTable theDataTable = null;
                    List<object> objList = new List<object>();
                    List<object> roList = null;
                    theGenericParam.UserID = ((User)Session["User"]).UserID;

                    

                    objList.Add(theGenericParam);
                    if (xmlSC_Doc.FirstChild["methodname"] != null)
                    {                        
                       roList = MethodRouter.DotNetMethod(xmlSC_Doc.FirstChild["methodname"].InnerText, objList);
                    }

                    if (xmlSC_Doc.FirstChild["custommethod"] != null)
                    {
                        objList.Add(pnlPopupDatatable);
                        roList = CustomMethod.DotNetMethod(xmlSC_Doc.FirstChild["custommethod"].InnerText, objList);
                    }

                    if (roList != null)
                    {
                        foreach (object obj in roList)
                        {
                            if (obj.GetType().Name == "DataTable")
                            {
                                theDataTable = (DataTable)obj;
                                break;
                            }
                        }
                    }

                    if (theDataTable != null)
                    {
                        gvPopupDataTable.DataSource = theDataTable;
                        gvPopupDataTable.DataBind();
                    }
                }                
            }
        }
    }
}
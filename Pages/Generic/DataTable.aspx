﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true" CodeFile="DataTable.aspx.cs" Inherits="Pages_Generic_Datatable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" Runat="Server">

    <asp:UpdatePanel ID="upGeneric" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlPopupDatatable">
                <div style="text-align: center;">
                    <asp:Label runat="server" ID="lblTopTitle" CssClass="TopTitle">Data Table View</asp:Label>
                </div>
                <br />
                <br />
                <br />
                <div style="text-align: center; height:300px; overflow:auto">
                    <table style="padding-right:10px;" cellpadding="5", cellspacing="0" width="100%">
                       
                        <tr>
                            <td colspan="2">
                                <asp:GridView ID="gvPopupDataTable" runat="server" ShowHeaderWhenEmpty="true"
                                    AutoGenerateColumns="true" CssClass="gridview" AllowPaging="false"
                                    HeaderStyle-CssClass="gridview_header" HeaderStyle-Height="12px" 
                                    onrowdatabound="gvTheGrid_RowDataBound" RowStyle-CssClass="gridview_row" 
                                    alternatingrowstyle-backcolor="#DCF2F0" GridLines="Both">
                                    <RowStyle CssClass="gridview_row" />
                                    <AlternatingRowStyle CssClass="gridview_row" />
                                    </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 30px;"></td>
                        </tr>
                    </table>
                </div>
                 <div style="text-align:center; bottom: 10px; padding-bottom:20px; padding-top:20px;">
                      <asp:LinkButton runat="server" ID="lnkClose" CssClass="btn" OnClientClick="parent.$.fancybox.close();return false;">
                                           <strong>Close</strong>
                                            </asp:LinkButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


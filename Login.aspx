﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Marketing.master" AutoEventWireup="true"
    CodeFile="Login.aspx.cs" Inherits="SignIn" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <style type="text/css">
        .ui-dialog-titlebar {
            visibility: hidden;
        }

        #divTwoFactorAuthenticator {
            display: none;
        }

        #divGetTwoFactorAuthenticator {
            display: none;
        }

        #ctl00_HomeContentPlaceHolder_txtLogInPassword {
            width: 89%;
            float: left;
            margin-bottom: 30px;
        }


        #showPassword_icon {
            /*background:url("Images/eye-open-edited3.png") center center no-repeat #fff;*/
            border: none;
            box-shadow: none;
            border-radius: 10px;
            width: 48px;
            height: 48px;
            display: block;
            float: right;
            margin-left: -17px;
        }

        @media (min-width: 992px) and (max-width: 1199px) {

            #ctl00_HomeContentPlaceHolder_txtLogInPassword {
                width: 86%;
            }
        }

        @media (min-width: 769px) and (max-width: 991px) {
            #ctl00_HomeContentPlaceHolder_txtLogInPassword {
                width: 95%;
            }
        }

        @media (min-width: 750px) and (max-width: 768px) {
            #ctl00_HomeContentPlaceHolder_txtLogInPassword {
                width: 95%;
                border-radius: 10px 5px 5px 10px;
            }
        }

        @media (min-width: 647px) and (max-width: 749px) {
            #ctl00_HomeContentPlaceHolder_txtLogInPassword {
                width: 94%;
                border-radius: 10px 5px 5px 10px;
            }
        }

        @media (min-width: 518px) and (max-width: 646px) {
            #ctl00_HomeContentPlaceHolder_txtLogInPassword {
                width: 92%;
                border-radius: 10px 5px 5px 10px;
            }
        }

        @media (min-width: 411px) and (max-width: 517px) {
            #ctl00_HomeContentPlaceHolder_txtLogInPassword {
                width: 88%;
                border-radius: 10px 5px 5px 10px;
            }
        }

        @media (min-width: 388px) and (max-width: 410px) {
            #ctl00_HomeContentPlaceHolder_txtLogInPassword {
                width: 87%;
                border-radius: 10px 5px 5px 10px;
            }
        }

        @media (min-width: 369px) and (max-width: 387px) {
            #ctl00_HomeContentPlaceHolder_txtLogInPassword {
                width: 87%;
                border-radius: 10px 5px 5px 10px;
            }
        }

        @media (min-width: 352px) and (max-width: 368px) {
            #ctl00_HomeContentPlaceHolder_txtLogInPassword {
                width: 86%;
                border-radius: 10px 5px 5px 10px;
            }
        }

        @media (max-width: 351px) {
            #ctl00_HomeContentPlaceHolder_txtLogInPassword {
                width: 85%;
                border-radius: 10px 5px 5px 10px;
            }
        }

        @media (min-width: 320px) and (max-width: 336px) {
            #ctl00_HomeContentPlaceHolder_txtLogInPassword {
                width: 83%;
                border-radius: 10px 5px 5px 10px;
            }
        }

        @media screen and (max-width: 767px) {
            .navbar-brand.hidden.visible-xs {
                margin-top: -20px;
            }

                .navbar-brand.hidden.visible-xs img {
                    width: 200px;
                }
        }

        @media screen and (max-width: 575px) {
            .navbar-brand.hidden.visible-xs {
                margin-top: -13px;
            }

                .navbar-brand.hidden.visible-xs img {
                    width: 160px;
                }
        }

        @media screen and (max-width: 325px) {
            #ctl00_HomeContentPlaceHolder_hone br {
                display: none;
            }

            #ctl00_HomeContentPlaceHolder_hone {
                margin-top: 26px;
                font-size: 28px;
            }

            .login-trial-bg {
                margin-top: 80px;
                margin-bottom: 35px;
                -webkit-border-top-left-radius: 40px;
                -webkit-border-bottom-right-radius: 40px;
                -moz-border-top-left-radius: 40px;
                -moz-border-bottom-right-radius: 40px;
                border-top-left-radius: 30px;
                border-bottom-right-radius: 30px;
                padding: 20px 20px 25px 20px;
                margin-top: 20px;
            }

                .login-trial-bg h1 {
                    font-size: 25px;
                    margin-top: 5px;
                }

            .form-inpt {
                border-radius: 8px;
                height: 40px;
            }

            #ctl00_HomeContentPlaceHolder_txtLogInPassword {
                margin-bottom: 15px;
            }

            #ctl00_HomeContentPlaceHolder_txtLogInPassword {
                border-radius: 10px 5px 5px 10px;
            }

            #showPassword_icon {
                border-radius: 8px;
                width: 48px;
                height: 40px;
            }

            .btn-primary {
                padding: 8px 12px;
            }
        }
    </style>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/swfobject.js")%>"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

    <link href="<%=ResolveUrl("~/Styles/jquery-ui-dbgcustom.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Script/jquery-ui-1.12.1.min.js")%>"></script>

    <script language="javascript" type="text/javascript">
         function abc() {
             var b = document.getElementById('<%= lnkLogIn.ClientID %>');
            if (b && typeof (b.click) == 'undefined') {
                b.click = function () {
                    var result = true;
                    if (b.onclick) result = b.onclick();
                    if (typeof (result) == 'undefined' || result) {
                        eval(b.getAttribute('href'));
                    }
                }
            }

         }

         $(document).ready(function () {
           
             //jeRome Ticket 2808
             //Show Password Feature for the Password Field
             var ua = window.navigator.userAgent;
             var trident = ua.indexOf('Trident/');
             var edge = ua.indexOf('Edge/');
             var msie = ua.indexOf('MSIE ');

             if ((msie > 0) || (trident > 0) || (edge > 0)) {
                 // IE 10 or older => return version number
                 //alert(parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10));
                 document.getElementById('ctl00_HomeContentPlaceHolder_txtLogInPassword').style.width = '100%';
                 document.getElementById('showPassword_icon').style.display = 'none';

             }

                 // other browser
             else {

                 $('#showPassword_icon').css('background', 'url("Images/eye-open-edited3.png")');
                 $('#showPassword_icon').css('background-position-x', 'center');
                 $('#showPassword_icon').css('background-position-y', 'center');
                 $('#showPassword_icon').css('background-repeat', 'no-repeat');
                 $('#showPassword_icon').css('background-color', 'white');

                 $('#showPassword_icon').on('click', function () {

                     if ($(this).attr('data-click-state') == 1) {
                         $(this).attr('data-click-state', 0)
                         document.getElementById('ctl00_HomeContentPlaceHolder_txtLogInPassword').type = 'text';
                         $(this).css('background', 'url("Images/eye-close-edited3.png")');
                         $(this).css('background-position-x', 'center');
                         $(this).css('background-position-y', 'center');
                         $(this).css('background-repeat', 'no-repeat');
                         $(this).css('background-color', 'white');


                     } else {
                         $(this).attr('data-click-state', 1)
                         document.getElementById('ctl00_HomeContentPlaceHolder_txtLogInPassword').type = 'password';
                         $(this).css('background', 'url("Images/eye-open-edited3.png")');
                         $(this).css('background-position-x', 'center');
                         $(this).css('background-position-y', 'center');
                         $(this).css('background-repeat', 'no-repeat');
                         $(this).css('background-color', 'white');
                     }

                 });


             }
             //return false;
             //end jeRome Ticket 2808

          });

        <%-- $(document).ready(function () {

             function IsFlashSupported() {
                 var hfFlashSupport = document.getElementById('<%= hfFlashSupport.ClientID %>');
                 if (swfobject.hasFlashPlayerVersion('1')) {
                     hfFlashSupport.value = 'yes';
                 }
                 else {
                     hfFlashSupport.value = 'no';
                 }
                 alert(hfFlashSupport.value);
             }
             IsFlashSupported();
             //setTimeout(function () { IsFlashSupported(); }, 1000);
         });--%>

    </script>

    <script type="text/javascript">
          $(document).ready(function () {
              $.ajax({
                  url: '<%=ResolveUrl("~/Pages/Record/ColumnRecordRes.ashx")%>?DeviceWidth=' + $(window).width() + "&DeviceHeight" + $(window).height(),
                cache: false,
                success: function (content) {
                    if (content == 'False') {
                        //alert('DeviceWidth Content Error!');
                    }
                    else { //alert('good');
                    }
                },
                error: function (a, b, c) {
                    // alert('DeviceWidth Ajax Error!');
                }
            });
        });

    </script>

    <asp:Panel ID="pnlFull" runat="server" DefaultButton="lnkLogIn">
        <%--<asp:HiddenField runat="server" ID="hfFlashSupport" Value="no" />--%>
        <div runat="server" id="divFull" onkeypress="abc();">

            <div runat="server" id="divbanner" class="container-fluid">
                <%-- 55% 0% no-repeat">--%>
                <%-- <asp:HiddenField runat="server" ID="hfScreenWidth" ClientIDMode="Static" />--%>
                <%--redredred--%>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            <h1 runat="server" id="hone" class="bnr-txt"></h1>
                        </div>
                        <div class="col-xs-12 col-md-4 col-lg-4 trial-bg">

                            <h1>Sign In</h1>
                            <%--<form role="form">--%>
                            <div class="form-group">
                                <asp:TextBox ID="txtLogInEmail" placeholder="Email..." required type="email" runat="server" class="form-control form-inpt"></asp:TextBox>

                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="txtLogInPassword" placeholder="Password..." type="password" class="form-control form-inpt" MaxLength="30" runat="server" TextMode="Password"></asp:TextBox>

                                <input id="showPassword_icon" type="button" data-click-state="1" /><%--//jeRome Ticket 2808--%>
                            </div>
                            <%-- </form>--%>
                            <div class="form-group">
                                <%--<button type="submit" id="lnkLogin" class="btn btn-primary">Sign In</button>--%>
                                <asp:LinkButton runat="server" ID="lnkLogIn" ValidationGroup="Login" class="btn btn-primary"
                                    ClientIDMode="Static" CausesValidation="true" OnClick="lnkLogIn_Click"><strong>Sign In</strong> </asp:LinkButton>
                                <asp:HiddenField ID="hfValidate2FA" runat="server" />
                            </div>
                            <div runat="server" visible="false" class="form-group" style="vertical-align: middle; padding-top: 20px;">
                                <asp:CheckBox ID="chkRememberMe" runat="server" />
                                <strong>Remember me</strong>
                            </div>
                            <div class="forgot-pw">
                                <asp:HyperLink runat="server" ID="hlForgotPassword" NavigateUrl="~/Security/PasswordResetRequest.aspx">Forgot your password? </asp:HyperLink>
                            </div>

                        </div>
                        <div class="col-sm-4 hidden-xs hidden-sm hidden-md">
                            <div class="form-group">
                                <div runat="server" visible="false" id="divContentCommon">
                                    <asp:Label runat="server" ID="lblContentCommon" Style="color: #0299C6;" Text="TheDatabase is a highly configurable online database that has been designed to be very easy to use.</br> </br>  Developed by DB Gurus Australia since 2012, they have provided customisation services since 2006 for over 200 clients throughout Australia and can modify TheDatabase if required."></asp:Label>
                                    <br />
                                    <div style="width: 100%; text-align: right;">
                                        <asp:HyperLink runat="server" ID="hlContentCommonEdit" Visible="false">
                                        <asp:Image runat="server" ImageUrl="~/App_Themes/Default/Images/iconEdit.png" AlternateText="Edit" ToolTip="Edit Login page content (only for Admin)." />
                                        </asp:HyperLink>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="SignUpDiv" runat="server" id="divSignUp" visible="false">
                                    <table width="100%">
                                        <tr style="height: 75px;" runat="server" id="trCommonContentHeight">
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td align="center" runat="server" id="tdContentCommon2">
                                                <%--<strong style="font-size: 20px;">New to the TheDatabase?</strong><br />  <br />
                                                                                            <p>Get started now. It quick, easy and free!</p>
                                                                                            <br />--%>
                                                <asp:Label runat="server" ID="lblContentCommon2"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="trSignUp">
                                            <td style="padding-left: 140px;">
                                                <asp:HyperLink runat="server" ID="hlSignUp" CssClass="btn" NavigateUrl="~/SignUp.aspx"
                                                    Font-Size="15px" Font-Bold="true"><strong>Register</strong> </asp:HyperLink>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                    <%--redredred--%>
                </div>
            </div>
        </div>

        <%-- Red Ticket 2653 22may2017 --%>
        <div id="divTwoFactorAuthenticator">

            <table>
                <tr>
                    <td>
                        <div style="text-align: center">
                            <h3 style="margin-top: 0px;">Two Factor Authentication</h3>

                        </div>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr style="text-align: center">
                    <td>
                        <div style="text-align: center">
                            <span style="font-size: small">Please enter the 6 digit code from the Google Authenticator app in the text box below:</span>

                            <asp:Label ID="lblbool" Visible="false" runat="server" Text="Label"></asp:Label>

                        </div>
                        <br />
                        <div style="text-align: center">

                            <asp:TextBox ID="txtPINAuthentication" runat="server"></asp:TextBox>
                            <br />
                            <asp:Label ID="lblTFAInvalid" runat="server" Text="" Visible="false" Font-Size="Small" ForeColor="Red"></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr style="text-align: center">
                    <td>&nbsp;</td>
                </tr>
                <tr style="text-align: center">
                    <td>&nbsp;</td>
                </tr>
                <tr style="text-align: center">
                    <td>&nbsp;</td>
                </tr>


                <tr>
                    <td>

                        <div style="text-align: center">
                            <asp:LinkButton runat="server" ID="FAlogin" OnClientClick="TFAGetPIN(); " CssClass="btn btn-primary"
                                ClientIDMode="Static" CausesValidation="false" OnClick="FAlogin_Click" ForeColor="White" Width="150px"><strong>Verify</strong> </asp:LinkButton>
                            &nbsp;
                                <asp:LinkButton runat="server" ID="FAClose" OnClientClick="TFAClose();" CssClass="btn btn-primary"
                                    ClientIDMode="Static" CausesValidation="false" ForeColor="White" Width="150px"><strong>Cancel</strong> </asp:LinkButton>


                        </div>

                    </td>
                </tr>


            </table>
        </div>
        <%-- end Red--%>


        <%-- Red Ticket 2653 22may2017 --%>
        <div id="divGetTwoFactorAuthenticator">

            <table>
                <tr>
                    <td colspan="2" valign="top">
                        <div style="text-align: center">
                            <h2 style="margin-top: 0px;">Two Factor Authentication</h2>

                        </div>
                    </td>
                </tr>

                <tr style="text-align: center">

                    <td style="margin-top: 10px;">
                        <div style="text-align: justify">
                            <span style="font-size: small">Please download and install the Google Authenticator App from Google Play Store or Apple App Store and scan this QRcode.</span>
                            <br />
                            <br />
                            <span style="font-size: small">If you are not ready yet with the App, you can click Cancel; otherwise click Continue.</span>


                        </div>
                        <div runat="server" id="divlblGetCode" style="text-align: right">

                            <asp:Label ID="lblGetCode" runat="server" Text="Label"></asp:Label>
                        </div>
                    </td>
                    <td>
                        <asp:Image ID="imgGetQrCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>

                <tr>
                    <td colspan="2">
                        <div style="text-align: center">

                            <asp:LinkButton runat="server" ID="lnkClose" OnClientClick="getTFAClose(); return false;" CssClass="btn btn-primary"
                                CausesValidation="false" ClientIDMode="Static" ForeColor="White" Width="150px"><strong>Continue</strong></asp:LinkButton>

                            &nbsp;
                                  <asp:LinkButton runat="server" ID="lnkCloseDoNothing" OnClientClick="getTFACloseAndDoNothing(); " CssClass="btn btn-primary"
                                      ClientIDMode="Static" CausesValidation="false" OnClick="ClosedTFA_Click" ForeColor="White" Width="150px"><strong>Cancel</strong> </asp:LinkButton>

                        </div>

                    </td>
                </tr>
            </table>

        </div>
        <%-- end Red--%>
    </asp:Panel>
    <%--  <script type="text/javascript">
        $(document).ready(function () {

            if (window.screen.width != null) {
                document.getElementById('hfScreenWidth').value = window.screen.width;
            }
            else {
                if (window.screen.width != null) {
                    document.getElementById('hfScreenWidth').value = $(window).width();
                }
            }
        });

        //             if (window.screen.availWidth != null) {
        //                 document.getElementById('hfScreenWidth').value = window.screen.availWidth;
        //             }  $(window).width()
        //      if (window.screen.availablewidth < 1000) {
        //          window.location.href = "http://sizeForSmallRez";
        //      }
        //      else {
        //          window.location.href = "http://siteForLargeRez";
        //      }
    </script>--%>
</asp:Content>

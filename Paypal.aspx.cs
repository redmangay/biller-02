﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using Stripe;

public partial class Paypal : SecurePage
{
    public string stripePublishableKey = SystemData.SystemOption_ValueByKey_Account("StripePublishableKey", null, null);
    private string secretKey = SystemData.SystemOption_ValueByKey_Account("StripeSecretKey", null, null);
    User _theUser;

    private int iTotalPayment = 14;

    public string GetTotalPaymentAmount()
    {
        return (iTotalPayment * 1).ToString();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        _theUser = (User)Session["User"];

    }
    protected void btnPaypal_Click(object sender, EventArgs e)
    {
        //string stripePaypalMonthlyAmount = SystemData.SystemOption_ValueByKey_Account("Paypal Monthly Amount", null, null);

        string strPaypalAmount = GetTotalPaymentAmount().ToString();
        Session["PaypalItemName"] = "DBGurus eContractor Payment";
        Session["PaymentAmount"] = strPaypalAmount == "" ? "1" : strPaypalAmount;
        Session["PayIntervalType"] = "D";//M
        Session["InvoiceID"] = _theUser.UserID.ToString() + "_" + Session["AccountID"].ToString(); // a TDB Reference

        Response.Redirect("~/Pages/Security/PayPal/sendpayment_recurring.aspx", false);
    }
    

    public void CleanCustomers()
    {
        try
        {
            StripeCustomerService stripeService = new StripeCustomerService(secretKey);
            var options = new StripeCustomerListOptions();
            options.Limit = 10000;
            var customers = stripeService.List(options);
            //string sTest = "";
            foreach (var customer in customers)
            {
                stripeService.Delete(customer.Id);
                //sTest = sTest + "  " + customer.Email;
            }
        }
        catch
        {
        }
    }
    protected void btnCleanCustomer_Click(object sender, EventArgs e)
    {
        StripeConfiguration.SetApiKey(secretKey);
        CleanCustomers();
    }
    protected void btnCutomersManagement_Click(object sender, EventArgs e)
    {
        try
        {
            StripeConfiguration.SetApiKey(secretKey);
            StripeCustomerService stripeService = new StripeCustomerService(secretKey);
            var options = new StripeCustomerListOptions();
            options.Limit = 10000;
            var customers = stripeService.List(options);
            //string sTest = "";
            foreach (var customer in customers)
            {
               
               //get customer metadata to compare accountid & userid
                //customer.Metadata["UserID"] & customer.Metadata["AccountID"]
                if (customer.Email == _theUser.Email && _theUser.UserID.ToString() == customer.Metadata["UserID"].ToString()
                    && Session["AccountID"].ToString() == customer.Metadata["AccountID"].ToString())
                {
                    //stripeService.Delete(customer.Id);//delete this customer and create a new subscription
                    //this is not good, we need to get customer subscription info and update that subsciprtion




                }

            }
        }
        catch
        {
        }
    }
    protected void btnPayOneTime_Click(object sender, EventArgs e)
    {
        string strPaypalAmount = GetTotalPaymentAmount().ToString();
        Session["PaypalItemName"] = "DBGurus eContractor Payment";
        Session["PaymentAmount"] = strPaypalAmount == "" ? "1" : strPaypalAmount;
        //Session["PayIntervalType"] = "D";//M
        Session["InvoiceID"] = _theUser.UserID.ToString() + "_" + Session["AccountID"].ToString(); // a TDB Reference

        Response.Redirect("~/Pages/Security/PayPal/sendpayment.aspx", false);
    }
}

//invoice.created 
//string json = "{ \"id\": \"evt_1Bw5GTCtiZ7cOVF6xgqt8Y6b\", \"object\": \"event\", \"api_version\": \"2018-01-23\", \"created\": 1518772341, \"data\": { \"object\": { \"id\": \"in_1Bw5GSCtiZ7cOVF6hbilNex4\", \"object\": \"invoice\", \"amount_due\": 1500, \"application_fee\": null, \"attempt_count\": 0, \"attempted\": true, \"billing\": \"charge_automatically\", \"charge\": \"ch_1Bw5GSCtiZ7cOVF65TPIR0iZ\", \"closed\": true, \"currency\": \"aud\", \"customer\": \"cus_CKkm9ANE0Q4bWD\", \"date\": 1518772340, \"description\": null, \"discount\": null, \"due_date\": null, \"ending_balance\": 0, \"forgiven\": false, \"lines\": { \"object\": \"list\", \"data\": [ { \"id\": \"sub_CKkm6mS5ABycwK\", \"object\": \"line_item\", \"amount\": 1500, \"currency\": \"aud\", \"description\": \"1 × DBG Weekly (at $15.00 / week)\", \"discountable\": true, \"livemode\": false, \"metadata\": { }, \"period\": { \"start\": 1518772340, \"end\": 1519377140 }, \"plan\": { \"id\": \"Weekly1\", \"object\": \"plan\", \"amount\": 1500, \"created\": 1518579072, \"currency\": \"aud\", \"interval\": \"week\", \"interval_count\": 1, \"livemode\": false, \"metadata\": { }, \"nickname\": null, \"product\": \"prod_CJuonKqEmeoKeB\", \"trial_period_days\": null, \"statement_descriptor\": \"S d\", \"name\": \"DBG Weekly\" }, \"proration\": false, \"quantity\": 1, \"subscription\": null, \"subscription_item\": \"si_CKkmb03giTUHzF\", \"type\": \"subscription\" } ], \"has_more\": false, \"total_count\": 1, \"url\": \"/v1/invoices/in_1Bw5GSCtiZ7cOVF6hbilNex4/lines\" }, \"livemode\": false, \"metadata\": { }, \"next_payment_attempt\": null, \"number\": \"e723441f36-0001\", \"paid\": true, \"period_end\": 1518772340, \"period_start\": 1518772340, \"receipt_number\": null, \"starting_balance\": 0, \"statement_descriptor\": null, \"subscription\": \"sub_CKkm6mS5ABycwK\", \"subtotal\": 1500, \"tax\": null, \"tax_percent\": null, \"total\": 1500, \"webhooks_delivered_at\": null } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_DJi8ixXNZ3Mdyx\", \"idempotency_key\": null }, \"type\": \"invoice.created\" }";


//"invoice.payment_succeeded"
//string json = "{ \"id\": \"evt_1Bw5GTCtiZ7cOVF6yc14ga6y\", \"object\": \"event\", \"api_version\": \"2018-01-23\", \"created\": 1518772341, \"data\": { \"object\": { \"id\": \"in_1Bw5GSCtiZ7cOVF6hbilNex4\", \"object\": \"invoice\", \"amount_due\": 1500, \"application_fee\": null, \"attempt_count\": 0, \"attempted\": true, \"billing\": \"charge_automatically\", \"charge\": \"ch_1Bw5GSCtiZ7cOVF65TPIR0iZ\", \"closed\": true, \"currency\": \"aud\", \"customer\": \"cus_CKkm9ANE0Q4bWD\", \"date\": 1518772340, \"description\": null, \"discount\": null, \"due_date\": null, \"ending_balance\": 0, \"forgiven\": false, \"lines\": { \"object\": \"list\", \"data\": [ { \"id\": \"sub_CKkm6mS5ABycwK\", \"object\": \"line_item\", \"amount\": 1500, \"currency\": \"aud\", \"description\": \"1 × DBG Weekly (at $15.00 / week)\", \"discountable\": true, \"livemode\": false, \"metadata\": { }, \"period\": { \"start\": 1518772340, \"end\": 1519377140 }, \"plan\": { \"id\": \"Weekly1\", \"object\": \"plan\", \"amount\": 1500, \"created\": 1518579072, \"currency\": \"aud\", \"interval\": \"week\", \"interval_count\": 1, \"livemode\": false, \"metadata\": { }, \"nickname\": null, \"product\": \"prod_CJuonKqEmeoKeB\", \"trial_period_days\": null, \"statement_descriptor\": \"S d\", \"name\": \"DBG Weekly\" }, \"proration\": false, \"quantity\": 1, \"subscription\": null, \"subscription_item\": \"si_CKkmb03giTUHzF\", \"type\": \"subscription\" } ], \"has_more\": false, \"total_count\": 1, \"url\": \"/v1/invoices/in_1Bw5GSCtiZ7cOVF6hbilNex4/lines\" }, \"livemode\": false, \"metadata\": { }, \"next_payment_attempt\": null, \"number\": \"e723441f36-0001\", \"paid\": true, \"period_end\": 1518772340, \"period_start\": 1518772340, \"receipt_number\": null, \"starting_balance\": 0, \"statement_descriptor\": null, \"subscription\": \"sub_CKkm6mS5ABycwK\", \"subtotal\": 1500, \"tax\": null, \"tax_percent\": null, \"total\": 1500, \"webhooks_delivered_at\": null } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_DJi8ixXNZ3Mdyx\", \"idempotency_key\": null }, \"type\": \"invoice.payment_succeeded\" }";

//customer.created
//string json = "{ \"id\": \"evt_1Bw5GSCtiZ7cOVF6v4NjzD6M\", \"object\": \"event\", \"api_version\": \"2018-01-23\", \"created\": 1518772340, \"data\": { \"object\": { \"id\": \"cus_CKkm9ANE0Q4bWD\", \"object\": \"customer\", \"account_balance\": 0, \"created\": 1518772340, \"currency\": null, \"default_source\": null, \"delinquent\": false, \"description\": null, \"discount\": null, \"email\": \"a3@a.com\", \"invoice_prefix\": \"e723441f36\", \"livemode\": false, \"metadata\": { }, \"shipping\": null, \"sources\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_CKkm9ANE0Q4bWD/sources\" }, \"subscriptions\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_CKkm9ANE0Q4bWD/subscriptions\" } } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_ZYmFeq9r0yTRtb\", \"idempotency_key\": null }, \"type\": \"customer.created\" }";

//StripeEvent stripeEvent = null;
//stripeEvent = StripeEventUtility.ParseEvent(json);

//try
//{
//    switch (stripeEvent.Type)
//    {
//        //case "charge.succeeded":
//        //    // do work
//        //    StripeCharge sCharge = Mapper<StripeCharge>.MapFromJson(stripeEvent.Data.Object.ToString());

//        //    break;
//        case "invoice.created":
//            // do work
//            StripeInvoice sInvoiceC = Mapper<StripeInvoice>.MapFromJson(stripeEvent.Data.Object.ToString());

//            break;
//        case "customer.created":
//            StripeCustomer sCustomer = Mapper<StripeCustomer>.MapFromJson(stripeEvent.Data.Object.ToString());
//            if (sCustomer != null)
//            {

//            }
//            // do work
//            break;
//        case "invoice.payment_succeeded":
//            StripeInvoice sInvoicePS = Mapper<StripeInvoice>.MapFromJson(stripeEvent.Data.Object.ToString());
//            if (sInvoicePS != null)
//            {

//            }
//            // do work
//            break;
//        case "customer.subscription.updated":
//        case "customer.subscription.deleted":
//        case "customer.subscription.created":
//            // do work
//            break;
//    }
//}
//catch (Exception ex)
//{
//    string strErrorE = "";
//    //Common.SendSingleEmail("r_mohsin@yahoo.com", "Stripe Error--" + ex.Message, ex.StackTrace, ref strErrorE);
//}



// Application_Start
//
//try
//{
//    var secretKey = WebConfigurationManager.AppSettings["StripeSecretKey"];
//    StripeConfiguration.SetApiKey(secretKey);


//    var options = new StripePlanCreateOptions
//    {
//        Currency = "aud",
//        Interval = "month",
//        Name = "Monthly10",
//        Amount = 11,
//    };
//    var service = new StripePlanService();
//    StripePlan plan = service.Create(options);

//}
//catch
//{
//    //
//}
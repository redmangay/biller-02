﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PasswordResetRequest.aspx.cs"
    MasterPageFile="~/Home/Marketing.master" Inherits="Security_PasswordResetRequest" EnableTheming="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
   <link href="<%=ResolveUrl("~/fancybox3/dist/jquery.fancybox.min.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/fancybox3/dist/jquery.fancybox.min.js")%>"></script>

    <style type="text/css">
        .ui-dialog-titlebar {
            visibility: hidden;
        }

        #divGetSecurityQuestion {
            display: none;
        }

        a.btn {
            /*line-height: 27px;*/
            /*height: 37px;*/
            /*padding-left: 10px;
        padding-right: 10px;
        padding-top: 10px;
        padding-bottom: 10px;*/
            text-decoration: none;
            /*font-size: 14px;
        font-weight: bold;*/
            /*font-weight: 600;*/
            color: #FFF;
            /*padding: 8px16px;*/
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            background: rgb(0,155,162); /* Old browsers */
            /* IE9 SVG, needs conditional override of 'filter' to 'none' */
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwNzU3OCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMGIyYjIiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
            background: -moz-linear-gradient(top, rgb(0,155,162) 0%, rgb(0,117,120) 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(0,155,162)), color-stop(100%,rgb(0,117,120))); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, rgb(0,155,162) 0%,rgb(0,117,120) 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, rgb(0,155,162) 0%,rgb(0,117,120) 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, rgb(0,155,162) 0%,rgb(0,117,120) 100%); /* IE10+ */
            background: linear-gradient(to bottom, rgb(0,155,162) 0%,rgb(0,117,120) 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3abd0e', endColorstr='#2e960b',GradientType=0 ); /* IE6-8 */
        }

            a.btn:hover {
                color: #dcd6d6;
                background: rgb(0,117,120); /* Old browsers */
                /* IE9 SVG, needs conditional override of 'filter' to 'none' */
                background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwNzU3OCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMGIyYjIiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
                background: -moz-linear-gradient(top, rgb(0,117,120) 0%, rgb(0,155,162) 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(0,117,120)), color-stop(100%,rgb(0,155,162))); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top, rgb(0,117,120) 0%,rgb(0,155,162) 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(top, rgb(0,117,120) 0%,rgb(0,155,162) 100%); /* Opera 11.10+ */
                background: -ms-linear-gradient(top, rgb(0,117,120) 0%,rgb(0,155,162) 100%); /* IE10+ */
                background: linear-gradient(to bottom, rgb(0,117,120) 0%,rgb(0,155,162) 100%); /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2e960b', endColorstr='#3abd0e',GradientType=0 ); /* IE6-8 */
            }
    </style>

    <asp:Panel ID="pnlFull" runat="server" DefaultButton="lnkSendPassword">
        <%--red Sha1_512 Update--%>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table class="ContentMainTra" style="width: 100%;">
                    <tr>
                        <td width="28"></td>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" style="width: 50%;"></td>
                                    <td align="left">
                                        <div style="width: 40px; height: 40px;">
                                            <asp:UpdateProgress class="ajax-indicator" ID="UpdateProgress1" runat="server">
                                                <ProgressTemplate>
                                                    <table style="width: 100%; text-align: center">
                                                        <tr>
                                                            <td>
                                                                <img alt="Processing..." src="../Images/ajax.gif" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                        </div>
                                    </td>
                                    <td align="right">
                                        <asp:HyperLink runat="server" Visible="false" ID="hlHelpCommon" ClientIDMode="Static"
                                            NavigateUrl="~/Pages/Help/Help.aspx?contentkey=PasswordReminderHelp">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/App_Themes/Default/images/help.png" />

                                        </asp:HyperLink>
                                        <asp:HiddenField ID="hfMemDate" ClientIDMode="Static" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="28"></td>
                    </tr>


                    <tr>
                        <td width="28">&nbsp;</td>
                        <td align="center">
                            <span class="TopTitle" style="font-size: x-large;">Password Reset</span>
                        </td>
                        <td width="28">&nbsp;</td>
                    </tr>

                    <tr>
                        <td colspan="3" style="height: 30px;"></td>
                    </tr>
                    <tr>
                        <td width="28"></td>
                        <td align="center">Please enter your email address and we&#39;ll send you a link to reset your password. 
                        <br />
                            <br />
                        </td>
                        <td width="28"></td>
                    </tr>
                    <tr id="trlblEmailStatus" runat="server">
                        <td width="28"></td>
                        <td align="center" height="20px">
                            <asp:Label ID="lblEmailStatus" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td width="28"></td>
                    </tr>
                    <tr>
                        <td width="28"></td>
                        <td align="center" height="22px">
                            <asp:TextBox ID="txtEmail" CssClass="form-control" MaxLength="200" runat="server" Style="min-width: 200px; max-width: 300px;"></asp:TextBox>
                            <br />
                            <br />
                        </td>
                        <td width="28"></td>
                    </tr>
                    <tr>
                        <td width="28"></td>
                        <td align="center" height="40px">
                            <div>
                                <table>
                                    <tr>

                                        <td>
                                            <div>
                                                <asp:LinkButton runat="server" ID="lnkReturn" OnClick="lnkReturn_Click" CssClass="btn"> <strong>Return</strong> </asp:LinkButton>
                                            </div>
                                        </td>
                                        <td style="width: 20px;"></td>
                                        <td>
                                            <div runat="server" id="divSave">
                                                <asp:LinkButton runat="server" ID="lnkSendPassword" CssClass="btn" OnClick="lnkSendPassword_Click"> <strong>Send Link</strong> </asp:LinkButton>
                                            </div>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td width="28"></td>
                    </tr>
                    <tr>
                        <td width="28"></td>
                        <td height="20"></td>
                        <td width="28"></td>
                    </tr>
                </table>
                <div id="divGetSecurityQuestion">

                    <table width="100%">
                        <tr>
                            <td>
                                <div style="text-align: center">
                                    <%--  <h2 style="text margin-top: 0px; color: #000000;" >Security Question</h2>                      --%>
                                    <span style="font-size: small">To tighten up security further, please enter your memorable date below to confirm you identify:</span>
                                    <br />
                                    <br />
                                </div>


                            </td>
                        </tr>

                        <tr id="trlblSecurityQuestion" runat="server" align="center">
                            <td style="margin-top: 10px">
                                <div style="text-align: center">
                                    <asp:Label ID="lblSecurityQuestion" Font-Size="Small" runat="server" Text="What is the name of you first pet? "></asp:Label>
                            </td>


                        </tr>
                        <tr align="center">
                            <td>&nbsp;                                                                        
                                            <%--//red 06092017--%>
                                <asp:TextBox runat="server" ID="txtMemDate" ClientIDMode="Static" Width="100px" ValidationGroup="MKE"
                                    BorderStyle="Solid" BorderColor="#909090"
                                    BorderWidth="1" />
                                <asp:ImageButton runat="server" ClientIDMode="Static" ID="ibMemDate" ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false" />
                                <ajaxToolkit:CalendarExtender ID="ce_MemDate" runat="server" TargetControlID="txtMemDate"
                                    Format="dd/MM/yyyy" PopupButtonID="ibMemDate" FirstDayOfWeek="Monday">
                                </ajaxToolkit:CalendarExtender>

                                <asp:RangeValidator ID="rngMemDate" runat="server" ControlToValidate="txtMemDate"
                                    ValidationGroup="MKE" ErrorMessage="*" Font-Bold="true" Display="Dynamic" Type="Date"
                                    MinimumValue="1/1/1753" MaximumValue="1/1/3000"></asp:RangeValidator>

                                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" TargetControlID="txtMemDate" WatermarkText="dd/mm/yyyy" runat="server" WatermarkCssClass="MaskText"></ajaxToolkit:TextBoxWatermarkExtender>

                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr id="trlblIncorrectAnswer" runat="server">
                            <td align="center" height="20px">
                                <asp:Label ID="lblIncorrectAnswer" ClientIDMode="Static" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                                <br />
                                <br />
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <div style="text-align: center">

                                    <asp:LinkButton runat="server" ID="lnkContinue" OnClientClick="GetMemDate();" OnClick="lnkContinue_Click" CssClass="btn btn-primary"
                                        CausesValidation="false" ClientIDMode="Static" ForeColor="White"><strong>Continue</strong></asp:LinkButton>

                                    &nbsp;
                                  <asp:LinkButton runat="server" ID="lnkCloseDoNothing" OnClientClick="GetSecurityQuestionClose(); " CssClass="btn btn-primary"
                                      ClientIDMode="Static" CausesValidation="false" ForeColor="White"><strong>Cancel</strong> </asp:LinkButton>

                                </div>

                            </td>
                        </tr>
                    </table>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <%--red Sha1_512 Update--%>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;


public partial class Security_PasswordReset : System.Web.UI.Page
{
    User theUser;
    int iRequestedID;
    protected void Page_Load(object sender, EventArgs e)
    {
        hlBack.Visible = false; //red Sha1_512 Update
        DataTable dtPasswordReset = null;
        if (Request.QueryString["iRequestID"] != null)
        {
            string strRequestID = Cryptography.Decrypt(Request.QueryString["iRequestID"].ToString());
            if (strRequestID != "")
            {
                iRequestedID = int.Parse(Cryptography.Decrypt(Request.QueryString["iRequestID"].ToString()));
                dtPasswordReset = SecurityManager.User_GetPasswordReset(iRequestedID);

            }
            if (dtPasswordReset == null || dtPasswordReset.Rows.Count == 0 || strRequestID == "")
            {
                //Session.Clear();
                //Response.Redirect(Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Login.aspx?AccessDenied=" + Server.UrlEncode(Request.RawUrl), false);
                trlblpassword.Visible = false;
                trPassword.Visible = false;
                trlblConfirm.Visible = false;
                trConformPassword.Visible = false;
                trButtons.Visible = false;
                trlblTitle.Visible = false;
                trlblTerminated.Visible = true;
                trlblMeg.Visible = true;
                lblMeg.Text = "Sorry but the page you are trying to access has been expired.";

                if (strRequestID == "")
                {
                    lblMeg.Text = "Sorry but there is something wrong with the link you are trying to access. Please try again and make sure the link is correct. <br > <br >";
                }
            }
            else
            {
                foreach (DataRow dr in dtPasswordReset.Rows)
                {
                    theUser = SecurityManager.User_Details(int.Parse(dr["UserID"].ToString()));
                }
                trlblTerminated.Visible = false;
                trlblSecurityQuestion.Visible = false;
                trlblMeg.Visible = false;
            }


        }
        else
        {
            Session.Clear();
            Response.Redirect(Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Login.aspx?AccessDenied=" + Server.UrlEncode(Request.RawUrl), false);
        }



        Title = "Password Reset";


        if (!IsPostBack)
        {
            Password.Text = "";
            ConformPassword.Text = "";
        }

        string strHelpJS = @" $(function () {
            $('#hlHelpCommon').fancybox({
                iframe : {
                    css : {
                        width : '600px',
                        height: '350px'
                    }
                },       
                toolbar  : false,
	            smallBtn : true, 
                scrolling: 'auto',
                type: 'iframe',
                'transitionIn': 'elastic',
                'transitionOut': 'none',
                titleShow: false
            });
        });";


        //ScriptManager.RegisterStartupScript(this, this.GetType(), "HelpJS", strHelpJS, true);

        string strSecurityQuestion = @" function GetSecurityQuestion() {                               
                            $('#divGetSecurityQuestion').dialog({   
                            title: ""Warning Log-off"",        
                            width: 400,
                            height: 300,
                            modal: true, 

                            close: function() {
                           
                                    }
                                    });
       
                            }

                        function GetSecurityQuestionClose() {                
                             $(""#divGetTwoFactorAuthenticator"").dialog('close');                               
                            }
                            
                                function GetMemDate() {
                                $(""#hfMemDate"").val($(""#txtMemDate"").val())
                                $(""#lblIncorrectAnswer"").show();
                                document.getElementById('lblIncorrectAnswer').innerText = ""Validating..."";
                                document.getElementById('hfMemDate').value = document.getElementById('txtMemDate').value
                                }

";
        //red Ticket 3059_4
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "SecurityQuestionJS", strSecurityQuestion, true);


    }


    //protected void cmdSave_Click(object sender, ImageClickEventArgs e)



    protected void lnkSave_Click(object sender, EventArgs e)
    {
        //  if (Request.QueryString["iRequestID"] != null)
        // {
        //int iRequestedID = int.Parse(Cryptography.Decrypt(Request.QueryString["iRequestID"].ToString()));
        //DataTable dtPasswordReset = SecurityManager.User_GetPasswordReset(iRequestedID);
        //if (dtPasswordReset != null || dtPasswordReset.Rows.Count > 0)
        //{
        //    foreach (DataRow dr in dtPasswordReset.Rows)
        //    {
        //        theUser = SecurityManager.User_Details(int.Parse(dr["UserID"].ToString()));
        //    }
        //}
        //   }

        //Trap empty
        //lblMeg.Text = "";
        //if (Session["DemoEmail"] != null)
        //{
        //    if (_ObjUser.Email.ToLower() == Session["DemoEmail"].ToString().ToLower())
        //    {
        //        lblMeg.Text = Common.DemoReadyOnlyMsg;
        //        return;

        //    }
        //}


        if (Password.Text.Trim().Length < 6)
        {
            //red Sha1_512 Update
            //lblMeg.Text = "Password Minimum length is 6.";
            Session["tdbmsgpb"] = "Password Minimum length is 6.";
            Password.Focus();
            return;
        }

        if (Password.Text.Trim() != ConformPassword.Text.Trim())
        {
            //red Sha1_512 Update
            Session["tdbmsgpb"] = "New & Confirm password should be same!";
            //lblMeg.Text = "New & Confirm password should be same!";
            return;
        }

        if (Password.Text.ToLower().IndexOf(theUser.FirstName.ToLower()) > 0)
        {
            //red Sha1_512 Update
            Session["tdbmsgpb"] = "Password should not have first name!";
            //lblMeg.Text = "Password should not have first name!";
            Password.Focus();
            return;
        }
        if (Password.Text.ToLower().IndexOf(theUser.LastName.ToLower()) > 0)
        {
            //red Sha1_512 Update
            Session["tdbmsgpb"] = "Password should not have last name!";
            //lblMeg.Text = "Password should not have last name!";
            Password.Focus();
            return;
        }
        if (theUser.Email.IndexOf("@") > 0)
        {
            if (Password.Text.ToLower().IndexOf(theUser.Email.ToLower().Substring(0, theUser.Email.IndexOf("@"))) > 0)
            {
                //red Sha1_512 Update
                Session["tdbmsgpb"] = "Password should not have email address!";
                //lblMeg.Text = "Password should not have email address!";
                Password.Focus();
                return;
            }
        }

        if (Password.Text.Length == 0)
        {
            //red Sha1_512 Update
            Session["tdbmsgpb"] = "Please enter your password.";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "PassEmptyErr", "alert('" + "Please enter your password." + "');", true);
        }
        else
        {
            ViewState["Password"] = Password.Text;
            //lets reset the user password
            int iResult = 0;
            // theUser.EncryptedPassword = ViewState["Password"].ToString();
            iResult = SecurityManager.User_UpdatePassword((int)theUser.UserID, ViewState["Password"].ToString());

            if (iResult > 0)
            {
                trlblpassword.Visible = false;
                trPassword.Visible = false;
                trlblConfirm.Visible = false;
                trConformPassword.Visible = false;
                trButtons.Visible = false;
                trlblMeg.Visible = true;
                //lblMeg.Text = "Reset password successfully!";
                Session["tdbmsgpb"] = "Password reset successfully!"; //red Sha1_512 Update
                //lets reset the password reset request
                Common.ExecuteText("UPDATE [UserPasswordReset] SET [IsActive] = 0 WHERE [UserID]=" + theUser.UserID);
                ViewState["Password"] = null;
            }
            //ScriptManager.RegisterStartupScript(this, GetType(), "displaySecurityQuestion", "GetSecurityQuestion();", true);
        }
    }

    protected void lnkContinue_Click(object sender, EventArgs e)
    {
        // User theUser = SecurityManager.User_Details()
        User theUserValidateMemDate = SecurityManager.User_ValidateMemorableDate(theUser.Email, hfMemDate.Value);
        hfMemDate.Value = "";
        if (theUserValidateMemDate != null)
        {
            //lets reset the user password
            int iResult = 0;
            // theUser.EncryptedPassword = ViewState["Password"].ToString();
            iResult = SecurityManager.User_UpdatePassword((int)theUser.UserID, ViewState["Password"].ToString());

            if (iResult > 0)
            {
                trlblpassword.Visible = false;
                trPassword.Visible = false;
                trlblConfirm.Visible = false;
                trConformPassword.Visible = false;
                trButtons.Visible = false;
                trlblMeg.Visible = true;
                lblMeg.Text = "Reset password successfully!";

                //lets reset the password reset request
                Common.ExecuteText("UPDATE [UserPasswordReset] SET [IsActive] = 0 WHERE [UserID]=" + theUser.UserID);
                ViewState["Password"] = null;
            }

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "displayDoNothing", "GetSecurityQuestion();", true);
            lblIncorrectAnswer.Visible = true;
            lblIncorrectAnswer.Text = "Invalid memorable date, please try again.";
        }
    }

}

﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
//using System.Data.Linq;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;

public partial class Security_ChangePassword : SecurePage
{
    //LFDataContext srcLF = new LFDataContext();
    User _ObjUser;
    int? _iUserID;
    bool IsCurrentUser = false;
    bool IsTempPassword = true;
    bool _bResponsive = false;
    protected void Page_PreInit(object sender, EventArgs e)
    {
        _ObjUser = (User)Session["User"];

        

        //if (_ObjUser.LoginCount == 0) //red Sha1_512 Update removed
        //{
        //   Session["LoginCount"] = _ObjUser.LoginCount;
        //    this.MasterPageFile = "~/Home/Blank.master";
        //}
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Page.MasterPageFile != null && Page.MasterPageFile.IndexOf("Responsive") > -1)
            _bResponsive = true;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "Change Password";
        hlHelpCommon.Visible = false; //red Sha1_512 Update

        if (_bResponsive)
        {
            OldPassword.Height = 20;
            Password.Height = 20;
        }
        if (_ObjUser != null && _ObjUser.UserID > 0)
        {
            lblUserName.Text = _ObjUser.FirstName + " " + _ObjUser.LastName;
        }
        else
        {
            if (Session["LoginAccount"] == null)
            {
                Response.Redirect("~/Login.aspx", false);
            }
            else
            {
                Response.Redirect("~/Login.aspx?" + Session["LoginAccount"].ToString(), false);
            }
            return;
        }

        if (!IsPostBack)
        {
            if (Request.UrlReferrer != null)
            {
                hlBack.NavigateUrl = Request.UrlReferrer.AbsoluteUri;
            }

            OldPassword.Text = "";
            Password.Text = "";
            //ConformPassword.Text = "";
        }

//        string strHelpJS = @" $(function () {
//            $('#hlHelpCommon').fancybox({
//                scrolling: 'auto',
//                type: 'iframe',
//                'transitionIn': 'elastic',
//                'transitionOut': 'none',
//                width: 600,
//                height: 350,
//                titleShow: false
//            });
//        });";


//        ScriptManager.RegisterStartupScript(this, this.GetType(), "HelpJS", strHelpJS, true);

        if (Request.QueryString["UserID"] != null)
        {
            _iUserID = int.Parse(Cryptography.Decrypt(Request.QueryString["UserID"].ToString()));

            if (Common.HaveAccess(Session["roletype"].ToString(), "1,2") && _iUserID != null && _iUserID != _ObjUser.UserID) //red Sha1_512 Update
            {
                IsCurrentUser = true;
                trlblOldPassword.Visible = false;
                trtxtOldPassword.Visible = false;
                lblUserName.Text = Cryptography.Decrypt(Request.QueryString["FName"].ToString()) + " " + Cryptography.Decrypt(Request.QueryString["LName"].ToString());
                lblNewPassword.Text = "Please enter the NEW password below:";
                //lblConfirmPassword.Text = "Please enter the NEW password AGAIN to confirm:";
                //RP Added - Ticket 4930 - Reset Password - Next Time
                //lnkNextTime.Visible = true;
                //End Modification
            }


        }

        if (Request.QueryString["QResetPassword"] != null)
        {
            string strQResetPasswordUserID = Cryptography.Decrypt(Request.QueryString["QResetPassword"].ToString());
            string strUserID = Cryptography.Decrypt(Request.QueryString["UserID"].ToString());

            if (int.Parse(strQResetPasswordUserID) == _ObjUser.UserID && int.Parse(strUserID) == _ObjUser.UserID)
            {
                IsCurrentUser = true;
                IsTempPassword = false;
                trlblOldPassword.Visible = false;
                trtxtOldPassword.Visible = false;
                lnkNextTime.Visible = true;
                // lblUserName.Text = Cryptography.Decrypt(Request.QueryString["FName"].ToString()) + " " + Cryptography.Decrypt(Request.QueryString["LName"].ToString());
                // lblNewPassword.Text = "Please enter the NEW password below:";

            }


        }




    }


    //protected void cmdSave_Click(object sender, ImageClickEventArgs e)

    protected void lnkSave_Click(object sender, EventArgs e)
    {
        //Trap empty
        lblMeg.Text = "";
        if (Session["DemoEmail"] != null)
        {
            if (_ObjUser.Email.ToLower() == Session["DemoEmail"].ToString().ToLower())
            {
                lblMeg.Text = Common.DemoReadyOnlyMsg;
                return;

            }
        }


        if (Password.Text.Trim().Length < 6)
        {
            //red Sha1_512 Update
            //lblMeg.Text = "Password Minimum length is 6.";
            Session["tdbmsgpb"] = "Password Minimum length is 6.";
            Password.Focus();
            return;
        }

        //if (Password.Text.Trim() != ConformPassword.Text.Trim())
        //{
        //    //red Sha1_512 Update
        //    Session["tdbmsgpb"] = "New & Confirm password should be same!";
        //    //lblMeg.Text = "New & Confirm password should be same!";
        //    return;
        //}

        if (Password.Text.ToLower().IndexOf(_ObjUser.FirstName.ToLower()) > 0)
        {
            //red Sha1_512 Update
            Session["tdbmsgpb"] = "Password should not have first name!";
            //lblMeg.Text = "Password should not have first name!";
            Password.Focus();
            return;
        }
        if (Password.Text.ToLower().IndexOf(_ObjUser.LastName.ToLower()) > 0)
        {
            //red Sha1_512 Update
            Session["tdbmsgpb"] = "Password should not have last name!";
            //lblMeg.Text = "Password should not have last name!";
            Password.Focus();
            return;
        }
        if (_ObjUser.Email.IndexOf("@") > 0)
        {
            if (Password.Text.ToLower().IndexOf(_ObjUser.Email.ToLower().Substring(0, _ObjUser.Email.IndexOf("@"))) > 0)
            {
                //red Sha1_512 Update
                Session["tdbmsgpb"] = "Password should not have email address!";
                //lblMeg.Text = "Password should not have email address!";
                Password.Focus();
                return;
            }
        }


        if (Password.Text.Length == 0)
        {
            //red Sha1_512 Update
            Session["tdbmsgpb"] = "Please enter your password.";
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "PassEmptyErr", "alert('" + "Please enter your password." + "');", true);

        }
        else
        {
            if (_iUserID != null && IsCurrentUser) //red Sha1_512 Update
            {
                int _UserID = int.Parse(_iUserID.ToString());
                int iResetPasswordStat = 0;
                iResetPasswordStat = SecurityManager.User_UpdatePassword(_UserID, Password.Text);

                if (iResetPasswordStat > 0)
                { //change help...

                    // if (IsTempPassword)
                    //{
                    // Common.ExecuteText("UPDATE [User] SET [IsTempPassword]=1, [DateUpdated]= Getdate() WHERE [UserID]=" + _UserID);
                    //} else
                    // {
                    //Common.ExecuteText("UPDATE [User] SET [IsTempPassword]=1, [DateUpdated]= Getdate() WHERE [UserID]=" + _UserID);
                    // }
                    int iTempPassword = IsTempPassword ? 1 : 0;
                    Common.ExecuteText("UPDATE [User] SET [IsTempPassword]=" + iTempPassword + ", [DateUpdated]= Getdate() WHERE [UserID]=" + _UserID);

                    Session["tdbmsgpb"] = "Password reset successfully!"; //red Ticket 3059_3

                    string url = "/Default.aspx";
                    if (iTempPassword != 0)
                    {
                        url = hlBack.NavigateUrl.Replace("~", "");
                    }
                    else
                    {
                        
                        SecurityManager.User_LoginCount_Increment((int)_ObjUser.UserID);
                        _ObjUser.LoginCount++;
                        _ObjUser.IsTempPassword = false;
                        Session["User"] = _ObjUser;
                        Session["LoginCount"] = _ObjUser.LoginCount;
                    }


                    string script = "setTimeout(function(){";
                    script += "window.location = '";
                    script += url;
                    script += "'; }, 3000)";

                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "alert", script, true);

                }
                else
                {
                    Session["tdbmsgpb"] = "Password reset not successful."; //red Ticket 3059_3
                }
            }
            else
            {


                int result = 0;

                result = SecurityManager.ChangePassword(OldPassword.Text.Trim(), Password.Text.Trim(), (int)_ObjUser.UserID);

                if (result > 0)
                {
                    //it is not temp anymore
                    Common.ExecuteText("UPDATE [User] SET [IsTempPassword]=0, [DateUpdated]= Getdate() WHERE [UserID]=" + _ObjUser.UserID);
                    Session["tdbmsgpb"] = "Password reset successfully!"; //red Ticket 3059_3

                    string url = "/Default.aspx";
                    if (_ObjUser.LoginCount != 0)
                    {
                        url = hlBack.NavigateUrl.Replace("~", "");
                    }
                    else
                    {
                        SecurityManager.User_LoginCount_Increment((int)_ObjUser.UserID);
                        _ObjUser.LoginCount++;
                        _ObjUser.IsTempPassword = false;
                        Session["User"] = _ObjUser;
                        Session["LoginCount"] = _ObjUser.LoginCount;
                    }
                    //string message = "Password reset successfully!";
                    //string script = "{ alert('";
                    //script += message;
                    //script += "');";
                    //script += "window.location = '";
                    //script += url;
                    //script += "'; }";
                    string script = "setTimeout(function(){";
                    //script += "');";
                    script += "window.location = '";
                    script += url;
                    script += "'; }, 2000)";

                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "alert", script, true);
                }

                else
                {
                    Session["tdbmsgpb"] = "Invalid Old Password."; //red Ticket 3059_3
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Old Password", "alert('" + "Invalid Old Password." + "');", true);
                }
            }
        }
    }


    //protected  int ChangePassword(string strOldPassword, string strNewPassword, int iUserID)
    //{
    //    using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
    //    {
    //        using (SqlCommand command = new SqlCommand("ChangePassword", connection))
    //        {

    //            command.CommandType = CommandType.StoredProcedure;
    //            SqlParameter pRV = new SqlParameter("@Result", SqlDbType.Int);
    //            pRV.Direction = ParameterDirection.Output;

    //            command.Parameters.Add(pRV);
    //            command.Parameters.Add(new SqlParameter("@OldPassword", strOldPassword));
    //            command.Parameters.Add(new SqlParameter("@NewPassword", strNewPassword));
    //            command.Parameters.Add(new SqlParameter("@nUserID", iUserID));


    //            connection.Open();
    //            try
    //            {
    //                command.ExecuteNonQuery();
    //                connection.Close();
    //                connection.Dispose();
    //                return int.Parse(pRV.Value.ToString());
    //            }
    //            catch
    //            {
    //                connection.Close();
    //                connection.Dispose();

    //            }
    //            return -1;
    //        }
    //    }
    //}



    //protected void lnkCancel_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("~/Default.aspx", false);
    //}


    //RP Added Ticket 4930 - Reset Password - Next Time
    protected void lnkNextTime_Click(object sender, EventArgs e)
    {
        string url = "/Default.aspx";

        SecurityManager.User_LoginCount_Increment((int)_ObjUser.UserID);
        _ObjUser.LoginCount++;
        _ObjUser.IsTempPassword = false;
        Session["User"] = _ObjUser;
        Session["LoginCount"] = _ObjUser.LoginCount;

        //string script = "setTimeout(function(){";
        //script += "window.location = '";
        //script += url;
        //script += "'; }, 3000)";

        //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "alert", script, true);
        Response.Redirect(url);
    }
    //End Modification
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PasswordReset.aspx.cs"
    MasterPageFile="~/Home/Marketing.master" EnableViewState="true" Inherits="Security_PasswordReset" EnableTheming="true" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <style type="text/css">
        .ui-dialog-titlebar {
            visibility: hidden;
        }

        #divGetSecurityQuestion {
            display: none;
        }

        a.btn {
            /*line-height: 27px;*/
            /*height: 37px;*/
            /*padding-left: 10px;
        padding-right: 10px;
        padding-top: 10px;
        padding-bottom: 10px;*/
            text-decoration: none;
            /*font-size: 14px;
        font-weight: bold;*/
            /*font-weight: 600;*/
            color: #FFF;
            /*padding: 8px16px;*/
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            background: rgb(0,155,162); /* Old browsers */
            /* IE9 SVG, needs conditional override of 'filter' to 'none' */
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwNzU3OCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMGIyYjIiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
            background: -moz-linear-gradient(top, rgb(0,155,162) 0%, rgb(0,117,120) 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(0,155,162)), color-stop(100%,rgb(0,117,120))); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, rgb(0,155,162) 0%,rgb(0,117,120) 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, rgb(0,155,162) 0%,rgb(0,117,120) 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, rgb(0,155,162) 0%,rgb(0,117,120) 100%); /* IE10+ */
            background: linear-gradient(to bottom, rgb(0,155,162) 0%,rgb(0,117,120) 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3abd0e', endColorstr='#2e960b',GradientType=0 ); /* IE6-8 */
        }

            a.btn:hover {
                color: #dcd6d6;
                background: rgb(0,117,120); /* Old browsers */
                /* IE9 SVG, needs conditional override of 'filter' to 'none' */
                background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzAwNzU3OCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMGIyYjIiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
                background: -moz-linear-gradient(top, rgb(0,117,120) 0%, rgb(0,155,162) 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(0,117,120)), color-stop(100%,rgb(0,155,162))); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top, rgb(0,117,120) 0%,rgb(0,155,162) 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(top, rgb(0,117,120) 0%,rgb(0,155,162) 100%); /* Opera 11.10+ */
                background: -ms-linear-gradient(top, rgb(0,117,120) 0%,rgb(0,155,162) 100%); /* IE10+ */
                background: linear-gradient(to bottom, rgb(0,117,120) 0%,rgb(0,155,162) 100%); /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2e960b', endColorstr='#3abd0e',GradientType=0 ); /* IE6-8 */
            }
    </style>


    <link href="<%=ResolveUrl("~/fancybox3/dist/jquery.fancybox.min.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/fancybox3/dist/jquery.fancybox.min.js")%>"></script>


    <script type="text/javascript">
        function validatePassword(validator, arg) {
            var textBox1 = document.getElementById('<%=Password.ClientID %>');
            var textBox2 = document.getElementById('<%=ConformPassword.ClientID %>');
            if (textBox1.value == textBox2.value)
                arg.IsValid = true; //Valid Value   
            else
                arg.IsValid = false; //Invalid Value   
        }


</script>
<asp:Panel ID="pnlFull" runat="server" DefaultButton="lnkSave">
        <%--red Sha1_512 Update--%>
        <div s class="ContentMainTra">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table border="0" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td width="28"></td>
                            <td colspan="2">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr >
                                        <td align="left" style="width: 50%;"></td>
                                        <td align="right">
                                            <asp:HyperLink runat="server" Visible="false" ID="hlHelpCommon" ClientIDMode="Static"
                                                NavigateUrl="~/Pages/Help/Help.aspx?contentkey=ChangePasswordHelp">
                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/App_Themes/Default/images/help.png" />
                                            </asp:HyperLink>
                                            <asp:HiddenField ID="hfMemDate" ClientIDMode="Static" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="28"></td>
                        </tr>
                        <tr>
                            <td width="28" rowspan="3"></td>
                            <td valign="top"></div>
                            <div id="content1">
                                <%-- <table border="0" cellpadding="0" cellspacing="0" width="100%" style="height: 400px">
                                    <tr>
                                        <td width="615" style="padding-left: 10px" valign="top">--%>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="padding-right: 6px" valign="top">
                                            <table border="0" align="left" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td height="30">
                                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                                                            ValidationGroup="reg" ShowMessageBox="true" ShowSummary="false" HeaderText="Please correct following errors:" Height="81px" />
                                                    </td>
                                                </tr>
                                                <tr id="trlblTitle" runat="server">
                                                    <td style="text-align: center;">
                                                        <span style="font-size: large;">Reset Password</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" height="5">&nbsp;</td>
                                                </tr>
                                                <tr id="trlblTerminated" runat="server">
                                                    <td height="5" align="center">
                                                        <h1>Access Denied</h1>
                                                    </td>
                                                </tr>
                                                <tr id="trlblpassword" runat="server">
                                                    <td height="5" align="center">Please enter your NEW password below:
                                                    </td>
                                                </tr>
                                                <tr id="trPassword" runat="server">
                                                    <td align="center" height="5">
                                                        <asp:TextBox ID="Password" runat="server" ClientIDMode="Static" CssClass="form-control" MaxLength="30" TextMode="Password" Style="min-width: 200px; max-width: 300px;"></asp:TextBox>
                                                        <ajaxToolkit:PasswordStrength ID="PS" runat="server" CalculationWeightings="50;15;15;20" DisplayPosition="RightSide" MinimumLowerCaseCharacters="2" MinimumNumericCharacters="2" MinimumSymbolCharacters="2" MinimumUpperCaseCharacters="1" PreferredPasswordLength="20" PrefixText="Strength:" RequiresUpperAndLowerCaseCharacters="true" StrengthIndicatorType="Text" TargetControlID="Password" TextCssClass="TextIndicator" TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent" TextStrengthDescriptionStyles="TextIndicator_Strength1;TextIndicator_Strength2;TextIndicator_Strength3;TextIndicator_Strength4;TextIndicator_Strength5" />
                                                        <br />
                                                        <asp:RegularExpressionValidator ID="revPasswordLength" runat="server" ControlToValidate="Password" Display="None" ErrorMessage="Password Minimum length is 6." ValidationExpression=".{6,30}" ValidationGroup="reg">
                                                        </asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator ID="RFVPassword" runat="server" ControlToValidate="Password" Display="None" ErrorMessage="New Password is required." ValidationGroup="reg"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>

                                               
                                                <tr id="trlblConfirm" runat="server">
                                                    <td align="center" height="5">Please enter your NEW password AGAIN to confirm:
                                                    </td>
                                                </tr>
                                                <tr id="trConformPassword" runat="server">
                                                    <td align="center" height="5">
                                                        <asp:TextBox ID="ConformPassword" runat="server" ClientIDMode="Static" CssClass="form-control" TextMode="Password" Style="min-width: 200px; max-width: 300px;"></asp:TextBox>
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="RfvConformPassword" runat="server" ControlToValidate="ConformPassword" Display="None" ErrorMessage="Confirm Password is required." ValidationGroup="reg"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" height="5">
                                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="validatePassword" Display="None" ErrorMessage="New and Confirm Passwords need to be same!" ValidationGroup="reg"></asp:CustomValidator>
                                                    </td>
                                                </tr>
                                                <tr id="trlblMeg" runat="server">
                                                    <td align="center" height="5">
                                                        <asp:Label ID="lblMeg" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                        &nbsp;<asp:HyperLink ID="hlGoHome" Font-Underline="true" NavigateUrl="~/Login.aspx" runat="server">Click this link to return to login page</asp:HyperLink>
                                                    </td>
                                                </tr>
                                               
                                                <tr id="trButtons" runat="server">
                                                    <td align="center" height="5">
                                                        <asp:HyperLink ID="hlBack" runat="server" CssClass="btn"> <strong>Cancel</strong> </asp:HyperLink>
                                                        &nbsp;<asp:LinkButton ID="lnkSave" runat="server" CausesValidation="true" CssClass="btn" OnClick="lnkSave_Click" ValidationGroup="reg"> <strong>Reset Password</strong> </asp:LinkButton>
                                                    </td>
                                                </tr>
                                                <tr >
                                                    <td align="center" height="25">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <%--  </td>
                                    </tr>
                                </table>--%>
                            </div>
                            </td>
                            <td width="27" rowspan="3"></td>
                            <td width="28" rowspan="3"></td>
                        </tr>
                    </table>
                    <div id="divGetSecurityQuestion">

                        <table width="100%">
                            <tr>
                                <td>
                                    <div style="text-align: center">
                                        <%--  <h2 style="text margin-top: 0px; color: #000000;" >Security Question</h2>                      --%>
                                        <span style="font-size: small">Please enter your Memorable Date as extra security:</span>
                                        <br />
                                        <br />
                                    </div>


                                </td>
                            </tr>

                            <tr id="trlblSecurityQuestion" runat="server" align="center">
                                <td style="margin-top: 10px">
                                    <div style="text-align: center">
                                        <asp:Label ID="lblSecurityQuestion" Font-Size="Small" runat="server" Text="What is the name of you first pet? "></asp:Label>
                                </td>


                            </tr>
                            <tr align="center">
                                <td>&nbsp;                                                                        
                                            <%--//red 06092017--%>
                                    <asp:TextBox runat="server" ID="txtMemDate" ClientIDMode="Static" Width="100px" ValidationGroup="MKE"
                                        BorderStyle="Solid" BorderColor="#909090"
                                        BorderWidth="1" />
                                    <asp:ImageButton runat="server" ClientIDMode="Static" ID="ibMemDate" ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false" />
                                    <ajaxToolkit:CalendarExtender ID="ce_MemDate" runat="server" TargetControlID="txtMemDate"
                                        Format="dd/MM/yyyy" PopupButtonID="ibMemDate" FirstDayOfWeek="Monday">
                                    </ajaxToolkit:CalendarExtender>

                                    <asp:RangeValidator ID="rngMemDate" runat="server" ControlToValidate="txtMemDate"
                                        ValidationGroup="MKE" ErrorMessage="*" Font-Bold="true" Display="Dynamic" Type="Date"
                                        MinimumValue="1/1/1753" MaximumValue="1/1/3000"></asp:RangeValidator>

                                    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" TargetControlID="txtMemDate" WatermarkText="dd/mm/yyyy" runat="server" WatermarkCssClass="MaskText"></ajaxToolkit:TextBoxWatermarkExtender>

                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr id="trlblIncorrectAnswer" runat="server">
                                <td align="center" height="20px">
                                    <asp:Label ID="lblIncorrectAnswer" ClientIDMode="Static" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                                    <br />
                                    <br />
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <div style="text-align: center">

                                        <asp:LinkButton runat="server" ID="lnkContinue" OnClientClick="GetMemDate();" OnClick="lnkContinue_Click" CssClass="btn btn-primary"
                                            CausesValidation="false" ClientIDMode="Static" ForeColor="White"><strong>Continue</strong></asp:LinkButton>

                                        &nbsp;
                                  <asp:LinkButton runat="server" ID="lnkCloseDoNothing" OnClientClick="GetSecurityQuestionClose(); " CssClass="btn btn-primary"
                                      ClientIDMode="Static" CausesValidation="false" ForeColor="White"><strong>Cancel</strong> </asp:LinkButton>

                                    </div>

                                </td>
                            </tr>
                        </table>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </asp:Panel>
    <%--red Sha1_512 Update--%>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;

public partial class Security_ChangeSecretQuestion : SecurePage
{
    User _ObjUser;
    protected void Page_PreInit(object sender, EventArgs e)
    {
        _ObjUser = (User)Session["User"];
        if (_ObjUser.LoginCount == 0)
        {
            Session["LoginCount"] = _ObjUser.LoginCount;
            this.MasterPageFile = "~/Home/Blank.master";
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        txtMemDate.Enabled = false;
        txtComfirmMemDate.Enabled = false;
        Title = "Change Password";
        if (_ObjUser != null && _ObjUser.UserID > 0)
        {
            lblUserName.Text = _ObjUser.FirstName + " " + _ObjUser.LastName;
        }
        else
        {
            if (Session["LoginAccount"] == null)
            {
                Response.Redirect("~/Login.aspx", false);
            }
            else
            {
                Response.Redirect("~/Login.aspx?" + Session["LoginAccount"].ToString(), false);
            }
            return;
        }

        if (!IsPostBack)
        {
            if (Request.UrlReferrer != null)
            {
                hlBack.NavigateUrl = Request.UrlReferrer.AbsoluteUri;
            }

            Password.Text = "";

        }

        string strHelpJS = @" $(function () {
            $('#hlHelpCommon').fancybox({
                iframe : {
                    css : {
                        width : '600px',
                        height: '350px'
                    }
                },       
                toolbar  : false,
	            smallBtn : true, 
                scrolling: 'auto',
                type: 'iframe',
                'transitionIn': 'elastic',
                'transitionOut': 'none',
                titleShow: false
            });
        });";


        ScriptManager.RegisterStartupScript(this, this.GetType(), "HelpJS", strHelpJS, true);


    }


    //protected void cmdSave_Click(object sender, ImageClickEventArgs e)

    protected void lnkSave_Click(object sender, EventArgs e)
    {
        //Trap empty
        lblMeg.Text = "";
        if (Session["DemoEmail"] != null)
        {
            if (_ObjUser.Email.ToLower() == Session["DemoEmail"].ToString().ToLower())
            {
                lblMeg.Text = Common.DemoReadyOnlyMsg;
                return;

            }
        }

        if (txtMemDate.Text.Trim() != txtComfirmMemDate.Text.Trim())
        {
            lblMeg.Text = "New & Confirm memorable date should be same!";
            return;
        }

        if (txtMemDate.Text.Length == 0 || txtComfirmMemDate.Text.Length == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PassEmptyErr", "alert('" + "Please enter memorable date password." + "');", true);

        }
        else
        {
            int iResetPasswordStat = 0;
            iResetPasswordStat = SecurityManager.User_ResetMemorableDate((int)_ObjUser.UserID, Password.Text, txtMemDate.Text);

            if (iResetPasswordStat > 0)
            {
                Session["tdbmsgpb"] = "Memorable date reset successfully!"; //red Ticket 3059_3
                _ObjUser = SecurityManager.User_Details((int)_ObjUser.UserID); //red Ticket 3059_3
                Session["User"] = _ObjUser;  //red Ticket 3059_3

                string url = "/Default.aspx";
                if (_ObjUser.LoginCount != 0)
                {
                    url = hlBack.NavigateUrl.Replace("~", "");
                }
                string script = "setTimeout(function(){";
                script += "window.location = '";
                script += url;
                script += "'; }, 3000)";

                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "alert", script, true);
            }
            else
            {
                Session["tdbmsgpb"] = "Invalid Password."; //red Ticket 3059_3
            }

        }
    }


    //protected  int ChangePassword(string strOldPassword, string strNewPassword, int iUserID)
    //{
    //    using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
    //    {
    //        using (SqlCommand command = new SqlCommand("ChangePassword", connection))
    //        {

    //            command.CommandType = CommandType.StoredProcedure;
    //            SqlParameter pRV = new SqlParameter("@Result", SqlDbType.Int);
    //            pRV.Direction = ParameterDirection.Output;

    //            command.Parameters.Add(pRV);
    //            command.Parameters.Add(new SqlParameter("@OldPassword", strOldPassword));
    //            command.Parameters.Add(new SqlParameter("@NewPassword", strNewPassword));
    //            command.Parameters.Add(new SqlParameter("@nUserID", iUserID));


    //            connection.Open();
    //            try
    //            {
    //                command.ExecuteNonQuery();
    //                connection.Close();
    //                connection.Dispose();
    //                return int.Parse(pRV.Value.ToString());
    //            }
    //            catch
    //            {
    //                connection.Close();
    //                connection.Dispose();

    //            }
    //            return -1;
    //        }
    //    }
    //}



    //protected void lnkCancel_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("~/Default.aspx", false);
    //}


}

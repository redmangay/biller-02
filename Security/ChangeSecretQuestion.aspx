﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangeSecretQuestion.aspx.cs" Inherits="Security_ChangeSecretQuestion"
    MasterPageFile="~/Home/rResponsive.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
   <link href="<%=ResolveUrl("~/fancybox3/dist/jquery.fancybox.min.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/fancybox3/dist/jquery.fancybox.min.js")%>"></script>

    <script type="text/javascript">
        function validatePassword(validator, arg) {
            var textBox1 = document.getElementById('<%=txtMemDate.ClientID %>');
            var textBox2 = document.getElementById('<%=txtComfirmMemDate.ClientID %>');
            if (textBox1.value == textBox2.value)
                arg.IsValid = true; //Valid Value   
            else
                arg.IsValid = false; //Invalid Value   
        }
                     
           
    </script>
    <div style="width: 970px;" class="ContentMainTra">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td width="28">
                        </td>
                        <td colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" style="width: 50%;">
                                        <span class="TopTitle">Reset Memorable Date</span>
                                    </td>
                                    <td align="right">
                                                <asp:HyperLink runat="server" ID="hlHelpCommon" ClientIDMode="Static"
                                                NavigateUrl="~/Pages/Help/Help.aspx?contentkey=ChangePasswordHelp" >
                                                <asp:Image ID="Image1" runat="server"  ImageUrl="~/App_Themes/Default/images/help.png"  />
                                                </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="28">
                        </td>
                    </tr>                 
                    <tr>
                        <td width="28" rowspan="3">
                        </td>
                        <td width="945" valign="top">
                            </div>
                            <div id="content1">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="height: 400px">
                                    <tr>
                                        <td width="615" style="padding-left: 10px" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td style="padding-right: 6px" valign="top">
                                                        <table border="0" align="left" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td height="30" colspan="2">
                                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                                                                        ValidationGroup="reg" ShowMessageBox="true" ShowSummary="false" HeaderText="Please correct following errors:" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">
                                                                    Full Name:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblUserName" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="5" colspan="2">
                                                                    <br />
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr id="trlblPassword" runat="server">
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">
                                                                      &nbsp;</td>
                                                                <td align="left">
                                                                    Please enter your password below:<br />
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr id="trtxtPassword" runat="server">
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">&nbsp;</td>
                                                                <td align="left">
                                                                    <asp:TextBox ID="Password" runat="server" AutoCompleteType="Disabled" ClientIDMode="Static" CssClass="NormalTextBox" MaxLength="30" TextMode="Password" Width="300px"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RFVOldPassword" runat="server" ControlToValidate="Password" Display="None" ErrorMessage="Password is required." ValidationGroup="reg"></asp:RequiredFieldValidator>
                                                                    <br />
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" height="5" align="center">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">
                                                                    &nbsp;</td>
                                                                <td align="left" valign="top">
                                                                    <asp:Label ID="lblNewPassword" runat="server" Text="Please enter your NEW Memorable Date below:"></asp:Label><br />
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">&nbsp;</td>
                                                                <td align="left" valign="top">
                                                                    <%--//red 06092017--%>
                                                <asp:TextBox runat="server" ID="txtMemDate"  ClientIDMode="Static"  Width="100px" ValidationGroup="MKE"
                                                    BorderStyle="Solid" BorderColor="#909090"
                                                    BorderWidth="1" />
                                                    <asp:ImageButton runat="server" ClientIDMode="Static" ID="ibMemDate"  ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false"/>
                                                <ajaxToolkit:CalendarExtender ID="ce_MemDate" runat="server" TargetControlID="txtMemDate"
                                                    Format="dd/MM/yyyy" PopupButtonID="ibMemDate"  FirstDayOfWeek="Monday">
                                                </ajaxToolkit:CalendarExtender>
                                            
                                                <asp:RangeValidator ID="rngMemDate" runat="server" ControlToValidate="txtMemDate"
                                                    ValidationGroup="MKE" ErrorMessage="*" Font-Bold="true" Display="Dynamic" Type="Date"
                                                    MinimumValue="1/1/1753" MaximumValue="1/1/3000"></asp:RangeValidator>

                                                  <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" TargetControlID="txtMemDate" WatermarkText="dd/mm/yyyy" runat="server" WatermarkCssClass="MaskText"></ajaxToolkit:TextBoxWatermarkExtender>
                             
                             <br />
                               <br />

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" height="5" align="center">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">
                                                                    &nbsp;</td>
                                                                <td align="left" height="22px">
                                                                    Please enter it AGAIN to confirm:<br />
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">&nbsp;</td>
                                                                <td align="left" height="22px">
                                                                     <%--//red 06092017--%>
                                                <asp:TextBox runat="server" ID="txtComfirmMemDate"  ClientIDMode="Static"  Width="100px" ValidationGroup="MKE"
                                                    BorderStyle="Solid" BorderColor="#909090"
                                                    BorderWidth="1" />
                                                    <asp:ImageButton runat="server" ClientIDMode="Static" ID="ibConfirmMemDate"  ImageUrl="~/Images/Calendar.png" AlternateText="Click to show calendar" CausesValidation="false"/>
                                                <ajaxToolkit:CalendarExtender ID="ce_ConfirmMemDate" runat="server" TargetControlID="txtComfirmMemDate"
                                                    Format="dd/MM/yyyy" PopupButtonID="ibConfirmMemDate"  FirstDayOfWeek="Monday">
                                                </ajaxToolkit:CalendarExtender>
                                            
                                                <asp:RangeValidator ID="rngConfirmMemDate" runat="server" ControlToValidate="txtComfirmMemDate"
                                                    ValidationGroup="MKE" ErrorMessage="*" Font-Bold="true" Display="Dynamic" Type="Date"
                                                    MinimumValue="1/1/1753" MaximumValue="1/1/3000"></asp:RangeValidator>

                                                  <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" TargetControlID="txtComfirmMemDate" WatermarkText="dd/mm/yyyy" runat="server" WatermarkCssClass="MaskText"></ajaxToolkit:TextBoxWatermarkExtender>
                             
                             <br />
                               <br />

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td height="5" align="left">
                                                                    <%--<asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Passwords do not match" ControlToCompare="ConformPassword" ControlToValidate="Password" ValidationGroup="reg" ></asp:CompareValidator>--%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label runat="server" ID="lblMeg" Text="" ForeColor="Red"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="padding-right: 5px">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="left" height="40px">
                                                                    <%--<asp:ImageButton ID="cmdSave" runat="server" ImageUrl="~/App_Themes/Default/Images/btnSave.png"
                                                                    CausesValidation="true" OnClick="cmdSave_Click" ValidationGroup="reg" />--%>
                                                                    &nbsp;
                                                                    <%--<asp:ImageButton ID="cmdCancel" runat="server" ImageUrl="~/App_Themes/Default/Images/btnCancel.png"
                                                                    CausesValidation="false" OnClick="cmdCancel_Click" />--%>
                                                                    <div>
                                                                        <table>
                                                                            <tr>
                                                                                
                                                                                <td>
                                                                                    <div>
                                                                                        <%--<asp:LinkButton runat="server" ID="lnkCancel" OnClick="lnkCancel_Click" CssClass="btn"
                                                                                            CausesValidation="false"> <strong>Cancel</strong> </asp:LinkButton>--%>
                                                                                        <asp:HyperLink runat="server" ID="hlBack" CssClass="btn" NavigateUrl="~/Default.aspx"
                                                                                            > <strong>Cancel</strong> </asp:HyperLink>
                                                                                    </div>
                                                                                </td>

                                                                                <td>
                                                                                    <div runat="server" id="divSave">
                                                                                        <asp:LinkButton runat="server" ID="lnkSave" CssClass="btn" OnClick="lnkSave_Click"
                                                                                            CausesValidation="true" ValidationGroup="reg"> <strong>Reset Memorable Date</strong> </asp:LinkButton>
                                                                                    </div>
                                                                                </td>

                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td width="27" rowspan="3">
                        </td>
                        <td width="28" rowspan="3">
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

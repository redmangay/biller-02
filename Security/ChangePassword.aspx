﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="Security_ChangePassword"
    MasterPageFile="~/Home/rResponsive.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
   
    <script type="text/javascript">
        function validatePassword(validator, arg) {
            var textBox1 = document.getElementById('<%=Password.ClientID %>');
            <%--var textBox2 = document.getElementById('<%=ConformPassword.ClientID %>');--%>
            //RP REMOVED Ticket 4197
            //if (textBox1.value == textBox2.value)
            //    arg.IsValid = true; //Valid Value   
            //else
            //    arg.IsValid = false; //Invalid Value
            //END MODIFICATION
        }
         
        //jeRome Ticket 2808
        //Show Password Feature for the Password Field
        $(document).ready(function () {

            var ua = window.navigator.userAgent;
            var trident = ua.indexOf('Trident/');
            var edge = ua.indexOf('Edge/');
            var msie = ua.indexOf('MSIE ');

            if ((msie > 0) || (trident > 0) || (edge > 0)) {

                // IE 10 or older => return version number

                document.getElementById('Password').style.width = '320px';
                document.getElementById('Password').style.borderRight = 'solid 1px #909090';
                //document.getElementById('ConformPassword').style.width = '320px';
                //document.getElementById('ConformPassword').style.borderRight = 'solid 1px #909090';
                document.getElementById('showPassword2_icon').style.display = 'none';
                //document.getElementById('showPassword3_icon').style.display = 'none';
                document.getElementById('showPassword_icon').style.display = 'none';
                document.getElementById('OldPassword').style.width = '320px';
                document.getElementById('OldPassword').style.borderRight = 'solid 1px #909090';


            }

                // other browser
            else {
                $('#showPassword3_icon').css('background', 'url("../Images/eye-open-edited2.png")');
                $('#showPassword3_icon').css('background-position-x', 'center');
                $('#showPassword3_icon').css('background-position-y', 'center');
                $('#showPassword3_icon').css('background-repeat', 'no-repeat');
                $('#showPassword3_icon').css('background-color', 'white');

                $('#showPassword2_icon').css('background', 'url("../Images/eye-open-edited2.png")');
                $('#showPassword2_icon').css('background-position-x', 'center');
                $('#showPassword2_icon').css('background-position-y', 'center');
                $('#showPassword2_icon').css('background-repeat', 'no-repeat');
                $('#showPassword2_icon').css('background-color', 'white');

                $('#showPassword_icon').css('background', 'url("../Images/eye-open-edited2.png")');
                $('#showPassword_icon').css('background-position-x', 'center');
                $('#showPassword_icon').css('background-position-y', 'center');
                $('#showPassword_icon').css('background-repeat', 'no-repeat');
                $('#showPassword_icon').css('background-color', 'white');

                $('#showPassword_icon').on('click', function () {

                    if ($(this).attr('value') == 1) {
                        $(this).attr('value', 0);
                        document.getElementById('OldPassword').type = 'text';
                        $(this).css('background', 'url("../Images/eye-close-edited2.png")');
                        $(this).css('background-position-x', 'center');
                        $(this).css('background-position-y', 'center');
                        $(this).css('background-repeat', 'no-repeat');
                        $(this).css('background-color', 'white');


                    } else {
                        $(this).attr('value', 1);
                        document.getElementById('OldPassword').type = 'password';
                        $(this).css('background', 'url("../Images/eye-open-edited2.png")');
                        $(this).css('background-position-x', 'center');
                        $(this).css('background-position-y', 'center');
                        $(this).css('background-repeat', 'no-repeat');
                        $(this).css('background-color', 'white');
                    }

                });

                $('#showPassword2_icon').on('click', function () {

                    if ($(this).attr('value') == 1) {
                        $(this).attr('value', 0);
                        document.getElementById('Password').type = 'text';
                        $(this).css('background', 'url("../Images/eye-close-edited2.png")');
                        $(this).css('background-position-x', 'center');
                        $(this).css('background-position-y', 'center');
                        $(this).css('background-repeat', 'no-repeat');
                        $(this).css('background-color', 'white');


                    } else {
                        $(this).attr('value', 1);
                        document.getElementById('Password').type = 'password';
                        $(this).css('background', 'url("../Images/eye-open-edited2.png")');
                        $(this).css('background-position-x', 'center');
                        $(this).css('background-position-y', 'center');
                        $(this).css('background-repeat', 'no-repeat');
                        $(this).css('background-color', 'white');
                    }

                });

                //$('#showPassword3_icon').on('click', function () {

                //    if ($(this).attr('value') == 1) {
                //        $(this).attr('value', 0);
                //        document.getElementById('ConformPassword').type = 'text';
                //        $(this).css('background', 'url("../Images/eye-close-edited2.png")');
                //        $(this).css('background-position-x', 'center');
                //        $(this).css('background-position-y', 'center');
                //        $(this).css('background-repeat', 'no-repeat');
                //        $(this).css('background-color', 'white');


                //    } else {
                //        $(this).attr('value', 1);
                //        document.getElementById('ConformPassword').type = 'password';
                //        $(this).css('background', 'url("../Images/eye-open-edited2.png")');
                //        $(this).css('background-position-x', 'center');
                //        $(this).css('background-position-y', 'center');
                //        $(this).css('background-repeat', 'no-repeat');
                //        $(this).css('background-color', 'white');
                //    }

                //});
            }

        });
        //end jeRome Ticket 2808  
           
    </script>
     <style>

         #showPassword_icon {
            /*background:url("../../Images/eye-open-edited2.png") center center no-repeat #fff;
            border: none;*/
            box-shadow: none;
           
            width:26px;
            height: 20px;
            display: block;
            /*float: right;*/
            /*margin-left:224px;*/
            border: solid 1px #909090;
            border-left:none;
            font-size:0px;
         }

         #showPassword2_icon {
            /*background:url("../../Images/eye-open-edited2.png") center center no-repeat #fff;*/
            border: none;
            box-shadow: none;
           
            width:26px;
            height: 20px;
            display: block;
            /*float: right;*/
            /*margin-left:224px;*/
            border: solid 1px #909090;
            border-left:none;
            font-size:0px;
         }

         #showPassword3_icon {
            /*background:url("../../Images/eye-open-edited2.png") center center no-repeat #fff;*/
            border: none;
            box-shadow: none;
           
            width:26px;
            height: 20px;
            display: block;
            /*float: right;*/
            margin-left:224px;
            border: solid 1px #909090;
            border-left:none;
            font-size:0px;
         }

        #OldPassword {
            float:left;
            /*margin-bottom:30px;*/
            border-right:none;

         }  
         #Password {
            float:left;
            /*margin-bottom:30px;*/
            border-right:none;

         }
         
          /*#ConformPassword {
            float:left;
            /*margin-bottom:30px;*/
            border-right:none;

         }*/

     </style>
    <div style="width: 970px;"   class="ContentMainTra">
       <asp:Panel ID="pnlFull" runat="server" DefaultButton="lnkSave">  <%--red Sha1_512 Update--%>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td width="28">
                        </td>
                        <td colspan="2">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" style="width: 50%;">
                                        <span class="TopTitle">Reset Password</span>
                                    </td>
                                    <td align="right">
                                                <asp:HyperLink runat="server" ID="hlHelpCommon" ClientIDMode="Static"
                                                NavigateUrl="~/Pages/Help/Help.aspx?contentkey=ChangePasswordHelp" >
                                                <asp:Image ID="Image1" runat="server"  ImageUrl="~/App_Themes/Default/images/help.png"  />
                                                </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="28">
                        </td>
                    </tr>                 
                    <tr>
                        <td width="28" rowspan="3">
                        </td>
                        <td width="945" valign="top">
                            </div>
                            <div id="content1">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="height: 400px">
                                    <tr>
                                        <td width="615" style="padding-left: 10px" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td style="padding-right: 6px" valign="top">
                                                        <table border="0" align="left" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td height="30" colspan="2">
                                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                                                                        ValidationGroup="reg" ShowMessageBox="true" ShowSummary="false" HeaderText="Please correct following errors:" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">
                                                                    Full Name:
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblUserName" runat="server" Text=""></asp:Label>
                                                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="5" colspan="2">
                                                                    <br />
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr id="trlblOldPassword" runat="server">
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">
                                                                      &nbsp;</td>
                                                                <td align="left">
                                                                    Please enter your OLD password below:<br />
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr id="trtxtOldPassword" runat="server">
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">&nbsp;</td>
                                                                <td align="left">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="OldPassword" runat="server" AutoCompleteType="Disabled" ClientIDMode="Static" CssClass="NormalTextBox" MaxLength="30" TextMode="Password" Width="300px"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <input id="showPassword_icon" type="button" value="1" />
                                                                                <%--//jeRome Ticket 2808--%>
                                                                            </td>
                                                                            <td>
                                                                                <asp:RequiredFieldValidator ID="RFVOldPassword" runat="server" ControlToValidate="OldPassword" Display="None" ErrorMessage="Old Password is required." ValidationGroup="reg"></asp:RequiredFieldValidator>
                                                                            </td>

                                                                        </tr>
                                                                    </table>
                                                                    
                                                                    
                                                                    
                                                                    <br />
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" height="5" align="center">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">
                                                                    &nbsp;</td>
                                                                <td align="left" valign="top">
                                                                    <asp:Label ID="lblNewPassword" runat="server" Text="Please enter your NEW password below:"></asp:Label><br />
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">&nbsp;</td>
                                                                <td align="left" valign="top">


                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="Password" runat="server" ClientIDMode="Static" CssClass="NormalTextBox" MaxLength="30" TextMode="Password" Width="300px"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <input id="showPassword2_icon" type="button" value="1" />
                                                                                <%--//jeRome Ticket 2808--%>
                                                                            </td>
                                                                            <td>
                                                                                <ajaxToolkit:PasswordStrength ID="PS" runat="server" CalculationWeightings="50;15;15;20" DisplayPosition="RightSide" MinimumLowerCaseCharacters="2" MinimumNumericCharacters="2" MinimumSymbolCharacters="2" MinimumUpperCaseCharacters="1" PreferredPasswordLength="20" PrefixText="Strength:" RequiresUpperAndLowerCaseCharacters="true" StrengthIndicatorType="Text" TargetControlID="Password" TextCssClass="TextIndicator" TextStrengthDescriptions="Very Poor;Weak;Average;Strong;Excellent" TextStrengthDescriptionStyles="TextIndicator_Strength1;TextIndicator_Strength2;TextIndicator_Strength3;TextIndicator_Strength4;TextIndicator_Strength5" />
                                                                                <asp:RegularExpressionValidator ID="revPasswordLength" runat="server" ControlToValidate="Password" Display="None" ErrorMessage="Password Minimum length is 6." ValidationExpression=".{6,30}" ValidationGroup="reg">
                                                                                </asp:RegularExpressionValidator>
                                                                                <asp:RequiredFieldValidator ID="RFVPassword" runat="server" ControlToValidate="Password" Display="None" ErrorMessage="New Password is required." ValidationGroup="reg"></asp:RequiredFieldValidator>
                                                                            </td>

                                                                        </tr>
                                                                    </table>


                                                                    
                                                                    
                                                                    
                                                                    <br />
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" height="5" align="center">
                                                                </td>
                                                            </tr>
                                                            <%--<tr>
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">
                                                                    &nbsp;</td>
                                                                <td align="left" height="22px">
                                                                    <asp:Label ID="lblConfirmPassword" runat="server" Text="Please enter your NEW password AGAIN to confirm:"></asp:Label>
                                                                    
                                                                    <br />
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="padding-right: 5px; font-weight: bold;">&nbsp;</td>
                                                                <td align="left" height="22px">
                                                                    <asp:TextBox ID="ConformPassword" runat="server" ClientIDMode="Static" CssClass="NormalTextBox" TextMode="Password" Width="300px"></asp:TextBox>
                                                                    <input id="showPassword3_icon" type="button" value="1" /> 
                                                                    <asp:RequiredFieldValidator ID="RfvConformPassword" runat="server" ControlToValidate="ConformPassword" Display="None" ErrorMessage="Confirm Password is required." ValidationGroup="reg"></asp:RequiredFieldValidator>
                                                                    <br />
                                                                    <br />
                                                                </td>
                                                            </tr>--%>
                                                            <%--
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td height="5" align="left">
                                                                    <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="validatePassword"
                                                                        ErrorMessage="New and Confirm Passwords need to be same!" ValidationGroup="reg"
                                                                        Display="None"></asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                            --%>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td align="left">
                                                                    <asp:Label runat="server" ID="lblMeg" Text="" ForeColor="Red"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" style="padding-right: 5px">
                                                                    &nbsp;
                                                                </td>
                                                                <td align="left" height="40px">
                                                                    <%--<asp:ImageButton ID="cmdSave" runat="server" ImageUrl="~/App_Themes/Default/Images/btnSave.png"
                                                                    CausesValidation="true" OnClick="cmdSave_Click" ValidationGroup="reg" />--%>
                                                                    &nbsp;
                                                                    <%--<asp:ImageButton ID="cmdCancel" runat="server" ImageUrl="~/App_Themes/Default/Images/btnCancel.png"
                                                                    CausesValidation="false" OnClick="cmdCancel_Click" />--%>
                                                                    <div>
                                                                        <table>
                                                                            <tr>
                                                                                
                                                                                <td>
                                                                                    <div>
                                                                                        <%--<asp:LinkButton runat="server" ID="lnkCancel" OnClick="lnkCancel_Click" CssClass="btn"
                                                                                            CausesValidation="false"> <strong>Cancel</strong> </asp:LinkButton>--%>
                                                                                        <asp:HyperLink runat="server" ID="hlBack" CssClass="btn" NavigateUrl="~/Default.aspx"
                                                                                            > <strong>Cancel</strong> </asp:HyperLink>
                                                                                    </div>
                                                                                </td>

                                                                                <td>
                                                                                    <div runat="server" id="divSave">
                                                                                       <%-- OnClientClick="alert('Password reset successfull');return false;"--%>
                                                                                        <asp:LinkButton runat="server"  ID="lnkSave" CssClass="btn" OnClick="lnkSave_Click" 
                                                                                            CausesValidation="true" ValidationGroup="reg"> <strong>Reset Password</strong> </asp:LinkButton>
                                                                                    </div>
                                                                                </td>

                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="2">
                                                                                    <br />
                                                                                    <%--RP Added Ticket 4930 - Reset Password - Next Time --%>
                                                                                    <asp:LinkButton ID="lnkNextTime" runat="server" CausesValidation="false" OnClick="lnkNextTime_Click" Visible="false">Next time when I log-in</asp:LinkButton>
                                                                                    <%-- ------------------ --%>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td width="27" rowspan="3">
                        </td>
                        <td width="28" rowspan="3">
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
            </asp:Panel>  <%--red Sha1_512 Update--%>
                </div>
</asp:Content>

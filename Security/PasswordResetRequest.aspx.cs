﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;


public partial class Security_PasswordResetRequest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "Password Reset Request";
        trlblEmailStatus.Visible = false;
        lblSecurityQuestion.Text = "";

        string strHelpJS = @" $(function () {
            $('#hlHelpCommon').fancybox({
                iframe : {
                    css : {
                        width : '600px',
                        height: '350px'
                    }
                },       
                toolbar  : false,
	            smallBtn : true, 
                scrolling: 'auto',
                type: 'iframe',
                'transitionIn': 'elastic',
                'transitionOut': 'none',
                titleShow: false
            });
        });";


        //ScriptManager.RegisterStartupScript(this, this.GetType(), "HelpJS", strHelpJS, true);

        string strSecurityQuestion = @" function GetSecurityQuestion() {                               
                            $('#divGetSecurityQuestion').dialog({   
                            title: ""Warning Log-off"",        
                            width: 400,
                            height: 300,
                            modal: true, 

                            close: function() {
                           
                                    }
                                    });
       
                            }

                        function GetSecurityQuestionClose() {                
                             $(""#divGetTwoFactorAuthenticator"").dialog('close');                               
                            }
                            
                                function GetMemDate() {
                                $(""#hfMemDate"").val($(""#txtMemDate"").val())
                                $(""#lblIncorrectAnswer"").show();
                                document.getElementById('lblIncorrectAnswer').innerText = ""Validating..."";
                                document.getElementById('hfMemDate').value = document.getElementById('txtMemDate').value
                                }

";
        //red Ticket 3059_4
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "SecurityQuestionJS", strSecurityQuestion, true);


    }

    protected void lnkContinue_Click(object sender, EventArgs e)
    {
        //trlblEmailStatus.Visible = true;
        User theUserValidateMemDate = SecurityManager.User_ValidateMemorableDate(txtEmail.Text, hfMemDate.Value);
        hfMemDate.Value = "";
        if (theUserValidateMemDate != null)
        {
            User theUser = SecurityManager.User_ByEmail(txtEmail.Text);
            if (EmailPasswordResetLink(theUser))
            {
                //int iRequesID = SecurityManager.User_PasswordReset_Insert(theUser.UserID);
                //lblEmailStatus.Text = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Security/PasswordReset.aspx?iRequestID=" + Cryptography.Encrypt(iRequesID.ToString());
                //string.Format("A password reset link has been sent to {0}", this.txtEmail.Text);
                //lblEmailStatus.Text =
                //string.Format("A password reset link has been sent to {0}", this.txtEmail.Text);
                Session["tdbmsgpb"] = string.Format("A password reset link has been sent to {0}", this.txtEmail.Text); //red Sha1_512 Update
                txtEmail.Text = string.Empty;
            }
            else
            {
                //lblEmailStatus.Text = "Password reset link email unsuccessful.";
                Session["tdbmsgpb"] = "Password reset link email unsuccessful."; //red Sha1_512 Update
            }
        }
        else
        {
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "LoginFail", "return false;", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "displayDoNothing", "GetSecurityQuestion();", true);
            lblIncorrectAnswer.Visible = true;
            lblIncorrectAnswer.Text = "Invalid memorable date, please try again.";
        }
    }



    protected void lnkSendPassword_Click(object sender, EventArgs e)
    {
        //Trap empty Email field
        int iTN = 0;
        if (txtEmail.Text.Length == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EmailEmptyErr", "alert('" + "Please enter your email address." + "');", true);
        }
        else
        {
            //Check if email entered is valid
            if (Regex.IsMatch(txtEmail.Text, "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$"))
            {
                User theUser = SecurityManager.User_ByEmail(txtEmail.Text);

                if (theUser != null)
                {
                    //red Ticket 3059_4
                    if (EmailPasswordResetLink(theUser))
                    {
                        //trlblEmailStatus.Visible = true;
                        //int iRequesID = SecurityManager.User_PasswordReset_Insert(theUser.UserID);
                        //lblEmailStatus.Text = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Security/PasswordReset.aspx?iRequestID=" + Cryptography.Encrypt(iRequesID.ToString());
                        //string.Format("A password reset link has been sent to {0}", this.txtEmail.Text);
                        // lblEmailStatus.Text =
                        // string.Format("A password reset link has been sent to {0}", this.txtEmail.Text);
                        //red Sha1_512 Update
                        Session["tdbmsgpb"] = string.Format("A password reset link has been sent to {0}", this.txtEmail.Text); //red Sha1_512 Update
                        txtEmail.Text = string.Empty;
                    }
                    //ScriptManager.RegisterStartupScript(this, GetType(), "displaySecurityQuestion", "GetSecurityQuestion();", true);
                }
                else
                {
                    //Pop a message if email not found in database.
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "EmailNotFoundErr", "alert('" + "Email does not exist in database." + "');", true);
                    Session["tdbmsgpb"] = "Email does not exist in database."; //red Sha1_512 Update
                }
            }
            else
            {
                //Pop a message if email is invalid.
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "EmailInvalidErr", "alert('" + "Email invalid." + "');", true);
                Session["tdbmsgpb"] = "Email invalid."; //red Sha1_512 Update
            }
        }
    }

    public bool EmailPasswordResetLink(User userRec)
    {
        bool bReturnValue = false;
        string strEmail = userRec.Email;

        string url = Request.Url.OriginalString;
        string rawURL;
        rawURL = url.Substring(0, url.IndexOf("/Security"));

        try
        {
            string sError = "";
            string sFrom = SystemData.SystemOption_ValueByKey_Account_Default("EmailFrom", null, null, "no-reply@dbgurus.com.au");
            string sHeading = SystemData.SystemOption_ValueByKey_Account_Default("PasswordReminderEmailHeading", null, null,
                "ETS - Password Reminder");

            //lets log the request
            int iRequesID = SecurityManager.User_PasswordReset_Insert(userRec.UserID);

            Content theContent = SystemData.Content_Details_ByKey("PasswordResetEmail", null);


            sHeading = theContent.Heading;
            theContent.ContentP = theContent.ContentP.Replace("[Link]", Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Security/PasswordReset.aspx?iRequestID=" + Cryptography.Encrypt(iRequesID.ToString()));
            theContent.ContentP = theContent.ContentP.Replace("[Email]", strEmail);

            if (Session["LoginAccount"] == null)
            {
                theContent.ContentP = theContent.ContentP.Replace("[URL]", Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Login.aspx");
            }
            else
            {
                theContent.ContentP = theContent.ContentP.Replace("[URL]", Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Login.aspx?" + Session["LoginAccount"].ToString());
            }

            if (DBGurus.SendEmail(theContent.ContentKey, null, null, sHeading, theContent.ContentP, sFrom, strEmail, "", "", null, null, out sError) != 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "EmailPasswordSendErr", "alert('" + sError + "');", true);
                ErrorLog theErrorLog = new ErrorLog(null, "Password Reminder", sError, sError, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);

            }
            else
            {
                bReturnValue = true;
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "EmailPasswordErr", "alert('" + ex.Message + "');", true);
            ErrorLog theErrorLog = new ErrorLog(null, "Password Reset Request", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);

        }
        return bReturnValue;
    }


    protected void lnkReturn_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["AccountID"] == null)
        {
            Response.Redirect("~/Login.aspx", false);
        }
        else
        {
            Response.Redirect("~/Login.aspx?AccountID=" + Request.QueryString["AccountID"].ToString(), false);
        }
    }
}

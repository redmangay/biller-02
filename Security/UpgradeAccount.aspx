﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Popup.master" AutoEventWireup="true" CodeFile="UpgradeAccount.aspx.cs" Inherits="Security_UpgradeAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet" />
  <!-- Stylesheets -->
  <link rel="stylesheet" type="text/css" href="../ontask/css/plugins.css" />
  <link rel="stylesheet" type="text/css" href="../ontask/css/YTPlayer.css" />
  <link rel="stylesheet" type="text/css" href="../ontask/css/style.css?v=1" />
  <link rel="stylesheet" type="text/css" href="../ontask/css/signup.css" />

   <%-- <style type="text/css">
        .stripe-button-el span{
                display: inline-block;
                font-size: 30px;
                color: #fff;
                background-color: #fd6203;
                line-height: 1.2em;
                padding: 7px 60px;
                border-radius: 1.6em;
                -webkit-transition: all 0.3s ease;
                -o-transition: all 0.3s ease;
                transition: all 0.3s ease;
            }

                .stripe-button-el span:hover,
                .stripe-button-el span:active,
                .stripe-button-el span:focus {
                    opacity: 0.8;
                    color: #fff;
                }
        @media screen and (max-width: 575px) {
                               .stripe-button-el span{
                font-size: 26px;
                padding: 7px 40px;
            }
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
<script type="text/javascript">

    $(document).ready(function () {
        //document.getElementsByClassName("stripe-button-el")[0].style.display = 'none';
        document.getElementsByClassName("stripe-button-el")[0].style.display = 'none';



    });

    $(".popup-cross").click(function () {
        $.fancybox.close();
    });
</script>
   <%-- <asp:Panel runat="server" ID="pnlWhiteWash" class="whitewash-full"
        Style="width: 100%; height: 100%; text-align: center;" >
        <table style="width: 100%; height: 100%; text-align: center;">

            <tr valign="middle">
                <td>
                    <div style="font-size: 20px;">
                        To access this paid feature please 
                                <asp:HyperLink runat="server" ID="hlUpgradeAccount" NavigateUrl="~/Pages/Security/ProcessPayment.aspx">upgrade your account</asp:HyperLink>
                        <br />
                        or
                                            <asp:HyperLink runat="server" ID="hlHomePage"
                                                NavigateUrl="~/Default.aspx">go back to the home page</asp:HyperLink>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>--%>

    <div>
        <div class="popup-wrap">
            <div class="popup-overlay"></div>
            <div class="popup-wrap-in">
                <div class="popup-in">
                    <div class="popup-heading">
                       
                       
                        <%--<div class="popup-cross">X</div>--%>
                        <table border="0" cellpadding="0" cellspacing="0" align="left">
                            <tr>
                                <td>
                                 <div class="popup-logo">
                                         <img src="../ontask/img/logo.png"/></div>
                                </td>
                                 <td>
                                     <h2 class="popup-title">THIS FEATURE IS ONLY AVAILABLE WHEN YOU UPGRADE</h2>
                                 </td>
                               
                            </tr>
                        </table>
                    </div>
                    <div class="popup-row">
                        <div class="popup-col">
                            <div class="popup-feature">
                                <h3>UPGRADE TO TEAM</h3>
                                <ul class="popup-feature-list">
                                    <li>Access all your data online<img src="../ontask/img/tik2.png"></li>
                                    <li>Export to Excel or CSV<img src="../ontask/img/tik2.png"></li>
                                    <li>Share data with team mates<img src="../ontask/img/tik2.png"></li>
                                    <li>Data retained for 24 months<img src="../ontask/img/tik2.png"></li>
                                    <li>Create templates<img src="../ontask/img/tik2.png"></li>
                                    <li>Allocate work<img src="../ontask/img/tik2.png"></li>
                                    <li>View locations<img src="../ontask/img/tik2.png"></li>
                                </ul>
                                <div class="popup-feature-subtitle">
                                    <ul class="popup-price">
                                        <li>$10<span>*</span> MONTHLY</li>
                                        <li>$100<span>*</span> YEARLY</li>
                                    </ul>
                                    <a href="https://www.ontaskhq.com.au/#pricing" target="_blank"><strong>FIND OUT MORE</strong></a> 
                                </div>
                            </div>
                            <div class="popup-meta">
                                <div class="popup-offer-text">PAY FOR 12 MONTHS  AND SAVE 20%  <asp:CheckBox runat="server" ID="chkYearly" Text="" AutoPostBack="true" OnCheckedChanged="chkYearly_CheckedChanged" /></div>
                                
                                <asp:Label runat="server" ID="lblCostInformation" Visible="false"></asp:Label>

                               <%-- <a href="#" class="upgrate-btn">UPGRADE NOW</a>--%>
                                 <button type="submit" class="upgrate-btn">UPGRADE NOW</button>
                                <asp:Panel runat="server" ID="pnlStripButton">
                                        <script
                                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                            data-key="<%= stripePublishableKey %>"
                                            data-amount="<%= GetTotalPaymentAmount() %>"
                                            data-currency="<%= GetCurrency() %>"
                                            data-name="DBGurus Australia"
                                            data-description="<%=DataDescription() %>"
                                            data-email="<%= GetUserEmail() %>"
                                            data-locale="auto"
                                            data-zip-code="false"
                                            data-panel-label="<%=DataPanelLabel() %>">
                                        </script>
                                    </asp:Panel>
                                 <asp:HiddenField runat="server" ID="hfBillingID" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

     <%--   
    <div style="padding:20px; text-align:center;">
        <table style="text-align:center;width:100%;">
            <tr>
                <td>
                    <h1>Upgrade Your Account</h1>
                </td>
            </tr>
            <tr>
                <td align="left" style="padding-left:50px;">
                    This screen you are trying to access is only visible when you upgrade to a <strong>Team </strong>  account.

                </td>
            </tr>
            <tr>
                

                <td style="text-align:left;padding-left:50px;">
                    The benefits of upgrading to Team:<br />
                    <asp:BulletedList ID="BulletedList1" runat="server">
                        <asp:ListItem Text="Access all your data online"></asp:ListItem>
                        <asp:ListItem Text="Export to Excel or CSV"></asp:ListItem>
                        <asp:ListItem Text="and more ..."></asp:ListItem>
                    </asp:BulletedList>
                </td>
            </tr>
            <tr>
                <td>
                   
                </td>
            </tr>
            <tr>
                <td>
                   
                </td>
            </tr>
            <tr>
                <td style="padding:20px;">

                      <div >

                                    

                                </div>

                     
                </td>
            </tr>
            <tr>
                <td>
                    <asp:HyperLink runat="server" ID="hlUpgradeTerms" NavigateUrl="https://www.ontaskhq.com.au/terms-of-service.pdf" Target="_blank" >Terms of Service</asp:HyperLink>
                </td>
            </tr>
        </table>
    </div>--%>





</asp:Content>


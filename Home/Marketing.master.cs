﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Red: for login page with About page
/// Red 29-Nov-2017: Added systemoption for showing menu yes or no.
/// 
/// </summary>

public partial class Marketing : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        //SetupNotification();
        string strAbout = SystemData.SystemOption_ValueByKey_Account("ShowAboutMenuPage", null, null);
        if (strAbout.ToLower() == "no")
        {
            about.Visible = false;
            client.Visible = false;
            features.Visible = false;
            pricing.Visible = false;
            signin.Visible = false;
            btnMenu.Visible = false;
        }
        else
        {
            about.Visible = true;
            client.Visible = true;
            features.Visible = true;
            pricing.Visible = true;
            signin.Visible = true;
            btnMenu.Visible = true;
        }

        string strSite = Common.GetDatabaseName().Replace("thedatabase_", "") + ".thedatabase.net";

        switch (strSite.ToLower())
        {
            case "ets.thedatabase.net":
                divrowlogo.Attributes["class"] = "row pad-tb-20";
                about.HRef = "../About_ETS.aspx";
                client.HRef = "../About_ETS.aspx#client";
                features.HRef = "../About_ETS.aspx#features";
                pricing.HRef = "../About_ETS.aspx#pricing";
                //signin.HRef = "../Login.aspx";
                btmaddress.InnerHtml = "Copyright&copy; "+DateTime.Now.Year.ToString()+". All rights reserved.<br />" +
                                        "<a href=" + "http://www.dbgurus.com.au/" + "" + " target = _blank" + "><strong>DB Gurus Pty Ltd </strong>.</a >, Australia.ACN 134 593 712 <br />" +
                                        "Phone <a href=" + "" + "tel:1800901096" + ">1800 90 10 96​</a>  <br />" +
                                        "Int: <a href=" + "" + "tel:+61242685672" + "> +61 2 4268 5672 </a > ";
                break;

            case "honeyman.thedatabase.net":
                home.Width = 230;
                divrowlogo.Attributes["class"] = "row pad-tb-10";
                about.HRef = "../About_Honeyman.aspx";
                client.HRef = "../About_Honeyman.aspx#client";
                features.HRef = "../About_Honeyman.aspx#features";
                pricing.HRef = "../About_Honeyman.aspx#pricing";
                //signin.HRef = "../Login.aspx";
                btmaddress.InnerHtml = "Copyright &copy; " + DateTime.Now.Year.ToString() + ". All rights reserved. Call:<a href=" +
                    "tel: +6421706434" + ">+6421706434</a> Email: <a href=" + "mailto: help @honeyman.co.nz" + ">help@honeyman.co.nz</a>";
                break;

            default:
                divrowlogo.Attributes["class"] = "row pad-tb-20";
                about.HRef = "../About.aspx";
                client.HRef = "../About.aspx#client";
                features.HRef = "../About.aspx#features";
                pricing.HRef = "../About.aspx#pricing";
                //signin.HRef = "../Login.aspx";
                btmaddress.InnerHtml = "Powered by <a href=" + "../About.aspx" + "> <u>TheDatabase</u></a > - the cloud database from <a href=" + "http://www.dbgurus.com.au/" + "" + " target = _blank" + "><u>DB Gurus</u></a > <br />" +
                                        "Copyright&copy; " + DateTime.Now.Year.ToString() + " Australia";
                //btmaddress.InnerHtml = "Copyright&copy; 2017. All rights reserved.<br />" +
                //                        "<a href=" + "http://www.dbgurus.com.au/" + "" + " target = _blank" + "><strong>DB Gurus Pty Ltd </strong>.</a >, Australia.ACN 134 593 712 <br />" +
                //                        "Phone <a href=" + "" + "tel:1800901096" + ">1800 90 10 96​</a>  <br />" +
                //                        "Int: <a href=" + "" + "tel:+61242685672" + "> +61 2 4268 5672 </a > ";
                break;

        }

        //if (strAbout.ToLower() == "ets")
        //{
        //    divrowlogo.Attributes["class"] = "row pad-tb-20";          
        //    about.HRef = "../About_ETS.aspx";
        //    client.HRef = "../About_ETS.aspx#client";
        //    features.HRef = "../About_ETS.aspx#features";
        //    pricing.HRef = "../About_ETS.aspx#pricing";
        //    signin.HRef = "../SignIn.aspx";
        //    btmaddress.InnerHtml = "Copyright&copy; 2017. All rights reserved.<br />" +
        //                            "<a href=" + "http://www.dbgurus.com.au/" + "" + " target = _blank" + "><strong>DB Gurus Pty Ltd </strong>.</a >, Australia.ACN 134 593 712 <br />" +
        //                            "Phone <a href=" + "" + "tel:1800901096" + ">1800 90 10 96​</a>  <br />" +
        //                            "Int: <a href=" + "" + "tel:+61242685672" + "> +61 2 4268 5672 </a > ";
        //}
        //else if (strAbout.ToLower() == "honeyman")
        //{
        //    home.Width = 230;
        //    divrowlogo.Attributes["class"] = "row pad-tb-10";
        //    about.HRef = "../About_Honeyman.aspx";
        //    client.HRef = "../About_Honeyman.aspx#client";
        //    features.HRef = "../About_Honeyman.aspx#features";
        //    pricing.HRef = "../About_Honeyman.aspx#pricing";
        //    signin.HRef = "../SignIn.aspx";
        //    btmaddress.InnerHtml = "Copyright &copy; 2017. All rights reserved. Call:<a href=" + 
        //        "tel: +6421706434" + ">+6421706434</a> Email: <a href=" + "mailto: help @honeyman.co.nz" + ">help@honeyman.co.nz</a>";
        //}
        //else
        //{
        //    divrowlogo.Attributes["class"] = "row pad-tb-20";
        //    about.HRef = "../About.aspx";
        //    client.HRef = "../About.aspx#client";
        //    features.HRef = "../About.aspx#features";
        //    pricing.HRef = "../About.aspx#pricing";
        //    signin.HRef = "../SignIn.aspx";
        //    btmaddress.InnerHtml = "Copyright&copy; 2017. All rights reserved.<br />" +
        //                            "<a href=" + "http://www.dbgurus.com.au/" + "" + " target = _blank" + "><strong>DB Gurus Pty Ltd </strong>.</a >, Australia.ACN 134 593 712 <br />" + 
        //                            "Phone <a href=" + ""+ "tel:1800901096" +">1800 90 10 96​</a>  <br />" + 
        //                            "Int: <a href=" + "" + "tel:+61242685672" + "> +61 2 4268 5672 </a > ";
        //}

        if (!IsPostBack)
        {
            if (Session["tdbmsg"] != null)
            {
                lblNotificationMessage.Text = Session["tdbmsg"].ToString();
                Session["tdbmsg"] = null;

                if (lblNotificationMessage.Text != "")
                {
                    lblNotificationMessage.Text = lblNotificationMessage.Text + "&nbsp; <a id='aNotificationMessageClose' href='#'>Close</a>";
                }
            }
            else
            {
                lblNotificationMessage.Text = "";
            }

        }
        else
        {
            Session["tdbmsg"] = null;
        }

    }


//    protected void SetupNotification()
//    {
//        double iSec = 3;
//        string strTopMsgNoOfMS = "3000";
//        string strTopMessageDisplayNumberSeconds = SystemData.SystemOption_ValueByKey_Account("Top Message Display Number Seconds", int.Parse(Session["AccountID"].ToString()), null);
//        if (strTopMessageDisplayNumberSeconds != "")
//        {
//            double dTemp = 0;
//            if (double.TryParse(strTopMessageDisplayNumberSeconds, out dTemp))
//            {
//                if (dTemp > 300)
//                    dTemp = 300;

//                iSec = dTemp;
//                dTemp = dTemp * 1000;
//                strTopMsgNoOfMS = dTemp.ToString("N0").Replace(",", "");

//            }

//        }



//        ltMasterStyles.Text = @"<style  type='text/css'>
//                            .cssanimations.csstransforms #divNotificationMessage {
//                    -webkit-transform: translateY(-50px);
//                    -webkit-animation: slideDown " + iSec.ToString() + @"s 0.2s 1 ease forwards;
//                    -moz-transform:    translateY(-50px);
//                    -moz-animation:    slideDown " + iSec.ToString() + @"s 0.2s 1 ease forwards;
//                }
// </style>
//            ";


//        string strHidedivNotificationMessage = @"                                                     
//                                                  $(document).ready(function () {
//
//                                                      try
//                                                        {
//                                                            window.setTimeout(HidedivNotificationMessage," + strTopMsgNoOfMS + @");
//                                                        }
//                                                      catch(err)
//                                                        {
//                                                            
//                                                        }
//     
//                                                    });
//                                                ";
//        ScriptManager.RegisterStartupScript(this, this.GetType(), "strHidedivNotificationMessageMaster", strHidedivNotificationMessage, true);


//    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            if (Session["tdbmsgpb"] != null)
            {
                lblNotificationMessage.Text = Session["tdbmsgpb"].ToString();
                Session["tdbmsgpb"] = null;

                if (lblNotificationMessage.Text != "")
                {
                    lblNotificationMessage.Text = lblNotificationMessage.Text + "&nbsp; <a id=\"aNotificationMessageClose\" onclick=\"document.getElementById('divNotificationMessage').style.display = 'none';return false;\" href=\"#\" >Close</a>";
                }
            }
            else
            {
                lblNotificationMessage.Text = "";
            }
        }



    }
}

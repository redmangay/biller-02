﻿using System;
//using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;
//using System.Web.DynamicData;
using System.Collections.Generic;
using System.Data;
//using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class Home_rResponsive : System.Web.UI.MasterPage
{
    //public string loginDate; /* Red: Feb-07-19 */
    //public string expressDate;  /* Red: Feb-07-19 */
    bool _bDemoUser = false;
    Account _theAccount;
    User _objUser;
    bool _bIsAccountHolder = false;
    UserRole _CurrentUserRole = null;
    Role _theRole = null;
    bool _bGod = false;
    string _strDocumentsMenu = "All";
    bool bWhiteWash = false;
    protected void Page_Init(object sender, EventArgs e)
    {

        if (Request.QueryString["public"] != null)
        {
            return;
        }



        if (Session["AccountID"] == null || Session["UserRole"] == null)
        {
            try
            {
                //RP Modified Ticket 4305
                //Response.Redirect("~/Login.aspx?Logout=yes", true);               
                Response.Redirect(ResolveUrl("~/Login.aspx?Logout=yes&ReturnURL=" + HttpUtility.UrlEncode(HttpContext.Current.Request.Url.PathAndQuery)), true);
            }
            catch
            {
                //
            }
            return;
        }

        if (IsPostBack && Request.Params["__EVENTTARGET"] != null && Request.Params["__EVENTTARGET"].ToString().IndexOf("menuETS") > -1)
        {
            Response.Redirect(Request.RawUrl, true);
            return;
        }

        _objUser = (User)Session["User"];
        _CurrentUserRole = (UserRole)Session["UserRole"];
        _theRole = SecurityManager.Role_Details((int)_CurrentUserRole.RoleID);
        //string strUserAccountCount = Common.GetValueFromSQL("SELECT COUNT(UserRoleID) FROM UserRole WHERE UserID=" + _objUser.UserID.ToString());

        if (!IsPostBack)
        {
            if (Request.QueryString["TableID"] != null || Request.QueryString["ColumnID"] != null
                           || Request.QueryString["DocumentID"] != null || Request.QueryString["GraphOptionID"] != null
                           || Request.QueryString["TerminologyID"] != null || Request.QueryString["MenuID"] != null
                           || Request.QueryString["UploadID"] != null || Request.QueryString["AccountID"] != null)
            {
                Common.FindTheAccount();
            }
        }






        if (_CurrentUserRole.IsAccountHolder != null && (bool)_CurrentUserRole.IsAccountHolder)
        {
            _bIsAccountHolder = true;
            hfIsAccountHolder.Value = "yes";
        }

        _theAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));

        _strDocumentsMenu = SystemData.SystemOption_ValueByKey_Account("Documents Menu", _theAccount.AccountID, null);


        if (Common.HaveAccess(Session["roletype"].ToString(), "1"))
        {
            _bGod = true;
        }

        if (_objUser != null && _objUser.UserID > 0)
        {

            if (Context.User.Identity.IsAuthenticated)
            {

                //lkProfile.Text = ObjUser.FirstName + " " + ObjUser.LastName;
            }

            if (_objUser.IsTempPassword != true)   //red Sha1_512 Update)
            {
                BindMenu();
                BindMenuProfile();
                BindMenuReport();
                if (_strDocumentsMenu.ToLower() != "none")
                {
                    BindOpenMenu();
                }
                else
                {
                    imgMenuOpen.Visible = false;
                    menuOpen.Visible = false;
                }


                //if (Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
                //{
                BindAccountMenu();
                //}
            }
            else
            {
                lnkEndDemo.Visible = false;
                menuAccount.Visible = false;
                menuOpen.Visible = false;
                menuProfile.Visible = false;
            }



        }
        else
        {
            if (Session["LoginAccount"] == null)
            {
                Session.Clear();
                FormsAuthentication.SignOut();
                Response.Redirect("~/Login.aspx", false);
            }
            else
            {
                string strLoginAccount = Session["LoginAccount"].ToString();
                Session.Clear();
                FormsAuthentication.SignOut();
                Response.Redirect("~/Login.aspx?" + strLoginAccount, false);
            }

        }
    }

    protected void CheckSelectedMenu()
    {

        string path = Request.AppRelativeCurrentExecutionFilePath;
        bool bGotSelected = false;


        foreach (MenuItem item in menuETS.Items)
        {
            if (Request.RawUrl.IndexOf("Record/RecordList.aspx") > -1)
            {
                //
            }
            else
            {

                if (item.NavigateUrl.IndexOf(path) > -1)
                {
                    item.Selectable = true;
                    item.Selected = true;
                    bGotSelected = true;
                    break;
                }
            }


            //    item.Selected = item.NavigateUrl.Equals(path, StringComparison.InvariantCultureIgnoreCase);

            if (bGotSelected == false)
            {
                if (item.ChildItems.Count > 0)
                {

                    foreach (MenuItem citem in item.ChildItems)
                    {
                        if (citem.NavigateUrl != "")
                        {
                            if (Request.RawUrl.IndexOf(citem.NavigateUrl.Replace("~", "")) > -1)
                            {
                                if (citem.Value == item.Value)
                                {
                                    item.Selectable = true;
                                    item.Selected = true;
                                    bGotSelected = true;
                                    break;
                                }
                            }

                            if (bGotSelected == false)
                            {
                                if (citem.ChildItems.Count > 0)
                                {
                                    foreach (MenuItem ccitem in citem.ChildItems)
                                    {
                                        if (Request.RawUrl.IndexOf(ccitem.NavigateUrl.Replace("~", "")) > -1
                                            && ccitem.NavigateUrl.Trim() != "")
                                        {
                                            if (ccitem.Value == item.Value)
                                            {
                                                item.Selectable = true;
                                                item.Selected = true;
                                                bGotSelected = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        if (bGotSelected == false)
        {
            if (Request.RawUrl.IndexOf("Record/RecordDetail.aspx") > -1)
            {
                if (Request.QueryString["TableID"] != null)
                {

                    Table menuTable = RecordManager.ets_Table_Details(int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString())));

                    if (menuTable != null)
                    {

                        //Menu menuMenu = RecordManager.ets_Menu_Details((int)menuTable.ParentMenuID);

                        Menu menuMenu = RecordManager.ets_Menu_By_TableID((int)menuTable.TableID);

                        if (menuMenu != null && menuMenu.ParentMenuID != null)
                        {
                            Menu pMenu = RecordManager.ets_Menu_Details((int)menuMenu.ParentMenuID);

                            if (pMenu != null)
                                menuMenu = pMenu;
                        }

                        if (menuMenu != null)
                        {
                            foreach (MenuItem item in menuETS.Items)
                            {
                                if (menuMenu.MenuP == item.Value)
                                {
                                    item.Selectable = true;
                                    item.Selected = true;
                                    bGotSelected = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }


        if (bGotSelected == false)
        {
            if (Request.RawUrl.IndexOf("Record/RecordList.aspx") > -1 ||
                Request.RawUrl.IndexOf("Record/RecordUpload.aspx") > -1 ||
                Request.RawUrl.IndexOf("Record/UploadValidation.aspx") > -1 ||
                Request.RawUrl.IndexOf("Graph/RecordChart.aspx") > -1)
            {
                if (Request.QueryString["TableID"] != null)
                {

                    Table menuTable = RecordManager.ets_Table_Details(int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString())));

                    if (menuTable != null)
                    {
                        //Menu menuMenu = RecordManager.ets_Menu_Details((int)menuTable.ParentMenuID);
                        Menu menuMenu = RecordManager.ets_Menu_By_TableID((int)menuTable.TableID);

                        if (menuMenu != null && menuMenu.ParentMenuID != null)
                        {
                            Menu pMenu = RecordManager.ets_Menu_Details((int)menuMenu.ParentMenuID);

                            if (pMenu != null)
                                menuMenu = pMenu;
                        }

                        if (menuMenu != null)
                        {

                            foreach (MenuItem item in menuETS.Items)
                            {
                                if (menuMenu.MenuP == item.Value)
                                {
                                    item.Selectable = true;
                                    item.Selected = true;
                                    bGotSelected = true;
                                    break;
                                }

                            }

                        }

                    }

                }
            }

        }


        if (bGotSelected == false)
        {
            if (Request.RawUrl.IndexOf("Record/TableDetail.aspx") > -1 ||
                Request.RawUrl.IndexOf("Record/RecordColumnDetail.aspx") > -1 ||
                Request.RawUrl.IndexOf("Instrument/SensorDetail.aspx") > -1 ||
                Request.RawUrl.IndexOf("Document/DocumentTypeDetail.aspx") > -1 ||
                Request.RawUrl.IndexOf("Record/TableGroupDetail.aspx") > -1 ||
                Request.RawUrl.IndexOf("Instrument/SensorTypeDetail.aspx") > -1 ||
                Request.RawUrl.IndexOf("Site/WorkSiteDetail.aspx") > -1 ||
                Request.RawUrl.IndexOf("User/Detail.aspx") > -1 ||
                Request.RawUrl.IndexOf("Security/AccountDetail.aspx") > -1 ||
                Request.RawUrl.IndexOf("SystemData/ContentDetail.aspx") > -1 ||
                Request.RawUrl.IndexOf("SystemData/ErrorLogDetail.aspx") > -1 ||
                Request.RawUrl.IndexOf("SystemData/SystemOptionDetail.aspx") > -1 ||
                Request.RawUrl.IndexOf("Document/DocumentDetail.aspx") > -1 ||
                Request.RawUrl.IndexOf("Instrument/CalibrationDetail.aspx") > -1 ||
                Request.RawUrl.IndexOf("SystemData/VisitorLog.aspx") > -1 ||
                Request.RawUrl.IndexOf("/ChartDefinition/") > -1 ||
                Request.RawUrl.IndexOf("/DemoEmail.aspx") > -1 ||
                 Request.RawUrl.IndexOf("/Security/MakePayment.aspx") > -1 ||
                  Request.RawUrl.IndexOf("/DocGen/DocumentStypeEdit.aspx") > -1 ||
                   Request.RawUrl.IndexOf("/DocGen/DocumentStyleList.aspx") > -1 ||
                    Request.RawUrl.IndexOf("/Document/Report.aspx") > -1 ||
                     Request.RawUrl.IndexOf("/Document/ReportDetail.aspx") > -1 ||
                     Request.RawUrl.IndexOf("/Schedule/ScheduleReport.aspx") > -1 ||
                       Request.RawUrl.IndexOf("/Schedule/ScheduleReportDetail.aspx") > -1 ||
                    Request.RawUrl.IndexOf("/Security/ChangePassword.aspx") > -1)
            {
                foreach (MenuItem item in menuETS.Items)
                {
                    if ("Admin" == item.Value)
                    {
                        item.Selectable = true;
                        item.Selected = true;
                        bGotSelected = true;
                        break;
                    }

                }
            }

        }


    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            if (Session["tdbmsgpb"] != null)
            {
                lblNotificationMessage.Text = Session["tdbmsgpb"].ToString();
                Session["tdbmsgpb"] = null;

                if (lblNotificationMessage.Text != "")
                {
                    lblNotificationMessage.Text = lblNotificationMessage.Text + "&nbsp; <a id=\"aNotificationMessageClose\" onclick=\"document.getElementById('divNotificationMessage').style.display = 'none';return false;\" href=\"#\" >Close</a>";
                }
            }
            else
            {
                lblNotificationMessage.Text = "";
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        string jsReportDates = @" 
                            function runReport(reportID, option, pathHandler) {
  
                                if(option == 'adhoc'){  
                                    $('.ajax-indicator-full').hide();
                                    var linkReportDate = document.getElementById('runReportDates');
                                    linkReportDate.setAttribute('href', '/Pages/ReportWriter/ReportDates.aspx?ReportID='+ reportID); 
                                    linkReportDate.click();
                                     }     
                                else { //scheduled
                                    var linkReportDateScheduled = document.getElementById('runReportDatesScheduled');
                                    jsReportShowHideProgress('yes');
                                    linkReportDateScheduled.setAttribute('href', pathHandler);
                                    linkReportDateScheduled.click();
                                }  
                            } 
                        ";

        ScriptManager.RegisterStartupScript(this, this.GetType(), "jsReportDates", jsReportDates, true);

        string jsOpenReport = @" 
                            function openReport(type, mode, tableId, searchcriteriaId, recordId, documentId, recordIdx) { 
                                    $('.ajax-indicator-full').hide();
                                    var linkOpenReport = document.getElementById('openReport');
                                    if(type == 'add'){
                                        linkOpenReport.setAttribute('href', '/Pages/Record/RecordDetail.aspx?DocumentID='+ documentId +'&printreport=yes&mode=' + mode +'&TableID='+ tableId +'&SearchCriteriaID='+ searchcriteriaId); 
                                    }
                                    else{
                                        linkOpenReport.setAttribute('href', '/Pages/Record/RecordDetail.aspx?DocumentID='+ documentId +'&RecordID='+ recordId +'&printreport=yes&mode=' + mode +'&TableID='+ tableId +'&SearchCriteriaID='+ searchcriteriaId); 
                                    }
                                    
                                    linkOpenReport.click();
                            } 
                        ";

        ScriptManager.RegisterStartupScript(this, this.GetType(), "jsOpenReport", jsOpenReport, true);


        if (_theAccount.AccountTypeID == 1)//&& _theAccount.ExpiryDate < DateTime.Today
        {
            string strFreeAccountWhiteWash = SystemData.SystemOption_ValueByKey_Account("Free Account White Wash", int.Parse(Session["AccountID"].ToString()), null);
            if (strFreeAccountWhiteWash.ToLower() == "yes")
            {
                bWhiteWash = true;
                if (Request.RawUrl.IndexOf("Record/RecordList.aspx") > -1
                    || Request.RawUrl.IndexOf("Record/RecordDetail.aspx") > -1
                    || Request.RawUrl.IndexOf("ReportWriter/Report.aspx") > -1
                    )
                {
                    //pnlWhiteWash.Visible = true;
                    //Session["redirectfrompayment"] = Request.RawUrl;
                    string sDefaultPage = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Default.aspx";
                    string strFancywhitewash = @"
                    $(function () {
                           $('.popupwhitewash').fancybox({
                                 iframe : {
                                    css : {
                                        width : '700px',
                                        height: '650px'
                                    }
                                },       
                                toolbar  : false,
	                            smallBtn : true,  
                                scrolling: 'auto',
                                type: 'iframe',
                                'transitionIn': 'elastic',
                                'transitionOut': 'none',
                                titleShow: false,
                                afterClose: function () {$('.ajax-indicator-full').show(); window.location.href = '" + sDefaultPage + @"';}
                            });
                        });
                         
                        $('.popupwhitewash').fancybox({
                                helpers:
                                    {
                                        overlay:
                                        {
                                              css: { 'background': 'rgba(255, 255, 255, 0.5)' }           
                                            //opacity: 0.1, // or the opacity you want 
                                            //css: { 'background-color': '#ff0000'} // or your preferred hex color value
                                        } // overlay 
                                    } // helpers
                                }); // fancybox



                         $('.popupwhitewash').fancybox({ afterShow: function() { $( '.fancybox-content' ).draggable();} });

                        $(document).ready(function () {

                            try
                            {
                                $('#hlWhiteWash').trigger('click');
                               // window.setTimeout($('#hlWhiteWash').trigger('click'),1000);
                            }
                            catch(err)
                            {
                                //
                            }                                                            
                        });



                        
                ";

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "strFancywhitewash", strFancywhitewash, true);

                    //ltMasterStyles.Text = ltMasterStyles.Text
                    //    + @"
                    //          <style type='text / css'>  
                    //              .fancybox-bg {
                    //                        background: #ff0000;
                    //                    }

                    //                    .fancybox-is-open .fancybox-bg {
                    //                        opacity: .6;
                    //                    }
                    //        </style>
                    //     "

                    //    ;

                }
            }
        }
        else
        {
            //pnlWhiteWash.Visible = false;
        }


        // Check session is expire or timeout. 
        if (Session["User"] == null)
        {
            Response.Redirect(ResolveUrl("~/Login.aspx?Logout=yes&ReturnURL=" + HttpUtility.UrlEncode(HttpContext.Current.Request.Url.PathAndQuery)), true);
            return;
        }

        lblCopyRightYear.Text = DateTime.Now.Year.ToString();

        /* ======================================= 
      = Red Feb-07-19: Commented out script below as per Jon Request
        ======================================= */
        /* == Red Start */
        // Get user login time or last activity time. 
        //DateTime date = DateTime.Now;
        //loginDate = date.ToString("u", DateTimeFormatInfo.InvariantInfo).Replace("Z", "");
        //int sessionTimeout = Session.Timeout;
        //DateTime dateExpress = date.AddMinutes(sessionTimeout);
        //expressDate = dateExpress.ToString("u", DateTimeFormatInfo.InvariantInfo).Replace("Z", "");
        /* == Red End */



        if (Request.QueryString["public"] != null)
        {
            imgHouse.Visible = false;
            imgResponsiveHouse.Visible = false;
            imgMenuOpen.Visible = false;
            Image2.Visible = false;

            PopulatePublicAccount();



            return;
        }

        if (Session["AccountID"] == null)
        {
            //if (Session["LoginAccount"] == null)
            //{
            //    Response.Redirect("~/Login.aspx",false);
            //}
            //else
            //{
            //    Response.Redirect("~/Login.aspx?" + Session["LoginAccount"].ToString(),false);
            //}

            if (Session["LoginAccount"] == null)
            {
                Session.Clear();
                Response.Redirect(Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Login.aspx?ReturnURL=" + Server.UrlEncode(Request.RawUrl), false);

            }
            else
            {
                string strLoginAccount = Session["LoginAccount"].ToString();
                Session.Clear();
                Response.Redirect(Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Login.aspx?ReturnURL=" + Server.UrlEncode(Request.RawUrl) + "&" + strLoginAccount, false);

            }
            return;


        }


        double iSec = 3;
        string strTopMsgNoOfMS = "3000";
        string strTopMessageDisplayNumberSeconds = SystemData.SystemOption_ValueByKey_Account("Top Message Display Number Seconds", _theAccount.AccountID, null);


        if (strTopMessageDisplayNumberSeconds != "")
        {
            double dTemp = 0;
            if (double.TryParse(strTopMessageDisplayNumberSeconds, out dTemp))
            {
                if (dTemp > 300)
                    dTemp = 300;

                iSec = dTemp;
                dTemp = dTemp * 1000;
                strTopMsgNoOfMS = dTemp.ToString("N0").Replace(",", "");

            }

        }



        ltMasterStyles.Text = @"<style  type='text/css'>
                            .cssanimations.csstransforms #divNotificationMessage {
                    -webkit-transform: translateY(-50px);
                    -webkit-animation: slideDown " + iSec.ToString() + @"s 0.2s 1 ease forwards;
                    -moz-transform:    translateY(-50px);
                    -moz-animation:    slideDown " + iSec.ToString() + @"s 0.2s 1 ease forwards;
                }
 </style>
            ";

        string strWebsiteMasterPageHeader = SystemData.SystemOption_ValueByKey_Account("Website Master Page Header", int.Parse(Session["AccountID"].ToString()), null);

        if (strWebsiteMasterPageHeader != "")
            this.Page.Title = strWebsiteMasterPageHeader;

        if (!Page.IsPostBack)
        {
            //Red Ticket 2653 22may2017
            hfIsTimerOutSet.Value = SystemData.SystemOption_ValueByKey_Account("Set Timer Logout", int.Parse(Session["AccountID"].ToString()), null);
            //end Red

            hlRenewNow.NavigateUrl = SystemData.SystemOption_ValueByKey_Account("ContactUsRenewal", null, null);
            //DataTable dtActiveTableList = Common.DataTableFromText("SELECT * FROM [Table] WHERE IsActive=1 AND AccountID=" + Session["AccountID"].ToString());
            //if (dtActiveTableList.Rows.Count == 0)
            //{
            //    divFirstTable.Visible = true;
            //}

            //string strHideDocumentsMenu = SystemData.SystemOption_ValueByKey_Account("Hide Documents Top Menu", int.Parse(Session["AccountID"].ToString()), null);

            //if (strHideDocumentsMenu != "" && strHideDocumentsMenu.ToLower() == "yes")
            //{
            //    menuOpen.Visible = false;
            //    imgMenuOpen.Visible = false;
            //}


            if (Session["LoginAccount"] != null)
            {
                //divMarketingMenu.Visible = false;
                Account theAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));

                if (theAccount != null)
                {
                    divCopyright.InnerText = theAccount.CopyRightInfo;

                }

            }
        }



        //if (Session["ExpireLeftDay"] != null)
        //{




        //Account theAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));
        //Session["DoNotAllow"] = null;
        //if (theAccount.ExpiryDate != null)
        //{
        //    Session["ExpireLeftDay"] = Common.DaysBetween((DateTime)DateTime.Today, (DateTime)theAccount.ExpiryDate);

        //    if (theAccount.ExpiryDate.Value.AddDays(0) < DateTime.Today)
        //    {
        //        Session["DoNotAllow"] = "true";

        //    }

        //}

        //if (int.Parse(Session["ExpireLeftDay"].ToString()) < 10)
        //{
        //    divRenew.Visible = true;
        //    if (Session["DoNotAllow"] == null)
        //    {
        //        lblRenewMessage.Text = "Account expires in " +
        //        Session["ExpireLeftDay"].ToString() + " days. ";
        //    }
        //    else
        //    {
        //        Response.Redirect("~/Login.aspx?Logout=yes", true);
        //        return;
        //    }
        //}
        //}
        //else
        //{

        //this is for the data exceed
        divRenew.Visible = false;
        if (Session["DoNotAllow"] != null)
        {
            //Response.Redirect("~/Login.aspx?Logout=yes", true);
            //return;
        }
        //}

        //RP - Temporarily removed 09/25/2018
        //if (!Page.IsPostBack)
        //{


        /*
        //    if(!Context.User.Identity.IsAuthenticated && Request.QueryString["autologin"]==null)
        //    {
        //        FormsAuthentication.RedirectToLoginPage();
        //    }
        //}
        */
        CheckSelectedMenu();



        string strHidedivNotificationMessage = @"                                                     
                                                  $(document).ready(function () {

                                                      try
                                                        {
                                                            window.setTimeout(HidedivNotificationMessage," + strTopMsgNoOfMS + @");
                                                        }
                                                      catch(err)
                                                        {
                                                            //
                                                        }                                                            
                                                    });
                                                ";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "strHidedivNotificationMessage", strHidedivNotificationMessage, true);




        if (!Page.IsPostBack)
        {


            //if (Session["IsFlashSupported"] == null )
            //{
            string strIsFlashSupportedJS = @"                                                     
                                                   $(document).ready(function () {

                                                        function IsFlashSupported() {
                                                            try
                                                            {
                                                                var hfFlashSupport = document.getElementById('" + hfFlashSupport.ClientID + @"');
                                                                if (swfobject.hasFlashPlayerVersion('1')) {
                                                                    hfFlashSupport.value = 'yes';
                                                                }
                                                                else {
                                                                    hfFlashSupport.value = 'no';
                                                                }
                                                                //alert(hfFlashSupport.value);

                                                                $.ajax({
                                                                    url: '" + ResolveUrl("~/Pages/DocGen/REST/SectionREST.ashx") + @"?type=FlashSupport&hfFlashSupport=' + hfFlashSupport.value,
                                                                    cache: false,
                                                                    success: function (content) {
                                                                       //
                                                                    },
                                                                    error: function (a, b, c) {
                                                                       //
                                                                    }
                                                                });
                                                            }
                                                            catch(err)
                                                            {
                                                                //
                                                            }             


                                                        }
                                                        IsFlashSupported();
                                                        //setTimeout(function () { IsFlashSupported(); }, 1000);
                                                    });
                                                    
                                                ";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "strIsFlashSupportedJS", strIsFlashSupportedJS, true);


            //                if(Session["Resize"]!=null)
            //                {
            //                    Session["Resize"] = null;
            //                    string strResize = @"                                                     
            //                                                   $(document).ready(function () {
            //
            //                                                        function resize() {
            //                                                            try
            //                                                            {
            //                                                               var innerWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
            //                                                                var innerHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
            //                                                                var targetWidth =  $(window).width()-200;
            //                                                                var targetHeight = 600;
            //                                                                window.resizeBy(targetWidth-innerWidth, targetHeight-innerHeight);
            //                                                                window.focus();
            //                                                            }
            //                                                            catch(err)
            //                                                            {
            //                                                                //
            //                                                            }             
            //
            //
            //                                                        }
            //                                                        
            //                                                        if(navigator.userAgent.toLowerCase().indexOf('chrome') > -1)
            //                                                            setTimeout(function () { resize(); }, 200);
            //                                                        else
            //                                                            resize();
            //
            //                                                    });
            //                                                    
            //                                                ";
            //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "strResize", strResize, true);

            //                }
            //}
            //hlReport.NavigateUrl = Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/Report.aspx?SSearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");
            //hlDocuments.NavigateUrl = Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/Document.aspx?SSearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");

            string strRefSite = "";

            if (Request.QueryString["Ref"] != null)
            {
                strRefSite = Request.QueryString["Ref"].ToString();
            }

            SystemData.VisitorInsert(_objUser, Request.UserHostAddress, Request.UserAgent, Request.AppRelativeCurrentExecutionFilePath, strRefSite);

            //User _objUser = (User)Session["User"];
            if (_objUser != null && _objUser.UserID > 0 && Session["DemoEmail"] != null)
            {
                if (_objUser.Email.ToLower() == Session["DemoEmail"].ToString().ToLower())
                {
                    trEndDemo.Visible = true;
                }
            }
            //CheckConcurrent();
            CheckDoNotAllow();
        }

        //        string  strCellToolTip = @"var mouseX;
        //                                var mouseY;
        //                                $(document).mousemove(function (e) {
        //                                    try
        //                                    {
        //                                        mouseX = e.pageX;
        //                                        mouseY = e.pageY;
        //                                    }
        //                                    catch (err)
        //                                    {
        //                                       // alert(err.message)
        //                                    }
        //        
        //                                });
        //    
        //
        //                                $(function () {
        //        
        //                                    $('.js-tooltip-container').hover(function () {
        //                                        //$(this).find('.js-tooltip').show();
        //                                        try {
        //                                            $(this).find('.js-tooltip').addClass('ajax-tooltip');
        //                                            $(this).find('.ajax-tooltip').css({ 'top': mouseY, 'left': mouseX }).fadeIn('slow');
        //                                        }
        //                                        catch (err) {
        //                                           // alert(err.message);
        //                                        }
        //                                    }, function () {
        //                                        try {
        //                                            $(this).find('.js-tooltip').hide();
        //                                            $(this).find('.js-tooltip').removeClass('ajax-tooltip');
        //                                            $(this).find('.ajax-tooltip').css({ 'top': mouseY, 'left': mouseX }).fadeOut('slow');
        //                                        }
        //                                        catch (err) {
        //                                            //alert(err.message);
        //                                        }
        //                                    });
        //       
        //                                });";


        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "strCellToolTip", strCellToolTip, true);


        //notifications



        if (!IsPostBack)
        {
            if (Session["tdbmsg"] != null)
            {
                lblNotificationMessage.Text = Session["tdbmsg"].ToString();
                Session["tdbmsg"] = null;

                if (lblNotificationMessage.Text != "")
                {
                    lblNotificationMessage.Text = lblNotificationMessage.Text + "&nbsp; <a id='aNotificationMessageClose' href='#'>Close</a>";
                }
            }
            else
            {
                lblNotificationMessage.Text = "";
            }

        }
        else
        {
            Session["tdbmsg"] = null;
        }

        //Red System Option get timer set value
        //Ticket 2653 22may2017
        if (hfIsTimerOutSet.Value != null && hfIsTimerOutSet.Value != "" && int.Parse(hfIsTimerOutSet.Value.ToString()) > 0)
        {
            string currentUrl = Request.Url.AbsoluteUri.ToLower();
            string reasonForChange = "";
            if (Request.QueryString["TableID"] != null && currentUrl.IndexOf("recorddetail.aspx") > 0)
            {
                int iTableID = int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
                reasonForChange = Common.GetValueFromSQL("SELECT ReasonChangeType FROM [Table] WHERE TableID = " + iTableID);
            }

            int iTimerLeft = 30; // 30 seconds
            int iWarningTime = 0;
            int iLogoutTimer = 0;

            iWarningTime = int.Parse(hfIsTimerOutSet.Value) * 1000;
            iLogoutTimer = iWarningTime + 30000; // add 30 seconds                                                

            string strTimerLogout = @"  

            $('#lnkResetTimer').click(function(e){
                ResetTimersButton(); return false;
            });
            
            document.body.onload = function(e){
                StartTimers();
            }; 
            document.body.onmousemove = function(e){
                    ResetTimers();
            };

            document.body.onmouseenter = function(e){
                    ResetTimers();
            };

            document.body.onmousewheel = function(e){
                    ResetTimers();
            };

            $(document).mousedown(function(event){
                 ResetTimers();
            });

            document.body.onmousedown = function(e){
                    ResetTimers();
            };

            document.body.onkeydown = function(e){
                ResetTimers();
            };
         
             // Lets set timeout variables.         
            var timoutWarning = '" + iWarningTime + @"';
            var timoutNow = '" + iLogoutTimer + @"';            
            //RP Added Ticket 4305
            var logoutUrl = '" + ResolveUrl("~/Login.aspx?Logout=yes&ReturnURL=" + HttpUtility.UrlEncode(HttpContext.Current.Request.Url.PathAndQuery)) + @"';
            var currentURL = window.location.toString().toLowerCase();
            var warningTimer = null;
            var vTimerLeft_Max=0;
            var timeCountIncrement=0;
            var timeCountInterval = null;
            var remaining_time = 0;
            var reasonForChange = '" + reasonForChange + @"';
            


         // Lets start the timers.
        function StartTimers() {
            try
            {
                deleteCookieSetTime();
                jsSetLogoutTime();
                clearTimeout(warningTimer);
                clearTimeout(timeCountInterval);
                timeCountIncrement=0;
                vTimerLeft_Max = '" + iTimerLeft + @"'; 
                remaining_time = 0;
                warningTimer = setTimeout(IdleWarning, timoutWarning);
            }
            catch(err)
            {
                //
            }     
         }


        // Lets reset the timers when mouse move and keydown.
        function ResetTimers() {
            try
            {
              if ($('#timeout').is(':visible')) {
                //do nothing
                }
                else {
                    deleteCookieSetTime();
                    jsSetLogoutTime();
                    StartTimers();
                    // $('#timeout').dialog('close');
                }
            }
            catch(err)
            {
                //
            }     
           
        }      

        function ResetTimersButton() {

            try
            {
               deleteCookieSetTime();
               jsSetLogoutTime();
                StartTimers();
                $(""#timeout"").dialog('close');
       
            }
            catch(err)
            {
                alert(err);
            }     
          
    }

        // Lets show warning
        function IdleWarning() {          
            clearTimeout(warningTimer);    
            timerCount();
            $('#timeout').dialog({   
                title: ""Warning Log-off"",        
                width: 460,
                height: 420,
                modal: true, 
                resizable: false,

            close: function() {
                ResetTimersButton();

            }

            });
       
        }

        //Timer Countdown
        function timerCount() { 
          
  
            timeCountIncrement = timeCountIncrement + 1; 
            remaining_time = vTimerLeft_Max - timeCountIncrement;

            /* == Red 13092019: Ticket 4854, once the user's machine is off (shutdown or hybernate) == */
            var currentDateToday = new Date();
            var timeCookieLastActivity = new Date(getCookieSetTime());
            var IdleTimeMilliSeconds = currentDateToday.getTime() - timeCookieLastActivity.getTime();
            var IdleTimeSeconds = IdleTimeMilliSeconds / 1000;

            if(IdleTimeSeconds >= (timoutNow/1000)){
                IdleTimeout();
            }
            /* == End Red == */

  	        if( remaining_time == 0  ){ 
                if (reasonForChange == 'mandatory' && currentURL.indexOf('recorddetail.aspx') > 0) 
                    { 
                        ValidatorEnable(document.getElementById('ctl00_HomeContentPlaceHolder_rfvReasonForChange'), false);
                    }
   		        $('#seconds-timer').html(remaining_time);
                 IdleTimeout();

	        }
            else{
          
    	        $('#seconds-timer').html(remaining_time);
    	        timeCountInterval = setTimeout(timerCount, 1000);
	        }
        }

        // Lets logout the user
        function IdleTimeout() {   
            if (confirmBeforeLeaving && currentURL.indexOf('recorddetail.aspx') > 0)  //&& reasonForChange != 'mandatory') 
            {
                    $('#lnkResetTimer').hide(); 
                    $('#attempToSaveRecord').show();
                    if (Page_IsValid) {
                        $('#hfLogmeOut').val('true');
                        setTimeout(function () {
                            //lets click the save button to save record before logging out
                            //2 seconds to show the label Attempting to save the record...
                            document.getElementById('ctl00_HomeContentPlaceHolder_lnkSaveClose').click();
                        }, 2000);
                    }
            }
            else 
            {
                confirmBeforeLeaving = false;
                //deleteCookieSetTime();
                //lets logout if no changes made in the record 
                window.location = logoutUrl; 
            }
        }";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "JSTimer", strTimerLogout, true);
        }

        //end Red

        //RP Added Ticket 4978
        if(!Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
        {
            String systemoptionvalues = SystemData.SystemOption_ValueByKey_Account("Administrator Menu", null, null);
            System.Collections.ArrayList adminmenus = new System.Collections.ArrayList(systemoptionvalues.Replace(", ", ",").Split(','));
            List<MenuItem> removemenulist = new List<MenuItem>();
            if (systemoptionvalues != "")
            {
                if (!adminmenus.Contains("All"))
                {
                    MenuItem administratormenu = menuETS.FindItem("Admin");
                    if (administratormenu != null)
                    {
                        foreach (MenuItem i in administratormenu.ChildItems)
                        {
                            if (!adminmenus.Contains(i.Text))
                            {
                                removemenulist.Add(i);
                            }
                        }

                        foreach (MenuItem temp in removemenulist)
                        {
                            menuETS.FindItem("Admin").ChildItems.Remove(temp);
                        }
                    }
                }
            }
        }
        //End Modification

        //RP Added Ticket 4445
        CreateMenu(menuETS, menuLiteralETS2);
        CreateMenu(menuProfile2, menuLiteralProfile2);
        CreateMenu(menuOpen2, menuLiteralDocs2);
        CreateMenu(menuAccount2, menuLiteralAccount2);
        //End Modification 

    }

    //RP Added Ticket 4978
    //protected void ShowHideAdminMenu(System.Collections.ArrayList adminmenus, MenuItemCollection mc)
    //{
    //    foreach (MenuItem mi in mc)
    //    {
    //        if (!adminmenus.Contains(mi.Text))
    //        {
    //            mc.Remove(mi);
    //        }
    //    }
    //}
    //End Modification
    //R{ Added Ticket 4445
    protected void CreateMenu(System.Web.UI.WebControls.Menu menu, Literal literal)
    {
        String a = "";
        foreach (MenuItem mi in menu.Items)
        {
            if (mi.ChildItems.Count > 0)
            {
                a = a + CreateSubItems(mi);
            }
            else
            {
                a = a + "<li><a href=\"" + Page.ResolveClientUrl(mi.NavigateUrl) + "\">" + mi.Text + "</a></li>";
            }
        }
        literal.Text = a;
    }

    protected String CreateSubItems(MenuItem mi)
    {
        String a = "";
        a = a + "<li class=\"dropdown-submenu\">";
        a = a + "<a class=\"test\" tabindex=\"-1\" href=\"#!\">" + mi.Text + "</a>";
        a = a + "<ul class=\"dropdown-menu\">";
        foreach (MenuItem subi in mi.ChildItems)
        {
            if (subi.ChildItems.Count > 0)
            {
                a = a + CreateSubItems(subi);
            }
            else
            {
                a = a + "<li><a href=\"" + Page.ResolveClientUrl(subi.NavigateUrl) + "\">" + subi.Text + "</a></li>";
            }
        }
        a = a + "</ul>";
        a = a + "</li>";
        return a;
    }

    //End Modification
    protected void PopulatePublicAccount()
    {
        try
        {
            if (Request.QueryString["TableID"] != null)
            {
                int iTableID = int.Parse(Cryptography.Decrypt(Request.QueryString["TableID"].ToString()));
                Table theTable = RecordManager.ets_Table_Details(iTableID);
                if (theTable != null)
                {
                    Account theAccount = SecurityManager.Account_Details((int)theTable.AccountID);

                    if (theAccount != null)
                    {
                        if (theAccount.Logo != null)
                        {
                            if ((bool)theAccount.UseDefaultLogo == false)
                                imgLogo.ImageUrl = "~/SSPhoto.ashx?AccountID=" + theAccount.AccountID.ToString() + "&type=o";
                        }

                    }

                }

            }

        }
        catch
        {
        }
    }

    //protected void CheckConcurrent()
    //{
    //    if (Session["User"] != null)
    //    {
    //        //User _objUser = (User)Session["User"];

    //        string strSessionID = SecurityManager.User_SessionID_Get((int)_objUser.UserID);

    //        if (strSessionID != "")
    //        {
    //            if (strSessionID != Session.SessionID.ToString())
    //            {
    //                //ops 
    //                SecurityManager.User_LoggedOutCount_Increment((int)_objUser.UserID);
    //                //lkLogout_Click(null, null);

    //                //Session["User"] = null;
    //                //Session.Abandon();

    //                //FormsAuthentication.SignOut();

    //                //HttpCookie oUseInfor = new HttpCookie("UserInformation", "nothing");
    //                //oUseInfor.Expires = DateTime.Now.AddDays(-3d);
    //                //Response.Cookies.Add(oUseInfor);                  

    //                //Response.Redirect("~/Login.aspx?Logout=concurrent", false);

    //            }


    //        }
    //    }


    //}

    protected void CheckDoNotAllow()
    {
        if (Request.RawUrl.IndexOf("Security/AccountDetail.aspx") > -1) //Request.RawUrl.IndexOf("/Security/AccountTypeChange.aspx") > -1 ||
        {
            //its ok
        }
        else
        {
            if (Session["DoNotAllow"] != null)
            {
                if (Session["DoNotAllow"].ToString() == "true")
                {
                    //Response.Redirect("~/Pages/Security/AccountTypeChange.aspx?type=renew", false);
                    //Response.Redirect("~/Default.aspx",false);

                }

            }

        }

    }

    protected void PopulateSubMenu(ref MenuItem menuRoot, ref MenuItem menuParent, int iParentMenuID)
    {
        string strAppPath = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath;

        /* == Red 24062019: Manu, updated the above script with ReportID and IsAdvancedSecurity ==*/
        DataTable dtSubMenu =
            Common.DataTableFromText(@"SELECT DocumentID, " +
                                        "ReportID, " +
                                        "DocumentTypeID," +
                                        "MenuID, " +
                                        "Menu, " +
                                        "Menu.TableID, " +
                                        "TableName," +
                                        "ExternalPageLink," +
                                        "OpenInNewWindow, " +
                                        "IsAdvancedSecurity, " +
                                        "MenuType " +
                                      "FROM Menu " +
                                      "LEFT JOIN [Table] ON [Table].TableID=Menu.TableID " +
                                      "WHERE Menu.IsActive=1 AND " +
                                      "Menu.AccountID=" + Session["AccountID"].ToString() + @" AND " +
                                      "ParentMenuID=" + iParentMenuID.ToString() +
                                      @" ORDER BY Menu.DisplayOrder");



        foreach (DataRow drSubMenu in dtSubMenu.Rows)
        {
            MenuItem miTempChild = new MenuItem();
            miTempChild.Text = drSubMenu["Menu"].ToString();
            miTempChild.Value = menuRoot.Value;

            /* == Red 24062019: Menu ==*/
            bool bShowMenu = false;
            bool IsAdvancedSecurity = drSubMenu["IsAdvancedSecurity"] == DBNull.Value ? false : (bool)drSubMenu["IsAdvancedSecurity"]; // Red 11092019: This is the-menu-isadvancedsecurity
            //bool IsAdvancedSecurity= drSubMenu["IsAdvancedSecurity"] == null ? false : (bool)drSubMenu["IsAdvancedSecurity"]; // Red 11092019: This is the-menu-isadvancedsecurity
            if (drSubMenu["TableID"] == DBNull.Value && !IsAdvancedSecurity)
            {
                bShowMenu = true;
            }
            else if (Common.HaveAccess(Session["roletype"].ToString(), "1") || (bool)_CurrentUserRole.IsAccountHolder)
            {
                DataTable dtMenuRole =  SecurityManager.Menu_Role_Details((int)drSubMenu["MenuID"]);
                if(dtMenuRole != null)
                {
                    if (dtMenuRole.Rows.Count > 0)
                    {
                        bShowMenu = true;
                    }
                }
            }
            else
            {
                bShowMenu = SecurityManager.Menu_ShowForUser((int)drSubMenu["MenuID"], (int)_CurrentUserRole.UserID);
            }

          
            /* == End Red == */

            /* == Red 10092019: Transferred below == */
            //if (miTempChild.Text == Common.MenuDividerText)
            //{
            //    miTempChild.Selectable = false;
            //    miTempChild.Text = "<hr />";
            //}
            /* == End Red ==*/

            if (drSubMenu["OpenInNewWindow"] != DBNull.Value && (bool)drSubMenu["OpenInNewWindow"])
            {
                miTempChild.Target = "_blank";
            }
            if (drSubMenu["TableID"] != DBNull.Value)
            {
                if (drSubMenu["Menu"].ToString() == "")
                    miTempChild.Text = drSubMenu["TableName"].ToString();


                miTempChild.NavigateUrl = "~/Pages/Record/RecordList.aspx?TableID=" +
                    Cryptography.Encrypt(drSubMenu["TableID"].ToString());

                /* Red, so here... we add the table Menu if user has access to the table as Advanced Security
                * else data owner */
                if (bShowMenu)
                {
                    menuParent.ChildItems.Add(miTempChild);
                }
            }           
            else if (drSubMenu["ReportID"] != DBNull.Value && drSubMenu["MenuType"].ToString() == "r")
            {
                Report report = ReportManager.ets_Report_Detail(int.Parse(drSubMenu["ReportID"].ToString()));
                if (report != null)
                {
                    if ((!report.ReportStartDate.HasValue 
                        && !report.ReportEndDate.HasValue 
                        && report.ReportDates == Report.ReportInterval.IntervalDateRange))
                    {
                        miTempChild.NavigateUrl = "javascript: runReport(" + drSubMenu["ReportId"].ToString() + ",'adhoc'); ";
                    }
                    else
                    {
                        string pathHandler = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                 "Pages/ReportWriter/ReportHandler.ashx" +
                                 "?ReportID=" + Cryptography.Encrypt(report.ReportID.ToString());
                        miTempChild.NavigateUrl = "javascript: runReport(" + drSubMenu["ReportId"].ToString() + ",'scheduled','" + pathHandler + "'); ";
                    }
                  
                       
                }
                if (bShowMenu)
                    menuParent.ChildItems.Add(miTempChild);

            }
            else if (drSubMenu["DocumentTypeID"] != DBNull.Value)
            {
                string strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/Document.aspx?Category=" + Cryptography.Encrypt(drSubMenu["DocumentTypeID"].ToString()) + "&TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");
                miTempChild.NavigateUrl = strURL;

                if (bShowMenu)
                    menuParent.ChildItems.Add(miTempChild);
            }
            else if (drSubMenu["MenuType"] != DBNull.Value && drSubMenu["MenuType"].ToString() == "doc")
            {
                string strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/Document.aspx?TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");

                miTempChild.NavigateUrl = strURL;

                if (bShowMenu)
                    menuParent.ChildItems.Add(miTempChild);

            }          
            else if (drSubMenu["DocumentID"] != DBNull.Value)
            {
                //
                string strURL = "#";
                bool withReportOptions = false;
                Document theDocument = DocumentManager.ets_Document_Detail(int.Parse(drSubMenu["DocumentID"].ToString()));
                if (theDocument != null)
                {

                    if (theDocument.ReportType == "ssrs")
                    {
                        strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/SSRS.aspx?DocumentID=" + Cryptography.Encrypt(theDocument.DocumentID.ToString()) + "&SearchCriteria=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SSearchCriteriaID=" + Cryptography.Encrypt("-1");

                        if (theDocument._iReportOptionTableID != null)
                        {
                            withReportOptions = true;
                        }
                    }
                    else if (theDocument.DocumentTypeID != null)
                    {

                        strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/DocGen/View.aspx?DocumentID=" + theDocument.DocumentID.ToString() + "&SearchCriteria=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SSearchCriteriaID=" + Cryptography.Encrypt("-1");
                    }

                }
                if (withReportOptions)
                {
                    //send recordid if table is not null, get the max recordid...
                    string userReportOptionRecordId = "";
                    string strMode = "add";
                    userReportOptionRecordId =
                        Common.GetValueFromSQL("SELECT MAX(RecordID) " +
                        "FROM Record WHERE IsActive = 1 " +
                        "AND TableID=" + theDocument._iReportOptionTableID.ToString() +
                        " AND EnteredBy=" + _CurrentUserRole.UserID.ToString());

                    if (string.IsNullOrEmpty(userReportOptionRecordId))
                    {
                        miTempChild.NavigateUrl = "javascript: openReport('add','" + Cryptography.Encrypt(strMode) + "','" + Cryptography.Encrypt(theDocument._iReportOptionTableID.ToString()) + "','" + Cryptography.Encrypt("-1") + "','" + Cryptography.Encrypt("-1") + "','" + theDocument.DocumentID.ToString() + "','-1'); ";
                    }
                    else
                    {
                        strMode = "edit";
                        miTempChild.NavigateUrl = "javascript: openReport('edit','" + Cryptography.Encrypt(strMode) + "','" + Cryptography.Encrypt(theDocument._iReportOptionTableID.ToString()) + "','" + Cryptography.Encrypt("-1") + "','" + Cryptography.Encrypt(userReportOptionRecordId) + "','" + theDocument.DocumentID.ToString() + "','" + userReportOptionRecordId + "'); ";
                    }

                    
                }
                else
                {
                    miTempChild.NavigateUrl = strURL;
                }
              

                if (bShowMenu)
                    menuParent.ChildItems.Add(miTempChild);
            }
            else if (drSubMenu["ExternalPageLink"] != DBNull.Value)
            {
                string strExternalPageLink = drSubMenu["ExternalPageLink"].ToString();
                if (!string.IsNullOrEmpty(strExternalPageLink))
                {
                    miTempChild.NavigateUrl = strExternalPageLink;
                }

                if (bShowMenu)
                    menuParent.ChildItems.Add(miTempChild);
            }
            else if(miTempChild.Text == Common.MenuDividerText && drSubMenu["MenuType"].ToString() == "d") /* Red 10092019: This is divider, we always show it */
            {
                miTempChild.Selectable = false;
                miTempChild.Text = "<hr />";
                menuParent.ChildItems.Add(miTempChild);
            }
            else
            {
                if (bShowMenu)
                    menuParent.ChildItems.Add(miTempChild);

            }
            PopulateSubMenu(ref menuRoot, ref miTempChild, int.Parse(drSubMenu["MenuID"].ToString()));
        }
        if (Request.QueryString["viewon"] != null &&
            Cryptography.Decrypt(Request.QueryString["viewon"].ToString()).ToLower().Equals("app"))
        {
            divRightMenu.Visible = false;
            divLeftMenu.Visible = false;
        }

    }

    protected void BindMenuProfile()
    {
        menuProfile.Items.Clear();
        string strAppPath = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath;

        //User _objUser = (User)Session["User"];

        if (_objUser != null && _objUser.UserID > 0)
        {
            MenuItem miUserName = new MenuItem();
            miUserName.Text = _objUser.FirstName + " " + _objUser.LastName;
            miUserName.NavigateUrl = "~/Default.aspx";
            miUserName.Value = "UserName";
            menuProfile.Items.Add(miUserName);
            //if (!Common.HaveAccess(Session["roletype"].ToString(), Common.UserRoleType.ReadOnly))
            //{

            if (_theAccount.HideMyAccount != null && (bool)_theAccount.HideMyAccount)
            {

            }
            else
            {
                //if (!Common.HaveAccess(Session["roletype"].ToString(), Common.UserRoleType.OwnData))
                //{
                MenuItem miMyAccount = new MenuItem();
                miMyAccount.Text = "My Account";
                miMyAccount.Value = "UserName";
                miMyAccount.NavigateUrl = "~/Pages/Security/AccountDetail.aspx?mode=" + Cryptography.Encrypt("edit") + "&accountid=" + Cryptography.Encrypt(Session["AccountID"].ToString());
                miUserName.ChildItems.Add(miMyAccount);

                MenuItem miMyAccount2 = new MenuItem();
                miMyAccount2.Text = miMyAccount.Text;
                miMyAccount2.Value = miMyAccount.Value;
                miMyAccount2.NavigateUrl = miMyAccount.NavigateUrl;
                menuProfile2.Items.Add(miMyAccount2);
                //}
            }


            //}

            //Ticket 3605
            //if (Common.GetDatabaseName().ToLower().IndexOf("_ontask") > 0
            //    && Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
            //{
            //    MenuItem miSubscriptionManager = new MenuItem();
            //    miSubscriptionManager.Text = "My Subscription";
            //    miSubscriptionManager.Value = "UserName";
            //    miSubscriptionManager.NavigateUrl = "~/Pages/Security/SubscriptionManagement.aspx";
            //    miUserName.ChildItems.Add(miSubscriptionManager);
            //}
            //End Modification


            string strHideLinkMenu = SystemData.SystemOption_ValueByKey_Account("Hide Link Account Menu", int.Parse(Session["AccountID"].ToString()), null);

            if (strHideLinkMenu != "" & strHideLinkMenu.ToLower() == "yes")
            {
                //avoid the menu
            }
            else
            {
                MenuItem miLinkToAnotherAccount = new MenuItem();
                miLinkToAnotherAccount.Text = "Link to another Account";
                miLinkToAnotherAccount.Value = "UserName";
                miLinkToAnotherAccount.NavigateUrl = "~/Pages/User/AddAccount.aspx?menu=yes&UserID=" + _objUser.UserID.ToString();
                miUserName.ChildItems.Add(miLinkToAnotherAccount);

                MenuItem miLinkToAnotherAccount2 = new MenuItem();
                miLinkToAnotherAccount2.Text = miLinkToAnotherAccount.Text;
                miLinkToAnotherAccount2.Value = miLinkToAnotherAccount.Value;
                miLinkToAnotherAccount2.NavigateUrl = miLinkToAnotherAccount.NavigateUrl;

                menuProfile2.Items.Add(miLinkToAnotherAccount2);
            }



            //MenuItem miChangePassword = new MenuItem();
            //miChangePassword.Text = "Change Password";
            //miChangePassword.Value = "UserName";
            //miChangePassword.NavigateUrl = "~/Security/ChangePassword.aspx";
            //miUserName.ChildItems.Add(miChangePassword);


            MenuItem miSignOut = new MenuItem();
            miSignOut.Text = "Sign Out";
            miSignOut.Value = "UserName";
            //miSignOut.NavigateUrl = "javascript:__doPostBack('ctl00$lkLogout','')";
            miSignOut.NavigateUrl = "~/Login.aspx?Logout=Yes";
            miUserName.ChildItems.Add(miSignOut);

            MenuItem miSignOut2 = new MenuItem();
            miSignOut2.Text = miSignOut.Text;
            miSignOut2.Value = miSignOut.Value;
            miSignOut2.NavigateUrl = miSignOut.NavigateUrl;

            menuProfile2.Items.Add(miSignOut2);

        }
    }


    protected void BindMenuReport()
    {

        //hlReport.NavigateUrl = Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/Report.aspx?SSearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");
        //hlDocuments.NavigateUrl = Request.Url.Scheme +"://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/Document.aspx?SSearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");

    }




    protected void BindAccountMenu()
    {
        imgHouse.Visible = false;
        imgResponsiveHouse.Visible = false;
        menuAccount.Items.Clear();



        if (Session["HideAccountMenu"] != null)
        {

            return;
        }


        string strAppPath = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath;

        //User _objUser = (User)Session["User"];
        int? iAccountID = SecurityManager.GetPrimaryAccountID((int)_objUser.UserID);
        if (_objUser != null && _objUser.UserID > 0)
        {


            DataTable dtTemp = Common.DataTableFromText(@"SELECT DISTINCT UserRole.AccountID,  AccountName FROM 
                    UserRole INNER JOIN Account ON UserRole.AccountID=Account.AccountID
                    WHERE UserID=" + _objUser.UserID.ToString() + " ORDER BY AccountName");

            if (dtTemp.Rows.Count > 1) /* Only show when there is more than one account */
            {
                menuAccount.Visible = true;
                //RP Removed temporaritly Ticket 4285
                imgHouse.Visible = true;
                imgResponsiveHouse.Visible = true;
                //END MODIFICATION
                int i = 0;


                MenuItem miAccounts = new MenuItem();
                miAccounts.Text = "Accounts";
                miAccounts.NavigateUrl = "~/Default.aspx";
                miAccounts.Value = "Account";
                menuAccount.Items.Add(miAccounts);
                foreach (DataRow dr in dtTemp.Rows)
                {


                    MenuItem miTempChild = new MenuItem();

                    miTempChild.Text = dr["AccountName"].ToString();
                    miTempChild.NavigateUrl = "javascript:document.getElementById('hfAccountIDToChangeAccount').value = '" + dr["AccountID"].ToString() + "';__doPostBack('ctl00$lkChangeAccount','')";
                    miAccounts.ChildItems.Add(miTempChild);

                    MenuItem miTempChild2 = new MenuItem();
                    miTempChild2.Text = miTempChild.Text;
                    miTempChild2.NavigateUrl = miTempChild.NavigateUrl;
                    menuAccount2.Items.Add(miTempChild2);

                    i = i + 1;
                }



                Account theAccount = SecurityManager.Account_Details((int)iAccountID);
                Account theAccountRoot = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));

                if (theAccountRoot != null)
                {
                    miAccounts.Text = theAccountRoot.AccountName;
                }

            }



        }
    }


    protected void BindOpenMenu()
    {
        menuOpen.Items.Clear();
        if (!Common.HaveAccess(Session["roletype"].ToString(), Common.UserRoleType.OwnData))
        {


            if (_strDocumentsMenu.ToLower() == "all")
            {
                MenuItem miOpen = new MenuItem();
                miOpen.Text = "Documents";
                //miOpen.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/Document.aspx?SSearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");
                miOpen.Value = "Documents";
                miOpen.Selectable = false;
                menuOpen.Items.Add(miOpen);

                MenuItem miDocuments = new MenuItem();
                miDocuments.Text = "Documents";
                miDocuments.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/Document.aspx?SSearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");
                miDocuments.Value = "Documents";
                miOpen.ChildItems.Add(miDocuments);

                MenuItem miDocuments2 = new MenuItem();
                miDocuments2.Text = miDocuments.Text;
                miDocuments2.Value = miDocuments.Value;
                miDocuments2.NavigateUrl = miDocuments.NavigateUrl;
                menuOpen2.Items.Add(miDocuments2);


                MenuItem miReportWriter = new MenuItem();
                miReportWriter.Text = "Report Writer";
                
                miReportWriter.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "Pages/ReportWriter/Report.aspx?SSearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");
                miReportWriter.Value = "Documents";
                miOpen.ChildItems.Add(miReportWriter);

                MenuItem miReportWriter2 = new MenuItem();
                miReportWriter2.Text = miReportWriter.Text;
                miReportWriter2.Value = miReportWriter.Value;
                miReportWriter2.NavigateUrl = miReportWriter.NavigateUrl;
                menuOpen2.Items.Add(miReportWriter2);
            }
            else
            {
                MenuItem miOpen = new MenuItem();
                miOpen.Text = "Reports";
                if (bWhiteWash)
                {
                    miOpen.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "Pages/Security/ProcessPayment.aspx?accounttypeid=2";

                }
                else
                {
                    miOpen.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "Pages/ReportWriter/Report.aspx?SSearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");

                }
                miOpen.Value = "Reports";
                miOpen.Selectable = true;
                menuOpen.Items.Add(miOpen);
            }





        }
        else
        {
            menuOpen.Visible = false;
            imgMenuOpen.Visible = false;
        }

    }
    

    public void BindMenu()
    {
        menuETS.Items.Clear();
        string strAppPath = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath;

        UserRole theUserRole = SecurityManager.GetUserRole((int)_objUser.UserID, int.Parse(Session["AccountID"].ToString()));
        if (theUserRole == null)
        {
            if (Session["roletype"].ToString() == "1")
            {
                theUserRole = SecurityManager.GetGlobalUserRole((int)_objUser.UserID);
            }

        }
        Session["UserRole"] = theUserRole;

        if (_objUser.Email.ToLower() == SystemData.SystemOption_ValueByKey_Account("DemoAccountCreator", null, null).ToLower())
        {
            _bDemoUser = true;
        }

        //string strHideByConditions = "";
        if ((bool)theUserRole.IsAdvancedSecurity)
        {
            //Session["STs"] = RecordManager.ets_Table_ByUser_AdvancedSecurity((int)_objUser.UserID, "-1,3,4,5,7,8,9");
            Session["STs"] = RecordManager.ets_Table_ByUser_AdvancedSecurity((int)theUserRole.RoleID);

            //strHideByConditions = RecordManager.ets_Table_Hide_ByShowMenu((int)theUserRole.RoleID);
        }
        else
        {
            Session["STs"] = "";

            if (Common.HaveAccess(Session["roletype"].ToString(), Common.UserRoleType.None))
            {
                Session["STs"] = "-1";
            }
        }



        if (_objUser != null && _objUser.UserID > 0)
        {

            //

            Account theAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));

            //populate accountwise things



            if (theAccount.Logo != null)
            {
                if ((bool)theAccount.UseDefaultLogo == false)
                    imgLogo.ImageUrl = "~/SSPhoto.ashx?AccountID=" + Session["AccountID"].ToString() + "&type=o";
            }

            //populate dynamic [Table] Group 
            List<Menu> listSTG = RecordManager.ets_Menu_List(int.Parse(Session["AccountID"].ToString()));


            if (!Common.HaveAccess(Session["roletype"].ToString(), Common.UserRoleType.OwnData))
            {

                if (theAccount.HomeMenuCaption.Trim() != "")
                {

                    MenuItem miDefault = new MenuItem();
                    miDefault.Text = theAccount.HomeMenuCaption;
                    miDefault.NavigateUrl = "~/Default.aspx";

                    if (!string.IsNullOrEmpty(_theRole.DashboardType))
                    {
                        if (_theRole.DashboardType == "T" && _theRole.DashboardTableID != null)
                        {
                            miDefault.NavigateUrl = "~/Pages/Record/RecordList.aspx?TableID=" +
                      Cryptography.Encrypt(_theRole.DashboardTableID.ToString());
                        }
                        if (_theRole.DashboardType == "L" && !string.IsNullOrEmpty(_theRole.DashboardLink))
                        {
                            miDefault.NavigateUrl = _theRole.DashboardLink;
                        }
                    }

                    menuETS.Items.Add(miDefault);
                }

            }


            /* == Red, this is the Menus generation, parentMenus == */
            foreach (Menu item in listSTG)
            {
                MenuItem miTemp = new MenuItem();
                miTemp.Text = item.MenuP;
                miTemp.Value = item.MenuP;

                /* == Red 24062019: Menu, Ticket 4689 == */
                bool IsAdvancedSecurity = item.IsAdvancedSecurity == null ? false : (bool)item.IsAdvancedSecurity; // Red 11092019: This is the-menu-isadvancedsecurity
                bool bShowMenu = false;
                if (item.TableID == null && !IsAdvancedSecurity)
                {
                    bShowMenu = true;
                }
                else if (Common.HaveAccess(Session["roletype"].ToString(), "1") || (bool)theUserRole.IsAccountHolder)
                {
                    bShowMenu = true;
                }
                else
                {
                    bShowMenu = SecurityManager.Menu_ShowForUser((int)item.MenuID, (int)theUserRole.UserID);

                }
                /* == End Red == */

                if (item.OpenInNewWindow != null && (bool)item.OpenInNewWindow)
                {
                    miTemp.Target = "_blank";
                }

                if (!string.IsNullOrEmpty(item.ExternalPageLink))
                {
                    miTemp.NavigateUrl = item.ExternalPageLink;
                }
                if (item.DocumentID != null)
                {
                    string strURL = "#";
                    Document theDocument = DocumentManager.ets_Document_Detail((int)item.DocumentID);
                    if (theDocument != null)
                    {

                        if (theDocument.ReportType == "ssrs")
                        {
                            strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/SSRS.aspx?DocumentID=" + Cryptography.Encrypt(theDocument.DocumentID.ToString()) + "&SearchCriteria=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SSearchCriteriaID=" + Cryptography.Encrypt("-1");

                        }
                        else if (theDocument.DocumentTypeID != null)
                        {

                            strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/DocGen/View.aspx?DocumentID=" + theDocument.DocumentID.ToString() + "&SearchCriteria=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SSearchCriteriaID=" + Cryptography.Encrypt("-1");
                        }

                    }
                    miTemp.NavigateUrl = strURL;

                }
                if (item.DocumentTypeID != null)
                {
                    string strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/Document.aspx?Category=" + Cryptography.Encrypt(item.DocumentTypeID.ToString()) + "&TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");

                    miTemp.NavigateUrl = strURL;

                }

                if (item.MenuType != "" && item.MenuType == "doc")
                {
                    string strURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/Document.aspx?TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");

                    miTemp.NavigateUrl = strURL;

                }

                if (item.TableID != null)
                {
                    miTemp.NavigateUrl = "~/Pages/Record/RecordList.aspx?TableID=" +
                        Cryptography.Encrypt(item.TableID.ToString());

                    miTemp.Value = item.MenuP;
                }

                if (item.ReportID != null && item.MenuType == "r")
                {

                    Report report = ReportManager.ets_Report_Detail(int.Parse(item.ReportID.ToString()));
                    if (report != null)
                    {
                        if ((!report.ReportStartDate.HasValue
                        && !report.ReportEndDate.HasValue
                        && report.ReportDates == Report.ReportInterval.IntervalDateRange))
                        {
                            miTemp.NavigateUrl = "javascript: runReport(" + item.ReportID.ToString() + ",'adhoc'); ";
                        }
                        else
                        {
                            string pathHandler = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                                     "Pages/ReportWriter/ReportHandler.ashx" +
                                     "?ReportID=" + Cryptography.Encrypt(report.ReportID.ToString());
                            miTemp.NavigateUrl = "javascript: runReport(" + item.ReportID.ToString() + ",'scheduled','" + pathHandler + "'); ";
                        }

                    }




                }

                /* Red, lets populate the sub menus */
                PopulateSubMenu(ref miTemp, ref miTemp, (int)item.MenuID);

                if (miTemp.ChildItems.Count > 0 || !string.IsNullOrEmpty(item.ExternalPageLink)
                    || item.TableID != null || item.DocumentID != null || item.MenuType != "" || item.DocumentTypeID != null)
                {
                    /* Red, so here we show if user has access to the table only, else owner of the data or admin */
                    //if (!((bool)_CurrentUserRole.IsAdvancedSecurity) || ((bool)_CurrentUserRole.IsAdvancedSecurity && (bShowMenu || miTemp.ChildItems.Count > 0
                    //    || miTemp.Text.Equals("help", StringComparison.InvariantCultureIgnoreCase))))
                    //    menuETS.Items.Add(miTemp);

                    if (bShowMenu || miTemp.ChildItems.Count > 0 || miTemp.Text.Equals("help", StringComparison.InvariantCultureIgnoreCase))
                        menuETS.Items.Add(miTemp);
                }
            }

            /* == End Red, Admin Menus generation == */
            if (Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
            {
                if (theAccount.IsReportTopMenu != null)
                {
                    if ((bool)theAccount.IsReportTopMenu)
                    {
                        MenuItem miTopReports = new MenuItem();
                        miTopReports.Text = "Reports";
                        miTopReports.Value = "Admin";
                        miTopReports.Selectable = true;
                        miTopReports.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/Report.aspx?SSearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");
                        menuETS.Items.Add(miTopReports);
                    }
                }
            }

            bool bHideAdminMenusExceptUsers = false;
            if (SystemData.SystemOption_ValueByKey_Account("HideAdminMenusExceptUsers", null, null).ToLower() == "yes"
                && Common.HaveAccess(Session["roletype"].ToString(), "1") == false)
            {
                bHideAdminMenusExceptUsers = true;
            }

            if (Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
            {

                MenuItem miAdmin = new MenuItem();
                miAdmin.Text = "Admin";
                miAdmin.Value = "Admin";
                miAdmin.Selectable = true;
                miAdmin.NavigateUrl = "~/Pages/User/List.aspx";
                menuETS.Items.Add(miAdmin);


                //RP MODIFIED TICKET 4263
                //ORIGINAL
                //if (_bIsAccountHolder || _bGod)
                //{
                //    MenuItem miAudit = new MenuItem();
                //    miAudit.Text = "Audit";
                //    miAudit.Value = "Admin";
                //    miAudit.NavigateUrl = "~/Pages/Document/AuditReport.aspx";
                //    miAdmin.ChildItems.Add(miAudit);
                //}
                //MODIFIED
                if (bHideAdminMenusExceptUsers == false)
                {
                    if (_bIsAccountHolder || _bGod)
                    {
                        MenuItem miAudit = new MenuItem();
                        miAudit.Text = "Audit";
                        miAudit.Value = "Admin";
                        miAudit.NavigateUrl = "~/Pages/Document/AuditReport.aspx";
                        miAdmin.ChildItems.Add(miAudit);
                    }
                }
                //END MODIFICATIOn

                if (bHideAdminMenusExceptUsers == false)
                {
                    if (_bIsAccountHolder || _bGod || Common.HaveAccess(Session["roletype"].ToString(), "2"))
                    {
                        MenuItem miUploads = new MenuItem();
                        miUploads.Text = "Auto Uploads";
                        miUploads.Value = "Admin";
                        miUploads.NavigateUrl = "~/Pages/Record/Upload.aspx";
                        miAdmin.ChildItems.Add(miUploads);
                    }
                }

                if (Common.HaveAccess(Session["roletype"].ToString(), "1"))
                {
                    MenuItem miAccounts = new MenuItem();
                    miAccounts.Text = "Accounts";
                    miAccounts.Value = "Admin";
                    miAccounts.NavigateUrl = "~/Pages/Security/AccountList.aspx";
                    miAdmin.ChildItems.Add(miAccounts);

                }
                else
                {
                    //User objUser = (User)Session["User"];

                    if (_bDemoUser)
                    {
                        MenuItem miAccounts = new MenuItem();
                        miAccounts.Text = "Add Account";
                        miAccounts.Value = "Admin";
                        miAccounts.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/SystemSignUp.aspx";
                        miAdmin.ChildItems.Add(miAccounts);
                    }

                }


                if (_bDemoUser == false)
                {

                    if (bHideAdminMenusExceptUsers == false && Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
                    {
                        string strOptionValue = SystemData.SystemOption_ValueByKey_Account("Show_Admin_Menu_Batches", int.Parse(Session["AccountID"].ToString()), null);
                        if (_bIsAccountHolder || _bGod || (strOptionValue != "" && strOptionValue.ToLower() == "yes"))
                        {
                            MenuItem miBatches = new MenuItem();
                            miBatches.Text = "Batches";
                            miBatches.Value = "Admin";
                            miBatches.NavigateUrl = "~/Pages/Record/Batches.aspx?menu=" + Cryptography.Encrypt("yes") + "&TableID=" + Cryptography.Encrypt("-1");
                            miAdmin.ChildItems.Add(miBatches);

                        }
                    }


                    if (Common.HaveAccess(Session["roletype"].ToString(), "1"))
                    {

                        MenuItem miConfiguration = new MenuItem();
                        miConfiguration.Text = "Configuration";
                        miConfiguration.Value = "Admin";
                        //miConfiguration.NavigateUrl = "~/Pages/Company/ContactUsAdmin.aspx";
                        miConfiguration.Selectable = false;
                        miAdmin.ChildItems.Add(miConfiguration);


                        MenuItem miCopyFields = new MenuItem();
                        miCopyFields.Text = "Copy Field";
                        miCopyFields.Value = "Admin";
                        miCopyFields.NavigateUrl = "~/Pages/SystemData/CopyRecordField.aspx";
                        miConfiguration.ChildItems.Add(miCopyFields);

                        MenuItem miResetValues = new MenuItem();
                        miResetValues.Text = "Reset Values";
                        miResetValues.Value = "Admin";
                        miResetValues.NavigateUrl = "~/Pages/SystemData/ResetValues.aspx";
                        miConfiguration.ChildItems.Add(miResetValues);

                        MenuItem miUpdateLinkedTables = new MenuItem();
                        miUpdateLinkedTables.Text = "Update Linked Tables";
                        miUpdateLinkedTables.Value = "Admin";
                        miUpdateLinkedTables.NavigateUrl = "~/Pages/SystemData/UpdateLinkedTables.aspx";
                        miConfiguration.ChildItems.Add(miUpdateLinkedTables);

                        MenuItem miContactAdmin = new MenuItem();
                        miContactAdmin.Text = "Contacts";
                        miContactAdmin.Value = "Admin";
                        miContactAdmin.NavigateUrl = "~/Pages/Company/ContactUsAdmin.aspx";
                        miAdmin.ChildItems.Add(miContactAdmin);

                        MenuItem miContents = new MenuItem();
                        miContents.Text = "Contents";
                        miContents.Value = "Admin";
                        miContents.NavigateUrl = "~/Pages/SystemData/Content.aspx";
                        miAdmin.ChildItems.Add(miContents);



                    }

                }

                if (Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
                {

                    if (bHideAdminMenusExceptUsers == false)
                    {


                        if (_bIsAccountHolder || _bGod)
                        {
                            MenuItem miDashboard = new MenuItem();
                            miDashboard.Text = "Dashboards";
                            miDashboard.Value = "Admin";
                            miDashboard.NavigateUrl = "~/Pages/Home/DashBoard.aspx";
                            miAdmin.ChildItems.Add(miDashboard);
                        }

                        MenuItem miGraph = new MenuItem();
                        miGraph.Text = "Graphs";
                        miGraph.Value = "Admin";
                        miGraph.NavigateUrl = "~/Pages/Graph/GraphOptions.aspx";
                        miAdmin.ChildItems.Add(miGraph);
                        if (_bIsAccountHolder || _bGod)
                        {
                            MenuItem miGraphDef = new MenuItem();
                            miGraphDef.Text = "Graph Definitions";
                            miGraphDef.Value = "Admin";
                            miGraphDef.NavigateUrl = "~/Pages/Graph/GraphDef.aspx";
                            miAdmin.ChildItems.Add(miGraphDef);
                        }
                    }

                }



                if (Common.HaveAccess(Session["roletype"].ToString(), "1"))
                {
                    MenuItem miErrorLogs = new MenuItem();
                    miErrorLogs.Text = "Error Logs";
                    miErrorLogs.Value = "Admin";
                    miErrorLogs.NavigateUrl = "~/Pages/SystemData/ErrorLog.aspx";
                    miAdmin.ChildItems.Add(miErrorLogs);


                    MenuItem miLookup = new MenuItem();
                    miLookup.Text = "Look up data";
                    miLookup.Value = "Admin";
                    miLookup.NavigateUrl = "#";
                    miAdmin.ChildItems.Add(miLookup);




                    MenuItem miCountry = new MenuItem();
                    miCountry.Text = "Country";
                    miCountry.Value = "Admin";
                    miCountry.NavigateUrl = "~/Pages/LookUp/LookUp.aspx?LookUpTypeID=" + Cryptography.Encrypt("-1");
                    miLookup.ChildItems.Add(miCountry);
                }


                if (Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
                {
                    if (bHideAdminMenusExceptUsers == false)
                    {
                        string strOptionValue = SystemData.SystemOption_ValueByKey_Account("Show_Admin_Menu_Menus", int.Parse(Session["AccountID"].ToString()), null);
                        if (_bIsAccountHolder || _bGod || (strOptionValue != "" && strOptionValue.ToLower() == "yes"))
                        {
                            MenuItem miMenus = new MenuItem();
                            miMenus.Text = "Menus";
                            miMenus.Value = "Admin";
                            miMenus.NavigateUrl = "~/Pages/Record/TableGroup.aspx";
                            miAdmin.ChildItems.Add(miMenus);
                        }
                    }



                    //RP MODIFIED Ticket 4263
                    //ORIGINAL
                    //MenuItem miMessages = new MenuItem();
                    //miMessages.Text = "Messages";
                    //miMessages.Value = "Admin";
                    //miMessages.NavigateUrl = "~/Pages/Record/MessageList.aspx";
                    //miAdmin.ChildItems.Add(miMessages);
                    //MODIFIED
                    if (bHideAdminMenusExceptUsers == false)
                    {
                        MenuItem miMessages = new MenuItem();
                        miMessages.Text = "Messages";
                        miMessages.Value = "Admin";
                        miMessages.NavigateUrl = "~/Pages/Record/MessageList.aspx";
                        miAdmin.ChildItems.Add(miMessages);
                    }
                    //END MODIFICATION
                }


                if (_bDemoUser == false)
                {
                    if (bHideAdminMenusExceptUsers == false && Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
                    {
                        string strOptionValue = SystemData.SystemOption_ValueByKey_Account("Show_Admin_Menu_Notification", int.Parse(Session["AccountID"].ToString()), null);
                        if (_bIsAccountHolder || _bGod || (strOptionValue != "" && strOptionValue.ToLower() == "yes"))
                        {
                            MenuItem miNotifications = new MenuItem();
                            miNotifications.Text = "Notifications";
                            miNotifications.Value = "Admin";
                            miNotifications.NavigateUrl = "~/Pages/Record/Notification.aspx";
                            miAdmin.ChildItems.Add(miNotifications);
                        }
                    }
                }

                if (Common.HaveAccess(Session["roletype"].ToString(), "1"))
                {

                    MenuItem miInvoices = new MenuItem();
                    miInvoices.Text = "Invoices";
                    miInvoices.Value = "Admin";
                    miInvoices.NavigateUrl = "~/Pages/Security/Invoice.aspx";
                    miAdmin.ChildItems.Add(miInvoices);

                }


                if (_bDemoUser == false)
                {
                    if (Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
                    {
                        if (bHideAdminMenusExceptUsers == false)
                        {
                            if (_bIsAccountHolder || _bGod)
                            {
                                MenuItem miReports = new MenuItem();
                                miReports.Text = "Reports (Custom)";
                                miReports.Value = "Admin";
                                miReports.NavigateUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath + "/Pages/Document/Report.aspx?SSearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt("-1") + "&SearchCriteriaID=" + Cryptography.Encrypt("-1");
                                //miReports.Selectable = false;
                                miAdmin.ChildItems.Add(miReports);
                            }

                        }
                    }


                    if (bHideAdminMenusExceptUsers == false && Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
                    {
                        string strOptionValue = SystemData.SystemOption_ValueByKey_Account("Show_Admin_Menu_Tables", int.Parse(Session["AccountID"].ToString()), null);
                        if (_bIsAccountHolder || _bGod || (strOptionValue != "" && strOptionValue.ToLower() == "yes"))
                        {
                            MenuItem miST = new MenuItem();
                            miST.Text = SecurityManager.etsTerminology("", "Tables", "Tables");
                            miST.Value = "Admin";
                            miST.NavigateUrl = "~/Pages/Record/TableList.aspx";
                            miAdmin.ChildItems.Add(miST);
                        }
                    }



                    if (Common.HaveAccess(Session["roletype"].ToString(), "1"))
                    {

                        MenuItem miSystemOptions = new MenuItem();
                        miSystemOptions.Text = "System Options";
                        miSystemOptions.Value = "Admin";
                        miSystemOptions.NavigateUrl = "~/Pages/SystemData/SystemOption.aspx";
                        miAdmin.ChildItems.Add(miSystemOptions);
                    }



                    if (Common.HaveAccess(Session["roletype"].ToString(), "1,2"))
                    {
                        if (bHideAdminMenusExceptUsers == false)
                        {
                            if (_bIsAccountHolder || _bGod)
                            {
                                MenuItem miTerminology = new MenuItem();
                                miTerminology.Text = "Terminology";
                                miTerminology.Value = "Admin";
                                miTerminology.NavigateUrl = "~/Pages/Security/Terminology.aspx";
                                miAdmin.ChildItems.Add(miTerminology);
                            }
                        }
                    }


                    MenuItem miUsers = new MenuItem();
                    miUsers.Text = "Users";

                    miUsers.Text = miUsers.Text.Replace("User", SecurityManager.etsTerminology(Request.Path.Substring(Request.Path.LastIndexOf("/") + 1), "User", "User"));


                    miUsers.Value = "Admin";
                    miUsers.NavigateUrl = "~/Pages/User/List.aspx";
                    miAdmin.ChildItems.Add(miUsers);

                    if (Common.HaveAccess(Session["roletype"].ToString(), "1"))
                    {



                        MenuItem miVisitorPageCount = new MenuItem();
                        miVisitorPageCount.Text = "Visitors";
                        miVisitorPageCount.Value = "Admin";
                        miVisitorPageCount.NavigateUrl = "~/Pages/SystemData/PageCount.aspx";
                        miAdmin.ChildItems.Add(miVisitorPageCount);




                        MenuItem miForm = new MenuItem();
                        miForm.Text = "Form";
                        miForm.Value = "Admin";
                        miForm.NavigateUrl = "~/Pages/Form/Form.aspx";
                        miAdmin.ChildItems.Add(miForm);

                    }

                }

            }
        }
        else
        {
            if (Session["LoginAccount"] == null)
            {
                Session.Clear();
                FormsAuthentication.SignOut();
                Response.Redirect("~/Login.aspx", false);
            }
            else
            {
                string strLoginAccount = Session["LoginAccount"].ToString();
                Session.Clear();
                FormsAuthentication.SignOut();
                Response.Redirect("~/Login.aspx?" + strLoginAccount, false);
            }

        }

    }
    public bool DisplayAdmin()
    {
        int iTN = 0;
        bool kq = false;
        if (Context.User.Identity.IsAuthenticated)
        {

            string roletype = Session["roletype"].ToString();
            if (roletype.Length > 0)
            {
                if (roletype.Contains("2"))
                {
                    kq = true;
                }
            }

        }
        return kq;
    }
    public bool DisplayGod()
    {
        bool kq = false;

        if (Context.User.Identity.IsAuthenticated)
        {
            string roletype = Session["roletype"].ToString();
            if (roletype.Length > 0)
            {
                if (roletype.Contains("2"))
                {
                    kq = true;
                }
            }

        }
        return kq;
    }
    protected void lkChangeAccount_Click(object sender, EventArgs e)
    {
        if (hfAccountIDToChangeAccount.Value != "")
        {
            try
            {
                if (Common.ChangeAccount((int)_objUser.UserID, int.Parse(hfAccountIDToChangeAccount.Value), true))
                {
                    Response.Redirect("~/Default.aspx", true);
                    return;
                }
                else
                {
                    Response.Redirect("~/Empty.aspx", true);
                }

            }
            catch
            {
                //
            }

        }

    }

    protected void lnkEndDemo_Click(object sender, EventArgs e)
    {

        Session["User"] = null;
        Session.Abandon();

        FormsAuthentication.SignOut();
        HttpCookie oUseInfor = new HttpCookie("UserInformation", "nothing");
        oUseInfor.Expires = DateTime.Now.AddDays(-3d);
        Response.Cookies.Add(oUseInfor);
        //Response.Redirect("~/Environmental-Monitoring-Management-Database-System.aspx?demo=yes", false);
        Response.Redirect("~/Login.aspx?demo=yes", false);

    }
    protected void lkLogout_Click(object sender, EventArgs e)
    {

        try
        {
            string strLoginAccount = "";
            if (Session["LoginAccount"] != null)
            {
                strLoginAccount = Session["LoginAccount"].ToString();
            }


            Session["User"] = null;
            Session.Abandon();

            FormsAuthentication.SignOut();
            HttpCookie oUseInfor = new HttpCookie("UserInformation", "nothing");
            oUseInfor.Expires = DateTime.Now.AddDays(-3d);
            Response.Cookies.Add(oUseInfor);




            if (Session["client"] != null)
            {
                Session.Clear();
                Response.Redirect("~/Login.aspx?Logout=yes&Account=" + Session["client"].ToString(), false);

                return;
            }

            if (Session["LoginAccount"] == null)
            {
                Session.Clear();
                Response.Redirect("~/Login.aspx?Logout=yes", true);
            }
            else
            {
                Session.Clear();
                Response.Redirect("~/Login.aspx?Logout=yes&" + strLoginAccount, true);
            }
            //}

        }
        catch
        {
            //
        }



    }




    protected void lnkChangeAccount_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Pages/Security/AccountList.aspx", false);
    }

    protected void lnkViewAccount_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Pages/Security/AccountDetail.aspx?mode=" + Cryptography.Encrypt("edit") + "&accountid=" + Cryptography.Encrypt(Session["AccountID"].ToString()), false);
    }

    protected void lnkChangePassword_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Security/ChangePassword.aspx", false);
    }


    MenuItem GetSelectedMenuItem(System.Web.UI.WebControls.Menu menu)
    {
        MenuItem selectedItem = null;
        if (Request.Form["__EVENTTARGET"] == menu.ID)
        {
            string value = Request.Form["__EVENTARGUMENT"];
            if (!string.IsNullOrEmpty(value))
            {
                foreach (MenuItem mi in menu.Items)
                {
                    if (mi.Value == value)
                    {
                        selectedItem = mi;
                        break;
                    }
                }
            }
        }
        else
        {
            selectedItem = menu.SelectedItem;
        }
        return selectedItem;
    }
}


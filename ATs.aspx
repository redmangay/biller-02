﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master"
    AutoEventWireup="true" CodeFile="ATs.aspx.cs" Inherits="Pages_Generic_ATs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

    <asp:UpdatePanel ID="upGeneric" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <asp:Panel runat="server" ID="pnlPopupDatatable">
                <asp:Label runat="server" ID="lblTopTitle" CssClass="TopTitle">Only relational tables with columns</asp:Label>
                <br />
                <asp:TextBox runat="server" ID="txtTableName" CssClass="NormalTextBox" Width="200px"></asp:TextBox>
                <br />
                <asp:LinkButton runat="server" ID="lnkSearch" CssClass="btn" OnClick="lnkSearch_Click"> 
                                                <strong>Go</strong> </asp:LinkButton>
                <br />
                <br />
                <div style="text-align: center;">
                    <table>

                        <tr>
                            <td style="padding-top: 50px;" valign="top">Order by Parent Table<br />
                                <asp:GridView ID="gvP" runat="server" ShowHeaderWhenEmpty="true"
                                    AutoGenerateColumns="true" CssClass="gridview" AllowPaging="false"
                                    HeaderStyle-CssClass="gridview_header" RowStyle-CssClass="gridview_row">
                                </asp:GridView>
                            </td>
                            <td valign="top">Order by Child Table<br />
                                <asp:GridView ID="gvC" runat="server" ShowHeaderWhenEmpty="true"
                                    AutoGenerateColumns="true" CssClass="gridview" AllowPaging="false"
                                    HeaderStyle-CssClass="gridview_header" RowStyle-CssClass="gridview_row">
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 30px;"></td>
                        </tr>
                        <tr>
                            <td style="width: 40%;"></td>
                            <td>
                                <table>

                                    <tr>
                                        <td></td>
                                        <td style="padding-left: 5px;">
                                            <asp:LinkButton runat="server" ID="lnkClose" CssClass="btn" OnClientClick="parent.$.fancybox.close();return false;">
                                           <strong>Close</strong>
                                            </asp:LinkButton>

                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>

                </div>


            </asp:Panel>

        </ContentTemplate>

    </asp:UpdatePanel>


</asp:Content>


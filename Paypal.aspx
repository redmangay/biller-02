﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true"
     CodeFile="Paypal.aspx.cs" Inherits="Paypal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

    <div style="padding: 50px; min-height: 300px;">
        <%--<asp:Panel runat="server" ID="pnlStripButton">
            <script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="<%= stripePublishableKey %>"
                data-amount="10000"
                data-currency="AUD"
                data-name="DBGurus.com.au"
                data-description="eContractor Monthly Charge"
                
                data-locale="auto"
                data-zip-code="false"
                data-panel-label="Pay Monthly {{amount}}">
            </script>
        </asp:Panel>--%>

        <br />
        <br />
        <asp:Button runat="server" ID="btnPaypal" Text="Recurring Pay by Paypal" OnClick="btnPaypal_Click" />

        <br />
         <br />
        <asp:Button runat="server" ID="btnPayOneTime" Text="One time Pay by Paypal" OnClick="btnPayOneTime_Click" />

        <br />

        <asp:Label runat="server" ID="lblMessage"></asp:Label>

        <br />
        <br />
        <asp:Button runat="server" ID="btnCleanCustomer" Text="Clean Customers" OnClick="btnCleanCustomer_Click" />

        <br />
        <br />
        <asp:Button runat="server" ID="btnCutomersManagement" Text="Customers" OnClick="btnCutomersManagement_Click" />

        <br />

    </div>


</asp:Content>


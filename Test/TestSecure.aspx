﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true" CodeFile="TestSecure.aspx.cs" Inherits="Test_TestSecure" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">


    <style type="text/css">
        .detailtable {
            display: block;
            height: 100%;
            overflow: hidden;
        }

            .detailtable td {
                width: 30px;
                max-width: 30px;
                height: 50px;
                border: 1px dotted black;
                overflow: visible;
                border-bottom: double;
            }

                .detailtable td:first-child {
                    width: 100px;
                    max-width: 200px;
                    height: 50px;
                    border: 1px dotted black;
                    overflow: visible;
                    border-bottom: double;
                }

                .detailtable td:nth-child(2), .detailtable td:last-child {
                    width: 60px;
                    max-width: 60px;
                    height: 50px;
                    border: 0px none;
                    border-bottom: none;
                    background-color: #EDEDED;
                }

                .detailtable td:last-child {
                    overflow: hidden;
                }

        .columnspan {
            display: block;
            background-color: #DDEBF7;
            border: 1px solid;
            height: 100%;
            width: 30px;
            position: relative;
            overflow: visible;
            float: left;
            /*resize:horizontal;*/
        }
    </style>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

    <script type="text/javascript">


        function EditRecord(thediv) {
            alert(thediv.attr('id'))
        }


        $(document).ready(function () {


            $('.columnspan').on("dragstart", function (event) {
                var dt = event.originalEvent.dataTransfer;
                dt.setData('Text', $(this).attr('id'));
                $(this).css('height', 30);

            });
            //eachdropcell
            //.detailtable td
            $('.eachdropcell').on("dragenter dragover drop", function (event) {


                event.preventDefault();

                if (event.type === 'drop' &&
                    (event.target == '[object HTMLTableCellElement]'
                        || event.target == '[object HTMLTableDataCellElement]')) {


                    var data = event.originalEvent.dataTransfer.getData('Text');
                    event.target.appendChild(document.getElementById(data));
                    $('#' + data).css('height', 50);

                    //$.ajax({
                    //    url: 'ColumnRecordRes.ashx?Coordinate=' + event.target.id + '&ColumnID=' + data,
                    //    cache: false,
                    //    success: function (content) {
                    //        if (content == 'False') {
                    //            alert('Error!');
                    //        }
                    //        else { //alert('good');
                    //        }
                    //    },
                    //    error: function (a, b, c) {
                    //        alert('Error!');
                    //    }
                    //});



                }
                else if (event.type === 'drop') {
                    var data = event.originalEvent.dataTransfer.getData('Text');
                    $('#' + data).css('height', 50);
                    alert('Please use an empty cell.');
                    //alert(event.target);
                };


            });
        })
    </script>


    <div style="padding: 20px;">


        <br />
        <br />
        <table runat="server" id="tblDetailTable" visible="true" cellpadding="0" cellspacing="0"
            class="detailtable">
        </table>
        <br />
        <br />


        <asp:Button runat="server" ID="btnTest" OnClick="btnTest_Click" Text="Show me" />
        <br />
        <asp:Label runat="server" ID="lblMsg"></asp:Label>
        <br />
        <br />

        <%--<asp:Calendar runat="server" ID="cldDate" Width="100%" OnDayRender="cldDate_DayRender"
            TitleStyle-CssClass="TitleStyle" DayHeaderStyle-CssClass="DayHeaderStyle" DayStyle-CssClass="DayStyle"
            NextPrevStyle-CssClass="NextPrevStyle" OtherMonthDayStyle-CssClass="OtherMonthDayStyle"
            SelectedDayStyle-CssClass="SelectedDayStyle" SelectorStyle-CssClass="SelectorStyle"
            TodayDayStyle-CssClass="TodayDayStyle" WeekendDayStyle-CssClass="WeekendDayStyle"
            CssClass="maincalender" OnSelectionChanged="cldDate_SelectionChanged" DayNameFormat="Full"
            BorderStyle="Solid" BorderWidth="1px" ForeColor="Black" SelectedDayStyle-BackColor="ActiveBorder"
            SelectedDayStyle-ForeColor="Black" ShowGridLines="true" OnVisibleMonthChanged="cldDate_VisibleMonthChanged">
            <DayHeaderStyle VerticalAlign="Middle" HorizontalAlign="Center" Height="30px" ForeColor="White" />
            <TitleStyle CssClass="TitleStyle" BackColor="White" Height="0px" />
        </asp:Calendar>--%>
    </div>

    <br />
    <span class="columnspan" style="resize: horizontal; width: 100px;">Test resize</span>
    <br />
    <div class="columnspan" style="width: 100px;" draggable="true">Test resize</div>
    <br />
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Xml;
using System.IO;
using System.Web.UI.HtmlControls;
public partial class Test_TestSecure : SecurePage
{

    protected void btnTest_Click(object sender, EventArgs e)
    {
       

    }
    protected void cldDate_DayRender(object sender, DayRenderEventArgs e)
    {

    }
    protected void cldDate_SelectionChanged(object sender, EventArgs e)
    {


    }
    protected void cldDate_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
    {

    }

    protected void cbcFilterTest_OnddlYAxis_Changed(object sender, EventArgs e)
    {
        //do nothing here
    }

    public int TableID;
    public int ColumnID;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            string strDayStart = "7:00";
            string strDayEnd = "18:30";
            TimeSpan tsDayStart = TimeSpan.Parse(strDayStart);
            TimeSpan tsDayEnd = TimeSpan.Parse(strDayEnd);
            int iStartHour = tsDayStart.Hours;
            int iEndHour = tsDayEnd.Hours;
            if(tsDayEnd.Minutes>0)
            {
                iEndHour = iEndHour + 1;
            }
            


            int iYmax = 7;//SELECT RecordID
            //int iXmax = 24;
            iYmax = iYmax + 1; // Add the header
            for (int y = 1; y <= iYmax; y++)
            {
                HtmlTableRow trTemp = new HtmlTableRow();
                trTemp.ClientIDMode = ClientIDMode.Static;
                trTemp.ID = "tr_" + y.ToString();
                HtmlTableCell tdTemp = new HtmlTableCell();
                tdTemp.ClientIDMode = ClientIDMode.Static;
                tdTemp.ID = "td_Name_" + y.ToString();

                HtmlTableCell tdTemp2 = new HtmlTableCell();
                tdTemp2.ClientIDMode = ClientIDMode.Static;
                tdTemp2.ID = "td_Before_" + y.ToString();

                var span = new HtmlGenericControl("span");
                 if(y==1)
                 {
                     string strHeader = "Name";//The column.DisplayName
                     
                     span.InnerHtml = "<strong>" + strHeader + "</strong>";
                     tdTemp.Controls.Add(span);

                     var span2 = new HtmlGenericControl("span");
                     span2.InnerHtml = "<strong>Before</strong>";
                     tdTemp2.Controls.Add(span2);
                 }
                 else
                 {
                     span.InnerHtml = "name " + y.ToString() ;
                     tdTemp.Controls.Add(span);
                 }
                 trTemp.Cells.Add(tdTemp);
                 trTemp.Cells.Add(tdTemp2);
                 tblDetailTable.Rows.Add(trTemp);
            }

            for (int y = 1; y <= iYmax; y++)
            {
                HtmlTableRow trTemp = (HtmlTableRow)tblDetailTable.FindControl("tr_" + y.ToString());
                for (int x = iStartHour; x <= iEndHour; x++)
                {
                    HtmlTableCell tdTemp = new HtmlTableCell();
                    tdTemp.ClientIDMode = ClientIDMode.Static;
                   

                    tdTemp.ID = "td_" + x.ToString() + "_" + y.ToString();
                    trTemp.Cells.Add(tdTemp);
                    if(y==1)
                    {
                        string strHeader = "";
                        if(x<12)
                        {
                            strHeader = x.ToString() + "am";
                        } else if (x==12)
                        {
                            strHeader = "12pm";
                        } else if (x>12 && x<24)
                        {
                            strHeader = (x-12).ToString() + "pm";
                        } else if (x==24)
                        {
                            strHeader = "12am";
                        }
                        var span = new HtmlGenericControl("span");
                        span.InnerHtml = "<strong>" + strHeader + "</strong>";
                        tdTemp.Controls.Add(span);
                        tdTemp.Attributes.Add("colspan", "2");
                    }
                    else
                    {
                        HtmlTableCell tdTemp2 = new HtmlTableCell();
                        tdTemp2.ClientIDMode = ClientIDMode.Static;
                        tdTemp2.ID = "td_" + x.ToString() + "_" + y.ToString() + "_half";
                        tdTemp2.Attributes.Add("class", "eachdropcell");
                        tdTemp.Attributes.Add("class", "eachdropcell");
                        trTemp.Cells.Add(tdTemp2);
                    }

                    if(y>1 && x==10)
                    {

                        var divContent = new HtmlGenericControl("div");
                        divContent.Attributes.Add("draggable", "true");
                        divContent.Attributes.Add("class", "columnspan");
                        divContent.Attributes.Add("id", "divContent_" + x.ToString() + "_" + y.ToString());
                        divContent.InnerHtml = "<p>Test "+y.ToString()+"</p>";

                        divContent.Attributes.Add("ondblclick", "EditRecord($(this));");

                        tdTemp.Controls.Add(divContent);
                        if (y == 4)
                        {
                            if (x != iEndHour)
                            {                                
                                divContent.Style.Add("width", "120px");
                            }

                        }
                    }
                }
                tblDetailTable.Rows.Add(trTemp);
            }

            for (int y = 1; y <= iYmax; y++)
            {
                HtmlTableRow trTemp = (HtmlTableRow)tblDetailTable.FindControl("tr_" + y.ToString());
                HtmlTableCell tdTemp = new HtmlTableCell();
                tdTemp.ClientIDMode = ClientIDMode.Static;
                tdTemp.ID = "td_After_" + y.ToString();
                var span = new HtmlGenericControl("span");
                  if(y==1)
                  {
                      span.InnerHtml = "<strong>After</strong>";
                      tdTemp.Controls.Add(span);
                  }
                  else
                  {

                  }
                  trTemp.Cells.Add(tdTemp);
                  tblDetailTable.Rows.Add(trTemp);
            }



        }
    }






}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="_Default"  %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <link rel="apple-touch-icon" sizes="57x57"  href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="60x60" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="72x72" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="76x76" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="114x114" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="120x120" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="144x144" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="152x152" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="180x180" href="../Images/favicon.ico" />
    <link rel="icon" type="image/png" sizes="192x192" href="../Images/favicon.ico" />
    <link rel="icon" type="image/png" sizes="32x32" href="../Images/favicon.ico" />
    <link rel="icon" type="image/png" sizes="96x96" href="../Images/favicon.ico" />
    <link rel="icon" type="image/png" sizes="16x16" href="../Images/favicon.ico" />
<link rel="manifest" href="Marketing/images/icon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="Marketing/images/icon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<title>Honeyman</title>
    <link href="Marketing/css/bootstrap.min.css" rel="stylesheet">
    <link href="Marketing/css/index.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-nav">
     <script type="text/javascript" src="<%=ResolveUrl("~/script/jquery-3.3.1.min.js")%>"></script>
    <script>

        $('.navbar-collapse ul li a').click(function () {
            $(".navbar-collapse").collapse('hide');
        });
    </script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="Marketing/js/bootstrap.min.js"></script>
    <!-- Scrolling Nav JavaScript -->
    <script src="Marketing/js/jquery.easing.min.js"></script>
    <script src="Marketing/js/scrolling-nav.js"></script> 
      <form role="form" runat="server">
        <div class="container" data-spy="affix" data-offset-top="120">
        	<div class="row pad-tb-10">
            	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
           	    	<a class="page-scroll" href="#page-top">
                           <img src="images/logo.png" class="img-responsive" width="230" height="auto" id="home" alt="Honeyman" /> </a>
                </div>
              	<div class="col-xs-6 col-sm-6 col-md-6 col-lg-5 col-lg-offset-1">
                 	<nav class="navbar navbar-right">
                      <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="menu">Menu</span>
                          </button>
                        <!--<a href="#" class="navbar-brand hidden visible-xs"><img src="images/sm-logo.jpg" class="img-responsive" alt="ETS" /></a>-->
    					</div>
                   <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                    			<li class="active"><a class="page-scroll" href="#page-top">Home</a></li>
                            	<li><a class="page-scroll" href="#client">Clients</a></li>
                                <li><a class="page-scroll" href="#features">Features</a></li>
                            	<li><a class="page-scroll" href="#pricing">Pricing</a></li>     
                                <li><a href="Login.aspx">Sign In</a></li>   
                  </ul>
					
                    </div>
                  </nav>
                </div>
            </div>
        </div> 





        <div class="container-fluid" style="background:url(Marketing/images/banner-bg.jpg) 55% 0% no-repeat">
        	<div class="container">
            	<div class="row">
                	<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                    	<h1 class="bnr-txt">Your all-in-one<br />
Beekeeper software<br />
solution</h1>
                    </div>
                	<div class="col-xs-12 col-md-4 col-lg-4 trial-bg">
                        <h1>Free Trial</h1>
                        <p><asp:Label ID="lblMessage" runat="server" 
                            Text="Fill in the form and we’ll contact you within 1 business day to schedule a free introductory session:"></asp:Label></p>
                        <div class="form-group">
                            <asp:TextBox ID="txtName" CssClass="form-control form-inpt" placeholder="Name*" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtEmail" CssClass="form-control form-inpt" placeholder="Email*" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtPhone" CssClass="form-control form-inpt" placeholder="Phone" runat="server"></asp:TextBox>
                        </div>
                        <br />
                        <asp:Button ID="btnSignUp" CssClass="btn btn-primary" Text="Sign Up" runat="server" OnClick="btnSignUp_Click" ValidationGroup="Form1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="White"
                                ErrorMessage="*Please enter your Name."   ControlToValidate="txtName" ValidationGroup="Form1">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2"   runat="server" ForeColor="White"
                                ErrorMessage="<br/>*Please enter your Email." ControlToValidate="txtEmail" ValidationGroup="Form1">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regexEmailValid"   runat="server"
                                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ControlToValidate="txtEmail" ErrorMessage="<br/>Please enter a valid email address." ValidationGroup="Form1">
                            </asp:RegularExpressionValidator>
                    </div>               

                </div>
            </div>
        </div>





				<div class="container-fluid pad-tb-30-70" style="background:url(Marketing/images/video-section-bg.png) 64% 72% repeat-x;">
                    <div class="container">
                    	<div class="row">
                       	  <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 text-center ets-txt">
                            <h3>The Honeyman Platform – A Beekeeper Solution</h3>
                            <p>Free up your business by storing site data in an online platform</p>
                            <p>Not only making the whole process simpler for you and your team it will also assist in keeping your bees healthier.</p>
                            <div class="col-md-10 col-md-offset-1">
                            	<img src="Marketing/images/ets-video.jpg" class="ets-video img-responsive" alt="ETS-video" />
                            </div>
                            
                             </div>
                        </div>
                    </div>
                 </div>   
                 
                 
                 <div class="container-fluid pad-tb-30-70" style="background:#f8f8f8;">
                    <div class="container">
                    	<div class="row spsheet">
                        	<div class="col-md-6 col-md-offset-3">
                            	<h2 class="text-center">Reason to use us:</h2>
                                <hr />
                            </div>
							<div class="col-md-6 col-lg-6 mr-tp-52">
               	    	    	<img src="Marketing/images/reason-to-use.png" class="img-responsive" alt="Are you in Spreadsheet hell" />
                            </div>
                            

							<div class="col-md-6 col-lg-6 mr-tp-22">
                            	
                                <p class="mr-tp-34"><strong></strong></p>
                                <ul>
	                                <li>Keep important information all in one place </li>
                                    <li>Trace your hives and your honey supers</li>
                                    <li>Visual warnings for American foulbrood (AFB) Infection </li>
                                    <li>Manage and log apiary activity on a daily basis</li>
<li>Track your hives and supers to comply with new MPI rules </li>
<li>Track your hives and supers for asset management</li>

                                </ul>
                                
                            </div>
                        
                        </div>
                    </div>
                 </div>     
                 
                 
                 
    <div class="container-fluid easy-bg">
                 <div class="container easy-way pad-tp-30">
                 	<div class="row">
                    	<div class="col-md-6 col-md-offset-3">
                           	<h2 class="text-center">The easy way to measure,<br />monitor, and manage</h2>
                            <hr />
                            </div>
                     </div>       
                    
			</div>
            <div class="container pad-bt-70">		
                     <div class="row">
                     		<div class="col-sm-6 col-md-5 col-lg-4 col-lg-offset-2 mr-left mr-right grow bg-white text-center pad-tb-mb-10 mr-tb-mb-10">
                                <img src="Marketing/images/grow.png" width="145" height="145" class="center-block mr-tb-60" /> 
                                <h2>Freedom to grow</h2>
                                <ul>
                            	<li>Expands as your business does</li>
                                <li>The easy way to measure, monitor and manage</li>
                                <li>Capture hive information </li>
                                <li>Report on costs and yields at your sites</li>
                            </ul>
                        </div>


							<div class="col-sm-6 col-md-5 col-lg-4 bg-white grow text-center pad-tb-mb-10">
                                <img src="Marketing/images/lock.png" width="145" height="145" class="center-block mr-tb-60" /> 
                                <h2>We won’t lock you in</h2>
                          <ul>
                            <li>Flexible database, download data when required</li>
                            <li>Log the landowner details and manage your sites</li>
                           <li>Store photos of individual frames – to cross check</li>
                           <li>Record location of Apiaries with Google Maps</li>
                          </ul>
                        </div>
                                             
					</div>
                 </div>  
               </div>  

					
                    
                     <div class="container clients pad-tp-30" id="client">
                    	<div class="row">
                        	<div class="col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 text-center">
                             <h2>Who is the Honeyman platform for?</h2>
                            	<hr />
                            <p>This platform has been designed with beekeepers and owners in mind <br />and has been created with direct input from NZ beekeepers:</p>
                            </div>
                        </div>
                        
                        <div class="row mr-tb-40">
                        	<div class="col-xs-12 col-sm-3 col-md-3 col-md-offset-1 col-lg-3">
                       	    	<img src="Marketing/images/rere-bay.png" class="center-block" alt="rere-bay" />
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            	<p>Honeymanhas improved and simplified the process we follow. I can now easily record operational information about my hives daily and can keep a track of where every hive is located. As a small beekeeper this is invaluable and the pricing suited my small operation.<br />
<br />
<strong>Luke<br />
Rere Bay Honey NZ Ltd</p></strong>

                            </div>
                              
                        </div>
                        <hr style="height:1px; width:30%; background-color:#ccc;"/>
                        <div class="row mr-tb-60">
                        
                        <div class="col-xs-12 col-sm-3 col-md-3 col-md-offset-1 col-lg-3">
                       	    	<img src="Marketing/images/mack-honey.jpg" class="center-block" alt="rere-bay" />
                            </div>
                        	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            	<p>Good, solid software, designed for beekeepers, in collaboration with beekeepers, with the future vision of the beekeeping industry incorporated.  It has an easy to use interface adaptable for the field or office.<br />
<br />
<strong>Cris<br />
MACK Honeys</strong>
</p>
                        </div>
                            
                            
                              
                        </div>
                        
                        <hr style="height:1px; width:30%; background-color:#ccc;"/>
                        
                        <div class="row mr-tb-40">
                        	<div class="col-xs-12 col-sm-3 col-md-3 col-md-offset-1 col-lg-3">
                       	    	<img src="Marketing/images/honeyman-nz.jpg" class="center-block" alt="rere-bay" />
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            	<p>I could see that our team needed a simple, understated, practical, non glitzy tool to make their and my life easier. The tool needed to be easy to use on site as well as back at the yard. I feel we now have that tool in the Honeyman platform.<br />
<br />
<strong>Audrey<br /> 
Honeyman NZ</strong>
</p>

                            </div>
                              
                        </div>
                        
                    </div>
                    
                    
                    <div class="container-fluid clients pad-tb-20" id="features">
                    	<div class="container">
                        	<div class="row">
                            	<div class="col-sm-8 col-md-8 col-md-offset-2 col-lg-6 text-center">
                                	<h3>The Honeyman Story</h3>
                                    <hr />
                                    <p>The Honeyman solution came about as most things in life do when the timing is just right and below is the shortened version of the creation of our platform.</p>
                                </div>
                                
								
                            </div>
                        </div>
                    </div>
                    
                    <div class="container">
                    	<div class="row">
                            	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 story-text">
                                	<p>My background is in software development and I am also an accountant, by training.</p>

<p>In 2015 I became involved in the development of a new beekeeping business in the Far North of New Zealand.</p>

<p>By 2017, not surprisingly, with my background, I seriously began to think about developing a tool that would:</p>
<ol>

<li>Record all of the information needed for the good management of our hives on a daily basis.</li>

<li>Record all asset information necessary for ensuring that we understand immediately whereabouts our valuable hives and apiaries were located, and their status.</li>

<li>Enable the traceability of supers or honey boxes from the site to the extracting plant in readiness for pending new regulation.</li>

<li>Produce reports for the longer term management of our business to enable us to maximise revenue and minimize costs.</li>

<li>Provide a means to enable an ALERT mechanism in the case of a serious issue such as American Foul Brood</li>

<li>I could see that our team needed a simple, understated, practical, non glitzy tool to provide them with this information. The tool needed to be easy to use on site as well as back at the yard.</li>
</ol>
<p>The users are:</p>
<ul style="list-style:lower-alpha">

<li>The Operations Managers and Beekeepers for daily management</li>

<li>The Administrators for registration and traceability as well as general administration</li>

<li>The Company Accountant for asset information</li>

<li>The Company Management for management reporting and compliance.</li>
</ul>


<p>With this clear picture of my team’s requirements I approached DB Gurus to help me to develop a database that could meet our needs.</p>

<p>Between us we then expanded on those initial ideas and built the Honeyman platform – A Beekeeper Solution. </p>

<p>Even the costs are straight forward as we are using hive numbers as the criteria so you have a one off fee for set up and then a monthly fee for hosting and support.</p>

<p>Along the way I have learned which barcodes are hardiest, which tools are easiest on site and hundreds of other pertinent pieces of information.</p>

                                </div>
                            </div>
                    </div>
                    
                    <div class="container-fluid pad-tb-20" id="features">
                    	<div class="container">
                        	<div class="row signup">
                            	<div class="col-sm-12 col-md-12 col-lg-12 text-center">
                                	<p>Call or email me. I will be thrilled to show you how our software works.</p>

<p>Audrey Campbell-Frear&nbsp;&nbsp;&nbsp;&nbsp;  <a href="tel:021706434"><strong>021 706 434</strong></a> &nbsp;&nbsp;&nbsp;&nbsp;  <a href="malto:audrey@honeyman.co.nz"><strong>audrey@honeyman.co.nz</strong></a></p>
                                </div>
                                
								
                            </div>
                        </div>
                    </div>

					<div class="container pad-tp-30">
                    	<div class="row reporting">
                        	<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 text-center">
                            	<h2>Why you’ll love the<br />Honeyman platform</h2>
								<hr />
                                
                                <h4><strong>Reporting is now easier than ever</strong></h4><br />
                                <p>This system is quick and easy to set up and requires minimal training to get started.</p>
                            </div>
                        </div>
                        <div class="row report-column mr-tp-42 text-center">
                        	<div class="col-sm-4 col-md-4 col-lg-4">
                       	    	<img src="Marketing/images/peace-of-mind.jpg" width="75" height="75" />
                                <h5><strong>We give you peace of mind</strong></h5>
                                <p>Unsure about cloud security? Cloud-based platforms are a little like air travel. It might seem more dangerous, but because of all the extra precautions we take, it’s actually a lot safer. Here’s what we do to ensure your data is secure in the cloud.</p>
                             </div>    
                             
                             
                             <div class="col-sm-4 col-md-4 col-lg-4 mr-tp-30">
                       	    	<img src="Marketing/images/Accessible.jpg" width="75" height="75" />
                                <h5><strong>Accessible from anywhere</strong></h5>
                                <p>The Honeyman platform is entirely web based. This means everything is in the one place and you can reach it from anywhere. So users can upload data directly from the hive site, even in remote locations. Perfect if you have more than one site or different users at various locations and need a centralised solution.</p>
                                
                             </div>
                             
                             
                             <div class="col-sm-4 col-md-4 col-lg-4">
                       	    	<img src="Marketing/images/importdata.jpg" width="75" height="75" />
                                <h5><strong>Import data from spreadsheets or upload data straight into the system</strong></h5>
                               <ul>
                                    <li>Paste in your text</li>
                                    <li>Upload a few photos</li>
                                    <li>Publish to Word or the Web</li>
                                </ul>
                             </div>                                   

                        </div>
                        
                        
                        <hr />
                        
                        <div class="row report-column pad-tb-30-70 text-center">
                        	<div class="col-sm-4 col-md-4 col-md-offset-2 col-lg-4">
                       	    	<img src="Marketing/images/safe.png" width="75" height="75" />
                                <h5><strong>Your data is safe</strong></h5>
                                <ul>
                                	<li>We do daily backups of the database as well as weekly backups to an offsite location to ensure there is no single point of failure.</li>
<li>You control who can access your sensitive data and what each user can do in each menu.</li>
<li>Honeyman does not allow data to be permanently deleted and all changes are tracked so you can see who changed what and why.
</li>
                                </ul>
                             </div>    
                             
                             
                             <div class="col-sm-4 col-md-4 col-lg-4">
                       	    	<img src="Marketing/images/support.png" width="75" height="75" />
                                <h5><strong>Support</strong></h5>
                               <p>If there’s a problem you can’t solve, we’re here to help. All our customers get free technical support. Depending on the level of support you need, your plan will include email or phone support from one of our skilled technicians.</p>
                             </div> 
                        </div>
                    </div>
                    
                    
                    <div class="container-fluid" style="background:#3e3e3e;" id="pricing">
                    	<div class="container pad-tp-30">
                        	<div class="row price">
                            	<h2 class="text-center">How it works</h2>
                                <hr class="text-center" />		
                            	<div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
                                	<p><strong>Monthly Support and Hosting Plans</strong></p>
<p>We believe in transparent pricing:</p><br />
<ul>
<li><strong>No Hidden Costs:</strong> We’re upfront about all our prices and you’ll never be charged for anything you haven’t agreed to.</li>
<li><strong>No Capital Outlay:</strong> We provide the software and licensing required for you to manage your beekeeping data online.</li>
<li><strong>Simple Pricing: </strong> We will give you a one off quote for data migration and configuration dependent on how many Hives you have. </li>
<li><strong>A Monthly Hosting And Support Plan</strong>: A selection of our monthly plans are shown below, there are numerous levels of pricing so do please get in touch and we can advise you of your monthly fee. </li>

</ul><br />

<p><strong>Not sure which one is right for you? <span><a class="page-scroll" href="#contact">Try it free, no obligation.</a></span></strong> </p>

                                </div>
                            </div>
                            
                            <div class="row mr-tb-40 price-column">
                            	<div class="col-sm-4 col-md-4 col-lg-4">
                                	<div class="col-one-top text-center">
                                    	<h3>$50 <small>NZD</small></h3>                                        <span>Small</span>	 
                                    </div>
                                    <div class="col-one-body">
                                    	<ul>
                                        	<li>Up to <strong>1,500 Hives</strong></li>
                                        	<li>Dashboard, maps</li>
                                            <li>Basic and advanced security</li>
                                            <li>Change tracking and audit report</li>
                                            <li>Simple document management systems</li>
                                            <li>Scheduling and reminders</li>
                                            <li>Report generator</li>
                                            <li>Upload from spreadsheets</li>
                                            <li>Alerts via email</li>
                                            <li class="disable">Alerts via sms</li>
                                            <li class="disable">Automated upload</li>
                                            <li>Support - Email & Phone</li>
                                            <li>Max number of records - <strong>10,000</strong></li>
                                            

                                        </ul>
                                    </div>
                                </div>
                                
                                <div class="col-sm-4 col-md-4 col-lg-4 col-two-style">
                                	<div class="col-two-top text-center">
                                    	<h3>$250 <small>NZD</small></h3>
                                        <span>Standard</span>	 
                                    </div>
                                    <div class="col-two-body">
                                    	<ul>
                                        <li>Up to <strong>20,000 Hives</strong></li>
                                        	<li>Dashboard, maps</li>
                                            <li>Basic and advanced security</li>
                                            <li>Change tracking and audit report</li>
                                            <li>Simple document management systems</li>
                                            <li>Scheduling and reminders</li>
                                            <li>Report generator</li>
                                            <li>Upload from spreadsheets</li>
                                            <li>Alerts via email</li>
                                            <li class="disable">Alerts via sms</li>
                                            <li class="disable">Automated upload</li>
                                            <li>Support - Email & Phone</li>
                                            <li>Max number of records - <strong>100,000</strong></li>
                                            

                                        </ul>
                                    </div>
                                </div>
                                
                                
                                <div class="col-sm-4 col-md-4 col-lg-4">
                                	<div class="col-three-top text-center">
                                    	<h3>$450 <small>NZD</small></h3>
                                        <span>Large</span>	 
                                    </div>
                                    <div class="col-three-body">
                                    	<ul>
                                        <li>Up to <strong>40,000+ Hives</strong></li>
                                        	<li>Dashboard, maps</li>
                                            <li>Basic and advanced security</li>
                                            <li>Change tracking and audit report</li>
                                            <li>Simple document management systems</li>
                                            <li>Scheduling and reminders</li>
                                            <li>Report generator</li>
                                            <li>Upload from spreadsheets</li>
                                            <li>Alerts via email</li>
                                            <li>Alerts via sms</li>
                                            <li>Automated upload</li>
                                            <li>Support - Email & Phone</li>
                                            <li>Max number of records - <strong>500,000</strong></li>
                                            

                                        </ul>
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div> 
                    </div>
                    
                    <div class="container pad-tp-30" id="contact">
                    	<div class="row">
                       		  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 btm-form-bg">
                    <h1>Free Trial</h1>
                    <p><asp:Label ID="lblMessage2" runat="server" Text="Fill in the form below and we’ll get you the answer."></asp:Label></p>
                    <div class="form-group">
                        <asp:TextBox ID="txtName2" CssClass="form-control form-inpt" placeholder="Name*" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="txtEmail2" CssClass="form-control form-inpt" placeholder="Email*" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:TextBox TextMode="MultiLine" rows="6" name="txtMessage" ID="txtMessage" CssClass="form-control form-inpt" placeholder="Question*" runat="server"></asp:TextBox>
                    </div>
                    <br />
                    <asp:Button ID="btnSignUp2" CssClass="btn btn-primary" Text="Send" runat="server" OnClick="btnSignUp2_Click" ValidationGroup="Form2" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ForeColor="White"
                            ErrorMessage="*Please enter your Name." ControlToValidate="txtName2" ValidationGroup="Form2">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ForeColor="White"
                            ErrorMessage="<br/>*Please enter your Email address." ControlToValidate="txtEmail2" ValidationGroup="Form2">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ForeColor="White"
                            ErrorMessage="<br/>*Please enter your Question." ControlToValidate="txtMessage" ValidationGroup="Form2">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ControlToValidate="txtEmail2" ErrorMessage="<br/>Please enter a valid Email address." ValidationGroup="Form2">
                        </asp:RegularExpressionValidator>
                    
                </div>
                             
                             
                             
                             <div class="col-sm-6 col-md-6 col-md-offset-1 col-lg-6 col-lg-offset-1 signup-text">
                             	<h2>How do I sign up for the free trial?</h2>
                                <hr class="pull-left" />
                               
                                <div class="text-one">
                           	    	<img src="Marketing/images/one.png" class="pull-left" width="41" height="41" />
                                    <h3>Sign-up form</h3>
                                  <p>Fill in the form and we’ll contact you within 1 business day to schedule a free introductory session and show you how to use it. </p>
                                </div>
                                 <div class="text-two">
                           	     	<img src="Marketing/images/two.png" class="pull-left" width="41" height="41" />
                                    <h3>Introductory session</h3>
                                    <p>The introductory session only takes half an hour and we can do it remotely over the phone. We’ll share screens and show you how it works.</p><br />

                                    <p>Use the system free for one month. If you like it at the end of the trial, we’ll discuss which plan is best for you.</p>
                                    <p><strong>No credit card details required 
</strong></p>
                                  </div>
                             </div>
                        </div>
                    </div>
                    
                       <div class="container-fluid" style="background:#ebebeb;">
                       		<div class="row">
                            	<div class="col-md-6 col-md-offset-3 text-center">
                                	<p class="lead">Copyright &copy; 2017. All rights reserved. Call:<a href="tel:+6421706434">+6421706434</a> Email: <a href="mailto:help@honeyman.co.nz">help@honeyman.co.nz</a>
</p>
                                </div>
                            </div>
   
                        </div> 
                        
                        
      </form>
</body>
</html>

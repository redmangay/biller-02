﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using Stripe;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.IO;
//using PayPal.Api;
//using PayPal.Sample;
//using PayPalSB;
using Newtonsoft.Json;
using DocGen.DAL;
using DBGPayment.DBGPaypal;

public partial class PaypalAndStripeTest : Page
{
    public string stripePublishableKey = SystemData.SystemOption_ValueByKey_Account("StripePublishableKey", null, null);
    private string secretKey = SystemData.SystemOption_ValueByKey_Account("StripeSecretKey", null, null);
    User _theUser;
    Account _theAccount;
    private int iTotalPayment = 14;
    static HttpClient client = new HttpClient();
    public string GetTotalPaymentAmount()
    {
        return (BillingAPI.TotalChargeAmount((int)_theAccount.AccountID,null,null,null) * 100).ToString();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        _theUser = (User)Session["User"];
        _theAccount = SecurityManager.Account_Details(int.Parse(Session["AccountID"].ToString()));

    }
    protected void btnPaypal_Click(object sender, EventArgs e)
    {
        //string stripePaypalMonthlyAmount = SystemData.SystemOption_ValueByKey_Account("Paypal Monthly Amount", null, null);

        string strPaypalAmount = GetTotalPaymentAmount().ToString();
        Session["PaypalItemName"] = "DBGurus eContractor Payment";
        Session["PaymentAmount"] = strPaypalAmount == "" ? "1" : strPaypalAmount;
        Session["PayIntervalType"] = "D";//M
        Session["InvoiceID"] = _theUser.UserID.ToString() + "_" + Session["AccountID"].ToString(); // a TDB Reference

        Response.Redirect("~/Pages/Security/PayPal/sendpayment_recurring.aspx", false);
    }
    

    public void CleanCustomers()
    {
        try
        {
            StripeCustomerService stripeService = new StripeCustomerService(secretKey);
            var options = new StripeCustomerListOptions();
            options.Limit = 10000;
            var customers = stripeService.List(options);
            //string sTest = "";
            foreach (var customer in customers)
            {
                stripeService.Delete(customer.Id);
                
                //sTest = sTest + "  " + customer.Email;
            }
        }
        catch
        {
        }
    }
    protected void btnCleanCustomer_Click(object sender, EventArgs e)
    {
        StripeConfiguration.SetApiKey(secretKey);
        CleanCustomers();
    }
    protected void btnCutomersManagement_Click(object sender, EventArgs e)
    {
        try
        {
            StripeConfiguration.SetApiKey(secretKey);
            StripeCustomerService stripeService = new StripeCustomerService(secretKey);
            var options = new StripeCustomerListOptions();
            options.Limit = 10000;
            var customers = stripeService.List(options);
            //string sTest = "";
            foreach (var customer in customers)
            {
               
               //get customer metadata to compare accountid & userid
                //customer.Metadata["UserID"] & customer.Metadata["AccountID"]
                if (customer.Email == _theUser.Email && _theUser.UserID.ToString() == customer.Metadata["UserID"].ToString()
                    && Session["AccountID"].ToString() == customer.Metadata["AccountID"].ToString())
                {
                    //stripeService.Delete(customer.Id);//delete this customer and create a new subscription
                    //this is not good, we need to get customer subscription info and update that subsciprtion




                }

            }
        }
        catch
        {
        }
    }
    protected void btnPayOneTime_Click(object sender, EventArgs e)
    {
        string strPaypalAmount = GetTotalPaymentAmount().ToString();
        Session["PaypalItemName"] = "DBGurus eContractor Payment";
        Session["PaymentAmount"] = strPaypalAmount == "" ? "1" : strPaypalAmount;
        //Session["PayIntervalType"] = "D";//M
        Session["InvoiceID"] = _theUser.UserID.ToString() + "_" + Session["AccountID"].ToString(); // a TDB Reference

        Response.Redirect("~/Pages/Security/PayPal/sendpayment.aspx", false);
    }

    protected void btnPP_Del_Subscription_Click(object sender, EventArgs e)
    {
        
    }

  




    protected void btnGetWebProfile_Click(object sender, EventArgs e)
    {
        string sPaymentID = "I-VELXSB5TCAXL";
        //string strAPI = "https://api.sandbox.paypal.com/v2/payments/authorizations/" + sPaymentID;

        string strAPI = "https://api.sandbox.paypal.com/v1/payment-experience/web-profiles";


        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(strAPI);
        request.Method = "GET";

        //request.UserAgent = RequestConstants.UserAgentValue;
        //request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

                     
        request.ContentType = "application/json";
        request.Headers.Add("Accept-Language:en_US");
        //string authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL:EMegY388yteRgur0cy3uAi2adyaiqmd6MQSbmyZtHyVy6n4bJ25WC3s04jiPICUj0l9OzJqZZiFcUcet"));
        string authInfo = BillingAPI.PaypalAuthInfo();

        //request.Headers["Authorization"] = "Basic " + authInfo;
        request.Headers["Authorization"] = "Basic " + authInfo;



        var content = string.Empty;

        using (var response = (HttpWebResponse)request.GetResponse())
        {
            using (var stream = response.GetResponseStream())
            {
                using (var sr = new StreamReader(stream))
                {
                    content = sr.ReadToEnd();
                }
            }
        }
                           

    }

    //protected void btnGetPaymentByService_Click(object sender, EventArgs e)
    //{


    //    var client = new PayPalAPIInterfaceClient();

    //    var credentials = new CustomSecurityHeaderType
    //    {
    //        Credentials = new UserIdPasswordType
    //        {
    //            Username = "dbgurustest2-facilitator_api1.gmail.com",
    //            Password = "WG4EZS6X5WXKSGHM",
    //            Signature = "Ab8forEpUh6-lUiSmcnz6VXH7zz6AB01PA48BvGhGld-enXV7dBu16cp"
    //        }
    //    };
    //    var request = new TransactionSearchReq
    //    {
    //        TransactionSearchRequest = new TransactionSearchRequestType
    //        {
    //            StartDate = DateTime.Now.AddDays(-3),
    //            Status = PaymentTransactionStatusCodeType.Success,
    //            Version = "95.0"
    //        }
    //    };

    //    var response = client.TransactionSearch(ref credentials, request);

    //    string sTest = "";

        


    //}

    //protected void btnDllPayment_Click(object sender, EventArgs e)
    //{

    //    try
    //    {
    //        var apiContext = Configuration.GetAPIContext();
    //        //var paymentList = PayPal.Api.Payment.List(apiContext, count: 1, startIndex: 0);

    //        // ### Create an invoice
    //        // For demonstration purposes, we will create a new invoice for this sample.
    //        var invoice = new PayPal.Api.Invoice()
    //        {
    //            // #### Merchant Information
    //            // Information about the merchant who is sending the invoice.
    //            merchant_info = new MerchantInfo()
    //            {
    //                email = "jziaja.test.merchant-facilitator@gmail.com",
    //                first_name = "Dennis",
    //                last_name = "Doctor",
    //                business_name = "Medical Professionals, LLC",
    //                phone = new Phone()
    //                {
    //                    country_code = "001",
    //                    national_number = "4083741550"
    //                },
    //                address = new InvoiceAddress()
    //                {
    //                    line1 = "1234 Main St.",
    //                    city = "Portland",
    //                    state = "OR",
    //                    postal_code = "97217",
    //                    country_code = "US"
    //                }
    //            },
    //            // #### Billing Information
    //            // Email address of invoice recipient and optional billing information.
    //            // > Note: PayPal currently only allows one recipient.
    //            billing_info = new List<BillingInfo>()
    //            {
    //                new BillingInfo()
    //                {
    //                    // **(Required)** Email address of the invoice recipient.
    //                    email = "example@example.com"
    //                }
    //            },
    //            // #### Invoice Items
    //            // List of items to be included in the invoice.
    //            // > Note: 100 max per invoice.
    //            items = new List<PayPal.Api.InvoiceItem>()
    //            {
    //                new PayPal.Api.InvoiceItem()
    //                {
    //                    name = "Sutures",
    //                    quantity = 1,
    //                    unit_price = new PayPal.Api.Currency()
    //                    {
    //                        currency = "AUD",
    //                        value = "5"
    //                    }
    //                }
    //            },
    //            // #### Invoice Note
    //            // Note to the payer. Maximum length is 4000 characters.
    //            note = "Medical Invoice 16 Jul, 2013 PST",
    //            // #### Payment Term
    //            // **(Optional)** Specifies the payment deadline for the invoice.
    //            // > Note: Either `term_type` or `due_date` can be sent, **but not both.**
    //            payment_term = new PayPal.Api.PaymentTerm()
    //            {
    //                term_type = "NET_30"
    //            },
    //            // #### Shipping Information
    //            // Shipping information for entities to whom items are being shipped.
    //            shipping_info = new ShippingInfo()
    //            {
    //                first_name = "Sally",
    //                last_name = "Patient",
    //                business_name = "Not applicable",
    //                address = new InvoiceAddress()
    //                {
    //                    line1 = "1234 Broad St.",
    //                    city = "Portland",
    //                    state = "OR",
    //                    postal_code = "97216",
    //                    country_code = "AU"
    //                }
    //            }
    //        };

    //        // Create the invoice
    //        var createdInvoice = invoice.Create(apiContext);


    //        string sDebug = "test";




    //        // Setup the profile we want to create
    //        //var profile = new WebProfile()
    //        //{
    //        //    name = Guid.NewGuid().ToString(),
    //        //    presentation = new Presentation()
    //        //    {
    //        //        brand_name = "DBG Sample profile 1",
    //        //        locale_code = "AU",
    //        //        logo_image = "https://www.paypal.com/"
    //        //    },
    //        //    input_fields = new InputFields()
    //        //    {
    //        //        address_override = 1,
    //        //        allow_note = true,
    //        //        no_shipping = 0
    //        //    },
    //        //    flow_config = new FlowConfig()
    //        //    {
    //        //        bank_txn_pending_url = "https://www.paypal.com/",
    //        //        landing_page_type = "billing"
    //        //    }
    //        //};

    //        // Create the profile
    //        //var response = profile.Create(apiContext);

    //        // Get the profile using the ID returned from the previous Create() call.
    //        //var retrievedProfile = WebProfile.Get(apiContext, response.id);


    //        // Cleanup by deleting the newly-created profile
    //        //retrievedProfile.Delete(apiContext);

    //        //var profileList = WebProfile.GetList(apiContext);

    //    }
    //    catch(Exception ex)
    //    {
    //        //
    //    }
        
        

    //}

    protected void btnGetAPaymentByAPI_Click(object sender, EventArgs e)
    {
        string sPaymentID = "PAYID-LUIF6YQ1NS27299DV434754V";
        string strAPI = "https://api.sandbox.paypal.com/v2/payments/authorizations/" + sPaymentID;
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(strAPI);
        request.Method = "GET";
        request.ContentType = "application/json";
        request.Headers.Add("Accept-Language:en_US");
        //string authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL:EMegY388yteRgur0cy3uAi2adyaiqmd6MQSbmyZtHyVy6n4bJ25WC3s04jiPICUj0l9OzJqZZiFcUcet"));

        string authInfo = BillingAPI.PaypalAuthInfo();
        request.Headers["Authorization"] = "Basic " + authInfo;
        var content = string.Empty;

        using (var response = (HttpWebResponse)request.GetResponse())
        {
            using (var stream = response.GetResponseStream())
            {
                using (var sr = new StreamReader(stream))
                {
                    content = sr.ReadToEnd();
                }
            }
        }


        string sTest = "";
    }

    //protected void btnGetPaymentByDLL_Click(object sender, EventArgs e)
    //{
    //    var apiContext = Configuration.GetAPIContext();
    //    var paymentId = "PAYID-LUIF6YQ1NS27299DV434754V";
    //    var payment = PayPal.Api.Payment.Get(apiContext, paymentId);
    //    string sTest = "";
    //}

    protected void btnStripeSinglePayment_Click(object sender, EventArgs e)
    {
        StripeConfiguration.SetApiKey(secretKey);


        var options = new StripeChargeCreateOptions
        {
            Amount = int.Parse(GetTotalPaymentAmount()),
            Currency = "aud",
            Description = "Charge for testing",
            SourceTokenOrExistingSourceId = Request.Form["stripeToken"].ToString(),
            Metadata = new Dictionary<string, string>()
                        {
                            { "UserID", _theUser.UserID.ToString() },
                            { "AccountID", Session["AccountID"].ToString() },
                            { "BillingID", "-1" }
                        },
            CustomerId = ""

        };

        var service = new StripeChargeService();
        StripeCharge charge = service.Create(options);
    }

    protected void btnStripeUpdateSubscription_Click(object sender, EventArgs e)
    {

    }

    protected void btnStripeSubscription_Click(object sender, EventArgs e)
    {

    }

    protected void btnStripeFindCustomerAndSubscription_Click(object sender, EventArgs e)
    {

        StripeCustomerService stripeService = new StripeCustomerService(secretKey);
        var options = new StripeCustomerListOptions();
        options.Limit = 10000;
        var customers = stripeService.List(options);
        //string sTest = "";
        string sSubscriptionID = "";
        foreach (var customer in customers)
        {
            if(customer.Email.ToLower()==_theUser.Email.ToLower())
            {
                System.Collections.Generic.Dictionary<string, string> cusMetaData = customer.Metadata;
                if (cusMetaData != null && cusMetaData["UserID"] == _theUser.UserID.ToString() && cusMetaData["AccountID"] == _theAccount.AccountID.ToString())
                {
                  StripeList<StripeSubscription> lstSubscription= customer.Subscriptions;
                    foreach(StripeSubscription eachSubscription in lstSubscription)
                    {
                        sSubscriptionID = eachSubscription.Id;
                        break;
                    }
                }
            }            
        }
        var service = new StripeSubscriptionService(secretKey);
        StripeSubscription theSubscription = service.Get(sSubscriptionID);
        int iPlanQuantity = BillingAPI.TotalChargeAmount((int)_theAccount.AccountID,null,null,null)*100;

        var items = new List<StripeSubscriptionItemUpdateOption> {
                    new StripeSubscriptionItemUpdateOption {
                        //PlanId = "ID_Daily01",
                        Id = theSubscription.Items.Data[0].Id,
                        Quantity=iPlanQuantity

                    }//plan_CBXbz9i7AIOTzr  //  //prod_FGUq8AU1HhiyXF
                };

        var Sub_options = new StripeSubscriptionUpdateOptions
        {
            Items = items,
            //Source = Request.Form["stripeToken"].ToString(),
            Metadata = new Dictionary<string, string>()
                        {
                            { "UserID", _theUser.UserID.ToString() },
                            { "AccountID", Session["AccountID"].ToString() }//,
                           // { "BillingID", Session["BillingID"].ToString() }
                        }
        };
        theSubscription = service.Update(theSubscription.Id, Sub_options);

        




        //        var items = new List<SubscriptionItemUpdateOption> {
        //    new SubscriptionItemUpdateOption {
        //        Id = subscription.Items.Data[0].Id,
        //        PlanId = "plan_CBb6IXqvTLXp3f",
        //    },
        //};
        //        var options = new SubscriptionUpdateOptions
        //        {
        //            CancelAtPeriodEnd = false,
        //            Items = items,
        //        };
        //        subscription = service.Update("sub_49ty4767H20z6a", options);

        //StripeSubscriptionService stripeService = new StripeSubscriptionService(secretKey);
        //var options = new StripeSubscriptionListOptions();
        //options.Limit = 10000;
        //var subscriptions = stripeService.List(options);


    }

    protected void btnStripeNextUpcomingInvoice_Click(object sender, EventArgs e)
    {
        StripeCustomerService stripeService = new StripeCustomerService(secretKey);
        var options = new StripeCustomerListOptions();
        options.Limit = 10000;
        var customers = stripeService.List(options);
        //string sTest = "";
        string sSubscriptionID = "";
        foreach (var customer in customers)
        {
            if (customer.Email.ToLower() == _theUser.Email.ToLower())
            {
                System.Collections.Generic.Dictionary<string, string> cusMetaData = customer.Metadata;
                if (cusMetaData != null && cusMetaData["UserID"] == _theUser.UserID.ToString() && cusMetaData["AccountID"] == _theAccount.AccountID.ToString())
                {
                    StripeList<StripeSubscription> lstSubscription = customer.Subscriptions;
                    foreach (StripeSubscription eachSubscription in lstSubscription)
                    {
                        sSubscriptionID = eachSubscription.Id;
                        break;                        
                    }
                    if(!string.IsNullOrEmpty(sSubscriptionID))
                    {
                        break;
                    }
                }
            }
        }

        var serviceUpInvoice = new StripeInvoiceService();
        var options_up_invoice = new StripeUpcomingInvoiceOptions
        {
             SubscriptionId = sSubscriptionID,
        };

        var serviceSub = new StripeSubscriptionService();
        StripeSubscription theSubscription = serviceSub.Get(sSubscriptionID);


        StripeInvoice upcoming = serviceUpInvoice.Upcoming(theSubscription.CustomerId, options_up_invoice);
        int iAmountDue = upcoming.AmountDue;

    }

    protected void btnStripeDeleteSubscriptionAndCreateAgain_Click(object sender, EventArgs e)
    {
        StripeCustomerService stripeService = new StripeCustomerService(secretKey);
        var options = new StripeCustomerListOptions();
        options.Limit = 10000;
        var customers = stripeService.List(options);
        //string sTest = "";
        string sSubscriptionID = "";
        string sCustomerID = "";
        foreach (var customer in customers)
        {
            if (customer.Email.ToLower() == _theUser.Email.ToLower())
            {
                System.Collections.Generic.Dictionary<string, string> cusMetaData = customer.Metadata;
                if (cusMetaData != null && cusMetaData["UserID"] == _theUser.UserID.ToString() && cusMetaData["AccountID"] == _theAccount.AccountID.ToString())
                {
                    StripeList<StripeSubscription> lstSubscription = customer.Subscriptions;
                    sCustomerID = customer.Id;
                    foreach (StripeSubscription eachSubscription in lstSubscription)
                    {
                        sSubscriptionID = eachSubscription.Id;
                        break;
                    }
                }
            }
        }
        var serviceSub = new StripeSubscriptionService();
        serviceSub.Cancel(sSubscriptionID);

        int iPlanQuantity = int.Parse(GetTotalPaymentAmount());
        var items = new List<StripeSubscriptionItemOption> {
                    new StripeSubscriptionItemOption {
                        PlanId = BillingAPI.StripeAccountPlanID(int.Parse(Session["AccountID"].ToString())),
                        Quantity=iPlanQuantity+1000

                    }//plan_CBXbz9i7AIOTzr  //  //prod_FGUq8AU1HhiyXF
                };

        var optionsSubCreate = new StripeSubscriptionCreateOptions
        {
            Items = items,
            //Source = Request.Form["stripeToken"].ToString(),
            Metadata = new Dictionary<string, string>()
                        {
                            { "UserID", _theUser.UserID.ToString() },
                            { "AccountID", Session["AccountID"].ToString() }//,
                            //{ "BillingID", Session["BillingID"].ToString() }
                        }
        };

        StripeSubscription subscription = serviceSub.Create(sCustomerID, optionsSubCreate);//  "cus_4fdAW5ftNQow1a"
        string sTestString = "";
    }


    protected void btnCheckAPI_Click(object sender, EventArgs e)
    {
        //string PP
        //HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://api.sandbox.paypal.com/v1/oauth2/token");

        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL()+ "/v1/oauth2/token");

        request.Method = "POST";
        request.Accept = "application/json";
        request.Headers.Add("Accept-Language:en_US");

        // this doesn't work:
        //* *request.Credentials = new NetworkCredential("A****", "E****"); **

        // DO THIS INSTEAD
        //client id:secret

        //string authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL:EMegY388yteRgur0cy3uAi2adyaiqmd6MQSbmyZtHyVy6n4bJ25WC3s04jiPICUj0l9OzJqZZiFcUcet"));

        string authInfo = BillingAPI.PaypalAuthInfo();
        request.Headers["Authorization"] = "Basic " + authInfo;
    
        using (StreamWriter swt = new StreamWriter(request.GetRequestStream()))
        {
            swt.Write("grant_type=client_credentials");
        }

        request.BeginGetResponse((r) =>
        {
            try
            {
                HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
                //....
            }
            catch (Exception x)
            {
                //.... 
            } // log the exception - 401 Unauthorized
        }, null);
    }

    protected void btnAPI_CreateProduct_Click(object sender, EventArgs e)
    {
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/catalogs/products");

        request.Method = "POST";
        //request.Accept = "application/json";
        request.ContentType = "application/json";
        //request.Headers.Add("Accept-Language:en_US");
        //string authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL:EMegY388yteRgur0cy3uAi2adyaiqmd6MQSbmyZtHyVy6n4bJ25WC3s04jiPICUj0l9OzJqZZiFcUcet"));

        string authInfo = BillingAPI.PaypalAuthInfo();
        request.Headers["Authorization"] = "Basic " + authInfo;       
        
        //string sProductJson = "{\"name\": \"OnTask\", \"description\": \"OnTask service\", \"type\": \"Service type\", \"category\": \"SOFTWARE category\", \"image_url\": \"https://www.ontaskhq.com.au/img/logo.png\", \"home_url\": \"https://www.ontaskhq.com.au\"}";
        string sProductJson = "{  \"name\": \"Ontask\","+
            "\"description\": \"OnTask service\", " +
            "\"type\": \"SERVICE\"," +
            "\"category\": \"SOFTWARE\"," +
            "\"image_url\": \"https://www.ontaskhq.com.au/img/logo.png\"," +
            "\"home_url\": \"https://www.ontaskhq.com.au\"}";

        Product newProduct = new Product();
        newProduct.name = "OnTask";
        newProduct.description = "OnTask service";
        newProduct.type = "SERVICE";
        newProduct.category = "SOFTWARE";
        newProduct.image_url = "https://www.ontaskhq.com.au/img/logo.png";
        newProduct.home_url = "https://www.ontaskhq.com.au";
        sProductJson = newProduct.GetJSONString();

        //     Gets a System.IO.Stream object to use to write request data.
        using (StreamWriter swt = new StreamWriter(request.GetRequestStream()))
        {
            swt.Write(sProductJson);           
        }
        //HttpWebResponse response2;
        request.BeginGetResponse((r) =>
        {
            try
            {
                HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
                System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                string strResponseJSON = reader.ReadToEnd().Trim();

                Product theProduct = (Product)JsonConvert.DeserializeObject(strResponseJSON, typeof(Product));
                if (theProduct != null)
                {
                  
                    string sProductID = theProduct.id;
                 
                }

            }
            catch (Exception x)
            {
                //.... 
            } // log the exception - 401 Unauthorized
        }, null);

        string sJSONWebResponse = "";// Response.Output.ToString();

    }

    protected void btnAPI_ListProduct_Click(object sender, EventArgs e)
    {
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/catalogs/products?page_size=200&page=1&total_required=true");

        request.Method = "GET";
        request.ContentType = "application/json";
        //string authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL:EMegY388yteRgur0cy3uAi2adyaiqmd6MQSbmyZtHyVy6n4bJ25WC3s04jiPICUj0l9OzJqZZiFcUcet"));
        string authInfo = BillingAPI.PaypalAuthInfo();
        request.Headers["Authorization"] = "Basic " + authInfo;

        request.BeginGetResponse((r) =>
        {
            try
            {
                HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
                System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                string strResponseJSON = reader.ReadToEnd().Trim();

                ProductList theProductList = (ProductList)JsonConvert.DeserializeObject(strResponseJSON, typeof(ProductList));
                if(theProductList!=null)
                {
                    foreach(Product eachProduct in theProductList.products)
                    {
                        string sProductName = eachProduct.name;
                        //delete testing products plz
                    }
                }
                
            }
            catch (Exception x)
            {
                //.... 
            } // log the exception - 401 Unauthorized
        }, null);

        string sJSONWebResponse = "";// Response.Output.ToString();


    }

    protected void btnAPI_CreatePlan_Click(object sender, EventArgs e)
    {
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/billing/plans");

        request.Method = "POST";
        request.Accept = "application/json";
        request.ContentType = "application/json";
        request.Headers.Add("Prefer: return=representation");
        //request.Headers.Add("Accept-Language:en_US");
        //string authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL:EMegY388yteRgur0cy3uAi2adyaiqmd6MQSbmyZtHyVy6n4bJ25WC3s04jiPICUj0l9OzJqZZiFcUcet"));

        string authInfo = BillingAPI.PaypalAuthInfo();
        request.Headers["Authorization"] = "Basic " + authInfo;

        DBGPayment.DBGPaypal.Plan newPlan = new DBGPayment.DBGPaypal.Plan();
        newPlan.product_id = "PROD-0TF91806F69784250";// "PROD -0TF91806F69784250";
        newPlan.name = "ontask plan";
        newPlan.description = "ontask description";

        Frequency theFrequency = new Frequency();
        theFrequency.interval_count = 1;
        theFrequency.interval_unit = "MONTH";
        BillingCycle theBillingCycle = new BillingCycle();
        theBillingCycle.frequency = theFrequency;
        theBillingCycle.tenure_type = "TRIAL";
        theBillingCycle.sequence = 1;
        theBillingCycle.total_cycles = 1;

        Frequency theFrequency2 = new Frequency();
        theFrequency2.interval_count = 1;
        theFrequency2.interval_unit = "MONTH";
        BillingCycle theBillingCycle2 = new BillingCycle();
        theBillingCycle2.frequency = theFrequency2;
        theBillingCycle2.tenure_type = "REGULAR";
        theBillingCycle2.sequence = 2;
        theBillingCycle2.total_cycles = 12;

        FixedPrice theFixedPrice = new FixedPrice();
        theFixedPrice.value = "10";
        theFixedPrice.currency_code = "USD";
        PricingScheme thePricingScheme = new PricingScheme();
        thePricingScheme.fixed_price = theFixedPrice;

        theBillingCycle2.pricing_scheme = thePricingScheme;

        PaymentPreferences thePaymentPreferences = new PaymentPreferences();
        thePaymentPreferences.service_type = "PREPAID";
        thePaymentPreferences.auto_bill_outstanding = true;
        SetupFee theSetupFee = new SetupFee();
        theSetupFee.value = "10";
        theSetupFee.currency_code = "USD";
        thePaymentPreferences.setup_fee = theSetupFee;
        thePaymentPreferences.setup_fee_failure_action = "CONTINUE";
        thePaymentPreferences.payment_failure_threshold = 3;

        List<BillingCycle> lstBillingCycle = new List<BillingCycle>();
        lstBillingCycle.Add(theBillingCycle);
        lstBillingCycle.Add(theBillingCycle2);

        newPlan.billing_cycles = lstBillingCycle;

        newPlan.payment_preferences = thePaymentPreferences;
        newPlan.quantity_supported = true;

        Taxes theTaxes = new Taxes();
        theTaxes.percentage = "10";
        theTaxes.inclusive = false;
        newPlan.taxes = theTaxes;



        string sPlanJson = newPlan.GetJSONString();

        string sPlanJson2 = "{"+
                            "\"product_id\": \"PROD-1RX94744BS028372A\"," +
                            "\"name\": \"Basic Plan\"," +
                            "\"description\": \"Basic plan\"," +
                            "\"billing_cycles\": [" +
                              "{"+
                                "\"frequency\": {" +
                                    "\"interval_unit\": \"MONTH\"," +
                                    "\"interval_count\": 1" +
                                "},"+
                                "\"tenure_type\": \"TRIAL\"," +
                                "\"sequence\": 1," +
                                "\"total_cycles\": 1" +
                              "},"+
                                "{"+
                                  "\"frequency\": {"+
                                    "\"interval_unit\": \"MONTH\","+
                                    "\"interval_count\": 1"+
                                  "},"+
                                  "\"tenure_type\": \"REGULAR\","+
                                  "\"sequence\": 2,"+
                                  "\"total_cycles\": 12,"+
                                  "\"pricing_scheme\": {"+
                                    "\"fixed_price\": {"+
                                      "\"value\": \"10\","+
                                      "\"currency_code\": \"USD\""+
                                    "}"+
                                  "}"+
                                "}"+
                              "],"+
                            "\"payment_preferences\": {"+
                              "\"service_type\": \"PREPAID\","+
                              "\"auto_bill_outstanding\": true,"+
                              "\"setup_fee\": {"+
                                "\"value\": \"10\","+
                                "\"currency_code\": \"USD\""+
                              "},"+
                              "\"setup_fee_failure_action\": \"CONTINUE\","+
                              "\"payment_failure_threshold\": 3"+
                            "},"+
                            "\"quantity_supported\": true,"+
                            "\"taxes\": {"+
                              "\"percentage\": \"10\","+
                              "\"inclusive\": false"+
                            "}"+
                        "}";



        //     Gets a System.IO.Stream object to use to write request data.
        using (StreamWriter swt = new StreamWriter(request.GetRequestStream()))
        {
            swt.Write(sPlanJson);
        }


        //HttpWebResponse response2;
        request.BeginGetResponse((r) =>
        {
            try
            {
                HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
                System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                string strResponseJSON = reader.ReadToEnd().Trim();

                DBGPayment.DBGPaypal.Plan thePlan = (DBGPayment.DBGPaypal.Plan)JsonConvert.DeserializeObject(strResponseJSON, typeof(DBGPayment.DBGPaypal.Plan));
                if (thePlan != null)
                {
                    //string sPlanID = thePlan.id;
                }
            }
            catch (Exception x)
            {
                //.... 
            } // log the exception - 401 Unauthorized
        }, null);

        string sJSONWebResponse = "";// Response.Output.ToString();
    }

    protected void btnAPI_ListPlan_Click(object sender, EventArgs e)
    {
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/payments/billing-plans?page_size=3&status=ALL&page_size=2&page=1&total_required=yes");

        request.Method = "GET";
        request.ContentType = "application/json";
        //string authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL:EMegY388yteRgur0cy3uAi2adyaiqmd6MQSbmyZtHyVy6n4bJ25WC3s04jiPICUj0l9OzJqZZiFcUcet"));
        string authInfo = BillingAPI.PaypalAuthInfo();

        request.Headers["Authorization"] = "Basic " + authInfo;

        request.BeginGetResponse((r) =>
        {
            try
            {
                HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
                System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                string strResponseJSON = reader.ReadToEnd().Trim();

                PlanList thePlanList = (PlanList)JsonConvert.DeserializeObject(strResponseJSON, typeof(PlanList));
                if (thePlanList != null)
                {
                    foreach (Plan eachPlan in thePlanList.plans)
                    {
                        string sPlanID = eachPlan.id;
                        //delete testing products plz //P-59U36987WV1239234VWP4ULI
                    }
                }

            }
            catch (Exception x)
            {
                //.... 
            } // log the exception - 401 Unauthorized
        }, null);

        string sJSONWebResponse = "";// Response.Output.ToString();
    }

    protected void btnAPI_UpdateSubscription_Click(object sender, EventArgs e)
    {

    }

    protected void btnAPI_CreateMonthlyPlan_Click(object sender, EventArgs e)
    {
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/billing/plans");

        request.Method = "POST";
        request.Accept = "application/json";
        request.ContentType = "application/json";
        request.Headers.Add("Prefer: return=representation");
        //request.Headers.Add("Accept-Language:en_US");
        //string authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL:EMegY388yteRgur0cy3uAi2adyaiqmd6MQSbmyZtHyVy6n4bJ25WC3s04jiPICUj0l9OzJqZZiFcUcet"));
        string authInfo = BillingAPI.PaypalAuthInfo();
        request.Headers["Authorization"] = "Basic " + authInfo;

        DBGPayment.DBGPaypal.Plan newPlan = new DBGPayment.DBGPaypal.Plan();
        newPlan.product_id = BillingAPI.PaypalProductID();// "PROD -0TF91806F69784250";
        newPlan.name = "OnTask Monthly Plan";
        newPlan.description = "OnTask Monthly Plan Description";

        Frequency theFrequency = new Frequency();
        theFrequency.interval_count = 1;
        theFrequency.interval_unit = BillingAPI.PaypalMonthlyInterval();// "MONTH";
        //BillingCycle theBillingCycle = new BillingCycle();
        //theBillingCycle.frequency = theFrequency;
        //theBillingCycle.tenure_type = "TRIAL";
        //theBillingCycle.sequence = 1;
        //theBillingCycle.total_cycles = 1;

        Frequency theFrequency2 = new Frequency();
        theFrequency2.interval_count = 1;
        theFrequency2.interval_unit = BillingAPI.PaypalMonthlyInterval(); //"MONTH";
        BillingCycle theBillingCycle2 = new BillingCycle();
        theBillingCycle2.frequency = theFrequency2;
        theBillingCycle2.tenure_type = "REGULAR";
        theBillingCycle2.sequence = 1;
        theBillingCycle2.total_cycles = 99;

        FixedPrice theFixedPrice = new FixedPrice();
        theFixedPrice.value = "0.01";//0.01
        theFixedPrice.currency_code = "AUD";
        PricingScheme thePricingScheme = new PricingScheme();
        thePricingScheme.fixed_price = theFixedPrice;

        theBillingCycle2.pricing_scheme = thePricingScheme;

        PaymentPreferences thePaymentPreferences = new PaymentPreferences();
        thePaymentPreferences.service_type = "PREPAID";
        thePaymentPreferences.auto_bill_outstanding = true;
        //SetupFee theSetupFee = new SetupFee();
        //theSetupFee.value = "0";
        //theSetupFee.currency_code = "AUD";
        //thePaymentPreferences.setup_fee = theSetupFee;
        thePaymentPreferences.setup_fee_failure_action = "CONTINUE";
        thePaymentPreferences.payment_failure_threshold = 3;

        List<BillingCycle> lstBillingCycle = new List<BillingCycle>();
        //lstBillingCycle.Add(theBillingCycle);
        lstBillingCycle.Add(theBillingCycle2);

        newPlan.billing_cycles = lstBillingCycle;

        newPlan.payment_preferences = thePaymentPreferences;
        newPlan.quantity_supported = true;

        //Taxes theTaxes = new Taxes();
        //theTaxes.percentage = "10";
        //theTaxes.inclusive = false;
        //newPlan.taxes = theTaxes;


        string sPlanJson = newPlan.GetJSONString();               

        //     Gets a System.IO.Stream object to use to write request data.
        using (StreamWriter swt = new StreamWriter(request.GetRequestStream()))
        {
            swt.Write(sPlanJson);
        }


        //HttpWebResponse response2;
        request.BeginGetResponse((r) =>
        {
            try
            {
                HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
                System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                string strResponseJSON = reader.ReadToEnd().Trim();

                DBGPayment.DBGPaypal.Plan thePlan = (DBGPayment.DBGPaypal.Plan)JsonConvert.DeserializeObject(strResponseJSON, typeof(DBGPayment.DBGPaypal.Plan));
                if (thePlan != null)
                {
                    string sPlanID = thePlan.id;
                }
            }
            catch (Exception x)
            {
                //.... 
            } // log the exception - 401 Unauthorized
        }, null);

        string sJSONWebResponse = "";// Response.Output.ToString();
    }

    protected void btnAPI_CreateYearlyPaln_Click(object sender, EventArgs e)
    {
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/billing/plans");

        request.Method = "POST";
        request.Accept = "application/json";
        request.ContentType = "application/json";
        request.Headers.Add("Prefer: return=representation");
        //request.Headers.Add("Accept-Language:en_US");
        //string authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL:EMegY388yteRgur0cy3uAi2adyaiqmd6MQSbmyZtHyVy6n4bJ25WC3s04jiPICUj0l9OzJqZZiFcUcet"));
        string authInfo = BillingAPI.PaypalAuthInfo();

        request.Headers["Authorization"] = "Basic " + authInfo;

        DBGPayment.DBGPaypal.Plan newPlan = new DBGPayment.DBGPaypal.Plan();
        newPlan.product_id = BillingAPI.PaypalProductID();// "PROD -0TF91806F69784250";
        newPlan.name = "OnTask Yearly Plan";
        newPlan.description = "OnTask Yearly Plan Description";

        Frequency theFrequency = new Frequency();
        theFrequency.interval_count = 1;
        theFrequency.interval_unit = BillingAPI.PaypalYearlyInterval();// "MONTH";
        //BillingCycle theBillingCycle = new BillingCycle();
        //theBillingCycle.frequency = theFrequency;
        //theBillingCycle.tenure_type = "TRIAL";
        //theBillingCycle.sequence = 1;
        //theBillingCycle.total_cycles = 1;

        Frequency theFrequency2 = new Frequency();
        theFrequency2.interval_count = 1;
        theFrequency2.interval_unit = BillingAPI.PaypalMonthlyInterval(); //"MONTH";
        BillingCycle theBillingCycle2 = new BillingCycle();
        theBillingCycle2.frequency = theFrequency2;
        theBillingCycle2.tenure_type = "REGULAR";
        theBillingCycle2.sequence = 1;
        theBillingCycle2.total_cycles = 99;

        FixedPrice theFixedPrice = new FixedPrice();
        theFixedPrice.value = "0.01";
        theFixedPrice.currency_code = "AUD";
        PricingScheme thePricingScheme = new PricingScheme();
        thePricingScheme.fixed_price = theFixedPrice;

        theBillingCycle2.pricing_scheme = thePricingScheme;

        PaymentPreferences thePaymentPreferences = new PaymentPreferences();
        thePaymentPreferences.service_type = "PREPAID";
        thePaymentPreferences.auto_bill_outstanding = true;
        //SetupFee theSetupFee = new SetupFee();
        //theSetupFee.value = "0";
        //theSetupFee.currency_code = "AUD";
        //thePaymentPreferences.setup_fee = theSetupFee;
        thePaymentPreferences.setup_fee_failure_action = "CONTINUE";
        //thePaymentPreferences.payment_failure_threshold = 3;

        List<BillingCycle> lstBillingCycle = new List<BillingCycle>();
        //lstBillingCycle.Add(theBillingCycle);
        lstBillingCycle.Add(theBillingCycle2);

        newPlan.billing_cycles = lstBillingCycle;

        newPlan.payment_preferences = thePaymentPreferences;
        newPlan.quantity_supported = true;

        //Taxes theTaxes = new Taxes();
        //theTaxes.percentage = "10";
        //theTaxes.inclusive = false;
        //newPlan.taxes = theTaxes;


        string sPlanJson = newPlan.GetJSONString();

        //     Gets a System.IO.Stream object to use to write request data.
        using (StreamWriter swt = new StreamWriter(request.GetRequestStream()))
        {
            swt.Write(sPlanJson);
        }


        //HttpWebResponse response2;
        request.BeginGetResponse((r) =>
        {
            try
            {
                HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
                System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                string strResponseJSON = reader.ReadToEnd().Trim();

                DBGPayment.DBGPaypal.Plan thePlan = (DBGPayment.DBGPaypal.Plan)JsonConvert.DeserializeObject(strResponseJSON, typeof(DBGPayment.DBGPaypal.Plan));
                if (thePlan != null)
                {
                    string sPlanID = thePlan.id;
                }
            }
            catch (Exception x)
            {
                //.... 
            } // log the exception - 401 Unauthorized
        }, null);

        string sJSONWebResponse = "";// Response.Output.ToString();
    }

    protected void btnAPI_ViewPlan_Click(object sender, EventArgs e)
    {
        string sPlanID =BillingAPI.PaypalMonthlyPlanID();
        string strAPI = SecurityManager.PaypalAPIBasicURL() + "/v1/billing/plans/" + sPlanID;
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(strAPI);

        request.Method = "GET";
        request.ContentType = "application/json";
        request.Headers.Add("Accept-Language:en_US");
        //string authInfo = Convert.ToBase64String(System.Text.Encoding.Default.GetBytes("Ac4Hg9VKviZg6khOTAO2HZhejZFd4MruDvpPOt4VeYZa5TlVFcINPh8TT6Az450Is7P2KMpoTlIjg-cL:EMegY388yteRgur0cy3uAi2adyaiqmd6MQSbmyZtHyVy6n4bJ25WC3s04jiPICUj0l9OzJqZZiFcUcet"));

        string authInfo = BillingAPI.PaypalAuthInfo();
        request.Headers["Authorization"] = "Basic " + authInfo;
        var content = string.Empty;

        using (var response = (HttpWebResponse)request.GetResponse())
        {
            using (var stream = response.GetResponseStream())
            {
                using (var sr = new StreamReader(stream))
                {
                    content = sr.ReadToEnd();
                }
            }
        }


        string sTest = "";

    }

    protected void btnAPI_UpdatePlan_Click(object sender, EventArgs e)
    {

        string sPlanID = BillingAPI.PaypalMonthlyPlanID();
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(SecurityManager.PaypalAPIBasicURL() + "/v1/billing/plans/" + sPlanID);

        request.Method = "PATCH";
        request.ContentType = "application/json";
        string authInfo = BillingAPI.PaypalAuthInfo();
        request.Headers["Authorization"] = "Basic " + authInfo;


        //string sPlanPATCH = "[" +
        //                       "{" +
        //                         " \"op\": \"replace\"," +
        //                         " \"path\": \"/payment_preferences/payment_failure_threshold\"," +
        //                          " \"value\": 1" +
        //                             "}" +
        //                      "]";
        string sPlanPATCH = "[" +
                               "{" +
                                 " \"op\": \"replace\"," +
                                 " \"path\": \"/payment_preferences/setup_fee_failure_action\"," +
                                  " \"value\": CONTINUE" +
                                     "}" +
                              "]";

        //string sPlanPATCH = "[" +
        //                       "{" +
        //                         " \"op\": \"replace\"," +
        //                         " \"path\": \"/billing_cycles[0]/pricing_scheme/fixed_price\"," +
        //                          " \"value\": \"5\"" +
        //                             "}" +
        //                      "]";




        //     Gets a System.IO.Stream object to use to write request data.
        using (StreamWriter swt = new StreamWriter(request.GetRequestStream()))
        {
            swt.Write(sPlanPATCH);
        }
        //HttpWebResponse response2;
        request.BeginGetResponse((r) =>
        {
            try
            {
                HttpWebResponse response = request.EndGetResponse(r) as HttpWebResponse; // Exception here
                System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                string strResponseJSON = reader.ReadToEnd().Trim();

                //Product theProduct = (Product)JsonConvert.DeserializeObject(strResponseJSON, typeof(Product));
                //if (theProduct != null)
                //{

                //    string sProductID = theProduct.id;

                //}

            }
            catch (Exception ex)
            {
                //.... 
            } // log the exception - 401 Unauthorized
        }, null);

        string sJSONWebResponse2 = "";// Response.Output.ToString();
    }
}




//static async Task<string> GetTestAsync(string path)
//{
//    //Product product = null;
//    client.DefaultRequestHeaders.Accept.Clear();
//    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

//    HttpResponseMessage response = await client.GetAsync(path);
//    //if (response.IsSuccessStatusCode)
//    //{
//    //    product = await response.Content.ReadAsAsync<Product>();
//    //}
//    return response.ToString();
//}



//invoice.created 
//string json = "{ \"id\": \"evt_1Bw5GTCtiZ7cOVF6xgqt8Y6b\", \"object\": \"event\", \"api_version\": \"2018-01-23\", \"created\": 1518772341, \"data\": { \"object\": { \"id\": \"in_1Bw5GSCtiZ7cOVF6hbilNex4\", \"object\": \"invoice\", \"amount_due\": 1500, \"application_fee\": null, \"attempt_count\": 0, \"attempted\": true, \"billing\": \"charge_automatically\", \"charge\": \"ch_1Bw5GSCtiZ7cOVF65TPIR0iZ\", \"closed\": true, \"currency\": \"aud\", \"customer\": \"cus_CKkm9ANE0Q4bWD\", \"date\": 1518772340, \"description\": null, \"discount\": null, \"due_date\": null, \"ending_balance\": 0, \"forgiven\": false, \"lines\": { \"object\": \"list\", \"data\": [ { \"id\": \"sub_CKkm6mS5ABycwK\", \"object\": \"line_item\", \"amount\": 1500, \"currency\": \"aud\", \"description\": \"1 × DBG Weekly (at $15.00 / week)\", \"discountable\": true, \"livemode\": false, \"metadata\": { }, \"period\": { \"start\": 1518772340, \"end\": 1519377140 }, \"plan\": { \"id\": \"Weekly1\", \"object\": \"plan\", \"amount\": 1500, \"created\": 1518579072, \"currency\": \"aud\", \"interval\": \"week\", \"interval_count\": 1, \"livemode\": false, \"metadata\": { }, \"nickname\": null, \"product\": \"prod_CJuonKqEmeoKeB\", \"trial_period_days\": null, \"statement_descriptor\": \"S d\", \"name\": \"DBG Weekly\" }, \"proration\": false, \"quantity\": 1, \"subscription\": null, \"subscription_item\": \"si_CKkmb03giTUHzF\", \"type\": \"subscription\" } ], \"has_more\": false, \"total_count\": 1, \"url\": \"/v1/invoices/in_1Bw5GSCtiZ7cOVF6hbilNex4/lines\" }, \"livemode\": false, \"metadata\": { }, \"next_payment_attempt\": null, \"number\": \"e723441f36-0001\", \"paid\": true, \"period_end\": 1518772340, \"period_start\": 1518772340, \"receipt_number\": null, \"starting_balance\": 0, \"statement_descriptor\": null, \"subscription\": \"sub_CKkm6mS5ABycwK\", \"subtotal\": 1500, \"tax\": null, \"tax_percent\": null, \"total\": 1500, \"webhooks_delivered_at\": null } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_DJi8ixXNZ3Mdyx\", \"idempotency_key\": null }, \"type\": \"invoice.created\" }";


//"invoice.payment_succeeded"
//string json = "{ \"id\": \"evt_1Bw5GTCtiZ7cOVF6yc14ga6y\", \"object\": \"event\", \"api_version\": \"2018-01-23\", \"created\": 1518772341, \"data\": { \"object\": { \"id\": \"in_1Bw5GSCtiZ7cOVF6hbilNex4\", \"object\": \"invoice\", \"amount_due\": 1500, \"application_fee\": null, \"attempt_count\": 0, \"attempted\": true, \"billing\": \"charge_automatically\", \"charge\": \"ch_1Bw5GSCtiZ7cOVF65TPIR0iZ\", \"closed\": true, \"currency\": \"aud\", \"customer\": \"cus_CKkm9ANE0Q4bWD\", \"date\": 1518772340, \"description\": null, \"discount\": null, \"due_date\": null, \"ending_balance\": 0, \"forgiven\": false, \"lines\": { \"object\": \"list\", \"data\": [ { \"id\": \"sub_CKkm6mS5ABycwK\", \"object\": \"line_item\", \"amount\": 1500, \"currency\": \"aud\", \"description\": \"1 × DBG Weekly (at $15.00 / week)\", \"discountable\": true, \"livemode\": false, \"metadata\": { }, \"period\": { \"start\": 1518772340, \"end\": 1519377140 }, \"plan\": { \"id\": \"Weekly1\", \"object\": \"plan\", \"amount\": 1500, \"created\": 1518579072, \"currency\": \"aud\", \"interval\": \"week\", \"interval_count\": 1, \"livemode\": false, \"metadata\": { }, \"nickname\": null, \"product\": \"prod_CJuonKqEmeoKeB\", \"trial_period_days\": null, \"statement_descriptor\": \"S d\", \"name\": \"DBG Weekly\" }, \"proration\": false, \"quantity\": 1, \"subscription\": null, \"subscription_item\": \"si_CKkmb03giTUHzF\", \"type\": \"subscription\" } ], \"has_more\": false, \"total_count\": 1, \"url\": \"/v1/invoices/in_1Bw5GSCtiZ7cOVF6hbilNex4/lines\" }, \"livemode\": false, \"metadata\": { }, \"next_payment_attempt\": null, \"number\": \"e723441f36-0001\", \"paid\": true, \"period_end\": 1518772340, \"period_start\": 1518772340, \"receipt_number\": null, \"starting_balance\": 0, \"statement_descriptor\": null, \"subscription\": \"sub_CKkm6mS5ABycwK\", \"subtotal\": 1500, \"tax\": null, \"tax_percent\": null, \"total\": 1500, \"webhooks_delivered_at\": null } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_DJi8ixXNZ3Mdyx\", \"idempotency_key\": null }, \"type\": \"invoice.payment_succeeded\" }";

//customer.created
//string json = "{ \"id\": \"evt_1Bw5GSCtiZ7cOVF6v4NjzD6M\", \"object\": \"event\", \"api_version\": \"2018-01-23\", \"created\": 1518772340, \"data\": { \"object\": { \"id\": \"cus_CKkm9ANE0Q4bWD\", \"object\": \"customer\", \"account_balance\": 0, \"created\": 1518772340, \"currency\": null, \"default_source\": null, \"delinquent\": false, \"description\": null, \"discount\": null, \"email\": \"a3@a.com\", \"invoice_prefix\": \"e723441f36\", \"livemode\": false, \"metadata\": { }, \"shipping\": null, \"sources\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_CKkm9ANE0Q4bWD/sources\" }, \"subscriptions\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_CKkm9ANE0Q4bWD/subscriptions\" } } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_ZYmFeq9r0yTRtb\", \"idempotency_key\": null }, \"type\": \"customer.created\" }";

//StripeEvent stripeEvent = null;
//stripeEvent = StripeEventUtility.ParseEvent(json);

//try
//{
//    switch (stripeEvent.Type)
//    {
//        //case "charge.succeeded":
//        //    // do work
//        //    StripeCharge sCharge = Mapper<StripeCharge>.MapFromJson(stripeEvent.Data.Object.ToString());

//        //    break;
//        case "invoice.created":
//            // do work
//            StripeInvoice sInvoiceC = Mapper<StripeInvoice>.MapFromJson(stripeEvent.Data.Object.ToString());

//            break;
//        case "customer.created":
//            StripeCustomer sCustomer = Mapper<StripeCustomer>.MapFromJson(stripeEvent.Data.Object.ToString());
//            if (sCustomer != null)
//            {

//            }
//            // do work
//            break;
//        case "invoice.payment_succeeded":
//            StripeInvoice sInvoicePS = Mapper<StripeInvoice>.MapFromJson(stripeEvent.Data.Object.ToString());
//            if (sInvoicePS != null)
//            {

//            }
//            // do work
//            break;
//        case "customer.subscription.updated":
//        case "customer.subscription.deleted":
//        case "customer.subscription.created":
//            // do work
//            break;
//    }
//}
//catch (Exception ex)
//{
//    string strErrorE = "";
//    //Common.SendSingleEmail("r_mohsin@yahoo.com", "Stripe Error--" + ex.Message, ex.StackTrace, ref strErrorE);
//}



// Application_Start
//
//try
//{
//    var secretKey = WebConfigurationManager.AppSettings["StripeSecretKey"];
//    StripeConfiguration.SetApiKey(secretKey);


//    var options = new StripePlanCreateOptions
//    {
//        Currency = "aud",
//        Interval = "month",
//        Name = "Monthly10",
//        Amount = 11,
//    };
//    var service = new StripePlanService();
//    StripePlan plan = service.Create(options);

//}
//catch
//{
//    //
//}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ErrorRedirect :SecurePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["ErrorLogID"] != null)
            {
                ErrorLog theErrorLog = SystemData.ErrorLog_Details(int.Parse(Cryptography.Decrypt(Request.QueryString["ErrorLogID"].ToString())));
                if(theErrorLog!=null)
                {
                    lblErrorMessage.Text = theErrorLog.ErrorMessage;
                    lblErrorStack.Text = theErrorLog.ErrorTrack;
                    lblErrorPath.Text = theErrorLog.Path;
                }
            }
        }
        catch
        {
            //
        }
        

        //if (Session["LoginAccount"] == null)
        //{
        //    Response.Redirect("~/Login.aspx?ErrorLogID=" + Request.QueryString["ErrorLogID"].ToString(),false);
        //}
        //else
        //{

        //    Response.Redirect("~/Login.aspx?ErrorLogID=" + Request.QueryString["ErrorLogID"].ToString() + "&" + Session["LoginAccount"].ToString(),false);
        //}
    }
}
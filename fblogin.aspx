﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/Marketing.master" AutoEventWireup="true" CodeFile="fblogin.aspx.cs" Inherits="fblogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" Runat="Server">


    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '1352642498228488',
                cookie: true,
                xfbml: true,
                version: 'v5.0'
            });

            FB.AppEvents.logPageView();
            FB.Event.subscribe('auth.statusChange', OnLogin);
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


        function checkLoginState() {
            FB.getLoginStatus(function (response) {
                statusChangeCallback(response);
            });
        }

        function OnLogin(response) {
            if (response.authResponse) {
                FB.api('/me?fields=id,name,gender,email,birthday', LoadValues);
            }
        }

        //This method will load the values to the labels
        function LoadValues(me) {
            if (me.name) {
                document.getElementById('displayname').innerHTML = me.name;
                document.getElementById('FBId').innerHTML = me.id;
                document.getElementById('DisplayEmail').innerHTML = me.email;
                document.getElementById('Gender').innerHTML = me.gender;
                document.getElementById('DOB').innerHTML = me.birthday;
                document.getElementById('auth-loggedin').style.display = 'block';
            }
        }

       
           

</script>


    <fb:login-button 
  scope="public_profile,email"
  onlogin="checkLoginState();">
</fb:login-button>

    <div id="auth-status">
                <div id="auth-loggedin" style="display: none">
                    Hi, <span id="displayname"></span><br />
                    Your Facebook ID : <span id="FBId"></span><br />
                    Your Email : <span id="DisplayEmail"></span><br />
                    Your Sex:, <span id="Gender"></span><br />
                    Your Date of Birth :, <span id="DOB"></span><br />
                </div>
            </div>
    <%--<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>--%>
    
    <%--<script type="text/javascript">
        window.fbAsyncInit = function () {
            FB.init({
                appId: '1352642498228488',
                xfbml: true,
                version: 'v5.0'
            });
            FB.AppEvents.logPageView();
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>--%>

<%--
     <script type="text/javascript">
         $("document").ready(function () {
             // Initialize the SDK upon load
             FB.init({
                 appId: '1352642498228488', // App ID
                 //channelUrl: '//' + window.location.hostname + '/channel', // Path to your Channel File
                 scope: 'id,name,gender,user_birthday,email', // This to get the user details back from Facebook
                 status: true, // check login status
                 cookie: true, // enable cookies to allow the server to access the session
                 xfbml: true  // parse XFBML
             });
             // listen for and handle auth.statusChange events
             FB.Event.subscribe('auth.statusChange', OnLogin);
         });

         // This method will be called after the user login into facebook.
         function OnLogin(response) {
             if (response.authResponse) {
                 FB.api('/me?fields=id,name,gender,email,birthday', LoadValues);
             }
         }

         //This method will load the values to the labels
         function LoadValues(me) {
             if (me.name) {
                 document.getElementById('displayname').innerHTML = me.name;
                 document.getElementById('FBId').innerHTML = me.id;
                 document.getElementById('DisplayEmail').innerHTML = me.email;
                 document.getElementById('Gender').innerHTML = me.gender;
                 document.getElementById('DOB').innerHTML = me.birthday;
                 document.getElementById('auth-loggedin').style.display = 'block';
             }
         }
    </script>--%>


</asp:Content>


﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LateDataSnooze.aspx.cs" Inherits="LateDataSnooze" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label runat="server" ID="lblMessage" Font-Size="Large" ForeColor="Green"></asp:Label>
        <br />
        <asp:HyperLink runat="server" NavigateUrl="~/Default.aspx">Back to the site</asp:HyperLink>
    </div>
    </form>
</body>
</html>

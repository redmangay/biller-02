﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
public partial class QuoteAccept : System.Web.UI.Page
{



    public void SendSignUpEmail(string strEmail, string strFirstName,int iAccountID)
    {
        try
        {
            string strEmailFrom = SystemData.SystemOption_ValueByKey_Account("EmailFrom", null, null);
            string strEmailServer = SystemData.SystemOption_ValueByKey_Account("EmailServer", null, null);
            string strEmailUserName = SystemData.SystemOption_ValueByKey_Account("EmailUserName", null, null);
            string strEmailPassword = SystemData.SystemOption_ValueByKey_Account("EmailPassword", null, null);

            //send email to user
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(strEmailFrom);

            //this might be different content for eContractor account
            Content theContent2 = SystemData.Content_Details_ByKey("eContractor Sign Up email", null);


            msg.Subject = theContent2.Heading;
            msg.IsBodyHtml = true;


            theContent2.ContentP = theContent2.ContentP.Replace("[First Name]", strFirstName);
            theContent2.ContentP = theContent2.ContentP.Replace("[Email]", strEmail);
            //theContent2.ContentP = theContent2.ContentP.Replace("[Password]", strPassword);

            Account theAccount = SecurityManager.Account_Details(iAccountID);
            if (theAccount != null)
            {
                string strConfirmEmailURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                    "/Email.aspx?key=" + Cryptography.Encrypt(theAccount.AccountID.ToString()) +
                    "&key2=" + Cryptography.Encrypt(theAccount.DateUpdated.ToString());
                theContent2.ContentP = theContent2.ContentP.Replace("[ConfirmMyEmai]", strConfirmEmailURL);
            }


            msg.Body = theContent2.ContentP;// Sb.ToString();

            SmtpClient smtpClient2 = new SmtpClient(strEmailServer);
            smtpClient2.Timeout = 99999;
            smtpClient2.Credentials = new System.Net.NetworkCredential(strEmailUserName, strEmailPassword);
            smtpClient2.Port = DBGurus.StringToInt(SystemData.SystemOption_ValueByKey_Account("SmtpPort", null, null));
            smtpClient2.EnableSsl = Convert.ToBoolean(SystemData.SystemOption_ValueByKey_Account("EnableSSL", null, null));

            msg.To.Add(strEmail);

            msg.Bcc.Add("info@dbgurus.com.au");
            smtpClient2.Send(msg);

        }
        catch
        {
            //
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        string strMessage = string.Empty;
        try
        {
            
            if(Request.QueryString["senttime"] != null)
            {
                try
                {
                    string sSentTime = Cryptography.Decrypt(Request.QueryString["senttime"].ToString());
                    DateTime dSentTime = DateTime.Parse(sSentTime);
                    if( dSentTime.AddHours(8)<DateTime.Now)
                    {
                        lblMessage.Text = "You can not accept or decline after 8 hours.";
                        lblMessage.ForeColor= System.Drawing.Color.Red;
                        return;
                    }
                }
                catch
                {
                    lblMessage.Text = "Invalid request.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    return;
                }
            }
            else
            {
                lblMessage.Text = "Invalid request.";
                lblMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }




            if (Request.QueryString["status"]!=null 
                && Request.QueryString["ID"] != null && Request.QueryString["RecordID"] != null && Request.QueryString["Email"] != null)
            {
                string sSenderAccountID = Cryptography.Decrypt(Request.QueryString["ID"].ToString());
                string sSenderQuoteRecordID = Cryptography.Decrypt(Request.QueryString["RecordID"].ToString());
                string sReceiverEmail = Cryptography.Decrypt(Request.QueryString["Email"].ToString());

                Record theSenderQuoteRecord = RecordManager.ets_Record_Detail_Full(int.Parse(sSenderQuoteRecordID), null, false);


                if (theSenderQuoteRecord != null && !string.IsNullOrEmpty(theSenderQuoteRecord.V015))
                {
                    if (theSenderQuoteRecord.V015.ToLower() == "a")
                    {
                        lblMessage.Text = "This quote has already been accepted.";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                    if (theSenderQuoteRecord.V015.ToLower() == "d")
                    {
                        lblMessage.Text = "This quote has already been declined.";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                    if (theSenderQuoteRecord.V015.ToLower() == "e")
                    {
                        lblMessage.Text = "This quote has been expired.";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                }





                User userReceiver = SecurityManager.User_ByEmail(sReceiverEmail);
                User userSender = SecurityManager.User_Details((int)theSenderQuoteRecord.EnteredBy);
                if (Request.QueryString["status"].ToString().ToLower() == "decline")
                {
                    theSenderQuoteRecord.V015 = "D";
                    RecordManager.ets_Record_Update(theSenderQuoteRecord,false);
                    try
                    {
                        Content theAcceptDeclineEmail = SystemData.Content_Details_ByKey("OnTask_QuoteDecline", null);
                        Record theSenderTaskRecord = RecordManager.ets_Record_Detail_Full(int.Parse(theSenderQuoteRecord.V002), null, false);

                        if (theAcceptDeclineEmail!=null && theSenderQuoteRecord!=null)
                        {
                            Record theClientRecord = new Record();
                            if (!string.IsNullOrEmpty(theSenderTaskRecord.V001))
                            {
                                theClientRecord = RecordManager.ets_Record_Detail_Full(int.Parse(theSenderTaskRecord.V001), null, false);
                            }
                            theAcceptDeclineEmail.Heading = theAcceptDeclineEmail.Heading.Replace("[TaskName]", theSenderTaskRecord.V002);
                            theAcceptDeclineEmail.Heading = theAcceptDeclineEmail.Heading.Replace("[Receiver Email]", sReceiverEmail);                            
                            theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[TaskName]", theSenderTaskRecord.V002);
                            if (theSenderTaskRecord.V001 != "")
                            {
                                string strClientName = Common.GetValueFromSQL("SELECT V001 FROM [Record] WHERE RecordID=" + theSenderTaskRecord.V001);
                                theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[ClientName]", strClientName);
                            }

                            if (theSenderTaskRecord.V004 != "")
                            {
                                LocationColumn theLocationColumn = DocGen.DAL.JSONField.GetTypedObject<LocationColumn>(theSenderTaskRecord.V004);
                                if (theLocationColumn != null)
                                {
                                    if (!string.IsNullOrEmpty(theLocationColumn.Address))
                                    {
                                        theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[TaskAddress]", theLocationColumn.Address);
                                    }
                                }
                            }
                            else
                            {
                                theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[TaskAddress]", "");
                            }


                           
                            Message theMessage = new Message(null, theSenderQuoteRecord.RecordID, theSenderQuoteRecord.TableID, int.Parse(sSenderAccountID), DateTime.Now, "E", "E", null,
                            userSender.Email, theAcceptDeclineEmail.Heading, theAcceptDeclineEmail.ContentP, null, "");


                            string sSendEmailError = "";
                                                        
                            DBGurus.SendEmail(theAcceptDeclineEmail.ContentKey, true, null, theAcceptDeclineEmail.Heading, theAcceptDeclineEmail.ContentP
                                , "", userSender.Email, "", "", null, theMessage, out sSendEmailError);


                        }
                    }
                    catch
                    {
                        //if there is any problem sending email
                    }
                    lblMessage.Text = "You have declined this quote.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    return;
                }


               

                

                if(userReceiver==null)
                {
                    //create a new account for the receiver                    

                    string sReceiver_AccountName = sReceiverEmail.Substring(0, sReceiverEmail.IndexOf("@"));
                    
                    string sRandomPassword = System.Web.Security.Membership.GeneratePassword(8, 1);
                    string strReceiver_FirstName = sReceiver_AccountName;
                    string strReceiver_LastName = "MR";
                    
                    string sReceiver_UserID = SecurityManager.CommonSignUp(sReceiver_AccountName, 2,
             sReceiverEmail, sRandomPassword, strReceiver_FirstName, strReceiver_LastName, "");

                    userReceiver = SecurityManager.User_Details(int.Parse(sReceiver_UserID));
                    //get the new account's ID
                    int? iReceiver_AccountID = SecurityManager.GetPrimaryAccountID(int.Parse(sReceiver_UserID));
                    string strTemplateAccountID = SystemData.SystemOption_ValueByKey_Account("TemplateAccountID", null, null).ToLower();

                    //Call the SP eContractor_Copy_Account
                    RecordManager.eContractor_Copy_Account(int.Parse(strTemplateAccountID), (int)iReceiver_AccountID, int.Parse(sReceiver_UserID));

                    Account theAccount = SecurityManager.Account_Details((int)iReceiver_AccountID);
                    theAccount.IsActive = false;//Wait for email validation
                    theAccount.Comment = theAccount.AccountID.ToString();
                    SecurityManager.Account_Update(theAccount);


                    SendSignUpEmail(sReceiverEmail, strReceiver_FirstName, (int)iReceiver_AccountID);

                    //string strReceiver_ContractorTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Contractor' AND AccountID="
                    //    + iReceiver_AccountID.ToString() + " AND IsActive=1");

                    //Record rReceiver_ContractorRecord = new Record();
                   
                    //rReceiver_ContractorRecord.TableID = int.Parse(strReceiver_ContractorTableID);
                    
                    //rReceiver_ContractorRecord.V001 = userReceiver.FirstName;
                    //rReceiver_ContractorRecord.V004 = userReceiver.Email;
                    //rReceiver_ContractorRecord.V005 = "True";
                    //rReceiver_ContractorRecord.V008 = sReceiver_UserID;
                    //rReceiver_ContractorRecord.EnteredBy = int.Parse(sReceiver_UserID);
                    //rReceiver_ContractorRecord.V011 = Guid.NewGuid().ToString();
                    //rReceiver_ContractorRecord.RecordID = RecordManager.ets_Record_Insert(rReceiver_ContractorRecord);
                }

                int? iReceiverAccountID = SecurityManager.GetPrimaryAccountID((int)userReceiver.UserID);

                if(iReceiverAccountID!=null)
                {
                    string strSenderTaskTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Task' and AccountID=" + sSenderAccountID);
                    
                    Record theSenderTaskRecord=new Record();
                    Record theSenderClientRecord = new Record();
                    //Record theSenderContractorRecord = new Record();

                    //if (!string.IsNullOrEmpty(theSenderQuoteRecord.V012))
                    //{
                    //    theSenderContractorRecord = RecordManager.ets_Record_Detail_Full(int.Parse(theSenderQuoteRecord.V012), null, false);
                    //}

                    if (!string.IsNullOrEmpty(theSenderQuoteRecord.V013))
                    {
                        theSenderClientRecord=RecordManager.ets_Record_Detail_Full(int.Parse(theSenderQuoteRecord.V013), null, false);
                    }

                   

                    if (string.IsNullOrEmpty(theSenderQuoteRecord.V002) || Common.isNumeric(theSenderQuoteRecord.V002)==false)
                    {
                        //create the task record
                        theSenderTaskRecord.TableID = int.Parse(strSenderTaskTableID);
                        theSenderTaskRecord.IsActive = true;
                        theSenderTaskRecord.EnteredBy = theSenderQuoteRecord.EnteredBy;
                        theSenderTaskRecord.V001 = theSenderQuoteRecord.V013;//client
                        theSenderTaskRecord.V002 = theSenderQuoteRecord.V011;//Task name

                        theSenderTaskRecord.V004 = theSenderClientRecord.V006;//Address
                        theSenderTaskRecord.V007 = theSenderClientRecord.V002;//client's email
                        theSenderTaskRecord.V008 = theSenderClientRecord.V005;//client mobile

                        theSenderTaskRecord.V009 = "Quote accepted at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm");

                        theSenderTaskRecord.V011 = "0";//status
                        theSenderTaskRecord.V016 = Guid.NewGuid().ToString() ;//Local ID
                        theSenderTaskRecord.V020 = theSenderQuoteRecord.V012;// theSenderContractorRecord.RecordID.ToString();//
                        theSenderTaskRecord.V003 = DateTime.Now.ToString("dd/MM/yyyy hh:mm");//
                        theSenderTaskRecord.V017 = DateTime.Now.AddHours(1).ToString("dd/MM/yyyy hh:mm");//start
                        theSenderTaskRecord.V018 = DateTime.Now.AddHours(2).ToString("dd/MM/yyyy hh:mm");//end
                       
                        theSenderTaskRecord.RecordID = RecordManager.ets_Record_Insert(theSenderTaskRecord);
                        theSenderQuoteRecord.V002 = theSenderTaskRecord.RecordID.ToString();
                      
                    }
                    else
                    {
                        theSenderTaskRecord = RecordManager.ets_Record_Detail_Full(int.Parse(theSenderQuoteRecord.V002), null, false);

                        //theSenderTaskRecord.TableID = int.Parse(strSenderTaskTableID);
                        //RecordManager.ets_Record_Update(theSenderTaskRecord, false);
                    }

                    theSenderQuoteRecord.V015 = "A";//status
                    RecordManager.ets_Record_Update(theSenderQuoteRecord, false);


                    //string strReceiverContractorRecordID = Common.GetValueFromSQL("SELECT R.RecordID FROM [Record] R JOIN [Table] T ON R.TableID=T.TableID WHERE R.V008='"
                    //    + userReceiver.UserID.ToString()+"' AND T.AccountID=" + iReceiverAccountID.ToString());


                    Record theTaskRecord = new Record();
                    if((int)iReceiverAccountID==int.Parse(sSenderAccountID))
                    {
                        //same account
                        theSenderTaskRecord.V009 =  theSenderTaskRecord.V009 + "Quote accepted at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm") + ".";
                        if(userReceiver!=null)
                            theSenderTaskRecord.V020 = userReceiver.UserID.ToString();// strReceiverContractorRecordID;//change the task contractor;

                        RecordManager.ets_Record_Update(theSenderTaskRecord, true);
                        theTaskRecord = theSenderTaskRecord;
                    }
                    else
                    {
                        //different account
                        string strReceiverTaskTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Task' and AccountID=" + iReceiverAccountID.ToString());
                        string strReceiverClientTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Client' and AccountID=" + iReceiverAccountID.ToString());

                        Record theReceiverTaskRecord = new Record();
                        theReceiverTaskRecord.TableID = int.Parse(strReceiverTaskTableID);

                        theReceiverTaskRecord.IsActive = true;
                        theReceiverTaskRecord.EnteredBy = userReceiver.UserID;

                        theReceiverTaskRecord.V002 = theSenderQuoteRecord.V011;//Task name
                        Record theReceiverClientRecord = new Record();
                        if (theSenderClientRecord!=null)
                        {                            
                            string strReceiverClientRecordID = Common.GetValueFromSQL("SELECT RecordID FROM [Record] WHERE TableID="
                                + strReceiverClientTableID + " AND V001='" + theSenderClientRecord.V001.Replace("'","''")+ "'");
                            if(string.IsNullOrEmpty(strReceiverClientRecordID))
                            {
                                //create a new client to the new account
                                theReceiverClientRecord.TableID = int.Parse(strReceiverClientTableID);
                                theReceiverClientRecord.IsActive = true;
                                theReceiverClientRecord.EnteredBy = userReceiver.UserID;
                                theReceiverClientRecord.V001 = theSenderClientRecord.V001;
                                theReceiverClientRecord.V002 = theSenderClientRecord.V002;
                                theReceiverClientRecord.V003 = theSenderClientRecord.V003;
                                theReceiverClientRecord.V004 = theSenderClientRecord.V004;
                                theReceiverClientRecord.V005 = theSenderClientRecord.V005;
                                theReceiverClientRecord.V006 = theSenderClientRecord.V006;
                                theReceiverClientRecord.RecordID = RecordManager.ets_Record_Insert(theReceiverClientRecord);

                            }
                            else
                            {
                                //found this client
                                theReceiverClientRecord = RecordManager.ets_Record_Detail_Full(int.Parse(strReceiverClientRecordID), null, false);
                            }
                        }


                        theReceiverTaskRecord.V001 = theReceiverClientRecord.RecordID.ToString();//client
                        theReceiverTaskRecord.V002 = theSenderTaskRecord.V002;
                        theReceiverTaskRecord.V003 = DateTime.Now.ToString("dd/MM/yyyy hh:mm");//

                        theReceiverTaskRecord.V004 = theSenderTaskRecord.V004;// theReceiverClientRecord.V006;//Address
                        theReceiverTaskRecord.V005 = theSenderTaskRecord.V005;
                        theReceiverTaskRecord.V006 = theSenderTaskRecord.V006;



                        theReceiverTaskRecord.V007 = theSenderTaskRecord.V007;// theReceiverClientRecord.V002;//client's email
                        theReceiverTaskRecord.V008 = theSenderTaskRecord.V008;// theReceiverClientRecord.V005;//client mobile

                        theReceiverTaskRecord.V009 = theSenderTaskRecord.V009; //"Quote accepted at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm");

                        theReceiverTaskRecord.V011 = "0";//status
                        theReceiverTaskRecord.V016 = Guid.NewGuid().ToString();//Local ID                      

                        theReceiverTaskRecord.V017 = theSenderTaskRecord.V017;// DateTime.Now.AddHours(1).ToString("dd/MM/yyyy hh:mm");//start
                        theReceiverTaskRecord.V018 = theSenderTaskRecord.V018;// DateTime.Now.AddHours(2).ToString("dd/MM/yyyy hh:mm");//end
                        theReceiverTaskRecord.V019 = theSenderTaskRecord.V019;
                        if (userReceiver != null)
                            theReceiverTaskRecord.V020 = userReceiver.UserID.ToString();// strReceiverContractorRecordID;//

                        theReceiverTaskRecord.V021 = theSenderTaskRecord.V021;
                        theReceiverTaskRecord.V022 = theSenderTaskRecord.V022;
                        theReceiverTaskRecord.V025 = theSenderTaskRecord.V025;
                        theReceiverTaskRecord.V028 = theSenderTaskRecord.V028;
                        theReceiverTaskRecord.V029 = theSenderTaskRecord.V029;
                        theReceiverTaskRecord.V030 = theSenderTaskRecord.V030;
                        theReceiverTaskRecord.V031 = theSenderTaskRecord.V031;
                        theReceiverTaskRecord.V032 = theSenderTaskRecord.V032;
                        theReceiverTaskRecord.V033 = theSenderTaskRecord.V033;
                        theReceiverTaskRecord.V034 = theSenderTaskRecord.V034;
                        theReceiverTaskRecord.V035 = theSenderTaskRecord.V035;
                        theReceiverTaskRecord.V036 = theSenderTaskRecord.V036;
                        theReceiverTaskRecord.V037 = theSenderTaskRecord.RecordID.ToString();

                        theReceiverTaskRecord.RecordID = RecordManager.ets_Record_Insert(theReceiverTaskRecord);
                        theTaskRecord = theReceiverTaskRecord;
                    }

                    if(theTaskRecord!=null)
                    {
                        strMessage = "Thanks. You have accepted this quote.";
                        Content theAcceptDeclineEmail = SystemData.Content_Details_ByKey("OnTask_QuoteAccept", null);
                        Record theClientRecord = new Record();
                        if(!string.IsNullOrEmpty(theTaskRecord.V001))
                        {
                            theClientRecord = RecordManager.ets_Record_Detail_Full(int.Parse(theTaskRecord.V001), null, false);
                        }
                        theAcceptDeclineEmail.Heading = theAcceptDeclineEmail.Heading.Replace("[TaskName]", theTaskRecord.V002);
                        theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[TaskName]", theTaskRecord.V002);

                        if (theTaskRecord.V001 != "")
                        {
                            string strClientName = Common.GetValueFromSQL("SELECT V001 FROM [Record] WHERE RecordID=" + theTaskRecord.V001);
                            theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[ClientName]", strClientName);
                        }

                        if (theTaskRecord.V004 != "")
                        {
                            LocationColumn theLocationColumn = DocGen.DAL.JSONField.GetTypedObject<LocationColumn>(theTaskRecord.V004);
                            if (theLocationColumn != null)
                            {
                                if (!string.IsNullOrEmpty(theLocationColumn.Address))
                                {
                                    theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[TaskAddress]", theLocationColumn.Address);
                                }
                            }
                        }
                        else
                        {
                            theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[TaskAddress]", "");
                        }

                        if (theTaskRecord.V020 != "")
                        {
                            User taskUser = SecurityManager.User_Details(int.Parse(theTaskRecord.V020));

                            if(taskUser!=null)
                            {
                                //string strContractorName = Common.GetValueFromSQL("SELECT V001 FROM [Record] WHERE RecordID=" + theTaskRecord.V020);
                                theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[Contractor Name]", taskUser.FirstName + " " + taskUser.LastName);
                            }
                           
                        }

                        Message theMessage = new Message(null, theSenderQuoteRecord.RecordID,theSenderQuoteRecord.TableID, int.Parse(sSenderAccountID), DateTime.Now, "E", "E", null,
                            userSender.Email, theAcceptDeclineEmail.Heading, theAcceptDeclineEmail.ContentP, null, "");
                       

                        string sSendEmailError = "";

                        //theTaskRecord.V007 = theClientRecord.V002;//Update Email of task
                        //theTaskRecord.V008 = theClientRecord.V005;//Update mobile
                        //RecordManager.ets_Record_Update(theTaskRecord, true);

                        DBGurus.SendEmail(theAcceptDeclineEmail.ContentKey, true, null, theAcceptDeclineEmail.Heading, theAcceptDeclineEmail.ContentP
                            , "",sReceiverEmail, userSender.Email, "", null, theMessage, out sSendEmailError);
                    }

                }
                else
                {
                    strMessage = "Please sign up";
                }               
                
            }
            else
            {
                strMessage = "Sorry that is an invalid response";
            }
        }
        catch(Exception ex)
        {
            strMessage = "Sorry that is an invalid response";
        }
        lblMessage.Text = strMessage;
    }


    public int CreateTask(int accountId, string recordId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("OnTask_QuoteTask_Create", connection))
            {

                command.CommandType = CommandType.StoredProcedure;

                SqlParameter pRV = new SqlParameter("@nNewID", SqlDbType.Int);
                pRV.Direction = ParameterDirection.Output;

                command.Parameters.Add(pRV);
                command.Parameters.Add(new SqlParameter("@nAccountID", accountId));
                command.Parameters.Add(new SqlParameter("@nRecordID", recordId));
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    connection.Close();
                    connection.Dispose();
                    return int.Parse(pRV.Value.ToString());
                }
                catch
                {
                    connection.Close();
                    connection.Dispose();

                }
                return -1;

            }
        }
    }
}



     // MR deleted 15-Apr-2019   
    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    string strMessage = string.Empty;
    //    try
    //    {
    //        if (Request.QueryString["ID"] != null && Request.QueryString["RecordID"] != null && Request.QueryString["Email"] != null)
    //        {
    //            string sID = Cryptography.Decrypt(Request.QueryString["ID"].ToString());
    //            string sRecordID = Cryptography.Decrypt(Request.QueryString["RecordID"].ToString());
    //            string sEmail = Cryptography.Decrypt(Request.QueryString["Email"].ToString());
    //            int result = CreateTask(int.Parse(sID), sRecordID);
    //            if (result == -2)
    //            {
    //                strMessage = "Sorry that is an invalid response";
    //            }
    //            else
    //            {
    //                strMessage = "Thanks. You have accepted this quote.";
    //                Content theAcceptDeclineEmail = SystemData.Content_Details_ByKey("OnTask_QuoteAccept", null);
    //                Record theQuoteRecord = RecordManager.ets_Record_Detail_Full(int.Parse(sRecordID), null, false);
    //                Record theTaskRecord = RecordManager.ets_Record_Detail_Full((int)result, null, false);
    //                Record theClientRecord = RecordManager.ets_Record_Detail_Full(int.Parse(theQuoteRecord.V013), null, false);




    //                if (theTaskRecord != null)
    //                {
    //                    // theMessage.TableID = theTaskRecord.TableID;

    //                    theAcceptDeclineEmail.Heading = theAcceptDeclineEmail.Heading.Replace("[TaskName]", theTaskRecord.V002);
    //                    theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[TaskName]", theTaskRecord.V002);

    //                    if (theTaskRecord.V001 != "")
    //                    {
    //                        string strClientName = Common.GetValueFromSQL("SELECT V001 FROM [Record] WHERE RecordID=" + theTaskRecord.V001);
    //                        theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[ClientName]", strClientName);
    //                    }

    //                    if (theTaskRecord.V004 != "")
    //                    {
    //                        LocationColumn theLocationColumn = DocGen.DAL.JSONField.GetTypedObject<LocationColumn>(theTaskRecord.V004);
    //                        if (theLocationColumn != null)
    //                        {
    //                            if (!string.IsNullOrEmpty(theLocationColumn.Address))
    //                            {
    //                                theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[TaskAddress]", theLocationColumn.Address);
    //                            }
    //                        }
    //                    }
    //                    else
    //                    {
    //                        theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[TaskAddress]", "");
    //                    }

    //                    if(theTaskRecord.V020 != "")
    //                    {
                            
    //                        string strContractorName = Common.GetValueFromSQL("SELECT V001 FROM [Record] WHERE RecordID=" + theTaskRecord.V020);
    //                        theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[Contractor Name]", strContractorName);
    //                    }

    //                    Message theMessage = new Message(null, null, null, int.Parse(sID), DateTime.Now, "E", "E",null, "", "", "", null, "");
    //                    string sSendEmailError = "";

    //                    theTaskRecord.V007 = theClientRecord.V002;//Update Email of task
    //                    theTaskRecord.V008 = theClientRecord.V005;//Update mobile
    //                    RecordManager.ets_Record_Update(theTaskRecord, true);

    //                    DBGurus.SendEmail(theAcceptDeclineEmail.ContentKey, true, null, theAcceptDeclineEmail.Heading, theAcceptDeclineEmail.ContentP
    //                        , "", sEmail, "", "", null, theMessage, out sSendEmailError);
    //                }
    //            }
    //        }
    //        else
    //        {
    //            strMessage = "Sorry that is an invalid response";
    //        }
    //    }
    //    catch
    //    {
    //        strMessage = "Sorry that is an invalid response";
    //    }
    //    lblMessage.Text = strMessage;
    //}

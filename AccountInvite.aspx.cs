﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net.Mail;
public partial class AccountInviteP : System.Web.UI.Page
{
    string strMessage = "";

    public void SendSignUpEmail(string strEmail, string strFirstName, int iAccountID)
    {
        try
        {
            string strEmailFrom = SystemData.SystemOption_ValueByKey_Account("EmailFrom", null, null);
            string strEmailServer = SystemData.SystemOption_ValueByKey_Account("EmailServer", null, null);
            string strEmailUserName = SystemData.SystemOption_ValueByKey_Account("EmailUserName", null, null);
            string strEmailPassword = SystemData.SystemOption_ValueByKey_Account("EmailPassword", null, null);

            //send email to user
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(strEmailFrom);

            //this might be different content for eContractor account
            Content theContent2 = SystemData.Content_Details_ByKey("eContractor Sign Up email", null);


            msg.Subject = theContent2.Heading;
            msg.IsBodyHtml = true;


            theContent2.ContentP = theContent2.ContentP.Replace("[First Name]", strFirstName);
            theContent2.ContentP = theContent2.ContentP.Replace("[Email]", strEmail);
            //theContent2.ContentP = theContent2.ContentP.Replace("[Password]", strPassword);
            Account theAccount = SecurityManager.Account_Details(iAccountID);
            if (theAccount != null)
            {
                string strConfirmEmailURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath +
                    "/Email.aspx?key=" + Cryptography.Encrypt(theAccount.AccountID.ToString()) +
                    "&key2=" + Cryptography.Encrypt(theAccount.DateUpdated.ToString());
                theContent2.ContentP = theContent2.ContentP.Replace("[ConfirmMyEmai]", strConfirmEmailURL);
            }


            msg.Body = theContent2.ContentP;// Sb.ToString();

            SmtpClient smtpClient2 = new SmtpClient(strEmailServer);
            smtpClient2.Timeout = 99999;
            smtpClient2.Credentials = new System.Net.NetworkCredential(strEmailUserName, strEmailPassword);
            smtpClient2.Port = DBGurus.StringToInt(SystemData.SystemOption_ValueByKey_Account("SmtpPort", null, null));
            smtpClient2.EnableSsl = Convert.ToBoolean(SystemData.SystemOption_ValueByKey_Account("EnableSSL", null, null));

            msg.To.Add(strEmail);

            msg.Bcc.Add("info@dbgurus.com.au");
            smtpClient2.Send(msg);

        }
        catch
        {
            //
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string sResponse = Request.QueryString["Response"].ToString();
            if (Request.QueryString["ID"] != null && (sResponse == "Accept" || sResponse == "Decline"))
            {
                bool bAcceptNewuser = false;
                string sID = Cryptography.Decrypt(Request.QueryString["ID"].ToString());
                AccountInvite theAccountInvite = InvoiceManager.AccountInvite_Detail(int.Parse(sID));

                string strInvitedUserName = "";
                if (theAccountInvite != null )
                {
                    
                    if (theAccountInvite.IsActive != null && (bool)theAccountInvite.IsActive)
                    {
                        theAccountInvite.IsActive = false;
                        InvoiceManager.AccountInvite_Update(theAccountInvite);

                        Record rSender_TaskRecord = RecordManager.ets_Record_Detail_Full((int)theAccountInvite.TaskRecordID, null, false);



                        if(rSender_TaskRecord!=null &&  string.IsNullOrEmpty(rSender_TaskRecord.V026))
                        {
                            lblMessage.Text = "This invitation has been cancelled.";
                            return;
                        }

                        Content theAcceptDeclineEmail = null;
                        Content theTaskAcceptNewUserEmail = SystemData.Content_Details_ByKey("OnTask_TaskAcceptNewUser", null);
                        if (sResponse == "Accept")
                        {
                            theAcceptDeclineEmail = SystemData.Content_Details_ByKey("OnTask_TaskAccept", null);
                        }
                        else
                        {
                            theAcceptDeclineEmail = SystemData.Content_Details_ByKey("OnTask_TaskDecline", null);
                        }

                        Message theMessage = new Message(null, null, null, theAccountInvite.AccountID,
               DateTime.Now, "E", "E",
                   null, "", "", "", null, "");

                        //global
                        
                        Record rReceiver_TaskRecord = new Record();                      

                        //Record rReceiver_ContractorRecord = new Record();
                        string strSender_ClientName = "";
                        //int? iReceiver_ContractorTableId = null;
                        //int? iSender_ContractorTableId = null;

                        int? iReceiver_TaskTableId = null;
                        //int? iReceiver_ClientTableID = null;//may be not needed

                        //string sSender_ContractorTableId = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Contractor' AND AccountID="
                        //          + theAccountInvite.AccountID.ToString() + " AND IsActive=1");
                        //iSender_ContractorTableId = int.Parse(sSender_ContractorTableId);

                        if (rSender_TaskRecord != null)
                        {
                            theMessage.TableID = rSender_TaskRecord.TableID;

                            theAcceptDeclineEmail.Heading = theAcceptDeclineEmail.Heading.Replace("[TaskName]", rSender_TaskRecord.V002);
                            theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[TaskName]", rSender_TaskRecord.V002);
                            theTaskAcceptNewUserEmail.Heading = theTaskAcceptNewUserEmail.Heading.Replace("[TaskName]", rSender_TaskRecord.V002);
                            theTaskAcceptNewUserEmail.ContentP = theTaskAcceptNewUserEmail.ContentP.Replace("[TaskName]", rSender_TaskRecord.V002);

                            if (rSender_TaskRecord.V001 != "")
                            {
                                strSender_ClientName = Common.GetValueFromSQL("SELECT V001 FROM [Record] WHERE RecordID=" + rSender_TaskRecord.V001);
                                theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[ClientName]", strSender_ClientName);
                                theTaskAcceptNewUserEmail.ContentP = theTaskAcceptNewUserEmail.ContentP.Replace("[ClientName]", strSender_ClientName);
                            }
                            DateTime dtTest = DateTime.Today;
                            if (rSender_TaskRecord.V017 != "")
                            {
                                if (DateTime.TryParse(rSender_TaskRecord.V017, out dtTest))
                                {
                                    theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[StartDate]", dtTest.ToShortDateString());
                                    theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[StartTime]", dtTest.ToShortTimeString());

                                    theTaskAcceptNewUserEmail.ContentP = theTaskAcceptNewUserEmail.ContentP.Replace("[StartDate]", dtTest.ToShortDateString());
                                    theTaskAcceptNewUserEmail.ContentP = theTaskAcceptNewUserEmail.ContentP.Replace("[StartTime]", dtTest.ToShortTimeString());
                                }
                            }
                            if (rSender_TaskRecord.V018 != "")
                            {
                                if (DateTime.TryParse(rSender_TaskRecord.V018, out dtTest))
                                {
                                    theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[EndDate]", dtTest.ToShortDateString());
                                    theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[EndTime]", dtTest.ToShortTimeString());

                                    theTaskAcceptNewUserEmail.ContentP = theTaskAcceptNewUserEmail.ContentP.Replace("[EndDate]", dtTest.ToShortDateString());
                                    theTaskAcceptNewUserEmail.ContentP = theTaskAcceptNewUserEmail.ContentP.Replace("[EndTime]", dtTest.ToShortTimeString());
                                }
                            }
                            if (rSender_TaskRecord.V004 != "")
                            {
                                LocationColumn theLocationColumn = DocGen.DAL.JSONField.GetTypedObject<LocationColumn>(rSender_TaskRecord.V004);
                                if (theLocationColumn != null)
                                {
                                    if (!string.IsNullOrEmpty(theLocationColumn.Address))
                                    {
                                        theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[TaskAddress]", theLocationColumn.Address);
                                        theTaskAcceptNewUserEmail.ContentP = theTaskAcceptNewUserEmail.ContentP.Replace("[TaskAddress]", theLocationColumn.Address);
                                    }
                                }
                            }

                        }



                        if (theAcceptDeclineEmail != null)
                        {

                        }
                        User uReceiver_User = null;
                        User uSender_User = SecurityManager.User_Details((int)theAccountInvite.InviterUserID);
                        if (sResponse == "Accept")
                        {
                            theAccountInvite.InviteStatus = "A";
                            if (theAccountInvite.InvitedUserID == null)
                            {
                                //create a new account for the receiver
                                string sEmail = string.IsNullOrEmpty(theAccountInvite.Email) ? theAccountInvite.Name.Replace(" ", "") + "_temp@dbgurus.com.au" : theAccountInvite.Email;
                                theAccountInvite.Email = sEmail;
                                string sReceiver_AccountName = "";
                                if (!string.IsNullOrEmpty(theAccountInvite.Name))
                                {
                                    sReceiver_AccountName = theAccountInvite.Name.Replace(" ", "");
                                }
                                else
                                {
                                    sReceiver_AccountName = sEmail.Substring(0, sEmail.IndexOf("@"));
                                }
                                theAccountInvite.RandomPassword = System.Web.Security.Membership.GeneratePassword(8, 1);
                                string strReceiver_FirstName = "John";
                                string strReceiver_LastName = "Doe";
                                if (!string.IsNullOrEmpty(theAccountInvite.Name))
                                {
                                    if (theAccountInvite.Name.IndexOf(" ") > -1)
                                    {
                                        strReceiver_FirstName = theAccountInvite.Name.Substring(0, theAccountInvite.Name.IndexOf(" "));
                                        strReceiver_LastName = theAccountInvite.Name.Replace(strReceiver_FirstName + " ", "");
                                    }
                                    else
                                    {
                                        strReceiver_FirstName = theAccountInvite.Name;
                                    }

                                }


                                string sReceiver_UserID = SecurityManager.CommonSignUp(sReceiver_AccountName, 1,
                         theAccountInvite.Email, theAccountInvite.RandomPassword, strReceiver_FirstName, strReceiver_LastName, theAccountInvite.PhoneNumber);

                                //get the new account's ID
                                int? iReceiver_AccountID = SecurityManager.GetPrimaryAccountID(int.Parse(sReceiver_UserID));
                                string strTemplateAccountID = SystemData.SystemOption_ValueByKey_Account("TemplateAccountID", null, null).ToLower();

                                //Call the SP eContractor_Copy_Account
                                RecordManager.eContractor_Copy_Account(int.Parse(strTemplateAccountID), (int)iReceiver_AccountID, int.Parse(sReceiver_UserID));

                                Account theAccount = SecurityManager.Account_Details((int)iReceiver_AccountID);
                                theAccount.IsActive = false;//Wait for email validation
                                theAccount.Comment = theAccount.AccountID.ToString();
                                SecurityManager.Account_Update(theAccount);


                                SendSignUpEmail(theAccountInvite.Email, strReceiver_FirstName, (int)iReceiver_AccountID);

                                //string strReceiver_ContractorTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Contractor' AND AccountID="
                                //    + iReceiver_AccountID.ToString() + " AND IsActive=1");
                                //if (strReceiver_ContractorTableID != "")
                                //    iReceiver_ContractorTableId = int.Parse(strReceiver_ContractorTableID);

                                //rReceiver_ContractorRecord.TableID = (int)iReceiver_ContractorTableId;
                                //rReceiver_ContractorRecord.V001 = theAccountInvite.Name;
                                //rReceiver_ContractorRecord.V004 = theAccountInvite.Email;
                                //rReceiver_ContractorRecord.V005 = "True";
                                //rReceiver_ContractorRecord.V008 = sReceiver_UserID;
                                //rReceiver_ContractorRecord.EnteredBy = int.Parse(sReceiver_UserID);
                                //rReceiver_ContractorRecord.V011 = Guid.NewGuid().ToString();
                                //rReceiver_ContractorRecord.RecordID = RecordManager.ets_Record_Insert(rReceiver_ContractorRecord);

                                theAccountInvite.InvitedUserID = int.Parse(sReceiver_UserID);// rReceiver_ContractorRecord.EnteredBy;
                                theAccountInvite.InvitedAccountID = iReceiver_AccountID;
                                bAcceptNewuser = true;
                            }
                            else
                            {
                                if (theAccountInvite.InvitedAccountID == null)
                                {
                                    theAccountInvite.InvitedAccountID = SecurityManager.GetPrimaryAccountID((int)theAccountInvite.InvitedUserID);
                                }
                                if (theAccountInvite.AddAsTeamMember != null && (bool)theAccountInvite.AddAsTeamMember)
                                {
                                    if (theAccountInvite.AccountID != theAccountInvite.InvitedAccountID)
                                    {
                                        //Add this user into the account
                                       
                                        UserRole theUserRole = SecurityManager.GetUserRole((int)theAccountInvite.InvitedUserID, (int)theAccountInvite.AccountID);
                                        if (theUserRole == null)
                                        {
                                            DataTable dtRoles = Common.DataTableFromText("SELECT RoleID FROM [Role]  WHERE RoleType='4' AND AccountID=" + theAccountInvite.AccountID.ToString());
                                            if (dtRoles.Rows.Count > 0)
                                            {
                                                UserRole newUserRole = new UserRole(null, theAccountInvite.InvitedUserID, int.Parse(dtRoles.Rows[0][0].ToString()), null, null);
                                                newUserRole.AccountID = theAccountInvite.AccountID;
                                                newUserRole.IsPrimaryAccount = false;
                                                newUserRole.IsAccountHolder = false;
                                                int iUserRoleID = SecurityManager.UserRole_Insert(newUserRole);

                                            }
                                        }
                                    }
                                }
                            }


                            //if (iReceiver_ContractorTableId == null)
                            //{
                            //    string sTemp = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Contractor' AND AccountID="
                            //      + theAccountInvite.InvitedAccountID.ToString() + " AND IsActive=1");
                            //    if (!string.IsNullOrEmpty(sTemp))
                            //    {
                            //        iReceiver_ContractorTableId = int.Parse(sTemp);
                            //    }
                            //}
                            if (theAccountInvite.AddAsTeamMember != null && (bool)theAccountInvite.AddAsTeamMember)
                            {

                                //string sS_ReceiverContractorID = Common.GetValueFromSQL("SELECT TOP 1 RecordID FROM [Record]  WHERE  IsActive=1 AND TableID="+sSender_ContractorTableId
                                //    +" AND V008='"+theAccountInvite.InvitedUserID.ToString()+"'");

                                //if(string.IsNullOrEmpty(sS_ReceiverContractorID))
                                //{
                                //    //add this user as contractor
                                //    rReceiver_ContractorRecord.TableID = (int)iSender_ContractorTableId;
                                //    rReceiver_ContractorRecord.V001 = theAccountInvite.Name;
                                //    rReceiver_ContractorRecord.V004 = theAccountInvite.Email;
                                //    rReceiver_ContractorRecord.V005 = "True";
                                //    rReceiver_ContractorRecord.V008 = theAccountInvite.InvitedUserID.ToString();
                                //    rReceiver_ContractorRecord.EnteredBy = theAccountInvite.InvitedUserID;
                                //    rReceiver_ContractorRecord.V011 = Guid.NewGuid().ToString();
                                //    rReceiver_ContractorRecord.RecordID = RecordManager.ets_Record_Insert(rReceiver_ContractorRecord);
                                //    sS_ReceiverContractorID = rReceiver_ContractorRecord.RecordID.ToString();
                                //}


                                //rSender_TaskRecord.V020 = sS_ReceiverContractorID;
                                if(theAccountInvite.InvitedUserID!=null)
                                {
                                    rSender_TaskRecord.V020 = theAccountInvite.InvitedUserID.ToString();
                                    rSender_TaskRecord.V027 = theAccountInvite.InvitedUserID.ToString();
                                }
                                   

                                //rSender_TaskRecord.V027 = sS_ReceiverContractorID;//


                            }
                            else if (theAccountInvite.AccountID != theAccountInvite.InvitedAccountID)
                            {
                                //Insert

                                string sReceiver_TaskTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Task' AND AccountID="
                                 + theAccountInvite.InvitedAccountID.ToString() + " AND IsActive=1");

                                iReceiver_TaskTableId = int.Parse(sReceiver_TaskTableID);

                                string strReceiverClientTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Client' and AccountID=" + theAccountInvite.InvitedAccountID.ToString());

                                //string sReceiver_ContractorRecordID = Common.GetValueFromSQL("SELECT RecordID FROM [Record] WHERE TableID=" + iReceiver_ContractorTableId.ToString()
                                //    + " AND V008='" + theAccountInvite.InvitedUserID.ToString() + "' AND IsActive=1");



                                rReceiver_TaskRecord = RecordManager.ets_Record_Detail_Full((int)theAccountInvite.TaskRecordID, null, false);
                                rReceiver_TaskRecord.RecordID = null;
                                rReceiver_TaskRecord.TableID = iReceiver_TaskTableId;
                                //rReceiver_TaskRecord.V001 = "";//client, do we need to create client? 
                                Record theReceiverClientRecord = new Record();
                                if (!string.IsNullOrEmpty(rReceiver_TaskRecord.V001))
                                {
                                    Record theSenderClientRecord = RecordManager.ets_Record_Detail_Full(int.Parse(rReceiver_TaskRecord.V001), null, false);
                                    if(theSenderClientRecord!=null)
                                    {
                                        string strReceiverClientRecordID = Common.GetValueFromSQL("SELECT RecordID FROM [Record] WHERE TableID="
                                            + strReceiverClientTableID + " AND V001='" + theSenderClientRecord.V001.Replace("'", "''") + "'");
                                        if (string.IsNullOrEmpty(strReceiverClientRecordID))
                                        {
                                            //create a new client to the new account
                                            theReceiverClientRecord.TableID = int.Parse(strReceiverClientTableID);
                                            theReceiverClientRecord.IsActive = true;
                                            theReceiverClientRecord.EnteredBy = theAccountInvite.InvitedUserID;
                                            theReceiverClientRecord.V001 = theSenderClientRecord.V001;
                                            theReceiverClientRecord.V002 = theSenderClientRecord.V002;
                                            theReceiverClientRecord.V003 = theSenderClientRecord.V003;
                                            theReceiverClientRecord.V004 = theSenderClientRecord.V004;
                                            theReceiverClientRecord.V005 = theSenderClientRecord.V005;
                                            theReceiverClientRecord.V006 = theSenderClientRecord.V006;
                                            theReceiverClientRecord.V007 = theSenderClientRecord.V007;
                                            theReceiverClientRecord.RecordID = RecordManager.ets_Record_Insert(theReceiverClientRecord);

                                        }
                                        else
                                        {
                                            //found this client
                                            theReceiverClientRecord = RecordManager.ets_Record_Detail_Full(int.Parse(strReceiverClientRecordID), null, false);
                                        }

                                    }
                                }

                                if(theReceiverClientRecord!=null && theReceiverClientRecord.RecordID!=null)
                                {
                                    rReceiver_TaskRecord.V001 = theReceiverClientRecord.RecordID.ToString();
                                }

                                if (theAccountInvite.InvitedUserID != null)
                                    rReceiver_TaskRecord.V020 = theAccountInvite.InvitedUserID.ToString();// sReceiver_ContractorRecordID;

                                rReceiver_TaskRecord.EnteredBy = theAccountInvite.InvitedUserID;
                                rReceiver_TaskRecord.DateTimeRecorded = DateTime.Now;
                               
                                rReceiver_TaskRecord.V016 = Guid.NewGuid().ToString();
                                rReceiver_TaskRecord.V037 = rSender_TaskRecord.RecordID.ToString();
                                //rReceiver_TaskRecord.V009 = rReceiver_TaskRecord.V009 + " Accepted Date:" + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
                                rReceiver_TaskRecord.V026 = "";
                                rReceiver_TaskRecord.V027 = "";
                                rReceiver_TaskRecord.V030 = "";
                                rReceiver_TaskRecord.RecordID = RecordManager.ets_Record_Insert(rReceiver_TaskRecord);

                                //add task's child records

                                string sReceiver_ChecklistTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Checklist' AND AccountID="
                                     + theAccountInvite.InvitedAccountID.ToString() + " AND IsActive=1");

                                string sReceiver_ChecklistItemTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Checklist Item' AND AccountID="
                                     + theAccountInvite.InvitedAccountID.ToString() + " AND IsActive=1");

                                string sReceiver_AttachmentTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Attachment' AND AccountID="
                                     + theAccountInvite.InvitedAccountID.ToString() + " AND IsActive=1");
                                string sReceiver_CommentTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Comment' AND AccountID="
                                     + theAccountInvite.InvitedAccountID.ToString() + " AND IsActive=1");

                                string sSender_ChecklistTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Checklist' AND AccountID="
                                     + theAccountInvite.AccountID.ToString() + " AND IsActive=1");

                                string sSender_ChecklistItemTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Checklist Item' AND AccountID="
                                     + theAccountInvite.AccountID.ToString() + " AND IsActive=1");

                                string sSender_AttachmentTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Attachment' AND AccountID="
                                     + theAccountInvite.AccountID.ToString() + " AND IsActive=1");
                                string sSender_CommentTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Comment' AND AccountID="
                                     + theAccountInvite.AccountID.ToString() + " AND IsActive=1");

                                //task's multiple checklist
                                DataTable dtSender_CheckListRecords = Common.DataTableFromText("SELECT RecordID FROM [Record]  WHERE  IsActive=1 AND TableID=" +
                                    sSender_ChecklistTableID + " AND V003='" + rSender_TaskRecord.RecordID.ToString() + "'");

                                if(dtSender_CheckListRecords!=null && dtSender_CheckListRecords.Rows.Count>0)
                                {
                                    foreach (DataRow drSender_CheckListRecords in dtSender_CheckListRecords.Rows)
                                    {
                                        string sSender_ChecklistRecordID = drSender_CheckListRecords[0].ToString();
                                        if (!string.IsNullOrEmpty(sSender_ChecklistRecordID))
                                        {
                                            //it has checklist
                                            Record rReceiver_CheckListRecord = RecordManager.ets_Record_Detail_Full(int.Parse(sSender_ChecklistRecordID), null, false);
                                            if (rReceiver_CheckListRecord != null)
                                            {
                                                rReceiver_CheckListRecord.RecordID = null;
                                                rReceiver_CheckListRecord.TableID = int.Parse(sReceiver_ChecklistTableID);
                                                rReceiver_CheckListRecord.EnteredBy = theAccountInvite.InvitedUserID;
                                                rReceiver_CheckListRecord.V001 = Guid.NewGuid().ToString();
                                                rReceiver_CheckListRecord.DateTimeRecorded = DateTime.Now;
                                                rReceiver_CheckListRecord.V003 = rReceiver_TaskRecord.RecordID.ToString();
                                                rReceiver_CheckListRecord.RecordID = RecordManager.ets_Record_Insert(rReceiver_CheckListRecord);

                                                //now let's add multiple checklist item
                                                DataTable dtSender_CheckListItemRecord = Common.DataTableFromText("SELECT RecordID FROM [Record]  WHERE  IsActive=1 AND TableID=" + sSender_ChecklistItemTableID + " AND V014='" + sSender_ChecklistRecordID + "'");
                                                if (dtSender_CheckListItemRecord != null && dtSender_CheckListItemRecord.Rows.Count > 0)
                                                {
                                                    //we have checklist item
                                                    foreach (DataRow drSender_CLItemRecord in dtSender_CheckListItemRecord.Rows)
                                                    {
                                                        Record rR_CLIRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drSender_CLItemRecord[0].ToString()), null, false);
                                                        if (rR_CLIRecord != null)
                                                        {
                                                            rR_CLIRecord.RecordID = null;
                                                            rR_CLIRecord.TableID = int.Parse(sReceiver_ChecklistItemTableID);
                                                            rR_CLIRecord.EnteredBy = theAccountInvite.InvitedUserID;
                                                            rR_CLIRecord.V011 = Guid.NewGuid().ToString();
                                                            rR_CLIRecord.DateTimeRecorded = DateTime.Now;
                                                            rR_CLIRecord.V013 = "";//?? somehow sender record has sender contrator here
                                                            rR_CLIRecord.V014 = rReceiver_CheckListRecord.RecordID.ToString();
                                                            rR_CLIRecord.RecordID = RecordManager.ets_Record_Insert(rR_CLIRecord);

                                                            //check attachements(checklist item)
                                                            DataTable dtSender_CLI_A_Record = Common.DataTableFromText("SELECT RecordID FROM [Record]  WHERE  IsActive=1 AND TableID=" + sSender_AttachmentTableID
                                                                + " AND V009='" + drSender_CLItemRecord[0].ToString() + "'");
                                                            if (dtSender_CLI_A_Record != null && dtSender_CLI_A_Record.Rows.Count > 0)
                                                            {
                                                                //we have checklist item attachment
                                                                foreach (DataRow drSender_CLI_A_Record in dtSender_CLI_A_Record.Rows)
                                                                {
                                                                    Record rR_CLI_A_Record = RecordManager.ets_Record_Detail_Full(int.Parse(drSender_CLI_A_Record[0].ToString()), null, false);
                                                                    if (rR_CLI_A_Record != null)
                                                                    {
                                                                        rR_CLI_A_Record.RecordID = null;
                                                                        rR_CLI_A_Record.TableID = int.Parse(sReceiver_AttachmentTableID);
                                                                        rR_CLI_A_Record.EnteredBy = theAccountInvite.InvitedUserID;
                                                                        rR_CLI_A_Record.V001 = Guid.NewGuid().ToString();
                                                                        rR_CLI_A_Record.DateTimeRecorded = DateTime.Now;
                                                                        rR_CLI_A_Record.V005 = "";//?? somehow sender record has sender contrator here
                                                                        rR_CLI_A_Record.V009 = rR_CLIRecord.RecordID.ToString();
                                                                        rR_CLI_A_Record.RecordID = RecordManager.ets_Record_Insert(rR_CLI_A_Record);


                                                                    }
                                                                }
                                                            }


                                                            //check comment(checklist item)
                                                            DataTable dtSender_CLI_Comment_Record = Common.DataTableFromText("SELECT RecordID FROM [Record]  WHERE  IsActive=1 AND TableID=" +
                                                                sSender_CommentTableID + " AND V003='" + drSender_CLItemRecord[0].ToString() + "'");
                                                            if (dtSender_CLI_Comment_Record != null && dtSender_CLI_Comment_Record.Rows.Count > 0)
                                                            {
                                                                //we have checklist item comments
                                                                foreach (DataRow drSender_CLI_Comment_Record in dtSender_CLI_Comment_Record.Rows)
                                                                {
                                                                    Record rR_CLI_Comment_Record = RecordManager.ets_Record_Detail_Full(int.Parse(drSender_CLI_Comment_Record[0].ToString()), null, false);
                                                                    if (rR_CLI_Comment_Record != null)
                                                                    {
                                                                        rR_CLI_Comment_Record.RecordID = null;
                                                                        rR_CLI_Comment_Record.TableID = int.Parse(sReceiver_CommentTableID);
                                                                        rR_CLI_Comment_Record.EnteredBy = theAccountInvite.InvitedUserID;
                                                                        rR_CLI_Comment_Record.V006 = Guid.NewGuid().ToString();
                                                                        rR_CLI_Comment_Record.DateTimeRecorded = DateTime.Now;
                                                                        rR_CLI_Comment_Record.V004 = "";//?? somehow sender record has sender contrator here
                                                                        rR_CLI_Comment_Record.V003 = rR_CLIRecord.RecordID.ToString();
                                                                        rR_CLI_Comment_Record.RecordID = RecordManager.ets_Record_Insert(rR_CLI_Comment_Record);

                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }

                                    }
                                }

                               

                                //task's multiple attachements
                                DataTable dtSender_Task_A_Record = Common.DataTableFromText("SELECT RecordID FROM [Record]  WHERE  IsActive=1 AND TableID=" + sSender_AttachmentTableID
                                    + " AND V011='" + rSender_TaskRecord.RecordID.ToString() + "'");
                                if (dtSender_Task_A_Record != null && dtSender_Task_A_Record.Rows.Count > 0)
                                {
                                    //we have task attachment
                                    foreach (DataRow drSender_Task_A_Record in dtSender_Task_A_Record.Rows)
                                    {
                                        Record rR_Task_A_Record = RecordManager.ets_Record_Detail_Full(int.Parse(drSender_Task_A_Record[0].ToString()), null, false);
                                        if (rR_Task_A_Record != null)
                                        {
                                            rR_Task_A_Record.RecordID = null;
                                            rR_Task_A_Record.TableID = int.Parse(sReceiver_AttachmentTableID);
                                            rR_Task_A_Record.EnteredBy = theAccountInvite.InvitedUserID;
                                            rR_Task_A_Record.V001 = Guid.NewGuid().ToString();
                                            rR_Task_A_Record.DateTimeRecorded = DateTime.Now;
                                            rR_Task_A_Record.V005 = "";//?? somehow sender record has sender contrator here
                                            rR_Task_A_Record.V011 = rReceiver_TaskRecord.RecordID.ToString();
                                            rR_Task_A_Record.RecordID = RecordManager.ets_Record_Insert(rR_Task_A_Record);
                                        }
                                    }
                                }



                                //task's multiple comments
                                DataTable dtSender_Task_Comment_Record = Common.DataTableFromText("SELECT RecordID FROM [Record]  WHERE  IsActive=1 AND TableID=" +
                                    sSender_CommentTableID + " AND V002='" + rSender_TaskRecord.RecordID.ToString() + "'");
                                if (dtSender_Task_Comment_Record != null && dtSender_Task_Comment_Record.Rows.Count > 0)
                                {
                                    //we have task comments
                                    foreach (DataRow drSender_Task_Comment_Record in dtSender_Task_Comment_Record.Rows)
                                    {
                                        Record rR_Task_Comment_Record = RecordManager.ets_Record_Detail_Full(int.Parse(drSender_Task_Comment_Record[0].ToString()), null, false);
                                        if (rR_Task_Comment_Record != null)
                                        {
                                            rR_Task_Comment_Record.RecordID = null;
                                            rR_Task_Comment_Record.TableID = int.Parse(sReceiver_CommentTableID);
                                            rR_Task_Comment_Record.EnteredBy = theAccountInvite.InvitedUserID;
                                            rR_Task_Comment_Record.V006 = Guid.NewGuid().ToString();
                                            rR_Task_Comment_Record.DateTimeRecorded = DateTime.Now;
                                            rR_Task_Comment_Record.V004 = "";//?? somehow sender record has sender contrator here
                                            rR_Task_Comment_Record.V002 = rReceiver_TaskRecord.RecordID.ToString();
                                            rR_Task_Comment_Record.RecordID = RecordManager.ets_Record_Insert(rR_Task_Comment_Record);
                                        }
                                    }
                                }

                            }
                            else
                            {
                                //update
                                //string sR_ContractorRecordID = Common.GetValueFromSQL("SELECT RecordID FROM [Record] WHERE TableID=" +
                                //    iSender_ContractorTableId.ToString() + " AND V008='" + theAccountInvite.InvitedUserID.ToString() + "' AND IsActive=1");
                                //rSender_TaskRecord.V020 = sR_ContractorRecordID;
                                
                                //rSender_TaskRecord.V027 = sR_ContractorRecordID;//
                                if(theAccountInvite.InvitedUserID!=null)
                                {
                                    rSender_TaskRecord.V020 = theAccountInvite.InvitedUserID.ToString();
                                    rSender_TaskRecord.V027 = theAccountInvite.InvitedUserID.ToString();
                                }
                                
                            }

                            //theAccountInvite.InviteStatus = "A";
                            //rSender_TaskRecord.V026 = "A";
                            //RecordManager.ets_Record_Update(rSender_TaskRecord, true);
                            strMessage = "Thanks. You have accepted this task and an email confirmation has been sent";
                        }
                        else
                        {
                            theAccountInvite.InviteStatus = "D";
                            //rSender_TaskRecord.V026 = "D";
                            //RecordManager.ets_Record_Update(rSender_TaskRecord, true);

                            //Decline                          
                            strMessage = "Thanks. You have declined this task and an email confirmation has been sent";
                        }

                        string sInvitedFullName = theAccountInvite.Name;
                        if (theAccountInvite.InvitedUserID != null)
                        {
                            uReceiver_User = SecurityManager.User_Details((int)theAccountInvite.InvitedUserID);
                            if (uReceiver_User != null)
                            {
                                sInvitedFullName = uReceiver_User.FirstName + " " + uReceiver_User.LastName;
                                strInvitedUserName = uReceiver_User.FirstName + " " + uReceiver_User.LastName;
                            }
                        }
                        theMessage.OtherParty = theAccountInvite.Email;
                        theAccountInvite.IsActive = false;
                        theAccountInvite.AcceptedDate = DateTime.Now;

                        InvoiceManager.AccountInvite_Update(theAccountInvite);

                        rSender_TaskRecord.V026 = theAccountInvite.InviteStatus;
                        RecordManager.ets_Record_Update(rSender_TaskRecord, false);
                        string strError = "";
                        //Common.SendSingleEmail("r_mohsin@yahoo.com", "Account Invite:", "rSender_TaskRecord.V026:"+ rSender_TaskRecord.V026 + ";theAccountInvite:"+ theAccountInvite.InvitedAccountID.ToString() + ";Time:" + DateTime.Now.ToString(), ref strError);

                        theAcceptDeclineEmail.Heading = theAcceptDeclineEmail.Heading.Replace("[SenderFullName]", uSender_User.FirstName + " " + uSender_User.LastName);
                        theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[SenderFullName]", uSender_User.FirstName + " " + uSender_User.LastName);
                        theTaskAcceptNewUserEmail.Heading = theTaskAcceptNewUserEmail.Heading.Replace("[SenderFullName]", uSender_User.FirstName + " " + uSender_User.LastName);
                        theTaskAcceptNewUserEmail.ContentP = theTaskAcceptNewUserEmail.ContentP.Replace("[SenderFullName]", uSender_User.FirstName + " " + uSender_User.LastName);

                        theAcceptDeclineEmail.Heading = theAcceptDeclineEmail.Heading.Replace("[InvitedFullName]", sInvitedFullName);
                        theAcceptDeclineEmail.ContentP = theAcceptDeclineEmail.ContentP.Replace("[InvitedFullName]", sInvitedFullName);
                        theTaskAcceptNewUserEmail.Heading = theTaskAcceptNewUserEmail.Heading.Replace("[InvitedFullName]", sInvitedFullName);
                        theTaskAcceptNewUserEmail.ContentP = theTaskAcceptNewUserEmail.ContentP.Replace("[InvitedFullName]", sInvitedFullName);


                        theTaskAcceptNewUserEmail.ContentP = theTaskAcceptNewUserEmail.ContentP.Replace("[ToEmail]", theAccountInvite.Email);
                        theTaskAcceptNewUserEmail.ContentP = theTaskAcceptNewUserEmail.ContentP.Replace("[TempPassword]", theAccountInvite.RandomPassword);

                        string sSendEmailError = "";
                        theMessage.Subject = theAcceptDeclineEmail.Heading;
                        theMessage.Body = theAcceptDeclineEmail.ContentP;
                        DBGurus.SendEmail(theAcceptDeclineEmail.ContentKey, true, null, theAcceptDeclineEmail.Heading, theAcceptDeclineEmail.ContentP
                            , "", uSender_User.Email, "", "", null, theMessage, out sSendEmailError);

                        if (theTaskAcceptNewUserEmail != null && bAcceptNewuser)
                        {
                            theMessage.Subject = theTaskAcceptNewUserEmail.Heading;
                            theMessage.Body = theTaskAcceptNewUserEmail.ContentP;
                            DBGurus.SendEmail(theTaskAcceptNewUserEmail.ContentKey, true, null, theTaskAcceptNewUserEmail.Heading, theTaskAcceptNewUserEmail.ContentP
                                , "", theAccountInvite.Email, "", "", null, theMessage, out sSendEmailError);
                        }

                        //if (strInvitedUserName == "" && !string.IsNullOrEmpty(theAccountInvite.Name))
                        //{
                        //    strInvitedUserName = theAccountInvite.Name;
                        //}
                        //if (strInvitedUserName == "" && !string.IsNullOrEmpty(theAccountInvite.Email))
                        //{
                        //    strInvitedUserName = theAccountInvite.Email;
                        //}


                        //do we need this
                        //rSender_TaskRecord = RecordManager.ets_Record_Detail_Full((int)theAccountInvite.TaskRecordID, null, false);
                        //if (rSender_TaskRecord != null)
                        //{
                        //    rSender_TaskRecord.V026 = theAccountInvite.InviteStatus;
                        //    string sAcceptedOrDecline = "Accepted";
                        //    if( !string.IsNullOrEmpty(theAccountInvite.InviteStatus) &&  theAccountInvite.InviteStatus.ToLower()=="d")
                        //    {
                        //        sAcceptedOrDecline = "Declined";
                        //    }
                        //    //rSender_TaskRecord.V009 = rSender_TaskRecord.V009 + " "+ sAcceptedOrDecline + " Date:" + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();

                        //    //rSender_TaskRecord.V027 = strInvitedUserName;
                        //    RecordManager.ets_Record_Update(rSender_TaskRecord, true);
                        //}
                       



                    }//if(theAccountInvite.IsActive!=null && (bool)theAccountInvite.IsActive)
                    else
                    {
                        strMessage = "That invitation has already been processed. It is not possible to process it a 2nd time";
                    }
                }//if(theAccountInvite!=null)
                else
                {
                    strMessage = "Sorry that ID does not exist";
                }
            }
            else
            {
                strMessage = "Sorry that is an invalid response";
            }
        }
        catch
        {
            strMessage = "Sorry that is an invalid response";
        }
        lblMessage.Text = strMessage;
    }
}
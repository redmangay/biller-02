﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="_Default"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="apple-touch-icon" sizes="57x57"  href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="60x60" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="72x72" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="76x76" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="114x114" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="120x120" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="144x144" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="152x152" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="180x180" href="../Images/favicon.ico" />
    <link rel="icon" type="image/png" sizes="192x192" href="../Images/favicon.ico" />
    <link rel="icon" type="image/png" sizes="32x32" href="../Images/favicon.ico" />
    <link rel="icon" type="image/png" sizes="96x96" href="../Images/favicon.ico" />
    <link rel="icon" type="image/png" sizes="16x16" href="../Images/favicon.ico" />
    <link rel="manifest" href="Marketing/images/icon/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="Marketing/images/icon/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />

    <title>ETS</title>
    <link href="Marketing/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Marketing/css/index.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-nav">
       <script type="text/javascript" src="<%=ResolveUrl("~/script/jquery-3.3.1.min.js")%>"></script>
    <script>

        $('.navbar-collapse ul li a').click(function () {
            $(".navbar-collapse").collapse('hide');
        });
    </script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="Marketing/js/bootstrap.min.js"></script>
    <!-- Scrolling Nav JavaScript -->
    <script src="Marketing/js/jquery.easing.min.js"></script>
    <script src="Marketing/js/scrolling-nav.js"></script>
    <form role="form" runat="server">
        <div class="container" data-spy="affix" data-offset-top="120">
            <div class="row pad-tb-20">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 hidden-xs">
                    <img src="images/logo.png" class="img-responsive hidden-xs" id="home" alt="Environment Tracking System" />
                </div>
                <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
                    <nav class="navbar navbar-right">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="menu">Menu</span>
                            </button>
                            <a href="#" class="navbar-brand hidden visible-xs">
                                <img src="Marketing/images/sm-logo.jpg" class="img-responsive" alt="ETS" /></a>
                        </div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                            <ul class="nav navbar-nav">
                                <li class="active"><a class="page-scroll" href="#page-top">Home</a></li>
                                <li><a class="page-scroll" href="#client">Clients</a></li>
                                <li><a class="page-scroll" href="#features">Features</a></li>
                                <li><a class="page-scroll" href="#pricing">Pricing</a></li>
                                <li><a href="Login.aspx">Sign In</a></li>
                            </ul>

                        </div>
                    </nav>
                </div>
            </div>
        </div>





        <div class="container-fluid" style="background: url(Marketing/images/banner-bg.jpg) 55% 0% no-repeat">
            <!-- div class="container-fluid" style="background: url(Marketing/images/banner-bg.jpg) 55% 50% no-repeat" -->
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <h1 class="bnr-txt">Your all-in-one<br />
                            environment management<br />
                            system software</h1>
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-4 trial-bg">
                        <h1>Free Trial</h1>
                        <p><asp:Label ID="lblMessage" runat="server" 
                            Text="Fill in the form and we’ll contact you within 1 business day to schedule a free introductory session:"></asp:Label></p>
                        <div class="form-group">
                            <asp:TextBox ID="txtName" CssClass="form-control form-inpt" placeholder="Name*" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtEmail" CssClass="form-control form-inpt" placeholder="Email*" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtPhone" CssClass="form-control form-inpt" placeholder="Phone" runat="server"></asp:TextBox>
                        </div>
                        <br />
                        <asp:Button ID="btnSignUp" CssClass="btn btn-primary" Text="Sign Up" runat="server" OnClick="btnSignUp_Click" ValidationGroup="Form1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="White"
                                ErrorMessage="*Please enter your Name." ControlToValidate="txtName" ValidationGroup="Form1">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ForeColor="White"
                                ErrorMessage="<br/>*Please enter your Email." ControlToValidate="txtEmail" ValidationGroup="Form1">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server"
                                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ControlToValidate="txtEmail" ErrorMessage="<br/>Please enter a valid email address." ValidationGroup="Form1">
                            </asp:RegularExpressionValidator>
                    </div>

                </div>
            </div>
        </div>





        <div class="container-fluid pad-tb-30-70" style="background: url(Marketing/images/video-section-bg.png) 64% 72% repeat-x;">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 text-center ets-txt">
                        <h3>The Environment Tracking System (ETS)</h3>
                        <p>The ETS is an environmental management system software package that helps you report and monitor your environmental data in a simple and straight forward way.</p>
                        <p>A 100% web-based software platform, it’s an easy to use, configurable, low-cost solution designed to reduce time and resources spent managing your environmental management system.</p>
                        <div class="col-md-10 col-md-offset-1">
                            <div class="embed-responsive embed-responsive-4by3">
                                <iframe class="ets-video embed-responsive-item" src="https://www.youtube.com/embed/f9ffP6sdsDc?rel=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>





        <div class="container-fluid pad-tb-30-70" style="background: #f8f8f8;">
            <div class="container">
                <div class="row spsheet">
                    <div class="col-md-6 col-md-offset-3">
                        <h2 class="text-center">Are you in spreadsheet hell?</h2>
                        <hr />
                    </div>
                    <div class="col-md-6 col-lg-6 mr-tp-52">
                        <img src="Marketing/images/spsheet-hell.jpg" class="img-responsive" alt="Are you in Spreadsheet hell" />
                    </div>

                    <div class="col-md-6 col-lg-6 mr-tp-22">
                        <p>Have you met Roger? Roger is responsible for managing data as part of his company’s environmental management system. </p>
                        <p class="mr-tp-34"><strong>You have a lot to deal with:</strong></p>
                        <ul>
                            <li>Different versions</li>
                            <li>Lost files</li>
                            <li>Late data</li>
                            <li>Staff turnover</li>
                        </ul>
                        <p class="mr-tp-34">You need a software package that takes care of all of it. Because that stuff is no fun for anyone, let alone you and Roger. </p>
                        <p>That’s why we created the ETS. Everything is in one place, it’s secure and you have full control over who has access. Plus, you can use it from anywhere.</p>
                        <p class="mr-tp-34">
                            It’s easy to get going, little or no training is required and best of all, you can try it free, today.
                        </p>
                    </div>

                </div>
            </div>
        </div>

        <div class="container easy-way pad-tp-30">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h2 class="text-center">The easy way to measure,<br />
                        monitor, and manage</h2>
                    <hr />
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <h3>Complex analysis made simple</h3>
                    <p>Our aim is to simplify your data monitoring so you can focus on environmental management. Easy to use tools coupled with real time reporting means you can: </p>
                    <ul>
                        <li>Be more responsive </li>
                        <li>Better evaluate risks </li>
                        <li>Make more accurate projections. </li>
                    </ul>
                    <p>This all helps you create a more effective environmental management system and stay compliant with international standards and regulations. </p>
                </div>

                <div class="col-md-6 col-lg-6">
                    <h3>One tool for everything</h3>
                    <p>The Environment Tracking System can be used to manage any type of environmental monitoring data, so you don’t need to waste time and money investing in different tools.</p>
                    <p>Whether that is for continuous emission monitoring, compliance reporting, abatement or efficiency programs, offset and credit tracking or emissions and energy monitoring.</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row pad-tb-20">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="easy-clmn-one">
                        <ul>
                            <li>Dust Depositional</li>
                            <li>Dust TEOM</li>
                            <li>CO2 emissions</li>
                            <li>Gas and Ventilation</li>
                            <li>Fauna / Flora</li>
                        </ul>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 mr-tb-10">
                    <div class="easy-clmn-two">
                        <ul>
                            <li>Surface Water</li>
                            <li>Ground Water </li>
                            <li>Weather</li>
                            <li>Energy Usage</li>
                            <li>Rehabilitation</li>
                        </ul>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="easy-clmn-three">
                        <ul>
                            <li>Effluent / Waste</li>
                            <li>Blast / Noise</li>
                            <li>Hydrocarbons </li>
                            <li>Radiation</li>
                            <li>And Many More</li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-2 col-lg-4 col-lg-offset-2 grow text-center">
                    <img src="Marketing/images/grow.png" width="165" height="165" class="center-block mr-tb-60" />
                    <h2>Freedom to grow</h2>
                    <p>The Environment Tracking System is configurable and you can adjust the system as your requirements change. So you won’t need to worry about investing more time and resources if you outgrow your current set-up.</p>
                </div>


                <div class="col-sm-6 col-md-4 col-lg-4 grow text-center">
                    <img src="Marketing/images/lock.png" width="165" height="165" class="center-block mr-tb-60" />
                    <h2>We won’t lock you in</h2>
                    <p>With our environmental management system software, you control your data and you can download it at any time. So if you do want to change your telemetry equipment or data providers, your data is safe and secure in one location.</p>
                </div>



            </div>

            <div class="row">
                <div id="client"></div>
            </div>

        </div>


        <hr />



        <div class="container clients">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 text-center">
                    <h2>Who is the Environment Tracking System for?</h2>
                    <hr />
                    <p>
                        This environmental reporting system has been specifically designed for<br />
                        mining and heavy industry.  Some of our users include:
                    </p>
                </div>
            </div>

            <div class="row mr-tb-40">
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <img src="Marketing/images/glencore.jpg" class="img-responsive mr-tp-52" alt="glencore" />
                </div>

                <div class="col-sm-3 col-md-3 col-lg-3">

                    <img src="Marketing/images/riotinto.jpg" class="img-responsive mr-tp-52" alt="RioTinto" />
                </div>


                <div class="col-sm-3 col-md-3 col-lg-3">
                    <img src="Marketing/images/bma.jpg" class="img-responsive mr-tp-22" alt="BMA" />
                </div>

                <div class="col-sm-3 col-md-3 col-lg-3">
                    <img src="Marketing/images/gauge.jpg" class="img-responsive" alt="Gauge" />
                </div>
            </div>




            <div class="row mr-tb-40">
                <div class="col-sm-3 col-md-3 col-lg-3">
                    <img src="Marketing/images/coal.jpg" class="img-responsive mr-tp-42" alt="Dalrymple Bay coal Terminal" />
                </div>

                <div class="col-sm-3 col-md-3 col-lg-3">

                    <img src="Marketing/images/tyrrells.jpg" class="img-responsive mr-tp-42" alt="Tyrrell's Wines" />
                </div>


                <div class="col-sm-3 col-md-3 col-lg-3">
                    <img src="Marketing/images/bhpbilliton.jpg" class="img-responsive" alt="bhpbilliton" />
                </div>

                <div class="col-sm-3 col-md-3 col-lg-3">
                    <img src="Marketing/images/epuron.jpg" class="img-responsive mr-tp-42" alt="EPURON" />
                </div>
            </div>
        </div>


        <!--
        
        <div class="container-fluid pad-tb-30-70" style="background: url(images/review-bg.jpg) 62% 2% no-repeat #087a31">
            <div class="container rview">
                <div class="row">
                    <h2 class="text-center">Here's what they have to say about the ETS</h2>
                    <hr />
                    <div class="col-sm-6 col-md-5 col-lg-5 col-xs-12 mr-left-4-1 mr-tp-22 rview-clnm">
                        <img src="Marketing/images/review.PNG" width="94" height="94" class="pull-left" />
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                            <br />

                        </p>
                        <a href="#">Name & Position</a>

                    </div>


                    <div class="col-sm-6 col-md-5 col-lg-5 col-xs-12 mr-tp-22 mr-left-8-3 rview-clnm">
                        <img src="Marketing/images/review.PNG" width="94" height="94" class="pull-left" />
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                            <br />

                        </p>
                        <a href="#">Name & Position</a>

                    </div>
                </div>
            </div>
        </div>

        -->
        <div class="container-fluid pad-tb-20" style="background: #6e6e6e;" id="features">
            <div class="container">
                <div class="row signup">
                    <div class="col-sm-6 col-md-6 col-lg-6 mr-left-4-1">
                        <h3>Want to learn more?</h3>
                        <p>Sign up for a free trial and we'll take you through a free introductory session</p>
                    </div>

                    <div class="col-sm-6 col-md-4 col-lg-4 mr-left-8-3">
                        <a href="#" class="btn btn-success btn-lg">Sign Up</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container pad-tp-30">
            <div class="row reporting">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 text-center">
                    <h2>Why you'll love the Environmental<br />
                        Tracking System</h2>
                    <hr />

                    <h4><strong>Reporting is now easier than ever</strong></h4>
                    <p>This system is quick and easy to set up and requires no training. Your team can be brought up to speed in no time, and what’s more, contractors or casual staff can hit the ground running. That way you’re happy, and all the Rogers of the world are happy, too.</p>
                </div>
            </div>
            <div class="row report-column mr-tp-42">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <img src="Marketing/images/import.png" width="75" height="75" />
                    <h5><strong>Import data from spreadsheets or upload data straight into the system</strong></h5>
                    <ul>
                        <li>Drag in your graphs</li>
                        <li>Drop in tables</li>
                        <li>Paste in your text</li>
                        <li>Upload a few photos</li>
                        <li>Publish to Word or the web</li>
                    </ul>
                </div>


                <div class="col-sm-4 col-md-4 col-lg-4 mr-tp-30">
                    <img src="Marketing/images/automated.png" width="75" height="75" />
                    <h5><strong>Automated Feeds and Telemetry</strong></h5>
                    <p>We make it simple to bring in data from other systems and from remote sensors:</p>
                    <ul>
                        <li>Automatically upload from email or FTP</li>
                        <li>Instant validation on entry</li>
                        <li>Automatic notifications</li>
                        <li>Warnings/Alerts by email or SMS</li>
                    </ul>
                </div>


                <div class="col-sm-4 col-md-4 col-lg-4">
                    <img src="Marketing/images/accessible.png" width="75" height="75" />
                    <h5><strong>Accessible from anywhere</strong></h5>
                    <p>
                        The ETS is entirely web based. This means everything is in the one place and you can reach it from anywhere. So reporters can upload data directly from the site, even in remote locations. 
Perfect for if you have more than one site or different reporters at various locations and need a centralised solution.
                    </p>
                </div>

            </div>


            <hr />

            <div class="row reporting">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 text-center">
                    <h4><strong>We give you peace of mind</strong></h4>
                    <p>Unsure about cloud security? Cloud-based platforms are a little like air travel. It might seem more dangerous, but because of all the extra precautions we take, it’s actually a lot safer. Here’s what we do to ensure your data is secure in the cloud.</p>
                </div>
            </div>

            <div class="row report-column pad-tb-30-70">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <img src="Marketing/images/safe.png" width="75" height="75" />
                    <h5><strong>Your data is safe</strong></h5>
                    <ul>
                        <li>We do hourly backups of the database as well as daily backups to an offsite location to ensure there is no single point of failure.</li>

                        <li>You control who can access your sensitive data and what each user can do in each menu.</li>

                        <li>The ETS does not allow data to be permanently deleted and all changes are tracked so you can see who changed what and why.</li>
                    </ul>
                </div>


                <div class="col-sm-4 col-md-4 col-lg-4 mr-tp-30">
                    <img src="Marketing/images/alert.png" width="75" height="75" />
                    <h5><strong>Alerts and warnings</strong></h5>
                    <p>We keep you informed of problems by email and SMS so you can respond quickly.</p>
                    <ul>
                        <li>When limits have been breached!</li>
                        <li>Invalid data or unlikely readings</li>
                        <li>Broken Monitors (flat lining)</li>
                        <li>Late Data / Missed Scheduled</li>
                        <li>Sent via Email or SMS</li>
                    </ul>
                </div>


                <div class="col-sm-4 col-md-4 col-lg-4">
                    <img src="Marketing/images/support.png" width="75" height="75" />
                    <h5><strong>Support</strong></h5>
                    <p>And if there’s a problem you can’t solve, we’re here to help. All our customers get free technical support. Depending on the level of support you need your plan will include email or phone support from one of our skilled technicians.</p>
                </div>

            </div>
        </div>



        <div class="container-fluid pad-tb-30-70" style="background: #f8f8f8;">
            <div class="container packed">
                <div class="row">
                    <h2 class="text-center">Packed with features</h2>
                    <hr class="text-center" />
                    <div class="col-sm-6 col-md-5 col-lg-5 mr-tp-22">
                        <img src="Marketing/images/packed.jpg" class="img-responsive" alt="Packed with features" />
                    </div>

                    <div class="col-sm-6 col-md-7 col-lg-7 mr-tp-42">
                        <h3>The more you use it, the more you'll love it</h3>
                        <p>
                            Our analysis and interpreting tools give you quick and valuable insight into your data.
                            <br />
                            If you’re like Roger and you get excited about things like graphs and tables, you’ll love what you can do with them in the Environment Tracking System. They’re easy to produce and you can drag and drop, too.
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <div class="container-fluid pad-tb-30-70 pad-bt-30 pad-tp-24" style="background: #0b9444;">
            <div class="container slide">
                <div class="row">
                    <div class="carousel slide" id="myCarousel" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                            <li data-target="#myCarousel" data-slide-to="4"></li>
                        </ol>

                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                    <img src="Marketing/images/slide1.png" class="img-responsive" alt="Easy to use dashboard" />
                                </div>
                                <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                    <h2>Easy to use dashboard</h2>
                                    <ul>
                                        <li>Map your sites</li>
                                        <li>Quick select graphs </li>
                                        <li>Recent Uploads </li>
                                        <li>Recent Warnings</li>
                                        <li>Latest values and limits</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="item">
                                <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                    <img src="Marketing/images/slide2.png" class="img-responsive" alt="Configurable Tables and Sample Types" />
                                </div>
                                <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                    <h2>Configurable Tables and Sample Types </h2>
                                    <ul>
                                        <li>Use a template and customise it</li>
                                        <li>Create from your spreadsheet</li>
                                        <li>Start from scratch and specify the columns</li>
                                        <li>Add, edit or remove columns in real time</li>
                                        <li>Create your own validation and limits</li>
                                        <li>Calculated columns, dropdowns, linked tables...</li>
                                    </ul>
                                </div>
                            </div>


                            <div class="item">
                                <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                    <img src="Marketing/images/slide3.png" class="img-responsive" alt="Tables and graphs" />
                                </div>
                                <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                    <h2>Tables and graphs</h2>
                                    <ul>
                                        <li>Graph between sample types</li>
                                        <li>Unlimited number of series</li>
                                        <li>Editable Axis: Left, Right & Relative</li>
                                        <li>Line, Bar, Point, Area and Min/Max/Mean</li>
                                        <li>Export to email, pdf or report</li>
                                    </ul>
                                </div>
                            </div>



                            <div class="item">
                                <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                    <img src="Marketing/images/slide4.png" class="img-responsive" alt="Telemetry and Uploads" />
                                </div>
                                <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                    <h2>Telemetry and Uploads</h2>
                                    <ul>
                                        <li>Automate your data uploads direct from your sensors</li>
                                        <li>Use 3rd party sampling companies to upload your data </li>
                                        <li>Notifications and warnings via email and SMS</li>
                                    </ul>
                                </div>
                            </div>


                            <div class="item">
                                <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                    <img src="Marketing/images/slide5.png" class="img-responsive" alt="Security features" />
                                </div>
                                <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                    <h2>Security features</h2>
                                    <ul>
                                        <li>Authenticated users only</li>
                                        <li>Full data audit / exception reporting </li>
                                        <li>Enterprise grade secure hosting</li>
                                        <li>SSL and Escrow facility available</li>
                                    </ul>
                                </div>

                            </div>


                        </div>

                    </div>

                </div>

            </div>

        </div>


        <div class="container-fluid" style="background: #3e3e3e;" id="pricing">
            <div class="container pad-tp-30">
                <div class="row price">
                    <h2 class="text-center">Pricing and Plans</h2>
                    <hr class="text-center" />
                    <div class="col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
                        <p><strong>Monthly Support and Hosting Plans</strong></p>
                        <p>We believe in transparent, clear and understandable pricing:</p>
                        <br />

                        <ul>
                            <li><strong>No Hidden Costs:</strong> We’re upfront about all our prices and you’ll never be charged for anything you haven’t agreed to.</li>
                            <li><strong>No Capital Outlay:</strong> We provide all of the hardware, software and licensing required for you to manage your environmental data online.</li>
                            <li><strong>Simple Pricing:</strong> We will give you a fixed quote for data migration and configuration. After that it is a straightforward monthly hosting and support plan. See below:</li>
                        </ul>
                        <br />

                    </div>
                </div>

                <div class="row mr-tb-40 price-column">

                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <div class="col-one-top text-center">
                            <h3>$300 <small>AUD</small></h3>
                            <span>Small</span>
                        </div>
                        <div class="col-one-body">
                            <ul>
                                <li>Dashboard, maps and beautiful graphs</li>
                                <li>Basic and advanced security options</li>
                                <li>All changes to your data are tracked</li>
                                <li>Simple document management systems</li>
                                <li>Scheduling and reminders</li>
                                <li>Report generator and export</li>
                                <li>Upload from spreadsheets</li>
                                <li>Alerts via email</li>
                                <li class="disable">Alerts via sms</li>
                                <li class="disable">Automated upload</li>
                                <li>Support - Email & Phone</li>
                                <li>Max number of records - <strong>10,000</strong></li>
                                <li>File space for documents and PDF’s <strong>100 MB</strong></li>
                                <li>Up to <strong>10 users</strong></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-4 col-md-4 col-lg-4 col-two-style">
                        <div class="col-two-top text-center">
                            <h3>$500 <small>AUD</small></h3>
                            <span>Standard</span>
                        </div>
                        <div class="col-two-body">
                            <ul>
                                <li>Dashboard, maps and beautiful graphs</li>
                                <li>Basic and advanced security options</li>
                                <li>All changes to your data are tracked</li>
                                <li>Simple document management systems</li>
                                <li>Scheduling and reminders</li>
                                <li>Report generator and export</li>
                                <li>Upload from spreadsheets</li>
                                <li>Alerts via email</li>
                                <li>Alerts via sms</li>
                                <li class="disable">Automated upload</li>
                                <li>Support - Email & Phone</li>
                                <li>Max number of records - <strong>100,000</strong></li>
                                <li>File space for documents and PDF’s <strong>1GB</strong></li>
                                <li>Up to <strong>100 users</strong></li>
                            </ul>
                        </div>
                    </div>


                    <div class="col-sm-4 col-md-4 col-lg-4">
                        <div class="col-three-top text-center">
                            <h3>$800 <small>AUD</small></h3>
                            <span>Corporate</span>
                        </div>
                        <div class="col-three-body">
                            <ul>
                                <li>Dashboard, maps and beautiful graphs</li>
                                <li>Basic and advanced security options</li>
                                <li>All changes to your data are tracked</li>
                                <li>Simple document management systems</li>
                                <li>Scheduling and reminders</li>
                                <li>Report generator and export</li>
                                <li>Upload from spreadsheets</li>
                                <li>Alerts via email</li>
                                <li>Alerts via sms</li>
                                <li>Automated upload</li>
                                <li>Support - Email & Phone</li>
                                <li>Max number of records - <strong>500,000</strong></li>
                                <li>File space for documents and PDF’s <strong>5GB</strong></li>
                                <li><strong>Unlimted users</strong></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 text-center ets-txt">
                    <br />
                    <p style="text-align:center"><strong>Not sure which one is right for you? <a href="#page-top">Try it free, no obligation.</a></strong> </p>
                    <p style="text-align:center">Note: If none are suitable please get in touch and we will create a plan to meet your needs.</p>
                    </div>
                </div>

            </div>
        </div>

        <div class="container pad-tp-30">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 btm-form-bg">
                    <h1>Got a question?</h1>
                    <p><asp:Label ID="lblMessage2" runat="server" Text="Fill in the form below and we’ll get you the answer."></asp:Label></p>
                    <div class="form-group">
                        <asp:TextBox ID="txtName2" CssClass="form-control form-inpt" placeholder="Name*" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="txtEmail2" CssClass="form-control form-inpt" placeholder="Email*" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:TextBox TextMode="MultiLine" rows="6" name="txtMessage" ID="txtMessage" CssClass="form-control form-inpt" placeholder="Question*" runat="server"></asp:TextBox>
                    </div>
                    <br />
                    <asp:Button ID="btnSignUp2" CssClass="btn btn-primary" Text="Send" runat="server" OnClick="btnSignUp2_Click" ValidationGroup="Form2" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ForeColor="White"
                            ErrorMessage="*Please enter your Name." ControlToValidate="txtName2" ValidationGroup="Form2">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ForeColor="White"
                            ErrorMessage="<br/>*Please enter your Email address." ControlToValidate="txtEmail2" ValidationGroup="Form2">
                        </asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ForeColor="White"
                            ErrorMessage="<br/>*Please enter your Question." ControlToValidate="txtMessage" ValidationGroup="Form2">
                        </asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                            ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ControlToValidate="txtEmail2" ErrorMessage="<br/>Please enter a valid Email address." ValidationGroup="Form2">
                        </asp:RegularExpressionValidator>
                    
                </div>



                <div class="col-sm-6 col-md-6 col-md-offset-1 col-lg-6 col-lg-offset-1 signup-text">
                    <h2>Contact Us</h2>
                    <hr class="pull-left" />

                  <div class="text-one">
                        <img src="Marketing/images/one.png" class="pull-left" width="41" height="41" />
                        <h3>Got a question?</h3>
                        <p>Please fill in the form on the right with any questions you have and we’ll get back to you within 1 business day with some answers. Please note we will not pass your details on to any other party.</p>
                        <br />
</div>
<div class="text-two">
                        <img src="Marketing/images/two.png" class="pull-left" width="41" height="41" />
                        <h3>Quick Demo?</h3>
                        <p>Would you like a quick half hour demo of the system online? The introductory session can do it remotely over the phone. We’ll share screens and show you how it works.</p>
                        <br />
</div>
<div class="text-three">
                        <img src="Marketing/images/three.png" class="pull-left" width="41" height="41" />
                        <h3>Free Trial?</h3>
                        <p>Do you want to give it a try free for one month. If you like it at the end of the trial, we’ll discuss which plan is best for you.</p>
                        <p>
                            <strong>No credit card details required 
                            </strong>
                        </p>
                    </div> 
                </div>
            </div>
        </div>

        <div class="container-fluid" style="background: #ebebeb;">
            <div class="row">
                <div class="col-md-12 text-center">
                                   <address class="lead">Copyright&copy; 2017. All rights reserved.<br />
<a href="http://www.dbgurus.com.au/" target="_blank"><strong>DB Gurus Pty Ltd</strong>.</a>, Australia. ACN 134 593 712<br />
Phone <a href="tel:1800901096">1800 90 10 96​</a>  <br />
Int:<a href="tel:+61242685672">+61 2 4268 5672</a></address>
                </div>
            </div>

        </div>
    </form>   
</body>
</html>

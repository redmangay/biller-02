﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Collections.ObjectModel;
public partial class TimeZone : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lblLocalTime.Text = DateTime.Now.ToString();
            ReadOnlyCollection<TimeZoneInfo> tzi;
            tzi = TimeZoneInfo.GetSystemTimeZones();
            foreach (TimeZoneInfo timeZone in tzi)
            {
                ddlTimeZone.Items.Add(new ListItem(timeZone.DisplayName, timeZone.Id));
            }
        }
    }
    protected void ddlTimeZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlTimeZone.SelectedIndex > 0)
        {
            DateTime dt = DateTime.Now;
            lblLocalTime.Text = DateTime.Now.ToString();
            lblTimeZone.Text = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dt,
                TimeZoneInfo.Local.Id, ddlTimeZone.SelectedValue).ToString();
        }
    }
 
}
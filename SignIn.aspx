﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="SignIn.aspx.cs" Inherits="SignIn" %>


<!doctype html>
<html class="no-js" lang="en">
<head runat="server">
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">
    <meta name="description" content="">
    <link rel="icon" href="favicon.ico">
    <!-- Page Title -->
    <title>Sign In</title>
    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="ontask/css/plugins.css">
    <link rel="stylesheet" type="text/css" href="ontask/css/style.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type="text/javascript" src="assets/vendor/backward/html5shiv.js"></script>
      <script type="text/javascript" src="assets/vendor/backward/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .tm-login-wrap .tm-login-form input[type=submit] {
            width: 100%;
            height: 50px;
            font-size: 28px;
            font-family: 'BebasNeue', sans-serif;
            background-color: #288ef8;
            border: none;
            color: #fff;
            cursor: pointer;
            font-weight: 700;
            margin-top: 5px;
            margin-bottom: 35px;
            -webkit-transition: all 0.4s ease;
            -o-transition: all 0.4s ease;
            transition: all 0.4s ease;
        }
        .login-input {
            font-family: Oswald
        }
    </style>
</head>

<body data-spy="scroll" data-target=".primary-nav">

    <!-- Preloader -->
    <div id="preloader"><div class="preloader-wave"></div></div>

    <!-- Start Site Header -->
    <header class="site-header support-header">
        <div class="container header-wrap">
            <div class="site-branding">
                <a href="index.html" class="custom-logo-link">
                    <img src="ontask/img/logo.png" alt="" class="custom-logo">
                </a>
            </div>
            <nav class="primary-nav">
                <ul class="primary-nav-list">
                    <li class="menu-item current-menu-parent"><a href="http://www.ontaskhq.com.au/index.html#yourday" class="nav-link">Your Day</a></li>
                    <li class="menu-item"><a href="http://www.ontaskhq.com.au/index.html#pricing" class="nav-link">Pricing</a></li>
                    <li class="menu-item"><a href="http://www.ontaskhq.com.au/index.html#faq" class="nav-link">FAQs</a></li>
                    <!--
                    <li class="menu-item"><a href="#subscribe" class="nav-link">Start</a></li>
                    -->
                    <li class="menu-item"><a href="http://www.ontaskhq.com.au/support.html" class="nav-link">Support</a></li>
                    <li class="menu-item"><a href="https://OnTask.thedatabase.net" class="nav-link">Sign In</a></li>
                    <li class="menu-item"><a href="https://OnTask.thedatabase.net/SignUp.aspx" class="nav-link"><strong>Sign Up</strong></a></li>
                </ul>
            </nav>
        </div>
    </header>

    <!-- Start Login Section -->
    <div class="tm-login-wrap" style="background-image: url('ontask/img/bg.jpg');">
        <div class="container">
            <h2 class="tm-login-text"><span>Sign in to quickly see where team members are, set up work schedules, create checklist templates, and much more.</span> </h2>
            <form id="frmMain" runat="server" action="#" class="tm-login-form font-bebasneue">
                <asp:ScriptManager ID="ScriptManager" runat="server" />
                <asp:UpdatePanel ID="updatePanel" runat="server">
                    <ContentTemplate>
                        <h1>SIGN IN</h1>
                        <%-- <input type="text" placeholder="Email"> --%>
                        <asp:TextBox ID="txtLogInEmail" runat="server" placeholder="EMAIL" CssClass="login-input" />
                        <%-- <input type="password" placeholder="Password"> --%>
                        <div style="position:relative">
                            <asp:TextBox ID="txtLogInPassword" ClientIDMode="Static" runat="server" placeholder="PASSWORD" TextMode="Password" style="position:absolute" CssClass="login-input"/>
                            <input id="showPassword_icon" type="button" data-click-state="1" style="position:absolute;width:10px;right:10px" />
                        </div><br/><br /><br/>
                        <%-- <button type="submit">Log In</button>--%>
                        <asp:Button ID="lnkLogin" runat="server" Text="SIGN IN" OnClick="lnkLogIn_Click" />
                        <%-- <a href="#" class="f-pass">Forgot your password?</a> --%>
                        <asp:HyperLink ID="hlForgotPassword" CssClass="f-pass" runat="server" Text="Forgot your password?" NavigateUrl="Security/PasswordResetRequest.aspx" />
                        <div class="tm-n-acc">Need an account? <a href="SignUp.aspx">Sign up!</a></div>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </form>
        </div>
    </div>
    <!-- End Login Section -->

    <!-- Start Site Footer -->
    <footer class="site-footer">
        <div class="container">
            <div class="footer-app-logo">
                <a href="https://itunes.apple.com/au/app/subway-surfers/id512939461?mt=8" class="app-btn" target="_blank"><img src="ontask/img/app-store.png" alt="Download from App Store"></a>
                <a href="https://play.google.com/store/apps/details?id=com.kiloo.subwaysurf" class="app-btn" target="_blank"><img src="ontask/img/play-store.png" alt="Download from Google Play Store"></a>
            </div>
            <br />
            <div class="footre-logo"><img src="ontask/img/footer-logo.png" alt=""></div>
            <ul class="contact-info font-bebasneue">
                <!---->
                <li>
                    <a href="http://www.ontaskhq.com.au/terms-of-service.pdf" style="letter-spacing:-0.3px;">
                        <img src="ontask/img/ContactUs.png" alt="Terms of Service Icon">
                        Terms of Service
                    </a>
                </li>
                <li>
                    <a href="http://www.ontaskhq.com.au/privacy-policy.pdf">
                        <img src="ontask/img/phone-icon.png" alt="Privacy Policy Icon">
                        Privacy Policy
                    </a>
                </li>
                <li>
                    <a href="contact.aspx">
                        <img src="ontask/img/envlop-icon.png" alt="Contact Us Icon">
                        Contact Us
                    </a>
                </li>
            </ul>
            <div class="copyright">
                <p>Copyright© 2018. All rights reserved.</p>
            </div>
        </div>
    </footer>
    <!-- Scripts -->
    <script src="ontask/js/modernizr-3.5.0.min.js"></script>
    <script src="ontask/js/jquery-1.12.4.min.js"></script>
    <script src="ontask/js/jquery.mb.YTPlayer.min.js"></script>
    <script src="ontask/js/plugins.js"></script>
    <script src="ontask/js/main.js"></script>
    <script type="text/javascript">
        var initialize = function () {
            //Show Password Feature for the Password Field
            var ua = window.navigator.userAgent;
            var trident = ua.indexOf('Trident/');
            var edge = ua.indexOf('Edge/');
            var msie = ua.indexOf('MSIE ');

            if ((msie > 0) || (trident > 0) || (edge > 0)) {
                // IE 10 or older => return version number
                //alert(parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10));
                document.getElementById('txtLogInPassword').style.width = '100%';
                document.getElementById('showPassword_icon').style.display = 'none';

            }
            else {

                $('#showPassword_icon').css('background', 'url("Images/eye-open-edited3.png")');
                $('#showPassword_icon').css('background-position-x', 'center');
                $('#showPassword_icon').css('background-position-y', 'center');
                $('#showPassword_icon').css('background-repeat', 'no-repeat');
                //$('#showPassword_icon').css('background-color', 'white');

                $('#showPassword_icon').on('click', function () {

                    if ($(this).attr('data-click-state') == 1) {
                        $(this).attr('data-click-state', 0)
                        document.getElementById('txtLogInPassword').type = 'text';
                        $(this).css('background', 'url("Images/eye-close-edited3.png")');
                        $(this).css('background-position-x', 'center');
                        $(this).css('background-position-y', 'center');
                        $(this).css('background-repeat', 'no-repeat');
                        //$(this).css('background-color', 'white');


                    } else {
                        $(this).attr('data-click-state', 1)
                        document.getElementById('txtLogInPassword').type = 'password';
                        $(this).css('background', 'url("Images/eye-open-edited3.png")');
                        $(this).css('background-position-x', 'center');
                        $(this).css('background-position-y', 'center');
                        $(this).css('background-repeat', 'no-repeat');
                        //$(this).css('background-color', 'white');
                    }

                });
            }
        };
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            // re-bind your jQuery events here
            initialize()
        });
        $(document).ready(initialize());
        //$("#txtLogInEmail").keyup(function () {
        //    if (this.value != "") {
        //        $("#txtLogInEmail").css("font-family", "Arial");
        //    }
        //    else {
        //        $("#txtLogInEmail").css("font-family", "inherit")
        //    }
        //});
        //$("#txtLogInPassword").keyup(function () {
        //    if (this.value != "") {
        //        $("#txtLogInPassword").css("font-family", "Arial");
        //    }
        //    else {
        //       $("#txtLogInPassword").css("font-family", "inherit");;
        //    }
        //});
    </script>
</body>
</html>

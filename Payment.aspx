﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true" CodeFile="Payment.aspx.cs" Inherits="Payment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

    <div style="padding: 50px; min-height: 300px;">
        <asp:Panel runat="server" ID="pnlStripButton">
            <script
                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="<%= stripePublishableKey %>"
                data-amount="<%= GetTotalPaymentAmount() %>"
                data-currency="AUD"
                data-name="DBGurus.com.au"
                data-description="OnTask Monthly Charge"
                data-email="<%= GetUserEmail() %>"
                data-locale="auto"
                data-zip-code="false"
                data-panel-label="Pay Monthly {{amount}}">
            </script>
        </asp:Panel>

        <br />
        <br />
        <%--<asp:Button runat="server" ID="btnPaypal" Text="Pay by Paypal" OnClick="btnPaypal_Click" />--%>

        <br />
        <asp:Label runat="server" ID="lblMessage"></asp:Label>

    </div>


</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class God : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User objUser = (User)Session["User"];
        if (Common.HaveAccess(Session["roletype"].ToString(), "1") || objUser.Email == "mohsinrazuan@gmail.com"
            || Request.Url.Authority.IndexOf("localhost") == 0 || objUser.Email=="r_mohsin@yahoo.com"
            || objUser.Email.IndexOf("@dbgurus.com.au")>-1)
        {
            //ready to go
        }
        else
        {
            Application["ALSLive"] = null;
            Response.Redirect("~/Default.aspx", false);
            return;
        }
         
        
    }
    protected void btnGetInt_Click(object sender, EventArgs e)
    {
       try
       {
           lblDecrypt.Text = "";
            lblDecrypt.Text=  Cryptography.Decrypt(txtEncrypted.Text.Trim());
       }
        catch(Exception ex)
       {
            lblDecrypt.Text=ex.Source + "---Stack --->" + ex.StackTrace;
        }
        
    }

    protected void btnEncrypt_Click(object sender, EventArgs e)
    {
        try
        {
            lblEncrypt.Text = "";
            lblEncrypt.Text = Cryptography.Encrypt(txtDycrepted.Text.Trim());
        }
        catch (Exception ex)
        {
            lblEncrypt.Text = ex.Source + "---Stack --->" + ex.StackTrace;
        }

    }


}
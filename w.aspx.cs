﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class w : System.Web.UI.Page
{


    protected override void OnPreInit(EventArgs e)
    {
        //if (Responsive.DeviceWidth()!=null)
        //    lblW.Text = Responsive.DeviceWidth().ToString();
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        

        if (Session["DeviceWidth"] != null)
            lblW.Text = Responsive.DeviceWidth().ToString();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
               

        if (Session["DeviceWidth"] == null)
        {
            ltJS.Text = @"<script type='text/javascript'>
     
                            $(document).ready(function () {
		                    window.location.reload();
                            });
                        </script>";

            return;
        }
    }
    protected void btnSetWidthNull_Click(object sender, EventArgs e)
    {
        lblW.Text = "";
        Session["DeviceWidth"] = null;
    }
}
﻿<%@ WebHandler Language="C#" Class="GetLocation" %>

using System;
using System.Web;
using System.Linq;
using System.Text;
using System.Data;
using DocGen.DAL;
using System.Collections.Generic;
using System.Xml;
using System.IO;
public class GetLocation : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        //context.Response.ContentType = "text/plain";
        //context.Response.Write("Hello World");
        string strJSON = "";
        try
        {
            string sAccountID= context.Session["AccountID"].ToString();

            if (context.Request.QueryString["ontask"] != null)
            {

                //string sContractorTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Contractor' AND AccountID="
                //                 + sAccountID + " AND IsActive=1");

                context.Response.Clear();
                context.Response.ContentType = "application/json";
                int TableID = 0;
                Int32.TryParse(Convert.ToString(context.Request.QueryString["TableID"]), out TableID);
                DocumentSection section = new DocumentSection();
                using (DocGenDataContext ctx = new DocGenDataContext())
                {
                    section = ctx.DocumentSections.SingleOrDefault<DocumentSection>(s => s.DocumentSectionID == int.Parse(context.Request.QueryString["DocumentSectionID"].ToString()));

                }

                LocationSectionDetail mapDetail = new LocationSectionDetail();
                if (section.Details != "")
                {
                    mapDetail = JSONField.GetTypedObject<LocationSectionDetail>(section.Details);

                }

                StringBuilder sb = new StringBuilder();



                if(context.Request.QueryString["Staff"]!=null &&  context.Request.QueryString["Staff"].ToString()=="true")
                {
                    //DataTable dtStaff = Common.DataTableFromText("SELECT distinct(UserID) FROM [UserLocation] WHERE CONVERT(Date, LastSeen)=CONVERT(Date, GETDATE())");

                    DataTable dtStaff = Common.DataTableFromText("SELECT DISTINCT (U.UserID) FROM [UserRole] UR JOIN [User] U ON UR.UserID=U.UserID  WHERE U.IsActive=1 AND AccountID="+ context.Session["AccountID"].ToString());


                    if(dtStaff!=null && dtStaff.Rows.Count>0)
                    {
                        foreach(DataRow drStaff in dtStaff.Rows)
                        {
                            //ANY DAY Last location
                            DataTable dtEachStaff = Common.DataTableFromText("SELECT top 1 * FROM [UserLocation] WHERE UserID="+
                                drStaff["UserID"].ToString()+" ORDER BY UserLocationID desc");

                            User theUser = SecurityManager.User_Details(int.Parse(drStaff["UserID"].ToString()));

                            foreach(DataRow drEachStaff in dtEachStaff.Rows)
                            {
                                EachLocationPinInfo aPin = new EachLocationPinInfo();
                                aPin.pintype = "s";
                                aPin.lat = drEachStaff["Latitude"].ToString();
                                aPin.lon =  drEachStaff["Longitude"].ToString();
                                aPin.userid = int.Parse(drStaff["UserID"].ToString());
                                if(theUser!=null)
                                {
                                    aPin.title = theUser.FirstName + " " + theUser.LastName;
                                    aPin.label = theUser.FirstName.Substring(0, 1).ToUpper() + theUser.LastName.Substring(0, 1).ToUpper();
                                }

                                aPin.pin = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath + "/Pages/Record/PinImages/Staff.png";
                                //aPin.rings = theTable.DistanceRings;
                                //aPin.order = theTable.PinDisplayOrder;
                                //aPin.onStart = theTable.ShowOnStart;
                                //aPin.ssid = drRecord["RecordID"].ToString();
                                //string sContractorRecordID = Common.GetValueFromSQL("SELECT RecordID FROM [Record] WHERE TableID=" +
                                //           sContractorTableID + " AND V008='" + drStaff["UserID"].ToString() + "' AND IsActive=1");//VOO8=[Contractor].User

                                //aPin.url = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?fixedurl=" + Cryptography.Encrypt("~/Default.aspx") + "&stackzero=yes&mode=" + Cryptography.Encrypt("view") +
                                //    "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt(sContractorTableID) + "&Recordid=" +
                                //    Cryptography.Encrypt(sContractorRecordID);

                                aPin.url = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?mode=" + Cryptography.Encrypt("view") +
                                    "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&userid=" + Cryptography.Encrypt(drStaff["UserID"].ToString());

                                string strPopup = "test popuo </br>need "+aPin.title+" detail info from contractor or user table";//need detail info about the contractor.
                                aPin.mappopup = strPopup;
                                aPin.recordid = int.Parse(drStaff["UserID"].ToString());//sContractorRecordID

                                sb.Append(",");
                                sb.Append(aPin.GetJSONString());
                            }

                        }
                    }
                }

                if(context.Request.QueryString["OpenTasks"]!=null && context.Request.QueryString["OpenTasks"].ToString()=="true")
                {
                    string sTaskTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Task' AND AccountID="
                                 + sAccountID + " AND IsActive=1");
                    DataTable dtOpenTasks = Common.DataTableFromText(@"SELECT R.RecordID,R.V020,R.V004,R.V001,R.V002 FROM [Record] R WHERE R.TableID="+sTaskTableID+@" AND R.IsActive=1
                          AND ISDATE(V017) =1 AND ISDATE(V018) =1 AND CONVERT(Date, GETDATE())>= CONVERT(Date,R.V017,103) AND CONVERT(Date, GETDATE())<= CONVERT(Date,R.V018,103) AND (R.V033<>'Yes' OR R.V033 IS NULL)");

                    if(dtOpenTasks!=null && dtOpenTasks.Rows.Count>0)
                    {
                        foreach(DataRow drOpenTasks in dtOpenTasks.Rows)
                        {

                            if( !string.IsNullOrEmpty(drOpenTasks["V004"].ToString()) )
                            {
                                try
                                {
                                    LocationColumn theLocationColumn = JSONField.GetTypedObject<LocationColumn>(drOpenTasks["V004"].ToString());
                                    if(theLocationColumn!=null)
                                    {
                                        EachLocationPinInfo aPin = new EachLocationPinInfo();
                                        aPin.pintype = "o";
                                        aPin.lat = theLocationColumn.Latitude.ToString();
                                        aPin.lon =  theLocationColumn.Longitude.ToString();
                                        //Record theContractorRecord = new Record();
                                        if( !string.IsNullOrEmpty(drOpenTasks["V020"].ToString()) )
                                        {
                                            //theContractorRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drOpenTasks["V020"].ToString()), null, false);
                                            //if(theContractorRecord!=null && !string.IsNullOrEmpty(theContractorRecord.V008))
                                            //{
                                            //    aPin.userid = int.Parse(theContractorRecord.V008);
                                            //}
                                            aPin.userid = int.Parse(drOpenTasks["V020"].ToString());
                                        }
                                        string sPinTitle = drOpenTasks["V002"].ToString();
                                        Record theClientRecord = new Record();
                                        if( !string.IsNullOrEmpty(drOpenTasks["V001"].ToString()) )
                                        {
                                            theClientRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drOpenTasks["V001"].ToString()), null, false);
                                            if(theClientRecord!=null && !string.IsNullOrEmpty(theClientRecord.V001))
                                            {
                                                sPinTitle = sPinTitle + " - " + theClientRecord.V001;
                                            }
                                        }
                                        aPin.title = sPinTitle;
                                        aPin.pin = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath + "/Pages/Record/PinImages/OpenTask.png";
                                        //aPin.rings = theTable.DistanceRings;
                                        //aPin.order = theTable.PinDisplayOrder;
                                        //aPin.onStart = theTable.ShowOnStart;
                                        //aPin.ssid = drRecord["RecordID"].ToString();                                    

                                        aPin.url = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?fixedurl=" + Cryptography.Encrypt("~/Default.aspx") + "&stackzero=yes&mode=" + Cryptography.Encrypt("view") +
                                            "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt(sTaskTableID) + "&Recordid=" +
                                            Cryptography.Encrypt(drOpenTasks["RecordID"].ToString());
                                        string strPopup = "test popup </br>need "+aPin.title+" detail info from contractor or user table";//need detail info about the contractor.
                                        aPin.mappopup = strPopup;
                                        aPin.recordid = int.Parse(drOpenTasks["RecordID"].ToString());

                                        sb.Append(",");
                                        sb.Append(aPin.GetJSONString());
                                    }
                                }
                                catch
                                {

                                }
                            }
                        }
                    }
                }

                if(context.Request.QueryString["CompletedTasks"]!=null && context.Request.QueryString["CompletedTasks"].ToString()=="true")
                {
                    string sTaskTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Task' AND AccountID="
                                 + sAccountID + " AND IsActive=1");
                    DataTable dtCompletedTasks = Common.DataTableFromText(@"SELECT R.RecordID,R.V020,R.V004,R.V001,R.V002 FROM [Record] R WHERE R.TableID="+sTaskTableID+@" AND R.IsActive=1
                          AND ISDATE(V017) =1 AND ISDATE(V018) =1 AND CONVERT(Date, GETDATE())>= CONVERT(Date,R.V017,103) AND CONVERT(Date, GETDATE())<= CONVERT(Date,R.V018,103) AND R.V033='Yes'");

                    if(dtCompletedTasks!=null && dtCompletedTasks.Rows.Count>0)
                    {
                        foreach(DataRow drCompletedTasks in dtCompletedTasks.Rows)
                        {

                            if( !string.IsNullOrEmpty(drCompletedTasks["V004"].ToString()) )
                            {
                                try
                                {
                                    LocationColumn theLocationColumn = JSONField.GetTypedObject<LocationColumn>(drCompletedTasks["V004"].ToString());
                                    if(theLocationColumn!=null)
                                    {
                                        EachLocationPinInfo aPin = new EachLocationPinInfo();
                                        aPin.pintype = "c";
                                        aPin.lat = theLocationColumn.Latitude.ToString();
                                        aPin.lon =  theLocationColumn.Longitude.ToString();
                                        //Record theContractorRecord = new Record();
                                        if( !string.IsNullOrEmpty(drCompletedTasks["V020"].ToString()) )
                                        {
                                            //theContractorRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drCompletedTasks["V020"].ToString()), null, false);
                                            //if(theContractorRecord!=null && !string.IsNullOrEmpty(theContractorRecord.V008))
                                            //{
                                            //    aPin.userid = int.Parse(theContractorRecord.V008);
                                            //}
                                            aPin.userid = int.Parse(drCompletedTasks["V020"].ToString());
                                        }
                                        string sPinTitle = drCompletedTasks["V002"].ToString();
                                        Record theClientRecord = new Record();
                                        if( !string.IsNullOrEmpty(drCompletedTasks["V001"].ToString()) )
                                        {
                                            theClientRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drCompletedTasks["V001"].ToString()), null, false);
                                            if(theClientRecord!=null && !string.IsNullOrEmpty(theClientRecord.V001))
                                            {
                                                sPinTitle = sPinTitle + " - " + theClientRecord.V001;
                                            }
                                        }

                                        aPin.pin = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath + "/Pages/Record/PinImages/CompletedTask.png";
                                        //aPin.rings = theTable.DistanceRings;
                                        //aPin.order = theTable.PinDisplayOrder;
                                        //aPin.onStart = theTable.ShowOnStart;
                                        //aPin.ssid = drRecord["RecordID"].ToString();                                    
                                        aPin.title = sPinTitle;
                                        aPin.url = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?fixedurl=" + Cryptography.Encrypt("~/Default.aspx") + "&stackzero=yes&mode=" + Cryptography.Encrypt("view") +
                                            "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt(sTaskTableID) + "&Recordid=" +
                                            Cryptography.Encrypt(drCompletedTasks["RecordID"].ToString());
                                        string strPopup = "test popuo </br>need "+aPin.title+" detail info from contractor or user table";//need detail info about the contractor.
                                        aPin.mappopup = strPopup;
                                        aPin.recordid = int.Parse(drCompletedTasks["RecordID"].ToString());

                                        //
                                        //Record theTaskRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drCompletedTasks["RecordID"].ToString()), null, false);

                                        //string strWidthStuff = "width='100%'";
                                        //int iHeight = 450;
                                        //string retVal = String.Format("<table width='100%'><tr><td align='left'><div class='calendar-wrapper'><iframe  id='iframe" + section.DocumentSectionID.ToString()
                                        //+ theTaskRecord.RecordID.ToString() + "' frameBorder='0' " + strWidthStuff + " height='"
                                        //+ iHeight.ToString() + "px' scrolling='no'   src=\"{0}/Pages/Record/RecordDisplay.aspx?TableID={1}&Recordid={2}\"></iframe></div></td></tr></table>",
                                        //HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath,
                                        //Cryptography.Encrypt(theTaskRecord.TableID.ToString()), Cryptography.Encrypt(theTaskRecord.RecordID.ToString())
                                        //);
                                        //aPin.mappopup = retVal;
                                        //




                                        sb.Append(",");
                                        sb.Append(aPin.GetJSONString());
                                    }
                                }
                                catch
                                {

                                }
                            }
                        }
                    }

                }


                if(context.Request.QueryString["taskrecordid"]!=null)
                {

                    Record theTaskRecord = RecordManager.ets_Record_Detail_Full(int.Parse(context.Request.QueryString["taskrecordid"].ToString()), null, false);
                    EachLocationPinInfo aPin = new EachLocationPinInfo();
                    string strWidthStuff = "width='1200px'";
                    int iHeight = 450;
                    string sTaskContent = "";
                    //sTaskContent = String.Format("<table width='100%'><tr><td align='left'><div ><iframe  id='iframe" + section.DocumentSectionID.ToString()
                    //+ theTaskRecord.RecordID.ToString() + "' frameBorder='0' " + strWidthStuff + " height='"
                    //+ iHeight.ToString() + "px' scrolling='auto'   src=\"{0}/Pages/Record/RecordDisplay.aspx?TableID={1}&Recordid={2}\"></iframe></div></td></tr></table>",
                    //HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath,
                    //Cryptography.Encrypt(theTaskRecord.TableID.ToString()),Cryptography.Encrypt(theTaskRecord.RecordID.ToString())
                    //);

                    Content theContent = new Content();
                    if(theTaskRecord.V033=="Yes")
                    {
                        theContent=SystemData.Content_Details_ByKey("ontask map completed task info", int.Parse(sAccountID));
                    }
                    else
                    {
                        theContent=SystemData.Content_Details_ByKey("ontask map open task info", int.Parse(sAccountID));
                    }

                    if(theContent!=null)
                    {
                        sTaskContent = theContent.ContentP;
                        if(!string.IsNullOrEmpty(theTaskRecord.V001) && Common.isNumeric(theTaskRecord.V001))
                        {
                            Record theClientRecord = RecordManager.ets_Record_Detail_Full(int.Parse(theTaskRecord.V001), null, false);
                            if(theClientRecord!=null)
                            {
                                sTaskContent = sTaskContent.Replace("[Client]", theClientRecord.V001);
                            }
                        }

                        sTaskContent = sTaskContent.Replace("[Task Name]", theTaskRecord.V002);
                        sTaskContent = sTaskContent.Replace("[Date and Time]", theTaskRecord.V003);
                        sTaskContent = sTaskContent.Replace("[Signature Required]", theTaskRecord.V005);
                        sTaskContent = sTaskContent.Replace("[Checklist To Be Completed]", theTaskRecord.V006);
                        sTaskContent = sTaskContent.Replace("[Client's Email]", theTaskRecord.V007);
                        sTaskContent = sTaskContent.Replace("[Client's Mobile]", theTaskRecord.V008);
                        sTaskContent = sTaskContent.Replace("[Notes]", theTaskRecord.V009);
                        sTaskContent = sTaskContent.Replace("[Status]", theTaskRecord.V011);
                        sTaskContent = sTaskContent.Replace("[Sign By]", theTaskRecord.V012);
                        sTaskContent = sTaskContent.Replace("[Signed Date]", theTaskRecord.V013);
                        sTaskContent = sTaskContent.Replace("[Signature]", theTaskRecord.V014);
                        sTaskContent = sTaskContent.Replace("[Start]", theTaskRecord.V017);
                        sTaskContent = sTaskContent.Replace("[End]", theTaskRecord.V018);
                        sTaskContent = sTaskContent.Replace("[Reason]", theTaskRecord.V019);

                        if(!string.IsNullOrEmpty(theTaskRecord.V020) && Common.isNumeric(theTaskRecord.V020))
                        {
                            //Record theContractorRecord = RecordManager.ets_Record_Detail_Full(int.Parse(theTaskRecord.V020), null, false);
                            //if(theContractorRecord!=null)
                            //{
                            //    sTaskContent = sTaskContent.Replace("[Contractor]", theContractorRecord.V001);
                            //}
                            User theContractor = SecurityManager.User_Details(int.Parse(theTaskRecord.V020));
                            if(theContractor!=null)
                            {
                                sTaskContent = sTaskContent.Replace("[Contractor]", theContractor.FirstName + " " + theContractor.LastName);
                            }
                        }
                        sTaskContent = sTaskContent.Replace("[Invoiced]", theTaskRecord.V021);
                        sTaskContent = sTaskContent.Replace("[Date Completed]", theTaskRecord.V023);
                        sTaskContent = sTaskContent.Replace("[Date Invoiced]", theTaskRecord.V024);
                        sTaskContent = sTaskContent.Replace("[Readable Address]", theTaskRecord.V025);
                        sTaskContent = sTaskContent.Replace("[Invite Status]", theTaskRecord.V026);

                        if(!string.IsNullOrEmpty(theTaskRecord.V027) && Common.isNumeric(theTaskRecord.V027))
                        {
                            //Record theContractorRecord = RecordManager.ets_Record_Detail_Full(int.Parse(theTaskRecord.V027), null, false);
                            //if(theContractorRecord!=null)
                            //{
                            //    sTaskContent = sTaskContent.Replace("[Invite Contractor]", theContractorRecord.V001);
                            //}
                            User theInviteContractor = SecurityManager.User_Details(int.Parse(theTaskRecord.V027));
                            if(theInviteContractor!=null)
                            {
                                sTaskContent = sTaskContent.Replace("[Contractor]", theInviteContractor.FirstName + " " + theInviteContractor.LastName);
                            }
                        }
                        sTaskContent = sTaskContent.Replace("[Reminder (minutes)]", theTaskRecord.V028);
                        sTaskContent = sTaskContent.Replace("[IsRecurring]", theTaskRecord.V029);
                        sTaskContent = sTaskContent.Replace("[Date Forwarded]", theTaskRecord.V030);
                        sTaskContent = sTaskContent.Replace("[Task Completion Location]", theTaskRecord.V031);
                        sTaskContent = sTaskContent.Replace("[Task sign off location]", theTaskRecord.V032);
                        sTaskContent = sTaskContent.Replace("[Task Completed]", theTaskRecord.V033);
                        sTaskContent = sTaskContent.Replace("[Cancellation Reason]", theTaskRecord.V034);
                        sTaskContent = sTaskContent.Replace("[Cancellation Other]", theTaskRecord.V035);
                    }



                    aPin.mappopup = sTaskContent;
                    sb.Append(",");
                    sb.Append(aPin.GetJSONString());


                }


                strJSON = sb.ToString();
                if (!String.IsNullOrEmpty(strJSON))
                {
                    strJSON = "[" + strJSON.Substring(1) + "]";
                }


                context.Response.Write(strJSON);
                ////Response.Flush();
                ////Response.End();
                //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
            }




            if (context.Request.QueryString["userpath"] != null)
            {
                context.Response.Clear();
                context.Response.ContentType = "application/json";
                string sUserID = context.Request.QueryString["userid"].ToString();
                User theUser = SecurityManager.User_Details(int.Parse(sUserID));
                Account theAccount = SecurityManager.Account_Details(int.Parse(sAccountID));

                string sUserContent = "";
                if(theUser!=null)
                {
                    Content theContent=SystemData.Content_Details_ByKey("ontask map user info", int.Parse(sAccountID));
                    if(theContent!=null)
                    {
                        sUserContent = theContent.ContentP;
                        if(!string.IsNullOrEmpty(theUser.ProfilePicture))
                        {
                            sUserContent = sUserContent.Replace("[ProfilePicture]", theUser.ProfilePicture);
                        }

                        //string sContractorTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE SystemName='Contractor' AND AccountID="
                        //         + sAccountID + " AND IsActive=1");
                        //if(!string.IsNullOrEmpty(sContractorTableID))
                        //{
                        //    string sContractorRecordID = Common.GetValueFromSQL("SELECT RecordID FROM [Record] WHERE TableID=" +
                        //              sContractorTableID + " AND V008='" + theUser.UserID.ToString() + "' AND IsActive=1");
                        //    if(!string.IsNullOrEmpty(sContractorRecordID) && Common.isNumeric(sContractorRecordID))
                        //    {
                        //        Record theContractorRecord = RecordManager.ets_Record_Detail_Full(int.Parse(sContractorRecordID), null, false);
                        //        if(theContractorRecord!=null)
                        //        {
                        //            sUserContent = sUserContent.Replace("[Full Name]", theContractorRecord.V001);
                        //            sUserContent = sUserContent.Replace("[Phone]", theContractorRecord.V003);
                        //            sUserContent = sUserContent.Replace("[Email]", theContractorRecord.V004);
                        //        }
                        //    }
                        //}

                        if(theUser!=null)
                        {
                            sUserContent = sUserContent.Replace("[Full Name]", theUser.FirstName + " " + theUser.LastName);
                            sUserContent = sUserContent.Replace("[Phone]", theUser.PhoneNumber);
                            sUserContent = sUserContent.Replace("[Email]", theUser.Email);
                        }


                    }
                }


                string sNumberOfLocation = "1000";
                if (theAccount.AccountTypeID == 1 )//&& theAccount.ExpiryDate < DateTime.Today
                {
                    sNumberOfLocation = "1";
                }

                DataTable dtUserLocation = Common.DataTableFromText("SELECT top "+sNumberOfLocation+" * FROM [UserLocation] WHERE  CONVERT(Date, LastSeen)=CONVERT(Date, GETDATE()) AND  UserID="+sUserID+" ORDER BY UserLocationID desc");

                if(dtUserLocation!=null && dtUserLocation.Rows.Count==0)
                {
                    dtUserLocation = Common.DataTableFromText("SELECT top 1 * FROM [UserLocation] WHERE   UserID="+sUserID+" ORDER BY UserLocationID desc");
                }

                StringBuilder sb = new StringBuilder();
                strJSON = "";
                int iCount = 0;
                foreach(DataRow dr in dtUserLocation.Rows)
                {
                    if (dr["Latitude"].ToString() != "" && dr["Longitude"].ToString() != "")
                    {
                        EachLocationPinInfo aPin = new EachLocationPinInfo();
                        string strDateAdded = "";
                        if (dr["DateAdded"].ToString()!="")
                        {
                            try
                            {
                                strDateAdded = ((DateTime)dr["DateAdded"]).ToString("hh:mm");
                            }
                            catch
                            {
                                //
                            }
                        }

                        string strLastSeen = "";
                        if (dr["LastSeen"].ToString()!="")
                        {
                            try
                            {
                                strLastSeen = ((DateTime)dr["LastSeen"]).ToString("hh:mm");
                            }
                            catch
                            {
                                //
                            }
                        }

                        string strTitle = "";
                        strTitle = strLastSeen;
                        if(strLastSeen!=strDateAdded)
                        {
                            strTitle = strDateAdded + " - " + strLastSeen;
                        }

                        aPin.lat = dr["Latitude"].ToString();
                        aPin.lon = dr["Longitude"].ToString();
                        aPin.title = strTitle;
                        if(iCount==0)
                        {
                            aPin.mappopup = sUserContent;//put it only the 1st pin .
                        }
                        iCount = iCount + 1;


                        sb.Append(",");
                        sb.Append(aPin.GetJSONString());
                    }
                }
                strJSON = sb.ToString();
                if (!String.IsNullOrEmpty(strJSON))
                {
                    strJSON = "[" + strJSON.Substring(1) + "]";
                }

                context.Response.Write(strJSON);

            }


            if (context.Request.QueryString["Location"] != null)
            {
                context.Response.Clear();
                context.Response.ContentType = "application/json";
                int TableID = 0;
                Int32.TryParse(Convert.ToString(context.Request.QueryString["TableID"]), out TableID);
                DocumentSection section = new DocumentSection();
                using (DocGenDataContext ctx = new DocGenDataContext())
                {
                    section = ctx.DocumentSections.SingleOrDefault<DocumentSection>(s => s.DocumentSectionID == int.Parse(context.Request.QueryString["DocumentSectionID"].ToString()));

                }
                string sMapSearch = "";
                MapSectionDetail mapDetail = new MapSectionDetail();

                if (context.Request.QueryString["sMapSearch"] != null)
                {
                    sMapSearch = context.Request.QueryString["sMapSearch"].ToString();

                }
                if (section.Details != "")
                {
                    mapDetail = JSONField.GetTypedObject<MapSectionDetail>(section.Details);

                }

                Column theSearchColumn = null;
                Column thePinControlColumn = null;

                if (mapDetail.ShowLocation != null && mapDetail.ShowLocation.ToString() == TableID.ToString())
                {
                    if (sMapSearch!="" && mapDetail.SearchColumnID != null)
                    {
                        theSearchColumn = RecordManager.ets_Column_Details((int)mapDetail.SearchColumnID);
                    }

                    if (mapDetail.PinControlColumnID != null)
                    {
                        thePinControlColumn = RecordManager.ets_Column_Details((int)mapDetail.PinControlColumnID);
                    }
                }

                DataTable dtPinControlInfo = null;

                if (thePinControlColumn != null && mapDetail != null && !string.IsNullOrEmpty(mapDetail.PinControlValueInfo))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(mapDetail.PinControlValueInfo);

                    XmlTextReader r = new XmlTextReader(new StringReader(xmlDoc.OuterXml));

                    DataSet ds = new DataSet();
                    ds.ReadXml(r);
                    if (ds.Tables[0] != null)
                    {
                        dtPinControlInfo = ds.Tables[0];
                    }
                }

                StringBuilder sb = new StringBuilder();

                DataTable dtSysName = Common.DataTableFromText(@"SELECT SystemName,TableID,MapPinHoverColumnID,ColumnID  From [Column] WHERE 
                    TableID=" + TableID.ToString() + @" AND  ColumnType='location' ORDER BY DisplayOrder");


                DataTable dtListOfLatLong = new DataTable();
                dtListOfLatLong.Columns.Add("Latitude", typeof(double));
                dtListOfLatLong.Columns.Add("Longitude", typeof(double));
                dtListOfLatLong.AcceptChanges();

                if (TableID == -1)
                {
                    dtSysName = Common.DataTableFromText(@" SELECT   DISTINCT  [Column].SystemName, [Table].TableID ,MapPinHoverColumnID,ColumnID,PinDisplayOrder
                            FROM         [Column] INNER JOIN
                                                  [Table] ON [Column].TableID = [Table].TableID 
                                                  WHERE [Column].ColumnType='location' AND [Table].IsActive=1 AND [Table].AccountID="
                        + context.Session["AccountID"].ToString() + @" ORDER BY PinDisplayOrder");
                }

                string sSearchClause = "";
                if(theSearchColumn!=null)
                {
                    sSearchClause = " AND " + theSearchColumn.SystemName + " LIKE '%" + sMapSearch.Replace("'", "''") + "%' ";
                }

                foreach (DataRow drSys in dtSysName.Rows)
                {

                    Column theMapPinHoderColumnID = null;
                    Column theColumn = RecordManager.ets_Column_Details(int.Parse(drSys["ColumnID"].ToString()));
                    if (drSys["MapPinHoverColumnID"] != DBNull.Value)
                    {
                        if (drSys["MapPinHoverColumnID"].ToString() != "")
                        {
                            theMapPinHoderColumnID = RecordManager.ets_Column_Details(int.Parse(drSys["MapPinHoverColumnID"].ToString()));
                        }
                    }

                    Table theTable = RecordManager.ets_Table_Details(int.Parse(drSys["TableID"].ToString()));
                    //DataTable dtRecords = Common.DataTableFromText(" SELECT " + drSys["SystemName"].ToString() + ",RecordID FROM Record WHERE TableID=" + drSys["TableID"].ToString() + " AND IsActive=1 AND " + drSys["SystemName"].ToString() + " IS NOT NULL");

                    DataTable dtRecords = Common.DataTableFromText(" SELECT * FROM Record WHERE TableID=" + drSys["TableID"].ToString() + " AND IsActive=1 AND " + drSys["SystemName"].ToString() + " IS NOT NULL" + sSearchClause);


                    if (theTable.PinImage == "")
                    {
                        theTable.PinImage = "Pages/Record/PINImages/DefaultPin.png";
                    }

                    foreach (DataRow drRecord in dtRecords.Rows)
                    {

                        if (drRecord[drSys["SystemName"].ToString()] != DBNull.Value)
                        {
                            if (drRecord[drSys["SystemName"].ToString()].ToString() != "")
                            {
                                try
                                {
                                    LocationColumn theLocationColumn = JSONField.GetTypedObject<LocationColumn>(drRecord[drSys["SystemName"].ToString()].ToString());
                                    if (theLocationColumn != null)
                                    {

                                        if (theLocationColumn.Latitude != null && theLocationColumn.Longitude != null)
                                        {
                                            if (TableID == -1)
                                            {
                                                if (IsLatLongExist(dtListOfLatLong, theLocationColumn))
                                                {
                                                    continue;
                                                }

                                                DataRow drNewLL = dtListOfLatLong.NewRow();
                                                //KG 22/8/17 Use TryParse instead of Parse
                                                //drNewLL[0] = double.Parse(theLocationColumn.Latitude);
                                                //drNewLL[1] = double.Parse(theLocationColumn.Longitude);
                                                double dLatitude;
                                                double dLongitute;
                                                if (double.TryParse(theLocationColumn.Latitude, out dLatitude) && double.TryParse(theLocationColumn.Longitude, out dLongitute))
                                                {
                                                    drNewLL[0] = dLatitude;
                                                    drNewLL[1] = dLongitute;
                                                    dtListOfLatLong.Rows.Add(drNewLL);
                                                }
                                                else
                                                {
                                                    continue;
                                                }

                                            }

                                            string strTT = "VIEW";
                                            if (theMapPinHoderColumnID != null)
                                            {
                                                strTT = Common.GetValueFromSQL("SELECT " + theMapPinHoderColumnID.SystemName + " FROM Record WHERE RecordID=" + drRecord["RecordID"].ToString());

                                                strTT = TheDatabaseS.EscapeStringValue(strTT);
                                            }

                                            string strPopup = strTT;
                                            if (theColumn.MapPopup != "")
                                            {
                                                Record thisRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drRecord["RecordID"].ToString()),null,false);
                                                DataTable _dtRecordColums = RecordManager.ets_Table_Columns_Summary((int)thisRecord.TableID, null);

                                                strPopup = theColumn.MapPopup;//"yes"; //



                                                DataTable dtColumns = Common.DataTableFromText("SELECT SystemName,DisplayName FROM [Column] WHERE IsStandard=0 AND   TableID="
                              + thisRecord.TableID.ToString() + "  ORDER BY DisplayName");
                                                foreach (DataRow drC in dtColumns.Rows)
                                                {
                                                    strPopup = strPopup.Replace("[" + drC["DisplayName"].ToString() + "]", drRecord[drC["SystemName"].ToString()].ToString());

                                                }


                                                //Work with 1 top level Parent tables.

                                                try
                                                {

                                                    if (strPopup.IndexOf("[") > -1 && strPopup.IndexOf(":") > 1 && strPopup.IndexOf("]") > -1)
                                                    {

                                                        DataTable dtPT = Common.DataTableFromText("SELECT distinct ParentTableID FROM TableChild WHERE ChildTableID="
                                                       + thisRecord.TableID.ToString()); //AND DetailPageType<>'not'

                                                        if (dtPT.Rows.Count > 0)
                                                        {

                                                            foreach (DataRow drPT in dtPT.Rows)
                                                            {

                                                                if (strPopup.IndexOf("[") > -1 && strPopup.IndexOf(":") > 1 && strPopup.IndexOf("]") > -1)
                                                                {
                                                                    //
                                                                }
                                                                else
                                                                {
                                                                    break;
                                                                }

                                                                for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                                                                {
                                                                    if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
                                                               && (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
                                                               || _dtRecordColums.Rows[i]["DropDownType"].ToString() == "tabledd")
                                                                && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
                                                               && _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
                                                                    {
                                                                        if (_dtRecordColums.Rows[i]["TableTableID"].ToString() == drPT["ParentTableID"].ToString())
                                                                        {
                                                                            if (drRecord[_dtRecordColums.Rows[i]["SystemName"].ToString()].ToString() != "")
                                                                            {


                                                                                Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["LinkedParentColumnID"].ToString()));

                                                                                //DataTable dtParentRecord = Common.DataTableFromText("SELECT * FROM Record WHERE RecordID=" + drRecord[_dtRecordColums.Rows[i]["SystemName"].ToString()].ToString());


                                                                                DataTable dtParentRecord = null;
                                                                                if (theLinkedColumn.SystemName.ToLower() == "recordid")
                                                                                {
                                                                                    dtParentRecord = Common.DataTableFromText("SELECT * FROM Record WHERE RecordID=" + drRecord[_dtRecordColums.Rows[i]["SystemName"].ToString()].ToString());
                                                                                }
                                                                                else
                                                                                {
                                                                                    dtParentRecord = Common.DataTableFromText("SELECT * FROM Record WHERE TableID=" + theLinkedColumn.TableID.ToString() + " AND " + theLinkedColumn.SystemName + "='" +
                                                                                        drRecord[_dtRecordColums.Rows[i]["SystemName"].ToString()].ToString().Replace("'", "''") + "'");
                                                                                }

                                                                                if (dtParentRecord.Rows.Count > 0)
                                                                                {

                                                                                    DataTable dtColumnsPT = Common.DataTableFromText(@"SELECT distinct SystemName, TableName + ':' + DisplayName AS DP FROM [Column] INNER JOIN [Table]
                                        ON [Column].TableID=[Table].TableID WHERE  [Column].IsStandard=0 AND  [Column].TableID=" + drPT["ParentTableID"].ToString());
                                                                                    foreach (DataRow drC in dtColumnsPT.Rows)
                                                                                    {
                                                                                        strPopup = strPopup.Replace("[" + drC["DP"].ToString() + "]", dtParentRecord.Rows[0][drC["SystemName"].ToString()].ToString());

                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    }

                                                }
                                                catch
                                                {
                                                    //
                                                }



                                            }


                                            string strPinImage = theTable.PinImage;



                                            if (dtPinControlInfo != null && thePinControlColumn != null
                                                && drRecord[thePinControlColumn.SystemName].ToString() != "")
                                            {
                                                foreach(DataRow drPC in dtPinControlInfo.Rows)
                                                {
                                                    if (drRecord[thePinControlColumn.SystemName].ToString() == drPC["value"].ToString())
                                                    {
                                                        if (drPC["custom1"].ToString() != "")
                                                        {
                                                            strPinImage = drPC["custom1"].ToString();
                                                        }
                                                        break;
                                                    }
                                                }
                                            }

                                            try
                                            {
                                                if (!string.IsNullOrEmpty(theTable.PinImageAdvanced))
                                                {
                                                    DataTable dtPinControlInfoT = null;
                                                    XmlDocument xmlDoc = new XmlDocument();
                                                    xmlDoc.LoadXml(theTable.PinImageAdvanced);

                                                    XmlTextReader r = new XmlTextReader(new StringReader(xmlDoc.OuterXml));

                                                    DataSet ds = new DataSet();
                                                    ds.ReadXml(r);
                                                    if (ds.Tables[0] != null)
                                                    {
                                                        dtPinControlInfoT = ds.Tables[0];

                                                        foreach (DataRow drPC in dtPinControlInfoT.Rows)
                                                        {
                                                            Column thePinControlColumnT = RecordManager.ets_Column_Details(int.Parse(drPC["columnid"].ToString()));
                                                            if (drRecord[thePinControlColumnT.SystemName].ToString() == drPC["value"].ToString())
                                                            {
                                                                if (drPC["ImageFile"].ToString() != "")
                                                                {
                                                                    strPinImage = drPC["ImageFile"].ToString();
                                                                }
                                                                break;
                                                            }
                                                        }

                                                    }
                                                }
                                            }
                                            catch
                                            {
                                                //
                                            }



                                            EachPinInfo aPin = new EachPinInfo();
                                            aPin.lat = theLocationColumn.Latitude.ToString();
                                            aPin.lon = theLocationColumn.Longitude.ToString();
                                            aPin.title = strTT;
                                            aPin.pin = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath + "/" + strPinImage;
                                            aPin.rings = theTable.DistanceRings;
                                            aPin.order = theTable.PinDisplayOrder;
                                            aPin.onStart = theTable.ShowOnStart;
                                            aPin.ssid = drRecord["RecordID"].ToString();
                                            aPin.url = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?fixedurl=" + Cryptography.Encrypt("~/Default.aspx") + "&stackzero=yes&mode=" + Cryptography.Encrypt("view") +
                                                "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt(drSys["TableID"].ToString()) + "&Recordid=" +
                                                Cryptography.Encrypt(drRecord["RecordID"].ToString());
                                            aPin.mappopup = strPopup;

                                            sb.Append(",");
                                            sb.Append(aPin.GetJSONString());

                                            //sb.Append(",{");

                                            //sb.Append(String.Format("\"lat\":{0}, \"lon\":{1}, \"title\":\"{2}\", \"pin\":\"{3}\", \"ssid\":\"{4}\", \"url\":\"{5}\", \"mappopup\":\"{6}\"", theLocationColumn.Latitude.ToString(), theLocationColumn.Longitude.ToString(),
                                            //       HttpUtility.JavaScriptStringEncode(strTT),HttpUtility.JavaScriptStringEncode( Request.Url.Scheme +"://" + context.Request.Url.Authority + context.Request.ApplicationPath + "/" +
                                            //       theTable.PinImage),HttpUtility.JavaScriptStringEncode( drRecord["RecordID"].ToString()),HttpUtility.JavaScriptStringEncode( Request.Url.Scheme +"://" + context.Request.Url.Authority +
                                            //       context.Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?fixedurl=" + Cryptography.Encrypt("~/Default.aspx") + "&stackzero=yes&mode=" + Cryptography.Encrypt("view") +
                                            //       "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt(drSys["TableID"].ToString()) + "&Recordid=" +
                                            //       Cryptography.Encrypt(drRecord["RecordID"].ToString())), HttpUtility.JavaScriptStringEncode(strPopup)));





                                            //sb.Append("}");
                                        }

                                    }

                                }
                                catch (Exception ex)
                                {
                                    ErrorLog theErrorLog = new ErrorLog(null, "Get location each error", ex.Message, ex.StackTrace, DateTime.Now, context.Request.Path);
                                    SystemData.ErrorLog_Insert(theErrorLog);

                                }
                            }

                        }



                    }

                }

                strJSON = sb.ToString();
                if (!String.IsNullOrEmpty(strJSON))
                {
                    strJSON = "[" + strJSON.Substring(1) + "]";
                }


                context.Response.Write(strJSON);
                ////Response.Flush();
                ////Response.End();
                //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
            }




            //if (context.Request.QueryString["econtractorline"] != null)
            //{
            //    context.Response.Clear();
            //    context.Response.ContentType = "application/json";
            //    string strJobTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE TableName='Task' AND AccountID=" + sAccountID + " AND IsActive=1");
            //    //string sJob_StartDateSys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + strJobTableID + " AND DisplayName='Start Date'");
            //    //string sJob_EndDateSys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + strJobTableID + " AND DisplayName='End Date'");
            //    //string sJob_ContractorSys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + strJobTableID + " AND DisplayName='Contractor'");

            //    //string sJobRecordID = context.Request.QueryString["sRecordID"].ToString();
            //    //string sUserID = Common.GetValueFromSQL("SELECT " + sJob_ContractorSys + " FROM [Record] WHERE RecordID=" + sJobRecordID);//need to talk with JB & Lan
            //    string sUserID=context.Request.QueryString["sRecordID"].ToString();
            //    string strWhereClause = "";
            //    if (context.Request.QueryString["sDate"] != null)
            //    {
            //        string sDate = context.Request.QueryString["sDate"].ToString();
            //        if(sDate!="")
            //        {
            //            strWhereClause = " AND CONVERT(date,DateAdded,103)= CONVERT(date, '" + sDate + "',103) ";
            //        }
            //    }

            //    DataTable dtUserLocation = Common.DataTableFromText("SELECT TOP 1000 * FROM [UserLocation] WHERE UserID=" + sUserID + strWhereClause + " ORDER BY DateAdded");
            //    StringBuilder sb = new StringBuilder();
            //    strJSON = "";
            //    foreach(DataRow dr in dtUserLocation.Rows)
            //    {
            //        if (dr["Latitude"].ToString() != "" && dr["Longitude"].ToString() != "")
            //        {
            //            EachPinInfo aPin = new EachPinInfo();
            //            string strDateAdded = "";
            //            if (dr["DateAdded"].ToString()!="")
            //            {
            //                try
            //                {
            //                    strDateAdded = ((DateTime)dr["DateAdded"]).ToString("dd/MM/yyyy hh:mm");
            //                }
            //                catch
            //                {
            //                    //
            //                }
            //            }
            //            aPin.lat = dr["Latitude"].ToString();
            //            aPin.lon = dr["Longitude"].ToString();
            //            aPin.title = strDateAdded;
            //            sb.Append(",");
            //            sb.Append(aPin.GetJSONString());
            //        }
            //    }
            //    strJSON = sb.ToString();
            //    if (!String.IsNullOrEmpty(strJSON))
            //    {
            //        strJSON = "[" + strJSON.Substring(1) + "]";
            //    }


            //    context.Response.Write(strJSON);

            //}

            //if (context.Request.QueryString["econtractor"] != null)
            //{
            //    context.Response.Clear();
            //    context.Response.ContentType = "application/json";
            //    int iJobTableID = 0;
            //    //Job table is the main virtual table for Record
            //    string strAccountID = context.Session["AccountID"].ToString();
            //    int iAccountID = int.Parse(strAccountID);
            //    string strJobTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE TableName='Task' AND AccountID=" + strAccountID + " AND IsActive=1");
            //    string strClientTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE TableName='Client' AND AccountID=" + strAccountID + " AND IsActive=1");
            //    //string strContractorTableID = Common.GetValueFromSQL("SELECT TableID FROM [Table] WHERE TableName='Contractor' AND AccountID=" + strAccountID + " AND IsActive=1");

            //    //use system options
            //    int iTemp=0;
            //    if (int.TryParse(strJobTableID, out iTemp) == false)
            //        strJobTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Job TableID", iAccountID, null);
            //    if (int.TryParse(strClientTableID, out iTemp) == false)
            //        strClientTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Client TableID", iAccountID, null);
            //    //if (int.TryParse(strContractorTableID, out iTemp) == false)
            //    //    strContractorTableID = SystemData.SystemOption_ValueByKey_Account("eContractor Contractor TableID", iAccountID, null);




            //    string sJob_JobNameSys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + strJobTableID + " AND DisplayName='Task Name'");
            //    string sJob_ClientSys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + strJobTableID + " AND DisplayName='Client'");
            //    string sJob_ContractorSys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + strJobTableID + " AND DisplayName='Contractor'");
            //    string sJob_StartDateSys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + strJobTableID + " AND DisplayName='Start Date'");
            //    string sJob_EndDateSys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + strJobTableID + " AND DisplayName='End Date'");

            //    string sClient_NameSys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + strClientTableID + " AND DisplayName='Name'");
            //    //string sContractor_FullNameSys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + strContractorTableID + " AND DisplayName='FullName'");
            //    //string sContractor_LastSeenLocationSys = Common.GetValueFromSQL("SELECT SystemName FROM [Column] WHERE TableID=" + strContractorTableID + " AND DisplayName='Last Seen Location'");


            //    if (sJob_JobNameSys=="")
            //        sJob_JobNameSys = SystemData.SystemOption_ValueByKey_Account("eContractor Job_JobNameSys", iAccountID, null);
            //    if (sJob_ClientSys == "")
            //        sJob_ClientSys = SystemData.SystemOption_ValueByKey_Account("eContractor Job_ClientSys", iAccountID, null);

            //    if (sJob_ContractorSys == "")
            //        sJob_ContractorSys = SystemData.SystemOption_ValueByKey_Account("eContractor Job_ContractorSys", iAccountID, null);

            //    if (sJob_StartDateSys == "")
            //        sJob_StartDateSys = SystemData.SystemOption_ValueByKey_Account("eContractor Job_StartDateSys", iAccountID, null);
            //    if (sJob_EndDateSys == "")
            //        sJob_EndDateSys = SystemData.SystemOption_ValueByKey_Account("eContractor Job_EndDateSys", iAccountID, null);
            //    if (sClient_NameSys == "")
            //        sClient_NameSys = SystemData.SystemOption_ValueByKey_Account("eContractor Client_NameSys", iAccountID, null);
            //    //if (sContractor_FullNameSys == "")
            //    //    sContractor_FullNameSys = SystemData.SystemOption_ValueByKey_Account("eContractor Contractor_FullNameSys", iAccountID, null);
            //    //if (sContractor_LastSeenLocationSys == "")
            //    //    sContractor_LastSeenLocationSys = SystemData.SystemOption_ValueByKey_Account("eContractor Contractor_LastSeenLocationSys", iAccountID, null);







            //    Int32.TryParse(strJobTableID, out iJobTableID);
            //    DocumentSection section = new DocumentSection();
            //    using (DocGenDataContext ctx = new DocGenDataContext())
            //    {
            //        section = ctx.DocumentSections.SingleOrDefault<DocumentSection>(s => s.DocumentSectionID == int.Parse(context.Request.QueryString["DocumentSectionID"].ToString()));

            //    }
            //    string sClient = "";
            //    string sJob = "";
            //    string sContrator = "";
            //    string sDate = "";

            //    MapSectionDetail mapDetail = new MapSectionDetail();

            //    if (context.Request.QueryString["sClient"] != null)
            //        sClient = context.Request.QueryString["sClient"].ToString();
            //    if (context.Request.QueryString["sJob"] != null)
            //        sJob = context.Request.QueryString["sJob"].ToString();
            //    if (context.Request.QueryString["sContrator"] != null)
            //        sContrator = context.Request.QueryString["sContrator"].ToString();
            //    if (context.Request.QueryString["sDate"] != null)
            //        sDate = context.Request.QueryString["sDate"].ToString();

            //    if (section.Details != "")
            //    {
            //        mapDetail = JSONField.GetTypedObject<MapSectionDetail>(section.Details);

            //    }

            //    //Column theSearchColumn = null;
            //    Column thePinControlColumn = null;
            //    //if (mapDetail.ShowLocation != null && mapDetail.ShowLocation.ToString() == iJobTableID.ToString())
            //    //{
            //    //if (sMapSearch != "" && mapDetail.SearchColumnID != null)
            //    //{
            //    //    theSearchColumn = RecordManager.ets_Column_Details((int)mapDetail.SearchColumnID);
            //    //}

            //    if (mapDetail.PinControlColumnID != null)
            //    {
            //        thePinControlColumn = RecordManager.ets_Column_Details((int)mapDetail.PinControlColumnID);
            //    }
            //    //}

            //    DataTable dtPinControlInfo = null;
            //    if (thePinControlColumn != null && mapDetail != null && !string.IsNullOrEmpty(mapDetail.PinControlValueInfo))
            //    {
            //        XmlDocument xmlDoc = new XmlDocument();
            //        xmlDoc.LoadXml(mapDetail.PinControlValueInfo);

            //        XmlTextReader r = new XmlTextReader(new StringReader(xmlDoc.OuterXml));

            //        DataSet ds = new DataSet();
            //        ds.ReadXml(r);
            //        if (ds.Tables[0] != null)
            //        {
            //            dtPinControlInfo = ds.Tables[0];
            //        }
            //    }

            //    StringBuilder sb = new StringBuilder();

            //    DataTable dtSysName = Common.DataTableFromText(@"SELECT SystemName,TableID,MapPinHoverColumnID,ColumnID  From [Column] WHERE 
            //        TableID=" + iJobTableID.ToString() + @" AND  ColumnType='location' ORDER BY DisplayOrder");


            //    DataTable dtListOfLatLong = new DataTable();
            //    dtListOfLatLong.Columns.Add("Latitude", typeof(double));
            //    dtListOfLatLong.Columns.Add("Longitude", typeof(double));
            //    dtListOfLatLong.AcceptChanges();

            //    if (iJobTableID == -1)
            //    {
            //        dtSysName = Common.DataTableFromText(@" SELECT   DISTINCT  SystemName, [Table].TableID ,MapPinHoverColumnID,ColumnID,PinDisplayOrder
            //                FROM         [Column] INNER JOIN
            //                                      [Table] ON [Column].TableID = [Table].TableID 
            //                                      WHERE [Column].ColumnType='location' AND [Table].IsActive=1 AND [Table].AccountID="
            //            + context.Session["AccountID"].ToString() + @" ORDER BY PinDisplayOrder");
            //    }

            //    string sSearchClause = "";
            //    //if (theSearchColumn != null)
            //    //{
            //    //    sSearchClause = " AND " + theSearchColumn.SystemName + " LIKE '%" + sMapSearch.Replace("'", "''") + "%' ";
            //    //} 
            //    if (sDate == "")
            //        sDate = DateTime.Today.ToString("dd/MM/yyyy");


            //    sSearchClause = " AND (CONVERT(date,Record." + sJob_StartDateSys + ",103)<= CONVERT(date, '" + sDate + "',103) AND CONVERT(date,Record."
            //        + sJob_EndDateSys + ",103)>= CONVERT(date, '" + sDate + "',103)) ";


            //    if (sJob != "")
            //        sSearchClause = sSearchClause + " AND " + sJob_JobNameSys + " LIKE '%" + sJob.Replace("'", "''") + "%'";

            //    if (sClient != "")
            //        sSearchClause = sSearchClause + " AND " + sJob_ClientSys + " IN (SELECT RecordID FROM [Record] WHERE IsActive=1 AND TableID=" + strClientTableID
            //            + " AND " + sClient_NameSys + " LIKE '%" + sClient.Replace("'", "''") + "%')";

            //    //if (sContrator != "")
            //    //    sSearchClause = sSearchClause + " AND " + sJob_ContractorSys + " IN (SELECT RecordID FROM [Record] WHERE IsActive=1 AND TableID=" + strContractorTableID
            //    //        + " AND " + sContractor_FullNameSys + " LIKE '%" + sContrator.Replace("'", "''") + "%')";
            //    if (sContrator != "")
            //        sSearchClause = sSearchClause + " AND " + sJob_ContractorSys + " IN (SELECT UserID FROM [User] WHERE IsActive=1 "
            //            + " AND  (FirstName + ' ' + LastName)  LIKE '%" + sContrator.Replace("'", "''") + "%')";

            //    foreach (DataRow drSys in dtSysName.Rows)
            //    {

            //        Column theMapPinHoderColumnID = null;
            //        Column theColumn = RecordManager.ets_Column_Details(int.Parse(drSys["ColumnID"].ToString()));
            //        if (drSys["MapPinHoverColumnID"] != DBNull.Value)
            //        {
            //            if (drSys["MapPinHoverColumnID"].ToString() != "")
            //            {
            //                theMapPinHoderColumnID = RecordManager.ets_Column_Details(int.Parse(drSys["MapPinHoverColumnID"].ToString()));
            //            }
            //        }

            //        Table theTable = RecordManager.ets_Table_Details(int.Parse(drSys["TableID"].ToString()));
            //        DataTable dtRecords = Common.DataTableFromText(" SELECT * FROM Record WHERE TableID=" + drSys["TableID"].ToString()
            //            + " AND IsActive=1 AND " + drSys["SystemName"].ToString() + " IS NOT NULL" + sSearchClause);

            //        if (theTable.PinImage == "")
            //        {
            //            theTable.PinImage = "Pages/Record/PINImages/DefaultPin.png";
            //        }

            //        foreach (DataRow drRecord in dtRecords.Rows)
            //        {

            //            if (drRecord[drSys["SystemName"].ToString()] != DBNull.Value)
            //            {
            //                if (drRecord[drSys["SystemName"].ToString()].ToString() != "")
            //                {
            //                    try
            //                    {
            //                        //This is Job address
            //                        //LocationColumn theLocationColumn = JSONField.GetTypedObject<LocationColumn>(drRecord[drSys["SystemName"].ToString()].ToString());
            //                        LocationColumn theLocationColumn=null;
            //                        //string strContractorRecordID=drRecord[sJob_ContractorSys].ToString();
            //                        string sUserID=drRecord[sJob_ContractorSys].ToString();
            //                        //if(strContractorRecordID!="")//
            //                        //{
            //                        //    string strLastSeenAddress = Common.GetValueFromSQL("SELECT " + sContractor_LastSeenLocationSys + " FROM Record WHERE RecordID=" + strContractorRecordID);
            //                        //    if(strLastSeenAddress!="")
            //                        //    {
            //                        //        theLocationColumn = JSONField.GetTypedObject<LocationColumn>(strLastSeenAddress);
            //                        //    }
            //                        //}

            //                        if(sUserID!="")//
            //                        {
            //                            User theUser = SecurityManager.User_Details(int.Parse(sUserID));
            //                            if (theUser != null && !string.IsNullOrEmpty(theUser.Attributes))
            //                            {
            //                                     UserAttributes_ontask userAttributes= DocGen.DAL.JSONField.GetTypedObject<UserAttributes_ontask>(theUser.Attributes);
            //                                    if(userAttributes!=null)
            //                                    {
            //                                        if(!string.IsNullOrEmpty(userAttributes.LastSeenLocation))
            //                                        {
            //                                            theLocationColumn = DocGen.DAL.JSONField.GetTypedObject<LocationColumn>(userAttributes.LastSeenLocation);

            //                                        }                                                    
            //                                    }
            //                            }

            //                        }


            //                        if (theLocationColumn != null)
            //                        {

            //                            if (theLocationColumn.Latitude != null && theLocationColumn.Longitude != null)
            //                            {
            //                                if (iJobTableID == -1)
            //                                {
            //                                    if (IsLatLongExist(dtListOfLatLong, theLocationColumn))
            //                                    {
            //                                        continue;
            //                                    }

            //                                    DataRow drNewLL = dtListOfLatLong.NewRow();
            //                                    double dLatitude;
            //                                    double dLongitute;
            //                                    if (double.TryParse(theLocationColumn.Latitude, out dLatitude) && double.TryParse(theLocationColumn.Longitude, out dLongitute))
            //                                    {
            //                                        drNewLL[0] = dLatitude;
            //                                        drNewLL[1] = dLongitute;
            //                                        dtListOfLatLong.Rows.Add(drNewLL);
            //                                    }
            //                                    else
            //                                    {
            //                                        continue;
            //                                    }

            //                                }

            //                                string strTT = "VIEW";

            //                                if(drRecord[sJob_JobNameSys].ToString()!="")
            //                                {
            //                                    strTT=drRecord[sJob_JobNameSys].ToString();
            //                                }





            //                                if (theMapPinHoderColumnID != null)
            //                                {
            //                                    strTT = Common.GetValueFromSQL("SELECT " + theMapPinHoderColumnID.SystemName + " FROM Record WHERE RecordID=" + drRecord["RecordID"].ToString());

            //                                    strTT = TheDatabaseS.EscapeStringValue(strTT);
            //                                }

            //                                string strPopup = strTT;
            //                                if (theColumn.MapPopup != "")
            //                                {
            //                                    Record thisRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drRecord["RecordID"].ToString()),null,false);
            //                                    DataTable _dtRecordColums = RecordManager.ets_Table_Columns_Summary((int)thisRecord.TableID, null);

            //                                    strPopup = theColumn.MapPopup;//"yes"; //

            //                                    DataTable dtColumns = Common.DataTableFromText("SELECT SystemName,DisplayName,ColumnType FROM [Column] WHERE IsStandard=0 AND   TableID="
            //                  + thisRecord.TableID.ToString() + "  ORDER BY DisplayName");
            //                                    foreach (DataRow drC in dtColumns.Rows)
            //                                    {
            //                                        if (drC["ColumnType"].ToString() == "location")
            //                                        {
            //                                            if (strPopup.IndexOf("[" + drC["DisplayName"].ToString() + "]")>-1)
            //                                            {
            //                                                LocationColumn theLocationTemp = JSONField.GetTypedObject<LocationColumn>(drRecord[drC["SystemName"].ToString()].ToString());
            //                                                if (theLocationTemp!=null)
            //                                                {
            //                                                    strPopup = strPopup.Replace("[" + drC["DisplayName"].ToString() + "]", theLocationTemp.Address);
            //                                                }

            //                                            }
            //                                        }
            //                                        else
            //                                        {
            //                                            strPopup = strPopup.Replace("[" + drC["DisplayName"].ToString() + "]", drRecord[drC["SystemName"].ToString()].ToString());
            //                                        }
            //                                    }
            //                                    //Work with 1 top level Parent tables.
            //                                    try
            //                                    {

            //                                        if (strPopup.IndexOf("[") > -1 && strPopup.IndexOf(":") > 1 && strPopup.IndexOf("]") > -1)
            //                                        {

            //                                            DataTable dtPT = Common.DataTableFromText("SELECT distinct ParentTableID FROM TableChild WHERE ChildTableID="
            //                                           + thisRecord.TableID.ToString()); //AND DetailPageType<>'not'

            //                                            if (dtPT.Rows.Count > 0)
            //                                            {

            //                                                foreach (DataRow drPT in dtPT.Rows)
            //                                                {

            //                                                    if (strPopup.IndexOf("[") > -1 && strPopup.IndexOf(":") > 1 && strPopup.IndexOf("]") > -1)
            //                                                    {
            //                                                        //
            //                                                    }
            //                                                    else
            //                                                    {
            //                                                        break;
            //                                                    }

            //                                                    for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
            //                                                    {
            //                                                        if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
            //                                                   && (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
            //                                                   || _dtRecordColums.Rows[i]["DropDownType"].ToString() == "tabledd")
            //                                                    && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
            //                                                   && _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
            //                                                        {
            //                                                            if (_dtRecordColums.Rows[i]["TableTableID"].ToString() == drPT["ParentTableID"].ToString())
            //                                                            {
            //                                                                if (drRecord[_dtRecordColums.Rows[i]["SystemName"].ToString()].ToString() != "")
            //                                                                {


            //                                                                    Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["LinkedParentColumnID"].ToString()));

            //                                                                    //DataTable dtParentRecord = Common.DataTableFromText("SELECT * FROM Record WHERE RecordID=" + drRecord[_dtRecordColums.Rows[i]["SystemName"].ToString()].ToString());


            //                                                                    DataTable dtParentRecord = null;
            //                                                                    if (theLinkedColumn.SystemName.ToLower() == "recordid")
            //                                                                    {
            //                                                                        dtParentRecord = Common.DataTableFromText("SELECT * FROM Record WHERE RecordID=" + drRecord[_dtRecordColums.Rows[i]["SystemName"].ToString()].ToString());
            //                                                                    }
            //                                                                    else
            //                                                                    {
            //                                                                        dtParentRecord = Common.DataTableFromText("SELECT * FROM Record WHERE TableID=" + theLinkedColumn.TableID.ToString() + " AND " + theLinkedColumn.SystemName + "='" +
            //                                                                            drRecord[_dtRecordColums.Rows[i]["SystemName"].ToString()].ToString().Replace("'", "''") + "'");
            //                                                                    }

            //                                                                    if (dtParentRecord.Rows.Count > 0)
            //                                                                    {

            //                                                                        DataTable dtColumnsPT = Common.DataTableFromText(@"SELECT distinct SystemName, TableName + ':' + DisplayName AS DP FROM [Column] INNER JOIN [Table]
            //                            ON [Column].TableID=[Table].TableID WHERE  [Column].IsStandard=0 AND  [Column].TableID=" + drPT["ParentTableID"].ToString());
            //                                                                        foreach (DataRow drC in dtColumnsPT.Rows)
            //                                                                        {
            //                                                                            strPopup = strPopup.Replace("[" + drC["DP"].ToString() + "]", dtParentRecord.Rows[0][drC["SystemName"].ToString()].ToString());

            //                                                                        }
            //                                                                    }
            //                                                                }
            //                                                            }
            //                                                        }
            //                                                    }
            //                                                }
            //                                            }

            //                                        }

            //                                    }
            //                                    catch
            //                                    {
            //                                        //
            //                                    }

            //                                }

            //                                string strPinImage = theTable.PinImage;


            //                                //if (dtPinControlInfo != null && thePinControlColumn != null
            //                                //    && drRecord[thePinControlColumn.SystemName].ToString() != "")
            //                                //{
            //                                //    foreach (DataRow drPC in dtPinControlInfo.Rows)
            //                                //    {
            //                                //        if (drRecord[thePinControlColumn.SystemName].ToString() == drPC["value"].ToString())
            //                                //        {
            //                                //            if (drPC["custom1"].ToString() != "")
            //                                //            {
            //                                //                strPinImage = drPC["custom1"].ToString();
            //                                //            }
            //                                //            break;
            //                                //        }
            //                                //    }
            //                                //}


            //                                if (dtPinControlInfo != null && thePinControlColumn != null
            //                                    && drRecord[sJob_ContractorSys].ToString() != "")
            //                                {

            //                                    int iContractorRecordID = 0;
            //                                    if (int.TryParse(drRecord[sJob_ContractorSys].ToString(),out iContractorRecordID))
            //                                    {
            //                                        string strContractorStatus = Common.GetValueFromSQL("SELECT " + thePinControlColumn.SystemName + " FROM [Record] WHERE RecordID=" + iContractorRecordID.ToString());

            //                                        foreach (DataRow drPC in dtPinControlInfo.Rows)
            //                                        {
            //                                            if (strContractorStatus == drPC["value"].ToString())
            //                                            {
            //                                                if (drPC["custom1"].ToString() != "")
            //                                                {
            //                                                    strPinImage = drPC["custom1"].ToString();
            //                                                }
            //                                                break;
            //                                            }
            //                                        }
            //                                    }


            //                                }

            //                                EachPinInfo aPin = new EachPinInfo();
            //                                aPin.lat = theLocationColumn.Latitude.ToString();
            //                                aPin.lon = theLocationColumn.Longitude.ToString();
            //                                aPin.title = strTT;
            //                                aPin.pin = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath + "/" + strPinImage;
            //                                aPin.rings = theTable.DistanceRings;
            //                                aPin.order = theTable.PinDisplayOrder;
            //                                aPin.onStart = theTable.ShowOnStart;
            //                                aPin.ssid = drRecord["RecordID"].ToString();
            //                                aPin.url = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath + "/Pages/Record/RecordDetail.aspx?fixedurl=" + Cryptography.Encrypt("~/Default.aspx") + "&stackzero=yes&mode=" + Cryptography.Encrypt("view") +
            //                                    "&SearchCriteriaID=" + Cryptography.Encrypt("-1") + "&TableID=" + Cryptography.Encrypt(drSys["TableID"].ToString()) + "&Recordid=" +
            //                                    Cryptography.Encrypt(drRecord["RecordID"].ToString());
            //                                aPin.mappopup = strPopup;

            //                                sb.Append(",");
            //                                sb.Append(aPin.GetJSONString());
            //                            }
            //                        }
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        ErrorLog theErrorLog = new ErrorLog(null, "Get location each error", ex.Message, ex.StackTrace, DateTime.Now, context.Request.Path);
            //                        SystemData.ErrorLog_Insert(theErrorLog);

            //                    }
            //                }

            //            }



            //        }

            //    }

            //    strJSON = sb.ToString();
            //    if (!String.IsNullOrEmpty(strJSON))
            //    {
            //        strJSON = "[" + strJSON.Substring(1) + "]";
            //    }


            //    context.Response.Write(strJSON);
            //    ////Response.Flush();
            //    ////Response.End();
            //    //HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
            //    //HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            //    //HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
            //}






        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Get Locations", ex.Message, ex.StackTrace, DateTime.Now, context.Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);

        }




    }

    private bool IsLatLongExist(DataTable dtListOfLatLong, LocationColumn theLocationColumn)
    {

        foreach (DataRow dr in dtListOfLatLong.Rows)
        {
            double dTest = 0;

            bool bIsDouble = true;

            if (double.TryParse(theLocationColumn.Latitude.ToString(),out dTest)==false
                || double.TryParse(theLocationColumn.Longitude.ToString(), out dTest) == false
                || double.TryParse(dr[0].ToString(), out dTest) == false
                || double.TryParse(dr[1].ToString(), out dTest) == false
                )
            {
                bIsDouble = false;
            }


            if (bIsDouble)
            {
                if (double.Parse(theLocationColumn.Latitude) == (double)dr[0] && double.Parse(theLocationColumn.Longitude) == (double)dr[1])
                {
                    return true;
                }
            }


        }
        return false;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}
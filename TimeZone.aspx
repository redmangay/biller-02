﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TimeZone.aspx.cs" Inherits="TimeZone" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="ddlTimeZone" runat="server" AutoPostBack="True"
                OnSelectedIndexChanged="ddlTimeZone_SelectedIndexChanged"
                AppendDataBoundItems="true">
                <asp:ListItem Text="Select a TimeZone" Value="Default value" />
            </asp:DropDownList>
            <br />
            <br />
            Local Time:
            <asp:Label ID="lblLocalTime" runat="server" Text=""></asp:Label>
            <br />
            <br />
            Converted Time:
            <asp:Label ID="lblTimeZone" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>

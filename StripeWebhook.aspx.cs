﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Stripe;
using System.IO;
public partial class StripeWebhook : System.Web.UI.Page
{

    public string stripePublishableKey = SystemData.SystemOption_ValueByKey_Account("StripePublishableKey", null, null);
    private string secretKey = SystemData.SystemOption_ValueByKey_Account("StripeSecretKey", null, null);

    protected void Page_Load(object sender, EventArgs e)
    {
        Stream req = Request.InputStream;
        req.Seek(0, SeekOrigin.Begin);

        string json = new StreamReader(req).ReadToEnd();

        //Test Json

        //invoice.created 
        //string json = "{ \"id\": \"evt_1Bw5GTCtiZ7cOVF6xgqt8Y6b\", \"object\": \"event\", \"api_version\": \"2018-01-23\", \"created\": 1518772341, \"data\": { \"object\": { \"id\": \"in_1Bw5GSCtiZ7cOVF6hbilNex4\", \"object\": \"invoice\", \"amount_due\": 1500, \"application_fee\": null, \"attempt_count\": 0, \"attempted\": true, \"billing\": \"charge_automatically\", \"charge\": \"ch_1Bw5GSCtiZ7cOVF65TPIR0iZ\", \"closed\": true, \"currency\": \"aud\", \"customer\": \"cus_CKkm9ANE0Q4bWD\", \"date\": 1518772340, \"description\": null, \"discount\": null, \"due_date\": null, \"ending_balance\": 0, \"forgiven\": false, \"lines\": { \"object\": \"list\", \"data\": [ { \"id\": \"sub_CKkm6mS5ABycwK\", \"object\": \"line_item\", \"amount\": 1500, \"currency\": \"aud\", \"description\": \"1 × DBG Weekly (at $15.00 / week)\", \"discountable\": true, \"livemode\": false, \"metadata\": { }, \"period\": { \"start\": 1518772340, \"end\": 1519377140 }, \"plan\": { \"id\": \"Weekly1\", \"object\": \"plan\", \"amount\": 1500, \"created\": 1518579072, \"currency\": \"aud\", \"interval\": \"week\", \"interval_count\": 1, \"livemode\": false, \"metadata\": { }, \"nickname\": null, \"product\": \"prod_CJuonKqEmeoKeB\", \"trial_period_days\": null, \"statement_descriptor\": \"S d\", \"name\": \"DBG Weekly\" }, \"proration\": false, \"quantity\": 1, \"subscription\": null, \"subscription_item\": \"si_CKkmb03giTUHzF\", \"type\": \"subscription\" } ], \"has_more\": false, \"total_count\": 1, \"url\": \"/v1/invoices/in_1Bw5GSCtiZ7cOVF6hbilNex4/lines\" }, \"livemode\": false, \"metadata\": { }, \"next_payment_attempt\": null, \"number\": \"e723441f36-0001\", \"paid\": true, \"period_end\": 1518772340, \"period_start\": 1518772340, \"receipt_number\": null, \"starting_balance\": 0, \"statement_descriptor\": null, \"subscription\": \"sub_CKkm6mS5ABycwK\", \"subtotal\": 1500, \"tax\": null, \"tax_percent\": null, \"total\": 1500, \"webhooks_delivered_at\": null } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_DJi8ixXNZ3Mdyx\", \"idempotency_key\": null }, \"type\": \"invoice.created\" }";


        //"invoice.payment_succeeded"
        //string json = "{ \"id\": \"evt_1Bw5GTCtiZ7cOVF6yc14ga6y\", \"object\": \"event\", \"api_version\": \"2018-01-23\", \"created\": 1518772341, \"data\": { \"object\": { \"id\": \"in_1Bw5GSCtiZ7cOVF6hbilNex4\", \"object\": \"invoice\", \"amount_due\": 1500, \"application_fee\": null, \"attempt_count\": 0, \"attempted\": true, \"billing\": \"charge_automatically\", \"charge\": \"ch_1Bw5GSCtiZ7cOVF65TPIR0iZ\", \"closed\": true, \"currency\": \"aud\", \"customer\": \"cus_CKkm9ANE0Q4bWD\", \"date\": 1518772340, \"description\": null, \"discount\": null, \"due_date\": null, \"ending_balance\": 0, \"forgiven\": false, \"lines\": { \"object\": \"list\", \"data\": [ { \"id\": \"sub_CKkm6mS5ABycwK\", \"object\": \"line_item\", \"amount\": 1500, \"currency\": \"aud\", \"description\": \"1 × DBG Weekly (at $15.00 / week)\", \"discountable\": true, \"livemode\": false, \"metadata\": { }, \"period\": { \"start\": 1518772340, \"end\": 1519377140 }, \"plan\": { \"id\": \"Weekly1\", \"object\": \"plan\", \"amount\": 1500, \"created\": 1518579072, \"currency\": \"aud\", \"interval\": \"week\", \"interval_count\": 1, \"livemode\": false, \"metadata\": { }, \"nickname\": null, \"product\": \"prod_CJuonKqEmeoKeB\", \"trial_period_days\": null, \"statement_descriptor\": \"S d\", \"name\": \"DBG Weekly\" }, \"proration\": false, \"quantity\": 1, \"subscription\": null, \"subscription_item\": \"si_CKkmb03giTUHzF\", \"type\": \"subscription\" } ], \"has_more\": false, \"total_count\": 1, \"url\": \"/v1/invoices/in_1Bw5GSCtiZ7cOVF6hbilNex4/lines\" }, \"livemode\": false, \"metadata\": { }, \"next_payment_attempt\": null, \"number\": \"e723441f36-0001\", \"paid\": true, \"period_end\": 1518772340, \"period_start\": 1518772340, \"receipt_number\": null, \"starting_balance\": 0, \"statement_descriptor\": null, \"subscription\": \"sub_CKkm6mS5ABycwK\", \"subtotal\": 1500, \"tax\": null, \"tax_percent\": null, \"total\": 1500, \"webhooks_delivered_at\": null } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_DJi8ixXNZ3Mdyx\", \"idempotency_key\": null }, \"type\": \"invoice.payment_succeeded\" }";

       // json = "{ \"id\": \"evt_1EttROCtiZ7cOVF6REqJ72Ed\", \"object\": \"event\", \"api_version\": \"2018-01-23\", \"created\": 1562579241, \"data\": { \"object\": { \"id\": \"in_1EttRMCtiZ7cOVF6YLGBcxb4\", \"object\": \"invoice\", \"account_country\": \"AU\", \"account_name\": \"DBG eContractor test\", \"amount_due\": 1000, \"amount_paid\": 1000, \"amount_remaining\": 0, \"application_fee\": null, \"attempt_count\": 1, \"attempted\": true, \"auto_advance\": false, \"billing\": \"charge_automatically\", \"billing_reason\": \"subscription_update\", \"charge\": \"ch_1EttRNCtiZ7cOVF62neF1I7h\", \"closed\": true, \"collection_method\": \"charge_automatically\", \"created\": 1562579240, \"currency\": \"aud\", \"custom_fields\": null, \"customer\": \"cus_FOgp7ohIRz2w3m\", \"customer_address\": null, \"customer_email\": \"mohsinrazuan@gmail.com\", \"customer_name\": null, \"customer_phone\": null, \"customer_shipping\": null, \"customer_tax_exempt\": \"none\", \"customer_tax_ids\": [ ], \"date\": 1562579240, \"default_payment_method\": null, \"default_source\": null, \"default_tax_rates\": [ ], \"description\": null, \"discount\": null, \"due_date\": null, \"ending_balance\": 0, \"finalized_at\": 1562579240, \"footer\": null, \"forgiven\": false, \"hosted_invoice_url\": \"https://pay.stripe.com/invoice/invst_yzYbYdCLgxNzZbogbTQmUumVKx\", \"invoice_pdf\": \"https://pay.stripe.com/invoice/invst_yzYbYdCLgxNzZbogbTQmUumVKx/pdf\", \"lines\": { \"object\": \"list\", \"data\": [ { \"id\": \"sub_FOgpZVzDVSimqE\", \"object\": \"line_item\", \"amount\": 1000, \"currency\": \"aud\", \"description\": \"1000 OTMonthly × OnTaskMonthly (at $0.01 / month)\", \"discountable\": true, \"livemode\": false, \"metadata\": { \"AccountID\": \"27388\" }, \"period\": { \"end\": 1565257640, \"start\": 1562579240 }, \"plan\": { \"id\": \"ID_OnTaskMonthly\", \"object\": \"plan\", \"active\": true, \"aggregate_usage\": null, \"amount\": 1, \"billing_scheme\": \"per_unit\", \"created\": 1561795169, \"currency\": \"aud\", \"interval\": \"month\", \"interval_count\": 1, \"livemode\": false, \"metadata\": { }, \"name\": \"OnTaskMonthly\", \"nickname\": \"OnTask Monthly\", \"product\": \"prod_FLHvscl3fZdaum\", \"statement_descriptor\": null, \"tiers\": null, \"tiers_mode\": null, \"transform_usage\": null, \"trial_period_days\": null, \"usage_type\": \"licensed\" }, \"proration\": false, \"quantity\": 1000, \"subscription\": null, \"subscription_item\": \"si_FOgpC53PiQUNdp\", \"tax_amounts\": [ ], \"tax_rates\": [ ], \"type\": \"subscription\", \"unique_line_item_id\": \"sli_9aaeaddca2d06c\" } ], \"has_more\": false, \"total_count\": 1, \"url\": \"/v1/invoices/in_1EttRMCtiZ7cOVF6YLGBcxb4/lines\" }, \"livemode\": false, \"metadata\": { }, \"next_payment_attempt\": null, \"number\": \"B0BD5E40-0001\", \"paid\": true, \"payment_intent\": \"pi_1EttRNCtiZ7cOVF6loYkRyZT\", \"period_end\": 1562579240, \"period_start\": 1562579240, \"post_payment_credit_notes_amount\": 0, \"pre_payment_credit_notes_amount\": 0, \"receipt_number\": null, \"starting_balance\": 0, \"statement_descriptor\": null, \"status\": \"paid\", \"status_transitions\": { \"finalized_at\": 1562579240, \"marked_uncollectible_at\": null, \"paid_at\": 1562579241, \"voided_at\": null }, \"subscription\": \"sub_FOgpZVzDVSimqE\", \"subtotal\": 1000, \"tax\": null, \"tax_percent\": null, \"total\": 1000, \"total_tax_amounts\": [ ], \"webhooks_delivered_at\": null } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_uLjkgvREYovD1D\", \"idempotency_key\": null }, \"type\": \"invoice.payment_succeeded\" }";
        

        //customer.created
        //string json = "{ \"id\": \"evt_1Bw5GSCtiZ7cOVF6v4NjzD6M\", \"object\": \"event\", \"api_version\": \"2018-01-23\", \"created\": 1518772340, \"data\": { \"object\": { \"id\": \"cus_CKkm9ANE0Q4bWD\", \"object\": \"customer\", \"account_balance\": 0, \"created\": 1518772340, \"currency\": null, \"default_source\": null, \"delinquent\": false, \"description\": null, \"discount\": null, \"email\": \"a3@a.com\", \"invoice_prefix\": \"e723441f36\", \"livemode\": false, \"metadata\": { }, \"shipping\": null, \"sources\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_CKkm9ANE0Q4bWD/sources\" }, \"subscriptions\": { \"object\": \"list\", \"data\": [ ], \"has_more\": false, \"total_count\": 0, \"url\": \"/v1/customers/cus_CKkm9ANE0Q4bWD/subscriptions\" } } }, \"livemode\": false, \"pending_webhooks\": 1, \"request\": { \"id\": \"req_ZYmFeq9r0yTRtb\", \"idempotency_key\": null }, \"type\": \"customer.created\" }";


        //
        StripeEvent stripeEvent = null;
        string strEmailBody = "";
        try
        {
            // as in header, you need https://github.com/jaymedavis/stripe.net
            // it's a great library that should have been offered by Stripe directly
            stripeEvent = StripeEventUtility.ParseEvent(json);


            if (stripeEvent == null)
                strEmailBody = "Incoming event empty";


            try
            {
                switch (stripeEvent.Type)
                {
                    //case "charge.succeeded":
                    //    // do work
                    //    StripeCharge sCharge = Mapper<StripeCharge>.MapFromJson(stripeEvent.Data.Object.ToString());

                    //    break;
                    case "invoice.created":
                        // do work
                        //StripeInvoice sInvoiceC = Mapper<StripeInvoice>.MapFromJson(stripeEvent.Data.Object.ToString());

                        break;
                    case "customer.created":
                        //StripeCustomer sCustomer = Mapper<StripeCustomer>.MapFromJson(stripeEvent.Data.Object.ToString());
                        //if (sCustomer != null)
                        //{

                        //}
                        // do work
                        break;
                    case "invoice.payment_succeeded":
                        StripeInvoice sInvoicePS = Mapper<StripeInvoice>.MapFromJson(stripeEvent.Data.Object.ToString());
                        if (sInvoicePS != null)
                        {
                            StripeCustomerService stripeService = new StripeCustomerService(secretKey);
                            StripeCustomer theCustomer= stripeService.Get(sInvoicePS.CustomerId);
                            
                            if (theCustomer!=null)
                            {
                                Dictionary<string, string> cusMetaData = theCustomer.Metadata;
                                
                                string sBillingID = Common.GetValueFromSQL("SELECT BillingID From Billing WHERE AccountID = " + cusMetaData["AccountID"].ToString() + " AND (PaymentSuccess=0 OR PaymentSuccess IS NULL) ORDER BY BillingID DESC");

                                //string sBillingID = Common.GetValueFromSQL(@"SELECT BillingID FROM [Billing] WHERE  YEAR(GETDATE())=YEAR(BillingDate) 
                                //AND MONTH(GETDATE())=MONTH(BillingDate) AND  (PaymentSuccess=0 OR PaymentSuccess IS NULL) AND AccountID=" + cusMetaData["AccountID"].ToString());

                                if(!string.IsNullOrEmpty(sBillingID))
                                {
                                    //need to check if this partial payment, if partial payment then we could charge & add another 
                                    BillingAPI.Billing theBilling = BillingAPI.GetBilling(int.Parse(sBillingID));
                                    theBilling.PaymentSuccess = true;
                                    theBilling.PaymentDate = DateTime.Now;
                                    theBilling.PaymentMethod = "S";                                 
                                    
                                    if(theBilling.TargetAccountTypeID!=null)
                                    {
                                        Account theAccount = SecurityManager.Account_Details(int.Parse(cusMetaData["AccountID"].ToString()));
                                        BillingAPI.AdjustUsersForAccountType((int)theAccount.AccountTypeID, (int)theBilling.TargetAccountTypeID, (int)theAccount.AccountID);

                                        theAccount.AccountTypeID = theBilling.TargetAccountTypeID;
                                        SecurityManager.Account_Update(theAccount);

                                    }

                                    BillingAPI.UpdateBilling(theBilling);

                                    strEmailBody = strEmailBody + "Updated Billing ID:" + sBillingID + "   ";
                                }
                                //else
                                //{
                                //    //create a new bill -- may be this will never happen
                                //   BillingAPI.Billing newBilling = new BillingAPI.Billing();
                                //    newBilling.AccountID = int.Parse(cusMetaData["AccountID"].ToString());
                                //    newBilling.Amount = sInvoicePS.Total / 100;          //BillingAPI.MonthlyCharge(newBilling.AccountID, -1);
                                //    newBilling.BillingDate = DateTime.Now;
                                //    newBilling.Currency = sInvoicePS.Currency;  //BillingAPI.GetAccountCurrency(newBilling.AccountID);
                                //    newBilling.NoOfUsers = -1;// dtUsers.Rows.Count;
                                //    newBilling.Remarks = sInvoicePS.Id;
                                //    newBilling.PaymentDate = DateTime.Now;
                                //    newBilling.PaymentMethod = "S";
                                //    newBilling.PaymentSuccess = true;

                                //    //newBilling.
                                //    int billingid = BillingAPI.CreateBilling(newBilling);
                                //    strEmailBody = strEmailBody + "Created Billing ID:" + billingid.ToString() + "   ";
                                //    //no need to add billing detail here
                                //    Account theAccount = SecurityManager.Account_Details(int.Parse(cusMetaData["AccountID"].ToString()));
                                //    if ((int)theAccount.AccountTypeID == 1)
                                //    {
                                //        theAccount.AccountTypeID = 2;// the user paid so let's make it a team account at least.
                                //        SecurityManager.Account_Update(theAccount);
                                //    }
                                //}
                            }
                        }
                        // do work
                        break;
                    case "customer.subscription.updated":
                        StripeSubscription sStripeSubscription = Mapper<StripeSubscription>.MapFromJson(stripeEvent.Data.Object.ToString());
                        if (sStripeSubscription != null)
                        {
                            StripeCustomerService stripeService = new StripeCustomerService(secretKey);
                            StripeCustomer theCustomer = stripeService.Get(sStripeSubscription.CustomerId);

                            if (theCustomer != null)
                            {
                                Dictionary<string, string> cusMetaData = theCustomer.Metadata;

                                string sBillingID = Common.GetValueFromSQL("SELECT BillingID From Billing WHERE AccountID = " + cusMetaData["AccountID"].ToString() + " AND (PaymentSuccess=0 OR PaymentSuccess IS NULL) ORDER BY BillingID DESC");

                                if (!string.IsNullOrEmpty(sBillingID))
                                {
                                    //may be here we can update billing.amount by stripe upcoming amount?? - MR

                                    BillingAPI.Billing theBilling = BillingAPI.GetBilling(int.Parse(sBillingID));
                                    if (theBilling.TargetAccountTypeID != null)
                                    {
                                        Account theAccount = SecurityManager.Account_Details(int.Parse(cusMetaData["AccountID"].ToString()));
                                        BillingAPI.AdjustUsersForAccountType((int)theAccount.AccountTypeID, (int)theBilling.TargetAccountTypeID, (int)theAccount.AccountID);
                                        theAccount.AccountTypeID = theBilling.TargetAccountTypeID;
                                        SecurityManager.Account_Update(theAccount);                                        
                                    }

                                    strEmailBody = strEmailBody + "Updated Billing ID:" + sBillingID + "   ";
                                }                               

                            }
                        }
                        break;


                    case "customer.subscription.deleted":
                    case "customer.subscription.created":
                        StripeSubscription sStripeSubscriptionC = Mapper<StripeSubscription>.MapFromJson(stripeEvent.Data.Object.ToString());
                        if (sStripeSubscriptionC != null)
                        {
                            StripeCustomerService stripeService = new StripeCustomerService(secretKey);
                            StripeCustomer theCustomer = stripeService.Get(sStripeSubscriptionC.CustomerId);

                            if (theCustomer != null)
                            {
                                Dictionary<string, string> cusMetaData = theCustomer.Metadata;

                                string sBillingID = Common.GetValueFromSQL("SELECT BillingID From Billing WHERE AccountID = " + cusMetaData["AccountID"].ToString() + " AND (PaymentSuccess=0 OR PaymentSuccess IS NULL) ORDER BY BillingID DESC");

                                if (!string.IsNullOrEmpty(sBillingID))
                                {
                                    //may be here we can update billing.amount by stripe upcoming amount?? - MR

                                    BillingAPI.Billing theBilling = BillingAPI.GetBilling(int.Parse(sBillingID));
                                    if (theBilling.TargetAccountTypeID != null)
                                    {
                                        Account theAccount = SecurityManager.Account_Details(int.Parse(cusMetaData["AccountID"].ToString()));
                                        BillingAPI.AdjustUsersForAccountType((int)theAccount.AccountTypeID, (int)theBilling.TargetAccountTypeID, (int)theAccount.AccountID);
                                        theAccount.AccountTypeID = theBilling.TargetAccountTypeID;
                                        SecurityManager.Account_Update(theAccount);
                                    }

                                    strEmailBody = strEmailBody + "Updated Billing ID:" + sBillingID + "   ";
                                }

                            }
                        }
                        break;

                }
            }
            catch (Exception ex)
            {
                string strErrorE = "";
                //Common.SendSingleEmail("r_mohsin@yahoo.com", "Stripe Error--" + ex.Message, ex.StackTrace, ref strErrorE);
            }

            strEmailBody = strEmailBody + stripeEvent.Type + Environment.NewLine + json;




        }
        catch (Exception ex)
        {
            strEmailBody = "Unable to parse incoming event - " + ex.Message + "----" + ex.StackTrace;
        }

        string strError = "";
        Common.SendSingleEmail("r_mohsin@yahoo.com", "Stripe IPN", strEmailBody, ref strError);


    }
}
(function($) {

	"use strict";

	/*--------------------------------------------------------------
		1. Scripts initialization
   --------------------------------------------------------------*/

   $(window).on('load', function() {
    $(window).trigger("scroll");
    $(window).trigger("resize");
  });


   $(document).ready(function() {
    $(window).trigger("resize");
    primaryMenuSetup();
    mobileMenu();
    scrollAnimation();
    sectionActive();
    scrollUp();
    accordianSetup();
    $(".player").mb_YTPlayer();

    // Pricing Table
    $('.mobile-plan-btn').on('click', function() {
      $(this).parents('.single-plan').toggleClass('active');
    });

    // Carousel
    $('#tm-day').carousel({
      interval: false
    }); 
    
    // Owl Carousel
    $('.hero-slider').owlCarousel({
      items: 1,
        animateOut: 'fadeOut',
        loop: true,
        autoplay: true      
    });

    // $('.tb-toggle-btn').on('click', function() {
    //   $('.tb-modal').addClass('active');
    // });
    // $('.tb-modal-layer, .tb-cross-btn').on('click', function() {
    //   $('.tb-modal').removeClass('active');
    // });
    $('.tb-v-mobile').on('click', function() {
      $(this).siblings('.tb-vartical-nav').toggleClass('active');
    });
    $('.tb-vartical-nav a').on('click', function() {
      $('.tb-vartical-nav').removeClass('active');
    });

  });


   $(window).on('resize', function() {
    mobileMenu();
  });


   $(window).on('scroll', function() {
    scrollFunction();
  });


	/*--------------------------------------------------------------
		2. Primary Menu
   --------------------------------------------------------------*/
   
   function primaryMenuSetup() {

    $( ".primary-nav-list" ).before( "<div class='m-menu-btn'><span></span></div>" );

    $(".m-menu-btn").on('click', function(){
     $( this ).toggleClass( "m-menu-btn-ext" );
     $(this).siblings('.primary-nav-list').slideToggle("slow");
   });

    $( ".menu-item-has-children > ul" ).before( "<i class='fa fa-plus m-dropdown'></i>" );

    $('.m-dropdown').on('click', function() {
     $(this).siblings('ul').slideToggle("slow");
     $(this).toggleClass("fa-plus fa-minus")
   });
    
    $('.m-menu .nav-link').on('click', function() {
      $(this).parents('.primary-nav-list').slideUp();
      $(this).parents('.primary-nav-list').siblings().removeClass('m-menu-btn-ext');
    });

  }

  function mobileMenu() {

    if ($(window).width() <= 991){  
      $('.primary-nav').addClass('m-menu').removeClass('primary-nav');
    } else {
      $('.m-menu').addClass('primary-nav').removeClass('m-menu');
    }
    if ($(window).width() <= 767){  
      $('.tb-vartical-nav').addClass('tb-v-mobile').removeClass('tb-v-desktop');
    } else {
      $('.tb-vartical-nav').addClass('tb-v-desktop').removeClass('tb-v-mobile');
    }
    if ($(window).width() <= 991){  
     $('.single-plan').addClass('mobile-plan');
   } else {
     $('.single-plan').removeClass('mobile-plan');
   }

 }


	/*--------------------------------------------------------------
		3. Scroll Function
   --------------------------------------------------------------*/

   function scrollFunction() {

    var scroll = $(window).scrollTop();

    if(scroll >= 10) {
      $(".site-header").addClass("small-height");
    } else {
     $(".site-header").removeClass("small-height");
   }

		// For Scroll Up
		if(scroll >= 350) {
      $(".scrollup").addClass("scrollup-show");
    } else {
     $(".scrollup").removeClass("scrollup-show");
   }
   if(scroll >= 590) {
      $(".accordian-wrapper").addClass("tm-fixed");
    } else {
      $(".accordian-wrapper").removeClass("tm-fixed");
    }

 }


	/*--------------------------------------------------------------
		4. Section Active and Scrolling Animation
   --------------------------------------------------------------*/

   function scrollAnimation() {

    $('a:not(".tm-your-day-wrap a")').bind('click', function(event) {
     var $anchor = $(this);
     $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top - 15)
    }, 1250, 'easeInOutExpo');
     event.preventDefault();
   });


    $('.tb-vartical-nav a').bind('click', function(event) {
       var $anchor = $(this);
       $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top - 80)
      }, 1250, 'easeInOutExpo');
       event.preventDefault();
     });

  }

  function sectionActive() {

    $('body').scrollspy({
     target: '.site-header',
     offset: 70
   });
  }


	/*--------------------------------------------------------------
		5. Scroll Up
   --------------------------------------------------------------*/

   function scrollUp() {

    $( "body" ).append( "<span class='scrollup'></span>" );

    $('.scrollup').on('click', function(e) {
     e.preventDefault();
     $('html,body').animate({
      scrollTop: 0
    }, 1000);
   });

  }

/*--------------------------------------------------------------
    6. Accordian
    --------------------------------------------------------------*/
    
    function accordianSetup() {

      var $this = $(this);
      $( ".accordian-head" ).append( "<span class='accordian-toggle fa fa-angle-down'></span>" );
      $('.single-accordian').filter(':nth-child(n+1)').children('.accordian-body').hide();
      $('.accordian-wrapper.tb-style1 .single-accordian:first-child').children('.accordian-body').show();
      $('.accordian-head').on('click', function() {
        $(this).parent('.single-accordian').siblings().children('.accordian-body').slideUp();
        $(this).siblings().slideToggle();
        /* Accordian Active Class */
        $(this).toggleClass('active');
        $(this).parent('.single-accordian').siblings().children('.accordian-head').removeClass('active');
      });

    }

    
})(jQuery); // End of use strict



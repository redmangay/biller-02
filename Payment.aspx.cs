﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using Stripe;
public partial class Payment : SecurePage
{
    //public string stripePublishableKey = WebConfigurationManager.AppSettings["StripePublishableKey"];
    public string stripePublishableKey = SystemData.SystemOption_ValueByKey_Account("StripePublishableKey", null, null);
    User _theUser;
    private int iTotalPayment = 130;

    public string GetTotalPaymentAmount()
    {
        return (iTotalPayment * 100).ToString();
    }
    public string GetUserEmail()
    {     
        if (_theUser==null)
        {
            _theUser = (User)Session["User"];
        }        
        return _theUser.Email;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        _theUser = (User)Session["User"];
        if(IsPostBack)
        {
            if(Request.Form["stripeToken"]!=null)
            {
                string stripeEmail = Request.Form["stripeEmail"].ToString();
                lblMessage.Text += "<br />Email:" + stripeEmail;
                lblMessage.Text += "<br />Stripe Token:" + Request.Form["stripeToken"].ToString();
                //pnlStripButton.Visible = false;

                //var secretKey = WebConfigurationManager.AppSettings["StripeSecretKey"];
                var secretKey = SystemData.SystemOption_ValueByKey_Account("StripeSecretKey", null, null);
                StripeConfiguration.SetApiKey(secretKey);


                var cus_options = new StripeCustomerCreateOptions
                {
                    Email = stripeEmail,//"jenny.rosen@example.com",
                    Metadata = new Dictionary<string, string>()
                        {
                            { "UserID", _theUser.UserID.ToString() },
                            { "AccountID", Session["AccountID"].ToString() }
                        }
                };
                var cus_service = new StripeCustomerService();


                var optionsCusList = new StripeCustomerListOptions();
                optionsCusList.Limit = 10000;
                var customers = cus_service.List(optionsCusList);
                //string sTest = "";
                bool bUpdateSubscription = false;
                StripeCustomer customer=new StripeCustomer();
                foreach (var customerEach in customers)
                {
                    if (cus_options.Email.ToLower().Trim() == customerEach.Email.ToLower().Trim())
                    {
                        System.Collections.Generic.Dictionary<string, string> cusMetaData = customerEach.Metadata;
                        if (cusMetaData != null && cusMetaData["UserID"] == _theUser.UserID.ToString() && cusMetaData["AccountID"] == Session["AccountID"].ToString())
                        {
                            customer = customerEach;
                            break;
                        }
                       
                    }
                    //cus_service.Delete(customer.Id);
                    //sTest = sTest + "  " + customer.Email;
                }



                if (customer.Id==null)
                    customer = cus_service.Create(cus_options);


                if(bUpdateSubscription)
                {
                    //what to do?
                }
                

                var items = new List<StripeSubscriptionItemOption> {
                    new StripeSubscriptionItemOption {
                        PlanId = BillingAPI.StripeAccountPlanID(int.Parse(Session["AccountID"].ToString())),
                        Quantity=10000
                    }//plan_CBXbz9i7AIOTzr  //  //prod_FGUq8AU1HhiyXF
                };      
               
                var options = new StripeSubscriptionCreateOptions
                {
                    Items = items,
                    Source = Request.Form["stripeToken"].ToString()
                };
                var service = new StripeSubscriptionService();
                StripeSubscription subscription = service.Create(customer.Id, options);//  "cus_4fdAW5ftNQow1a"


                lblMessage.Text = "Thank you!<br />" + lblMessage.Text;
            }
        }

    }
    protected void btnPaypal_Click(object sender, EventArgs e)
    {
        string stripePaypalMonthlyAmount = SystemData.SystemOption_ValueByKey_Account("Paypal Monthly Amount", null, null);
        Session["PaypalItemName"] = "DBGurus eContractor Payment";
        Session["PaymentAmount"] = stripePaypalMonthlyAmount == "" ? "1" : stripePaypalMonthlyAmount;
        Session["PayIntervalType"] = "D";//M
        Session["InvoiceID"] = "tdb_" + Guid.NewGuid().ToString(); // a TDB Reference

        Response.Redirect("~/Pages/Security/PayPal/sendpayment_recurring.aspx", false);
    }
}



// Application_Start
//
//try
//{
//    var secretKey = WebConfigurationManager.AppSettings["StripeSecretKey"];
//    StripeConfiguration.SetApiKey(secretKey);


//    var options = new StripePlanCreateOptions
//    {
//        Currency = "aud",
//        Interval = "month",
//        Name = "Monthly10",
//        Amount = 11,
//    };
//    var service = new StripePlanService();
//    StripePlan plan = service.Create(options);

//}
//catch
//{
//    //
//}
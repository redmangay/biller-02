﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="TestPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


</head>
<body>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Script/jquery.js")%>"></script>
    <link type="text/css" rel="stylesheet" href="<%=ResolveUrl("~/fancybox/jquery.fancybox-1.3.4.css")%>" />
    <script type="text/javascript" src="<%=ResolveUrl("~/fancybox/jquery.fancybox-1.3.4.pack.js")%>"></script>--%>

    <script type="text/javascript" src="<%=ResolveUrl("~/script/jquery-3.3.1.min.js")%>"></script>
    <link href="<%=ResolveUrl("~/fancybox3/dist/jquery.fancybox.min.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/fancybox3/dist/jquery.fancybox.min.js")%>"></script>


    <script type="text/javascript">
        //SyntaxError()
        $(document).ready(function () {
            $(function () {
                $(".PopupRecordDetailAdd").fancybox({
                    scrolling: 'auto',
                    type: 'iframe',
                    //width: 900,
                    //height: 300,
                    titleShow: false
                });
            });
        });
    </script>
    <form id="form1" runat="server">
        <div>
            <asp:Label runat="server" ID="lblMessages"></asp:Label>
        </div>

        <div>
            <%--<asp:GridView ID="grdTest" runat="server" AutoGenerateColumns="true"></asp:GridView>--%>
            <table>
                <tr>
                    <td>Task RecordID(2237546)</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTaskID"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtEmail"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Name</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtName"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <asp:Button runat="server" ID="btnInviteUser" OnClick="btnInviteUser_Click" Text="Send Invitation" />
            <br />
            <br />
            <br />
            <asp:HyperLink runat="server" ID="hlPopupRecordDetailAdd" CssClass="PopupRecordDetailAdd" Text="Add"
                NavigateUrl="~/Pages/Record/RecordDetail.aspx?mode=hBe/iopMPf0=&TableID=f8c5IqArhDk=&SearchCriteriaID=kdUxjBEM5oo=&recordpopup=add&ParentDropDownList=ddlVXXX"></asp:HyperLink>

            <br />
            <br />
            <asp:DropDownList runat="server" ID="ddlVXXX" ClientIDMode="Static">
                <asp:ListItem Value="123" Text="A"></asp:ListItem>
                <asp:ListItem Value="234" Text="B"></asp:ListItem>
                <asp:ListItem Value="321" Text="C"></asp:ListItem>
            </asp:DropDownList>

        </div>
    </form>
</body>
</html>

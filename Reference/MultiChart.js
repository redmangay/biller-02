var numberOfAnalytes = 2; //Number of chart/graph series.
var chartType = ['line']; //The chart/graph type of the series.
ExecuteMultiChart(); //To execute multi chart UI

$(function () {
    var chart = $('#WebChartViewer1').highcharts({
        chart: {
            height: Number($('#chartHeight').val()),
            width: Number($('#chartWidth').val()),
            alignTicks: false,
            events: {
                load: function () {
                    addWarnings(this);
                    addTrendline(this);
                }
            }
        },
        title: {
            text: $('#chartTitle').prop('textContent'),
            x: -20 //center
        },
        subtitle: {
            text: $('#chartSubtitle').prop('textContent'),
            x: -20
        },
        yAxis: getYAxis(),
        xAxis: {
            type: 'datetime'
        },
        series: getSeries(),
        legend: {
            align: 'center',
            verticalAlign: 'bottom',
            borderWidth: 0,
            width: 600,
            itemWidth: 300
        },
        credits: {
            enabled: false
        },
    });
});
// ===========================================
// Base code functions. PLEASE DO NOT MODIFY EXCEPT FOR CHART CUSTOMIZATIONS!
// ===========================================
function getSeries() {
	var seriesArr = [];
	for(var i=0; i<numberOfAnalytes; i++)
    {
        var dataArr = ProcessData(i);
        for (var c = 0; c < dataArr.length; c++)
        {
            //Customize series here, like colors. Please see Highcharts API.
            //Note: For multiple colors, make an array, like chartType.
            seriesArr.push({
                data: dataArr[c],
                name: getName(i, c),
                type: chartType[i > chartType.length - 1 ? chartType.length - 1 : i],
                yAxis: i
            });
        }
	}
	return seriesArr;
}
function getYAxis() {
    var yAxisArr = [];
    var yAxisText;
    var yMinText;
    var yMaxText;
    var yIntervalText;
    for(var i=0; i<numberOfAnalytes; i++)
    {
        var yAxisDictionaryCurr = yAxisDictionary == null ? null : yAxisDictionary[i];
        if(selectedYAxis == -1 || selectedYAxis == i || yAxisDictionaryCurr == null) {
            yAxisText = $('#YAxisTile').prop('textContent');
            yMinText = Number($('#YAxisMin').val()) == "" ? 0 : Number($('#YAxisMin').val());
            yMaxText = Number($('#YAxisMax').val()) == "" ? null : Number($('#YAxisMax').val());
            yIntervalText = Number($('#YAxisInterval').val()) == "" ? null : Number($('#YAxisInterval').val());
        }
        else {
            yAxisText = yAxisDictionaryCurr.filter(function (obj) { return obj.key == "field"; })[0].value;
            yMinText = yAxisDictionaryCurr.filter(function (obj) { return obj.key == "min"; })[0].value == "" ? 0 : yAxisDictionaryCurr.filter(function (obj) { return obj.key == "min"; })[0].value;
            yMaxText = yAxisDictionaryCurr.filter(function (obj) { return obj.key == "max"; })[0].value == "" ? null : yAxisDictionaryCurr.filter(function (obj) { return obj.key == "max"; })[0].value;
            yIntervalText = yAxisDictionaryCurr.filter(function (obj) { return obj.key == "interval"; })[0].value == "" ? null : yAxisDictionaryCurr.filter(function (obj) { return obj.key == "interval"; })[0].value;
        }
        //Customize Y - Axis here. Please see Highcharts API.
        yAxisArr.push({
            title: {
                text: yAxisText,
                offset: 6
            },
            labels: {
                x: i % 2 == 0 ? -26 : 26
            },
            opposite: i % 2 != 0,
            min: yMinText,
            max: yMaxText,
            tickInterval: yIntervalText,
            gridLineWidth: i == 0 ? 1 : 0
        });
    }
    return yAxisArr;
}
function addTrendline(chart) {
    var chartData = [];
    for (var i = 0; i < chart.options.series.length; i++) {
        var optSeriesName = chart.options.series[i].name;
        var optSeriesData = chart.options.series[i].data;
        jQuery.map(optSeriesData, function (val) { if (isNaN(val[1])) { val[1] = null } });
        var warningCaption = $('#warningHighCaption').val();
        var errorCaption = $('#errorHighCaption').val();
        if (optSeriesName != null) {
            if (optSeriesName.indexOf(warningCaption) != -1 && warningCaption.length > 0 ||
                optSeriesName.indexOf(errorCaption) != -1 && errorCaption.length > 0) {
                continue;
            }
            chartData.push(optSeriesData);
        }
    }
    if ($('#showTrendline').val() === '1') {
        chart.addSeries({
            name: 'Trendline',
            type: 'line',
            data: fitData([].concat.apply([], chartData)).data,
            marker: {
                enabled: false
            },
            enableMouseTracking: false
        });
    }
}
function addWarnings(chart) {
    var extremes = chart.xAxis[0].getExtremes();
    var hfSelectedAxisVal = $('#hfSelectedAxis').val() == "" ? 0 : $('#hfSelectedAxis').val();
    if ($('#showWarnings').val() === '1') {
        //Customize limit style here, like dash style. Please see Highcharts API.
        if ($('#warningHigh').val()) {
            chart.addSeries({
                name: $('#warningHighCaption').val() + " Upper",
                type: 'line',
                data: [{
                    x: extremes.min,
                    y: Number($('#warningHigh').val())
                },
                {
                    x: extremes.max,
                    y: Number($('#warningHigh').val())
                }],
                dashStyle: 'dash',
                color: $('#warningHighColor').val(),
                marker: {
                    enabled: false
                },
                yAxis: parseInt(hfSelectedAxisVal)
            });
        }
        if ($('#warningLow').val()) {
            chart.addSeries({
                name: $('#warningHighCaption').val() + " Lower",
                type: 'line',
                data: [{
                    x: extremes.min,
                    y: Number($('#warningLow').val())
                },
                {
                    x: extremes.max,
                    y: Number($('#warningLow').val())
                }],
                dashStyle: 'dash',
                color: $('#warningHighColor').val(),
                marker: {
                    enabled: false
                },
                yAxis: parseInt(hfSelectedAxisVal)
            });
        }
    }
    if ($('#showErrors').val() === '1') {
        //Customize limit style here, like dash style. Please see Highcharts API.
        if ($('#errorHigh').val()) {
            chart.addSeries({
                name: $('#errorHighCaption').val() + " Upper",
                type: 'line',
                data: [{
                    x: extremes.min,
                    y: Number($('#errorHigh').val())
                },
                {
                    x: extremes.max,
                    y: Number($('#errorHigh').val())
                }],
                dashStyle: 'dash',
                color: $('#errorHighColor').val(),
                marker: {
                    enabled: false
                },
                yAxis: parseInt(hfSelectedAxisVal)
            });
        }
        if ($('#errorLow').val()) {
            chart.addSeries({
                name: $('#errorHighCaption').val() + " Lower",
                type: 'line',
                data: [{
                    x: extremes.min,
                    y: Number($('#errorLow').val())
                },
                {
                    x: extremes.max,
                    y: Number($('#errorLow').val())
                }],
                dashStyle: 'dash',
                color: $('#errorHighColor').val(),
                marker: {
                    enabled: false
                },
                yAxis: parseInt(hfSelectedAxisVal)
            });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using Stripe;
public partial class StripePayNoSub : SecurePage
{
    //public string stripePublishableKey = WebConfigurationManager.AppSettings["StripePublishableKey"];
    public string stripePublishableKey = SystemData.SystemOption_ValueByKey_Account("StripePublishableKey", null, null);
    User _theUser;
    private int iTotalPayment = 13;

    public string GetTotalPaymentAmount()
    {
        return (iTotalPayment * 100).ToString();
    }
    public string GetUserEmail()
    {     
        if (_theUser==null)
        {
            _theUser = (User)Session["User"];
        }        
        return _theUser.Email;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        _theUser = (User)Session["User"];
        if(IsPostBack)
        {
            if(Request.Form["stripeToken"]!=null)
            {
                string stripeEmail = Request.Form["stripeEmail"].ToString();
                lblMessage.Text += "<br />Email:" + stripeEmail;
                lblMessage.Text += "<br />Stripe Token:" + Request.Form["stripeToken"].ToString();
                //pnlStripButton.Visible = false;

                //var secretKey = WebConfigurationManager.AppSettings["StripeSecretKey"];
                var secretKey = SystemData.SystemOption_ValueByKey_Account("StripeSecretKey", null, null);
                StripeConfiguration.SetApiKey(secretKey);
                              

                var options = new StripeChargeCreateOptions
                {
                    Amount = int.Parse(GetTotalPaymentAmount()),
                    Currency = "aud",
                    Description = "Charge for testing",
                    SourceTokenOrExistingSourceId=Request.Form["stripeToken"].ToString(),
                    Metadata = new Dictionary<string, string>()
                        {
                            { "UserID", _theUser.UserID.ToString() },
                            { "AccountID", Session["AccountID"].ToString() },
                            { "BillingID", "-1" }
                        }
                    //CustomerId = customer.Id
                    
                };
              
                var service = new StripeChargeService();
                StripeCharge charge = service.Create(options);

                //var invociceItemOptions = new StripeInvoiceItemCreateOptions
                //{
                //    Amount = int.Parse(GetTotalPaymentAmount()),
                //    Currency = "aud",
                //    CustomerId = customer.Id,
                //    Description = "One-time setup fee- quick"
                    
                //};

                //var service = new StripeInvoiceItemService();
                ////invociceItemOptions.CustomerId=
                //var invoiceItem = service.Create(invociceItemOptions);

                //var invoiceOptions = new StripeInvoiceCreateOptions
                //{
                //    Metadata = new Dictionary<string, string>()
                //        {
                //            { "UserID", _theUser.UserID.ToString() },
                //            { "AccountID", Session["AccountID"].ToString() }
                //        }                      
                //        //,
                //    //CustomerId = "cus_4fdAW5ftNQow1a",
                //    //AutoAdvance = true, // auto-finalize this draft after ~1 hour
                //};

                

                //var serviceI = new StripeInvoiceService();
                //var invoice = serviceI.Create(customer.Id,  invoiceOptions);



                ////var items = new List<StripeSubscriptionItemOption> {
                //    new StripeSubscriptionItemOption {
                //        PlanId = "ID_Daily01",
                //        Quantity=10000
                //    }
                //};      
               
                //var options = new StripeSubscriptionCreateOptions
                //{
                //    Items = items,
                //    Source = Request.Form["stripeToken"].ToString()
                //};
                //var service = new StripeSubscriptionService();
                //StripeSubscription subscription = service.Create(customer.Id, options);


                lblMessage.Text = "Thank you!<br />" + lblMessage.Text;
            }
        }

    }
    protected void btnPaypal_Click(object sender, EventArgs e)
    {
        string stripePaypalMonthlyAmount = SystemData.SystemOption_ValueByKey_Account("Paypal Monthly Amount", null, null);
        Session["PaypalItemName"] = "DBGurus eContractor Payment";
        Session["PaymentAmount"] = stripePaypalMonthlyAmount == "" ? "1" : stripePaypalMonthlyAmount;
        Session["PayIntervalType"] = "D";//M
        Session["InvoiceID"] = "tdb_" + Guid.NewGuid().ToString(); // a TDB Reference

        Response.Redirect("~/Pages/Security/PayPal/sendpayment_recurring.aspx", false);
    }
}



// Application_Start
//
//try
//{
//    var secretKey = WebConfigurationManager.AppSettings["StripeSecretKey"];
//    StripeConfiguration.SetApiKey(secretKey);


//    var options = new StripePlanCreateOptions
//    {
//        Currency = "aud",
//        Interval = "month",
//        Name = "Monthly10",
//        Amount = 11,
//    };
//    var service = new StripePlanService();
//    StripePlan plan = service.Create(options);

//}
//catch
//{
//    //
//}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        // ClearForm();

    }


    protected void btnSignUp_Click(object sender, EventArgs e)
    {
        SendSignupEmail(txtName.Text, txtPhone.Text, txtEmail.Text, true, "SignUpEmail");
    }

    protected void btnSignUp2_Click(object sender, EventArgs e)
    {
        SendSignupEmail(txtName2.Text, txtMessage.Text, txtEmail2.Text, false, "ContactUsEmail");
    }

    private void SendSignupEmail(string name, string phone, string email, bool bIsSignUp, string Contentkey)
    {

        Content theContent = SystemData.Content_Details_ByKey(Contentkey, null);

        string sErrorMessage = "";
        string message = theContent.ContentP;

        if (bIsSignUp)
        {
            theContent.ContentP = theContent.ContentP.Replace("[Name]", txtName.Text);
            theContent.ContentP = theContent.ContentP.Replace("[Phone]", txtPhone.Text);
            theContent.ContentP = theContent.ContentP.Replace("[Email]", txtEmail.Text);

        }
        else
        {
            //need to ask JB
            theContent.ContentP = theContent.ContentP.Replace("[Name]", txtName2.Text);
            theContent.ContentP = theContent.ContentP.Replace("[Message]", txtMessage.Text);
            theContent.ContentP = theContent.ContentP.Replace("[Email]", txtEmail2.Text);
        }


        SendEmail(theContent.Heading, theContent.ContentP,
            email, out sErrorMessage);

        if (sErrorMessage == "")
        {
            ClearForm();
            lblMessage.Text = "Thank you! Your request has been sent and we will get back to you within one business day to discuss your requirements.";
            lblMessage.ForeColor = System.Drawing.Color.GreenYellow;
            lblMessage2.Text = lblMessage.Text;
            lblMessage2.ForeColor = lblMessage.ForeColor;
        }
        else
        {
            lblMessage.Text = "Sorry there was a problem sending the email. Can you please try again later or contact us at info@dbgurus.com.au<br/>" + sErrorMessage;
        }
    }


    private int SendEmail
    (
       string sHeading,
       string sMessage,
       string sTo,
       out string sErrorMessage
    )
    {
        string sFrom = "";
        string strEmailServer = "";
        string smtpUsername = "";
        string smtpPassword = "";
        string strSmtpPort = "";
        string strEnableSSL = "";

        sFrom = SystemData.SystemOption_ValueByKey_Account("EmailFrom", null, null);
        smtpUsername = SystemData.SystemOption_ValueByKey_Account("EmailUsername", null, null);
        smtpPassword = SystemData.SystemOption_ValueByKey_Account("EmailPassword", null, null);
        strEmailServer = SystemData.SystemOption_ValueByKey_Account("EmailServer", null, null);
        strSmtpPort = SystemData.SystemOption_ValueByKey_Account("SmtpPort", null, null);
        strEnableSSL = SystemData.SystemOption_ValueByKey_Account("EnableSSL", null, null);

        //let gets the CC
        string strCC = SystemData.SystemOption_ValueByKey_Account("SignUpEmail_CC", null, null);

        //let gets the BCC
        string strBCC = SystemData.SystemOption_ValueByKey_Account("SignUpEmail_BCC", null, null);

        sErrorMessage = "";
        int returnValue = 0;
        try
        {
            MailMessage oMailMessage = new MailMessage();
            oMailMessage.From = new MailAddress(sFrom);

            string[] sToCollection = sTo.Split(';');
            foreach (string str in sToCollection)
            {
                oMailMessage.To.Add(str);
            }

            if ((strCC != null) && (strCC.Length > 0))
            {
                string[] sCCCollection = strCC.Split(';');
                foreach (string str in sCCCollection)
                {
                    oMailMessage.CC.Add(str);
                }
            }

            if ((strBCC != null) && (strBCC.Length > 0))
            {
                string[] sBCCCollection = strBCC.Split(';');
                foreach (string str in sBCCCollection)
                {
                    oMailMessage.Bcc.Add(str);
                }
            }

            oMailMessage.Subject = sHeading;
            oMailMessage.Body = sMessage;
            oMailMessage.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient(strEmailServer);
            if (((smtpUsername != null) && (smtpUsername.Length > 0)) && ((smtpPassword != null) && (smtpPassword.Length > 0)))
            {
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(smtpUsername, smtpPassword);
                smtp.Credentials = credentials;
                smtp.Port = int.Parse(strSmtpPort);
                smtp.EnableSsl = Convert.ToBoolean(strEnableSSL);
            }
            smtp.Send(oMailMessage);
            returnValue = 1;
        }
        catch (Exception e)
        {
            sErrorMessage = e.Message;
            returnValue = -1;
        }

        return returnValue;
    }

    private void ClearForm()
    {
        txtEmail.Text = string.Empty;
        txtEmail2.Text = string.Empty;
        txtName.Text = string.Empty;
        txtName2.Text = string.Empty;
        txtPhone.Text = string.Empty;
        txtMessage.Text = string.Empty;
        lblMessage.Text = string.Empty;
        lblMessage.Text = string.Empty;
        lblMessage2.Text = string.Empty;
    }
}

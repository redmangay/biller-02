﻿//using Pop3;
//using OpenPOP.POP3;
//using OpenPOP;
//using OpenPOP.MIMEParser;
//using Independentsoft;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using OpenPop.Common;
using OpenPop.Mime;
using OpenPop.Pop3;

using DocGen.DAL;
using System.IO;
using System.IO.Packaging;
using System.Globalization;
using System.Reflection;
using System.Web.Compilation;
using MailKit;
using MailKit.Net.Imap;
using System.Net;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Wordprocessing;
using MimeKit;

public partial class DBEmail_WinSchedules : System.Web.UI.Page
{
    string _strFilesLocation = "";
    string _strFilesPhisicalPath = "";
    string _strAuthority = "xdev.thedatabase.net";
    bool _bTestTime = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        _strFilesPhisicalPath = SystemData.SystemOption_ValueByKey_Account("FilesPhisicalPath", null, null);
        _strFilesLocation = SystemData.SystemOption_ValueByKey_Account("FilesLocation", null, null);

#if (DEBUG)
        _strAuthority="localhost:8081_no"; //MR Local
#endif

        if (!IsPostBack)
        {

            Page.Server.ScriptTimeout = 3600;
            //ALSStatus();

            //ExecuteRegularTasks();
            ExecuteRegularTasks2();
            //ProcessFiles("C:\\FTPRoot");

            //Dynamasters Blast data - Latedata.aspx
            //if (Request.Url.Authority == "emd.thedatabase.net" ||Request.QueryString["blast"]!=null)
            //{
            //    try
            //    {
            //        RegisterAsyncTask(new PageAsyncTask(EcotecImportBlastEvents_A));
            //        if(Request.QueryString["blast"]!=null)
            //        {
            //            return;//only run this method
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        ErrorLog theErrorLog = new ErrorLog(null, " DBEmail -EcotecImportBlastEvents_A ", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            //        SystemData.ErrorLog_Insert(theErrorLog);
            //    }

            //}

            try
            {
                if (Request.QueryString["AutoImportRecords"] != null)
                {
                    if (Request.Url.Authority == "emd.thedatabase.net" && Application["ALSLive"] == null)
                    {
                        RegisterAsyncTask(new PageAsyncTask(AutoImportRecords_A));

                    }
                    else if (Request.Url.Authority != "emd.thedatabase.net")
                    {
                        RegisterAsyncTask(new PageAsyncTask(AutoImportRecords_A));

                    }
                    return;//only run this method
                }
                /* == Red: will be using Special Notification == */
                //else if (Request.QueryString["DataReminderEmail"] != null)
                //{
                //    RegisterAsyncTask(new PageAsyncTask(DataReminderEmail_A));
                //    return;
                //}
                else if (Request.QueryString["BatchAutoProcessImport"] != null)
                {
                    RegisterAsyncTask(new PageAsyncTask(BatchAutoProcessImport_A));
                    return;
                }
                else if (Request.QueryString["ReadIncomingEmails"] != null)
                {
                    RegisterAsyncTask(new PageAsyncTask(ReadIncomingEmails_A));
                    return;
                }

            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, " DBEmail -WinSchedules - " + Request.QueryString["DataReminderEmail"].ToString(), ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }

            try
            {

                if (Application["ALSLive"] == null)
                {
                    RegisterAsyncTask(new PageAsyncTask(AutoImportRecords_A));
                    //AutoImportRecords(); //for testing on local
                }
                else
                {
                    if (Application["ALSLabAttemt"] == null)
                    {
                        Application["ALSLabAttemt"] = 1;
                    }
                    if (int.Parse(Application["ALSLabAttemt"].ToString()) > 2)
                    {
                        Application["ALSLabAttemt"] = null;
                        Application["ALSLive"] = null;
                        RegisterAsyncTask(new PageAsyncTask(AutoImportRecords_A));

                    }
                }

            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, " DBEmail -WinSchedules -AutoImportRecords ", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }


            /* == Red: will be using Special Notification == */
            //try
            //{

            //    RegisterAsyncTask(new PageAsyncTask(DataReminderEmail_A));

            //}
            //catch (Exception ex)
            //{
            //    ErrorLog theErrorLog = new ErrorLog(null, " DBEmail -WinSchedules -DataReminderEmail ", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            //    SystemData.ErrorLog_Insert(theErrorLog);
            //}

            try
            {

                RegisterAsyncTask(new PageAsyncTask(ReadIncomingEmails_A));

            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, " DBEmail -WinSchedules -ReadIncomingEmails ", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }


            try
            {

                RegisterAsyncTask(new PageAsyncTask(BatchAutoProcessImport_A));

            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, " DBEmail -WinSchedules -BatchAutoProcessImport ", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }




            try
            {
                if (Request.Url.Authority == _strAuthority)
                {
                    RegisterAsyncTask(new PageAsyncTask(emd_WinSchedules_A));
                }

            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, " DBEmail - EMD_WinSchedules_A ", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }

            try
            {
                if (Request.Url.Authority == _strAuthority)
                {

                    RegisterAsyncTask(new PageAsyncTask(rrp_WinSchedules_A));
                }

            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, " DBEmail -rrp ", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }

            try
            {
                if (Request.Url.Authority == _strAuthority)
                {
                    RegisterAsyncTask(new PageAsyncTask(syduni_WinSchedules_A));
                }

            }
            catch (Exception ex)
            {
                ErrorLog theErrorLog = new ErrorLog(null, " DBEmail -syduni_WinSchedules_A ", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }




        }

    }





    ////Import Dynamasters Blast data

    //if (Request.Url.Authority == "emd.thedatabase.net")
    //{
    //    try
    //    {
    //        EcotechBlastDataImport ecotechBlastDataImport = new EcotechBlastDataImport();
    //        ecotechBlastDataImport.ImportBlastEvents();
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorLog theErrorLog = new ErrorLog(null, " DBEmail -WinSchedules -EcotechBlastDataImport ", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //        SystemData.ErrorLog_Insert(theErrorLog);
    //    }
    //}


    public void AutoImportRecords()
    {
        int? accountId = null;
        if (Request.QueryString["AccountID"] != null)
        {
            int intValue = 0;
            if (int.TryParse(Request.QueryString["AccountID"], out intValue))
            accountId = intValue;
        }

        string ftpRoot = SystemData.SystemOption_ValueByKey_Account("FTPRootFolder", accountId, null);
        if (!String.IsNullOrEmpty(ftpRoot))
            ProcessFiles(ftpRoot);


        InitEmailProcessingLogRecord();

        bool isAnotherImapConnectionActive = false;
        if (Application["IMAPSessionActive"] != null)
        {
            bool.TryParse(Application["IMAPSessionActive"].ToString(), out isAnotherImapConnectionActive);
        }

        if (!isAnotherImapConnectionActive)
        {
            Application["IMAPSessionActive"] = true;
            ImapClient imap = new ImapClient();
            if (InitImap(imap, accountId))
            {
                IMailFolder processedFolder;
                IMailFolder notProcessedFolder;
                if (CheckFolders(imap, out processedFolder, out notProcessedFolder))
                    ProcessEmails(imap, processedFolder, notProcessedFolder, accountId);

                imap.Disconnect(true);
            }
            Application["IMAPSessionActive"] = false;
        }
        else
        {
            //_logs.Add(new LogMessage("Another IMAP connection is active.", LoggingOption.Minimal, LogMessageCategory.Message));
        }
        FinalizeEmailProcessingLogRecord();
    }

    private void InitEmailProcessingLogRecord()
    {
        try
        {
        }
        catch (Exception)
        {
        }
        finally
        {
        }
    }

    private bool InitImap(ImapClient imap, int? accountId)
    {
        bool ret = false;

        string strImapUserName = SystemData.SystemOption_ValueByKey_Account("ImapUserName", accountId, null);
        string strImapPassword = SystemData.SystemOption_ValueByKey_Account("ImapPassword", accountId, null);
        string strImapServer = SystemData.SystemOption_ValueByKey_Account("ImapServer", accountId, null);
        string strImapPort = SystemData.SystemOption_ValueByKey_Account("ImapPort", accountId, null);
        string strImapEnableSsl = SystemData.SystemOption_ValueByKey_Account("ImapEnableSsl", accountId, null);

        ICredentials credentials = new NetworkCredential(strImapUserName, strImapPassword);

        string protocol;
        string url;
        if (bool.Parse(strImapEnableSsl))
            protocol = "imaps";
        else
            protocol = "imap";
        url = string.Format("{0}://{1}:{2}", protocol, strImapServer, int.Parse(strImapPort));
        Uri uri = new Uri(url);

        try
        {
            imap.Connect(uri);
            imap.Authenticate(credentials);
            ret = true;
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - Init IMAP", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
        }

        return ret;
    }

    private bool CheckFolders(ImapClient imap, out IMailFolder processedFolder, out IMailFolder notProcessedFolder)
    {
        string strImapProcessedFolder = SystemData.SystemOption_ValueByKey_Account("ImapProcessedFolder", null, null);
        string strImapNotProcessedFolder = SystemData.SystemOption_ValueByKey_Account("ImapNotProcessedFolder", null, null);
        bool ret = false;
        processedFolder = null;
        notProcessedFolder = null;
        try
        {
            IMailFolder rootFolder = imap.GetFolder(imap.PersonalNamespaces[0]);

            bool isProcessedFolderExist = false;
            bool isNotProcessedFolderExist = false;
            IEnumerable<IMailFolder> folders = rootFolder.GetSubfolders();
            foreach (IMailFolder folder in folders)
            {
                if (String.Compare(folder.Name, strImapProcessedFolder, StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    isProcessedFolderExist = true;
                    processedFolder = folder;
                }
                else if (String.Compare(folder.Name, strImapNotProcessedFolder, StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    isNotProcessedFolderExist = true;
                    notProcessedFolder = folder;
                }
            }

            if (!isProcessedFolderExist)
            {
                processedFolder = rootFolder.Create("Processed", true);
            }
            if (!isNotProcessedFolderExist)
            {
                notProcessedFolder = rootFolder.Create("Not Processed", true);
            }

            ret = true;
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - Check Folders", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
        }

        return ret;
    }

    private void FinalizeEmailProcessingLogRecord()
    {
        try
        {
        }
        catch (Exception)
        {
        }
        finally
        {
        }
    }



    protected void ProcessEmails(ImapClient imap, IMailFolder processedFolder, IMailFolder notProcessedFolder, int? accountIdImap)
    {
        try
        {
            string strSamplesFolder = _strFilesPhisicalPath + "/UserFiles/AppFiles";

            //work with all messages

            imap.Inbox.Open(FolderAccess.ReadWrite);
            IList<IMessageSummary> messages = imap.Inbox.Fetch(0, -1, MessageSummaryItems.Full | MessageSummaryItems.UniqueId | MessageSummaryItems.BodyStructure);

            List<InComingEmail> listInComingEmail = new List<InComingEmail>();

            foreach (IMessageSummary message in messages)
            {
                bool hasDataAttachment = false;
                bool hasDocumentAttachment = false;

                try
                {
                    //InComingEmail theInComingEmail = EmailManager.ets_InComingEmail_Detail_By_MessageID(messageH.MessageId);

                    //if (theInComingEmail != null)
                    //{
                    //    //found, so we are done
                    //    //tn.Rollback();
                    //    goto L_1;
                    //}

                    //a new single message, we need to play with it.

                    string strInComingAttachFileIDs = "";
                    string strBatchIDs = "";
                    List<Batch> listBatch = new List<Batch>();
                    Dictionary<int, List<int>> dictMappedBatch = new Dictionary<int, List<int>>();

                    //message is a single mail message                   
                    string strDBRefEmailID = "none";
                    if (message != null)
                    {
                        // Check TheDatabase Ref

                        //get the user

                        //User theUser = SecurityManager.User_ByEmail(message.FromEmail, ref connection, ref tn);

                        int iUserID;
                        int iAccountID = -1;

                        int iTableID = -1;
                        bool bUseMapping = false;
                        int iMappingKey = 0;
                        int? iTemplateID = null;
                        string strFileName = "";
                        string strFileNameCriteria = "";
                        // bool bSaveAttachment = false;
                        int? iDocumentTypeID = null;
                        //bool bOkayFileNameCriteriaToSave = false;
                        string[] strFileNameCriteriaArray = null;
                        string itemFileName = "";

                        //KG ticket 2532
                        List<Document> MappedDocuments = new List<Document>();
                        int iMappedAccountID = 0;

                        //KG 26/6/2017 Ticket 2740
                        List<Batch> InvalidBatches = new List<Batch>();

                        DataTable dtUploadEmail = Common.DataTableFromText(
                            @"SELECT     [Table].AccountID, Upload.UploadID, Upload.TableID,  
                                Upload.UploadName, Upload.EmailFrom, Upload.Filename, Upload.SaveAttachment, Upload.FileNameCriteria, Upload.DocumentTypeID 
                                FROM         Upload INNER JOIN
                                   [Table] ON Upload.TableID = [Table].TableID WHERE UploadType = 'Email' AND EmailFrom LIKE '%" +
                            ((MailboxAddress) message.Envelope.From[0]).Address + "%'");

                        if (dtUploadEmail.Rows.Count > 0)
                        {
                        }
                        else
                        {
                            goto EndMsg;
                        }

                        //check if there is any attachements

                        iUserID = int.Parse(SystemData.SystemOption_ValueByKey_Account("AutoUploadUserID", accountIdImap, null));

                        string strFileUniqueName;
                        int unnamed = 0;

                        //KG 3/10/17 - some attachments are skipped so we need to change the checker
                        //foreach (BodyPartBasic attachment in message.Attachments)                  
                        foreach (BodyPartBasic attachment in message.BodyParts.Where(x => x.FileName != null))
                        {
                            MimeEntity entity = imap.Inbox.GetBodyPart(message.UniqueId, attachment);
                            if (entity is MimeKit.MessagePart)
                            {
                                MimeKit.MessagePart rfc822 = (MimeKit.MessagePart) entity;
                                //string path = Path.Combine("", attachment.PartSpecifier + ".eml");
                                //rfc822.Message.WriteTo(path);
                            }
                            else
                            {
                                MimePart part = (MimePart) entity;
                                string fileName = part.FileName;
                                if (string.IsNullOrEmpty(fileName))
                                    fileName = string.Format("unnamed-{0}", ++unnamed);

                                //put here to be used by Docu to save and File to be uploaded (imported)
                                string strFileExtension = "";

                                switch (fileName.Substring(fileName.LastIndexOf('.') + 1).ToLower())
                                {
                                    case "dbf":
                                        strFileExtension = ".dbf";
                                        break;
                                    case "txt":
                                        strFileExtension = ".txt";
                                        break;
                                    case "csv":
                                        strFileExtension = ".csv";
                                        break;
                                    case "xls":
                                        strFileExtension = ".xls";
                                        break;
                                    case "xlsx":
                                        strFileExtension = ".xlsx";
                                        break;
                                    case "pdf":
                                        strFileExtension = ".pdf";
                                        break;
                                    case "doc":
                                        strFileExtension = ".doc";
                                        break;
                                    case "docx":
                                        strFileExtension = ".docx";
                                        break;
                                    default:
                                        strFileExtension = "";
                                        break;
                                }

                                DataTable dtUpload = Common.DataTableFromText(
                                    @"SELECT     [Table].AccountID, Upload.UploadID,
                                Upload.TableID, Upload.MappingKey, Upload.UploadName, Upload.EmailFrom, Upload.Filename, Upload.TemplateID,   
                                Upload.SaveAttachment, Upload.FileNameCriteria, Upload.DocumentTypeID                             
                                FROM         Upload INNER JOIN
                                [Table] ON Upload.TableID = [Table].TableID 
                                WHERE UploadType = 'Email' AND EmailFrom LIKE '%" + ((MailboxAddress) message.Envelope.From[0]).Address + "%'");

                                bool bThisFileIsOK = false;

                                if (dtUpload.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dtUpload.Rows)
                                    {
                                        strFileName = dr["Filename"].ToString();
                                        strFileNameCriteria = dr["FileNameCriteria"].ToString();
                                        //bOkayFileNameCriteriaToSave = false;

                                        if (fileName.ToLower()
                                                .IndexOf(strFileName.ToLower().Replace("%", ""),
                                                    StringComparison.InvariantCultureIgnoreCase) > -1)
                                        {
                                            iAccountID = int.Parse(dr["AccountID"].ToString());
                                            iTableID = int.Parse(dr["TableID"].ToString());
                                            bUseMapping =
                                                dr["MappingKey"] != DBNull.Value &&
                                                int.TryParse(dr["MappingKey"].ToString(), out iMappingKey);
                                            if (dr["TemplateID"] != DBNull.Value)
                                                iTemplateID = int.Parse(dr["TemplateID"].ToString());
                                            bThisFileIsOK = true;
                                            hasDataAttachment = true;

                                            //KG 2/6/2017 - Ticket 2532
                                            //strFileNameCriteriaArray = strFileNameCriteria.Split(';').Select(sValue => sValue.Trim()).ToArray();
                                            //foreach (string strFileNameCriteriaSplitted in strFileNameCriteriaArray)
                                            //{
                                            //    if (item.FileName.ToLower().IndexOf(strFileNameCriteriaSplitted.ToLower().Replace("%", "")) > -1 && !string.IsNullOrEmpty(strFileNameCriteria))
                                            //    {
                                            //        if (dr["DocumentTypeID"] != DBNull.Value)
                                            //            iDocumentTypeID = int.Parse(dr["DocumentTypeID"].ToString());
                                            //        bOkayFileNameCriteriaToSave = true;
                                            //    }
                                            //}

                                            //break;
                                        }

                                        strFileNameCriteriaArray =
                                            strFileNameCriteria.Split(';').Select(sValue => sValue.Trim()).ToArray();

                                        foreach (string strFileNameCriteriaSplitted in strFileNameCriteriaArray)
                                        {
                                            if (fileName.ToLower()
                                                    .IndexOf(strFileNameCriteriaSplitted.ToLower().Replace("%", ""),
                                                        StringComparison.InvariantCultureIgnoreCase) > -1
                                                && itemFileName != fileName &&
                                                !string.IsNullOrEmpty(strFileNameCriteria))
                                            {
                                                //bOkayFileNameCriteriaToSave = false;
                                                iAccountID = int.Parse(dr["AccountID"].ToString());

                                                if (dr["DocumentTypeID"] != DBNull.Value)
                                                    iDocumentTypeID = int.Parse(dr["DocumentTypeID"].ToString());


                                                try
                                                {
                                                    Guid guidNewDocu = Guid.NewGuid();
                                                    string strUniqueNameCriteria =
                                                        guidNewDocu.ToString() + strFileExtension;
                                                    string strDocumentFolder =
                                                        _strFilesPhisicalPath + "\\UserFiles\\Documents";

                                                    long documentSize = 0;
                                                    using (FileStream streamDocument =
                                                        File.Create(strDocumentFolder + "\\" + strUniqueNameCriteria))
                                                    {
                                                        part.ContentObject.DecodeTo(streamDocument);
                                                        documentSize = streamDocument.Length;
                                                    }

                                                    Document newDocument = new Document(null, iAccountID, fileName,
                                                        iDocumentTypeID, strUniqueNameCriteria,
                                                        fileName,
                                                        DateTime.ParseExact(DateTime.Today.ToString("d/M/yyyy"),
                                                            "d/M/yyyy", CultureInfo.InvariantCulture),
                                                        null, null, iUserID,
                                                        null);
                                                    hasDocumentAttachment = true;

                                                    newDocument.Size = documentSize;

                                                    //KG ticket 2532
                                                    if ((bool) dr["UseMapping"])
                                                        MappedDocuments.Add(newDocument);
                                                    else
                                                    {
                                                        int iDocumentIDToEmail =
                                                            DocumentManager.ets_Document_Insert(newDocument);
                                                        strInComingAttachFileIDs =
                                                            strInComingAttachFileIDs + iDocumentIDToEmail.ToString() +
                                                            ",";

                                                        itemFileName = fileName;
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    ErrorLog theErrorLog =
                                                        new ErrorLog(null,
                                                            "Auto Import records - attachment as document ", ex.Message,
                                                            ex.StackTrace, DateTime.Now, Request.Path);
                                                    SystemData.ErrorLog_Insert(theErrorLog);
                                                }

                                                //break;
                                            }
                                        }

                                    }

                                }
                                else
                                {
                                    continue;
                                }

                                bool bRecordxceeded = false;
                                if (iAccountID != -1)
                                {
                                    bRecordxceeded = SecurityManager.IsRecordsExceeded(iAccountID);

                                    //if bRecordxceeded true then may be we need to send an email as we can not display message here

                                    if (bRecordxceeded)
                                    {
                                        Content theContent =
                                            SystemData.Content_Details_ByKey("RecordsExceededAuto", null);

                                        if (theContent != null)
                                        {
                                            User theAccountHolder = SecurityManager.User_AccountHolder(iAccountID);
                                            if (theAccountHolder != null)
                                            {
                                                string sEmailError = "";

                                                //Guid guidNewE = Guid.NewGuid();
                                                //string strEmailUID = guidNewE.ToString();

                                                //EmailLog theEmailLog = new EmailLog(null, (int)iAccountID, theContent.Heading,
                                                //  theAccountHolder.Email, DateTime.Now, null,
                                                //  null,
                                                // theContent.ContentKey, theContent.ContentP);

                                                //theEmailLog.EmailUID = strEmailUID;


                                                Message theMessage = new Message(null, null, null, iAccountID,
                                                    DateTime.Now, "E", "E",
                                                    null, theAccountHolder.Email, theContent.Heading,
                                                    theContent.ContentP, null, "");


                                                DBGurus.SendEmail(theContent.ContentKey, true, null, theContent.Heading,
                                                    theContent.ContentP, "",
                                                    theAccountHolder.Email, "", "", null, theMessage, out sEmailError);
                                            }
                                        }

                                        //tn.Rollback();

                                        InComingEmail newInComingEmailE = new InComingEmail(null,
                                            message.Envelope.Subject, message.Envelope.From.ToString(),
                                            message.Envelope.To[0].ToString(), "", "", "",
                                            DateTime.Now, message.Envelope.MessageId, "RecordsExceededAuto",
                                            "RecordsExceededAuto", "RecordsExceededAuto", "", DateTime.Now,
                                            null, strBatchIDs);
                                        EmailManager.ets_InComingEmail_Insert(newInComingEmailE);

                                        goto EndMsg;
                                    }
                                }

                                if (iTableID == -1 || iAccountID == -1 || bThisFileIsOK == false)
                                {
                                    continue;
                                }
                                Table theTable = RecordManager.ets_Table_Details(iTableID);

                                //Now insert a row in File table & then in get string of FileIDs for IncomingEmail.Attachements
                                Guid guidNew = Guid.NewGuid();
                                //strFileUniqueName = guidNew.ToString() + "." + item.ContentFileName.Substring(item.ContentFileName.LastIndexOf(".") + 1);
                                strFileUniqueName = guidNew.ToString() + strFileExtension;

                                dbgFile newFile = new dbgFile
                                (
                                    null, attachment.ContentDescription,
                                    fileName.Substring(
                                        fileName.LastIndexOf(".", StringComparison.InvariantCultureIgnoreCase) + 1),
                                    fileName, strFileUniqueName,
                                    null, null, iAccountID, false, true);

                                //save it and get FileID

                                int iFileID = EmailManager.ets_File_Insert(newFile);

                                //prepare sting for Email.Attachments or InComingEmail.Attachments
                                strInComingAttachFileIDs = strInComingAttachFileIDs + iFileID.ToString() + ",";
                                //got a attachment here, so lets save it.

                                //message.SaveAttachment(item, strSamplesFolder + "\\" + strFileUniqueName);

                                using (FileStream stream =
                                    File.Create(strSamplesFolder + "\\" + strFileUniqueName))
                                {
                                    part.ContentObject.DecodeTo(stream);
                                }



                                //here i need to create a batch!

                                int iBatchID;
                                string strMsg;


                                ImportTemplate theImportTemplate = null;


                                if (iTemplateID.HasValue)
                                {
                                    theImportTemplate = ImportManager.dbg_ImportTemplate_Detail(iTemplateID.Value);
                                }

                                if ((theImportTemplate != null) && !String.IsNullOrEmpty(theImportTemplate.SPProcessFile))
                                {
                                    if (theImportTemplate.FileFormat.ToLower() == "other")
                                    {
                                        UploadManager.UploadCSV(iUserID, theTable, message.Envelope.Subject,
                                            fileName, guidNew, strSamplesFolder,
                                            out strMsg, out iBatchID, strFileExtension, "",
                                            iAccountID, null, iTemplateID, null, null);
                                    }
                                    else
                                    {
                                        UploadManager.UploadCSVRaw(iUserID, theTable, message.Envelope.Subject,
                                            fileName, guidNew, strSamplesFolder,
                                            out strMsg, out iBatchID, strFileExtension, "",
                                            iAccountID, null);

                                        Byte[] parameters = new Byte[16];
                                        Byte[] batchIDPart = BitConverter.GetBytes((long) iBatchID);
                                        for (int j = 0; j < 8; j++)
                                        {
                                            parameters[j] = batchIDPart[j];
                                            parameters[j + 8] = 0;
                                        }

                                        UploadManager.UploadCSV(iUserID, theTable, message.Envelope.Subject,
                                            fileName, new Guid(parameters), null,
                                            out strMsg, out iBatchID, "preProcessorSQL", null,
                                            iAccountID, null, iTemplateID, null, null);
                                    }
                                }
                                else
                                {
                                    UploadManager.UploadCSV(iUserID, theTable, message.Envelope.Subject,
                                        fileName, guidNew, strSamplesFolder,
                                        out strMsg, out iBatchID, strFileExtension, "",
                                        iAccountID, null, iTemplateID, null, null);
                                }

                                //UploadManager.UploadCSV(iUserID, theTable, messageH.Subject,
                                //    item.FileName, guidNew, strSamplesFolder,
                                //    out strMsg, out iBatchID, strFileExtension, "",
                                //    iAccountID, null, iTemplateID, null);

                                //eventLog1.WriteEntry(strMsg);

                                strBatchIDs = strBatchIDs + iBatchID.ToString() + ",";
                                Batch oBatch = UploadManager.ets_Batch_Details(iBatchID);
                                listBatch.Add(oBatch);

                                //string strImportMsg = UploadManager.ExecuteImportFunctions(oBatch, ref connection, ref tn);                                

                                if (bUseMapping)
                                {
                                    string strMappingSP = MappedImport.GetMappingSP(iMappingKey);

                                    if (!String.IsNullOrEmpty(strMappingSP))
                                    {
                                        // inappropriate: will hurt the temp data list
                                        //oBatch.IsIntermediate = true;
                                        //UploadManager.ets_Batch_Update(oBatch);

                                        List<int> children = new List<int>();
                                        dictMappedBatch.Add(iBatchID, children);

                                        DataSet ds = new DataSet();
                                        string error = String.Empty;
                                        SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString);
                                        connection.Open();
                                        SqlCommand executeCommand = new SqlCommand(strMappingSP, connection)
                                        {
                                            CommandType = CommandType.StoredProcedure,
                                            CommandTimeout = DBGurus.GetCommandTimeout()
                                        };
                                        executeCommand.Parameters.AddWithValue("@TableID", theTable.TableID);
                                        executeCommand.Parameters.AddWithValue("@BatchID", iBatchID);

                                        System.Data.SqlClient.SqlDataAdapter dataAdapter =
                                            new SqlDataAdapter(executeCommand);
                                        try
                                        {
                                            dataAdapter.Fill(ds);
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog theErrorLog = new ErrorLog(null, "Auto Import records - mapping",
                                                ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                                            SystemData.ErrorLog_Insert(theErrorLog);
                                        }

                                        connection.Close();
                                        connection.Dispose();


                                        if (ds != null && (ds.Tables.Count > 0) && (ds.Tables[0].Rows.Count > 0))
                                        {
                                            int parentBatchID = iBatchID;
                                            foreach (DataRow dr in ds.Tables[0].Rows)
                                            {
                                                int targetTableID = 0;
                                                if (int.TryParse(dr[0].ToString(), out targetTableID))
                                                {
                                                    Table targetTable = RecordManager.ets_Table_Details(targetTableID);

                                                    //KG Ticket 2532
                                                    iMappedAccountID = (int) targetTable.AccountID;

                                                    Byte[] parameters = new Byte[16];
                                                    Byte[] batchIDPart = BitConverter.GetBytes((long) parentBatchID);
                                                    Byte[] tableIDPart =
                                                        BitConverter.GetBytes(theTable.TableID.Value);
                                                    Byte[] mappingKeyPart = BitConverter.GetBytes(iMappingKey);
                                                    for (int j = 0; j < 8; j++)
                                                    {
                                                        parameters[j] = batchIDPart[j];
                                                        if (j < 4)
                                                            parameters[j + 8] = tableIDPart[j];
                                                        else
                                                            parameters[j + 8] = mappingKeyPart[j - 4];
                                                    }

                                                    //Table tempTableMapped = RecordManager.ets_Table_Details(targetTableID);

                                                    //UploadManager.UploadCSV(iUserID, tempTableMapped, messageH.Subject,
                                                    //    "", new Guid(parameters),
                                                    //    "", out strMsg, out iBatchID, "als", "",
                                                    //        tempTableMapped.AccountID, null, null, parentBatchID);



                                                    UploadManager.UploadCSV(iUserID, targetTable,
                                                        message.Envelope.Subject,
                                                        null, new Guid(parameters), strSamplesFolder,
                                                        out strMsg, out iBatchID, "virtual", "", iMappedAccountID, null,
                                                        null,
                                                        null, null);

                                                    strBatchIDs = strBatchIDs + iBatchID.ToString() + ",";
                                                    Batch oMapBatch = UploadManager.ets_Batch_Details(iBatchID);
                                                    listBatch.Add(oMapBatch);
                                                    dictMappedBatch[parentBatchID].Add(iBatchID);
                                                }
                                            }
                                        }

                                        //KG 26/6/2017 Ticket 2740
                                        if (ds != null && (ds.Tables.Count > 1) && (ds.Tables[1].Rows.Count > 0))
                                        {
                                            foreach (DataRow dr in ds.Tables[1].Rows)
                                            {
                                                int iNewAccountID = -1;
                                                string strMappedAccountID = Common.GetValueFromSQL(
                                                    "SELECT TOP 1 AccountID FROM [Account] WHERE UPPER([AccountName])='"
                                                    + dr[0].ToString().ToUpper() + "'");
                                                if (int.TryParse(strMappedAccountID, out iNewAccountID))
                                                {
                                                    Batch oMapBatch = UploadManager.ets_Batch_Details(iBatchID);

                                                    Batch newBatch = new Batch(
                                                        null,
                                                        null,
                                                        oMapBatch.BatchDescription,
                                                        oMapBatch.UploadedFileName,
                                                        DateTime.Now,
                                                        new Guid(),
                                                        oMapBatch.UserIDUploaded,
                                                        iNewAccountID
                                                    );

                                                    int iNewBatchID = UploadManager.ets_Batch_Insert(newBatch);
                                                    newBatch.BatchID = iNewBatchID;
                                                    newBatch.SourceBatchID = iBatchID;
                                                    InvalidBatches.Add(newBatch);
                                                }
                                            }
                                        }
                                    }
                                }

                                // commented 8/6/2017 by KG for Ticket 2532
                                //// meets the Auto Upload: "FileName" and "Saveattachment => FileNameCriteria" requirements
                                //if (bOkayFileNameCriteriaToSave)
                                //{
                                //    try

                                //    {

                                //        Guid guidNewDocu = Guid.NewGuid();
                                //        string strUniqueNameCriteria = guidNewDocu.ToString() + strFileExtension;

                                //        string strDocumentFolder = _strFilesPhisicalPath + "\\UserFiles\\Documents";

                                //        FileStream StreamDocument = new FileStream(strDocumentFolder + "\\" + strUniqueNameCriteria, FileMode.Create);
                                //        BinaryWriter BinaryStreamDocument = new BinaryWriter(StreamDocument);
                                //        BinaryStreamDocument.Write(item.Body);
                                //        long DocumentSize = BinaryStreamDocument.BaseStream.Length;
                                //        BinaryStreamDocument.Close();

                                //        Document newDocument = new Document(null, iAccountID, item.FileName,
                                //        iDocumentTypeID, strUniqueNameCriteria,
                                //        item.FileName, DateTime.ParseExact(DateTime.Today.ToString("d/M/yyyy"), "d/M/yyyy", CultureInfo.InvariantCulture),
                                //        null, null, iUserID,
                                //        null);

                                //        newDocument.Size = DocumentSize;

                                //        int iDocumentIDToEmail = DocumentManager.ets_Document_Insert(newDocument);
                                //        strInComingAttachFileIDs = strInComingAttachFileIDs + iDocumentIDToEmail.ToString() + ",";

                                //    }
                                //    catch (Exception ex)
                                //    {
                                //        ErrorLog theErrorLog = new ErrorLog(null, "Auto Import records - attachment as document ", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                                //        SystemData.ErrorLog_Insert(theErrorLog);
                                //    }
                                //}
                            }
                        } //end attachment foreach

                        if (iTableID == -1 || iAccountID == -1)
                        {
                            //tn.Rollback();
                            //connection.Close();
                            //connection.Dispose();

                            goto EndMsg;
                        }

                        //tn.Commit();
                        //connection.Close();
                        //connection.Dispose();   

                        //Lets import and  send emails.

                        try
                        {
                            //KG Ticket 2532 : save document to target account
                            foreach (Document mappedDocument in MappedDocuments)
                            {
                                mappedDocument.AccountID = iMappedAccountID;
                                if (mappedDocument.DocumentTypeID != null && mappedDocument.DocumentTypeID != 0)
                                {
                                    DocumentType OriginalDocementType =
                                        DocumentManager.ets_DocumentType_Detail((int) mappedDocument.DocumentTypeID);
                                    string strTargetDocementTypeID = Common.GetValueFromSQL(
                                        "SELECT [DocumentTypeID] FROM [DocumentType] WHERE [DocumentTypeName]='"
                                        + OriginalDocementType.DocumentTypeName + "' AND [AccountID] =" +
                                        iMappedAccountID);
                                    if (strTargetDocementTypeID != "" && strTargetDocementTypeID != "0")
                                        mappedDocument.DocumentTypeID = int.Parse(strTargetDocementTypeID);
                                }

                                int iDocumentIDToEmail = DocumentManager.ets_Document_Insert(mappedDocument);

                                strInComingAttachFileIDs = strInComingAttachFileIDs + iDocumentIDToEmail.ToString() +
                                                           ",";

                                //itemFileName = item.FileName;
                            }

                            foreach (Batch item in listBatch)
                            {
                                if (item.BatchID.HasValue && !dictMappedBatch.ContainsKey(item.BatchID.Value))
                                {

                                    string strRollBackSQL =
                                        @"DELETE Record WHERE BatchID=" + item.BatchID.ToString() +
                                        "; Update Batch set IsImported=0 WHERE BatchID=" + item.BatchID.ToString() +
                                        ";";

                                    string strImportMsg = UploadManager.ExecuteImportFunctions(item);
                                    if (strImportMsg != "ok")
                                    {
                                        Common.ExecuteText(strRollBackSQL);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //eventLog1.WriteEntry("Sending email failed." + " - " + ex.Message + ex.StackTrace);
                            ErrorLog theErrorLog = new ErrorLog(null, "Auto Import records", ex.Message, ex.StackTrace,
                                DateTime.Now, Request.Path);
                            SystemData.ErrorLog_Insert(theErrorLog);
                        }


                        try
                        {
                            foreach (Batch item in listBatch)
                            {
                                //UploadManager.SamplesImportEmail(item);
                                string strImportedSamples = "0";
                                string strRoot = Request.Url.Scheme + "://" + Request.Url.Authority +
                                                 Request.ApplicationPath;

                                if (item.BatchID.HasValue && dictMappedBatch.ContainsKey(item.BatchID.Value))
                                {
                                    string strMappingUpdateBatchSP = MappedImport.GetMappingUpdateBatchSP(iMappingKey);

                                    if (!String.IsNullOrEmpty(strMappingUpdateBatchSP))
                                    {
                                        SqlCommand cmd = new SqlCommand();
                                        cmd.Parameters.AddWithValue("@TableID", item.TableID);
                                        cmd.Parameters.AddWithValue("@BatchID", item.BatchID);
                                        string error = "";
                                        DBGurus.ExecuteSP(strMappingUpdateBatchSP, cmd, out error);
                                    }

                                    MappedImport.NotificationMessage(iMappingKey, item,
                                        dictMappedBatch[item.BatchID.Value]);

                                    //KG 26/6/2017 Ticket 2740
                                    foreach (Batch InvalidBatch in InvalidBatches)
                                    {
                                        if (InvalidBatch.SourceBatchID == item.BatchID)
                                        {
                                            Batch updatedBatch = UploadManager.ets_Batch_Details((int) item.BatchID);
                                            if (updatedBatch.ValidRecordCount == 0)
                                            {
                                                Common.ExecuteText(
                                                    "UPDATE Batch SET ImportTemplateID= " +
                                                    updatedBatch.ImportTemplateID +
                                                    " ,ValidRecordCount =" + updatedBatch.ValidRecordCount +
                                                    " ,WarningRecordCount =" + updatedBatch.WarningRecordCount +
                                                    " ,InvalidRecordCount =" + updatedBatch.InvalidRecordCount +
                                                    " ,UploadEmail ='" + updatedBatch.UploadEmail + "'" +
                                                    " ,WarningEmail ='" + updatedBatch.WarningEmail + "'" +
                                                    " WHERE BatchID=" + InvalidBatch.BatchID.ToString());
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //KG 26/4/2017 Ticket 2516
                                    Common.IsExceedance = true;
                                    Common.IsWarning = true;

                                    UploadManager.RecordsImportEmail(item, ref strImportedSamples, strRoot);
                                }

                                //UploadManager.RecordsImportEmail(item, ref strImportedSamples, strRoot);  

                                //SecurityManager.CheckSamplesExceeded((int)item.AccountID);
                            }
                        }
                        catch (Exception ex)
                        {
                            //eventLog1.WriteEntry("Sending email failed." + " - " + ex.Message + ex.StackTrace);
                            ErrorLog theErrorLog = new ErrorLog(null, "Auto Import records - notification", ex.Message,
                                ex.StackTrace, DateTime.Now, Request.Path);
                            SystemData.ErrorLog_Insert(theErrorLog);
                        }

                        string strTextMessage = "";

                        //KG 9/5/17 Added checker for TextBody and HtmlBody first to avoid throwing errors
                        //TextPart = imap.Inbox.GetBodyPart(message.UniqueId, message.TextBody) as TextPart;
                        TextPart plainText = null;
                        if (message.TextBody != null)
                        {
                            plainText = imap.Inbox.GetBodyPart(message.UniqueId, message.TextBody) as TextPart;
                        }
                        if (plainText != null)
                        {
                            strTextMessage = plainText.Text;
                        }

                        string strBodyMessage = "";

                        //TextPart html = imap.Inbox.GetBodyPart(message.UniqueId, message.HtmlBody) as TextPart;
                        TextPart html = null;
                        if (message.HtmlBody != null)
                        {
                            html = imap.Inbox.GetBodyPart(message.UniqueId, message.HtmlBody) as TextPart;
                        }
                        if (html != null)
                        {
                            strBodyMessage = html.Text;
                        }

                        InComingEmail newInComingEmail = new InComingEmail(null, message.Envelope.Subject,
                            message.Envelope.From.ToString(),
                            message.Envelope.To[0].ToString(), "", "", strInComingAttachFileIDs,
                            DateTime.Now, message.Envelope.MessageId, strBodyMessage, strTextMessage, strBodyMessage,
                            "", DateTime.Now,
                            null, strBatchIDs);
                        EmailManager.ets_InComingEmail_Insert(newInComingEmail);

                        //Message theMessage2 = new Message(null, null, iTableID, iAccountID, DateTime.Now, "E", "E",
                        //    true, messageH.From.Address, messageH.Subject, strTextMessage, null, "");

                        //EmailManager.Message_Insert(theMessage2, null, null);
                    }
                    else
                    {
                        // message is empty, so do nothing  
                        //tn.Rollback();
                        //connection.Close();
                        //connection.Dispose();
                    }

                    EndMsg:

                    //do nothing    
                    int iWhy = 0;
                    //eventLog1.WriteEntry("ReadUploadEmail OK");
                    //tn.Rollback();
                }
                catch (Exception ex)
                {
                    //tn.Rollback();
                    //connection.Close();
                    //connection.Dispose();
                    //if (popClient.Connected)
                    //    popClient.Disconnect();

                    ErrorLog theErrorLog = new ErrorLog(null, "Auto Import records", ex.Message, ex.StackTrace,
                        DateTime.Now, Request.Path);
                    SystemData.ErrorLog_Insert(theErrorLog);
                }
                finally
                {
                    imap.Inbox.MoveTo(message.UniqueId,
                        (hasDataAttachment || hasDocumentAttachment) ? processedFolder : notProcessedFolder);
                }
            }
            //end of message reading

            //connection.Close();
            //connection.Dispose();
            //eventLog1.WriteEntry("ReadUploadEmail OK");

        }
        catch (Exception ex)
        {

            //tn.Rollback();
            //connection.Close();
            //connection.Dispose();
            ErrorLog theErrorLog = new ErrorLog(null, "Auto Import records", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);

            //eventLog1.WriteEntry("ReadUploadEmail Error: " + ex.Message.ToString() + "--Detail-->" + ex.StackTrace);
        }
    }



    protected void ProcessFiles(string rootFolder)
    {
        const string PROCESSED = "Processed";
        const string NOT_PROCESSED = "Not Processed";

        if (Directory.Exists(rootFolder))
        {
            int maxNumberOfFiles = 0;
            int.TryParse(SystemData.SystemOption_ValueByKey_Account("FTPFilesToProcess", null, null), out maxNumberOfFiles);
            if (maxNumberOfFiles == 0)
                maxNumberOfFiles = int.MaxValue;

            foreach (string location in Directory.EnumerateDirectories(rootFolder, "*.*", SearchOption.AllDirectories))
            {
                try
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(location);
                    if (String.Compare(dirInfo.Name, PROCESSED, true) == 0 || String.Compare(dirInfo.Name, NOT_PROCESSED, true) == 0)
                        continue;

                    DataTable dtUploadFile = Common.DataTableFromText(
                        @"SELECT [Table].AccountID, Upload.UploadID, Upload.TableID, 
                                Upload.UseMapping, Upload.EmailFrom, Upload.Filename, Upload.TemplateID,   
                                Upload.FileNameCriteria, Upload.DocumentTypeID                             
                                FROM Upload INNER JOIN
                                [Table] ON Upload.TableID = [Table].TableID 
                                WHERE UploadType = 'FTP' AND EmailFrom = '" + location.Substring(rootFolder.Length + 1) + "'");
                    //RP Added Ticket 3427 "Folder Ignored FTP Auto Upload"
                    if (dtUploadFile.Rows.Count == 0)
                        continue;
                        
                    int iUserId = int.Parse(SystemData.SystemOption_ValueByKey_Account("AutoUploadUserID", null, null));

                    int fileCount = 0;
                    foreach (string file in Directory.EnumerateFiles(location, "*.*"))
                    {
                        fileCount++;
                        if (fileCount > maxNumberOfFiles)
                            break;

                        FileInfo fi = new FileInfo(file);
                        bool isDataFile = false;
                        bool isDocumentFile = false;
                        bool isRecordsExceeded = false;

                        try
                        {
                            string strIncomingFileIds = "";
                            string strBatchIds = "";
                            List<Batch> listBatch = new List<Batch>();
                            Dictionary<int, List<int>> dictMappedBatch = new Dictionary<int, List<int>>();

                            int iAccountId = -1;
                            int iTableId = -1;
                            bool bUseMapping = false;
                            int? iTemplateId = null;
                            int? iDocumentTypeId = null;
                            string itemFileName = "";

                            List<Document> mappedDocuments = new List<Document>();
                            int iMappedAccountId = 0;

                            List<Batch> invalidBatches = new List<Batch>();

                            if (dtUploadFile.Rows.Count > 0)
                            {
                                string strFileExtension = fi.Extension.ToLower();

                                foreach (DataRow dr in dtUploadFile.Rows)
                                {
                                    string strFileName = dr["Filename"].ToString();
                                    string strFileNameCriteria = dr["FileNameCriteria"].ToString();

                                    if (fi.Name.ToLower()
                                            .IndexOf(strFileName.ToLower().Replace("%", ""),
                                                StringComparison.InvariantCultureIgnoreCase) > -1)
                                    {
                                        isDataFile = true;
                                        iAccountId = int.Parse(dr["AccountID"].ToString());
                                        iTableId = int.Parse(dr["TableID"].ToString());
                                        bUseMapping = (bool) dr["UseMapping"];
                                        if (dr["TemplateID"] != DBNull.Value)
                                            iTemplateId = int.Parse(dr["TemplateID"].ToString());
                                    }

                                    string[] strFileNameCriteriaArray =
                                        strFileNameCriteria.Split(';').Select(sValue => sValue.Trim()).ToArray();
                                    foreach (string strFileNameCriteriaSplitted in strFileNameCriteriaArray)
                                    {
                                        if (fi.Name.ToLower()
                                                .IndexOf(strFileNameCriteriaSplitted.ToLower().Replace("%", ""),
                                                    StringComparison.InvariantCultureIgnoreCase) > -1 &&
                                            itemFileName != fi.Name && !string.IsNullOrEmpty(strFileNameCriteria))
                                        {
                                            if (dr["DocumentTypeID"] != DBNull.Value)
                                                iDocumentTypeId = int.Parse(dr["DocumentTypeID"].ToString());

                                            try
                                            {
                                                Guid guidNewDocu = Guid.NewGuid();
                                                string strUniqueFileName = guidNewDocu.ToString() + strFileExtension;
                                                string strDocumentFolder =
                                                    _strFilesPhisicalPath + "\\UserFiles\\Documents";

                                                File.Copy(fi.FullName, strDocumentFolder + "\\" + strUniqueFileName);
                                                long documentSize = fi.Length;

                                                Document newDocument = new Document(null, iAccountId, fi.Name,
                                                    iDocumentTypeId,
                                                    strUniqueFileName, fi.FullName, fi.CreationTime,
                                                    null, null, iUserId, null)
                                                {
                                                    Size = documentSize
                                                };

                                                isDocumentFile = true;

                                                //KG ticket 2532
                                                if ((bool) dr["UseMapping"])
                                                    mappedDocuments.Add(newDocument);
                                                else
                                                {
                                                    int iDocumentId = DocumentManager.ets_Document_Insert(newDocument);
                                                    strIncomingFileIds =
                                                        strIncomingFileIds + iDocumentId.ToString() + ",";
                                                    itemFileName = fi.Name;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ErrorLog theErrorLog =
                                                    new ErrorLog(null,
                                                        "Auto Import records - file as document ", ex.Message,
                                                        ex.StackTrace, DateTime.Now, Request.Path);
                                                SystemData.ErrorLog_Insert(theErrorLog);
                                            }
                                        }
                                    }
                                }

                                if (iAccountId != -1)
                                {
                                    isRecordsExceeded = SecurityManager.IsRecordsExceeded(iAccountId);

                                    //if bRecordxceeded true then may be we need to send an email as we can not display message here

                                    if (isRecordsExceeded)
                                    {
                                        Content theContent =
                                            SystemData.Content_Details_ByKey("RecordsExceededAuto", null);
                                        if (theContent != null)
                                        {
                                            User theAccountHolder = SecurityManager.User_AccountHolder(iAccountId);
                                            if (theAccountHolder != null)
                                            {
                                                string sEmailError = "";

                                                Message theMessage = new Message(null, null, null, iAccountId,
                                                    DateTime.Now, "E", "E",
                                                    null, theAccountHolder.Email, theContent.Heading,
                                                    theContent.ContentP, null, "");

                                                DBGurus.SendEmail(theContent.ContentKey, true, null, theContent.Heading,
                                                    theContent.ContentP, "",
                                                    theAccountHolder.Email, "", "", null, theMessage, out sEmailError);
                                            }
                                        }
                                    }
                                }

                                if (iTableId != -1 && iAccountId != -1 && !isRecordsExceeded)
                                {
                                    if (isDataFile)
                                    {
                                        string strDataFolder = _strFilesPhisicalPath + "/UserFiles/AppFiles";
                                        Table theTable = RecordManager.ets_Table_Details(iTableId);

                                        Guid guidNew = Guid.NewGuid();
                                        string strFileUniqueName = guidNew.ToString() + strFileExtension;

                                        dbgFile newFile =
                                            new dbgFile(null, fi.FullName, fi.Extension,
                                                fi.Name, strFileUniqueName, null, null, iAccountId, false, true);
                                        int iFileId = EmailManager.ets_File_Insert(newFile);

                                        File.Copy(fi.FullName, strDataFolder + "\\" + strFileUniqueName);

                                        int iBatchId;
                                        string strMsg;

                                        ImportTemplate theImportTemplate = null;

                                        if (iTemplateId.HasValue)
                                        {
                                            theImportTemplate =
                                                ImportManager.dbg_ImportTemplate_Detail(iTemplateId.Value);
                                        }

                                        if ((theImportTemplate != null) && !String.IsNullOrEmpty(theImportTemplate.SPProcessFile))
                                        {
                                            if (theImportTemplate.FileFormat.ToLower() == "other")
                                            {
                                                UploadManager.UploadCSV(iUserId, theTable, "FILE:" + fi.Name, fi.Name,
                                                    guidNew,
                                                    strDataFolder,
                                                    out strMsg, out iBatchId, strFileExtension, "", iAccountId, null,
                                                    iTemplateId, null, null);
                                            }
                                            else
                                            {
                                                UploadManager.UploadCSVRaw(iUserId, theTable, "FILE:" + fi.Name,
                                                    fi.Name, guidNew, strDataFolder,
                                                    out strMsg, out iBatchId, strFileExtension, "",
                                                    iAccountId, null);

                                                Byte[] parameters = new Byte[16];
                                                Byte[] batchIdPart = BitConverter.GetBytes((long) iBatchId);
                                                for (int j = 0; j < 8; j++)
                                                {
                                                    parameters[j] = batchIdPart[j];
                                                    parameters[j + 8] = 0;
                                                }

                                                UploadManager.UploadCSV(iUserId, theTable, "FILE:" + fi.Name,
                                                    fi.Name, new Guid(parameters), null,
                                                    out strMsg, out iBatchId, "preProcessorSQL", null,
                                                    iAccountId, null, iTemplateId, null, null);
                                            }
                                        }
                                        else
                                        {
                                            UploadManager.UploadCSV(iUserId, theTable, "FILE:" + fi.Name,
                                                fi.Name, guidNew, strDataFolder,
                                                out strMsg, out iBatchId, strFileExtension, "",
                                                iAccountId, null, iTemplateId, null, null);
                                        }

                                        strBatchIds = strBatchIds + iBatchId.ToString() + ",";
                                        Batch oBatch = UploadManager.ets_Batch_Details(iBatchId);
                                        listBatch.Add(oBatch);

                                        if (bUseMapping)
                                        {
                                            // inappropriate: will hurt the temp data list
                                            //oBatch.IsIntermediate = true;
                                            //UploadManager.ets_Batch_Update(oBatch);

                                            List<int> children = new List<int>();
                                            dictMappedBatch.Add(iBatchId, children);

                                            DataSet ds = new DataSet();
                                            string error = String.Empty;
                                            SqlConnection connection =
                                                new SqlConnection(DBGurus.strGlobalConnectionString);
                                            connection.Open();
                                            SqlCommand executeCommand = new SqlCommand("dbg_Import_Mapping", connection)
                                            {
                                                CommandType = CommandType.StoredProcedure,
                                                CommandTimeout = DBGurus.GetCommandTimeout(),
                                            };
                                            executeCommand.Parameters.AddWithValue("@TableID", theTable.TableID);
                                            executeCommand.Parameters.AddWithValue("@BatchID", iBatchId);

                                            System.Data.SqlClient.SqlDataAdapter dataAdapter =
                                                new SqlDataAdapter(executeCommand);
                                            try
                                            {
                                                dataAdapter.Fill(ds);
                                            }
                                            catch (Exception ex)
                                            {
                                                ErrorLog theErrorLog = new ErrorLog(null,
                                                    "Auto Import records - mapping",
                                                    ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                                                SystemData.ErrorLog_Insert(theErrorLog);
                                            }
                                            connection.Close();
                                            connection.Dispose();


                                            if (ds != null && (ds.Tables.Count > 0) && (ds.Tables[0].Rows.Count > 0))
                                            {
                                                int parentBatchId = iBatchId;
                                                foreach (DataRow dr in ds.Tables[0].Rows)
                                                {
                                                    int targetTableId = 0;
                                                    if (int.TryParse(dr[0].ToString(), out targetTableId))
                                                    {
                                                        Table targetTable =
                                                            RecordManager.ets_Table_Details(targetTableId);

                                                        //KG Ticket 2532
                                                        iMappedAccountId = (int) targetTable.AccountID;

                                                        Byte[] parameters = new Byte[16];
                                                        Byte[] batchIdPart =
                                                            BitConverter.GetBytes((long) parentBatchId);
                                                        Byte[] tableIdPart =
                                                            BitConverter.GetBytes((long) theTable.TableID.Value);
                                                        for (int j = 0; j < 8; j++)
                                                        {
                                                            parameters[j] = batchIdPart[j];
                                                            parameters[j + 8] = tableIdPart[j];
                                                        }

                                                        UploadManager.UploadCSV(iUserId, targetTable,
                                                            "FILE: " + fi.Name,
                                                            null, new Guid(parameters), strDataFolder,
                                                            out strMsg, out iBatchId, "virtual", "", iMappedAccountId,
                                                            null,
                                                            null,
                                                            null, null);

                                                        strBatchIds = strBatchIds + iBatchId.ToString() + ",";
                                                        Batch oMapBatch = UploadManager.ets_Batch_Details(iBatchId);
                                                        listBatch.Add(oMapBatch);
                                                        dictMappedBatch[parentBatchId].Add(iBatchId);
                                                    }
                                                }
                                            }

                                            //KG 26/6/2017 Ticket 2740
                                            if (ds != null && (ds.Tables.Count > 0) && (ds.Tables[1].Rows.Count > 0))
                                            {
                                                foreach (DataRow dr in ds.Tables[1].Rows)
                                                {
                                                    int iNewAccountId = -1;
                                                    string strMappedAccountId = Common.GetValueFromSQL(
                                                        "SELECT TOP 1 AccountID FROM [Account] WHERE UPPER([AccountName])='"
                                                        + dr[0].ToString().ToUpper() + "'");
                                                    if (int.TryParse(strMappedAccountId, out iNewAccountId))
                                                    {
                                                        Batch oMapBatch = UploadManager.ets_Batch_Details(iBatchId);

                                                        Batch newBatch = new Batch(
                                                            null,
                                                            null,
                                                            oMapBatch.BatchDescription,
                                                            oMapBatch.UploadedFileName,
                                                            DateTime.Now,
                                                            new Guid(),
                                                            oMapBatch.UserIDUploaded,
                                                            iNewAccountId
                                                        );

                                                        int iNewBatchId = UploadManager.ets_Batch_Insert(newBatch);
                                                        newBatch.BatchID = iNewBatchId;
                                                        newBatch.SourceBatchID = iBatchId;
                                                        invalidBatches.Add(newBatch);
                                                    }
                                                }
                                            }
                                        }

                                        //Lets import and  send emails.

                                        try
                                        {

                                            foreach (Batch item in listBatch)
                                            {
                                                if (item.BatchID.HasValue &&
                                                    !dictMappedBatch.ContainsKey(item.BatchID.Value))
                                                {

                                                    string strRollBackSql =
                                                        @"DELETE Record WHERE BatchID=" + item.BatchID.ToString() +
                                                        "; Update Batch set IsImported=0 WHERE BatchID=" +
                                                        item.BatchID.ToString() +
                                                        ";";

                                                    string strImportMsg = UploadManager.ExecuteImportFunctions(item);
                                                    if (strImportMsg != "ok")
                                                    {
                                                        Common.ExecuteText(strRollBackSql);
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog theErrorLog = new ErrorLog(null, "Auto Import records - File",
                                                ex.Message,
                                                ex.StackTrace,
                                                DateTime.Now, Request.Path);
                                            SystemData.ErrorLog_Insert(theErrorLog);
                                        }

                                        try
                                        {
                                            foreach (Batch item in listBatch)
                                            {
                                                string strImportedSamples = "0";
                                                string strRoot = Request.Url.Scheme + "://" + Request.Url.Authority +
                                                                 Request.ApplicationPath;

                                                if (item.BatchID.HasValue &&
                                                    dictMappedBatch.ContainsKey(item.BatchID.Value))
                                                {
                                                    SqlCommand cmd = new SqlCommand();
                                                    cmd.Parameters.AddWithValue("@TableID", item.TableID);
                                                    cmd.Parameters.AddWithValue("@BatchID", item.BatchID);
                                                    string error = "";
                                                    DBGurus.ExecuteSP("dbg_Import_Mapping_Update_Batch", cmd,
                                                        out error);

                                                    ALSCommon.ALSNotificationMessage(item,
                                                        dictMappedBatch[item.BatchID.Value]);

                                                    //KG 26/6/2017 Ticket 2740
                                                    foreach (Batch invalidBatch in invalidBatches)
                                                    {
                                                        if (invalidBatch.SourceBatchID == item.BatchID)
                                                        {
                                                            Batch updatedBatch =
                                                                UploadManager.ets_Batch_Details((int) item.BatchID);
                                                            if (updatedBatch.ValidRecordCount == 0)
                                                            {
                                                                Common.ExecuteText(
                                                                    "UPDATE Batch SET ImportTemplateID= " +
                                                                    updatedBatch.ImportTemplateID +
                                                                    " ,ValidRecordCount =" + updatedBatch
                                                                        .ValidRecordCount +
                                                                    " ,WarningRecordCount =" + updatedBatch
                                                                        .WarningRecordCount +
                                                                    " ,InvalidRecordCount =" + updatedBatch
                                                                        .InvalidRecordCount +
                                                                    " ,UploadEmail ='" + updatedBatch.UploadEmail +
                                                                    "'" +
                                                                    " ,WarningEmail ='" + updatedBatch.WarningEmail +
                                                                    "'" +
                                                                    " WHERE BatchID=" + invalidBatch.BatchID
                                                                        .ToString());
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //KG 26/4/2017 Ticket 2516
                                                    Common.IsExceedance = true;
                                                    Common.IsWarning = true;

                                                    UploadManager.RecordsImportEmail(item, ref strImportedSamples,
                                                        strRoot);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog theErrorLog = new ErrorLog(null,
                                                "Auto Import records - notification",
                                                ex.Message,
                                                ex.StackTrace, DateTime.Now, Request.Path);
                                            SystemData.ErrorLog_Insert(theErrorLog);
                                        }
                                    }

                                    if (isDocumentFile)
                                    {
                                        //KG Ticket 2532 : save document to target account
                                        foreach (Document mappedDocument in mappedDocuments)
                                        {
                                            mappedDocument.AccountID = iMappedAccountId;
                                            if (mappedDocument.DocumentTypeID != null &&
                                                mappedDocument.DocumentTypeID != 0)
                                            {
                                                DocumentType originalDocumentType =
                                                    DocumentManager.ets_DocumentType_Detail((int) mappedDocument
                                                        .DocumentTypeID);
                                                string strTargetDocumentTypeId = Common.GetValueFromSQL(
                                                    "SELECT [DocumentTypeID] FROM [DocumentType] WHERE [DocumentTypeName]='"
                                                    + originalDocumentType.DocumentTypeName + "' AND [AccountID] =" +
                                                    iMappedAccountId);
                                                if (strTargetDocumentTypeId != "" && strTargetDocumentTypeId != "0")
                                                    mappedDocument.DocumentTypeID = int.Parse(strTargetDocumentTypeId);
                                            }

                                            int iDocumentId = DocumentManager.ets_Document_Insert(mappedDocument);
                                            strIncomingFileIds = strIncomingFileIds + iDocumentId.ToString() + ",";
                                        }
                                    }

                                    IncomingFile newIncomingFile = new IncomingFile(null, location, fi.Name,
                                        strIncomingFileIds, fi.CreationTime, DateTime.Now, strBatchIds);
                                    EmailManager.ets_IncomingFile_Insert(newIncomingFile);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ErrorLog theErrorLog = new ErrorLog(null, "Auto Import records - Files", ex.Message,
                                ex.StackTrace,
                                DateTime.Now, Request.Path);
                            SystemData.ErrorLog_Insert(theErrorLog);
                        }
                        finally
                        {
                            string processedFolder = location + "\\" + PROCESSED;
                            string notProcessedFolder = location + "\\" + NOT_PROCESSED;
                            if (!Directory.Exists(processedFolder))
                                Directory.CreateDirectory(processedFolder);
                            if (!Directory.Exists(notProcessedFolder))
                                Directory.CreateDirectory(notProcessedFolder);
                            string targetFolder = ((isDataFile || isDocumentFile) && !isRecordsExceeded
                                ? processedFolder
                                : notProcessedFolder);
                            int dotIndex = fi.Name.IndexOf(".", StringComparison.InvariantCultureIgnoreCase);
                            string targetFileLeft = fi.Name.Substring(0, dotIndex);
                            string targetFileMid = DateTime.Now.ToString("s").Replace(":", "-");
                            string targetFileRight = fi.Name.Substring(dotIndex + 1);
                            if (File.Exists(targetFolder + "\\" + targetFileLeft + "." + targetFileMid + "." +
                                            targetFileRight))
                            {
                                int index = 0;
                                foreach (string f in Directory.EnumerateFiles(targetFolder,
                                    targetFileLeft + "." + targetFileMid + " (*)." + targetFileRight))
                                {
                                    Regex rx = new Regex(@".*\((\d+)");
                                    Match m = rx.Match(f);
                                    if (m != null && m.Groups.Count > 1)
                                    {
                                        int idx = 0;
                                        if (int.TryParse(m.Groups[1].Value, out idx))
                                            if (idx > index)
                                                index = idx;
                                    }
                                }
                                targetFileMid += String.Format(" ({0})", ++index);
                            }
                            File.Move(fi.FullName,
                                targetFolder + "\\" + targetFileLeft + "." + targetFileMid + "." + targetFileRight);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "Auto Import records", ex.Message, ex.StackTrace,
                        DateTime.Now,
                        Request.Path);
                    SystemData.ErrorLog_Insert(theErrorLog);
                }
            }
        }
        else
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Auto Import records - files",
                "Cannot find folder " + rootFolder, "", DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
        }
    }

    private string CreateUriFromFilename(string pFileName)
    {
        pFileName = pFileName.Replace(" ", "_");
        pFileName = "/" + System.IO.Path.GetFileName(pFileName);
        //pFileName = pFileName.Replace("./", "");
        return pFileName;
    }

    protected void BatchAutoProcessImport()
    {
        try
        {
            DataTable dtBatchList = Common.DataTableFromText("SELECT BatchID FROM Batch WHERE AutoProcess=1");

            foreach (DataRow dr in dtBatchList.Rows)
            {

                try
                {
                    Batch newSourceBatch = UploadManager.ets_Batch_Details(int.Parse(dr[0].ToString()));


                    if (newSourceBatch != null)
                    {
                        string strTotalTempRecordCount = Common.GetValueFromSQL("SELECT COUNT(RecordID) FROM [TempRecord] WHERE BatchID="
                            + newSourceBatch.BatchID.ToString());
                        if (strTotalTempRecordCount == "" || strTotalTempRecordCount == "0")
                        {
                            Common.ExecuteText("UPDATE Batch SET AutoProcess=0 WHERE BatchID=" + newSourceBatch.BatchID.ToString());
                            continue;
                        }
                    }

                    string strOutMsg = "";
                    int iFinalBatchID = 0;

                    //Table theTable = RecordManager.ets_Table_Details((int)newSourceBatch.TableID);
                    UploadManager.UploadCSV(null, null, "", "", null, "", out strOutMsg, out iFinalBatchID, "", "",
                        newSourceBatch.AccountID, null, null, newSourceBatch.BatchID, null);
                    Batch theBatch = UploadManager.ets_Batch_Details(iFinalBatchID);

                    string strImportMsg = UploadManager.ExecuteImportFunctions(theBatch);

                    if (strImportMsg == "ok")
                    {
                        Common.ExecuteText("UPDATE Batch SET AutoProcess=0 WHERE BatchID=" + newSourceBatch.BatchID.ToString());
                        //send email
                        string strImportedSamples = "0";
                        string strRoot = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath;
                        UploadManager.RecordsImportEmail(theBatch, ref strImportedSamples, strRoot);
                    }

                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "batch Auto Process", ex.Message + "-- BatchID=" + dr[0].ToString(), ex.StackTrace, DateTime.Now, Request.Path);
                    SystemData.ErrorLog_Insert(theErrorLog);
                }

            }
        }
        catch
        {
            //unknown
        }
    }
    protected void btnSetNullALS_Click(object sender, EventArgs e)
    {
        Application["ALSLive"] = null;
        Application["ALSLabAttemt"] = null;
        ALSStatus();
    }
    protected void ALSStatus()
    {
        if (Application["ALSLive"] != null)
        {
            lblALSLive.Text = " Application[ALSLive]=" + Application["ALSLive"].ToString();
        }
        else
        {
            lblALSLive.Text = " Application[ALSLive]=null";
        }
        if (Application["ALSLabAttemt"] != null)
        {
            lblALSLabAttemt.Text = " Application[ALSLabAttemt]=" + Application["ALSLabAttemt"].ToString();
        }
        else
        {
            lblALSLabAttemt.Text = " Application[ALSLabAttemt]=null";
        }
    }

    // lets process the parts of the incoming email
    //red 2739 19102017
    protected string ProcessIncomingEmail(string SPName, int? iMessageID, int? iUserID, string strMSBody)
    {
        string strValue = "";
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand(SPName, connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = DBGurus.GetCommandTimeout();

                SqlParameter pRV = new SqlParameter("@Return", SqlDbType.VarChar);
                pRV.Size = 4000;
                pRV.Direction = ParameterDirection.Output;
                command.Parameters.Add(pRV);

                if (iMessageID != null)
                    command.Parameters.Add(new SqlParameter("@iMessageID", iMessageID));


                if (iUserID != null)
                    command.Parameters.Add(new SqlParameter("@UserID", iUserID));

                if (strMSBody != "")
                    command.Parameters.Add(new SqlParameter("@Body", strMSBody));

                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                    strValue = pRV.Value.ToString();
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - Process Incoming Email",
                        ex.Message +
                        (ex.InnerException == null
                            ? ""
                            : (Environment.NewLine + " Inner exception: " + ex.InnerException.Message)), ex.StackTrace,
                        DateTime.Now, Request.Path);
                    SystemData.ErrorLog_Insert(theErrorLog);
                }


                connection.Close();
                connection.Dispose();
                return strValue;
            }

        }

    }


    protected void ReadIncomingEmails()
    {
        try
        {
            //red 2739 19102017
            string strSPNameToRun = SystemData.SystemOption_ValueByKey_Account("Process Incoming Email SP", null, null);
            string strProcessNonRecordID = SystemData.SystemOption_ValueByKey_Account("Process Incoming Email With No RecordID", null, null);
            int iUserID = int.Parse(SystemData.SystemOption_ValueByKey_Account("AutoUploadUserID", null, null));

            DataTable dtInComingAccounts = Common.DataTableFromText(@"SELECT AccountID FROM Account WHERE POP3Email IS NOT NULL AND POP3UserName  IS NOT NULL AND POP3Password IS NOT NULL 
                                AND POP3Port IS NOT NULL  AND POP3Server IS NOT NULL  AND POP3SSL IS NOT NULL ");

            string strRecordID = "";

            foreach (DataRow dr in dtInComingAccounts.Rows)
            {
                try
                {
                    strRecordID = "";
                    Account theAccount = SecurityManager.Account_Details(int.Parse(dr["AccountID"].ToString()));
                    if (theAccount != null)
                    {
                        if (theAccount.POP3Email != "" && theAccount.POP3UserName != "" && theAccount.POP3Password != "" && theAccount.POP3Server != ""
                                            && theAccount.POP3Port != null && theAccount.POP3SSL != null)
                        {

                            string strPopEmailFrom = theAccount.POP3Email;
                            string strPopServer = theAccount.POP3Server;
                            string strPopUserName = theAccount.POP3UserName;
                            string strPopPassword = theAccount.POP3Password;
                            string strPopPort = theAccount.POP3Port.ToString();
                            string strPopEnableSSL = theAccount.POP3SSL.ToString();
                            if (strPopEnableSSL == "1")
                                strPopEnableSSL = "True";

                            if (strPopEnableSSL == "0")
                                strPopEnableSSL = "False";

                            Pop3Client popClient = new Pop3Client();

                            try
                            {


                                if (popClient.Connected)
                                    popClient.Disconnect();

                                popClient.Connect(strPopServer, int.Parse(strPopPort), bool.Parse(strPopEnableSSL));
                                popClient.Authenticate(strPopUserName, strPopPassword, OpenPop.Pop3.AuthenticationMethod.UsernameAndPassword);

                                int Count = popClient.GetMessageCount();

                                List<InComingEmail> listInComingEmail = new List<InComingEmail>();
                                int iReadMsgCount = 0;

                                for (int i = Count; i >= 1; i -= 1)
                                {
                                    strRecordID = "";
                                    OpenPop.Mime.Header.MessageHeader messageH = popClient.GetMessageHeaders(i);

                                    if (messageH == null)
                                    {
                                        goto EndMsg;
                                    }

                                    //InComingEmail theServerInComingEmail = new InComingEmail();

                                    //theServerInComingEmail = EmailManager.ets_InComingEmail_Detail_By_SERVER(strPopServer);

                                    //InComingEmail theInComingEmail = EmailManager.ets_InComingEmail_Detail_By_MessageID(messageH.MessageId);


                                    Message theInComingMessage = EmailManager.Message_Detail_BY_ExternalMessageKey(messageH.MessageId);

                                    iReadMsgCount = iReadMsgCount + 1;

                                    if (theInComingMessage != null || iReadMsgCount > 1000)
                                    {
                                        goto EndMailbox;
                                    }


                                    string strSubject = messageH.Subject.Trim();



                                    if (messageH.Subject != "" && messageH.Subject.Trim().Length > 2)
                                    {
                                        if (strSubject.IndexOf("#") > -1)
                                        {
                                            if (strSubject.LastIndexOf("#") > -1)
                                            {
                                                if (strSubject.IndexOf("#") != strSubject.LastIndexOf("#"))
                                                {
                                                    strRecordID = strSubject.Substring(strSubject.IndexOf("#") + 1, strSubject.LastIndexOf("#") - strSubject.IndexOf("#") - 1);

                                                    try
                                                    {
                                                        int iTest = int.Parse(strRecordID);
                                                    }
                                                    catch
                                                    {
                                                        strRecordID = "";
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    //red 2739 19102017
                                    Record theRecord = null;

                                    if (strRecordID != "")
                                    {
                                        // goto EndMsg;
                                        int iRecordID = int.Parse(strRecordID);
                                        theRecord = RecordManager.ets_Record_Detail_Full(iRecordID, null, false);
                                    }

                                    if (theRecord == null && strProcessNonRecordID.ToLower() == "no")
                                    {
                                        goto EndMsg;
                                    }

                                    //red 2739 19102017
                                    //if (theRecord == null)
                                    //{
                                    //    goto EndMsg;
                                    //}

                                    //start a message
                                    OpenPop.Mime.Message message = popClient.GetMessage(i);

                                    if (message != null)
                                    {

                                        try
                                        {
                                            int? iNewMessage = null;
                                            string strMsgBody = "";

                                            //if (theRecord != null)
                                            // {

                                            OpenPop.Mime.MessagePart html = message.FindFirstHtmlVersion();

                                            if (html != null)
                                            {
                                                if (html.Body != null)
                                                    strMsgBody = System.Text.Encoding.UTF8.GetString(html.Body);
                                            }

                                            if (theRecord != null)
                                            {
                                                Message theMessage = new Message(null, theRecord.RecordID, theRecord.TableID, theAccount.AccountID, messageH.DateSent, "E", "E",
                                               true, messageH.From.Address, messageH.Subject, strMsgBody, null, messageH.MessageId);

                                                iNewMessage = EmailManager.Message_Insert(theMessage);
                                            }
                                            // }
                                            else
                                            {
                                                if (strProcessNonRecordID.ToLower() == "yes")
                                                {
                                                    Message theMessage = new Message(null, null, null, theAccount.AccountID, messageH.DateSent, "E", "E",
                                                            true, messageH.From.Address, messageH.Subject, strMsgBody, null, messageH.MessageId);
                                                    iNewMessage = EmailManager.Message_Insert(theMessage);
                                                }
                                            }
                                            
                                            //process incoming emails... red 2932 19102017
                                            if (strSPNameToRun != "" && iNewMessage != null)
                                            {
                                                ProcessIncomingEmail(strSPNameToRun, iNewMessage, iUserID, strMsgBody);
                                            }


                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorLog theErrorLog = new ErrorLog(null, "Incoming Message" + theAccount == null ? "" : "-" + theAccount.AccountName + "--" + strRecordID,
                                                ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                                            SystemData.ErrorLog_Insert(theErrorLog);

                                            goto EndMsg;
                                        }
                                    }

                                    EndMsg:
                                    //do nothing    
                                    int iWhy = 0;

                                }
                            }
                            catch (Exception ex)
                            {

                                ErrorLog theErrorLog = new ErrorLog(null, "Incoming Message" + (theAccount == null ? "" : "-" + theAccount.AccountName + "--" + strRecordID),
                                                          ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                                SystemData.ErrorLog_Insert(theErrorLog);
                            }
                            finally
                            {
                                if (popClient.Connected)
                                    popClient.Disconnect();
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    //
                    ErrorLog theErrorLog = new ErrorLog(null, "IncomingEmail attachments", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                    SystemData.ErrorLog_Insert(theErrorLog);
                }

                EndMailbox:
                int iWhy2 = 0;
            }



        }
        catch (Exception ex)
        {
            //
            ErrorLog theErrorLog = new ErrorLog(null, "IncomingEmail attachments", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
        }
    }


    //protected async System.Threading.Tasks.Task ALSLive_A()
    //{
    //    await System.Threading.Tasks.Task.Run(() => ALSLive());
    //}

    protected async System.Threading.Tasks.Task EcotecImportBlastEvents_A()
    {
        await System.Threading.Tasks.Task.Run(() => EcotecImportBlastEvents());
    }



    protected void EcotecImportBlastEvents()
    {
        try
        {
            EcotechBlastDataImport ecotechBlastDataImport = new EcotechBlastDataImport();
            ecotechBlastDataImport.ImportBlastEvents();
        }
        catch (Exception ex)
        {
            ErrorLog theErrorLog = new ErrorLog(null, " DBEmail -WinSchedules -EcotecImportBlastEvents ", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
        }
    }



    //protected void ALSLive()
    //{
    //    try
    //    {
    //        ALSLiveImport als = new ALSLiveImport();
    //        als.ImportALSLiveData();
    //        AutoImportRecords();
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorLog theErrorLog = new ErrorLog(null, " DBEmail -WinSchedules -ALSLiveImport ", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //        SystemData.ErrorLog_Insert(theErrorLog);
    //    }
    //}


    protected async System.Threading.Tasks.Task ReadIncomingEmails_A()
    {
        await System.Threading.Tasks.Task.Run(() => ReadIncomingEmails());
    }
    protected async System.Threading.Tasks.Task BatchAutoProcessImport_A()
    {
        await System.Threading.Tasks.Task.Run(() => BatchAutoProcessImport());
    }

    /* == Red: will be using Special Notification == */
    //protected async System.Threading.Tasks.Task DataReminderEmail_A()
    //{
    //    await System.Threading.Tasks.Task.Run(() => DataReminderEmail());
    //}

    protected async System.Threading.Tasks.Task AutoImportRecords_A()
    {
        await System.Threading.Tasks.Task.Run(() => AutoImportRecords());
    }

    protected async System.Threading.Tasks.Task syduni_WinSchedules_A()
    {
        await System.Threading.Tasks.Task.Run(() => syduni_WinSchedules());
    }

    protected async System.Threading.Tasks.Task rrp_WinSchedules_A()
    {
        await System.Threading.Tasks.Task.Run(() => rrp_WinSchedules());
    }
    protected async System.Threading.Tasks.Task emd_WinSchedules_A()
    {
        await System.Threading.Tasks.Task.Run(() => emd_WinSchedules());
    }

    protected void emd_WinSchedules()
    {
        System.Net.WebRequest webRequestEMD = System.Net.WebRequest.Create("http://emd.thedatabase.net/DBEmail/WinSchedules.aspx");
        System.Net.WebResponse webRespEMD = webRequestEMD.GetResponse();
    }
    protected void rrp_WinSchedules()
    {
        System.Net.WebRequest webRequestRRP = System.Net.WebRequest.Create("http://rrp.thedatabase.net/DBEmail/WinSchedules.aspx");
        System.Net.WebResponse webRespRRP = webRequestRRP.GetResponse();

    }
    protected void syduni_WinSchedules()
    {
        System.Net.WebRequest webRequestSydUni = System.Net.WebRequest.Create("http://syduni.thedatabase.net/DBEmail/WinSchedules.aspx");
        System.Net.WebResponse webRespSydUni = webRequestSydUni.GetResponse();
    }


    //protected async System.Threading.Tasks.Task all_WinSchedules_A(string strWinSchedules)
    //{
    //    await System.Threading.Tasks.Task.Run(() => all_WinSchedules(strWinSchedules));
    //}

    //protected void all_WinSchedules(string strWinSchedules)
    //{
    //    System.Net.WebRequest webRequestAll = System.Net.WebRequest.Create(strWinSchedules);
    //    System.Net.WebResponse webRespAll = webRequestAll.GetResponse();
    //}

    //protected void AttachIncomingEmails()
    //{
    //    try
    //    {
    //        if (SystemData.SystemOption_ValueByKey_Account("EmailAttachments",null,null).ToLower() == "yes")
    //        {

    //            DataTable dtIncoomingTables = Common.DataTableFromText("SELECT TableID FROM [Table] WHERE IsActive=1 AND JSONAttachmentPOP3 IS NOT NULL AND JSONAttachmentInfo IS NOT NULL");


    //            foreach (DataRow dr in dtIncoomingTables.Rows)
    //            {
    //                try
    //                {

    //                    Table theTable = RecordManager.ets_Table_Details(int.Parse(dr["TableID"].ToString()));
    //                    if (theTable != null)
    //                    {
    //                        if (theTable.JSONAttachmentInfo != "" && theTable.JSONAttachmentPOP3!="")
    //                        {
    //                            AttachmentSetting theAttachmentSetting = JSONField.GetTypedObject<AttachmentSetting>(theTable.JSONAttachmentInfo);

    //                            if (theAttachmentSetting.AttachIncomingEmails != null)
    //                            {
    //                                if ((bool)theAttachmentSetting.AttachIncomingEmails )
    //                                {
    //                                    AttachmentPOP3 theAttachmentPOP3 = JSONField.GetTypedObject<AttachmentPOP3>(theTable.JSONAttachmentPOP3);

    //                                    if (theAttachmentPOP3 != null)
    //                                    {
    //                                        if (theAttachmentPOP3.Email != "" && theAttachmentPOP3.Password != "" && theAttachmentPOP3.POP3Server != ""
    //                                            && theAttachmentPOP3.Port != null && theAttachmentPOP3.SSL != null)
    //                                        {

    //                                            string strPopEmailFrom = theAttachmentPOP3.Email;
    //                                            string strPopServer = theAttachmentPOP3.POP3Server;
    //                                            string strPopUserName = theAttachmentPOP3.Username;
    //                                            string strPopPassword = theAttachmentPOP3.Password;
    //                                            string strPopPort = theAttachmentPOP3.Port.ToString();
    //                                            string strPopEnableSSL = theAttachmentPOP3.SSL.ToString();

    //                                            //string strSamplesFolder = Server.MapPath("~/UserFiles/AppFiles");

    //                                            string strSamplesFolder = _strFilesPhisicalPath + "/UserFiles/AppFiles";

    //                                            //SqlTransaction tn;
    //                                            //SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString);
    //                                            Pop3Client popClient = new Pop3Client();
    //                                            //connection.Open();
    //                                            //tn = connection.BeginTransaction();


    //                                            try
    //                                            {

    //                                                //OpenPOP.POP3.Utility.Log = true;
    //                                                if(popClient.Connected)
    //                                                    popClient.Disconnect();

    //                                                popClient.Connect(strPopServer, int.Parse(strPopPort),bool.Parse(strPopEnableSSL));
    //                                                popClient.Authenticate(strPopUserName, strPopPassword, OpenPop.Pop3.AuthenticationMethod.UsernameAndPassword);

    //                                                int Count = popClient.GetMessageCount();

    //                                                List<InComingEmail> listInComingEmail = new List<InComingEmail>();


    //                                                for (int i = Count; i >= 1; i -= 1)
    //                                                {

    //                                                   OpenPop.Mime.Header.MessageHeader messageH = popClient.GetMessageHeaders(i);

    //                                                    if (messageH == null)
    //                                                    {
    //                                                        goto EndMsg;
    //                                                    }

    //                                                    InComingEmail theServerInComingEmail = new InComingEmail();

    //                                                    theServerInComingEmail = EmailManager.ets_InComingEmail_Detail_By_SERVER(strPopServer);

    //                                                    InComingEmail theInComingEmail = EmailManager.ets_InComingEmail_Detail_By_MessageID(messageH.MessageId);

    //                                                    if (theInComingEmail != null)
    //                                                    {
    //                                                        goto EndMailbox;
    //                                                    }


    //                                                    if (theServerInComingEmail != null)
    //                                                    {
    //                                                        if (theServerInComingEmail.MessageID.ToLower() ==
    //                                                            messageH.MessageId.ToLower())
    //                                                        {
    //                                                            goto EndMailbox;
    //                                                        }
    //                                                    }
    //                                                    else
    //                                                    {


    //                                                        ////not found any email
    //                                                        //InComingEmail newInComingEmail = new InComingEmail(null, "IncomingEmail test", "dbg",
    //                                                        //     "IncomingEmail test", "", "", "",
    //                                                        //     DateTime.Now, messageH.MessageId, messageH.Subject, "", "", "", DateTime.Now,
    //                                                        //     null, "");
    //                                                        //newInComingEmail.POPServer = strPopServer;
    //                                                        //EmailAndIncoming.ets_InComingEmail_Insert(newInComingEmail);

    //                                                        ////tn.Commit();
    //                                                        //goto EndMailbox;


    //                                                    }

    //                                                    //start a message
    //                                                    OpenPop.Mime.Message message = popClient.GetMessage(i);

    //                                                    if (message != null)
    //                                                    {
    //                                                        if (messageH.Subject != "" && messageH.Subject.Trim().Length > 2)
    //                                                        {
    //                                                            string strSubject = messageH.Subject.Trim();

    //                                                            string strRecordID = "";

    //                                                            if (strSubject.IndexOf("#") > -1)
    //                                                            {
    //                                                                if (strSubject.LastIndexOf("#") > -1)
    //                                                                {
    //                                                                    if (strSubject.IndexOf("#") != strSubject.LastIndexOf("#"))
    //                                                                    {
    //                                                                        strRecordID = strSubject.Substring(strSubject.IndexOf("#") + 1, strSubject.LastIndexOf("#") - strSubject.IndexOf("#") - 1);

    //                                                                        try
    //                                                                        {
    //                                                                            int iTest = int.Parse(strRecordID);
    //                                                                        }
    //                                                                        catch
    //                                                                        {
    //                                                                            strRecordID = "";
    //                                                                        }
    //                                                                    }
    //                                                                }
    //                                                            }

    //                                                            //if (messageH.Subject.Trim().Substring(0, 1) == "#" &&
    //                                                            //    messageH.Subject.Trim().Substring(messageH.Subject.Trim().Length - 1,1)=="#") 
    //                                                            if(strRecordID!="")
    //                                                            {
    //                                                                //string strRecordID = messageH.Subject.Trim().Replace("#", "");
    //                                                                try
    //                                                                {
    //                                                                    int iRecordID = int.Parse(strRecordID);

    //                                                                    if (theAttachmentSetting.InIdentifierColumnID != null)
    //                                                                    {
    //                                                                        Column theIdentifierColumn = RecordManager.ets_Column_Details((int)theAttachmentSetting.InIdentifierColumnID);

    //                                                                        if (theIdentifierColumn != null)
    //                                                                        {

    //                                                                            if (theIdentifierColumn.SystemName.ToLower() != "recordid")
    //                                                                            {
    //                                                                                string strDBRecordID = Common.GetValueFromSQL(" SELECT RecordID FROM [Record] WHERE IsActive=1 AND TableID="+theIdentifierColumn.TableID.ToString()
    //                                                                                    +" AND "+theIdentifierColumn.SystemName+"='"+strRecordID+"'");
    //                                                                                if (strDBRecordID != "")
    //                                                                                {
    //                                                                                    iRecordID = int.Parse(strDBRecordID);
    //                                                                                }
    //                                                                            }
    //                                                                        }

    //                                                                    }
    //                                                                    Record theRecord = RecordManager.ets_Record_Detail_Full(iRecordID);
    //                                                                    if (theRecord != null)
    //                                                                    {
    //                                                                        if (theRecord.TableID == theTable.TableID)
    //                                                                        {

    //                                                                            if (theAttachmentSetting.InSavetoTableID != null)
    //                                                                            {
    //                                                                                Record theChildRecord = new Record();
    //                                                                                theChildRecord.TableID = (int)theAttachmentSetting.InSavetoTableID;
    //                                                                                theChildRecord.EnteredBy = theRecord.EnteredBy;

    //                                                                                DataTable dtChildColumn = Common.DataTableFromText("SELECT * FROM [Column] WHERE TableID=" + theChildRecord.TableID.ToString() + " AND TableTableID=" + theTable.TableID.ToString());

    //                                                                                if (dtChildColumn.Rows.Count > 0)
    //                                                                                {
    //                                                                                    Column lnkColumn = RecordManager.ets_Column_Details(int.Parse(dtChildColumn.Rows[0]["LinkedParentColumnID"].ToString()));
    //                                                                                    RecordManager.MakeTheRecord(ref theChildRecord, dtChildColumn.Rows[0]["SystemName"].ToString(), RecordManager.GetRecordValue(ref theRecord, lnkColumn.SystemName));

    //                                                                                }

    //                                                                                if (theAttachmentSetting.InSaveSubjectColumnID != null)
    //                                                                                {
    //                                                                                    Column aColumn = RecordManager.ets_Column_Details((int)theAttachmentSetting.InSaveSubjectColumnID);

    //                                                                                    RecordManager.MakeTheRecord(ref theChildRecord, aColumn.SystemName, messageH.Subject);
    //                                                                                }

    //                                                                                if (theAttachmentSetting.InSaveSenderColumnID != null)
    //                                                                                {
    //                                                                                    Column aColumn = RecordManager.ets_Column_Details((int)theAttachmentSetting.InSaveSenderColumnID);

    //                                                                                    RecordManager.MakeTheRecord(ref theChildRecord, aColumn.SystemName, messageH.From.Address);
    //                                                                                }


    //                                                                                if (theAttachmentSetting.InSaveEmailColumnID != null)
    //                                                                                {
    //                                                                                    Column aColumn = RecordManager.ets_Column_Details((int)theAttachmentSetting.InSaveEmailColumnID);


    //                                                                                    //make .msg file
    //                                                                                    //OpenPOP.MIMEParser.Message
    //                                                                                    //MemoryStream stream = new MemoryStream();
    //                                                                                    //message.Save(stream);

    //                                                                                    //byte[] data = stream.ToArray();
    //                                                                                    //stream.Close();
    //                                                                                    //stream.Dispose();
    //                                                                                    //stream = null;
    //                                                                                    //string strFolder = "../Pages/Record/RecordFile_dev/";
    //                                                                                    //string strFileName = "Message.msg";
    //                                                                                    //string strUniqueName = Guid.NewGuid().ToString() + "_" + strFileName;// +"." + file.FileName.Substring(file.FileName.LastIndexOf('.') + 1).ToLower();
    //                                                                                    //string strPath = Server.MapPath(strFolder + "\\" + strUniqueName);

    //                                                                                    //Independentsoft.Email.Mime.Message mine = new Independentsoft.Email.Mime.Message(data);
    //                                                                                    //Independentsoft.Msg.Message msg = new Independentsoft.Msg.Message(mine);
    //                                                                                    //msg.Save(strPath, true);

    //                                                                                    theServerInComingEmail.MessageID = messageH.MessageId;
    //                                                                                    //string strMessage = message.RawMessage.ToString();
    //                                                                                    OpenPop.Mime.MessagePart html = message.FindFirstHtmlVersion();
    //                                                                                    if (html != null)
    //                                                                                    {
    //                                                                                        if(html.Body!=null)
    //                                                                                            RecordManager.MakeTheRecord(ref theChildRecord, aColumn.SystemName, System.Text.Encoding.UTF8.GetString(html.Body));
    //                                                                                    }
    //                                                                                }
    //                                                                                if (theAttachmentSetting.InSaveAttachmentColumnID != null)
    //                                                                                {

    //                                                                                   List<OpenPop.Mime.MessagePart> Items= message.FindAllAttachments();


    //                                                                                   int iAttachCount = 0;
    //                                                                                   foreach (OpenPop.Mime.MessagePart item in Items)
    //                                                                                   {
    //                                                                                       if (item.FileName != "" && item.IsAttachment)
    //                                                                                       {
    //                                                                                           iAttachCount = iAttachCount + 1;
    //                                                                                       }
    //                                                                                   }

    //                                                                                   if (iAttachCount > 1)
    //                                                                                   {
    //                                                                                       List<string> lstFiles = new List<string>();

    //                                                                                       foreach (OpenPop.Mime.MessagePart item in Items)
    //                                                                                       {
    //                                                                                           if (item.FileName != "" && item.IsAttachment)
    //                                                                                           {
    //                                                                                               string strFileUniqueName = Guid.NewGuid().ToString() + "_" + item.FileName;
    //                                                                                               string strPath = strSamplesFolder + "\\" + strFileUniqueName;
    //                                                                                               FileStream Stream = new FileStream(strPath, FileMode.Create);

    //                                                                                               BinaryWriter BinaryStream = new BinaryWriter(Stream);
    //                                                                                               BinaryStream.Write(item.Body);
    //                                                                                               BinaryStream.Close();

    //                                                                                               lstFiles.Add(strPath);
    //                                                                                           }
    //                                                                                       }

    //                                                                                       string strOnlyZipName=Guid.NewGuid().ToString() + "_AttachmentFiles.zip";
    //                                                                                       string strZipFilename = strSamplesFolder + "\\" + strOnlyZipName;
    //                                                                                      //System.IO.Packaging 
    //                                                                                       Package pZip = ZipPackage.Open(strZipFilename, FileMode.OpenOrCreate, FileAccess.ReadWrite);

    //                                                                                       foreach (string aFilePath in lstFiles)
    //                                                                                       {
    //                                                                                           if(File.Exists(aFilePath))
    //                                                                                           {
    //                                                                                               Uri partUri = new Uri(CreateUriFromFilename(aFilePath), UriKind.Relative);
    //                                                                                               PackagePart pkgPart = pZip.CreatePart(partUri, System.Net.Mime.MediaTypeNames.Application.Zip, CompressionOption.Normal);
    //                                                                                               byte[] arrBuffer = File.ReadAllBytes(aFilePath);
    //                                                                                               pkgPart.GetStream().Write(arrBuffer, 0, arrBuffer.Length);

    //                                                                                           }

    //                                                                                       }
    //                                                                                       pZip.Close();
    //                                                                                       pZip = null;

    //                                                                                       Column aColumn = RecordManager.ets_Column_Details((int)theAttachmentSetting.InSaveAttachmentColumnID);

    //                                                                                       RecordManager.MakeTheRecord(ref theChildRecord, aColumn.SystemName, strOnlyZipName);
    //                                                                                   }
    //                                                                                   else
    //                                                                                   {

    //                                                                                       foreach (OpenPop.Mime.MessagePart item in Items)
    //                                                                                       {
    //                                                                                           if (item.FileName != "" && item.IsAttachment)
    //                                                                                           {
    //                                                                                               string strFileUniqueName = Guid.NewGuid().ToString() + "_" + item.FileName;
    //                                                                                               string strPath = strSamplesFolder + "\\" + strFileUniqueName;
    //                                                                                               FileStream Stream = new FileStream(strPath, FileMode.Create);

    //                                                                                               BinaryWriter BinaryStream = new BinaryWriter(Stream);
    //                                                                                               BinaryStream.Write(item.Body);
    //                                                                                               BinaryStream.Close();

    //                                                                                               Column aColumn = RecordManager.ets_Column_Details((int)theAttachmentSetting.InSaveAttachmentColumnID);

    //                                                                                               RecordManager.MakeTheRecord(ref theChildRecord, aColumn.SystemName, strFileUniqueName);
    //                                                                                               break;
    //                                                                                           }

    //                                                                                       }
    //                                                                                   }

    //                                                                                }


    //                                                                                try
    //                                                                                {
    //                                                                                    RecordManager.ets_Record_Insert(theChildRecord);
    //                                                                                    EmailManager.ets_InComingEmail_Update(theServerInComingEmail, null);

    //                                                                                    InComingEmail newInComingEmail = new InComingEmail(null, "IncomingEmail test", "dbg",
    //                                                                                       "IncomingEmail test", "", "", "",
    //                                                                                       DateTime.Now, messageH.MessageId, messageH.Subject, "", "", "", DateTime.Now,
    //                                                                                       null, "");
    //                                                                                    newInComingEmail.POPServer = strPopServer;
    //                                                                                    EmailManager.ets_InComingEmail_Insert(newInComingEmail);

    //                                                                                }
    //                                                                                catch (Exception ex)
    //                                                                                {
    //                                                                                    //
    //                                                                                    ErrorLog theErrorLog = new ErrorLog(null, "IncomingEmail attachments", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //                                                                                    SystemData.ErrorLog_Insert(theErrorLog);

    //                                                                                }

    //                                                                            }
    //                                                                        }

    //                                                                    }


    //                                                                }
    //                                                                catch(Exception ex)
    //                                                                {

    //                                                                    ErrorLog theErrorLog = new ErrorLog(null, "IncomingEmail attachments", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //                                                                    SystemData.ErrorLog_Insert(theErrorLog);

    //                                                                    goto EndMsg;
    //                                                                }


    //                                                            }

    //                                                        }

    //                                                    }

    //                                                EndMsg:
    //                                                    //do nothing    
    //                                                    int iWhy = 0;

    //                                                }
    //                                                //tn.Commit();
    //                                            }
    //                                            catch(Exception ex)
    //                                            {

    //                                                ErrorLog theErrorLog = new ErrorLog(null, "IncomingEmail attachments", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //                                                SystemData.ErrorLog_Insert(theErrorLog);
    //                                                //tn.Rollback();
    //                                                //
    //                                            }
    //                                            finally
    //                                            {
    //                                                //connection.Close();
    //                                                //connection.Dispose();
    //                                                if (popClient.Connected)
    //                                                    popClient.Disconnect();
    //                                            }
    //                                        }
    //                                    }
    //                                }                               

    //                            }                                
    //                        }
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    //
    //                    ErrorLog theErrorLog = new ErrorLog(null, "IncomingEmail attachments", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //                    SystemData.ErrorLog_Insert(theErrorLog);
    //                }

    //            EndMailbox:
    //                int iWhy2 = 0;
    //            }

    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        //
    //        ErrorLog theErrorLog = new ErrorLog(null, "IncomingEmail attachments", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //        SystemData.ErrorLog_Insert(theErrorLog);
    //    }
    //}


    /* == Red: will be using Special Notification == */
    /*
    protected void DataReminderEmail()
    {

        if (_bTestTime)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Not Error-Testing", "DataReminderEmail", "Start", DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
        }

        string strUpdatedRecordIDs = "-1";
        try
        {
            //int iUserID = int.Parse(SystemData.SystemOption_ValueByKey("AutoUploadUserID"));
            DataTable dtDRUsers = Common.DataTableFromText(@"SELECT     DataReminder.*, DataReminderUser.*,  [Table].TableName,[Table].AccountID,
                 [Column].TableID, [User].FirstName, [User].LastName, [User].Email, 
                 [Column].SystemName, [Column].DisplayName, [Column].ColumnType
                FROM         DataReminder INNER JOIN
                  DataReminderUser ON DataReminder.DataReminderID = DataReminderUser.DataReminderID 
                  INNER JOIN
                  [Column] ON DataReminder.ColumnID = [Column].ColumnID INNER JOIN
                  [Table] ON [Column].TableID = [Table].TableID   
	               LEFT JOIN
                  [User] ON DataReminderUser.UserID = [User].UserID
                  INNER JOIN Account ON Account.AccountID=[Table].AccountID
                  WHERE [Table].IsActive=1 AND Account.IsActive=1 
                  AND ([Column].ColumnType='datetime' OR [Column].ColumnType='date')");


            string strWarningSMSEMail = SystemData.SystemOption_ValueByKey_Account("WarningSMSEmail", null, null);


            foreach (DataRow dr in dtDRUsers.Rows)
            //Red <Begin> foreach "all" datareminder and datareminderuser
            {
                //lets send emails ReminderContent
                DataTable _dtRecordColums = RecordManager.ets_Table_Columns_Summary(int.Parse(dr["TableID"].ToString()), null);


                Table theTable = RecordManager.ets_Table_Details(int.Parse(dr["TableID"].ToString()));


                string strBody = dr["ReminderContent"].ToString();
                Column theColumn = RecordManager.ets_Column_Details(int.Parse(dr["ColumnID"].ToString()));

                //lets find records

                DataTable dtRecords = Common.DataTableFromText("SELECT RecordID FROM Record WHERE IsActive=1 AND TableID=" + dr["TableID"].ToString()
                    + " AND " + theColumn.SystemName + " IS NOT NULL AND ReminderSentDate IS NULL AND ISDATE(" + theColumn.SystemName + ")=1 "
                    + "  AND " + " CONVERT(Datetime," + theColumn.SystemName + ",103) >= CONVERT(Datetime,'" + DateTime.Today.Date.ToShortDateString() + "',103)");


                if (dtRecords.Rows.Count > 0)
                {

                    foreach (DataRow drR in dtRecords.Rows)
                    {
                        Record thisRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drR["RecordID"].ToString()), null, false);

                        DateTime dateReminder;
                        if (!DateTime.TryParseExact(RecordManager.GetRecordValue(ref thisRecord, theColumn.SystemName), Common.Dateformats, new CultureInfo("en-GB"), DateTimeStyles.None, out dateReminder))
                        {

                            if (!DateTime.TryParseExact(RecordManager.GetRecordValue(ref thisRecord, theColumn.SystemName), Common.DateTimeformats, new CultureInfo("en-GB"), DateTimeStyles.None, out dateReminder))
                            {
                                //not a date or valid date format, so continue to next record
                                continue;
                            }

                        }

                        dateReminder = dateReminder.AddDays(-int.Parse(dr["NumberOfDays"].ToString()));

                        //if (dr["ReminderColumnID"] != DBNull.Value)
                        //{
                        //    int iTest = 0;
                        //}

                        if (dateReminder.ToShortDateString() == DateTime.Today.Date.ToShortDateString())
                        {
                            string strEachRecordBody = strBody;
                            DataTable dtColumns = Common.DataTableFromText("SELECT SystemName,DisplayName FROM [Column] WHERE IsStandard=0 AND   TableID="
                                + dr["TableID"].ToString() + "  ORDER BY DisplayName");
                            foreach (DataRow drC in dtColumns.Rows)
                            {
                                strEachRecordBody = strEachRecordBody.Replace("[" + drC["DisplayName"].ToString() + "]", RecordManager.GetRecordValue(ref thisRecord, drC["SystemName"].ToString()));

                            }



                            //Work with 1 top level Parent tables.

                            DataTable dtPT = Common.DataTableFromText("SELECT distinct ParentTableID FROM TableChild WHERE ChildTableID=" + dr["TableID"].ToString()); //AND DetailPageType<>'not'

                            if (dtPT.Rows.Count > 0)
                            {

                                foreach (DataRow drPT in dtPT.Rows)
                                {

                                    for (int i = 0; i < _dtRecordColums.Rows.Count; i++)
                                    {
                                        if (_dtRecordColums.Rows[i]["TableTableID"] != DBNull.Value
                                   && (_dtRecordColums.Rows[i]["DropDownType"].ToString() == "table"
                                   || _dtRecordColums.Rows[i]["DropDownType"].ToString() == "tabledd")
                                    && _dtRecordColums.Rows[i]["ColumnType"].ToString() == "dropdown"
                                   && _dtRecordColums.Rows[i]["DisplayColumn"].ToString() != "")
                                        {
                                            if (_dtRecordColums.Rows[i]["TableTableID"].ToString() == drPT["ParentTableID"].ToString())
                                            {
                                                if (RecordManager.GetRecordValue(ref thisRecord, _dtRecordColums.Rows[i]["SystemName"].ToString()) != "")
                                                {

                                                    //DataTable dtParentRecord = Common.DataTableFromText("SELECT * FROM Record WHERE RecordID=" + drR[_dtRecordColums.Rows[i]["SystemName"].ToString()].ToString());


                                                    Column theLinkedColumn = RecordManager.ets_Column_Details(int.Parse(_dtRecordColums.Rows[i]["LinkedParentColumnID"].ToString()));
                                                    DataTable dtParentRecord = null;
                                                    if (theLinkedColumn.SystemName.ToLower() == "recordid")
                                                    {
                                                        dtParentRecord = Common.DataTableFromText("SELECT RecordID FROM Record WHERE RecordID=" + RecordManager.GetRecordValue(ref thisRecord, _dtRecordColums.Rows[i]["SystemName"].ToString()));
                                                    }
                                                    else
                                                    {
                                                        dtParentRecord = Common.DataTableFromText("SELECT RecordID FROM Record WHERE TableID=" + theLinkedColumn.TableID.ToString() + " AND " + theLinkedColumn.SystemName + "='" +
                                                            RecordManager.GetRecordValue(ref thisRecord, _dtRecordColums.Rows[i]["SystemName"].ToString()).ToString().Replace("'", "''") + "'");
                                                    }

                                                    if (dtParentRecord.Rows.Count > 0)
                                                    {
                                                        Record theParentRecord = RecordManager.ets_Record_Detail_Full(int.Parse(dtParentRecord.Rows[0]["RecordID"].ToString()), null, false);
                                                        //Ricky Modified Ticket 4164
                                                        //DataTable dtColumnsPT = Common.DataTableFromText(@"SELECT distinct SystemName, TableName + ':' + DisplayName AS DP FROM [Column] INNER JOIN [Table]
                                                        //ON [Column].TableID=[Table].TableID WHERE  [Column].IsStandard=0 AND  [Column].TableID=" + drPT["ParentTableID"].ToString());
                                                        DataTable dtColumnsPT = Common.DataTableFromText(@"SELECT distinct [Column].SystemName, TableName + ':' + DisplayName AS DP FROM [Column] INNER JOIN [Table]
                                                        ON [Column].TableID=[Table].TableID WHERE  [Column].IsStandard=0 AND  [Column].TableID=" + drPT["ParentTableID"].ToString());
                                                        //END MODIFICATION
                                                        foreach (DataRow drC in dtColumnsPT.Rows)
                                                        {
                                                            strEachRecordBody = strEachRecordBody.Replace("[" + drC["DP"].ToString() + "]", RecordManager.GetRecordValue(ref theParentRecord, drC["SystemName"].ToString()));

                                                        }
                                                    }



                                                }



                                            }

                                        }

                                    }




                                }



                            }

                            //
                            //lets send an email

                            string strSubject = dr["ReminderHeader"].ToString();

                            string strTo = "";
                            // Red changed, include user who created the record...
                            //if (dr["Email"] != DBNull.Value)
                            //{
                            //    //msg.To.Add(dr["Email"].ToString());
                            //    strTo=dr["Email"].ToString();
                            //    strEachRecordBody = strEachRecordBody.Replace("[LastName]", dr["LastName"].ToString());
                            //    strEachRecordBody = strEachRecordBody.Replace("[FirstName]", dr["FirstName"].ToString());
                            //}
                            //else
                            //{
                            //    if (dr["ReminderColumnID"] != DBNull.Value)
                            //    {
                            //        Column theReminderColumn = RecordManager.ets_Column_Details(int.Parse(dr["ReminderColumnID"].ToString()));
                            //        if (theReminderColumn != null)
                            //        {
                            //            string strEmailRC = Common.GetValueFromSQL("  SELECT " + theReminderColumn.SystemName + " FROM Record WHERE RecordID=" + drR["RecordID"].ToString());
                            //            //msg.To.Add(strEmailRC);
                            //            strTo=strEmailRC;
                            //        }
                            //    }
                            //}

                            // Red <Begin> Ticket 2013
                            if (dr["Email"] != DBNull.Value && dr["ReminderColumnID"] == DBNull.Value)
                            {
                                //msg.To.Add(dr["Email"].ToString());
                                strTo = dr["Email"].ToString();
                                strEachRecordBody = strEachRecordBody.Replace("[LastName]", dr["LastName"].ToString());
                                strEachRecordBody = strEachRecordBody.Replace("[FirstName]", dr["FirstName"].ToString());
                            }

                            if (dr["ReminderColumnID"] != DBNull.Value && dr["Email"] == DBNull.Value)
                            {
                                Column theReminderColumn = RecordManager.ets_Column_Details(int.Parse(dr["ReminderColumnID"].ToString()));

                                if (theReminderColumn != null)
                                {

                                    if (theReminderColumn.ColumnType == "text")
                                    {
                                        string strEmailRC = Common.GetValueFromSQL("  SELECT " + theReminderColumn.SystemName + " FROM Record WHERE RecordID=" + drR["RecordID"].ToString());
                                        //msg.To.Add(strEmailRC);
                                        strTo = strEmailRC;
                                    }

                                    //JB or MR
                                    if (theReminderColumn.ColumnType == "dropdown")
                                    {

                                        int iUserIDReminder = 0;
                                        if (int.TryParse(Common.GetValueFromSQL("  SELECT " + theReminderColumn.SystemName + " FROM Record WHERE RecordID=" + drR["RecordID"].ToString()), out iUserIDReminder))
                                        {
                                            //
                                        }
                                        string strEmailRC = Common.GetValueFromSQL("  SELECT Email FROM [User] WHERE UserID=" + iUserIDReminder);

                                        if (string.IsNullOrEmpty(strEmailRC))
                                        {
                                            if (iUserIDReminder > 0)
                                            {
                                                //iUserIDReminder is a RecordID
                                                Record theRecordLevel2 = RecordManager.ets_Record_Detail_Full(iUserIDReminder, null, false);
                                                if (theRecordLevel2 != null)
                                                {
                                                    string strUserSys = Common.GetValueFromSQL("SELECT TOP 1 SystemName FROM [Column] WHERE TableTableID=-1 AND TableID=" + theRecordLevel2.TableID.ToString());
                                                    if (!string.IsNullOrEmpty(strUserSys))
                                                    {
                                                        string strUserID = RecordManager.GetRecordValue(ref theRecordLevel2, strUserSys);
                                                        if (!string.IsNullOrEmpty(strUserID))
                                                        {
                                                            strEmailRC = Common.GetValueFromSQL(" SELECT EMAIL FROM [USER] WHERE [USERID] = " + strUserID);
                                                            //This will be a recursive but not now
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        strTo = strEmailRC;
                                    }
                                }
                            }

                            //Red Ticket 432 - This is for User who created the record
                            //<Begin>
                            if (dr["ReminderColumnID"] == DBNull.Value && dr["Email"] == DBNull.Value)
                            {
                                Record theReminderRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drR["RecordID"].ToString()), null, false);
                                if (theReminderRecord != null)
                                {
                                    string strEmailRC = Common.GetValueFromSQL("  SELECT Email FROM [User] WHERE [UserID] = (SELECT ENTEREDBY FROM [Record] WHERE RecordID = " + drR["RecordID"].ToString() + ")");
                                    //msg.To.Add(strEmailRC);                                    
                                    strTo = strEmailRC;
                                }
                            }
                            //<End>

                            //Red Ticket 432 - this is for user who last edited the record
                            //<Begin>
                            if (dr["ReminderColumnID"] != DBNull.Value && dr["Email"] != DBNull.Value)
                            {
                                Record theReminderRecord = RecordManager.ets_Record_Detail_Full(int.Parse(drR["RecordID"].ToString()), null, false);
                                if (theReminderRecord != null)
                                {
                                    string strEmailRC = Common.GetValueFromSQL("  SELECT Email FROM [User] WHERE [UserID] = (SELECT [LastUpdatedUserID] FROM [Record] WHERE RecordID = " + drR["RecordID"].ToString() + ")");
                                    //msg.To.Add(strEmailRC);                                    
                                    strTo = strEmailRC;
                                }
                            }
                            //<End>


                            // Red <End>

                            //msg.Body = strEachRecordBody;

                            try
                            {


                                if (strTo != "")
                                {

                                    string sSendEmailError = "";

                                    Message theMessage = new Message(null, null, int.Parse(dr["TableID"].ToString()), int.Parse(dr["AccountID"].ToString()),
                   DateTime.Now, "W", "E",
                       null, strTo, strSubject, strEachRecordBody, null, "");


                                    DBGurus.SendEmail("Reminder", true, null, strSubject, strEachRecordBody, "",
                                        strTo, "", "", null, theMessage, out sSendEmailError);

                                    strUpdatedRecordIDs = strUpdatedRecordIDs + "," + drR["RecordID"].ToString();
                                    //Common.ExecuteText("UPDATE Record SET ReminderSentDate=GETDATE() WHERE RecordID=" + drR["RecordID"].ToString());

                                    //if (SystemData.SystemOption_ValueByKey_Account("EmailAttachments",null,null) == "Yes")
                                    //    {
                                    //        try
                                    //        {
                                    //            if (theTable.JSONAttachmentInfo != "")
                                    //            {
                                    //                AttachmentSetting theAttachmentSetting = JSONField.GetTypedObject<AttachmentSetting>(theTable.JSONAttachmentInfo);
                                    //                if (theAttachmentSetting.AttachOutgoingEmails != null)
                                    //                {
                                    //                    if ((bool)theAttachmentSetting.AttachOutgoingEmails)
                                    //                    {
                                    //                        if (theAttachmentSetting.OutSavetoTableID != null)
                                    //                        {
                                    //                            Record theChildRecord = new Record();
                                    //                            theChildRecord.TableID = (int)theAttachmentSetting.OutSavetoTableID;
                                    //                            theChildRecord.EnteredBy = int.Parse(dr["UserID"].ToString());

                                    //                            //link the record with the parent table

                                    //                            DataTable dtChildColumn = Common.DataTableFromText("SELECT * FROM [Column] WHERE TableID=" + theChildRecord.TableID.ToString() + " AND TableTableID=" + theTable.TableID.ToString());

                                    //                            if (dtChildColumn.Rows.Count > 0)
                                    //                            {
                                    //                                Column lnkColumn = RecordManager.ets_Column_Details(int.Parse(dtChildColumn.Rows[0]["LinkedParentColumnID"].ToString()));
                                    //                                RecordManager.MakeTheRecord(ref theChildRecord, dtChildColumn.Rows[0]["SystemName"].ToString(), RecordManager.GetRecordValue(ref thisRecord, lnkColumn.SystemName));

                                    //                            }

                                    //                            if (theAttachmentSetting.OutSaveRecipientColumnID != null)
                                    //                            {
                                    //                                Column aColumn = RecordManager.ets_Column_Details((int)theAttachmentSetting.OutSaveRecipientColumnID);

                                    //                                RecordManager.MakeTheRecord(ref theChildRecord, aColumn.SystemName, strTo);
                                    //                            }

                                    //                            if (theAttachmentSetting.OutSaveSubjectColumnID != null)
                                    //                            {
                                    //                                Column aColumn = RecordManager.ets_Column_Details((int)theAttachmentSetting.OutSaveSubjectColumnID);

                                    //                                RecordManager.MakeTheRecord(ref theChildRecord, aColumn.SystemName, strSubject);
                                    //                            }

                                    //                            if (theAttachmentSetting.OutSaveBodyColumnID != null)
                                    //                            {
                                    //                                Column aColumn = RecordManager.ets_Column_Details((int)theAttachmentSetting.OutSaveBodyColumnID);

                                    //                                RecordManager.MakeTheRecord(ref theChildRecord, aColumn.SystemName, strEachRecordBody);
                                    //                            }


                                    //                            try
                                    //                            {
                                    //                                RecordManager.ets_Record_Insert(theChildRecord);
                                    //                            }
                                    //                            catch
                                    //                            {
                                    //                                //

                                    //                            }

                                    //                        }

                                    //                    }

                                    //                }

                                    //            }
                                    //        }
                                    //        catch
                                    //        {
                                    //            //
                                    //        }


                                    //    }


                                }


                            }
                            catch (Exception ex)
                            {
                                ErrorLog theErrorLog = new ErrorLog(null, "Send Reminder Emails Error", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
                                SystemData.ErrorLog_Insert(theErrorLog);
                                //strErrorMsg = "Server could not send warning Email & SMS";
                            }
                        }


                    }

                }





                //msg.Body = strBody;





            }
            //Red <End> foreach "all" datareminder and datareminderuser
            if (strUpdatedRecordIDs != "-1")
            {
                Common.ExecuteText("UPDATE Record SET ReminderSentDate=GETDATE() WHERE RecordID IN ( " + strUpdatedRecordIDs + ")");
            }

        }
        catch (Exception ex)
        {

            if (strUpdatedRecordIDs != "-1")
            {
                Common.ExecuteText("UPDATE Record SET ReminderSentDate=GETDATE() WHERE RecordID IN ( " + strUpdatedRecordIDs + ")");
            }

            ErrorLog theErrorLog = new ErrorLog(null, "Reminder Error", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
            //eventLog1.WriteEntry("Send Alerts Emails Error: " + ex.Message.ToString() + "--Detail-->" + ex.StackTrace);
        }


        if (_bTestTime)
        {
            ErrorLog theErrorLog = new ErrorLog(null, "Not Error-Testing", "DataReminderEmail-" + strUpdatedRecordIDs, "End", DateTime.Now, Request.Path);
            SystemData.ErrorLog_Insert(theErrorLog);
        }


    }
    */
    protected void ExecuteRegularTasks()
    {
        DataTable tasks = GetRegularTaskList();
        foreach (DataRow task in tasks.Rows)
        {
            int taskId = (int)task["RegularTaskID"];

            string className = task["ClassName"].ToString();
            string methodName = task["MethodName"].ToString();

            if (String.IsNullOrEmpty(className) || String.IsNullOrEmpty(methodName))
            {
                ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - ExecuteRegularTasks",
                    "Both Class Name and Method Name should be provided", "", DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }
            else
            {
                LockRegularTask(taskId);
                DateTime startDateTime = DateTime.Now;
                string methodMessage = String.Empty;

                try
                {
                    // Instantiate Import Class
                    Type classType = BuildManager.GetType(className, false);
                    if (classType == null)
                    {
                        ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - ExecuteRegularTasks",
                            String.Format("GetType failed for {0}", className), "", DateTime.Now, Request.Path);
                        SystemData.ErrorLog_Insert(theErrorLog);
                    }
                    else
                    {
                        object classObj = Activator.CreateInstance(classType);
                        if (classObj == null)
                        {
                            ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - ExecuteRegularTasks",
                                String.Format("CreateInstance failed for {0}", className), "", DateTime.Now,
                                Request.Path);
                            SystemData.ErrorLog_Insert(theErrorLog);
                        }
                        else
                        {
                            object[] execParams = new object[2];
                            execParams[0] = task["MethodParameters"].ToString();
                            execParams[1] = "";
                            Type[] execParamsType = new Type[2];
                            execParamsType[0] = execParams[0].GetType();
                            execParamsType[1] = execParams[1].GetType().MakeByRefType(); // out parameter

                            MethodInfo mi = classType.GetMethod(methodName, execParamsType);
                            if (mi == null)
                            {
                                ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - ExecuteRegularTasks",
                                    String.Format("GetMethod failed for {0}.{1}", className, methodName), "",
                                    DateTime.Now,
                                    Request.Path);
                                SystemData.ErrorLog_Insert(theErrorLog);
                            }
                            else
                            {
                                object returnValue = mi.Invoke(classObj, execParams);
                                if (!String.IsNullOrEmpty(execParams[1].ToString()))
                                {
                                    ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - ExecuteRegularTasks",
                                        String.Format("{0}.{1} returned: {2}", className, methodName, execParams[1]), "",
                                        DateTime.Now,
                                        Request.Path);
                                    SystemData.ErrorLog_Insert(theErrorLog);
                                }
                                else
                                {
                                    if (returnValue is string)
                                        methodMessage = returnValue.ToString();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - ExecuteRegularTasks",
                        ex.Message +
                        (ex.InnerException == null
                            ? ""
                            : (Environment.NewLine + " Inner exception: " + ex.InnerException.Message)), ex.StackTrace,
                        DateTime.Now, Request.Path);
                    SystemData.ErrorLog_Insert(theErrorLog);
                }

                ReleaseRegularTask(taskId, startDateTime, methodMessage);
            }
        }
        return;
    }


    protected void ExecuteRegularTasks2()
    {
        DataTable tasks = GetRegularTaskList();
        foreach (DataRow task in tasks.Rows)
        {
            int taskId = (int)task["RegularTaskID"];

           // string className = task["ClassName"].ToString();
            string methodName = task["MethodName"].ToString();

            if (String.IsNullOrEmpty(methodName))
            {
                ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - ExecuteRegularTasks",
                    "Method Name should be provided", "", DateTime.Now, Request.Path);
                SystemData.ErrorLog_Insert(theErrorLog);
            }
            else
            {
                LockRegularTask(taskId);
                DateTime startDateTime = DateTime.Now;
                string methodMessage = String.Empty;

                try
                {
                    // Instantiate Import Class

                    List<object> objList = new List<object>();
                    objList.Add(taskId);
                    objList.Add(task["MethodParameters"]);
                    List<object> roList = new List<object>();
                    roList = MethodRouter.DotNetMethod(methodName, objList);
                    if (roList != null && roList.Count > 0)
                    {
                        foreach (object obj in roList)
                        {
                            if (obj.GetType().Name == "String")
                            {
                                methodMessage = (string)obj;
                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - ExecuteRegularTasks",
                        ex.Message +
                        (ex.InnerException == null
                            ? ""
                            : (Environment.NewLine + " Inner exception: " + ex.InnerException.Message)), ex.StackTrace,
                        DateTime.Now, Request.Path);
                    SystemData.ErrorLog_Insert(theErrorLog);
                }

                ReleaseRegularTask(taskId, startDateTime, methodMessage);
            }
        }
        return;
    }
    private DataTable GetRegularTaskList()
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("dbg_RegularTask_GetQueue", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter()
                {
                    SelectCommand = command
                };
                DataTable dt = new DataTable();
                connection.Open();
                try
                {
                    da.Fill(dt);
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - GetRegularTaskList",
                        ex.Message +
                        (ex.InnerException == null
                            ? ""
                            : (Environment.NewLine + " Inner exception: " + ex.InnerException.Message)), ex.StackTrace,
                        DateTime.Now, Request.Path);
                    SystemData.ErrorLog_Insert(theErrorLog);
                }

                return dt;
            }
        }
    }

    private void LockRegularTask(int taskId)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("dbg_RegularTask_Lock", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("TaskID", taskId);
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - LockRegularTask",
                        ex.Message +
                        (ex.InnerException == null
                            ? ""
                            : (Environment.NewLine + " Inner exception: " + ex.InnerException.Message)), ex.StackTrace,
                        DateTime.Now, Request.Path);
                    SystemData.ErrorLog_Insert(theErrorLog);
                }
            }
        }
    }

    private void ReleaseRegularTask(int taskId, DateTime startDateTime, string message)
    {
        using (SqlConnection connection = new SqlConnection(DBGurus.strGlobalConnectionString))
        {
            using (SqlCommand command = new SqlCommand("dbg_RegularTask_Release", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("TaskID", taskId);
                command.Parameters.AddWithValue("StartDateTime", startDateTime);
                command.Parameters.AddWithValue("Message", message);
                connection.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    ErrorLog theErrorLog = new ErrorLog(null, "WinSchedules - LockRegularTask",
                        ex.Message +
                        (ex.InnerException == null
                            ? ""
                            : (Environment.NewLine + " Inner exception: " + ex.InnerException.Message)), ex.StackTrace,
                        DateTime.Now, Request.Path);
                    SystemData.ErrorLog_Insert(theErrorLog);
                }
            }
        }
    }

    //    protected void SendSydnUniConditionsIssue()
    //    {
    //        try
    //        {
    //            string strRC=Common.GetValueFromSQL("SELECT Count(*) FROM Conditions");
    //            if (strRC == "")
    //                strRC = "0";

    //            if (int.Parse(strRC) < 50)
    //            {
    //                string strEmail = SystemData.SystemOption_ValueByKey("EmailFrom");
    //                string strEmailServer = SystemData.SystemOption_ValueByKey("EmailServer");
    //                string strEmailUserName = SystemData.SystemOption_ValueByKey("EmailUserName");
    //                string strEmailPassword = SystemData.SystemOption_ValueByKey("EmailPassword");
    //                string strWarningSMSEMail = SystemData.SystemOption_ValueByKey("WarningSMSEmail");
    //                string strEnableSSL = SystemData.SystemOption_ValueByKey("EnableSSL");
    //                string strSmtpPort = SystemData.SystemOption_ValueByKey("SmtpPort");

    //                SmtpClient smtpClient = new SmtpClient(strEmailServer);
    //                smtpClient.Timeout = 99999;
    //                smtpClient.Credentials = new System.Net.NetworkCredential(strEmailUserName, strEmailPassword);

    //                smtpClient.EnableSsl = bool.Parse(strEnableSSL);
    //                smtpClient.Port = int.Parse(strSmtpPort);

    //                MailMessage msg = new MailMessage();
    //                msg.From = new MailAddress(strEmail);


    //                msg.Subject = "Conditions DELETED -- " + Request.Url.Authority;

    //                msg.IsBodyHtml = false;

    //                msg.Body = "Conditions records has been deleted, please check " + Request.Url.Authority + " DB " + ", It has only " + strRC + " record(s) ";// Sb.ToString();

    //                msg.To.Clear();
    //                msg.To.Add("info@dbgurus.com.au");
    //                msg.To.Add("r_mohsin@yahoo.com");
    //                msg.To.Add("61403959116" + strWarningSMSEMail);
    //                //msg.To.Add("8801944613448" + strWarningSMSEMail);

    //#if (!DEBUG)
    //                smtpClient.Send(msg);
    //#endif
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            ErrorLog theErrorLog = new ErrorLog(null, "Conditions - temp ", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //            SystemData.ErrorLog_Insert(theErrorLog);     
    //        }

    //    }

    //    protected void SendAlarmEmail()
    //    {



    //        try
    //        {

    //            DataTable dtMSUsers = Common.DataTableFromText(@"SELECT     MonitorSchedule.MonitorScheduleID,[User].FirstName, [User].UserID,
    //                        [User].LastName, [User].Email,[Table].TableID,  [Table].TableName, MonitorSchedule.AccountID,MonitorSchedule.HasAlarm, 
    //                            MonitorSchedule.AlarmDateTime, MonitorSchedule.IsAlarmSent, MonitorSchedule.MonitorScheduleID, MonitorSchedule.Description
    //                            FROM         MonitorSchedule INNER JOIN
    //                            MonitorScheduleUser ON MonitorSchedule.MonitorScheduleID = MonitorScheduleUser.MonitorScheduleID INNER JOIN
    //                            [User] ON 
    //                            MonitorScheduleUser.UserID = [User].UserID  LEFT JOIN
    //                            [Table] ON MonitorSchedule.TableID = [Table].TableID
    //                            WHERE MonitorSchedule.IsAlarmSent=0 AND CONVERT(date,MonitorSchedule.AlarmDateTime)<=CONVERT(date, getdate())");



    //            //string strEmail = SystemData.SystemOption_ValueByKey("EmailFrom");
    //            //string strEmailServer = SystemData.SystemOption_ValueByKey("EmailServer");
    //            //string strEmailUserName = SystemData.SystemOption_ValueByKey("EmailUserName");
    //            //string strEmailPassword = SystemData.SystemOption_ValueByKey("EmailPassword");
    //            string strWarningSMSEMail = SystemData.SystemOption_ValueByKey("WarningSMSEmail");
    //            //string strEnableSSL = SystemData.SystemOption_ValueByKey("EnableSSL");
    //            //string strSmtpPort = SystemData.SystemOption_ValueByKey("SmtpPort");


    //            Content theContentEmail = null;
    //            theContentEmail = SystemData.Content_Details_ByKey("ScheduleAlarm", null);
    //            if (theContentEmail != null)
    //            {
    //                foreach (DataRow dr in dtMSUsers.Rows)
    //                {
    //                    //lets send emails

    //                    string strBody = theContentEmail.ContentP;


    //                    strBody = strBody.Replace("[TableName]", dr["TableName"].ToString() == "" ? "n/a" : dr["TableName"].ToString());
    //                    strBody = strBody.Replace("[AlarmDateTime]", dr["AlarmDateTime"].ToString());
    //                    strBody = strBody.Replace("[Description]", dr["Description"].ToString());

    //                    //MailMessage msg = new MailMessage();
    //                    //msg.From = new MailAddress(strEmail);

    //                    string strSubject = theContentEmail.Heading;

    //                    //msg.IsBodyHtml = true;

    //                    ////msg.Body = strBody;
    //                    //SmtpClient smtpClient = new SmtpClient(strEmailServer);
    //                    //smtpClient.Timeout = 99999;
    //                    //smtpClient.Credentials = new System.Net.NetworkCredential(strEmailUserName, strEmailPassword);

    //                    //smtpClient.EnableSsl = bool.Parse(strEnableSSL);
    //                    //smtpClient.Port = int.Parse(strSmtpPort);


    //                    //msg.To.Clear();
    //                    string strTo=dr["Email"].ToString();


    //                    strBody = strBody.Replace("[LastName]", dr["LastName"].ToString());
    //                    strBody = strBody.Replace("[FirstName]", dr["FirstName"].ToString());
    //                    //msg.Body = strBody;
    //                    try
    //                    {

    //#if (!DEBUG)
    //                        //smtpClient.Send(msg);
    //#endif


    //                        //if (System.Web.HttpContext.Current.Session["AccountID"] != null)
    //                        //{

    //                        //    SecurityManager.Account_SMS_Email_Count(int.Parse(System.Web.HttpContext.Current.Session["AccountID"].ToString()), true, null, null, null);
    //                        //}




    //                        //if (msg.To.Count > 0)
    //                        //{
    //                            Guid guidNew = Guid.NewGuid();
    //                            string strEmailUID = guidNew.ToString();

    //                            EmailLog theEmailLog = new EmailLog(null, int.Parse(dr["AccountID"].ToString()), strSubject,
    //                              strTo, DateTime.Now, dr["TableID"] == DBNull.Value ? null : (int?)int.Parse(dr["TableID"].ToString()),
    //                              null,
    //                              "Alert", strBody);
    //                            theEmailLog.EmailUID = strEmailUID;
    //                            //EmailAndIncoming.dbg_EmailLog_Insert(theEmailLog, null, null);

    //                            string sSendEmailError = "";
    //                            DBGurus.SendEmail("Alert", true, null, strSubject, strBody, "",
    //                                strTo, "", "", null, theEmailLog, out sSendEmailError);


    //                            Common.ExecuteText("UPDATE MonitorSchedule SET IsAlarmSent=1 WHERE MonitorScheduleID=" + dr["MonitorScheduleID"].ToString(), null);
    //                            try
    //                            {

    //                                if (SystemData.SystemOption_ValueByKey("EmailAttachments") == "Yes"
    //                                    && dr["TableID"]!=DBNull.Value)
    //                                {
    //                                    Table theTable = RecordManager.ets_Table_Details(int.Parse(dr["TableID"].ToString()));
    //                                    if (theTable.JSONAttachmentInfo != "")
    //                                    {
    //                                        AttachmentSetting theAttachmentSetting = JSONField.GetTypedObject<AttachmentSetting>(theTable.JSONAttachmentInfo);
    //                                        if (theAttachmentSetting.AttachOutgoingEmails != null)
    //                                        {
    //                                            if ((bool)theAttachmentSetting.AttachOutgoingEmails)
    //                                            {
    //                                                if (theAttachmentSetting.OutSavetoTableID != null)
    //                                                {
    //                                                    Record theChildRecord = new Record();
    //                                                    theChildRecord.TableID = (int)theAttachmentSetting.OutSavetoTableID;
    //                                                    theChildRecord.EnteredBy = int.Parse(dr["UserID"].ToString());

    //                                                    //link the record with the parent table

    //                                                    //DataTable dtChildColumn = Common.DataTableFromText("SELECT * FROM [Column] WHERE TableID=" + theChildRecord.TableID.ToString() + " AND TableTableID=" + theTable.TableID.ToString());

    //                                                    //if (dtChildColumn.Rows.Count > 0)
    //                                                    //{
    //                                                    //    Column lnkColumn = RecordManager.ets_Column_Details(int.Parse(dtChildColumn.Rows[0]["LinkedParentColumnID"].ToString()));
    //                                                    //    RecordManager.MakeTheRecord(ref theChildRecord, dtChildColumn.Rows[0]["SystemName"].ToString(), RecordManager.GetRecordValue(ref this, lnkColumn.SystemName));

    //                                                    //}

    //                                                    if (theAttachmentSetting.OutSaveRecipientColumnID != null)
    //                                                    {
    //                                                        Column aColumn = RecordManager.ets_Column_Details((int)theAttachmentSetting.OutSaveRecipientColumnID);

    //                                                        RecordManager.MakeTheRecord(ref theChildRecord, aColumn.SystemName, strTo);
    //                                                    }

    //                                                    if (theAttachmentSetting.OutSaveSubjectColumnID != null)
    //                                                    {
    //                                                        Column aColumn = RecordManager.ets_Column_Details((int)theAttachmentSetting.OutSaveSubjectColumnID);

    //                                                        RecordManager.MakeTheRecord(ref theChildRecord, aColumn.SystemName, strSubject);
    //                                                    }

    //                                                    if (theAttachmentSetting.OutSaveBodyColumnID != null)
    //                                                    {
    //                                                        Column aColumn = RecordManager.ets_Column_Details((int)theAttachmentSetting.OutSaveBodyColumnID);

    //                                                        RecordManager.MakeTheRecord(ref theChildRecord, aColumn.SystemName, strBody);
    //                                                    }
    //                                                    try
    //                                                    {
    //                                                        RecordManager.ets_Record_Insert(theChildRecord);
    //                                                    }
    //                                                    catch
    //                                                    {
    //                                                        //

    //                                                    }

    //                                                }

    //                                            }

    //                                        }

    //                                    }


    //                                }

    //                            }
    //                            catch
    //                            {
    //                                //
    //                            }



    //                        //}


    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        ErrorLog theErrorLog = new ErrorLog(null, "Send Alerts Emails Error", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //                        SystemData.ErrorLog_Insert(theErrorLog);
    //                        //strErrorMsg = "Server could not send warning Email & SMS";
    //                    }

    //                }

    //            }

    //        }
    //        catch (Exception ex)
    //        {
    //            ErrorLog theErrorLog = new ErrorLog(null, "Send Alerts Emails Error", ex.Message, ex.StackTrace, DateTime.Now, Request.Path);
    //            SystemData.ErrorLog_Insert(theErrorLog);
    //            //eventLog1.WriteEntry("Send Alerts Emails Error: " + ex.Message.ToString() + "--Detail-->" + ex.StackTrace);
    //        }





    //    }
}





//public partial class Weather : System.Web.UI.Page
//{
//    private string weatherProviderUrl = "http://ourweatherprovider.com/api/get";

//    protected void Page_Load(object sender, EventArgs e)
//    {
//        RegisterAsyncTask(new PageAsyncTask(GetWeatherData));
//    }

//    public async Task GetWeatherData()
//    {
//        using (HttpClient httpClient = new HttpClient())
//        {
//            var response = await httpClient.GetAsync(this.weatherProviderUrl);
//            string jsonResp = await response.Content.ReadAsStringAsync();
//            ForecastVM forecastModel = await JsonConvert.DeserializeObjectAsync<ForecastVM>(jsonResp);
//            this.ourControl.DataSource = forecastModel;
//            this.ourControl.DataBind();
//        }
//    }
//}
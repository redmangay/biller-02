﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DBEmail_GetALSLive : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dtWebServices = Common.DataTableFromText("SELECT * FROM [ALS_Live_Web_Services]");
            if (dtWebServices != null)
            {
                foreach (DataRow service in dtWebServices.Rows)
                {
                    dataList.Items.Add(new ListItem(service["Site"].ToString() + " - " + service["Sample Type"].ToString(), service["Record ID"].ToString()));
                }
            }
        }
    }

    protected void run_Click(object sender, EventArgs e)
    {
        DataTable dtWebServices = Common.DataTableFromText(String.Format("SELECT * FROM [ALS_Live_Web_Services] WHERE [Record ID]={0}", dataList.SelectedValue));
        if (dtWebServices != null && dtWebServices.Rows.Count > 0)
        {
            DataRow serviceRow = dtWebServices.Rows[0];
            int userID = int.Parse(SystemData.SystemOption_ValueByKey_Account("AutoUploadUserID", null, null));
            int siteTableID = -1;
            int.TryParse(serviceRow["Sample Site Table ID"].ToString(), out siteTableID);
            //Rolleston - Site TableID - Information (Surface Water)
            int tempTableID = -1;
            int.TryParse(serviceRow["Temp Table ID"].ToString(), out tempTableID); //OEMD -ALS Live Data -

            EMDDataImport.ALSLiveImport liveImport = new EMDDataImport.ALSLiveImport();
            DateTime startDate;
            DateTime endDate;
            if (DateTime.TryParse(dateFrom.Text, out startDate) && DateTime.TryParse(dateTo.Text, out endDate))
            {
                startDate = new DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0);
                while (startDate <= endDate)
                {
                    liveImport.GetData(serviceRow["URI"].ToString(),
                        raw.Checked ? EMDDataImport.ALSLiveImport.ALSLiveDataType.Raw : EMDDataImport.ALSLiveImport.ALSLiveDataType.Checked,
                        serviceRow["Site"].ToString(), serviceRow["Sample Type"].ToString(), startDate,
                        siteTableID, tempTableID, userID);
                    startDate = startDate.AddDays(1);
                }
            }
        }
    }
}
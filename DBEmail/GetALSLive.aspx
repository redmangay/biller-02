﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GetALSLive.aspx.cs" Inherits="DBEmail_GetALSLive" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Get ALS Live</title>

    <style type="text/css">
        .cl1 {
            margin-bottom: 10px;
        }
            .cl1 label {
                width: 7em;
                float: left;
                text-align: right;
                margin-right: 5px;
            }
            .cl1 input {
                width: 10em;
            }
            .cl1 select {
                width: 20em;
            }
        .cl2 {
            margin-left: 7em;
            padding-left: 5px;
            margin-bottom: 10px;
        }
        label {
            font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="cl1"><label for="dataList" runat="server">Data</label><asp:DropDownList ID="dataList" runat="server" /></div>
        <div class="cl2">
            <asp:RadioButton GroupName="dataType" ID="raw" Checked="true" runat="server" />
            <label for="raw">Raw</label>
            <asp:RadioButton GroupName="dataType" ID="qc" Checked="false" runat="server" />
            <label for="qc">QC</label>
        </div>
        <div class="cl1"><label for="dateFrom" runat="server">Date From</label><asp:TextBox ID="dateFrom" runat="server"></asp:TextBox></div>
        <div class="cl1"><label for="dateTo" runat="server">Date To</label><asp:TextBox ID="dateTo" runat="server"></asp:TextBox></div>
        <div class="cl2">
            <asp:Button ID="run" runat="server" Text="Run" OnClick="run_Click" />
        </div>
    </div>
    </form>
</body>
</html>

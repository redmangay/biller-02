﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignUp.aspx.cs" Inherits="Login"
    EnableEventValidation="false" ValidateRequest="true" MasterPageFile="~/Home/Marketing2.master"
    EnableTheming="true" %>

<%@ Register Assembly="DBGWebControl" Namespace="DBGWebControl" TagPrefix="DBGurus" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">

    <style>
        #showPassword_icon {
            /*background:url("Images/eye-open-edited2.png") center center no-repeat #fff;*/
            border: none;
            box-shadow: none;
            width: 26px;
            height: 20px;
            display: block;
            float: left;
            margin: 0px 3px 0px 0px;
            border: solid 1px #909090;
            border-left: none;
        }

        #ctl00_HomeContentPlaceHolder_TDBSignupPassword {
            float: left;
            /*margin-bottom:30px;*/
            border-right: none;
        }
    </style>
    <%--ADDED RP - Ticket 4187 --%>
    <style type="text/css">
        .modal {
            position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.2;
            filter: alpha(opacity=80);
            -moz-opacity: 0.2;
            min-height: 100%;
            width: 100%;
        }

        .loading {
            font-family: Arial;
            font-size: 10pt;
            border: 5px solid #67CFF5;
            width: 200px;
            height: 100px;
            display: none;
            position: fixed;
            background-color: White;
            z-index: 999;
        }
    </style>
    <%--END RP - Ticket 4187 --%>

    <script language="javascript" type="text/javascript">

        //jeRome Ticket 2808
        //Added this code to turn off the autocomplete attribute of the form and the fields.

        //This is for the Show Password feature
        $(document).ready(function () {

            var ua = window.navigator.userAgent;
            var trident = ua.indexOf('Trident/');
            var edge = ua.indexOf('Edge/');
            var msie = ua.indexOf('MSIE ');

            if ((msie > 0) || (trident > 0) || (edge > 0)) {
                // IE 10 or older => return version number
                //alert(parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10));
                document.getElementById('ctl00_HomeContentPlaceHolder_TDBSignupPassword').style.width = '274px';
                document.getElementById('ctl00_HomeContentPlaceHolder_TDBSignupPassword').style.borderRight = 'solid 1px #909090';
                document.getElementById('showPassword_icon').style.display = 'none';
                document.getElementById('ctl00_HomeContentPlaceHolder_TDBSignupPassword').style.marginRight = '3px';
                //$('#ctl00$HomeContentPlaceHolder$TDBSignupPassword').css('width', '100%');
                //$('#showPassword_icon').css('display', 'none');

            }

                // other browser
            else {

                $('#showPassword_icon').css('background', 'url("Images/eye-open-edited2.png")');
                $('#showPassword_icon').css('background-position-x', 'center');
                $('#showPassword_icon').css('background-position-y', 'center');
                $('#showPassword_icon').css('background-repeat', 'no-repeat');
                $('#showPassword_icon').css('background-color', 'white');

                $('#showPassword_icon').on('click', function () {

                    if ($(this).attr('data-click-state') == 1) {
                        $(this).attr('data-click-state', 0);
                        document.getElementById('ctl00_HomeContentPlaceHolder_TDBSignupPassword').type = 'text';
                        $(this).css('background', 'url("Images/eye-close-edited2.png")');
                        $(this).css('background-position-x', 'center');
                        $(this).css('background-position-y', 'center');
                        $(this).css('background-repeat', 'no-repeat');
                        $(this).css('background-color', 'white');


                    } else {
                        $(this).attr('data-click-state', 1);
                        document.getElementById('ctl00_HomeContentPlaceHolder_TDBSignupPassword').type = 'password';
                        $(this).css('background', 'url("Images/eye-open-edited2.png")');
                        $(this).css('background-position-x', 'center');
                        $(this).css('background-position-y', 'center');
                        $(this).css('background-repeat', 'no-repeat');
                        $(this).css('background-color', 'white');
                    }

                });
            }
        });

        //*****end JA Ticket 2808
    </script>

    <%--ADDED RP - Ticket 4187 --%>
    <script type="text/javascript">
        function ShowProgress() {
            //RP MODIFIED Ticket 4274
            //OLD
            //if (Page_IsValid) {
            //NEW
            if (Page_ClientValidate("reg2")) {
            //END MODIFICATION
                setTimeout(function () {
                    var modal = $('<div />');
                    modal.addClass("modal");
                    $('body').append(modal);
                    var loading = $(".loading");
                    loading.show();
                    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                    loading.css({ top: top, left: left });
                }, 200);
            }
        }
    </script>
    <%--END RP - Ticket 4187 --%>

    <div class="ContentMain" style="text-align: left;">

        <table border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td width="29" rowspan="22"></td>
                <td width="943" valign="top">
                    <div id="menu">
                    </div>
                    <div id="content1">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <table width="700px">
                                        <tr>
                                            <td>
                                                <h1>1 Step <span style="color: #14A3BA;">Sign Up </span>
                                                </h1>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="margin-top: 5px;">
                                <td width="700" style="padding-left: 0px" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td width="10" style="background-color: Transparent;">
                                                <img src="App_Themes/Default/Images/emd_03.png" />
                                            </td>
                                            <td background="App_Themes/Default/Images/emd_04.png"></td>
                                            <td width="14">
                                                <img src="App_Themes/Default/Images/emd_05.png" />
                                            </td>
                                        </tr>
                                        <tr style="background-color: White;">
                                            <td background="App_Themes/Default/Images/emd_09.png"></td>
                                            <td valign="top">
                                                <div>
                                                    <asp:Panel ID="pnlReg" runat="server">
                                                        <div id="divReg">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td width="700" style="padding-left: 10px" valign="top">
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td style="padding-right: 6px" valign="top">
                                                                                    <div runat="server" id="divStep1" visible="true">
                                                                                        <table border="0" align="left" cellpadding="0" cellspacing="10" width="100%">
                                                                                            <tr>
                                                                                                <td height="10" colspan="2">
                                                                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" EnableClientScript="true"
                                                                                                        ValidationGroup="reg2" ShowMessageBox="false" ShowSummary="false" HeaderText="Please correct following errors:" />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="right" valign="middle">
                                                                                                    <strong>Your Email Address:</strong>
                                                                                                </td>
                                                                                                <td valign="middle">
                                                                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                                                        <ContentTemplate>
                                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="TDBSignupEmail" CssClass="NormalTextBox" runat="server" Width="275px" Text=""
                                                                                                                            OnTextChanged="Email_TextChanged" AutoPostBack="true" autocomplete="off"></asp:TextBox>

                                                                                                                    </td>

                                                                                                                    <td style="padding-left: 3px;">
                                                                                                                        <asp:Label runat="server" ForeColor="Red" ID="lblEmailErrorMsg" Text=""></asp:Label>
                                                                                                                        <asp:RequiredFieldValidator ID="RFVEmail" runat="server" ControlToValidate="TDBSignupEmail"
                                                                                                                            ForeColor="Red" Display="Dynamic" ValidationGroup="reg2" ErrorMessage="Email Address is required"></asp:RequiredFieldValidator>
                                                                                                                        <asp:RegularExpressionValidator ForeColor="Red" Display="Dynamic" ID="REVEmail" runat="server"
                                                                                                                            ControlToValidate="TDBSignupEmail" ValidationGroup="reg2" ErrorMessage="Invalid Email"
                                                                                                                            ValidationExpression="^([a-zA-Z0-9_\-\.])+@(([0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5]\.[0-2]?[0-5]?[0-5])|((([a-zA-Z0-9\-])+\.)+([a-zA-Z\-])+))$">
                                                                                                                        </asp:RegularExpressionValidator>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ContentTemplate>
                                                                                                        <Triggers>
                                                                                                            <asp:AsyncPostBackTrigger ControlID="TDBSignupEmail" />
                                                                                                        </Triggers>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="right">
                                                                                                    <strong>Choose a Password:</strong>
                                                                                                </td>
                                                                                                <td id="tdPassword">

                                                                                                    <asp:TextBox ID="TDBSignupPassword" CssClass="NormalTextBox" runat="server" TextMode="Password"
                                                                                                        Width="250px" Text="" name="TDBSignupPassword"></asp:TextBox>

                                                                                                    <input id="showPassword_icon" type="button" data-click-state="1"></input><%--jeRome Ticket 2808--%>

                                                                                                    <cc1:PasswordStrength ID="PS" runat="server" TargetControlID="TDBSignupPassword" DisplayPosition="BelowLeft"
                                                                                                        StrengthIndicatorType="Text" PreferredPasswordLength="12" PrefixText="" TextCssClass="TextIndicator"
                                                                                                        MinimumNumericCharacters="1" MinimumSymbolCharacters="1" RequiresUpperAndLowerCaseCharacters="false"
                                                                                                        MinimumLowerCaseCharacters="1" MinimumUpperCaseCharacters="1" TextStrengthDescriptions="Too short;Weak;Good;Strong;Strong"
                                                                                                        TextStrengthDescriptionStyles="TextIndicator_Strength1;TextIndicator_Strength2;TextIndicator_Strength3;TextIndicator_Strength4;TextIndicator_Strength5"
                                                                                                        CalculationWeightings="50;15;15;20" />
                                                                                                    <asp:RegularExpressionValidator ID="revPasswordLength" runat="server" ControlToValidate="TDBSignupPassword"
                                                                                                        ValidationGroup="reg2" ErrorMessage="Must be at least 6 characters" ValidationExpression=".{6,30}"
                                                                                                        ForeColor="Red" Display="Dynamic">
                                                                                                    </asp:RegularExpressionValidator>
                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TDBSignupPassword"
                                                                                                        ValidationGroup="reg2" ErrorMessage="Password is required" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                                </td>

                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td align="right" valign="middle">
                                                                                                    <strong>Account Name:</strong>
                                                                                                </td>
                                                                                                <td valign="middle">
                                                                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                                                                        <ContentTemplate>
                                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="txtAccount" CssClass="NormalTextBox" runat="server" Width="275px" Text=""
                                                                                                                            OnTextChanged="Account_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td style="padding-left: 3px;">
                                                                                                                        <asp:Label runat="server" ForeColor="Red" ID="lblAccountErrorMsg" Text=""></asp:Label>
                                                                                                                        <asp:RequiredFieldValidator ID="rfvtxtAccount" runat="server" ControlToValidate="txtAccount"
                                                                                                                            ForeColor="Red" Display="Dynamic" ValidationGroup="reg2" ErrorMessage="Account Name is required"></asp:RequiredFieldValidator>

                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ContentTemplate>
                                                                                                        <Triggers>
                                                                                                            <asp:AsyncPostBackTrigger ControlID="txtAccount" />

                                                                                                        </Triggers>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <%--JA Ticket 3868 - Sign Up Page Update---Additional Fields--%>
                                                                                            <tr>
                                                                                                <td align="right" valign="middle">
                                                                                                    <strong>First Name:</strong>
                                                                                                </td>
                                                                                                <td valign="middle">
                                                                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                                                        <ContentTemplate>
                                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="txtFname" CssClass="NormalTextBox" runat="server" Width="275px" Text=""
                                                                                                                            OnTextChanged="Fname_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td style="padding-left: 3px;">
                                                                                                                        <asp:Label runat="server" ForeColor="Red" ID="lblFnameErrorMsg" Text=""></asp:Label>
                                                                                                                        <asp:RequiredFieldValidator ID="rfvtxtFname" runat="server" ControlToValidate="txtFname"
                                                                                                                            ForeColor="Red" Display="Dynamic" ValidationGroup="reg2" ErrorMessage="First Name is required"></asp:RequiredFieldValidator>

                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ContentTemplate>
                                                                                                        <Triggers>
                                                                                                            <asp:AsyncPostBackTrigger ControlID="txtAccount" />

                                                                                                        </Triggers>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td align="right" valign="middle">
                                                                                                    <strong>Last Name:</strong>
                                                                                                </td>
                                                                                                <td valign="middle">
                                                                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                                                                        <ContentTemplate>
                                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:TextBox ID="txtLname" CssClass="NormalTextBox" runat="server" Width="275px" Text=""
                                                                                                                            OnTextChanged="Lname_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td style="padding-left: 3px;">
                                                                                                                        <asp:Label runat="server" ForeColor="Red" ID="lblLnameErrorMsg" Text=""></asp:Label>
                                                                                                                        <asp:RequiredFieldValidator ID="rvftxtLname" runat="server" ControlToValidate="txtFname"
                                                                                                                            ForeColor="Red" Display="Dynamic" ValidationGroup="reg2" ErrorMessage="Last Name is required"></asp:RequiredFieldValidator>

                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ContentTemplate>
                                                                                                        <Triggers>
                                                                                                            <asp:AsyncPostBackTrigger ControlID="txtAccount" />

                                                                                                        </Triggers>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td align="right" valign="middle">
                                                                                                    <strong>Country</strong>
                                                                                                </td>
                                                                                                <td valign="middle">
                                                                                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                                                                        <ContentTemplate>
                                                                                                            <table cellpadding="0" cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <asp:DropDownList runat="server" ID="ddlCountry" CssClass="NormalTextBox"
                                                                                                                            AutoPostBack="false" DataTextField="DisplayText" DataValueField="LookupDataID" Width="279px">
                                                                                                                        </asp:DropDownList>

                                                                                                                    </td>
                                                                                                                    <td style="padding-left: 3px;">
                                                                                                                        <%-- <asp:Label runat="server" ForeColor="Red" ID="Label1" Text=""></asp:Label>
                                                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFname"
                                                                                                                            ForeColor="Red" Display="Dynamic" ValidationGroup="reg2" ErrorMessage="Last Name is required"></asp:RequiredFieldValidator>--%>
                                                                                                                       
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </ContentTemplate>
                                                                                                        <Triggers>
                                                                                                            <asp:AsyncPostBackTrigger ControlID="txtAccount" />

                                                                                                        </Triggers>
                                                                                                    </asp:UpdatePanel>
                                                                                                </td>
                                                                                            </tr>


                                                                                            <tr>
                                                                                                <td align="right">&nbsp;
                                                                                                </td>
                                                                                                <td align="left">
                                                                                                    <table>
                                                                                                        <tr>
                                                                                                            <td style="padding-left: 123px;">
                                                                                                                <div>
                                                                                                                    <asp:HyperLink runat="server" NavigateUrl="~/Login.aspx"> <strong>Sign In</strong> </asp:HyperLink>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                            <td style="padding-left: 21px;">
                                                                                                                <div>
                                                                                                                    <%-- ADDED RP - Ticket 4187 --%>
                                                                                                                    <%-- OLD
                                                                                                                    <asp:LinkButton runat="server" ID="LinkButton1" ValidationGroup="reg2" CssClass="btn"
                                                                                                                        ClientIDMode="Static" CausesValidation="true" OnClick="lnkNext1_Click"> <strong>Sign Up</strong> </asp:LinkButton>
                                                                                                                     NEW--%>
                                                                                                                    <asp:LinkButton runat="server" ID="lnkNext1" ValidationGroup="reg2" CssClass="btn"
                                                                                                                        ClientIDMode="Static" CausesValidation="true" OnClick="lnkNext1_Click" OnClientClick="ShowProgress();return true;"> <strong>Sign Up</strong> </asp:LinkButton>
                                                                                                                    <%-- END RP - Ticket 4187 --%>
                                                                                                                </div>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td colspan="2" style="padding-left: 40px;">By clicking Sign Up you agree to our <a href="ETS-Terms-Of-Service.pdf" target="_blank">Terms of Service</a> and <a href="ETS-Privacy-Policy.pdf" target="_blank">Privacy Policy</a>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td colspan="2" height="5" align="center">
                                                                                                    <asp:Label ID="LblMessage" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </td>
                                            <td background="App_Themes/Default/Images/emd_11.png"></td>
                                        </tr>
                                        <tr>
                                            <td background="App_Themes/Default/Images/emd_09.png"></td>
                                            <td>
                                                <%--<img src="App_Themes/Default/Images/spacer.gif" height="5" />--%>
                                            </td>
                                            <td background="App_Themes/Default/Images/emd_11.png"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img src="App_Themes/Default/Images/emd_12.png" />
                                            </td>
                                            <td background="App_Themes/Default/Images/emd_13.png"></td>
                                            <td>
                                                <img src="App_Themes/Default/Images/emd_14.png" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td width="28"></td>
            </tr>
        </table>
    </div>
    <%-- RICKY PABLO - TICKET 4187 --%>
    <%-- ADDED --%>
    <div class="loading" align="center">
        Setting up your new account<br />
        <br />
        <img src="../Images/ajax.gif" alt="" />
    </div>
</asp:Content>

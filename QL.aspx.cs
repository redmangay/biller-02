﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;

public partial class QL : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ID"] != null || Request.QueryString["RID"] != null)
        {
            int iQuickLinkID;
            string strURL;
            //RP Added Ticket 4384
            int RecordIDwithCheckDigit;
            if (int.TryParse(Request.QueryString["RID"], out RecordIDwithCheckDigit))
            {
                int RecordID = Int32.Parse(RecordIDwithCheckDigit.ToString().Substring(0, RecordIDwithCheckDigit.ToString().Length - 1));
                strURL = RecordManager.dbg_QuickLink_DetailWithRecordID(RecordID, RecordIDwithCheckDigit);
                //FIX TO GET THE WHOLE QUERYSTRING
                String QueryString = "";
                int iqs = strURL.IndexOf('?');
                if (iqs >= 0)
                {
                    QueryString = (iqs < strURL.Length - 1) ? strURL.Substring(iqs + 1) : String.Empty;
                }
                //GET ALL PARAMETERS
                var URLParam = System.Web.HttpUtility.ParseQueryString(QueryString);
                if (!String.IsNullOrWhiteSpace(URLParam.Get("LoginToken")))
                {
                    String LoginToken = URLParam.Get("LoginToken");
                    Record rec = RecordManager.ets_Record_Detail_Full(RecordID, null, true);
                    if (rec != null)
                    {
                        String EditLink = "~/Pages/Record/RecordDetail.aspx?mode=" + Cryptography.Encrypt("edit") + "&TableID=" + Cryptography.Encrypt(rec.TableID.ToString()) + "&Recordid=" + Cryptography.Encrypt(rec.RecordID.ToString()) + "&autologin=1&SearchCriteriaID=" + Cryptography.Encrypt("-1");
                        String url = strURL.Replace(QueryString, "");

                        strURL = url + "LoginToken=" + LoginToken + "&ReturnURL=" + HttpUtility.UrlEncode(EditLink);
                        Response.Redirect(strURL);
                    }
                }
            }
            //End Modification
            else if (int.TryParse(Request.QueryString["ID"], out iQuickLinkID))
            {
                strURL = RecordManager.dbg_QuickLink_Detail(iQuickLinkID);
                Response.Redirect(strURL);
            }
        }
    }
}
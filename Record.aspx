﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true" CodeFile="Record.aspx.cs"
     Inherits="RecordPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" runat="Server">
    <div style="padding: 20px;">
        <table>
            <%--<tr>
                <td>Pass Code<asp:TextBox runat="server" ID="txtPass"></asp:TextBox>
                    <br />
                </td>
            </tr>--%>
            <tr>
                <td>RecordID<asp:TextBox runat="server" ID="txtRecordID"></asp:TextBox>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Button runat="server" ID="btnGo" Text="Show Record" OnClick="btnGo_Click" />
                    <br />
                </td>
            </tr>
            <tr>
                <asp:GridView ID="gvRecord" runat="server" ShowHeaderWhenEmpty="true"
                    AutoGenerateColumns="true" CssClass="gridview" AllowPaging="false"
                    HeaderStyle-CssClass="gridview_header" RowStyle-CssClass="gridview_row">
                </asp:GridView>
            </tr>
        </table>
    </div>

</asp:Content>


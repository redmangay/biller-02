﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class Pages_Generic_ATs : SecurePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if(!IsPostBack)
        {
            lnkSearch_Click(null, null);
           

        }

    }
    protected void lnkSearch_Click(object sender, EventArgs e)
    {
        string strTable ="";

        if( txtTableName.Text!="")
            strTable = " WHERE ParentTable LIKE ''%" + txtTableName.Text.Replace("'", "''") + "%''  ";
        
        gvP.DataSource = Common.DataTableFromText(@"SELECT * FROM (SELECT (SELECT TableName FROM [Table] WHERE isactive=1 and TableID=C.TableTableID) ParentTable,C.TableTableID P_TableID,T.TableName ChildTable,
                    C.TableID C_TableID,
                    C.DisplayName ChildJoinColumn, C.ColumnID,   C.DisplayColumn as [FieldsToShow] FROM [Column] C JOIN [Table] T ON C.TableID=T.TableID
                    WHERE T.AccountID=" + Session["AccountID"].ToString() + "  AND C.TableTableID IS NOT NULL ) FullTable " + strTable + "  ORDER BY ParentTable");


        gvP.DataBind();

       if( strTable!="")
           strTable = " and T.TableName like '%" + txtTableName.Text.Replace("'", "''") + " %'  ";

        gvC.DataSource=Common.DataTableFromText(@"SELECT (SELECT TableName FROM [Table] WHERE TableID=C.TableTableID) ParentTable,C.TableTableID P_TableID,T.TableName ChildTable,
            C.TableID C_TableID,C.DisplayName ChildJoinColumn,    C.DisplayColumn as [FieldsToShow] FROM [Column] C JOIN [Table] T ON C.TableID=T.TableID 
            WHERE T.AccountID="+Session["AccountID"].ToString()+"  AND C.TableTableID IS NOT NULL and t.isactive=1 "+strTable+@"  ORDER BY ChildTable");
        gvC.DataBind();
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home/rResponsive.master" AutoEventWireup="true" 
    CodeFile="ErrorRedirect.aspx.cs" Inherits="ErrorRedirect" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HomeContentPlaceHolder" Runat="Server">

    <div style="padding:50px; min-width:450px;">
        
         
        <h1>System has encountered an error, this is embarrassing. Please try again.</h1>
        <table>
            <tr>
                <td align="right" style="width:150px;">
                    <strong>Path:</strong>
                </td>
                <td align="left">
                    <asp:Label runat="server" ID="lblErrorPath"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">
                    <strong>Error Message:</strong>
                </td>
                <td align="left">
                    <asp:Label runat="server" ID="lblErrorMessage"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">
                    <strong>Error Stack:</strong>
                </td>
                <td align="left">
                    <asp:Label runat="server" ID="lblErrorStack"></asp:Label>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>

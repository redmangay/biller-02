﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="_Default"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="apple-touch-icon" sizes="57x57"  href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="60x60" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="72x72" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="76x76" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="114x114" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="120x120" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="144x144" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="152x152" href="../Images/favicon.ico" />
    <link rel="apple-touch-icon" sizes="180x180" href="../Images/favicon.ico" />
    <link rel="icon" type="image/png" sizes="192x192" href="../Images/favicon.ico" />
    <link rel="icon" type="image/png" sizes="32x32" href="../Images/favicon.ico" />
    <link rel="icon" type="image/png" sizes="96x96" href="../Images/favicon.ico" />
    <link rel="icon" type="image/png" sizes="16x16" href="../Images/favicon.ico" />
    <link rel="manifest" href="Marketing/images/icon/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="Marketing/images/icon/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />

    <title>The Database</title>
    <link href="Marketing/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Marketing/css/index.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-nav">
     <script type="text/javascript" src="<%=ResolveUrl("~/script/jquery-3.3.1.min.js")%>"></script>
    <script>

        $('.navbar-collapse ul li a').click(function () {
            $(".navbar-collapse").collapse('hide');
        });
    </script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="Marketing/js/bootstrap.min.js"></script>
    <!-- Scrolling Nav JavaScript -->
    <script src="Marketing/js/jquery.easing.min.js"></script>
    <script src="Marketing/js/scrolling-nav.js"></script>
    <form role="form" runat="server">
        <div class="container" data-spy="affix" data-offset-top="120">
            <div class="row pad-tb-20">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 hidden-xs">
                    <img src="images/logo.png" class="img-responsive hidden-xs" id="home" alt="Logo" />
                </div>
                <div class="col-xs-12 col-sm-6 col-md-5 col-md-offset-1 col-lg-5 col-lg-offset-1">
                    <nav class="navbar navbar-right">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="menu">Menu</span>
                            </button>
                            <a href="#" class="navbar-brand hidden visible-xs">
                                <img src="images/logo.png" class="img-responsive" alt="Logo" /></a>
                        </div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                            <ul class="nav navbar-nav">
                                <li class="active"><a class="page-scroll" href="#page-top">Home</a></li>
                                <li><a class="page-scroll" href="#client">Clients</a></li>
                                <li><a class="page-scroll" href="#features">Features</a></li>
                                <li><a class="page-scroll" href="#pricing">Pricing</a></li>
                                <li><a href="Login.aspx">Sign In</a></li>
                            </ul>

                        </div>
                    </nav>
                </div>
            </div>
        </div>



         <div class="container-fluid" style="background: url(Marketing/images/banner-bg.jpg) no-repeat; background-size:cover;">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <h1 class="bnr-txt">Your database<br />
                            secure and customised<br />
                            in the cloud</h1>
                    </div>
                    <div class="col-xs-12 col-md-4 col-lg-4 trial-bg">
                        <h1>Free Trial</h1>
                        <p><asp:Label ID="lblMessage" runat="server" 
                            Text="Fill in the form and we’ll contact you within 1 business day to schedule a free introductory session:"></asp:Label></p>
                        <div class="form-group">
                            <asp:TextBox ID="txtName" CssClass="form-control form-inpt" placeholder="Name*" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtEmail" CssClass="form-control form-inpt" placeholder="Email*" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="txtPhone" CssClass="form-control form-inpt" placeholder="Phone" runat="server"></asp:TextBox>
                        </div>
                        <br />
                        <asp:Button ID="btnSignUp" CssClass="btn btn-primary" Text="Sign Up" runat="server" OnClick="btnSignUp_Click" ValidationGroup="Form1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="White"
                                ErrorMessage="*Please enter your Name."   ControlToValidate="txtName" ValidationGroup="Form1">
                            </asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2"   runat="server" ForeColor="White"
                                ErrorMessage="<br/>*Please enter your Email." ControlToValidate="txtEmail" ValidationGroup="Form1">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regexEmailValid"   runat="server"
                                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                ControlToValidate="txtEmail" ErrorMessage="<br/>Please enter a valid email address." ValidationGroup="Form1">
                            </asp:RegularExpressionValidator>
                    </div>

                </div>
            </div>
        </div>
        
				<div class="container-fluid pad-tb-30-70" style="background:url(Marketing/images/video-section-bg.png) 64% 72% repeat-x;">
                    <div class="container">
                    	<div class="row">
                       	  <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 text-center ets-txt">
                            <h3>TheDatabase Platform</h3>
                            <p>We have created our own database platform that allows us to create database prototypes in a few hours. These can then be developed with your feedback towards a final solution.</p>

<p>In most cases we can create about 90% of the solution quickly through configuration. The remaining part we then code to your exact requirements creating a unique customised solution to suit your business.</p>
                            
                            
                             </div>
                        </div>
                    </div>
                    <div class="container-fluid pad-tb-30-70 pad-bt-30 pad-tp-24">
                    	<div class="container slide">
                        	<div class="row">
                            	<div class="carousel slide" id="myCarousel" data-ride="carousel">
                                <ol class="carousel-indicators">
                                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                  <li data-target="#myCarousel" data-slide-to="1"></li>
								  <li data-target="#myCarousel" data-slide-to="2"></li>                                  <li data-target="#myCarousel" data-slide-to="3"></li>
                                  <li data-target="#myCarousel" data-slide-to="4"></li>
                                    </ol>
                                    
                                  <div class="carousel-inner">
                                  	<div class="item active">
                                          <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                            <img src="Marketing/images/slide1.png" class="img-responsive img-thumbnail" alt="Easy to use dashboard" /> 
                                          </div>
                                          <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                                <h2>Configurable Dashboard</h2>
                                                <p>Configure your dashboard to show</p>
                                                <ul>
                                                    <li>Maps</li>
                                                    <li>Graphs</li>
                                                    <li>Tables</li>
                                                    <li>Calendars</li>
                                                    <li>PicturesM</li>
                                                    <li>Text</li>
                                                </ul>
                                                <p>Dashboards and be configured by user role or by individual user.</p>
                                          </div>
                                    </div>
                                    
                                    <div class="item">
                                          <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                            <img src="Marketing/images/slide2.png" class="img-responsive img-thumbnail" alt="Configurable Tables and Sample Types" /> 
                                          </div>
                                          <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                                <h2>Configurable Data</h2>
                                                <ul>
                                                    <li>Add tables or fields in real time </li>
<li>Edit field attributes</li>
<li>Many different types of fields (dropdowns, radio buttons, file uploads etc.)</li>
<li>Many types of validation available</li>
<li>Calculations and formulas</li>
<li>Users can choose their own columns and filters to show</li>
                                                </ul>
                                          </div>
                                    </div>
                                    
                                    
                                    <div class="item">
                                          <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                            <img src="Marketing/images/slide3.png" class="img-responsive img-thumbnail" alt="Tables and graphs" /> 
                                          </div>
                                          <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                                <h2>Configurable Menus</h2>
                                                <p>Create menus to show any of the following:</p>
                                                <ul>
                                                    <li>Record Listing Pages</li>
                                                    <li>Document Repositories</li>
                                                    <li>Reports you have created</li>
                                                    <li>Links to other</li>
                                                    <li>Sub-menus and dividers</li>
                                                </ul>
                                          </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="item">
                                          <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                            <img src="Marketing/images/slide4.png" class="img-responsive img-thumbnail" alt="Schedule and Sensors" /> 
                                          </div>
                                          <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                                <h2>Configurable Calendars</h2>
                                                <ul>
                                                    <li>Add calendars to show upcoming events</li>
<li>Drill down into events</li>
<li>Highlight events based on data values </li>
                                                </ul>
                                          </div>
                                    </div>
                                    
                                    
                                    <div class="item">
                                          <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                            <img src="Marketing/images/slide5.png" class="img-responsive img-thumbnail" alt="Security features" /> 
                                          </div>
                                          <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                                <h2>Customisable Graphs</h2>
                                                <ul>
                                                    <li>Line, area, column, bar, stacked, pie, scatter, bubble, dynamic, 3d, gauges</li>
<li>See Here for possible graphs</li>
<li>Add them to your dashboard for instant access</li>
                                                </ul>
                                          </div>
                                          
                                    </div>
                                    
                                         
                                      </div>

                   			 </div>

                            </div>

                        </div>

                    </div>
                 </div>   
                 
                 
                 
              
                 
                 <div class="container-fluid pad-tb-30-70" style="background:#f8f8f8;">
                    <div class="container">
                    	<div class="row spsheet">                        	
                            <div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 text-center ets-txt">
                            	<h2 class="text-center">Database Services</h2>
                                <hr />
                            </div>
						</div>
                        <div class="row">
                <div class="features">
                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap text-center">
                            <i class="fa fa-comments-o"></i>
                            <h3>Meeting Your Requirements</h3>
                            <p>In most cases we can configure our database platform to meet your requirements but if not we can customise it to meet your needs.</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap text-center">
                            <i class="fa fa-check-square"></i>
                            <h3>Robust/Tested Code</h3>
                            <p>3 years in the making our database platforms have undergone 100s of hours of testing and have now reached a level of maturity.</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap text-center">
                            <i class="fa fa-clock-o"></i>
                            <h3>Fast Setup Time</h3>
                            <p>Quick configuration and a large number of standard components can deliver a working prototype in a few days giving you assurance and controlling costs.</p>
                        </div>
                    </div>
                
                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap text-center">
                            <i class="fa fa-leaf"></i>
                            <h3>Flexible and Configurable</h3>
                            <p>The best thing about our systems are that they are not fixed – you can add columns, add tables, change validation, messages and much more… by yourself at NO COST!</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap text-center">
                            <i class="fa fa-globe"></i>
                            <h3>Accessible Anywhere Any device</h3>
                            <p>As a Cloud based solution you can access it from anywhere that has an internet connection: Phone, Tablet, Laptop or Desktop.</p>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 600ms; animation-name: fadeInDown;">
                        <div class="feature-wrap text-center">
                            <i class="fa fa-lock"></i>
                            <h3>Secure</h3>
                            <p>Our sophisticated security system ensures that only the right people access the data they are allowed to.</p>
                        </div>
                    </div>
                </div>
            </div>
                    </div>
                 </div>     
                 
                 
                 
                 
                 
                 <div class="container easy-way pad-tp-30">
                 	<div class="row">
                    	<div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 text-center ets-txt">
                           	<h2 class="text-center">One Database<br />Many Applications</h2>
                            <hr />
                            </div>
                     </div>       
                    
			</div>
            <div class="container">		
                   <div class="row pad-tb-20">
                   		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        	<div class="easy-clmn-one">
                            	<h3><strong>Environmental</strong></h3>
                            	<ul>
                                	<li>Monitoring</li>
                                    <li>Compliance</li>
                                    <li>Reporting</li>
                                    <li>Alarms</li>
                                    <li>Change of Custody</li>
								</ul>
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 mr-tb-10">
                        	<div class="easy-clmn-two">
                            	<h3><strong>Medical</strong></h3>
                            	<ul>
                                	<li>Assessments</li>
                                    <li>Questionnaires</li>
                                    <li>Patient Data</li>
                                    <li>Test Results</li>
                                    <li>Consent Forms</li>
								</ul>
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        	<div class="easy-clmn-three">
                            	<h3><strong>Corporate</strong></h3>
                            	<ul>
                                	<li>Job Control</li>
                                    <li>Project Management</li>
                                    <li>Health and Safety</li>
                                    <li>Event Organisation</li>
                                    <li>Fincancial Tracking</li>
								</ul>
                            </div>
                        </div>
                     </div>
                     
                     
                     <div class="row">
                     		<div class="col-sm-6 col-md-4 col-md-offset-2 col-lg-4 col-lg-offset-2 grow text-center">
                                <img src="Marketing/images/grow.png" width="165" height="165" class="center-block mr-tb-60" /> 
                                <h2>Freedom to grow</h2>
                                <p>TheDatabse is configurable and you can adjust the system as your requirements change. So you won’t need to worry about investing more time and resources if you outgrow your current set-up.</p>
                        </div>


							<div class="col-sm-6 col-md-4 col-lg-4 grow text-center">
                                <img src="Marketing/images/lock.png" width="165" height="165" class="center-block mr-tb-60" /> 
                                <h2>We won’t lock you in</h2>
                                <p>With our software you control your data and you can download it at any time. So if do you want to change your telemetry equipment or data providers, your data is safe and secure in one location.</p>
                        </div>
                                             

                                      	
                     </div>
                     
                     <div class="row">
                     	<div id="client"></div>
                     </div>
    
                 </div>  


					<hr />

                  
                  
                    <div class="container clients">
                    	<div class="row">
                        	<div class="col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 text-center">
                            <h2>Our Customers</h2>
                            	<hr />
                            <p>This environmental reporting system has been specifically designed for<br />mining and heavy industry.  Some of our users include:</p>
                            </div>
                        </div>
                        
                        <div class="row mr-tb-40">
                        	<div class="col-sm-3 col-md-3 col-lg-3">
                       	    	<img src="Marketing/images/partner1.png" class="img-responsive mr-tp-22" alt="glencore" />
                            </div>
                             
                             <div class="col-sm-3 col-md-3 col-lg-3">
		
        						<img src="Marketing/images/partner22.png" class="img-responsive" alt="RioTinto" />
                             </div>
                             
                             
                             <div class="col-sm-3 col-md-3 col-lg-3">
                       	    	<img src="Marketing/images/partner33.png" class="img-responsive" alt="BMA" />
                             </div>
                             
                             <div class="col-sm-3 col-md-3 col-lg-3">
                       	    	<img src="Marketing/images/partner44.png" class="img-responsive" alt="Gauge" />
                             </div>
                        </div>
                        
                    </div>
                    
                    
                    
                    
                    <div class="container-fluid pad-tb-20" style="background:#6e6e6e;" id="features">
                    	<div class="container">
                        	<div class="row signup">
                            	<div class="col-sm-6 col-md-6 col-lg-6 mr-left-4-1">
                                	<h3>Want to learn more?</h3>
                                    <p>Sign up for a free trial and we'll take you through a free introductory session</p>
                                </div>
                                
								<div class="col-sm-6 col-md-4 col-lg-4 mr-left-8-3">
                                	<a href="http://www.dbgurus.com.au/ContactUs.html" target="_blank" class="btn btn-success btn-lg">Contact Us</a>
                                </div>
                            </div>
                        </div>
                    </div>

					<div class="container pad-tp-30">
                    	<div class="row reporting">
                        	<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 text-center">
                            	<h2>Why you'll love TheDatabase</h2>
								<hr />
                                
                                <h4><strong>Reporting is now easier than ever</strong></h4>
                                <p>This system is quick and easy to set up and requires no training. Your team can be brought up to speed in no time, and what’s more, contractors or casual staff can hit the ground running. That way you’re happy, and all the Rogers of the world are happy, too.</p>
                            </div>
                        </div>
                        <div class="row report-column mr-tp-42">
                        	<div class="col-sm-4 col-md-4 col-lg-4">
                       	    	<img src="Marketing/images/import.png" width="75" height="75" />
                                <h5><strong>Import data from spreadsheets or upload data straight into the system</strong></h5>
                                <ul>
                                	<li>Drag in your graphs</li>
                                    <li>Drop in tables</li>
                                    <li>Paste in your text</li>
                                    <li>Upload a few photos</li>
                                    <li>Publish to Word or the web</li>
                                </ul>
                             </div>  
                             
                             
                             <div class="col-sm-4 col-md-4 col-lg-4 mr-tp-30">
                       	    	<img src="Marketing/images/automated.png" width="75" height="75" />
                                <h5><strong>Automated Feeds and Telemetry</strong></h5>
                                <p>We make it simple to bring in data from other systems and from remote sensors:</p>
                                <ul>
                                	<li>Automatically upload from email or FTP</li>
                                    <li>Automate validation on entry</li>
                                    <li>Automatic notifications including warnings and alerts</li>
                                </ul>
                             </div>
                             
                             
                             <div class="col-sm-4 col-md-4 col-lg-4">
                       	    	<img src="Marketing/images/accessible.png" width="75" height="75" />
                                <h5><strong>Accessible from anywhere</strong></h5>
                               <p>TheDatabase is entirely web based. This means everything is in the one place and you can reach it from anywhere. So your team can upload data directly from the site, even in remote locations. 
Perfect for if you have more than one site or different reporters at various locations and need a centralised solution. </p>
                             </div>                       

                        </div>
                        
                        
                        <hr />
                        
                        <div class="row reporting">
                        	<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 text-center">
                            	<h4><strong>We give you peace of mind</strong></h4>
								<p>Unsure about cloud security? Cloud-based platforms are a little like air travel. It might seem more dangerous, but because of all the extra precautions we take, it’s actually a lot safer. Here’s what we do to ensure your data is secure in the cloud.</p>
                            </div>
                        </div>
                        
                        <div class="row report-column pad-tb-30-70">
                        	<div class="col-sm-4 col-md-4 col-lg-4">
                       	    	<img src="Marketing/images/safe.png" width="75" height="75" />
                                <h5><strong>Your data is safe</strong></h5>
                                <ul>
                                	<li>We do hourly backups of the database as well as daily backups to an offsite location to ensure there is no single point of failure.</li>

<li>You control who can access your sensitive data and what each user can do in each menu.</li>

<li>TheDatabase does not allow data to be permanently deleted and all changes are tracked so you can see who changed what and why.</li>
                                </ul>
                             </div>  
                             
                             
                             <div class="col-sm-4 col-md-4 col-lg-4 mr-tp-30">
                       	    	<img src="Marketing/images/alert.png" width="75" height="75" />
                                <h5><strong>Alerts and warnings</strong></h5>
                                <p>We keep you informed of problems by email and SMS so you can respond quickly.</p>
                                <ul>
                                	<li>When limits have been breached!</li>
                                    <li>Invalid data or unlikely readings</li>
                                    <li>Broken Monitors (flat lining)</li>
                                    <li>Late Data / Missed Scheduled</li>
                                    <li>Sent via Email or SMS</li>
                                </ul>
                             </div>
                             
                             
                             <div class="col-sm-4 col-md-4 col-lg-4">
                       	    	<img src="Marketing/images/support.png" width="75" height="75" />
                                <h5><strong>Support</strong></h5>
                               <p>And if there’s a problem you can’t solve, we’re here to help. All our customers get free technical support. Depending on the level of support you need your plan will include email or phone support from one of our skilled technicians.</p>
                             </div>     
                        	
                        </div>
                    </div>
                    
                    
                    
                    
                    
                    
                    <div class="container-fluid pad-tb-30-70 pad-bt-30 pad-tp-24" style="background:#03acff;">
                    	<div class="container slide white-txt">
                        	<div class="row">
                            	<div class="carousel slide" id="btCarousel" data-ride="carousel">
                                <ol class="carousel-indicators">
                                  <li data-target="#btCarousel" data-slide-to="0" class="active"></li>
                                  <li data-target="#btCarousel" data-slide-to="1"></li>
								  <li data-target="#btCarousel" data-slide-to="2"></li>
                                  <li data-target="#btCarousel" data-slide-to="3"></li>
                                  <li data-target="#btCarousel" data-slide-to="4"></li>
                                    </ol>
                                    
                                  <div class="carousel-inner">
                                  	<div class="item active">
                                          <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                            <img src="Marketing/images/slider2-1.png" class="img-responsive img-thumbnail" alt="Easy to use dashboard" /> 
                                          </div>
                                          <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                                <h2>Configurable Imports</h2>
                                                <ul>
                                                    <li>Create import templates</li>
                                                    <li>Select fields to import</li>
                                                    <li>Multiple types of validation</li>
                                                    <li>Many types of warnings</li>
                                                    <li>Records holding area before entry</li>
                                                    <li>Optional notification of upload or entry</li>
                                                </ul>
                                          </div>
                                    </div>
                                    
                                    <div class="item">
                                          <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                            <img src="Marketing/images/slider2-2.png" class="img-responsive img-thumbnail" alt="Configurable Tables and Sample Types" /> 
                                          </div>
                                          <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                                <h2>Exports</h2>
                                                <ul>
                                                    <li>Create your own export templates</li>
                                                    <li>Tick the fields you want</li>
                                                    <li>Choose the rows to export</li>
                                                    <li>Output as CSV, XLS, Word or PDF</li>
                                                    <li>Data dump of all connected tables</li>
                                                    <li>Export graphs into Excel format</li>
                                                </ul>
                                          </div>
                                    </div>
                                    
                                    
                                    <div class="item">
                                          <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                            <img src="Marketing/images/slider2-3.png" class="img-responsive img-thumbnail" alt="Tables and graphs" /> 
                                          </div>
                                          <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                                <h2>Reports</h2>
                                                <ul>
                                                    <li>Power of SQL Server Reporting Services</li>
                                                    <li>Listings</li>
                                                    <li>Graphs</li>
                                                    <li>Tables</li>
                                                    <li>Groupings</li>
                                                    <li>Calculations</li>
                                                </ul>
                                          </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="item">
                                          <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                            <img src="Marketing/images/slider2-4.png" class="img-responsive img-thumbnail" alt="Schedule and Sensors" /> 
                                          </div>
                                          <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                                <h2>Advanced Security</h2>
                                                <ul>
                                                    <li>Password protection</li>
                                                    <li>Simple built in roles</li>
                                                    <li>Configurable roles</li>
                                                    <li>Control who can view, edit & export</li>
                                                    <li>2 stage delete process</li>
                                                    <li>Full audit of changes</li>
                                                </ul>
                                          </div>
                                    </div>
                                    
                                    
                                    <div class="item">
                                          <div class="col-lg-7 col-xs-12 col-md-7 col-sm-6">
                                            <img src="Marketing/images/slider2-5.png" class="img-responsive img-thumbnail" alt="Security features" /> 
                                          </div>
                                          <div class="col-lg-5 col-xs-12 col-md-5 col-sm-6">
                                                <h2>Communications</h2>
                                                <ul>
                                                    <li>Send messages by emails or SMS</li>
                                                    <li>Edit content with WYSIWYG editor</li>
                                                    <li>Embed data like a mail merge</li>
                                                    <li>Generate Word documents as attachments</li>
                                                    <li>Alerts and warnings</li>
                                                    <li>Reminders and notifications</li>
                                                </ul>
                                          </div>
                                          
                                    </div>
                                    
                                         
                                      </div>

                   			 </div>

                            </div>

                        </div>

                    </div>    
                    

           <div class="container pad-tp-30" id="pricing">
                    	<div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 btm-form-bg">
                                <h1>Got a question?</h1>
                                <p><asp:Label ID="lblMessage2" runat="server" Text="Fill in the form below and we’ll get back to you within 1 business day."></asp:Label></p>
                                <div class="form-group">
                                    <asp:TextBox ID="txtName2" CssClass="form-control form-inpt" placeholder="Name*" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="txtEmail2" CssClass="form-control form-inpt" placeholder="Email*" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox TextMode="MultiLine" rows="6" name="txtMessage" ID="txtMessage" CssClass="form-control form-inpt" placeholder="Question*" runat="server"></asp:TextBox>
                                </div>
                                <br />
                                <asp:Button ID="btnSignUp2" CssClass="btn btn-primary" Text="Send" runat="server" OnClick="btnSignUp2_Click" ValidationGroup="Form2" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ForeColor="White"
                                                            ErrorMessage="*Please enter your Name." ControlToValidate="txtName2" ValidationGroup="Form2">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ForeColor="White"
                                                            ErrorMessage="<br/>*Please enter your Email address." ControlToValidate="txtEmail2" ValidationGroup="Form2">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ForeColor="White"
                                                            ErrorMessage="<br/>*Please enter your Question." ControlToValidate="txtMessage" ValidationGroup="Form2">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                ControlToValidate="txtEmail2" ErrorMessage="<br/>Please enter a valid Email address." ValidationGroup="Form2">
                                </asp:RegularExpressionValidator>

                            </div>

                             
                             
                             
                             <div class="col-sm-6 col-md-6 col-md-offset-1 col-lg-6 col-lg-offset-1 signup-text">
                    <h2>Contact Us</h2>
                    <hr class="pull-left" />

                    <div class="text-one">
                        <img src="Marketing/images/one.png" class="pull-left" width="41" height="41"/>
                        <h3>Got a question?</h3>
                        <p>Please fill in the form on the right with any questions you have and we’ll get back to you within 1 business day with some answers. Please note we will not pass your details on to any other party.</p>
                        <br/>
</div>
<div class="text-two">
                        <img src="Marketing/images/two.png" class="pull-left" width="41" height="41"/>
                        <h3>Quick Demo?</h3>
                        <p>Would you like a quick half hour demo of the system online? The introductory session can do it remotely over the phone. We’ll share screens and show you how it works.</p>
                        <br/>
</div>
<div class="text-three">
                        <img src="Marketing/images/three.png" class="pull-left" width="41" height="41"/>
                        <h3>Free Trial?</h3>
                        <p>Do you want to give it a try free for one month. If you like it at the end of the trial, we’ll discuss which plan is best for you.</p>
                        <p><strong>No credit card details required</strong></p>
                    </div> 
                </div>
                        </div>
                    </div>

        <div class="container-fluid" style="background: #ebebeb;">
            <div class="row">
                <div class="col-md-12 text-center">
                                   <address class="lead">Copyright&copy; <%=DateTime.Now.Year.ToString() %>. All rights reserved.<br />
<a href="http://www.dbgurus.com.au/" target="_blank"><strong>DB Gurus Pty Ltd</strong>.</a>, Australia. ACN 134 593 712<br />
Phone <a href="tel:1800901096">1800 90 10 96​</a>  <br />
Int:<a href="tel:+61242685672">+61 2 4268 5672</a></address>
                </div>
            </div>

        </div>
    </form>   
</body>
</html>
